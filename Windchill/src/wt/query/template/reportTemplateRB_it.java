package wt.query.template;

import wt.util.resource.*;

@RBUUID("wt.query.template.reportTemplateRB")
public final class reportTemplateRB_it extends WTListResourceBundle {
   /**
/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
    * Report Template Names
    **/
   @RBEntry("Utenti disabilitati")
   public static final String PRIVATE_CONSTANT_0 = "Disabled Users";

   @RBEntry("Riepilogo autori iterazioni")
   public static final String PRIVATE_CONSTANT_1 = "Iterated Author Summary";

   @RBEntry("Elementi in ritardo della richiesta di modifica")
   public static final String PRIVATE_CONSTANT_2 = "Overdue Change Request Work";

   @RBEntry("Processi in ritardo")
   public static final String PRIVATE_CONSTANT_3 = "Overdue Processes";

   @RBEntry("Processi in esecuzione in ritardo")
   public static final String PRIVATE_CONSTANT_4 = "Overdue Running Processes";

   @RBEntry("Elementi in ritardo per categoria")
   public static final String PRIVATE_CONSTANT_5 = "Overdue Work By Category";

   @RBEntry("Baseline parte")
   public static final String PRIVATE_CONSTANT_6 = "Part Baselines";

   @RBEntry("Versioni parte")
   public static final String PRIVATE_CONSTANT_7 = "Part Versions";

   @RBEntry("Processi avviati dall'utente attuale")
   public static final String PRIVATE_CONSTANT_8 = "Processes Initiated by Current User";

   @RBEntry("Richieste di modifica del progetto")
   public static final String PRIVATE_CONSTANT_9 = "Project Change Requests";

   @RBEntry("Processi del progetto")
   public static final String PRIVATE_CONSTANT_10 = "Project Processes";

   @RBEntry("Riepilogo utenti")
   public static final String PRIVATE_CONSTANT_11 = "User Summary";

   @RBEntry("Tutte le richieste di modifica aperte (contesto corrente)")
   public static final String PRIVATE_CONSTANT_12 = "Open Change Requests";

   @RBEntry("Tutte le notifiche di modifica aperte (contesto corrente)")
   public static final String PRIVATE_CONSTANT_13 = "Open Change Notices";

   @RBEntry("Tutti i report di problema aperti (contesto corrente)")
   public static final String PRIVATE_CONSTANT_14 = "Open Problem Reports";

   @RBEntry("Tutte le richieste di modifica aperte (intero sistema)")
   public static final String PRIVATE_CONSTANT_15 = "All Open Change Requests";

   @RBEntry("Tutte le notifiche di modifica aperte (intero sistema)")
   public static final String PRIVATE_CONSTANT_16 = "All Open Change Notices";

   @RBEntry("Tutte le richieste di modifica aperte (contesto corrente)")
   public static final String PRIVATE_CONSTANT_17 = "Open ECRs";

   @RBEntry("Tutte le notifiche di modifica aperte (contesto corrente)")
   public static final String PRIVATE_CONSTANT_18 = "Open ECNs";

   @RBEntry("Tutti i report di problema aperti (intero sistema)")
   public static final String PRIVATE_CONSTANT_19 = "All Open Problem Reports";

   @RBEntry("Tutte le richieste di modifica aperte (intero sistema)")
   public static final String PRIVATE_CONSTANT_20 = "All Open ECRs";

   @RBEntry("Tutte le notifiche di modifica aperte (intero sistema)")
   public static final String PRIVATE_CONSTANT_21 = "All Open ECNs";

   @RBEntry("Tempo medio di completamento di un report di problema (contesto corrente)")
   public static final String PRIVATE_CONSTANT_22 = "Average Problem Report Completion Time";

   @RBEntry("Tempo medio di completamento di una richiesta di modifica di (contesto corrente)")
   public static final String PRIVATE_CONSTANT_23 = "Average ECR Completion Time";

   @RBEntry("Tempo medio di completamento di una notifica di modifica (contesto corrente)")
   public static final String PRIVATE_CONSTANT_24 = "Average ECN Completion Time";

   @RBEntry("Tempo medio di completamento di un report di problema (intero sistema)")
   public static final String PRIVATE_CONSTANT_25 = "All Average Problem Report Completion Time";

   @RBEntry("Tempo medio di completamento di una richiesta di modifica (intero sistema)")
   public static final String PRIVATE_CONSTANT_26 = "All Average ECR Completion Time";

   @RBEntry("Tempo medio di completamento di una notifica di modifica (intero sistema)")
   public static final String PRIVATE_CONSTANT_27 = "All Average ECN Completion Time";

   @RBEntry("Tempo medio di completamento di una richiesta di modifica di (contesto corrente)")
   public static final String PRIVATE_CONSTANT_28 = "Average Change Request Completion Time";

   @RBEntry("Tempo medio di completamento di una notifica di modifica (contesto corrente)")
   public static final String PRIVATE_CONSTANT_29 = "Average Change Notice Completion Time";

   @RBEntry("Tempo medio di completamento di una richiesta di modifica (intero sistema)")
   public static final String PRIVATE_CONSTANT_30 = "All Average Change Request Completion Time";

   @RBEntry("Tempo medio di completamento di una notifica di modifica (intero sistema)")
   public static final String PRIVATE_CONSTANT_31 = "All Average Change Notice Completion Time";

   @RBEntry("Report richieste delle mie parti")
   public static final String PRIVATE_CONSTANT_101 = "My Part Requests Report";

   @RBEntry("Report richieste parti attive")
   public static final String PRIVATE_CONSTANT_102 = "Active Part Requests Report";

   /**
    * Column Names
    **/
   @RBEntry("Attività")
   public static final String PRIVATE_CONSTANT_32 = "Activity";

   @RBEntry("Nome autenticazione")
   public static final String PRIVATE_CONSTANT_33 = "Authentication Name";

   @RBEntry("Baseline")
   public static final String PRIVATE_CONSTANT_34 = "Baseline";

   @RBEntry("Richiesta di modifica")
   public static final String PRIVATE_CONSTANT_35 = "Change Request";

   @RBEntry("Data creazione")
   public static final String PRIVATE_CONSTANT_36 = "Created";

   @RBEntry("Scadenza")
   public static final String PRIVATE_CONSTANT_37 = "Deadline";

   @RBEntry("E-mail")
   public static final String PRIVATE_CONSTANT_38 = "EMail";

   @RBEntry("Ora di fine")
   public static final String PRIVATE_CONSTANT_39 = "End Time";

   @RBEntry("Iterazione")
   public static final String PRIVATE_CONSTANT_40 = "Iteration";

   @RBEntry("Autore modifiche")
   public static final String PRIVATE_CONSTANT_41 = "Last Modified By";

   @RBEntry("Autore modifiche")
   public static final String PRIVATE_CONSTANT_42 = "Last Updated By";

   @RBEntry("Data ultima modifica")
   public static final String PRIVATE_CONSTANT_43 = "Last Updated";

   @RBEntry("Data ultima modifica")
   public static final String PRIVATE_CONSTANT_44 = "Last Update";

   @RBEntry("Nome")
   public static final String PRIVATE_CONSTANT_45 = "Name";

   @RBEntry("Numero")
   public static final String PRIVATE_CONSTANT_46 = "Number";

   @RBEntry("Proprietario")
   public static final String PRIVATE_CONSTANT_47 = "Owner";

   @RBEntry("Priorità")
   public static final String PRIVATE_CONSTANT_48 = "Priority";

   @RBEntry("Processo")
   public static final String PRIVATE_CONSTANT_49 = "Process";

   @RBEntry("Progetto")
   public static final String PRIVATE_CONSTANT_50 = "Project";

   @RBEntry("Ora d'inizio")
   public static final String PRIVATE_CONSTANT_51 = "Start Time";

   @RBEntry("Stato")
   public static final String PRIVATE_CONSTANT_52 = "Status";

   @RBEntry("Versione")
   public static final String PRIVATE_CONSTANT_53 = "Version";

   @RBEntry("Vista")
   public static final String PRIVATE_CONSTANT_54 = "View";

   @RBEntry("Anno")
   public static final String PRIVATE_CONSTANT_55 = "Year";

   @RBEntry("Mese")
   public static final String PRIVATE_CONSTANT_56 = "Month";

   @RBEntry("Tipo")
   public static final String PRIVATE_CONSTANT_57 = "Type";

   @RBEntry("Data creazione")
   public static final String PRIVATE_CONSTANT_100 = "Created On";

   /**
    * Parameter Names
    **/
   @RBEntry("Categoria")
   public static final String PRIVATE_CONSTANT_58 = "Category";

   @RBEntry("Numero di parte")
   public static final String PRIVATE_CONSTANT_59 = "Part Number";

   @RBEntry("Stato del ciclo di vita")
   public static final String PRIVATE_CONSTANT_60 = "State";

   @RBEntry("Dominio team")
   public static final String PRIVATE_CONSTANT_61 = "Team Domain";

   @RBEntry("Nome team")
   public static final String PRIVATE_CONSTANT_62 = "Team Name";

   @RBEntry("Report di problema")
   public static final String PRIVATE_CONSTANT_63 = "Problem Report";

   @RBEntry("Notifica di modifica")
   public static final String PRIVATE_CONSTANT_64 = "Change Notice";

   @RBEntry("Data di creazione")
   public static final String PRIVATE_CONSTANT_65 = "Creation Date";

   @RBEntry("Autore")
   @RBComment("The author of the change object")
   public static final String PRIVATE_CONSTANT_66 = "Creator";

   @RBEntry("Contesto")
   @RBComment("The context of the change object (ie. the container)")
   public static final String PRIVATE_CONSTANT_67 = "Context";

   @RBEntry("Conteggio")
   @RBComment("The total number of items used in the query.  Should be the same as Total above.")
   public static final String PRIVATE_CONSTANT_68 = "Count";

   @RBEntry("Data di fine")
   @RBComment("The label for the String \"End Date\"")
   public static final String PRIVATE_CONSTANT_69 = "End Date";

   @RBEntry("Data di inizio")
   @RBComment("The label for the beginning date of the report")
   public static final String PRIVATE_CONSTANT_70 = "Begin Date";

   @RBEntry("Contesto")
   @RBComment("The label for the list of containers.")
   public static final String PRIVATE_CONSTANT_71 = "Containers";

   @RBEntry("Media (giorni)")
   @RBComment("The average number of days as calculated")
   public static final String PRIVATE_CONSTANT_72 = "Average (Days)";

   @RBEntry("Conteggio")
   @RBComment("The total number of items used in the query")
   public static final String PRIVATE_CONSTANT_73 = "Total";

   @RBEntry("Dominio")
   @RBComment("The domain of the object")
   public static final String PRIVATE_CONSTANT_74 = "Domain";
}
