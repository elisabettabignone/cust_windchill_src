package wt.query.template;

import wt.util.resource.*;

@RBUUID("wt.query.template.reportTemplateRB")
public final class reportTemplateRB extends WTListResourceBundle {
   /**
/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
    * Report Template Names
    **/
   @RBEntry("Disabled Users")
   public static final String PRIVATE_CONSTANT_0 = "Disabled Users";

   @RBEntry("Iterated Author Summary")
   public static final String PRIVATE_CONSTANT_1 = "Iterated Author Summary";

   @RBEntry("Overdue Change Request Work")
   public static final String PRIVATE_CONSTANT_2 = "Overdue Change Request Work";

   @RBEntry("Overdue Processes")
   public static final String PRIVATE_CONSTANT_3 = "Overdue Processes";

   @RBEntry("Overdue Running Processes")
   public static final String PRIVATE_CONSTANT_4 = "Overdue Running Processes";

   @RBEntry("Overdue Work By Category")
   public static final String PRIVATE_CONSTANT_5 = "Overdue Work By Category";

   @RBEntry("Part Baselines")
   public static final String PRIVATE_CONSTANT_6 = "Part Baselines";

   @RBEntry("Part Versions")
   public static final String PRIVATE_CONSTANT_7 = "Part Versions";

   @RBEntry("Processes Initiated by Current User")
   public static final String PRIVATE_CONSTANT_8 = "Processes Initiated by Current User";

   @RBEntry("Project Change Requests")
   public static final String PRIVATE_CONSTANT_9 = "Project Change Requests";

   @RBEntry("Project Processes")
   public static final String PRIVATE_CONSTANT_10 = "Project Processes";

   @RBEntry("User Summary")
   public static final String PRIVATE_CONSTANT_11 = "User Summary";

   @RBEntry("All Open Change Requests (Current Context)")
   public static final String PRIVATE_CONSTANT_12 = "Open Change Requests";

   @RBEntry("All Open Change Notices (Current Context)")
   public static final String PRIVATE_CONSTANT_13 = "Open Change Notices";

   @RBEntry("All Open Problem Reports (Current Context)")
   public static final String PRIVATE_CONSTANT_14 = "Open Problem Reports";

   @RBEntry("All Open Change Requests (Entire System)")
   public static final String PRIVATE_CONSTANT_15 = "All Open Change Requests";

   @RBEntry("All Open Change Notices (Entire System)")
   public static final String PRIVATE_CONSTANT_16 = "All Open Change Notices";

   @RBEntry("All Open Change Requests (Current Context)")
   public static final String PRIVATE_CONSTANT_17 = "Open ECRs";

   @RBEntry("All Open Change Notices (Current Context)")
   public static final String PRIVATE_CONSTANT_18 = "Open ECNs";

   @RBEntry("All Open Problem Reports (Entire System)")
   public static final String PRIVATE_CONSTANT_19 = "All Open Problem Reports";

   @RBEntry("All Open Change Requests (Entire System)")
   public static final String PRIVATE_CONSTANT_20 = "All Open ECRs";

   @RBEntry("All Open Change Notices (Entire System)")
   public static final String PRIVATE_CONSTANT_21 = "All Open ECNs";

   @RBEntry("Average Problem Report Completion Time (Current Context)")
   public static final String PRIVATE_CONSTANT_22 = "Average Problem Report Completion Time";

   @RBEntry("Average Change Request Completion Time (Current Context)")
   public static final String PRIVATE_CONSTANT_23 = "Average ECR Completion Time";

   @RBEntry("Average Change Notice Completion Time (Current Context)")
   public static final String PRIVATE_CONSTANT_24 = "Average ECN Completion Time";

   @RBEntry("Average Problem Report Completion Time (Entire System)")
   public static final String PRIVATE_CONSTANT_25 = "All Average Problem Report Completion Time";

   @RBEntry("Average Change Request Completion Time (Entire System)")
   public static final String PRIVATE_CONSTANT_26 = "All Average ECR Completion Time";

   @RBEntry("Average Change Notice Completion Time (Entire System)")
   public static final String PRIVATE_CONSTANT_27 = "All Average ECN Completion Time";

   @RBEntry("Average Change Request Completion Time (Current Context)")
   public static final String PRIVATE_CONSTANT_28 = "Average Change Request Completion Time";

   @RBEntry("Average Change Notice Completion Time (Current Context)")
   public static final String PRIVATE_CONSTANT_29 = "Average Change Notice Completion Time";

   @RBEntry("Average Change Request Completion Time (Entire System)")
   public static final String PRIVATE_CONSTANT_30 = "All Average Change Request Completion Time";

   @RBEntry("Average Change Notice Completion Time (Entire System)")
   public static final String PRIVATE_CONSTANT_31 = "All Average Change Notice Completion Time";

   @RBEntry("My Part Requests Report")
   public static final String PRIVATE_CONSTANT_101 = "My Part Requests Report";

   @RBEntry("Active Part Requests Report")
   public static final String PRIVATE_CONSTANT_102 = "Active Part Requests Report";

   /**
    * Column Names
    **/
   @RBEntry("Activity")
   public static final String PRIVATE_CONSTANT_32 = "Activity";

   @RBEntry("Authentication Name")
   public static final String PRIVATE_CONSTANT_33 = "Authentication Name";

   @RBEntry("Baseline")
   public static final String PRIVATE_CONSTANT_34 = "Baseline";

   @RBEntry("Change Request")
   public static final String PRIVATE_CONSTANT_35 = "Change Request";

   @RBEntry("Created")
   public static final String PRIVATE_CONSTANT_36 = "Created";

   @RBEntry("Deadline")
   public static final String PRIVATE_CONSTANT_37 = "Deadline";

   @RBEntry("EMail")
   public static final String PRIVATE_CONSTANT_38 = "EMail";

   @RBEntry("End Time")
   public static final String PRIVATE_CONSTANT_39 = "End Time";

   @RBEntry("Iteration")
   public static final String PRIVATE_CONSTANT_40 = "Iteration";

   @RBEntry("Modified By")
   public static final String PRIVATE_CONSTANT_41 = "Last Modified By";

   @RBEntry("Modified By")
   public static final String PRIVATE_CONSTANT_42 = "Last Updated By";

   @RBEntry("Last Modified")
   public static final String PRIVATE_CONSTANT_43 = "Last Updated";

   @RBEntry("Last Modified")
   public static final String PRIVATE_CONSTANT_44 = "Last Update";

   @RBEntry("Name")
   public static final String PRIVATE_CONSTANT_45 = "Name";

   @RBEntry("Number")
   public static final String PRIVATE_CONSTANT_46 = "Number";

   @RBEntry("Owner")
   public static final String PRIVATE_CONSTANT_47 = "Owner";

   @RBEntry("Priority")
   public static final String PRIVATE_CONSTANT_48 = "Priority";

   @RBEntry("Process")
   public static final String PRIVATE_CONSTANT_49 = "Process";

   @RBEntry("Project")
   public static final String PRIVATE_CONSTANT_50 = "Project";

   @RBEntry("Start Time")
   public static final String PRIVATE_CONSTANT_51 = "Start Time";

   @RBEntry("Status")
   public static final String PRIVATE_CONSTANT_52 = "Status";

   @RBEntry("Version")
   public static final String PRIVATE_CONSTANT_53 = "Version";

   @RBEntry("View")
   public static final String PRIVATE_CONSTANT_54 = "View";

   @RBEntry("Year")
   public static final String PRIVATE_CONSTANT_55 = "Year";

   @RBEntry("Month")
   public static final String PRIVATE_CONSTANT_56 = "Month";

   @RBEntry("Type")
   public static final String PRIVATE_CONSTANT_57 = "Type";

   @RBEntry("Created On")
   public static final String PRIVATE_CONSTANT_100 = "Created On";

   /**
    * Parameter Names
    **/
   @RBEntry("Category")
   public static final String PRIVATE_CONSTANT_58 = "Category";

   @RBEntry("Part Number")
   public static final String PRIVATE_CONSTANT_59 = "Part Number";

   @RBEntry("State")
   public static final String PRIVATE_CONSTANT_60 = "State";

   @RBEntry("Team Domain")
   public static final String PRIVATE_CONSTANT_61 = "Team Domain";

   @RBEntry("Team Name")
   public static final String PRIVATE_CONSTANT_62 = "Team Name";

   @RBEntry("Problem Report")
   public static final String PRIVATE_CONSTANT_63 = "Problem Report";

   @RBEntry("Change Notice")
   public static final String PRIVATE_CONSTANT_64 = "Change Notice";

   @RBEntry("Creation Date")
   public static final String PRIVATE_CONSTANT_65 = "Creation Date";

   @RBEntry("Creator")
   @RBComment("The author of the change object")
   public static final String PRIVATE_CONSTANT_66 = "Creator";

   @RBEntry("Context")
   @RBComment("The context of the change object (ie. the container)")
   public static final String PRIVATE_CONSTANT_67 = "Context";

   @RBEntry("Count")
   @RBComment("The total number of items used in the query.  Should be the same as Total above.")
   public static final String PRIVATE_CONSTANT_68 = "Count";

   @RBEntry("End Date")
   @RBComment("The label for the String \"End Date\"")
   public static final String PRIVATE_CONSTANT_69 = "End Date";

   @RBEntry("Begin Date")
   @RBComment("The label for the beginning date of the report")
   public static final String PRIVATE_CONSTANT_70 = "Begin Date";

   @RBEntry("Context")
   @RBComment("The label for the list of containers.")
   public static final String PRIVATE_CONSTANT_71 = "Containers";

   @RBEntry("Average (Days)")
   @RBComment("The average number of days as calculated")
   public static final String PRIVATE_CONSTANT_72 = "Average (Days)";

   @RBEntry("Count")
   @RBComment("The total number of items used in the query")
   public static final String PRIVATE_CONSTANT_73 = "Total";

   @RBEntry("Domain")
   @RBComment("The domain of the object")
   public static final String PRIVATE_CONSTANT_74 = "Domain";
}
