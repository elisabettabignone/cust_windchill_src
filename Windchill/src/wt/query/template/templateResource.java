package wt.query.template;

import wt.util.resource.*;

@RBUUID("wt.query.template.templateResource")
public final class templateResource extends WTListResourceBundle {
   /**
/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
    * Labels -----------------------------------------------------------------
    * 
    **/
   @RBEntry("Generate Report")
   public static final String GENERATE_REPORT = "100";

   @RBEntry("Generate")
   public static final String REPORT_GENERATE = "101";

   @RBEntry("Standard")
   public static final String REPORT_STANDARD_XSL = "102";

   @RBEntry("Custom")
   public static final String REPORT_CUSTOM_XSL = "103";

   @RBEntry("XSL URL 1:")
   public static final String REPORT_CUSTOM_XSL_URL_1 = "104";

   @RBEntry("XSL URL 2:")
   public static final String REPORT_CUSTOM_XSL_URL_2 = "105";

   @RBEntry("Input Parameters")
   public static final String REPORT_INPUT_PARAMETERS = "106";

   @RBEntry("Output Format")
   public static final String REPORT_OUTPUT_FORMAT = "107";

   @RBEntry("Macro")
   public static final String REPORT_MACRO = "108";

   @RBEntry("Execute Query:")
   public static final String EXECUTE_QUERY = "109";

   @RBEntry("Generate Report:")
   @RBComment("Browser window title for the Report Generation Execute Page; translation can be found in the <TITLE> element of the 7.0 WCL10N template src/wt/query/htmltmpl/report/ExecutePage*.html")
   public static final String EXECUTE_PAGE_TITLE = "110";

   @RBEntry("Report Template")
   public static final String DISPLAY_TYPE = "200";

   @RBEntry("{0}: {1}")
   public static final String DISPLAY_IDENTITY = "201";

   @RBEntry("HTML")
   public static final String STANDARD_XSL_HTML = "HTML";

   @RBEntry("XML")
   public static final String STANDARD_XSL_XML = "XML";

   @RBEntry("CSV (Comma Separated Variable)")
   public static final String STANDARD_XSL_CSV = "CSV";

   @RBEntry("TSV (Tab Separated Variable)")
   public static final String STANDARD_XSL_TSV = "TSV";

   @RBEntry("HTML (with sorting and merging)")
   public static final String STANDARD_XSL_HTML_WITH_SORTING_AND_MERGING = "HTMLWithSortingAndMerging";

   @RBEntry("HTML (with sorting)")
   public static final String STANDARD_XSL_HTML_WITH_SORTING = "HTMLWithSorting";

   @RBEntry("HTML (with merging)")
   public static final String STANDARD_XSL_HTML_WITH_MERGING = "HTMLWithMerging";

   @RBEntry("Microsoft Word 2000 HTML (Portrait)")
   public static final String STANDARD_XSL_HTML_MSWORD_PORTRAIT = "MSWord2000Portrait";

   @RBEntry("Microsoft Word 2000 HTML (Landscape)")
   public static final String STANDARD_XSL_HTML_MSWORD_LANDSCAPE = "MSWord2000Landscape";

   @RBEntry("PDF")
   public static final String STANDARD_XSL_PDF = "PDF";

   @RBEntry("CSV UTF-8 (Comma Separated Variable)")
   public static final String UTF8_XSL_CSV = "CSV_UTF8";

   @RBEntry("TSV UTF-8 (Tab Separated Variable)")
   public static final String UTF8_XSL_TSV = "TSV_UTF8";

   /**
    * Messages ---------------------------------------------------------------
    * 
    **/
   @RBEntry("The XML string is invalid.")
   public static final String INVALID_XML = "0";

   @RBEntry("The template object \"{0}\" is not a valid report template.")
   public static final String INVALID_CONTEXT_OBJECT = "1";

   @RBEntry("The maximum string length limit has been exceeded.")
   public static final String STRING_LENGTH_LIMIT = "2";

   @RBEntry("Access is restricted for this user.")
   public static final String INVALID_ACCESS = "3";

   @RBEntry("Partial representation of actual SQL.")
   public static final String PARTIAL_SQL = "4";

   @RBEntry("The macro name \"{0}\" is not a valid.  The following is a list of valid macro names:{1}")
   public static final String INVALID_MACRO = "5";

   @RBEntry("The timestamp value \"{0}\" could not be parsed.  The value must be parsed using one of the following Java formats: \"{1}\", \"{2}\", \"{3}\", \"{4}\", \"{5}\", \"{6}\", \"{7}\", \"{8}\", \"{9}\", \"{10}\", \"{11}\", \"{12}\" , \"{13}\", \"{14}\", \"{15}\", \"{16}\" .")
   public static final String INVALID_DATE_PARSE_VALUE = "6";

   @RBEntry("Invalid XSL format stream entry: resource=\"{0}\" locale=\"{1}\" path=\"{2}\".")
   public static final String INVALID_XSL_FORMAT = "7";

   @RBEntry("Report definition not found: \"{0}\".")
   public static final String REPORT_DEF_NOT_FOUND = "8";

   @RBEntry("ExportedReportArtifacts.")
   public static final String EXPORTED_REPORT_ARTIFACTS = "9";

   @RBEntry("The Report Template \"{0}\" cannot be modified because a heading name \"{1}\" has been removed and that heading is used by Report Template \"{2}\".")
   public static final String INVALID_VIEW_COLUMN_MODIFICATION = "20";

   @RBEntry("The Report Template \"{0}\" cannot be modified because the name has been changed from \"{1}\" and it used by another Report Template.")
   public static final String INVALID_VIEW_NAME_MODIFICATION = "21";

   @RBEntry("The Report Template \"{0}\" cannot be modified because the context has been changed from \"{1}\" to \"{2}\" and it used by another Report Template.")
   public static final String INVALID_VIEW_CONTEXT_MODIFICATION = "22";


   /**
    * Preference Labels ---------------------------------------------------------------
    * 
    **/
   @RBEntry("Reports")
   public static final String REPORTS_CATEGORY = "REPORTS_CATEGORY";

   @RBEntry("XSL Formatting Objects Report Processor")
   public static final String XSLFO_NAME = "XSLFO_NAME";

   @RBEntry("Specifies the XSL formatting objects report processor class name.")
   public static final String XSLFO_SHORT_DESCRIPTION = "XSLFO_SHORT_DESCRIPTION";

   @RBEntry("This preference specifies the class that will be instantiated and used as the XSL formatting objects report processor.")
   public static final String XSLFO_LONG_DESCRIPTION = "XSLFO_LONG_DESCRIPTION";

   @RBEntry("SVG Report Processor")
   public static final String SVG_NAME = "SVG_NAME";

   @RBEntry("Specifies the SVG report processor class name.")
   public static final String SVG_SHORT_DESCRIPTION = "SVG_SHORT_DESCRIPTION";

   @RBEntry("This preference specifies the class that will be instantiated and used as the SVG report processor.")
   public static final String SVG_LONG_DESCRIPTION = "SVG_LONG_DESCRIPTION";

   @RBEntry("Report Raster Format")
   public static final String RASTER_FORMAT_NAME = "RASTER_FORMAT_NAME";

   @RBEntry("Specifies the raster format for SVG report output.")
   public static final String RASTER_FORMAT_SHORT_DESCRIPTION = "RASTER_FORMAT_SHORT_DESCRIPTION";

   @RBEntry("This preference specifies the raster format that will be used for SVG report output.")
   public static final String RASTER_FORMAT_LONG_DESCRIPTION = "RASTER_FORMAT_LONG_DESCRIPTION";
   
   // Ad Hoc Report Page Labels ---------------------------------------------------------------

   @RBEntry("equals")
   @RBComment("Label for EQUAL constraint in constraint dropdown on ad hoc report pages")
   public static final String EQUAL = "equal";
   
   @RBEntry("not")
   @RBComment("Label for NOT constraint in constraint dropdown on ad hoc report pages")
   public static final String NOT = "notEqual";

   @RBEntry("null")
   @RBComment("Label for NULL constraint in constraint dropdown on ad hoc report pages")
   public static final String NULL = "isNull";

   @RBEntry("not null")
   @RBComment("Label for NOT NULL constraint in constraint dropdown on ad hoc report pages")
   public static final String NOT_NULL = "notNull";

   @RBEntry("like")
   @RBComment("Label for LIKE constraint in constraint dropdown on ad hoc report pages")
   public static final String LIKE = "like";

   @RBEntry("not like")
   @RBComment("Label for NOT LIKE constraint in constraint dropdown on ad hoc report pages")
   public static final String NOT_LIKE = "notLike"; 
   
   @RBEntry("<")
   @RBComment("Label for < constraint in constraint dropdown on ad hoc report pages")
   public static final String LESS_THAN = "lessThan";
   
   @RBEntry("<=")
   @RBComment("Label for <= to constraint in constraint dropdown on ad hoc report pages")
   public static final String LESS_THAN_OR_EQUAL = "lessThanOrEqual";
   
   @RBEntry(">")
   @RBComment("Label for > constraint in constraint dropdown on ad hoc report pages")
   public static final String GREATER_THAN = "greaterThan";
   
   @RBEntry(">=")
   @RBComment("Label for >= constraint in constraint dropdown on ad hoc report pages")
   public static final String GREATER_THAN_OR_EQUAL = "greaterThanOrEqual";  
   
   @RBEntry("between")
   @RBComment("Label for BETWEEN constraint in constraint dropdown on ad hoc report pages")
   public static final String BETWEEN = "BETWEEN";

   @RBEntry("yesterday")
   @RBComment("Label for LASTDAY constraint in constraint dropdown on ad hoc report pages")
   public static final String LASTDAY = "LASTDAY";

   @RBEntry("last week")
   @RBComment("Label for LASTWEEK constraint in constraint dropdown on ad hoc report pages")
   public static final String LASTWEEK = "LASTWEEK";

   @RBEntry("last month")
   @RBComment("Label for LASTMONTH constraint in constraint dropdown on ad hoc report pages")
   public static final String  LASTMONTH = "LASTMONTH";

   @RBEntry("last quarter")
   @RBComment("Label for LASTQUARTER constraint in constraint dropdown on ad hoc report pages")
   public static final String LASTQUARTER = "LASTQUARTER";

   @RBEntry("last year")
   @RBComment("Label for LASTYEAR constraint in constraint dropdown on ad hoc report pages")
   public static final String LASTYEAR = "LASTYEAR";

   @RBEntry("today")
   @RBComment("Label for THISDAY constraint in constraint dropdown on ad hoc report pages")
   public static final String THISDAY = "THISDAY";

   @RBEntry("this week")
   @RBComment("Label for THISWEEK constraint in constraint dropdown on ad hoc report pages")
   public static final String THISWEEK = "THISWEEK";

   @RBEntry("this month")
   @RBComment("Label for THISMONTH constraint in constraint dropdown on ad hoc report pages")
   public static final String THISMONTH = "THISMONTH";

   @RBEntry("this quarter")
   @RBComment("Label for THISQUARTER constraint in constraint dropdown on ad hoc report pages")
   public static final String THISQUARTER = "THISQUARTER";

   @RBEntry("this year")
   @RBComment("Label for THISYEAR constraint in constraint dropdown on ad hoc report pages")
   public static final String THISYEAR = "THISYEAR";

   @RBEntry("tomorrow")
   @RBComment("Label for NEXTDAY constraint in constraint dropdown on ad hoc report pages")
   public static final String NEXTDAY = "NEXTDAY";

   @RBEntry("next week")
   @RBComment("Label for NEXTWEEK constraint in constraint dropdown on ad hoc report pages")
   public static final String NEXTWEEK = "NEXTWEEK";

   @RBEntry("next month")
   @RBComment("Label for NEXTMONTH constraint in constraint dropdown on ad hoc report pages")
   public static final String NEXTMONTH = "NEXTMONTH";

   @RBEntry("next quarter")
   @RBComment("Label for NEXTQUARTER constraint in constraint dropdown on ad hoc report pages")
   public static final String NEXTQUARTER = "NEXTQUARTER";  

   @RBEntry("next year")
   @RBComment("Label for NEXTYEAR constraint in constraint dropdown on ad hoc report pages")
   public static final String NEXTYEAR = "NEXTYEAR";

   @RBEntry("Generate")
   @RBComment("Label for the Generate button on ad hoc report pages")
   public static final String GENERATE_ADHOC_RESULTS = "GENERATE_ADHOC_RESULTS";   
   
   @RBEntry("Generate Report Results")
   @RBComment("Label for the Generate button tooltip on ad hoc report pages")
   public static final String GENERATE_ADHOC_RESULTS_TOOLTIP = "GENERATE_ADHOC_RESULTS_TOOLTIP";
   
   @RBEntry("Save")
   @RBComment("Label for the Save Report Button")
   public static final String SAVE_REPORT_BUTTON = "SAVE_REPORT_BUTTON";  
   
   @RBEntry("Save Report")
   @RBComment("Label for the Save Report Button tooltip")
   public static final String SAVE_REPORT_BUTTON_TOOLTIP = "SAVE_REPORT_BUTTON_TOOLTIP";
   
   @RBEntry("Saved Reports")
   @RBComment("Label for the Saved Reports Button")
   public static final String SAVED_REPORTS_BUTTON = "SAVED_REPORTS_BUTTON";   
   
   @RBEntry("Saved Reports Menu")
   @RBComment("Label for the Saved Reports Button tooltip")
   public static final String SAVED_REPORTS_BUTTON_TOOLTIP = "SAVED_REPORTS_BUTTON_TOOLTIP";
   
   @RBEntry("Clear")
   @RBComment("Label for the Clear Page Settings button on ad hoc report pages")
   public static final String CLEAR_ADHOC_PAGE = "CLEAR_ADHOC_PAGE";
   
   @RBEntry("Clear Page Settings")
   @RBComment("Label for the Clear Page Settings button tooltip on ad hoc report pages")
   public static final String CLEAR_ADHOC_PAGE_TOOLTIP = "CLEAR_ADHOC_PAGE_TOOLTIP";
   
   @RBEntry("Cancel")
   @RBComment("Label for the Cancel Page button on ad hoc report pages")
   public static final String CANCEL_ADHOC_PAGE = "CANCEL_ADHOC_PAGE";
      
   @RBEntry("Cancel and Close Window")
   @RBComment("Label for the Cancel Page button tooltip on ad hoc report pages")
   public static final String CANCEL_ADHOC_PAGE_TOOLTIP = "CANCEL_ADHOC_PAGE_TOOLTIP";

   @RBEntry("No Fields Selected")
   @RBComment("Label for title of no fields selected message")
   public static final String NO_FIELDS_SELECTED_TITLE = "NO_FIELDS_SELECTED_TITLE";
   
   @RBEntry("Please select at least one field to report on.")
   @RBComment("Label for validation message when no fields selected")
   public static final String NO_FIELDS_SELECTED = "NO_FIELDS_SELECTED";
   
   @RBEntry("Enter Saved Report Name...")
   @RBComment("Empty text for saved report name textbox")
   public static final String ENTER_SAVED_REPORT_NAME = "ENTER_SAVED_REPORT_NAME";
   
   @RBEntry("Saved Report Name Blank")
   @RBComment("Title for error when no saved report name is entered")
   public static final String SAVED_REPORT_NAME_BLANK_TITLE = "SAVED_REPORT_NAME_BLANK_TITLE";
   
   @RBEntry("Please enter a saved report name")
   @RBComment("Error when no saved report name is entered")
   public static final String SAVED_REPORT_NAME_BLANK = "SAVED_REPORT_NAME_BLANK";
   
   @RBEntry("Save Successful")
   @RBComment("Successful saved report title")
   public static final String SAVED_REPORT_SAVED_TITLE = "SAVED_REPORT_SAVED_TITLE";
   
   @RBEntry("Saved report was successfully saved: ")
   @RBComment("Successful saved report")
   public static final String SAVED_REPORT_SAVED = "SAVED_REPORT_SAVED";
   
   @RBEntry("Save Failed")
   @RBComment("Failed saving saved report title")
   public static final String SAVED_REPORT_FAILED_TITLE = "SAVED_REPORT_FAILED_TITLE";
   
   @RBEntry("Please enter a different saved report name.")
   @RBComment("Error when saved report is not unique")
   public static final String SAVED_REPORT_NAME_NOT_UNIQUE = "SAVED_REPORT_NAME_NOT_UNIQUE";
   
   @RBEntry("Modify Successful")
   @RBComment("Successful modify saved report title")
   public static final String SAVED_REPORT_MODIFIED_TITLE = "SAVED_REPORT_MODIFIED_TITLE";
   
   @RBEntry("Saved report was successfully modified: ")
   @RBComment("Successful modify saved report")
   public static final String SAVED_REPORT_MODIFIED = "SAVED_REPORT_MODIFIED";
   
   @RBEntry("Modify Saved Report Failed")
   @RBComment("Failed saved report title")
   public static final String MODIFY_REPORT_FAILED_TITLE = "MODIFY_REPORT_FAILED_TITLE";

   @RBEntry("Please enter a numeric value in field:")
   @RBComment("Label for validation message when invalid number entered")
   public static final String NUMBER_VALUE_REQUIRED = "NUMBER_VALUE_REQUIRED";

   @RBEntry("Constants")
   @RBComment("Label for the Constants section on ad hoc report pages")
   public static final String CONSTANTS = "CONSTANTS";

   @RBEntry("Back")
   @RBComment("Label for the Back button on ad hoc report results")
   public static final String BACK_BUTTON_TEXT = "BACK_BUTTON_TEXT";
   
   @RBEntry("Start Date")
   @RBComment("Label for error message displayed on start date data entry")
   public static final String START_DATE_LABEL = "START_DATE_LABEL";
   
   @RBEntry("End Date")
   @RBComment("Label for error message displayed on end date data entry")
   public static final String END_DATE_LABEL = "END_DATE_LABEL";
   
   @RBEntry("Select All")
   @RBComment("Label for Select All button/link")
   public static final String SELECT_ALL = "SELECT_ALL";
   
   @RBEntry("Deselect All")
   @RBComment("Label for Deselect All button/link")
   public static final String DESELECT_ALL = "DESELECT_ALL";
   
   @RBEntry("Include All Fields")
   @RBComment("Label for Include All Fields button/link")
   public static final String INCLUDE_ALL_FIELDS = "INCLUDE_ALL_FIELDS";
   
   @RBEntry("Deselect All Fields")
   @RBComment("Label for Deselect All Fields button/link")
   public static final String DESELECT_ALL_FIELDS = "DESELECT_ALL_FIELDS";
   
   @RBEntry("Input Parameters")
   @RBComment("Input Parameters")
   public static final String INPUT_PARAMETER_HEADER = "INPUT_PARAMETER_HEADER";
   
   @RBEntry("Load Report")
   @RBComment("Label for Load Report")
   public static final String LOAD_REPORT = "LOAD_REPORT";
   
   @RBEntry("Modify Report")
   @RBComment("Label for Modify Report")
   public static final String MODIFY_REPORT = "MODIFY_REPORT";
   
   @RBEntry("Save New Report")
   @RBComment("Label for Save New Report")
   public static final String SAVE_NEW_REPORT = "SAVE_NEW_REPORT";

   @RBEntry("Select a saved report to load...")
   @RBComment("Label for select a report dropdown")
   public static final String SELECT_SAVED_REPORT = "SELECT_SAVED_REPORT";   

   @RBEntry("Close")
   @RBComment("Label for close button")
   public static final String CLOSE = "CLOSE";

   @RBEntry("Results Table")
   @RBComment("Label for ad hoc results table")
   public static final String RESULTS_TABLE = "RESULTS_TABLE";
   
   @RBEntry("number value is required for this field.")
   @RBComment("Error text for number validation")
   public static final String NOT_A_NUMBER = "NOT_A_NUMBER";  
   
   @RBEntry("invalid date format")
   @RBComment("Error text for date validation")
   public static final String INVALID_DATE_FORMAT = "INVALID_DATE_FORMAT";
   
   @RBEntry("The date in this field must be equal to or after {0}")
   @RBComment("Error text for max date validation")
   public static final String INVALID_END_DATE = "INVALID_END_DATE";
    
   @RBEntry("The date in this field must be equal to or before {0}")
   @RBComment("Error text for min date validation")
   public static final String INVALID_START_DATE = "INVALID_START_DATE";
   
   @RBEntry("Today")
   public static final String DATEPICKER_TODAY_TEXT = "DATEPICKER_TODAY_TEXT";
   
   @RBEntry("{0}")
   public static final String DATEPICKER_TODAY_TIP = "DATEPICKER_TODAY_TIP";

   @RBEntry("Previous Month")
   public static final String DATEPICKER_PREV_MONTH = "DATEPICKER_PREV_MONTH";
   
   @RBEntry("Next Month")
   public static final String DATEPICKER_NEXT_MONTH = "DATEPICKER_NEXT_MONTH";

   @RBEntry("&#160;OK&#160;")
   public static final String DATEPICKER_OK = "DATEPICKER_OK";

   @RBEntry("Cancel")
   public static final String DATEPICKER_CANCEL = "DATEPICKER_CANCEL";

   @RBEntry("Choose a month and year")
   public static final String DATEPICKER_MONTH_YEAR = "DATEPICKER_MONTH_YEAR";
   
   @RBEntry("Clear all")
   @RBComment("Clear selected values in multi select dropdowns")
   public static final String CLEAR_ALL_VALUES = "CLEAR_ALL_VALUES";   
   
   @RBEntry("No saved reports exist")
   @RBComment("No saved report exist dropdown message")
   public static final String NO_SAVED_REPORTS = "NO_SAVED_REPORTS";
   
   @RBEntry("Show only my reports")
   @RBComment("Label in saved reports dropdown menu")
   public static final String SHOW_ONLY_MY_REPORTS = "SHOW_ONLY_MY_REPORTS";
   
   @RBEntry("Show all reports")
   @RBComment("Label in saved reports dropdown menu")
   public static final String SHOW_ALL_REPORTS = "SHOW_ALL_REPORTS";
   
   @RBEntry("Criteria")
   @RBComment("Criteria panel title")
   public static final String CRITERIA_PANEL_HEADER = "CRITERIA_PANEL_HEADER";
     
   @RBEntry("Report")
   @RBComment("Ad hoc report pages header for report")
   public static final String REPORT_HEADER = "REPORT_HEADER";   
   
   @RBEntry("Saved Report")
   @RBComment("Ad hoc report pages header for saved report")
   public static final String SAVED_REPORT_HEADER = "SAVED_REPORT_HEADER";   
   
   @RBEntry("From within the Criteria panel, choose the criteria to be used and click the 'Generate' button. ")
   @RBComment("Ad hoc report results pages header text")
   public static final String REPORT_RESULTS_TEXT = "REPORT_RESULTS_TEXT";
   
   @RBEntry("Created by Me")
   @RBComment("Label in saved reports dropdown menu, header for my reports")
   public static final String SAVED_REPORTS_CREATED_BY_ME = "SAVED_REPORTS_CREATED_BY_ME";
   
   @RBEntry("Shared with Me")
   @RBComment("Label in saved reports dropdown menu, header for reports created by others")
   public static final String SAVED_REPORTS_SHARED_WITH_ME = "SAVED_REPORTS_SHARED_WITH_ME";  
   
   @RBEntry("Report generated on:")
   @RBComment("Label for report generation time in ad hoc results")
   public static final String REPORT_GENERATED_TIME = "REPORT_GENERATED_TIME";  
   
   @RBEntry("Generated by:")
   @RBComment("Label for report generated by in ad hoc results")
   public static final String REPORT_GENERATED_BY = "REPORT_GENERATED_BY";   
   
   @RBEntry("The criteria panel cannot be displayed because this report does not have any selectable fields.  Please use Query Builder to add selectable fields to your report.")
   @RBComment("Message displayed when there are no reportable fields for the criteria panel.")
   public static final String NO_REPORTABLE_FIELDS_MSG = "NO_REPORTABLE_FIELDS_MSG";
   
   @RBEntry("No subtypes found for type:")
   @RBComment("Message displayed when there are no subtypes to display for a given type.")
   public static final String NO_SUBTYPES_FOR_TYPE = "NO_SUBTYPES_FOR_TYPE";
   
   @RBEntry("Report template no longer exists for report:")
   @RBComment("Message displayed when the report template no longer exists.")
   public static final String REPORT_TEMPLATE_NOT_FOUND = "REPORT_TEMPLATE_NOT_FOUND";
   
   @RBEntry("This report uses functions:")
   @RBComment("Message used to display functions in report.")
   public static final String FUNCTIONS_IN_REPORT = "FUNCTIONS_IN_REPORT";
   
   @RBEntry("This report contains OR conditions for columns:")
   @RBComment("Message used to notify user that OR conditions are used in report.")
   public static final String OR_CONDITION_IN_REPORT = "OR_CONDITION_IN_REPORT";
   
   @RBEntry("This report contains NOT conditions.")
   @RBComment("Message used to notify user that NOT conditions are used in report.")
   public static final String NOT_CONDITION_IN_REPORT = "NOT_CONDITION_IN_REPORT";
   
   @RBEntry("This report contains SUB-SELECTS.")
   @RBComment("Message used to notify user that sub-selects are used in report.")
   public static final String SUBSELECT_IN_REPORT = "SUBSELECT_IN_REPORT";   
   
   @RBEntry("Report Builder")
   @RBComment("Title of the report builder (formerly ad hoc) window.")
   public static final String REPORT_BUILDER_TITLE = "REPORT_BUILDER_TITLE";
   
   @RBEntry("Pin Criteria Panel")
   @RBComment("Tooltip for pinning tool on criteria panel.")
   public static final String PIN_CRITERIA_PANEL = "PIN_CRITERIA_PANEL";

   @RBEntry("Unpin Criteria Panel")
   @RBComment("Tooltip for unpinning tool on criteria panel.")
   public static final String UNPIN_CRITERIA_PANEL = "UNPIN_CRITERIA_PANEL";   
   
   @RBEntry("Collapse Criteria Panel")
   @RBComment("Tooltip for collapse tool on criteria panel.")
   public static final String COLLAPSE_CRITERIA_PANEL = "COLLAPSE_CRITERIA_PANEL";

   @RBEntry("Expand Criteria Panel")
   @RBComment("Tooltip for expand tool on criteria panel.")
   public static final String EXPAND_CRITERIA_PANEL = "EXPAND_CRITERIA_PANEL";
   
   @RBEntry("S")
   @RBComment("ReportBulder date lookup, one character representing day of week: Sunday")
   public static final String WEEK_SUN = "WEEK_SUN";

   @RBEntry("M")
   @RBComment("ReportBulder date lookup, one character representing day of week: Monday")
   public static final String WEEK_MON = "WEEK_MON";

   @RBEntry("T")
   @RBComment("ReportBulder date lookup, one character representing day of week: Tuesday")
   public static final String WEEK_TUE = "WEEK_TUE";

   @RBEntry("W")
   @RBComment("ReportBulder date lookup, one character representing day of week: Wednesday")
   public static final String WEEK_WED = "WEEK_WED";

   @RBEntry("T")
   @RBComment("ReportBulder date lookup, one character representing day of week: Thursday")
   public static final String WEEK_THU = "WEEK_THU";

   @RBEntry("F")
   @RBComment("ReportBulder date lookup, one character representing day of week: Friday")
   public static final String WEEK_FRI = "WEEK_FRI";

   @RBEntry("S")
   @RBComment("ReportBulder date lookup, one character representing day of week: Saturday")
   public static final String WEEK_SAT = "WEEK_SAT";
   
  @RBEntry("January")
  @RBComment("ReportBulder date lookup. All characters used for date lookup header, only first 3 characters used for month selection. Make sure first 3 characters represent month, Asian languages may use number.")
  public static final String MON_JAN = "MON_JAN";

  @RBEntry("February")
  @RBComment("ReportBulder date lookup. All characters used for date lookup header, only first 3 characters used for month selection. Make sure first 3 characters represent month, Asian languages may use number.")
  public static final String MON_FEB = "MON_FEB";

  @RBEntry("March")    
  @RBComment("ReportBulder date lookup. All characters used for date lookup header, only first 3 characters used for month selection. Make sure first 3 characters represent month, Asian languages may use number.")
  public static final String MON_MAR = "MON_MAR";

  @RBEntry("April")
  @RBComment("ReportBulder date lookup. All characters used for date lookup header, only first 3 characters used for month selection. Make sure first 3 characters represent month, Asian languages may use number.")
  public static final String MON_APR = "MON_APR";

  @RBEntry("May")
  @RBComment("ReportBulder date lookup. All characters used for date lookup header, only first 3 characters used for month selection. Make sure first 3 characters represent month, Asian languages may use number.")
  public static final String MON_MAY = "MON_MAY";

  @RBEntry("June")
  @RBComment("ReportBulder date lookup. All characters used for date lookup header, only first 3 characters used for month selection. Make sure first 3 characters represent month, Asian languages may use number.")
  public static final String MON_JUN = "MON_JUN";

  @RBEntry("July")
  @RBComment("ReportBulder date lookup. All characters used for date lookup header, only first 3 characters used for month selection. Make sure first 3 characters represent month, Asian languages may use number.")
  public static final String MON_JUL = "MON_JUL";

  @RBEntry("August")
  @RBComment("ReportBulder date lookup. All characters used for date lookup header, only first 3 characters used for month selection. Make sure first 3 characters represent month, Asian languages may use number.")
  public static final String MON_AUG = "MON_AUG";

  @RBEntry("September")
  @RBComment("ReportBulder date lookup. All characters used for date lookup header, only first 3 characters used for month selection. Make sure first 3 characters represent month, Asian languages may use number.")
  public static final String MON_SEP = "MON_SEP";

  @RBEntry("October")
  @RBComment("ReportBulder date lookup. All characters used for date lookup header, only first 3 characters used for month selection. Make sure first 3 characters represent month, Asian languages may use number.")
  public static final String MON_OCT = "MON_OCT";

  @RBEntry("November")
  @RBComment("ReportBulder date lookup. All characters used for date lookup header, only first 3 characters used for month selection. Make sure first 3 characters represent month, Asian languages may use number.")
  public static final String MON_NOV = "MON_NOV";

  @RBEntry("December")
  @RBComment("ReportBulder date lookup. All characters used for date lookup header, only first 3 characters used for month selection. Make sure first 3 characters represent month, Asian languages may use number.")
  public static final String MON_DEC = "MON_DEC";
  
  @RBEntry("Custom XSL file is specified for report output")
  @RBComment("Message displayed in legacy mode when a custom XSL file is used for report output.")
  public static final String REPORT_USES_CUSTOM_XSL = "REPORT_USES_CUSTOM_XSL"; 
  
  @RBEntry("Legacy Mode Output Formats")
  @RBComment("Tooltip for the Legacy Mode output format dropdown")
  public static final String LEGACY_MODE_OUTPUT_FORMAT_TOOLTIP = "LEGACY_MODE_OUTPUT_FORMAT_TOOLTIP";
  
  @RBEntry("Generate Legacy Mode Output")
  @RBComment("Button used to generate Legacy Mode output for a report")
  public static final String GENERATE_LEGACY_MODE_OUTPUT_BUTTON = "GENERATE_LEGACY_MODE_OUTPUT_BUTTON";  
  
  @RBEntry("This report has no input parameters.")
  @RBComment("Message displayed in legacy mode when a report has no input parameters")
  public static final String LEGACY_MODE_NO_INPUT_PARAMETERS = "LEGACY_MODE_NO_INPUT_PARAMETERS";
  
  @RBEntry("Report Template \"{0}\" in context \"{1}\" is used as a view but it does not exist.")
  public static final String REPORT_TEMPLATE_VIEW_DOES_NOT_EXIST = "300";
  
  @RBEntry("Report Templates \"{0}\" in context \"{1}\" are used as views but not all exist.")
  public static final String REPORT_TEMPLATE_VIEWS_DO_NOT_EXIST = "301";

  @RBEntry("This report may not display as expected because the same parameter name is used for multiple fields. Please check the Report Template for duplicate parameters:")
  @RBComment("Message displayed when the same parameter name is used for multiple fields.")
  public static final String ILLEGAL_PARAMETER_MATCH = "ILLEGAL_PARAMETER_MATCH";
}
