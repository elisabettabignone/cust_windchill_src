package wt.query.template;

import wt.util.resource.*;

@RBUUID("wt.query.template.templateResource")
public final class templateResource_it extends WTListResourceBundle {
   /**
/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
    * Labels -----------------------------------------------------------------
    * 
    **/
   @RBEntry("Genera report")
   public static final String GENERATE_REPORT = "100";

   @RBEntry("Genera")
   public static final String REPORT_GENERATE = "101";

   @RBEntry("Standard")
   public static final String REPORT_STANDARD_XSL = "102";

   @RBEntry("Personalizzato")
   public static final String REPORT_CUSTOM_XSL = "103";

   @RBEntry("XSL URL 1:")
   public static final String REPORT_CUSTOM_XSL_URL_1 = "104";

   @RBEntry("XSL URL 2:")
   public static final String REPORT_CUSTOM_XSL_URL_2 = "105";

   @RBEntry("Parametri di input")
   public static final String REPORT_INPUT_PARAMETERS = "106";

   @RBEntry("Formato output")
   public static final String REPORT_OUTPUT_FORMAT = "107";

   @RBEntry("Macro")
   public static final String REPORT_MACRO = "108";

   @RBEntry("Esegui interrogazione:")
   public static final String EXECUTE_QUERY = "109";

   @RBEntry("Genera report:")
   @RBComment("Browser window title for the Report Generation Execute Page; translation can be found in the <TITLE> element of the 7.0 WCL10N template src/wt/query/htmltmpl/report/ExecutePage*.html")
   public static final String EXECUTE_PAGE_TITLE = "110";

   @RBEntry("Modello di report")
   public static final String DISPLAY_TYPE = "200";

   @RBEntry("{0}: {1}")
   public static final String DISPLAY_IDENTITY = "201";

   @RBEntry("HTML")
   public static final String STANDARD_XSL_HTML = "HTML";

   @RBEntry("XML")
   public static final String STANDARD_XSL_XML = "XML";

   @RBEntry("CSV (variabile separata da virgola)")
   public static final String STANDARD_XSL_CSV = "CSV";

   @RBEntry("TSV (variabile separata da tabulazione)")
   public static final String STANDARD_XSL_TSV = "TSV";

   @RBEntry("HTML (con ordinamento e unione)")
   public static final String STANDARD_XSL_HTML_WITH_SORTING_AND_MERGING = "HTMLWithSortingAndMerging";

   @RBEntry("HTML (con ordinamento)")
   public static final String STANDARD_XSL_HTML_WITH_SORTING = "HTMLWithSorting";

   @RBEntry("HTML (con unione)")
   public static final String STANDARD_XSL_HTML_WITH_MERGING = "HTMLWithMerging";

   @RBEntry("Microsoft Word 2000 HTML (verticale)")
   public static final String STANDARD_XSL_HTML_MSWORD_PORTRAIT = "MSWord2000Portrait";

   @RBEntry("Microsoft Word 2000 HTML (orizzontale)")
   public static final String STANDARD_XSL_HTML_MSWORD_LANDSCAPE = "MSWord2000Landscape";

   @RBEntry("PDF")
   public static final String STANDARD_XSL_PDF = "PDF";

   @RBEntry("CSV UTF-8 (variabili separate da virgole)")
   public static final String UTF8_XSL_CSV = "CSV_UTF8";

   @RBEntry("TSV UTF-8 (variabili separate da tabulazioni)")
   public static final String UTF8_XSL_TSV = "TSV_UTF8";

   /**
    * Messages ---------------------------------------------------------------
    * 
    **/
   @RBEntry("La stringa XML non è valida.")
   public static final String INVALID_XML = "0";

   @RBEntry("L'oggetto modello \"{0}\" non è un modello di report valido.")
   public static final String INVALID_CONTEXT_OBJECT = "1";

   @RBEntry("È stata superata la lunghezza massima consentita della stringa.")
   public static final String STRING_LENGTH_LIMIT = "2";

   @RBEntry("L'utente dispone di accesso limitato.")
   public static final String INVALID_ACCESS = "3";

   @RBEntry("Rappresentazione parziale dell'SQL effettivo.")
   public static final String PARTIAL_SQL = "4";

   @RBEntry("Il nome della macro \"{0}\" non è valido. Segue un elenco di nomi di macro validi:{1}")
   public static final String INVALID_MACRO = "5";

   @RBEntry("Impossibile analizzare il valore di data \"{0}\". Il valore deve essere analizzato utilizzando uno dei formati Java che seguono: \"{1}\", \"{2}\", \"{3}\", \"{4}\", \"{5}\", \"{6}\", \"{7}\", \"{8}\", \"{9}\", \"{10}\", \"{11}\", \"{12}\" , \"{13}\", \"{14}\", \"{15}\", \"{16}\".")
   public static final String INVALID_DATE_PARSE_VALUE = "6";

   @RBEntry("Formato XSL non valido per la voce flusso: risorsa=\"{0}\" impostazioni locali=\"{1}\" percorso=\"{2}\".")
   public static final String INVALID_XSL_FORMAT = "7";

   @RBEntry("Impossibile trovare la definizione report: \"{0}\".")
   public static final String REPORT_DEF_NOT_FOUND = "8";

   @RBEntry("Elementi report esportati.")
   public static final String EXPORTED_REPORT_ARTIFACTS = "9";

   @RBEntry("Impossibile modificare il modello di report \"{0}\". Il nome dell'intestazione \"{1}\" è stato rimosso e l'intestazione è utilizzata dal modello di report \"{2}\".")
   public static final String INVALID_VIEW_COLUMN_MODIFICATION = "20";

   @RBEntry("Impossibile modificare il modello di report \"{0}\". Il nome \"{1}\" è stato modificato ed è utilizzato da un altro modello di report.")
   public static final String INVALID_VIEW_NAME_MODIFICATION = "21";

   @RBEntry("Impossibile modificare il modello di report \"{0}\". Il contesto è stato modificato da \"{1}\" a \"{2}\" ed è utilizzato da un altro modello di report.")
   public static final String INVALID_VIEW_CONTEXT_MODIFICATION = "22";


   /**
    * Preference Labels ---------------------------------------------------------------
    * 
    **/
   @RBEntry("Report")
   public static final String REPORTS_CATEGORY = "REPORTS_CATEGORY";

   @RBEntry("Processore report oggetti di formattazione XSL")
   public static final String XSLFO_NAME = "XSLFO_NAME";

   @RBEntry("Specifica il nome classe del processore report oggetti di formattazione XSL.")
   public static final String XSLFO_SHORT_DESCRIPTION = "XSLFO_SHORT_DESCRIPTION";

   @RBEntry("La preferenza specifica la classe che sarà istanziata e utilizzata come processore report oggetti di formattazione XSL.")
   public static final String XSLFO_LONG_DESCRIPTION = "XSLFO_LONG_DESCRIPTION";

   @RBEntry("Processore report SVG")
   public static final String SVG_NAME = "SVG_NAME";

   @RBEntry("Specifica il nome classe del Processore report SVG.")
   public static final String SVG_SHORT_DESCRIPTION = "SVG_SHORT_DESCRIPTION";

   @RBEntry("La preferenza specifica la classe che sarà istanziata e utilizzata come processore report SVG.")
   public static final String SVG_LONG_DESCRIPTION = "SVG_LONG_DESCRIPTION";

   @RBEntry("Formato raster report")
   public static final String RASTER_FORMAT_NAME = "RASTER_FORMAT_NAME";

   @RBEntry("Specifica il formato raster per l'output del report SVG.")
   public static final String RASTER_FORMAT_SHORT_DESCRIPTION = "RASTER_FORMAT_SHORT_DESCRIPTION";

   @RBEntry("La preferenza specifica il formato raster che sarà utilizzato per l'output del report SVG.")
   public static final String RASTER_FORMAT_LONG_DESCRIPTION = "RASTER_FORMAT_LONG_DESCRIPTION";
   
   // Ad Hoc Report Page Labels ---------------------------------------------------------------

   @RBEntry("uguale a")
   @RBComment("Label for EQUAL constraint in constraint dropdown on ad hoc report pages")
   public static final String EQUAL = "equal";
   
   @RBEntry("diverso da")
   @RBComment("Label for NOT constraint in constraint dropdown on ad hoc report pages")
   public static final String NOT = "notEqual";

   @RBEntry("nullo")
   @RBComment("Label for NULL constraint in constraint dropdown on ad hoc report pages")
   public static final String NULL = "isNull";

   @RBEntry("non nullo")
   @RBComment("Label for NOT NULL constraint in constraint dropdown on ad hoc report pages")
   public static final String NOT_NULL = "notNull";

   @RBEntry("simile a")
   @RBComment("Label for LIKE constraint in constraint dropdown on ad hoc report pages")
   public static final String LIKE = "like";

   @RBEntry("non simile a")
   @RBComment("Label for NOT LIKE constraint in constraint dropdown on ad hoc report pages")
   public static final String NOT_LIKE = "notLike"; 
   
   @RBEntry("<")
   @RBComment("Label for < constraint in constraint dropdown on ad hoc report pages")
   public static final String LESS_THAN = "lessThan";
   
   @RBEntry("<=")
   @RBComment("Label for <= to constraint in constraint dropdown on ad hoc report pages")
   public static final String LESS_THAN_OR_EQUAL = "lessThanOrEqual";
   
   @RBEntry(">")
   @RBComment("Label for > constraint in constraint dropdown on ad hoc report pages")
   public static final String GREATER_THAN = "greaterThan";
   
   @RBEntry(">=")
   @RBComment("Label for >= constraint in constraint dropdown on ad hoc report pages")
   public static final String GREATER_THAN_OR_EQUAL = "greaterThanOrEqual";  
   
   @RBEntry("tra")
   @RBComment("Label for BETWEEN constraint in constraint dropdown on ad hoc report pages")
   public static final String BETWEEN = "BETWEEN";

   @RBEntry("ieri")
   @RBComment("Label for LASTDAY constraint in constraint dropdown on ad hoc report pages")
   public static final String LASTDAY = "LASTDAY";

   @RBEntry("settimana scorsa")
   @RBComment("Label for LASTWEEK constraint in constraint dropdown on ad hoc report pages")
   public static final String LASTWEEK = "LASTWEEK";

   @RBEntry("mese scorso")
   @RBComment("Label for LASTMONTH constraint in constraint dropdown on ad hoc report pages")
   public static final String  LASTMONTH = "LASTMONTH";

   @RBEntry("trimestre scorso")
   @RBComment("Label for LASTQUARTER constraint in constraint dropdown on ad hoc report pages")
   public static final String LASTQUARTER = "LASTQUARTER";

   @RBEntry("anno scorso")
   @RBComment("Label for LASTYEAR constraint in constraint dropdown on ad hoc report pages")
   public static final String LASTYEAR = "LASTYEAR";

   @RBEntry("oggi")
   @RBComment("Label for THISDAY constraint in constraint dropdown on ad hoc report pages")
   public static final String THISDAY = "THISDAY";

   @RBEntry("questa settimana")
   @RBComment("Label for THISWEEK constraint in constraint dropdown on ad hoc report pages")
   public static final String THISWEEK = "THISWEEK";

   @RBEntry("questo mese")
   @RBComment("Label for THISMONTH constraint in constraint dropdown on ad hoc report pages")
   public static final String THISMONTH = "THISMONTH";

   @RBEntry("questo trimestre")
   @RBComment("Label for THISQUARTER constraint in constraint dropdown on ad hoc report pages")
   public static final String THISQUARTER = "THISQUARTER";

   @RBEntry("quest'anno")
   @RBComment("Label for THISYEAR constraint in constraint dropdown on ad hoc report pages")
   public static final String THISYEAR = "THISYEAR";

   @RBEntry("domani")
   @RBComment("Label for NEXTDAY constraint in constraint dropdown on ad hoc report pages")
   public static final String NEXTDAY = "NEXTDAY";

   @RBEntry("settimana prossima")
   @RBComment("Label for NEXTWEEK constraint in constraint dropdown on ad hoc report pages")
   public static final String NEXTWEEK = "NEXTWEEK";

   @RBEntry("mese prossimo")
   @RBComment("Label for NEXTMONTH constraint in constraint dropdown on ad hoc report pages")
   public static final String NEXTMONTH = "NEXTMONTH";

   @RBEntry("trimestre prossimo")
   @RBComment("Label for NEXTQUARTER constraint in constraint dropdown on ad hoc report pages")
   public static final String NEXTQUARTER = "NEXTQUARTER";  

   @RBEntry("anno prossimo")
   @RBComment("Label for NEXTYEAR constraint in constraint dropdown on ad hoc report pages")
   public static final String NEXTYEAR = "NEXTYEAR";

   @RBEntry("Genera")
   @RBComment("Label for the Generate button on ad hoc report pages")
   public static final String GENERATE_ADHOC_RESULTS = "GENERATE_ADHOC_RESULTS";   
   
   @RBEntry("Genera risultati report")
   @RBComment("Label for the Generate button tooltip on ad hoc report pages")
   public static final String GENERATE_ADHOC_RESULTS_TOOLTIP = "GENERATE_ADHOC_RESULTS_TOOLTIP";
   
   @RBEntry("Salva")
   @RBComment("Label for the Save Report Button")
   public static final String SAVE_REPORT_BUTTON = "SAVE_REPORT_BUTTON";  
   
   @RBEntry("Salva report")
   @RBComment("Label for the Save Report Button tooltip")
   public static final String SAVE_REPORT_BUTTON_TOOLTIP = "SAVE_REPORT_BUTTON_TOOLTIP";
   
   @RBEntry("Report salvati")
   @RBComment("Label for the Saved Reports Button")
   public static final String SAVED_REPORTS_BUTTON = "SAVED_REPORTS_BUTTON";   
   
   @RBEntry("Menu Report salvati")
   @RBComment("Label for the Saved Reports Button tooltip")
   public static final String SAVED_REPORTS_BUTTON_TOOLTIP = "SAVED_REPORTS_BUTTON_TOOLTIP";
   
   @RBEntry("Reimposta")
   @RBComment("Label for the Clear Page Settings button on ad hoc report pages")
   public static final String CLEAR_ADHOC_PAGE = "CLEAR_ADHOC_PAGE";
   
   @RBEntry("Reimposta impostazioni pagina")
   @RBComment("Label for the Clear Page Settings button tooltip on ad hoc report pages")
   public static final String CLEAR_ADHOC_PAGE_TOOLTIP = "CLEAR_ADHOC_PAGE_TOOLTIP";
   
   @RBEntry("Annulla")
   @RBComment("Label for the Cancel Page button on ad hoc report pages")
   public static final String CANCEL_ADHOC_PAGE = "CANCEL_ADHOC_PAGE";
      
   @RBEntry("Annulla e chiude la finestra")
   @RBComment("Label for the Cancel Page button tooltip on ad hoc report pages")
   public static final String CANCEL_ADHOC_PAGE_TOOLTIP = "CANCEL_ADHOC_PAGE_TOOLTIP";

   @RBEntry("Nessun campo selezionato")
   @RBComment("Label for title of no fields selected message")
   public static final String NO_FIELDS_SELECTED_TITLE = "NO_FIELDS_SELECTED_TITLE";
   
   @RBEntry("Selezionare almeno un campo.")
   @RBComment("Label for validation message when no fields selected")
   public static final String NO_FIELDS_SELECTED = "NO_FIELDS_SELECTED";
   
   @RBEntry("Immettere nome di report salvato...")
   @RBComment("Empty text for saved report name textbox")
   public static final String ENTER_SAVED_REPORT_NAME = "ENTER_SAVED_REPORT_NAME";
   
   @RBEntry("Nome di report salvato vuoto")
   @RBComment("Title for error when no saved report name is entered")
   public static final String SAVED_REPORT_NAME_BLANK_TITLE = "SAVED_REPORT_NAME_BLANK_TITLE";
   
   @RBEntry("Immettere un nome per il report salvato")
   @RBComment("Error when no saved report name is entered")
   public static final String SAVED_REPORT_NAME_BLANK = "SAVED_REPORT_NAME_BLANK";
   
   @RBEntry("Salvataggio completato")
   @RBComment("Successful saved report title")
   public static final String SAVED_REPORT_SAVED_TITLE = "SAVED_REPORT_SAVED_TITLE";
   
   @RBEntry("Il report è stato salvato: ")
   @RBComment("Successful saved report")
   public static final String SAVED_REPORT_SAVED = "SAVED_REPORT_SAVED";
   
   @RBEntry("Salvataggio non riuscito")
   @RBComment("Failed saving saved report title")
   public static final String SAVED_REPORT_FAILED_TITLE = "SAVED_REPORT_FAILED_TITLE";
   
   @RBEntry("Immettere un altro nome per il report salvato.")
   @RBComment("Error when saved report is not unique")
   public static final String SAVED_REPORT_NAME_NOT_UNIQUE = "SAVED_REPORT_NAME_NOT_UNIQUE";
   
   @RBEntry("Modifica completata")
   @RBComment("Successful modify saved report title")
   public static final String SAVED_REPORT_MODIFIED_TITLE = "SAVED_REPORT_MODIFIED_TITLE";
   
   @RBEntry("Il report salvato è stato modificato: ")
   @RBComment("Successful modify saved report")
   public static final String SAVED_REPORT_MODIFIED = "SAVED_REPORT_MODIFIED";
   
   @RBEntry("Modifica del report salvato non riuscita")
   @RBComment("Failed saved report title")
   public static final String MODIFY_REPORT_FAILED_TITLE = "MODIFY_REPORT_FAILED_TITLE";

   @RBEntry("Immettere un valore numerico nel campo:")
   @RBComment("Label for validation message when invalid number entered")
   public static final String NUMBER_VALUE_REQUIRED = "NUMBER_VALUE_REQUIRED";

   @RBEntry("Costanti")
   @RBComment("Label for the Constants section on ad hoc report pages")
   public static final String CONSTANTS = "CONSTANTS";

   @RBEntry("Indietro")
   @RBComment("Label for the Back button on ad hoc report results")
   public static final String BACK_BUTTON_TEXT = "BACK_BUTTON_TEXT";
   
   @RBEntry("Data d'inizio")
   @RBComment("Label for error message displayed on start date data entry")
   public static final String START_DATE_LABEL = "START_DATE_LABEL";
   
   @RBEntry("Data di fine")
   @RBComment("Label for error message displayed on end date data entry")
   public static final String END_DATE_LABEL = "END_DATE_LABEL";
   
   @RBEntry("Seleziona tutto")
   @RBComment("Label for Select All button/link")
   public static final String SELECT_ALL = "SELECT_ALL";
   
   @RBEntry("Deseleziona tutto")
   @RBComment("Label for Deselect All button/link")
   public static final String DESELECT_ALL = "DESELECT_ALL";
   
   @RBEntry("Includi tutti i campi")
   @RBComment("Label for Include All Fields button/link")
   public static final String INCLUDE_ALL_FIELDS = "INCLUDE_ALL_FIELDS";
   
   @RBEntry("Deseleziona tutti i campi")
   @RBComment("Label for Deselect All Fields button/link")
   public static final String DESELECT_ALL_FIELDS = "DESELECT_ALL_FIELDS";
   
   @RBEntry("Parametri di input")
   @RBComment("Input Parameters")
   public static final String INPUT_PARAMETER_HEADER = "INPUT_PARAMETER_HEADER";
   
   @RBEntry("Carica report")
   @RBComment("Label for Load Report")
   public static final String LOAD_REPORT = "LOAD_REPORT";
   
   @RBEntry("Modifica report")
   @RBComment("Label for Modify Report")
   public static final String MODIFY_REPORT = "MODIFY_REPORT";
   
   @RBEntry("Salva nuovo report")
   @RBComment("Label for Save New Report")
   public static final String SAVE_NEW_REPORT = "SAVE_NEW_REPORT";

   @RBEntry("Selezionare un report salvato da caricare...")
   @RBComment("Label for select a report dropdown")
   public static final String SELECT_SAVED_REPORT = "SELECT_SAVED_REPORT";   

   @RBEntry("Chiudi")
   @RBComment("Label for close button")
   public static final String CLOSE = "CLOSE";

   @RBEntry("Tabella risultati")
   @RBComment("Label for ad hoc results table")
   public static final String RESULTS_TABLE = "RESULTS_TABLE";
   
   @RBEntry("Il campo richiede un valore numerico.")
   @RBComment("Error text for number validation")
   public static final String NOT_A_NUMBER = "NOT_A_NUMBER";  
   
   @RBEntry("Formato data non valido")
   @RBComment("Error text for date validation")
   public static final String INVALID_DATE_FORMAT = "INVALID_DATE_FORMAT";
   
   @RBEntry("La data in questo campo deve essere uguale o successiva a {0}")
   @RBComment("Error text for max date validation")
   public static final String INVALID_END_DATE = "INVALID_END_DATE";
    
   @RBEntry("La data in questo campo deve essere uguale o precedente a {0}")
   @RBComment("Error text for min date validation")
   public static final String INVALID_START_DATE = "INVALID_START_DATE";
   
   @RBEntry("Oggi")
   public static final String DATEPICKER_TODAY_TEXT = "DATEPICKER_TODAY_TEXT";
   
   @RBEntry("{0}")
   public static final String DATEPICKER_TODAY_TIP = "DATEPICKER_TODAY_TIP";

   @RBEntry("Mese precedente")
   public static final String DATEPICKER_PREV_MONTH = "DATEPICKER_PREV_MONTH";
   
   @RBEntry("Mese successivo")
   public static final String DATEPICKER_NEXT_MONTH = "DATEPICKER_NEXT_MONTH";

   @RBEntry("&#160;OK&#160;")
   public static final String DATEPICKER_OK = "DATEPICKER_OK";

   @RBEntry("Annulla")
   public static final String DATEPICKER_CANCEL = "DATEPICKER_CANCEL";

   @RBEntry("Scegliere un mese e un anno")
   public static final String DATEPICKER_MONTH_YEAR = "DATEPICKER_MONTH_YEAR";
   
   @RBEntry("Reimposta tutto")
   @RBComment("Clear selected values in multi select dropdowns")
   public static final String CLEAR_ALL_VALUES = "CLEAR_ALL_VALUES";   
   
   @RBEntry("Non esistono report salvati")
   @RBComment("No saved report exist dropdown message")
   public static final String NO_SAVED_REPORTS = "NO_SAVED_REPORTS";
   
   @RBEntry("Mostra solo i miei report")
   @RBComment("Label in saved reports dropdown menu")
   public static final String SHOW_ONLY_MY_REPORTS = "SHOW_ONLY_MY_REPORTS";
   
   @RBEntry("Mostra tutti i report")
   @RBComment("Label in saved reports dropdown menu")
   public static final String SHOW_ALL_REPORTS = "SHOW_ALL_REPORTS";
   
   @RBEntry("Criteri")
   @RBComment("Criteria panel title")
   public static final String CRITERIA_PANEL_HEADER = "CRITERIA_PANEL_HEADER";
     
   @RBEntry("Report")
   @RBComment("Ad hoc report pages header for report")
   public static final String REPORT_HEADER = "REPORT_HEADER";   
   
   @RBEntry("Report salvato")
   @RBComment("Ad hoc report pages header for saved report")
   public static final String SAVED_REPORT_HEADER = "SAVED_REPORT_HEADER";   
   
   @RBEntry("Nel pannello Criteri, scegliere i criteri da utilizzare e fare clic su Genera. ")
   @RBComment("Ad hoc report results pages header text")
   public static final String REPORT_RESULTS_TEXT = "REPORT_RESULTS_TEXT";
   
   @RBEntry("Creati da me")
   @RBComment("Label in saved reports dropdown menu, header for my reports")
   public static final String SAVED_REPORTS_CREATED_BY_ME = "SAVED_REPORTS_CREATED_BY_ME";
   
   @RBEntry("Condivisi con me")
   @RBComment("Label in saved reports dropdown menu, header for reports created by others")
   public static final String SAVED_REPORTS_SHARED_WITH_ME = "SAVED_REPORTS_SHARED_WITH_ME";  
   
   @RBEntry("Data di generazione report:")
   @RBComment("Label for report generation time in ad hoc results")
   public static final String REPORT_GENERATED_TIME = "REPORT_GENERATED_TIME";  
   
   @RBEntry("Generato da:")
   @RBComment("Label for report generated by in ad hoc results")
   public static final String REPORT_GENERATED_BY = "REPORT_GENERATED_BY";   
   
   @RBEntry("Impossibile visualizzare il pannello dei criteri. Il report non contiene campi selezionabili. Per aggiungere campi selezionabili al report, utilizzare Query Builder.")
   @RBComment("Message displayed when there are no reportable fields for the criteria panel.")
   public static final String NO_REPORTABLE_FIELDS_MSG = "NO_REPORTABLE_FIELDS_MSG";
   
   @RBEntry("Nessun sottotipo trovato per il tipo:")
   @RBComment("Message displayed when there are no subtypes to display for a given type.")
   public static final String NO_SUBTYPES_FOR_TYPE = "NO_SUBTYPES_FOR_TYPE";
   
   @RBEntry("Il modello di report non esiste più per il report:")
   @RBComment("Message displayed when the report template no longer exists.")
   public static final String REPORT_TEMPLATE_NOT_FOUND = "REPORT_TEMPLATE_NOT_FOUND";
   
   @RBEntry("Report con funzioni:")
   @RBComment("Message used to display functions in report.")
   public static final String FUNCTIONS_IN_REPORT = "FUNCTIONS_IN_REPORT";
   
   @RBEntry("Il report contiene condizioni OR per le colonne:")
   @RBComment("Message used to notify user that OR conditions are used in report.")
   public static final String OR_CONDITION_IN_REPORT = "OR_CONDITION_IN_REPORT";
   
   @RBEntry("Il report contiene condizioni NOT.")
   @RBComment("Message used to notify user that NOT conditions are used in report.")
   public static final String NOT_CONDITION_IN_REPORT = "NOT_CONDITION_IN_REPORT";
   
   @RBEntry("Il report contiene SUB-SELECTS.")
   @RBComment("Message used to notify user that sub-selects are used in report.")
   public static final String SUBSELECT_IN_REPORT = "SUBSELECT_IN_REPORT";   
   
   @RBEntry("Creazione report")
   @RBComment("Title of the report builder (formerly ad hoc) window.")
   public static final String REPORT_BUILDER_TITLE = "REPORT_BUILDER_TITLE";
   
   @RBEntry("Blocca pannello Criteri")
   @RBComment("Tooltip for pinning tool on criteria panel.")
   public static final String PIN_CRITERIA_PANEL = "PIN_CRITERIA_PANEL";

   @RBEntry("Sblocca pannello Criteri")
   @RBComment("Tooltip for unpinning tool on criteria panel.")
   public static final String UNPIN_CRITERIA_PANEL = "UNPIN_CRITERIA_PANEL";   
   
   @RBEntry("Comprimi pannello Criteri")
   @RBComment("Tooltip for collapse tool on criteria panel.")
   public static final String COLLAPSE_CRITERIA_PANEL = "COLLAPSE_CRITERIA_PANEL";

   @RBEntry("Espandi pannello Criteri")
   @RBComment("Tooltip for expand tool on criteria panel.")
   public static final String EXPAND_CRITERIA_PANEL = "EXPAND_CRITERIA_PANEL";
   
   @RBEntry("D")
   @RBComment("ReportBulder date lookup, one character representing day of week: Sunday")
   public static final String WEEK_SUN = "WEEK_SUN";

   @RBEntry("L")
   @RBComment("ReportBulder date lookup, one character representing day of week: Monday")
   public static final String WEEK_MON = "WEEK_MON";

   @RBEntry("M")
   @RBComment("ReportBulder date lookup, one character representing day of week: Tuesday")
   public static final String WEEK_TUE = "WEEK_TUE";

   @RBEntry("M")
   @RBComment("ReportBulder date lookup, one character representing day of week: Wednesday")
   public static final String WEEK_WED = "WEEK_WED";

   @RBEntry("G")
   @RBComment("ReportBulder date lookup, one character representing day of week: Thursday")
   public static final String WEEK_THU = "WEEK_THU";

   @RBEntry("V")
   @RBComment("ReportBulder date lookup, one character representing day of week: Friday")
   public static final String WEEK_FRI = "WEEK_FRI";

   @RBEntry("S")
   @RBComment("ReportBulder date lookup, one character representing day of week: Saturday")
   public static final String WEEK_SAT = "WEEK_SAT";
   
  @RBEntry("gennaio")
  @RBComment("ReportBulder date lookup. All characters used for date lookup header, only first 3 characters used for month selection. Make sure first 3 characters represent month, Asian languages may use number.")
  public static final String MON_JAN = "MON_JAN";

  @RBEntry("febbraio")
  @RBComment("ReportBulder date lookup. All characters used for date lookup header, only first 3 characters used for month selection. Make sure first 3 characters represent month, Asian languages may use number.")
  public static final String MON_FEB = "MON_FEB";

  @RBEntry("marzo")
  @RBComment("ReportBulder date lookup. All characters used for date lookup header, only first 3 characters used for month selection. Make sure first 3 characters represent month, Asian languages may use number.")
  public static final String MON_MAR = "MON_MAR";

  @RBEntry("aprile")
  @RBComment("ReportBulder date lookup. All characters used for date lookup header, only first 3 characters used for month selection. Make sure first 3 characters represent month, Asian languages may use number.")
  public static final String MON_APR = "MON_APR";

  @RBEntry("maggio")
  @RBComment("ReportBulder date lookup. All characters used for date lookup header, only first 3 characters used for month selection. Make sure first 3 characters represent month, Asian languages may use number.")
  public static final String MON_MAY = "MON_MAY";

  @RBEntry("giugno")
  @RBComment("ReportBulder date lookup. All characters used for date lookup header, only first 3 characters used for month selection. Make sure first 3 characters represent month, Asian languages may use number.")
  public static final String MON_JUN = "MON_JUN";

  @RBEntry("luglio")
  @RBComment("ReportBulder date lookup. All characters used for date lookup header, only first 3 characters used for month selection. Make sure first 3 characters represent month, Asian languages may use number.")
  public static final String MON_JUL = "MON_JUL";

  @RBEntry("agosto")
  @RBComment("ReportBulder date lookup. All characters used for date lookup header, only first 3 characters used for month selection. Make sure first 3 characters represent month, Asian languages may use number.")
  public static final String MON_AUG = "MON_AUG";

  @RBEntry("settembre")
  @RBComment("ReportBulder date lookup. All characters used for date lookup header, only first 3 characters used for month selection. Make sure first 3 characters represent month, Asian languages may use number.")
  public static final String MON_SEP = "MON_SEP";

  @RBEntry("ottobre")
  @RBComment("ReportBulder date lookup. All characters used for date lookup header, only first 3 characters used for month selection. Make sure first 3 characters represent month, Asian languages may use number.")
  public static final String MON_OCT = "MON_OCT";

  @RBEntry("novembre")
  @RBComment("ReportBulder date lookup. All characters used for date lookup header, only first 3 characters used for month selection. Make sure first 3 characters represent month, Asian languages may use number.")
  public static final String MON_NOV = "MON_NOV";

  @RBEntry("dicembre")
  @RBComment("ReportBulder date lookup. All characters used for date lookup header, only first 3 characters used for month selection. Make sure first 3 characters represent month, Asian languages may use number.")
  public static final String MON_DEC = "MON_DEC";
  
  @RBEntry("Per l'output del report è specificato un file XLS personalizzato")
  @RBComment("Message displayed in legacy mode when a custom XSL file is used for report output.")
  public static final String REPORT_USES_CUSTOM_XSL = "REPORT_USES_CUSTOM_XSL"; 
  
  @RBEntry("Formati output modalità legacy")
  @RBComment("Tooltip for the Legacy Mode output format dropdown")
  public static final String LEGACY_MODE_OUTPUT_FORMAT_TOOLTIP = "LEGACY_MODE_OUTPUT_FORMAT_TOOLTIP";
  
  @RBEntry("Genera output modalità legacy")
  @RBComment("Button used to generate Legacy Mode output for a report")
  public static final String GENERATE_LEGACY_MODE_OUTPUT_BUTTON = "GENERATE_LEGACY_MODE_OUTPUT_BUTTON";  
  
  @RBEntry("Parametri di input non specificati per questo report.")
  @RBComment("Message displayed in legacy mode when a report has no input parameters")
  public static final String LEGACY_MODE_NO_INPUT_PARAMETERS = "LEGACY_MODE_NO_INPUT_PARAMETERS";
  
  @RBEntry("Il modello di report \"{0}\" nel contesto \"{1}\" è utilizzato come vista, ma non esiste.")
  public static final String REPORT_TEMPLATE_VIEW_DOES_NOT_EXIST = "300";
  
  @RBEntry("I modelli di report \"{0}\" nel contesto \"{1}\" sono utilizzati come viste, ma non esistono tutti.")
  public static final String REPORT_TEMPLATE_VIEWS_DO_NOT_EXIST = "301";

  @RBEntry("È possibile che il report non venga visualizzato come previsto. Lo stesso nome di parametro è utilizzato in più campi. Verificare la presenza di parametri duplicati nel modello di report:")
  @RBComment("Message displayed when the same parameter name is used for multiple fields.")
  public static final String ILLEGAL_PARAMETER_MATCH = "ILLEGAL_PARAMETER_MATCH";
}
