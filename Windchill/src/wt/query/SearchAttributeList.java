/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */

package wt.query;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.lang.Class;
import java.lang.ClassNotFoundException;
import java.lang.String;
import java.util.Locale;
import java.util.Vector;
import wt.query.SearchAttributeListDelegate;
import wt.util.WTException;

import java.beans.PropertyDescriptor;
import java.util.Locale;
import java.util.StringTokenizer;

import wt.introspection.ClassInfo;
import wt.introspection.WTIntrospector;
import wt.introspection.WTIntrospectionException;
import wt.util.WTMessage;
import wt.util.WTProperties;
import wt.util.InstalledProperties;
import wt.doc.WTDocument;
import wt.part.WTPart;
import wt.csm.businessentity.BusinessEntity;
import java.text.Collator;
import java.text.CollationKey;

/**
 * This class is used to set the drop dow list, search criteria, and search
 * result columns for HTML Local Search.  This class has been deprecated
 * in X05 and all newly developed search UIs should use the Advanced Search
 * DCA Picker.  Please see the customization guide for search for more information.
 *
 *
 * To use this list set the following property in wt.properties:
 *
 * wt.query.searchAttributeList=STANDARD
 *
 * To customize and change the drop down, search criteria, and search result
 * columns in the local search attributes:
 *
 * 1.  Sub-class this class and change the setLocale method to use different
 * lists.  Two constructors will also be required.  See below:
 *    public SearchAttributeList( Locale locale ) {
 *       setLocale(locale);
 *    }
 *    public SearchAttributeList() {
 *       return;
 *    }
 *
 * 2.  Add an entry into service.properties similar to:
 *
 * wt.services/svc/default/wt.query.SearchAttributeListDelegate/MINE/java.lang.Object/0=mine.MySearchAttributeList/duplicate
 *
 * 3.  Change the entry in the wt.properties to:
 *
 * wt.query.searchAttributeList=MINE
 *
 *
 *
 * <BR><BR><B>Supported API: </B>true
 * <BR><BR><B>Extendable: </B>true
 *
 * @deprecated
 *
 * @version   1.0
 **/
public class SearchAttributeList implements SearchAttributeListDelegate, Externalizable {
   private static final String RESOURCE = "wt.query.queryResource";
   private static final String CLASSNAME = SearchAttributeList.class.getName();
   static final long serialVersionUID = 1;
   public static final long EXTERNALIZATION_VERSION_UID = 957977401221134810L;
   protected static final long OLD_FORMAT_VERSION_UID = -5576937025565697491L;

   protected int index;
   protected int classCount;
   protected String[] pickList;                // Look below to customize.
   protected String[] pickValues;              // Look below to customize.
   protected Locale clientLocale;
   protected static String[] queryClass;       // Look below to customize.
   protected static String[] inputAttributes;  // Look below to customize.
   protected static String[] inputProcessing;  // Look below to customize.
   protected static String[] outputAttributes; // Look below to customize.
   protected static String[] outputProcessing; // Look below to customize.
   protected static String[] sortAttributes;   // Look below to customize.  **New for 6.0.
   protected static String[] sortPref;         // Look below to customize.  **New for 6.0.
   protected String currentQueryResource;  // **New for 6.2 SP3.  Allows a different queryResource (e.g. com.ptc.windchill.pdmlink.search.searchResource)

   private static String  CODEBASE;
   private static boolean VERBOSE;
   public static final String NULL_PARAMETER = "0";

   static {
      try {
         WTProperties properties = WTProperties.getLocalProperties();
         VERBOSE = properties.getProperty ("wt.query.verbose", false);
         CODEBASE = properties.getProperty ("wt.server.codebase", "");
      }
      catch (Throwable t) {
         Object[] param = {SearchAttributeList.class.getName ()};
         System.err.println(WTMessage.getLocalizedMessage(
                             "wt.enterprise.enterpriseResource",
                             wt.enterprise.enterpriseResource.ERROR_INITIALIZING,
                             param));
         t.printStackTrace(System.err);
         throw new ExceptionInInitializerError(t);
      }
   }

   /**
    * Writes the non-transient fields of this class to an external source.
    *
    * <BR><BR><B>Supported API: </B>false
    *
    * @deprecated
    *
    * @param     output
    * @exception java.io.IOException
    **/
   public void writeExternal( ObjectOutput output )
            throws IOException {
      output.writeLong( EXTERNALIZATION_VERSION_UID );
   }

   /**
    * Reads the non-transient fields of this class from an external source.
    *
    * <BR><BR><B>Supported API: </B>false
    *
    * @deprecated
    *
    * @param     input
    * @exception java.io.IOException
    * @exception java.lang.ClassNotFoundException
    **/
   public void readExternal( ObjectInput input )
            throws IOException, ClassNotFoundException {
      long readSerialVersionUID = input.readLong();                  // consume UID
      readVersion( this, input, readSerialVersionUID, false, false );  // read fields
   }

   /**
    * Reads the non-transient fields of this class from an external source.
    *
    * <BR><BR><B>Supported API: </B>false
    *
    * @deprecated
    *
    * @param     thisObject
    * @param     input
    * @param     readSerialVersionUID
    * @param     passThrough
    * @param     superDone
    * @return    boolean
    * @exception java.io.IOException
    * @exception java.lang.ClassNotFoundException
    **/
   protected boolean readVersion( SearchAttributeList thisObject, ObjectInput input, long readSerialVersionUID, boolean passThrough, boolean superDone )
            throws IOException, ClassNotFoundException {
      boolean success = true;

      if ( readSerialVersionUID == 957977401221134810L )
         return readVersion957977401221134810L( input, readSerialVersionUID, superDone );
      else
         success = readOldVersion( input, readSerialVersionUID, passThrough, superDone );

      if (input instanceof wt.pds.PDSObjectInput)
         wt.fc.EvolvableHelper.requestRewriteOfEvolvedBlobbedObject();

      return success;
   }

   /**
    * Reads the non-transient fields of this class from an external source,
    * which is not the current version.
    *
    * @param     input
    * @param     readSerialVersionUID
    * @param     passThrough
    * @param     superDone
    * @return    boolean
    * @exception java.io.IOException
    * @exception java.lang.ClassNotFoundException
    **/
   private boolean readOldVersion( ObjectInput input, long readSerialVersionUID, boolean passThrough, boolean superDone )
            throws IOException, ClassNotFoundException {
      boolean success = true;

      if ( readSerialVersionUID == OLD_FORMAT_VERSION_UID ) {          // handle previous version
      }
      else
         throw new java.io.InvalidClassException( CLASSNAME, "Local class not compatible:"
                           + " stream classdesc externalizationVersionUID=" + readSerialVersionUID
                           + " local class externalizationVersionUID=" + EXTERNALIZATION_VERSION_UID );

      return success;
   }

   /**
    *
    * <BR><BR><B>Supported API: </B>false
    *
    * @deprecated
    *
    * @param     locale
    * @return    SearchAttributeList
    **/
   public SearchAttributeList( Locale locale ) {
      setLocale(locale);
   }

   /**
    *
    * <BR><BR><B>Supported API: </B>false
    *
    * @deprecated
    *
    * @return    SearchAttributeList
    **/
   public SearchAttributeList() {
      return;
   }

   @Override
   public void setLocale( Locale locale ) {
      // Load in the values for the drop down list for selecting what to to search against.
      // These should refer to your resource bundle and not queryResource.

      clientLocale = locale;

      // **Customize ----------------------------------------------------------------------------
      //      Add new classes to search to list below.
      //      Make sure that they are assigned numbers in sequence from 0 to N.
      //      Set dropDownListCount to N+1.

      final int ALL                   = 0;
      final int WTPART                = 1;
      final int WTCHANGEISSUE         = 2;
      final int WTCHANGEREQUEST       = 3;
      final int WTCHANGEINVESTIGATION = 4;
      final int WTCHANGEPROPOSAL      = 5;
      final int WTANALYSISACTIVITY    = 6;
      final int WTCHANGEORDER         = 7;
      final int WTCHANGEACTIVITY      = 8;
      final int WTDOCUMENT            = 9;
      final int PROXYPARTMASTER       = 10;
      final int PROXYDOCUMENTMASTER   = 11;
      final int WFPROCESS             = 12;
      final int EPMDOCUMENT           = 13;
      final int BUSINESSENTITY        = 14;
      final int CONFIGURATIONITEM     = 15;
      final int WTPRODUCTINSTANCE2    = 16;
      final int REPORTTEMPLATE        = 17;
      final int WTUNIT                = 18;
      final int WTPRODUCT             = 19;
      final int WTPRODUCTCONFIGURATION = 20;

      int dropDownListCount = 21;
      //  -------------------------------------------------------------------------------------

      index = dropDownListCount;

      // Identify the Routed systems Explorer classes and put at the end of the list.
      final int RSCOMP               = index;
      final int RSCONN               = index + 1;
      final int RSSPOOL              = index + 2;
      final int RSFITTING            = index + 3;
      final int RSMANUF              = index + 4;
      if ((InstalledProperties.isInstalled("WEF.RSExplorerApplet")) ||
          (InstalledProperties.isInstalled("WEF.RSExplorerHTML")))
        index = index + 5;

      // Identify the eNC class and put at the end of the list.
      final int WTMFGMODEL           = index;
      if (InstalledProperties.isInstalled("WEF.eNCExplorer"))
         index++;

      // Required for CADX HTMLUI support.
      // Add WTMANAGEDBASELINE to support the CADX HTMLUI Picker.
      // Set "available" types size to one more so WTMANAGEDBASELINE
      // is found. NOTE: This will add WTMANAGEDBASELINE to the values returned in
      // getPickList and getPickValues. getPickList and getPickValues have been modified to
      // "filter out" CONFIGURATIONITEM. This will not expose WTMANAGEDBASELINE since it is not
      // supported as a selectable type.
      final int WTMANAGEDBASELINE     = index;
      index++;

      classCount = dropDownListCount + 11;  // Include Mfg Planning Factor and Routed systems Explorer Classes
      pickList = new String[classCount];
      pickList[ALL] = WTMessage.getLocalizedMessage(RESOURCE,queryResource.ALL,null,clientLocale);
      pickList[WTPART] = WTMessage.getLocalizedMessage(RESOURCE,queryResource.WTPART,null,clientLocale);
      pickList[WTCHANGEISSUE] = WTMessage.getLocalizedMessage(RESOURCE,queryResource.WTCHANGEISSUE,null,clientLocale);
      pickList[WTCHANGEREQUEST] = WTMessage.getLocalizedMessage(RESOURCE,queryResource.WTCHANGEREQUEST,null,clientLocale);
      pickList[WTCHANGEINVESTIGATION] = WTMessage.getLocalizedMessage(RESOURCE,queryResource.WTCHANGEINVESTIGATION,null,clientLocale);
      pickList[WTANALYSISACTIVITY] = WTMessage.getLocalizedMessage(RESOURCE,queryResource.WTANALYSISACTIVITY,null,clientLocale);
      pickList[WTCHANGEPROPOSAL] = WTMessage.getLocalizedMessage(RESOURCE,queryResource.WTCHANGEPROPOSAL,null,clientLocale);
      pickList[WTCHANGEORDER] = WTMessage.getLocalizedMessage(RESOURCE,queryResource.WTCHANGEORDER,null,clientLocale);
      pickList[WTCHANGEACTIVITY] = WTMessage.getLocalizedMessage(RESOURCE,queryResource.WTCHANGEACTIVITY,null,clientLocale);
      pickList[WTDOCUMENT] = WTMessage.getLocalizedMessage(RESOURCE,queryResource.WTDOCUMENT,null,clientLocale);
      pickList[PROXYPARTMASTER] = WTMessage.getLocalizedMessage(RESOURCE,queryResource.PROXYPARTMASTER,null,clientLocale);
      pickList[PROXYDOCUMENTMASTER] = WTMessage.getLocalizedMessage(RESOURCE,queryResource.PROXYDOCUMENTMASTER,null,clientLocale);
      pickList[WFPROCESS] = WTMessage.getLocalizedMessage(RESOURCE,queryResource.WFPROCESS,null,clientLocale);
      pickList[EPMDOCUMENT] = WTMessage.getLocalizedMessage(RESOURCE,queryResource.EPMDOCUMENT,null,clientLocale);
      pickList[WTUNIT] = WTMessage.getLocalizedMessage(RESOURCE,queryResource.WTUNIT,null,clientLocale);
      pickList[BUSINESSENTITY] = WTMessage.getLocalizedMessage(RESOURCE,queryResource.BUSINESSENTITY,null,clientLocale);
      pickList[CONFIGURATIONITEM] = WTMessage.getLocalizedMessage(RESOURCE,queryResource.CONFIGURATIONITEM,null,clientLocale);
      pickList[REPORTTEMPLATE] = WTMessage.getLocalizedMessage(RESOURCE,queryResource.REPORTTEMPLATE,null,clientLocale);
      pickList[WTPRODUCT] = WTMessage.getLocalizedMessage(RESOURCE,queryResource.WTPRODUCT,null,clientLocale);
      pickList[WTPRODUCTCONFIGURATION] = WTMessage.getLocalizedMessage(RESOURCE,queryResource.WTPRODUCTCONFIGURATION,null,clientLocale);
      pickList[WTPRODUCTINSTANCE2] = WTMessage.getLocalizedMessage(RESOURCE,queryResource.WTPRODUCTINSTANCE2,null,clientLocale);
      pickList[RSCOMP] = WTMessage.getLocalizedMessage(RESOURCE,queryResource.RSCOMP,null,clientLocale);
      pickList[RSCONN] = WTMessage.getLocalizedMessage(RESOURCE,queryResource.RSCONN,null,clientLocale);
      pickList[RSSPOOL] = WTMessage.getLocalizedMessage(RESOURCE,queryResource.RSSPOOL,null,clientLocale);
      pickList[RSFITTING] = WTMessage.getLocalizedMessage(RESOURCE,queryResource.RSFITTING,null,clientLocale);
      pickList[RSMANUF] = WTMessage.getLocalizedMessage(RESOURCE,queryResource.RSMANUF,null,clientLocale);
      pickList[WTMFGMODEL] = WTMessage.getLocalizedMessage(RESOURCE,queryResource.WTMFGMODEL,null,clientLocale);
      pickList[WTMANAGEDBASELINE] = WTMessage.getLocalizedMessage(RESOURCE,queryResource.WTMANAGEDBASELINE,null,locale );

      pickValues = new String[classCount];
      pickValues[ALL] = queryResource.ALL;
      pickValues[WTPART] = queryResource.WTPART;
      pickValues[WTCHANGEISSUE] = queryResource.WTCHANGEISSUE;
      pickValues[WTCHANGEREQUEST] = queryResource.WTCHANGEREQUEST;
      pickValues[WTCHANGEINVESTIGATION] = queryResource.WTCHANGEINVESTIGATION;
      pickValues[WTANALYSISACTIVITY] = queryResource.WTANALYSISACTIVITY;
      pickValues[WTCHANGEPROPOSAL] = queryResource.WTCHANGEPROPOSAL;
      pickValues[WTCHANGEORDER] = queryResource.WTCHANGEORDER;
      pickValues[WTCHANGEACTIVITY] = queryResource.WTCHANGEACTIVITY;
      pickValues[WTDOCUMENT] = queryResource.WTDOCUMENT;
      pickValues[PROXYPARTMASTER] = queryResource.PROXYPARTMASTER;
      pickValues[PROXYDOCUMENTMASTER] = queryResource.PROXYDOCUMENTMASTER;
      pickValues[WFPROCESS] = queryResource.WFPROCESS;
      pickValues[EPMDOCUMENT] = queryResource.EPMDOCUMENT;
      pickValues[BUSINESSENTITY] = queryResource.BUSINESSENTITY;
      pickValues[CONFIGURATIONITEM] = queryResource.CONFIGURATIONITEM;
      pickValues[REPORTTEMPLATE] = queryResource.REPORTTEMPLATE;
      pickValues[WTUNIT] = queryResource.WTUNIT;
      pickValues[WTPRODUCT] = queryResource.WTPRODUCT;
      pickValues[WTPRODUCTCONFIGURATION] = queryResource.WTPRODUCTCONFIGURATION;
      pickValues[WTPRODUCTINSTANCE2] = queryResource.WTPRODUCTINSTANCE2;
      pickValues[RSCOMP] = queryResource.RSCOMP;
      pickValues[RSCONN] = queryResource.RSCONN;
      pickValues[RSSPOOL] = queryResource.RSSPOOL;
      pickValues[RSFITTING] = queryResource.RSFITTING;
      pickValues[RSMANUF] = queryResource.RSMANUF;
      pickValues[WTMFGMODEL] = queryResource.WTMFGMODEL;
      pickValues[WTMANAGEDBASELINE] = queryResource.WTMANAGEDBASELINE;

      // **Customize  You will need a string in here to correspond to each item in pickList
      // The string is a space separated list of what classes to query
      // against.  If you want to query against multiple classes that have a common parent that
      // has all of the attributes that you are interested in use that one class.  If you want
      // to query against multiple classes that don't have a good common parent then you can
      // add them to a list and the search will loop through each class and combine the results
      // at the end.  All classes in one list must only search against COMMON attributes or
      // attributes with the same name and of the same class!  If you add both a parent and
      // a child class to the list you will get duplicate entries, when the results are
      // combined duplicate entries are not deleted.
      queryClass = new String[classCount];

      queryClass[ALL] =
         "wt.part.WTPart wt.doc.WTDocument wt.change2.WTChangeIssue wt.change2.WTChangeRequest2 " +
         "wt.change2.WTChangeInvestigation wt.change2.WTAnalysisActivity wt.change2.WTChangeProposal " +
         "wt.change2.WTChangeOrder2 wt.change2.WTChangeActivity2 wt.csm.businessentity.BusinessEntity " +
         "wt.effectivity.ConfigurationItem wt.epm.EPMDocument " +
         "wt.replication.unit.WTUnit " +
         "wt.workflow.engine.WfProcess " +
         "wt.part.WTProductConfiguration ";  // Please remember to keep a space at the end so that conditionally added items work.

         //added for RSDB
         if ((InstalledProperties.isInstalled("WEF.RSExplorerApplet")) ||
             (InstalledProperties.isInstalled("WEF.RSExplorerHTML"))){
            queryClass[ALL] = queryClass[ALL] +
                                 "rs.component.RSComp " +
                                 "rs.connection.RSConn " +
                                 "rs.spec.RSSpool " +
                                 "rs.spec.RSFitting " +
                                 "rs.manufacturing.RSManufFeature ";
         }
      queryClass[WTPART] = "wt.part.WTPart";
      queryClass[WTCHANGEISSUE] = "wt.change2.WTChangeIssue";
      queryClass[WTCHANGEREQUEST] = "wt.change2.WTChangeRequest2";
      queryClass[WTCHANGEINVESTIGATION] = "wt.change2.WTChangeInvestigation";
      queryClass[WTANALYSISACTIVITY] = "wt.change2.WTAnalysisActivity";
      queryClass[WTCHANGEPROPOSAL] = "wt.change2.WTChangeProposal";
      queryClass[WTCHANGEORDER] = "wt.change2.WTChangeOrder2";
      queryClass[WTCHANGEACTIVITY] = "wt.change2.WTChangeActivity2";
      queryClass[WTDOCUMENT] = "wt.doc.WTDocument";
      queryClass[PROXYPARTMASTER] = "wt.federation.ProxyPartMaster";
      queryClass[PROXYDOCUMENTMASTER] = "wt.federation.ProxyDocumentMaster";
      queryClass[WFPROCESS] = "wt.workflow.engine.WfProcess";
      queryClass[EPMDOCUMENT] = "wt.epm.EPMDocument";
      queryClass[BUSINESSENTITY] = "wt.csm.businessentity.BusinessEntity";
      queryClass[CONFIGURATIONITEM] = "wt.effectivity.ConfigurationItem";
      queryClass[REPORTTEMPLATE] = "wt.query.template.ReportTemplate";
      queryClass[WTUNIT] = "wt.replication.unit.WTUnit";
      queryClass[WTPRODUCT] = "wt.part.WTPart";
      queryClass[WTPRODUCTCONFIGURATION] = "wt.part.WTProductConfiguration";
      queryClass[WTPRODUCTINSTANCE2] = "wt.part.WTProductInstance2";
      queryClass[RSCOMP] = "rs.component.RSComp";
      queryClass[RSCONN] = "rs.connection.RSConn";
      queryClass[RSSPOOL] = "rs.spec.RSSpool";
      queryClass[RSFITTING] = "rs.spec.RSFitting";
      queryClass[RSMANUF] = "rs.manufacturing.RSManufFeature";
      queryClass[WTMFGMODEL] = "com.ptc.mfg.model.MfgModel";
      queryClass[WTMANAGEDBASELINE] = "wt.vc.baseline.ManagedBaseline";

      // **Customize  These are the
      // attributes that can be queried against.
      inputAttributes = new String[classCount];
      inputAttributes[ALL] =
         "number name lifeCycleState teamTemplateId cabinet creator modifier modifyTimestamp";
      inputAttributes[WTPART] =
         "number name view versionIdentifier partType source lifeCycleState cabinet creator modifier modifyTimestamp";
      inputAttributes[WTCHANGEISSUE] =
         "number name creator createTimestamp requester issuePriority category lifeCycleState teamTemplateId cabinet modifyTimestamp description";
      inputAttributes[WTCHANGEREQUEST] =
         "number name creator createTimestamp needDate complexity requestPriority category lifeCycleState teamTemplateId cabinet modifyTimestamp description";
      inputAttributes[WTCHANGEINVESTIGATION] =
         "number name creator createTimestamp needDate lifeCycleState teamTemplateId cabinet modifyTimestamp description results";
      inputAttributes[WTANALYSISACTIVITY] =
         "number name creator createTimestamp needDate results lifeCycleState teamTemplateId cabinet modifyTimestamp description";
      inputAttributes[WTCHANGEPROPOSAL] =
         "number name creator createTimestamp lifeCycleState teamTemplateId cabinet modifyTimestamp description";
      inputAttributes[WTCHANGEORDER] =
         "number name creator createTimestamp lifeCycleState teamTemplateId cabinet modifyTimestamp description";
      inputAttributes[WTCHANGEACTIVITY] =
         "number name creator createTimestamp needDate lifeCycleState teamTemplateId cabinet modifyTimestamp description";
      inputAttributes[WTDOCUMENT] =
         "number name docType versionIdentifier lifeCycleState cabinet format creator modifier modifyTimestamp";
      inputAttributes[PROXYPARTMASTER] =
         "number name sourceNumber sourceName";
      inputAttributes[PROXYDOCUMENTMASTER] =
         "number name sourceNumber sourceName";
      inputAttributes[WFPROCESS] =
         "name state cabinet startTime creator";
      inputAttributes[EPMDOCUMENT] =
         "number name docType ownerApplication authoringApplication versionIdentifier lifeCycleState cabinet creator modifier modifyTimestamp";
      inputAttributes[BUSINESSENTITY] =
         "name modifyTimestamp";
      inputAttributes[CONFIGURATIONITEM] =
         "name lifeCycleState cabinet modifyTimestamp";
      inputAttributes[REPORTTEMPLATE] =
         "name description cabinet modifyTimestamp";
      inputAttributes[WTUNIT] =
         "number name versionIdentifier lifeCycleState teamTemplateId cabinet modifyTimestamp";
      inputAttributes[WTPRODUCT] =
         "number name view versionIdentifier partType source lifeCycleState cabinet creator modifier modifyTimestamp";
      inputAttributes[WTPRODUCTCONFIGURATION] =
         "configurationName productNumber productName";
      inputAttributes[WTPRODUCTINSTANCE2] =
         "serialNumber productNumber productName versionIdentifier plannedIncorporation incorporated";
      inputAttributes[RSCOMP] =
         "name versionIdentifier lifeCycleState";
      inputAttributes[RSCONN] =
         "name versionIdentifier lifeCycleState";
      inputAttributes[RSSPOOL] =
         "name versionIdentifier lifeCycleState";
      inputAttributes[RSFITTING] =
         "name versionIdentifier lifeCycleState";
      inputAttributes[RSMANUF] =
         "name versionIdentifier lifeCycleState";
      inputAttributes[WTMFGMODEL] =
         "name modifyTimestamp";
      inputAttributes[WTMANAGEDBASELINE] =
         "name number lifeCycleState modifyTimestamp";

      // **Customize  Each individual
      // string must match with the string listed above for the inputAttributes.  "0" stands for no
      // input processing.  If an attribute is an enumerated type use "0" and the code will generate
      // the drop down list.  In the first string: teamTeamplate is in the fourth position in inputAttributes
      // so the method to generate the drop down list for it is also in the fourth position in the
      // string.  The "0"s and methods must match in number with the number of attributes listed
      // under inputAttributes.  You may add a fully qualified method from your customization package
      // as long as it is static and returns a vector of strings.
      inputProcessing = new String[classCount];
      inputProcessing[ALL] =
         "0 0 0  wt.query.LocalSearchProcessor.getTeamTemplateList wt.query.LocalSearchProcessor.getCabinetList 0 0 0";
      inputProcessing[WTPART] =
         "0 0 wt.query.LocalSearchProcessor.getViewList 0 0 0 0  wt.query.LocalSearchProcessor.getCabinetList 0 0 0";
      inputProcessing[WTCHANGEISSUE] =
         "0 0 0 0 0 0 0 0  wt.query.LocalSearchProcessor.getTeamTemplateList wt.query.LocalSearchProcessor.getCabinetList 0 0";
      inputProcessing[WTCHANGEREQUEST] =
         "0 0 0 0 0 0 0 0 0  wt.query.LocalSearchProcessor.getTeamTemplateList wt.query.LocalSearchProcessor.getCabinetList 0 0";
      inputProcessing[WTCHANGEINVESTIGATION] =
         "0 0 0 0 0 0  wt.query.LocalSearchProcessor.getTeamTemplateList wt.query.LocalSearchProcessor.getCabinetList 0 0 0";
      inputProcessing[WTANALYSISACTIVITY] =
         "0 0 0 0 0 0 0  wt.query.LocalSearchProcessor.getTeamTemplateList wt.query.LocalSearchProcessor.getCabinetList 0 0";
      inputProcessing[WTCHANGEPROPOSAL] =
         "0 0 0 0 0  wt.query.LocalSearchProcessor.getTeamTemplateList wt.query.LocalSearchProcessor.getCabinetList 0 0";
      inputProcessing[WTCHANGEORDER] =
         "0 0 0 0 0  wt.query.LocalSearchProcessor.getTeamTemplateList wt.query.LocalSearchProcessor.getCabinetList 0 0";
      inputProcessing[WTCHANGEACTIVITY] =
         "0 0 0 0 0 0  wt.query.LocalSearchProcessor.getTeamTemplateList wt.query.LocalSearchProcessor.getCabinetList 0 0";
      inputProcessing[WTDOCUMENT] =
         "0 0 0 0 0  wt.query.LocalSearchProcessor.getCabinetList wt.query.LocalSearchProcessor.getDataFormatList 0 0 0";
      inputProcessing[PROXYPARTMASTER] =
         "0 0 0 0";
      inputProcessing[PROXYDOCUMENTMASTER] =
         "0 0 0 0";
      inputProcessing[WFPROCESS] =
         "0 0 wt.query.LocalSearchProcessor.getCabinetList 0 0";
      inputProcessing[EPMDOCUMENT] =
         "0 0 0 0 0 0 0  wt.query.LocalSearchProcessor.getCabinetList 0";
      inputProcessing[CONFIGURATIONITEM] =
         "0 0 wt.query.LocalSearchProcessor.getCabinetList 0";
      inputProcessing[REPORTTEMPLATE] =
         "0 0 wt.query.LocalSearchProcessor.getCabinetList 0";
      inputProcessing[BUSINESSENTITY] =
         "0 0";
      inputProcessing[WTUNIT] =
         "0 0 0 0 wt.query.LocalSearchProcessor.getTeamTemplateList wt.query.LocalSearchProcessor.getCabinetList 0";
      inputProcessing[WTPRODUCT] =
         "0 0 wt.query.LocalSearchProcessor.getViewList 0 0 0 0  wt.query.LocalSearchProcessor.getCabinetList 0 0 0";
      inputProcessing[WTPRODUCTCONFIGURATION] =
         "0 0 0";
      inputProcessing[WTPRODUCTINSTANCE2] =
         "0 0 0 0 0 0";
      inputProcessing[RSCOMP] =
         "0 0 0";
      inputProcessing[RSCONN] =
         "0 0 0";
      inputProcessing[RSSPOOL] =
         "0 0 0";
      inputProcessing[RSFITTING] =
         "0 0 0";
      inputProcessing[RSMANUF] =
         "0 0 0";
      inputProcessing[WTMFGMODEL] =
         "0 0";
      inputProcessing[WTMANAGEDBASELINE] =
         "0 0 0 0";

      // **Customize  This is similar in concept to inputAttributes only these are the attributes
      // that will be displayed in the search results.
      outputAttributes = new String[classCount];
      outputAttributes[ALL] =
         "number name iterationInfo viewName displayType lifeCycleState teamId modifyTimestamp";
      outputAttributes[WTPART] =
         "number name iterationInfo viewName teamId lifeCycleState modifyTimestamp";
      outputAttributes[WTCHANGEISSUE] =
         "number name creator createTimestamp requester issuePriority category";
      outputAttributes[WTCHANGEREQUEST] =
         "number name creator category requestPriority lifeCycleState";
      outputAttributes[WTCHANGEINVESTIGATION] =
         "number name creator createTimestamp needDate lifeCycleState";
      outputAttributes[WTANALYSISACTIVITY] =
         "number name creator createTimestamp needDate lifeCycleState";
      outputAttributes[WTCHANGEPROPOSAL] =
         "number name creator createTimestamp lifeCycleState";
      outputAttributes[WTCHANGEORDER] =
         "number name creator createTimestamp lifeCycleState";
      outputAttributes[WTCHANGEACTIVITY] =
         "number name creator createTimestamp needDate lifeCycleState";
      outputAttributes[WTDOCUMENT] =
         "number name iterationInfo teamId lifeCycleState modifyTimestamp";
      outputAttributes[PROXYPARTMASTER] =
         "number name sourceNumber sourceName serviceId";
      outputAttributes[PROXYDOCUMENTMASTER] =
         "number name sourceNumber sourceName serviceId";
      outputAttributes[WFPROCESS] =
         "name state startTime creator";
      outputAttributes[EPMDOCUMENT] =
         "number name iterationInfo teamId lifeCycleState modifyTimestamp";
      outputAttributes[BUSINESSENTITY] =
         "name modifyTimestamp";
      outputAttributes[CONFIGURATIONITEM] =
         "name solutionReference lifeCycleState modifyTimestamp";
      outputAttributes[REPORTTEMPLATE] =
         "name modifyTimestamp";
      outputAttributes[WTUNIT] =
         "number name iterationInfo viewName teamId lifeCycleState modifyTimestamp";
      outputAttributes[WTPRODUCT] =
         "number name iterationInfo viewName teamId lifeCycleState modifyTimestamp";
      outputAttributes[WTPRODUCTCONFIGURATION] =
         "configurationName productNumber productName";
      outputAttributes[WTPRODUCTINSTANCE2] =
         "serialNumber productNumber productName versionDisplayIdentifier configurationName displayType plannedIncorporation incorporated";
      outputAttributes[RSCOMP] =
         "name versionDisplayIdentifier lifeCycleState";
      outputAttributes[RSCONN] =
         "name versionDisplayIdentifier lifeCycleState";
      outputAttributes[RSSPOOL] =
         "name versionDisplayIdentifier lifeCycleState";
      outputAttributes[RSFITTING] =
         "name versionDisplayIdentifier lifeCycleState";
      outputAttributes[RSMANUF] =
         "name versionDisplayIdentifier lifeCycleState";
      outputAttributes[WTMFGMODEL] =
         "name modifyTimestamp";
      outputAttributes[WTMANAGEDBASELINE] =
         "number name lifeCycleState modifyTimestamp";

      // **Customize  This is similar in concept to inputProcessing in that the "0"s and the
      // processing must match the number of attributes in outputAttributes.  ObjProps is what
      // generates the links to the property page for the object.
      outputProcessing = new String[classCount];
      outputProcessing[ALL] =
         "ObjProps 0 0 0 0 0 0 0";
      outputProcessing[WTPART] =
         "ObjProps 0 0 0 0 0 0";
      outputProcessing[WTCHANGEISSUE] =
         "ObjProps 0 0 0 0 0 0";
      outputProcessing[WTCHANGEREQUEST] =
         "ObjProps 0 0 0 0 0";
      outputProcessing[WTCHANGEINVESTIGATION] =
         "ObjProps 0 0 0 0 0";
      outputProcessing[WTANALYSISACTIVITY] =
         "ObjProps 0 0 0 0 0";
      outputProcessing[WTCHANGEPROPOSAL] =
         "ObjProps 0 0 0 0";
      outputProcessing[WTCHANGEORDER] =
         "ObjProps 0 0 0 0";
      outputProcessing[WTCHANGEACTIVITY] =
         "ObjProps 0 0 0 0 0";
      outputProcessing[WTDOCUMENT] =
         "ObjProps 0 0 0 0 0";
      outputProcessing[PROXYPARTMASTER] =
         "ObjProps 0 0 0 0";
      outputProcessing[PROXYDOCUMENTMASTER] =
         "ObjProps 0 0 0 0";
      outputProcessing[WFPROCESS] =
         "ProcessManager 0 0 0";
      outputProcessing[EPMDOCUMENT] =
         "ObjProps 0 0 0 0 0 0 0";
      outputProcessing[BUSINESSENTITY] =
         "ObjProps 0";
      outputProcessing[CONFIGURATIONITEM] =
         "ObjProps 0 0 0";
      outputProcessing[REPORTTEMPLATE] =
         "ObjProps 0";
      outputProcessing[WTUNIT] =
         "ObjProps 0 0 0 0 0 0 0";
      outputProcessing[WTPRODUCT] =
         "ObjProps 0 0 0 0 0 0";
      outputProcessing[WTPRODUCTCONFIGURATION] =
         "ObjProps 0 0";
      outputProcessing[WTPRODUCTINSTANCE2] =
         "ObjProps 0 0 0 0 0 0 0";
      outputProcessing[RSCOMP] =
         "ObjProps 0 0";
      outputProcessing[RSCONN] =
         "ObjProps 0 0";
      outputProcessing[RSSPOOL] =
         "ObjProps 0 0";
      outputProcessing[RSFITTING] =
         "ObjProps 0 0";
      outputProcessing[RSMANUF] =
         "ObjProps 0 0";
      outputProcessing[WTMFGMODEL] =
         "ObjProps 0";
      outputProcessing[WTMANAGEDBASELINE] =
         "ObjProps 0 0 0";

      // **New for 6.0
      // **Customize  This is similar in concept to outputAttributes only this list is used
      // to indicate which attributes can be sorted, can't be sorted, or an alternate attribute
      // that can be sorted to have the same affect as the display attribute.  The string that is used
      // here should be the column descriptor so that it can be used to create the ClassAttribute for
      // the query.  The query that is used for search is a simple query that will not sort on all
      // of the display attributes.  Changing the 0 to 1 for an unsupported attribute will
      // either cause exceptions or sorts that don't work.  Attributes of the following types are
      // just some examples of the attributes that will either throw exceptions or sort incorrectly:
      // EnumeratedType, CabinetReference, DataFormatReference, LifeCycleTemplateReference,  TeamTemplateReference,
      // and ViewReference.
      sortAttributes = new String[classCount];
      sortAttributes[ALL] =
         "1 1 1 0 0 0 0 1";
      sortAttributes[WTPART] =
         "1 1 1 0 0 0 1";
      sortAttributes[WTCHANGEISSUE] =
         "1 1 0 1 1 0 0";
      sortAttributes[WTCHANGEREQUEST] =
         "1 1 0 0 0 0";
      sortAttributes[WTCHANGEINVESTIGATION] =
         "1 1 0 1 1 0";
      sortAttributes[WTANALYSISACTIVITY] =
         "1 1 0 1 1 0";
      sortAttributes[WTCHANGEPROPOSAL] =
         "1 1 0 1 0";
      sortAttributes[WTCHANGEORDER] =
         "1 1 0 1 0";
      sortAttributes[WTCHANGEACTIVITY] =
         "1 1 0 1 1 0";
      sortAttributes[WTDOCUMENT] =
         "1 1 1 0 0 1";
      sortAttributes[PROXYPARTMASTER] =
         "1 1 1 1 0";
      sortAttributes[PROXYDOCUMENTMASTER] =
         "1 1 1 1 0";
      sortAttributes[WFPROCESS] =
         "1 0 1 0";
      sortAttributes[EPMDOCUMENT] =
         "1 1 1 0 0 1";
      sortAttributes[BUSINESSENTITY] =
         "1 1";
      sortAttributes[CONFIGURATIONITEM] =
         "1 0 0 1";
      sortAttributes[REPORTTEMPLATE] =
         "1 1";
      sortAttributes[WTUNIT] =
         "1 1 1 0 0 0 1";
      sortAttributes[WTPRODUCT] =
         "1 1 1 0 0 0 1";
      sortAttributes[WTPRODUCTCONFIGURATION] =
         "1 0 0";
      sortAttributes[WTPRODUCTINSTANCE2] =
         "1 1 1 1 1 0 1 1";
      sortAttributes[RSCOMP] =
         "1 1 0";
      sortAttributes[RSCONN] =
         "1 1 0";
      sortAttributes[RSSPOOL] =
         "1 1 0";
      sortAttributes[RSFITTING] =
         "1 1 0";
      sortAttributes[RSMANUF] =
         "1 1 0";
      sortAttributes[WTMFGMODEL] =
         "1 1";
      sortAttributes[WTMANAGEDBASELINE] =
         "1 1 1 1";

      // **New for 6.0
      // **Customize  This is similar in concept to outputAttributes only this list is used
      // for assigning a unique key to the sort preferences for this search.  This string will
      // be persisted and used to retrive the sort preferences for users.  If the value of one
      // of these strings is changed or deleted after the system is in operation it will create orphaned
      // preferences in the system and users will lose the value that they had persisted for that
      // search.  New entries can be added when a new search is added so that sort preferences
      // can be saved for that new search.  These strings are arbitrary and never displayed to the user.
      sortPref = new String[classCount];
      sortPref[ALL] =
         "all";
      sortPref[WTPART] =
         "wtpart";
      sortPref[WTCHANGEISSUE] =
         "wtchangeissue";
      sortPref[WTCHANGEREQUEST] =
         "wtchangereq";
      sortPref[WTCHANGEINVESTIGATION] =
         "wtchangeinvest";
      sortPref[WTANALYSISACTIVITY] =
         "wtanalysisact";
      sortPref[WTCHANGEPROPOSAL] =
         "wtchangeprop";
      sortPref[WTCHANGEORDER] =
         "wtchangeorder";
      sortPref[WTCHANGEACTIVITY] =
         "wtchangeact";
      sortPref[WTDOCUMENT] =
         "wtdoc";
      sortPref[PROXYPARTMASTER] =
         "proxypartmaster";
      sortPref[PROXYDOCUMENTMASTER] =
         "proxydocmaster";
      sortPref[WFPROCESS] =
         "wfprocess";
      sortPref[EPMDOCUMENT] =
         "epmdoc";
      sortPref[BUSINESSENTITY] =
         "busent";
      sortPref[CONFIGURATIONITEM] =
         "configitem";
      sortPref[REPORTTEMPLATE] =
         "reporttemplate";
      sortPref[WTUNIT] =
         "wtunit";
      sortPref[WTPRODUCT] =
         "wtproduct";
      sortPref[WTPRODUCTCONFIGURATION] =
         "wtproductconfiguration";
      sortPref[WTPRODUCTINSTANCE2] =
         "wtproductinstance2";
      sortPref[RSCOMP] =
         "rscomp";
      sortPref[RSCONN] =
         "rsconn";
      sortPref[RSSPOOL] =
         "rsspool";
      sortPref[RSFITTING] =
         "rsfitting";
      sortPref[RSMANUF] =
         "rsmanuf";
      sortPref[WTMFGMODEL] =
         "wtmfgmodel";
      sortPref[WTMANAGEDBASELINE] =
         "wtmanagedbaseline";
   }

   @Override
   public Vector getPickList() {
      int size = index;
      Vector list = new Vector(size);

      // To support the CADX HTMLUI Picker WTMANAGEDBASELINE has been added
      // in setLocale. WTMANAGEDBASELINE is not supported in WC Foundation's
      // HTML Search, so WTMANAGEDBASELINE has to be filtered out.
      for (int i = 0; i < size; i++) {
         if (!pickList[i].equals(WTMessage.getLocalizedMessage(RESOURCE,queryResource.WTMANAGEDBASELINE,null,clientLocale))) {
            list.addElement(pickList[i]);
         }
      }
      return list;
   }

   @Override
   public Vector getPickValues() {
      int size = index;
      Vector list = new Vector(size);

      // To support the CADX HTMLUI Picker WTMANAGEDBASELINE has been added
      // in setLocale. WTMANAGEDBASELINE is not supported in WC Foundation's
      // HTML Search, so WTMANAGEDBASELINE has to be filtered out.
      for (int i = 0; i < size; i++) {
         if (!pickValues[i].equals(queryResource.WTMANAGEDBASELINE)) {
            list.addElement(pickValues[i]);
         }
      }
      return list;
   }

   @Override
   public int getQueryType( String value )
            throws WTException {
      String pick = WTMessage.getLocalizedMessage(getQueryResource(), value, null, clientLocale);
      boolean found = false;
      int i;
      int size = index;
      for (i = 0; i < size && !found; i++) {
         if (pickList[i].equals(pick))
            found = true;
      }
      if (!found) {
         Object[] param = {pick};
         throw new WTException(RESOURCE,queryResource.BAD_CONFIGURATION,param);
      }
      else
         return i - 1;
   }

   @Override
   public Vector getQueryClass( int query_type )
            throws WTException {
      Vector vector;
      try {
         vector = getStringVector(queryClass,query_type);
      }
      catch (WTException wte) {
        Object[] param = {"\"queryClass\""};
        throw new WTException(wte,RESOURCE,queryResource.BAD_CONFIGURATION,
                                                                         param);
      }
      int size = vector.size();
      Vector classes = new Vector(size);
      for (int i = 0; i < size; i++) {
         String class_name = (String)vector.elementAt(i);
         try {
            classes.addElement(Class.forName(class_name));
         }
         catch (ClassNotFoundException cnfe) {
           Object[] param = {class_name};
           throw new WTException(cnfe,RESOURCE,queryResource.BAD_CONFIGURATION,
                                                                         param);
         }
      }
      return classes;
   }

   @Override
   public Vector getInputAttributes( int query_type )
            throws WTException {
      Vector vector;
      try {
         vector = getStringVector(inputAttributes,query_type);
      }
      catch (WTException wte) {
        Object[] param = {"\"inputAttributes\""};
        throw new WTException(wte,RESOURCE,queryResource.BAD_CONFIGURATION,
                                                                         param);
      }
      return vector;
   }

   /**
    *
    * <BR><BR><B>Supported API: </B>false
    *
    * @deprecated
    *
    * @param     attributes
    * @param     class_name
    * @return    Vector
    * @exception wt.util.WTException
    **/
   public static Vector getPropertyDescriptors( Vector attributes, Class class_name )
            throws WTException {
      Vector vector = new Vector(attributes.size());
      ClassInfo context_info;
      try {
         context_info = WTIntrospector.getClassInfo( class_name);
      }
      catch (WTException wtie) {
        Object[] param = {"\"display_attributes\""};
        throw new WTException(wtie,RESOURCE,queryResource.BAD_CONFIGURATION,
                                                                         param);
      }
      for (int i = 0; i < attributes.size(); i++) {
         try {
            PropertyDescriptor prop =
                 context_info.getPropertyDescriptor((String) attributes.elementAt(i));
            vector.addElement(prop);
         }
         catch (WTIntrospectionException wtie) {
            vector.addElement(null);
         }
      }
      return vector;
   }

   /**
    *
    * <BR><BR><B>Supported API: </B>false
    *
    * @deprecated
    *
    * @param     attributes
    * @param     classes
    * @return    Vector
    * @exception wt.util.WTException
    **/
   public static Vector getPropertyDescriptors( Vector attributes, Vector classes )
            throws WTException {
      Vector vector = new Vector(classes.size());
      try {
         for (int i = 0; i < classes.size(); i++) {
            Vector pd = getPropertyDescriptors(attributes,(Class)classes.elementAt(i));
            vector.addElement(pd);
         }
      }
      catch (WTException wtie) {
        Object[] param = {"\"display_attributes\""};
        throw new WTException(wtie,RESOURCE,queryResource.BAD_CONFIGURATION,
                                                                         param);
      }
      return vector;
   }

   @Override
   public Vector getInputProcessing( int query_type )
            throws WTException {
      Vector vector;
      try {
         vector = getStringVector(inputProcessing,query_type);
      }
      catch (WTException wte) {
        Object[] param = {"\"inputProcessing\""};
        throw new WTException(wte,RESOURCE,queryResource.BAD_CONFIGURATION,
                                                                         param);
      }
      return vector;
   }

   @Override
   public Vector getOutputAttributes( int query_type )
            throws WTException {
      Vector vector;
      try {
         vector = getStringVector(outputAttributes,query_type);
      }
      catch (WTException wte) {
        Object[] param = {"\"outputAttributes\""};
        throw new WTException(wte,RESOURCE,queryResource.BAD_CONFIGURATION,
                                                                         param);
      }
      return vector;
   }

   @Override
   public Vector getOutputProcessing( int query_type )
            throws WTException {
      Vector vector;
      try {
         vector = getStringVector(outputProcessing,query_type);
      }
      catch (WTException wte) {
        Object[] param = {"\"outputProcessing\""};
        throw new WTException(wte,RESOURCE,queryResource.BAD_CONFIGURATION,
                                                                         param);
      }
      return vector;
   }

   @Override
   public Vector [] getSortedPick() {
        //get pick list
        Vector pickList = getPickList();
        //get pick values
        Vector pickValues = getPickValues();

        //create collator based on locale
        Collator collator = Collator.getInstance(clientLocale);
        CollationKey tmpKey = null;
        CollationKey[] pickListKeys = new CollationKey[pickList.size()];
        String[] pickValuesString = new String[pickValues.size()];
        String tmpString = null;

        for (int j=0; j<pickList.size(); j++) {
            pickListKeys[j] = collator.getCollationKey((String)pickList.get(j));
            pickValuesString[j] = (String)pickValues.get(j);
        }

        // start at 1 so that "All" is always the first value in the drop down list regardless of where it comes alphabetically.
        for (int i=1; i<pickListKeys.length; i++) {
            for (int j=1; j<pickListKeys.length; j++) {
                if( pickListKeys[i].compareTo( pickListKeys[j] ) < 0 ) {
                    // swap pickListKeys[i] and pickListKeys[j]
                    tmpKey = pickListKeys[j];
                    pickListKeys[j] = pickListKeys[i];
                    pickListKeys[i] = tmpKey;

                    // swap pickValuesString[i] and pickValuesString[j]
                    tmpString = pickValuesString[j];
                    pickValuesString[j] = pickValuesString[i];
                    pickValuesString[i] = tmpString;
                }
            }
        }

        pickList = new Vector();
        pickValues = new Vector();

        //put new sorted list back into Vectors.
        for (int j=0; j<pickListKeys.length; j++) {
            pickList.add(pickListKeys[j].getSourceString());
            pickValues.add(pickValuesString[j]);
        }

        return new Vector[] {pickList, pickValues};
   }

   @Override
   public int [] getSortedQueryType( String value )
            throws WTException {
      String pick = WTMessage.getLocalizedMessage(getQueryResource(), value, null, clientLocale);
      boolean found = false;
      int i;
      int size = index;
      int queryValue = 0;
      Vector sortedList = getSortedPick()[0];
      Vector sortedValues = getSortedPick()[1];
      for (i = 0; i < size && !found; i++) {
        //Found the correct entry for the sorted list
         if (((String)sortedList.get(i)).equals(pick))
         {
            //The sortedValues correspond to the rbinfo files
            //so the query type will not be correct according to the
            //integers defined in the class for each class type
            for (int x=0; x < classCount; x++)
            {
                if (((String)pickList[x]).equals(pick))
                {
                    queryValue = x;
                    found = true;
                    break;
                }
            }
         }
      }
      if (!found) {
         Object[] param = {pick};
         throw new WTException(RESOURCE,queryResource.BAD_CONFIGURATION,param);
      }
      else
      {
         return new int [] {queryValue,i - 1};
      }
   }

   /**
    * Reads the non-transient fields of this class from an external source.
    *
    * @param     input
    * @param     readSerialVersionUID
    * @param     superDone
    * @return    boolean
    * @exception java.io.IOException
    * @exception java.lang.ClassNotFoundException
    **/
   private boolean readVersion957977401221134810L( ObjectInput input, long readSerialVersionUID, boolean superDone )
            throws IOException, ClassNotFoundException {
      return true;
   }

   private Vector getStringVector(String[] array, int query_type) throws WTException {
      if (array.length < query_type) {
        throw new WTException();
      }
      String s = array[query_type];
      Vector vector = new Vector(20);
      StringTokenizer st = new StringTokenizer(s);
      while (st.hasMoreTokens()) {
        vector.addElement(st.nextToken());
      }
      return vector;
   }

   /**
    *  Return the list of attributes for only the attributes that can
    *  be sorted.
    * <BR><BR><B>Supported API: </B>false
    *
    * @param     query_type
    * @return    Vector
    * @exception wt.util.WTException
    **/
   public Vector getSortAttributes( int query_type )
            throws WTException {
      Vector output;
      Vector sortable;
      Vector list = new Vector();
      try {
         output = getStringVector(outputAttributes,query_type);
         sortable = getStringVector(sortAttributes,query_type);
      }
      catch (WTException wte) {
        Object[] param = {"\"sortAttributes\""};
        throw new WTException(wte,RESOURCE,queryResource.BAD_CONFIGURATION,
                                                                         param);
      }
      int outsize = output.size();
      for (int i = 0; i < outsize; i++) {
         String sort_attrib = (String)sortable.elementAt(i);
         if (sort_attrib.equals("1")) {
            list.addElement((String)output.elementAt(i));
         }
         else if (!sort_attrib.equals("0")) {
            list.addElement(sort_attrib);
         }
      }
      return list;
   }

   /**
    *  Return the list of display attributes for only the attributes that can
    *  be sorted.
    * <BR><BR><B>Supported API: </B>false
    *
    * @param     query_type
    * @return    Vector
    * @exception wt.util.WTException
    **/
   public Vector getSortList( int query_type )
            throws WTException {
      Vector output;
      Vector sortable;
      Vector list = new Vector();
      try {
         sortable = getStringVector(sortAttributes,query_type);
         output = getStringVector(outputAttributes,query_type);
      }
      catch (WTException wte) {
        Object[] param = {"\"sortAttributes\""};
        throw new WTException(wte,RESOURCE,queryResource.BAD_CONFIGURATION,
                                                                         param);
      }
      int outsize = output.size();
      for (int i = 0; i < outsize; i++) {
         String sort_attrib = (String)sortable.elementAt(i);
         if (!sort_attrib.equals("0")) {
            list.addElement((String)output.elementAt(i));
         }
      }
      return list;
   }

   /**
    *  Return the list of attributes for all the attributes that can
    *  be sorted.  Put in blanks for attributes that can't be sorted.
    * <BR><BR><B>Supported API: </B>false
    *
    * @param     query_type
    * @return    Vector
    * @exception wt.util.WTException
    **/
   public Vector getAllSortAttributes( int query_type )
            throws WTException {
      Vector output;
      Vector sortable;
      Vector list = new Vector();
      try {
         output = getStringVector(outputAttributes,query_type);
         sortable = getStringVector(sortAttributes,query_type);
      }
      catch (WTException wte) {
        Object[] param = {"\"sortAttributes\""};
        throw new WTException(wte,RESOURCE,queryResource.BAD_CONFIGURATION,
                                                                         param);
      }
      int outsize = output.size();
      for (int i = 0; i < outsize; i++) {
         String sort_attrib = (String)sortable.elementAt(i);
         if (sort_attrib.equals("1")) {
            list.addElement((String)output.elementAt(i));
         }
         else if (!sort_attrib.equals("0")) {
            list.addElement(sort_attrib);
         }
         else
            list.addElement("");
      }
      return list;
   }

   /**
    *  Return the sort key for a query type
    * <BR><BR><B>Supported API: </B>false
    *
    * @param     query_type
    * @return    String
    **/
   public String getSortKey( int query_type ) {
      return sortPref[query_type];
   }

   /**
    *  Sets the query resource file to use.
    * <BR><BR><B>Supported API: </B>false
    *
    * @param     theQueryResource
    * @return    String
    **/
   public String setQueryResource( String theQueryResource ) {
      if (theQueryResource == null || theQueryResource.length() == 0) {
        currentQueryResource = RESOURCE;
      }
      else {
        currentQueryResource = theQueryResource;
      }
      return currentQueryResource;
   }

   /**
    *  Gets the current query resource file.
    * <BR><BR><B>Supported API: </B>false
    *
    * @return    String
    **/
   public String getQueryResource( ) {
     if (currentQueryResource == null || currentQueryResource.length() == 0) {
        setQueryResource(RESOURCE);
     }
     return currentQueryResource;
   }
}
