package wt.query;

import wt.util.resource.*;

@RBUUID("wt.query.queryResource")
public final class queryResource extends WTListResourceBundle {
   /**
/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
    * queryResource message resource bundle [English/US]
    **/
   @RBEntry("A query error occurred. System message follows:")
   public static final String QUERY_ERROR = "0";

   @RBEntry("Select statement syntax error: \"{0}\"")
   @RBArgComment0(" {0} is the specific syntax error:")
   public static final String SELECT_SYNTAX_ERROR = "1";

   @RBEntry("Attribute \"{0}\" is not a member of class \"{1}\"")
   @RBArgComment0(" {0} is the attribute name")
   @RBArgComment1(" {1} is the class name:")
   public static final String ATTR_NOT_IN_CLASS = "2";

   @RBEntry("Attribute \"{0}\" of class \"{1}\" is not persistable")
   @RBArgComment0(" {0} is the attribute name")
   @RBArgComment1(" {1} is the class name:")
   public static final String ATTR_NOT_PERSISTABLE = "3";

   @RBEntry("Attribute \"{0}\" of class \"{1}\" is not of type \"{2}\"")
   @RBArgComment0(" {0} is the attribute name")
   @RBArgComment1(" {1} is the class name")
   @RBArgComment2(" {2} is the value type of the Search Condition:")
   public static final String TYPE_MISMATCH = "4";

   @RBEntry("Search Condition value is not persistent.  ObjectIdentifier is null.")
   public static final String NULL_OID = "5";

   @RBEntry("Search Condition class \"{0}\" is not a subclass of or equal to target class \"{1}\"")
   @RBArgComment0(" {0} is the name of the SearchCondition class")
   @RBArgComment1(" {1} is the name of the QuerySpec target class:")
   public static final String TARGET_CLASS_MISMATCH = "6";

   @RBEntry("Search Condition class \"{0}\" is not a subclass of or equal to target class \"{1}\" nor link class \"{2}\"")
   @RBArgComment0(" {0} is the name of the SearchCondition class")
   @RBArgComment1(" {1} is the name of the QuerySpec target class")
   @RBArgComment2(" {2} is the name of the QuerySpec link class:")
   public static final String TARGET_OR_LINK_MISMATCH = "7";

   @RBEntry("Timestamp values for Search Condition are null.")
   public static final String NULL_TIMESTAMP = "8";

   @RBEntry("Value array is empty for Search Condition.")
   public static final String EMPTY_VALUE_ARRAY = "9";

   @RBEntry("Error converting value for search, \"{0}\" is not a valid \"{1}\".")
   @RBArgComment0("  {0} is the given value")
   @RBArgComment1(" {1} is the type it fails to match:")
   public static final String VALUE_MISMATCH = "10";

   @RBEntry("Search Condition class position, \"{0}\", is greater than QuerySpec max value, \"{1}\".")
   @RBArgComment0("  {0} is the given value,")
   @RBArgComment1(" {1} is the type it fails to match:")
   public static final String SC_OUT_OF_RANGE = "11";

   @RBEntry("Illegal java type for AttributeRange, \"{0}\".")
   @RBArgComment0("  {0} is a java type that is not one of the legal types for AttributeRange:")
   public static final String ILLEGAL_RANGE_JAVA_TYPE = "12";

   @RBEntry("Error converting values for search, \"{0}\"  \"{1}\" one or both are not a valid \"{2}\".")
   @RBArgComment0("  {0} is the start value of a range")
   @RBArgComment1(" {1} is the end value and {2} is the type that one or both fail to match.")
   public static final String RANGE_VALUES_INVALID = "13";

   @RBEntry("Cabinet \"{0}\" not valid.")
   @RBArgComment0("  {0} is the cabinet name.")
   public static final String INVALID_CABINET = "14";

   @RBEntry("Search type \"{0}\" not supported on this type \"{1}\" of field.")
   public static final String UNSUPPORTED_SEARCH_TYPE = "15";

   @RBEntry("SQL Function type \"{0}\" with format \"{1}\" is not supported.")
   public static final String UNSUPPORTED_SQL_FUNCTION = "16";

   @RBEntry("SQL Function type \"{0}\" with attribute \"{1}\" of type \"{2}\" is not supported.")
   @RBArgComment0("  {0} is the name of the function")
   @RBArgComment1(" {1} is the attribute specified, {2} is the java type of the attribute")
   public static final String UNSUPPORTED_ATTR_TYPE = "17";

   @RBEntry("The SQLFunction class requires a function.  Null is not valid.")
   @RBComment(" Null function is not allowed message.")
   public static final String NULL_SQL_FUNCTION = "18";

   @RBEntry("SQL Function type \"{0}\" requires a format. Null is not valid.")
   @RBComment(" Null format is not allowed message.")
   @RBArgComment0(" {0} is the name of the function.")
   public static final String NULL_SQL_FORMAT = "19";

   /**
    * HTML Local Search Text for class picker.  No underbars needed for values!!
    *
    **/
   @RBEntry("All")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String ALL = "20";

   @RBEntry("Part")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WTPART = "21";

   @RBEntry("Document")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WTDOCUMENT = "22";

   @RBEntry("Change Request")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WTCHANGEREQUEST = "23";

   @RBEntry("Change Activity")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WTCHANGEACTIVITY = "24";

   @RBEntry("Change Order")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WTCHANGEORDER = "25";

   @RBEntry("Replication Unit")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WTUNIT = "65";

   @RBEntry("Problem in configuring this search in {0}.")
   @RBArgComment0(" {0} is the array name or attribute name for the data that configures the columns, criteria, pick list for the HTML local search.")
   public static final String BAD_CONFIGURATION = "26";

   @RBEntry("Search Results for {0}")
   public static final String SEARCH_SUBTITLE = "27";

   @RBEntry("Error formating the search screen.")
   public static final String SEARCH_FORMAT = "28";

   @RBEntry("Search On:")
   public static final String SEARCH_PICKER = "29";

   @RBEntry("Your search found {0} objects.  But was unable to format the {1} class objects for display.")
   @RBArgComment0(" {0} is the count of objects found in search")
   @RBArgComment1(" {1} is the class of the object that couldn't be formated.")
   public static final String SEARCH_RESULT = "30";

   @RBEntry("Your search \"{0}\" found {1} objects.")
   @RBArgComment0(" {0} the search criteria")
   @RBArgComment1(" {1} is the count of objects found in search.")
   public static final String SEARCH_SUCCESS = "31";

   @RBEntry("Error setting up or executing the search.")
   public static final String SEARCH_ERROR = "32";

   @RBEntry("Search")
   public static final String DO_SEARCH = "33";

   @RBEntry("Fill in one or more search criteria and click \"Do Search\" to return your search results.")
   public static final String DO_SEARCH_TEXT = "34";

   @RBEntry("Error converting value, \"{0}\" is not a valid Date.")
   @RBArgComment0(" {0} is the given value")
   @RBArgComment1(" {1} is the type it fails to match:")
   public static final String INVALID_DATE = "35";

   @RBEntry("Proxy Part Master")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String PROXYPARTMASTER = "36";

   @RBEntry("Proxy Document Master")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String PROXYDOCUMENTMASTER = "37";

   @RBEntry("Class Attribute class \"{0}\" is not a super class of or equal to any of the table classes \"{1}\"")
   public static final String CLASS_ATTRIBUTE_MISMATCH = "38";

   @RBEntry("Both start and end range values are null.")
   public static final String NULL_RANGE = "39";

   @RBEntry("Change Issue")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WTCHANGEISSUE = "40";

   @RBEntry("The index specified does not reference a valid expression in the SQL FROM clause.")
   public static final String INVALID_FROM_INDEX = "41";

   @RBEntry("Process")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WFPROCESS = "42";

   @RBEntry("Change Investigation")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WTCHANGEINVESTIGATION = "43";

   @RBEntry("Analysis Activity")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WTANALYSISACTIVITY = "44";

   @RBEntry("Change Proposal")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WTCHANGEPROPOSAL = "45";

   @RBEntry("Old Change Request")
   public static final String OLDWTCHANGEREQUEST = "46";

   @RBEntry("Old Change Order")
   public static final String OLDWTCHANGEORDER = "47";

   @RBEntry("Old Change Activity")
   public static final String OLDWTCHANGEACTIVITY = "48";

   @RBEntry("CAD Document")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String EPMDOCUMENT = "49";

   @RBEntry("Business Entity")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String BUSINESSENTITY = "53";

   @RBEntry("Configuration Item")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String CONFIGURATIONITEM = "54";

   @RBEntry("Product Instance")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WTPRODUCTINSTANCE2 = "55";

   @RBEntry("View all {0} results")
   @RBArgComment0("  {0} is the total number of results.")
   public static final String VIEW_ALL_RESULTS = "50";

   @RBEntry("The first {0} results are displayed.")
   @RBArgComment0("  {0} is the number of results displayed.")
   public static final String NUMBER_SHOWN_MESSAGE = "51";

   @RBEntry("(1 - {0} of {1})")
   @RBArgComment0("  {0} is the number of results displayed on the current page")
   @RBArgComment1(" {1} is the total number of results found.")
   public static final String NUMBER_SHOWN = "52";

   @RBEntry("Manufacturing Operation")
   public static final String PMOPERATION = "56";

   @RBEntry("Manufacturing Feature")
   public static final String PMMFGFEATURE = "58";

   @RBEntry("Manufacturing Assembly")
   public static final String PMASSEMBLY = "59";

   @RBEntry("Manufacturing Study")
   public static final String PMSTUDY = "60";

   @RBEntry("Document Master")
   public static final String WTDOCUMENTMASTER = "61";

   @RBEntry("Manufacturing Resources")
   public static final String PMRESOURCES = "62";

   @RBEntry("Part Master")
   public static final String WTPARTMASTER = "63";

   @RBEntry("Report Template")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String REPORTTEMPLATE = "64";

   @RBEntry("Error closing the paging session from the last search.")
   public static final String CLOSE_PAGING_ERROR = "66";

   @RBEntry("Error fetching the next page of results from the database.")
   public static final String FETCH_NEXT_PAGE_ERROR = "67";

   @RBEntry("Error executing the database query for this search.")
   public static final String DATABASE_QUERY_ERROR = "68";

   @RBEntry("Error converting the criteria into a database query for this search.")
   public static final String FORMING_CRITERIA_ERROR = "69";

   @RBEntry("The object selected to search against is not a valid object: {0}.")
   @RBArgComment0("Class name that could not be understood for the search.")
   public static final String INVALID_SEARCH_CLASS = "70";

   @RBEntry("Error using the results of the content search to access objects in the database.")
   public static final String OID_CRITERIA = "71";

   @RBEntry("Error creating the configuration specification to filter this search.")
   public static final String CONFIG_SPEC = "72";

   @RBEntry("The object returned from the content search is not a recognizable object in this system: {0}.")
   @RBArgComment0("Full classname from content search engine.")
   public static final String CONTENT_CLASS_ERROR = "73";

   @RBEntry("Error in adding the following to the search critieria: name = {0}, value = {1}.")
   @RBArgComment0("Attribute name")
   @RBArgComment1("Attribute value")
   public static final String ERROR_NAME_VALUE = "74";

   @RBEntry("XXX")
   @RBPseudo(false)
   public static final String XXX_75 = "75";

   @RBEntry("XXX")
   @RBPseudo(false)
   public static final String XXX_76 = "76";

   @RBEntry("XXX")
   @RBPseudo(false)
   public static final String XXX_77 = "77";

   @RBEntry("XXX")
   @RBPseudo(false)
   public static final String XXX_78 = "78";

   @RBEntry("XXX")
   @RBPseudo(false)
   public static final String XXX_79 = "79";

   @RBEntry("XXX")
   @RBPseudo(false)
   public static final String XXX_80 = "80";

   @RBEntry("XXX")
   @RBPseudo(false)
   public static final String XXX_81 = "81";

   @RBEntry("XXX")
   @RBPseudo(false)
   public static final String XXX_82 = "82";

   @RBEntry("XXX")
   @RBPseudo(false)
   public static final String XXX_83 = "83";

   @RBEntry("XXX")
   @RBPseudo(false)
   public static final String XXX_84 = "84";

   @RBEntry("Error, no object types were specified to search on for this search.")
   public static final String NO_OBJECTS_FOR_SEARCH = "85";

   @RBEntry("Error initializing a query spec for: {0}.")
   @RBArgComment0("Class name of object that is having a problem.")
   public static final String INIT_QUERY_SPEC = "86";

   @RBEntry("XXX")
   @RBPseudo(false)
   public static final String XXX_87 = "87";

   @RBEntry("XXX")
   @RBPseudo(false)
   public static final String XXX_88 = "88";

   @RBEntry("XXX")
   @RBPseudo(false)
   public static final String XXX_89 = "89";

   @RBEntry("XXX")
   @RBPseudo(false)
   public static final String XXX_90 = "90";

   @RBEntry("Cannot handle multiple from indices in an ItemBuilder")
   public static final String MULTIPLE_FROM_INDICES = "91";

   @RBEntry("The number of classes in the FROM portion of this query ({0}) exceeds the page result table column size ({1}).")
   @RBArgComment0(" {0} is the number of classes in the FROM portion of the query")
   @RBArgComment1(" {1} is the number of columns in the page results table")
   public static final String OUT_OF_CLASS_COUNT_RANGE = "92";

   @RBEntry("This is an invalid paging statementspec since distinct feature is set for it.")
   public static final String DISTINCT_NOT_ALLOWED_IN_PAGING = "94";

   @RBEntry("This is an invalid statementspec.")
   public static final String INVALID_STATEMENTSPEC = "95";

   @RBEntry("Cannot handle non-QuerySpec components in CompoundQuerySpec or CompositeQuerySpec.")
   public static final String NESTED_NON_QUERYSPEC_NOT_SUPPORTED = "96";

   @RBEntry("This is an invalid CompoundQuerySpec because it has null or only one component.")
   public static final String INVALID_COMPOUND_QUERYSPEC = "97";

   @RBEntry("This is an invalid CompositeQuerySpec because it has null or only one component.")
   public static final String INVALID_COMPOSITE_QUERYSPEC = "98";

   @RBEntry("Product")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WTPRODUCT = "99";

   @RBEntry("Constant value is null.")
   public static final String NULL_CONSTANT_VALUE = "100";

   @RBEntry("Wild cards like * cannot be used in the following field: {0}")
   @RBArgComment0("Localized fieldname for field that does not allow wild cards.")
   public static final String WILD_CARD_NOT_SUPPORTED = "101";

   @RBEntry("Component")
   @RBComment("RSDB searchable objects  DO NOT MODIFY THE INDEX NUMBERS, it will break HTML Search.")
   public static final String RSCOMP = "102";

   @RBEntry("Connection")
   @RBComment("RSDB searchable objects  DO NOT MODIFY THE INDEX NUMBERS, it will break HTML Search.")
   public static final String RSCONN = "103";

   @RBEntry("Spool")
   @RBComment("RSDB searchable objects  DO NOT MODIFY THE INDEX NUMBERS, it will break HTML Search.")
   public static final String RSSPOOL = "104";

   @RBEntry("Fitting")
   @RBComment("RSDB searchable objects  DO NOT MODIFY THE INDEX NUMBERS, it will break HTML Search.")
   public static final String RSFITTING = "105";

   @RBEntry("Manufacturing Feature")
   @RBComment("RSDB searchable objects  DO NOT MODIFY THE INDEX NUMBERS, it will break HTML Search.")
   public static final String RSMANUF = "106";

   @RBEntry("Manufacturing Model")
   @RBComment("eNC searchable object  DO NOT MODIFY THE INDEX NUMBERS, it will break HTML Search.")
   public static final String WTMFGMODEL = "107";

   @RBEntry("Array expression has no values.")
   public static final String NO_ARRAY_VALUES = "108";

   @RBEntry("Class \"{0}\" and none of its subclasses are a Persistable, concrete class.")
   @RBArgComment0(" {0} is the name of the class:")
   public static final String CLASS_NOT_PERSISTABLE_CONCRETE = "109";

   @RBEntry("Serial Numbered Part")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WTSERIALNUMBEREDPART = "110";

   @RBEntry("Product Configuration")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WTPRODUCTCONFIGURATION = "111";

   @RBEntry(":")
   @RBPseudo(false)
   @RBComment("Colon that is added to attribute names in search criteria.")
   public static final String ATTRIBUTE_COLON = "112";

   @RBEntry("Specified SQL keyword and type are not compatible: name={0} type={1} specified type={2}.")
   public static final String INVALID_KEYWORD_TYPE = "113";

   @RBEntry("Managed Baseline")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WTMANAGEDBASELINE = "114";

   @RBEntry("Search engine is not configured properly or it is not running.")
   @RBComment("Error message to display when the Search Engine (Rware) is not configured with Windchill")
   public static final String SEARCH_ENGINE_NOT_CONFIGURED = "115";

   @RBEntry("String \"{0}\" is not a valid hint.")
   @RBComment("{0} is the string representation of the hint for a SQL statement")
   public static final String INVALID_HINT = "116";

   @RBEntry("String \"{0}\" is not a valid constant.")
   @RBComment("{0} is the string representation of the constant for a SQL statement")
   public static final String INVALID_CONSTANT = "117";

   @RBEntry("Full persistable retrieval is not valid using ClassViewExpression.")
   @RBComment("Full persistable retrieval is not valid using ClassViewExpression")
   public static final String INVALID_FULL_PERSISTABLE_RETRIEVAL = "118";

   @RBEntry("Column Alias \"{0}\" is not valid. The column alias should be a valid datastore identifier and datastore reserved words are not allowed.")
   @RBComment("{0} is the string representation of the alias for a SQL Column.")
   public static final String INVALID_ALIAS = "119";

   @RBEntry("Date")
   @RBComment("In previous version of Windchill the actual \"object\" name was displayed.  Since we are receiving field problems in other languages of non-translated strings this would should be translated.  It appears within quotes within INVALID_DATE above (the second entry)")
   public static final String DATE = "120";

   @RBEntry("Search condition option \"{0}\" is not valid. The option can only be used for specifying an escape character.")
   @RBComment("{0} is the string representation of the search condition option.")
   public static final String INVALID_OPTION = "121";

   @RBEntry("The query cannot be executed in paging mode.")
   public static final String INVALID_PAGING_QUERY = "122";

   @RBEntry("OK")
   @RBComment("HTML Search Find Users Ok button")
   public static final String OK_BUTTON = "123";

   @RBEntry("Browse...")
   @RBComment("HTML Search Find Users Find button")
   public static final String FIND_USERS_BUTTON = "124";

   @RBEntry("Error generating the Find Users function.")
   public static final String FIND_USER_ERROR = "125";

   @RBEntry("Search")
   @RBComment("HTML Search Criteria do search button.")
   public static final String SEARCH_BUTTON = "126";

   @RBEntry("String \"{0}\" is not a valid operator.")
   @RBComment("{0} is the string representation of the operator for a search condition operator")
   public static final String INVALID_OPERATOR = "127";

   @RBEntry("Column expression list is not valid because it contains no expressions.")
   public static final String EMPTY_COLUMN_LIST = "128";

   @RBEntry("Array length \"{0}\" is not valid.")
   @RBComment("{0} is the length of the array")
   public static final String INVALID_ARRAY_LENGTH = "129";

   @RBEntry("Invalid attribute name ")
   public static final String INVALID_ATTRIBUTE_NAME_MESSAGE = "331";

   @RBEntry("Unknown search criteria type ")
   public static final String UNKNOWN_SEARCH_TYPE = "333";

   @RBEntry("\"{0}\" is not a valid SQL function.")
   @RBComment("{0} is the SQL function name")
   public static final String INVALID_SQL_FUNCTION = "400";

   @RBEntry("\"{0}\" is not a valid table name since it is not a valid datastore identifier.")
   @RBComment("{0} is the string representation of the SQL Table name.")
   public static final String INVALID_TABLE_NAME = "500";

   @RBEntry("\"{0}\" is not a valid column name since it is not a valid datastore identifier.")
   @RBComment("{0} is the string representation of the SQL Column name.")
   public static final String INVALID_COLUMN_NAME = "501";

   @RBEntry("\"{0}\" attributes are selected in the subselect and the subselect should have one select attribute.")
   @RBComment("{0} is the number of select attributes in the subselect.")
   public static final String INVALID_NUMBER_SELECT_ATTRIBUTE_IN_THE_SUBSELECT = "502";
}
