/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */

package wt.query;

import java.beans.PropertyDescriptor;
import java.lang.Class;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.Vector;

import wt.introspection.ClassInfo;
import wt.introspection.WTIntrospector;
import wt.introspection.WTIntrospectionException;
import wt.util.WTException;
import wt.util.WTMessage;
import wt.util.WTProperties;
import wt.util.WTStringUtilities;

/**
 * SearchAttributeList
 * There should be no strings that need localizing in here.  The strings in here should be
 * converted to the display values through introspection or resource bundles.
 * This is a temporary storage place for all of the hard coding for HTML Local Search.
 *
 * Supported API: false
 * Extendable: false
 *
 **/
public class ObjectSearchAttributeList {
   private static String  CODEBASE;
   private static boolean VERBOSE;
   private static final String RESOURCE = "wt.query.queryResource";

   static {
      try {
         WTProperties properties = WTProperties.getLocalProperties();
         VERBOSE = properties.getProperty ("wt.enterprise.verbose", false);
         CODEBASE = properties.getProperty ("wt.server.codebase", "");
      }
      catch (Throwable t) {
         Object[] param = { ObjectSearchAttributeList.class.getName ()};
         System.err.println(WTMessage.getLocalizedMessage(
                             "wt.enterprise.enterpriseResource",
                             wt.enterprise.enterpriseResource.ERROR_INITIALIZING,
                             param));
         t.printStackTrace(System.err);
         throw new ExceptionInInitializerError(t);
      }
   }

   private Locale clientLocale;

   public ObjectSearchAttributeList( Locale locale ) {
      clientLocale = locale;
   }

   private final String[] displayName = {
      getDisplayName( 0 ), //"Baselines",
      getDisplayName( 1 ), //"Part Masters",
      getDisplayName( 2 ), //"Configuration Item",
      getDisplayName( 3 ), //"Product Configuration"
      getDisplayName( 4 ), //"Product Instance 2"
      //deprecated Product_Instance  getDisplayName( 6 ), // old style "Product Instance",
   };

   private static final String[] queryClass = {
      // Baseline
      "wt.vc.baseline.ManagedBaseline",
      // Part Master
      "wt.part.WTPartMaster",
      // Configuration Item
      "wt.effectivity.ConfigurationItem",
      // Product Configuration
      "wt.part.WTProductConfiguration",
      // Product Instance 2
      "wt.part.WTProductInstance2",
      // Product Instance
   //deprecated Product_Instance   "wt.effectivity.WTProductInstance",
   };

   private static final String[] inputAttributes = {
      // Baseline
      "number name lifeCycleState",
      // Part Master
      "number name",
      // Configuration Item
      "name effectivityType lifeCycleState",
      // Product Configuration
      "configurationName productNumber productName",
      // Product Instance 2
      "serialNumber productNumber productName",
      // Product Instance
  //deprecated Product_Instance    "configItemName serialNumber",
   };

   public static final String NULL_PARAMETER = "0";

   private static final String[] inputProcessing = {
      // Baseline
      "0 0 0",
      // Part Master
      "0 0",
      // Configuration Item
      "0 0 0",
      // Product Configuration
      "0 0 0",
      // Product Instance 2
      "0 0 0",
      // Product Instance
   //deprecated Product_Instance   "0 0",
   };

   private static final String[] outputAttributes = {
      // Baseline
      "number name displayType teamId lifeCycleState containerName modifyTimestamp",
      // Part Master
      "number name displayType containerName modifyTimestamp",
      // Configuration Item
      "name displayType solutionReference lifeCycleState",
      // Product Configuration
      "configurationName productNumber productName displayType modifyTimestamp",
      // Product Instance 2
      "serialNumber productNumber productName versionDisplayIdentifier displayType modifyTimestamp",
      // Product Instance
  //deprecated Product_Instance    "serialNumber configItemReference solutionReference buildDate",
   };

   private static final String[] displayAttributes = {
      "name",
      "name",
      "name",
      "displayIdentifier",
      "displayIdentifier"
 //deprecated Product_Instance     "serialNumber",
   };

   private static final String[] outputProcessing = {
      // Baseline
      "ObjProps 0 0 0 0 0 0",
      // Part Master
      "ObjProps 0 0 0 0",
      // Configuration Item
      "ObjProps 0 0 0",
      // Product Configuration
      "ObjProps 0 0 0 0",
      // Product Instance 2
      "ObjProps 0 0 0 0 0",
      // Product Instance
 //deprecated Product_Instance     "ObjProps 0 0 0",
   };

   public int getQueryType( String value ) throws WTException{
      return Integer.valueOf(value).intValue();
   }

   public Vector getQueryClass( int query_type ) throws WTException {
      Vector vector;
      try {
         vector = getStringVector(queryClass,query_type);
      }
      catch (WTException wte) {
        Object[] param = {"\"queryClass\""};
        throw wte;
      }
      Vector classes = new Vector(vector.size());
      for (int i = 0; i < vector.size(); i++) {
         String class_name = (String)vector.elementAt(i);
         try {
            classes.addElement(Class.forName(class_name));
         }
         catch (ClassNotFoundException cnfe) {
           Object[] param = {class_name};
           throw new WTException( cnfe );
         }
      }
      return classes;
   }

   public Vector getInputAttributes(int query_type) throws WTException {
      Vector vector;
      try {
         vector = getStringVector(inputAttributes,query_type);
      }
      catch (WTException wte) {
        Object[] param = {"\"inputAttributes\""};
        throw wte;
      }
      return vector;
   }

   public static Vector getPropertyDescriptors(Vector attributes,Class class_name)
                                                            throws WTException {
      Vector vector = new Vector(attributes.size());
      ClassInfo context_info;
      try {
         context_info = WTIntrospector.getClassInfo( class_name);
      }
      catch (WTException wtie) {
        Object[] param = {"\"display_attributes\""};
        throw wtie;
      }
      for (int i = 0; i < attributes.size(); i++) {
         try {
            PropertyDescriptor prop =
                 context_info.getPropertyDescriptor((String) attributes.elementAt(i));
            vector.addElement(prop);
         }
         catch (WTIntrospectionException wtie) {
            vector.addElement(null);
         }
      }
      return vector;
   }

   public static Vector getPropertyDescriptors(Vector attributes,Vector classes)
                                                            throws WTException {
      Vector vector = new Vector(classes.size());
      try {
         for (int i = 0; i < classes.size(); i++) {
            Vector pd = getPropertyDescriptors(attributes,(Class)classes.elementAt(i));
            vector.addElement(pd);
         }
      }
      catch (WTException wtie) {
        Object[] param = {"\"display_attributes\""};
        throw wtie;
      }
      return vector;
   }

   public Vector getInputProcessing(int query_type) throws WTException {
      Vector vector;
      try {
         vector = getStringVector(inputProcessing,query_type);
      }
      catch (WTException wte) {
        Object[] param = {"\"inputProcessing\""};
        throw wte;
      }
      return vector;
   }

   public Vector getOutputAttributes(int query_type) throws WTException {
      Vector vector;
      try {
         vector = getStringVector(outputAttributes,query_type);
      }
      catch (WTException wte) {
        Object[] param = {"\"outputAttributes\""};
        throw wte;
      }
      return vector;
   }

   public Vector getOutputProcessing(int query_type) throws WTException {
      Vector vector;
      try {
         vector = getStringVector(outputProcessing,query_type);
      }
      catch (WTException wte) {
        Object[] param = {"\"outputProcessing\""};
        throw wte;
      }
      return vector;
   }

   private Vector getStringVector(String[] array, int query_type) throws WTException {
      if (array.length < query_type) {
        throw new WTException();
      }
      String s = array[query_type];
      Vector vector = new Vector(20);
      StringTokenizer st = new StringTokenizer(s);
      while (st.hasMoreTokens()) {
        vector.addElement(st.nextToken());
      }
      return vector;
   }

   public String getDisplayName( int query_type ) {
      String display_name  = "";
      String query_class   = queryClass[ query_type ];
      try {
         ClassInfo query_class_info = WTIntrospector.getClassInfo( query_class );
         if( query_class_info != null ) {
            display_name = query_class_info.getDisplayName( clientLocale );

         } else {
			   display_name = WTStringUtilities.toDisplayName( WTStringUtilities.tail( query_class, '.' ) );
		   }
		} catch( WTException wte ) {
	      display_name = WTStringUtilities.toDisplayName( WTStringUtilities.tail( query_class, '.' ) );
	   }

      return( display_name );
   }

   public String getDisplayAttribute( int query_type ) {
      return( displayAttributes[ query_type ] );
   }
}
