package wt.query.htmlsearch;

import wt.util.resource.*;

@RBUUID("wt.query.htmlsearch.htmlsearchResource")
public final class htmlsearchResource extends WTListResourceBundle {
   /**
/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
    * htmlsearchResource message resource bundle [English/US]
    **/
   @RBEntry("Find Users")
   @RBComment("HTML Search Find Users screen title")
   public static final String FIND_USERS_TITLE = "1";

   @RBEntry("OK")
   @RBComment("HTML Search Find Users Ok button")
   public static final String OK_BUTTON = "2";

   @RBEntry("Results")
   @RBComment("HTML Search Find Users results section title")
   public static final String FIND_USERS_RESULTS = "3";

   @RBEntry("Error generating the Find Users function.")
   public static final String FIND_USER_ERROR = "4";

   @RBEntry("Find Users:")
   @RBComment("HTML Search Find Users text string label")
   public static final String FIND_USERS_LABEL = "5";

   @RBEntry("Search")
   @RBComment("HTML Search Find Users search button label")
   public static final String FIND_USERS_SEARCH = "6";

   @RBEntry("Browse...")
   @RBComment("HTML Search Find Users Find button")
   public static final String FIND_USERS_BUTTON = "7";

   @RBEntry("List of users will appear here")
   @RBComment("HTML Search Find Users users not queried yet")
   public static final String FIND_USERS_EMPTY = "8";

   @RBEntry("No Users Found.")
   @RBComment("HTML Search Find Users message stating no users are found")
   public static final String NO_USERS_FOUND = "9";
}
