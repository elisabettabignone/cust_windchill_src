package wt.query.specification;

import wt.util.resource.*;

@RBUUID("wt.query.specification.specificationResource")
public final class specificationResource extends WTListResourceBundle {
   /**
/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
    **/
   @RBEntry("The operator is not supported for the specified Instance Type.")
   public static final String INVALID_TYPE_OPERATOR_PAIR = "0";
}
