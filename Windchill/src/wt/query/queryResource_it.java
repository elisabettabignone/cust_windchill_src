package wt.query;

import wt.util.resource.*;

@RBUUID("wt.query.queryResource")
public final class queryResource_it extends WTListResourceBundle {
   /**
/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
    * queryResource message resource bundle [English/US]
    **/
   @RBEntry("L'interrogazione specificata è errata. Messaggio di sistema:")
   public static final String QUERY_ERROR = "0";

   @RBEntry("Selezionare l'errore nella sintassi dell'istruzione: \"{0}\"")
   @RBArgComment0(" {0} is the specific syntax error:")
   public static final String SELECT_SYNTAX_ERROR = "1";

   @RBEntry("L'attributo \"{0}\" non è un membro della classe \"{1}\"")
   @RBArgComment0(" {0} is the attribute name")
   @RBArgComment1(" {1} is the class name:")
   public static final String ATTR_NOT_IN_CLASS = "2";

   @RBEntry("L'attributo \"{0}\" della classe \"{1}\" non è persistente")
   @RBArgComment0(" {0} is the attribute name")
   @RBArgComment1(" {1} is the class name:")
   public static final String ATTR_NOT_PERSISTABLE = "3";

   @RBEntry("L'attributo \"{0}\" della classe \"{1}\" non è di tipo \"{2}\"")
   @RBArgComment0(" {0} is the attribute name")
   @RBArgComment1(" {1} is the class name")
   @RBArgComment2(" {2} is the value type of the Search Condition:")
   public static final String TYPE_MISMATCH = "4";

   @RBEntry("Il valore relativo alla condizione di ricerca non è persistente. ObjectIdentifier è nullo.")
   public static final String NULL_OID = "5";

   @RBEntry("La classe della condizione di ricerca \"{0}\" non è una sottoclasse della classe di destinazione \"{1}\" o non è equivalente a essa")
   @RBArgComment0(" {0} is the name of the SearchCondition class")
   @RBArgComment1(" {1} is the name of the QuerySpec target class:")
   public static final String TARGET_CLASS_MISMATCH = "6";

   @RBEntry("La classe della condizione di ricerca \"{0}\" non è una sottoclasse della classe di destinazione \"{1}\" o della classe di link \"{2}\", né è equivalente a esse.")
   @RBArgComment0(" {0} is the name of the SearchCondition class")
   @RBArgComment1(" {1} is the name of the QuerySpec target class")
   @RBArgComment2(" {2} is the name of the QuerySpec link class:")
   public static final String TARGET_OR_LINK_MISMATCH = "7";

   @RBEntry("I valori relativi alle date della condizione di ricerca sono nulli.")
   public static final String NULL_TIMESTAMP = "8";

   @RBEntry("L'insieme di valori per la condizione di ricerca è vuoto.")
   public static final String EMPTY_VALUE_ARRAY = "9";

   @RBEntry("Errore durante la conversione del valore per la ricerca. \"{0}\" non è un \"{1}\" valido.")
   @RBArgComment0("  {0} is the given value")
   @RBArgComment1(" {1} is the type it fails to match:")
   public static final String VALUE_MISMATCH = "10";

   @RBEntry("La posizione della classe relativa alla condizione di ricerca, \"{0}\", è maggiore del valore massimo QuerySpec \"{1}\".")
   @RBArgComment0("  {0} is the given value,")
   @RBArgComment1(" {1} is the type it fails to match:")
   public static final String SC_OUT_OF_RANGE = "11";

   @RBEntry("Tipo Java errato per AttributeRange, \"{0}\".")
   @RBArgComment0("  {0} is a java type that is not one of the legal types for AttributeRange:")
   public static final String ILLEGAL_RANGE_JAVA_TYPE = "12";

   @RBEntry("Errore durante la conversione dei valori per la ricerca. \"{0}\" e/o \"{1}\" non sono un \"{2}\" valido.")
   @RBArgComment0("  {0} is the start value of a range")
   @RBArgComment1(" {1} is the end value and {2} is the type that one or both fail to match.")
   public static final String RANGE_VALUES_INVALID = "13";

   @RBEntry("Schedario \"{0}\" non valido.")
   @RBArgComment0("  {0} is the cabinet name.")
   public static final String INVALID_CABINET = "14";

   @RBEntry("Il tipo di ricerca \"{0}\" non è supportato sul tipo di campo \"{1}\".")
   public static final String UNSUPPORTED_SEARCH_TYPE = "15";

   @RBEntry("Il tipo di funzione SQL \"{0}\" con formato \"{1}\" non è supportato.")
   public static final String UNSUPPORTED_SQL_FUNCTION = "16";

   @RBEntry("Il tipo di funzione SQL \"{0}\" con attributo \"{1}\" di tipo \"{2}\" non è supportato.")
   @RBArgComment0("  {0} is the name of the function")
   @RBArgComment1(" {1} is the attribute specified, {2} is the java type of the attribute")
   public static final String UNSUPPORTED_ATTR_TYPE = "17";

   @RBEntry("La classe SQLFunction richiede una funzione.  Il valore nullo non è valido.")
   @RBComment(" Null function is not allowed message.")
   public static final String NULL_SQL_FUNCTION = "18";

   @RBEntry("Il tipo di funzione SQL \"{0}\" richiede un formato. Il valore nullo non è valido.")
   @RBComment(" Null format is not allowed message.")
   @RBArgComment0(" {0} is the name of the function.")
   public static final String NULL_SQL_FORMAT = "19";

   /**
    * HTML Local Search Text for class picker.  No underbars needed for values!!
    *
    **/
   @RBEntry("Tutto")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String ALL = "20";

   @RBEntry("Parte")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WTPART = "21";

   @RBEntry("Documento")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WTDOCUMENT = "22";

   @RBEntry("Richiesta di modifica")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WTCHANGEREQUEST = "23";

   @RBEntry("Operazione di modifica")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WTCHANGEACTIVITY = "24";

   @RBEntry("Ordine di modifica")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WTCHANGEORDER = "25";

   @RBEntry("Unità di replica")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WTUNIT = "65";

   @RBEntry("Problema durante la configurazione della ricerca in {0}.")
   @RBArgComment0(" {0} is the array name or attribute name for the data that configures the columns, criteria, pick list for the HTML local search.")
   public static final String BAD_CONFIGURATION = "26";

   @RBEntry("Risultati della ricerca per {0}")
   public static final String SEARCH_SUBTITLE = "27";

   @RBEntry("Errore durante la formattazione della schermata di ricerca.")
   public static final String SEARCH_FORMAT = "28";

   @RBEntry("Chiave di ricerca:")
   public static final String SEARCH_PICKER = "29";

   @RBEntry("La ricerca ha restituito {0} oggetti. Impossibile formattare e visualizzare gli oggetti di classe {1}.")
   @RBArgComment0(" {0} is the count of objects found in search")
   @RBArgComment1(" {1} is the class of the object that couldn't be formated.")
   public static final String SEARCH_RESULT = "30";

   @RBEntry("La ricerca \"{0}\" ha restituito {1} oggetti.")
   @RBArgComment0(" {0} the search criteria")
   @RBArgComment1(" {1} is the count of objects found in search.")
   public static final String SEARCH_SUCCESS = "31";

   @RBEntry("Errore durante l'impostazione o l'esecuzione della ricerca.")
   public static final String SEARCH_ERROR = "32";

   @RBEntry("Cerca")
   public static final String DO_SEARCH = "33";

   @RBEntry("Inserire uno o più criteri di ricerca e fare clic su \"Cerca\" per visualizzare i risultati della ricerca.")
   public static final String DO_SEARCH_TEXT = "34";

   @RBEntry("Errore durante la conversione del valore. \"{0}\" non è una data valida.")
   @RBArgComment0(" {0} is the given value")
   @RBArgComment1(" {1} is the type it fails to match:")
   public static final String INVALID_DATE = "35";

   @RBEntry("Master parte proxy")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String PROXYPARTMASTER = "36";

   @RBEntry("Master documento proxy")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String PROXYDOCUMENTMASTER = "37";

   @RBEntry("La classe Attributo di classe \"{0}\" non è una superclasse e non equivale ad alcuna delle classi della tabella \"{1}\"")
   public static final String CLASS_ATTRIBUTE_MISMATCH = "38";

   @RBEntry("Sia il valore iniziale che quello finale dell'intervallo sono nulli.")
   public static final String NULL_RANGE = "39";

   @RBEntry("Suggerimento di modifica")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WTCHANGEISSUE = "40";

   @RBEntry("L'indice specificato non fa riferimento a un'espressione valida nella proposizione SQL FROM.")
   public static final String INVALID_FROM_INDEX = "41";

   @RBEntry("Processo")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WFPROCESS = "42";

   @RBEntry("Investigazione di modifica")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WTCHANGEINVESTIGATION = "43";

   @RBEntry("Analisi")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WTANALYSISACTIVITY = "44";

   @RBEntry("Proposta di modifica")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WTCHANGEPROPOSAL = "45";

   @RBEntry("Vecchia richiesta di modifica")
   public static final String OLDWTCHANGEREQUEST = "46";

   @RBEntry("Vecchio ordine di modifica")
   public static final String OLDWTCHANGEORDER = "47";

   @RBEntry("Vecchia operazione di modifica")
   public static final String OLDWTCHANGEACTIVITY = "48";

   @RBEntry("Documento CAD")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String EPMDOCUMENT = "49";

   @RBEntry("Entità aziendale")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String BUSINESSENTITY = "53";

   @RBEntry("Configuration item")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String CONFIGURATIONITEM = "54";

   @RBEntry("Istanza di prodotto")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WTPRODUCTINSTANCE2 = "55";

   @RBEntry("Visualizza i {0} risultati.")
   @RBArgComment0("  {0} is the total number of results.")
   public static final String VIEW_ALL_RESULTS = "50";

   @RBEntry("Sono visualizzati i primi {0} risultati.")
   @RBArgComment0("  {0} is the number of results displayed.")
   public static final String NUMBER_SHOWN_MESSAGE = "51";

   @RBEntry("(1 - {0} di {1})")
   @RBArgComment0("  {0} is the number of results displayed on the current page")
   @RBArgComment1(" {1} is the total number of results found.")
   public static final String NUMBER_SHOWN = "52";

   @RBEntry("Operazione di fabbricazione")
   public static final String PMOPERATION = "56";

   @RBEntry("Caratteristica di fabbricazione")
   public static final String PMMFGFEATURE = "58";

   @RBEntry("Assieme di fabbricazione")
   public static final String PMASSEMBLY = "59";

   @RBEntry("Studio di fabbricazione")
   public static final String PMSTUDY = "60";

   @RBEntry("Documento master")
   public static final String WTDOCUMENTMASTER = "61";

   @RBEntry("Risorse di fabbricazione")
   public static final String PMRESOURCES = "62";

   @RBEntry("Parte master")
   public static final String WTPARTMASTER = "63";

   @RBEntry("Modello di report")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String REPORTTEMPLATE = "64";

   @RBEntry("Errore durante la chiusura della sessione di impaginazione dell'ultima ricerca.")
   public static final String CLOSE_PAGING_ERROR = "66";

   @RBEntry("Errore durante il recupero della pagina successiva di risultati dal database.")
   public static final String FETCH_NEXT_PAGE_ERROR = "67";

   @RBEntry("Errore durante l'esecuzione dell'interrogazione del database per questa ricerca.")
   public static final String DATABASE_QUERY_ERROR = "68";

   @RBEntry("Errore durante la conversione dei criteri in un'interrogazione del database per questa ricerca.")
   public static final String FORMING_CRITERIA_ERROR = "69";

   @RBEntry("L'oggetto selezionato per la ricerca non è valido: {0}.")
   @RBArgComment0("Class name that could not be understood for the search.")
   public static final String INVALID_SEARCH_CLASS = "70";

   @RBEntry("Errore durante l'utilizzo dei risultati della ricerca dei dati per accedere agli oggetti del database.")
   public static final String OID_CRITERIA = "71";

   @RBEntry("Errore durante la creazione della specifca di configurazione per filtrare l'attuale ricerca.")
   public static final String CONFIG_SPEC = "72";

   @RBEntry("L'oggetto restituito dalla ricerca non è riconoscibile dal sistema: {0}.")
   @RBArgComment0("Full classname from content search engine.")
   public static final String CONTENT_CLASS_ERROR = "73";

   @RBEntry("Errore durante l'aggiunta delle informazioni seguenti ai criteri di ricerca: nome = {0}, valore = {1}.")
   @RBArgComment0("Attribute name")
   @RBArgComment1("Attribute value")
   public static final String ERROR_NAME_VALUE = "74";

   @RBEntry("XXX")
   @RBPseudo(false)
   public static final String XXX_75 = "75";

   @RBEntry("XXX")
   @RBPseudo(false)
   public static final String XXX_76 = "76";

   @RBEntry("XXX")
   @RBPseudo(false)
   public static final String XXX_77 = "77";

   @RBEntry("XXX")
   @RBPseudo(false)
   public static final String XXX_78 = "78";

   @RBEntry("XXX")
   @RBPseudo(false)
   public static final String XXX_79 = "79";

   @RBEntry("XXX")
   @RBPseudo(false)
   public static final String XXX_80 = "80";

   @RBEntry("XXX")
   @RBPseudo(false)
   public static final String XXX_81 = "81";

   @RBEntry("XXX")
   @RBPseudo(false)
   public static final String XXX_82 = "82";

   @RBEntry("XXX")
   @RBPseudo(false)
   public static final String XXX_83 = "83";

   @RBEntry("XXX")
   @RBPseudo(false)
   public static final String XXX_84 = "84";

   @RBEntry("Errore: specificare almeno un tipo di oggetto per la ricerca.")
   public static final String NO_OBJECTS_FOR_SEARCH = "85";

   @RBEntry("Errore durante l'inizializzazione della specifica d'interrogazione per: {0}.")
   @RBArgComment0("Class name of object that is having a problem.")
   public static final String INIT_QUERY_SPEC = "86";

   @RBEntry("XXX")
   @RBPseudo(false)
   public static final String XXX_87 = "87";

   @RBEntry("XXX")
   @RBPseudo(false)
   public static final String XXX_88 = "88";

   @RBEntry("XXX")
   @RBPseudo(false)
   public static final String XXX_89 = "89";

   @RBEntry("XXX")
   @RBPseudo(false)
   public static final String XXX_90 = "90";

   @RBEntry("Impossibile gestire più From_Indices in un ItemBuilder")
   public static final String MULTIPLE_FROM_INDICES = "91";

   @RBEntry("Il numero di classi indicate nella parte FROM (DA)  nell'interrogazione ({0}) supera la misura massima per le colonne della tabella dei risultati ({1}).")
   @RBArgComment0(" {0} is the number of classes in the FROM portion of the query")
   @RBArgComment1(" {1} is the number of columns in the page results table")
   public static final String OUT_OF_CLASS_COUNT_RANGE = "92";

   @RBEntry("Istruzione di impaginazione non valida. La funzione è già stata impostata.")
   public static final String DISTINCT_NOT_ALLOWED_IN_PAGING = "94";

   @RBEntry("Istruzione non valida.")
   public static final String INVALID_STATEMENTSPEC = "95";

   @RBEntry("Impossibile gestire componenti non-QuerySpec in CompoundQuerySpec o CompositeQuerySpec.")
   public static final String NESTED_NON_QUERYSPEC_NOT_SUPPORTED = "96";

   @RBEntry("CompoundQuerySpec non valido perchè un componente è nullo o perchè contiene solo un componente.")
   public static final String INVALID_COMPOUND_QUERYSPEC = "97";

   @RBEntry("CompoundQuerySpec non valido perchè un componente è nullo o perchè contiene solo un componente.")
   public static final String INVALID_COMPOSITE_QUERYSPEC = "98";

   @RBEntry("Prodotto")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WTPRODUCT = "99";

   @RBEntry("Valore costante nullo.")
   public static final String NULL_CONSTANT_VALUE = "100";

   @RBEntry("I caratteri jolly come * non possono essere usati nel campo seguente: {0}")
   @RBArgComment0("Localized fieldname for field that does not allow wild cards.")
   public static final String WILD_CARD_NOT_SUPPORTED = "101";

   @RBEntry("Componente")
   @RBComment("RSDB searchable objects  DO NOT MODIFY THE INDEX NUMBERS, it will break HTML Search.")
   public static final String RSCOMP = "102";

   @RBEntry("Connessione")
   @RBComment("RSDB searchable objects  DO NOT MODIFY THE INDEX NUMBERS, it will break HTML Search.")
   public static final String RSCONN = "103";

   @RBEntry("Bobina")
   @RBComment("RSDB searchable objects  DO NOT MODIFY THE INDEX NUMBERS, it will break HTML Search.")
   public static final String RSSPOOL = "104";

   @RBEntry("Raccordo")
   @RBComment("RSDB searchable objects  DO NOT MODIFY THE INDEX NUMBERS, it will break HTML Search.")
   public static final String RSFITTING = "105";

   @RBEntry("Caratteristica di fabbricazione")
   @RBComment("RSDB searchable objects  DO NOT MODIFY THE INDEX NUMBERS, it will break HTML Search.")
   public static final String RSMANUF = "106";

   @RBEntry("Modello di fabbricazione")
   @RBComment("eNC searchable object  DO NOT MODIFY THE INDEX NUMBERS, it will break HTML Search.")
   public static final String WTMFGMODEL = "107";

   @RBEntry("Espressione array senza valore")
   public static final String NO_ARRAY_VALUES = "108";

   @RBEntry("La classe \"{0}\" e le relative sottoclassi non sono classi Persistable concrete.")
   @RBArgComment0(" {0} is the name of the class:")
   public static final String CLASS_NOT_PERSISTABLE_CONCRETE = "109";

   @RBEntry("Parte con numero di serie")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WTSERIALNUMBEREDPART = "110";

   @RBEntry("Configurazione di prodotto")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WTPRODUCTCONFIGURATION = "111";

   @RBEntry(":")
   @RBPseudo(false)
   @RBComment("Colon that is added to attribute names in search criteria.")
   public static final String ATTRIBUTE_COLON = "112";

   @RBEntry("La parola chiave SQL e il tipo specificati non sono compatibili: nome={0} tipo={1} tipo specificato={2}.")
   public static final String INVALID_KEYWORD_TYPE = "113";

   @RBEntry("Baseline")
   @RBComment("HTML Integrated Search drop down menu item.  Value should not contain underbars.")
   public static final String WTMANAGEDBASELINE = "114";

   @RBEntry("Il motore di ricerca non è configurato correttamente o non è in esecuzione.")
   @RBComment("Error message to display when the Search Engine (Rware) is not configured with Windchill")
   public static final String SEARCH_ENGINE_NOT_CONFIGURED = "115";

   @RBEntry("La stringa \"{0}\" non è un suggerimento valido.")
   @RBComment("{0} is the string representation of the hint for a SQL statement")
   public static final String INVALID_HINT = "116";

   @RBEntry("La stringa \"{0}\" non è una costante valida.")
   @RBComment("{0} is the string representation of the constant for a SQL statement")
   public static final String INVALID_CONSTANT = "117";

   @RBEntry("Il recupero completamente persistente non è valido se si usa ClassViewExpression.")
   @RBComment("Full persistable retrieval is not valid using ClassViewExpression")
   public static final String INVALID_FULL_PERSISTABLE_RETRIEVAL = "118";

   @RBEntry("La colonna Alias \"{0}\" non è valida. La colonna alias deve essere un identificatore valido dell'archivio dati. Non è consentito l'uso delle parole riservate dell'archivio dati.")
   @RBComment("{0} is the string representation of the alias for a SQL Column.")
   public static final String INVALID_ALIAS = "119";

   @RBEntry("Data")
   @RBComment("In previous version of Windchill the actual \"object\" name was displayed.  Since we are receiving field problems in other languages of non-translated strings this would should be translated.  It appears within quotes within INVALID_DATE above (the second entry)")
   public static final String DATE = "120";

   @RBEntry("L'opzione della condizione di ricerca \"{0}\" non è valida. L'opzione può essere usata solo per specificare un carattere di escape.")
   @RBComment("{0} is the string representation of the search condition option.")
   public static final String INVALID_OPTION = "121";

   @RBEntry("Impossibile eseguire l'interrogazione in modalità d'impaginazione.")
   public static final String INVALID_PAGING_QUERY = "122";

   @RBEntry("OK")
   @RBComment("HTML Search Find Users Ok button")
   public static final String OK_BUTTON = "123";

   @RBEntry("Sfoglia...")
   @RBComment("HTML Search Find Users Find button")
   public static final String FIND_USERS_BUTTON = "124";

   @RBEntry("Errore durante la generazione della funzione di ricerca utenti.")
   public static final String FIND_USER_ERROR = "125";

   @RBEntry("Cerca")
   @RBComment("HTML Search Criteria do search button.")
   public static final String SEARCH_BUTTON = "126";

   @RBEntry("La stringa \"{0}\" non è un operatore valido.")
   @RBComment("{0} is the string representation of the operator for a search condition operator")
   public static final String INVALID_OPERATOR = "127";

   @RBEntry("L'elenco delle espressioni colonna non contiene espressioni, pertanto non è valido.")
   public static final String EMPTY_COLUMN_LIST = "128";

   @RBEntry("Lunghezza array \"{0}\" non valida.")
   @RBComment("{0} is the length of the array")
   public static final String INVALID_ARRAY_LENGTH = "129";

   @RBEntry("Nome attributo non valido ")
   public static final String INVALID_ATTRIBUTE_NAME_MESSAGE = "331";

   @RBEntry("Tipo criterio di ricerca non valido ")
   public static final String UNKNOWN_SEARCH_TYPE = "333";

   @RBEntry("\"{0}\" non è una funzione SQL valida.")
   @RBComment("{0} is the SQL function name")
   public static final String INVALID_SQL_FUNCTION = "400";

   @RBEntry("\"{0}\" non è un nome di tabella valido in quanto non è un identificatore di archivio dati valido")
   @RBComment("{0} is the string representation of the SQL Table name.")
   public static final String INVALID_TABLE_NAME = "500";

   @RBEntry("\"{0}\" non è un nome di colonna valido in quanto non è un identificatore di archivio dati valido")
   @RBComment("{0} is the string representation of the SQL Column name.")
   public static final String INVALID_COLUMN_NAME = "501";

   @RBEntry("\"{0}\" attributi selezionati nella sottoselezione. Ne può essere selezionato solo uno.")
   @RBComment("{0} is the number of select attributes in the subselect.")
   public static final String INVALID_NUMBER_SELECT_ATTRIBUTE_IN_THE_SUBSELECT = "502";
}
