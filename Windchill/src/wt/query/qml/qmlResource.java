package wt.query.qml;

import wt.util.resource.*;

@RBUUID("wt.query.qml.qmlResource")
public final class qmlResource extends WTListResourceBundle {
   /**
/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
    **/
   @RBEntry("Duplicate alias \"{0}\" cannot be used.")
   public static final String DUPLICATE_ALIAS = "0";

   @RBEntry("Alias attribute is required for node \"{0}\".")
   public static final String ALIAS_REQUIRED = "1";

   @RBEntry("Invalid alias \"{0}\" cannot be used.")
   public static final String INVALID_ALIAS = "2";

   @RBEntry("Invalid macro specified \"{0}\".")
   public static final String INVALID_MACRO_NAME = "3";

   @RBEntry("Invalid QML specified.")
   public static final String INVALID_QML = "4";

   @RBEntry("Invalid outer joins specified for the join between \"{0}\" and \"{1}\".")
   public static final String INVALID_LINK_OUTER_JOIN = "5";

   @RBEntry("Invalid correlated link join: link={0}.")
   public static final String INVALID_CORRELATED_LINK_JOIN = "6";

   @RBEntry("Invalid correlated reference join: class={0} reference={1}.")
   public static final String INVALID_CORRELATED_REFERENCE_JOIN = "7";

   @RBEntry("Invalid Order By specified for compound query.")
   public static final String INVALID_COMPOUND_ORDER_BY = "8";

   @RBEntry("No value specified for parameter \"{0}\".")
   public static final String NO_PARAMETER_VALUE = "9";

   @RBEntry("Class name \"{0}\" not in list of allowed classes")
   public static final String INVALID_CLASS = "100";

   @RBEntry("Method \"{0}\" not in list of allowed methods for class \"{1}\"")
   public static final String INVALID_METHOD = "101";
   
   @RBEntry("Report Template \"{0}\" in context \"{1}\" is used as a view, but it does not exist.")
   public static final String REPORT_TEMPLATE_VIEW_DOES_NOT_EXIST = "200";
}
