package wt.query.qml;

import wt.util.resource.*;

@RBUUID("wt.query.qml.qmlResource")
public final class qmlResource_it extends WTListResourceBundle {
   /**
/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
    **/
   @RBEntry("Non è consentito l'uso dell'alias doppio \"{0}\".")
   public static final String DUPLICATE_ALIAS = "0";

   @RBEntry("L'alias dell'attributo è necessario per il nodo \"{0}\".")
   public static final String ALIAS_REQUIRED = "1";

   @RBEntry("L'alias \"{0}\" non è valido e non può essere usato.")
   public static final String INVALID_ALIAS = "2";

   @RBEntry("La macro specificata \"{0}\" non è valida.")
   public static final String INVALID_MACRO_NAME = "3";

   @RBEntry("Il QML specificato non è valido.")
   public static final String INVALID_QML = "4";

   @RBEntry("Gli outer join specificati sono invalidi per il join tra \"{0}\" e \"{1}\".")
   public static final String INVALID_LINK_OUTER_JOIN = "5";

   @RBEntry("Join link correlato non valido: link={0}.")
   public static final String INVALID_CORRELATED_LINK_JOIN = "6";

   @RBEntry("Join riferimento correlato non valido: classe={0} riferimento={1}.")
   public static final String INVALID_CORRELATED_REFERENCE_JOIN = "7";

   @RBEntry("Nell'interrogazione composta il valore specificato per Ordina per non è valido.")
   public static final String INVALID_COMPOUND_ORDER_BY = "8";

   @RBEntry("È stato specificato un valore non corretto per il parametro \"{0}\".")
   public static final String NO_PARAMETER_VALUE = "9";

   @RBEntry("Il nome della classe \"{0}\" non è incluso nell'elenco delle classi consentite")
   public static final String INVALID_CLASS = "100";

   @RBEntry("Il metodo \"{0}\" non è incluso nell'elenco dei metodi consentiti per la classe \"{1}\"")
   public static final String INVALID_METHOD = "101";
   
   @RBEntry("Il modello di report \"{0}\" nel contesto \"{1}\" è utilizzato come vista, ma non esiste.")
   public static final String REPORT_TEMPLATE_VIEW_DOES_NOT_EXIST = "200";
}
