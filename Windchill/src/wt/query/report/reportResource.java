package wt.query.report;

import wt.util.resource.*;

@RBUUID("wt.query.report.reportResource")
public final class reportResource extends WTListResourceBundle {
   /**
/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
    **/
   @RBEntry("Invalid indexes to access result set: row = {0}, column = {1}")
   public static final String INVALID_RESULT_SET_ACCESS = "0";

   @RBEntry("Invalid column index \"{0}\"")
   public static final String INVALID_COLUMN_ACCESS = "1";

   @RBEntry("Invalid argument \"{0}\"")
   public static final String INVALID_ARG = "2";

   @RBEntry("Mismatch between object and expected class: object = {0}, expected = {1}")
   public static final String OBJECT_CLASS_MISMATCH = "3";

   @RBEntry("Error deserializing query resource \"{0}\"")
   public static final String QUERY_DESERIALIZATION_ERROR = "4";

   @RBEntry("Report query did not return any results.")
   public static final String NO_RESULTS = "5";

   @RBEntry("!! Error accessing value: error={0}.")
   public static final String ERROR_ACCESSING_VALUE = "6";

   @RBEntry("Script method does not exist: class={0}, method={1}, argumentCount={2}")
   public static final String NO_SCRIPT_METHOD = "100";

   @RBEntry("Script method not authorized: class={0}")
   public static final String UNAUTHORIZED_SCRIPT_METHOD_CLASS = "101";

   @RBEntry("Script method not authorized: class={0}, method={1}")
   public static final String UNAUTHORIZED_SCRIPT_METHOD_NAME = "102";

   @RBEntry("Yes")
   public static final String BOOLEAN_TRUE = "201";

   @RBEntry("No")
   public static final String BOOLEAN_FALSE = "202";
}
