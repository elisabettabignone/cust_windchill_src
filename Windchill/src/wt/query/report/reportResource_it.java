package wt.query.report;

import wt.util.resource.*;

@RBUUID("wt.query.report.reportResource")
public final class reportResource_it extends WTListResourceBundle {
   /**
/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
    **/
   @RBEntry("Indici non validi per accedere ai risultati: riga = {0}, colonna = {1}")
   public static final String INVALID_RESULT_SET_ACCESS = "0";

   @RBEntry("Indice di colonna non valido \"{0}\"")
   public static final String INVALID_COLUMN_ACCESS = "1";

   @RBEntry("Argomento non valido \"{0}\"")
   public static final String INVALID_ARG = "2";

   @RBEntry("Mancata corrispondenza tra l'oggetto e la classe corretta: oggetto = {0}, classe corretta = {1}")
   public static final String OBJECT_CLASS_MISMATCH = "3";

   @RBEntry("Errore durante la deserializzazione delle risorse dell'interrogazione \"{0}\"")
   public static final String QUERY_DESERIALIZATION_ERROR = "4";

   @RBEntry("L'interrogazione del report non ha restituito alcun risultato.")
   public static final String NO_RESULTS = "5";

   @RBEntry("Errore durante l'accesso al valore: errore={0}.")
   public static final String ERROR_ACCESSING_VALUE = "6";

   @RBEntry("Script method inesistente: classe={0}, method={1}, numero argomenti={2}")
   public static final String NO_SCRIPT_METHOD = "100";

   @RBEntry("Script method non autorizzato: classe={0}")
   public static final String UNAUTHORIZED_SCRIPT_METHOD_CLASS = "101";

   @RBEntry("Script method non autorizzato: classe={0}, method={1}")
   public static final String UNAUTHORIZED_SCRIPT_METHOD_NAME = "102";

   @RBEntry("Sì")
   public static final String BOOLEAN_TRUE = "201";

   @RBEntry("No")
   public static final String BOOLEAN_FALSE = "202";
}
