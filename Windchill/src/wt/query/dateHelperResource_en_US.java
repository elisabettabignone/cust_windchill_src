/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */

package wt.query;

import java.util.ListResourceBundle;

/**
 * dateHelperResource message resource bundle [English/US]
 *
 **/

 /*  LOCALIZATION INSTRUCTIONS:  Do not translate any strings in this file.  Only
 *   add desired date formats to this file.
 *   What the symbols mean:
 *   yyyy - 4 digit year (dates must have 4 digit years)
 *   M    - month
 *   d    - day
 *   HH   - Hour
 *   mm   - minute
 *   ss   - Seconds
 *    S   - Milleseconds
 */

public class dateHelperResource_en_US extends ListResourceBundle {

    private static final String MILLISECOND = "millisecond";
    private static final String SECOND = "second";
    private static final String MINUTE = "minute";
    private static final String HOUR = "hour";
    private static final String TIME = "time";
    private static final String DAY = "day";
    private static final String DAY1 = "day1";
    private static final String MONTH = "month";
    private static final String YEAR = "year";
    private static final String DATE_INPUT_FORMAT = "dateInputFormat";
    private static final String DATE_OUTPUT_FORMAT = "dateOutputFormat";

    public Object[][] getContents() {
        return contents;
    }
    static final Object[][] contents = {
/*<NEW>*/        {MILLISECOND, new String[]{"M/d/yyyy HH:mm:ss.S", "yyyy/M/d HH:mm:ss.S",
                                    "M-d-yyyy HH:mm:ss.S", "yyyy-M-d HH:mm:ss.S",
                                    "M.d.yyyy HH:mm:ss.S", "yyyy.M.d HH:mm:ss.S",
                                    "M,d,yyyy HH:mm:ss.S", "yyyy,M,d HH:mm:ss.S",
                                    "M d yyyy HH:mm:ss.S", "yyyy M d HH:mm:ss.S"}},
/*<NEW>*/        {SECOND, new String[]{"M/d/yyyy HH:mm:ss", "yyyy/M/d HH:mm:ss",
                                "M-d-yyyy HH:mm:ss", "yyyy-M-d HH:mm:ss",
                                "M.d.yyyy HH:mm:ss", "yyyy.M.d HH:mm:ss",
                                "M,d,yyyy HH:mm:ss", "yyyy,M,d HH:mm:ss",
                                "M d yyyy HH:mm:ss", "yyyy M d HH:mm:ss"}},
/*<NEW>*/        {MINUTE, new String[]{"M/d/yyyy HH:mm", "yyyy/M/d HH:mm a",
                                "M-d-yyyy HH:mm", "yyyy-M-d HH:mm a",
                                "M.d.yyyy HH:mm", "yyyy.M.d HH:mm a",
                                "M,d,yyyy HH:mm", "yyyy,M,d HH:mm a",
                                "M d yyyy HH:mm", "yyyy M d HH:mm a"}},
/*<NEW>*/        {HOUR, new String[]{"M/d/yyyy HH", "yyyy/M/d HH",
                            "M-d-yyyy HH", "yyyy-M-d HH",
                            "M.d.yyyy HH", "yyyy.M.d HH",
                            "M,d,yyyy HH", "yyyy,M,d HH",
                            "M d yyyy HH", "yyyy M d HH"}},
/*<NEW>*/        {TIME, new String[]{"M/d/yyyy HH:mm:ss", "yyyy/M/d HH:mm:ss",
                            "M-d-yyyy HH:mm:ss", "yyyy-M-d HH:mm:ss",
                            "M.d.yyyy HH:mm:ss", "yyyy.M.d HH:mm:ss",
                            "M,d,yyyy HH:mm:ss", "yyyy,M,d HH:mm:ss",
                            "M d yyyy HH:mm:ss", "yyyy M d HH:mm:ss",
                            "M/d/yyyy HH:mm:ss.S", "yyyy/M/d HH:mm:ss.S",
                            "M-d-yyyy HH:mm:ss.S", "yyyy-M-d HH:mm:ss.S",
                            "M.d.yyyy HH:mm:ss.S", "yyyy.M.d HH:mm:ss.S",
                            "M,d,yyyy HH:mm:ss.S", "yyyy,M,d HH:mm:ss.S",
                            "M d yyyy HH:mm:ss.S", "yyyy M d HH:mm:ss.S"}},
/*<NEW>*/        {DAY, new String[]{"M/d/yyyy", "yyyy/M/d",
                            "M-d-yyyy", "yyyy-M-d",
                            "M.d.yyyy", "yyyy.M.d",
                            "M,d,yyyy", "yyyy,M,d",
                            "M d yyyy", "yyyy M d"}},
                 {DAY1, new String[]{"yyyy/MM/dd",
                                     "yyyy-MM-dd",
                                     "yyyy.MM.dd",
                                     "yyyy,MM,dd",
                                     "yyyy MM dd"}},
/*<NEW>*/        {MONTH, new String[]{"M/yyyy", "yyyy/M",
                            "M-yyyy", "yyyy-M",
                            "M.yyyy", "yyyy.M",
                            "M,yyyy", "yyyy,M",
                            "M yyyy", "yyyy M"}},
/*<NEW>*/        {YEAR, new String[]{"yyyy"}},

        // The following string is to display next to date fields in our html forms where
        // the user needs to see the syntactically correct way to enter the date
        // Note: this will be different for each locale

        // The following string is to display next to date fields in our html forms where
        // the user needs to see the syntactically correct way to enter the date
        // Note: this will be different for each locale
        {DATE_INPUT_FORMAT,           "MM/DD/YYYY"},

        // The following singleton string array is used by Windchill to display localized
        // date values in a manner consistent with that specified by DATE_INPUT_FORMAT.
        //
        // DATE_OUTPUT_FORMAT must represent exactly the same date format as DATE_INPUT_FORMAT,
        // but may use only the case-sensitive English letters "y", "M", and "d" for year,
        // month, and day, respectively.
        //
        // For example, in French, DATE_INPUT_FORMAT is "JJ/MM/AAAA", corresponding to
        // day, month, year; so the value of the DATE_OUTPUT_FORMAT string should be "dd/MM/yyy".
        {DATE_OUTPUT_FORMAT, new String[] { "MM/dd/yyyy" } },
        
        // This is an output display format showing the full date with hours 
        // and minutes.
        {dateHelperResource.DATE_MINUTES_DISPLAY_FORMAT,           "yyyy-MM-dd HH:mm"},

    };
}
