/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.pds;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.pds.VerifyPrimaryKeyIndicesResource")
public final class VerifyPrimaryKeyIndicesResource extends WTListResourceBundle {
   @RBEntry("Primary Key Index Verification Failed :-")
   public static final String VERIFICATION_FAILED = "1";
   
   @RBEntry("Primary Key Index PK_{0} does not exist.")
   @RBArgComment0("tableName")
   public static final String MISSING_INDEX = "2";
   
   @RBEntry("Primary Key Constraint PK_{0} does not exist.")
   @RBArgComment0("tableName")
   public static final String MISSING_CONSTRAINT = "3";
   
   @RBEntry("For any missing primary key index or primary key constraints, follow the steps below for each table reported in the log:- \n 1) Examine existing constraints on the table and check if there exists any other constraint on the primary key column for that table. \n  2) If no constraint exists on the primary key column, create the primary key constraint. See example shown below. \n 3) If a constraint exists on the primary key column, drop the existing constraint and recreate it with the correct name as reported in the log message. \n For example, If a primary key constraint, PK_WTDocument, is missing on WTDocument, following statement could be used to create it:- \n ALTER TABLE WTDocument ADD CONSTRAINT PK_WTDocument PRIMARY KEY (idA2A2) USING INDEX; \n If Primary key constraint exists but Primary key index is missing, create the index as below:- \n CREATE UNIQUE INDEX PK_WTDocument ON WTDocument(idA2A2); \n To see the existing constraints on a table, run the following query:- \n SELECT CONSTRAINT_NAME, INDEX_NAME from USER_CONSTRAINTS where TABLE_NAME='WTDOCUMENT';")
   public static final String CORRECTION_STEPS = "4";   
   
   
   @RBEntry("For any missing primary key index on SQL Server Database, create primary key constraint on the table. \n For example, If a primary key index, PK_WTDocument, is missing on table WTDocument, following statement could be used to create the primary key constraint which in turn will create the primary key index as well:- \n ALTER TABLE [pdmpjlx20_73].[WTDocument] ADD  CONSTRAINT [PK_WTDocument] PRIMARY KEY CLUSTERED ([idA2A2] ASC)ON [PRIMARY] \n")
   public static final String CORRECTION_STEPS_SQL = "5"; 
   
   @RBEntry("***This does not apply to SqlServer***")
   public static final String SQL_SERVER = "6";
   
   @RBEntry("Invalid Index Name for Primary Key Constraint PK_{0}.")
   @RBArgComment0("tableName")
   public static final String INVALID_INDEX_NAME = "7";
   
   @RBEntry("For any invalid index name on a primary key constraint, follow the steps below for each table reported in the log:- \n 1) The index name and the constraint name should be identical and follow the naming convention PK_<TableName>; for example on table EPMDOCUMENT, the constraint and index name should each be 'PK_EPMDOCUMENT'. \n  2) Drop the constraint: \n ALTER TABLE EPMDOCUMENT DROP CONSTRAINT PK_EPMDOCUMENT; \n 3) Recreate the constraint and index: \n ALTER TABLE EPMDOCUMENT ADD CONSTRAINT PK_EPMDOCUMENT PRIMARY KEY (IDA2A2) USING INDEX PK_EPMDOCUMENT; \n To see the existing constraints on a table, run the following query:- \n SELECT CONSTRAINT_NAME, INDEX_NAME from USER_CONSTRAINTS where TABLE_NAME='EPMDOCUMENT ';")
   public static final String CORRECTION_STEPS_INVALID_INDEX_NAME = "8"; 
   
}
