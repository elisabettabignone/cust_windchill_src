package wt.pds;

import wt.util.resource.*;

@RBUUID("wt.pds.pdsResource")
public final class pdsResource extends WTListResourceBundle {
   /**
/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
    * pdsResource message resource bundle [English/US]
    * 
    * Usage notes:
    * Last USED ID: 9
    * DO NOT REUSE IDS.  If an message becomes obsolete, comment it out
    * but do not create a new message with that id.
    * @version 	1.0, 97 August
    * @author	Dave Hoplin
    **/
   @RBEntry("A persistence error occurred. System message follows:")
   public static final String GENERAL_ERROR = "0";

   @RBEntry("A SQL error has occured. Database system message follows:")
   public static final String GENERAL_SQL_ERROR = "1";

   @RBEntry("Class \"{0}\" is not a persistent class.")
   @RBArgComment0(" {0} is the name of the class that is not eligible for a persistence opertion:")
   public static final String CLASS_NOT_PERSISTENT = "2";

   @RBEntry("Object \"{0}\" is already persistent.")
   @RBArgComment0("  {0} is the id of the object that is already in the database, so can't be inserted:")
   public static final String ALREADY_PERSISTENT = "3";

   @RBEntry("Object \"{0}\" is not a persistent.")
   @RBArgComment0(" {0} is the id of the object that is not in the database, so can't be updated nor deleted:")
   public static final String NOT_PERSISTENT = "4";

   @RBEntry("Invalid association")
   public static final String INVALID_ASSOCIATION = "5";

   @RBEntry("Role \"{0}\" is not valid in this association.")
   @RBArgComment0(" {0} is the role name:")
   public static final String INVALID_ROLE_FOR_ASSOC = "6";

   @RBEntry("Class \"{0}\" is not valid in this association.")
   @RBArgComment0(" {0} is the class name:")
   public static final String INVALID_CLASS_FOR_ASSOC = "7";

   @RBEntry("Database operation interrupted")
   public static final String DATABASE_OP_INTERRUPT = "8";

   @RBEntry("Database operation \"{0}\" interrupted")
   public static final String DATABASE_FOR_OP_INTERRUPT = "9";

   @RBEntry("LOB write error - chunk size = {0}, actual written = {1}")
   @RBArgComment0(" a number")
   @RBArgComment1(" a number")
   public static final String LOB_WRITE_ERROR = "10";

   @RBEntry("Resetting to invalid mark")
   public static final String INVALID_MARK = "11";

   @RBEntry("The {0} object is already locked in the database.")
   @RBArgComment0(" {0} is the identifier of the object:")
   public static final String ROW_LOCKED = "12";

   @RBEntry("Class \"{0}\" and none of its subclasses are a concrete class.")
   @RBArgComment0(" {0} is the name of the class:")
   public static final String CLASS_NOT_CONCRETE = "13";

   @RBEntry("Attribute \"{0}\" is not a member of class \"{1}\"")
   @RBArgComment0(" {0} is the attribute name")
   @RBArgComment1(" {1} is the class name")
   public static final String ATTR_NOT_IN_CLASS = "14";

   @RBEntry("Class \"{0}\" is not a subclass of or equal to target class \"{1}\"")
   @RBArgComment0(" {0} is the name of the class")
   @RBArgComment1(" {1} is the name of the target class")
   public static final String CLASS_MISMATCH = "15";

   @RBEntry("Compound statement cannot be built because there are no component statements.")
   public static final String NOT_ENOUGH_COMPONENTS = "16";

   @RBEntry("Compound statement cannot be built becuase a component statement must evaluate to a single SQL statement.")
   public static final String INVALID_COMPONENT = "17";

   @RBEntry("Query object is invalid.")
   public static final String INVALID_QUERY = "18";

   @RBEntry("Statement cannot be built because a sub-select statement must evaluate to a single SQL statement.")
   public static final String INVALID_SUBSELECT = "19";

   @RBEntry("The TableExpression in the FromClause at position {0} must represent a class.")
   @RBArgComment0(" integer position")
   public static final String INVALID_TABLE_CLASS = "20";

   @RBEntry("Class \"{0}\" must be a Link class.")
   public static final String INVALID_LINK = "21";

   @RBEntry("The returned query result has exceeded the query limit, and only {0} result returned.")
   @RBArgComment0(" partial result number with accessed controlled")
   public static final String INCOMPLETE_RESULT_RETURN = "22";

   @RBEntry("Access is not permitted to advanced query capabilities.")
   public static final String ADVANCED_QUERY_ACCESS = "23";

   @RBEntry("The specified LOB length is too large for this operation.")
   public static final String LOB_LENGTH_UNSUPPORTED = "24";

   @RBEntry("Error writing LOB as stream.")
   public static final String LOB_WRITE_UPDATE_ERROR = "25";

   @RBEntry("An attribute of type small BLOB exceeded the maximum size ")
   public static final String SMALL_BLOB_SIZE_EXCEEDED = "26";

   @RBEntry("Invalid reference property name: class=\"{0}\" property=\"{1}\" ")
   @RBArgComment0(" {0} is a Java class name")
   @RBArgComment1(" {1} is a Java beans property name")
   public static final String INVALID_REFERENCE_PROPERTY_NAME = "27";

   @RBEntry("Invalid reference type: {0} ")
   @RBArgComment0(" {0} is a reference class name")
   public static final String INVALID_REFERENCE_TYPE = "28";

   @RBEntry("Database query operation interrupted")
   public static final String DATABASE_QUERY_INTERRUPT = "29";

   @RBEntry("Invalid role in association: link class=\"{0}\" role=\"{1}\".")
   @RBArgComment0("link class name of the association")
   @RBArgComment1("role name of the association")
   public static final String INVALID_ROLE_FOR_LINK_ASSOC = "30";

   @RBEntry("LOB Locator object ID does not reference an existing LOB: oid={0}.")
   public static final String LOB_LOCATOR_INVALID_OID = "31";

   @RBEntry("{0} is not unique.")
   @RBArgComment0(" {0} is the identifier of the object:")
   public static final String NOT_UNIQUE = "32";

   @RBEntry("Deadlock is detected on the {0} object in the database.")
   @RBArgComment0(" {0} is the identifier of the object:")
   public static final String DEAD_LOCK_DETECTED = "33";

   @RBEntry("The table for class {0}  is already locked in the database.")
   @RBArgComment0(" {0} is the class name:")
   public static final String CLASS_TABLE_LOCKED = "34";

   @RBEntry("Deadlock is detected on the {0} object in the database.")
   @RBArgComment0(" {0} is the identifier of the object:")
   public static final String DEAD_LOCK_DETECTED_FOR_CLASSTABLE = "35";

   @RBEntry("A statement with aggregate function can not be used with access control.")
   public static final String AGGREGATE_QUERY_NOT_ALLOWED_WITH_ACCESS_CONTROL = "36";

   @RBEntry("This query with string expression of {0} is not allowed with FOR UPDATE options.")
   @RBArgComment0(" {0} is the string expression of the query object:")
   public static final String FOR_UPDATE_NOT_ALLOWED = "37";

   @RBEntry("Invalid function argument: value=\"{0}\".")
   @RBArgComment0(" {0} is the string representation of the invalid function")
   public static final String INVALID_FUNCTION_ARGUMENT = "38";

   @RBEntry("Invalid access controller: value=\"{0}\".")
   @RBArgComment0(" {0} is the string representation of the invalid access controller")
   public static final String INVALID_ACCESS_CONTROLLER = "39";

   @RBEntry("Unsupported source type for navigate: class=\"{0}\".")
   @RBArgComment0(" {0} is the class name of the unnsupported source of the navigate")
   public static final String UNSUPPORTED_NAVIGATE_SOURCE = "40";

   @RBEntry("Object \"{0}\" which is going to be reinsert does not have valid persistence info set.")
   @RBArgComment0("  {0} is the id of the object that is going to be reinsert in the database.")
   public static final String REINSERT_WITH_INVALID_PERSISTINFO = "41";

   @RBEntry("Blob encoding error: attribute=\"{0}\" data=\"{1}\".")
   @RBArgComment0("attribute name of the Blob")
   @RBArgComment1("data value of the Blob")
   public static final String BLOB_ENCODING_ERROR = "42";

   @RBEntry("The batch status expected length \"{0}\" does not match the actual length \"{1}\".")
   @RBArgComment0("Expected length of the batch status array")
   @RBArgComment1("Actual length of the batch status array")
   public static final String BATCH_STATUS_INVALID_LENGTH = "50";

   @RBEntry("The batch status indicates a failure: \"{0}\".")
   @RBArgComment0("List of batch status error codes")
   public static final String BATCH_STATUS_INVALID_UPDATE_COUNTS = "51";

   @RBEntry("Deadlock is detected on one or more \"{0}\" objects in the database.")
   @RBArgComment0(" {0} is the type of the object")
   public static final String DEAD_LOCK_DETECTED_FOR_MULTIPLE_OBJECTS = "100";

   @RBEntry("SQL function \"{0}\" must have two or more arguments.  The number of arguments is {1}.")
   @RBArgComment0("{0} is the SQL function name")
   @RBArgComment1("{1} is the number of SQL function arguments")
   public static final String INVALID_INFIX_ARGUMENT_COUNT = "200";

   @RBEntry("SQL function \"{0}\" must have at least {1} arguments and the number of arguments is {2}.")
   @RBArgComment0("{0} is the SQL function name")
   @RBArgComment1("{1} is the minimum number of SQL function arguments")
   @RBArgComment2("{2} is the actual number of SQL function arguments")
   public static final String INVALID_MINIMUM_ARGUMENT_COUNT = "201";

   @RBEntry("SQL function \"{0}\" cannot have more than {1} arguments and the number of arguments is {2}.")
   @RBArgComment0("{0} is the SQL function name")
   @RBArgComment1("{1} is the maximum number of SQL function arguments")
   @RBArgComment2("{2} is the actual number of SQL function arguments")
   public static final String INVALID_MAXIMUM_ARGUMENT_COUNT = "202";

   @RBEntry("Deadlock is detected on one or more objects in the database.")
   @RBArgComment0(" {0} is the type of the object")
   public static final String DEAD_LOCK_DETECTED_FOR_QUERY = "203";

   @RBEntry("Argument value must be an array of the appropriate type.  The expected element type is \"{0}\" and the actual value type is \"{1}\".")
   @RBArgComment0(" {0} is the element type")
   @RBArgComment1(" {1} is the type of the value")
   public static final String INVALID_ARGUMENT_ARRAY_TYPE = "210";

}
