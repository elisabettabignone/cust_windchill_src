/* bcwti
 *
 * Copyright (c) 2012 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */

package wt.pds.passwordexpiry;

import wt.util.resource.*;

@RBUUID("wt.pds.passwordexpiry.passwordExpiryResource")
public final class passwordExpiryResource_it extends WTListResourceBundle {

   @RBEntry("Password archivio dati in scadenza fra {0} giorni per Windchill all'indirizzo {1}")
   public static final String SUBJECT = "0";

   @RBEntry("Password archivio dati in scadenza oggi per Windchill all'indirizzo {0}")
   public static final String SUBJECT_TODAY = "1";
}
