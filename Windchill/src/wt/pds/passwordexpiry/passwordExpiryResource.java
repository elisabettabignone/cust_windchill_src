/* bcwti
 *
 * Copyright (c) 2012 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */

package wt.pds.passwordexpiry;

import wt.util.resource.*;

@RBUUID("wt.pds.passwordexpiry.passwordExpiryResource")
public final class passwordExpiryResource extends WTListResourceBundle {

   @RBEntry("Datastore password expiring in {0} days for Windchill at {1}")
   public static final String SUBJECT = "0";

   @RBEntry("Datastore password expiring today for Windchill at {0}")
   public static final String SUBJECT_TODAY = "1";
}
