package wt.pds;

import wt.util.resource.*;

@RBUUID("wt.pds.pdsResource")
public final class pdsResource_it extends WTListResourceBundle {
   /**
/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
    * pdsResource message resource bundle [English/US]
    * 
    * Usage notes:
    * Last USED ID: 9
    * DO NOT REUSE IDS.  If an message becomes obsolete, comment it out
    * but do not create a new message with that id.
    * @version 	1.0, 97 August
    * @author	Dave Hoplin
    **/
   @RBEntry("Si è verificato un errore di persistenza. Messaggio di sistema:")
   public static final String GENERAL_ERROR = "0";

   @RBEntry("Si è verificato un errore SQL. Messaggio di sistema del database:")
   public static final String GENERAL_SQL_ERROR = "1";

   @RBEntry("La classe \"{0}\" non è una classe persistente.")
   @RBArgComment0(" {0} is the name of the class that is not eligible for a persistence opertion:")
   public static final String CLASS_NOT_PERSISTENT = "2";

   @RBEntry("L'oggetto \"{0}\" è già persistente.")
   @RBArgComment0("  {0} is the id of the object that is already in the database, so can't be inserted:")
   public static final String ALREADY_PERSISTENT = "3";

   @RBEntry("L'oggetto \"{0}\" non è persistente.")
   @RBArgComment0(" {0} is the id of the object that is not in the database, so can't be updated nor deleted:")
   public static final String NOT_PERSISTENT = "4";

   @RBEntry("Associazione non valida")
   public static final String INVALID_ASSOCIATION = "5";

   @RBEntry("Il ruolo \"{0}\" non è valido nell'associazione corrente.")
   @RBArgComment0(" {0} is the role name:")
   public static final String INVALID_ROLE_FOR_ASSOC = "6";

   @RBEntry("La classe \"{0}\" non è valida nell'associazione corrente.")
   @RBArgComment0(" {0} is the class name:")
   public static final String INVALID_CLASS_FOR_ASSOC = "7";

   @RBEntry("Operazione sul database interrotta")
   public static final String DATABASE_OP_INTERRUPT = "8";

   @RBEntry("Operazione \"{0}\" sul database interrotta")
   public static final String DATABASE_FOR_OP_INTERRUPT = "9";

   @RBEntry("Errore di scrittura LOB: dimensione segmento = {0}, dimensione effettiva scritta = {1}")
   @RBArgComment0(" a number")
   @RBArgComment1(" a number")
   public static final String LOB_WRITE_ERROR = "10";

   @RBEntry("Reimpostazione su un marcatore non valido")
   public static final String INVALID_MARK = "11";

   @RBEntry("L'oggetto {0} è già bloccato nel database.")
   @RBArgComment0(" {0} is the identifier of the object:")
   public static final String ROW_LOCKED = "12";

   @RBEntry("La classe \"{0}\" e tutte le relative sottoclassi non sono classi concrete.")
   @RBArgComment0(" {0} is the name of the class:")
   public static final String CLASS_NOT_CONCRETE = "13";

   @RBEntry("L'attributo \"{0}\" non è un membro della classe \"{1}\"")
   @RBArgComment0(" {0} is the attribute name")
   @RBArgComment1(" {1} is the class name")
   public static final String ATTR_NOT_IN_CLASS = "14";

   @RBEntry("La classe \"{0}\" non è una sottoclasse e non è equivalente alla classe di destinazione \"{1}\"")
   @RBArgComment0(" {0} is the name of the class")
   @RBArgComment1(" {1} is the name of the target class")
   public static final String CLASS_MISMATCH = "15";

   @RBEntry("Impossibile creare un'istruzione composta perché non comprende dei componenti.")
   public static final String NOT_ENOUGH_COMPONENTS = "16";

   @RBEntry("La valutazione di un'istruzione componente deve corrispondere a una singola istruzione SQL. Impossibile creare l'istruzione composta.")
   public static final String INVALID_COMPONENT = "17";

   @RBEntry("L'oggetto dell'interrogazione è invalido.")
   public static final String INVALID_QUERY = "18";

   @RBEntry("Impossibile creare questa istruzione perché una sottoistruzione deve generare una sola istruzione SQL.")
   public static final String INVALID_SUBSELECT = "19";

   @RBEntry("L'espressione della tabella nella FromClause nella posizione {0} deve rappresentare una classe.")
   @RBArgComment0(" integer position")
   public static final String INVALID_TABLE_CLASS = "20";

   @RBEntry("La classe \"{0}\" deve essere una classe di link.")
   public static final String INVALID_LINK = "21";

   @RBEntry("Il risultato dell'interrogazione ha ecceduto il limite dell'interrogazione. È stato restituito solo il risultato {0}.")
   @RBArgComment0(" partial result number with accessed controlled")
   public static final String INCOMPLETE_RESULT_RETURN = "22";

   @RBEntry("L'accesso alle funzioni di interrogazione avanzate non è consentito.")
   public static final String ADVANCED_QUERY_ACCESS = "23";

   @RBEntry("La lunghezza specificata LOB è troppo grande per quest'operazione.")
   public static final String LOB_LENGTH_UNSUPPORTED = "24";

   @RBEntry("Errore durante la scrittura di LOB come flusso")
   public static final String LOB_WRITE_UPDATE_ERROR = "25";

   @RBEntry("Un attributo di tipo BLOB piccolo supera la dimensione massima")
   public static final String SMALL_BLOB_SIZE_EXCEEDED = "26";

   @RBEntry("Nome della proprietà di riferimento non valido: \"{0}\" proprietà=\"{1}\" ")
   @RBArgComment0(" {0} is a Java class name")
   @RBArgComment1(" {1} is a Java beans property name")
   public static final String INVALID_REFERENCE_PROPERTY_NAME = "27";

   @RBEntry("Tipo di riferimento non valido: {0}")
   @RBArgComment0(" {0} is a reference class name")
   public static final String INVALID_REFERENCE_TYPE = "28";

   @RBEntry("Operazione di interrogazione database interrotta")
   public static final String DATABASE_QUERY_INTERRUPT = "29";

   @RBEntry("Ruolo non valido nell'associazione: classe link=\"{0}\" ruolo=\"{1}\"")
   @RBArgComment0("link class name of the association")
   @RBArgComment1("role name of the association")
   public static final String INVALID_ROLE_FOR_LINK_ASSOC = "30";

   @RBEntry("L'ID oggetto del localizzatore LOB non referenzia un LOB esistente: oid={0}.")
   public static final String LOB_LOCATOR_INVALID_OID = "31";

   @RBEntry("{0} non è univoco.")
   @RBArgComment0(" {0} is the identifier of the object:")
   public static final String NOT_UNIQUE = "32";

   @RBEntry("È stato rilevato un blocco critico sull'oggetto {0} del database.")
   @RBArgComment0(" {0} is the identifier of the object:")
   public static final String DEAD_LOCK_DETECTED = "33";

   @RBEntry("La tabella della clasee {0} è già bloccata nel database.")
   @RBArgComment0(" {0} is the class name:")
   public static final String CLASS_TABLE_LOCKED = "34";

   @RBEntry("È stato rilevato un blocco critico sull'oggetto {0} del database.")
   @RBArgComment0(" {0} is the identifier of the object:")
   public static final String DEAD_LOCK_DETECTED_FOR_CLASSTABLE = "35";

   @RBEntry("Un'istruzione con funzione di aggregazione non può essere usata con il controllo d'accesso.")
   public static final String AGGREGATE_QUERY_NOT_ALLOWED_WITH_ACCESS_CONTROL = "36";

   @RBEntry("L'interrogazione con l'espressione {0} non è consentita con le opzioni PER AGGIORNAMENTO.")
   @RBArgComment0(" {0} is the string expression of the query object:")
   public static final String FOR_UPDATE_NOT_ALLOWED = "37";

   @RBEntry("Argomento funzione non valido: valore=\"{0}\".")
   @RBArgComment0(" {0} is the string representation of the invalid function")
   public static final String INVALID_FUNCTION_ARGUMENT = "38";

   @RBEntry("Controller accesso non valido: valore=\"{0}\".")
   @RBArgComment0(" {0} is the string representation of the invalid access controller")
   public static final String INVALID_ACCESS_CONTROLLER = "39";

   @RBEntry("Tipo di origine non supportato per la navigazione. Classe = \"{0}\".")
   @RBArgComment0(" {0} is the class name of the unnsupported source of the navigate")
   public static final String UNSUPPORTED_NAVIGATE_SOURCE = "40";

   @RBEntry("L'oggetto \"{0}\" da reinserire non dispone di informazioni impostate di persistenza valide.")
   @RBArgComment0("  {0} is the id of the object that is going to be reinsert in the database.")
   public static final String REINSERT_WITH_INVALID_PERSISTINFO = "41";

   @RBEntry("Errore di codifica blob: attributo = \"{0}\" dati = \"{1}\".")
   @RBArgComment0("attribute name of the Blob")
   @RBArgComment1("data value of the Blob")
   public static final String BLOB_ENCODING_ERROR = "42";

   @RBEntry("La lunghezza prevista dello stato batch ({0}) non corrisponde alla lunghezza effettiva: {1}.")
   @RBArgComment0("Expected length of the batch status array")
   @RBArgComment1("Actual length of the batch status array")
   public static final String BATCH_STATUS_INVALID_LENGTH = "50";

   @RBEntry("Lo stato batch indica un errore: \"{0}\".")
   @RBArgComment0("List of batch status error codes")
   public static final String BATCH_STATUS_INVALID_UPDATE_COUNTS = "51";

   @RBEntry("È stato rilevato un blocco critico su uno o più oggetti \"{0}\" del database.")
   @RBArgComment0(" {0} is the type of the object")
   public static final String DEAD_LOCK_DETECTED_FOR_MULTIPLE_OBJECTS = "100";

   @RBEntry("La funzione SQL \"{0}\" deve avere due o più argomenti. Il numero di argomenti è {1}.")
   @RBArgComment0("{0} is the SQL function name")
   @RBArgComment1("{1} is the number of SQL function arguments")
   public static final String INVALID_INFIX_ARGUMENT_COUNT = "200";

   @RBEntry("La funzione SQL \"{0}\" deve avere almeno {1} argomenti. Il numero di argomenti è {2}.")
   @RBArgComment0("{0} is the SQL function name")
   @RBArgComment1("{1} is the minimum number of SQL function arguments")
   @RBArgComment2("{2} is the actual number of SQL function arguments")
   public static final String INVALID_MINIMUM_ARGUMENT_COUNT = "201";

   @RBEntry("La funzione SQL \"{0}\" non può avere più di {1} argomenti. Il numero di argomenti è {2}.")
   @RBArgComment0("{0} is the SQL function name")
   @RBArgComment1("{1} is the maximum number of SQL function arguments")
   @RBArgComment2("{2} is the actual number of SQL function arguments")
   public static final String INVALID_MAXIMUM_ARGUMENT_COUNT = "202";

   @RBEntry("È stato rilevato un blocco critico su uno o più oggetti del database.")
   @RBArgComment0(" {0} is the type of the object")
   public static final String DEAD_LOCK_DETECTED_FOR_QUERY = "203";

   @RBEntry("Il valore dell'argomento deve essere un array di tipo appropriato. Il tipo di elemento previsto è \"{0}\" e il tipo di valore effettivo è \"{1}\".")
   @RBArgComment0(" {0} is the element type")
   @RBArgComment1(" {1} is the type of the value")
   public static final String INVALID_ARGUMENT_ARRAY_TYPE = "210";

}
