package wt.pds.db2;

import wt.util.resource.*;

@RBUUID("wt.pds.db2.db2Resource")
public final class db2Resource extends WTListResourceBundle {
   /**
/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
    * sqlServerResource message resource bundle [English/US]
    * 
    * Usage notes:
    * Last USED ID:
    * DO NOT REUSE IDS.  If an message becomes obsolete, comment it out
    * but do not create a new message with that id.
    **/
   @RBEntry("Status indicates failure in passing stored procedure arguments.")
   public static final String STORED_PROC_INPUT_ARG_FAIL = "0";

   @RBEntry("Invalid stored procedure parameter offset: index={0} result sets skipped={1} offset={2}.")
   @RBArgComment0("Stored procedure parameter index.")
   @RBArgComment1("The number of ResultSets that have been skip so far.")
   @RBArgComment2("The offset value specified by the stored procedure.")
   public static final String INVALID_STORED_PROC_PARAMETER_OUTPUT_OFFSET = "1";
}
