/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.router;

import wt.util.resource.*;

@RBUUID("wt.router.routerResource")
public final class routerResource_it extends WTListResourceBundle {
   /**
    * Error Messages
    **/
   @RBEntry("Router: impossibile trovare la classe d'evento \"{0}\".")
   public static final String EVENT_CLASS_NOT_FOUND = "0";

   @RBEntry("Router: impossibile trovare la classe \"{0}\".")
   public static final String CLASS_CLASS_NOT_FOUND = "1";

   @RBEntry("Router: impossibile trovare il metodo della classe: \"{0}\"")
   public static final String CLASS_OF_POSTED_METHOD_NOT_FOUND = "2";

   @RBEntry("Router: impossibile trovare il metodo: \"{0}\"")
   public static final String POSTED_METHOD_NOT_FOUND = "3";

   @RBEntry("Router: il metodo è inaccessibile: \"{0}\"")
   public static final String POSTED_METHOD_NOT_ACCESSIBLE = "4";

   @RBEntry("Router: il numero di code per il router \"{0}\" è insufficiente: {1}")
   public static final String NUMBER_PROC_QUEUES_ZERO = "5";
}
