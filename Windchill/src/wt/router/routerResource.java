/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.router;

import wt.util.resource.*;

@RBUUID("wt.router.routerResource")
public final class routerResource extends WTListResourceBundle {
   /**
    * Error Messages
    **/
   @RBEntry("Router: Class of event \"{0}\" not found")
   public static final String EVENT_CLASS_NOT_FOUND = "0";

   @RBEntry("Router: Class \"{0}\" not found")
   public static final String CLASS_CLASS_NOT_FOUND = "1";

   @RBEntry("Router: Class of posted method not found: \"{0}\"")
   public static final String CLASS_OF_POSTED_METHOD_NOT_FOUND = "2";

   @RBEntry("Router: Posted method not found: \"{0}\"")
   public static final String POSTED_METHOD_NOT_FOUND = "3";

   @RBEntry("Router: Posted method is not accessible: \"{0}\"")
   public static final String POSTED_METHOD_NOT_ACCESSIBLE = "4";

   @RBEntry("Router: Number of processing queues for router \"{0}\" is too small: {1}")
   public static final String NUMBER_PROC_QUEUES_ZERO = "5";
}
