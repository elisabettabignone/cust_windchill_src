/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.team;

import wt.util.resource.*;

@RBUUID("wt.team.teamResource")
public final class teamResource extends WTListResourceBundle {
   @RBEntry("Could not get Cabinet Reference")
   public static final String NO_CAB_REF = "0";

   @RBEntry("Could not get folder path")
   public static final String NO_FOLDER_PATH = "1";

   @RBEntry("The administrative domain: \"{0}\" has multiple teams with name: \"{1}\".  The team has not been set for this object.")
   public static final String MULTIPLE_TEAMS = "10";

   @RBEntry("The value passed for parameter - \"{0}\" was null.")
   public static final String NULL_PARAMETER = "11";

   @RBEntry("The team \"{0}\" was not found in the system, cannot continue search.")
   public static final String NO_TEAM_FOR_SEARCH = "12";

   @RBEntry("\"{0}\" is an invalid role.  Valid roles are found in the following resource bundles: wt.project.RoleRB.java or wt.project.ActorRoleRB.java")
   public static final String INVALID_ROLE = "13";

   @RBEntry("You cannot set the Team because the {0} object is already persisted.  Use the Reassign Team option.  ")
   public static final String CANT_SET_TEAM_WHEN_PERSISTENT = "14";

   @RBEntry("Reteam")
   public static final String RETEAM = "15";

   @RBEntry("You are not authorized to {0}.  You need modify rights on the object to complete this task.")
   public static final String NOT_AUTHORIZED_TO_MODIFY = "16";

   @RBEntry("You are not allowed to reteam a working copy.  Check-in the object before proceeding.")
   public static final String WORKING_COPY = "17";

   @RBEntry("The team template \"{0}\" was not found in the system, cannot continue search.")
   public static final String NO_TEAM_TEMPLATE_FOR_SEARCH = "18";

   @RBEntry("{0} not allowed because the object is not the latest iteration.  Refresh the data and try again.  ")
   public static final String OBJECT_NOT_LATEST_ITERATION = "19";

   @RBEntry("Could not get location")
   public static final String NO_LOCATION = "2";

   @RBEntry("You may not {0} a checked out object.")
   public static final String CHECKED_OUT_OBJECT = "20";

   @RBEntry("Copy Of")
   public static final String COPY_OF = "21";

   @RBEntry("The {0} team associated with the {1} object is not enabled.  Select a team that is enabled.")
   public static final String ASSOC_TEAM_NOT_ENABLED = "22";

   @RBEntry(", ")
   public static final String IDENTITY_STRING_DELIMITER = "23";

   @RBEntry("Multiple teams were found:  {0}.  Clarify the team identity and try again.  ")
   public static final String CLARIFY_TEAM_IDENTITY = "24";

   @RBEntry("The {0} team is in use.  All uses must be removed before the {0} team can be deleted.")
   public static final String DELETE_TEAM_PROHIBITED = "25";

   @RBEntry("The {0} team template is in use.  All uses must be removed before the {0} team template can be deleted.")
   public static final String DELETE_TEAM_TEMPLATE_PROHIBITED = "26";

   @RBEntry("<B>")
   @RBPseudo(false)
   public static final String LABEL_BEGIN = "27";

   @RBEntry(": </B>")
   @RBPseudo(false)
   public static final String LABEL_END = "28";

   @RBEntry("</TD></TR><TR><TD align=right VALIGN=TOP>")
   @RBPseudo(false)
   public static final String ROLE_BEGIN = "29";

   @RBEntry("You are not authorized to update the role participants list.  To complete this action, you must have modify permissions on {0}. ")
   public static final String ROLE_PARTICIPANT_MODIFY_NOT_ALLOWED = "3";

   @RBEntry("Role Participants:")
   public static final String ROLE_PARTICIPANTS_LABEL = "30";

   @RBEntry("Multiple teams were found:  {0}.  Clarify the team template identity and try again.  ")
   public static final String CLARIFY_TEAM_TEMPLATE_IDENTITY = "31";

   @RBEntry("The administrative domain: \"{0}\" has multiple team templates with name: \"{1}\".  The team template has not been set for this object.")
   public static final String MULTIPLE_TEAM_TEMPLATES = "32";

   @RBEntry("A team template \"{0}\" does not exist in the administrative domain: \"{1}\".  The team template has not been set for this object.")
   public static final String NONEXISTENT_TEAM_TEMPLATE = "33";

   @RBEntry("A team \"{0}\" does not exist in the administrative domain: \"{1}\".  The team has not been set for this object.")
   public static final String NONEXISTENT_TEAM = "34";

   @RBEntry("The team distribution list \"{0}\" was not found in the system, cannot continue search.")
   public static final String NO_TEAM_DISTRIBUTION_LIST_FOR_SEARCH = "35";

   @RBEntry("Unable to set pool to {0}.  Pool members must be a WTPrincipal or a Team.")
   public static final String INVALID_POOL_MEMBER = "36";

   @RBEntry("Multiple teams were found:  {0}.  Clarify the team distribution list identity and try again.  ")
   public static final String CLARIFY_TEAM_DISTRIBUTION_LIST_IDENTITY = "37";

   @RBEntry("A team distribution list \"{0}\" does not exist in the administrative domain: \"{1}\".  ")
   public static final String NONEXISTENT_TEAM_DISTRIBUTION_LIST = "38";

   @RBEntry("The administrative domain: \"{0}\" has multiple team distribution lists with name: \"{1}\". ")
   public static final String MULTIPLE_TEAM_DISTRIBUTION_LISTS = "39";

   @RBEntry("You cannot set the TeamTemplate because the {0} object is already persisted.  Use the Reassign Team option.  ")
   public static final String CANT_SET_TEAM_TEMPLATE_WHEN_PERSISTENT = "40";

   @RBEntry("PROPERTIES ")
   public static final String PROPERTIES = "41";

   @RBEntry("A team cannot be stored in a personal cabinet.  Choose another location.  ")
   public static final String TEAM_IN_USER_LOCATION = "4";

   @RBEntry("A team template cannot be stored in a personal cabinet.  Choose another location.  ")
   public static final String TEAM_TEMPLATE_IN_USER_LOCATION = "5";

   @RBEntry(", ")
   public static final String PARTICIPANT_SEPARATOR = "6";

   @RBEntry("Unable to fill in the attributes for the team in the object.")
   public static final String INFLATE_FAILED = "7";

   @RBEntry("Unable to set search criteria to team = {0}.")
   public static final String SEARCH_CRITERIA = "8";

   @RBEntry(": </TD><TD colspan=3 wrap>")
   @RBPseudo(false)
   public static final String ROLE_END = "9";

   @RBEntry("No roles are available for setting participants")
   public static final String NO_ROLE_FOR_SETTING_PARTICIPANTS = "42";

   @RBEntry("Team Member List.")
   public static final String TEAM_MEMBERS_LIST = "43";
   
   @RBEntry("Roles")
   public static final String  ROLES= "44";
   
   @RBEntry("Participants")
   public static final String PARTICIPANTS = "45";
   
   @RBEntry("Last Modified")
   public static final String  LAST_MODIFIED= "46";

}
