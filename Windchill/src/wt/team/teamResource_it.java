/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.team;

import wt.util.resource.*;

@RBUUID("wt.team.teamResource")
public final class teamResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile ottenere i riferimenti dello schedario")
   public static final String NO_CAB_REF = "0";

   @RBEntry("Impossibile individuare il percorso della cartella")
   public static final String NO_FOLDER_PATH = "1";

   @RBEntry("Il dominio amministrativo \"{0}\" contiene più team con nome \"{1}\".  Impossibile impostare il team per l'oggetto.")
   public static final String MULTIPLE_TEAMS = "10";

   @RBEntry("Il valore trasferito per il parametro \"{0}\" era nullo.")
   public static final String NULL_PARAMETER = "11";

   @RBEntry("Il team \"{0}\" non è stato trovato nel sistema. Impossibile proseguire la ricerca.")
   public static final String NO_TEAM_FOR_SEARCH = "12";

   @RBEntry("\"{0}\" non è un ruolo valido.  I ruoli validi si trovano nei resource bundle seguenti:  wt.project.RoleRB.java o wt.project.ActorRoleRB.java")
   public static final String INVALID_ROLE = "13";

   @RBEntry("L'oggetto {0} è già persistente. Impossibile impostare il team. Utilizzare l'opzione Assegna a un altro team.")
   public static final String CANT_SET_TEAM_WHEN_PERSISTENT = "14";

   @RBEntry("Ricrea team")
   public static final String RETEAM = "15";

   @RBEntry("Nessuna autorizzazione per {0}. È necessario modificare i diritti sull'oggetto per completare il task.")
   public static final String NOT_AUTHORIZED_TO_MODIFY = "16";

   @RBEntry("Non è consentito ricreare il team per una copia in modifica.  Effettuare Check-In dell'oggetto prima di continuare.")
   public static final String WORKING_COPY = "17";

   @RBEntry("Il modello team \"{0}\" non è stato trovato nel sistema. Impossibile proseguire la ricerca.")
   public static final String NO_TEAM_TEMPLATE_FOR_SEARCH = "18";

   @RBEntry("L'oggetto non è l'ultima iterazione. L'azione {0} non è consentita.  Aggiornare i dati e riprovare.")
   public static final String OBJECT_NOT_LATEST_ITERATION = "19";

   @RBEntry("Impossibile individuare la posizione")
   public static final String NO_LOCATION = "2";

   @RBEntry("Potrebbe non essere possibile {0} un oggetto sottoposto a Check-Out.")
   public static final String CHECKED_OUT_OBJECT = "20";

   @RBEntry("Copia di")
   public static final String COPY_OF = "21";

   @RBEntry("Il team {0} asociato all'oggetto {1} non è attivato. Selezionare un team attivato.")
   public static final String ASSOC_TEAM_NOT_ENABLED = "22";

   @RBEntry(", ")
   public static final String IDENTITY_STRING_DELIMITER = "23";

   @RBEntry("Più team sono stati trovati: {0}.  Specificare il team identificativo dell'oggetto e riprovare.")
   public static final String CLARIFY_TEAM_IDENTITY = "24";

   @RBEntry("Il team {0} è in uso al momento. Prima di eliminare il team {0} è necessario rimuovere tutti i componenti.")
   public static final String DELETE_TEAM_PROHIBITED = "25";

   @RBEntry("Il modello di team {0} è in uso al momento. Prima di eliminare il modello di team {0} è necessario rimuovere tutti i componenti.")
   public static final String DELETE_TEAM_TEMPLATE_PROHIBITED = "26";

   @RBEntry("<B>")
   @RBPseudo(false)
   public static final String LABEL_BEGIN = "27";

   @RBEntry(": </B>")
   @RBPseudo(false)
   public static final String LABEL_END = "28";

   @RBEntry("</TD></TR><TR><TD align=right VALIGN=TOP>")
   @RBPseudo(false)
   public static final String ROLE_BEGIN = "29";

   @RBEntry("L'utente non è autorizzato ad aggiornare l'elenco dei partecipanti ai ruoli. Per completare l'azione, è necessario disporre del permesso di modifica di {0}.")
   public static final String ROLE_PARTICIPANT_MODIFY_NOT_ALLOWED = "3";

   @RBEntry("Partecipanti ai ruoli:")
   public static final String ROLE_PARTICIPANTS_LABEL = "30";

   @RBEntry("Più team sono stati trovati: {0}.  Specificare l'identificativo del modello di team e riprovare.")
   public static final String CLARIFY_TEAM_TEMPLATE_IDENTITY = "31";

   @RBEntry("Il dominio amministrativo \"{0}\" contiene più modelli di team con nome \"{1}\".  Impossibile impostare il modello di team per l'oggetto.")
   public static final String MULTIPLE_TEAM_TEMPLATES = "32";

   @RBEntry("Il modello di team \"{0}\" non esiste nel dominio amministrativo: \"{1}\".  Impossibile impostare il modello di team per l'oggetto.")
   public static final String NONEXISTENT_TEAM_TEMPLATE = "33";

   @RBEntry("Il team \"{0}\" non esiste nel dominio amministrativo \"{1}\".  Impossibile impostare il team per l'oggetto.")
   public static final String NONEXISTENT_TEAM = "34";

   @RBEntry("La lista di distribuzione del team \"{0}\" non è stata trovata nel sistema. Impossibile proseguire la ricerca.")
   public static final String NO_TEAM_DISTRIBUTION_LIST_FOR_SEARCH = "35";

   @RBEntry("Impossibile impostare il pool a {0}. Solo gli utenti/gruppi/ruoli (WTPrincipal) o i team possono essere membri del pool.")
   public static final String INVALID_POOL_MEMBER = "36";

   @RBEntry("Sono stati trovati più team:  {0}. Chiarire la lista di distribuzione di team e riprovare.")
   public static final String CLARIFY_TEAM_DISTRIBUTION_LIST_IDENTITY = "37";

   @RBEntry("La lista di distribuzione di team \"{0}\" non esiste nel dominio amministrativo \"{1}\".  ")
   public static final String NONEXISTENT_TEAM_DISTRIBUTION_LIST = "38";

   @RBEntry("Il dominio amministrativo \"{0}\" ha più liste di distribuzione di team chiamate \"{1}\". ")
   public static final String MULTIPLE_TEAM_DISTRIBUTION_LISTS = "39";

   @RBEntry("Impossibile impostare il modello di team (TeamTemplate) perché l'oggetto {0} è già persistente.  Usare l'opzione Assegna a un altro team.")
   public static final String CANT_SET_TEAM_TEMPLATE_WHEN_PERSISTENT = "40";

   @RBEntry("PROPRIETÀ")
   public static final String PROPERTIES = "41";

   @RBEntry("Impossibile memorizzare un team in uno schedario personale. Selezionare un'altra posizione.")
   public static final String TEAM_IN_USER_LOCATION = "4";

   @RBEntry("Impossibile memorizzare un modello di team in uno schedario personale. Selezionare un'altra posizione.")
   public static final String TEAM_TEMPLATE_IN_USER_LOCATION = "5";

   @RBEntry(", ")
   public static final String PARTICIPANT_SEPARATOR = "6";

   @RBEntry("Impossibile completare gli attributi per il team nell'oggetto.")
   public static final String INFLATE_FAILED = "7";

   @RBEntry("Impossibile impostare i criteri di ricerca al team = {0}.")
   public static final String SEARCH_CRITERIA = "8";

   @RBEntry(": </TD><TD colspan=3 wrap>")
   @RBPseudo(false)
   public static final String ROLE_END = "9";

   @RBEntry("Non è disponibile alcun ruolo per l'impostazione dei partecipanti")
   public static final String NO_ROLE_FOR_SETTING_PARTICIPANTS = "42";

   @RBEntry("Lista membri team.")
   public static final String TEAM_MEMBERS_LIST = "43";
   
   @RBEntry("Ruoli")
   public static final String  ROLES= "44";
   
   @RBEntry("Partecipanti")
   public static final String PARTICIPANTS = "45";
   
   @RBEntry("Data ultima modifica")
   public static final String  LAST_MODIFIED= "46";

}
