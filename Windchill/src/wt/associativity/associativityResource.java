/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.associativity;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBArgComment1;
import wt.util.resource.RBArgComment2;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.associativity.associativityResource")
public final class associativityResource extends WTListResourceBundle {
   @RBEntry("Paths {0} and {1} do not have an equivalent context")
   @RBArgComment0("{0} - First path for which validation applies")
   @RBArgComment1("{1} - Second path for which validation applies")
   public static final String PATH_WITHOUT_COMMON_CONSUMABLE = "0";

   @RBEntry("Equivalence link creation is prohibited for {0}")
   @RBArgComment0("{0} - Equivalence Link for which creation is prohibited")
   public static final String EQUIVALENCE_CREATION_PROHIBITED = "1";

   @RBEntry("Equivalence link deletion is prohibited for {0}")
   @RBArgComment0("{0} - Equivalence Link for which validation applies")
   public static final String EQUIVALENCE_DELETION_PROHIBITED = "2";

   @RBEntry("Equivalence link modification is prohibited for {0}")
   @RBArgComment0("{0} - Equivalence Link for which validation applies")
   public static final String EQUIVALENCE_MODIFICATION_PROHIBITED = "3";

   @RBEntry("Equivalence link must have a downstream context in order to be created")
   public static final String EQUIVALENCE_MUST_HAVE_DOWNSTREAM_CONTEXT = "4";

   @RBEntry("Equivalence link must have a upstream context in order to be created")
   public static final String EQUIVALENCE_MUST_HAVE_UPSTREAM_CONTEXT = "5";

   @RBEntry("If the downstream context is equal to the upstream context, the part cannot be equivalent to itself. No equivalent link can be created for this link {0}.")
   @RBArgComment0("{0} - Equivalence Link for which validation applies")
   public static final String EQUIVALENCE_DOWNSTREAM_AND_UPSTREAM_CONTEXT_MUST_BE_DIFFERENT = "6";

   @RBEntry("This equivalence association {0} has already been set to this component")
   @RBArgComment0("{0} - Equivalence Link for which validation applies")
   public static final String EQUIVALENCE_LINK_ALREADY_EXISTS = "7";

   @RBEntry("There is no equivalence link to support that occurrence link {0} -> {1}; therefore, it cannot be created.")
   @RBArgComment0("{0} - Downstream Occurrence of the Occurrence Link for which validation applies")
   @RBArgComment1("{1} - Upstream Occurrence of the Occurrence Link for which validation applies")
   public static final String CONSUMPTION_MUST_HAVE_UNDERLYING_EQUIVALENCE_LINK = "8";

   @RBEntry("Upstream context {0} cannot be a descendant view of downstream context {1}; therefore, link {2} cannot be created.")
   @RBArgComment0("{0} - Upstream Context of Equivalence Link for which validation applies")
   @RBArgComment1("{1} - Downstream Context of Equivalence Link for which validation applies")
   @RBArgComment2("{2} - Equivalence Link for which validation applies")
   public static final String EQUIVALENCE_LINK_UPSTREAM_CONTEXT_CANT_BE_DESCENDANT_OF_DOWNSTREAM_CONTEXT = "9";

   @RBEntry("{0} has to be checked out in order to split the part usage.")
   @RBArgComment0("{0} - Part that must be check out ")
   public static final String PARENT_PART_MUST_BE_CHECKED_OUT = "10";

   @RBEntry("The quantity to keep on the current part usage cannot be less than or equal to zero.")
   public static final String SPLIT_USAGE_LINK_QUANTITY_CANNOT_BE_NEGATIVE = "11";

   @RBEntry("The quantity to keep on the selected part usage cannot be greater than or equal to the quantity on the current part usage.")
   public static final String SPLIT_USAGE_LINK_QUANTITY_CANNOT_BE_GREATER = "12";

   @RBEntry("The quantity to keep on the current part usage cannot have a decimal value when the unit value is set to each.")
   public static final String SPLIT_USAGE_LINK_QUANTITY_CANNOT_BE_DECIMAL = "13";

   @RBEntry("Did not provide a part usage to split.")
   public static final String MISSING_PART_USAGE_LINK = "14";

   @RBEntry("Occurrence to keep is not in the part usage provided.")
   public static final String OCCURRENCE_OF_ANOTHER_PART_USAGE_LINK = "15";

   @RBEntry("The upstream occurrence of the part [{0},{1}] is already consumed in the downstream BOM. Do you want to proceed and use it again?")
   @RBArgComment0("{0} - Part name")
   @RBArgComment1("{1} - Part number")
   public static final String SAME_OCCURRENCE_LINK_WARNING = "16";

   @RBEntry("All equivalent links between the selected objects have already been set.")
   public static final String ALL_EQUIVALENCE_LINK_EXISTS = "17";

   @RBEntry("Equivalence links could not be updated. Alternate BOM {0} for the selected part has been checked out by another user and must be checked in before changes can be propagated to all alternate BOMs. ")
   public static final String ALTERNATE_BOM_CHECKED_OUT_BY_ANOTHER_USER_PROPAGATION_MESSAGE = "18";

    @RBEntry("Error while creation/preservation of Consumption Link. Possible reason can be a usage with a non-occurrenceable unit in path.")
    public static final String CONSUMPTION_LINK_CREATE_FAILED_AS_OCCURRENCES_COULD_NOT_BE_PERSISTED_MESSAGE = "19";

    @RBEntry("Unable to resolve usage path to occurrence path. Possible cause can be wrong expanision criteria.")
    public static final String UNABLE_TO_RESOLVE_USAGE_PATH_TO_OCCURRENCE_PATH_INVALID_EC = "20";

    @RBEntry("The version is view-independent, it cannot be branched")
    public static final String CAN_NOT_BRANCH_INDEPENDENT = "21";

    @RBEntry("Cannot delete view \"{0}\" because equivalence links have been assigned to it.")
    public static final String VIEW_IN_USE_BY_EQUIVALENCE_LINKS = "VIEW_IN_USE_BY_EQUIVALENCE_LINKS";
}
