/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.associativity;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBArgComment1;
import wt.util.resource.RBArgComment2;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.associativity.associativityResource")
public final class associativityResource_it extends WTListResourceBundle {
   @RBEntry("I percorsi {0} e {1} non hanno un contesto equivalente")
   @RBArgComment0("{0} - First path for which validation applies")
   @RBArgComment1("{1} - Second path for which validation applies")
   public static final String PATH_WITHOUT_COMMON_CONSUMABLE = "0";

   @RBEntry("Creazione link di equivalenza non consentita per {0}")
   @RBArgComment0("{0} - Equivalence Link for which creation is prohibited")
   public static final String EQUIVALENCE_CREATION_PROHIBITED = "1";

   @RBEntry("Eliminazione link di equivalenza non consentita per {0}")
   @RBArgComment0("{0} - Equivalence Link for which validation applies")
   public static final String EQUIVALENCE_DELETION_PROHIBITED = "2";

   @RBEntry("Modifica link di equivalenza non consentita per {0}")
   @RBArgComment0("{0} - Equivalence Link for which validation applies")
   public static final String EQUIVALENCE_MODIFICATION_PROHIBITED = "3";

   @RBEntry("Per creare un link di equivalenza, è necessario che questo disponga di un contesto a valle")
   public static final String EQUIVALENCE_MUST_HAVE_DOWNSTREAM_CONTEXT = "4";

   @RBEntry("Per creare un link di equivalenza, è necessario che questo disponga di un contesto a monte")
   public static final String EQUIVALENCE_MUST_HAVE_UPSTREAM_CONTEXT = "5";

   @RBEntry("Se il contesto a valle è uguale al contesto a monte, la parte non può essere equivalente a se stessa. Impossibile creare un link di equivalenza per il link {0}.")
   @RBArgComment0("{0} - Equivalence Link for which validation applies")
   public static final String EQUIVALENCE_DOWNSTREAM_AND_UPSTREAM_CONTEXT_MUST_BE_DIFFERENT = "6";

   @RBEntry("L'associazione di equivalenza {0} è stata già impostata per questo componente")
   @RBArgComment0("{0} - Equivalence Link for which validation applies")
   public static final String EQUIVALENCE_LINK_ALREADY_EXISTS = "7";

   @RBEntry("Non esiste un link di equivalenza per supportare il link caso d'impiego {0} -> {1}. Impossibile creare il link.")
   @RBArgComment0("{0} - Downstream Occurrence of the Occurrence Link for which validation applies")
   @RBArgComment1("{1} - Upstream Occurrence of the Occurrence Link for which validation applies")
   public static final String CONSUMPTION_MUST_HAVE_UNDERLYING_EQUIVALENCE_LINK = "8";

   @RBEntry("Il contesto a monte {0} non può essere una vista discendente del contesto a valle {1}. Impossibile creare il link {2}.")
   @RBArgComment0("{0} - Upstream Context of Equivalence Link for which validation applies")
   @RBArgComment1("{1} - Downstream Context of Equivalence Link for which validation applies")
   @RBArgComment2("{2} - Equivalence Link for which validation applies")
   public static final String EQUIVALENCE_LINK_UPSTREAM_CONTEXT_CANT_BE_DESCENDANT_OF_DOWNSTREAM_CONTEXT = "9";

   @RBEntry("È necessario sottoporre {0} a Check-Out per poter dividere l'utilizzo parte.")
   @RBArgComment0("{0} - Part that must be check out ")
   public static final String PARENT_PART_MUST_BE_CHECKED_OUT = "10";

   @RBEntry("Impossibile mantenere una quantità pari o inferiore a zero per l'utilizzo parte corrente.")
   public static final String SPLIT_USAGE_LINK_QUANTITY_CANNOT_BE_NEGATIVE = "11";

   @RBEntry("La quantità da mantenere per l'utilizzo parte selezionato non può essere maggiore o uguale alla quantità dell'utilizzo parte corrente.")
   public static final String SPLIT_USAGE_LINK_QUANTITY_CANNOT_BE_GREATER = "12";

   @RBEntry("Impossibile attribuire un valore decimale alla quantità per l'utilizzo parte corrente se il valore di unità è \"ciasc.\"")
   public static final String SPLIT_USAGE_LINK_QUANTITY_CANNOT_BE_DECIMAL = "13";

   @RBEntry("Non è stato fornito un utilizzo parte da dividere.")
   public static final String MISSING_PART_USAGE_LINK = "14";

   @RBEntry("Il caso d'impiego da mantenere non è nell'utilizzo parte fornito.")
   public static final String OCCURRENCE_OF_ANOTHER_PART_USAGE_LINK = "15";

   @RBEntry("Il caso d'impiego a monte della parte [{0},{1}] è già utilizzato nella distinta base a valle. Continuare e utilizzarlo nuovamente?")
   @RBArgComment0("{0} - Part name")
   @RBArgComment1("{1} - Part number")
   public static final String SAME_OCCURRENCE_LINK_WARNING = "16";

   @RBEntry("Tutti i link equivalenti tra gli oggetti selezionati sono già stati impostati.")
   public static final String ALL_EQUIVALENCE_LINK_EXISTS = "17";

   @RBEntry("Impossibile aggiornare i link di equivalenza. La distinta base alternativa {0} per la parte selezionata è stata sottoposta a Check-Out da un altro utente. È necessario effettuarne il Check-In prima che le modifiche possano essere propagate a tutte le distinte base alternative.")
   public static final String ALTERNATE_BOM_CHECKED_OUT_BY_ANOTHER_USER_PROPAGATION_MESSAGE = "18";

    @RBEntry("Errore durante la creazione/conservazione del link di consumo. Il possibile motivo è l'utilizzo di un'unità non applicabile a casi d'impiego nel percorso.")
    public static final String CONSUMPTION_LINK_CREATE_FAILED_AS_OCCURRENCES_COULD_NOT_BE_PERSISTED_MESSAGE = "19";

    @RBEntry("Impossibile risolvere il percorso di utilizzo nel percorso del caso d'impiego. Ciò può essere dovuto a criteri di espansione errati.")
    public static final String UNABLE_TO_RESOLVE_USAGE_PATH_TO_OCCURRENCE_PATH_INVALID_EC = "20";

    @RBEntry("La versione è indipendente dalla vista. Impossibile creare una diramazione")
    public static final String CAN_NOT_BRANCH_INDEPENDENT = "21";

    @RBEntry("Impossibile eliminare la vista \"{0}\" in quanto dispone di link di equivalenza assegnati.")
    public static final String VIEW_IN_USE_BY_EQUIVALENCE_LINKS = "VIEW_IN_USE_BY_EQUIVALENCE_LINKS";
}
