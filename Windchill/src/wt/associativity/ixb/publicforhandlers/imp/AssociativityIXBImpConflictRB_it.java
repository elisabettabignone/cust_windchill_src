/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.associativity.ixb.publicforhandlers.imp;

import wt.util.resource.*;

@RBUUID("wt.associativity.ixb.publicforhandlers.imp.AssociativityIXBImpConflictRB")
public final class AssociativityIXBImpConflictRB_it extends WTListResourceBundle {
   @RBEntry("Contesto a monte")
   public static final String UP_STREAM_CONTEXT = "0";

   @RBEntry("Contesto a valle")
   public static final String DOWN_STREAM_CONTEXT = "1";

   @RBEntry("Vista non trovata")
   public static final String VIEW_NOT_FOUND = "2";

   @RBEntry("Attivazione link caso d'impiego")
   public static final String IS_CONSUMABLE = "3";

   @RBEntry("Descrizione")
   public static final String DESCRIPTION = "4";
}
