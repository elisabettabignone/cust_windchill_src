/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.associativity.ixb.publicforhandlers.imp;

import wt.util.resource.*;

@RBUUID("wt.associativity.ixb.publicforhandlers.imp.AssociativityIXBImpConflictRB")
public final class AssociativityIXBImpConflictRB extends WTListResourceBundle {
   @RBEntry("Upstream Context")
   public static final String UP_STREAM_CONTEXT = "0";

   @RBEntry("Downstream Context")
   public static final String DOWN_STREAM_CONTEXT = "1";

   @RBEntry("View Not Found")
   public static final String VIEW_NOT_FOUND = "2";

   @RBEntry("Occurrence Link Enabler")
   public static final String IS_CONSUMABLE = "3";

   @RBEntry("Description")
   public static final String DESCRIPTION = "4";
}
