/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.jmx.core.mbeans;

/* In addition to providing useful functionality, this interface is intended to
 * serve as a example MBean interface to aid developers and customizers author
 * their own MBean interfaces and implement these.
 *
 * Even though source code for this class is provided, compilation of this
 * source under the existing class name (i.e. in place of the class delivered)
 * is still not supported.
 */

/* For each of our MBean interface source files, xxxMBean.java, automated
 * tooling is used to produce a corresponding xxxMBeanResource.rbInfo file.
 * Thus in the case of this MBean inteface, ProcessCpuTimeMonitorMBeanResource.rbInfo
 * is produced by this tooling.
 * 
 * To [re]generate the rbInfo for an MBean interface source file (upon creating
 * a new MBean interface file or making changes), invoke the following command
 * from the source root directory in a Windchill shell:
 *   javac -proc:only -processor wt.annotations.processors.MBeanInterfaceProcessor <path of source file>
 * For instance, for this MBean interface (on Windows) one would invoke
 *   javac -proc:only -processor wt.annotations.processors.MBeanInterfaceProcessor wt\jmx\core\mbeans\ProcessCpuTimeMonitorMBean.java
 *
 * The tool garners descriptions and operation parameter names from the formal
 * parameter names and Javadoc comments in the MBean interface source file and
 * expresses these in the rbInfo.  At runtime, the wt.jmx.core.StandardMBean
 * reads these rbInfos and incorporates this additional metadata into the
 * MBeans' MBeanInfo.  This allows for localization, though no localized
 * versions of these particular bundles are currently produced for delivery
 * with the product.  Re-use of formal parameters and Javadoc comment information
 * avoids redundant documentation efforts and ensures consistency between
 * Javadoc documentation and MBean documentation at runtime.
 */

import javax.management.MBeanOperationInfo;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.OpenDataException;

import wt.jmx.annotations.MBeanOperationImpact;


/** Monitors (average) percentage CPU used by process and CPU times accumulated
 *  by each thread, over a given time interval and since a baseline time.
 *
 *    <BR><BR><B>Supported API: </B>true
 *    <BR><BR><B>Extendable: </B>true
 */
public interface  ProcessCpuTimeMonitorMBean
  extends SelfEmailingMBean
{
  /** Type of notification produced when ProcessPercentCpuThreshold is initially exceeded
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  public static String  PROCESS_PERCENT_CPU_TE_INITIAL_NOTIF_TYPE = ProcessCpuTimeMonitorMBean.class.getName() + ".processPercentCpuThreshold.exceeded.initial";

  /** Type of notification produced when ProcessPercentCpuThreshold is still exceeded
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  public static String  PROCESS_PERCENT_CPU_TE_STILL_NOTIF_TYPE = ProcessCpuTimeMonitorMBean.class.getName() + ".processPercentCpuThreshold.exceeded.still";
  
  /** Type of notification produced when ProcessPercentCpuThreshold is no longer exceeded
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  public static String  PROCESS_PERCENT_CPU_TE_NO_LONGER_NOTIF_TYPE = ProcessCpuTimeMonitorMBean.class.getName() + ".processPercentCpuThreshold.exceeded.nolonger";
  
  /** Average percentage of CPU used by process since its start
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  public double  getAverageProcessPercentCpu();
  
  /** Threshold percentage CPU used by process
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  public double  getProcessPercentCpuThreshold();
  
  /** Threshold percentage CPU used by process
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  public void  setProcessPercentCpuThreshold( double processPercentCpuThreshold );
  
  /** Duration (in seconds) of time interval over which CPU usage is averaged
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  public int  getRecentIntervalSeconds();
  
  /** Duration (in seconds) of time interval over which CPU usage is averaged
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  public void  setRecentIntervalSeconds( int recentIntervalSeconds );

  /** Data on CPU usage during recent time interval
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  public CompositeData  getRecentCpuData()
    throws OpenDataException;
  
  /** Data on CPU usage during time interval since baseline CPU time statistics
   *  were established (by application startup or the resetBaselineStatistics()
   *  operation whichever was more recent)
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  public CompositeData  getBaselineCpuData()
    throws OpenDataException;
  
  /** Resets CPU time baseline statistics
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  /* Next line is an annotation that tells wt.jmx.core.StandardMBean that this
   * operation has an "action", i.e. "write", behavior, or "impact" in JMX
   * terminology.  wt.jmx.core.StandardMBean will ensure that this information
   * is included in this MBean's MBeanInfo when it is provided.  See
   * {@link javax.management.MBeanOperationInfo} for other possible values.
   * Specifying the impact in this way is necessary for MBean operations, as
   * the impact of MBean attribute getters and setters is already clear to JMX.
   */
  @MBeanOperationImpact( MBeanOperationInfo.ACTION )
  public void  resetBaselineStatistics();
}
