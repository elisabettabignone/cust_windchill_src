/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.jmx.core.mbeans;


import java.lang.management.ThreadInfo;
import java.util.Date;
import java.util.Map;
import java.util.ResourceBundle;
import javax.management.ListenerNotFoundException;
import javax.management.MBeanInfo;
import javax.management.MBeanNotificationInfo;
import javax.management.NotCompliantMBeanException;
import javax.management.Notification;
import javax.management.NotificationBroadcasterSupport;
import javax.management.NotificationEmitter;
import javax.management.NotificationFilter;
import javax.management.NotificationListener;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenDataException;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.TabularType;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import wt.jmx.core.MBeanRegistry;
import wt.jmx.core.MBeanUtilities;
import wt.jmx.core.PeriodicTaskSupport;
import wt.jmx.core.SelfAwareMBean;
import wt.log4j.LogR;


/* In addition to providing useful functionality, this class is intended to
 * serve as a example SelfAwareMBean implementation to aid both developers and
 * customizers in the implementation of other SelfAwareMBeans.  This class also
 * demonstrates the use of JMX notifications, PeriodicTaskSupport, and JMX
 * "open" data types.
 *
 * Even though source code for this class is provided, compilation of this
 * source under the existing class name (i.e. in place of the class delivered)
 * is still not supported.  Additionally, details in the actual implementation
 * of this class may change over time so one should not rely on such
 * implementation details.
 */


/** Implementation of ProcessCpuTimeMonitorMBean.
 *  <p>
 *  Direct usage not supported as ProcessCpuTimeMonitorMBean interface and
 *  specification of this class name to the MBeanLoader MBean should suffice.
 *
 *    <BR><BR><B>Supported API: </B>false
 *    <BR><BR><B>Extendable: </B>false
 */
// Class must be public to be persisted and restored by MBeanLoader
// Class could have been non-final; it is documented as non-extendable, though, so we have compiler enforce this in this case
public final class  ProcessCpuTimeMonitor
  extends SelfAwareMBean
  implements ProcessCpuTimeMonitorMBean, NotificationEmitter
{
  //
  // Particularly relevant static fields (others declared near end of class)
  //

  /** Returned by getObjectNameSuffix()
   */
  public static final String[][]  OBJECT_NAME_SUFFIX =
  {
    { MBeanRegistry.SUB_SYSTEM_PROP_KEY, MBeanRegistry.MONITORS_SUB_SYSTEM_PROP },
    { MBeanRegistry.MONITOR_TYPE_PROP_KEY, "ProcessCpuTime" }
  };

  /** Provides localization only to this JVMs default locale as JMX remoting
   *  provides no standard for clients to provide client locale.
   */
  private static final ResourceBundle  RESOURCE = ResourceBundle.getBundle( mbeansResource.class.getName() );

  /** Number of nanoseconds per second
   */
  private static final double  NANOS_PER_SECOND = 1.0e9;

  /** Notification info returned by getNotificationInfo()
   */
  private static final MBeanNotificationInfo  NOTIFICATION_INFO[] =
  {
    new MBeanNotificationInfo( new String[]
                               {
                                 PROCESS_PERCENT_CPU_TE_INITIAL_NOTIF_TYPE,
                                 PROCESS_PERCENT_CPU_TE_STILL_NOTIF_TYPE,
                                 PROCESS_PERCENT_CPU_TE_NO_LONGER_NOTIF_TYPE
                               },
                               Notification.class.getName(),
                               RESOURCE.getString( mbeansResource.PROCESS_PERCENT_CPU_TE_NOTIF_DESCR ) )
  };

  //
  // Constructors
  //

  /** To add your own MBean to the persisted MBean configuration, you start by
   * making your class public and providing either a public no-arg constructor
   * or additional clues to the JavaBean XML persistence mechanisms as per
   * <a href="http://java.sun.com/products/jfc/tsc/articles/persistence4/">
   * http://java.sun.com/products/jfc/tsc/articles/persistence4/</a>.  [The
   * former approach is that supported by MBeanLoaderMBean for "top-level"
   * MBeans as described below.]
   * <p>
   * Once you have such an MBean class, you can instantiate it in a desired
   * server tier (servlet engine, method server, or server manager) via a JMX
   * console UI or programmatically by calling MBeanLoaderMBean's addObject()
   * operation and providing the name of your MBean implementation class as the
   * argument.  [The MBeanLoaderMBean will appear as "Loader" in JMX console
   * trees within the "Windchill" domain.]  That will add an instance of your
   * MBean to the list of MBeans directly controlled by the given loader, but
   * will not persist this instance to the configuration files for subsequent
   * restarts, etc.  To do this, invoke the MBeanLoaderMBean's save() operation.
   * [The save() operation also serves to capture changes you make to your
   * MBean's attributes.]
   * <p>
   * MBeans directly controlled by a loader can introduce their own "child"
   * MBeans which they control.  These are generally nested under their parent
   * in the JMX console tree for clarity.  These are (or are not) persisted
   * with their parent according to the
   * <a href="http://java.sun.com/products/jfc/tsc/articles/persistence4/">
   * JavaBean XML persistence mechanism</a>.
   */
  public  ProcessCpuTimeMonitor()
    throws NotCompliantMBeanException
  {
    super( ProcessCpuTimeMonitorMBean.class );
  }

  //
  // StandardMBean overrides
  //

  /** Overriden to augment the MBeanInfo produced by StandardMBean to provide
   *  more precise information on the types of TabularData data returned by
   *  MBean interface methods than that which can be provided by reflection
   *  on the MBean interface.
   *  <p>
   *  Note that such precision is <i>not</i> required and this override is
   *  done here in large part to show how MBeanInfo adjustment or augmentation
   *  can be achieved by overriding this StandardMBean method.
   */
  @Override  // tell compiler to throw an error if this is not a method override as intended
  protected MBeanInfo  createMBeanInfo( final MBeanInfo baseClassMBeanInfo )
  {
    final Map<String,OpenType>  attrNameToTypeMap = MBeanUtilities.newHashMap( 2, 0.75f );
    attrNameToTypeMap.put( "RecentCpuData", OVERALL_CPU_DATA_TYPE );  // See open data types below...
    attrNameToTypeMap.put( "BaselineCpuData", OVERALL_CPU_DATA_TYPE );
    return ( MBeanUtilities.fixOpenMBeanAttrInfo( super.createMBeanInfo( baseClassMBeanInfo ), attrNameToTypeMap ) );
  }

  //
  // SelfAwareMBean implementation
  //

  // Implementation of abstract SelfAwareMBean method.
  // Specifies naming of this MBean (and thus position/naming in the jconsole MBean tree)
  @Override  // tell compiler to throw an error if this is not a method override as intended
  public String[][]  getObjectNameSuffix()
  {
    return ( OBJECT_NAME_SUFFIX );
  }

  //
  // ProcessCpuTimeMonitorMBean implementation
  //

  // Implementation of ProcessCpuTimeMonitorMBean interface method
  @Override
  public double  getAverageProcessPercentCpu()
  {
    return ( getPercentCpuUsage( MBeanUtilities.getProcessCpuTime(), System.nanoTime() - VM_START_TIME_IN_NANOS ) );
  }

  // Implementation of ProcessCpuTimeMonitorMBean interface method
  @Override
  public double  getProcessPercentCpuThreshold()
  {
    return ( processPercentCpuThreshold );
  }

  // Implementation of ProcessCpuTimeMonitorMBean interface method
  @Override
  public void  setProcessPercentCpuThreshold( final double processPercentCpuThreshold )
  {
    this.processPercentCpuThreshold = processPercentCpuThreshold;
  }

  // Implementation of ProcessCpuTimeMonitorMBean interface method
  @Override
  public int  getRecentIntervalSeconds()
  {
    return ( backgroundTask.getTaskIntervalSeconds() );
  }

  // Implementation of ProcessCpuTimeMonitorMBean interface method
  @Override
  public void  setRecentIntervalSeconds( final int recentIntervalSeconds )
  {
    backgroundTask.setTaskIntervalSeconds( recentIntervalSeconds );
  }

  // Implementation of ProcessCpuTimeMonitorMBean interface method
  /* A single, composite attribute and volatile field containing an immutable
   * data structure are used to ensure consistency amongst constituent data,
   * i.e. so that there is no chance to that one receives part of this data
   * from prior to a statistics update and part of it from afterwards.
   */
  @Override
  public CompositeData  getRecentCpuData()
    throws OpenDataException
  {
    final IntervalData  recentData = this.recentData;
    if ( recentData != null )
      /* Return data summarizing period from start of recentData interval to
       * current moment as recentData interval may be up to recent interval
       * seconds ago by now.  The downsides as compared to just using
       * recentData.getData() are that this API now has to do more work and the
       * interval covered by the results may cover up to two recent intervals
       * worth of time.
       */
      return ( BaselineData.getData( recentData.startData, new BaselineData( false ) ) );
    return ( getBaselineCpuData() );
  }

  // Implementation of ProcessCpuTimeMonitorMBean interface method
  /* A single, composite attribute and volatile field containing an immutable
   * data structure are used to ensure consistency amongst constituent data,
   * i.e. so that there is no chance to that one receives part of this data
   * from prior to a statistics update and part of it from afterwards.
   */
  @Override
  public CompositeData  getBaselineCpuData()
    throws OpenDataException
  {
    return ( baselineData.getData() );
  }

  // Implementation of ProcessCpuTimeMonitorMBean interface method
  @Override
  public void  resetBaselineStatistics()
  {
    baselineData = new BaselineData( false );
  }

  /* NOTE: The parent classes already fully implement SelfEmailingMBean (the
   * parent interface of ProcessCpuTimeMonitorMBean).
   *
   * In other words, SelfEmailingMBean functionality is available for free by
   * declaring it as a parent of your MBean interface *IF* you subclass
   * SelfAwareMBean or subclass wt.jmx.core.StandardMBean and provide a
   * 'public ObjectName getObjectName()' method that returns the ObjectName of
   * your MBean.
   */

  //
  // NotificationEmitter implementation
  //

  // Implementation of NotificationEmitter interface method
  @Override
  public MBeanNotificationInfo[]  getNotificationInfo()
  {
    return ( NOTIFICATION_INFO );
  }

  // Implementation of NotificationEmitter interface method
  @Override
  public void  addNotificationListener( final NotificationListener listener, final NotificationFilter filter, final Object handback )
    throws IllegalArgumentException
  {
    notificationEmitter.addNotificationListener( listener, filter, handback );
  }

  // Implementation of NotificationEmitter interface method
  @Override
  public void  removeNotificationListener( final NotificationListener listener )
    throws ListenerNotFoundException
  {
    notificationEmitter.removeNotificationListener( listener );
  }

  // Implementation of NotificationEmitter interface method
  @Override
  public void  removeNotificationListener( final NotificationListener listener, final NotificationFilter filter, final Object handback )
    throws ListenerNotFoundException
  {
    notificationEmitter.removeNotificationListener( listener, filter, handback );
  }

  //
  // SelfAwareMBean method overrides
  //

  // Override SelfAwareMBean.onStart() to call backgroundTask.start()
  @Override  // tell compiler to throw an error if this is not a method override as intended
  protected void  onStart()  // note that start() is synchronized
  {
    super.onStart();  // be sure to call this first thing in onStart() overrides
    backgroundTask.start();  // start background task
  }

  // Override SelfAwareMBean.onStop() to call backgroundTask.stop()
  @Override  // tell compiler to throw an error if this is not a method override as intended
  protected void  onStop()  // note that stop() is synchronized
  {
    backgroundTask.stop();  // stop background task
    super.onStop();  // be sure to call this last thing from onStop() overrides
  }

  //
  // Private implementation stuff
  //

  /** Runnable class to be called periodically to track percentage CPU usage by
   *  this process.
   */
  private final class  BackgroundTask
    implements Runnable
  {
    BackgroundTask()
    {
      // take initial clock and CPU time measurements for use in next computation
      lastBaselineData = new BaselineData( false );
    }

    // Implementation of Runnable interface method.
    // Will be called periodically by wt.jmx.core.PeriodicTaskSupport.  See below.
    @Override
    public void  run()
    {
      // take current clock and CPU time measurements
      final BaselineData  currentData = new BaselineData( false );

      // capture all recent data
      final IntervalData  recentData = new IntervalData( lastBaselineData, currentData );
      ProcessCpuTimeMonitor.this.recentData = recentData;  // copy local variable into field

      // determine whether to send a notification and, if so, which to send; also log recentProcessPercentCpu with appropriate log level
      final double  recentProcessPercentCpu = recentData.getProcessPercentCpuUsage();
      final double  processPercentCpuThreshold = ProcessCpuTimeMonitor.this.processPercentCpuThreshold;  // copy field into local variable
      final boolean  lastWasOverThreshold = ProcessCpuTimeMonitor.this.lastWasOverThreshold;  // copy field into local variable
      final boolean  thresholdEnabled = ( processPercentCpuThreshold > 0.0 );
      final boolean  overThreshold = ( thresholdEnabled && ( recentProcessPercentCpu > processPercentCpuThreshold ) );
      logger.log( overThreshold ? Level.WARN : Level.INFO, "recentProcessPercentCpu=" + recentProcessPercentCpu );
      if ( overThreshold || lastWasOverThreshold )
      {
        final String  notifType;
        final String  notifMsgFormat;
        if ( overThreshold )
        {
          if ( !lastWasOverThreshold )
          {
            notifType = PROCESS_PERCENT_CPU_TE_INITIAL_NOTIF_TYPE;
            notifMsgFormat = mbeansResource.PROCESS_PERCENT_CPU_TE_INITIAL_NOTIF_MSG;
            ProcessCpuTimeMonitor.this.lastWasOverThreshold = true;
          }
          else
          {
            notifType = PROCESS_PERCENT_CPU_TE_STILL_NOTIF_TYPE;
            notifMsgFormat = mbeansResource.PROCESS_PERCENT_CPU_TE_STILL_NOTIF_MSG;
          }
        }
        else /* if ( !overThreshold ) */
        {
          notifType = PROCESS_PERCENT_CPU_TE_NO_LONGER_NOTIF_TYPE;
          notifMsgFormat = mbeansResource.PROCESS_PERCENT_CPU_TE_NO_LONGER_NOTIF_MSG;
          ProcessCpuTimeMonitor.this.lastWasOverThreshold = false;
        }

        // create and send notification
        final Notification  notification = new Notification( notifType, getObjectName(), MBeanUtilities.getNotificationSequenceNumber(),
                                                             MBeanUtilities.formatMessage( RESOURCE.getString( notifMsgFormat ),
                                                                                           recentProcessPercentCpu ) );
        notification.setUserData( recentProcessPercentCpu );  // use CompositeData or TabularData here if sending more than single primitive, String, Date, or ObjectName
        if ( logger.isDebugEnabled() )
          logger.debug( "Sending notification of type: " + notifType );
        notificationEmitter.sendNotification( notification );
      }

      // retain these measurements to use in next computation
      lastBaselineData = currentData;
    }

    private BaselineData  lastBaselineData;
  }

  /** Aggregation of all baseline statistics data.
   */
  private static final class  BaselineData
  {
    /** Create baseline data.
     *   @param initialStartupData whether this data should reflect initial startup (false implies current data state instead)
     */
    BaselineData( final boolean initialStartupData )
    {
      if ( initialStartupData )
      {
        milliTime = MBeanUtilities.VM_START_TIME;
        threadDataMap = null;
        nanoTime = VM_START_TIME_IN_NANOS;
        processCpuNanos = 0L;
      }
      else
      {
        milliTime = System.currentTimeMillis();
        threadDataMap = getThreadDataMap();
        nanoTime = System.nanoTime();
        processCpuNanos = MBeanUtilities.getProcessCpuTime();
      }
    }

    /** Computes baseline statistics from current and baseline data.
     */
    CompositeData  getData()
      throws OpenDataException
    {
      return ( getData( this, new BaselineData( false ) ) );
    }

    /** Given two BaselineData objects find percentage of CPU used by this process
     *  during this time interval.
     *   @param startData data for start of time interval
     *   @param endData data for end of time interval
     */
    // for use by IntervalData.getProcessPercentCpuUsage()
    static double  getProcessPercentCpuUsage( final BaselineData startData, final BaselineData endData )
    {
      return ( getPercentCpuUsage( endData.processCpuNanos - startData.processCpuNanos,
                                   endData.nanoTime - startData.nanoTime ) );
    }

    /** Given two BaselineData objects return CompositeData of type
     *  OVERALL_CPU_DATA_TYPE summarizing CPU usage during this time interval.
     *   @param startData data for start of time interval
     *   @param endData data for end of time interval
     */
    static CompositeData  getData( final BaselineData startData, final BaselineData endData )
      throws OpenDataException
    {
      final long  startMillis = startData.milliTime;
      final long  endMillis = endData.milliTime;
      final long  startNanos = startData.nanoTime;
      final long  endNanos = endData.nanoTime;
      final long  elapsedNanos = endNanos - startNanos;
      final long  elapsedProcessCpuNanos = endData.processCpuNanos - startData.processCpuNanos;
      final long  totalTrackedBytesAllocatedHolder[] = { 0L };
      final long  bytesAllocatedData[] = getBytesAllocatedData( endData.threadDataMap, startData.threadDataMap,
                                                                totalTrackedBytesAllocatedHolder );
      final long  totalTrackedBytesAllocated = totalTrackedBytesAllocatedHolder[0];
      return ( getOverallCpuOpenData( new Date( startMillis ),
                                      new Date( endMillis ),
                                      ( (double) elapsedNanos ) / NANOS_PER_SECOND,
                                      getPercentCpuUsage( elapsedProcessCpuNanos, elapsedNanos ),
                                      totalTrackedBytesAllocated,
                                      getThreadOpenData( endData.threadDataMap, startData.threadDataMap, elapsedProcessCpuNanos,
                                                         bytesAllocatedData, totalTrackedBytesAllocated ) ) );
    }

    private final long  milliTime;
    private final long  nanoTime;
    private final long  processCpuNanos;
    private final Map<Long,ThreadData>  threadDataMap;
  }

  /** Aggregation of all interval statistics data.
   */
  private static final class  IntervalData
  {
    IntervalData( final BaselineData startData, final BaselineData endData )
    {
      this.startData = startData;
      this.endData = endData;
    }

    /** Return percentage of CPU used by process during interval.
     */
    // for use by BackgroundTask.run
    double  getProcessPercentCpuUsage()
    {
      return ( BaselineData.getProcessPercentCpuUsage( startData, endData ) );
    }

    /** Performs lazy computation/construction of CompositeData.
     */
    CompositeData  getData()
      throws OpenDataException
    {
      CompositeData  overallCpuData = this.overallCpuData;
      if ( overallCpuData != null )
        return ( overallCpuData );
      overallCpuData = BaselineData.getData( startData, endData );
      this.overallCpuData = overallCpuData;  // copy local variable into field
      return ( overallCpuData );
    }

    private final BaselineData  startData;
    private final BaselineData  endData;

    private volatile CompositeData  overallCpuData;  // lazily computed and then cached (see getData())
  }

  /** Compute percentage of CPU used.
   *   @param cpuNanos nanoseconds of CPU time consumed during time interval
   *   @param elapsedNanos length of time interval in nanoseconds
   */
  private static double  getPercentCpuUsage( final long cpuNanos, final long elapsedNanos )
  {
    return ( 100.0 * ( (double) cpuNanos / (double) elapsedNanos ) / (double) MBeanUtilities.getAvailableProcessors() );
  }

  /** Returns a map from thread ids to data of interest for each thread.
   */
  private static Map<Long,ThreadData>  getThreadDataMap()
  {
    final boolean  threadCpuTimeMonitoringEnabled = MBeanUtilities.NON_CURRENT_THREAD_CPU_TIME_SUPPORTED &&
                                                    MBeanUtilities.isThreadCpuTimeEnabled();

    // collect all active thread ids
    final long  threadIds[] = MBeanUtilities.THREAD_MBEAN.getAllThreadIds();

    // collect all thread infos, CPU/user times, and byte allocations
    final ThreadInfo  threadInfos[] = MBeanUtilities.THREAD_MBEAN.getThreadInfo( threadIds );
    final long  threadUserTimes[];
    final long  threadCpuTimes[];
    if ( threadCpuTimeMonitoringEnabled )
    {
      threadUserTimes = MBeanUtilities.getThreadUserTimes( threadIds );
      threadCpuTimes = MBeanUtilities.getThreadCpuTimes( threadIds );
    }
    else
    {
      threadUserTimes = null;
      threadCpuTimes = null;
    }
    final long  threadAllocatedBytes[] = MBeanUtilities.getThreadAllocatedBytes( threadIds );

    // collect information on current servlet and method contexts
    final Map<Long,Object>  currentRequestIdMap = MBeanUtilities.ServletRequestMonitorAccessor.instance.getCurrentRequestIdMap();
    final Map<Long,Object>  currentContextIdMap = MBeanUtilities.MethodContextAccessor.instance.getCurrentContextIdMap();

    // produce map from thread ids to the information we collected on them
    final int  nThreads = threadIds.length;
    final Map<Long,ThreadData>  threadDataMap = MBeanUtilities.newHashMap( nThreads, 0.75f );
    for ( int ii = 0; ii < nThreads; ++ii )
    {
      final ThreadInfo  threadInfo = threadInfos[ii];
      if ( threadInfo == null )
        continue;  // thread completed and exited before we could get any data
      final long  threadId = threadIds[ii];
      final long  threadCpuTime;
      final long  threadUserTime;
      if ( threadCpuTimeMonitoringEnabled )
      {
        threadCpuTime = threadCpuTimes[ii];
        threadUserTime = threadUserTimes[ii];
      }
      else
      {
        threadCpuTime = -1L;
        threadUserTime = -1L;
      }
      final long  threadBytes = ( ( threadAllocatedBytes != null ) ? threadAllocatedBytes[ii] : -1L );
      final Object  servletRequestId = ( ( currentRequestIdMap != null ) ? currentRequestIdMap.get( threadId ) : null );
      final Object  methodContextId = ( ( currentContextIdMap != null ) ? currentContextIdMap.get( threadId ) : null );
      threadDataMap.put( threadId, new ThreadData( threadInfo, threadCpuTime, threadUserTime, threadBytes, servletRequestId, methodContextId ) );
    }
    return ( threadDataMap );
  }

  private static final class  ThreadData
  {
    ThreadData( final ThreadInfo threadInfo, final long cpuNanos, final long userNanos, final long bytesAllocated,
                final Object servletRequestId, final Object methodContextId )
    {
      threadName = threadInfo.getThreadName();
      blockedCount = threadInfo.getBlockedCount();
      blockedMillis = threadInfo.getBlockedTime();
      waitedCount = threadInfo.getWaitedCount();
      waitedMillis = threadInfo.getWaitedTime();
      this.cpuNanos = cpuNanos;
      this.userNanos = userNanos;
      this.bytesAllocated = bytesAllocated;
      this.servletRequestId = servletRequestId;
      this.methodContextId = methodContextId;
    }

    final String  threadName;
    final long  blockedCount;
    final long  blockedMillis;
    final long  waitedCount;
    final long  waitedMillis;
    final long  cpuNanos;
    final long  userNanos;
    final long  bytesAllocated;
    final Object  servletRequestId;
    final Object  methodContextId;
  }

  /** Given thread data maps collects per-thread byte allocation data where available and returns the total as well.
   *   @param threadDataMap2 thread data map for second point in time
   *   @param threadDataMap1 thread data map for first point in time
   *   @param totalTrackedBytesAllocatedHolder holder for returning total tracked bytes allocated by these threads
   */
  private static long[]  getBytesAllocatedData( final Map<Long,ThreadData> threadDataMap2, final Map<Long,ThreadData> threadDataMap1,
                                                final long totalTrackedBytesAllocatedHolder[] )
  {
    if ( threadDataMap2 == null )
      return ( null );

    // collect byte allocation data
    int  ii = 0;
    long  totalTrackedBytesAllocated = 0L;
    final int  nThreads = threadDataMap2.size();
    final long  bytesAllocatedData[] = new long[nThreads];
    for ( Map.Entry<Long,ThreadData> threadDataMap2Entry : threadDataMap2.entrySet() )
    {
      final Long  threadIdObj = threadDataMap2Entry.getKey();
      final ThreadData  data = threadDataMap2Entry.getValue();
      final ThreadData  baselineData = ( ( threadDataMap1 != null ) ? threadDataMap1.get( threadIdObj ) : null );
      final long  bytesAllocated = ( ( baselineData != null )
                                          ? ( ( ( data.bytesAllocated >= 0L ) && ( baselineData.bytesAllocated >= 0L ) )
                                                    ? data.bytesAllocated - baselineData.bytesAllocated
                                                    : -1L )
                                          : data.bytesAllocated );
      if ( bytesAllocated > 0L )
        totalTrackedBytesAllocated += bytesAllocated;
      bytesAllocatedData[ii] = bytesAllocated;
      ++ii;
    }
    if ( totalTrackedBytesAllocatedHolder != null )
      totalTrackedBytesAllocatedHolder[0] = totalTrackedBytesAllocated;
    return ( bytesAllocatedData );
  }

  /** Produces TabularData summarizing the thread data for a given time interval.
   *   @param threadDataMap2 thread data map for second point in time
   *   @param threadDataMap1 thread data map for first point in time
   *   @param elapsedProcessCpuNanos overall process CPU usage (in nanoseconds)
   *   @param bytesAllocatedData bytes allocated by each these thread; -1 signifying unknown
   *   @param totalTrackedBytesAllocated total tracked bytes allocated by these threads
   */
  private static TabularData  getThreadOpenData( final Map<Long,ThreadData> threadDataMap2, final Map<Long,ThreadData> threadDataMap1,
                                                 final long elapsedProcessCpuNanos, final long bytesAllocatedData[],
                                                 final long totalTrackedBytesAllocated )
    throws OpenDataException
  {
    if ( threadDataMap2 == null )
      return ( null );

    // divide by 100 once here rather than multiply by 100 repeatedly later...
    final double  totalCpuTimeDiv100 = ( (double) elapsedProcessCpuNanos ) / 100.0;
    final double  totalTrackedBytesAllocatedDiv100 = ( (double) totalTrackedBytesAllocated ) / 100.0;

    // create tabular data describing thread CPU times accumulated and byte allocation data for each thread
    /* [CompositeTypes and TabularTypes are a pain to define (see below), but
     * are easy to use thereafter.  They allow JMX management consoles to
     * display complex data sets and allow us to provide descriptions of all
     * types and fields therein, though most consoles currently do not display
     * most of these.]
     */
    int  ii = 0;
    final TabularData  threadCpuTimeData = MBeanUtilities.newTabularDataSupport( THREAD_CPU_TIME_TYPE, threadDataMap2.size(), 0.75f );
    for ( Map.Entry<Long,ThreadData> threadDataMap2Entry : threadDataMap2.entrySet() )
    {
      final Long  threadIdObj = threadDataMap2Entry.getKey();
      final ThreadData  data = threadDataMap2Entry.getValue();
      final ThreadData  baselineData = ( ( threadDataMap1 != null ) ? threadDataMap1.get( threadIdObj ) : null );
      final long  blockedCount;
      final long  blockedMillis;
      final long  waitedCount;
      final long  waitedMillis;
      final double  cpuTimeNanos;
      final double  userTimeNanos;
      final Object  servletRequestId;
      final Object  methodContextId;
      if ( baselineData != null )
      {
        blockedCount = ( ( ( data.blockedCount >= 0L ) && ( baselineData.blockedCount >= 0L ) ) ? data.blockedCount - baselineData.blockedCount : -1L );
        blockedMillis = ( ( ( data.blockedMillis >= 0L ) && ( baselineData.blockedMillis >= 0L ) ) ? data.blockedMillis - baselineData.blockedMillis : -1L );
        waitedCount = ( ( ( data.waitedCount >= 0L ) && ( baselineData.waitedCount >= 0L ) ) ? data.waitedCount - baselineData.waitedCount : -1L );
        waitedMillis = ( ( ( data.waitedMillis >= 0L ) && ( baselineData.waitedMillis >= 0L ) ) ? data.waitedMillis - baselineData.waitedMillis : -1L );
        cpuTimeNanos = ( ( ( data.cpuNanos >= 0L ) && ( baselineData.cpuNanos >= 0L ) ) ? data.cpuNanos - baselineData.cpuNanos : -1L );
        userTimeNanos = ( ( ( data.userNanos >= 0L ) && ( baselineData.userNanos >= 0L ) ) ? data.userNanos - baselineData.userNanos : -1L );
        servletRequestId = ( equals( data.servletRequestId, baselineData.servletRequestId ) ? data.servletRequestId : null );
        methodContextId = ( equals( data.methodContextId, baselineData.methodContextId ) ? data.methodContextId : null );
      }
      else
      {
        blockedCount = data.blockedCount;
        blockedMillis = data.blockedMillis;
        waitedCount = data.waitedCount;
        waitedMillis = data.waitedMillis;
        cpuTimeNanos = data.cpuNanos;
        userTimeNanos = data.userNanos;
        servletRequestId = data.servletRequestId;
        methodContextId = data.methodContextId;
      }
      final double  blockedSeconds = ( ( blockedMillis >= 0L ) ? blockedMillis / 1000.0 : -1.0 );
      final double  waitedSeconds = ( ( waitedMillis >= 0L ) ? waitedMillis / 1000.0 : -1.0 );
      final double  percentCpuTime = ( ( ( cpuTimeNanos >= 0L ) && ( totalCpuTimeDiv100 > 0.0 ) )
                                             ? cpuTimeNanos / totalCpuTimeDiv100
                                             : 0.0 );
      final double  cpuTimeInSeconds = ( ( cpuTimeNanos >= 0L ) ? cpuTimeNanos / NANOS_PER_SECOND : -1.0 );  // times are in nanoseconds
      final double  userTimeInSeconds = ( ( userTimeNanos >= 0L ) ? userTimeNanos / NANOS_PER_SECOND : -1.0 );
      final long  bytesAllocated = bytesAllocatedData[ii];
      final double  percentTrackedBytesAllocated = ( ( ( bytesAllocated >= 0L ) && ( totalTrackedBytesAllocatedDiv100 > 0.0 ) )
                                                            ? bytesAllocated / totalTrackedBytesAllocatedDiv100
                                                            : 0.0 );
      threadCpuTimeData.put( new CompositeDataSupport( THREAD_CPU_TIME_ROW_TYPE, THREAD_CPU_TIME_ROW_TYPE_ITEMS,
                                                       new Object[]
                                                       {
                                                         threadIdObj,
                                                         data.threadName,
                                                         blockedCount,
                                                         blockedSeconds,
                                                         waitedCount,
                                                         waitedSeconds,
                                                         percentCpuTime,
                                                         cpuTimeInSeconds,
                                                         userTimeInSeconds,
                                                         bytesAllocated,
                                                         percentTrackedBytesAllocated,
                                                         ( servletRequestId != null ) ? servletRequestId.toString() : null,
                                                         ( methodContextId != null ) ? methodContextId.toString() : null
                                                       } ) );
      ++ii;
    }
    return ( threadCpuTimeData );
  }

  /** Check two objects for equality without triggering a NullPointerException if one or both are null.
   */
  private static boolean  equals( final Object o1, final Object o2 )
  {
    if ( o1 == null )
      return ( o2 == null );
    return ( o1.equals( o2 ) );
  }

  /** Simple utility to create a CompositeData object containing various CPU data items.
   *   @param startTime start of measurement interval
   *   @param endTime end of measurement interval
   *   @param elapsedSeconds elapsed time of measurement interval in seconds
   *   @param processPercentCpu percentage of overall CPU time consumed by process during measurement interval
   *   @param totalTrackedBytesAllocated total tracked bytes allocated by these threads
   *   @param threadCpuTimeData thread CPU time data as per getThreadCpuTimeData()
   */
  private static CompositeData  getOverallCpuOpenData( final Date startTime, final Date endTime, final double elapsedSeconds,
                                                       final double processPercentCpu, final long totalTrackedBytesAllocated,
                                                       final TabularData threadCpuTimeData )
    throws OpenDataException
  {
    // as per comment above, CompositeData is easy to create once you've defined the types
    return ( new CompositeDataSupport( OVERALL_CPU_DATA_TYPE, OVERALL_CPU_DATA_TYPE_ITEMS,
                                       new Object[] { startTime, endTime, elapsedSeconds, processPercentCpu, totalTrackedBytesAllocated,
                                                      threadCpuTimeData } ) );
  }

  //
  // Static declarations for JMX open data types
  //   [Note that such declarations become much less necessary with MXBeans in Java 6, but we're supporting Java 5.]
  //

  /** Name of thread id item of THREAD_CPU_TIME_ROW_TYPE
   */
  private static final String  THREAD_CPU_TIME_THREAD_ID_ITEM = "threadId";

  /** All item names of THREAD_CPU_TIME_ROW_TYPE; both it and
   *  getThreadCpuTimeData() depend on the order of this array.
   */
  private static final String  THREAD_CPU_TIME_ROW_TYPE_ITEMS[] =
  {
    THREAD_CPU_TIME_THREAD_ID_ITEM,
    "name",
    "blockedCount",
    "blockedSeconds",
    "waitedCount",
    "waitedSeconds",
    "percentCpuTime",
    "cpuTimeInSeconds",
    "userTimeInSeconds",
    "bytesAllocated",
    "percentTrackedBytesAllocated",
    "servletRequestId",
    "methodContextId"
  };

  /** Row type of THREAD_CPU_TIME_DATA_TYPE
   */
  private static final CompositeType  THREAD_CPU_TIME_ROW_TYPE;
  static
  {
    try
    {
      THREAD_CPU_TIME_ROW_TYPE = new CompositeType( ProcessCpuTimeMonitor.class.getName() + ".threadDataRowType",
                                                    RESOURCE.getString( mbeansResource.THREAD_CPU_TIME_ROW_TYPE_DESCR ),
                                                    THREAD_CPU_TIME_ROW_TYPE_ITEMS,
                                                    new String[]
                                                    {
                                                      RESOURCE.getString( mbeansResource.THREAD_ID_ITEM_DESCR ),
                                                      RESOURCE.getString( mbeansResource.THREAD_NAME_ITEM_DESCR ),
                                                      RESOURCE.getString( mbeansResource.THREAD_BLOCKED_COUNT_ITEM_DESCR ),
                                                      RESOURCE.getString( mbeansResource.THREAD_BLOCKED_SECONDS_ITEM_DESCR ),
                                                      RESOURCE.getString( mbeansResource.THREAD_WAITED_COUNT_ITEM_DESCR ),
                                                      RESOURCE.getString( mbeansResource.THREAD_WAITED_SECONDS_ITEM_DESCR ),
                                                      RESOURCE.getString( mbeansResource.THREAD_PERC_CPU_TIME_ITEM_DESCR ),
                                                      RESOURCE.getString( mbeansResource.THREAD_CPU_TIME_ITEM_DESCR ),
                                                      RESOURCE.getString( mbeansResource.THREAD_USER_TIME_ITEM_DESCR ),
                                                      RESOURCE.getString( mbeansResource.THREAD_BYTES_ALLOCATED_DESCR ),
                                                      RESOURCE.getString( mbeansResource.THREAD_PERC_TRACKED_BYTES_ALLOCATED_DESCR ),
                                                      RESOURCE.getString( mbeansResource.THREAD_SERVLET_REQUEST_ID_DESCR ),
                                                      RESOURCE.getString( mbeansResource.THREAD_METHOD_CONTEXT_ID_DESCR )
                                                    },
                                                    new OpenType[]
                                                    {
                                                      SimpleType.LONG,
                                                      SimpleType.STRING,
                                                      SimpleType.LONG,
                                                      SimpleType.DOUBLE,
                                                      SimpleType.LONG,
                                                      SimpleType.DOUBLE,
                                                      SimpleType.DOUBLE,
                                                      SimpleType.DOUBLE,
                                                      SimpleType.DOUBLE,
                                                      SimpleType.LONG,
                                                      SimpleType.DOUBLE,
                                                      SimpleType.STRING,
                                                      SimpleType.STRING
                                                    }
      );
    }
    catch ( OpenDataException e )
    {
      throw new ExceptionInInitializerError( e );
    }
  }

  /** Type of TabularData returned by getThreadCpuTimeData()
   */
  private static final TabularType  THREAD_CPU_TIME_TYPE;
  static
  {
    try
    {
      THREAD_CPU_TIME_TYPE = new TabularType( ProcessCpuTimeMonitor.class.getName() + ".threadDataType",
                                              RESOURCE.getString( mbeansResource.THREAD_CPU_TIME_TYPE_DESCR ),
                                              THREAD_CPU_TIME_ROW_TYPE,
                                              new String[] { THREAD_CPU_TIME_THREAD_ID_ITEM } );
    }
    catch ( OpenDataException e )
    {
      throw new ExceptionInInitializerError( e );
    }
  }

  /** All item names of OVERALL_CPU_DATA_TYPE; both it and getOverallCpuData()
   *  depend on the order of this array.
   */
  private static final String  OVERALL_CPU_DATA_TYPE_ITEMS[] =
  {
    "startTime",
    "endTime",
    "elapsedSeconds",
    "processPercentCpu",
    "totalTrackedBytesAllocated",
    "threadCpuTimeData"
  };

  /** Type of CompositeData returned by getOverallCpuData()
   */
  private static final CompositeType  OVERALL_CPU_DATA_TYPE;
  static
  {
    try
    {
      OVERALL_CPU_DATA_TYPE = new CompositeType( ProcessCpuTimeMonitor.class.getName() + ".overallCpuDataType",
                                                 RESOURCE.getString( mbeansResource.OVERALL_CPU_DATA_TYPE_DESCR ),
                                                 OVERALL_CPU_DATA_TYPE_ITEMS,
                                                 new String[]
                                                 {
                                                   RESOURCE.getString( mbeansResource.START_TIME_ITEM_DESCR ),
                                                   RESOURCE.getString( mbeansResource.END_TIME_ITEM_DESCR ),
                                                   RESOURCE.getString( mbeansResource.ELAPSED_TIME_ITEM_DESCR ),
                                                   RESOURCE.getString( mbeansResource.PROCESS_PERC_CPU_ITEM_DESCR ),
                                                   RESOURCE.getString( mbeansResource.TOTAL_TRACKED_BYTES_ALLOCATED_DESCR ),
                                                   RESOURCE.getString( mbeansResource.THREAD_CPU_TIME_DATA_ITEM_DESCR ),
                                                 },
                                                 new OpenType[]
                                                 {
                                                   SimpleType.DATE,
                                                   SimpleType.DATE,
                                                   SimpleType.DOUBLE,
                                                   SimpleType.DOUBLE,
                                                   SimpleType.LONG,
                                                   THREAD_CPU_TIME_TYPE
                                                 }
      );
    }
    catch ( OpenDataException e )
    {
      throw new ExceptionInInitializerError( e );
    }
  }

  //
  // Miscellaneous statics of minor import
  //

  // computed once here and held onto rather than recomputing
  // [though getNanoTimeFromMilliTime() is deprecated there is no better alternative in this particular case]
  @SuppressWarnings( "deprecation" )
  private static final long  VM_START_TIME_IN_NANOS = MBeanUtilities.getNanoTimeFromMilliTime( MBeanUtilities.VM_START_TIME );

  //
  // Instance fields
  //

  /* Get a reference to a log4j logger for this instance; should be done and
   * assigned to a field as getLogger() is significantly more expensive than the
   * Logger instance methods used to determine whether to do logging.
   */
  final Logger  logger = LogR.getLogger( ProcessCpuTimeMonitor.class.getName() );  // not private to allow more direct access by BackgroundTask

  final NotificationBroadcasterSupport  notificationEmitter = new NotificationBroadcasterSupport();  // not private to allow more direct access by BackgroundTask

  // create a periodic task support instance with default interval of 30 seconds and using the default worker thread
  private final PeriodicTaskSupport  backgroundTask = new PeriodicTaskSupport( 30 )
  {
    // Implementation of abstract PeriodicTaskSupport method
    // note that this is called whenever the task interval is changed, thus replacing the task instance with a new one
    @Override
    protected final Runnable  createTask()
    {
      // return background task as defined above
      return ( new BackgroundTask() );
    }
  };

  // volatile to ensure all threads always use latest values without synchronization
  volatile double  processPercentCpuThreshold;  // not private to allow more direct access by BackgroundTask
  private volatile BaselineData  baselineData = new BaselineData( true );
  volatile IntervalData  recentData;  // not private to allow more direct access by BackgroundTask
  volatile boolean  lastWasOverThreshold;  // for use by BackgroundTask; hoisted to outer class so as not to be lost when changing BackgroundTask instances; not private to allow more direct access by BackgroundTask
}
