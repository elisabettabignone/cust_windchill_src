/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.jmx.core.mbeans;

/* In addition to providing useful functionality, this interface is intended to
 * serve as a example MBean interface to aid developers and customizers author
 * their own MBean interfaces and implement these.
 *
 * Even though source code for this class is provided, compilation of this
 * source under the existing class name (i.e. in place of the class delivered)
 * is still not supported.
 */

/* For each of our MBean interface source files, xxxMBean.java, automated
 * tooling is used to produce a corresponding xxxMBeanResource.rbInfo file.
 * Thus in the case of this MBean inteface, SelfEmailingMBeanResource.rbInfo
 * is produced by this tooling.
 *
 * To [re]generate the rbInfo for an MBean interface source file (upon creating
 * a new MBean interface file or making changes), invoke the following command
 * from the source root directory in a Windchill shell:
 *   javac -proc:only -processor wt.annotations.processors.MBeanInterfaceProcessor <path of source file>
 * For instance, for this MBean interface (on Windows) one would invoke
 *   javac -proc:only -processor wt.annotations.processors.MBeanInterfaceProcessor wt\jmx\core\mbeans\SelfEmailingMBean.java
 *
 * The tool garners descriptions and operation parameter names from the formal
 * parameter names and Javadoc comments in the MBean interface source file and
 * expresses these in the rbInfo.  At runtime, the wt.jmx.core.StandardMBean
 * reads these rbInfos and incorporates this additional metadata into the
 * MBeans' MBeanInfo.  This allows for localization, though no localized
 * versions of these particular bundles are currently produced for delivery
 * with the product.  Re-use of formal parameters and Javadoc comment information
 * avoids redundant documentation efforts and ensures consistency between
 * Javadoc documentation and MBean documentation at runtime.
 */

import javax.management.MBeanOperationInfo;

import wt.jmx.annotations.MBeanOperationImpact;


/** An MBean capable of e-mailing data about itself.
 *
 *    <BR><BR><B>Supported API: </B>true
 *    <BR><BR><B>Extendable: </B>true
 */
public interface  SelfEmailingMBean
{
  /** E-mail styled data on this MBean and (optionally) its "children"
   *
   *    <BR><BR><B>Supported API: </B>true
   *
   *  @param addressesOrEmailList Comma-delimited list of e-mail addresses or name of e-mail list to send e-mail to
   *  @param subject Subject to give e-mail
   *  @param includeChildren Whether to include other MBeans which have the same ObjectName domain and include all the same ObjectName property pairs as this one
   */
  @MBeanOperationImpact( MBeanOperationInfo.INFO )
  public void  emailThisMBean( String addressesOrEmailList, String subject, boolean includeChildren )
    throws Exception;
}
