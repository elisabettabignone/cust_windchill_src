/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.meeting;

import wt.util.resource.*;

@RBUUID("wt.meeting.meetingResource")
public final class meetingResource extends WTListResourceBundle {
   @RBEntry("The meeting \"{0}\" has duplicate subject items.")
   public static final String DUPLICATE_SUBJECT_ITEM = "0";

   @RBEntry("The subject item \"{0}\" for meeting \"{1}\" does not exist.")
   public static final String INVALID_SUBJECT_ITEM = "1";

   @RBEntry("The user or group \"{0}\" for meeting \"{1}\" does not exist.")
   public static final String INVALID_USER_OR_GROUP = "2";

   @RBEntry("The meeting \"{0}\" has duplicate participants.")
   public static final String DUPLICATE_PARTICIPANT = "3";

   @RBEntry("Invalid entry : \"{0}\" \n Duration should be more than 0")
   public static final String ILLEGAL_DURATION = "4";

   @RBEntry("Illegal format for input value of number of phone lines for meeting: \"{0}\".")
   public static final String ILLEGAL_NUM_LINES = "5";

   @RBEntry("Meeting Notification")
   public static final String NOTIFICATION_MEETING_ANNOUNCEMENT = "6";

   @RBEntry("Subject:")
   public static final String NOTIFICATION_MEETING_NAME = "7";

   @RBEntry("Meeting date:")
   public static final String NOTIFICATION_MEETING_DATE = "8";

   @RBEntry("Meeting time:")
   public static final String NOTIFICATION_MEETING_TIME_LABEL = "9";

   @RBEntry("Meeting password:")
   public static final String NOTIFICATION_MEETING_PASSWD = "9001";

   @RBEntry("{0}")
   public static final String NOTIFICATION_MEETING_SCHEDULED = "10";

   @RBEntry("Edited: {0}")
   public static final String NOTIFICATION_MEETING_UPDATED = "11";

   @RBEntry("Cancelled: {0}")
   public static final String NOTIFICATION_MEETING_CANCELED = "12";

   @RBEntry("The meeting described below has been cancelled.")
   public static final String NOTIFICATION_MEETING_CANCELED_MESSAGE = "16";

   @RBEntry("Meeting type:")
   public static final String NOTIFICATION_MEETING_TYPE_LABEL = "17";

   @RBEntry("Standard meeting.")
   public static final String NOTIFICATION_MEETING_TYPE_TRADITIONAL = "18";

   @RBEntry("Webex web-based meeting. The Webex client software must be installed on your computer in order to participate in this meeting.")
   public static final String NOTIFICATION_MEETING_TYPE_WEBEX = "19";

   @RBEntry("Creo View web-based meeting. The Creo View application software must be installed on your computer in order to participate in this meeting.")
   public static final String NOTIFICATION_MEETING_TYPE_PRODUCTVIEW = "20";

   @RBEntry("Error starting the Creo View collaboration server. Check the installation and configuration of this software.")
   public static final String COLLAB_SERVER_START_ERROR = "21";

   @RBEntry("Error performing meeting center operation. Error is: \"{0}\"")
   public static final String WEBEX_OPERATION_ERROR = "29";

   @RBEntry("Error connecting to the meeting server. Unable to perform the operation.")
   public static final String WEBEX_CONNECTION_ERROR = "30";

   @RBEntry("Error connecting to the meeting server. HTTP response code is {0}.")
   public static final String WEBEX_CONNECTION_ERROR_HTTP_ERROR = "3001";

   @RBEntry("Error connecting to the meeting server. Unknown host.")
   public static final String WEBEX_CONNECTION_ERROR_BAD_HOST = "3002";

   @RBEntry("A URL for the meeting center must be supplied in wt.properties. Property name is \"{0}\"")
   public static final String MISSING_WEBEX_CENTER_URL = "31";

   @RBEntry("The meeting \"{0}\" has minutes associated with it and can not be canceled.")
   public static final String MEETING_MINUTES_EXIST = "32";

   @RBEntry("The meeting \"{0}\" has action items associated with it and can not be canceled.")
   public static final String MEETING_ACTIONITEMS_EXIST = "33";

   @RBEntry("There are no subject objects to delete.")
   public static final String NO_SUBJECT_OBJECTS_TO_DELETE = "34";

   @RBEntry("Trying to remove more meeting participants than are in the meeting.")
   public static final String REMOVE_PARTICIPANTS_NUMBER = "35";

   @RBEntry("Illegal method name to return properties page for object.")
   public static final String ILLEGAL_METHOD_NAME = "36";

   @RBEntry("No meeting with object reference string of \"{0}\".")
   public static final String ILLEGAL_MEETING_OID = "37";

   @RBEntry("No project with object reference string of \"{0}\".")
   public static final String ILLEGAL_PROJECT_OID = "38";

   @RBEntry("The input password is not the correct password for this meeting.")
   public static final String ILLEGAL_MEETING_PASSWD = "39";

   @RBEntry("Only the meeting creator is allowed to host a meeting.")
   public static final String ILLEGAL_HOST_OPERATION = "3901";

   @RBEntry("Agenda:")
   public static final String NOTIFICATION_AGENDA = "40";

   @RBEntry("Call information:")
   public static final String NOTIFICATION_CALL_INFO = "41";

   @RBEntry("Password:")
   public static final String NOTIFICATION_PASSWORD = "42";

   @RBEntry("Invitees:")
   public static final String NOTIFICATION_INVITEES = "43";

   @RBEntry("Meeting Objects:")
   public static final String NOTIFICATION_REFERENCES = "44";

   @RBEntry("Group")
   public static final String NOTIFICATION_GROUP = "45";

   @RBEntry("{0} - {1} {2}")
   public static final String NOTIFICATION_MEETING_TIME = "46";

   @RBEntry("Illegal method name to send notifications for meetings.")
   public static final String ILLEGAL_NOTIFICATION_METHOD = "47";

   @RBEntry("Meeting notification sender method must be public and static.")
   public static final String ILLEGAL_NOTIFICATION_METHOD_MOD = "48";

   @RBEntry("None")
   public static final String NOTIFICATION_NONE = "49";

   @RBEntry("Teleconference information:")
   public static final String NOTIFICATION_TELECONFERENCE_INFO = "50";

   @RBEntry("A URL for the meeting center, \"{0}\", has an invalid format.")
   public static final String ILLEGAL_WEBEX_CENTER_URL = "51";

   @RBEntry("?")
   @RBPseudo(false)
   public static final String NOTIFICATION_END_TIME = "52";

   @RBEntry("Location:")
   public static final String NOTIFICATION_LOCATION_LABEL = "53";

   @RBEntry("View Information")
   public static final String NOTIFICATION_VIEW_DETAILS = "54";

   @RBEntry("The Add Minutes field should contain no more than \"{0}\" characters.")
   @RBArgComment0("name of attribute")
   public static final String ADDMINS_LENGTH_EXCEEDED = "94";

   @RBEntry("The value for meeting name exceeds the upper limit of \"{0}\" characters.")
   @RBArgComment0("name of attribute")
   public static final String MINNAME_LENGTH_EXCEEDED = "95";

   /**
    * entries for the JSP pages
    **/
   @RBEntry("Host Meeting Response")
   public static final String MEETING_HOST_COMPLETE_TITLE = "570";

   @RBEntry("Sorry, the meeting has already finished.")
   public static final String MEETING_HOST_ALREADY_FINISHED = "580";

   @RBEntry("Error trying to host the meeting. Error code is \"{0}\".")
   public static final String MEETING_HOST_GENERAL_ERROR = "590";

   @RBEntry("Meeting completed.")
   public static final String MEETING_HOST_COMPLETE = "600";

   @RBEntry("Join Meeting Response")
   public static final String MEETING_JOIN_COMPLETE_TITLE = "610";

   @RBEntry("Sorry, the meeting has not yet started.")
   public static final String MEETING_JOIN_NOT_STARTED = "620";

   @RBEntry("Sorry, the meeting has been locked.")
   public static final String MEETING_JOIN_LOCKED = "630";

   @RBEntry("Error trying to join meeting. Error code is \"{0}\".")
   public static final String MEETING_JOIN_GENERAL_ERROR = "640";

   @RBEntry("Close")
   @RBComment("Label on button to close the form")
   public static final String CLOSE_BUTTON = "650";

   @RBEntry("The email address, \"{0}\", has been used by another user with a different Webex user ID. Webex requires that all users have a unique email address.")
   public static final String MEETING_EMAIL_CONFLICT_ERROR = "651";

   @RBEntry("The Webex meeting center reports that automatic login is not enabled for this site.")
   public static final String WEBEX_AUTOLOGINDISABLED = "652";

   @RBEntry("The Webex meeting center reports that this session is being initiated from as host with an invalid IP address.")
   public static final String WEBEX_REFERRERDOMAINERROR = "653";

   @RBEntry("The Webex meeting center reports that you are already logged on to the meeting center. Open a new browser session and try the operation again.")
   public static final String WEBEX_ALREADYLOGON = "654";

   @RBEntry("The Webex meeting center reports an unknown error during login. Status is \"{0}\", reason is \"{1}\".")
   public static final String WEBEX_GENERAL_LOGON_ERROR = "655";

   @RBEntry("The Webex meeting center reports an unknown error during meeting creation. Status is \"{0}\", reason is \"{1}\".")
   public static final String WEBEX_GENERAL_MEETING_CREATE_ERROR = "656";

   @RBEntry("Current meeting status is \"{0}\". Only meetings with status of \"{1}\" can be hosted.")
   public static final String MEETING_WRONG_STATUS_FOR_HOST = "657";

   @RBEntry("Current meeting status is \"{0}\". Only meetings with status of \"{1}\" can be joined.")
   public static final String MEETING_WRONG_STATUS_FOR_JOIN = "658";

   @RBEntry("The WebEx partner ID for this site, \"{0}\", does not match the value in the WebEx database.")
   public static final String WEBEX_PARTNERID_ERROR = "659";

   @RBEntry("The IP address for this site does not match the IP address information in the WebEx database.")
   public static final String WEBEX_IPRANGE_ERROR = "660";

   @RBEntry("Your WebEx user ID, \"{0}\", is already in use.")
   public static final String WEBEX_ID_CONFLICT_ERROR = "661";

   @RBEntry("During new user signup, the WebEx server reports that the URL \"{0}\", is \"Unknown\".")
   public static final String WEBEX_UNKNOWN_WEB_SERVER = "662";

   @RBEntry("During new user signup, the WebEx server has returned an error with the unknown code: \"{0}\".")
   public static final String WEBEX_UNKNOWN_SU_ERROR = "663";

   @RBEntry("The user, \"{0}\", has no email address. An email address is required to host a meeting.")
   public static final String MEETING_USER_NO_EMAIL_ERROR = "664";

   @RBEntry("Error parsing response from the WebEx server. Complete response follows:")
   public static final String WEBEX_RESPONSE_PARSE_ERROR = "665";

   @RBEntry("Webex operation error")
   public static final String WEBEX_GENERAL_OP_ERROR_TITLE = "666";

   @RBEntry("The following error occurred performing the meeting operation:")
   public static final String WEBEX_GENERAL_OP_ERROR_MSG = "667";

   @RBEntry("Only WTPart, WTDocument and EPMDocument can be a subject item of a meeting. Please select a valid subject item.")
   public static final String INVALID_MEETING_SUBJECT_ITEM = "668";

   @RBEntry("The user, \"{0}\", has no email address. Email notifications will not be sent.")
   public static final String MEETING_NOTIFICATION_SENDER_NO_EMAIL_ERROR = "669";
   
   @RBEntry("The meeting is hosted successfully")
   public static final String MEETING_HOSTED_SUCCESS="670";
   
   @RBEntry("You have joined the meeting successfully")
   public static final String MEETING_JOINED_SUCCESS="671";
}
