// Generated WebexUserInfoDelegate%436A797800FF: Fri 11/04/05 14:08:38
/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */

package wt.meeting;

import java.lang.String;
import wt.org.WTUser;
import wt.util.WTException;

//##begin user.imports preserve=yes
//##end user.imports

//##begin WebexUserInfoDelegate%436A797800FF.doc preserve=yes
/**
 *
 * <BR><BR><B>Supported API: </B>true
 * <BR><BR><B>Extendable: </B>false
 * <BR><BR>
 *
 * The purpose of this delegate is to allow customization of the way
 * Webex account names and passwords are created and stored.  It is used
 * to create the account name and password.  It is the responsibility of
 * any class implementing this interface to conform with all Webex
 * requirements.  Names, email address, and password must conform to the
 * Webex requirements listed in the Webex API guides.  If storing any
 * information is required to make this work, that is also the
 * responsibility of the implementor.
 *
 *
 * <BR><BR>
 * @version   1.0
 **/
//##end WebexUserInfoDelegate%436A797800FF.doc

public interface WebexUserInfoDelegate {


   //##begin user.attributes preserve=yes
   //##end user.attributes

   //##begin static.initialization preserve=yes
   //##end static.initialization


   // --- Operation Section ---

   //##begin getFirstName%436A79C60026g.doc preserve=yes
   /**
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @return    String
    **/
   //##end getFirstName%436A79C60026g.doc

   public String getFirstName();

   //##begin getLastName%436A79E80304g.doc preserve=yes
   /**
    *
    * <BR><BR><B>Supported API: </B>true
    *<BR><BR>
    * Return the user's first name as a string.
    *<BR><BR>
    * @return    String
    **/
   //##end getLastName%436A79E80304g.doc

   public String getLastName();

   //##begin getUsername%436A79EC026Ag.doc preserve=yes
   /**
    *
    * <BR><BR><B>Supported API: </B>true
    *<BR><BR>
    * Return the user's last name as a string.
    *<BR><BR>

    * @return    String
    **/
   //##end getUsername%436A79EC026Ag.doc

   public String getUsername();

   //##begin getPassword%436A79F50099g.doc preserve=yes
   /**
    *
    * <BR><BR><B>Supported API: </B>true
    *<BR><BR>
    * Return the user's Webex account name as a string.
    *<BR><BR>
    * @return    String
    **/
   //##end getPassword%436A79F50099g.doc

   public String getPassword();

   //##begin getEmail%436A79FE0253g.doc preserve=yes
   /**
    *
    * <BR><BR><B>Supported API: </B>true
    *<BR><BR>
    * Return the user's email address as a string.
    *<BR><BR>
    * @return    String
    **/
   //##end getEmail%436A79FE0253g.doc

   public String getEmail();

   //##begin initialize%436A7A04013C.doc preserve=yes
   /**
    *
    * <BR><BR><B>Supported API: </B>true
    * <BR><BR>
    * This will be called as the last step in object construction.  Before it
    * is called, there is no guarantee that any valid data will be returned by
    * other methods of this class.  After it is called, it is required that
    * every call to a method of this class will return valid data.
    *<BR><BR>
    * @param     user The Windchill user object.
    * @exception wt.util.WTException

    **/
   //##end initialize%436A7A04013C.doc

   public void initialize( WTUser user )
            throws WTException;

   //##begin user.operations preserve=yes
   //##end user.operations
}
