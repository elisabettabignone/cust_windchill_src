/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.meeting.actionitem;

import wt.util.resource.*;

@RBUUID("wt.meeting.actionitem.actionitemResource")
public final class actionitemResource extends WTListResourceBundle {
   @RBEntry("An Action Item has been Created")
   public static final String NOTIFICATION_ACTIONITEM_CREATED = "17";

   @RBEntry("An Action Item has been Edited")
   public static final String NOTIFICATION_ACTIONITEM_UPDATED = "18";

   @RBEntry("An Action Item has been Deleted")
   public static final String NOTIFICATION_ACTIONITEM_DELETED = "19";

   @RBEntry("An Action Item has been Resolved")
   public static final String NOTIFICATION_ACTIONITEM_RESOLVED = "1901";

   @RBEntry("An Action Item is Overdue")
   public static final String NOTIFICATION_ACTIONITEM_OVERDUE = "1902";

   @RBEntry("An Action Item has been Reassigned")
   public static final String NOTIFICATION_ACTIONITEM_REASSIGNED = "1903";

   @RBEntry("Action item \"{0}\" has been created in project \"{1}\"")
   public static final String NOTIFICATION_ACTIONITEM_CREATED_FOR_PROJ = "20";

   @RBEntry("Action item \"{0}\" has been edited in project \"{1}\"")
   public static final String NOTIFICATION_ACTIONITEM_UPDATED_FOR_PROJ = "21";

   @RBEntry("Action item \"{0}\" has been deleted from project \"{1}\"")
   public static final String NOTIFICATION_ACTIONITEM_DELETED_FOR_PROJ = "22";

   @RBEntry("Action item \"{0}\" in project \"{1}\" has been resolved")
   public static final String NOTIFICATION_ACTIONITEM_RESOLVED_FOR_PROJ = "2201";

   @RBEntry("Action item \"{0}\" in project \"{1}\" is overdue")
   public static final String NOTIFICATION_ACTIONITEM_OVERDUE_FOR_PROJ = "2202";

   @RBEntry("Action item \"{0}\" in project \"{1}\" has been reassigned to {2}")
   public static final String NOTIFICATION_ACTIONITEM_REASSIGNED_FOR_PROJ = "2203";

   @RBEntry("Action item \"{0}\" in project \"{1}\" is now obsolete")
   public static final String NOTIFICATION_ACTIONITEM_OBSOLETE_FOR_PROJ = "2204";

   @RBEntry("Action Item Announcement")
   public static final String NOTIFICATION_ACTIONITEM_ANNOUNCEMENT = "23";

   @RBEntry("Name:")
   public static final String NOTIFICATION_ACTIONITEM_NAME = "24";

   @RBEntry("Description:")
   public static final String NOTIFICATION_ACTIONITEM_DESCRIPTION = "25";

   @RBEntry("Due Date:")
   public static final String NOTIFICATION_ACTIONITEM_DATE = "26";

   @RBEntry("Owner:")
   public static final String NOTIFICATION_ACTIONITEM_OWNER = "27";

   @RBEntry("The action item described below has been deleted.")
   public static final String NOTIFICATION_ACTIONITEM_DELETED_MESSAGE = "28";

   @RBEntry("Illegal method name to send notifications for action items.")
   public static final String ILLEGAL_NOTIFICATION_METHOD = "29";

   @RBEntry("Action item notification sender method must be public and static.")
   public static final String ILLEGAL_NOTIFICATION_METHOD_MOD = "30";

   @RBEntry("None")
   public static final String NOTIFICATION_NONE = "31";

   @RBEntry("Resolution information for this action item changed.")
   public static final String NOTIFICATION_ACTIONITEM_RESOLVED_MESSAGE = "32";

   @RBEntry("Sorry, you do not have permission to modify this action item.")
   public static final String MODIFICATION_NOT_AUTHORIZED = "33";

   @RBEntry("The action item must have a name.")
   public static final String ACTIONITEM_MISSING_NAME = "34";

   @RBEntry("Sorry, you do not have permission to resolve this action item.")
   public static final String RESOLUTION_NOT_AUTHORIZED = "35";

   @RBEntry("Can not send a notification email message about action item \"{0}\" because the sender has no email address.")
   public static final String NOTIFICATION_ACTIONITEM_NO_EMAIL_ADDR = "36";

   @RBEntry("{0} is a required field.")
   public static final String REQUIRED_EXTENDED_ATTRIBUTE = "37";

   @RBEntry("{0}:")
   @RBPseudo(false)
   public static final String LABEL_COLON = "38";

   @RBEntry("Assignee:")
   public static final String NOTIFICATION_ACTIONITEM_ASSIGNEE = "39";

   @RBEntry("Identity:")
   public static final String NOTIFICATION_ACTIONITEM_IDENTITY = "40";

   @RBEntry("Created By:")
   public static final String ACTIONITEM_VIEW_CREATOR = "41";

   @RBEntry("Priority:")
   public static final String ACTIONITEM_PRIORITY_LABEL = "42";

   @RBEntry("Done:")
   public static final String ACTIONITEM_PERCENT_COMPLETE_LABEL = "43";

   @RBEntry("Status:")
   public static final String ACTIONITEM_HEALTH_STATUS_LABEL = "44";

   @RBEntry("Status Description:")
   public static final String ACTIONITEM_STATUS_DESC_LABEL = "45";

   @RBEntry("State:")
   public static final String ACTIONITEM_STATE_LABEL = "46";

   @RBEntry("Created:")
   public static final String ACTIONITEM_VIEW_CREATION_DATE = "47";

   @RBEntry("Resolution:")
   public static final String NOTIFICATION_RESOLUTION_LABEL = "48";

   @RBEntry("Status Description:")
   public static final String ACTIONITEM_STATUS_DES_LABEL = "49";

   @RBEntry("{0} must be entered in the format hh:mm.")
   public static final String TIME_FORMAT_ERROR = "50";

   @RBEntry("The value exceeds the upper limit of \"{0}\" for \"{1}\". ")
   public static final String ATTRIBUTE_VALUE_EXCEEDED = "51";
   
   @RBEntry("Status")
   public static final String ACTIONITEM_HEALTH_STATUS_VIEW_LABEL = "ACTIONITEM_HEALTH_STATUS_VIEW_LABEL";
}