/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.meeting.actionitem;

import wt.util.resource.*;

@RBUUID("wt.meeting.actionitem.actionitemResource")
public final class actionitemResource_it extends WTListResourceBundle {
   @RBEntry("È stata creata un'azione")
   public static final String NOTIFICATION_ACTIONITEM_CREATED = "17";

   @RBEntry("È stata modificata un'azione")
   public static final String NOTIFICATION_ACTIONITEM_UPDATED = "18";

   @RBEntry("È stata annullata un'azione")
   public static final String NOTIFICATION_ACTIONITEM_DELETED = "19";

   @RBEntry("È stata eseguita un'azione")
   public static final String NOTIFICATION_ACTIONITEM_RESOLVED = "1901";

   @RBEntry("Un'azione è in ritardo")
   public static final String NOTIFICATION_ACTIONITEM_OVERDUE = "1902";

   @RBEntry("È stata riassegnata un'azione")
   public static final String NOTIFICATION_ACTIONITEM_REASSIGNED = "1903";

   @RBEntry("È stata creata l'azione \"{0}\" per il progetto \"{1}\"")
   public static final String NOTIFICATION_ACTIONITEM_CREATED_FOR_PROJ = "20";

   @RBEntry("È stata modificata l'azione \"{0}\" per il progetto \"{1}\"")
   public static final String NOTIFICATION_ACTIONITEM_UPDATED_FOR_PROJ = "21";

   @RBEntry("È stata eliminata l'azione \"{0}\" per il progetto \"{1}\"")
   public static final String NOTIFICATION_ACTIONITEM_DELETED_FOR_PROJ = "22";

   @RBEntry("È stata eseguita l'azione \"{0}\" per il progetto \"{1}\"")
   public static final String NOTIFICATION_ACTIONITEM_RESOLVED_FOR_PROJ = "2201";

   @RBEntry("L'azione \"{0}\" del progetto \"{1}\" è in ritardo")
   public static final String NOTIFICATION_ACTIONITEM_OVERDUE_FOR_PROJ = "2202";

   @RBEntry("L'azione \"{0}\" del progetto \"{1}\" è stata riassegnata a {2}")
   public static final String NOTIFICATION_ACTIONITEM_REASSIGNED_FOR_PROJ = "2203";

   @RBEntry("L'azione \"{0}\" del progetto \"{1}\" è obsoleta")
   public static final String NOTIFICATION_ACTIONITEM_OBSOLETE_FOR_PROJ = "2204";

   @RBEntry("Annuncio d'azione")
   public static final String NOTIFICATION_ACTIONITEM_ANNOUNCEMENT = "23";

   @RBEntry("Nome:")
   public static final String NOTIFICATION_ACTIONITEM_NAME = "24";

   @RBEntry("Descrizione:")
   public static final String NOTIFICATION_ACTIONITEM_DESCRIPTION = "25";

   @RBEntry("Data di scadenza:")
   public static final String NOTIFICATION_ACTIONITEM_DATE = "26";

   @RBEntry("Proprietario:")
   public static final String NOTIFICATION_ACTIONITEM_OWNER = "27";

   @RBEntry("L'azione sotto descritta è stata eliminata.")
   public static final String NOTIFICATION_ACTIONITEM_DELETED_MESSAGE = "28";

   @RBEntry("Metodo non valido per la notifica di azioni.")
   public static final String ILLEGAL_NOTIFICATION_METHOD = "29";

   @RBEntry("Il metodo di notifica d'azione deve essere pubblico e statico.")
   public static final String ILLEGAL_NOTIFICATION_METHOD_MOD = "30";

   @RBEntry("Nessuno")
   public static final String NOTIFICATION_NONE = "31";

   @RBEntry("Le informazioni sull'esecuzione dell'azione sono cambiate.")
   public static final String NOTIFICATION_ACTIONITEM_RESOLVED_MESSAGE = "32";

   @RBEntry("Non si dispone dei permessi per modificare l'azione.")
   public static final String MODIFICATION_NOT_AUTHORIZED = "33";

   @RBEntry("L'azione deve avere un nome.")
   public static final String ACTIONITEM_MISSING_NAME = "34";

   @RBEntry("Non si dispone dei permessi per eseguire l'azione.")
   public static final String RESOLUTION_NOT_AUTHORIZED = "35";

   @RBEntry("Impossibile inviare un messaggio di e-mail di notifica sull'azione \"{0}\" perché il mittente non ha un indirizzo di e-mail.")
   public static final String NOTIFICATION_ACTIONITEM_NO_EMAIL_ADDR = "36";

   @RBEntry("{0} è un campo obbligatorio.")
   public static final String REQUIRED_EXTENDED_ATTRIBUTE = "37";

   @RBEntry("{0}:")
   @RBPseudo(false)
   public static final String LABEL_COLON = "38";

   @RBEntry("Assegnatario:")
   public static final String NOTIFICATION_ACTIONITEM_ASSIGNEE = "39";

   @RBEntry("Identificativo")
   public static final String NOTIFICATION_ACTIONITEM_IDENTITY = "40";

   @RBEntry("Autore:")
   public static final String ACTIONITEM_VIEW_CREATOR = "41";

   @RBEntry("Priorità:")
   public static final String ACTIONITEM_PRIORITY_LABEL = "42";

   @RBEntry("Completato:")
   public static final String ACTIONITEM_PERCENT_COMPLETE_LABEL = "43";

   @RBEntry("Stato:")
   public static final String ACTIONITEM_HEALTH_STATUS_LABEL = "44";

   @RBEntry("Descrizione stato:")
   public static final String ACTIONITEM_STATUS_DESC_LABEL = "45";

   @RBEntry("Stato del ciclo di vita:")
   public static final String ACTIONITEM_STATE_LABEL = "46";

   @RBEntry("Data creazione:")
   public static final String ACTIONITEM_VIEW_CREATION_DATE = "47";

   @RBEntry("Risoluzione:")
   public static final String NOTIFICATION_RESOLUTION_LABEL = "48";

   @RBEntry("Descrizione stato:")
   public static final String ACTIONITEM_STATUS_DES_LABEL = "49";

   @RBEntry("{0} deve essere immesso nel formato hh.mm.")
   public static final String TIME_FORMAT_ERROR = "50";

   @RBEntry("Il valore supera il limite superiore di \"{0}\" per \"{1}\".")
   public static final String ATTRIBUTE_VALUE_EXCEEDED = "51";
   
   @RBEntry("Stato")
   public static final String ACTIONITEM_HEALTH_STATUS_VIEW_LABEL = "ACTIONITEM_HEALTH_STATUS_VIEW_LABEL";
}
