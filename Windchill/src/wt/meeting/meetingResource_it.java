/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.meeting;

import wt.util.resource.*;

@RBUUID("wt.meeting.meetingResource")
public final class meetingResource_it extends WTListResourceBundle {
   @RBEntry("Il meeting \"{0}\" contiene degli oggetti di discussione duplicati.")
   public static final String DUPLICATE_SUBJECT_ITEM = "0";

   @RBEntry("L'oggetto di discussione \"{0}\" del meeting \"{1}\" non esiste.")
   public static final String INVALID_SUBJECT_ITEM = "1";

   @RBEntry("L'utente o il gruppo \"{0}\" del meeting \"{1}\" non esiste.")
   public static final String INVALID_USER_OR_GROUP = "2";

   @RBEntry("I nominativi dei partecipanti del meeting \"{0}\" sono stati duplicati.")
   public static final String DUPLICATE_PARTICIPANT = "3";

   @RBEntry("Valore non valido: \"{0}\" \n La durata deve essere superiore a 0")
   public static final String ILLEGAL_DURATION = "4";

   @RBEntry("Formato non valido del valore di input del numero di linee telefoniche per il meeting \"{0}\".")
   public static final String ILLEGAL_NUM_LINES = "5";

   @RBEntry("Notifica meeting")
   public static final String NOTIFICATION_MEETING_ANNOUNCEMENT = "6";

   @RBEntry("Oggetto:")
   public static final String NOTIFICATION_MEETING_NAME = "7";

   @RBEntry("Data meeting:")
   public static final String NOTIFICATION_MEETING_DATE = "8";

   @RBEntry("Ora meeting:")
   public static final String NOTIFICATION_MEETING_TIME_LABEL = "9";

   @RBEntry("Password meeting:")
   public static final String NOTIFICATION_MEETING_PASSWD = "9001";

   @RBEntry("{0}")
   public static final String NOTIFICATION_MEETING_SCHEDULED = "10";

   @RBEntry("Modificato: {0}")
   public static final String NOTIFICATION_MEETING_UPDATED = "11";

   @RBEntry("Annullato: {0}")
   public static final String NOTIFICATION_MEETING_CANCELED = "12";

   @RBEntry("Il meeting descritto di seguito è stato annullato.")
   public static final String NOTIFICATION_MEETING_CANCELED_MESSAGE = "16";

   @RBEntry("Tipo di meeting:")
   public static final String NOTIFICATION_MEETING_TYPE_LABEL = "17";

   @RBEntry("Meeting standard.")
   public static final String NOTIFICATION_MEETING_TYPE_TRADITIONAL = "18";

   @RBEntry("Meeting Web Webex. Il software Webex per client deve essere installato sul proprio computer per permettere la partecipazione al meeting.")
   public static final String NOTIFICATION_MEETING_TYPE_WEBEX = "19";

   @RBEntry("Meeting Web Creo View. Il software dell'applicazione Creo View deve essere installato sul computer in uso per consentire la partecipazione al meeting.")
   public static final String NOTIFICATION_MEETING_TYPE_PRODUCTVIEW = "20";

   @RBEntry("Errore durante l'avvio del server di collaborazione Creo View. Controllare l'installazione e la configurazione del software.")
   public static final String COLLAB_SERVER_START_ERROR = "21";

   @RBEntry("Errore durante l'esecuzione di un'operazione del centro meeting. Errore: {0}")
   public static final String WEBEX_OPERATION_ERROR = "29";

   @RBEntry("Errore durante la connessione al meeting server. Impossibile eseguire l'operazione.")
   public static final String WEBEX_CONNECTION_ERROR = "30";

   @RBEntry("Errore durante la connessione al meeting server. Il codice di risposta HTTP è {0}.")
   public static final String WEBEX_CONNECTION_ERROR_HTTP_ERROR = "3001";

   @RBEntry("Errore durante la connessione al meeting server. Host sconosciuto.")
   public static final String WEBEX_CONNECTION_ERROR_BAD_HOST = "3002";

   @RBEntry("È necessario indicare un URL per il centro meeting in wt.properties. Il nome della proprietà è \"{0}\"")
   public static final String MISSING_WEBEX_CENTER_URL = "31";

   @RBEntry("Al meeting \"{0}\" sono stati allegati degli appunti. Il meeting non può essere annullato.")
   public static final String MEETING_MINUTES_EXIST = "32";

   @RBEntry("Sono state associate delle azioni al meeting \"{0}\". Il meeting non può essere annullato.")
   public static final String MEETING_ACTIONITEMS_EXIST = "33";

   @RBEntry("Non esistono oggetti da eliminare.")
   public static final String NO_SUBJECT_OBJECTS_TO_DELETE = "34";

   @RBEntry("Impossibile eliminare un numero di partecipanti superiore a quello previsto per il meeting.")
   public static final String REMOVE_PARTICIPANTS_NUMBER = "35";

   @RBEntry("Nome metodo non valido per la restituzione della pagina delle proprietà per l'oggetto.")
   public static final String ILLEGAL_METHOD_NAME = "36";

   @RBEntry("Non esiste un meeting con stringa di riferimento oggetto \"{0}\"")
   public static final String ILLEGAL_MEETING_OID = "37";

   @RBEntry("Non esiste un progetto con stringa di riferimento oggetto \"{0}\"")
   public static final String ILLEGAL_PROJECT_OID = "38";

   @RBEntry("La password di input non è corretta per questo meeting.")
   public static final String ILLEGAL_MEETING_PASSWD = "39";

   @RBEntry("Solo l'autore del meeting può moderarlo.")
   public static final String ILLEGAL_HOST_OPERATION = "3901";

   @RBEntry("Agenda:")
   public static final String NOTIFICATION_AGENDA = "40";

   @RBEntry("Informazioni di chiamata:")
   public static final String NOTIFICATION_CALL_INFO = "41";

   @RBEntry("Password:")
   public static final String NOTIFICATION_PASSWORD = "42";

   @RBEntry("Invitati:")
   public static final String NOTIFICATION_INVITEES = "43";

   @RBEntry("Oggetti meeting:")
   public static final String NOTIFICATION_REFERENCES = "44";

   @RBEntry("Gruppo")
   public static final String NOTIFICATION_GROUP = "45";

   @RBEntry("{0} - {1} {2}")
   public static final String NOTIFICATION_MEETING_TIME = "46";

   @RBEntry("Metodo non valido per la notifica di meeting")
   public static final String ILLEGAL_NOTIFICATION_METHOD = "47";

   @RBEntry("Il metodo di notifica meeting deve essere pubblico e statico.")
   public static final String ILLEGAL_NOTIFICATION_METHOD_MOD = "48";

   @RBEntry("Nessuno")
   public static final String NOTIFICATION_NONE = "49";

   @RBEntry("Informazioni teleconferenza:")
   public static final String NOTIFICATION_TELECONFERENCE_INFO = "50";

   @RBEntry("L'URL del centro meeting \"{0}\" è di formato non valido.")
   public static final String ILLEGAL_WEBEX_CENTER_URL = "51";

   @RBEntry("?")
   @RBPseudo(false)
   public static final String NOTIFICATION_END_TIME = "52";

   @RBEntry("Luogo:")
   public static final String NOTIFICATION_LOCATION_LABEL = "53";

   @RBEntry("Visualizza informazioni")
   public static final String NOTIFICATION_VIEW_DETAILS = "54";

   @RBEntry("La lunghezza del testo immesso nel campo Aggiungi appunti non può essere maggiore di \"{0}\" caratteri.")
   @RBArgComment0("name of attribute")
   public static final String ADDMINS_LENGTH_EXCEEDED = "94";

   @RBEntry("Il valore del nome meeting supera il limite massimo di \"{0}\" caratteri.")
   @RBArgComment0("name of attribute")
   public static final String MINNAME_LENGTH_EXCEEDED = "95";

   /**
    * entries for the JSP pages
    **/
   @RBEntry("Risposta moderatore meeting")
   public static final String MEETING_HOST_COMPLETE_TITLE = "570";

   @RBEntry("Il meeting è terminato.")
   public static final String MEETING_HOST_ALREADY_FINISHED = "580";

   @RBEntry("Errore durante il tentativo di condurre il meeting. Codice errore: \"{0}\".")
   public static final String MEETING_HOST_GENERAL_ERROR = "590";

   @RBEntry("Meeting completato.")
   public static final String MEETING_HOST_COMPLETE = "600";

   @RBEntry("Risposta partecipante meeting")
   public static final String MEETING_JOIN_COMPLETE_TITLE = "610";

   @RBEntry("Il meeting non è ancora iniziato.")
   public static final String MEETING_JOIN_NOT_STARTED = "620";

   @RBEntry("Il meeting è stato bloccato.")
   public static final String MEETING_JOIN_LOCKED = "630";

   @RBEntry("Errore durante il tentativo di partecipazione al meeting. Codice errore: \"{0}\".")
   public static final String MEETING_JOIN_GENERAL_ERROR = "640";

   @RBEntry("Chiudi")
   @RBComment("Label on button to close the form")
   public static final String CLOSE_BUTTON = "650";

   @RBEntry("L'indirizzo di e-mail, \"{0}\", è stato usato da un altro utente con un diverso ID utente Webex. Webex richiede che tutti gli utenti abbiano un indirizzo di e-mail univoco.")
   public static final String MEETING_EMAIL_CONFLICT_ERROR = "651";

   @RBEntry("Il centro meeting Webex comunica che l'accesso automatico non è abilitato per questo sito.")
   public static final String WEBEX_AUTOLOGINDISABLED = "652";

   @RBEntry("Il centro meeting Webex comunica che la presente sessione è stata iniziata da un moderatore con indirizzo IP non valido.")
   public static final String WEBEX_REFERRERDOMAINERROR = "653";

   @RBEntry("Il centro meeting Webex comunica che l'utente ha già effettuato l'accesso al meeting center. Aprire una nuova sessione del browser e riprovare.")
   public static final String WEBEX_ALREADYLOGON = "654";

   @RBEntry("Il centro meeting Webex comunica il verificarsi di un errore sconosciuto durante l'accesso. Stato: \"{0}\"; ragione: \"{1}\".")
   public static final String WEBEX_GENERAL_LOGON_ERROR = "655";

   @RBEntry("Il centro meeting Webex comunica il verificarsi di un errore sconosciuto durante la creazione del meeting. Stato: \"{0}\", ragione: \"{1}\".")
   public static final String WEBEX_GENERAL_MEETING_CREATE_ERROR = "656";

   @RBEntry("Stato meeting attuale: \"{0}\". Possono essere moderati solo i meeting con stato \"{1}\".")
   public static final String MEETING_WRONG_STATUS_FOR_HOST = "657";

   @RBEntry("Stato meeting attuale: \"{0}\". È possibile partecipare solo ai meeting con stato \"{1}\".")
   public static final String MEETING_WRONG_STATUS_FOR_JOIN = "658";

   @RBEntry("L'ID del partner WebEx per questo sito, \"{0}\", non corrisponde al valore del database WebEx.")
   public static final String WEBEX_PARTNERID_ERROR = "659";

   @RBEntry("L'indirizzo IP per questo sito non corrisponde alle informazioni dell'indirizzo IP del database WebEx.")
   public static final String WEBEX_IPRANGE_ERROR = "660";

   @RBEntry("L'ID WebEx dell'utente \"{0}\" è già utilizzato.")
   public static final String WEBEX_ID_CONFLICT_ERROR = "661";

   @RBEntry("Durante la registrazione del nuovo utente, il server WebEx comunica che l'URL \"{0}\" è \"sconosciuto\".")
   public static final String WEBEX_UNKNOWN_WEB_SERVER = "662";

   @RBEntry("Durante la registrazione del nuovo utente, il server WebEx ha segnalato l'errore con codice sconosciuto \"{0}\".")
   public static final String WEBEX_UNKNOWN_SU_ERROR = "663";

   @RBEntry("L'utente \"{0}\" non ha indirizzo di e-mail. L'indirizzo di e-mail è necessario per moderare il meeting.")
   public static final String MEETING_USER_NO_EMAIL_ERROR = "664";

   @RBEntry("Errore durante l'analisi della risposta del server WebEx. Risposta completa:")
   public static final String WEBEX_RESPONSE_PARSE_ERROR = "665";

   @RBEntry("Errore operazione Webex")
   public static final String WEBEX_GENERAL_OP_ERROR_TITLE = "666";

   @RBEntry("Errore durante l'esecuzione dell'operazione del meeting:")
   public static final String WEBEX_GENERAL_OP_ERROR_MSG = "667";

   @RBEntry("Solo gli oggetti WTPart, WTDocument e EPMDocument possono essere oggetto di discussione per un meeting. Selezionare un oggetto di discussione valido.")
   public static final String INVALID_MEETING_SUBJECT_ITEM = "668";

   @RBEntry("L'utente \"{0}\" non ha indirizzo di e-mail. Non saranno inviate notifiche via e-mail.")
   public static final String MEETING_NOTIFICATION_SENDER_NO_EMAIL_ERROR = "669";
   
   @RBEntry("Meeting avviato correttamente")
   public static final String MEETING_HOSTED_SUCCESS="670";
   
   @RBEntry("Accesso per la partecipazione al meeting completato")
   public static final String MEETING_JOINED_SUCCESS="671";
}
