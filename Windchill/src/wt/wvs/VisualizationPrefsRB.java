/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.wvs;

import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.wvs.VisualizationPrefsRB")
public final class VisualizationPrefsRB extends WTListResourceBundle {
   /**
    * ##########################################################################
    * These text strings are used to describe visualization preferences
    * on the Visualization tab of the user preferences html client
    * ##########################################################################
    **/
   @RBEntry("Visualization")
   public static final String PRIVATE_CONSTANT_0 = "VISUALIZATION_DISPLAY_NAME";

   @RBEntry("Preferences relating to the presentation and operation of visualization features")
   public static final String PRIVATE_CONSTANT_1 = "VISUALIZATION_DESCRIPTION";

   @RBEntry("(from Visualization preferences)")
   @RBComment("must translate Visualization same as VISUALIZATION_DISPLAY_NAME")
   public static final String PRIVATE_CONSTANT_2 = "VISUALIZATION_ROUTING_TAG";

   @RBEntry("Thumbnail Display in Search Results")
   public static final String PRIVATE_CONSTANT_3 = "THUMBNAILS_ON_WC_SEARCH_DISPLAY_NAME";

   @RBEntry("Choose whether thumbnail images display in search results.")
   public static final String PRIVATE_CONSTANT_4 = "THUMBNAILS_ON_WC_SEARCH_SHORT_DESCRIPTION";

   @RBEntry("Choose whether thumbnail images display in search results.  Only objects viewable in Creo View have thumbnail images available.  Note that the search results load more quickly if thumbnail images are not displayed.  The system default is to not display the thumbnails.")
   public static final String PRIVATE_CONSTANT_5 = "THUMBNAILS_ON_WC_SEARCH_DESCRIPTION";

   @RBEntry("Display thumbnails in search results")
   public static final String PRIVATE_CONSTANT_6 = "THUMBNAILS_ON_WC_SEARCH_CHECKBOX_LABEL";

   @RBEntry("Thumbnail Display on Information Pages")
   public static final String PRIVATE_CONSTANT_7 = "THUMBNAILS_ON_PROP_PAGE_DISPLAY_NAME";

   @RBEntry("Choose whether thumbnail images display on Information pages.")
   public static final String PRIVATE_CONSTANT_8 = "THUMBNAILS_ON_PROP_PAGE_SHORT_DESCRIPTION";

   @RBEntry("Choose whether thumbnail images display on Information pages.  Only objects viewable in Creo View have thumbnail images available.  Note that the Information pages load more quickly if thumbnail images are not displayed.  The system default is to display the thumbnails.")
   public static final String PRIVATE_CONSTANT_9 = "THUMBNAILS_ON_PROP_PAGE_DESCRIPTION";

   @RBEntry("Display thumbnails on Information pages")
   public static final String PRIVATE_CONSTANT_10 = "THUMBNAILS_ON_PROP_PAGE_CHECKBOX_LABEL";

   @RBEntry("Thumbnail Display in the Visualization Home Page")
   public static final String PRIVATE_CONSTANT_11 = "THUMBNAILS_ON_VIS_PORTAL_DISPLAY_NAME";

   @RBEntry("Choose whether thumbnail images display in Visualization search results.")
   public static final String PRIVATE_CONSTANT_12 = "THUMBNAILS_ON_VIS_PORTAL_SHORT_DESCRIPTION";

   @RBEntry("Choose whether thumbnail images display in Visualization home page search results.  Only objects viewable in Creo View have thumbnail images available.  Note that the Visualization search results load more quickly if thumbnail images are not displayed.  The system default is to display the thumbnails.")
   public static final String PRIVATE_CONSTANT_13 = "THUMBNAILS_ON_VIS_PORTAL_DESCRIPTION";

   @RBEntry("Display thumbnails in Visualization search results")
   public static final String PRIVATE_CONSTANT_14 = "THUMBNAILS_ON_VIS_PORTAL_CHECKBOX_LABEL";

   @RBEntry("Tool Tips Display")
   public static final String PRIVATE_CONSTANT_15 = "TOOL_TIPS_DISPLAY_NAME";

   @RBEntry("Choose whether tool tips display on Visualization pages.")
   public static final String PRIVATE_CONSTANT_16 = "TOOL_TIPS_SHORT_DESCRIPTION";

   @RBEntry("Choose whether to display tool tips on the Visualization pages when the mouse cursor is over an icon.  Note that tool tips are not available for the <B>Publish Monitor</B> page.  The system default is to display the tool tips.")
   public static final String PRIVATE_CONSTANT_17 = "TOOL_TIPS_DESCRIPTION";

   @RBEntry("Display tool tips on Visualization pages")
   public static final String PRIVATE_CONSTANT_18 = "TOOL_TIPS_CHECKBOX_LABEL";

   @RBEntry("Loading Creo View Data")
   public static final String PRIVATE_CONSTANT_19 = "PRODUCT_VIEW_UPLOAD_DISPLAY_NAME";

   @RBEntry("Choose how to load files into Creo View.")
   public static final String PRIVATE_CONSTANT_20 = "PRODUCT_VIEW_UPLOAD_SHORT_DESCRIPTION";

   @RBEntry("Choose how to load files from Windchill into Creo View.  Loading all related files as a single PVZ file is best for viewing a large assembly.  Loading files on an as-needed basis is best for viewing a small assembly or pieces of a large assembly.  The system default is to load files as needed.")
   public static final String PRIVATE_CONSTANT_21 = "PRODUCT_VIEW_UPLOAD_DESCRIPTION";

   @RBEntry("Load all related files as a single PVZ file")
   public static final String PRIVATE_CONSTANT_22 = "PRODUCT_VIEW_UPLOAD_LOAD_ALL";

   @RBEntry("Load files as needed")
   public static final String PRIVATE_CONSTANT_23 = "PRODUCT_VIEW_UPLOAD_LOAD_ON_DEMAND";

   @RBEntry("Select Viewing Tool")
   public static final String PRIVATE_CONSTANT_24 = "USEPV_DISPLAY_NAME";

   @RBEntry("Choose either ProductView Standard Edition or ProductView Lite/Professional Edition as the Viewing tool. ")
   public static final String PRIVATE_CONSTANT_25 = "USEPV_SHORT_DESCRIPTION";

   @RBEntry("Choose whether ProductView Standard Edition is to be used as the default Visualization client, if not the ProductView Lite/Professional Edition will be used.")
   public static final String PRIVATE_CONSTANT_26 = "USEPV_DESCRIPTION";

   @RBEntry("Use ProductView Standard Edition ")
   public static final String PRIVATE_CONSTANT_27 = "USEPV_CHECKBOX_LABEL";

   @RBEntry("Autoload ProductView Data")
   public static final String PRIVATE_CONSTANT_28 = "PRODUCT_VIEW_AUTOLOAD_DISPLAY_NAME";

   @RBEntry("Choose how to autoload files when launching ProductView Standard.")
   public static final String PRIVATE_CONSTANT_29 = "PRODUCT_VIEW_AUTOLOAD_SHORT_DESCRIPTION";

   @RBEntry("Choose how to autoload files from Windchill when ProductView Standard is launched.  Options are all/none/single, single autoloads with one component.")
   public static final String PRIVATE_CONSTANT_30 = "PRODUCT_VIEW_AUTOLOAD_DESCRIPTION";

   @RBEntry("Autoload all files")
   public static final String PRIVATE_CONSTANT_31 = "PRODUCT_VIEW_AUTOLOAD_ALL";

   @RBEntry("Autoload none of the files")
   public static final String PRIVATE_CONSTANT_32 = "PRODUCT_VIEW_AUTOLOAD_NONE";

   @RBEntry("Autoload single file")
   public static final String PRIVATE_CONSTANT_33 = "PRODUCT_VIEW_AUTOLOAD_SINGLE";

   @RBEntry("Share Representations and Markups")
   public static final String PRIVATE_CONSTANT_34 = "SHARE_REPSNMARKUPS_DISPLAY_NAME";

   @RBEntry("Share representation and markups along with representables")
   public static final String PRIVATE_CONSTANT_35 = "SHARE_REPSNMARKUPS_SHORT_DESCRIPTION";

   @RBEntry("When this preference is enabled, the ad-hoc access control rules that are added to the representable at the time of a share are also added to the existing representations that are associated to that representable. Disabling this preference does not unshare already shared representations. You need to unshare the shared representable to restrict access to the shared representations. Note that the out-of-the-box access control policy for DerivedImage (for example, representations) grants full control for all users. You must change this policy to make use of this preference. (Default is Yes)")
   public static final String PRIVATE_CONSTANT_36 = "SHARE_REPSNMARKUPS_DESCRIPTION";

   @RBEntry("Creo to CATIA V5 CAD Conversion Attribute")
   public static final String PRIVATE_CONSTANT_37 = "PROE_TO_CATIAV5_CONVERSION_ATTRIBUTE_DISPLAY_NAME";

   @RBEntry("This preference defines the name of a boolean attribute that is set on the Creo source CAD document and is evaluated by the Creo to CATIA V5 CAD publisher. The attribute value determines whether the Creo CAD document is converted into CATIA V5 format by this server-side process.")
   public static final String PRIVATE_CONSTANT_38 = "PROE_TO_CATIAV5_CONVERSION_ATTRIBUTE_DESCRIPTION";

   @RBEntry("CATIA V5 to Creo CAD Conversion Attribute")
   public static final String PRIVATE_CONSTANT_39 = "CATIAV5_TO_PROE_CONVERSION_ATTRIBUTE_DISPLAY_NAME";

   @RBEntry("This preference defines the name of a boolean attribute that is set on the CATIA V5 source CAD document and is evaluated by the CAD publisher. The attribute value determines whether the CATIA V5 CAD document is converted into Creo format by this server-side process.")
   public static final String PRIVATE_CONSTANT_40 = "CATIAV5_TO_PROE_CONVERSION_ATTRIBUTE_DESCRIPTION";

   @RBEntry("ProductView Client in Visualization Tab")
   public static final String PRIVATE_CONSTANT_41 = "VISTABPVUI_DISPLAY_NAME";

   @RBEntry("Show ProductView Client in the Visualization Tab, if not the 3D view window will be displayed")
   public static final String PRIVATE_CONSTANT_42 = "VISTABPVUI_SHORT_DESCRIPTION";

   @RBEntry("Show ProductView Client in the Visualization Tab, if not the 3D view window will be displayed")
   public static final String PRIVATE_CONSTANT_43 = "VISTABPVUI_DESCRIPTION";
   
   @RBEntry("Copy Representations to Described Parts")
   public static final String PREFERENCE_COPY_TO_DESCRIBED_PARTS_CATEGORY = "PREFERENCE_COPY_TO_DESCRIBED_PARTS_CATEGORY";
   
   @RBEntry("Enabled")
   public static final String COPY_TO_DESCRIBED_PARTS_ENABLED_DISPLAY_NAME = "COPY_TO_DESCRIBED_PARTS_ENABLED_DISPLAY_NAME";

   @RBEntry("Enables representation copying when a representation is created.")
   public static final String COPY_TO_DESCRIBED_PARTS_ENABLED_SHORT_DESCRIPTION = "COPY_TO_DESCRIBED_PARTS_ENABLED_SHORT_DESCRIPTION";

   @RBEntry("Enables representation copying to described parts when a representation is created.  A described part is a WTPart that is associated to an EPMDocument in which the relationship is NOT an owner association.")
   public static final String COPY_TO_DESCRIBED_PARTS_ENABLED_DESCRIPTION = "COPY_TO_DESCRIBED_PARTS_ENABLED_DESCRIPTION";
   
   @RBEntry("Navigation")
   public static final String COPY_TO_DESCRIBED_PARTS_LINK_TYPE_DISPLAY_NAME = "COPY_TO_DESCRIBED_PARTS_LINK_TYPE_DISPLAY_NAME";

   @RBEntry("Specifies association types to navigate when locating described parts for the copy operation")
   public static final String COPY_TO_DESCRIBED_PARTS_LINK_TYPE_SHORT_DESCRIPTION = "COPY_TO_DESCRIBED_PARTS_LINK_TYPE_SHORT_DESCRIPTION";

   @RBEntry("Specifies association types to navigate when locating described parts for the copy operation. Non-owner Associations refer to Contributing Content, Image and Contributing Image associations. Non-Primary owner associations are all owner associations except for the first created association.")
   public static final String COPY_TO_DESCRIBED_PARTS_LINK_TYPE_DESCRIPTION = "COPY_TO_DESCRIBED_PARTS_LINK_TYPE_DESCRIPTION";
   
   @RBEntry("Non-owner associations")
   public static final String COPY_TO_DESCRIBED_PARTS_LINK_TYPE_BH = "COPY_TO_DESCRIBED_PARTS_LINK_TYPE_BH";

   @RBEntry("Non-primary owner associations")
   public static final String COPY_TO_DESCRIBED_PARTS_LINK_TYPE_BO = "COPY_TO_DESCRIBED_PARTS_LINK_TYPE_BO";
   
   @RBEntry("Content associations")
   public static final String COPY_TO_DESCRIBED_PARTS_LINK_TYPE_D = "COPY_TO_DESCRIBED_PARTS_LINK_TYPE_D";
   
   @RBEntry("All associations")
   public static final String COPY_TO_DESCRIBED_PARTS_LINK_TYPE_B = "COPY_TO_DESCRIBED_PARTS_LINK_TYPE_B";
   
   @RBEntry("Choose Which Representation to Copy")
   public static final String COPY_TO_DESCRIBED_PARTS_REP_TYPE_DISPLAY_NAME = "COPY_TO_DESCRIBED_PARTS_REP_TYPE_DISPLAY_NAME";

   @RBEntry("Specifies which representations to copy when a representation is created")
   public static final String COPY_TO_DESCRIBED_PARTS_REP_TYPE_SHORT_DESCRIPTION = "COPY_TO_DESCRIBED_PARTS_REP_TYPE_SHORT_DESCRIPTION";

   @RBEntry("Specifies which representations to copy when a representation is created - either the default representation, or any representation.")
   public static final String COPY_TO_DESCRIBED_PARTS_REP_TYPE_DESCRIPTION = "COPY_TO_DESCRIBED_PARTS_REP_TYPE_DESCRIPTION";
   
   @RBEntry("Default Representation")
   public static final String COPY_TO_DESCRIBED_PARTS_REP_TYPE_D = "COPY_TO_DESCRIBED_PARTS_REP_TYPE_D";
   
   @RBEntry("Any Representation")
   public static final String COPY_TO_DESCRIBED_PARTS_REP_TYPE_A = "COPY_TO_DESCRIBED_PARTS_REP_TYPE_A";
   
   @RBEntry("Include Markups")
   public static final String COPY_TO_DESCRIBED_PARTS_MARKUPS_DISPLAY_NAME = "COPY_TO_DESCRIBED_PARTS_MARKUPS_DISPLAY_NAME";

   @RBEntry("Specifies whether to include markups when copying a representation to described parts")
   public static final String COPY_TO_DESCRIBED_PARTS_MARKUPS_SHORT_DESCRIPTION = "COPY_TO_DESCRIBED_PARTS_MARKUPS_SHORT_DESCRIPTION";

   @RBEntry("Specifies whether to include markups when copying a representation to described parts.")
   public static final String COPY_TO_DESCRIBED_PARTS_MARKUPS_DESCRIPTION = "COPY_TO_DESCRIBED_PARTS_MARKUPS_DESCRIPTION";
   
   @RBEntry("Require owner association")
   public static final String COPY_TO_DESCRIBED_PARTS_OWNER_LINK_DISPLAY_NAME = "COPY_TO_DESCRIBED_PARTS_OWNER_LINK_DISPLAY_NAME";

   @RBEntry("Specifies whether an owner association to a part is required in order for the representation to be copied to described parts")
   public static final String COPY_TO_DESCRIBED_PARTS_OWNER_LINK_SHORT_DESCRIPTION = "COPY_TO_DESCRIBED_PARTS_OWNER_LINK_SHORT_DESCRIPTION";

   @RBEntry("Specifies whether an owner association to a part is required in order for the representation to be copied to described parts.")
   public static final String COPY_TO_DESCRIBED_PARTS_OWNER_LINK_DESCRIPTION = "COPY_TO_DESCRIBED_PARTS_OWNER_LINK_DESCRIPTION";
   
   @RBEntry("Latest Iteration")
   public static final String COPY_TO_DESCRIBED_PARTS_LATEST_ITERATION_DISPLAY_NAME = "COPY_TO_DESCRIBED_PARTS_LATEST_ITERATION_DISPLAY_NAME";

   @RBEntry("Allows representations to be copied to described parts only if the representable being published is the latest iteration.")
   public static final String COPY_TO_DESCRIBED_PARTS_LATEST_ITERATION_SHORT_DESCRIPTION = "COPY_TO_DESCRIBED_PARTS_LATEST_ITERATION_SHORT_DESCRIPTION";

   @RBEntry("Allows representations to be copied to described parts only if the representable being published is the latest iteration.")
   public static final String COPY_TO_DESCRIBED_PARTS_LATEST_ITERATION_DESCRIPTION = "COPY_TO_DESCRIBED_PARTS_LATEST_ITERATION_DESCRIPTION";
   
   @RBEntry("List Representation File Extension Types")
   public static final String LIST_REP_FILE_TYPES_DISPLAY_NAME = "LIST_REP_FILE_TYPES_DISPLAY_NAME";

   @RBEntry("Specifies file types that allow corresponding content download links in the representation list.")
   public static final String LIST_REP_FILE_TYPES_SHORT_DESCRIPTION = "LIST_REP_FILE_TYPES_SHORT_DESCRIPTION";

   @RBEntry("A comma-separated list which specifies the file extension types that can be downloaded for a representation in the representation list. The preference value ALL allows every file type to be downloaded.")
   public static final String LIST_REP_FILE_TYPES_DESCRIPTION = "LIST_REP_FILE_TYPES_DESCRIPTION";   
   
   @RBEntry("Batch Print with Print Services")
   public static final String PREFERENCE_BATCH_PRINT_CATEGORY = "PREFERENCE_BATCH_PRINT_CATEGORY";

   @RBEntry("Enabled")
   public static final String BATCH_PRINT_ENABLED_DISPLAY_NAME = "BATCH_PRINT_ENABLED_DISPLAY_NAME";

   @RBEntry("Enables Batch Printing")
   public static final String BATCH_PRINT_ENABLED_SHORT_DESCRIPTION = "BATCH_PRINT_ENABLED_SHORT_DESCRIPTION";

   @RBEntry("Enables Batch Printing (Default is No).")
   public static final String BATCH_PRINT_ENABLED_DESCRIPTION = "BATCH_PRINT_ENABLED_DESCRIPTION";

   @RBEntry("Printable File Types")
   public static final String BATCH_PRINT_PRINTABLE_TYPES_DISPLAY_NAME = "BATCH_PRINT_PRINTABLE_TYPES_DISPLAY_NAME";

   @RBEntry("Specifies Printable File Types")
   public static final String BATCH_PRINT_PRINTABLE_TYPES_SHORT_DESCRIPTION = "BATCH_PRINT_PRINTABLE_TYPES_SHORT_DESCRIPTION";

   @RBEntry("Comma separated list of Printable File Types (Defaults are model, document, drawing, image, illustration, ecad)")
   public static final String BATCH_PRINT_PRINTABLE_TYPES_DESCRIPTION = "BATCH_PRINT_PRINTABLE_TYPES_DESCRIPTION";

   @RBEntry("Interference Detection")
   public static final String PREFERENCE_BATCH_CLASH_CATEGORY = "PREFERENCE_BATCH_CLASH_CATEGORY";

   @RBEntry("Enabled")
   public static final String BATCH_CLASH_ENABLED_DISPLAY_NAME = "BATCH_CLASH_ENABLED_DISPLAY_NAME";

   @RBEntry("Enables Interference Detection")
   public static final String BATCH_CLASH_ENABLED_SHORT_DESCRIPTION = "BATCH_CLASH_ENABLED_SHORT_DESCRIPTION";

   @RBEntry("Enables Interference Detection (Default is No).")
   public static final String BATCH_CLASH_ENABLED_DESCRIPTION = "BATCH_CLASH_ENABLED_DESCRIPTION";

   @RBEntry("Interference Detection Definition Type")
   public static final String BATCH_CLASH_DEFINITION_SOFTTYPE_DISPLAY_NAME = "BATCH_CLASH_DEFINITION_SOFTTYPE_DISPLAY_NAME";

   @RBEntry("Defines Document Type for Interference Detection Definitions")
   public static final String BATCH_CLASH_DEFINITION_SOFTTYPE_SHORT_DESCRIPTION = "BATCH_CLASH_DEFINITION_SOFTTYPE_SHORT_DESCRIPTION";

   @RBEntry("The fully qualified logical ID for Interference Detection Definition Documents (Default is wt.doc.WTDocument|com.ptc.InterferenceDetectionDefinition).")
   public static final String BATCH_CLASH_DEFINITION_SOFTTYPE_DESCRIPTION = "BATCH_CLASH_DEFINITION_SOFTTYPE_DESCRIPTION";

   @RBEntry("Interference Detection Definition Units of Measure")
   public static final String BATCH_CLASH_DEFINITION_UNITS_DISPLAY_NAME = "BATCH_CLASH_DEFINITION_UNITS_DISPLAY_NAME";

   @RBEntry("Defines Units of Measure for New Interference Detection Definitions")
   public static final String BATCH_CLASH_DEFINITION_UNITS_SHORT_DESCRIPTION = "BATCH_CLASH_DEFINITION_UNITS_SHORT_DESCRIPTION";

   @RBEntry("Defines Units of Measure for New Interference Detection Definitions (Default is MM).")
   public static final String BATCH_CLASH_DEFINITION_UNITS_DESCRIPTION = "BATCH_CLASH_DEFINITION_UNITS_DESCRIPTION";

   @RBEntry("Meters")
   public static final String BATCH_CLASH_DEFINITION_UNITS_M = "BATCH_CLASH_DEFINITION_UNITS_M";

   @RBEntry("Decimeters")
   public static final String BATCH_CLASH_DEFINITION_UNITS_DM = "BATCH_CLASH_DEFINITION_UNITS_DM";

   @RBEntry("Centimeters")
   public static final String BATCH_CLASH_DEFINITION_UNITS_CM = "BATCH_CLASH_DEFINITION_UNITS_CM";

   @RBEntry("Millimeters")
   public static final String BATCH_CLASH_DEFINITION_UNITS_MM = "BATCH_CLASH_DEFINITION_UNITS_MM";

   @RBEntry("Feet")
   public static final String BATCH_CLASH_DEFINITION_UNITS_FT = "BATCH_CLASH_DEFINITION_UNITS_FT";

   @RBEntry("Inches")
   public static final String BATCH_CLASH_DEFINITION_UNITS_IN = "BATCH_CLASH_DEFINITION_UNITS_IN";

   @RBEntry("Default Tolerable Penetration")
   public static final String BATCH_CLASH_DEFINITION_PENETRATION_DEFAULT_DISPLAY_NAME = "BATCH_CLASH_DEFINITION_PENETRATION_DEFAULT_DISPLAY_NAME";

   @RBEntry("Default Tolerable Penetration Value for New Interference Detection Definitions")
   public static final String BATCH_CLASH_DEFINITION_PENETRATION_DEFAULT_SHORT_DESCRIPTION = "BATCH_CLASH_DEFINITION_PENETRATION_DEFAULT_SHORT_DESCRIPTION";

   @RBEntry("Specifies Default Tolerable Penetration Value for New Interference Detection Definitions (Default is 0.0 in Default Units)")
   public static final String BATCH_CLASH_DEFINITION_PENETRATION_DEFAULT_DESCRIPTION = "BATCH_CLASH_DEFINITION_PENETRATION_DEFAULT_DESCRIPTION";

   @RBEntry("Default Tolerance")
   public static final String BATCH_CLASH_DEFINITION_TOLERANCE_DEFAULT_DISPLAY_NAME = "BATCH_CLASH_DEFINITION_TOLERANCE_DEFAULT_DISPLAY_NAME";

   @RBEntry("Default Tolerance Value for New Interference Detection Definitions")
   public static final String BATCH_CLASH_DEFINITION_TOLERANCE_DEFAULT_SHORT_DESCRIPTION = "BATCH_CLASH_DEFINITION_TOLERANCE_DEFAULT_SHORT_DESCRIPTION";

   @RBEntry("Specifies Default Tolerance Value for New Interference Detection Definitions (Default is 0.0 in Default Units)")
   public static final String BATCH_CLASH_DEFINITION_TOLERANCE_DEFAULT_DESCRIPTION = "BATCH_CLASH_DEFINITION_TOLERANCE_DEFAULT_DESCRIPTION";

   @RBEntry("Default Required Clearance")
   public static final String BATCH_CLASH_DEFINITION_CLEARANCE_DEFAULT_DISPLAY_NAME = "BATCH_CLASH_DEFINITION_CLEARANCE_DEFAULT_DISPLAY_NAME";

   @RBEntry("Default Required Clearance Value for New Interference Detection Definitions")
   public static final String BATCH_CLASH_DEFINITION_CLEARANCE_DEFAULT_SHORT_DESCRIPTION = "BATCH_CLASH_DEFINITION_CLEARANCE_DEFAULT_SHORT_DESCRIPTION";

   @RBEntry("Default Required Clearance Value for New Interference Detection Definitions (Default is 0.01 in Default Units)")
   public static final String BATCH_CLASH_DEFINITION_CLEARANCE_DEFAULT_DESCRIPTION = "BATCH_CLASH_DEFINITION_CLEARANCE_DEFAULT_DESCRIPTION";

   @RBEntry("Interference Lifecycle Template")
   public static final String BATCH_CLASH_CLASH_LIFECYCLE_TEMPLATE_DISPLAY_NAME = "BATCH_CLASH_CLASH_LIFECYCLE_TEMPLATE_DISPLAY_NAME";

   @RBEntry("Specifies the name of the Lifecycle Template to assign to Interferences, Contacts, and Clearances")
   public static final String BATCH_CLASH_CLASH_LIFECYCLE_TEMPLATE_SHORT_DESCRIPTION = "BATCH_CLASH_CLASH_LIFECYCLE_TEMPLATE_SHORT_DESCRIPTION";

   @RBEntry("Specifies the name of the Lifecycle Template to assign to Interferences, Contacts, and Clearances (Default is Interference)")
   public static final String BATCH_CLASH_CLASH_LIFECYCLE_TEMPLATE_DESCRIPTION = "BATCH_CLASH_CLASH_LIFECYCLE_TEMPLATE_DESCRIPTION";

   @RBEntry("Interference Report Lifecycle Template")
   public static final String BATCH_CLASH_CLASH_REPORT_LIFECYCLE_TEMPLATE_DISPLAY_NAME = "BATCH_CLASH_CLASH_REPORT_LIFECYCLE_TEMPLATE_DISPLAY_NAME";

   @RBEntry("Specifies the name of the Lifecycle Template to assign to Interference Reports")
   public static final String BATCH_CLASH_CLASH_REPORT_LIFECYCLE_TEMPLATE_SHORT_DESCRIPTION = "BATCH_CLASH_CLASH_REPORT_LIFECYCLE_TEMPLATE_SHORT_DESCRIPTION";

   @RBEntry("Specifies the name of the Lifecycle Template to assign to Interference Reports (Default is Interference Report)")
   public static final String BATCH_CLASH_CLASH_REPORT_LIFECYCLE_TEMPLATE_DESCRIPTION = "BATCH_CLASH_CLASH_REPORT_LIFECYCLE_TEMPLATE_DESCRIPTION";

   @RBEntry("Object Types to Associate Interference Detections to")
   public static final String BATCH_CLASH_REPORT_LINKAGE_DISPLAY_NAME = "BATCH_CLASH_REPORT_LINKAGE_DISPLAY_NAME";

   @RBEntry("Specifies the Object Types to Associate Interference Detections to")
   public static final String BATCH_CLASH_REPORT_LINKAGE_SHORT_DESCRIPTION = "BATCH_CLASH_REPORT_LINKAGE_SHORT_DESCRIPTION";

   @RBEntry("1. WTPART - All new Interference Detection instances will have a link created to the two WTParts that are interfering. If the representation doesn't have WTPart references to the interfering parts, then the Interference Detection instance will not be referenced to any interfering parts.<br>2. EPMDOCUMENT -  All new Interference Detection instances will have a link created to the two EPMDocuments that are interfering. If the representation doesn't have EPMDocument references to the interfering parts, then the Interference Detection instance will not be referenced to any interfering parts.<br>3. WTPART_EPMDOCUMENT (Default) - All new Interference Detection instances will have a link created to the two WTParts that are interfering, if a WTPart reference exists. Otherwise, if the representation doesn't have a reference to the interfering WTParts, then the reference to the EPMDocuments will be used instead.<br>4. EPMDOCUMENT_WTPART - All new Interference Detection instances will have a link created to the two EPMDocuments that are interfering, if an EPMDocument reference exists. Otherwise, if the representation doesn't have a reference to the interfering EPMDocuments, then the reference to the WTParts will be used instead.<br>")
   public static final String BATCH_CLASH_REPORT_LINKAGE_DESCRIPTION = "BATCH_CLASH_REPORT_LINKAGE_DESCRIPTION";

   @RBEntry("WTPART")
   public static final String BATCH_CLASH_REPORT_LINKAGE_WTPART = "BATCH_CLASH_REPORT_LINKAGE_WTPART";

   @RBEntry("EPMDOCUMENT")
   public static final String BATCH_CLASH_REPORT_LINKAGE_EPMDOCUMENT = "BATCH_CLASH_REPORT_LINKAGE_EPMDOCUMENT";

   @RBEntry("WTPART_EPMDOCUMENT")
   public static final String BATCH_CLASH_REPORT_LINKAGE_WTPART_EPMDOCUMENT = "BATCH_CLASH_REPORT_LINKAGE_WTPART_EPMDOCUMENT";

   @RBEntry("EPMDOCUMENT_WTPART")
   public static final String BATCH_CLASH_REPORT_LINKAGE_EPMDOCUMENT_WTPART = "BATCH_CLASH_REPORT_LINKAGE_EPMDOCUMENT_WTPART";

   @RBEntry("Adobe LiveCycle server")
   public static final String PREFERENCE_LIVE_CYCLE_SERVER_CATEGORY = "PREFERENCE_LIVE_CYCLE_SERVER_CATEGORY";

   @RBEntry("Reader Extensions Enabled")
   public static final String LIVE_CYCLE_SERVER_TAGGING_ENABLED_DISPLAY_NAME = "LIVE_CYCLE_SERVER_TAGGING_ENABLED_DISPLAY_NAME";

   @RBEntry("Enables Reader Extensions for applying to PDF files using a configured Adobe LiveCycle server")
   public static final String LIVE_CYCLE_SERVER_TAGGING_ENABLED_SHORT_DESCRIPTION = "LIVE_CYCLE_SERVER_TAGGING_ENABLED_SHORT_DESCRIPTION";

   @RBEntry("Enables Reader Extensions for applying to PDF files to allow their markup with Creo View and Adobe Reader using a configured Adobe LiveCycle server. (Default is No).")
   public static final String LIVE_CYCLE_SERVER_TAGGING_ENABLED_DESCRIPTION = "LIVE_CYCLE_SERVER_TAGGING_ENABLED_DESCRIPTION";

   @RBEntry("DDX Template Enabled")
   public static final String LIVE_CYCLE_SERVER_DDXTEMPLATE_ENABLED_DISPLAY_NAME = "LIVE_CYCLE_SERVER_DDXTEMPLATE_ENABLED_DISPLAY_NAME";

   @RBEntry("Enables PDF files to be processed with a DDX template using a configured Adobe LiveCycle server")
   public static final String LIVE_CYCLE_SERVER_DDXTEMPLATE_ENABLED_SHORT_DESCRIPTION = "LIVE_CYCLE_SERVER_DDXTEMPLATE_ENABLED_SHORT_DESCRIPTION";

   @RBEntry("Enables PDF files to be processed with instructions defined in a DDX template using a configured Adobe LiveCycle server. An example using this capability is to apply watermarks on PDF files during the publishing process (Default is No).")
   public static final String LIVE_CYCLE_SERVER_DDXTEMPLATE_ENABLED_DESCRIPTION = "LIVE_CYCLE_SERVER_DDXTEMPLATE_ENABLED_DESCRIPTION";

}
