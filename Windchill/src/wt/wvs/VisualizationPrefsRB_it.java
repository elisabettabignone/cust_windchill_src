/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.wvs;

import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.wvs.VisualizationPrefsRB")
public final class VisualizationPrefsRB_it extends WTListResourceBundle {
   /**
    * ##########################################################################
    * These text strings are used to describe visualization preferences
    * on the Visualization tab of the user preferences html client
    * ##########################################################################
    **/
   @RBEntry("Visualizzazione")
   public static final String PRIVATE_CONSTANT_0 = "VISUALIZATION_DISPLAY_NAME";

   @RBEntry("Preferenze relative alla presentazione e utilizzo delle caratteristiche di visualizzazione")
   public static final String PRIVATE_CONSTANT_1 = "VISUALIZATION_DESCRIPTION";

   @RBEntry("(dalle preferenze di visualizzazione)")
   @RBComment("must translate Visualization same as VISUALIZATION_DISPLAY_NAME")
   public static final String PRIVATE_CONSTANT_2 = "VISUALIZATION_ROUTING_TAG";

   @RBEntry("Visualizzazione delle miniature nei risultati della ricerca")
   public static final String PRIVATE_CONSTANT_3 = "THUMBNAILS_ON_WC_SEARCH_DISPLAY_NAME";

   @RBEntry("Scegliere se visualizzare le miniature nei risultati di ricerca.")
   public static final String PRIVATE_CONSTANT_4 = "THUMBNAILS_ON_WC_SEARCH_SHORT_DESCRIPTION";

   @RBEntry("Scegliere se visualizzare le miniature nei risultati della ricerca. Solo gli oggetti visualizzabili in Creo View dispongono di miniature. Si tenga presente che i risultati della ricerca vengono caricati più velocemente se le miniature non sono visualizzate. Per default, il sistema non visualizza le miniature.")
   public static final String PRIVATE_CONSTANT_5 = "THUMBNAILS_ON_WC_SEARCH_DESCRIPTION";

   @RBEntry("Visualizza le miniature nei risultati della ricerca")
   public static final String PRIVATE_CONSTANT_6 = "THUMBNAILS_ON_WC_SEARCH_CHECKBOX_LABEL";

   @RBEntry("Visualizzazione miniature sulle pagine delle informazioni")
   public static final String PRIVATE_CONSTANT_7 = "THUMBNAILS_ON_PROP_PAGE_DISPLAY_NAME";

   @RBEntry("Scegliere se visualizzare le miniature sulle pagine delle informazioni.")
   public static final String PRIVATE_CONSTANT_8 = "THUMBNAILS_ON_PROP_PAGE_SHORT_DESCRIPTION";

   @RBEntry("Scegliere se visualizzare le miniature sulle pagine delle informazioni. Solo gli oggetti visualizzabili in Creo View dispongono di miniature. Si tenga presente che le pagine delle informazioni vengono caricate più velocemente se le miniature non sono visualizzate. Per default, il sistema visualizza le miniature.")
   public static final String PRIVATE_CONSTANT_9 = "THUMBNAILS_ON_PROP_PAGE_DESCRIPTION";

   @RBEntry("Visualizza le miniature sulle pagine delle informazioni")
   public static final String PRIVATE_CONSTANT_10 = "THUMBNAILS_ON_PROP_PAGE_CHECKBOX_LABEL";

   @RBEntry("Visualizzazione miniature sulla home page di Visualization")
   public static final String PRIVATE_CONSTANT_11 = "THUMBNAILS_ON_VIS_PORTAL_DISPLAY_NAME";

   @RBEntry("Scegliere se visualizzare le miniature nei risultati di ricerca in Visualization.")
   public static final String PRIVATE_CONSTANT_12 = "THUMBNAILS_ON_VIS_PORTAL_SHORT_DESCRIPTION";

   @RBEntry("Scegliere se visualizzare le miniature nei risultati della ricerca della home page di Visualization. Solo gli oggetti visualizzabili in Creo View dispongono di miniature. Si tenga presente che i risultati della ricerca di Visualization vengono caricati più velocemente se le miniature non sono visualizzate. Per default, il sistema visualizza le miniature.")
   public static final String PRIVATE_CONSTANT_13 = "THUMBNAILS_ON_VIS_PORTAL_DESCRIPTION";

   @RBEntry("Visualizza le miniature nei risultati della ricerca di Visualization")
   public static final String PRIVATE_CONSTANT_14 = "THUMBNAILS_ON_VIS_PORTAL_CHECKBOX_LABEL";

   @RBEntry("Visualizzazione delle descrizioni dei comandi")
   public static final String PRIVATE_CONSTANT_15 = "TOOL_TIPS_DISPLAY_NAME";

   @RBEntry("Scegliere se visualizzare le descrizioni comandi sulle pagine di Visualization")
   public static final String PRIVATE_CONSTANT_16 = "TOOL_TIPS_SHORT_DESCRIPTION";

   @RBEntry("Scegliere se visualizzare le descrizioni comandi sulle pagine di Visualization quando il cursore del mouse si trova sopra un'icona. Tenere presente che le descrizioni comandi non sono disponibili per la pagina <B>Monitor pubblicazione</B>. Per default, il sistema visualizza le descrizioni comandi.")
   public static final String PRIVATE_CONSTANT_17 = "TOOL_TIPS_DESCRIPTION";

   @RBEntry("Visualizza le descrizioni comandi sulle pagine di Visualization")
   public static final String PRIVATE_CONSTANT_18 = "TOOL_TIPS_CHECKBOX_LABEL";

   @RBEntry("Caricamento di dati Creo View")
   public static final String PRIVATE_CONSTANT_19 = "PRODUCT_VIEW_UPLOAD_DISPLAY_NAME";

   @RBEntry("Scegliere la modalità di caricamento dei file in Creo View")
   public static final String PRIVATE_CONSTANT_20 = "PRODUCT_VIEW_UPLOAD_SHORT_DESCRIPTION";

   @RBEntry("Scegliere come caricare i file in Creo View da Windchill. Il caricamento di tutti i file correlati come un unico file PVZ è la migliore soluzione per la visualizzazione di un assieme di grandi dimensioni. Per visualizzare assiemi di piccole dimensioni o porzioni di un grande assieme è preferibile scegliere di caricare in base alle necessità. Per default, il sistema carica i file in base alla necessità.")
   public static final String PRIVATE_CONSTANT_21 = "PRODUCT_VIEW_UPLOAD_DESCRIPTION";

   @RBEntry("Carica tutti i file correlati come un unico file PVZ")
   public static final String PRIVATE_CONSTANT_22 = "PRODUCT_VIEW_UPLOAD_LOAD_ALL";

   @RBEntry("Carica i file in base alla necessità")
   public static final String PRIVATE_CONSTANT_23 = "PRODUCT_VIEW_UPLOAD_LOAD_ON_DEMAND";

   @RBEntry("Seleziona strumento di visualizzazione")
   public static final String PRIVATE_CONSTANT_24 = "USEPV_DISPLAY_NAME";

   @RBEntry("Scegliere ProductView Standard Edition o ProductView Lite/Professional Edition come strumento di visualizzazione. ")
   public static final String PRIVATE_CONSTANT_25 = "USEPV_SHORT_DESCRIPTION";

   @RBEntry("Decidere se usare ProductView Standard Edition come client Visualization di default. Altrimenti verrà usato ProductView Lite/Professional Edition.")
   public static final String PRIVATE_CONSTANT_26 = "USEPV_DESCRIPTION";

   @RBEntry("Usa ProductView Standard Edition ")
   public static final String PRIVATE_CONSTANT_27 = "USEPV_CHECKBOX_LABEL";

   @RBEntry("Caricamento automatico di dati ProductView")
   public static final String PRIVATE_CONSTANT_28 = "PRODUCT_VIEW_AUTOLOAD_DISPLAY_NAME";

   @RBEntry("Scegliere la modalità di caricamento automatico dei file all'avvio di ProductView Standard.")
   public static final String PRIVATE_CONSTANT_29 = "PRODUCT_VIEW_AUTOLOAD_SHORT_DESCRIPTION";

   @RBEntry("Scegliere la modalità di caricamento automatico di file da Windchill all'avvio di ProductView Standard. Le opzioni sono tuti/nessuno/singolo. Singolo effettua il caricamento automatico con un componente.")
   public static final String PRIVATE_CONSTANT_30 = "PRODUCT_VIEW_AUTOLOAD_DESCRIPTION";

   @RBEntry("Caricamento automatico - tutti i file")
   public static final String PRIVATE_CONSTANT_31 = "PRODUCT_VIEW_AUTOLOAD_ALL";

   @RBEntry("Caricamento automatico - nessun file")
   public static final String PRIVATE_CONSTANT_32 = "PRODUCT_VIEW_AUTOLOAD_NONE";

   @RBEntry("Caricamento automatico - file singolo")
   public static final String PRIVATE_CONSTANT_33 = "PRODUCT_VIEW_AUTOLOAD_SINGLE";

   @RBEntry("Condividi rappresentazioni e markup")
   public static final String PRIVATE_CONSTANT_34 = "SHARE_REPSNMARKUPS_DISPLAY_NAME";

   @RBEntry("Consente di condividere rappresentazioni e markup con oggetti rappresentabili")
   public static final String PRIVATE_CONSTANT_35 = "SHARE_REPSNMARKUPS_SHORT_DESCRIPTION";

   @RBEntry("Quando questa preferenza è attiva, le regole di controllo di accesso ad hoc, aggiunte all'oggetto rappresentabile al momento di una condivisione, vengono aggiunte anche alle rappresentazioni esistenti associate all'oggetto rappresentabile. La disattivazione di questa preferenza non comporta la rimozione automatica della condivisione delle rappresentazioni già condivise. Per limitare l'accesso alle rappresentazioni condivise, sarà necessario rimuovere la condivisione dell'oggetto rappresentabile. Si noti che le regole di controllo di accesso predefinite di DerivedImage (ad esempio le rappresentazioni) concedono il controllo completo a tutti gli utenti. È necessario modificare la regola per utilizzare la preferenza. L'impostazione di default è Sì.")
   public static final String PRIVATE_CONSTANT_36 = "SHARE_REPSNMARKUPS_DESCRIPTION";

   @RBEntry("Attributo di conversione CAD da Creo a CATIA V5")
   public static final String PRIVATE_CONSTANT_37 = "PROE_TO_CATIAV5_CONVERSION_ATTRIBUTE_DISPLAY_NAME";

   @RBEntry("Questa preferenza definisce il nome di un attributo booleano impostato sul documento CAD di origine di Creo e viene valutato dal server di pubblicazione CAD Creo - CATIA V5. Il valore dell'attributo determina se il documento CAD di Creo viene convertito in formato CATIA V5 dal processo lato server.")
   public static final String PRIVATE_CONSTANT_38 = "PROE_TO_CATIAV5_CONVERSION_ATTRIBUTE_DESCRIPTION";

   @RBEntry("Attributo di conversione CAD da CATIA V5 a Creo")
   public static final String PRIVATE_CONSTANT_39 = "CATIAV5_TO_PROE_CONVERSION_ATTRIBUTE_DISPLAY_NAME";

   @RBEntry("Questa preferenza definisce il nome di un attributo booleano impostato sul documento CAD di origine di CATIA V5 e viene valutato dal server di pubblicazione CAD. Il valore dell'attributo determina se il documento CAD di CATIA V5 viene convertito in formato Creo dal processo lato server.")
   public static final String PRIVATE_CONSTANT_40 = "CATIAV5_TO_PROE_CONVERSION_ATTRIBUTE_DESCRIPTION";

   @RBEntry("Client ProductView nella scheda Visualization")
   public static final String PRIVATE_CONSTANT_41 = "VISTABPVUI_DISPLAY_NAME";

   @RBEntry("Mostra il client ProductView nella scheda Visualization. In caso contrario verrà visualizzata la finestra della vista 3D")
   public static final String PRIVATE_CONSTANT_42 = "VISTABPVUI_SHORT_DESCRIPTION";

   @RBEntry("Mostra il client ProductView nella scheda Visualization. In caso contrario verrà visualizzata la finestra della vista 3D")
   public static final String PRIVATE_CONSTANT_43 = "VISTABPVUI_DESCRIPTION";
   
   @RBEntry("Copia rappresentazioni nelle parti descritte")
   public static final String PREFERENCE_COPY_TO_DESCRIBED_PARTS_CATEGORY = "PREFERENCE_COPY_TO_DESCRIBED_PARTS_CATEGORY";
   
   @RBEntry("Attivata")
   public static final String COPY_TO_DESCRIBED_PARTS_ENABLED_DISPLAY_NAME = "COPY_TO_DESCRIBED_PARTS_ENABLED_DISPLAY_NAME";

   @RBEntry("Attiva la copia rappresentazioni quando viene creata una rappresentazione.")
   public static final String COPY_TO_DESCRIBED_PARTS_ENABLED_SHORT_DESCRIPTION = "COPY_TO_DESCRIBED_PARTS_ENABLED_SHORT_DESCRIPTION";

   @RBEntry("Attiva la copia rappresentazioni quando viene creata una rappresentazione. Una parte descritta è una WTPart associata a un EPMDocument nel quale la relazione NON è un'associazione proprietario.")
   public static final String COPY_TO_DESCRIBED_PARTS_ENABLED_DESCRIPTION = "COPY_TO_DESCRIBED_PARTS_ENABLED_DESCRIPTION";
   
   @RBEntry("Navigazione")
   public static final String COPY_TO_DESCRIBED_PARTS_LINK_TYPE_DISPLAY_NAME = "COPY_TO_DESCRIBED_PARTS_LINK_TYPE_DISPLAY_NAME";

   @RBEntry("Specifica i tipi di associazione in cui spostarsi quando si individuano le parti descritte per l'operazione di copia")
   public static final String COPY_TO_DESCRIBED_PARTS_LINK_TYPE_SHORT_DESCRIPTION = "COPY_TO_DESCRIBED_PARTS_LINK_TYPE_SHORT_DESCRIPTION";

   @RBEntry("Specifica i tipi di associazione in cui spostarsi quando si individuano le parti descritte per l'operazione di copia. Le associazioni non proprietario fanno riferimento alle associazioni contenuto contribuente, immagine e immagine contribuente. Le associazioni proprietario non principali sono tutte associazioni proprietario tranne per la prima associazione creata.")
   public static final String COPY_TO_DESCRIBED_PARTS_LINK_TYPE_DESCRIPTION = "COPY_TO_DESCRIBED_PARTS_LINK_TYPE_DESCRIPTION";
   
   @RBEntry("Associazioni non proprietario")
   public static final String COPY_TO_DESCRIBED_PARTS_LINK_TYPE_BH = "COPY_TO_DESCRIBED_PARTS_LINK_TYPE_BH";

   @RBEntry("Associazioni proprietario non principali")
   public static final String COPY_TO_DESCRIBED_PARTS_LINK_TYPE_BO = "COPY_TO_DESCRIBED_PARTS_LINK_TYPE_BO";
   
   @RBEntry("Associazioni contenuto")
   public static final String COPY_TO_DESCRIBED_PARTS_LINK_TYPE_D = "COPY_TO_DESCRIBED_PARTS_LINK_TYPE_D";
   
   @RBEntry("Tutte le associazioni")
   public static final String COPY_TO_DESCRIBED_PARTS_LINK_TYPE_B = "COPY_TO_DESCRIBED_PARTS_LINK_TYPE_B";
   
   @RBEntry("Scegli la rappresentazione da copiare")
   public static final String COPY_TO_DESCRIBED_PARTS_REP_TYPE_DISPLAY_NAME = "COPY_TO_DESCRIBED_PARTS_REP_TYPE_DISPLAY_NAME";

   @RBEntry("Specifica le rappresentazioni da copiare quando viene creata una rappresentazione")
   public static final String COPY_TO_DESCRIBED_PARTS_REP_TYPE_SHORT_DESCRIPTION = "COPY_TO_DESCRIBED_PARTS_REP_TYPE_SHORT_DESCRIPTION";

   @RBEntry("Specifica le rappresentazioni da copiare quando viene creata una rappresentazione, di default o di qualsiasi altro tipo.")
   public static final String COPY_TO_DESCRIBED_PARTS_REP_TYPE_DESCRIPTION = "COPY_TO_DESCRIBED_PARTS_REP_TYPE_DESCRIPTION";
   
   @RBEntry("Rappresentazione di default")
   public static final String COPY_TO_DESCRIBED_PARTS_REP_TYPE_D = "COPY_TO_DESCRIBED_PARTS_REP_TYPE_D";
   
   @RBEntry("Qualsiasi rappresentazione")
   public static final String COPY_TO_DESCRIBED_PARTS_REP_TYPE_A = "COPY_TO_DESCRIBED_PARTS_REP_TYPE_A";
   
   @RBEntry("Includi markup")
   public static final String COPY_TO_DESCRIBED_PARTS_MARKUPS_DISPLAY_NAME = "COPY_TO_DESCRIBED_PARTS_MARKUPS_DISPLAY_NAME";

   @RBEntry("Specifica se includere markup quando si copia una rappresentazione nelle parti descritte")
   public static final String COPY_TO_DESCRIBED_PARTS_MARKUPS_SHORT_DESCRIPTION = "COPY_TO_DESCRIBED_PARTS_MARKUPS_SHORT_DESCRIPTION";

   @RBEntry("Specifica se includere markup quando si copia una rappresentazione nelle parti descritte.")
   public static final String COPY_TO_DESCRIBED_PARTS_MARKUPS_DESCRIPTION = "COPY_TO_DESCRIBED_PARTS_MARKUPS_DESCRIPTION";
   
   @RBEntry("Richiedi associazione proprietario")
   public static final String COPY_TO_DESCRIBED_PARTS_OWNER_LINK_DISPLAY_NAME = "COPY_TO_DESCRIBED_PARTS_OWNER_LINK_DISPLAY_NAME";

   @RBEntry("Specifica se un'associazione proprietario a una parte è necessaria affinché la rappresentazione venga copiata nelle parti descritte")
   public static final String COPY_TO_DESCRIBED_PARTS_OWNER_LINK_SHORT_DESCRIPTION = "COPY_TO_DESCRIBED_PARTS_OWNER_LINK_SHORT_DESCRIPTION";

   @RBEntry("Specifica se un'associazione proprietario a una parte è necessaria affinché la rappresentazione venga copiata nelle parti descritte.")
   public static final String COPY_TO_DESCRIBED_PARTS_OWNER_LINK_DESCRIPTION = "COPY_TO_DESCRIBED_PARTS_OWNER_LINK_DESCRIPTION";
   
   @RBEntry("Ultima iterazione")
   public static final String COPY_TO_DESCRIBED_PARTS_LATEST_ITERATION_DISPLAY_NAME = "COPY_TO_DESCRIBED_PARTS_LATEST_ITERATION_DISPLAY_NAME";

   @RBEntry("Consente di copiare le rappresentazioni nelle parti descritte solo se l'oggetto rappresentabile pubblicato è l'ultima iterazione.")
   public static final String COPY_TO_DESCRIBED_PARTS_LATEST_ITERATION_SHORT_DESCRIPTION = "COPY_TO_DESCRIBED_PARTS_LATEST_ITERATION_SHORT_DESCRIPTION";

   @RBEntry("Consente di copiare le rappresentazioni nelle parti descritte solo se l'oggetto rappresentabile pubblicato è l'ultima iterazione.")
   public static final String COPY_TO_DESCRIBED_PARTS_LATEST_ITERATION_DESCRIPTION = "COPY_TO_DESCRIBED_PARTS_LATEST_ITERATION_DESCRIPTION";
   
   @RBEntry("Elenco tipi di estensione file di rappresentazione")
   public static final String LIST_REP_FILE_TYPES_DISPLAY_NAME = "LIST_REP_FILE_TYPES_DISPLAY_NAME";

   @RBEntry("Specifica i tipi di file che consentono link di scaricamento contenuto corrispondenti nell'elenco rappresentazioni.")
   public static final String LIST_REP_FILE_TYPES_SHORT_DESCRIPTION = "LIST_REP_FILE_TYPES_SHORT_DESCRIPTION";

   @RBEntry("Elenco separato da virgole che specifica i tipi di estensione file scaricabili per una rappresentazione nell'elenco rappresentazioni. Il valore della preferenza TUTTI consente di scaricare ogni tipo di file.")
   public static final String LIST_REP_FILE_TYPES_DESCRIPTION = "LIST_REP_FILE_TYPES_DESCRIPTION";   
   
   @RBEntry("Stampa batch con servizi di stampa")
   public static final String PREFERENCE_BATCH_PRINT_CATEGORY = "PREFERENCE_BATCH_PRINT_CATEGORY";

   @RBEntry("Attivata")
   public static final String BATCH_PRINT_ENABLED_DISPLAY_NAME = "BATCH_PRINT_ENABLED_DISPLAY_NAME";

   @RBEntry("Attiva la stampa batch")
   public static final String BATCH_PRINT_ENABLED_SHORT_DESCRIPTION = "BATCH_PRINT_ENABLED_SHORT_DESCRIPTION";

   @RBEntry("Attiva la stampa batch (il default è No).")
   public static final String BATCH_PRINT_ENABLED_DESCRIPTION = "BATCH_PRINT_ENABLED_DESCRIPTION";

   @RBEntry("Tipi di file stampabili")
   public static final String BATCH_PRINT_PRINTABLE_TYPES_DISPLAY_NAME = "BATCH_PRINT_PRINTABLE_TYPES_DISPLAY_NAME";

   @RBEntry("Specifica i tipi di file stampabili")
   public static final String BATCH_PRINT_PRINTABLE_TYPES_SHORT_DESCRIPTION = "BATCH_PRINT_PRINTABLE_TYPES_SHORT_DESCRIPTION";

   @RBEntry("Elenco di tipi di file stampabili separati da virgole (le impostazioni di default sono modello, documento, disegno, immagine, illustrazione, ecad)")
   public static final String BATCH_PRINT_PRINTABLE_TYPES_DESCRIPTION = "BATCH_PRINT_PRINTABLE_TYPES_DESCRIPTION";

   @RBEntry("Rilevamento interferenze")
   public static final String PREFERENCE_BATCH_CLASH_CATEGORY = "PREFERENCE_BATCH_CLASH_CATEGORY";

   @RBEntry("Attivato")
   public static final String BATCH_CLASH_ENABLED_DISPLAY_NAME = "BATCH_CLASH_ENABLED_DISPLAY_NAME";

   @RBEntry("Attiva il rilevamento interferenze")
   public static final String BATCH_CLASH_ENABLED_SHORT_DESCRIPTION = "BATCH_CLASH_ENABLED_SHORT_DESCRIPTION";

   @RBEntry("Attiva il rilevamento interferenze (il default è No).")
   public static final String BATCH_CLASH_ENABLED_DESCRIPTION = "BATCH_CLASH_ENABLED_DESCRIPTION";

   @RBEntry("Tipo di definizione rilevamento interferenze")
   public static final String BATCH_CLASH_DEFINITION_SOFTTYPE_DISPLAY_NAME = "BATCH_CLASH_DEFINITION_SOFTTYPE_DISPLAY_NAME";

   @RBEntry("Definisce il tipo di documento per le definizioni di rilevamento interferenze")
   public static final String BATCH_CLASH_DEFINITION_SOFTTYPE_SHORT_DESCRIPTION = "BATCH_CLASH_DEFINITION_SOFTTYPE_SHORT_DESCRIPTION";

   @RBEntry("ID logico completo per i documenti di definizione di rilevamento interferenze (il default è wt.doc.WTDocument|com.ptc.InterferenceDetectionDefinition).")
   public static final String BATCH_CLASH_DEFINITION_SOFTTYPE_DESCRIPTION = "BATCH_CLASH_DEFINITION_SOFTTYPE_DESCRIPTION";

   @RBEntry("Unità di misura per definizione rilevamento interferenze")
   public static final String BATCH_CLASH_DEFINITION_UNITS_DISPLAY_NAME = "BATCH_CLASH_DEFINITION_UNITS_DISPLAY_NAME";

   @RBEntry("Definisce le unità di misura per le nuove definizioni di rilevamento interferenze")
   public static final String BATCH_CLASH_DEFINITION_UNITS_SHORT_DESCRIPTION = "BATCH_CLASH_DEFINITION_UNITS_SHORT_DESCRIPTION";

   @RBEntry("Definisce le unità di misura per le nuove definizioni di rilevamento interferenze (il default è MM).")
   public static final String BATCH_CLASH_DEFINITION_UNITS_DESCRIPTION = "BATCH_CLASH_DEFINITION_UNITS_DESCRIPTION";

   @RBEntry("Metri")
   public static final String BATCH_CLASH_DEFINITION_UNITS_M = "BATCH_CLASH_DEFINITION_UNITS_M";

   @RBEntry("Decimetri")
   public static final String BATCH_CLASH_DEFINITION_UNITS_DM = "BATCH_CLASH_DEFINITION_UNITS_DM";

   @RBEntry("Centimetri")
   public static final String BATCH_CLASH_DEFINITION_UNITS_CM = "BATCH_CLASH_DEFINITION_UNITS_CM";

   @RBEntry("Millimetri")
   public static final String BATCH_CLASH_DEFINITION_UNITS_MM = "BATCH_CLASH_DEFINITION_UNITS_MM";

   @RBEntry("Piedi")
   public static final String BATCH_CLASH_DEFINITION_UNITS_FT = "BATCH_CLASH_DEFINITION_UNITS_FT";

   @RBEntry("Pollici")
   public static final String BATCH_CLASH_DEFINITION_UNITS_IN = "BATCH_CLASH_DEFINITION_UNITS_IN";

   @RBEntry("Penetrazione tollerabile di default")
   public static final String BATCH_CLASH_DEFINITION_PENETRATION_DEFAULT_DISPLAY_NAME = "BATCH_CLASH_DEFINITION_PENETRATION_DEFAULT_DISPLAY_NAME";

   @RBEntry("Valore della penetrazione tollerabile di default per le nuove definizioni di rilevamento interferenze")
   public static final String BATCH_CLASH_DEFINITION_PENETRATION_DEFAULT_SHORT_DESCRIPTION = "BATCH_CLASH_DEFINITION_PENETRATION_DEFAULT_SHORT_DESCRIPTION";

   @RBEntry("Specifica il valore della penetrazione tollerabile di default per le nuove definizioni di rilevamento interferenze (il default è 0,0 nelle unità di default)")
   public static final String BATCH_CLASH_DEFINITION_PENETRATION_DEFAULT_DESCRIPTION = "BATCH_CLASH_DEFINITION_PENETRATION_DEFAULT_DESCRIPTION";

   @RBEntry("Tolleranza di default")
   public static final String BATCH_CLASH_DEFINITION_TOLERANCE_DEFAULT_DISPLAY_NAME = "BATCH_CLASH_DEFINITION_TOLERANCE_DEFAULT_DISPLAY_NAME";

   @RBEntry("Valore della tolleranza di default per le nuove definizioni di rilevamento interferenze")
   public static final String BATCH_CLASH_DEFINITION_TOLERANCE_DEFAULT_SHORT_DESCRIPTION = "BATCH_CLASH_DEFINITION_TOLERANCE_DEFAULT_SHORT_DESCRIPTION";

   @RBEntry("Specifica il valore della tolleranza di default per le nuove definizioni di rilevamento interferenze (il default è 0,0 nelle unità di default)")
   public static final String BATCH_CLASH_DEFINITION_TOLERANCE_DEFAULT_DESCRIPTION = "BATCH_CLASH_DEFINITION_TOLERANCE_DEFAULT_DESCRIPTION";

   @RBEntry("Distanza obbligatoria di default")
   public static final String BATCH_CLASH_DEFINITION_CLEARANCE_DEFAULT_DISPLAY_NAME = "BATCH_CLASH_DEFINITION_CLEARANCE_DEFAULT_DISPLAY_NAME";

   @RBEntry("Valore della distanza obbligatoria di default per le nuove definizioni di rilevamento interferenze")
   public static final String BATCH_CLASH_DEFINITION_CLEARANCE_DEFAULT_SHORT_DESCRIPTION = "BATCH_CLASH_DEFINITION_CLEARANCE_DEFAULT_SHORT_DESCRIPTION";

   @RBEntry("Valore della distanza obbligatoria di default per le nuove definizioni di rilevamento interferenze (il default è 0,01 nelle unità di default)")
   public static final String BATCH_CLASH_DEFINITION_CLEARANCE_DEFAULT_DESCRIPTION = "BATCH_CLASH_DEFINITION_CLEARANCE_DEFAULT_DESCRIPTION";

   @RBEntry("Modello di ciclo di vita interferenze")
   public static final String BATCH_CLASH_CLASH_LIFECYCLE_TEMPLATE_DISPLAY_NAME = "BATCH_CLASH_CLASH_LIFECYCLE_TEMPLATE_DISPLAY_NAME";

   @RBEntry("Specifica il nome del modello di ciclo di vita da assegnare a interferenze, contatti e distanze")
   public static final String BATCH_CLASH_CLASH_LIFECYCLE_TEMPLATE_SHORT_DESCRIPTION = "BATCH_CLASH_CLASH_LIFECYCLE_TEMPLATE_SHORT_DESCRIPTION";

   @RBEntry("Specifica il nome del modello di ciclo di vita da assegnare a interferenze, contatti e distanze (il default è Interferenza)")
   public static final String BATCH_CLASH_CLASH_LIFECYCLE_TEMPLATE_DESCRIPTION = "BATCH_CLASH_CLASH_LIFECYCLE_TEMPLATE_DESCRIPTION";

   @RBEntry("Modello di ciclo di vita report interferenze")
   public static final String BATCH_CLASH_CLASH_REPORT_LIFECYCLE_TEMPLATE_DISPLAY_NAME = "BATCH_CLASH_CLASH_REPORT_LIFECYCLE_TEMPLATE_DISPLAY_NAME";

   @RBEntry("Specifica il nome del modello di ciclo di vita da assegnare ai report interferenze")
   public static final String BATCH_CLASH_CLASH_REPORT_LIFECYCLE_TEMPLATE_SHORT_DESCRIPTION = "BATCH_CLASH_CLASH_REPORT_LIFECYCLE_TEMPLATE_SHORT_DESCRIPTION";

   @RBEntry("Specifica il nome del modello di ciclo di vita da assegnare ai report interferenze (il default è Report interferenze)")
   public static final String BATCH_CLASH_CLASH_REPORT_LIFECYCLE_TEMPLATE_DESCRIPTION = "BATCH_CLASH_CLASH_REPORT_LIFECYCLE_TEMPLATE_DESCRIPTION";

   @RBEntry("Tipi di oggetto a cui associare i rilevamenti interferenze")
   public static final String BATCH_CLASH_REPORT_LINKAGE_DISPLAY_NAME = "BATCH_CLASH_REPORT_LINKAGE_DISPLAY_NAME";

   @RBEntry("Specifica i tipi di oggetto a cui associare i rilevamenti interferenze")
   public static final String BATCH_CLASH_REPORT_LINKAGE_SHORT_DESCRIPTION = "BATCH_CLASH_REPORT_LINKAGE_SHORT_DESCRIPTION";

   @RBEntry("1. WTPART - Tutte le nuove istanze di rilevamento interferenza disporranno di un link creato per le due parti WTPart interferenti. Se la rappresentazione non include alcun riferimento WTPart per le parti interferenti, l'istanza di rilevamento interferenze non viene referenziata ad alcuna parte interferente.<br>2. EPMDOCUMENT - Tutte le nuove istanze di rilevamento interferenze disporranno di un link creato per i due documenti EPMDocument interferenti. Se la rappresentazione non include alcun riferimento EPMDocument alle parti interferenti, l'istanza di rilevamento interferenze non viene referenziata ad alcuna parte interferente.<br>3. WTPART_EPMDOCUMENT (default) - Tutte le nuove istanze di rilevamento interferenze disporranno di un link creato per le due parti WTPart interferenti, se esiste un riferimento WTPart. Altrimenti, se la rappresentazione non include un riferimento alle parti WTPart interferenti, viene utilizzato il riferimento ai documenti EPMDocument.<br>4. EPMDOCUMENT_WTPART - Tutte le nuove istanze di rilevamento interferenze disporranno di un link creato per i due documenti EPMDocument interferenti, se esiste un riferimento EPMDocument. Altrimenti, se la rappresentazione non include un riferimento per i documenti EPMDocument interferenti, viene utilizzato il riferimento alle parti WTPart.<br>")
   public static final String BATCH_CLASH_REPORT_LINKAGE_DESCRIPTION = "BATCH_CLASH_REPORT_LINKAGE_DESCRIPTION";

   @RBEntry("WTPART")
   public static final String BATCH_CLASH_REPORT_LINKAGE_WTPART = "BATCH_CLASH_REPORT_LINKAGE_WTPART";

   @RBEntry("EPMDOCUMENT")
   public static final String BATCH_CLASH_REPORT_LINKAGE_EPMDOCUMENT = "BATCH_CLASH_REPORT_LINKAGE_EPMDOCUMENT";

   @RBEntry("WTPART_EPMDOCUMENT")
   public static final String BATCH_CLASH_REPORT_LINKAGE_WTPART_EPMDOCUMENT = "BATCH_CLASH_REPORT_LINKAGE_WTPART_EPMDOCUMENT";

   @RBEntry("EPMDOCUMENT_WTPART")
   public static final String BATCH_CLASH_REPORT_LINKAGE_EPMDOCUMENT_WTPART = "BATCH_CLASH_REPORT_LINKAGE_EPMDOCUMENT_WTPART";

   @RBEntry("Server Adobe LiveCycle")
   public static final String PREFERENCE_LIVE_CYCLE_SERVER_CATEGORY = "PREFERENCE_LIVE_CYCLE_SERVER_CATEGORY";

   @RBEntry("Reader Extensions attivato")
   public static final String LIVE_CYCLE_SERVER_TAGGING_ENABLED_DISPLAY_NAME = "LIVE_CYCLE_SERVER_TAGGING_ENABLED_DISPLAY_NAME";

   @RBEntry("Attiva Reader Extensions per i file PDF tramite un server Adobe LifeCycle configurato")
   public static final String LIVE_CYCLE_SERVER_TAGGING_ENABLED_SHORT_DESCRIPTION = "LIVE_CYCLE_SERVER_TAGGING_ENABLED_SHORT_DESCRIPTION";

   @RBEntry("Attiva Reader Extensions per i file PDF per consentirne il markup con Creo View e Adobe Reader tramite un server Adobe LiveCycle configurato. Il default è No.")
   public static final String LIVE_CYCLE_SERVER_TAGGING_ENABLED_DESCRIPTION = "LIVE_CYCLE_SERVER_TAGGING_ENABLED_DESCRIPTION";

   @RBEntry("Modello DDX attivato")
   public static final String LIVE_CYCLE_SERVER_DDXTEMPLATE_ENABLED_DISPLAY_NAME = "LIVE_CYCLE_SERVER_DDXTEMPLATE_ENABLED_DISPLAY_NAME";

   @RBEntry("Consente l'elaborazione di file PDF con un modello DDX tramite un server Adobe LifeCycle configurato")
   public static final String LIVE_CYCLE_SERVER_DDXTEMPLATE_ENABLED_SHORT_DESCRIPTION = "LIVE_CYCLE_SERVER_DDXTEMPLATE_ENABLED_SHORT_DESCRIPTION";

   @RBEntry("Consente l'elaborazione di file PDF in base a istruzioni definite in un modello DDX tramite un server Adobe LiveCycle configurato. Un esempio consiste nell'applicazione di filigrana a file PDF durante il processo di pubblicazione. Il default è No.")
   public static final String LIVE_CYCLE_SERVER_DDXTEMPLATE_ENABLED_DESCRIPTION = "LIVE_CYCLE_SERVER_DDXTEMPLATE_ENABLED_DESCRIPTION";

}
