/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.wvs;

import wt.util.resource.*;

@RBUUID("wt.wvs.wvsResource")
public final class wvsResource extends WTListResourceBundle {
   @RBEntry("Error initializing the visualization interface.")
   public static final String VISUALIZATION_ERROR = "001";

   @RBEntry("Display List of Representations")
   public static final String REPRESENTATIONS = "002";

   @RBEntry("View in ProductView")
   public static final String VIEW_MARKUP = "003";

   @RBEntry("Create default representation")
   public static final String PUBLISH = "004";
}
