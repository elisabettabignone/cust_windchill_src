/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.wvs;

import wt.util.resource.*;

@RBUUID("wt.wvs.wvsResource")
public final class wvsResource_it extends WTListResourceBundle {
   @RBEntry("Errore durante l'inizializzazione dell'interfaccia di visualizzazione.")
   public static final String VISUALIZATION_ERROR = "001";

   @RBEntry("Visualizza elenco di rappresentazioni")
   public static final String REPRESENTATIONS = "002";

   @RBEntry("Visualizza in ProductView")
   public static final String VIEW_MARKUP = "003";

   @RBEntry("Crea rappresentazione di default")
   public static final String PUBLISH = "004";
}
