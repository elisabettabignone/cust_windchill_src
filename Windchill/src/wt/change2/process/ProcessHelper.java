/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */

package wt.change2.process;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.lang.Boolean;
import java.lang.ClassNotFoundException;
import java.lang.String;
import wt.change2.ChangeAnalysis;
import wt.change2.ChangeException2;
import wt.change2.ChangeInvestigation;
import wt.change2.ChangeItem;
import wt.change2.ChangeOrder2;
import wt.change2.ChangeRequest2;
import wt.change2.ChangeRequestIfc;
import wt.change2.WTChangeIssue;
import wt.util.WTException;

import wt.change2.Change2DelegateFactory;
import wt.change2.ChangeActivityIfc;
import wt.change2.ChangeOrderIfc;
import wt.change2.ChangeItemIfc;
import wt.change2.Complexity;
import wt.change2.FindChangeRequestDelegate;
import wt.change2.IssuePriority;
import wt.eff.EffManagedVersion;
import wt.fc.Persistable;
import wt.fc.QueryResult;
import wt.lifecycle.State;
import wt.lifecycle.LifeCycleManaged;
import wt.lifecycle.LifeCycleHelper;
import wt.util.WTProperties;
import wt.util.InstalledProperties;
import wt.change2.ChangeHelper2;  // Preserved unmodeled dependency

/**
 * This class contains static methods to support the Change Management objects'
 * workflow processes.
 *
 * These methods are intended to be called by routing expressions and synchronization
 * robot expressions.
 *
 * <BR><BR><B>Supported API: </B>true
 * <BR><BR><B>Extendable: </B>false
 *
 * @version   1.0
 **/
public class ProcessHelper implements Externalizable {
   private static final String RESOURCE = "wt.change2.process.processResource";
   private static final String CLASSNAME = ProcessHelper.class.getName();

   /**
    * <BR><BR><B>Supported API: </B>true
    **/
   public static final String CRITICAL = "CRITICAL";

   /**
    * <BR><BR><B>Supported API: </B>true
    **/
   public static final String NON_CRITICAL = "NON_CRITICAL";

   /**
    * <BR><BR><B>Supported API: </B>true
    **/
   public static final String COMPLETE = "COMPLETE";

   /**
    * <BR><BR><B>Supported API: </B>true
    **/
   public static final String CANCELLED = "CANCELLED";

   /**
    * <BR><BR><B>Supported API: </B>true
    **/
   public static final String NOT_ASSOCIATED = "NOT_ASSOCIATED";

   /**
    * <BR><BR><B>Supported API: </B>true
    **/
   public static final String NOT_FINISHED = "NOT_FINISHED";

   /**
    * <BR><BR><B>Supported API: </B>true
    **/
   public static final String NO_SUBORDINATES = "NO_SUBORDINATES";

   /**
    * <BR><BR><B>Supported API: </B>true
    **/
   public static final String NOT_SUBMITTED = "NOT_SUBMITTED";
   static final long serialVersionUID = 1;
   public static final long EXTERNALIZATION_VERSION_UID = 957977401221134810L;
   protected static final long OLD_FORMAT_VERSION_UID = -8926078900841157025L;

   /**
    * Writes the non-transient fields of this class to an external source.
    *
    * <BR><BR><B>Supported API: </B>false
    *
    * @param     output
    * @exception java.io.IOException
    **/
   public void writeExternal( ObjectOutput output )
            throws IOException {
      output.writeLong( EXTERNALIZATION_VERSION_UID );
   }

   /**
    * Reads the non-transient fields of this class from an external source.
    *
    * <BR><BR><B>Supported API: </B>false
    *
    * @param     input
    * @exception java.io.IOException
    * @exception java.lang.ClassNotFoundException
    **/
   public void readExternal( ObjectInput input )
            throws IOException, ClassNotFoundException {
      long readSerialVersionUID = input.readLong();                  // consume UID
      readVersion( this, input, readSerialVersionUID, false, false );  // read fields
   }

   /**
    * Reads the non-transient fields of this class from an external source.
    *
    * <BR><BR><B>Supported API: </B>false
    *
    * @param     thisObject
    * @param     input
    * @param     readSerialVersionUID
    * @param     passThrough
    * @param     superDone
    * @return    boolean
    * @exception java.io.IOException
    * @exception java.lang.ClassNotFoundException
    **/
   protected boolean readVersion( ProcessHelper thisObject, ObjectInput input, long readSerialVersionUID, boolean passThrough, boolean superDone )
            throws IOException, ClassNotFoundException {
      boolean success = true;

      if ( readSerialVersionUID == 957977401221134810L )
         return readVersion957977401221134810L( input, readSerialVersionUID, superDone );
      else
         success = readOldVersion( input, readSerialVersionUID, passThrough, superDone );

      if (input instanceof wt.pds.PDSObjectInput)
         wt.fc.EvolvableHelper.requestRewriteOfEvolvedBlobbedObject();

      return success;
   }

   /**
    * Reads the non-transient fields of this class from an external source,
    * which is not the current version.
    *
    * @param     input
    * @param     readSerialVersionUID
    * @param     passThrough
    * @param     superDone
    * @return    boolean
    * @exception java.io.IOException
    * @exception java.lang.ClassNotFoundException
    **/
   private boolean readOldVersion( ObjectInput input, long readSerialVersionUID, boolean passThrough, boolean superDone )
            throws IOException, ClassNotFoundException {
      boolean success = true;

      if ( readSerialVersionUID == OLD_FORMAT_VERSION_UID ) {          // handle previous version
      }
      else
         throw new java.io.InvalidClassException( CLASSNAME, "Local class not compatible:"
                           + " stream classdesc externalizationVersionUID=" + readSerialVersionUID
                           + " local class externalizationVersionUID=" + EXTERNALIZATION_VERSION_UID );

      return success;
   }

   /**
    * Determine if the passed Change Issue is critical or non-critical.
    *
    *
    * Returns ProcessHelper.CRITICAL if the issue has IssuePriority "Emergency",
    * and ProcessHelper.NON_CRITICAL otherwise.
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @param     cIssue
    * @return    String
    **/
   public static String getIssuePriority( WTChangeIssue cIssue ) {
      if (cIssue.getIssuePriority().equals(IssuePriority.toIssuePriority("EMERGENCY"))) {
        return new String(ProcessHelper.CRITICAL);
      }
      return new String(ProcessHelper.NON_CRITICAL);
   }

   /**
    * Determine if the passed Change Issue has been formalized into a Change
    * Request.
    *
    * Returns true if the passed Change Issue is related to a Change Request
    * through a Formalized By association.  Returns false otherwise.
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @param     cIssue
    * @return    Boolean
    * @exception wt.change2.ChangeException2
    * @exception wt.util.WTException
    **/
   public static Boolean checkIssueFormalized( WTChangeIssue cIssue )
            throws ChangeException2, WTException {
      QueryResult result = ChangeHelper2.service.getChangeRequest(cIssue, false);
      return new Boolean(result.hasMoreElements());
   }

   /**
    * Determine if the parent Change Request is in state "Complete" or "Cancelled."
    *
    * Returns ProcessHelper.COMPLETE if the parent Change Request is in
    * state "Complete."  Returns ProcessHelper.CANCELLED if the parent Change
    * Request is in state "Cancelled."  Returns ProcessHelper.NOT_ASSOCIATED
    * if the Change Issue has no parent Change Request.
    *
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @param     cIssue
    * @return    String
    * @exception wt.change2.ChangeException2
    * @exception wt.util.WTException
    **/
   public static String checkRequestFinished( WTChangeIssue cIssue )
            throws ChangeException2, WTException {
      QueryResult result = ChangeHelper2.service.getChangeRequest(cIssue);
      if (result.hasMoreElements()) {
        return checkLifeCycleManagedFinished( result );
      }
      else {
        // checkLifeCycleManagedFinished would return ProcessHelper.NO_SUBORDINATES in this case
        return new String(ProcessHelper.NOT_ASSOCIATED);
      }
   }

   /**
    * Determine the complexity of the change based on parent Change Request
    * of the passed Change Item.
    *
    * If the Change Request is a ChangeRequest2, then it returns the String
    * value of the Complexity Enumerated Type as stored on the Change Item's
    * parent Change Request; otherwise returns the empty string "".
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @param     cItem
    * @return    String
    * @exception wt.change2.ChangeException2
    * @exception wt.util.WTException
    **/
   public static String getComplexity( ChangeItem cItem )
            throws ChangeException2, WTException {
      ChangeRequestIfc change_request = null;
      if (cItem instanceof ChangeRequestIfc) {
        change_request = (ChangeRequestIfc) cItem;
      }
      else {
        Change2DelegateFactory factory = new Change2DelegateFactory();
        FindChangeRequestDelegate delegate = factory.getFindChangeRequestDelegate( (ChangeItemIfc) cItem );
        change_request = delegate.findChangeRequest( (ChangeItemIfc) cItem );
      }
      if (change_request instanceof ChangeRequest2)
      {
          Complexity complexity = ((ChangeRequest2)change_request).getComplexity();
          if ( complexity != null )
            return complexity.toString();
      }
      return new String();
   }

   /**
    * Determine if the associated Change Investigation has any pending Analysis
    * Activities.
    *
    * Returns ProcessHelper.COMPLETE if at least one Change Activity is
    * in state "Complete" and the rest are in state "Complete" or "Cancel."
    *  Returns ProcessHelper.CANCELLED if all Analysis Activities are in
    * state "Cancelled."  Returns ProcessHelper.NOT_FINISHED if at least
    * one Analysis Activity is pending (i.e. has not reached either state
    * "Complete" or "Cancelled").
    *
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @param     cInvestigation
    * @return    String
    * @exception wt.change2.ChangeException2
    * @exception wt.util.WTException
    **/
   public static String checkAnalysisActivitiesFinished( ChangeInvestigation cInvestigation )
            throws ChangeException2, WTException {
      QueryResult result = ChangeHelper2.service.getAnalysisActivities(cInvestigation);
      return checkLifeCycleManagedFinished( result );
   }

   /**
    * Determine if the passed Change Analysis (Change Proposal or Change
    * Investigation) has any pending Analysis Activities.
    *
    * Returns ProcessHelper.COMPLETE if at least one Change Activity is
    * in state "Complete" and the rest are in state "Complete" or "Cancel."
    *  Returns ProcessHelper.CANCELLED if all Analysis Activities are in
    * state "Cancelled."  Returns ProcessHelper.NOT_FINISHED if at least
    * one Analysis Activity is pending (i.e. has not reached either state
    * "Complete" or "Cancelled").
    *
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @param     cAnalysis
    * @return    String
    * @exception wt.change2.ChangeException2
    * @exception wt.util.WTException
    **/
   public static String checkAnalysisActivitiesFinished( ChangeAnalysis cAnalysis )
            throws ChangeException2, WTException {
      QueryResult result = ChangeHelper2.service.getAnalysisActivities(cAnalysis);
      return checkLifeCycleManagedFinished( result );
   }

   /**
    * Determine if the passed Change Request has any pending Change Proposals.
    *
    * Returns ProcessHelper.COMPLETE if at least one Change Proposal is
    * in state "Complete" and the rest are in state "Complete" or "Cancel."
    *  Returns ProcessHelper.CANCELLED if all Change Proposals are in state
    * "Cancelled."  Returns ProcessHelper.NOT_FINISHED if at least one Change
    * Proposal is pending (i.e. has not reached either state "Complete"
    * or "Cancelled").
    *
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @param     cRequest
    * @return    String
    * @exception wt.change2.ChangeException2
    * @exception wt.util.WTException
    **/
   public static String checkProposalsFinished( ChangeRequest2 cRequest )
            throws ChangeException2, WTException {
      QueryResult result = ChangeHelper2.service.getChangeProposals(cRequest);
      return checkLifeCycleManagedFinished( result );
   }

   /**
    * Determine if the passed Change Request has any pending Change Orders.
    *
    * Returns ProcessHelper.COMPLETE if at least one Change Order is in
    * state "Complete" and the rest are in state "Complete" or "Cancel."
    *  Returns ProcessHelper.CANCELLED if all Change Orders are in state
    * "Cancelled."  Returns ProcessHelper.NOT_FINISHED if at least one Change
    * Order is pending (i.e. has not reached either state "Complete" or
    * "Cancelled").
    *
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @param     cRequest
    * @return    String
    * @exception wt.change2.ChangeException2
    * @exception wt.util.WTException
    **/
   public static String checkOrdersFinished( ChangeRequest2 cRequest )
            throws ChangeException2, WTException {
      QueryResult result = ChangeHelper2.service.getChangeOrders(cRequest);
      return checkLifeCycleManagedFinished( result );
   }

   /**
    * Determine if the parent Change Request has been submitted and returns
    * the complexity of the change.
    *
    * Returns ProcessHelper.NOT_SUBMITTED if the Change Request has not
    * been submitted.  Otherwise, if the Change Request is a ChangeRequest2,
    * then it returns the Complexity Enumerated Type String value as stored
    * on the Change Item's parent Change Request.  If the Change Request
    * is not a ChangeRequest2, then it returns the empty string "".
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @param     cItem
    * @return    String
    * @exception wt.change2.ChangeException2
    * @exception wt.util.WTException
    **/
   public static String checkRequestSubmitted( ChangeItem cItem )
            throws ChangeException2, WTException {
      ChangeRequestIfc change_request = null;
      // if the change object passed was a Change Request, then use it
      if (cItem instanceof ChangeRequestIfc) {
        change_request = (ChangeRequestIfc) cItem;
      }
      // otherwise find the parent change request
      else {
        Change2DelegateFactory factory = new Change2DelegateFactory();
        FindChangeRequestDelegate delegate = factory.getFindChangeRequestDelegate( (ChangeItemIfc) cItem );
        change_request = delegate.findChangeRequest( (ChangeItemIfc) cItem );
      }
      if (change_request instanceof LifeCycleManaged) {
        LifeCycleManaged life_cycled = (LifeCycleManaged) change_request;
        if (LifeCycleHelper.service.isInInitialPhase(life_cycled)) {
            if (!(life_cycled.isLifeCycleAtGate())) {
                return ProcessHelper.NOT_SUBMITTED;
            }
        }
        if (life_cycled instanceof ChangeRequest2) {
            ChangeRequest2 request = (ChangeRequest2) life_cycled;
            Complexity complexity = request.getComplexity();
            if ( complexity != null )
                return complexity.toString();
        }
        return new String();    // We don't know how to get a complexity
      }
      return null;  // We can't check if its been submitted
   }

   /**
    * Determine if the passed Change Order has any pending Change Activities.
    *
    * Returns ProcessHelper.COMPLETE if at least one Change Activity is
    * in state "Complete" and the rest are in state "Complete" or "Cancel."
    *  Returns ProcessHelper.CANCELLED if all Change Activities are in state
    * "Cancelled."  Returns ProcessHelper.NOT_FINISHED if at least one Change
    * Activity is pending (i.e. has not reached either state "Complete"
    * or "Cancelled").
    *
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @param     cOrder
    * @return    String
    * @exception wt.change2.ChangeException2
    * @exception wt.util.WTException
    **/
   public static String checkChangeActivitiesFinished( ChangeOrder2 cOrder )
            throws ChangeException2, WTException {
      QueryResult result = ChangeHelper2.service.getChangeActivities(cOrder);
      return checkLifeCycleManagedFinished( result );
   }

   /**
    * Determine if the passed Change Request has any pending Change Investigations.
    *
    * Returns ProcessHelper.COMPLETE if at least one Change Investigation
    * is in state "Complete" and the rest are in state "Complete" or "Cancel."
    *  Returns ProcessHelper.CANCELLED if all Change Investigations are
    * in state "Cancelled."  Returns ProcessHelper.NOT_FINISHED if at least
    * one Change Investigation is pending (i.e. has not reached either state
    * "Complete" or "Cancelled").
    *
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @param     cRequest
    * @return    String
    * @exception wt.change2.ChangeException2
    * @exception wt.util.WTException
    **/
   public static String checkInvestigationsFinished( ChangeRequest2 cRequest )
            throws ChangeException2, WTException {
      QueryResult result = ChangeHelper2.service.getChangeInvestigations(cRequest);
      return checkLifeCycleManagedFinished( result );
   }

   /**
    * Determine if the passed Change Request is in state "Complete" or "Cancelled."
    *
    * Returns ProcessHelper.COMPLETE if the Change Request is in state "Complete."
    *  Returns ProcessHelper.CANCELLED if the Change Request is in state
    * "Cancelled."
    *
    *
    * <BR><BR><B>Supported API: </B>false
    *
    * @param     cRequest
    * @return    String
    * @exception wt.change2.ChangeException2
    * @exception wt.util.WTException
    **/
   public static String checkRequestFinished( ChangeRequest2 cRequest )
            throws ChangeException2, WTException {
      return checkLifeCycleManagedFinished( cRequest );
   }

   /**
    * Determine if the passed Change Request has any Change Activities which
    * have eff-managed new data versions.
    *
    * Returns true if at least one Change Activity contains a new data version
    * which is effectivity managable (aka can have effectivity assigned
    * to it).
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @param     changeRequest  Change Request primary object in the workflow process
    * @return    boolean
    * @exception wt.util.WTException
    **/
   public static boolean hasEffManagedNewDataVersions( ChangeRequestIfc changeRequest )
            throws WTException {
      // Acquire change orders for the change request
      QueryResult chgOrders = ChangeHelper2.service.getChangeOrders(changeRequest, true);
      if (chgOrders == null) return false;

      QueryResult chgActivities = null;
      QueryResult newDataVersions = null;

      // Loop through change orders
      while (chgOrders.hasMoreElements()) {
         // Acquire change activities for current change order
         chgActivities = ChangeHelper2.service.getChangeActivities((ChangeOrderIfc) chgOrders.nextElement(), true);
         if (chgActivities != null) {
            // Loop through change activities
            while (chgActivities.hasMoreElements()) {
               // Acquire new data versions for current change activity
               newDataVersions = ChangeHelper2.service.getChangeablesAfter((ChangeActivityIfc) chgActivities.nextElement(), true);
               if (newDataVersions != null) {
                  // Loop through new data versions, searching for first effectity managed object
                  while (newDataVersions.hasMoreElements()) {
                     if (newDataVersions.nextElement() instanceof EffManagedVersion)
                        return true;
                  }
               }
            }
         }
      }

      // No effectivity managed new data version found anywhere on the change request
      return false;
   }

   /**
    * Reads the non-transient fields of this class from an external source.
    *
    * @param     input
    * @param     readSerialVersionUID
    * @param     superDone
    * @return    boolean
    * @exception java.io.IOException
    * @exception java.lang.ClassNotFoundException
    **/
   private boolean readVersion957977401221134810L( ObjectInput input, long readSerialVersionUID, boolean superDone )
            throws IOException, ClassNotFoundException {
      return true;
   }

   private static String checkLifeCycleManagedFinished( QueryResult result ) {
      // Initially assume NO_SUBORDINATES
      String result_value = ProcessHelper.NO_SUBORDINATES;
      while (result.hasMoreElements()) {
        Persistable persistable = (Persistable) result.nextElement();
        if (persistable instanceof LifeCycleManaged) {
            LifeCycleManaged life_cycle_managed = (LifeCycleManaged) persistable;
            if (life_cycle_managed.getLifeCycleState().equals(State.toState("COMPLETED"))) {
                // If we were in CANCELLED state, then set it to COMPLETE
                result_value = ProcessHelper.COMPLETE;
            }
            else if (life_cycle_managed.getLifeCycleState().equals(State.toState("CANCELLED"))) {
                if (result_value == ProcessHelper.NO_SUBORDINATES) {
                    // If there were NO_SUBORDINATES, then we move to CANCELLED
                    // otherwise we stay in the CANCELLED or COMPLETE state
                    result_value = ProcessHelper.CANCELLED;
                }
            }
            else {
                // There are still activies that are not CANCELLED and not COMPLETE
                return new String(ProcessHelper.NOT_FINISHED);
            }
        }
      }
      return new String(result_value);
   }

   public static String checkLifeCycleManagedFinished( LifeCycleManaged life_cycle_managed ) {
      String result_value = ProcessHelper.NOT_FINISHED;
      if (life_cycle_managed.getLifeCycleState().equals(State.toState("COMPLETED"))) {
        result_value = ProcessHelper.COMPLETE;
      }
      else if (life_cycle_managed.getLifeCycleState().equals(State.toState("CANCELLED"))) {
        result_value = ProcessHelper.CANCELLED;
      }
      else {
        result_value = ProcessHelper.NOT_FINISHED;
      }
      return new String(result_value);
   }

   ////////////////////////////////////////////////////////////////////////////

   /**
    *   This method will return whether the Default Change Process is the
    *   CMII Closed-Loop change process or the default Change2 implementation.
    *   This can be overridden for testing purposes by setting
    * <BR><code>wt.change2.process.cmii=false</code> in wt.properties.
    *
    * <BR><BR><B>Supported API: </B>false
    *  @return true if the Default process is CMII
    */
   ////////////////////////////////////////////////////////////////////////////
   public static boolean isCMIIDefaultChangeProcess( )
   {
       try
       {
            Class c = Class.forName("com.ptc.windchill.pdmlink.change.server.processors.ChangeMonitorTemplateProcessor");
            if ( c != null )
            {
                try
                {
                    WTProperties localProps = WTProperties.getLocalProperties();
                    boolean override = localProps.getProperty("wt.change2.process.cmii",true);
                    if ( override )
                        return true;
                }
                catch( IOException wte )
                {
                    wte.printStackTrace();
                }
            }
       }
       catch( ClassNotFoundException cnfe )
       {

       }
       return false;
   }

   ////////////////////////////////////////////////////////////////////////////

   /**
    *   This method will return whether CMII is stand alone or is running within
    *   a PDMLink environment.
    *
    *   <BR><BR><B>Supported API: </B>false
    *
    *   @return true if it is installed in a Modular fashion
    */
   ////////////////////////////////////////////////////////////////////////////
   public static boolean isCMIIStandAlone( )
   {
       // This will likely change once the Module Install is done.
       if ( InstalledProperties.isInstalled("Windchill.PDMLink") )
           return false;
       return true;
   }

   public static void main(String args[] )
   {
        System.out.println("Is CMII Enabled? " + isCMIIDefaultChangeProcess() );
        System.out.println("Is CMII Standalone? " + isCMIIStandAlone() );
        System.exit(0);
   }
}
