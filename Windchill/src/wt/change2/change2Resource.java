/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.change2;

import wt.util.resource.*;

@RBUUID("wt.change2.change2Resource")
public final class change2Resource extends WTListResourceBundle {
   @RBEntry("The name for the new change request is not valid")
   public static final String INVALID_CR_NAME = "0";

   @RBEntry("The name for the new change notice is not valid")
   public static final String INVALID_CO_NAME = "1";

   @RBEntry("The name for the new task is not valid")
   public static final String INVALID_CA_NAME = "2";

   @RBEntry("A name must be provided for the change request")
   public static final String NO_CR_NAME = "3";

   @RBEntry("A description must be provided for the change request")
   public static final String NO_CR_DESCRIPTION = "4";

   @RBEntry("A complexity must be provided for the change request")
   public static final String NO_CR_COMPLEXITY = "5";

   @RBEntry("A change request type must be provided for the change request")
   public static final String NO_CR_REQUESTTYPE = "6";

   @RBEntry("A name must be provided for the change notice")
   public static final String NO_CO_NAME = "7";

   @RBEntry("A description must be provided for the problem report")
   public static final String NO_CI_DESCRIPTION = "8";

   @RBEntry("A name must be provided for the change investigation")
   public static final String NO_CINV_NAME = "9";

   @RBEntry("A name must be provided for the task")
   public static final String NO_CA_NAME = "10";

   @RBEntry("A description must be provided for the change investigation")
   public static final String NO_CINV_DESCRIPTION = "11";

   @RBEntry("No change notice was found for task \"{0}\".")
   public static final String NO_CO_FOR_CA = "12";

   @RBEntry("This object cannot be created in a personal cabinet.")
   public static final String NOT_IN_SHARE_CABINET = "13";

   @RBEntry("\"{0}\" is a working copy.  It may not be associated with task \"{1}\".")
   public static final String IDENTIFIES_WORKING_ITEM = "14";

   @RBEntry("\"{0}\" is a working copy.  It may not be associated with change notice \"{1}\".")
   public static final String AFFECTS_WORKING_ITEM = "15";

   @RBEntry("The name for the new problem report is not valid")
   public static final String INVALID_CI_NAME = "16";

   @RBEntry("No change request was found for problem report \"{0}\".")
   public static final String NO_CR_FOR_CI = "17";

   @RBEntry("A name must be provided for the problem report")
   public static final String NO_CI_NAME = "18";

   @RBEntry("A requester must be provided for the problem report")
   public static final String NO_CI_REQUESTER = "19";

   @RBEntry("Edit")
   public static final String UPDATE = "20";

   @RBEntry("The name for the new analysis activity is not valid")
   public static final String INVALID_AA_NAME = "21";

   @RBEntry("A name must be supplied for the analysis activity")
   public static final String NO_AA_NAME = "22";

   @RBEntry("Null invalid for \"{0}\" (parameter \"{1}\") in \"{2}\"")
   public static final String NULL_ARGUMENT = "23";

   @RBEntry("Unable to save \"{0}\".")
   public static final String STORE_ERROR = "24";

   @RBEntry("The \"{0}\" argument, a \"{1}\" object, is not \"{2}\" as expected")
   public static final String INVALID_DELEGATE_ARGUMENT_TYPE = "25";

   @RBEntry("The system was not able to save because the life cycle specified, \"{0}\", does not exist.  Please have your Windchill system administrator verify that the life cycles referenced in the Change Management property file (change2.properties) exist in the system.")
   public static final String LIFECYCLE_ERROR = "26";

   @RBEntry("Unable to create a \"{0}\" link between the \"{1}\" and the \"{2}\".")
   public static final String LINK_ERROR = "27";

   @RBEntry("Affected end item revision cannot be added to or removed from \"{0}\" because the user does not have access or it is locked.")
   public static final String ILLEGAL_REVISES = "28";

   @RBEntry("\"{0}\" is checked out by another user.  It may not be added to \"{1}\".")
   public static final String LOCKED_FOR_STORE = "29";

   @RBEntry("\"{0}\" is checked out by another user.  It may not be removed from \"{1}\".")
   public static final String LOCKED_FOR_DELETE = "30";

   @RBEntry("Cannot edit \"{0}\" because the user does not have it locked.")
   public static final String CHANGE_ITEM_NOT_LOCKED = "31";

   @RBEntry("Deleting \"{0}\" is not allowed since it is affected by a task")
   public static final String CHANGEABLE_HAS_BEFORE_CA = "32";

   @RBEntry("Deleting \"{0}\" is not allowed since it is changed by a task")
   public static final String CHANGEABLE_HAS_AFTER_CA = "33";

   @RBEntry("Deleting \"{0}\" is not allowed since it is relevant to an analysis activity")
   public static final String CHANGEABLE_HAS_AA = "34";

   @RBEntry("The name for the new change investigation is not valid")
   public static final String INVALID_CINV_NAME = "35";

   @RBEntry("The name for the new change proposal is not valid")
   public static final String INVALID_CP_NAME = "36";

   @RBEntry("A name must be supplied for the change proposal")
   public static final String NO_CP_NAME = "37";

   @RBEntry("\"{0}\" is checked out by another user.  Its listing on \"{1}\" may not be modified.")
   public static final String LOCKED_FOR_MODIFY = "38";

   @RBEntry("An issue can be associated to at most one change request")
   public static final String CI_ALLOWS_ONLY_ONE_CR = "39";

   @RBEntry("Missing Required Parameters for script call")
   public static final String SCRIPT_PARMS_REQUIRED = "40";

   @RBEntry("A description must be supplied for the change notice")
   public static final String NO_CO_DESCRIPTION = "41";

   @RBEntry("A description must be supplied for the task")
   public static final String NO_CA_DESCRIPTION = "42";

   @RBEntry("A description must be supplied for the change proposal")
   public static final String NO_CP_DESCRIPTION = "43";

   @RBEntry("A description must be supplied for the analysis activity")
   public static final String NO_AA_DESCRIPTION = "44";

   @RBEntry("Deleting \"{0}\" is not allowed since it is relevant to a change request")
   public static final String CHANGEABLE_HAS_CR = "45";

   @RBEntry("The following Problem Reports are already associated to a Change Request:\n\n{0}  ")
   @RBArgComment0("A list of Problem Report numbers that did not satisfy the condition like 100-232, 100-233 etc.  The \n new lines can be placed where it is reasonable for display.  NOTE: two \n's should appear at the end to separate the statements from the list.")
   public static final String PR_ALLOWS_ONLY_ONE_ECR_ADDITION = "46";

   @RBEntry("The following problem reports are already associated to a change request:{0}  ")
   @RBArgComment0("A list of change issue numbers that did not satisfy the condition like 100-232, 100-233 etc.")
   public static final String CI_ALLOWS_ONLY_ONE_ECR_ADDITION = "47";

   @RBEntry("The following End Items are already affected by this Problem Report and were not added:\n\n{0}")
   public static final String CMII_PR_END_ITEMS_ASSOCIATED = "49";

   @RBEntry("The following End Items are already affected by this Change Request and were not added:\n\n{0}")
   public static final String CMII_ECR_END_ITEMS_ASSOCIATED = "50";

   @RBEntry("Deleting is not allowed since it is affected by a task")
   public static final String MO_CHANGEABLE_HAS_BEFORE_CA = "51";

   @RBEntry("Deleting is not allowed since it is changed by a task")
   public static final String MO_CHANGEABLE_HAS_AFTER_CA = "52";

   @RBEntry("Deleting is not allowed since it is relevant to an analysis activity")
   public static final String MO_CHANGEABLE_HAS_AA = "53";

   @RBEntry("Deleting is not allowed since it is relevant to a change request")
   public static final String MO_CHANGEABLE_HAS_CR = "54";

   @RBEntry("Not allowed to create Change Objects with ProIntralink installed")
   public static final String CANNOT_CREATE_CHANGEITEM_WITH_PROI_INSTALLED = "55";

   @RBEntry(" Product to be added is not valid")
   public static final String INVALID_PRODUCT = "56";

   @RBEntry("The name must be supplied for the variance")
   public static final String NO_VARIANCE_NAME = "57";

   @RBEntry("The name for the new variance is not valid")
   public static final String INVALID_VARIANCE_NAME = "58";

   @RBEntry("The description must be supplied for the variance")
   public static final String NO_VARIANCE_DESCRIPTION = "59";

   @RBEntry("A category must be supplied for the variance")
   public static final String NO_VARIANCE_CATEGORY = "60";

   @RBEntry("Variance Owner is a required attribute and must be supplied for the variance")
   public static final String NO_VARIANCE_OWNER = "61";

   @RBEntry("Name is a required attribute.")
   public static final String NO_NAME = "62";

   @RBEntry("You do not have access permission to change the contents of {0}")
   public static final String NO_MODIFY_CONTENT_PERMISSION_ON_CHANGE = "63";

   @RBEntry("You are not allowed to edit {0} as it is related to change for which you do not have modify content permission ")
   public static final String NO_MODIFY_CONTENT_PERMISSION_ON_RELATEDCHANGE = "64";

   @RBEntry("Changing \"{0}\" from End Item is not allowed since it is a subject of a change.")
   public static final String CANNOT_CHANGE_FROM_ENDITEM = "65";

   @RBEntry("Cannot delete non-latest version of a Change Item.")
   public static final String CANNOT_DELETE_NON_LATEST_CHANGE = "66";

   @RBEntry("You are not allowed to delete {0} as it is related to change")
   public static final String NO_DELETE_PERMISSION_SUPPORTINGMATERIAL = "67";

   @RBEntry("Change Request cannot be attached, because this Change Notice is already attached to another Change request with multiple Change notices: {0}")
   @RBComment("It is not allowed to attach an ECR to an ECN if the ECN is already associated to another ECR with multiple ECN's")
   public static final String ECR_ATTACHED_INVALID = "68";

   @RBEntry("Change Request cannot be attached, because it is already attached to another Change Notice with multiple Change Requests: {0}")
   @RBComment("It is not allowed to attach an ECR if that ECR is already attached to an ECN with multpile ECR's")
   public static final String ECN_ATTACHED_INVALID = "69";

   @RBEntry("Change Request cannot be attached. It is not allowed to attach a change request with multiple Change Notices to a Change Notice with multiple Change Requests.")
   @RBComment("It is not allowed to attach an ECR with multiple ECN's to an ECN with multpile ECR's")
   public static final String MANY_ECNECR_ATTACHED_INVALID = "70";

   @RBEntry("You are not allowed to delete {0} as it is associated with a problem report or a variance.")
   public static final String NO_DELETE_CHANGEABLE_WITH_PRREPORT = "71";

   @RBEntry("\"{0}\" cannot be associated to \"{1}\" since it is not contained in a product or library.")
   @RBComment("It is not allowed to attach an object in a product or program container to a Change Object.")
   public static final String INVALID_CHANGE_ASSOCIATION = "72";

   @RBEntry(" The container for \"{0}\" could not be obtained to validate the association of {0} to {1}.")
   @RBComment("Need the container of the object to validate the association of the object to a Change Object.")
   public static final String COULDNOT_VALIDATE_CONTAINER = "73";

   @RBEntry(" The association of \"{0}\" to \"{1}\" was successfully created.")
   @RBComment("Success message for creating the impacts association.")
   public static final String LOAD_IMPACTS_SUCCESS_MSG = "74";

   @RBEntry("The change directive \"{0}\" could not be found.")
   @RBComment("Require change directive to exist in the system to make the impacts association.")
   public static final String CHANGE_DIRECTIVE_NOT_EXIST_MSG = "75";

   @RBEntry("The impacted item \"{0}\" could not be found.")
   @RBComment("Require impacted item to exist in the system to make the impacts association.")
   public static final String IMPACTED_ITEM_NOT_EXIST_MSG = "76";

   @RBEntry("You cannot delete the association between a change directive and a configuration item if the configuration item has a design solution that fulfills a change action for the given change directive.")
   @RBComment("Cannot remove the impacts with change actions in solved state.")
   public static final String REMOVEIMPACTS_ERROR = "77";

   @RBEntry("You cannot delete a change directive that is associated to a configuration item.")
   @RBComment("Cannot delete a change directive if it is associated to a configuration item.")
   public static final String CD_CI_ASSOCIATION_ERROR = "78";

   @RBEntry("Deleting is not allowed since it is associated to a change request")
   public static final String CHANGE2_HAS_ANNOTATION = "79";

   @RBEntry("You are not allowed to edit {0} as it is related to change for which you do not have modify permission ")
   public static final String NO_MODIFY_PERMISSION_ON_RELATEDCHANGE = "80";

   @RBEntry("no context")
   public static final String NO_EFF_CONTEXT = "81";

   @RBEntry("You are not allowed to delete {0} as it is associated with a change task.")
   public static final String NO_DELETE_CHANGEABLE_WITH_HANGING_CHANGE = "82";
   
   @RBEntry("The change request is already associated to another existing issue")
   public static final String CHANGE_REQUEST_ALREADY_ASSOCIATED = "83";
   
   @RBEntry("The change request is already associated to another change order")
   public static final String CHANGE_REQUEST_ALREADY_ADDRESSED = "84";
   
   @RBEntry("A change request can be associated to at most one change order")
   public static final String ECR_ALLOWS_ONLY_ONE_CO = "85";

   @RBEntry("Change Objects do not support one-off versions")
   public static final String ONE_OFF_VERSION_NOT_SUPPORTED = "86";

}
