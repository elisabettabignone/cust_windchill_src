package wt.change2;

import wt.util.resource.*;

@RBUUID("wt.change2.changeStatusCalculatorResource")
public final class changeStatusCalculatorResource extends WTListResourceBundle {
   /**
/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
    * 
    * This file contains the messages displayed by ChangeStatusCalculator tool
    * 
    **/
   @RBEntry("Change status calculator has executed successfully. Check the method server log for detailed results.")
   @RBComment("Status message to be displayed after tool executes successfully")
   public static final String TOOL_SUCCESS = "TOOL_SUCCESS";

   @RBEntry("Change status calculator has failed to execute. Check the method server log for detailed results.")
   @RBComment("Status message to be displayed after tool fails to execute")
   public static final String TOOL_FAILURE = "TOOL_FAILURE";

   @RBEntry("Usage : wt.change2.ChangeStatusCalculator [--help][--preview] \n\n Options : \n\n --Help,--help,--h - Help \n --preview         - Run the tool in PREVIEW mode \n --onlyResulting   - Run the tool only for resulting changes")
   @RBComment("Tool usage syntax")
   public static final String TOOL_USAGE = "TOOL_USAGE";

   @RBEntry("Found {0} objects to update the {1} property.")
   @RBComment("message to be displayed for changeables found that require change status to be set")
   public static final String SET_PREVIEW_MESSAGE = "SET_PREVIEW_MESSAGE";

   @RBEntry("Change status calculator is executing in PREVIEW mode.")
   @RBComment("message to be displayed for preview mode")
   public static final String PREVIEW_MESSAGE = "PREVIEW_MESSAGE";

   @RBEntry("Updated change status for {0} objects.")
   @RBComment("message to be displayed while performing update")
   public static final String UPDATE_MESSAGE = "UPDATE_MESSAGE";
   
   @RBEntry("Only the resulting change status was processed.")
   @RBComment("message to indicate that only the resulting change status will be processed.")
   public static final String RESULTING_CHANGE_ONLY_MESSAGE = "RESULTING_CHANGE_ONLY_MESSAGE";
}
