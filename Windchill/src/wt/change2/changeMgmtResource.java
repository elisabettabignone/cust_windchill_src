/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 * 
 */
    
package wt.change2;

import wt.util.resource.*;

@RBUUID("wt.change2.changeMgmtResource")
public final class changeMgmtResource extends WTListResourceBundle {

   @RBEntry("There were no instantiable types defined for {0}. Check with the system administrator.")
   @RBComment("Error message in the case of multiple intantiable typed links.")
   public static final String NO_INSTANTIABLE_TYPES = "NO_INSTANTIABLE_TYPES";
   
   @RBEntry("Update Change Task Attribute Layouts")
   @RBComment("Title for loading the updated change task attribute layouts.")
   public static final String UPDATE_CHANGE_TASK_ATTRIBUTE_LAYOUTS = "UPDATE_CHANGE_TASK_ATTRIBUTE_LAYOUTS";   

}
