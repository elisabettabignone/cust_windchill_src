/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.change2.htmlclient;

import wt.util.resource.*;

@RBUUID("wt.change2.htmlclient.htmlclientResource")
public final class htmlclientResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile creare l'operazione di modifica \"{0}\". Per creare una nuova operazione di modifica, aggiornare l'ordine di modifica relativo.")
   public static final String ILLEGAL_CA_STORE = "0";

   @RBEntry("Cancellazione di \"{0}\" non consentita. È allegato ad una operazione di modifica.")
   public static final String CHANGEABLE_HAS_CA = "1";

   @RBEntry("Impossibile eliminare l'operazione di modifica \"{0}\". Per eliminare un'operazione di modifica, aggiornare l'ordine di modifica collegato all'operazione di modifica.")
   public static final String ILLEGAL_CA_DELETE = "2";

   @RBEntry("L'eliminazione diretta del link IncludedIn non è consentita.")
   public static final String ILLEGAL_II_DELETE = "3";

   @RBEntry("Il nome della nuova richiesta di modifica non è valido.")
   public static final String INVALID_CR_NAME = "4";

   @RBEntry("Il nome del nuovo ordine di modifica non è valido.")
   public static final String INVALID_CO_NAME = "5";

   @RBEntry("Il nome della nuova operazione di modifica non è valido.")
   public static final String INVALID_CA_NAME = "6";

   @RBEntry("È necessario fornire un nome per la richiesta di modifica.")
   public static final String NO_CR_NAME = "7";

   @RBEntry("Deve essere fornita una descrizione per la richiesta di modifica")
   public static final String NO_CR_DESCRIPTION = "8";

   @RBEntry("È necessario fornire un richiedente per la richiesta di modifica.")
   public static final String NO_CR_REQUESTER = "9";

   @RBEntry("Deve essere fornito un tipo per la richiesta di modifica")
   public static final String NO_CR_REQUESTTYPE = "10";

   @RBEntry("È necessario fornire un nome per l'ordine di modifica.")
   public static final String NO_CO_NAME = "11";

   @RBEntry("È necessario fornire una descrizione per l'ordine di modifica")
   public static final String NO_CO_DESCRIPTION = "12";

   @RBEntry("È necessario fornire un tipo di ordine di modifica per l'ordine di modifica.")
   public static final String NO_CO_ORDERTYPE = "13";

   @RBEntry("È necessario fornire un nome per l'operazione di modifica.")
   public static final String NO_CA_NAME = "14";

   @RBEntry("È necessario fornire una descrizione per l'operazione di modifica")
   public static final String NO_CA_DESCRIPTION = "15";

   @RBEntry("Non è stato trovato alcun ordine di modifica per l'operazione di modifica \"{0}\".")
   public static final String NO_CO_FOR_CA = "16";

   @RBEntry("\"{0}\" è sottoposto a Check-Out. Non può essere aggiunto all'operazione di modifica \"{1}\".")
   public static final String LOCKED_FOR_STORE = "17";

   @RBEntry("\"{0}\" è sottoposto a Check-Out. Non può essere rimosso dall'operazione di modifica \"{1}\".")
   public static final String LOCKED_FOR_DELETE = "18";

   @RBEntry("\"{0}\" è sottoposto a Check-Out. La descrizione dell'operazione di modifica non può essere modificata.")
   public static final String LOCKED_FOR_MODIFY = "19";

   @RBEntry("Le operazioni di modifica non possono essere aggiunte o rimosse dall'ordine di modifica \"{0}\" perché l'utente non dispone del diritto di accesso oppure l'oggetto è bloccato.")
   public static final String ILLEGAL_INCLUDED_IN = "20";

   @RBEntry("Gli elementi revisionati non possono essere aggiunti o rimossi dall'operazione di modifica \"{0}\" perché l'utente non dispone del diritto di accesso oppure l'oggetto è bloccato.")
   public static final String ILLEGAL_REVISES = "21";

   @RBEntry("Impossibile modificare \"{0}\" perchè l'utente non l'ha bloccato.")
   public static final String CHANGE_ITEM_NOT_LOCKED = "22";

   @RBEntry("Questo elemento non può essere creato in uno schedario personale.")
   public static final String NOT_IN_SHARE_CABINET = "23";

   @RBEntry("\"{0}\" è una copia in modifica. Non può essere associata ad un'operazione di modifica \"{1}\".")
   public static final String IDENTIFIES_WORKING_ITEM = "24";

   @RBEntry("\"{0}\" è una copia in modifica. Non può essere associata ad un ordine di modifica \"{1}\".")
   public static final String AFFECTS_WORKING_ITEM = "25";

   @RBEntry("Il nome del nuovo suggerimento di modifica non è valido.")
   public static final String INVALID_CI_NAME = "26";

   @RBEntry("Non è stata trovata alcuna richiesta di modifica per il suggerimento di modifica \"{0}\".")
   public static final String NO_CR_FOR_CI = "27";

   @RBEntry("È necessario fornire un nome per il suggerimento di modifica.")
   public static final String NO_CI_NAME = "28";

   @RBEntry("Deve essere fornito un richiedente per il suggerimento di modifica")
   public static final String NO_CI_REQUESTER = "29";

   @RBEntry("Aggiorna")
   public static final String UPDATE = "30";

   @RBEntry("Il nome della nuova analisi non è valido")
   public static final String INVALID_AA_NAME = "31";

   @RBEntry("È necessario fornire un nome per l'analisi.")
   public static final String NO_AA_NAME = "32";

   @RBEntry("Modulo gestione modifiche")
   public static final String CM_PAGE_TITLE = "33";

   @RBEntry("Non è stata restituita alcuna richiesta di modifica.")
   public static final String NO_CHANGE_REQUEST_RETURNED = "34";

   @RBEntry("Non è stata restituita alcuna analisi.")
   public static final String NO_ANALYSIS_ACTIVITIES_RETURNED = "35";

   @RBEntry("Non è stata restituita alcuna operazione di modifica.")
   public static final String NO_CHANGE_ACTIVITIES_RETURNED = "36";

   @RBEntry("Lo script method Windchill non ha valore per il parametro \"{0}\". Questo parametro dovrebbe essere impostato ad un nome di classe valido.")
   public static final String NULL_CLASS_PARAMETER = "45";

   @RBEntry("Lo script method Windchill ha il valore \"{0}\" per il parametro \"{1}\". Non è una classe valida.")
   public static final String INVALID_CLASS_PARAMETER = "46";

   @RBEntry("Lo script method Windchill non ha un valore per il parametro \"{0}\".")
   public static final String NULL_STRING_PARAMETER = "47";

   @RBEntry("Il tasto \"{0}\" non è valido per il resource bundle indicato nella chiamata dello script method Windchill.")
   public static final String INVALID_RESOURCEKEY_PARAMETER = "48";

   /**
    * Keys used by the HTML User Interface
    **/
   @RBEntry("Aggiorna")
   public static final String UPDATE_LINK = "37";

   @RBEntry("OK")
   public static final String ACCEPT_LINK = "38";

   @RBEntry("Annulla")
   public static final String CANCEL_LINK = "39";

   @RBEntry("Nuova investigazione")
   public static final String NEW_INVESTIGATION = "40";

   @RBEntry("Nuova proposta")
   public static final String NEW_PROPOSAL = "41";

   @RBEntry("Nuova analisi")
   public static final String NEW_ANALYSIS_ACTIVITY = "42";

   @RBEntry("Nuovo ordine di modifica")
   public static final String NEW_ORDER = "43";

   @RBEntry("Nuova operazione di modifica")
   public static final String NEW_CHANGE_ACTIVITY = "44";

   @RBEntry("Avvia processo di modifica")
   public static final String SUBMIT = "49";

   @RBEntry("Crea una nuova investigazione di modifica")
   public static final String NEW_INVESTIGATION_ALT = "50";

   @RBEntry("Crea una nuova proposta di modifica")
   public static final String NEW_PROPOSAL_ALT = "51";

   @RBEntry("Crea una nuova analisi")
   public static final String NEW_ANALYSIS_ACTIVITY_ALT = "52";

   @RBEntry("Crea un nuovo ordine di modifica")
   public static final String NEW_ORDER_ALT = "53";

   @RBEntry("Crea una nuova operazione di modifica")
   public static final String NEW_CHANGE_ACTIVITY_ALT = "54";

   @RBEntry("Aggiorna questa richiesta di modifica")
   public static final String UPDATE_REQUEST = "55";

   @RBEntry("Aggiorna questa investigazione di modifica")
   public static final String UPDATE_INVESTIGATION = "56";

   @RBEntry("Aggiorna questa proposta di modifica")
   public static final String UPDATE_PROPOSAL = "57";

   @RBEntry("Aggiorna questa analisi")
   public static final String UPDATE_ANALYSIS_ACTIVITY = "58";

   @RBEntry("Aggiorna questo ordine di modifica")
   public static final String UPDATE_ORDER = "59";

   @RBEntry("Aggiorna questa operazione di modifica")
   public static final String UPDATE_CHANGE_ACTIVITY = "60";

   @RBEntry("Annulla modifiche")
   public static final String CANCEL_CHANGES = "61";

   @RBEntry("Accetta modifiche")
   public static final String ACCEPT_CHANGES = "62";

   @RBEntry("Guida per le richieste di modifica")
   public static final String HELP_REQUEST = "63";

   @RBEntry("Guida per investigazioni di modifica")
   public static final String HELP_INVESTIGATION = "64";

   @RBEntry("Guida per proposte di modifica")
   public static final String HELP_PROPOSAL = "65";

   @RBEntry("Guida per analisi")
   public static final String HELP_ANALYSIS_ACTIVITY = "66";

   @RBEntry("Guida per ordini di modifica")
   public static final String HELP_ORDER = "67";

   @RBEntry("Guida per operazioni di modifica")
   public static final String HELP_CHANGE_ACTIVITY = "68";

   @RBEntry("Elimina")
   public static final String DELETE_LINK = "69";

   @RBEntry("Elimina l'investigazione di modifica")
   public static final String DELETE_INVESTIGATION = "70";

   @RBEntry("Elimina la proposta di modifica")
   public static final String DELETE_PROPOSAL = "71";

   @RBEntry("Elimina l'ordine di modifica")
   public static final String DELETE_ORDER = "72";

   @RBEntry("Elimina l'analisi")
   public static final String DELETE_ANALYSIS_ACTIVITY = "73";

   @RBEntry("Elimina l'operazione di modifica")
   public static final String DELETE_CHANGE_ACTIVITY = "74";

   @RBEntry("Aggiungi l'effettività dell'ordine di modifica")
   public static final String EFFECTIVITY_ORDER = "75";

   @RBEntry("Aggiungi&nbsp;effettività")
   public static final String EFFECTIVITY_LINK = "76";

   @RBEntry("Aggiungi l'effettività dell'operazione di modifica")
   public static final String EFFECTIVITY_ACTIVITY = "77";

   @RBEntry("Commenti:")
   public static final String COMMENTS = "78";

   @RBEntry("Imposta")
   public static final String ADD_EFFECTIVITY = "79";

   @RBEntry("Effettività")
   public static final String EFFECTIVITY = "80";

   @RBEntry("Il processo di modifica è stato avviato.")
   public static final String SUBMIT_SUCCESSFUL = "81";

   @RBEntry("La richiesta di modifica è stata salvata.")
   public static final String SUBMIT_FAILED = "82";

   @RBEntry("[Dati referenziati pertinenti a questa richiesta di modifica]")
   public static final String CR_CHANGEABLES_EXPLAIN = "83";

   @RBEntry("[Dati referenziati rilevanti per questa analisi]")
   public static final String AA_CHANGEABLES_EXPLAIN = "84";

   @RBEntry("[Dati referenziati esistenti che verranno revisionati]")
   public static final String CA_ORIGCHANGEABLES_EXPLAIN = "85";

   @RBEntry("[Nuovi dati referenziati creati a seguito di questa operazione di modifica]")
   public static final String CA_NEWCHANGEABLES_EXPLAIN = "86";

   @RBEntry("Crea {0}")
   @RBArgComment0("is the display name of a change object. ex. \"Create Change Activity\"")
   public static final String CREATE_CHANGE_OBJECT = "87";

   @RBEntry("Aggiorna {0}")
   @RBArgComment0("is the display identity (summary and number) of a change object.")
   public static final String UPDATE_CHANGE_OBJECT = "88";

   @RBEntry("Firme")
   public static final String SIGNATURE_LINK = "89";

   @RBEntry("Sottoscrizioni")
   public static final String SUBSCRIPTION_LINK = "90";

   @RBEntry("Mostra firme elettroniche")
   public static final String SHOW_SIGNATURES = "91";

   @RBEntry("Mostra sottoscrizioni")
   public static final String SHOW_SUBSCRIPTIONS = "92";

   @RBEntry("[Prodotti referenziati pertinenti a questa richiesta di modifica]")
   public static final String CR_PRODUCTS_EXPLAIN = "93";

   @RBEntry("Mostra forum discussioni")
   public static final String SHOW_FORUM = "94";

   @RBEntry("Cronologia del ciclo di vita")
   public static final String LIFECYCLEHISTORY_LINK = "95";

   @RBEntry("Aggiorna il suggerimento modifica")
   public static final String UPDATE_ISSUE = "96";

   @RBEntry("Mostra cronologia ciclo di vita")
   public static final String SHOW_LIFECYCLE_HISTORY = "97";

   @RBEntry("Copia negli Appunti")
   public static final String COPY_LINK = "98";

   @RBEntry("Stato condivisione")
   public static final String SHARE_STATUS = "99";

   @RBEntry("Aggiungi a progetto")
   public static final String ADD_TO_PROJECT_LINK = "100";

   @RBEntry("Visualizza cronologia target di distribuzione")
   public static final String ESI_DISTRIBUTION_LIST = "101";

   @RBEntry("Condiviso con")
   @RBComment("comment=copied from com.ptc.netmarkets.model.modelResource to remove dep")
   public static final String SHARED_TO_TITLE = "102";

   @RBEntry("Stato")
   @RBComment("comment=copied from com.ptc.netmarkets.model.modelResource to remove dep")
   public static final String STATUS_TITLE = "103";

   @RBEntry("Da")
   @RBComment("comment=copied from com.ptc.netmarkets.model.modelResource to remove dep")
   public static final String SHARED_BY_TITLE = "104";

   @RBEntry("Data condivisione")
   @RBComment("comment=copied from com.ptc.netmarkets.model.modelResource to remove dep")
   public static final String SHARED_ON_TITLE = "105";
}
