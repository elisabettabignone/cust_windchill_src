/* bcwti
 *
 * Copyright (c) 2011 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.change2.adminlock;

import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.access.change.changeAdminLockResource")
public class changeAdminLockResource_it extends WTListResourceBundle {
    
    @RBEntry("Impossibile modificare il link sull'oggetto a causa di un blocco amministrativo.")
    @RBComment("Error message with the object identity string in the case where the user tries to create a Resulting or Hanging Change link with a changeable which has Administrative lock.")   
    public static final String ADMIN_LOCK_CANNOT_MODIFY_LINK_WITH_OBJECT_ID = "ADMIN_LOCK_CANNOT_MODIFY_LINK_WITH_OBJECT_ID";
    
    @RBEntry("Impossibile promuovere l'oggetto a causa di un blocco amministrativo: {0}")
    @RBComment("Error message with the object identity string in the case of where the user can't promote the object due to Administrative lock.")   
    public static final String ADMIN_LOCK_CANNOT_PROMOTE_WITH_OBJECT_ID = "ADMIN_LOCK_CANNOT_PROMOTE_WITH_OBJECT_ID";
    
    @RBEntry("Impossibile promuovere l'oggetto a causa di un blocco amministrativo.")
    @RBComment("Error message in the case of where the user can't promote the object due to Administrative lock.")   
    public static final String ADMIN_LOCK_CANNOT_PROMOTE = "ADMIN_LOCK_CANNOT_PROMOTE";    
    
}
