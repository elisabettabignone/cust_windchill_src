/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.change2;

import wt.util.resource.*;

@RBUUID("wt.change2.change2Resource")
public final class change2Resource_it extends WTListResourceBundle {
   @RBEntry("Il nome della nuova richiesta di modifica non è valido.")
   public static final String INVALID_CR_NAME = "0";

   @RBEntry("Il riepilogo della nuova notifica di modifica non è valida")
   public static final String INVALID_CO_NAME = "1";

   @RBEntry("Il riepilogo del nuovo task non è valido")
   public static final String INVALID_CA_NAME = "2";

   @RBEntry("È necessario fornire un nome per la richiesta di modifica")
   public static final String NO_CR_NAME = "3";

   @RBEntry("È necessario fornire una descrizione per la richiesta di modifica")
   public static final String NO_CR_DESCRIPTION = "4";

   @RBEntry("È necessario fornire una complessità per la richiesta di modifica")
   public static final String NO_CR_COMPLEXITY = "5";

   @RBEntry("È necessario fornire un tipo per la richiesta di modifica")
   public static final String NO_CR_REQUESTTYPE = "6";

   @RBEntry("È necessario fornire un nome per la notifica di modifica")
   public static final String NO_CO_NAME = "7";

   @RBEntry("È necessario fornire una descrizione per il report di problema")
   public static final String NO_CI_DESCRIPTION = "8";

   @RBEntry("È necessario fornire un nome per l'investigazione di modifica")
   public static final String NO_CINV_NAME = "9";

   @RBEntry("È necessario fornire un nome per il task")
   public static final String NO_CA_NAME = "10";

   @RBEntry("È necessario fornire una descrizione per l'investigazione di modifica")
   public static final String NO_CINV_DESCRIPTION = "11";

   @RBEntry("Non è stata trovata alcuna notifica di modifica per il task \"{0}\".")
   public static final String NO_CO_FOR_CA = "12";

   @RBEntry("Questo oggetto non può essere creato in uno schedario personale.")
   public static final String NOT_IN_SHARE_CABINET = "13";

   @RBEntry("\"{0}\" è una copia in modifica. Non può essere associata al task \"{1}\".")
   public static final String IDENTIFIES_WORKING_ITEM = "14";

   @RBEntry("\"{0}\" è una copia in modifica. Non può essere associata a una notifica di modifica \"{1}\".")
   public static final String AFFECTS_WORKING_ITEM = "15";

   @RBEntry("Il nome del nuovo report di problema non è valido")
   public static final String INVALID_CI_NAME = "16";

   @RBEntry("Non è stata trovata alcuna richiesta di modifica per il report di problema \"{0}\".")
   public static final String NO_CR_FOR_CI = "17";

   @RBEntry("È necessario fornire un nome per il report di problema")
   public static final String NO_CI_NAME = "18";

   @RBEntry("È necessario fornire un richiedente per il report di problema")
   public static final String NO_CI_REQUESTER = "19";

   @RBEntry("Modifica")
   public static final String UPDATE = "20";

   @RBEntry("Il nome della nuova analisi non è valido")
   public static final String INVALID_AA_NAME = "21";

   @RBEntry("È necessario fornire un nome per l'analisi")
   public static final String NO_AA_NAME = "22";

   @RBEntry("Valore nullo non valido per \"{0}\" (parametro \"{1}\") in \"{2}\"")
   public static final String NULL_ARGUMENT = "23";

   @RBEntry("Impossibile salvare \"{0}\".")
   public static final String STORE_ERROR = "24";

   @RBEntry("Argomento \"{0}\", oggetto \"{1}\", non \"{2}\" come previsto.")
   public static final String INVALID_DELEGATE_ARGUMENT_TYPE = "25";

   @RBEntry("Il sistema non è riuscito a salvare perchè il ciclo di vita specificato \"{0}\" non esiste. Chiedere all'amministratore del sistema Windchill di verificare che i cicli di vita referenziati nel file delle proprietà di gestione modifiche (change2.properties) esistano.")
   public static final String LIFECYCLE_ERROR = "26";

   @RBEntry("Impossibile creare un link \"{0}\" tra \"{1}\" e \"{2}\".")
   public static final String LINK_ERROR = "27";

   @RBEntry("La revisione dei prodotti finali interessati non può essere aggiunta a o rimossa dall'operazione \"{0}\" perché l'utente non dispone del diritto di accesso o perché è bloccata.")
   public static final String ILLEGAL_REVISES = "28";

   @RBEntry("\"{0}\" è sottoposto a Check-Out. Non può essere aggiunto all'operazione \"{1}\".")
   public static final String LOCKED_FOR_STORE = "29";

   @RBEntry("\"{0}\" è sottoposto a Check-Out. Non può essere rimosso dall'operazione \"{1}\".")
   public static final String LOCKED_FOR_DELETE = "30";

   @RBEntry("Impossibile modificare \"{0}\" perché l'utente non l'ha bloccato.")
   public static final String CHANGE_ITEM_NOT_LOCKED = "31";

   @RBEntry("Impossibile cancellare \"{0}\" in quanto interessato da un task")
   public static final String CHANGEABLE_HAS_BEFORE_CA = "32";

   @RBEntry("Impossibile cancellare \"{0}\" in quanto modificato da un task")
   public static final String CHANGEABLE_HAS_AFTER_CA = "33";

   @RBEntry("Impossibile cancellare \"{0}\" in quanto è rilevante per un analisi")
   public static final String CHANGEABLE_HAS_AA = "34";

   @RBEntry("Il nome della nuova investigazione di modifica non è valido")
   public static final String INVALID_CINV_NAME = "35";

   @RBEntry("Il nome della nuova proposta di modifica non è valido")
   public static final String INVALID_CP_NAME = "36";

   @RBEntry("È necessario fornire un nome per la proposta di modifica")
   public static final String NO_CP_NAME = "37";

   @RBEntry("\"{0}\" è sottoposto a Check-Out. Non può essere modificato dall'operazione \"{1}\".")
   public static final String LOCKED_FOR_MODIFY = "38";

   @RBEntry("Un problema può essere associato a non più di una richiesta di modifica")
   public static final String CI_ALLOWS_ONLY_ONE_CR = "39";

   @RBEntry("Mancanza dei parametri richiesti per il richiamo della procedura")
   public static final String SCRIPT_PARMS_REQUIRED = "40";

   @RBEntry("È necessario fornire una descrizione per la notifica di modifica")
   public static final String NO_CO_DESCRIPTION = "41";

   @RBEntry("È necessario fornire una descrizione per il task")
   public static final String NO_CA_DESCRIPTION = "42";

   @RBEntry("È necessario fornire una descrizione per la proposta di modifica")
   public static final String NO_CP_DESCRIPTION = "43";

   @RBEntry("È necessario fornire una descrizione per l'analisi")
   public static final String NO_AA_DESCRIPTION = "44";

   @RBEntry("Cancellazione di \"{0}\" non consentita. È pertinente a una richiesta di modifica.")
   public static final String CHANGEABLE_HAS_CR = "45";

   @RBEntry("I report di problema seguenti sono già associati a una richiesta di modifica:\n\n{0} ")
   @RBArgComment0("A list of Problem Report numbers that did not satisfy the condition like 100-232, 100-233 etc.  The \n new lines can be placed where it is reasonable for display.  NOTE: two \n's should appear at the end to separate the statements from the list.")
   public static final String PR_ALLOWS_ONLY_ONE_ECR_ADDITION = "46";

   @RBEntry("I report di problema seguenti sono già associati a una richiesta di modifica: {0} ")
   @RBArgComment0("A list of change issue numbers that did not satisfy the condition like 100-232, 100-233 etc.")
   public static final String CI_ALLOWS_ONLY_ONE_ECR_ADDITION = "47";

   @RBEntry("I seguenti prodotti finali sono già interessati da questo report di problema e non sono stati aggiunti:\n\n{0}")
   public static final String CMII_PR_END_ITEMS_ASSOCIATED = "49";

   @RBEntry("I seguenti prodotti finali sono già interessati da questa richiesta di modifica e non sono stati aggiunti:\n\n{0}")
   public static final String CMII_ECR_END_ITEMS_ASSOCIATED = "50";

   @RBEntry("Eliminazione non consentita in quanto l'elemento è interessato da un task")
   public static final String MO_CHANGEABLE_HAS_BEFORE_CA = "51";

   @RBEntry("Eliminazione non consentita in quanto l'elemento è modificato da un task")
   public static final String MO_CHANGEABLE_HAS_AFTER_CA = "52";

   @RBEntry("Eliminazione non consentita, l'elemento è pertinente a un'attività di analisi.")
   public static final String MO_CHANGEABLE_HAS_AA = "53";

   @RBEntry("Eliminazione non consentita, l'elemento è pertinente a una richiesta di modifica.")
   public static final String MO_CHANGEABLE_HAS_CR = "54";

   @RBEntry("La creazione di oggetti di modifica è consentita se ProIntralink è installato")
   public static final String CANNOT_CREATE_CHANGEITEM_WITH_PROI_INSTALLED = "55";

   @RBEntry(" Il prodotto da aggiungere non è valido")
   public static final String INVALID_PRODUCT = "56";

   @RBEntry("È necessario fornire un nome per la soluzione temporanea.")
   public static final String NO_VARIANCE_NAME = "57";

   @RBEntry("Il nome della nuova soluzione temporanea non è valido.")
   public static final String INVALID_VARIANCE_NAME = "58";

   @RBEntry("È necessario fornire una descrizione per la soluzione temporanea")
   public static final String NO_VARIANCE_DESCRIPTION = "59";

   @RBEntry("È necessario fornire una categoria per la soluzione temporanea")
   public static final String NO_VARIANCE_CATEGORY = "60";

   @RBEntry("Il proprietario della soluzione temporanea è un attributo obbligatorio")
   public static final String NO_VARIANCE_OWNER = "61";

   @RBEntry("Il nome è un attributo obbligatorio.")
   public static final String NO_NAME = "62";

   @RBEntry("Non si dispone dei permessi di accesso per modificare il contenuto di {0}")
   public static final String NO_MODIFY_CONTENT_PERMISSION_ON_CHANGE = "63";

   @RBEntry("Non si dispone dei permessi per modificare {0} in quanto è correlato a una modifica il cui contenuto non si è autorizzati a modificare ")
   public static final String NO_MODIFY_CONTENT_PERMISSION_ON_RELATEDCHANGE = "64";

   @RBEntry("Impossibile modificare \"{0}\" da prodotto finale. È sottoposto a modifica.")
   public static final String CANNOT_CHANGE_FROM_ENDITEM = "65";

   @RBEntry("Impossibile eliminare una versione non aggiornata di un elemento di modifica.")
   public static final String CANNOT_DELETE_NON_LATEST_CHANGE = "66";

   @RBEntry("Impossibile eliminare {0}. È correlato a una modifica")
   public static final String NO_DELETE_PERMISSION_SUPPORTINGMATERIAL = "67";

   @RBEntry("Impossibile allegare la seguente richiesta di modifica. È allegata a un'altra richiesta di modifica con più notifiche di modifica: {0}")
   @RBComment("It is not allowed to attach an ECR to an ECN if the ECN is already associated to another ECR with multiple ECN's")
   public static final String ECR_ATTACHED_INVALID = "68";

   @RBEntry("Impossibile allegare la seguente richiesta di modifica. È allegata a un'altra notifica di modifica con più richieste di modifica: {0}")
   @RBComment("It is not allowed to attach an ECR if that ECR is already attached to an ECN with multpile ECR's")
   public static final String ECN_ATTACHED_INVALID = "69";

   @RBEntry("Impossibile allegare la seguente richiesta di modifica. Non è consentito allegare una richiesta di modifica con più notifiche di modifica a una notifica di modifica con più richieste di modifica.")
   @RBComment("It is not allowed to attach an ECR with multiple ECN's to an ECN with multpile ECR's")
   public static final String MANY_ECNECR_ATTACHED_INVALID = "70";

   @RBEntry("Impossibile eliminare {0}. È correlato a un report di problema o a una soluzione temporanea.")
   public static final String NO_DELETE_CHANGEABLE_WITH_PRREPORT = "71";

   @RBEntry("Impossibile associare \"{0}\" a \"{1}\" in quanto non è contenuto in un prodotto o libreria.")
   @RBComment("It is not allowed to attach an object in a product or program container to a Change Object.")
   public static final String INVALID_CHANGE_ASSOCIATION = "72";

   @RBEntry(" Impossibile ottenere il contenitore per \"{0}\" per la convalida dell'associazione fra {0} e {1}.")
   @RBComment("Need the container of the object to validate the association of the object to a Change Object.")
   public static final String COULDNOT_VALIDATE_CONTAINER = "73";

   @RBEntry(" L'associazione fra \"{0}\" e \"{1}\" è stata creata.")
   @RBComment("Success message for creating the impacts association.")
   public static final String LOAD_IMPACTS_SUCCESS_MSG = "74";

   @RBEntry("Impossibile trovare la direttiva di modifica \"{0}\".")
   @RBComment("Require change directive to exist in the system to make the impacts association.")
   public static final String CHANGE_DIRECTIVE_NOT_EXIST_MSG = "75";

   @RBEntry("Impossibile trovare l'elemento interessato \"{0}\".")
   @RBComment("Require impacted item to exist in the system to make the impacts association.")
   public static final String IMPACTED_ITEM_NOT_EXIST_MSG = "76";

   @RBEntry("Impossibile eliminare l'associazione tra una direttiva di modifica e un configuration item qualora il configuration item presenti una soluzione di progettazione che soddisfa un'azione di modifica per una determinata direttiva di modifica.")
   @RBComment("Cannot remove the impacts with change actions in solved state.")
   public static final String REMOVEIMPACTS_ERROR = "77";

   @RBEntry("Impossibile eliminare una direttiva di modifica associata a un configuration item.")
   @RBComment("Cannot delete a change directive if it is associated to a configuration item.")
   public static final String CD_CI_ASSOCIATION_ERROR = "78";

   @RBEntry("Eliminazione non consentita, l'elemento è associato a una richiesta di modifica.")
   public static final String CHANGE2_HAS_ANNOTATION = "79";

   @RBEntry("Non si dispone dei permessi per modificare {0} in quanto è correlato a una modifica che non si è autorizzati a modificare ")
   public static final String NO_MODIFY_PERMISSION_ON_RELATEDCHANGE = "80";

   @RBEntry("nessun contesto")
   public static final String NO_EFF_CONTEXT = "81";

   @RBEntry("Non si dispone dei permessi per eliminare {0} in quanto è un elemento associato a un task di modifica.")
   public static final String NO_DELETE_CHANGEABLE_WITH_HANGING_CHANGE = "82";
   
   @RBEntry("La richiesta di modifica è già associata a un altro problema esistente")
   public static final String CHANGE_REQUEST_ALREADY_ASSOCIATED = "83";
   
   @RBEntry("La richiesta di modifica è già associata a un altro ordine di modifica")
   public static final String CHANGE_REQUEST_ALREADY_ADDRESSED = "84";
   
   @RBEntry("Un problema può essere associato a non più di una richiesta di modifica")
   public static final String ECR_ALLOWS_ONLY_ONE_CO = "85";

   @RBEntry("Gli oggetti di modifica non supportano versioni variante")
   public static final String ONE_OFF_VERSION_NOT_SUPPORTED = "86";

}
