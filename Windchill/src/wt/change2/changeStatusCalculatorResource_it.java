package wt.change2;

import wt.util.resource.*;

@RBUUID("wt.change2.changeStatusCalculatorResource")
public final class changeStatusCalculatorResource_it extends WTListResourceBundle {
   /**
/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
    * 
    * This file contains the messages displayed by ChangeStatusCalculator tool
    * 
    **/
   @RBEntry("Calcolo stato modifiche eseguito correttamente. Consultare il file di log del method server per i dettagli.")
   @RBComment("Status message to be displayed after tool executes successfully")
   public static final String TOOL_SUCCESS = "TOOL_SUCCESS";

   @RBEntry("Calcolo stato modifiche non eseguito. Consultare il file di log del method server per i dettagli.")
   @RBComment("Status message to be displayed after tool fails to execute")
   public static final String TOOL_FAILURE = "TOOL_FAILURE";

   @RBEntry("Uso: wt.change2.ChangeStatusCalculator [--help][--preview] \n\n Opzioni: \n\n --Help,--help,--h - Help \n --preview         - Esegue lo strumento in modalità anteprima (preview) \n --onlyResulting   - Esegue lo strumento solo per le modifiche risultanti")
   @RBComment("Tool usage syntax")
   public static final String TOOL_USAGE = "TOOL_USAGE";

   @RBEntry("Trovati {0} oggetti di cui aggiornare la proprietà {1}.")
   @RBComment("message to be displayed for changeables found that require change status to be set")
   public static final String SET_PREVIEW_MESSAGE = "SET_PREVIEW_MESSAGE";

   @RBEntry("Il calcolo dello stato delle modifiche viene eseguito in modalità anteprima (preview).")
   @RBComment("message to be displayed for preview mode")
   public static final String PREVIEW_MESSAGE = "PREVIEW_MESSAGE";

   @RBEntry("Stato modifiche aggiornato per {0} oggetti.")
   @RBComment("message to be displayed while performing update")
   public static final String UPDATE_MESSAGE = "UPDATE_MESSAGE";
   
   @RBEntry("È stato elaborato solo lo stato delle modifiche risultanti.")
   @RBComment("message to indicate that only the resulting change status will be processed.")
   public static final String RESULTING_CHANGE_ONLY_MESSAGE = "RESULTING_CHANGE_ONLY_MESSAGE";
}
