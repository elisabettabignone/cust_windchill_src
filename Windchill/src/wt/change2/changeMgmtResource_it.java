/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 * 
 */
    
package wt.change2;

import wt.util.resource.*;

@RBUUID("wt.change2.changeMgmtResource")
public final class changeMgmtResource_it extends WTListResourceBundle {

   @RBEntry("Non sono presenti tipi sottoponibili a istanza definiti per {0}. Rivolgersi all'amministratore di sistema.")
   @RBComment("Error message in the case of multiple intantiable typed links.")
   public static final String NO_INSTANTIABLE_TYPES = "NO_INSTANTIABLE_TYPES";
   
   @RBEntry("Aggiorna layout attributi task di modifica")
   @RBComment("Title for loading the updated change task attribute layouts.")
   public static final String UPDATE_CHANGE_TASK_ATTRIBUTE_LAYOUTS = "UPDATE_CHANGE_TASK_ATTRIBUTE_LAYOUTS";   

}
