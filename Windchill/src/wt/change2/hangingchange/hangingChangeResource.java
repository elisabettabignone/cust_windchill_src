package wt.change2.hangingchange;

import wt.util.resource.*;

@RBUUID("wt.change2.hangingchange.hangingChangeResource")
public final class hangingChangeResource extends WTListResourceBundle {
   /**
/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
    * 
    * This file contains all the strings used by the Change Management hanging change service apis.
    * 
    **/
   @RBEntry("Unincorporated changes cannot be created because the following objects have unapproved unincorporated changes in progress:\n{0}")
   @RBComment("Cannot create a new unincorporated hanging change if changeable has an unapproved unincorporated hanging change")
   public static final String UNINCORPORATED_CHANGE_EXIST = "UNINCORPORATED_CHANGE_EXIST";

   @RBEntry("Unincorporated change cannot be created for the following non-latest versions:\n{0}")
   @RBComment("Error when the user tried to persist an unincorporated change against a non-latest version.")
   @RBArgComment0("The item ID of one or more items separated by a newline (inserted at runtime)")
   public static final String NON_LATEST_CREATE_DENIED = "NON_LATEST_CREATE_DENIED";

   @RBEntry("The following objects cannot be deleted because unincorporated changes exist:\n{0}")
   @RBComment("Error when deleting changeables associated to an original hanging change link when less than 10 changeables")
   @RBArgComment0("The item ID of one or more items separated by a newline (inserted at runtime)")
   public static final String CHANGEABLE_DELETE_DENIED_UNDER_LIMIT = "CHANGEABLE_DELETE_DENIED_UNDER_LIMIT";

   @RBEntry("{0} objects cannot be deleted because unincorporated changes exist.")
   @RBComment("Error when deleting changeables associated to an original hanging change link when more than 10 changeables")
   @RBArgComment0("The number of items")
   public static final String CHANGEABLE_DELETE_DENIED_OVER_LIMIT = "CHANGEABLE_DELETE_DENIED_OVER_LIMIT";

   @RBEntry("The following objects cannot be deleted because unincorporated changes with revised changeables exist:\n{0}")
   @RBComment("Error message while deleting change task having a unincorporated changes with revised changeable exist.")
   public static final String CHANGE_TASK_DELETE_DENIED_UNDER_LIMIT = "CHANGE_TASK_DELETE_DENIED_UNDER_LIMIT";

   @RBEntry("The number of objects that cannot be deleted because unincorporated changes with revised changeables exist: {0}")
   @RBComment("Error when deleting change task having a unincorporated changes with revised changeable exist when more than 10 change notice")
   @RBArgComment0("The number of items")
   public static final String CHANGE_TASK_DELETE_DENIED_OVER_LIMIT = "CHANGE_TASK_DELETE_DENIED_OVER_LIMIT";

   @RBEntry("The following objects cannot be deleted because unincorporated changes with revised changeables exist:\n{0}")
   @RBComment("Error message while deleting change notice having a unincorporated changes with revised changeable exist.")
   public static final String CHANGE_NOTICE_DELETE_DENIED_UNDER_LIMIT = "CHANGE_NOTICE_DELETE_DENIED_UNDER_LIMIT";

   @RBEntry("The number of objects that cannot be deleted because unincorporated changes with revised changeables exist: {0}")
   @RBComment("Error when deleting change notice having a unincorporated changes with revised changeable exist when more than 10 change notice")
   @RBArgComment0("The number of items")
   public static final String CHANGE_NOTICE_DELETE_DENIED_OVER_LIMIT = "CHANGE_NOTICE_DELETE_DENIED_OVER_LIMIT";

   @RBEntry("The following objects cannot be revised because the latest revision of the object has unincorporated changes:\n{0}")
   @RBComment("Cannot revise a Object if  least one later revision of the object has unincorporated changes.")
   @RBArgComment0("The item ID of one or more items separated by a newline (inserted at runtime)")
   public static final String NON_LATEST_REVISE_DENIED_UNDER_LIMIT = "NON_LATEST_REVISE_DENIED_UNDER_LIMIT";

   @RBEntry("The number of objects that cannot be revised because the latest revision of the object has unincorporated changes: {0}")
   @RBComment("Cannot revise a Object if  least one later revision of the object has unincorporated changes.")
   @RBArgComment0("The number of items")
   public static final String NON_LATEST_REVISE_DENIED_OVER_LIMIT = "NON_LATEST_REVISE_DENIED_OVER_LIMIT";
}
