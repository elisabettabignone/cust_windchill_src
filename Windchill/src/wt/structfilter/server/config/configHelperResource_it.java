/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.structfilter.server.config;

import wt.util.resource.*;

/**
 * String resources for wt.spatial package.
 *
 * <BR>
 * <BR>
 * <B>Supported API: </B>false <BR>
 * <BR>
 * <B>Extendable: </B>false
 */
@RBUUID("wt.structfilter.server.config.configHelperResource")
public final class configHelperResource_it extends WTListResourceBundle {

    /**
     * Default label used when we cannot find the Label for an attribute.
     */
   @RBEntry("Attributo sconosciuto")
   public static final String UNKNOWN_ATTRIBUTE= "UNKNOWN_ATTRIBUTE";

    /**
     * Filtering message when we cannot find the Label for an attribute.
     */
   @RBEntry("Rilevati problemi per uno o più attributi nel filtro attributi. È possibile modificare il filtro attributi per rimuovere gli attributi sconosciuti oppure segnalare il problema al responsabile del filtro o all'amministratore di sistema.")
   public static final String UNKNOWN_ATTRIBUTE_IN_FILTER= "UNKNOWN_ATTRIBUTE_IN_FILTER";
}
