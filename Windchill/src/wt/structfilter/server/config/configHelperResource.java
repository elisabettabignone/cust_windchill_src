/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.structfilter.server.config;

import wt.util.resource.*;

/**
 * String resources for wt.spatial package.
 *
 * <BR>
 * <BR>
 * <B>Supported API: </B>false <BR>
 * <BR>
 * <B>Extendable: </B>false
 */
@RBUUID("wt.structfilter.server.config.configHelperResource")
public final class configHelperResource extends WTListResourceBundle {

    /**
     * Default label used when we cannot find the Label for an attribute.
     */
   @RBEntry("Unknown Attribute")
   public static final String UNKNOWN_ATTRIBUTE= "UNKNOWN_ATTRIBUTE";

    /**
     * Filtering message when we cannot find the Label for an attribute.
     */
   @RBEntry("One or more of the attributes in your Attribute Filter have problems.  You can edit the Attribute Filter to remove the unknown attribute(s) or report the problem to the person responsible for this Attribute Filter or report the problem to a System Administrator.")
   public static final String UNKNOWN_ATTRIBUTE_IN_FILTER= "UNKNOWN_ATTRIBUTE_IN_FILTER";
}
