package wt.replacement;

import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;


@RBUUID("wt.replacement.LoadReplacementRB")
public class LoadReplacementRB_it extends WTListResourceBundle{

	//resources for Alternate link
    @RBEntry("La parte alternativa \"{0}, {1}\" non esiste.")
    public static final String ALTERNATE_PART_NOT_EXIST = "ALTERNATE_PART_NOT_EXIST";

    @RBEntry("La parte \"{0}, {1}\" non esiste.")
    public static final String ALTERNATE_FOR_PART_NOT_EXIST = "ALTERNATE_FOR_PART_NOT_EXIST";

    @RBEntry("La parte sostitutiva non può essere uguale alla parte sostituita")
    public static final String ROLE_A_AND_ROLE_B_EQUALS = "ROLE_A_AND_ROLE_B_EQUALS";


       // Strings used by LoadReplacement loader for Substitutes
    @RBEntry("La parte padre \"{0} {1}\" non esiste.")
    public static final String PARENT_PART_NOT_FOUND = "PARENT_PART_NOT_FOUND";

    @RBEntry("La parte di sostituzione \"{0} {1}\" non esiste.")
    public static final String SUBSTITUTE_PART_NOT_FOUND = "SUBSTITUTE_PART_NOT_FOUND";

    @RBEntry("Impossibile trovare la parte padre \"{0} {1}\" con l'input specificato.")
    public static final String PARENT_PART_NO_MATCH = "PARENT_PART_NO_MATCH";

    @RBEntry("La parte sottoassieme \"{0} {1}\" non esiste.")
    public static final String SUB_ASSEMBLY_PART_NOT_FOUND = "SUB_ASSEMBLY_PART_NOT_FOUND";

    @RBEntry("Nessuna relazione di utilizzo per la parte \"{0}\".")
    public static final String USAGE_RELATIONSHIP_NOT_FOUND = "USAGE_RELATIONSHIP_NOT_FOUND";

    @RBEntry("Impossibile aggiungere la parte di sostituzione \"{0} {1}\" a se stessa.")
    public static final String SUBSTITUTE_CHILD_CYCLIC_RELATION = "SUBSTITUTE_CHILD_CYCLIC_RELATION";

    @RBEntry("Relazione di utilizzo parte inesistente tra gli oggetti \"{0}\" e \"{1}\" con l'input specificato.")
    public static final String NO_USAGE_RELATION_BETWEEN_OBJECTS= "NO_USAGE_RELATION_BETWEEN_OBJECTS";

    @RBEntry("Impossibile caricare le sostituzioni per la relazione specificata a causa degli errori elencati sopra.")
    public static final String SUBSTITUTE_RELATION_FAILED = "SUBSTITUTE_RELATION_FAILED";

    @RBEntry("Tipo di sostituzione non valido: \"{0}\".")
    public static final String INVALID_REPLACEMENT_TYPE = "INVALID_REPLACEMENT_TYPE";
}
