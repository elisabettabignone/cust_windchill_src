package wt.replacement;

import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;


@RBUUID("wt.replacement.LoadReplacementRB")
public class LoadReplacementRB extends WTListResourceBundle{

	//resources for Alternate link
    @RBEntry("Alternate part \"{0}, {1}\" does not exist.")
    public static final String ALTERNATE_PART_NOT_EXIST = "ALTERNATE_PART_NOT_EXIST";

    @RBEntry("Part \"{0}, {1}\" does not exist.")
    public static final String ALTERNATE_FOR_PART_NOT_EXIST = "ALTERNATE_FOR_PART_NOT_EXIST";

    @RBEntry("A replacement part can not be the same as the part it is intended to replace")
    public static final String ROLE_A_AND_ROLE_B_EQUALS = "ROLE_A_AND_ROLE_B_EQUALS";


       // Strings used by LoadReplacement loader for Substitutes
    @RBEntry("Parent part \"{0} {1}\" does not exist.")
    public static final String PARENT_PART_NOT_FOUND = "PARENT_PART_NOT_FOUND";

    @RBEntry("Substitute part \"{0} {1}\" does not exist.")
    public static final String SUBSTITUTE_PART_NOT_FOUND = "SUBSTITUTE_PART_NOT_FOUND";

    @RBEntry("Cannot find the Parent part \"{0} {1}\" with the given input.")
    public static final String PARENT_PART_NO_MATCH = "PARENT_PART_NO_MATCH";

    @RBEntry("Sub-Assembly part \"{0} {1}\" does not exist.")
    public static final String SUB_ASSEMBLY_PART_NOT_FOUND = "SUB_ASSEMBLY_PART_NOT_FOUND";

    @RBEntry("No usage relationship exist for part \"{0}\".")
    public static final String USAGE_RELATIONSHIP_NOT_FOUND = "USAGE_RELATIONSHIP_NOT_FOUND";

    @RBEntry("Substitute part \"{0} {1}\" cannot be added to itself.")
    public static final String SUBSTITUTE_CHILD_CYCLIC_RELATION = "SUBSTITUTE_CHILD_CYCLIC_RELATION";

    @RBEntry("Part usage relationship does not exist between objects \"{0}\" and \"{1}\" with the given input.")
    public static final String NO_USAGE_RELATION_BETWEEN_OBJECTS= "NO_USAGE_RELATION_BETWEEN_OBJECTS";

    @RBEntry("Did not load the substitutes for the given relationship because of the errors listed above.")
    public static final String SUBSTITUTE_RELATION_FAILED = "SUBSTITUTE_RELATION_FAILED";

    @RBEntry("Invalid replacement type found \"{0}\".")
    public static final String INVALID_REPLACEMENT_TYPE = "INVALID_REPLACEMENT_TYPE";
}
