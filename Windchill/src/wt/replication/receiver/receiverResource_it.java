/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.replication.receiver;

import wt.util.resource.*;

@RBUUID("wt.replication.receiver.receiverResource")
public final class receiverResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile creare l'oggetto WTUnit con il nome \"{0}\" nella cartella \"{1}\" e salvarlo.")
   public static final String WTUNIT_CREATION_FAILED = "0";

   @RBEntry("L'aggiornamento del file di contenuto (o InputStream) sull'unità di replica \"{0}\" con numero di unità \"{1}\" è fallito.")
   public static final String UPDATE_CONTENT_FAILED = "1";

   @RBEntry("Impossibile impostare ContentRoleType {1} a ApplicationData con identificativo \"{0}\".")
   public static final String SET_ROLETYPE_FAILED = "2";

   @RBEntry("Data trovata nulla, ma onSchedule è true.")
   public static final String NULL_TIMESTAMP = "3";

   @RBEntry("La richiesta HTTP di {0} è fallita.")
   public static final String HTTP_REQUEST_FAILED = "4";

   @RBEntry("Il sito associato a WTUnit con nome \"{0}\" e numero \"{1}\" non esiste.")
   public static final String SITE_NOT_UNIQUE = "5";

   @RBEntry("Autenticazione di \"{0}\" è fallita.")
   public static final String AUTHENTICATION_FAILED = "6";

   @RBEntry("Il protocollo richiesto \"{1}\" trovato in \"{0}\" non è supportato.")
   public static final String PROTOCOL_NOT_SUPPORTED = "7";

   @RBEntry("L'oggetto sito non esiste per l'URL \"{0}\".")
   public static final String SITE_NOT_FOUND = "8";

   @RBEntry("L'oggetto CameFromUnit non è univoco per l'URL \"{0}\" e il numero di WTUnit {1}.")
   public static final String CAME_FROM_UNIT_NOT_UNIQUE = "9";

   @RBEntry("La creazione della coda di programmazione per l'oggetto WTUnit con nome <{0}> e numero <{1}> è fallita.")
   public static final String CREATE_SCHEDULEQUEUE_FAILED = "10";

   @RBEntry("InputStream nullo ricevuto per l'URL \"{0}\" associato a WTUnit con numero di unità \"{1}\".")
   public static final String NULL_IS_RECEIVED = "11";

   @RBEntry("Impossibile recuperare il file manifest da <{0}>.")
   public static final String EMB_BAD_MANIFEST = "12";

   @RBEntry("La cartella <{0}> non esiste o non è una cartella.")
   public static final String EMB_NO_FOLDER = "13";

   @RBEntry("L'InputStream per il file <{0}> non è valido.")
   public static final String EMB_BAD_CONTENT = "14";

   @RBEntry("Nessun utente/gruppo/ruolo è stato assegnato al sito <{0}> dal sito <{1}>.")
   public static final String NO_PRINCIPAL = "15";

   @RBEntry("Impossibile trovare la cartella per salvare WTUnit per - nome unità: {0}, numero: <{1}>, team: <{2}> nel sito: <{3}>.")
   public static final String NULL_UNIT_FOLDER = "16";

   @RBEntry("Il sito <{0}> non è un sito peer di replica di prodotto nel sito <{1}>.")
   public static final String NOT_PEER_SITE = "17";

   @RBEntry("L'operazione di ricezione è iniziata {0}.")
   public static final String RECIEVE_STARTED = "18";

   @RBEntry("L'operazione di ricezione è terminata {0}.")
   public static final String RECIEVE_FINISHED = "19";

   @RBEntry("Impossibile trovare l'oggetto UFID corrispondente in <{0}>.")
   public static final String BAD_UFID = "20";

   @RBEntry("Non è stato trovato alcun oggetto ApplicationData per l'oggetto Federatable <{0}>.")
   public static final String NO_APP_DATA = "21";

   @RBEntry("Impossibile aggiornare lo stato di <{1}> per l'unità di replica <{0}>.")
   public static final String SET_RECV_STATUS_FAILED = "22";

   @RBEntry("Impossibile estrarre il contenuto dal sito <{0}> con URL <{1}>.")
   public static final String UPLOAD_CONTENT_FAUILED = "23";

   @RBEntry("Record ricezione unità")
   public static final String UNIT_RECEIVER_RECORD = "24";

   @RBEntry("Impossibile salvare l'unità di replica <{0}>, associata al team <{1}> nello schedario personale <{2}>.")
   public static final String TEAMTEMPLATE_FOLDER_IN_PERSONAL_CABINET = "25";
}
