/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.replication.receiver;

import wt.util.resource.*;

@RBUUID("wt.replication.receiver.receiverResource")
public final class receiverResource extends WTListResourceBundle {
   @RBEntry("Can not create WTUnit object with name \"{0}\" at folder \"{1}\" and save it.")
   public static final String WTUNIT_CREATION_FAILED = "0";

   @RBEntry("Update content file (or InputStream) to replication unit \"{0}\" with unit number \"{1}\" failed.")
   public static final String UPDATE_CONTENT_FAILED = "1";

   @RBEntry("Can not set ContentRoleType {1} to ApplicationData with Identity \"{0}\".")
   public static final String SET_ROLETYPE_FAILED = "2";

   @RBEntry("Null Timestamp found, but onSchedule is true.")
   public static final String NULL_TIMESTAMP = "3";

   @RBEntry("HTTP Request to {0} failed. ")
   public static final String HTTP_REQUEST_FAILED = "4";

   @RBEntry("The associated Site with WTUnit with name \"{0}\" and number \"{1}\" does not exist .")
   public static final String SITE_NOT_UNIQUE = "5";

   @RBEntry("Authentication to \"{0}\" Failed.")
   public static final String AUTHENTICATION_FAILED = "6";

   @RBEntry("The requested protocol \"{1}\" found in \"{0}\" is not supported.")
   public static final String PROTOCOL_NOT_SUPPORTED = "7";

   @RBEntry("The Site object does not exist for URL \"{0}\".")
   public static final String SITE_NOT_FOUND = "8";

   @RBEntry("The CameFromUnit object not unique for URL \"{0}\" and WTUnit number {1}.")
   public static final String CAME_FROM_UNIT_NOT_UNIQUE = "9";

   @RBEntry("Create scheduling queue for WTUnit object with name: <{0}> and number <{1}> failed.")
   public static final String CREATE_SCHEDULEQUEUE_FAILED = "10";

   @RBEntry("Null InputStream received for URL \"{0}\" associated with WTUnit with unit number \"{1}\".")
   public static final String NULL_IS_RECEIVED = "11";

   @RBEntry("Can not retrieve manifest from <{0}>.")
   public static final String EMB_BAD_MANIFEST = "12";

   @RBEntry("The folder <{0}> does not exist or is not a folder.")
   public static final String EMB_NO_FOLDER = "13";

   @RBEntry("The InputStream for file <{0}> is corrupted.")
   public static final String EMB_BAD_CONTENT = "14";

   @RBEntry("No principal assigned to site <{0}> by site <{1}>.")
   public static final String NO_PRINCIPAL = "15";

   @RBEntry("Can not find folder to save WTUnit for - Unit name: {0}, number: <{1}>, Team: <{2}> at Site: <{3}>.")
   public static final String NULL_UNIT_FOLDER = "16";

   @RBEntry("The Site <{0}> is not a Product Replication Peer Site at Site <{1}>.")
   public static final String NOT_PEER_SITE = "17";

   @RBEntry("The Recieve Operation is Started on {0}.")
   public static final String RECIEVE_STARTED = "18";

   @RBEntry("The Recieve Operation is Finished on {0}.")
   public static final String RECIEVE_FINISHED = "19";

   @RBEntry("Can not find the corresponding Ufid object from <{0}>.")
   public static final String BAD_UFID = "20";

   @RBEntry("No ApplicationData object found for Federatable object <{0}}>.")
   public static final String NO_APP_DATA = "21";

   @RBEntry("Unable to update the status to <{1}> for replication unit <{0}>.")
   public static final String SET_RECV_STATUS_FAILED = "22";

   @RBEntry("Failed to pull content from site <{0}> with URL <{1}>.")
   public static final String UPLOAD_CONTENT_FAUILED = "23";

   @RBEntry("Unit Receive Record")
   public static final String UNIT_RECEIVER_RECORD = "24";

   @RBEntry("The replication unit <{0}>, associated with team <{1}>, can not be saved in personal cabinet <{2}>.")
   public static final String TEAMTEMPLATE_FOLDER_IN_PERSONAL_CABINET = "25";
}
