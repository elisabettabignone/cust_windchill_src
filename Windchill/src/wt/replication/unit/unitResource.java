/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.replication.unit;

import wt.util.resource.*;

@RBUUID("wt.replication.unit.unitResource")
public final class unitResource extends WTListResourceBundle {
   /**
    * Messages
    * 
    **/
   @RBEntry("The value for the argument \"{0}\" can not be null")
   public static final String NULL_ARGUMENT = "0";

   @RBEntry("The number attribute must have a value")
   public static final String NULL_NUMBER = "1";

   @RBEntry("The name attribute must have a value")
   public static final String NULL_NAME = "2";

   @RBEntry("The replication unit not found by number: \"{0}\"")
   public static final String UNIT_NOT_FOUND = "3";

   /**
    * htmlGeneration messages
    * 
    **/
   @RBEntry("The context object is not {0}.")
   public static final String OBJECT_NOT = "4";

   @RBEntry("Error initializing \"{0}\".")
   public static final String ERROR_INITIALIZING = "5";

   @RBEntry("Send Unit")
   public static final String SEND_UNIT_URL_LABEL = "6";

   @RBEntry("Invalid applet parameters. \"{0}\" ")
   public static final String INVALID_PARAMETERS = "7";

   @RBEntry("Exception retrieving the object: \"{0}\" ")
   public static final String RETRIEVING_OBJECT = "8";

   @RBEntry("Update")
   public static final String UPDATE = "10";

   @RBEntry("View")
   public static final String VIEW = "11";

   @RBEntry("Unit")
   public static final String SEND_UNIT_NAME = "12";

   @RBEntry("Version")
   public static final String SEND_UNIT_VERSION = "13";

   @RBEntry("Description")
   public static final String SEND_UNIT_DESCRIPTION = "14";

   @RBEntry("State")
   public static final String SEND_UNIT_STATE = "15";

   @RBEntry("Destination")
   public static final String SEND_UNIT_SITE = "16";

   @RBEntry("Full Project Name")
   public static final String SEND_UNIT_DOMAIN_PROJ = "17";

   @RBEntry("Send Manifest on")
   public static final String SEND_UNIT_MANIFEST_ON = "18";

   @RBEntry("Send Manifest at")
   public static final String SEND_UNIT_MANIFEST_AT = "19";

   @RBEntry("Send content")
   public static final String SEND_UNIT_CONTENT = "20";

   @RBEntry("on demand")
   public static final String SEND_UNIT_ON_DEMAND = "21";

   @RBEntry("on schedule")
   public static final String SEND_UNIT_ON_SCHEDULE = "22";

   @RBEntry("Submit")
   public static final String SEND_UNIT_SUBMIT = "24";

   @RBEntry("Send Unit Results")
   public static final String SEND_RESULTS_TITLE = "25";

   @RBEntry("Export Unit")
   public static final String EXPORT_UNIT_URL_LABEL = "26";

   @RBEntry("Export Unit Results")
   public static final String EXPORT_RESULTS_TITLE = "27";

   @RBEntry("Import Unit")
   public static final String IMPORT_UNIT_URL_LABEL = "28";

   @RBEntry("Import Unit Results")
   public static final String IMPORT_RESULTS_TITLE = "29";

   @RBEntry("Receive Unit")
   public static final String RECEIVE_UNIT_URL_LABEL = "30";

   @RBEntry("Receive Unit Results")
   public static final String RECEIVE_RESULTS_TITLE = "31";

   @RBEntry("Unit Parts")
   public static final String LISTPARTS_UNIT_URL_LABEL = "32";

   @RBEntry("Objects Contained In Unit")
   public static final String LIST_UNIT_PARTS_TITLE = "33";

   @RBEntry("The unit was sent.")
   public static final String SEND_UNIT_COMPLETE = "34";

   @RBEntry("The unit failed to be sent.")
   public static final String SEND_UNIT_FAILED = "35";

   @RBEntry("The error is:")
   public static final String UNIT_ERROR_IS = "36";

   @RBEntry("Export Status")
   public static final String EXPORTSTATUS_UNIT_URL_LABEL = "37";

   @RBEntry("Export Status Report")
   public static final String EXPORTSTATUS_UNIT_TITLE = "38";

   @RBEntry("An error occurred. See system administrator.")
   public static final String INTERAL_ERROR = "39";

   @RBEntry("Export")
   public static final String EXPORT = "40";

   @RBEntry("Immediate")
   public static final String IMMEDIATE_LABEL = "41";

   @RBEntry("On Schedule")
   public static final String ON_SCHEDULE_LABEL = "42";

   @RBEntry("Start Time")
   public static final String START_TIME_LABEL = "43";

   @RBEntry("Start Date")
   public static final String START_DATE_LABEL = "44";

   @RBEntry("Submit")
   public static final String SUBMIT_LABEL = "45";

   @RBEntry("Export Unit")
   public static final String EXPORT_UNIT_TITLE = "46";

   @RBEntry("This unit has not been scheduled for export or the scheduled export has not yet occurred!")
   public static final String EXPORT_NOT_SCHEDULED = "47";

   @RBEntry("Invalid date or time.")
   public static final String INVALID_TIMESTAMP = "48";

   @RBEntry("The unit has been scheduled for export.")
   public static final String EXPORT_SCHEDULED = "49";

   @RBEntry("The unit was already scheduled for export.")
   public static final String EXPORT_ALREADY_SCHEDULED = "50";

   @RBEntry("Suspend")
   public static final String SUSPEND_LABEL = "51";

   @RBEntry("Resume")
   public static final String RESUME_LABEL = "52";

   @RBEntry("Edit")
   public static final String EDIT_LABEL = "53";

   @RBEntry("Delete")
   public static final String DELETE_LABEL = "54";

   @RBEntry("The scheduling process is already suspended for this item.")
   public static final String CANNOT_SUSPEND = "55";

   @RBEntry("The scheduling process has already been resumed for this item.")
   public static final String CANNOT_RESUME = "56";

   @RBEntry("The scheduled export of this item is complete.")
   public static final String EXPORT_COMPLETE = "57";

   @RBEntry("The scheduled item cannot be edited.")
   public static final String CANNOT_EDIT = "58";

   @RBEntry("The scheduled item cannot be deleted.")
   public static final String CANNOT_DELETE = "59";

   @RBEntry("The scheduling process for this item has been suspened.")
   public static final String ITEM_SUSPENED = "60";

   @RBEntry("The scheduling process for this item has been resumed.")
   public static final String ITEM_RESUMED = "61";

   @RBEntry("The scheduled item been deleted.")
   public static final String ITEM_DELETED = "62";

   @RBEntry("Send Unit")
   public static final String SEND_UNIT_TITLE = "63";

   @RBEntry("The unit has been scheduled to be sent.")
   public static final String SEND_SCHEDULED = "64";

   @RBEntry("The unt was not abled to be scheduled.")
   public static final String SEND_NOT_SCHEDULED = "65";

   @RBEntry("Send Status")
   public static final String SENDSTATUS_UNIT_URL_LABEL = "66";

   @RBEntry("Send Status Report")
   public static final String SENDSTATUS_UNIT_TITLE = "67";

   @RBEntry("Unsupported Seed Object Operation")
   public static final String UNSUPPORTED_SEED_OPER = "68";

   @RBEntry("Inconsistent Unit Baseline Operation")
   public static final String INCONSISTENT_UNIT_BASELINE_OPER = "69";

   @RBEntry("The same seed object linked to the unit more than one time.")
   public static final String DUPLICATED_SEED_OBJECT = "70";

   @RBEntry("Unsupported Unit ConfigSpec operation.")
   public static final String UNSUPPORTED_CONFIG_SPEC_OPER = "71";

   @RBEntry("ConfigSpec is inconsistent with set mode.")
   public static final String INCONSISTENT_CONFIG_SPEC = "72";

   @RBEntry("Replication Baseline already exists with opposite type (as Imported vs. as Exported).")
   public static final String UNIT_BASELINE_EXIST = "73";

   @RBEntry("Unsupported Unit Baseline operation.")
   public static final String UNSUPPORTED_BASELINE_OPER = "74";

   @RBEntry("ReplicationProdStructClass = \"{0}\" does not implement ReplicationProductStruct interface.")
   public static final String PROD_STRUC_CL_DOES_NOT_IMPL_INT = "75";

   @RBEntry("ReplicationProdStructClass = \"{0}\" does not exist.")
   public static final String PROD_STRUC_CL_DOES_NOT_EXIST = "76";

   @RBEntry("ReplicationProdStructClass = \"{0}\" : class object cannot be instantiated because it is an interface or is an abstract class.")
   public static final String PROD_STRUC_CL_INST_EXC = "77";

   @RBEntry("ReplicationProdStructClass = \"{0}\" is not public and resides in other package than wt.replication.unit.*")
   public static final String PROD_STRUC_CL_ILLEG_ACC_EXC = "78";

   @RBEntry("The object <{0}> is associated with Read-only Replication and can not be modified.")
   public static final String INVALID_READ_ONLY_REPL_OPER = "79";

   @RBEntry("Received or Imported unit \"{0}\" can not be revised.")
   public static final String NO_REVISION_FOR_UNIT = "80";

   @RBEntry("Exported, Received or Imported unit \"{0}\" can not be checked out.")
   public static final String NO_CHECKOUT_FOR_UNIT = "81";

   @RBEntry("Exported, Received or Imported unit \"{0}\" can not be updated.")
   public static final String NO_UPDATE_FOR_UNIT = "82";

   @RBEntry("Replication Unit")
   public static final String UNIT_DISPLAY_TYPE = "83";
}
