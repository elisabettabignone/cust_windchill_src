/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.replication.unit.htmlGeneration;

import wt.util.resource.*;

@RBUUID("wt.replication.unit.htmlGeneration.htmlGenerationResource")
public final class htmlGenerationResource_it extends WTListResourceBundle {
   @RBEntry("L'oggetto del contesto non è {0}.")
   public static final String OBJECT_NOT = "1";

   @RBEntry("Errore durante l'inizializzazione di \"{0}\".")
   public static final String ERROR_INITIALIZING = "2";

   @RBEntry("Invia unità")
   public static final String SEND_UNIT_URL_LABEL = "3";

   @RBEntry("Parametri applet non validi. \"{0}\"")
   public static final String INVALID_PARAMETERS = "4";

   @RBEntry("Si è verificata un'eccezione durante il recupero dell'oggetto: \"{0}\"")
   public static final String RETRIEVING_OBJECT = "5";

   @RBEntry("Aggiorna")
   public static final String UPDATE = "7";

   @RBEntry("Visualizza")
   public static final String VIEW = "8";

   @RBEntry("Nome unità")
   public static final String SEND_UNIT_NAME = "9";

   @RBEntry("Versione unità")
   public static final String SEND_UNIT_VERSION = "10";

   @RBEntry("Descrizione")
   public static final String SEND_UNIT_DESCRIPTION = "11";

   @RBEntry("Stato del ciclo di vita")
   public static final String SEND_UNIT_STATE = "12";

   @RBEntry("Sito di destinazione")
   public static final String SEND_UNIT_SITE = "13";

   @RBEntry("Team di destinazione")
   public static final String SEND_UNIT_DOMAIN_TEAMTEMPLATE = "14";

   @RBEntry("Invia manifest il")
   public static final String SEND_UNIT_MANIFEST_ON = "15";

   @RBEntry("Invia manifest a")
   public static final String SEND_UNIT_MANIFEST_AT = "16";

   @RBEntry("Invia contenuto")
   public static final String SEND_UNIT_CONTENT = "17";

   @RBEntry("su richiesta")
   public static final String SEND_UNIT_ON_DEMAND = "18";

   @RBEntry("momento programmato")
   public static final String SEND_UNIT_ON_SCHEDULE = "19";

   @RBEntry("Invia")
   public static final String SEND_UNIT_SUBMIT = "20";

   @RBEntry("Risultati dell'invio dell'unità")
   public static final String SEND_RESULTS_TITLE = "21";

   @RBEntry("Esporta unità")
   public static final String EXPORT_UNIT_URL_LABEL = "22";

   @RBEntry("Risultati dell'esportazione dell'unità")
   public static final String EXPORT_RESULTS_TITLE = "23";

   @RBEntry("Importa unità")
   public static final String IMPORT_UNIT_URL_LABEL = "24";

   @RBEntry("Risultati dell'importazione dell'unità")
   public static final String IMPORT_RESULTS_TITLE = "25";

   @RBEntry("Ricevi unità")
   public static final String RECEIVE_UNIT_URL_LABEL = "26";

   @RBEntry("Risultati della ricezione dell'unità")
   public static final String RECEIVE_RESULTS_TITLE = "27";

   @RBEntry("Parti unità")
   public static final String LISTPARTS_UNIT_URL_LABEL = "28";

   @RBEntry("Oggetti contenuti nell'unità")
   public static final String LIST_UNIT_PARTS_TITLE = "29";

   @RBEntry("Unità inviata.")
   public static final String SEND_UNIT_COMPLETE = "30";

   @RBEntry("L'invio dell'unità è fallito.")
   public static final String SEND_UNIT_FAILED = "31";

   @RBEntry("L'errore è:")
   public static final String UNIT_ERROR_IS = "32";

   @RBEntry("Stato esportazione")
   public static final String EXPORTSTATUS_UNIT_URL_LABEL = "33";

   @RBEntry("Report dello stato dell'esportazione")
   public static final String EXPORTSTATUS_UNIT_TITLE = "34";

   @RBEntry("Errore. Consultare l'amministratore di sistema.")
   public static final String INTERAL_ERROR = "35";

   @RBEntry("Esportazione")
   public static final String EXPORT = "36";

   @RBEntry("Immediata")
   public static final String IMMEDIATE_LABEL = "37";

   @RBEntry("Momento programmato")
   public static final String ON_SCHEDULE_LABEL = "38";

   @RBEntry("Ora d'inizio")
   public static final String START_TIME_LABEL = "39";

   @RBEntry("Data d'inizio")
   public static final String START_DATE_LABEL = "40";

   @RBEntry("Invia")
   public static final String SUBMIT_LABEL = "41";

   @RBEntry("Esporta unità")
   public static final String EXPORT_UNIT_TITLE = "42";

   @RBEntry("Questa unità non è stata programmata per l'esportazione oppure l'esportazione programmata non si è ancora verificata oppure l'attività programmata è stata eliminata.")
   public static final String EXPORT_NOT_SCHEDULED = "43";

   @RBEntry("Ora o data non validi.")
   public static final String INVALID_TIMESTAMP = "44";

   @RBEntry("L'unità è stata programmata per l'esportazione.")
   public static final String EXPORT_SCHEDULED = "45";

   @RBEntry("L'unità era già programmata per l'esportazione.")
   public static final String EXPORT_ALREADY_SCHEDULED = "46";

   @RBEntry("Sospendi")
   public static final String SUSPEND_LABEL = "47";

   @RBEntry("Riprendi")
   public static final String RESUME_LABEL = "48";

   @RBEntry("Modifica")
   public static final String EDIT_LABEL = "49";

   @RBEntry("Elimina")
   public static final String DELETE_LABEL = "50";

   @RBEntry("Il processo di programmazione è già sospeso per questa attività.")
   public static final String CANNOT_SUSPEND = "51";

   @RBEntry("Il processo di programmazione è già stato ripreso per questa attività.")
   public static final String CANNOT_RESUME = "52";

   @RBEntry("L'esportazione programmata di questa attività è completa.")
   public static final String EXPORT_COMPLETE = "53";

   @RBEntry("L'attività programmata non può essere modificata.")
   public static final String CANNOT_EDIT = "54";

   @RBEntry("L'attività programmata non può essere eliminata.")
   public static final String CANNOT_DELETE = "55";

   @RBEntry("Il processo di programmazione per questa attività è stato sospeso.")
   public static final String ITEM_SUSPENED = "56";

   @RBEntry("Il processo di programmazione per questa attività è stato ripreso.")
   public static final String ITEM_RESUMED = "57";

   @RBEntry("L'attività programmata è stata eliminata.")
   public static final String ITEM_DELETED = "58";

   @RBEntry("Invia unità")
   public static final String SEND_UNIT_TITLE = "59";

   @RBEntry("È stato programmato l'invio dell'unità.")
   public static final String SEND_SCHEDULED = "60";

   @RBEntry("Non è stato possibile programmare l'invio dell'unità.")
   public static final String SEND_NOT_SCHEDULED = "61";

   @RBEntry("Stato invio")
   public static final String SENDSTATUS_UNIT_URL_LABEL = "62";

   @RBEntry("Report dello stato di invio")
   public static final String SENDSTATUS_UNIT_TITLE = "63";

   @RBEntry("Numero")
   public static final String NUMBER_LABEL = "64";

   @RBEntry("Nome")
   public static final String NAME_LABEL = "65";

   @RBEntry("Tipo")
   public static final String TYPE_LABEL = "66";

   @RBEntry("Versione")
   public static final String VERSION_LABEL = "67";

   @RBEntry("Data ultima modifica")
   public static final String LASTUPDATE_LABEL = "68";

   @RBEntry("Stato programmazione")
   public static final String SCHEDULE_STATUS_LABEL = "69";

   @RBEntry("L'unità non contiene oggetti.")
   public static final String UNIT_CONTAIN_NO_OBJECTS = "70";

   @RBEntry("Ricevi")
   public static final String RECEIVE = "71";

   @RBEntry("La ricezione dell'unità è già stata programmata.")
   public static final String RECEIVE_ALREADY_SCHEDULED = "72";

   @RBEntry("La ricezione dell'unità è stata programmata.")
   public static final String RECEIVE_SCHEDULED = "73";

   @RBEntry("Stato ricezione")
   public static final String RECEIVESTATUS_UNIT_URL_LABEL = "74";

   @RBEntry("Stato importazione")
   public static final String IMPORTSTATUS_UNIT_URL_LABEL = "75";

   @RBEntry("Report dello stato di importazione")
   public static final String IMPORTSTATUS_UNIT_TITLE = "76";

   @RBEntry("Report dello stato di ricezione")
   public static final String RECEIVESTATUS_UNIT_TITLE = "77";

   @RBEntry("Importa")
   public static final String IMPORT = "78";

   @RBEntry("L'importazione dell'unità è già stata programmata.")
   public static final String IMPORT_ALREADY_SCHEDULED = "79";

   @RBEntry("L'importazione dell'unità è stata programmata.")
   public static final String IMPORT_SCHEDULED = "80";

   @RBEntry("L'unità non era abilitata alla programmazione per l'importazione.")
   public static final String IMPORT_NOT_SCHEDULED = "81";

   @RBEntry("L'unità non era abilitata alla programmazione per la ricezione.")
   public static final String RECEIVE_NOT_SCHEDULED = "82";

   @RBEntry("Programmazione in dettaglio")
   public static final String SCHEDULE_DETAIL = "83";

   @RBEntry("Inizio programmazione")
   public static final String SCHEDULE_START = "84";

   @RBEntry("Inizio esecuzione")
   public static final String EXECUTION_START = "85";

   @RBEntry("Fine esecuzione")
   public static final String EXECUTION_FINISH = "86";

   @RBEntry("Sospeso")
   public static final String SUSPENDED_LABEL = "87";

   @RBEntry("True")
   public static final String TRUE_LABEL = "88";

   @RBEntry("False")
   public static final String FALSE_LABEL = "89";

   @RBEntry("Struttura di prodotto")
   public static final String PRODUCT_STRUCTURE_LABEL = "90";

   @RBEntry("La pagina non è aggiornata. Aggiornare.")
   public static final String REFRESH_PAGE = "91";

   @RBEntry("Nome unità")
   public static final String UNIT_NAME_LABEL = "92";

   @RBEntry("L'operazione di programmazione è fallita.")
   public static final String SCHEDULING_FAILURE = "93";

   @RBEntry("L'unità era già stata ricevuta.")
   public static final String ALREADY_RECEIVED = "94";

   @RBEntry("L'unità era già esportata.")
   public static final String ALREADY_EXPORTED = "95";

   @RBEntry("L'unità era già stata importata.")
   public static final String ALREADY_IMPORTED = "96";

   @RBEntry("Modifica l'esportazione dell'unità")
   public static final String EDIT_EXPORT_UNIT_TITLE = "97";

   @RBEntry("Modifica l'invio dell'unità")
   public static final String EDIT_SEND_UNIT_TITLE = "98";

   @RBEntry("Modifica la ricezione dell'unità")
   public static final String EDIT_RECEIVE_UNIT_TITLE = "99";

   @RBEntry("Modifica l'importazione dell'unità")
   public static final String EDIT_IMPORT_UNIT_TITLE = "100";

   @RBEntry("Tipo di trasporto")
   public static final String TRANSPORT_TYPE = "101";

   @RBEntry("Cartella Supporti esterni")
   public static final String EMB_FOLDER = "102";

   @RBEntry("Trasporto con supporti esterni")
   public static final String EMB_LABEL = "103";

   @RBEntry("Il nome del team di destinazione è nullo o non valido.")
   public static final String INVALID_TEAMTEMPLATE_NAME = "104";

   @RBEntry("Destinazione nulla o invalida.")
   public static final String INVALID_DESTINATION = "105";

   @RBEntry("Continua")
   public static final String SEND_UNIT_CONTINUE = "106";

   @RBEntry("Team unità")
   public static final String SEND_UNIT_TEAMTEMPLATE = "107";

   @RBEntry("Formato data:   GG/MM/AA(AAAA)")
   public static final String DATE_FORMAT_LABEL = "108";

   @RBEntry("Formato ora:   HH:MM ")
   public static final String TIME_FORMAT_LABEL = "109";

   @RBEntry("Commento")
   public static final String COMMENT_LABEL = "110";

   @RBEntry("Visualizza log")
   public static final String VIEW_LOG = "111";

   @RBEntry("File regole")
   public static final String RULES_FILE = "112";

   @RBEntry("Le operazioni di esportazione, invio e ricezione non sono consentite nello schedario personale")
   public static final String NO_OP_IN_PERSONAL_CABINET = "113";
}
