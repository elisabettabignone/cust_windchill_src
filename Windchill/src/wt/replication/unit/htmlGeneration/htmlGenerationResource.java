/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.replication.unit.htmlGeneration;

import wt.util.resource.*;

@RBUUID("wt.replication.unit.htmlGeneration.htmlGenerationResource")
public final class htmlGenerationResource extends WTListResourceBundle {
   @RBEntry("The context object is not {0}.")
   public static final String OBJECT_NOT = "1";

   @RBEntry("Error initializing \"{0}\".")
   public static final String ERROR_INITIALIZING = "2";

   @RBEntry("Send Unit")
   public static final String SEND_UNIT_URL_LABEL = "3";

   @RBEntry("Invalid applet parameters. \"{0}\" ")
   public static final String INVALID_PARAMETERS = "4";

   @RBEntry("Exception retrieving the object: \"{0}\" ")
   public static final String RETRIEVING_OBJECT = "5";

   @RBEntry("Update")
   public static final String UPDATE = "7";

   @RBEntry("View")
   public static final String VIEW = "8";

   @RBEntry("Unit Name")
   public static final String SEND_UNIT_NAME = "9";

   @RBEntry("Unit Version")
   public static final String SEND_UNIT_VERSION = "10";

   @RBEntry("Description")
   public static final String SEND_UNIT_DESCRIPTION = "11";

   @RBEntry("State")
   public static final String SEND_UNIT_STATE = "12";

   @RBEntry("Destination Site")
   public static final String SEND_UNIT_SITE = "13";

   @RBEntry("Destination Team")
   public static final String SEND_UNIT_DOMAIN_TEAMTEMPLATE = "14";

   @RBEntry("Send Manifest on")
   public static final String SEND_UNIT_MANIFEST_ON = "15";

   @RBEntry("Send Manifest at")
   public static final String SEND_UNIT_MANIFEST_AT = "16";

   @RBEntry("Send content")
   public static final String SEND_UNIT_CONTENT = "17";

   @RBEntry("on demand")
   public static final String SEND_UNIT_ON_DEMAND = "18";

   @RBEntry("on schedule")
   public static final String SEND_UNIT_ON_SCHEDULE = "19";

   @RBEntry("Submit")
   public static final String SEND_UNIT_SUBMIT = "20";

   @RBEntry("Send Unit Results")
   public static final String SEND_RESULTS_TITLE = "21";

   @RBEntry("Export Unit")
   public static final String EXPORT_UNIT_URL_LABEL = "22";

   @RBEntry("Export Unit Results")
   public static final String EXPORT_RESULTS_TITLE = "23";

   @RBEntry("Import Unit")
   public static final String IMPORT_UNIT_URL_LABEL = "24";

   @RBEntry("Import Unit Results")
   public static final String IMPORT_RESULTS_TITLE = "25";

   @RBEntry("Receive Unit")
   public static final String RECEIVE_UNIT_URL_LABEL = "26";

   @RBEntry("Receive Unit Results")
   public static final String RECEIVE_RESULTS_TITLE = "27";

   @RBEntry("Unit Parts")
   public static final String LISTPARTS_UNIT_URL_LABEL = "28";

   @RBEntry("Objects Contained In Unit")
   public static final String LIST_UNIT_PARTS_TITLE = "29";

   @RBEntry("The unit was sent.")
   public static final String SEND_UNIT_COMPLETE = "30";

   @RBEntry("The unit failed to be sent.")
   public static final String SEND_UNIT_FAILED = "31";

   @RBEntry("The error is:")
   public static final String UNIT_ERROR_IS = "32";

   @RBEntry("Export Status")
   public static final String EXPORTSTATUS_UNIT_URL_LABEL = "33";

   @RBEntry("Export Status Report")
   public static final String EXPORTSTATUS_UNIT_TITLE = "34";

   @RBEntry("An error occurred. See system administrator.")
   public static final String INTERAL_ERROR = "35";

   @RBEntry("Export")
   public static final String EXPORT = "36";

   @RBEntry("Immediate")
   public static final String IMMEDIATE_LABEL = "37";

   @RBEntry("On Schedule")
   public static final String ON_SCHEDULE_LABEL = "38";

   @RBEntry("Start Time")
   public static final String START_TIME_LABEL = "39";

   @RBEntry("Start Date")
   public static final String START_DATE_LABEL = "40";

   @RBEntry("Submit")
   public static final String SUBMIT_LABEL = "41";

   @RBEntry("Export Unit")
   public static final String EXPORT_UNIT_TITLE = "42";

   @RBEntry("This unit has not been scheduled for export or the scheduled export has not yet occurred or the scheduled item has been deleted!")
   public static final String EXPORT_NOT_SCHEDULED = "43";

   @RBEntry("Invalid date or time.")
   public static final String INVALID_TIMESTAMP = "44";

   @RBEntry("The unit has been scheduled for export.")
   public static final String EXPORT_SCHEDULED = "45";

   @RBEntry("The unit was already scheduled for export.")
   public static final String EXPORT_ALREADY_SCHEDULED = "46";

   @RBEntry("Suspend")
   public static final String SUSPEND_LABEL = "47";

   @RBEntry("Resume")
   public static final String RESUME_LABEL = "48";

   @RBEntry("Edit")
   public static final String EDIT_LABEL = "49";

   @RBEntry("Delete")
   public static final String DELETE_LABEL = "50";

   @RBEntry("The scheduling process is already suspended for this item.")
   public static final String CANNOT_SUSPEND = "51";

   @RBEntry("The scheduling process has already been resumed for this item.")
   public static final String CANNOT_RESUME = "52";

   @RBEntry("The scheduled export of this item is complete.")
   public static final String EXPORT_COMPLETE = "53";

   @RBEntry("The scheduled item cannot be edited.")
   public static final String CANNOT_EDIT = "54";

   @RBEntry("The scheduled item cannot be deleted.")
   public static final String CANNOT_DELETE = "55";

   @RBEntry("The scheduling process for this item has been suspened.")
   public static final String ITEM_SUSPENED = "56";

   @RBEntry("The scheduling process for this item has been resumed.")
   public static final String ITEM_RESUMED = "57";

   @RBEntry("The scheduled item has been deleted.")
   public static final String ITEM_DELETED = "58";

   @RBEntry("Send Unit")
   public static final String SEND_UNIT_TITLE = "59";

   @RBEntry("The unit has been scheduled to be sent.")
   public static final String SEND_SCHEDULED = "60";

   @RBEntry("The unit was not abled to be scheduled to be sent.")
   public static final String SEND_NOT_SCHEDULED = "61";

   @RBEntry("Send Status")
   public static final String SENDSTATUS_UNIT_URL_LABEL = "62";

   @RBEntry("Send Status Report")
   public static final String SENDSTATUS_UNIT_TITLE = "63";

   @RBEntry("Number")
   public static final String NUMBER_LABEL = "64";

   @RBEntry("Name")
   public static final String NAME_LABEL = "65";

   @RBEntry("Type")
   public static final String TYPE_LABEL = "66";

   @RBEntry("Version")
   public static final String VERSION_LABEL = "67";

   @RBEntry("Last Modified")
   public static final String LASTUPDATE_LABEL = "68";

   @RBEntry("Schedule Status")
   public static final String SCHEDULE_STATUS_LABEL = "69";

   @RBEntry("The unit contains no objects.")
   public static final String UNIT_CONTAIN_NO_OBJECTS = "70";

   @RBEntry("Receive")
   public static final String RECEIVE = "71";

   @RBEntry("The unit is already scheduled to be received.")
   public static final String RECEIVE_ALREADY_SCHEDULED = "72";

   @RBEntry("The unit has been scheduled to be received.")
   public static final String RECEIVE_SCHEDULED = "73";

   @RBEntry("Receive Status")
   public static final String RECEIVESTATUS_UNIT_URL_LABEL = "74";

   @RBEntry("Import Status")
   public static final String IMPORTSTATUS_UNIT_URL_LABEL = "75";

   @RBEntry("Import Status Report")
   public static final String IMPORTSTATUS_UNIT_TITLE = "76";

   @RBEntry("Receive Status Report")
   public static final String RECEIVESTATUS_UNIT_TITLE = "77";

   @RBEntry("Import")
   public static final String IMPORT = "78";

   @RBEntry("The unit was already scheduled for import.")
   public static final String IMPORT_ALREADY_SCHEDULED = "79";

   @RBEntry("The unit is scheduled for import.")
   public static final String IMPORT_SCHEDULED = "80";

   @RBEntry("The unit was not abled to be scheduled for import.")
   public static final String IMPORT_NOT_SCHEDULED = "81";

   @RBEntry("The unit was not abled to be scheduled to be received.")
   public static final String RECEIVE_NOT_SCHEDULED = "82";

   @RBEntry("Schedule Detail")
   public static final String SCHEDULE_DETAIL = "83";

   @RBEntry("Schedule Start")
   public static final String SCHEDULE_START = "84";

   @RBEntry("Execution Start")
   public static final String EXECUTION_START = "85";

   @RBEntry("Execution Finish")
   public static final String EXECUTION_FINISH = "86";

   @RBEntry("Suspended")
   public static final String SUSPENDED_LABEL = "87";

   @RBEntry("True")
   public static final String TRUE_LABEL = "88";

   @RBEntry("False")
   public static final String FALSE_LABEL = "89";

   @RBEntry("Product Structure")
   public static final String PRODUCT_STRUCTURE_LABEL = "90";

   @RBEntry("The page is stale. Please refresh the page.")
   public static final String REFRESH_PAGE = "91";

   @RBEntry("Unit Name")
   public static final String UNIT_NAME_LABEL = "92";

   @RBEntry("The scheduling operation failed.")
   public static final String SCHEDULING_FAILURE = "93";

   @RBEntry("The unit was already received.")
   public static final String ALREADY_RECEIVED = "94";

   @RBEntry("The unit was already exported.")
   public static final String ALREADY_EXPORTED = "95";

   @RBEntry("The unit was already imported.")
   public static final String ALREADY_IMPORTED = "96";

   @RBEntry("Edit Export Unit")
   public static final String EDIT_EXPORT_UNIT_TITLE = "97";

   @RBEntry("Edit Send Unit")
   public static final String EDIT_SEND_UNIT_TITLE = "98";

   @RBEntry("Edit Receive Unit")
   public static final String EDIT_RECEIVE_UNIT_TITLE = "99";

   @RBEntry("Edit Import Unit")
   public static final String EDIT_IMPORT_UNIT_TITLE = "100";

   @RBEntry("Transport Type")
   public static final String TRANSPORT_TYPE = "101";

   @RBEntry("External Media Folder")
   public static final String EMB_FOLDER = "102";

   @RBEntry("External Media Based Transport")
   public static final String EMB_LABEL = "103";

   @RBEntry("The destination Team name is null or invalid.")
   public static final String INVALID_TEAMTEMPLATE_NAME = "104";

   @RBEntry("The destination is null or invalid.")
   public static final String INVALID_DESTINATION = "105";

   @RBEntry("Continue")
   public static final String SEND_UNIT_CONTINUE = "106";

   @RBEntry("Unit Team")
   public static final String SEND_UNIT_TEAMTEMPLATE = "107";

   @RBEntry("Date Format:   M(MM)/DD/YY(YYYY)")
   public static final String DATE_FORMAT_LABEL = "108";

   @RBEntry("Time Format:   H(HH):MM AM(PM)")
   public static final String TIME_FORMAT_LABEL = "109";

   @RBEntry("Comment")
   public static final String COMMENT_LABEL = "110";

   @RBEntry("View Log")
   public static final String VIEW_LOG = "111";

   @RBEntry("Rules File")
   public static final String RULES_FILE = "112";

   @RBEntry("Export, send, receive and import operations are not allowed in personal cabinet")
   public static final String NO_OP_IN_PERSONAL_CABINET = "113";
}
