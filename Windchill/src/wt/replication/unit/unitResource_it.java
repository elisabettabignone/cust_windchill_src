/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.replication.unit;

import wt.util.resource.*;

@RBUUID("wt.replication.unit.unitResource")
public final class unitResource_it extends WTListResourceBundle {
   /**
    * Messages
    * 
    **/
   @RBEntry("Il valore per l'argomento \"{0}\" non può essere nullo")
   public static final String NULL_ARGUMENT = "0";

   @RBEntry("L'attributo numero deve avere un valore")
   public static final String NULL_NUMBER = "1";

   @RBEntry("All'attributo nome deve essere associato un valore")
   public static final String NULL_NAME = "2";

   @RBEntry("Impossibile trovare l'unità di replica per numero: \"{0}\"")
   public static final String UNIT_NOT_FOUND = "3";

   /**
    * htmlGeneration messages
    * 
    **/
   @RBEntry("L'oggetto del contesto non è {0}.")
   public static final String OBJECT_NOT = "4";

   @RBEntry("Errore durante l'inizializzazione di \"{0}\".")
   public static final String ERROR_INITIALIZING = "5";

   @RBEntry("Invia unità")
   public static final String SEND_UNIT_URL_LABEL = "6";

   @RBEntry("Parametri applet non validi. \"{0}\"")
   public static final String INVALID_PARAMETERS = "7";

   @RBEntry("Si è verificata un'eccezione durante il recupero dell'oggetto: \"{0}\"")
   public static final String RETRIEVING_OBJECT = "8";

   @RBEntry("Aggiorna")
   public static final String UPDATE = "10";

   @RBEntry("Vista")
   public static final String VIEW = "11";

   @RBEntry("Unità")
   public static final String SEND_UNIT_NAME = "12";

   @RBEntry("Versione")
   public static final String SEND_UNIT_VERSION = "13";

   @RBEntry("Descrizione")
   public static final String SEND_UNIT_DESCRIPTION = "14";

   @RBEntry("Stato del ciclo di vita")
   public static final String SEND_UNIT_STATE = "15";

   @RBEntry("Sito di destinazione")
   public static final String SEND_UNIT_SITE = "16";

   @RBEntry("Progetto di destinazione")
   public static final String SEND_UNIT_DOMAIN_PROJ = "17";

   @RBEntry("Invia manifest il")
   public static final String SEND_UNIT_MANIFEST_ON = "18";

   @RBEntry("Invia manifest a")
   public static final String SEND_UNIT_MANIFEST_AT = "19";

   @RBEntry("Invia contenuto")
   public static final String SEND_UNIT_CONTENT = "20";

   @RBEntry("su richiesta")
   public static final String SEND_UNIT_ON_DEMAND = "21";

   @RBEntry("momento programmato")
   public static final String SEND_UNIT_ON_SCHEDULE = "22";

   @RBEntry("Invia")
   public static final String SEND_UNIT_SUBMIT = "24";

   @RBEntry("Risultati dell'invio dell'unità")
   public static final String SEND_RESULTS_TITLE = "25";

   @RBEntry("Esporta unità")
   public static final String EXPORT_UNIT_URL_LABEL = "26";

   @RBEntry("Risultati dell'esportazione dell'unità")
   public static final String EXPORT_RESULTS_TITLE = "27";

   @RBEntry("Importa unità")
   public static final String IMPORT_UNIT_URL_LABEL = "28";

   @RBEntry("Risultati dell'importazione dell'unità")
   public static final String IMPORT_RESULTS_TITLE = "29";

   @RBEntry("Ricevi unità")
   public static final String RECEIVE_UNIT_URL_LABEL = "30";

   @RBEntry("Risultati della ricezione dell'unità")
   public static final String RECEIVE_RESULTS_TITLE = "31";

   @RBEntry("Parti unità")
   public static final String LISTPARTS_UNIT_URL_LABEL = "32";

   @RBEntry("Oggetti contenuti nell'unità")
   public static final String LIST_UNIT_PARTS_TITLE = "33";

   @RBEntry("Unità inviata.")
   public static final String SEND_UNIT_COMPLETE = "34";

   @RBEntry("L'invio dell'unità è fallito.")
   public static final String SEND_UNIT_FAILED = "35";

   @RBEntry("L'errore è:")
   public static final String UNIT_ERROR_IS = "36";

   @RBEntry("Stato esportazione")
   public static final String EXPORTSTATUS_UNIT_URL_LABEL = "37";

   @RBEntry("Report dello stato dell'esportazione")
   public static final String EXPORTSTATUS_UNIT_TITLE = "38";

   @RBEntry("Errore. Consultare l'amministratore di sistema.")
   public static final String INTERAL_ERROR = "39";

   @RBEntry("Esporta")
   public static final String EXPORT = "40";

   @RBEntry("Immediato")
   public static final String IMMEDIATE_LABEL = "41";

   @RBEntry("Momento programmato")
   public static final String ON_SCHEDULE_LABEL = "42";

   @RBEntry("Ora d'inizio")
   public static final String START_TIME_LABEL = "43";

   @RBEntry("Data d'inizio")
   public static final String START_DATE_LABEL = "44";

   @RBEntry("Invia")
   public static final String SUBMIT_LABEL = "45";

   @RBEntry("Esporta unità")
   public static final String EXPORT_UNIT_TITLE = "46";

   @RBEntry("Questa unità non è stata programmata per l'esportazione oppure l'esportazione programmata non si è ancora verificata oppure l'elemento programmato è stato eliminato.")
   public static final String EXPORT_NOT_SCHEDULED = "47";

   @RBEntry("Ora o data non validi.")
   public static final String INVALID_TIMESTAMP = "48";

   @RBEntry("L'unità è stata programmata per l'esportazione.")
   public static final String EXPORT_SCHEDULED = "49";

   @RBEntry("L'unità era già programmata per l'esportazione.")
   public static final String EXPORT_ALREADY_SCHEDULED = "50";

   @RBEntry("Sospendi")
   public static final String SUSPEND_LABEL = "51";

   @RBEntry("Riprendi")
   public static final String RESUME_LABEL = "52";

   @RBEntry("Modifica")
   public static final String EDIT_LABEL = "53";

   @RBEntry("Elimina")
   public static final String DELETE_LABEL = "54";

   @RBEntry("Il processo di programmazione è già sospeso per questa attività.")
   public static final String CANNOT_SUSPEND = "55";

   @RBEntry("Il processo di programmazione è già stato ripreso per questa attività.")
   public static final String CANNOT_RESUME = "56";

   @RBEntry("L'esportazione programmata di questa attività è completa.")
   public static final String EXPORT_COMPLETE = "57";

   @RBEntry("L'attività programmata non può essere modificata.")
   public static final String CANNOT_EDIT = "58";

   @RBEntry("L'attività programmata non può essere eliminata.")
   public static final String CANNOT_DELETE = "59";

   @RBEntry("Il processo di programmazione per questa attività è stato sospeso.")
   public static final String ITEM_SUSPENED = "60";

   @RBEntry("Il processo di programmazione per questa attività è stato ripreso.")
   public static final String ITEM_RESUMED = "61";

   @RBEntry("L'attività programmata è stata eliminata.")
   public static final String ITEM_DELETED = "62";

   @RBEntry("Invia unità")
   public static final String SEND_UNIT_TITLE = "63";

   @RBEntry("È stato programmato l'invio dell'unità.")
   public static final String SEND_SCHEDULED = "64";

   @RBEntry("Non è stato possibile programmare l'invio dell'unità.")
   public static final String SEND_NOT_SCHEDULED = "65";

   @RBEntry("Stato invio")
   public static final String SENDSTATUS_UNIT_URL_LABEL = "66";

   @RBEntry("Report dello stato di invio")
   public static final String SENDSTATUS_UNIT_TITLE = "67";

   @RBEntry("Operazione su oggetto origine non supportato")
   public static final String UNSUPPORTED_SEED_OPER = "68";

   @RBEntry("Operazione baseline unità non conforme")
   public static final String INCONSISTENT_UNIT_BASELINE_OPER = "69";

   @RBEntry("Lo stesso oggetto origine è stato collegato all'unità più di una volta.")
   public static final String DUPLICATED_SEED_OBJECT = "70";

   @RBEntry("Operazione ConfigSpec unità non supportata.")
   public static final String UNSUPPORTED_CONFIG_SPEC_OPER = "71";

   @RBEntry("ConfigSpec non coincide alla modalità impostata.")
   public static final String INCONSISTENT_CONFIG_SPEC = "72";

   @RBEntry("Baseline di replica esiste nel tipo opposto (importata/esportata)")
   public static final String UNIT_BASELINE_EXIST = "73";

   @RBEntry("Operazione baseline unità non supportata.")
   public static final String UNSUPPORTED_BASELINE_OPER = "74";

   @RBEntry("ReplicationProdStructClass = \"{0}\" non implementa l'interfaccia ReplicationProductStruct.")
   public static final String PROD_STRUC_CL_DOES_NOT_IMPL_INT = "75";

   @RBEntry("ReplicationProdStructClass = \"{0}\" non esiste.")
   public static final String PROD_STRUC_CL_DOES_NOT_EXIST = "76";

   @RBEntry("ReplicationProdStructClass = \"{0}\": l'oggetto della classe non può essere istanziato perché è un'interfaccia o è una classe astratta.")
   public static final String PROD_STRUC_CL_INST_EXC = "77";

   @RBEntry("ReplicationProdStructClass = \"{0}\" non è pubblico e risiede in un package diverso da wt.replication.unit.*")
   public static final String PROD_STRUC_CL_ILLEG_ACC_EXC = "78";

   @RBEntry("L'oggetto <{0}> è associato alla replica di sola lettura e non può essere modificato.")
   public static final String INVALID_READ_ONLY_REPL_OPER = "79";

   @RBEntry("Impossibile revisionare l'unità ricevuta o importata \"{0}\".")
   public static final String NO_REVISION_FOR_UNIT = "80";

   @RBEntry("Impossibile effettuare il Check-Out dell'unità esportata, ricevuta o importata \"{0}\".")
   public static final String NO_CHECKOUT_FOR_UNIT = "81";

   @RBEntry("Impossibile aggiornare l'unità esportata, ricevuta o importata \"{0}\".")
   public static final String NO_UPDATE_FOR_UNIT = "82";

   @RBEntry("Unità di replica")
   public static final String UNIT_DISPLAY_TYPE = "83";
}
