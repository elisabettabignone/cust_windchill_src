/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.replication.sender;

import wt.util.resource.*;

@RBUUID("wt.replication.sender.senderResource")
public final class senderResource extends WTListResourceBundle {
   @RBEntry("WTUnit objects not unique in database for unit number \"{0}\" -- we have {1} matches.")
   public static final String WTUNIT_NOT_UNIQUE = "0";

   @RBEntry("WTUnit object not found in database for WTUnit \"{0}\".")
   public static final String WTUNIT_NOT_EXIST = "1";

   @RBEntry("Timestamp can not be null if \"onSchedule\" is true.")
   public static final String TIMESTAMP_IS_NULL = "2";

   @RBEntry("Can not find manifest for WTUnit \"{0}\" with unit number \"{1}\".")
   public static final String MANIFEST_IS_NULL = "3";

   @RBEntry("The Site object not found for local site.")
   public static final String LOCAL_SITE_NOT_FOUND = "4";

   @RBEntry("The Site object does not exist for URL \"{0}\".")
   public static final String SITE_NOT_FOUND = "5";

   @RBEntry("The UnitSendRecord object for site \"{0}\" and unit number \"{1}\" is not unique.")
   public static final String UNIT_SEND_REC_NOT_UNIQUE = "6";

   @RBEntry("Can not Manifest object associated with WTUnit \"{0}\" with unit number \"{1}\".")
   public static final String CREATE_MANIFEST_FAILED = "7";

   @RBEntry("Upload for WTUnit \"{0}\" with unit number \"{1}\" to <{2}> failed.")
   public static final String UPLOAD_INIT_FAILED = "8";

   @RBEntry("{2} manifest files found for WTUnit \"{0}\" with unit number \"{1}\".")
   public static final String MANIFEST_NOT_UNIQUE = "9";

   @RBEntry("Failed to retrieve remote teams for site <{0}>.")
   public static final String RMT_TEAMTEMPLATE_ERROR = "10";

   @RBEntry("Unable to set the status for send operation to site \"{0}\".")
   public static final String SET_STATUS_ERROR = "11";

   @RBEntry("The UnitSendRecord object not found for site \"{0}\" and unit number \"{1}\".")
   public static final String UNIT_SEND_REC_NOT_FOUND = "12";

   @RBEntry("The UnitSendRecord object not found in database for UnitSendRecord \"{0}\".")
   public static final String UNIT_SEND_REC_NOT_EXIST = "13";

   @RBEntry("The External Media Based Transport folder <{0}> is invalid: either it contains files or it is not a folder.")
   public static final String EMB_FOLDER_EXIST = "14";

   @RBEntry("Target Site can not be null for transport type: \"{0}\".")
   public static final String NULL_TARGET_SITE = "15";

   @RBEntry("The Site <{0}> is not a Product Replication Peer site.")
   public static final String NOT_PEER_SITE = "16";

   @RBEntry("The Send Operation is Started on {0}.")
   public static final String SEND_STARTED = "17";

   @RBEntry("The Send Operation is Finished on {0}.")
   public static final String SEND_FINISHED = "18";

   @RBEntry(" ")
   @RBPseudo(false)
   public static final String UNIT_SENDER_RECORD = "19";
}
