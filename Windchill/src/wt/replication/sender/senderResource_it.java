/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.replication.sender;

import wt.util.resource.*;

@RBUUID("wt.replication.sender.senderResource")
public final class senderResource_it extends WTListResourceBundle {
   @RBEntry("Gli oggetti WTUnit non sono univoci nel database per il numero di unità \"{0}\" -- {1} corrispondenze.")
   public static final String WTUNIT_NOT_UNIQUE = "0";

   @RBEntry("Impossibile trovare l'oggetto WTUnit nel database per WTUnit \"{0}\".")
   public static final String WTUNIT_NOT_EXIST = "1";

   @RBEntry("La data non può essere nulla se \"onSchedule\" è true.")
   public static final String TIMESTAMP_IS_NULL = "2";

   @RBEntry("Impossibile trovare il file manifest per WTUnit \"{0}\" con il numero di unità \"{1}\".")
   public static final String MANIFEST_IS_NULL = "3";

   @RBEntry("L'oggetto sito non è stato trovato per il sito locale.")
   public static final String LOCAL_SITE_NOT_FOUND = "4";

   @RBEntry("L'oggetto sito non esiste per l'URL \"{0}\".")
   public static final String SITE_NOT_FOUND = "5";

   @RBEntry("L'oggetto UnitSendRecord per il sito \"{0}\" e il numero d'unità \"{1}\" non è univoco.")
   public static final String UNIT_SEND_REC_NOT_UNIQUE = "6";

   @RBEntry("Impossibile creare il manifest di WTUnit \"{0}\" con il numero d'unità \"{1}\".")
   public static final String CREATE_MANIFEST_FAILED = "7";

   @RBEntry("Caricamento di WTUnit \"{0}\" con il numero di unità \"{1}\" su <{2}> non riuscito.")
   public static final String UPLOAD_INIT_FAILED = "8";

   @RBEntry("{2} file manifest trovati per WTUnit \"{0}\" con numero d'unità \"{1}\".")
   public static final String MANIFEST_NOT_UNIQUE = "9";

   @RBEntry("Impossibile recuperare i team remoti per il sito <{0}>.")
   public static final String RMT_TEAMTEMPLATE_ERROR = "10";

   @RBEntry("Impossibile impostare lo stato dell'operazione di invio sul sito \"{0}\".")
   public static final String SET_STATUS_ERROR = "11";

   @RBEntry("L'oggetto UnitSendRecord non è stato ritrovato per il sito \"{0}\" e per il numero di unità \"{1}\".")
   public static final String UNIT_SEND_REC_NOT_FOUND = "12";

   @RBEntry("L'oggetto UnitSendRecord non è stato ritrovato nel database per UnitSendRecord \"{0}\".")
   public static final String UNIT_SEND_REC_NOT_EXIST = "13";

   @RBEntry("La cartella per il trasporto su supporti esterni <{0}> non è valida: o contiene file o non è una cartella.")
   public static final String EMB_FOLDER_EXIST = "14";

   @RBEntry("Il sito di destinazione non può essere nullo per il tipo di trasporto: \"{0}\".")
   public static final String NULL_TARGET_SITE = "15";

   @RBEntry("Il sito <{0}> non è un sito peer di replica di prodotto.")
   public static final String NOT_PEER_SITE = "16";

   @RBEntry("L'operazione di invio è iniziata: {0}.")
   public static final String SEND_STARTED = "17";

   @RBEntry("L'operazione di invio è terminata: {0}.")
   public static final String SEND_FINISHED = "18";

   @RBEntry(" ")
   @RBPseudo(false)
   public static final String UNIT_SENDER_RECORD = "19";
}
