/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.replication.imp;

import wt.util.resource.*;

@RBUUID("wt.replication.imp.impResource")
public final class impResource_it extends WTListResourceBundle {
   @RBEntry("Il valore per l'argomento \"{0}\" non può essere nullo")
   public static final String NULL_ARGUMENT = "0";

   @RBEntry("L'operazione di importazione è iniziata {0}.")
   public static final String IMPORT_STARTED = "1";

   @RBEntry("L'operazione di importazione è terminata {0}.")
   public static final String IMPORT_FINISHED = "2";

   @RBEntry(" ")
   @RBPseudo(false)
   public static final String UNIT_IMPORT_RECORD = "3";
}
