/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.replication.export;

import wt.util.resource.*;

@RBUUID("wt.replication.export.exportResource")
public final class exportResource_it extends WTListResourceBundle {
   @RBEntry("Il valore per l'argomento \"{0}\" non può essere nullo")
   public static final String NULL_ARGUMENT = "0";

   @RBEntry("L'oggetto non è WTPart, WTDocument o EPMDocument")
   public static final String NOT_APPROPRIATE_OBJECT = "1";

   @RBEntry("Impossibile costruire la struttura del prodotto. Verificare ConfigSpec.")
   public static final String INCOMPLETE_PRODUCT_STRUCTURE = "2";

   @RBEntry("WTPartConfigSpec non è impostato.")
   public static final String PART_CS_IS_NOT_SET = "3";

   @RBEntry("La baseline ConfigSpec è NULLA.")
   public static final String CONFIG_SPEC_BASELINE_IS_NULL = "4";

   @RBEntry("Non esiste un oggetto radice per \"{0}\" conforme a ConfigSpec.")
   public static final String NO_CONFIG_SPEC_ROOT_OBJECT = "5";

   @RBEntry("La family table non è stata ancora implementata.")
   public static final String FAM_TABLE_IS_NOT_IMPLEMENTED = "6";

   @RBEntry("EPMDocConfigSpec non è impostato.")
   public static final String EPM_DOC_CS_IS_NOT_SET = "7";

   @RBEntry("L'operazione di esportazione è iniziata {0}.")
   public static final String EXPORT_STARTED = "8";

   @RBEntry("L'operazione di esportazione è terminata {0}.")
   public static final String EXPORT_FINISHED = "9";

   @RBEntry("Impossibile creare il manifest di WTUnit \"{0}\" con il numero d'unità \"{1}\".")
   public static final String CREATE_MANIFEST_FAILED = "10";

   @RBEntry(" ")
   @RBPseudo(false)
   public static final String UNIT_EXPORT_RECORD = "11";
}
