/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.replication.export;

import wt.util.resource.*;

@RBUUID("wt.replication.export.exportResource")
public final class exportResource extends WTListResourceBundle {
   @RBEntry("The value for the argument \"{0}\" can not be null")
   public static final String NULL_ARGUMENT = "0";

   @RBEntry("The object is not WTPart, WTDocument or EPMDocument")
   public static final String NOT_APPROPRIATE_OBJECT = "1";

   @RBEntry("The product structure can't be built. Verify your ConfigSpec.")
   public static final String INCOMPLETE_PRODUCT_STRUCTURE = "2";

   @RBEntry("WTPartConfigSpec is not set.")
   public static final String PART_CS_IS_NOT_SET = "3";

   @RBEntry("ConfigSpec Baseline is NULL.")
   public static final String CONFIG_SPEC_BASELINE_IS_NULL = "4";

   @RBEntry("There is no root object for \"{0}\" ,complying with ConfigSpec.")
   public static final String NO_CONFIG_SPEC_ROOT_OBJECT = "5";

   @RBEntry("The family table is not implemented yet.")
   public static final String FAM_TABLE_IS_NOT_IMPLEMENTED = "6";

   @RBEntry("EPMDocConfigSpec is not set.")
   public static final String EPM_DOC_CS_IS_NOT_SET = "7";

   @RBEntry("The Export Operation is Started on {0}.")
   public static final String EXPORT_STARTED = "8";

   @RBEntry("The Export Operation is Finished on {0}.")
   public static final String EXPORT_FINISHED = "9";

   @RBEntry("Can not Manifest object associated with WTUnit \"{0}\" with unit number \"{1}\".")
   public static final String CREATE_MANIFEST_FAILED = "10";

   @RBEntry(" ")
   @RBPseudo(false)
   public static final String UNIT_EXPORT_RECORD = "11";
}
