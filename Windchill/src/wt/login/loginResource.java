package wt.login;

import wt.util.resource.*;

@RBUUID("wt.login.loginResource")
public final class loginResource extends WTListResourceBundle
{
  @RBEntry("Login to Windchill")
  public static final String LOGIN_TO_WINDCHILL = "0";

  @RBEntry("Enter credentials.")
  public static final String ENTER_CREDENTIALS = "1";

  @RBEntry("User name:")
  public static final String USER_NAME_LABEL = "2";

  @RBEntry("Password:")
  public static final String PASSWORD_LABEL = "3";

  @RBEntry("OK")
  public static final String OK = "4";

  @RBEntry("Login failed. Re-enter credentials.")
  public static final String RE_ENTER_CREDENTIALS = "5";
}
