/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.lifecycle;

import wt.util.resource.*;

@RBUUID("wt.lifecycle.lifecycleResource")
public final class lifecycleResource_it extends WTListResourceBundle {
   @RBEntry("L'oggetto non è stato inizializzato perché nullo.")
   public static final String NULL_OBJECT = "0";

   @RBEntry("Il valore trasferito per il parametro \"{0}\" era nullo.")
   @RBArgComment0("{0} refers to the parameters that is null.")
   public static final String NULL_PARAMETER = "1";

   @RBEntry("Impossibile promuovere. Una fase finale non ha fasi successive")
   public static final String NO_SUCCESSOR = "2";

   @RBEntry("Impossibile declassare. Una fase iniziale non ha fasi precedenti")
   public static final String NO_PREDECESSOR = "3";

   @RBEntry("Impossibile modificare lo stato del ciclo di vita. Oggetto non persistente")
   public static final String NOT_PERSISTENT = "4";

   @RBEntry("Errore interno: nessuno stato del ciclo di vita associato a PhaseTemplate.")
   public static final String NO_STATE = "5";

   @RBEntry("Errore interno: fase corrente non trovata. Cause probabili: il task in coda di assegnazione dell'oggetto a un ciclo di vita non è stato ancora completato o non è riuscito. Riprovare successivamente o chiedere all'amministratore di controllare il log della coda.")
   public static final String NO_PHASE = "6";

   @RBEntry("Errore interno: impossibile trovare la definizione del ciclo di vita predefinito")
   public static final String NO_DEFAULT = "7";

   @RBEntry("Il ciclo di vita assegnato è nullo")
   public static final String NULL_TEMPLATE = "8";

   @RBEntry("La firma è nulla")
   public static final String NULL_SIGNATURE = "9";

   @RBEntry("La firma deve avere un firmatario designato")
   public static final String NO_SIGNER = "10";

   @RBEntry("La firma deve avere un voto specificato")
   public static final String NO_VOTE = "11";

   @RBEntry("Il task in coda di assegnazione dell'oggetto a un ciclo di vita non è stato ancora completato o non è riuscito. Riprovare il task \"{0}\" successivamente o chiedere all'amministratore di controllare il log della coda.")
   @RBArgComment0("{0} refers to the name of the task")
   public static final String NOT_ASSIGNED_YET = "12";

   @RBEntry("\"{0}\" non inviato. Impossibile effettuare \"{1}\". ")
   @RBArgComment0("{0} refers to the an identification of an object ")
   @RBArgComment1("{1} refers to the name of the task that was attempted")
   public static final String NOT_SUBMITTED = "13";

   @RBEntry("Non si è \"{0}\" per l'oggetto {1}.")
   @RBArgComment0("{0} refers to the name of role")
   @RBArgComment1("{1} refers to the name of object")
   public static final String NOT_ROLEPLAYER = "14";

   @RBEntry("Le seguenti operazioni non sono consentite su un oggetto sottoposto a Check-Out: \"{0}\"")
   @RBArgComment0("{0} refers to the name of the task")
   public static final String CHECKED_OUT_OBJECT = "15";

   @RBEntry("Impossibile inviare una copia in modifica. È necessario effettuare il Check-In.")
   public static final String WORKING_COPY_SUBMIT = "16";

   @RBEntry("Condizione non valida per il contenitore delle transazioni")
   public static final String INVALID_ASSERTION = "17";

   @RBEntry("Oggetto non valido rilevato in BatchContainer")
   public static final String INVALID_BC_OBJECT = "18";

   @RBEntry("Oggetto non valido rilevato in RoleBatchContainer")
   public static final String INVALID_RBC_OBJECT = "19";

   @RBEntry("Cartella di sistema non trovata. Impossibile memorizzare il ciclo di vita. ")
   public static final String NO_SYSTEM_FOLDER = "20";

   @RBEntry("Condizione di link non valida")
   public static final String INVALID_LINK_ASSERTION = "21";

   @RBEntry("L'elenco notifiche può essere correlato solo a un PhaseTemplate")
   public static final String TOO_MANY_DN = "22";

   @RBEntry("L'elenco notifiche deve essere correlato a un PhaseTemplate")
   public static final String ZERO_DN = "23";

   @RBEntry("La lista di controllo di accesso può essere correlata solo a un PhaseTemplate.")
   public static final String TOO_MANY_AHAL = "24";

   @RBEntry("La lista di controllo di accesso deve essere correlata a un PhaseTemplate.")
   public static final String ZERO_AHAL = "25";

   @RBEntry("L'elenco criteri può essere correlato solo a un PhaseTemplate.")
   public static final String TOO_MANY_DC = "26";

   @RBEntry("Il criterio deve essere correlato a un PhaseTemplate")
   public static final String ZERO_DC = "27";

   @RBEntry("PhaseTemplate può essere correlato solo a un LifeCycleTemplate")
   public static final String TOO_MANY_PL = "28";

   @RBEntry("PhaseTemplate deve essere correlato a un LifeCycleTemplate")
   public static final String ZERO_PL = "29";

   @RBEntry("PhaseTemplate può essere correlato solo a un PhaseTemplate predecessore")
   public static final String TOO_MANY_PREDECESSOR = "30";

   @RBEntry("PhaseTemplate deve essere correlato a un PhaseTemplate predecessore")
   public static final String ZERO_PREDECESSOR = "31";

   @RBEntry("PhaseTemplate può essere correlato solo a un PhaseTemplate successore")
   public static final String TOO_MANY_SUCCESSOR = "32";

   @RBEntry("PhaseTemplate deve essere correlato a un PhaseTemplate successore")
   public static final String ZERO_SUCCESSOR = "33";

   @RBEntry("Elimina condizione non valida: il ciclo di vita è in uso.")
   public static final String LCT_IN_USE = "34";

   @RBEntry("Ruolo di autore invio obbligatorio.")
   public static final String SBMTR_ROLE_REQD = "35";

   @RBEntry("Ruolo di promotore obbligatorio.")
   public static final String PRMTR_ROLE_REQD = "36";

   @RBEntry("Cartella di sistema non trovata. Impossibile memorizzare il progetto")
   public static final String NO_SYS_FOLDER = "37";

   @RBEntry("Impossibile eliminare un ciclo di vita")
   public static final String CANNOT_DELETE_LCT = "38";

   @RBEntry("** Errore **")
   public static final String ERROR_ALERT = "39";

   @RBEntry("Errore durante la lettura delle proprietà wt.workflow.*")
   public static final String PROPERTIES_ERROR = "40";

   @RBEntry("Sì")
   public static final String VOTE_YES_INDICATOR = "41";

   @RBEntry("No")
   public static final String VOTE_NO_INDICATOR = "42";

   @RBEntry("Sì")
   public static final String SATISFIED_INDICATOR = "43";

   @RBEntry("No")
   public static final String NOT_SATISFIED_INDICATOR = "44";

   @RBEntry("Non è stata assegnata alcuna cartella in cui memorizzare il progetto. Progetto non salvato.")
   public static final String NO_PROJECT_FOLDER = "45";

   @RBEntry("Dominio amministrativo \"{0}\" contiene più progetti con nome \"{1}\". Impossibile impostare il progetto per l'oggetto.")
   @RBArgComment0("{0} refers to the name of the administrative domain")
   @RBArgComment1("{1} refers to the name of an administrative domain")
   public static final String MULTIPLE_PROJECTS = "46";

   @RBEntry("Il progetto \"{0}\" non esiste nel dominio amministrativo \"{1}\". Impossibile impostare il progetto per l'oggetto.")
   @RBArgComment0("{0} refers to a project name")
   @RBArgComment1("{1} refers to the name of an administrative domain")
   public static final String NONEXISTENT_PROJECT = "47";

   @RBEntry("Il progetto è impostato per l'oggetto LifeCycleManaged ma non fa riferimento ad un progetto effettivo.")
   public static final String PROJECT_REF_NOT_SET = "48";

   @RBEntry("Impossibile risolvere un ruolo obbligatorio e inviare una notifica all'autore invio, al proprietario dell'oggetto o all'utente corrente.")
   public static final String NULL_CURRENT_USER = "49";

   @RBEntry("Impossibile eliminare i progetti dal sistema.")
   public static final String PROHIBIT_DELETE_PROJECT = "50";

   @RBEntry("Impossibile eliminare i cicli di vita dal sistema.")
   public static final String PROHIBIT_DELETE_LCT = "51";

   @RBEntry("Impossibile modificare l'identificativo di un progetto.")
   public static final String PROHIBIT_IDCHANGE_PROJECT = "52";

   @RBEntry("Impossibile modificare l'identificativo di un ciclo di vita.")
   public static final String PROHIBIT_IDCHANGE_LCT = "53";

   @RBEntry("L'oggetto è in attesa di promozione. Durante questa fase, l'invio non è consentito.")
   public static final String SUBMIT_FROM_GATE = "54";

   @RBEntry("L'oggetto è nella fase finale. Promozione dalla fase finale non consentita.")
   public static final String PROMOTE_FROM_FINAL_PHASE = "55";

   @RBEntry("L'oggetto è nella fase iniziale. Declassamento dalla fase iniziale non consentito.")
   public static final String DEMOTE_FROM_INITIAL_PHASE = "56";

   @RBEntry("Impossibile trovare l'amministratore")
   public static final String MISSING_ADMINISTRATOR = "57";

   @RBEntry("Parametri applet non validi: \"{0}\".  ")
   @RBArgComment0("{0} refers to the parameters that were passsed to the applet.")
   public static final String BAD_PARAMS = "58";

   @RBEntry("Stati del ciclo di vita non univoci nella lista: {0}. Gli stati devono essere univoci. ")
   @RBComment("This error message indicates that a list of states is being used in a situation where the elements (states) in that list must be unique, but are not (i.e., there is at least one duplicate state in the list).")
   @RBArgComment0("{0} refers to a list of life cycle states.")
   public static final String INIT_DUPLICATE_STATE = "184";

   @RBEntry("La transizione \"{0}\" non può mappare lo stato del ciclo di vita \"{1}\" a più di uno stato {2}. Controllare il valore della seguente proprietà di sistema: \"wt.lifecycle.transitions.Default.{0}.\"")
   @RBComment("This error message indicates that an attempt is being made to initialize in a non-deterministic fashion a transition that is configured to be deterministic (i.e., the transition can be used to map one state to only one other).")
   @RBArgComment0("{0} refers to a life cycle transition.")
   @RBArgComment1("{1} refers to the source life cycle state.")
   @RBArgComment2("{2} refers to a set of destination life cycle states that has more than one element.")
   public static final String INIT_DETERMINISTIC_TRANSITION = "185";

   @RBEntry("Errore di configurazione del sistema. Le transizioni di tipo \"{0}\" sono riservate per l'uso interno da parte del software e non possono essere utilizzate esplicitamente nella proprietà {1}.")
   @RBComment("This error message indicates that an attempt has been made to use an internal (non-selectable) transition.  These are reserved for internal use by the software and may not be specified for other purposes.")
   @RBArgComment0("{0} refers to the transition type being used illegally")
   @RBArgComment1("{1} is the entire property value containing the syntax error")
   public static final String INIT_ILLEGAL_TRANSITION = "186";

   @RBEntry("Errore di configurazione del sistema. Rilevato errore di sintassi fra i caratteri {0} e {1} della stringa di proprietà {2}.")
   @RBComment("This error message indicates that property for default transition initialization contains a syntax error.")
   @RBArgComment0("{0} refers to the column in which the start of the syntax error was detected")
   @RBArgComment1("{1} refers to the column in which the end of the syntax error was detected")
   @RBArgComment2("{2} is the entire property value containing the syntax error")
   public static final String INIT_BAD_TOKEN = "187";

   @RBEntry("La transizione \"{0}\" per il modello di ciclo di vita \"{1}\" non può mappare lo stato del ciclo di vita \"{2}\" a più di uno stato: {3}.")
   @RBComment("This error message indicates that an attempt is being made to initialize in a non-deterministic fashion a transition that is configured to be deterministic (i.e., the transition can be used to map one state to only one other).")
   @RBArgComment0("{0} refers to a life cycle transition.")
   @RBArgComment1("{1} refers to the source life cycle template.")
   @RBArgComment2("{2} refers to the source life cycle state.")
   @RBArgComment3("{3} refers to a set of destination life cycle states that has more than one element.")
   public static final String INVALID_MULTIPLE_TRANSITIONS = "195";

   @RBEntry("La transizione \"{0}\" è richiesta per il modello di ciclo di vita \"{1}\" dello stato \"{2}\".")
   @RBComment("This error message indicates that there is no transition defined from a given state to another and one is required.")
   @RBArgComment0("{0} refers to a life cycle transition.")
   @RBArgComment1("{1} refers to the source life cycle template.")
   @RBArgComment2("{2} refers to the source life cycle state.")
   public static final String NO_DEFINED_TRANSITIONS = "196";

   @RBEntry("Promuovi ")
   public static final String PROMOTE = "87";

   @RBEntry("Riesame")
   public static final String REVIEW = "88";

   @RBEntry("Osserva ")
   public static final String OBSERVE = "89";

   @RBEntry("Invia ")
   public static final String SUBMIT = "90";

   @RBEntry("Approva")
   public static final String APPROVE_VOTE_LABEL = "144";

   @RBEntry("Non approvare")
   public static final String DO_NOT_APPROVE_VOTE_LABEL = "145";

   @RBEntry("Promuovi")
   public static final String PROMOTE_VOTE_LABEL = "146";

   @RBEntry("Rifiuta")
   public static final String DENY_VOTE_LABEL = "147";

   @RBEntry("Declassa")
   public static final String DEMOTE_VOTE_LABEL = "148";

   @RBEntry("Abbandona")
   public static final String DROP_VOTE_LABEL = "149";

   @RBEntry(":")
   public static final String COLON = "150";

   /**
    * Message Text for wt.lifecycle.LoadLifeCycle
    **/
   @RBEntry("LoadLifeCycle: il campo \"{0}\" non è valido per questo tipo di registrazione. Registrazione interrotta.")
   @RBArgComment0("{0}- an invalid field")
   public static final String LOAD_INVALID_FIELD = "59";

   @RBEntry("Avviso di LoadLifeCycle: impossibile trovare nel file di input un valore per il campo \"{0}\". È in corso il tentativo di rendere comunque persistente l'elemento...  ")
   @RBArgComment0("{0}- a field which did not have a value")
   public static final String LOAD_NO_VALUE = "60";

   @RBEntry("LoadLifeCycle: per costruire l'oggetto corrente è obbligatorio specificare un valore per \"{0}\". Registrazione interrotta.")
   @RBArgComment0("{0}- a field which requires a value")
   public static final String LOAD_NULL_ILLEGAL = "61";

   @RBEntry("LoadLifeCycle: impossibile trovare il ruolo \"{0}\" nell'insieme di risorse di ruolo (RoleRB.class). Il ruolo deve esistere nell'insieme di risorse. Registrazione interrotta.")
   @RBArgComment0("{0}- a Role name")
   public static final String LOAD_NO_ROLE_BAD = "62";

   @RBEntry("LoadLifeCycle: impossibile trovare il ruolo \"{0}\" nell'insieme di risorse di ruolo (RoleRB.class). Ruolo nullo ok. Continua...")
   @RBArgComment0("{0}- a Role name")
   public static final String LOAD_NO_ROLE_OK = "63";

   @RBEntry("LoadLifeCycle: impossibile trovare nel file di input il valore booleano per \"{0}\". Tale valore viene impostato in modo predefinito a \"{1}\".")
   @RBArgComment0("{0}- a field name for a boolean field")
   public static final String LOAD_NO_BOOLEAN_OK = "64";

   @RBEntry("LoadLifeCycle: impossibile trovare nel file di input il valore booleano per \"{0}\". Definire questo valore nel file di input.")
   @RBArgComment0("{0}- a field name for a boolean field")
   public static final String LOAD_NO_BOOLEAN_BAD = "65";

   @RBEntry("LoadLifeCycle: lo schedario {0} è inesistente. Impossibile creare LifeCycleTemplate.")
   public static final String LOAD_NO_SYSTEM_CABINET = "66";

   @RBEntry("LoadLifeCycle: nessuna posizione di cartella specificata. Salvataggio in corso di LifeCycleTemplate in \"{0}\".")
   @RBArgComment0("{0}- the default folder location for life cycle")
   public static final String LOAD_NULL_LOCATION = "67";

   @RBEntry("LoadLifeCycle: i LifeCycleTemplate devono essere memorizzati nello schedario \"{0}\". Memorizzazione in corso nella cartella predefinita \"{1}\".")
   @RBArgComment0("{0}- name of the cabinet in which LifeCycleTemplates must be stored")
   @RBArgComment1("{1}- name of default folder")
   public static final String LOAD_NOT_SYSTEM_CABINET = "68";

   @RBEntry("LoadLifeCycle: la posizione della cartella \"{0}\" è inesistente. Impossibile salvare LifeCycleTemplate.")
   @RBArgComment0("{0}- folder location which does not exist")
   public static final String LOAD_INVALID_FOLDER = "70";

   @RBEntry("LoadLifeCycle: rilevato errore in {0}. Verificare log del method server.")
   @RBArgComment0("{0}- name of Java method in which error occured")
   public static final String LOAD_EXCEPTION = "71";

   @RBEntry("LoadLifeCycle: nessun PhaseTemplate corrente nella memoria cache. Registrazione interrotta.")
   public static final String LOAD_NO_CURRENT_PT = "72";

   @RBEntry("LoadLifeCycle: assicurarsi che la registrazione RoleHolder sia preceduta da una registrazione PhaseTemplateBegin.")
   public static final String LOAD_INSURE_PT_RH = "73";

   @RBEntry("LoadLifeCycle: assicurarsi che la registrazione LifeCycleRoleHolder sia preceduta da una registrazione PhaseTemplateBegin.")
   public static final String LOAD_INSURE_PT_LCRH = "74";

   @RBEntry("LoadLifeCycle: assicurarsi che la registrazione LifeCycleRoleContext sia preceduta da una registrazione PhaseTemplateBegin.")
   public static final String LOAD_INSURE_PT_LCRC = "75";

   @RBEntry("LoadLifeCycle: l'utente/gruppo/ruolo \"{0}\" non è valido. Specificare un utente, un gruppo o un ruolo valido. Registrazione RoleHolder non elaborata.")
   @RBArgComment0("{0}- name of a user or a group")
   public static final String LOAD_INVALID_PRINCIPAL = "76";

   @RBEntry("LoadLifeCycle: impossibile trovare lo stato del ciclo di vita \"{0}\" nell'insieme di risorse di stato (StateRB.class). Lo stato deve esistere nell'insieme di risorse. Registrazione PhaseTemplate non elaborata.")
   @RBArgComment0("(0}- name of a life cycle state which is not in the resource bundle")
   public static final String LOAD_INVALID_STATE = "77";

   @RBEntry("LoadLifeCycle: un createPhaseTemplateEnd deve precedere il record, poiché questa non è la prima fase. PhaseTemplate interrotto.")
   public static final String LOAD_MISSING_PTEND = "78";

   @RBEntry("LoadLifeCycle: nessun valore nella cache per l'oggetto LifeCycleTemplate. Impossibile rendere persistente PhaseTemplate.")
   public static final String LOAD_NO_CACHE_LCT = "79";

   @RBEntry("LoadLifeCycle: la posizione della cartella \"{0}\" è inesistente. Salvataggio in corso del progetto in \"{1}\"")
   @RBArgComment0("{0}- a folder location that does not exist")
   @RBArgComment1("{1}- default folder location for Projects")
   public static final String LOAD_INVALID_FOLDER_DEFAULT = "80";

   @RBEntry("LoadLifeCycle: la posizione della cartella \"{0}\" è inesistente. Impossibile rendere persistente il progetto.")
   @RBArgComment0(" {0}- a folder location that does not exist")
   public static final String LOAD_INVALID_FOLDER_FINAL = "81";

   @RBEntry("LoadLifeCycle: assicurarsi che la registrazione PhaseTemplateEnd sia preceduta da una registrazione PhaseTemplateBegin.")
   public static final String LOAD_INSURE_PT_PTEND = "82";

   @RBEntry("LoadLifeCycle: nessun valore memorizzato nella cache per il progetto corrente. Assicurarsi che esista una record ProjectBegin prima del record ProjectEnd.")
   public static final String LOAD_NO_CACHE_PROJECT = "83";

   @RBEntry("LoadLifeCycle: nessun AdHocNotificationList corrente nella memoria cache. Registrazione interrotta. Assicurarsi che la registrazione sia preceduta da una registrazione createAHNLBegin.")
   public static final String LOAD_NO_CACHE_AHNL = "84";

   @RBEntry("LoadLifeCycle: l'utente/gruppo/ruolo \"{0}\" non è valido. Specificare un utente, un gruppo o un ruolo valido. Registrazione NotificationSetPrincipal non elaborata.")
   @RBArgComment0("{0}- name of a user or a group")
   public static final String LOAD_INVALID_PRINCIPAL_NOTIFY = "85";

   @RBEntry("Impossibile recuperare il ruolo \"{0}\" per questo task. ")
   @RBArgComment0("{0} refers to the role name who is to perform a task with the applet.")
   public static final String ROLE_NOT_FOUND = "91";

   @RBEntry("Task incompleto ")
   public static final String TASK_FAILED_TITLE = "92";

   @RBEntry("Impossibile completare il task. \"{0}\" non disponibile. ")
   @RBArgComment0("{0} refers to the object identifier of the task's target object")
   public static final String TASK_OBJECT_MISSING = "93";

   @RBEntry("In sospeso")
   public static final String VOTE_PENDING_INDICATOR = "94";

   @RBEntry("Impossibile costruire la tabella contenente i riesami. ")
   public static final String REVIEW_TABLE_FAILED = "95";

   @RBEntry("Riesami")
   public static final String REVIEW_COLUMN_LABEL = "96";

   @RBEntry("Ruolo")
   public static final String ROLE_COLUMN_LABEL = "97";

   @RBEntry("Approva")
   public static final String APPROVE_COLUMN_LABEL = "98";

   @RBEntry("Commenti")
   public static final String COMMENTS_COLUMN_LABEL = "99";

   @RBEntry("completato")
   public static final String WAS_SUCCESSFUL = "100";

   @RBEntry("non riuscito")
   public static final String FAILED = "101";

   @RBEntry("Task obsoleto. Il task fa riferimento all'oggetto \"{0}\" nello stato del ciclo di vita \"{1}\", ma l'oggetto è stato passato allo stato \"{2}\". Cancellare il task dalla posta in arrivo.")
   @RBArgComment0("{0}- identification attribute of the task's target object")
   @RBArgComment1("{1}- The lifecycle state of the object that the task expected")
   @RBArgComment2("{2}- The actual lifecycle state of the task's target object")
   public static final String OBSOLETE_TASK = "102";

   @RBEntry("Impossibile memorizzare un progetto in uno schedario personale. Selezionare un'altra posizione.")
   public static final String PROJECT_IN_USER_LOCATION = "103";

   @RBEntry("Impossibile inviare un oggetto memorizzato in uno schedario personale. Spostare l'oggetto in una posizione condivisa prima di inviare. ")
   public static final String PERSONAL_CABINET_SUBMIT = "104";

   @RBEntry("Parametri applet non validi. \"{0}\" ")
   public static final String INVALID_PARAMETERS = "105";

   @RBEntry("Oggetto sottoposto a processo di riesame per l'eventuale promozione alla fase successiva. Non è consentita alcuna modifica.  ")
   public static final String PROHIBIT_MODIFY_AT_GATE = "106";

   @RBEntry("\"{0}\" non consentito. L'oggetto non è gestito dal ciclo di vita.  ")
   @RBArgComment0("{0}- Action (Submit, Promote, Demote, Drop, etc) that is not allowed")
   public static final String OBJECT_NOT_LCMD = "107";

   @RBEntry("\"{0}\" non consentito. L'oggetto non rappresenta l'ultima iterazione. Aggiornare i dati e riprovare.  ")
   @RBArgComment0("{0}- Action (Submit, Promote, Demote, Drop, etc) that is not allowed")
   public static final String OBJECT_NOT_LATEST_ITERATION = "108";

   @RBEntry("Impossibile completare gli attributi per il ciclo di vita nell'oggetto.")
   public static final String INFLATE_FAILED = "109";

   @RBEntry("Completato automaticamente.")
   public static final String ROBOT_SIGNATURE_TEXT = "110";

   @RBEntry("Impossibile impostare i criteri di ricerca a LifeCycle = {0}.")
   @RBArgComment0("{0} = the lifecycletemplate object.")
   public static final String SEARCH_CRITERIA = "111";

   @RBEntry("LifeCycle \"{0}\" non valido.")
   @RBArgComment0("{0} is the LifeCycleTemplate name.")
   public static final String LCTEMPLATE_INVALID = "112";

   @RBEntry("Nessuna autorizzazione per {0}. È necessario modificare i diritti sull'oggetto per completare il task.")
   @RBArgComment0("{0} action the user is not authorized to complete")
   public static final String NOT_AUTHORIZED_TO_MODIFY = "113";

   @RBEntry("Nessuna autorizzazione per aggiornare i partecipanti al ruolo {0}.")
   @RBArgComment0("{0} the role that the current user is not allowed to augment")
   public static final String NOT_AUTHORIZED_TO_AUGMENT_ROLE = "114";

   @RBEntry("Cronologia del ciclo di vita")
   public static final String LC_HISTORY = "115";

   @RBEntry("L'oggetto \"{0}\" non è gestito da ciclo di vita. Il task richiede che l'oggetto aziendale principale sia gestito da ciclo di vita.")
   @RBArgComment0("{0} is the class name of the object which is the target of one of the lifecycle tasks (Submit, Review, Promote, and Observe).")
   public static final String INVALID_TASK_OBJECT = "116";

   @RBEntry("Invia")
   public static final String SUBMIT_URL_LABEL = "117";

   @RBEntry("Impossibile inviare l'oggetto {0} in questo momento. Per completare questa azione, inviare un task per questo oggetto. Il task sarà ricevuto quando il task Invia sarà attivo nel workflow appropriato.  ")
   @RBArgComment0("{0}  is the identity of the object")
   public static final String OBJECT_NOT_SUBMITABLE = "118";

   @RBEntry("Impossibile aggiornare i partecipanti al ruolo {0}. Ruolo inesistente.  ")
   @RBArgComment0("{0}  the name of the role that is attempting to be augmented")
   public static final String AUGMENT_NON_EXISTENT_ROLE = "119";

   @RBEntry("Tentativo di impostare lo stato del ciclo di vita dell'oggetto {0} non riuscito. Lo stato {1} non è valido nel ciclo di vita {2}.")
   @RBArgComment0("{0} the identity of the Life Cycle Managed object")
   @RBArgComment1("{1} the state the user attempted to change it to")
   @RBArgComment2("{2} the Life Cycle associated with the Life Cycle Managed object")
   public static final String INVALID_STATE = "120";

   @RBEntry("Il ciclo di vita deve essere memorizzato in uno schedario personale o nello schedario System.")
   public static final String LCT_CABINET_INVALID = "121";

   @RBEntry("Il ciclo di vita selezionato non è l'ultima iterazione. Aggiornare e riprovare.  ")
   public static final String SELECTED_LCT_NOT_LATEST = "122";

   @RBEntry("Il ciclo di vita selezionato non è attivato. Sceglierne uno attivato. ")
   public static final String SELECTED_LCT_NOT_ENABLED = "123";

   @RBEntry("La copia in modifica del ciclo di vita {0} è usata in {1} posizioni. Rimuovere i seguenti componenti prima di completare il Check-In: {2}")
   @RBArgComment0("{0} the name of the Life Cycle")
   @RBArgComment1("{1} the number of places where the Life Cycle is in use.")
   @RBArgComment2("{2} the identities of the objects using the Life Cycle")
   public static final String CHECKIN_LCT_IS_IN_USE = "124";

   @RBEntry("Questa iterazione del ciclo di vita {0} è usata in {1} posizioni. Rimuovere i seguenti componenti prima di completare l'eliminazione: {2}")
   @RBArgComment0("{0} the name of the Life Cycle")
   @RBArgComment1("{1} the number of places where the Life Cycle is in use.")
   @RBArgComment2("{2} the identities of the objects using the Life Cycle")
   public static final String DELETE_LCT_IS_IN_USE = "125";

   @RBEntry("I cicli di vita master non possono essere eliminati dal sistema.")
   public static final String PROHIBIT_DELETE_LCTM = "126";

   @RBEntry("Impossibile impostare il ciclo di vita. L'oggetto {0} è già persistente. Utilizzare l'opzione Assegna a un altro ciclo di vita.  ")
   @RBArgComment0("{0}  the LifeCycle Managed object")
   public static final String CANT_SET_LC_WHEN_PERSISTENT = "127";

   @RBEntry("Impossibile impostare lo stato del ciclo di vita. L'oggetto {0} è già persistente. ")
   @RBArgComment0("{0}  the LifeCycle Managed object")
   public static final String CANT_SET_STATE_WHEN_PERSISTENT = "128";

   @RBEntry("Creazione dell'oggetto {0} nello stato {1} del ciclo di vita {2} non riuscita. {1} non rappresenta uno stato valido nel ciclo di vita {2}.  ")
   @RBArgComment0("{0}  the identity of the LifeCycle Managed object")
   @RBArgComment1("{1}  the state the object is being created into")
   @RBArgComment2("{2}  the Life Cycle the object is set to")
   public static final String INVALID_STATE_ON_CREATE = "129";

   @RBEntry("Impossibile costruire questo task. Il business object principale associato al task è stato eliminato.")
   public static final String PRIMARY_BUSINESS_OBJECT_DELETED = "130";

   @RBEntry("Questa iterazione del ciclo di vita {0} è usata in {1} posizioni. Rimuovere i seguenti componenti prima di aggiornare il ciclo di vita: {2}")
   @RBArgComment0("{0} the name of the Life Cycle")
   @RBArgComment1("{1} the number of places where the Life Cycle is in use.")
   @RBArgComment2("{2} the identities of the objects using the Life Cycle")
   public static final String PROHIBIT_MODIFY_LCT_IN_USE = "131";

   @RBEntry("Non si dispone dell'autorizzazione ad impostare lo stato del ciclo di vita dell'oggetto {0}: per completare l'azione sono necessari i permessi di amministrazione per l'oggetto.")
   @RBArgComment0("{0} the identity of the Life Cycle Managed object")
   public static final String NOT_AUTHORIZED_TO_SET_STATE = "132";

   @RBEntry("Copia di ")
   public static final String COPY_OF = "133";

   @RBEntry("Avvertenza: impossibile trovare il modello di workflow \"{0}\". Il processo per rendere persistente la fase del ciclo di vita senza questo attributo di fase è in corso.")
   public static final String LOAD_INVALID_PHASE_WORKFLOW = "134";

   @RBEntry("Avvertenza: impossibile trovare il modello di workflow \"{0}\". Il processo per rendere persistente la fase del ciclo di vita senza questo attributo del gate è in corso.")
   public static final String LOAD_INVALID_GATE_WORKFLOW = "135";

   @RBEntry("Impossibile trovare o creare \"{0}\".  ")
   @RBArgComment0("{0} the name of the file or directory that we are trying to create")
   public static final String UNABLE_TO_CREATE_DIR = "136";

   @RBEntry(", ")
   public static final String IDENTITY_STRING_DELIMITER = "137";

   @RBEntry("{0} è impiegato dal ciclo di vita in {1} posizioni. Rimuovere i seguenti componenti prima di completare l'eliminazione: {2}")
   @RBArgComment0("{0} the identity of the Workflow")
   @RBArgComment1("{1} the number of places where the workflow is in use.")
   @RBArgComment2("{2} the identities of the objects using the Workflow")
   public static final String DELETE_WF_IS_IN_USE = "138";

   @RBEntry("Perché i task del ciclo di vita (invia, esamina, promuovi, osserva) siano validi, il loro processo di workflow deve essere associato ad un business object principale. Completare l'attività che ha generato questo task con la Gestione processi.")
   public static final String PBO_REQUIRED_FOR_LC_TASKS = "139";

   @RBEntry("Avvertenza: la classe supportata {0} non è stata trovata nel sistema attuale. Tentativo di creazione del ciclo di vita senza la specificazione della classe supportata in corso...")
   @RBArgComment0("{0} the name of the class that is not found in the current system")
   public static final String LOAD_INVALID_SUPPORTED_CLASS = "140";

   @RBEntry("Imposta stato del ciclo di vita")
   public static final String SET_STATE = "141";

   @RBEntry("L'impostazione dello stato del ciclo di vita non è consentita su una copia in modifica.")
   public static final String WORKING_COPY_SET_STATE = "142";

   @RBEntry("Lo stato del ciclo di vita di un oggetto memorizzato in uno schedario personale non può essere modificato. Spostare l'oggetto in una posizione condivisa prima di tentare di impostare lo stato. ")
   public static final String PERSONAL_CABINET_SET_STATE = "143";

   @RBEntry("Errore interno: impossibile trovare la fase corrente.  ")
   public static final String NO_PHASE_FOUND = "151";

   @RBEntry("Completato")
   public static final String COMPLETED_LABEL = "152";

   @RBEntry("Cronologia")
   public static final String HISTORY_LABEL = "153";

   @RBEntry(" - ")
   @RBComment(" do not translate!")
   public static final String STATES_SEPARATOR = "154";

   @RBEntry("<B>")
   @RBPseudo(false)
   @RBComment(" do not translate!")
   public static final String CURRENT_STATE_BEGIN = "155";

   @RBEntry("</B>")
   @RBPseudo(false)
   @RBComment(" do not translate!")
   public static final String CURRENT_STATE_END = "156";

   @RBEntry("<B>")
   @RBPseudo(false)
   @RBComment(" do not translate!")
   public static final String LABEL_BEGIN = "157";

   @RBEntry(": </B>")
   @RBPseudo(false)
   @RBComment(" do not translate!")
   public static final String LABEL_END = "158";

   @RBEntry(" (")
   @RBComment(" do not translate!")
   public static final String STATE_LIST_BEGIN = "159";

   @RBEntry(") ")
   @RBComment(" do not translate!")
   public static final String STATE_LIST_END = "160";

   @RBEntry(" [")
   @RBComment(" do not translate!")
   public static final String DROPPED_STATE_BEGIN = "161";

   @RBEntry("] ")
   @RBComment(" do not translate!")
   public static final String DROPPED_STATE_END = "162";

   @RBEntry(" (")
   @RBComment(" do not translate!")
   public static final String HISTORY_NOTATION_BEGIN = "163";

   @RBEntry(") ")
   @RBComment(" do not translate!")
   public static final String HISTORY_NOTATION_END = "164";

   @RBEntry("Sì")
   public static final String IS_AT_GATE = "165";

   @RBEntry("No")
   public static final String IS_NOT_AT_GATE = "166";

   @RBEntry("Fasi:")
   public static final String PHASES_LABEL = "167";

   @RBEntry("Avvertenza: è stata creata una nuova iterazione per il ciclo di vita {0}. ")
   @RBArgComment0("{0} the name of the life cycle")
   public static final String NEW_ITERATION_CREATED = "168";

   @RBEntry("Il ciclo di vita {0} è stato ignorato perché fa riferimento a una classe supportata non valida.")
   @RBArgComment0("{0} the name of the life cycle ")
   public static final String LCT_REFERENCES_INVALID_CLASS = "169";

   @RBEntry("Non si dispone di autorizzazione alla modifica di {0}")
   @RBArgComment0("{0} - is the Wf Process template object that the user is trying to modify ")
   public static final String MODIFY_NOT_ALLOWED = "170";

   @RBEntry("Non si dispone dell'autorizzazione a creare un ciclo di vita.")
   public static final String CREATE_LCT_NOT_ALLOWED = "171";

   @RBEntry("LoadLifeCycle: nessun AdHocAclSpec è caricato al momento nella cache.")
   public static final String LOAD_NO_CACLE_AHAS = "172";

   @RBEntry("LoadLifeCycle: nessun valore nella cache per l'oggetto ProjectTemplate. Impossibile rendere persistente ProjectTemplate.")
   public static final String LOAD_NO_CACHE_TEAMTEMPLATE = "173";

   @RBEntry("Avvertenza: l'importazione del ciclo di vita {0} non è riuscito. Impossibile importare un'iterazione nuova di un modello di ciclo di vita sottoposto a Check-Out.")
   public static final String LIFE_CYCLE_CHECKED_OUT = "174";

   @RBEntry("LoadLifeCycle: Non sono ammessi più stati con il nome \"{0}\" nello stesso ciclo di vita.")
   @RBArgComment0("{0}- A LifeCycleState string.")
   public static final String LOAD_INVALID_MULTI_STATES = "175";

   @RBEntry("{0} è in fase di revisione e possibile promozione alla fase successiva, pertanto non è possibile apportarvi modifiche. ")
   @RBArgComment0(" {0} The identity of the object.")
   public static final String MULTI_OBJECT_PROHIBIT_MODIFY_AT_GATE = "176";

   @RBEntry("Non è possibile effettuare l'operazione \"{0}\" in quanto {1} non è l'iterazione più recente. Aggiornare i dati e tentare nuovamente l'operazione.")
   @RBArgComment0("{0}- Action (Submit, Promote, Demote, Drop, etc) that is not allowed")
   @RBArgComment1(" {1} Identity of the object.")
   public static final String MULTI_OBJECT_NOT_LATEST_ITERATION = "177";

   @RBEntry(" \"{0}\" è una copia di lavoro e non può essere inviata. È necessario effettuare il Check-In.")
   @RBArgComment0("{0} The identity of the object.")
   public static final String MULTI_OBJECT_WORKING_COPY_SUBMIT = "178";

   @RBEntry("Impossibile effettuare l'operazione \"{0}\" su {1}. È in stato di Check-Out. ")
   @RBArgComment0("{0} refers to the name of the task")
   @RBArgComment1("{1} the identity of the object.")
   public static final String MULTI_OBJECT_CHECKED_OUT = "179";

   @RBEntry("  {1} non è uno stato valido nel ciclo di vita {2}.")
   @RBArgComment1("{0} the state the user attempted to change it to")
   @RBArgComment2("{1} the Life Cycle associated with the Life Cycle Managed object")
   public static final String INVALID_LIFECYCLE_STATE = "180";

   @RBEntry("\"{0}\" non è un'oggetto LifeCycleManaged. Per eseguire l'operazione, è necessario che l'oggetto destinazione sia gestito da cicli di vita.")
   @RBArgComment0("{0} is the class name of the invalid object that was encountered")
   public static final String INVALID_LCM_OBJECT = "181";

   @RBEntry("Un oggetto {0} è una copia in modifica. L'impostazione dello stato del ciclo di vita non è consentita sulle copie in modifica.")
   public static final String MULTI_OBJECT_WORKING_COPY_SET_STATE = "182";

   @RBEntry("L'oggetto {0} è memorizzato in uno schedario personale, pertanto il suo stato del ciclo di vita non può essere modificato. Spostare l'oggetto in una posizione condivisa prima di tentare di impostare lo stato.")
   public static final String MULTI_OBJECT_PERSONAL_CABINET_SET_STATE = "183";

   @RBEntry("Aggiorna team")
   @RBComment("label for update team link")
   public static final String UPDATE_TEAM_LINK_LABEL = "188";

   /**
    * Localization of event names for LifeCycle History display
    **/
   @RBEntry("Aggiorna partecipanti ai ruoli")
   public static final String PRIVATE_CONSTANT_0 = "Augment_Life_Cycle";

   @RBEntry("Declassa")
   public static final String PRIVATE_CONSTANT_1 = "Demote";

   @RBEntry("Rifiuta")
   public static final String PRIVATE_CONSTANT_2 = "Deny";

   @RBEntry("Abbandona")
   public static final String PRIVATE_CONSTANT_3 = "Drop";

   @RBEntry("Immetti fase")
   public static final String PRIVATE_CONSTANT_4 = "Enter_Phase";

   @RBEntry("Promuovi")
   public static final String PRIVATE_CONSTANT_5 = "Promote";

   @RBEntry("Assegna a un altro ciclo di vita")
   public static final String PRIVATE_CONSTANT_6 = "Reassign";

   @RBEntry("Assegna a un altro progetto")
   public static final String PRIVATE_CONSTANT_7 = "Reproject";

   @RBEntry("Assegna a un altro team")
   public static final String PRIVATE_CONSTANT_8 = "Reteam";

   @RBEntry("Imposta stato del ciclo di vita")
   public static final String PRIVATE_CONSTANT_9 = "Set_State";

   @RBEntry("Invia")
   public static final String PRIVATE_CONSTANT_10 = "Submit";

   @RBEntry("Voto")
   public static final String PRIVATE_CONSTANT_11 = "Vote";

   /**
    * Headers for LifeCycle History page
    **/
   @RBEntry("Cronologia del ciclo di vita per ")
   public static final String LIFECYCLE_HEADER_PREFIX = "200";

   @RBEntry("Data")
   public static final String DATE_HEADER = "201";

   @RBEntry("Utente ")
   public static final String USER_HEADER = "202";

   @RBEntry("Azione")
   public static final String ACTION_HEADER = "203";

   @RBEntry("Stato del ciclo di vita")
   public static final String STATE_HEADER = "204";

   @RBEntry("Ciclo di vita")
   public static final String LIFE_CYCLE_HEADER = "205";

   @RBEntry("Team")
   public static final String TEAM_HEADER = "206";

   /**
    * For Multiple reassignment
    **/
   @RBEntry("Regola di inizializzazione non definita per il tipo di oggetto selezionato.")
   public static final String NO_LIFECYCLE_INIT_RULE = "207";

   @RBEntry("Ciclo di vita")
   public static final String LIFECYCLE_DISPLAY_NAME = "208";

   @RBEntry("Problema durante la creazione dell'oggetto {0} \"{1}\" per il modello di ciclo di vita \"{2}\". Consultare il log del method server.")
   @RBArgComment1("{0} The object type that reported the problem.")
   @RBArgComment2("{1} The name of the object type.")
   @RBArgComment3("{2} The name of the lifecycle template.")
   public static final String FAILURE_MESSAGE = "209";

   @RBEntry("Nessun nome definito per l'oggetto {0}.")
   @RBArgComment1("{0} The object type that reported the problem.")
   public static final String FAILURE_MESSAGE1 = "210";

   @RBEntry("CRONOLOGIA DEL CICLO DI VITA")
   public static final String LIFECYCLE_HISTORY_CAPS = "211";

   @RBEntry("Criterio:")
   public static final String CRITERION_COLON = "212";

   @RBEntry("Soddisfatto")
   public static final String SATISFIED = "213";

   @RBEntry("Condizione")
   public static final String ASSERTION = "214";

   @RBEntry("Firme:")
   public static final String SIGNATURES_COLON = "215";

   @RBEntry("Firmatario")
   public static final String SIGNER = "216";

   @RBEntry("Inviato")
   public static final String SUBMITTED = "217";

   @RBEntry("Invio di {0}")
   public static final String SUBMIT_OF = "218";

   @RBEntry("per")
   public static final String FOR_LABEL = "219";

   @RBEntry("Voto")
   public static final String VOTE = "220";

   @RBEntry("Impossibile determinare il modello del ciclo di vita in base alle regole di inizializzazione per l'oggetto. Non esistono voci definite per il modello o non è possibile trovare modelli validi in base alle voci regola di inizializzazione oggetto.")
   public static final String NO_VALID_LIFECYCLE = "221";

   @RBEntry("Transizioni di revisione non definite per la versione più recente nel modello del ciclo di vita.")
   public static final String NO_REVISETRANSITION_LATEST = "222";

   @RBEntry("Transizioni di revisione non definite per la versione corrente nel modello del ciclo di vita.")
   public static final String NO_REVISETRANSITION_CURRENT = "223";

   @RBEntry("L'esportazione di modelli di ciclo di vita disattivati non è supportata.")
   public static final String NO_EXPORT_LC_DISABLED = "224";

   @RBEntry("L'esportazione di modelli di ciclo di vita nascosti non è supportata.")
   public static final String NO_EXPORT_LC_HIDDEN = "225";

   @RBEntry("L'importazione di modelli di ciclo di vita disattivati non è supportata.")
   public static final String NO_IMPORT_LC_DISABLED = "226";

   @RBEntry("L'importazione di modelli di ciclo di vita nascosti non è supportata.")
   public static final String NO_IMPORT_LC_HIDDEN = "227";
   
   @RBEntry("Esci dalla fase")
   public static final String EXIT_PHASE_CONSTANT = "Exit_Phase";
   
   @RBEntry("ATTENZIONE: non è possibile disattivare un modello di ciclo di vita impostato come ciclo di vita di default.")
   public static final String CANT_DISABLE_LCT = "CANT_DISABLE_LCT";

   @RBEntry("ATTENZIONE: il riferimento del modello del ciclo di vita non deve essere nullo per gli oggetti gestiti dal ciclo di vita.")
   public static final String NULL_LIFECYCCLE_TEMPLATEREF = "NULL_LIFECYCCLE_TEMPLATEREF";
   
   @RBEntry("ATTENZIONE: il modello del ciclo di vita {0} è stato sottoposto a Check-Out da {1}. Non è possibile importare un'iterazione più recente se non si esegue il Check-In del modello.")
   public static final String LIFE_CYCLE_CHECKED_OUT_MSG = "LIFE_CYCLE_CHECKED_OUT_MSG";

   
}
