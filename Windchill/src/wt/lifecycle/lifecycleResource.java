/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.lifecycle;

import wt.util.resource.*;

@RBUUID("wt.lifecycle.lifecycleResource")
public final class lifecycleResource extends WTListResourceBundle {
   @RBEntry("The object has not been initialized (i.e., it was null).")
   public static final String NULL_OBJECT = "0";

   @RBEntry("The value passed for parameter - \"{0}\" was null.")
   @RBArgComment0("{0} refers to the parameters that is null.")
   public static final String NULL_PARAMETER = "1";

   @RBEntry("Cannot promote. A final phase has no successor phase")
   public static final String NO_SUCCESSOR = "2";

   @RBEntry("Cannot demote. An initial phase has no predecessor phase")
   public static final String NO_PREDECESSOR = "3";

   @RBEntry("Cannot do state change. Object is not persistent")
   public static final String NOT_PERSISTENT = "4";

   @RBEntry("Internal error:The PhaseTemplate has no state to associated to it.")
   public static final String NO_STATE = "5";

   @RBEntry("Internal error: Current Phase not found.  Probable causes:  The queued task to assign the object to a life cycle has not completed yet or failed.  Try again later or ask your administrator to check the queue log.")
   public static final String NO_PHASE = "6";

   @RBEntry("Internal error: Default life cycle definition not found")
   public static final String NO_DEFAULT = "7";

   @RBEntry("The assigned Life Cycle is null")
   public static final String NULL_TEMPLATE = "8";

   @RBEntry("The signature is null")
   public static final String NULL_SIGNATURE = "9";

   @RBEntry("The signature must have a signer designated")
   public static final String NO_SIGNER = "10";

   @RBEntry("The signature must have a vote specified")
   public static final String NO_VOTE = "11";

   @RBEntry("The queued task to assign the object to a life cycle has not completed yet or failed.  Try the \"{0}\" task later or ask your administrator to check the queue log.")
   @RBArgComment0("{0} refers to the name of the task")
   public static final String NOT_ASSIGNED_YET = "12";

   @RBEntry("\"{0}\" has not been submitted. The \"{1}\" cannot be done. ")
   @RBArgComment0("{0} refers to the an identification of an object ")
   @RBArgComment1("{1} refers to the name of the task that was attempted")
   public static final String NOT_SUBMITTED = "13";

   @RBEntry("You are not a \"{0}\" for the {1} object")
   @RBArgComment0("{0} refers to the name of role")
   @RBArgComment1("{1} refers to the name of object")
   public static final String NOT_ROLEPLAYER = "14";

   @RBEntry("The following operations are not allowed on a checked-out object:{0}")
   @RBArgComment0("{0} refers to the name of the task")
   public static final String CHECKED_OUT_OBJECT = "15";

   @RBEntry("A working copy may not be submitted. Check-in is required.")
   public static final String WORKING_COPY_SUBMIT = "16";

   @RBEntry("Invalid Assertion for this transaction container")
   public static final String INVALID_ASSERTION = "17";

   @RBEntry("Invalid Object found in BatchContainer")
   public static final String INVALID_BC_OBJECT = "18";

   @RBEntry("Invalid Object found in RoleBatchContainer")
   public static final String INVALID_RBC_OBJECT = "19";

   @RBEntry("System Folder not found - Cannot folder the Life Cycle ")
   public static final String NO_SYSTEM_FOLDER = "20";

   @RBEntry("Invalid Link Assertion")
   public static final String INVALID_LINK_ASSERTION = "21";

   @RBEntry("The Notification List can only be related to 1 PhaseTemplate")
   public static final String TOO_MANY_DN = "22";

   @RBEntry("The NotificationList must be related to a PhaseTemplate")
   public static final String ZERO_DN = "23";

   @RBEntry("The Access Control List can only be related to 1 PhaseTemplate")
   public static final String TOO_MANY_AHAL = "24";

   @RBEntry("The Access Control List must be related to a PhaseTemplate")
   public static final String ZERO_AHAL = "25";

   @RBEntry("The Criterion List can only be related to 1 PhaseTemplate")
   public static final String TOO_MANY_DC = "26";

   @RBEntry("The Criterion must be related to a PhaseTemplate")
   public static final String ZERO_DC = "27";

   @RBEntry("The PhaseTemplate can only be related to 1 LifeCycleTemplate")
   public static final String TOO_MANY_PL = "28";

   @RBEntry("The PhaseTemplate must be related to a LifeCycleTemplate")
   public static final String ZERO_PL = "29";

   @RBEntry("The PhaseTemplate can only be related to 1 Predecessor PhaseTemplate")
   public static final String TOO_MANY_PREDECESSOR = "30";

   @RBEntry("The PhaseTemplate must be related to a Predecessor PhaseTemplate")
   public static final String ZERO_PREDECESSOR = "31";

   @RBEntry("The PhaseTemplate can only be related to 1 Successor PhaseTemplate")
   public static final String TOO_MANY_SUCCESSOR = "32";

   @RBEntry("The PhaseTemplate must be related to a Successor PhaseTemplate")
   public static final String ZERO_SUCCESSOR = "33";

   @RBEntry("Invalid Delete Assertion:  The Life Cycle is in use.")
   public static final String LCT_IN_USE = "34";

   @RBEntry("Submitter role is required.")
   public static final String SBMTR_ROLE_REQD = "35";

   @RBEntry("Promoter role is required.")
   public static final String PRMTR_ROLE_REQD = "36";

   @RBEntry("System Folder not found - Cannot folder the Project ")
   public static final String NO_SYS_FOLDER = "37";

   @RBEntry("Cannot delete a Life Cycle")
   public static final String CANNOT_DELETE_LCT = "38";

   @RBEntry("** Error **")
   public static final String ERROR_ALERT = "39";

   @RBEntry("Error reading wt.workflow.* properties")
   public static final String PROPERTIES_ERROR = "40";

   @RBEntry("Yes")
   public static final String VOTE_YES_INDICATOR = "41";

   @RBEntry("No")
   public static final String VOTE_NO_INDICATOR = "42";

   @RBEntry("Yes")
   public static final String SATISFIED_INDICATOR = "43";

   @RBEntry("No")
   public static final String NOT_SATISFIED_INDICATOR = "44";

   @RBEntry("No folder has been assigned in which to store the Project.  Project not saved.")
   public static final String NO_PROJECT_FOLDER = "45";

   @RBEntry("The administrative domain: \"{0}\" has multiple projects with name: \"{1}\".  The project has not been set for this object.")
   @RBArgComment0("{0} refers to the name of the administrative domain")
   @RBArgComment1("{1} refers to the name of an administrative domain")
   public static final String MULTIPLE_PROJECTS = "46";

   @RBEntry("A project \"{0}\" does not exist in the administrative domain: \"{1}\".  The project has not been set for this object.")
   @RBArgComment0("{0} refers to a project name")
   @RBArgComment1("{1} refers to the name of an administrative domain")
   public static final String NONEXISTENT_PROJECT = "47";

   @RBEntry("The project is set for the LifeCycleManaged object but does not reference an actual project.")
   public static final String PROJECT_REF_NOT_SET = "48";

   @RBEntry("A required role could not be resolved, and it was not possible to send a notification to the Submitter, object owner, or current user.")
   public static final String NULL_CURRENT_USER = "49";

   @RBEntry("Projects cannot be deleted from the system.")
   public static final String PROHIBIT_DELETE_PROJECT = "50";

   @RBEntry("Life Cycles cannot be deleted from the system.")
   public static final String PROHIBIT_DELETE_LCT = "51";

   @RBEntry("You can not change the identity of a project.")
   public static final String PROHIBIT_IDCHANGE_PROJECT = "52";

   @RBEntry("You can not change the identity of a Life Cycle.")
   public static final String PROHIBIT_IDCHANGE_LCT = "53";

   @RBEntry("The object is currently awaiting promotion.  A submit is not allowed when the object is awaiting promotion.")
   public static final String SUBMIT_FROM_GATE = "54";

   @RBEntry("The object is in the final phase.  A promote is not allowed from the final phase.")
   public static final String PROMOTE_FROM_FINAL_PHASE = "55";

   @RBEntry("The object is in the initial phase.  A demote is not allowed from the initial phase.")
   public static final String DEMOTE_FROM_INITIAL_PHASE = "56";

   @RBEntry("The administrator user is not found")
   public static final String MISSING_ADMINISTRATOR = "57";

   @RBEntry("Invalid applet parameters: \"{0}\".  ")
   @RBArgComment0("{0} refers to the parameters that were passsed to the applet.")
   public static final String BAD_PARAMS = "58";

   @RBEntry("The life cycle states in the list must be unique: {0}.")
   @RBComment("This error message indicates that a list of states is being used in a situation where the elements (states) in that list must be unique, but are not (i.e., there is at least one duplicate state in the list).")
   @RBArgComment0("{0} refers to a list of life cycle states.")
   public static final String INIT_DUPLICATE_STATE = "184";

   @RBEntry("Transition \"{0}\" may not map state \"{1}\" to more than one state: {2}.  Check the value of the system property: \"wt.lifecycle.transitions.Default.{0}.\"")
   @RBComment("This error message indicates that an attempt is being made to initialize in a non-deterministic fashion a transition that is configured to be deterministic (i.e., the transition can be used to map one state to only one other).")
   @RBArgComment0("{0} refers to a life cycle transition.")
   @RBArgComment1("{1} refers to the source life cycle state.")
   @RBArgComment2("{2} refers to a set of destination life cycle states that has more than one element.")
   public static final String INIT_DETERMINISTIC_TRANSITION = "185";

   @RBEntry("System configuration error.  Transitions of type \"{0}\" are reserved for internal purposes and may not be used explicitly in property: {1}.")
   @RBComment("This error message indicates that an attempt has been made to use an internal (non-selectable) transition.  These are reserved for internal use by the software and may not be specified for other purposes.")
   @RBArgComment0("{0} refers to the transition type being used illegally")
   @RBArgComment1("{1} is the entire property value containing the syntax error")
   public static final String INIT_ILLEGAL_TRANSITION = "186";

   @RBEntry("System configuration error.  Syntax error between characters {0} and {1} of property string: {2}.")
   @RBComment("This error message indicates that property for default transition initialization contains a syntax error.")
   @RBArgComment0("{0} refers to the column in which the start of the syntax error was detected")
   @RBArgComment1("{1} refers to the column in which the end of the syntax error was detected")
   @RBArgComment2("{2} is the entire property value containing the syntax error")
   public static final String INIT_BAD_TOKEN = "187";

   @RBEntry("Transition \"{0}\" for LifeCycleTemplate \"{1}\" may not map state \"{2}\" to more than one state: {3}.")
   @RBComment("This error message indicates that an attempt is being made to initialize in a non-deterministic fashion a transition that is configured to be deterministic (i.e., the transition can be used to map one state to only one other).")
   @RBArgComment0("{0} refers to a life cycle transition.")
   @RBArgComment1("{1} refers to the source life cycle template.")
   @RBArgComment2("{2} refers to the source life cycle state.")
   @RBArgComment3("{3} refers to a set of destination life cycle states that has more than one element.")
   public static final String INVALID_MULTIPLE_TRANSITIONS = "195";

   @RBEntry("Transition \"{0}\" is required for LifeCycleTemplate \"{1}\" from state \"{2}\".")
   @RBComment("This error message indicates that there is no transition defined from a given state to another and one is required.")
   @RBArgComment0("{0} refers to a life cycle transition.")
   @RBArgComment1("{1} refers to the source life cycle template.")
   @RBArgComment2("{2} refers to the source life cycle state.")
   public static final String NO_DEFINED_TRANSITIONS = "196";

   @RBEntry("Promote ")
   public static final String PROMOTE = "87";

   @RBEntry("Review ")
   public static final String REVIEW = "88";

   @RBEntry("Observe ")
   public static final String OBSERVE = "89";

   @RBEntry("Submit ")
   public static final String SUBMIT = "90";

   @RBEntry("Approve")
   public static final String APPROVE_VOTE_LABEL = "144";

   @RBEntry("Do not approve")
   public static final String DO_NOT_APPROVE_VOTE_LABEL = "145";

   @RBEntry("Promote")
   public static final String PROMOTE_VOTE_LABEL = "146";

   @RBEntry("Deny")
   public static final String DENY_VOTE_LABEL = "147";

   @RBEntry("Demote")
   public static final String DEMOTE_VOTE_LABEL = "148";

   @RBEntry("Drop")
   public static final String DROP_VOTE_LABEL = "149";

   @RBEntry(":")
   public static final String COLON = "150";

   /**
    * Message Text for wt.lifecycle.LoadLifeCycle
    **/
   @RBEntry("LoadLifeCycle: The field \"{0}\" is not a valid field for this record type. Aborting this record.")
   @RBArgComment0("{0}- an invalid field")
   public static final String LOAD_INVALID_FIELD = "59";

   @RBEntry("LoadLifeCycle Warning: A value for field \"{0}\" was not found in the input file.  Trying to persist the entry anyway.....  ")
   @RBArgComment0("{0}- a field which did not have a value")
   public static final String LOAD_NO_VALUE = "60";

   @RBEntry("LoadLifeCycle: A value for \"{0}\" is required to construct the current object.  Aborting this record.")
   @RBArgComment0("{0}- a field which requires a value")
   public static final String LOAD_NULL_ILLEGAL = "61";

   @RBEntry("LoadLifeCycle: Role \"{0}\" not found in Role resource bundle (RoleRB.class).  The role must exist in the resource bundle.  Aborting this record.")
   @RBArgComment0("{0}- a Role name")
   public static final String LOAD_NO_ROLE_BAD = "62";

   @RBEntry("LoadLifeCycle: Role \"{0}\" not found in Role resource bundle (RoleRB.class).  Null Role ok.  Continuing...")
   @RBArgComment0("{0}- a Role name")
   public static final String LOAD_NO_ROLE_OK = "63";

   @RBEntry("LoadLifeCycle: Boolean value for \"{0}\" not found in input file.  Defaulting to \"{1}\".")
   @RBArgComment0("{0}- a field name for a boolean field")
   public static final String LOAD_NO_BOOLEAN_OK = "64";

   @RBEntry("LoadLifeCycle: Boolean value for \"{0}\" not found in input file.  This value must be defined in the input file.")
   @RBArgComment0("{0}- a field name for a boolean field")
   public static final String LOAD_NO_BOOLEAN_BAD = "65";

   @RBEntry("LoadLifeCycle: {0} cabinet is nonexistent.  LifeCycleTemplate can not be created.")
   public static final String LOAD_NO_SYSTEM_CABINET = "66";

   @RBEntry("LoadLifeCycle: No folder location was given.  Attempting to save LifeCycleTemplate in \"{0}\".")
   @RBArgComment0("{0}- the default folder location for life cycle")
   public static final String LOAD_NULL_LOCATION = "67";

   @RBEntry("LoadLifeCycle: LifeCycleTemplates must be stored in the cabinet \"{0}\".  Defaulting to folder \"{1}\".")
   @RBArgComment0("{0}- name of the cabinet in which LifeCycleTemplates must be stored")
   @RBArgComment1("{1}- name of default folder")
   public static final String LOAD_NOT_SYSTEM_CABINET = "68";

   @RBEntry("LoadLifeCycle: Folder location \"{0}\" does not exist.  LifeCycleTemplate can not be saved.")
   @RBArgComment0("{0}- folder location which does not exist")
   public static final String LOAD_INVALID_FOLDER = "70";

   @RBEntry("LoadLifeCycle: Error detected in {0}.  Check Method Server log.")
   @RBArgComment0("{0}- name of Java method in which error occured")
   public static final String LOAD_EXCEPTION = "71";

   @RBEntry("LoadLifeCycle: Aborting record because there is no cached current PhaseTemplate.")
   public static final String LOAD_NO_CURRENT_PT = "72";

   @RBEntry("LoadLifeCycle: Insure that a PhaseTemplateBegin record preceeded the RoleHolder record.")
   public static final String LOAD_INSURE_PT_RH = "73";

   @RBEntry("LoadLifeCycle: Insure that a PhaseTemplateBegin record preceeded the LifeCycleRoleHolder record.")
   public static final String LOAD_INSURE_PT_LCRH = "74";

   @RBEntry("LoadLifeCycle: Insure that a PhaseTemplateBegin record preceeded the LifeCycleRoleContext record.")
   public static final String LOAD_INSURE_PT_LCRC = "75";

   @RBEntry("LoadLifeCycle: The principal \"{0}\" is not a valid user or group.  A valid principal is required.  RoleHolder record not processed.")
   @RBArgComment0("{0}- name of a user or a group")
   public static final String LOAD_INVALID_PRINCIPAL = "76";

   @RBEntry("LoadLifeCycle: State \"{0}\" not found in State resource bundle (StateRB.class).  The state must exist in the resource bundle.  PhaseTemplate record not processed.")
   @RBArgComment0("(0}- name of a life cycle state which is not in the resource bundle")
   public static final String LOAD_INVALID_STATE = "77";

   @RBEntry("LoadLifeCycle: A createPhaseTemplateEnd must preceed this record since this is not the first phase.  Aborting PhaseTemplate.")
   public static final String LOAD_MISSING_PTEND = "78";

   @RBEntry("LoadLifeCycle: No value in cache for LifeCycleTemplate object.  PhaseTemplate will not be persisted.")
   public static final String LOAD_NO_CACHE_LCT = "79";

   @RBEntry("LoadLifeCycle: Folder location \"{0}\" does not exist.  Attempting to save Project in \"{1}\"")
   @RBArgComment0("{0}- a folder location that does not exist")
   @RBArgComment1("{1}- default folder location for Projects")
   public static final String LOAD_INVALID_FOLDER_DEFAULT = "80";

   @RBEntry("LoadLifeCycle: Folder location \"{0}\" does not exist.  Project can not be persisted.")
   @RBArgComment0(" {0}- a folder location that does not exist")
   public static final String LOAD_INVALID_FOLDER_FINAL = "81";

   @RBEntry("LoadLifeCycle: Insure that a PhaseTemplateBegin record preceeded the PhaseTemplateEnd record.")
   public static final String LOAD_INSURE_PT_PTEND = "82";

   @RBEntry("LoadLifeCycle: No cached value for current Project.  Insure that a ProjectBegin record occurred before the ProjectEnd record.")
   public static final String LOAD_NO_CACHE_PROJECT = "83";

   @RBEntry("LoadLifeCycle: Aborting this record because there is no cached current AdHocNotificationList.  Insure that a createAHNLBegin record preceeded this record.")
   public static final String LOAD_NO_CACHE_AHNL = "84";

   @RBEntry("LoadLifeCycle: The principal \"{0}\" is not a valid user.  A valid principal is required.  NotificationSetPrincipal record not processed.")
   @RBArgComment0("{0}- name of a user or a group")
   public static final String LOAD_INVALID_PRINCIPAL_NOTIFY = "85";

   @RBEntry("The role: \"{0}\" for this task could not be retrieved. ")
   @RBArgComment0("{0} refers to the role name who is to perform a task with the applet.")
   public static final String ROLE_NOT_FOUND = "91";

   @RBEntry("Incomplete task ")
   public static final String TASK_FAILED_TITLE = "92";

   @RBEntry("The task cannot be completed because \"{0}\" is not available. ")
   @RBArgComment0("{0} refers to the object identifier of the task's target object")
   public static final String TASK_OBJECT_MISSING = "93";

   @RBEntry("Pending")
   public static final String VOTE_PENDING_INDICATOR = "94";

   @RBEntry("The table containing the reviews could not be built. ")
   public static final String REVIEW_TABLE_FAILED = "95";

   @RBEntry("Reviews")
   public static final String REVIEW_COLUMN_LABEL = "96";

   @RBEntry("Role")
   public static final String ROLE_COLUMN_LABEL = "97";

   @RBEntry("Approve")
   public static final String APPROVE_COLUMN_LABEL = "98";

   @RBEntry("Comments")
   public static final String COMMENTS_COLUMN_LABEL = "99";

   @RBEntry("was successful")
   public static final String WAS_SUCCESSFUL = "100";

   @RBEntry("failed")
   public static final String FAILED = "101";

   @RBEntry("This task is obsolete.  The task references the \"{0}\"  object in state \"{1}\", but the object has moved to state \"{2}\".  Delete this task from your inbox.")
   @RBArgComment0("{0}- identification attribute of the task's target object")
   @RBArgComment1("{1}- The lifecycle state of the object that the task expected")
   @RBArgComment2("{2}- The actual lifecycle state of the task's target object")
   public static final String OBSOLETE_TASK = "102";

   @RBEntry("A project cannot be stored in a personal cabinet.  Choose another location.  ")
   public static final String PROJECT_IN_USER_LOCATION = "103";

   @RBEntry("An object stored in a personal cabinet may not be submitted.  Move the object to a shared location before attempting the Submit. ")
   public static final String PERSONAL_CABINET_SUBMIT = "104";

   @RBEntry("Invalid applet parameters. \"{0}\" ")
   public static final String INVALID_PARAMETERS = "105";

   @RBEntry("This object is undergoing a review process and possible promotion to the next phase.  No changes are allowed.  ")
   public static final String PROHIBIT_MODIFY_AT_GATE = "106";

   @RBEntry("\"{0}\" not allowed because the object is not Life Cycle Managed.  ")
   @RBArgComment0("{0}- Action (Submit, Promote, Demote, Drop, etc) that is not allowed")
   public static final String OBJECT_NOT_LCMD = "107";

   @RBEntry("\"{0}\" not allowed because the object is not the latest iteration.  Refresh the data and try again.  ")
   @RBArgComment0("{0}- Action (Submit, Promote, Demote, Drop, etc) that is not allowed")
   public static final String OBJECT_NOT_LATEST_ITERATION = "108";

   @RBEntry("Unable to fill in the attributes for the Life Cycle in the object.")
   public static final String INFLATE_FAILED = "109";

   @RBEntry("Completed by Robot")
   public static final String ROBOT_SIGNATURE_TEXT = "110";

   @RBEntry("Unable to set search criteria to the LifeCycle = {0}.")
   @RBArgComment0("{0} = the lifecycletemplate object.")
   public static final String SEARCH_CRITERIA = "111";

   @RBEntry("LifeCycle \"{0}\" not valid.")
   @RBArgComment0("{0} is the LifeCycleTemplate name.")
   public static final String LCTEMPLATE_INVALID = "112";

   @RBEntry("You are not authorized to {0}.  You need modify rights on the object to complete this task.")
   @RBArgComment0("{0} action the user is not authorized to complete")
   public static final String NOT_AUTHORIZED_TO_MODIFY = "113";

   @RBEntry("You are not authorized to update role participants for the {0} role.")
   @RBArgComment0("{0} the role that the current user is not allowed to augment")
   public static final String NOT_AUTHORIZED_TO_AUGMENT_ROLE = "114";

   @RBEntry("Life Cycle History")
   public static final String LC_HISTORY = "115";

   @RBEntry("\"{0}\" is not LifeCycleManaged.  This task requires the primaryBusinessObject to be LifeCycleManaged.")
   @RBArgComment0("{0} is the class name of the object which is the target of one of the lifecycle tasks (Submit, Review, Promote, and Observe).")
   public static final String INVALID_TASK_OBJECT = "116";

   @RBEntry("Submit")
   public static final String SUBMIT_URL_LABEL = "117";

   @RBEntry("The {0} object cannot be submitted at this time.  To complete this action, you must have a submit work item for this object.  You will receive this work item when the Submit task is active in the appropriate Workflow.  ")
   @RBArgComment0("{0}  is the identity of the object")
   public static final String OBJECT_NOT_SUBMITABLE = "118";

   @RBEntry("Not able to update {0} role participants.  The role does not exist.  ")
   @RBArgComment0("{0}  the name of the role that is attempting to be augmented")
   public static final String AUGMENT_NON_EXISTENT_ROLE = "119";

   @RBEntry("The attempt to set the state of the {0} object failed.  {1} is not a valid state in the {2} Life Cycle.")
   @RBArgComment0("{0} the identity of the Life Cycle Managed object")
   @RBArgComment1("{1} the state the user attempted to change it to")
   @RBArgComment2("{2} the Life Cycle associated with the Life Cycle Managed object")
   public static final String INVALID_STATE = "120";

   @RBEntry("The Life Cycle must be stored in a personal cabinet or the System cabinet.")
   public static final String LCT_CABINET_INVALID = "121";

   @RBEntry("The selected Life Cycle is not the latest iteration.  Refresh your data and retry.  ")
   public static final String SELECTED_LCT_NOT_LATEST = "122";

   @RBEntry("The selected Life Cycle is not enabled.  Choose an Life Cycle that is enabled.  ")
   public static final String SELECTED_LCT_NOT_ENABLED = "123";

   @RBEntry("The working copy of the {0} Life Cycle is used in {1} places.  The following uses must be removed before completing the checkin: {2}")
   @RBArgComment0("{0} the name of the Life Cycle")
   @RBArgComment1("{1} the number of places where the Life Cycle is in use.")
   @RBArgComment2("{2} the identities of the objects using the Life Cycle")
   public static final String CHECKIN_LCT_IS_IN_USE = "124";

   @RBEntry("This iteration of the {0} Life Cycle is used in {1} places.  The following uses must be removed before completing the delete: {2}")
   @RBArgComment0("{0} the name of the Life Cycle")
   @RBArgComment1("{1} the number of places where the Life Cycle is in use.")
   @RBArgComment2("{2} the identities of the objects using the Life Cycle")
   public static final String DELETE_LCT_IS_IN_USE = "125";

   @RBEntry("Life Cycle Masters cannot be deleted from the system.")
   public static final String PROHIBIT_DELETE_LCTM = "126";

   @RBEntry("You cannot set the Life Cycle because the {0} object is already persisted.  Use the Reassign Life Cycle option.  ")
   @RBArgComment0("{0}  the LifeCycle Managed object")
   public static final String CANT_SET_LC_WHEN_PERSISTENT = "127";

   @RBEntry("You cannot set the Life Cycle State because the {0} object is already persisted. ")
   @RBArgComment0("{0}  the LifeCycle Managed object")
   public static final String CANT_SET_STATE_WHEN_PERSISTENT = "128";

   @RBEntry("The attempt to create the {0} object in the {1} state of the {2} Life Cycle failed.  {1} is not a valid state in the {2} Life Cycle.  ")
   @RBArgComment0("{0}  the identity of the LifeCycle Managed object")
   @RBArgComment1("{1}  the state the object is being created into")
   @RBArgComment2("{2}  the Life Cycle the object is set to")
   public static final String INVALID_STATE_ON_CREATE = "129";

   @RBEntry("This task cannot be constructed because the primary business object associated to the task has been deleted.")
   public static final String PRIMARY_BUSINESS_OBJECT_DELETED = "130";

   @RBEntry("This iteration of the {0} Life Cycle is used in {1} places.  The following uses must be removed before updating the Life Cycle: {2}")
   @RBArgComment0("{0} the name of the Life Cycle")
   @RBArgComment1("{1} the number of places where the Life Cycle is in use.")
   @RBArgComment2("{2} the identities of the objects using the Life Cycle")
   public static final String PROHIBIT_MODIFY_LCT_IN_USE = "131";

   @RBEntry("You are not authorized to set the state of the {0} object.  You need administrative permission on the object to complete this action.")
   @RBArgComment0("{0} the identity of the Life Cycle Managed object")
   public static final String NOT_AUTHORIZED_TO_SET_STATE = "132";

   @RBEntry("Copy Of ")
   public static final String COPY_OF = "133";

   @RBEntry("Warning:  The \"{0}\" workflow template is not found.  Persisting the Life Cycle Phase without this phase attribute....")
   public static final String LOAD_INVALID_PHASE_WORKFLOW = "134";

   @RBEntry("Warning:  The \"{0}\" workflow template is not found.  Persisting the Life Cycle Phase without this gate attribute....")
   public static final String LOAD_INVALID_GATE_WORKFLOW = "135";

   @RBEntry("Unable to find or create \"{0}\".  ")
   @RBArgComment0("{0} the name of the file or directory that we are trying to create")
   public static final String UNABLE_TO_CREATE_DIR = "136";

   @RBEntry(", ")
   public static final String IDENTITY_STRING_DELIMITER = "137";

   @RBEntry("{0} is used by Life Cycle in {1} places.  The following uses must be removed before completing the delete: {2}")
   @RBArgComment0("{0} the identity of the Workflow")
   @RBArgComment1("{1} the number of places where the workflow is in use.")
   @RBArgComment2("{2} the identities of the objects using the Workflow")
   public static final String DELETE_WF_IS_IN_USE = "138";

   @RBEntry("Life cycle tasks (submit, review, promote, observe) must have a Primary Business Object associated with their workflow process for the tasks to be valid.  You can complete the activity which generated this task with the Process Manager.")
   public static final String PBO_REQUIRED_FOR_LC_TASKS = "139";

   @RBEntry("Warning:  The {0} supported class is not found in the current system.  Attempting to create the Life Cycle without a supported class specified...")
   @RBArgComment0("{0} the name of the class that is not found in the current system")
   public static final String LOAD_INVALID_SUPPORTED_CLASS = "140";

   @RBEntry("Set State ")
   public static final String SET_STATE = "141";

   @RBEntry("A set state is not allowed on a working copy.")
   public static final String WORKING_COPY_SET_STATE = "142";

   @RBEntry("An object stored in a personal cabinet can not have it's state changed.  Move the object to a shared location before attempting the Set State. ")
   public static final String PERSONAL_CABINET_SET_STATE = "143";

   @RBEntry("Internal error: Current Phase not found.  ")
   public static final String NO_PHASE_FOUND = "151";

   @RBEntry("Completed")
   public static final String COMPLETED_LABEL = "152";

   @RBEntry("History")
   public static final String HISTORY_LABEL = "153";

   @RBEntry(" - ")
   @RBComment(" do not translate!")
   public static final String STATES_SEPARATOR = "154";

   @RBEntry("<B>")
   @RBPseudo(false)
   @RBComment(" do not translate!")
   public static final String CURRENT_STATE_BEGIN = "155";

   @RBEntry("</B>")
   @RBPseudo(false)
   @RBComment(" do not translate!")
   public static final String CURRENT_STATE_END = "156";

   @RBEntry("<B>")
   @RBPseudo(false)
   @RBComment(" do not translate!")
   public static final String LABEL_BEGIN = "157";

   @RBEntry(": </B>")
   @RBPseudo(false)
   @RBComment(" do not translate!")
   public static final String LABEL_END = "158";

   @RBEntry(" (")
   @RBComment(" do not translate!")
   public static final String STATE_LIST_BEGIN = "159";

   @RBEntry(") ")
   @RBComment(" do not translate!")
   public static final String STATE_LIST_END = "160";

   @RBEntry(" [")
   @RBComment(" do not translate!")
   public static final String DROPPED_STATE_BEGIN = "161";

   @RBEntry("] ")
   @RBComment(" do not translate!")
   public static final String DROPPED_STATE_END = "162";

   @RBEntry(" (")
   @RBComment(" do not translate!")
   public static final String HISTORY_NOTATION_BEGIN = "163";

   @RBEntry(") ")
   @RBComment(" do not translate!")
   public static final String HISTORY_NOTATION_END = "164";

   @RBEntry("Yes")
   public static final String IS_AT_GATE = "165";

   @RBEntry("No")
   public static final String IS_NOT_AT_GATE = "166";

   @RBEntry("Phases:")
   public static final String PHASES_LABEL = "167";

   @RBEntry("Warning:  A new iteration was created for the {0} Life Cycle. ")
   @RBArgComment0("{0} the name of the life cycle")
   public static final String NEW_ITERATION_CREATED = "168";

   @RBEntry("The {0} Life Cycle is ignored because it references an invalid Supported Class.")
   @RBArgComment0("{0} the name of the life cycle ")
   public static final String LCT_REFERENCES_INVALID_CLASS = "169";

   @RBEntry("You are not authorized to modify {0}")
   @RBArgComment0("{0} - is the Wf Process template object that the user is trying to modify ")
   public static final String MODIFY_NOT_ALLOWED = "170";

   @RBEntry("You are not authorized to create a Life Cycle.")
   public static final String CREATE_LCT_NOT_ALLOWED = "171";

   @RBEntry("LoadLifeCycle: There is no current AdHocAclSpec loaded into the cache.")
   public static final String LOAD_NO_CACLE_AHAS = "172";

   @RBEntry("LoadLifeCycle: No value in cache for ProjectTemplate object.  ProjectTemplate will not be persisted.")
   public static final String LOAD_NO_CACHE_TEAMTEMPLATE = "173";

   @RBEntry("Warning:  Import of {0} Life Cycle was unsuccessful.  You cannot import a new iteration of a checked out Life Cycle template.")
   public static final String LIFE_CYCLE_CHECKED_OUT = "174";

   @RBEntry("LoadLifeCycle: Multiple states with state name \"{0}\" are not allowed in the same lifecycle.")
   @RBArgComment0("{0}- A LifeCycleState string.")
   public static final String LOAD_INVALID_MULTI_STATES = "175";

   @RBEntry("{0} is undergoing a review process and possible promotion to the next phase.  No changes are allowed. ")
   @RBArgComment0(" {0} The identity of the object.")
   public static final String MULTI_OBJECT_PROHIBIT_MODIFY_AT_GATE = "176";

   @RBEntry("\"{0}\" not allowed because {1} is not the latest iteration.  Refresh the data and try again.")
   @RBArgComment0("{0}- Action (Submit, Promote, Demote, Drop, etc) that is not allowed")
   @RBArgComment1(" {1} Identity of the object.")
   public static final String MULTI_OBJECT_NOT_LATEST_ITERATION = "177";

   @RBEntry(" \"{0}\" is a working copy and can not be submitted. Check-in is required.")
   @RBArgComment0("{0} The identity of the object.")
   public static final String MULTI_OBJECT_WORKING_COPY_SUBMIT = "178";

   @RBEntry("You may not \"{0}\" {1} because it is checked out ")
   @RBArgComment0("{0} refers to the name of the task")
   @RBArgComment1("{1} the identity of the object.")
   public static final String MULTI_OBJECT_CHECKED_OUT = "179";

   @RBEntry("  {1} is not a valid state in the {2} Life Cycle.")
   @RBArgComment1("{0} the state the user attempted to change it to")
   @RBArgComment2("{1} the Life Cycle associated with the Life Cycle Managed object")
   public static final String INVALID_LIFECYCLE_STATE = "180";

   @RBEntry("\"{0}\" is not LifeCycleManaged.  This operation requires the target be LifeCycleManaged.")
   @RBArgComment0("{0} is the class name of the invalid object that was encountered")
   public static final String INVALID_LCM_OBJECT = "181";

   @RBEntry("A {0} is a working copy.  Set state is not allowed on a working copy.")
   public static final String MULTI_OBJECT_WORKING_COPY_SET_STATE = "182";

   @RBEntry("{0} is stored in a personal cabinet and can not have it's state changed.  Move the object to a shared location before attempting the Set State. ")
   public static final String MULTI_OBJECT_PERSONAL_CABINET_SET_STATE = "183";

   @RBEntry("Update Team")
   @RBComment("label for update team link")
   public static final String UPDATE_TEAM_LINK_LABEL = "188";

   /**
    * Localization of event names for LifeCycle History display
    **/
   @RBEntry("Update Role Participants")
   public static final String PRIVATE_CONSTANT_0 = "Augment_Life_Cycle";

   @RBEntry("Demote")
   public static final String PRIVATE_CONSTANT_1 = "Demote";

   @RBEntry("Deny")
   public static final String PRIVATE_CONSTANT_2 = "Deny";

   @RBEntry("Drop")
   public static final String PRIVATE_CONSTANT_3 = "Drop";

   @RBEntry("Enter Phase")
   public static final String PRIVATE_CONSTANT_4 = "Enter_Phase";

   @RBEntry("Promote")
   public static final String PRIVATE_CONSTANT_5 = "Promote";

   @RBEntry("Reassign Life Cycle")
   public static final String PRIVATE_CONSTANT_6 = "Reassign";

   @RBEntry("Reassign Project")
   public static final String PRIVATE_CONSTANT_7 = "Reproject";

   @RBEntry("Reassign Team")
   public static final String PRIVATE_CONSTANT_8 = "Reteam";

   @RBEntry("Set Life Cycle State ")
   public static final String PRIVATE_CONSTANT_9 = "Set_State";

   @RBEntry("Submit")
   public static final String PRIVATE_CONSTANT_10 = "Submit";

   @RBEntry("Vote")
   public static final String PRIVATE_CONSTANT_11 = "Vote";

   /**
    * Headers for LifeCycle History page
    **/
   @RBEntry("Life Cycle History for ")
   public static final String LIFECYCLE_HEADER_PREFIX = "200";

   @RBEntry("Date")
   public static final String DATE_HEADER = "201";

   @RBEntry("User")
   public static final String USER_HEADER = "202";

   @RBEntry("Action")
   public static final String ACTION_HEADER = "203";

   @RBEntry("State")
   public static final String STATE_HEADER = "204";

   @RBEntry("Life Cycle")
   public static final String LIFE_CYCLE_HEADER = "205";

   @RBEntry("Team")
   public static final String TEAM_HEADER = "206";

   /**
    * For Multiple reassignment
    **/
   @RBEntry("Initialization rule not defined for the selected object type.")
   public static final String NO_LIFECYCLE_INIT_RULE = "207";

   @RBEntry("Life Cycle")
   public static final String LIFECYCLE_DISPLAY_NAME = "208";

   @RBEntry("PROBLEM creating {0} object \"{1}\" for \"{2}\" Lifecyle template. Check Method Server log.")
   @RBArgComment1("{0} The object type that reported the problem.")
   @RBArgComment2("{1} The name of the object type.")
   @RBArgComment3("{2} The name of the lifecycle template.")
   public static final String FAILURE_MESSAGE = "209";

   @RBEntry("No name defined for {0} object.")
   @RBArgComment1("{0} The object type that reported the problem.")
   public static final String FAILURE_MESSAGE1 = "210";

   @RBEntry("LIFE CYCLE HISTORY")
   public static final String LIFECYCLE_HISTORY_CAPS = "211";

   @RBEntry("Criterion:")
   public static final String CRITERION_COLON = "212";

   @RBEntry("Satisfied")
   public static final String SATISFIED = "213";

   @RBEntry("Assertion")
   public static final String ASSERTION = "214";

   @RBEntry("Signatures:")
   public static final String SIGNATURES_COLON = "215";

   @RBEntry("Signer")
   public static final String SIGNER = "216";

   @RBEntry("Submitted")
   public static final String SUBMITTED = "217";

   @RBEntry("Submit of {0}")
   public static final String SUBMIT_OF = "218";

   @RBEntry("for")
   public static final String FOR_LABEL = "219";

   @RBEntry("Vote")
   public static final String VOTE = "220";

   @RBEntry("The Life Cycle template could not be determined based on the object initialization rules. Either no entries are defined for the template or no valid templates could be found based on the object initialization rule entries")
   public static final String NO_VALID_LIFECYCLE = "221";

   @RBEntry("Revise transitions not defined for latest version in life cycle template.")
   public static final String NO_REVISETRANSITION_LATEST = "222";

   @RBEntry("Revise transitions not defined for current version in life cycle template.")
   public static final String NO_REVISETRANSITION_CURRENT = "223";

   @RBEntry("Do not support export on a disabled life cycle template.")
   public static final String NO_EXPORT_LC_DISABLED = "224";

   @RBEntry("Do not support export on a hidden life cycle template.")
   public static final String NO_EXPORT_LC_HIDDEN = "225";

   @RBEntry("Do not support import on a disabled life cycle template.")
   public static final String NO_IMPORT_LC_DISABLED = "226";

   @RBEntry("Do not support import on a hidden life cycle template.")
   public static final String NO_IMPORT_LC_HIDDEN = "227";
   
   @RBEntry("Exit Phase")
   public static final String EXIT_PHASE_CONSTANT = "Exit_Phase";
   
   @RBEntry("ATTENTION: You can not disable a lifecycle template that is set as the default lifecycle.")
   public static final String CANT_DISABLE_LCT = "CANT_DISABLE_LCT";

   @RBEntry("ATTENTION: Lifecycle Template Reference should not be null for lifecycle managed objects.")
   public static final String NULL_LIFECYCCLE_TEMPLATEREF = "NULL_LIFECYCCLE_TEMPLATEREF";
   
   @RBEntry("ATTENTION: Life cycle template {0} has been checked out by {1}. You cannot import a newer iteration unless this template is checked in.")
   public static final String LIFE_CYCLE_CHECKED_OUT_MSG = "LIFE_CYCLE_CHECKED_OUT_MSG";

   
}
