/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.configuration;

import wt.util.resource.*;

@RBUUID("wt.configuration.configurationResource")
public final class configurationResource extends WTListResourceBundle {
   @RBEntry("More than one version of {0} was selected by the config spec.  The config spec should select at most one version of a given master.")
   @RBArgComment0("The identity the master.")
   public static final String CONFIG_SPEC_SELECTED_MULTIPLE_VERSIONS = "0";

   @RBEntry("Attempted to remove a member that does not exist.")
   public static final String MEMBER_TO_REMOVE_NO_LONGER_MEMBER = "1";

   @RBEntry("Cannot add override:  the path's context is not valid.")
   public static final String OVERRIDE_CONTEXT_INVALID = "2";

   @RBEntry("Cannot add version as an override:  there is no version to override in the configuration.")
   public static final String NOTHING_TO_OVERRIDE = "3";

   @RBEntry("Cannot add version as an override:  it is the same as the normal version in the configuration.")
   public static final String OVERRIDE_SAME_AS_NORMAL = "4";

   @RBEntry("Cannot remove or replace override:  it is not an override for this configuration.")
   public static final String OVERRIDE_NOT_FOR_CONFIGURATION = "5";

   @RBEntry("Cannot add serial number mapping:  the path is not valid for this instance.")
   public static final String INVALID_PATH_FOR_ADD_MAPPING = "6";

   @RBEntry("Cannot remove or replace serial number mapping:  it is not a mapping for this instance.")
   public static final String MAPPING_NOT_FOR_INSTANCE = "7";

   @RBEntry("Cannot assign configuration:  it is not a configuration for this instance's product.")
   public static final String CONFIGURATION_NOT_FOR_INSTANCE = "8";

   @RBEntry("Cannot set a start incorporation date:  {0} has already been incorporated.")
   @RBArgComment0("Identity of the product instance.")
   public static final String ALREADY_INCORPORATED = "9";

   @RBEntry("Invalid start incorporation date:  start incorporation dates cannot be set to a date in the future.")
   public static final String INVALID_FUTURE_INCORPORATION_DATE = "10";

   @RBEntry("Invalid path:  it does not terminate at the product.")
   public static final String INVALID_PATH = "11";

   @RBEntry("{0} cannot be added to the configuration:  the version of a serial-numbered object is identified when the parent instance allocates a serial-numbered instance of it.")
   @RBArgComment0("Identity of invalid element.")
   public static final String INVALID_TYPE_FOR_CONFIGURATION = "12";

   @RBEntry("The class specified in the query spec is not valid; it must be valid for the instance role of the serialNumbered object.")
   @RBComment("This is a development-time exception (that is, something that shouldn't normally occur in a well-implemented system) for the server-side getUnmappedIncorporatedInstances.")
   public static final String INVALID_QUERYSPEC_CLASS_FOR_GET_UNMAPPED = "13";

   @RBEntry("{0} is allocated more than once.")
   @RBArgComment0("The identity of the over-allocated instance.")
   public static final String INSTANCE_ALLOCATED_MORE_THAN_ONCE = "14";

   @RBEntry("{0} -")
   @RBComment("The open-ended range for an allocated instance, i.e 1/1/2001 -")
   @RBArgComment0("The date this instance was allocated")
   public static final String ALLOCATED_AS_STRING_END_NULL = "15";

   @RBEntry("{0} - {1}")
   @RBComment("The range for an allocated instance, i.e 1/1/2001 - 2/1/2001")
   @RBArgComment0("The date this instance was allocated")
   @RBArgComment1("The date this instance was unallocated")
   public static final String ALLOCATED_AS_STRING = "16";

   @RBEntry("{0} cannot be added to the configuration:  it is not an instance of ConfigurationManageable.")
   @RBArgComment0("Identity of invalid element.")
   public static final String INVALID_CLASS_FOR_CONFIGURATION = "17";

   @RBEntry("{0} cannot be added to the configuration:  the configuration of this object is established when it is allocated by the parent instance.")
   @RBArgComment0("Identity of invalid element.")
   public static final String INVALID_CONFIGURED_VERSION_FOR_CONFIGURATION = "18";

   @RBEntry("{0} cannot be added to the configuration because it is currently in work; objects that are checked out or in a personal cabinet cannot be added to a configuration.")
   @RBArgComment0("Identity of invalid element.")
   public static final String INVALID_IN_WORK_VERSION_FOR_CONFIGURATION = "19";

   @RBEntry("You cannot modify configuration {0} because it has incorporated instances which use it.")
   @RBArgComment0("The identity of the configuration.")
   public static final String CONFIGURATION_FROZEN_BY_INSTANCE = "20";

   @RBEntry("{0} cannot be modified or deleted because it has been incorporated.  To modify this instance, create a new version of it.")
   @RBArgComment0("The identity of the instance.")
   public static final String INSTANCE_FROZEN = "21";

   @RBEntry("{0} cannot be deleted because it is being called out as an override in a configuration.")
   @RBArgComment0("The identity of the version.")
   public static final String CONFIGURATION_MANAGEABLE_VERSION_FROZEN = "22";

   @RBEntry("{0} cannot be deleted because it has been allocated into another instance.")
   @RBArgComment0("The identity of the instance.")
   public static final String INSTANCE_IFC_FROZEN = "23";

   @RBEntry("{0} cannot be incorporated because it does not have an associated configuration.")
   @RBArgComment0("The identity of the instance.")
   public static final String INSTANCE_HAS_NO_CONFIGURATION = "24";

   @RBEntry("{0} cannot be incorporated because the configuration is incomplete.  Versions of the following objects must be configured:{1}")
   @RBArgComment0("The identity of the instance.")
   @RBArgComment1("A newline-separated list of objects which starts with a newline.")
   public static final String INSTANCE_HAS_UNCONFIGURED_OBJECTS = "25";

   @RBEntry("{0} of {1} instances of {2}.")
   @RBArgComment0("The number of unmapped usages.")
   @RBArgComment1("The total number of usages of the object.")
   @RBArgComment2("The identity of the unmapped object.")
   public static final String UNMAPPED_USAGES = "26";

   @RBEntry("Any of the usages of {0} in {1}.")
   @RBArgComment0("The identity of the unmapped object.")
   @RBArgComment1("The identity of the object that uses this object.")
   public static final String NO_MAPPED_USAGES = "27";

   @RBEntry("{0} cannot be incorporated because the following have not been allocated:{1}")
   @RBArgComment0("The identity of the instance.")
   @RBArgComment1("A newline-separated list of objects which starts with a newline.")
   public static final String INSTANCE_NOT_FULLY_MAPPED = "28";

   @RBEntry("{0} cannot be incorporated because the following instances used by this instance have not been incorporated:{1}")
   @RBArgComment0("The identity of the instance.")
   @RBArgComment1("A newline-separated list of objects which starts with a newline.")
   public static final String MAPPED_INSTANCES_NOT_INCORPORATED = "29";

   @RBEntry("{0} is already incorporated into {1}.")
   @RBArgComment0("The identity of the instance that would be overly incorporated.")
   @RBArgComment1("The identity of the instance that incorporated {0}.")
   public static final String ALREADY_INCORPORATED_INTO = "30";

   @RBEntry("{0} cannot be incorporated until the following over-allocations are addressed:{1}")
   @RBArgComment0("The identity of the instance.")
   @RBArgComment1("A newline-separated list of 30's.")
   public static final String INSTANCES_OVERLY_INCORPORATED = "31";

   @RBEntry("Unable to populate {0} because no versions of it were selected by the config spec.")
   @RBArgComment0("The identity the master.")
   public static final String CONFIG_SPEC_SELECTED_NO_VERSIONS = "32";

   @RBEntry("{0} is already allocated by {1}.")
   @RBArgComment0("The identity of the instance that is allocated more than once.")
   @RBArgComment1("The identity of the instance that incorporated {0}.")
   public static final String ALREADY_ALLOCATED = "33";

   @RBEntry("The following errors occurred while attempting to add objects to the configuration:")
   public static final String MULTI_OBJECT_PRE_VALIDATE_MESSAGE = "34";

   @RBEntry("You cannot modify the following configurations because they have incorporated instances which use them.")
   public static final String CONFIGURATION_MANAGEABLE_VERSIONS_FROZEN = "35";

   @RBEntry("You cannot delete the following configurations because instances have been created which use them.")
   public static final String CONFIGURATIONS_FROZEN_BY_INSTANCES = "36";

   @RBEntry("You cannot delete configuration {0} because instances have been created which use it.")
   @RBArgComment0("The identity of the configuration.")
   public static final String CONFIGURATION_HAS_INSTANCE = "37";

   @RBEntry("The following cannot be deleted because they are being called out as overrides in configurations:")
   public static final String CONFIGURATIONS_HAVE_INSTANCES = "38";

   @RBEntry("Method {0} is not supported for basic configurations.")
   public static final String UNSUPPORTED_FOR_BASIC = "39";

   @RBEntry("{0} cannot be converted from basic mode to full.")
   @RBArgComment0("The identity of the configuration.")
   public static final String INVALID_CONVERSION_FROM_BASIC = "40";

   @RBEntry("Method {0} is not supported for full configurations.")
   @RBArgComment0("The unsupported method's name")
   public static final String UNSUPPORTED_FOR_FULL = "41";

   @RBEntry("A basic configuration cannot be repopulated.  You must create a new one.")
   public static final String REPOPULATE_UNSUPPORTED_FOR_BASIC = "42";

   @RBEntry("Failed to resolve navigated masters to iterations during basic populate.")
   public static final String BASIC_POPULATE_MISSING_ITERATIONS = "43";

   @RBEntry("Invalid end incorporation date:  end incorporation dates cannot be set to a date in the future.")
   public static final String INVALID_FUTURE_UNINCORPORATION_DATE = "44";

   @RBEntry("Invalid start/end incorporation dates:  the start incorporation date must precede the end incorporation date.")
   public static final String INVALID_INCORP_UNINCORP_DATES = "45";

   @RBEntry("Cannot undo the incorporation because the instance is in use.")
   public static final String CANNOT_UNDO_INCORPORATION_IN_USE = "46";

   @RBEntry("Cannot change incorporation date:  the new incorporation date overlaps with an existing version's incorporation range.")
   public static final String INCORPORATED_DATE_CHANGE_OVERLAPS = "47";

   @RBEntry("Cannot change end incorporation date:  the new end incorporation date overlaps with an existing version's start/end incorporation range.")
   public static final String UN_INCORPORATED_DATE_CHANGE_OVERLAPS = "48";

   @RBEntry("Cannot assign this end incorporation date:  the instance is in use by another incorporated instance.")
   public static final String CANNOT_LESSEN_UN_INCORPORATION_IN_USE = "49";

   @RBEntry("Another instance is already incorporated as of the date specified.  Redo its incorporation range or pick another date.")
   public static final String INCORPORATION_DATE_OVERLAPS = "50";

   @RBEntry("Invalid incorporation range:  the instance uses at least one instance that is not incorporated at some time in the new range.")
   public static final String CHILD_INSTANCE_NOT_INCORPORATED_IN_NEW_RANGE = "51";
}
