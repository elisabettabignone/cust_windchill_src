/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.configuration;

import wt.util.resource.*;

@RBUUID("wt.configuration.configurationResource")
public final class configurationResource_it extends WTListResourceBundle {
   @RBEntry("La specifica di configurazione ha selezionato più di una versione di {0}. La specifica di configurazione dovrebbe selezionare al massimo una versione per master.")
   @RBArgComment0("The identity the master.")
   public static final String CONFIG_SPEC_SELECTED_MULTIPLE_VERSIONS = "0";

   @RBEntry("Impossibile eliminare un membro che non esiste.")
   public static final String MEMBER_TO_REMOVE_NO_LONGER_MEMBER = "1";

   @RBEntry("Impossibile sovrascrivere: il contesto del percorso non è valido.")
   public static final String OVERRIDE_CONTEXT_INVALID = "2";

   @RBEntry("Impossibile aggiungere una versione con forzatura:  non esiste alcuna versione da forzare nella configurazione.")
   public static final String NOTHING_TO_OVERRIDE = "3";

   @RBEntry("Impossibile aggiungere la versione sovrascrivendo: è una versione uguale a quella normale della configurazione.")
   public static final String OVERRIDE_SAME_AS_NORMAL = "4";

   @RBEntry("Impossibile rimuovere o sostituire la sovrascrittura: non appartiene a questa configurazione.")
   public static final String OVERRIDE_NOT_FOR_CONFIGURATION = "5";

   @RBEntry("Impossibile aggiungere la mappatura dei numeri di serie: il percorso non è valido per questa istanza.")
   public static final String INVALID_PATH_FOR_ADD_MAPPING = "6";

   @RBEntry("Impossibile rimuovere o sostituire la mappatura dei numeri di serie: la mappatura non riguarda questa istanza.")
   public static final String MAPPING_NOT_FOR_INSTANCE = "7";

   @RBEntry("Impossibile assegnare la configurazione: non si tratta di una configurazione per il prodotto di questa istanza.")
   public static final String CONFIGURATION_NOT_FOR_INSTANCE = "8";

   @RBEntry("Impossibile definire la data di inizio incorporazione: {0} è già stato incorporato.")
   @RBArgComment0("Identity of the product instance.")
   public static final String ALREADY_INCORPORATED = "9";

   @RBEntry("Data di inizio incorporazione non valida: le date di incorporazione non possono essere date future.")
   public static final String INVALID_FUTURE_INCORPORATION_DATE = "10";

   @RBEntry("Percorso non valido: non conduce al prodotto.")
   public static final String INVALID_PATH = "11";

   @RBEntry("{0} non può essere aggiunto alla configurazione: la versione di un oggetto con numero di serie è identificata quando l'istanza padre alloca una sua istanza con numero di serie.")
   @RBArgComment0("Identity of invalid element.")
   public static final String INVALID_TYPE_FOR_CONFIGURATION = "12";

   @RBEntry("La classe specificata nella specifica d'interrogazione non è valida; deve essere valida per il ruolo d'istanza dell'oggetto serialNumbered.")
   @RBComment("This is a development-time exception (that is, something that shouldn't normally occur in a well-implemented system) for the server-side getUnmappedIncorporatedInstances.")
   public static final String INVALID_QUERYSPEC_CLASS_FOR_GET_UNMAPPED = "13";

   @RBEntry("{0} è allocato più di una volta.")
   @RBArgComment0("The identity of the over-allocated instance.")
   public static final String INSTANCE_ALLOCATED_MORE_THAN_ONCE = "14";

   @RBEntry("{0} -")
   @RBComment("The open-ended range for an allocated instance, i.e 1/1/2001 -")
   @RBArgComment0("The date this instance was allocated")
   public static final String ALLOCATED_AS_STRING_END_NULL = "15";

   @RBEntry("{0} - {1}")
   @RBComment("The range for an allocated instance, i.e 1/1/2001 - 2/1/2001")
   @RBArgComment0("The date this instance was allocated")
   @RBArgComment1("The date this instance was unallocated")
   public static final String ALLOCATED_AS_STRING = "16";

   @RBEntry("{0} non può essere aggiunto alla configurazione:  non è un'istanza di ConfigurationManageable.")
   @RBArgComment0("Identity of invalid element.")
   public static final String INVALID_CLASS_FOR_CONFIGURATION = "17";

   @RBEntry("{0} non può essere aggiunto alla configurazione: la configurazione di questo oggetto è stabilita quando viene allocata dall'istanza padre.")
   @RBArgComment0("Identity of invalid element.")
   public static final String INVALID_CONFIGURED_VERSION_FOR_CONFIGURATION = "18";

   @RBEntry("{0} non può essere aggiunto alla configurazione perché è in modifica; gli oggetti sottoposti a Check-Out o in uno schedario personale non possono essere aggiunti ad una configurazione.")
   @RBArgComment0("Identity of invalid element.")
   public static final String INVALID_IN_WORK_VERSION_FOR_CONFIGURATION = "19";

   @RBEntry("Impossibile modificare la configurazione {0} in quanto è utilizzata da istanze incorporate.")
   @RBArgComment0("The identity of the configuration.")
   public static final String CONFIGURATION_FROZEN_BY_INSTANCE = "20";

   @RBEntry("{0} non può essere modificato o eliminato perché è stato incorporato. Per modificare l'istanza, creare una nuova versione.")
   @RBArgComment0("The identity of the instance.")
   public static final String INSTANCE_FROZEN = "21";

   @RBEntry("{0} non può essere eliminato perché viene richiamato come elemento di sovrascrittura in una configurazione.")
   @RBArgComment0("The identity of the version.")
   public static final String CONFIGURATION_MANAGEABLE_VERSION_FROZEN = "22";

   @RBEntry("{0} non può essere eliminato perché è stato allocato ad un'altra istanza.")
   @RBArgComment0("The identity of the instance.")
   public static final String INSTANCE_IFC_FROZEN = "23";

   @RBEntry("{0} non può essere incorporato perché non ha una configurazione associata.")
   @RBArgComment0("The identity of the instance.")
   public static final String INSTANCE_HAS_NO_CONFIGURATION = "24";

   @RBEntry("{0} non può essere incorporato perché la configurazione non è completa. È necessario configurare le versioni degli oggetti seguenti: {1}")
   @RBArgComment0("The identity of the instance.")
   @RBArgComment1("A newline-separated list of objects which starts with a newline.")
   public static final String INSTANCE_HAS_UNCONFIGURED_OBJECTS = "25";

   @RBEntry("{0} di  {1} istanze di {2}.")
   @RBArgComment0("The number of unmapped usages.")
   @RBArgComment1("The total number of usages of the object.")
   @RBArgComment2("The identity of the unmapped object.")
   public static final String UNMAPPED_USAGES = "26";

   @RBEntry("Qualsiasi utilizzo di  {0} in {1}.")
   @RBArgComment0("The identity of the unmapped object.")
   @RBArgComment1("The identity of the object that uses this object.")
   public static final String NO_MAPPED_USAGES = "27";

   @RBEntry("{0} non può essere incorporato perché quanto segue non è ancora stato allocato: {1}")
   @RBArgComment0("The identity of the instance.")
   @RBArgComment1("A newline-separated list of objects which starts with a newline.")
   public static final String INSTANCE_NOT_FULLY_MAPPED = "28";

   @RBEntry("{0} non può essere incorporato perché le istanze seguenti usate dalla presente istanza non sono state incorporate: {1}")
   @RBArgComment0("The identity of the instance.")
   @RBArgComment1("A newline-separated list of objects which starts with a newline.")
   public static final String MAPPED_INSTANCES_NOT_INCORPORATED = "29";

   @RBEntry("{0} è già incorporato in {1}.")
   @RBArgComment0("The identity of the instance that would be overly incorporated.")
   @RBArgComment1("The identity of the instance that incorporated {0}.")
   public static final String ALREADY_INCORPORATED_INTO = "30";

   @RBEntry("{0} non può essere incorporato finché le allocazioni in eccesso verranno risolte: {1}")
   @RBArgComment0("The identity of the instance.")
   @RBArgComment1("A newline-separated list of 30's.")
   public static final String INSTANCES_OVERLY_INCORPORATED = "31";

   @RBEntry("Impossibile completare {0} perché nessuna versione è stata selezionata dalla specifica di config.")
   @RBArgComment0("The identity the master.")
   public static final String CONFIG_SPEC_SELECTED_NO_VERSIONS = "32";

   @RBEntry("{0} è allocato da {1}.")
   @RBArgComment0("The identity of the instance that is allocated more than once.")
   @RBArgComment1("The identity of the instance that incorporated {0}.")
   public static final String ALREADY_ALLOCATED = "33";

   @RBEntry("Si sono verificati i seguenti errori durante l'aggiunta di oggetti alla configurazione:")
   public static final String MULTI_OBJECT_PRE_VALIDATE_MESSAGE = "34";

   @RBEntry("Impossibile modificare le configurazioni che seguono in quanto sono utilizzate da istanze incorporate.")
   public static final String CONFIGURATION_MANAGEABLE_VERSIONS_FROZEN = "35";

   @RBEntry("Impossibile modificare le configurazioni che seguono in quanto sono state create istanze che le utilizzano.")
   public static final String CONFIGURATIONS_FROZEN_BY_INSTANCES = "36";

   @RBEntry("Impossibile eliminare la configurazione {0} in quanto sono state create istanze che la utilizzano.")
   @RBArgComment0("The identity of the configuration.")
   public static final String CONFIGURATION_HAS_INSTANCE = "37";

   @RBEntry("Impossibile eliminare gli elementi che seguono in quanto vengono richiamati come elementi di sovrascrittura nelle configurazioni:")
   public static final String CONFIGURATIONS_HAVE_INSTANCES = "38";

   @RBEntry("Il metodo {0} non è supportato per configurazioni di base.")
   public static final String UNSUPPORTED_FOR_BASIC = "39";

   @RBEntry("Impossibile convertire {0} dalla modalità di base a quella completa.")
   @RBArgComment0("The identity of the configuration.")
   public static final String INVALID_CONVERSION_FROM_BASIC = "40";

   @RBEntry("Il metodo {0} non è supportato per configurazioni complete.")
   @RBArgComment0("The unsupported method's name")
   public static final String UNSUPPORTED_FOR_FULL = "41";

   @RBEntry("Impossibile ricompletare le configurazioni di base. Crearne una nuova.")
   public static final String REPOPULATE_UNSUPPORTED_FOR_BASIC = "42";

   @RBEntry("Impossibile risolvere i master cercati alle iterazioni durante il completamento di base.")
   public static final String BASIC_POPULATE_MISSING_ITERATIONS = "43";

   @RBEntry("Data di fine incorporazione non valida: le date di fine incorporazione non possono essere date future.")
   public static final String INVALID_FUTURE_UNINCORPORATION_DATE = "44";

   @RBEntry("Date di avvio/fine incorporazione non valide: la data di inizio incorporazione deve precedere quella di fine incorporazione.")
   public static final String INVALID_INCORP_UNINCORP_DATES = "45";

   @RBEntry("Impossibile annullare l'incorporazione: l'istanza è in uso.")
   public static final String CANNOT_UNDO_INCORPORATION_IN_USE = "46";

   @RBEntry("Impossibile modificare la data di incorporazione: la nuova data di incorporazione rientra nell'intervallo di incorporazione di una versione esistente.")
   public static final String INCORPORATED_DATE_CHANGE_OVERLAPS = "47";

   @RBEntry("Impossibile modificare la data di fine incorporazione: la nuova data di fine incorporazione rientra nell'intervallo di incorporazione di una versione esistente.")
   public static final String UN_INCORPORATED_DATE_CHANGE_OVERLAPS = "48";

   @RBEntry("Impossibile assegnare la data di fine incorporazione: l'istanza è utilizzata da un'altra istanza incorporata successiva a questa data.")
   public static final String CANNOT_LESSEN_UN_INCORPORATION_IN_USE = "49";

   @RBEntry("Esiste già un'istanza incorporata per la data specificata. Modificare l'intervallo di incorporazione o scegliere una data differente.")
   public static final String INCORPORATION_DATE_OVERLAPS = "50";

   @RBEntry("Intervallo di incorporazione non valido: l'istanza usa almeno un'istanza non incorporata in un momento del nuovo intervallo.")
   public static final String CHILD_INSTANCE_NOT_INCORPORATED_IN_NEW_RANGE = "51";
}
