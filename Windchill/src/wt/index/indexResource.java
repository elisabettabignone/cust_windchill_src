/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.index;

import wt.util.resource.*;

@RBUUID("wt.index.indexResource")
public final class indexResource extends WTListResourceBundle {
   @RBEntry("The operation:   \"{0}\" failed.")
   public static final String OPERATION_FAILED = "0";

   @RBEntry("Cannot create index policy list: bad parameters.")
   public static final String BAD_LIST_SELECTOR = "1";

   @RBEntry("Cannot create index policy rule: bad parameters.")
   public static final String BAD_RULE_SELECTOR = "2";

   @RBEntry("Cannot queue entry: no administrator user.")
   public static final String MISSING_ADMINISTRATOR = "3";

   @RBEntry("Cannot find indexing queue.")
   public static final String MISSING_QUEUE = "4";

   @RBEntry("Cannot read index policy (possibly lack of authorization).")
   public static final String MISSING_POLICY = "5";

   @RBEntry("Cannot refresh object")
   public static final String PROBLEM_REFRESHING_OBJ = "6";

   @RBEntry("Error trying to initialize the index accessor")
   public static final String BAD_IDX_ACCESS = "7";

   @RBEntry("Failed to index {0}")
   public static final String INDEX_FAILURE = "9";

   @RBEntry("Cannot get indexer for collection with name {0}")
   public static final String CANNOT_GET_INDEX = "10";

   @RBEntry("The operation {0} is not supported")
   public static final String OP_NOT_SUPPORTED = "11";

   @RBEntry("The collection {0} does not exist")
   public static final String COL_DOES_NOT_EXIST = "12";

   @RBEntry("You need to specify which Index to use")
   public static final String INDEX_OBJ_IS_NULL = "13";

   @RBEntry("You need to specify which object to Index")
   public static final String OBJ_IS_NULL = "14";

   @RBEntry("The object passed is not indexable")
   public static final String OBJ_NOT_INDEXABLE = "15";

   @RBEntry("Principal {0} has been disabled")
   public static final String PRINCIPAL_DISABLED = "16";

   @RBEntry("An indexing rule is already defined for Domain: \"{0}\", Type: \"{1}\", State: \"{2}\", Collections: \"{3}\".")
   @RBComment("PolicyRuleAlreadyExistsException: An attempt was made to create an indexing rule that already exists")
   @RBArgComment0("Path name of the domain the rule applies to")
   @RBArgComment1("External type identifier of the type in the rule")
   @RBArgComment2("State name (values defined in wt.lifecycle.StateRB) of the state in the rule")
   @RBArgComment3("List of collections in the existing rule")
   public static final String RULE_ALREADY_EXISTS = "17";

   @RBEntry("There was an error while communicating with RetrievalWare.  Please inform your administrator of the time of this error and the action being performed.")
   @RBComment("Some error was received from Rware while indexing or deleting an index.  This is meant to generalize that error for indexing.")
   public static final String RWARE_ERROR = "18";
}
