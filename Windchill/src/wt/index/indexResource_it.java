/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.index;

import wt.util.resource.*;

@RBUUID("wt.index.indexResource")
public final class indexResource_it extends WTListResourceBundle {
   @RBEntry("L'operazione \"{0}\" non è riuscita.")
   public static final String OPERATION_FAILED = "0";

   @RBEntry("Impossibile creare l'elenco d'indicizzazione. Parametri non validi.")
   public static final String BAD_LIST_SELECTOR = "1";

   @RBEntry("Impossibile creare la regola d'indicizzazione. Parametri non validi.")
   public static final String BAD_RULE_SELECTOR = "2";

   @RBEntry("Impossibile inserire l'elemento nella coda: utente amministratore mancante.")
   public static final String MISSING_ADMINISTRATOR = "3";

   @RBEntry("Impossibile trovare la coda di indicizzazione.")
   public static final String MISSING_QUEUE = "4";

   @RBEntry("Impossibile leggere la regola d'indicizzazione. Possibile mancanza di autorizzazione.")
   public static final String MISSING_POLICY = "5";

   @RBEntry("Impossibile aggiornare oggetto")
   public static final String PROBLEM_REFRESHING_OBJ = "6";

   @RBEntry("Errore nell'inizializzazione dell'accesso all'indice")
   public static final String BAD_IDX_ACCESS = "7";

   @RBEntry("Indicizzazione di {0} non riuscita")
   public static final String INDEX_FAILURE = "9";

   @RBEntry("Impossibile ottenere indicizzatore per raccolta con nome {0}")
   public static final String CANNOT_GET_INDEX = "10";

   @RBEntry("L'operazione {0} non è supportata")
   public static final String OP_NOT_SUPPORTED = "11";

   @RBEntry("La raccolta {0} è inesistente")
   public static final String COL_DOES_NOT_EXIST = "12";

   @RBEntry("Specificare l'indice da utilizzare")
   public static final String INDEX_OBJ_IS_NULL = "13";

   @RBEntry("Specificare l'oggetto da indicizzare")
   public static final String OBJ_IS_NULL = "14";

   @RBEntry("L'oggetto trasferito non è indicizzabile")
   public static final String OBJ_NOT_INDEXABLE = "15";

   @RBEntry("L'utente/gruppo/ruolo {0} non è più abilitato")
   public static final String PRINCIPAL_DISABLED = "16";

   @RBEntry("Una regola di indicizzazione è già stata definita per il dominio: \"{0}\", tipo: \"{1}\", stato del ciclo di vita: \"{2}\", raccolte: \"{3}\".")
   @RBComment("PolicyRuleAlreadyExistsException: An attempt was made to create an indexing rule that already exists")
   @RBArgComment0("Path name of the domain the rule applies to")
   @RBArgComment1("External type identifier of the type in the rule")
   @RBArgComment2("State name (values defined in wt.lifecycle.StateRB) of the state in the rule")
   @RBArgComment3("List of collections in the existing rule")
   public static final String RULE_ALREADY_EXISTS = "17";

   @RBEntry("Errore durante la comunicazione con RetrievalWare. Comunicare all'amministratore l'ora a cui si è verificato l'errore e l'azione che si stava eseguendo.")
   @RBComment("Some error was received from Rware while indexing or deleting an index.  This is meant to generalize that error for indexing.")
   public static final String RWARE_ERROR = "18";
}
