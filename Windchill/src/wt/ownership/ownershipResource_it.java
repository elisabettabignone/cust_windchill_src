/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.ownership;

import wt.util.resource.*;

@RBUUID("wt.ownership.ownershipResource")
public final class ownershipResource_it extends WTListResourceBundle {
   @RBEntry("È stata assegnata la proprietà dell'oggetto. Messaggio di sistema:")
   public static final String PRIVATE_CONSTANT_0 = "0";

   @RBEntry("L'oggetto ha già un proprietario. Impossibile acquisirne la proprietà.")
   @RBComment("Message stating that the user cannot assume ownership of the object because it is currently owned by another user.")
   public static final String ASSIGN_NOT_ALLOWED = "1";

   @RBEntry("L'oggetto appartiene a un altro proprietario. Potrebbe non essere possibile rilasciarne la proprietà.")
   @RBComment("Message stating that the user may not release ownership of the object because it is owned by another user.")
   public static final String RELEASE_NOT_ALLOWED = "2";

   @RBEntry("Impossibile utilizzare il metodo setOwner su un oggetto già persistente. Errore di programmazione")
   @RBComment("Message stating that the setOwner operation is not allowed on an already persisted object.")
   public static final String SET_NOT_ALLOWED = "3";

   @RBEntry("Il valore del parametro \"{0}\" non può essere nullo.")
   @RBComment("WTInvalidParameterException: A null value is passed to a method for a required parameter.")
   @RBArgComment0("Name of parameter")
   public static final String NULL_PARAMETER = "4";

   @RBEntry("L'oggetto \"{0}\" ha già un proprietario. Impossibile acquisirne la proprietà.")
   @RBComment("Message stating that the user cannot assume ownership of the specified object because it is currently owned by another user.")
   public static final String OBJECT_ASSIGN_NOT_ALLOWED = "5";
   
   @RBEntry("L'utente/gruppo/ruolo che si sta tentando di assegnare come proprietario non è supportato.")
   @RBComment("Message stating that the principal type you are attempting to assign as owner of an object is not supported.")
   public static final String OBJECT_ASSIGN_TO_PRINCIPAL_NOT_ALLOWED = "6";
   
   @RBEntry("L'oggetto a cui si sta tentando di assegnare un proprietario non è persistente.")
   @RBComment("Message stating that the object you are attempting to assign an owner to is currenty not persisted.")
   public static final String OBJECT_NOT_PERSISTED = "7";
}
