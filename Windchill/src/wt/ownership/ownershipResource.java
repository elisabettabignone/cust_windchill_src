/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.ownership;

import wt.util.resource.*;

@RBUUID("wt.ownership.ownershipResource")
public final class ownershipResource extends WTListResourceBundle {
   @RBEntry("An ownership occurred. System message follows:")
   public static final String PRIVATE_CONSTANT_0 = "0";

   @RBEntry("You may not become the object owner because it is already owned.")
   @RBComment("Message stating that the user cannot assume ownership of the object because it is currently owned by another user.")
   public static final String ASSIGN_NOT_ALLOWED = "1";

   @RBEntry("You may not release the ownership of the object because you are not it's owner.")
   @RBComment("Message stating that the user may not release ownership of the object because it is owned by another user.")
   public static final String RELEASE_NOT_ALLOWED = "2";

   @RBEntry("setOwner is not allowed on an already Persisted object. Programming error")
   @RBComment("Message stating that the setOwner operation is not allowed on an already persisted object.")
   public static final String SET_NOT_ALLOWED = "3";

   @RBEntry("The \"{0}\" parameter value cannot be null.")
   @RBComment("WTInvalidParameterException: A null value is passed to a method for a required parameter.")
   @RBArgComment0("Name of parameter")
   public static final String NULL_PARAMETER = "4";

   @RBEntry("You may not become the owner of object \"{0}\" because it is already owned.")
   @RBComment("Message stating that the user cannot assume ownership of the specified object because it is currently owned by another user.")
   public static final String OBJECT_ASSIGN_NOT_ALLOWED = "5";
   
   @RBEntry("The principal type you are trying to assign as owner is not supported.")
   @RBComment("Message stating that the principal type you are attempting to assign as owner of an object is not supported.")
   public static final String OBJECT_ASSIGN_TO_PRINCIPAL_NOT_ALLOWED = "6";
   
   @RBEntry("The object you are trying to assign ownership to is not persisted.")
   @RBComment("Message stating that the object you are attempting to assign an owner to is currenty not persisted.")
   public static final String OBJECT_NOT_PERSISTED = "7";
}
