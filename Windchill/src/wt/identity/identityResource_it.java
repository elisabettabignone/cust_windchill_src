/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.identity;

import wt.util.resource.*;

@RBUUID("wt.identity.identityResource")
public final class identityResource_it extends WTListResourceBundle {
   @RBEntry("{0} - {1}")
   @RBComment("IDENTITY - format is <type> - <id>")
   @RBArgComment0("type, eg \"Document\" OR \"Part etc.")
   @RBArgComment1(" identifier, eg \"123, Demo Organization\" OR \"123\" OR \"123, A.2 (Design)\" etc.")
   public static final String IDENTITY = "1";

   @RBEntry("{0}")
   @RBComment("VALUE")
   @RBArgComment0("Any old value to go into a message -- no localization needed.")
   public static final String VALUE = "2";

   @RBEntry("{0} ({1}) - {2}")
   @RBComment("NUMBERNAME - should not be used by X10")
   @RBArgComment0("number value")
   @RBArgComment1("orgUniqueIdentifier value")
   @RBArgComment2("name value")
   public static final String NUMBERNAME = "3";

   @RBEntry("{0}, {1}")
   @RBComment("Format is <id of master>, <version> eg 123, A.2 (Design) OR 123, Demo Organization, A.2 (Design)")
   @RBArgComment0("the identifier of the Master. eg 123 OR 123, Demo Organization")
   @RBArgComment1("the identifier of the Version. eg A.2 OR A.2 (Design)")
   public static final String VERSIONED_IDENTIFIER = "4";

   @RBEntry("Rev")
   @RBComment("Displayable string for a revision -- short version")
   public static final String REVISION_BRIEF = "5";

   @RBEntry("Revisione")
   @RBComment("Displayable string for a revision -- long version")
   public static final String REVISION_FULL = "6";

   @RBEntry(" ")
   @RBPseudo(false)
   @RBComment("The value to show when a Revision has not yet been set, ie before object is saved.  The default is an empty string.")
   public static final String REVISION_NULL = "7";

   @RBEntry("{0} {1}")
   @RBComment("identity to show for a revision.")
   @RBArgComment0("the type, eg a value from REVISION_BRIEF or REVISION_FULL")
   @RBArgComment1("the identifier for the rev, eg A, B, A.A")
   public static final String REVISION_IDENTITY = "8";

   @RBEntry("{0}")
   @RBComment("use when there is no number - should not be used in X10")
   @RBArgComment0("name value")
   public static final String NUMBERNAME_NUMBER_NULL = "9";

   @RBEntry("{0}, {1}")
   @RBComment("use when there is no name. eg 123, Demo Organization")
   @RBArgComment0("number value")
   @RBArgComment1("orgUniqueIdentifier value")
   public static final String NUMBERNAME_NAME_NULL = "10";

   @RBEntry("()")
   @RBComment("use when number and name are null")
   public static final String NUMBERNAME_BOTH_NULL = "11";

   @RBEntry("Visualizza")
   public static final String VIEW = "12";

   @RBEntry(" ")
   @RBPseudo(false)
   @RBComment("The value to show when no view has not yet been set, ie before object is saved.  The default is an empty string.")
   public static final String VIEW_NULL = "13";

   @RBEntry("{1} {0}")
   @RBComment("VIEW_IDENTITY - identity to show for a view")
   @RBArgComment0("{0} - the type, eg a value from VIEW")
   @RBArgComment1("{1} - the identifier for the rev, eg manufacturing, engineering, etc.")
   public static final String VIEW_IDENTITY = "14";

   @RBEntry("{0} ({1})")
   @RBComment("REVISION_VIEW_TYPE - Describes type for a revisable view managed version  (don't expect this one to be used much if at all)")
   @RBArgComment0("{0} - revision type, eg \"Rev \"")
   @RBArgComment1("{1} - view type, eg \"View\"")
   public static final String REVISION_VIEW_TYPE = "15";

   @RBEntry("{0} ({1})")
   @RBComment("REVISION_VIEW_IDENTIFIER - identification for revisable view managed version")
   @RBArgComment0("{0} - revision identifier, eg \"A\"")
   @RBArgComment1("{1} - view identifier, eg \"Manufacturing\"")
   public static final String REVISION_VIEW_IDENTIFIER = "16";

   @RBEntry("{0}")
   @RBComment("REVISION_VIEW_IDENTIFIER_NULL_VIEW - identification for revisable view managed version where view is null")
   @RBArgComment0("{0} - revision identifier, eg \"A\"")
   public static final String REVISION_VIEW_IDENTIFIER_NULL_VIEW = "17";

   @RBEntry("{0} {1}")
   @RBComment("REVISION_VIEW_IDENTITY - identity for revisable view managed version")
   @RBArgComment0("{0} - revision identity, eg \"Rev A\"")
   @RBArgComment1("{1} - view identity, eg \"Manufacturing View\"")
   public static final String REVISION_VIEW_IDENTITY = "18";

   @RBEntry("{0}-{1}")
   @RBComment("REVISION_PLUS_ONE_OFF_IDENTIFIER - identifiation for a one off version")
   @RBArgComment0("{0} - the revision identifier, eg \"A\"")
   @RBArgComment1("{1} - the one off version identifier, eg \"1\"")
   public static final String REVISION_PLUS_ONE_OFF_IDENTIFIER = "19";

   @RBEntry("{0}")
   @RBComment("use when there is no name and no organization unique identifier")
   @RBArgComment0("number value")
   public static final String NUMBERNAME_NAME_ORGID_NULL = "20";

   @RBEntry("{0} - {1}")
   @RBComment("use when there is no name and no organization unique identifier.")
   @RBArgComment0("number value")
   @RBArgComment1("name value")
   public static final String NUMBERNAME_ORGID_NULL = "21";

   @RBEntry("-Informazioni protette-")
   @RBComment("Displayable string for secured information.")
   public static final String SECURED_INFO = "22";

   @RBEntry("{0}.{1}")
   @RBComment("Concatenation of the version and iteration identifiers")
   @RBArgComment0("the version/revision identifier, \"A\"")
   @RBArgComment1("the iteration identifier, \"1\"")
   public static final String VERSION_PLUS_ITERATION = "23";

   @RBEntry(" ")
   @RBComment("The value to show when a version has not yet been set, ie before object is saved.  The default is an empty string.")
   public static final String VERSION_NULL = "24";

   @RBEntry("Ver")
   @RBComment("Displayable string for a version -- short version")
   public static final String VERSION_BRIEF = "25";

   @RBEntry("{0} {1}")
   @RBComment("identity to show for a version.")
   @RBArgComment0("the type, eg a value from VERSION_BRIEF or VERSION_FULL")
   @RBArgComment1("the identifier for the version, eg A.1, B.2, A.A.4")
   public static final String VERSION_IDENTITY = "26";

   @RBEntry("{0}-{1}.{2}")
   @RBComment("identifiation for a one off version (A-1.1)")
   @RBArgComment0("the revision identifier, eg \"A\"")
   @RBArgComment1("the one off version identifier, eg \"1\"")
   @RBArgComment2("the iteration identifier, eg \"1\"")
   public static final String VERSION_PLUS_ONE_OFF_PLUS_ITERATION = "27";

   @RBEntry("{0}-{1}")
   @RBComment("identifiation for a one off version (A-1)")
   @RBArgComment0("the revision identifier, eg \"A\"")
   @RBArgComment1("the one off version identifier, eg \"1\"")
   public static final String VERSION_PLUS_ONE_OFF = "28";

   @RBEntry("{0}.{1}")
   @RBComment("identifiation for a one off version (1.1)")
   @RBArgComment0("the one off version identifier, eg \"1\"")
   @RBArgComment1("the iteration identifier, eg \"1\"")
   public static final String ONE_OFF_PLUS_ITERATION = "29";

   @RBEntry("{0}, {1}: {2}")
   @RBComment("use when there is no name")
   @RBArgComment0("number value")
   @RBArgComment1("orgUniqueIdentifier label. eg \"Organization Id\" OR \"CAGE Code\"")
   @RBArgComment2("orgUniqueIdentifier value")
   public static final String NUMBERNAME_NAME_NULL_LABELS = "30";

   @RBEntry("{0}, Versione: {1}")
   @RBComment("shows the identifier for a versioned object, eg. 123, Organization ID:Demo Organization, Version:A.2 (Design)")
   @RBArgComment0("the identifier of the Master. eg. 123, Organization ID:Demo Organization")
   @RBArgComment1("the label for version e.g. \"Version\"")
   @RBArgComment2("the identifier of the Version. eg. A.2 (Design)")
   public static final String VERSIONED_IDENTIFIER_LABELS = "31";

   @RBEntry("{0} - {1}")
   @RBComment("VALUE_LABELS")
   @RBArgComment0("Any old value to go into a message -- no localization needed.")
   public static final String VALUE_LABELS = "32";

   @RBEntry("{0}, {1}: {2}")
   @RBComment("same as NUMBERNAME_NAME_NULL_LABELS")
   @RBArgComment0("the identifier of the Master. eg. 123, Organization ID:Demo Organization")
   @RBArgComment1("orgUniqueIdentifier label. eg \"Organization Id\" OR \"CAGE Code\"")
   @RBArgComment2("orgUniqueIdentifier value")
   public static final String NUMBERNAME_NAME_NULL_LABELS_CAGE = "33";

   @RBEntry("{0}")
   @RBComment("use when there is no name and no organization unique identifier")
   @RBArgComment0("name value")
   public static final String NUMBERNAME_NUMBER_ORGID_NULL = "34";

   @RBEntry("{0}, {1}: {2}")
   @RBComment("use when there is no name")
   @RBArgComment0("name value")
   @RBArgComment1("orgUniqueIdentifier label. eg \"Organization Id\" OR \"CAGE Code\"")
   @RBArgComment2("orgUniqueIdentifier value")
   public static final String NUMBERNAME_NUMBER_NULL_LABELS = "35";

   @RBEntry("{0}, {1}: {2}")
   @RBComment("use when there is no number")
   @RBArgComment0("the identifier of the Master. eg. 123, Organization ID:Demo Organization")
   @RBArgComment1("orgUniqueIdentifier label. eg \"Organization Id\" OR \"CAGE Code\"")
   @RBArgComment2("orgUniqueIdentifier value")
   public static final String NUMBERNAME_NUMBER_NULL_LABELS_CAGE = "36";

   @RBEntry("{0} ({1})")
   @RBComment("used to display when no Revision")
   @RBArgComment0("displayIdentifier")
   @RBArgComment1("name value")
   public static final String NUMBER_NAME_TYPE = "37";

   @RBEntry("()")
   @RBComment("use when number is null (irrespective of org is null or not)")
   public static final String NUMBERORG_BOTH_NULL = "38";

   @RBEntry("{0}")
   @RBComment("use when number is there but org is null")
   public static final String NUMBERORG_ORG_NULL = "39";

   @RBEntry("{0}, {1}")
   @RBComment("use when number and org both are known")
   @RBArgComment0("number value")
   @RBArgComment1("orgUniqueIdentifier value")
   public static final String NUMBERORG = "40";

   @RBEntry("{0}, {1}:{2}")
   @RBComment("use when number and org both are known")
   @RBArgComment0("number value")
   @RBArgComment1("org label")
   @RBArgComment2("orgUniqueIdentifier value")
   public static final String NUMBERORG_LABELS = "41";

   @RBEntry("{0}, revisione: {1}")
   @RBComment("shows the identifier for a revisioned object, eg. 123, Organization ID:Demo Organization, Revision: A")
   @RBArgComment0("the identifier of the Master. eg. 123, Organization ID:Demo Organization")
   @RBArgComment1("the label for version e.g. \"Revision\"")
   @RBArgComment2("the identifier of the Revision. eg. A")
   public static final String REVISIONED_IDENTIFIER_LABELS = "42";

   @RBEntry("Profilo utente")
   @RBComment("shows the display type of a Profile")
   public static final String GROUP_PROFILE = "43";

   @RBEntry("{0}, {1}")
   @RBArgComment0("View Value")
   @RBArgComment1("Variation1/Variation2 Value")
   public static final String VIEW_VARIATION_IDENTITY = "44";

   @RBEntry("{0}, {1}, {2}")
   @RBArgComment0("View Value")
   @RBArgComment1("Variation1 Value")
   @RBArgComment2("Variation2 Value")
   public static final String VIEW_VARIATIONS_IDENTITY = "45";

   @RBEntry("{0}, {1}")
   @RBArgComment0("Number value")
   @RBArgComment1("Name value")
   public static final String NUMBER_NAME = "46";

   @RBEntry("{0}, {1}, {2}")
   @RBArgComment0("Number value")
   @RBArgComment1("Name value")
   @RBArgComment2("OrgUniqueIdentifier value")
   public static final String NUMBER_NAME_ORG = "47";

   @RBEntry("{0}, {1}, {2}:{3}")
   @RBArgComment0("Number value")
   @RBArgComment1("Name value")
   @RBArgComment2("org label")
   @RBArgComment3("OrgUniqueIdentifier value")
   public static final String NUMBER_NAME_ORG_LABEL = "48";
}
