/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.visitor;

import wt.util.resource.*;

@RBUUID("wt.visitor.visitorResource")
public final class visitorResource_it extends WTListResourceBundle {
   @RBEntry("Il percorso\n\n{0}\nè ricorsivo.")
   @RBArgComment0("An indented list representing the recursive path composed of identities, one per line.")
   public static final String RECURSION_DETECTED = "0";
}
