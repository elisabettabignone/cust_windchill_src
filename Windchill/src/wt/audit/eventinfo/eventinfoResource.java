/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.audit.eventinfo;

import wt.util.resource.*;

@RBUUID("wt.audit.eventinfo.eventinfoResource")
public final class eventinfoResource extends WTListResourceBundle {
   /**
    * The following string are the display names of the attribute labels for audit report
    **/
   @RBEntry("Old Identity:")
   @RBComment("This string is for ChangeIdentityEventInfo")
   public static final String OLD_IDENTITY = "00";

   @RBEntry("Old Life Cycle State:")
   @RBComment("This string is for ChangeLifecycleStateEventInfo")
   public static final String OLD_LIFECYCLE_STATE = "10";

   @RBEntry("Old Iteration Identity:")
   @RBComment("This string is for CheckinEventInfo")
   public static final String OLD_ITERATION_IDENTITY = "20";

   @RBEntry("Working Copy Folder Path:")
   @RBComment("This string is for CheckoutEventInfo")
   public static final String WORKING_COPY_FOLDER_PATH = "30";

   @RBEntry("New Object:")
   @RBComment("This string is for CopyEventInfo")
   public static final String NEW_OBJECT = "40";

   @RBEntry("Download Filename:")
   @RBComment("The filename for downloading. It is for DownloadEventInfo")
   public static final String DOWNLOAD_FILENAME = "50";

   @RBEntry("Markup:")
   @RBComment("This string is for MarkupOrAnnotationIdentity")
   public static final String MARKUP_OR_ANNOTATION = "60";

   @RBEntry("Participant:")
   @RBComment("This string is for ModifyAccessPolicyEventInfo")
   public static final String PARTICIPANT_LABEL = "70";

   @RBEntry("Permissions Granted:")
   @RBComment("This string is for ModifyAccessPolicyEventInfo")
   public static final String PERMISSIONS_GRANTED = "71";

   @RBEntry("Permissions Denied:")
   @RBComment("This string is for ModifyAccessPolicyEventInfo")
   public static final String PERMISSIONS_DENIED = "72";

   @RBEntry("Life Cycle State:")
   @RBComment("This string is for ModifyAccessPolicyEventInfo")
   public static final String LIFECYCLE_STATE = "73";

   @RBEntry("Object Type:")
   @RBComment("This string is for ModifyAccessPolicyEventInfo")
   public static final String OBJECT_TYPE = "74";

   @RBEntry("Content Name Added:")
   @RBComment("This string is for ModifyContentEventInfo")
   public static final String CONTENT_NAME_ADDED = "80";

   @RBEntry("Content Name Removed:")
   @RBComment("This string is for ModifyContentEventInfo")
   public static final String CONTENT_NAME_REMOVED = "81";

   @RBEntry("Permission Type:")
   @RBComment("This string is for ModifyObjectAccessEventInfo")
   public static final String PERMISSION_TYPE = "90";

   @RBEntry("Ad hoc")
   @RBComment("This string is for ModifyObjectAccessEventInfo")
   public static final String ADHOC_CHANGE = "91";

   @RBEntry("Domain")
   @RBComment("This string is for ModifyObjectAccessEventInfo")
   public static final String DOMAIN_CHANGE = "92";

   @RBEntry("Ad hoc and Domain")
   @RBComment("This string is for ModifyObjectAccessEventInfo")
   public static final String ADHOC_DOMAIN_CHANGE = "93";

   @RBEntry("Ad hoc Access Control List:")
   @RBComment("This string is for ModifyObjectAccessEventInfo")
   public static final String ADHOC_ACLENTRYSET = "94";

   @RBEntry("Policy Access Control List:")
   @RBComment("This string is for ModifyObjectAccessEventInfo")
   public static final String POLICY_ACLENTRYSET = "95";

   @RBEntry("Child Added:")
   @RBComment("This string is for ModifyProStructureEventInfo")
   public static final String CHILD_ADDED = "100";

   @RBEntry("Child Removed:")
   @RBComment("This string is for ModifyProStructureEventInfo")
   public static final String CHILD_REMOVED = "101";

   @RBEntry("Child Quantity Changed:")
   @RBComment("This string is for ModifyProStructureEventInfo")
   public static final String CHILD_QUANTITY_CHANGED = "102";

   @RBEntry("Participants Added:")
   @RBComment("This string is for ModifyGroupEventInfo and ModifyTeamEventInfo")
   public static final String PRINCIPAL_ADDED = "110";

   @RBEntry("Participants Removed:")
   @RBComment("This string is for ModifyGroupEventInfo and ModifyTeamEventInfo")
   public static final String PRINCIPAL_REMOVED = "111";

   @RBEntry("Role:")
   @RBComment("This string is for ModifyTeamEventInfo")
   public static final String ROLE_LABEL = "112";

   @RBEntry("Member:")
   @RBComment("This string is for ModifyTeamRoleEventInfo")
   public static final String MEMBER_LABEL = "120";

   @RBEntry("From Folder Path:")
   @RBComment("This string is for MoveEventInfo")
   public static final String FROM_FOLDER_PATH = "130";

   @RBEntry("Permission:")
   @RBComment("This string is for NoAuthorizedAccessEventInfo")
   public static final String PERMISSION_LABEL = "140";

   @RBEntry("Message:")
   @RBComment("This string is for NoAuthorizedAccessEventInfo")
   public static final String MESSAGE_LABEL = "141";

   @RBEntry("Search Criteria:")
   @RBComment("This string is for SearchEventInfo")
   public static final String SEARCH_CRITERIA = "150";

   @RBEntry("To Folder Path:")
   @RBComment("This string is for ShareEventInfo")
   public static final String TO_FOLDER_PATH = "160";

   @RBEntry("Concurrency Users:")
   @RBComment("This string is for UserSessionEventInfo")
   public static final String CONCURRENCY_USERS = "170";

   @RBEntry("Old Version:")
   @RBComment("This string is for VersionEventInfo")
   public static final String OLD_VERSION = "180";

   @RBEntry("Representation:")
   @RBComment("This string is for ViewRepresentationsEventInfo")
   public static final String REPRESENTATION_NAME = "190";

   @RBEntry("File '{0}' of Representation '{1}' sent to printer '{2}'")
   @RBComment("This string is for SentToPrintEventInfo")
   @RBArgComment0("name of the file that was sent to print")
   @RBArgComment1("name of the Representation whose file was sent to print")
   @RBArgComment2("name of the printer that the representation file was sent to")
   public static final String REPRESENTATION_FILE_SENT_TO_PRINT = "REPRESENTATION_FILE_SENT_TO_PRINT";
   
   @RBEntry("File '{0}' sent to printer '{1}'")
   @RBComment("This string is for SentToPrintEventInfo")
   @RBArgComment0("name of the file that was sent to print")
   @RBArgComment1("name of the printer that the representation file was sent to")
   public static final String FILE_SENT_TO_PRINT = "FILE_SENT_TO_PRINT";

   @RBEntry("Unknown")
   @RBComment("This string is in Event Specific Data to indicate an unknown object")
   public static final String UNKNOWN = "UNKNOWN";

   @RBEntry("Name:")
   @RBComment("This string is for WfVariableEventInfo")
   public static final String NAME_LABEL = "200";

   @RBEntry("Type:")
   @RBComment("This string is for WfVariableEventInfo")
   public static final String TYPE_LABEL = "201";

   @RBEntry("Value:")
   @RBComment("This string is for WfVariableEventInfo")
   public static final String VALUE_LABEL = "202";

   @RBEntry("Old Security Labels:")
   @RBComment("This string is for ModifySecurityLabelsEventInfo")
   public static final String OLD_SECURITY_LABELS = "210";

   @RBEntry("Part Identities:")
   @RBComment("This string is for AssociateDisassociateEventInfo")
   public static final String PART_INFO = "220";

   @RBEntry("All Except Participant:")
   @RBComment("This string is for ModifyAccessPolicyEventInfo")
   public static final String ALL_EXCEPT_PARTICIPANT_LABEL = "ALL_EXCEPT_PARTICIPANT_LABEL";

   @RBEntry("Permissions:")
   @RBComment("This string is for ModifyAccessPolicyEventInfo")
   public static final String PERMISSIONS_LABEL = "PERMISSIONS_LABEL";

   @RBEntry("Permissions Absolutely Denied:")
   @RBComment("This string is for ModifyAccessPolicyEventInfo")
   public static final String PERMISSIONS_ABSOLUTELY_DENIED = "PERMISSIONS_ABSOLUTELY_DENIED";

   @RBEntry("Request ID:")
   @RBComment("This string is for CSRFEventInfo")
   public static final String REQUEST_ID = "REQUEST_ID";

   @RBEntry("Request URI:")
   @RBComment("This string is for CSRFEventInfo")
   public static final String REQUEST_URI = "REQUEST_URI";

   @RBEntry("Referrer:")
   @RBComment("This string is for CSRFEventInfo")
   public static final String REFERER = "REFERER";

   @RBEntry("Download Acknowledgment:")
   @RBComment("This string is for SLDownloadAckEventInfo")
   public static final String DOWNLOAD_ACKNOWLEDGMENT = "DOWNLOAD_ACKNOWLEDGMENT";
}
