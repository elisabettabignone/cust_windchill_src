/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.audit.eventinfo;

import wt.util.resource.*;

@RBUUID("wt.audit.eventinfo.eventinfoResource")
public final class eventinfoResource_it extends WTListResourceBundle {
   /**
    * The following string are the display names of the attribute labels for audit report
    **/
   @RBEntry("Identificativo precedente:")
   @RBComment("This string is for ChangeIdentityEventInfo")
   public static final String OLD_IDENTITY = "00";

   @RBEntry("Stato del ciclo di vita precedente:")
   @RBComment("This string is for ChangeLifecycleStateEventInfo")
   public static final String OLD_LIFECYCLE_STATE = "10";

   @RBEntry("Identificativo di iterazione precedente:")
   @RBComment("This string is for CheckinEventInfo")
   public static final String OLD_ITERATION_IDENTITY = "20";

   @RBEntry("Percorso cartella della copia in modifica:")
   @RBComment("This string is for CheckoutEventInfo")
   public static final String WORKING_COPY_FOLDER_PATH = "30";

   @RBEntry("Nuovo oggetto:")
   @RBComment("This string is for CopyEventInfo")
   public static final String NEW_OBJECT = "40";

   @RBEntry("Nome del file per lo scaricamento:")
   @RBComment("The filename for downloading. It is for DownloadEventInfo")
   public static final String DOWNLOAD_FILENAME = "50";

   @RBEntry("Markup:")
   @RBComment("This string is for MarkupOrAnnotationIdentity")
   public static final String MARKUP_OR_ANNOTATION = "60";

   @RBEntry("Partecipante:")
   @RBComment("This string is for ModifyAccessPolicyEventInfo")
   public static final String PARTICIPANT_LABEL = "70";

   @RBEntry("Permessi concessi:")
   @RBComment("This string is for ModifyAccessPolicyEventInfo")
   public static final String PERMISSIONS_GRANTED = "71";

   @RBEntry("Permessi negati:")
   @RBComment("This string is for ModifyAccessPolicyEventInfo")
   public static final String PERMISSIONS_DENIED = "72";

   @RBEntry("Stato del ciclo di vita:")
   @RBComment("This string is for ModifyAccessPolicyEventInfo")
   public static final String LIFECYCLE_STATE = "73";

   @RBEntry("Tipo di oggetto:")
   @RBComment("This string is for ModifyAccessPolicyEventInfo")
   public static final String OBJECT_TYPE = "74";

   @RBEntry("Nome contenuto aggiunto:")
   @RBComment("This string is for ModifyContentEventInfo")
   public static final String CONTENT_NAME_ADDED = "80";

   @RBEntry("Nome contenuto rimosso:")
   @RBComment("This string is for ModifyContentEventInfo")
   public static final String CONTENT_NAME_REMOVED = "81";

   @RBEntry("Tipo di permesso:")
   @RBComment("This string is for ModifyObjectAccessEventInfo")
   public static final String PERMISSION_TYPE = "90";

   @RBEntry("Ad hoc")
   @RBComment("This string is for ModifyObjectAccessEventInfo")
   public static final String ADHOC_CHANGE = "91";

   @RBEntry("Dominio")
   @RBComment("This string is for ModifyObjectAccessEventInfo")
   public static final String DOMAIN_CHANGE = "92";

   @RBEntry("Ad hoc e dominio")
   @RBComment("This string is for ModifyObjectAccessEventInfo")
   public static final String ADHOC_DOMAIN_CHANGE = "93";

   @RBEntry("Lista di controllo d'accesso ad hoc:")
   @RBComment("This string is for ModifyObjectAccessEventInfo")
   public static final String ADHOC_ACLENTRYSET = "94";

   @RBEntry("Lista di controllo d'accesso in base a regole:")
   @RBComment("This string is for ModifyObjectAccessEventInfo")
   public static final String POLICY_ACLENTRYSET = "95";

   @RBEntry("Figlio aggiunto:")
   @RBComment("This string is for ModifyProStructureEventInfo")
   public static final String CHILD_ADDED = "100";

   @RBEntry("Figlio rimosso:")
   @RBComment("This string is for ModifyProStructureEventInfo")
   public static final String CHILD_REMOVED = "101";

   @RBEntry("Quantità figlio modificata:")
   @RBComment("This string is for ModifyProStructureEventInfo")
   public static final String CHILD_QUANTITY_CHANGED = "102";

   @RBEntry("Partecipanti aggiunti:")
   @RBComment("This string is for ModifyGroupEventInfo and ModifyTeamEventInfo")
   public static final String PRINCIPAL_ADDED = "110";

   @RBEntry("Partecipanti rimossi:")
   @RBComment("This string is for ModifyGroupEventInfo and ModifyTeamEventInfo")
   public static final String PRINCIPAL_REMOVED = "111";

   @RBEntry("Ruolo:")
   @RBComment("This string is for ModifyTeamEventInfo")
   public static final String ROLE_LABEL = "112";

   @RBEntry("Membro:")
   @RBComment("This string is for ModifyTeamRoleEventInfo")
   public static final String MEMBER_LABEL = "120";

   @RBEntry("Percorso cartella di origine:")
   @RBComment("This string is for MoveEventInfo")
   public static final String FROM_FOLDER_PATH = "130";

   @RBEntry("Permesso:")
   @RBComment("This string is for NoAuthorizedAccessEventInfo")
   public static final String PERMISSION_LABEL = "140";

   @RBEntry("Messaggio:")
   @RBComment("This string is for NoAuthorizedAccessEventInfo")
   public static final String MESSAGE_LABEL = "141";

   @RBEntry("Criteri di ricerca:")
   @RBComment("This string is for SearchEventInfo")
   public static final String SEARCH_CRITERIA = "150";

   @RBEntry("Percorso cartella di destinazione:")
   @RBComment("This string is for ShareEventInfo")
   public static final String TO_FOLDER_PATH = "160";

   @RBEntry("Utenti in simultanea:")
   @RBComment("This string is for UserSessionEventInfo")
   public static final String CONCURRENCY_USERS = "170";

   @RBEntry("Versione precedente:")
   @RBComment("This string is for VersionEventInfo")
   public static final String OLD_VERSION = "180";

   @RBEntry("Rappresentazione:")
   @RBComment("This string is for ViewRepresentationsEventInfo")
   public static final String REPRESENTATION_NAME = "190";

   @RBEntry("File '{0}' della rappresentazione '{1}' inviato alla stampate '{2}'")
   @RBComment("This string is for SentToPrintEventInfo")
   @RBArgComment0("name of the file that was sent to print")
   @RBArgComment1("name of the Representation whose file was sent to print")
   @RBArgComment2("name of the printer that the representation file was sent to")
   public static final String REPRESENTATION_FILE_SENT_TO_PRINT = "REPRESENTATION_FILE_SENT_TO_PRINT";
   
   @RBEntry("File '{0}' inviato alla stampante '{1}'")
   @RBComment("This string is for SentToPrintEventInfo")
   @RBArgComment0("name of the file that was sent to print")
   @RBArgComment1("name of the printer that the representation file was sent to")
   public static final String FILE_SENT_TO_PRINT = "FILE_SENT_TO_PRINT";

   @RBEntry("Sconosciuto")
   @RBComment("This string is in Event Specific Data to indicate an unknown object")
   public static final String UNKNOWN = "UNKNOWN";

   @RBEntry("Nome:")
   @RBComment("This string is for WfVariableEventInfo")
   public static final String NAME_LABEL = "200";

   @RBEntry("Tipo:")
   @RBComment("This string is for WfVariableEventInfo")
   public static final String TYPE_LABEL = "201";

   @RBEntry("Valore:")
   @RBComment("This string is for WfVariableEventInfo")
   public static final String VALUE_LABEL = "202";

   @RBEntry("Etichette di sicurezza precedenti:")
   @RBComment("This string is for ModifySecurityLabelsEventInfo")
   public static final String OLD_SECURITY_LABELS = "210";

   @RBEntry("Identificativi parte:")
   @RBComment("This string is for AssociateDisassociateEventInfo")
   public static final String PART_INFO = "220";

   @RBEntry("Tutti tranne partecipante:")
   @RBComment("This string is for ModifyAccessPolicyEventInfo")
   public static final String ALL_EXCEPT_PARTICIPANT_LABEL = "ALL_EXCEPT_PARTICIPANT_LABEL";

   @RBEntry("Permessi:")
   @RBComment("This string is for ModifyAccessPolicyEventInfo")
   public static final String PERMISSIONS_LABEL = "PERMISSIONS_LABEL";

   @RBEntry("Permessi negati in modo non modificabile:")
   @RBComment("This string is for ModifyAccessPolicyEventInfo")
   public static final String PERMISSIONS_ABSOLUTELY_DENIED = "PERMISSIONS_ABSOLUTELY_DENIED";

   @RBEntry("ID richiesta:")
   @RBComment("This string is for CSRFEventInfo")
   public static final String REQUEST_ID = "REQUEST_ID";

   @RBEntry("URI richiesta:")
   @RBComment("This string is for CSRFEventInfo")
   public static final String REQUEST_URI = "REQUEST_URI";

   @RBEntry("Referente:")
   @RBComment("This string is for CSRFEventInfo")
   public static final String REFERER = "REFERER";

   @RBEntry("Riconoscimento :scaricamento")
   @RBComment("This string is for SLDownloadAckEventInfo")
   public static final String DOWNLOAD_ACKNOWLEDGMENT = "DOWNLOAD_ACKNOWLEDGMENT";
}
