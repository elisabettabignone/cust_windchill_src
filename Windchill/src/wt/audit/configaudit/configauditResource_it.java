/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.audit.configaudit;

import wt.util.resource.*;

@RBUUID("wt.audit.configaudit.configauditResource")
public final class configauditResource_it extends WTListResourceBundle {
   @RBEntry("Oggetto di destinazione nullo ricevuto da {0}")
   public static final String NULL_TARGET_OBJECT = "0";

   @RBEntry("Oggetto non valido trasmesso a {0}. È stato ricevuto un {1} invece di un'azione.")
   public static final String NEED_ACTION_ITEM = "1";

   @RBEntry("Errore di inizializzazione del registro di verifica, la classe {0} non esiste nel codebase. Probabile errore di digitazione in {1}.")
   public static final String ILLEGAL_HANDLER_NAME = "2";

   @RBEntry("Tag evento sconosciuto: {0}, correggere in {1}")
   public static final String UNKNOWN_EVENT_ENTRY_TAG = "3";

   @RBEntry("{0} può gestire solo oggetti di destinazione {1}, specificare un altro gestore per questa combinazione classe/evento in {2}")
   public static final String HANDLER_CANT_DO_ALL = "4";

   @RBEntry("Errore in {0}, voce tag {1}, impossibile trovare la classe {2}.")
   public static final String ERROR_IN_FORUM_EVENT_TAG = "5";

   @RBEntry("Eliminato")
   @RBComment("Text shown instead of the user's (or group's) name when the user (or group) has been deleted.")
   public static final String DELETED_PRINCIPAL = "6";
}
