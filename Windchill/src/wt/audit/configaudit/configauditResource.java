/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.audit.configaudit;

import wt.util.resource.*;

@RBUUID("wt.audit.configaudit.configauditResource")
public final class configauditResource extends WTListResourceBundle {
   @RBEntry("Null target object recieved by{0}")
   public static final String NULL_TARGET_OBJECT = "0";

   @RBEntry("Illegal object passed to {0} expected an ActionItem recieved a {1}")
   public static final String NEED_ACTION_ITEM = "1";

   @RBEntry("Error in initializing audit registry, the class {0} does not exist in the codebase, this is probably due to a typographical error in {1}")
   public static final String ILLEGAL_HANDLER_NAME = "2";

   @RBEntry("Unknown event entry tag: {0}, please repair your entries in {1}")
   public static final String UNKNOWN_EVENT_ENTRY_TAG = "3";

   @RBEntry("{0} can only handle targets which are {1}, please specify another handler for this class/event combination in {2}")
   public static final String HANDLER_CANT_DO_ALL = "4";

   @RBEntry("Error in {0}, entry for tag {1}, the class {2} was not found.")
   public static final String ERROR_IN_FORUM_EVENT_TAG = "5";

   @RBEntry("Deleted")
   @RBComment("Text shown instead of the user's (or group's) name when the user (or group) has been deleted.")
   public static final String DELETED_PRINCIPAL = "6";
}
