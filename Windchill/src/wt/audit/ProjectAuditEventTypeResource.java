/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.audit;

import wt.util.resource.*;

@RBUUID("wt.audit.ProjectAuditEventType")
public final class ProjectAuditEventTypeResource extends WTListResourceBundle {
   @RBEntry("Create")
   public static final String PRIVATE_CONSTANT_0 = "*/wt.fc.PersistenceManagerEvent/POST_STORE";

   @RBEntry("Modify")
   public static final String PRIVATE_CONSTANT_1 = "*/wt.fc.PersistenceManagerEvent/POST_MODIFY";

   @RBEntry("Delete")
   public static final String PRIVATE_CONSTANT_2 = "*/wt.fc.PersistenceManagerEvent/PRE_DELETE";

   @RBEntry("Delete")
   public static final String PRIVATE_CONSTANT_3 = "*/wt.fc.PersistenceManagerEvent/PRE_REMOVE";

   @RBEntry("Checkin")
   public static final String PRIVATE_CONSTANT_4 = "*/wt.vc.wip.WorkInProgressServiceEvent/POST_CHECKIN";

   @RBEntry("Checkout")
   public static final String PRIVATE_CONSTANT_5 = "*/wt.vc.wip.WorkInProgressServiceEvent/POST_CHECKOUT";

   @RBEntry("Undo Checkout")
   public static final String PRIVATE_CONSTANT_6 = "*/wt.vc.wip.WorkInProgressServiceEvent/POST_UNDO_CHECKOUT";

   @RBEntry("Rollback to a previous iteration, removing the newer iterations")
   public static final String PRIVATE_CONSTANT_7 = "*/wt.vc.VersionControlServiceEvent/POST_ROLLBACK";

   @RBEntry("Revise")
   public static final String PRIVATE_CONSTANT_8 = "*/wt.vc.VersionControlServiceEvent/NEW_VERSION";

   @RBEntry("Workflow state change")
   public static final String PRIVATE_CONSTANT_9 = "*/wt.workflow.engine.WfEngineServiceEvent/PROCESS_STATE_CHANGED";

   @RBEntry("Workflow activity state change")
   public static final String PRIVATE_CONSTANT_10 = "*/wt.workflow.engine.WfEngineServiceEvent/ACTIVITY_STATE_CHANGED";

   @RBEntry("Workflow variable change")
   public static final String PRIVATE_CONSTANT_11 = "*/wt.workflow.engine.WfEngineServiceEvent/PROCESS_CONTEXT_CHANGED";

   @RBEntry("Workflow activity variable change")
   public static final String PRIVATE_CONSTANT_12 = "*/wt.workflow.engine.WfEngineServiceEvent/ACTIVITY_CONTEXT_CHANGED";

   @RBEntry("Project management node creation")
   public static final String PRIVATE_CONSTANT_13 = "*/wt.projmgmt.ProjMgmtServiceEvent/NODE_CREATION";

   @RBEntry("Project management state change")
   public static final String PRIVATE_CONSTANT_14 = "*/wt.projmgmt.ProjMgmtServiceEvent/STATE_CHANGE";

   @RBEntry("Project management health status change")
   public static final String PRIVATE_CONSTANT_15 = "*/wt.projmgmt.ProjMgmtServiceEvent/STATUS_CHANGE";

   @RBEntry("Project management percent complete change")
   public static final String PRIVATE_CONSTANT_16 = "*/wt.projmgmt.ProjMgmtServiceEvent/PERCENT_CHANGE";

   @RBEntry("Project management estimated finish date change")
   public static final String PRIVATE_CONSTANT_17 = "*/wt.projmgmt.ProjMgmtServiceEvent/FINISH_CHANGE";

   @RBEntry("Project management deadline change")
   public static final String PRIVATE_CONSTANT_18 = "*/wt.projmgmt.ProjMgmtServiceEvent/DEADLINE_CHANGE";

   @RBEntry("Project management object deletion")
   public static final String PRIVATE_CONSTANT_19 = "*/wt.projmgmt.ProjMgmtServiceEvent/OBJECT_DELETION";

   @RBEntry("Project management object owner change")
   public static final String PRIVATE_CONSTANT_20 = "*/wt.projmgmt.ProjMgmtServiceEvent/OWNER_CHANGE";

   @RBEntry("View object content")
   public static final String PRIVATE_CONSTANT_21 = "*/wt.projmgmt.ProjMgmtServiceEvent/VIEW_CONTENT";

   @RBEntry("Export object")
   public static final String PRIVATE_CONSTANT_22 = "*/wt.projmgmt.ProjMgmtServiceEvent/EXPORT_OBJECT";

   @RBEntry("Ad hoc rights change")
   public static final String PRIVATE_CONSTANT_23 = "*/wt.access.AccessControlEvent/AD_HOC_CHANGE";

   @RBEntry("Authorization exception")
   public static final String PRIVATE_CONSTANT_24 = "*/wt.access.AccessControlEvent/NOT_AUTHORIZED";

   @RBEntry("ActionItem resolved")
   public static final String PRIVATE_CONSTANT_25 = "*/wt.meeting.actionitem.ActionItemEvent/RESOLVE";

   @RBEntry("ActionItem updated")
   public static final String PRIVATE_CONSTANT_26 = "*/wt.meeting.actionitem.ActionItemEvent/UPDATE";

   @RBEntry("ActionItem deleted")
   public static final String PRIVATE_CONSTANT_27 = "*/wt.meeting.actionitem.ActionItemEvent/DELETE";

   @RBEntry("Add member to group")
   public static final String PRIVATE_CONSTANT_28 = "*/wt.org.OrganizationServicesEvent/ADD_MEMBER";

   @RBEntry("Remove member from group")
   public static final String PRIVATE_CONSTANT_29 = "*/wt.org.OrganizationServicesEvent/REMOVE_MEMBER";

   @RBEntry("Posting or topic created in a forum")
   public static final String PRIVATE_CONSTANT_30 = "*/wt.workflow.forum.ForumServiceEvent/NEW_DISCUSSION";

   @RBEntry("Rename")
   public static final String PRIVATE_CONSTANT_31 = "*/wt.fc.IdentityServiceEvent/PRE_CHANGE_IDENTITY";

   @RBEntry("Item moved to new location")
   public static final String PRIVATE_CONSTANT_32 = "*/wt.folder.FolderServiceEvent/POST_CHANGE_FOLDER";

   @RBEntry("Change CAD Name")
   public static final String PRIVATE_CONSTANT_33 = "*/wt.epm.EPMDocumentManagerEvent/PRE_CHANGE_CAD_NAME";

   @RBEntry("Add role to team")
   public static final String PRIVATE_CONSTANT_34 = "*/wt.team.TeamServiceEvent/ADD_ROLE";

   @RBEntry("Remove role from team")
   public static final String PRIVATE_CONSTANT_35 = "*/wt.team.TeamServiceEvent/REMOVE_ROLE";

   @RBEntry("Add role to organization")
   public static final String PRIVATE_CONSTANT_36 = "*/wt.inf.team.NmOrganizationServiceEvent/ADD_ROLE";

   @RBEntry("Remove role from organization")
   public static final String PRIVATE_CONSTANT_37 = "*/wt.inf.team.NmOrganizationServiceEvent/REMOVE_ROLE";
}
