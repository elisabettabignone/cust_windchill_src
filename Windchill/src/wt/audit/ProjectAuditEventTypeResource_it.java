/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.audit;

import wt.util.resource.*;

@RBUUID("wt.audit.ProjectAuditEventType")
public final class ProjectAuditEventTypeResource_it extends WTListResourceBundle {
   @RBEntry("Crea")
   public static final String PRIVATE_CONSTANT_0 = "*/wt.fc.PersistenceManagerEvent/POST_STORE";

   @RBEntry("Modifica")
   public static final String PRIVATE_CONSTANT_1 = "*/wt.fc.PersistenceManagerEvent/POST_MODIFY";

   @RBEntry("Elimina")
   public static final String PRIVATE_CONSTANT_2 = "*/wt.fc.PersistenceManagerEvent/PRE_DELETE";

   @RBEntry("Elimina")
   public static final String PRIVATE_CONSTANT_3 = "*/wt.fc.PersistenceManagerEvent/PRE_REMOVE";

   @RBEntry("Check-In")
   public static final String PRIVATE_CONSTANT_4 = "*/wt.vc.wip.WorkInProgressServiceEvent/POST_CHECKIN";

   @RBEntry("Check-Out")
   public static final String PRIVATE_CONSTANT_5 = "*/wt.vc.wip.WorkInProgressServiceEvent/POST_CHECKOUT";

   @RBEntry("Annulla Check-Out")
   public static final String PRIVATE_CONSTANT_6 = "*/wt.vc.wip.WorkInProgressServiceEvent/POST_UNDO_CHECKOUT";

   @RBEntry("Ritorna all'iterazione precedente e rimuovi le iterazioni più recenti.")
   public static final String PRIVATE_CONSTANT_7 = "*/wt.vc.VersionControlServiceEvent/POST_ROLLBACK";

   @RBEntry("Nuova revisione")
   public static final String PRIVATE_CONSTANT_8 = "*/wt.vc.VersionControlServiceEvent/NEW_VERSION";

   @RBEntry("Modifica dello stato di workflow")
   public static final String PRIVATE_CONSTANT_9 = "*/wt.workflow.engine.WfEngineServiceEvent/PROCESS_STATE_CHANGED";

   @RBEntry("Modifica dello stato dell'attività di workflow")
   public static final String PRIVATE_CONSTANT_10 = "*/wt.workflow.engine.WfEngineServiceEvent/ACTIVITY_STATE_CHANGED";

   @RBEntry("Modifica della variabile di workflow")
   public static final String PRIVATE_CONSTANT_11 = "*/wt.workflow.engine.WfEngineServiceEvent/PROCESS_CONTEXT_CHANGED";

   @RBEntry("Modifica della variabile dell'attività di workflow")
   public static final String PRIVATE_CONSTANT_12 = "*/wt.workflow.engine.WfEngineServiceEvent/ACTIVITY_CONTEXT_CHANGED";

   @RBEntry("Gestione progetto - creazione del nodo")
   public static final String PRIVATE_CONSTANT_13 = "*/wt.projmgmt.ProjMgmtServiceEvent/NODE_CREATION";

   @RBEntry("Gestione progetto - Modifica di stato del ciclo di vita")
   public static final String PRIVATE_CONSTANT_14 = "*/wt.projmgmt.ProjMgmtServiceEvent/STATE_CHANGE";

   @RBEntry("Gestione progetto - Modifica di stato")
   public static final String PRIVATE_CONSTANT_15 = "*/wt.projmgmt.ProjMgmtServiceEvent/STATUS_CHANGE";

   @RBEntry("Gestione progetto - Modifica della percentuale di completamento")
   public static final String PRIVATE_CONSTANT_16 = "*/wt.projmgmt.ProjMgmtServiceEvent/PERCENT_CHANGE";

   @RBEntry("Gestione progetto - Modifica della data di completamento prevista")
   public static final String PRIVATE_CONSTANT_17 = "*/wt.projmgmt.ProjMgmtServiceEvent/FINISH_CHANGE";

   @RBEntry("Gestione progetto - Modifica di scadenza")
   public static final String PRIVATE_CONSTANT_18 = "*/wt.projmgmt.ProjMgmtServiceEvent/DEADLINE_CHANGE";

   @RBEntry("Gestione progetto - Eliminazione di oggetto")
   public static final String PRIVATE_CONSTANT_19 = "*/wt.projmgmt.ProjMgmtServiceEvent/OBJECT_DELETION";

   @RBEntry("Gestione progetto - Modifica proprietario oggetto")
   public static final String PRIVATE_CONSTANT_20 = "*/wt.projmgmt.ProjMgmtServiceEvent/OWNER_CHANGE";

   @RBEntry("Visualizza contenuto oggetto")
   public static final String PRIVATE_CONSTANT_21 = "*/wt.projmgmt.ProjMgmtServiceEvent/VIEW_CONTENT";

   @RBEntry("Esportazione oggetto")
   public static final String PRIVATE_CONSTANT_22 = "*/wt.projmgmt.ProjMgmtServiceEvent/EXPORT_OBJECT";

   @RBEntry("Modifica dei diritti ad hoc")
   public static final String PRIVATE_CONSTANT_23 = "*/wt.access.AccessControlEvent/AD_HOC_CHANGE";

   @RBEntry("Eccezione d'autorizzazione")
   public static final String PRIVATE_CONSTANT_24 = "*/wt.access.AccessControlEvent/NOT_AUTHORIZED";

   @RBEntry("Azione eseguita")
   public static final String PRIVATE_CONSTANT_25 = "*/wt.meeting.actionitem.ActionItemEvent/RESOLVE";

   @RBEntry("Azione aggiornata")
   public static final String PRIVATE_CONSTANT_26 = "*/wt.meeting.actionitem.ActionItemEvent/UPDATE";

   @RBEntry("Azione eliminata")
   public static final String PRIVATE_CONSTANT_27 = "*/wt.meeting.actionitem.ActionItemEvent/DELETE";

   @RBEntry("Aggiunta membro al gruppo")
   public static final String PRIVATE_CONSTANT_28 = "*/wt.org.OrganizationServicesEvent/ADD_MEMBER";

   @RBEntry("Rimozione membro dal gruppo")
   public static final String PRIVATE_CONSTANT_29 = "*/wt.org.OrganizationServicesEvent/REMOVE_MEMBER";

   @RBEntry("Creazione di messaggio o argomento nel forum")
   public static final String PRIVATE_CONSTANT_30 = "*/wt.workflow.forum.ForumServiceEvent/NEW_DISCUSSION";

   @RBEntry("Rinomina")
   public static final String PRIVATE_CONSTANT_31 = "*/wt.fc.IdentityServiceEvent/PRE_CHANGE_IDENTITY";

   @RBEntry("Elementi spostati nella nuova posizione")
   public static final String PRIVATE_CONSTANT_32 = "*/wt.folder.FolderServiceEvent/POST_CHANGE_FOLDER";

   @RBEntry("Modifica nome CAD")
   public static final String PRIVATE_CONSTANT_33 = "*/wt.epm.EPMDocumentManagerEvent/PRE_CHANGE_CAD_NAME";

   @RBEntry("Aggiungi ruolo al team")
   public static final String PRIVATE_CONSTANT_34 = "*/wt.team.TeamServiceEvent/ADD_ROLE";

   @RBEntry("Rimuovi ruolo dal team")
   public static final String PRIVATE_CONSTANT_35 = "*/wt.team.TeamServiceEvent/REMOVE_ROLE";

   @RBEntry("Aggiungi ruolo all'organizzazione")
   public static final String PRIVATE_CONSTANT_36 = "*/wt.inf.team.NmOrganizationServiceEvent/ADD_ROLE";

   @RBEntry("Rimuovi ruolo dall'organizzazione")
   public static final String PRIVATE_CONSTANT_37 = "*/wt.inf.team.NmOrganizationServiceEvent/REMOVE_ROLE";
}
