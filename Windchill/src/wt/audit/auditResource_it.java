/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.audit;

import wt.util.resource.*;

@RBUUID("wt.audit.auditResource")
public final class auditResource_it extends WTListResourceBundle {
   @RBEntry("La data d'inizio deve precedere la data di fine.")
   @RBComment("Message issued when one tries to create an invalid time interval.")
   public static final String START_AFTER_END = "0";

   /**
    * The following strings are for the Audit Record Purge functionality
    **/
   @RBEntry("Record di verifica eliminati")
   @RBComment("Name of the purged audit records file.")
   public static final String RESULT_FILENAME = "40";

   /**
    * The following strings are for License Usage Report notification
    **/
   @RBEntry("MMMM yyyy")
   @RBPseudoTrans("MMMM yyyy")
   @RBComment("Date Format that appears in License Usage Report notification (August 2006)")
   public static final String NOTIFICATION_DATE_FORMAT = "50";

   @RBEntry("Il presente report mensile di attività sul server Windchill viene inviato per consentire il monitoraggio dell'uso del sistema e della conformità delle licenze. Sono stati superati i limiti relativi alla licenza per {0}. Vedere il report seguente per i dettagli.")
   @RBComment("Notification description ")
   @RBArgComment0("Date (June 2006)")
   public static final String DESCRIPTION_TEXT_1 = "51";

   @RBEntry("Il presente report mensile di attività sul server Windchill viene inviato per consentire il monitoraggio dell'uso del sistema e della conformità delle licenze. Non sono stati superati i limiti relativi alla licenza per {0}. Vedere il report seguente per i dettagli.")
   @RBComment("Notification description ")
   @RBArgComment0("Date (June 2006)")
   public static final String DESCRIPTION_TEXT_2 = "52";

   @RBEntry("Contattare il rappresentante dell'account PTC di competenza per mettersi in regola con il contratto di licenza per il software o contattare il rappresentante locale del supporto per le licenze visitando la pagina al seguente link: {0}. ")
   @RBComment("Notification description ")
   @RBArgComment0("link (http://www.ptc.com/license_support)")
   public static final String DESCRIPTION_TEXT_3 = "53";

   @RBEntry("Report utilizzo delle licenze per {0}")
   @RBComment("License Usage Report notification subject")
   @RBArgComment0("String that indicate the current month of the Usage Report ")
   public static final String TITLE_TEXT = "54";

   @RBEntry("Organizzazione host sito:")
   public static final String HOST_ORGANIZATION_LABEL = "55";

   @RBEntry("Conforme:")
   public static final String COMPLIANT_LABEL = "56";

   @RBEntry("Totale licenze:")
   public static final String LICENSE_COUNT_LABEL = "57";

   @RBEntry("Mese")
   public static final String MONTH_LABEL = "58";

   @RBEntry("Utenti")
   public static final String USER_LABEL = "60";

   @RBEntry("Conformità")
   public static final String COMPLIANCE_LABEL = "63";

   @RBEntry("Totale licenze")
   public static final String LICENSE_COUNT_LABEL_2 = "64";

   @RBEntry("Utenti simultanei")
   public static final String CONCURRENT_USER_LABEL = "65";

   @RBEntry("Report utilizzo delle licenze Windchill (sito conforme) - {0}")
   @RBComment("License Usage Report notification subject")
   @RBArgComment0("(date - November 2006)")
   public static final String NOTIFICATION_SUBJECT_1 = "66";

   @RBEntry("Report utilizzo delle licenze Windchill (sito non conforme) - {0}")
   @RBComment("License Usage Report notification subject")
   @RBArgComment0("(date - November 2006)")
   public static final String NOTIFICATION_SUBJECT_2 = "67";

   @RBEntry("Interno")
   public static final String INTERNAL_LABEL = "68";

   @RBEntry("Esterno")
   public static final String EXTERNAL_LABEL = "69";

   @RBEntry("Sì")
   public static final String YES_LABEL = "70";

   @RBEntry("No")
   public static final String NO_LABEL = "72";

   @RBEntry("Report di verifica programmati")
   public static final String SCHEDULE_AUDIT_REPORT_FOLDER = "73";

   @RBEntry("Report utente")
   public static final String USER_AUDIT_REPORT_FOLDER = "74";

   /**
    * The following strings are for the Audit Report column display
    **/
   @RBEntry("ID contesto")
   public static final String AppContainerID = "App Container ID";

   @RBEntry("Nome contesto")
   public static final String AppContainerName = "App Container Name";

   @RBEntry("ID ramo tipo di contesto")
   public static final String AppContainerTypeBranchID = "App Container Type Branch ID";

   @RBEntry("ID ramo")
   public static final String BranchID = "Branch ID";

   @RBEntry("Percorso dominio")
   public static final String DomainPath = "Domain Path";

   @RBEntry("Chiave evento")
   public static final String EventKey = "Event Key";

   @RBEntry("Etichetta evento")
   public static final String EventLabel = "Event Label";

   @RBEntry("Ora evento")
   public static final String EventTime = "Event Time";

   @RBEntry("Percorso della cartella")
   public static final String FolderPath = "Folder Path";

   @RBEntry("Identificativo")
   public static final String Identity = "Identity";

   @RBEntry("Indirizzo IP")
   public static final String IPAddress = "IPAddress";

   @RBEntry("Stato del ciclo di vita")
   public static final String LifecycleState = "Lifecycle State";

   @RBEntry("ID Master")
   public static final String MasterID = "Master ID";

   @RBEntry("ID organizzazione")
   public static final String OrgContainerID = "Org Container ID";

   @RBEntry("Nome organizzazione")
   public static final String OrgContainerName = "Org Container Name";

   @RBEntry("Etichette di sicurezza")
   public static final String SecurityLabels = "Security Labels";

   @RBEntry("ID oggetto")
   public static final String TargetID = "Target ID";

   @RBEntry("Identificativo oggetto")
   public static final String TargetIdentity = "Target Identity";

   @RBEntry("Nome oggetto")
   public static final String TargetName = "Target Name";

   @RBEntry("Numero oggetto")
   public static final String TargetNumber = "Target Number";

   @RBEntry("Tipo di oggetto")
   public static final String TargetType = "Target Type";

   @RBEntry("ID ramo tipo di oggetto")
   public static final String TargetTypeBranchID = "Target Type Branch ID";

   @RBEntry("Descrizione transazione")
   public static final String TransactionDescription = "Transaction Description";

   @RBEntry("ID utente")
   public static final String UserID = "User Id";

   @RBEntry("Nome utente")
   public static final String UserName = "User Name";

   @RBEntry("Organizzazione utente")
   public static final String UserOrgName = "User Org Name";

   @RBEntry("Versione")
   public static final String VersionID = "Version ID";

   @RBEntry("ID ramo in uso")
   public static final String WorkingBranchID = "Working Branch ID";

   @RBEntry("Dati specifici dell'evento")
   public static final String EventSpecificData = "Event Specific Data";

   @RBEntry("I risultati sotto riportati sono un'anteprima dell'intero report e possono non contenere l'intero insieme dei record. Per vedere l'intero report, selezionare l'azione Genera report.")
   public static final String ReportPreviewNote = "Report Preview Note";

   @RBEntry("Report utilizzo - {0}")
   @RBComment("Usage Report notification subject")
   @RBArgComment0("String that indicate the current month of the Usage Report ")
   public static final String AUDIT_USAGE_REPORT_EMAIL_SUBJECT = "AUDIT_USAGE_REPORT_EMAIL_SUBJECT";

   @RBEntry("Report utilizzo di Windchill per {0}")
   @RBComment("Usage Report notification subject")
   @RBArgComment0("date of the audit usage report")
   public static final String AUDIT_USAGE_REPORT = "AUDIT_USAGE_REPORT";

   @RBEntry("Il presente report mensile di attività sul server Windchill viene inviato per consentire il monitoraggio dell'uso del sistema.")
   @RBComment(" the embedded text for the uage report email notification ")
   public static final String AUDIT_USAGE_EMAIL_EMBEDDED_TEXT = "AUDIT_USAGE_EMAIL_EMBEDDED_TEXT";

   @RBEntry("Anteprima report di verifica")
   public static final String PreviewAuditReportTitle = "Preview Audit Report";
}
