/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.audit;

import wt.util.resource.*;

@RBUUID("wt.audit.auditResource")
public final class auditResource extends WTListResourceBundle {
   @RBEntry("Start date must precede end date.")
   @RBComment("Message issued when one tries to create an invalid time interval.")
   public static final String START_AFTER_END = "0";

   /**
    * The following strings are for the Audit Record Purge functionality
    **/
   @RBEntry("Audit Records Purged")
   @RBComment("Name of the purged audit records file.")
   public static final String RESULT_FILENAME = "40";

   /**
    * The following strings are for License Usage Report notification
    **/
   @RBEntry("MMMM yyyy")
   @RBPseudoTrans("MMMM yyyy")
   @RBComment("Date Format that appears in License Usage Report notification (August 2006)")
   public static final String NOTIFICATION_DATE_FORMAT = "50";

   @RBEntry("You are receiving this monthly report of activity on a Windchill Server to help monitor system usage and license compliance. You have exceeded your license for {0}. See details in the following report.")
   @RBComment("Notification description ")
   @RBArgComment0("Date (June 2006)")
   public static final String DESCRIPTION_TEXT_1 = "51";

   @RBEntry("You are receiving this monthly report of activity on a Windchill Server to help monitor system usage and license compliance. You have not exceeded your license for {0}. See details in the following report.")
   @RBComment("Notification description ")
   @RBArgComment0("Date (June 2006)")
   public static final String DESCRIPTION_TEXT_2 = "52";

   @RBEntry("Please contact your PTC account representative to understand how to ensure compliance with your software license agreement or contact your regional license support representative by selecting the following link: {0}. ")
   @RBComment("Notification description ")
   @RBArgComment0("link (http://www.ptc.com/license_support)")
   public static final String DESCRIPTION_TEXT_3 = "53";

   @RBEntry("License Usage Report for {0}")
   @RBComment("License Usage Report notification subject")
   @RBArgComment0("String that indicate the current month of the Usage Report ")
   public static final String TITLE_TEXT = "54";

   @RBEntry("Site Host Organization:")
   public static final String HOST_ORGANIZATION_LABEL = "55";

   @RBEntry("Compliant:")
   public static final String COMPLIANT_LABEL = "56";

   @RBEntry("License Count:")
   public static final String LICENSE_COUNT_LABEL = "57";

   @RBEntry("Month")
   public static final String MONTH_LABEL = "58";

   @RBEntry("Users")
   public static final String USER_LABEL = "60";

   @RBEntry("Compliance")
   public static final String COMPLIANCE_LABEL = "63";

   @RBEntry("License Count")
   public static final String LICENSE_COUNT_LABEL_2 = "64";

   @RBEntry("Concurrent Users")
   public static final String CONCURRENT_USER_LABEL = "65";

   @RBEntry("Windchill License Usage Report (Site Compliant) - {0}")
   @RBComment("License Usage Report notification subject")
   @RBArgComment0("(date - November 2006)")
   public static final String NOTIFICATION_SUBJECT_1 = "66";

   @RBEntry("Windchill License Usage Report (Site Not Compliant) - {0}")
   @RBComment("License Usage Report notification subject")
   @RBArgComment0("(date - November 2006)")
   public static final String NOTIFICATION_SUBJECT_2 = "67";

   @RBEntry("Internal")
   public static final String INTERNAL_LABEL = "68";

   @RBEntry("External")
   public static final String EXTERNAL_LABEL = "69";

   @RBEntry("Yes")
   public static final String YES_LABEL = "70";

   @RBEntry("No")
   public static final String NO_LABEL = "72";

   @RBEntry("Scheduled Audit Reports")
   public static final String SCHEDULE_AUDIT_REPORT_FOLDER = "73";

   @RBEntry("User Reports")
   public static final String USER_AUDIT_REPORT_FOLDER = "74";

   /**
    * The following strings are for the Audit Report column display
    **/
   @RBEntry("Context ID")
   public static final String AppContainerID = "App Container ID";

   @RBEntry("Context Name")
   public static final String AppContainerName = "App Container Name";

   @RBEntry("Context Type Branch ID")
   public static final String AppContainerTypeBranchID = "App Container Type Branch ID";

   @RBEntry("Branch ID")
   public static final String BranchID = "Branch ID";

   @RBEntry("Domain Path")
   public static final String DomainPath = "Domain Path";

   @RBEntry("Event Key")
   public static final String EventKey = "Event Key";

   @RBEntry("Event Label")
   public static final String EventLabel = "Event Label";

   @RBEntry("Event Time")
   public static final String EventTime = "Event Time";

   @RBEntry("Folder Path")
   public static final String FolderPath = "Folder Path";

   @RBEntry("Identity")
   public static final String Identity = "Identity";

   @RBEntry("IP Address")
   public static final String IPAddress = "IPAddress";

   @RBEntry("Lifecycle State")
   public static final String LifecycleState = "Lifecycle State";

   @RBEntry("Master ID")
   public static final String MasterID = "Master ID";

   @RBEntry("Organization ID")
   public static final String OrgContainerID = "Org Container ID";

   @RBEntry("Organization Name")
   public static final String OrgContainerName = "Org Container Name";

   @RBEntry("Security Labels")
   public static final String SecurityLabels = "Security Labels";

   @RBEntry("Object ID")
   public static final String TargetID = "Target ID";

   @RBEntry("Object Identity")
   public static final String TargetIdentity = "Target Identity";

   @RBEntry("Object Name")
   public static final String TargetName = "Target Name";

   @RBEntry("Object Number")
   public static final String TargetNumber = "Target Number";

   @RBEntry("Object Type")
   public static final String TargetType = "Target Type";

   @RBEntry("Object Type Branch ID")
   public static final String TargetTypeBranchID = "Target Type Branch ID";

   @RBEntry("Transaction Description")
   public static final String TransactionDescription = "Transaction Description";

   @RBEntry("User ID")
   public static final String UserID = "User Id";

   @RBEntry("User Name")
   public static final String UserName = "User Name";

   @RBEntry("User Organization")
   public static final String UserOrgName = "User Org Name";

   @RBEntry("Version")
   public static final String VersionID = "Version ID";

   @RBEntry("Working Branch ID")
   public static final String WorkingBranchID = "Working Branch ID";

   @RBEntry("Event Specific Data")
   public static final String EventSpecificData = "Event Specific Data";

   @RBEntry("The results below are a preview of the full report and may not contain a full set of records. To view the entire report, choose the Generate Report action.")
   public static final String ReportPreviewNote = "Report Preview Note";

   @RBEntry("Usage Report - {0}")
   @RBComment("Usage Report notification subject")
   @RBArgComment0("String that indicate the current month of the Usage Report ")
   public static final String AUDIT_USAGE_REPORT_EMAIL_SUBJECT = "AUDIT_USAGE_REPORT_EMAIL_SUBJECT";

   @RBEntry("Windchill Usage Report for {0}")
   @RBComment("Usage Report notification subject")
   @RBArgComment0("date of the audit usage report")
   public static final String AUDIT_USAGE_REPORT = "AUDIT_USAGE_REPORT";

   @RBEntry("You are receiving this monthly report of activity on a Windchill server to help monitor system usage.")
   @RBComment(" the embedded text for the uage report email notification ")
   public static final String AUDIT_USAGE_EMAIL_EMBEDDED_TEXT = "AUDIT_USAGE_EMAIL_EMBEDDED_TEXT";

   @RBEntry("Preview Audit Report")
   public static final String PreviewAuditReportTitle = "Preview Audit Report";
}
