/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.introspection;

import wt.util.resource.*;

@RBUUID("wt.introspection.introspectionResource")
public final class introspectionResource_it extends WTListResourceBundle {
   @RBEntry("Si è verificato un errore SQL. Messaggio di sistema del database:")
   public static final String SQL_ERROR = "1";

   @RBEntry("Impossibile trovare la colonna {0} per {1}.")
   public static final String COLUMN_NOT_FOUND = "2";

   @RBEntry("ClassInfo trovato. Tuttavia il nome di classe \"{0}\" non corrisponde \"{1}\"")
   public static final String NAME_MISMATCH = "3";

   @RBEntry("Errore nel caricamento della risorsa Info per {0}")
   public static final String LOAD_FAILURE = "4";

   @RBEntry("Impossibile trovare la risorsa Info di {0}.")
   public static final String INFO_NOT_FOUND = "8";

   @RBEntry("Classe non registrata in modelRegistry.properties: {0}.")
   public static final String CLASS_NOT_REGISTERED = "9";

   @RBEntry("Si è verificato un errore d'introspezione. Messaggio di sistema:")
   public static final String ERROR = "6";

   @RBEntry("{0} è una colonna VARCHAR ridotta da {1} alla massima lunghezza di memoria del database.")
   public static final String COLUMN_LENGTH = "7";

   @RBEntry("Il valore supera il limite superiore di \"{1}\" per \"{0}\".")
   @RBArgComment0("name of attribute")
   @RBArgComment1("a number, which is the limit")
   public static final String UPPER_LIMIT = "20";

   @RBEntry("Il valore è inferiore al limite minimo di \"{1}\" per \"{0}\".")
   @RBArgComment0("name of attribute")
   @RBArgComment1("a number, which is the limit")
   public static final String LOWER_LIMIT = "21";

   @RBEntry("Il valore di \"{0}\" non può essere nullo poiché è un attributo \"obbligatorio\".")
   @RBArgComment0("name of attribute")
   public static final String REQUIRED_ATTRIBUTE = "22";

   @RBEntry("Il valore assegnato a \"{0}\" deve essere di tipo \"{1}\".")
   @RBArgComment0("name of attribute")
   @RBArgComment1("name of correct class (type)")
   public static final String WRONG_TYPE = "35";

   @RBEntry("Impossibile richiamare metodo {0}.")
   @RBComment("Method signature, eg: ClassX.someMethod()")
   public static final String UNABLE_TO_INVOKE = "101";

   @RBEntry("Impossibile creare l'oggetto {0}.")
   @RBComment("Object type.")
   public static final String UNABLE_TO_INSTANTIATE = "102";

   @RBEntry("{0} troppo lungo. Deve essere costituito da massimo {1} caratteri. Modificare {0} e riprovare.")
   @RBArgComment0("name of attribute")
   @RBArgComment1("a number, which is the limit")
   public static final String DESCRIPTION_CHARACTER_LIMIT = "103";
}
