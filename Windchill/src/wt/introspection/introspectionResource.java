/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.introspection;

import wt.util.resource.*;

@RBUUID("wt.introspection.introspectionResource")
public final class introspectionResource extends WTListResourceBundle {
   @RBEntry("A SQL error has occured. Database system message follows:")
   public static final String SQL_ERROR = "1";

   @RBEntry("{0} column not found for {1}.")
   public static final String COLUMN_NOT_FOUND = "2";

   @RBEntry("ClassInfo found, but its classname: \"{0}\" does not match \"{1}\"")
   public static final String NAME_MISMATCH = "3";

   @RBEntry("Failure loading Info resource for {0}.")
   public static final String LOAD_FAILURE = "4";

   @RBEntry("Info resource not found for {0}.")
   public static final String INFO_NOT_FOUND = "8";

   @RBEntry("Class not registered in modelRegistry.properties: {0}.")
   public static final String CLASS_NOT_REGISTERED = "9";

   @RBEntry("An introspection error has occured. System message follows:")
   public static final String ERROR = "6";

   @RBEntry("{0} is a VARCHAR column being reduced from {1}, to the maximum database storage length.")
   public static final String COLUMN_LENGTH = "7";

   @RBEntry("The value exceeds the upper limit of \"{1}\" for \"{0}\".")
   @RBArgComment0("name of attribute")
   @RBArgComment1("a number, which is the limit")
   public static final String UPPER_LIMIT = "20";

   @RBEntry("The value is less than the lower limit of \"{1}\" for \"{0}\".")
   @RBArgComment0("name of attribute")
   @RBArgComment1("a number, which is the limit")
   public static final String LOWER_LIMIT = "21";

   @RBEntry("The value of \"{0}\" cannot be set to null, since it is a \"required\" attribute.")
   @RBArgComment0("name of attribute")
   public static final String REQUIRED_ATTRIBUTE = "22";

   @RBEntry("The value assigned to \"{0}\" must be of the type \"{1}\".")
   @RBArgComment0("name of attribute")
   @RBArgComment1("name of correct class (type)")
   public static final String WRONG_TYPE = "35";

   @RBEntry("Unable to invoke method {0}.")
   @RBComment("Method signature, eg: ClassX.someMethod()")
   public static final String UNABLE_TO_INVOKE = "101";

   @RBEntry("Unable to create object {0}.")
   @RBComment("Object type.")
   public static final String UNABLE_TO_INSTANTIATE = "102";

   @RBEntry("The {0} is too large. It must be {1} characters or less.Change the {0} and try again.")
   @RBArgComment0("name of attribute")
   @RBArgComment1("a number, which is the limit")
   public static final String DESCRIPTION_CHARACTER_LIMIT = "103";
}
