/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.workflow.engine;

import wt.util.resource.*;

@RBUUID("wt.workflow.engine.engineResource")
public final class engineResource extends WTListResourceBundle {
   @RBEntry("The operation: {0} failed.")
   public static final String OPERATION_FAILED = "0";

   @RBEntry("{0} transition not allowed in current state: {1}")
   public static final String TRANSITION_NOT_ALLOWED = "1";

   @RBEntry("Process or activity {0} already running")
   public static final String ALREADY_RUNNING = "2";

   @RBEntry("{0} transition failed during expression evaluation")
   public static final String TRANSITION_FAILED = "3";

   @RBEntry("Variable doesn't exist: {0}")
   public static final String INVALID_VARIABLE = "4";

   @RBEntry("Couldn't retrieve peformer process")
   public static final String NO_PERFORMER = "5";

   @RBEntry("Event source {0} is not available")
   public static final String NO_SOURCE = "6";

   @RBEntry("Process expected: {0} is not a process")
   public static final String PROCESS_EXPECTED = "7";

   @RBEntry("{0} variable value is not compatible with its type")
   public static final String INVALID_VARIABLE_VALUE = "8";

   @RBEntry("Required value has not been provided for variables: {1} in {0}")
   @RBArgComment0("name of workflow process or activity")
   @RBArgComment1("list of required variables with missing values")
   public static final String MISSING_DATA = "9";

   @RBEntry("Invalid transition: {0}")
   public static final String INVALID_TRANSITION = "10";

   @RBEntry("Can't delete process {0} because it is nested")
   public static final String NESTED_PROCESS = "11";

   @RBEntry("{0} workflow exception")
   @RBArgComment0("workflow process or activity identity")
   @RBArgComment1("workflow process or activity name")
   @RBArgComment2("workflow process or activity description")
   public static final String EXCEPTION_NOTIFICATION_SUBJECT = "12";

   @RBEntry("Can't set variable {0} because value is not persistent")
   public static final String NON_PERSISTENT = "13";

   @RBEntry("Invalid block: no parent process: {0}")
   public static final String INVALID_BLOCK = "14";

   @RBEntry("{0} aborted")
   @RBArgComment0("workflow process or activity name")
   public static final String ABORT_NOTIFICATION_SUBJECT = "15";

   @RBEntry("{0} overdue")
   public static final String OVERDUE_NOTIFICATION_SUBJECT = "16";

   @RBEntry("{0} is completed")
   public static final String COMPLETE_NOTIFICATION_SUBJECT = "17";

   @RBEntry("{0} is approaching deadline")
   public static final String APPROACHING_DEADLINE_SUBJECT = "18";

   @RBEntry("{0} has passed deadline")
   public static final String PAST_DEADLINE_SUBJECT = "19";

   @RBEntry("Activity {0} can't be resumed because host process {1} is suspended")
   @RBArgComment0("activity name")
   @RBArgComment1("host process name")
   public static final String SUSPENDED_PROCESS = "20";

   @RBEntry("{0} transition expression for {1} returned false")
   public static final String EXPRESSION_RETURNED_FALSE = "21";

   @RBEntry("Can't start: existing ad hoc process is not closed for {0}")
   @RBArgComment0("activity name")
   public static final String PERFORMER_NOT_CLOSED = "22";

   @RBEntry("Can't create queue for process {0} because limit for the number of dedicated queues ({1}) has been reached.")
   public static final String QUEUE_LIMIT_REACHED = "23";

   @RBEntry("Can't restore worktitems for activity {0}.")
   public static final String CANT_RESTORE_STATE = "24";

   @RBEntry("Error while retrieving the value for variable {0}.")
   public static final String GET_VARIABLE_VALUE_ERROR = "25";

   @RBEntry("Error while setting the value for variable {0}.")
   public static final String SET_VARIABLE_VALUE_ERROR = "26";

   @RBEntry("The {0} project is in use.  All uses must be removed before the {0} project can be deleted.")
   @RBArgComment0("the identity of the project that the user is trying to delete")
   public static final String DELETE_PROJECT_PROHIBITED = "27";

   @RBEntry("The {0} process template was not found.")
   @RBArgComment0("name of a process template")
   public static final String TEMPLATE_NOT_FOUND = "28";

   @RBEntry("Missing or empty synchronization workflow parameter.")
   public static final String INVALID_PARAMETER = "29";

   @RBEntry("Error setting Process Attributes. Team Template passed, expected Team Instance.")
   public static final String INVALID_TEAM_INSTANCE  = "30";

   @RBEntry("Parent Process:")
   public static final String PARENT_PROCESS_LABEL = "31";

   @RBEntry("Overdue Notification")
   public static final String OVERDUE_NOTIFICATION_TITLE = "32";

   @RBEntry("Abort Notification")
   public static final String ABORT_NOTIFICATION_TITLE = "33";

   @RBEntry("Approaching Deadline Notification")
   public static final String APPROACHING_DEADLINE_NOTIFICATION_TITLE = "34";

   @RBEntry("Complete Notification")
   public static final String COMPLETE_NOTIFICATION_TITLE = "35";

   @RBEntry("Exception Notification")
   public static final String EXCEPTION_NOTIFICATION_TITLE = "36";

   @RBEntry("Notification")
   public static final String NOTIFICATION_TITLE = "37";

   @RBEntry("The {0} team is in use.  All uses must be removed before the {0} team can be deleted.")
   @RBArgComment0("the identity of the team that the user is trying to delete")
   public static final String DELETE_TEAM_PROHIBITED = "38";

   @RBEntry("Task:")
   public static final String TASK_LABEL = "39";

   @RBEntry("Owners of Overdue task:")
   public static final String TASK_OWNER_LABEL = "40";

   @RBEntry("Role:")
   public static final String ROLE_LABEL = "41";

   @RBEntry("Date of Delegation:")
   public static final String DATE_OF_DELEGATION = "42";

   @RBEntry("Delegation Period:")
   public static final String DELEGATION_PERIOD = "43";

   @RBEntry("Delegated By:")
   public static final String DELEGATED_BY = "44";

   @RBEntry("Please click {0} to access Windchill")
   public static final String CLICK_HERE = "45";

   @RBEntry("You have been designated as a delegate for {0}")
   public static final String DELEGATE_FOR = "46";

   @RBEntry("Indefinite")
   public static final String INDEFINITE = "47";

   @RBEntry("Delegate Notification")
   public static final String DELEGATE_NOTIFICATION = "48";

   @RBEntry("Time:")
   public static final String TIME_COLON = "49";

   @RBEntry("Previous state:")
   public static final String PREVIOUS_STATE_COLON = "50";

   @RBEntry("Parent process:")
   public static final String PARENT_PROCESS_COLON = "51";

   @RBEntry("Go to Process Manager:")
   public static final String GOTO_PROCESSMANAGER_COLON = "52";

   @RBEntry("Deadline:")
   public static final String DEADLINE_COLON = "53";

   @RBEntry("Error message:")
   public static final String ERRORMESSAGE_COLON = "54";

   @RBEntry("Workflow Administrator")
   public static final String WORKFLOW_ADMINISTRATOR = "55";

   @RBEntry("Process Templates")
   public static final String PROCESS_TEMPLATES = "56";

   @RBEntry("Reassign Workitems")
   public static final String REASSIGN_WORKITEMS = "57";

   @RBEntry("Reassign workitems to:")
   public static final String REASSIGN_WORKITEMS_TO_COLON = "58";

   @RBEntry("Update WorkItem Due Dates")
   public static final String UPDATE_WORKITEMS_DUEDATES = "59";

   @RBEntry("Update the due dates to:")
   public static final String UPDATE_DUEDATESTO_COLON = "60";

   @RBEntry("Process overview")
   public static final String PROCESS_OVERVIEW = "61";

   @RBEntry("Process Overview is not Available")
   public static final String NO_PROCESS_OVERVIEW = "62";

   @RBEntry("Suspended by ")
   public static final String COMMENT_SUSPEND = "63";

   @RBEntry("Resumed by")
   public static final String COMMENT_RESUME = "64";

   @RBEntry("Completed by")
   public static final String COMMENT_COMPLETE = "65";

   @RBEntry("Terminated by")
   public static final String COMMENT_TERMINATE = "66";

   @RBEntry("Activity Instructions")
   public static final String ACTIVITY_INSTR = "67";

   @RBEntry("Passed Deadline Notification")
   public static final String PAST_DEADLINE_NOTIFICATION = "68";

   @RBEntry("Activity has passed deadline.")
   public static final String PAST_DEADLINE_COMMENT = "69";

   @RBEntry("Checkpoint not found : {0}")
   public static final String NO_CHECKPOINT_FOUND = "70";
   
   @RBEntry("Primary Business Object:")
   public static final String PBO_LINK = "71";
   
   @RBEntry("Process:")
   public static final String PM_LINK = "72";
}
