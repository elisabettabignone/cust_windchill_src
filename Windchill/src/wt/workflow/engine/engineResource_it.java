/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.workflow.engine;

import wt.util.resource.*;

@RBUUID("wt.workflow.engine.engineResource")
public final class engineResource_it extends WTListResourceBundle {
   @RBEntry("L'operazione {0} non è riuscita.")
   public static final String OPERATION_FAILED = "0";

   @RBEntry("Transizione \"{0}\" non permessa nello stato del ciclo di vita corrente:  \"{1}\"")
   public static final String TRANSITION_NOT_ALLOWED = "1";

   @RBEntry("Processo o attività \"{0}\" già in esecuzione")
   public static final String ALREADY_RUNNING = "2";

   @RBEntry("Transizione \"{0}\" non riuscita durante la valutazione dell'espressione. ")
   public static final String TRANSITION_FAILED = "3";

   @RBEntry("Variabile non esistente: {0}")
   public static final String INVALID_VARIABLE = "4";

   @RBEntry("Impossibile recuperare il processo dell'esecutore")
   public static final String NO_PERFORMER = "5";

   @RBEntry("L'origine dell'evento \"{0}\" non è disponibile")
   public static final String NO_SOURCE = "6";

   @RBEntry("Processo previsto: \"{0}\" non è un processo")
   public static final String PROCESS_EXPECTED = "7";

   @RBEntry("Il valore della variabile \"{0}\" non è compatibile con il relativo tipo ")
   public static final String INVALID_VARIABLE_VALUE = "8";

   @RBEntry("Valore obbligatorio non fornito per le variabili: {1} in {0}")
   @RBArgComment0("name of workflow process or activity")
   @RBArgComment1("list of required variables with missing values")
   public static final String MISSING_DATA = "9";

   @RBEntry("Transizione non valida: {0}")
   public static final String INVALID_TRANSITION = "10";

   @RBEntry("Il processo {0} non può essere eliminato perché annidato")
   public static final String NESTED_PROCESS = "11";

   @RBEntry("Eccezione \"{0}\" nel workflow")
   @RBArgComment0("workflow process or activity identity")
   @RBArgComment1("workflow process or activity name")
   @RBArgComment2("workflow process or activity description")
   public static final String EXCEPTION_NOTIFICATION_SUBJECT = "12";

   @RBEntry("Valore non persistente. Impossibile impostare la variabile {0}")
   public static final String NON_PERSISTENT = "13";

   @RBEntry("Blocco invalido: manca il processo padre: {0}")
   public static final String INVALID_BLOCK = "14";

   @RBEntry("{0} è stato interrotto")
   @RBArgComment0("workflow process or activity name")
   public static final String ABORT_NOTIFICATION_SUBJECT = "15";

   @RBEntry("{0} è in ritardo")
   public static final String OVERDUE_NOTIFICATION_SUBJECT = "16";

   @RBEntry("{0} è completato")
   public static final String COMPLETE_NOTIFICATION_SUBJECT = "17";

   @RBEntry("{0} si sta avvicinando alla scadenza")
   public static final String APPROACHING_DEADLINE_SUBJECT = "18";

   @RBEntry("{0} ha oltrepassato la scadenza")
   public static final String PAST_DEADLINE_SUBJECT = "19";

   @RBEntry("L'attività {0} non può essere ripresa perché il processo host {1} è sospeso")
   @RBArgComment0("activity name")
   @RBArgComment1("host process name")
   public static final String SUSPENDED_PROCESS = "20";

   @RBEntry("L'espressione di transizione {0} per {1} è falsa")
   public static final String EXPRESSION_RETURNED_FALSE = "21";

   @RBEntry("Impossibile iniziare: un processo ad hoc esistente non è chiuso per {0}")
   @RBArgComment0("activity name")
   public static final String PERFORMER_NOT_CLOSED = "22";

   @RBEntry("Impossibile creare coda per il processo {0} perché è stato raggiunto il limite massimo di code ({1}).")
   public static final String QUEUE_LIMIT_REACHED = "23";

   @RBEntry("Impossibile ripristinare gli incarichi per l'attività {0}.")
   public static final String CANT_RESTORE_STATE = "24";

   @RBEntry("Errore durante il recupero del valore della variabile {0}.")
   public static final String GET_VARIABLE_VALUE_ERROR = "25";

   @RBEntry("Errore durante l'impostazione del valore della variabile {0}.")
   public static final String SET_VARIABLE_VALUE_ERROR = "26";

   @RBEntry("Il progetto {0} è correntemente in uso. Prima di eliminare il progetto {0} è necessario rimuovere tutti i componenti.")
   @RBArgComment0("the identity of the project that the user is trying to delete")
   public static final String DELETE_PROJECT_PROHIBITED = "27";

   @RBEntry("Impossibile trovare il modello del processo {0}.")
   @RBArgComment0("name of a process template")
   public static final String TEMPLATE_NOT_FOUND = "28";

   @RBEntry("Il parametro di workflow per la sincronizzazione è mancante o vuoto.")
   public static final String INVALID_PARAMETER = "29";

   @RBEntry("Errore nella definizione degli attributi del processo. Modello team trasmesso, istanza team attesa.")
   public static final String INVALID_TEAM_INSTANCE  = "30";

   @RBEntry("Processo superiore:")
   public static final String PARENT_PROCESS_LABEL = "31";

   @RBEntry("Notifica di scadenza")
   public static final String OVERDUE_NOTIFICATION_TITLE = "32";

   @RBEntry("Interrompi notifica")
   public static final String ABORT_NOTIFICATION_TITLE = "33";

   @RBEntry("Notifica di scadenza in avvicinamento")
   public static final String APPROACHING_DEADLINE_NOTIFICATION_TITLE = "34";

   @RBEntry("Notifica di completamento")
   public static final String COMPLETE_NOTIFICATION_TITLE = "35";

   @RBEntry("Notifica di eccezione")
   public static final String EXCEPTION_NOTIFICATION_TITLE = "36";

   @RBEntry("Notifica")
   public static final String NOTIFICATION_TITLE = "37";

   @RBEntry("Il team {0} è in uso al momento. Prima di eliminare il team {0} è necessario rimuovere tutti i componenti.")
   @RBArgComment0("the identity of the team that the user is trying to delete")
   public static final String DELETE_TEAM_PROHIBITED = "38";

   @RBEntry("Task:")
   public static final String TASK_LABEL = "39";

   @RBEntry("Proprietari del task in ritardo:")
   public static final String TASK_OWNER_LABEL = "40";

   @RBEntry("Ruolo:")
   public static final String ROLE_LABEL = "41";

   @RBEntry("Data della delega:")
   public static final String DATE_OF_DELEGATION = "42";

   @RBEntry("Periodo di delega:")
   public static final String DELEGATION_PERIOD = "43";

   @RBEntry("Delegante:")
   public static final String DELEGATED_BY = "44";

   @RBEntry("Per accedere a WIndchill, fare clic su {0}")
   public static final String CLICK_HERE = "45";

   @RBEntry("È stata ricevuta delega per {0}")
   public static final String DELEGATE_FOR = "46";

   @RBEntry("Indefinito")
   public static final String INDEFINITE = "47";

   @RBEntry("Notifica di delega")
   public static final String DELEGATE_NOTIFICATION = "48";

   @RBEntry("Ora:")
   public static final String TIME_COLON = "49";

   @RBEntry("Stato precedente:")
   public static final String PREVIOUS_STATE_COLON = "50";

   @RBEntry("Processo padre:")
   public static final String PARENT_PROCESS_COLON = "51";

   @RBEntry("Vai a Gestione processi:")
   public static final String GOTO_PROCESSMANAGER_COLON = "52";

   @RBEntry("Scadenza:")
   public static final String DEADLINE_COLON = "53";

   @RBEntry("Messaggio di errore:")
   public static final String ERRORMESSAGE_COLON = "54";

   @RBEntry("Amministrazione workflow")
   public static final String WORKFLOW_ADMINISTRATOR = "55";

   @RBEntry("Modelli di processo")
   public static final String PROCESS_TEMPLATES = "56";

   @RBEntry("Riassegna task")
   public static final String REASSIGN_WORKITEMS = "57";

   @RBEntry("Riassegna task a:")
   public static final String REASSIGN_WORKITEMS_TO_COLON = "58";

   @RBEntry("Aggiorna date di scadenza task")
   public static final String UPDATE_WORKITEMS_DUEDATES = "59";

   @RBEntry("Nuove scadenze:")
   public static final String UPDATE_DUEDATESTO_COLON = "60";

   @RBEntry("Panoramica del processo")
   public static final String PROCESS_OVERVIEW = "61";

   @RBEntry("La panoramica del processo non è disponibile.")
   public static final String NO_PROCESS_OVERVIEW = "62";

   @RBEntry("Sospeso da")
   public static final String COMMENT_SUSPEND = "63";

   @RBEntry("Ripreso da")
   public static final String COMMENT_RESUME = "64";

   @RBEntry("Completato da")
   public static final String COMMENT_COMPLETE = "65";

   @RBEntry("Terminato da")
   public static final String COMMENT_TERMINATE = "66";

   @RBEntry("Istruzioni attività")
   public static final String ACTIVITY_INSTR = "67";

   @RBEntry("Notifica di scadenza oltrepassata")
   public static final String PAST_DEADLINE_NOTIFICATION = "68";

   @RBEntry("L'attività ha oltrepassato la scadenza.")
   public static final String PAST_DEADLINE_COMMENT = "69";

   @RBEntry("Punto d'arresto non trovato: {0}")
   public static final String NO_CHECKPOINT_FOUND = "70";
   
   @RBEntry("Business object principale:")
   public static final String PBO_LINK = "71";
   
   @RBEntry("Processo:")
   public static final String PM_LINK = "72";
}
