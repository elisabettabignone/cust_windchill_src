/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.workflow.robots;

import wt.util.resource.*;

@RBUUID("wt.workflow.robots.robotsResource")
public final class robotsResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile trovare {0}")
   public static final String URL_NOT_FOUND = "0";

   @RBEntry("Impossibile trovare l'amministratore")
   public static final String MISSING_ADMINISTRATOR = "1";

   @RBEntry("L'espressione restituita dal robot è falsa")
   public static final String RETURNED_FALSE = "2";

   @RBEntry("Il robot dell'applicazione {0} non ha funzionato. Messaggio di errore: {1}")
   public static final String RETURNED_NON_ZERO = "3";

   @RBEntry("Eccezione durante l'esecuzione del robot dell'applicazione {0}")
   public static final String EXECUTION_EXCEPTION = "4";

   @RBEntry("Chiave evento mancante nel robot {0}")
   public static final String MISSING_EVENT_KEY = "5";

   @RBEntry("Riga di comandi invalida nel robot dell'applicazione {0}")
   public static final String BAD_COMMAND_LINE = "6";

   @RBEntry("La variabile di destinazione {0} non è stata inizializzata")
   public static final String TARGET_VAR_NOT_SET = "7";

   @RBEntry("L'oggetto di destinazione trovato nella variabile {0} non è notificabile")
   public static final String TARGET_OBJECT_NOT_NOTIFIABLE = "8";

   @RBEntry("URL non corretto per il robot {0}")
   public static final String BAD_TASK_URL = "9";

   @RBEntry("L'URL per il robot {0} risultante dall'espansione della variabile non è corretto")
   public static final String BAD_EXEC_URL = "10";
}
