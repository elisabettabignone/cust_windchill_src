/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.workflow.robots;

import wt.util.resource.*;

@RBUUID("wt.workflow.robots.synchEventResource")
public final class synchEventResource_it extends WTListResourceBundle {
   @RBEntry("CHECK-IN")
   public static final String PRIVATE_CONSTANT_0 = "*/wt.vc.wip.WorkInProgressServiceEvent/POST_CHECKIN";

   @RBEntry("CHECK-OUT")
   public static final String PRIVATE_CONSTANT_1 = "*/wt.vc.wip.WorkInProgressServiceEvent/POST_CHECKOUT";

   @RBEntry("CREA")
   public static final String PRIVATE_CONSTANT_2 = "*/wt.fc.PersistenceManagerEvent/POST_STORE";

   @RBEntry("ELIMINA")
   public static final String PRIVATE_CONSTANT_3 = "*/wt.fc.PersistenceManagerEvent/POST_DELETE";

   @RBEntry("MODIFICA")
   public static final String PRIVATE_CONSTANT_4 = "*/wt.fc.PersistenceManagerEvent/POST_MODIFY";

   @RBEntry("BLOCCA")
   public static final String PRIVATE_CONSTANT_5 = "*/wt.locks.LockServiceEvent/POST_LOCK";

   @RBEntry("SBLOCCA")
   public static final String PRIVATE_CONSTANT_6 = "*/wt.locks.LockServiceEvent/POST_UNLOCK";

   @RBEntry("MODIFICA DI STATO DEL CICLO DI VITA")
   public static final String PRIVATE_CONSTANT_7 = "*/wt.lifecycle.LifeCycleServiceEvent/STATE_CHANGE";

   @RBEntry("ANNULLA CHECK-OUT")
   public static final String PRIVATE_CONSTANT_8 = "*/wt.vc.wip.WorkInProgressServiceEvent/POST_UNDO_CHECKOUT";

   @RBEntry("NUOVA VERSIONE")
   public static final String PRIVATE_CONSTANT_9 = "*/wt.vc.VersionControlServiceEvent/NEW_VERSION";

   @RBEntry("MODIFICA DOMINIO")
   public static final String PRIVATE_CONSTANT_10 = "*/wt.admin.AdministrativeDomainManagerEvent/POST_CHANGE_DOMAIN";

   @RBEntry("CAMBIA CARTELLA")
   public static final String PRIVATE_CONSTANT_11 = "*/wt.folder.FolderServiceEvent/POST_CHANGE_FOLDER";

   @RBEntry("CREAZIONE PROCESSO")
   public static final String PRIVATE_CONSTANT_12 = "*/wt.workflow.engine.WfEngineServiceEvent/PROCESS_CREATED";

   @RBEntry("CAMBIAMENTO STATO PROCESSO")
   public static final String PRIVATE_CONSTANT_13 = "*/wt.workflow.engine.WfEngineServiceEvent/PROCESS_STATE_CHANGED";

   @RBEntry("CAMBIAMENTO CONTESTO PROCESSO")
   public static final String PRIVATE_CONSTANT_14 = "*/wt.workflow.engine.WfEngineServiceEvent/PROCESS_CONTEXT_CHANGED";

   @RBEntry("CAMBIAMENTO ASSEGNAZIONE ATTIVITÀ")
   public static final String PRIVATE_CONSTANT_15 = "*/wt.workflow.engine.WfEngineServiceEvent/ACTIVITY_ASSIGNMENT_CHANGED";

   @RBEntry("CAMBIAMENTO CONTESTO ATTIVITÀ")
   public static final String PRIVATE_CONSTANT_16 = "*/wt.workflow.engine.WfEngineServiceEvent/ACTIVITY_CONTEXT_CHANGED";

   @RBEntry("CAMBIAMENTO RISULTATO ATTIVITÀ")
   public static final String PRIVATE_CONSTANT_17 = "*/wt.workflow.engine.WfEngineServiceEvent/ACTIVITY_RESULT_CHANGED";

   @RBEntry("CAMBIAMENTO STATO ATTIVITÀ")
   public static final String PRIVATE_CONSTANT_18 = "*/wt.workflow.engine.WfEngineServiceEvent/ACTIVITY_STATE_CHANGED";

   @RBEntry("CAMBIAMENTO RISULTATO PROCESSO")
   public static final String PRIVATE_CONSTANT_19 = "*/wt.workflow.engine.WfEngineServiceEvent/PROCESS_RESULT_CHANGED";

   @RBEntry("ERRORE D'ESECUZIONE")
   public static final String PRIVATE_CONSTANT_20 = "*/wt.workflow.engine.WfEngineServiceEvent/EXECUTION_ERROR";

   @RBEntry("FUORI MAGAZZINO")
   public static final String PRIVATE_CONSTANT_21 = "*/wt.workflow.engine.WfCustomEvent/OUT_OF_STOCK";

   @RBEntry("OVERFLOW")
   public static final String PRIVATE_CONSTANT_22 = "*/wt.fv.FvServiceEvent/OVERFLOW";

   @RBEntry("SUGGERIMENTO DI MODIFICA FORMALIZZATO")
   public static final String PRIVATE_CONSTANT_23 = "*/wt.change2.ChangeService2Event/ISSUE_FORMALIZED";

   @RBEntry("SUGGERIMENTO DI MODIFICA NON FORMALIZZATO")
   public static final String PRIVATE_CONSTANT_24 = "*/wt.change2.ChangeService2Event/ISSUE_UNFORMALIZED";

   @RBEntry("MODIFICA STATO OPERAZIONE DI MODIFICA")
   public static final String PRIVATE_CONSTANT_25 = "*/wt.change2.ChangeService2Event/CA_STATE_CHANGED";

   @RBEntry("MODIFICA STATO NOTIFICA DI MODIFICA")
   public static final String PRIVATE_CONSTANT_26 = "*/wt.change2.ChangeService2Event/CN_STATE_CHANGED";

   @RBEntry("IMPLEMENTAZIONE MODIFICHE")
   public static final String PRIVATE_CONSTANT_27 = "*/wt.change2.ChangeService2Event/CHANGE_IMPLEMENTATION";

   @RBEntry("COMPLETAMENTO RILASCIO ESI")
   public static final String PRIVATE_CONSTANT_28 = "*/com.ptc.windchill.esi.wf.ESIResultEvent/ESI_RELEASE_COMPLETE";

   @RBEntry("POST RILASCIO")
   public static final String PRIVATE_CONSTANT_29 = "*/com.ptc.windchill.esi.svc.ESIServiceEvent/POST_RELEASE";

   @RBEntry("RICHIESTA CAPA CREATA")
   public static final String PRIVATE_CONSTANT_30 = "*/com.ptc.qualitymanagement.capa.CAPAServiceEvent/CAPA_REQUEST_CREATED";

   @RBEntry("ATTIVITÀ CAPA COMPLETATA")
   public static final String PRIVATE_CONSTANT_31 = "*/com.ptc.qualitymanagement.capa.CAPAServiceEvent/CAPA_ACTIVITY_COMPLETED";

   @RBEntry("MODIFICA STATO CAPACHANGEACTIVITY")
   public static final String PRIVATE_CONSTANT_32 = "*/com.ptc.qualitymanagement.capa.CAPAServiceEvent/CAPACHANGEACTIVITY_STATE_CHANGE";

   @RBEntry("NC CREATA")
   public static final String PRIVATE_CONSTANT_33 = "*/com.ptc.qualitymanagement.nc.NCServiceEvent/NC_CREATED";

   @RBEntry("MODIFICA STATO DISPOSITIONACTION NC")
   public static final String PRIVATE_CONSTANT_34 = "*/com.ptc.qualitymanagement.nc.NCServiceEvent/NC_DISPOSITIONACTION_STATE_CHANGE";

   @RBEntry("STATO RISOLUZIONE DISPOSITIONACTION NC COMPLETATO")
   public static final String PRIVATE_CONSTANT_35 = "*/com.ptc.qualitymanagement.nc.NCServiceEvent/NC_DISPOSITIONACTION_DISPOSITION_STATE_COMPLETED";

   @RBEntry("RECLAMO CREATO")
   public static final String PRIVATE_CONSTANT_36 = "*/com.ptc.qualitymanagement.cem.CustomerExperienceServiceEvent/COMPLAINT_CREATED";

   @RBEntry("VALUTAZIONE RECLAMO COMPLETATA")
   public static final String PRIVATE_CONSTANT_37 = "*/com.ptc.qualitymanagement.cem.CustomerExperienceServiceEvent/COMPLAINT_EVALUATION_COMPLETED";

   @RBEntry("ATTIVITÀ CEM COMPLETATA")
   public static final String PRIVATE_CONSTANT_38 = "*/com.ptc.qualitymanagement.cem.CustomerExperienceServiceEvent/CEM_ACTIVITY_COMPLETED";

   @RBEntry("NOTIFICA DI MODIFICA CAPA COMPLETATA")
   public static final String PRIVATE_CONSTANT_39 = "*/com.ptc.qualitymanagement.capa.CAPAServiceEvent/CAPA_CHANGE_NOTICE_COMPLETED";
}
