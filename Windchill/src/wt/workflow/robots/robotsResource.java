/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.workflow.robots;

import wt.util.resource.*;

@RBUUID("wt.workflow.robots.robotsResource")
public final class robotsResource extends WTListResourceBundle {
   @RBEntry("{0} was not found")
   public static final String URL_NOT_FOUND = "0";

   @RBEntry("The administrator user is not found")
   public static final String MISSING_ADMINISTRATOR = "1";

   @RBEntry("Robot expression returned false")
   public static final String RETURNED_FALSE = "2";

   @RBEntry("Application robot {0} failed, error message is: {1}")
   public static final String RETURNED_NON_ZERO = "3";

   @RBEntry("Exception occurred during execution of application robot {0}")
   public static final String EXECUTION_EXCEPTION = "4";

   @RBEntry("Missing event key in robot {0}")
   public static final String MISSING_EVENT_KEY = "5";

   @RBEntry("Invalid command line in application robot {0}")
   public static final String BAD_COMMAND_LINE = "6";

   @RBEntry("Target variable was not initialized {0}")
   public static final String TARGET_VAR_NOT_SET = "7";

   @RBEntry("Target object found in variable {0} is not Notifiable")
   public static final String TARGET_OBJECT_NOT_NOTIFIABLE = "8";

   @RBEntry("Malformed URL for Robot {0}")
   public static final String BAD_TASK_URL = "9";

   @RBEntry("Malformed URL for Robot {0} following variable expansion")
   public static final String BAD_EXEC_URL = "10";
}
