/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.workflow.worklist;

import wt.util.resource.*;

@RBUUID("wt.workflow.worklist.worklistResource")
public final class worklistResource_it extends WTListResourceBundle {
    @RBEntry("Elemento {0} non corretto.")
    @RBComment("Translators--this is a message for developer use only :-)")
    public static final String BETH_MSG = "142";

    @RBEntry("Il parametro relativo al task non è stato trovato.")
    public static final String MISSING_PARAMETER = "1";

    @RBEntry("Istruzioni")
    public static final String INSTRUCTIONS = "2";

    @RBEntry("Descrizione")
    public static final String DESCRIPTION = "2a";

    @RBEntry(":")
    @RBPseudo(false)
    public static final String COLON = "3";

    @RBEntry("Nome variabile")
    public static final String VARIABLE_NAME = "4";

    @RBEntry("Valore")
    public static final String VALUE = "5";

    @RBEntry("Task di workflow")
    public static final String WORKFLOW_TASK_TITLE = "6";

    @RBEntry("Il task è stato completato.")
    public static final String WORKITEM_COMPLETE = "7";

    @RBEntry("Il task non è stato completato a causa dell'errore seguente")
    public static final String WORKITEM_COMPLETION_FAILED = "8";

    @RBEntry("L'oggetto di destinazione non è disponibile.")
    public static final String TARGET_OBJECT_GONE = "24";

    @RBEntry("Completa task")
    public static final String TASK_COMPLETE_BUTTON = "9";

    @RBEntry("Processo")
    public static final String PROCESS_NAME = "10";

    @RBEntry("Data di scadenza")
    public static final String DUE_DATE = "11";

    @RBEntry("Iniziatore processo")
    public static final String PROCESS_INITIATOR = "12";
    
    @RBEntry("Iniziatore processo:")
    public static final String PROCESS_INITIATOR_LABEL = "PROCESS_INITIATOR_LABEL";

    @RBEntry("Processo avviato")
    public static final String PROCESS_INITIATED = "12.1";

    @RBEntry("Assegnatario")
    public static final String ASSIGNEE = "13";

    @RBEntry("Oggetto di destinazione")
    public static final String TARGET_OBJECT = "14";

    @RBEntry("Aggiorna")
    public static final String UPDATE_LABEL = "19";

    @RBEntry("Contenuto")
    public static final String CONTENT_LABEL = "20";

    @RBEntry("Aggiorna il contenuto di {0}")
    public static final String CONTENT_LINK_LABEL = "118";

    @RBEntry("Istruzioni del processo")
    public static final String PROCESS_INSTRUCTIONS = "21";

    @RBEntry("Descrizione del processo")
    public static final String PROCESS_DESCRIPTION = "22";

    @RBEntry("Il task è stato completato e non è più valido")
    public static final String OBSOLETE_WORKITEM = "17";

    @RBEntry("Il task è stato sospeso.")
    public static final String SUSPENDED_WORKITEM_MSG = "81";

    @RBEntry("Non disponibile")
    public static final String NOT_AVAILABLE = "79";

    @RBEntry("(Sospeso)")
    public static final String SUSPENDED_WORKITEM_FLAG = "80";

    @RBEntry("Sì")
    public static final String YES = "82";

    @RBEntry("No")
    public static final String NO = "83";

    @RBEntry("True")
    public static final String TRUE = "134";

    @RBEntry("False")
    public static final String FALSE = "135";

    @RBEntry("Elimina")
    public static final String DELETE = "145";

    @RBEntry("Aggiorna")
    public static final String UPDATE = "146";

    @RBEntry("Crea attività ad hoc")
    public static final String CREATE_ADHOC_ACTIVITIES_LINK = "86";

    @RBEntry("Definisci progetti")
    public static final String DEFINE_PROJECTS_LINK = "113";

    @RBEntry("Definisci i partecipanti ai ruoli del progetto")
    public static final String SET_UP_PARTICIPANTS_LINK = "112";

    @RBEntry("Colonna {0}")
    @RBArgComment0("{0} - Number e.g. Column 1 Column 2")
    public static final String COLUMN = "138";

    @RBEntry("Origine")
    @RBComment("The data source of the worklist:  Current user's workitems or all workitems")
    public static final String WORKLIST_SOURCE = "139";

    @RBEntry("Salva con nome")
    public static final String SAVE_AS = "140";

    @RBEntry("Usa come impostazione predefinita")
    public static final String USE_AS_DEFAULT = "141";

    @RBEntry("Usa adesso")
    public static final String USE_NOW = "149";

    @RBEntry("Selettore task")
    public static final String WORKITEM_SELECTOR = "154";

    @RBEntry("(Delegato per {0})")
    public static final String DELEGATE_FOR = "167";

    /**
     * Buttons
     **/
    @RBEntry("Accetta")
    public static final String ACCEPT_BUTTON = "25";

    @RBEntry("Riassegna")
    public static final String REASSIGN_BUTTON = "26";

    @RBEntry("Aggiorna data di scadenza")
    public static final String UPDATE_DUE_DATE_BUTTON = "27";

    @RBEntry("Guida")
    public static final String HELP_BUTTON = "28";

    @RBEntry("Ricomponi elenco incarichi")
    @RBComment("Translator--This item was changed from \"Create Work List\" to \"Rebuild Work List\"")
    public static final String CREATE_WORKLIST_BUTTON = "30";

    @RBEntry("Invia")
    public static final String SUBMIT_BUTTON = "29";

    @RBEntry(" OK ")
    @RBComment("Translator--Please leave the leading spaces if this is a short (<5 char) word")
    public static final String OK_BUTTON = "148";

    @RBEntry("Avvia attività")
    public static final String START_ADHOC_ACTIVITIES_BUTTON = "87";

    @RBEntry("Altre attività")
    public static final String MORE_ACTIVITIES_BUTTON = "108";

    @RBEntry("Crea automaticamente notifica di modifica")
    public static final String AUTOMATE_FAST_TRACK_CHECKBOX = "225";

    @RBEntry("Azioni")
    public static final String ACTIONS_LABEL = "226";

    @RBEntry("Visualizza immagine processo lightweight")
    public static final String VIEW_LIGHTWEIGHT_PROCESS = "227";

    @RBEntry("Imposta partecipanti")
    @RBComment("Label displayed for the Set Up Participants icon on the workflow task page")
    public static final String PARTICIPANTS_LABEL = "228";

    @RBEntry("Partecipanti:")
    @RBComment("Participants inthe workflow task page")
    public static final String PARTICIPANTS = "229";

    /**
     * Labels & table headers --------------------------------------------------
     **/
    @RBEntry("Ordina per")
    public static final String SORT_BY = "31";

    @RBEntry("Raggruppa per")
    public static final String GROUP_BY = "32";

    @RBEntry("Task")
    public static final String WORKITEM_COLUMN_HEADER = "65";

    @RBEntry("Stato")
    public static final String STATUS_COLUMN_HEADER = "66";

    @RBEntry("Completato")
    public static final String COMPLETE_STATUS = "68";

    @RBEntry("Non riuscito")
    public static final String FAILED_STATUS = "69";

    @RBEntry("Assegnato a")
    public static final String ASSIGNED_TO = "73";

    @RBEntry("Tutti i task")
    public static final String ALL_WORKITEMS = "58";

    @RBEntry("Task aggiornati")
    public static final String UPDATED_WORKITEMS = "115";

    @RBEntry("Data di inizio")
    public static final String START_TIME = "231";

    @RBEntry("Data di completamento")
    public static final String END_TIME = "232";

    @RBEntry("Stato processo")
    public static final String PROCESS_STATUS = "233";

    /**
     * Titles ------------------------------------------------------------------
     **/
    @RBEntry("Elenco incarichi")
    public static final String WORKLIST = "35";

    @RBEntry("Elenco incarichi < {0} >")
    @RBArgComment0("{0}-Worklist Owner's name or \"All Workitems\"")
    public static final String WORKLIST_TITLE = "132";

    @RBEntry("Aggiorna date di scadenza task")
    public static final String UPD_DUE_DATE_TASK_TITLE = "67";

    @RBEntry("Riassegna task")
    public static final String REASSIGN_TASK_TITLE = "72";

    @RBEntry("Accetta task")
    public static final String ACCEPT_TASK_TITLE = "75";

    @RBEntry("Aggiorna partecipanti ai ruoli")
    public static final String AUGMENT_LINK_TITLE = "78";

    @RBEntry("Gestisci layout")
    public static final String MANAGE_LAYOUTS = "147";

    /**
     * ALT tags -----------------------------------------------------------------
     **/
    @RBEntry("Avvia processo")
    public static final String INITIATE_PROCESS = "101";

    @RBEntry("Processi in esecuzione")
    public static final String RUNNING_PROCESSES = "102";

    @RBEntry("Tutti i processi")
    public static final String ALL_PROCESSES = "103";

    @RBEntry("Cerca processi")
    public static final String SEARCH_FOR_PROCESSES = "111";

    @RBEntry("Processi completati")
    public static final String COMPLETED_PROCESSES = "104";

    @RBEntry("Gestisci processi")
    public static final String MANAGE_PROCESSES = "105";

    /**
     * Text & Messages ----------------------------------------------------------
     **/
    @RBEntry("Riassegna")
    public static final String REASSIGN = "33";

    @RBEntry("Riassegnati")
    public static final String REASSIGNED = "34";

    @RBEntry("Task - {0} {1}")
    @RBComment("Title display for the name of the task.")
    @RBArgComment0("The name of the task.  For example \"Submit Problem Report\"")
    @RBArgComment1("The primary business object display if available (may not be present).  Example: Problem Report: 001221 or something")
    public static final String TASK_NAME_TITLE = "163";

    @RBEntry("{0}-{1}")
    @RBComment("Display name of the WorkItem")
    @RBArgComment0("Name of the WFProcess, WorkItem belongs to.")
    @RBArgComment1("Name of the WFActivity of WorkItem.")
    public static final String WORKITEM_TITLE = "163.1";

    @RBEntry("Contenuto task")
    public static final String TASK_CONTENT = "164";

    @RBEntry("Forum di discussione")
    public static final String DISCUSSION_FORUM = "165";

    @RBEntry("Cronologia riassegnazione")
    @RBComment("Label for the reassignment history table in a task details page.")
    public static final String ACTIVITY_REASSIGNMENT_HISTORY = "166";

    @RBEntry("Non ci sono variabili di progetto definite in questa attività.")
    public static final String NO_PROJECTS_TO_DEFINE = "114";

    @RBEntry("Commenti")
    public static final String COMMENTS = "136";

    @RBEntry("L'autenticazione utente è stata cambiata da {0} a {1}. Il task {2} non è stato completato.")
    public static final String SIGNATURE_VALIDATION_FAILED = "137";

    @RBEntry("Accettare il task {0}?")
    @RBArgComment0("{0} - identifying attribute of the workitem to be accepted")
    public static final String ACCEPT_WORKITEM = "36";

    @RBEntry("{0} non è una data valida...Tornare alla pagina precedente e immettere nuovamente la data secondo il formato visualizzato.")
    @RBArgComment0("{o} - The badly formatted date")
    public static final String INVALID_DATE = "70";

    @RBEntry("L'aggiornamento della data di scadenza non è riuscito. Informazioni dettagliate sull'errore:")
    public static final String UPD_DUE_DATE_FAILED = "71";

    @RBEntry("Riassegnazione non riuscita. Errore:")
    public static final String REASSIGN_FAILED = "74";

    @RBEntry("Selezionare almeno un instradamento per completare questo task.")
    public static final String NO_ROUTE_SELECTED = "76";

    @RBEntry("Questo task non può essere completato perché l'attività padre è stata sospesa.")
    public static final String WORKITEM_IS_SUSPENDED = "77";

    @RBEntry("Immettere un valore per la variabile {0}.")
    @RBArgComment0("{0} - Name of the variable that was not supplied.")
    public static final String MISSING_REQUIRED_VARIABLE = "85";

    @RBEntry("I dati {0} devono essere di tipo {1}")
    @RBArgComment0("{0} - field name")
    @RBArgComment1("{1} - expected data type")
    public static final String INVALID_VARIABLE_TYPE = "18";

    @RBEntry("{0} non è un utente, gruppo, ruolo o progetto valido.")
    @RBArgComment0("{0} - assignee name")
    public static final String INVALID_ASSIGNEE = "88";

    @RBEntry("Non è stato selezionato alcun task. Utilizzare le caselle di controllo per indicare i task desiderati.")
    public static final String NO_WORKITEMS_SELECTED = "119";

    @RBEntry("Sono stati accettati {0} task.")
    @RBArgComment0("{0} - number of workitems accepted successfully")
    public static final String ACCEPT_SUCCESSFUL = "120";

    @RBEntry("È stato accettato {0} task.")
    @RBArgComment0("{0} - number of workitems accepted successfully (it should be 1--this is the singular case")
    public static final String ACCEPT_ONE_SUCCESSFUL = "121";

    @RBEntry("Accettazione dei task non riuscita. Errore: {0}")
    @RBArgComment0("{0} - the exception text of why the accepting  of the workitems failed for all selected workitems")
    public static final String ACCEPT_FAILED = "122";

    @RBEntry("Il task {0} non è stato accettato a causa dell'errore seguente: {1}")
    @RBArgComment0("{0} - identity of the workitem which could not be accepted")
    @RBArgComment1("{1} - exception text of why the workitem wasn't accepted")
    public static final String ACCEPT_WORKITEM_FAILED = "123";

    @RBEntry("Sono stati riassegnati {0} task.")
    @RBArgComment0("{0} - number of workitems reassigned successfully")
    public static final String REASSIGN_SUCCESSFUL = "124";

    @RBEntry("È stato riassegnato {0} task.")
    @RBArgComment0("{0} - number of workitems reassigned successfully (it should be 1--this is the singular case)")
    public static final String REASSIGN_ONE_SUCCESSFUL = "125";

    @RBEntry("La riassegnazione dei task è fallita. Errore: {0}")
    @RBArgComment0("{0} - the exception text of why the reassigning the workitems failed for all the selected workitems")
    public static final String REASSIGNMENT_FAILED = "126";

    @RBEntry("Il task {0} non è stato riassegnato a causa dell'errore seguente: {1}")
    @RBArgComment0("{0} - identity of the workitem which could not be reassigned")
    @RBArgComment1("{1} - exception text of why the workitem wasn't reasigned")
    public static final String REASSIGN_WORKITEM_FAILED = "127";

    @RBEntry("Sono stati aggiornati {0} task.")
    @RBArgComment0("{0} - number of workitems whose due date was successfully updated")
    public static final String UPDATE_DUE_DATE_SUCCESSFUL = "128";

    @RBEntry("È stato aggiornato {0} task.")
    @RBArgComment0("{0} - number of workitems whose due date was successfully updated (it should be 1--this is the singular case)")
    public static final String UPDATE_ONE_DUE_DATE_SUCCESSFUL = "129";

    @RBEntry("Aggiornamento delle scadenze dei task non riuscito. Errore: {0}")
    @RBArgComment0("{0} - the exception text of why the updating the workitems' due dates failed for all the selected workitems")
    public static final String UPDATE_DUE_DATE_FAILED = "130";

    @RBEntry("Il task {0} non è stato aggiornato a causa dell'errore seguente: {1}")
    @RBArgComment0("{0} - identity of the workitem whose due date could not be updated")
    @RBArgComment1("{1} - exception text of why the workitem wasn't updated")
    public static final String UPDATE_WORKITEM_DUE_DATE_FAILED = "131";

    @RBEntry("Una procedura activityVariable personalizzata fa riferimento alla variabile \"{0}\". Nessuna variabile con quel nome viene definita in questa attività.")
    @RBArgComment0("{0} - The user defined name of the variable that could not be found")
    public static final String MISSING_ACTIVITY_VARIABLE = "133";

    @RBEntry("Il layout \"{0}\" è stato eliminato.")
    @RBArgComment0("{0} - The name of the deleted layout")
    public static final String DELETED_LAYOUT = "143";

    @RBEntry("Il layout \"{0}\" non è stato eliminato.")
    @RBArgComment0("{0} - The name of the layout that should have been deleted")
    public static final String LAYOUT_NOT_DELETED = "144";

    @RBEntry("Prima di salvare, attribuire un nome al layout.")
    public static final String NO_LAYOUT_NAME = "150";

    @RBEntry("Il campo selezionato per il raggruppamento deve essere compreso nelle colonne definite in questo layout.")
    public static final String GROUP_KEY_NOT_IN_LAYOUT = "151";

    @RBEntry("Il campo selezionato per l'ordinamento deve essere compreso nelle colonne definite in questo layout.")
    public static final String SORT_KEY_NOT_IN_LAYOUT = "152";

    @RBEntry("Nel layout deve essere definita almeno una colonna.")
    public static final String NO_COLUMNS_DEFINED = "153";

    /**
     * Attribute table headers ---------------------------------------------------
     **/
    @RBEntry("Id")
    public static final String ID = "37";

    @RBEntry("Ruolo")
    public static final String ROLE = "38";

    @RBEntry("Proprietario")
    public static final String OWNER = "39";

    @RBEntry("Task")
    public static final String TASK = "40";

    @RBEntry("Origine")
    public static final String SOURCE = "41";

    @RBEntry("Priorità")
    public static final String PRIORITY = "42";

    @RBEntry("Obbligatorio")
    public static final String REQUIRED = "43";

    @RBEntry("Attore")
    public static final String ACTOR_NAME = "44";

    @RBEntry("Accettato")
    public static final String ACCEPTED = "57";

    @RBEntry("Completato")
    public static final String COMPLETED = "45";

    @RBEntry("Progetto")
    public static final String PROJECT = "46";

    @RBEntry("Tipo")
    public static final String TYPE = "47";

    @RBEntry("Stato (task)")
    public static final String STATUS = "84";

    @RBEntry("Scadenza")
    public static final String DEADLINE = "48";

    @RBEntry("Avvio attività")
    public static final String ACTIVITY_START = "49";

    @RBEntry("Nome attività")
    public static final String ACTIVITY_NAME = "50";

    @RBEntry("Descrizione attività")
    public static final String ACTIVITY_DESCRIPTION = "51";

    @RBEntry("Nome workflow")
    public static final String WORKFLOW_NAME = "54";

    @RBEntry("Scadenza workflow")
    public static final String WORKFLOW_DEADLINE = "52";

    @RBEntry("Avvio workflow")
    public static final String WORKFLOW_START = "53";

    @RBEntry("Descrizione workflow")
    public static final String WORKFLOW_DESCRIPTION = "55";

    @RBEntry("Oggetto")
    @RBComment(" Translater - this changed from \"Primary Business Object\" to subject")
    public static final String PRIMARY_BUSINESS_OBJECT = "56";

    @RBEntry("Tipo")
    public static final String PRIMARY_BUSINESS_OBJECT_TYPE = "116";

    @RBEntry("Stato del ciclo di vita")
    public static final String PRIMARY_BUSINESS_OBJECT_STATE = "117";


    @RBEntry("Oggetto dell'assegnazione")
    @RBComment("Primary Business Object of an assignment(task). Used to display column in search table.")
    public static final String SEARCH_SUBJECT_OF_ASSIGNMENT = "SEARCH_SUBJECT_OF_ASSIGNMENT";

    /**
     * Workitem priorities -------------------------------------------------------
     **/
    @RBEntry("Massima")
    public static final String PRIORITY_ONE = "59";

    @RBEntry("Alta")
    public static final String PRIORITY_TWO = "60";

    @RBEntry("Normale")
    public static final String PRIORITY_THREE = "61";

    @RBEntry("Bassa")
    public static final String PRIORITY_FOUR = "62";

    @RBEntry("Minima")
    public static final String PRIORITY_FIVE = "63";

    /**
     * ---------------------------------------------------------------------------
     **/
    @RBEntry("ID")
    public static final String AD_HOC_ACTIVITY_ID = "89";

    @RBEntry("Nome attività")
    public static final String AD_HOC_ACTIVITY_NAME = "90";

    @RBEntry("Assegnatario")
    public static final String AD_HOC_ASSIGNEE = "91";

    @RBEntry("Offerta")
    public static final String AD_HOC_OFFER = "92";

    @RBEntry("Durata")
    public static final String AD_HOC_DURATION = "93";

    @RBEntry("Predecessori")
    public static final String AD_HOC_PREDECESSORS = "94";

    @RBEntry("Task")
    public static final String AD_HOC_TASK = "95";

    @RBEntry("Istruzioni:")
    public static final String AD_HOC_INSTRUCTIONS = "96";

    @RBEntry("Invia notifica e-mail")
    public static final String AD_HOC_EMAIL_NOTIFICATION = "97";

    @RBEntry("Le attività sono state avviate")
    public static final String AD_HOC_COMPLETE = "100";

    @RBEntry("Le attività sono in esecuzione al momento")
    public static final String AD_HOC_IN_PROGRESS = "106";

    @RBEntry("I processi circolari non sono ammessi. Controllare che i predecessori non abbiano dipendenze circolari")
    public static final String AD_HOC_CYCLIC_ERROR = "107";

    @RBEntry("Non sono state trovate attività.")
    public static final String AD_HOC_LIST_EMPTY = "109";

    @RBEntry("Il predecessore {0} è fuori dai limiti")
    @RBArgComment0(" {0} is the id of the predecessor that was entered out of bounds.")
    public static final String AD_HOC_OUT_OF_BOUNDS = "110";

    @RBEntry("Le attività ad hoc sono state create.")
    public static final String Ad_HOC_CREATED_SUCCESSFULLY = "adhocCreatedSuccessfully";

    @RBEntry("*Password")
    public static final String PASSWORD = "PASSWORD";

    /**
     * Ad hoc labels --------------------------------------------------------------
     **/
    @RBEntry("Progetto:")
    public static final String AD_HOC_PROJECT_LABEL = "98";

    @RBEntry("Task padre:")
    public static final String AD_HOC_PARENT_TASK_LABEL = "99";

    /**
     * Trouble: Exceptions & Messages ---------------------------------------------
     **/
    @RBEntry("Impossibile recuperare l'attività padre per il task selezionato.")
    public static final String MISSING_PARENT = "64";

    /**
     * Notifications
     **/
    @RBEntry("Nome progetto:")
    public static final String NOTIFICATION_PROJECT_NAME = "200";

    @RBEntry("Autore progetto:")
    public static final String NOTIFICATION_PROJECT_CREATOR = "201";

    @RBEntry("Proprietario progetto:")
    public static final String NOTIFICATION_PROJECT_OWNER = "201a";

    @RBEntry("Organizzazione host:")
    public static final String NOTIFICATION_PROJECT_HOST = "202";

    @RBEntry("Descrizione progetto:")
    public static final String NOTIFICATION_PROJECT_DESC = "203";

    @RBEntry("Nessuno")
    public static final String NOTIFICATION_NONE = "204";

    @RBEntry("È stata ricevuta l'assegnazione di un task di tipo <A HREF=\"{0}\">{1} </A>.")
    @RBComment(" This is a line of text that is seen at the top of a workflow notification email that contains a hyperlink to the workflow details page in a Windchill system.  The translation can be found in the <BODY> element of the 7.0 WCL10N template srcwtworkflowhtmltmplworkNotificationGeneral*.html")
    @RBArgComment0("This is the url to the details page for the workflow task.  This is not seen by the user.")
    @RBArgComment1("This is the type of the workflow task that is generated by the system. Examples of this are Submit and Approve.  The final produced string would look something like the following where Submit is the variable for arg1 and Submit task is a hyperlink; You have been assigned a Submit task.")
    public static final String NOTIFICATION_TASK_URL = "205";

    @RBEntry("Assegnazione ricevuta in qualità di delegato per {0}")
    public static final String NOTIFICATION_DELEGATE_FOR = "206";

    @RBEntry("Il task è stato già completato.")
    public static final String TASK_COMPLETE = "230";

    /**
     * Online help ----------------------------------------------------------------
     **/
    @RBEntry("wt/workflow/help_it/NotificationHelp.html")
    public static final String PRIVATE_CONSTANT_0 = "Help/WorkNotification/MainHelp";

    @RBEntry("...Localizzato...")
    public static final String PRIVATE_CONSTANT_1 = "TestString";

    /**
     * ---------------------------------------------------------------------------
     **/
    @RBEntry("Il layout non può contenere \"/\".")
    public static final String INVALID_LAYOUT_NAME = "155";

    /**
     * Teams ----------------------------------------------------------------------
     **/
    @RBEntry("Team")
    public static final String TEAM = "156";

    @RBEntry("Definisci team")
    public static final String DEFINE_TEAMS_LINK = "157";

    @RBEntry("Non ci sono variabili di team definite in questa attività.")
    public static final String NO_TEAMS_TO_DEFINE = "158";

    @RBEntry("Team:")
    public static final String AD_HOC_TEAM_LABEL = "159";

    @RBEntry("Nome team:")
    public static final String NOTIFICATION_TEAM_NAME = "160";

    @RBEntry("Autore team:")
    public static final String NOTIFICATION_TEAM_CREATOR = "161";

    @RBEntry("Descrizione team:")
    public static final String NOTIFICATION_TEAM_DESC = "162";

    /**
     * ---------------------------------------------------------------------------- Preferences
     **/
    @RBEntry("Ordina per data d'inizio")
    @RBComment("Localized name for Sort By Start worklist")
    public static final String PRIVATE_CONSTANT_2 = "SORT_BY_START_NAME";

    @RBEntry("La preferenza Ordina per data d'inizio viene usata per ordinare gli incarichi per data d'inizio.")
    @RBComment("Localized description for Sort By Start worklist")
    public static final String PRIVATE_CONSTANT_3 = "SORT_BY_START_DESC";

    @RBEntry("Default di sistema")
    @RBComment("Localized name for System Default worklist")
    public static final String PRIVATE_CONSTANT_4 = "SYSTEM_DEFAULT_NAME";

    @RBEntry("Elenco incarichi del default di sistema")
    @RBComment("Localized description for System Default worklist")
    public static final String PRIVATE_CONSTANT_5 = "SYSTEM_DEFAULT_DESC";

    /**
     * ---------------------------------------------------------------------------- Labels for Manage Layout page
     **/
    @RBEntry("Gruppo:")
    public static final String GROUP_LABEL = "220";

    @RBEntry("Ordina:")
    public static final String SORT_LABEL = "221";

    @RBEntry("Layout definiti")
    public static final String DEFINED_LAYOUTS_LABEL = "222";

    @RBEntry("Stato a cui è richiesta la promozione")
    public static final String REQUESTED_PROMOTION_STATE = "223";

    @RBEntry("Layout:")
    public static final String LAYOUTS_LABEL = "224";

    /**
     * ------------------------------------------------------ Preferences for assignments table
     **/
    @RBEntry("Tabella assegnazioni")
    @RBComment("Assignments table related preferences.")
    public static final String ASSIGNMENTS_TABLE_CAT_NAME = "ASSIGNMENTS_TABLE_CAT_NAME";

    @RBEntry("Preferenze correlate alla tabella delle assegnazioni.")
    public static final String ASSIGNMENTS_TABLE_CAT_DESCR = "ASSIGNMENTS_TABLE_CAT_DESCR";

    @RBEntry("Tabella assegnazioni in Home --> Panoramica")
    @RBComment("Preferences for Assignments table at Home-->Overview.")
    public static final String HOME_OVRV_ASSIGNMENTS_TABLE_CAT_NAME = "HOME_OVRV_ASSIGNMENTS_TABLE_CAT_NAME";

    @RBEntry("Preferenze per la tabella delle assegnazioni in Home --> Panoramica.")
    public static final String HOME_OVRV_ASSIGNMENTS_TABLE_CAT_DESCR = "HOME_OVRV_ASSIGNMENTS_TABLE_CAT_DESCR";

    /**
     * removed QUERY_LIMIT_PREF ------------------------------------------------------
     **/
    @RBEntry("Le modifiche non salvate andranno perse. Salvare la pagina dei task per non perdere le modifiche.")
    @RBComment("Confirmation message while setup participant table expansion on task detail page")
    public static final String UNSAVED_CHANGES_WARNING = "234";

    @RBEntry("Per avviare l'attività è necessario almeno un nome di attività e un assegnatario.")
    @RBComment("Validation error message when Adhoc activities are started without specifying Name and Assignee")
    public static final String ADHOC_VALIDATION_ERROR1 = "235";

    @RBEntry("Per avviare le attività è necessario completare i campi Nome attività e Assegnatario.")
    @RBComment("Validation error message when Adhoc activities are started without specifying Name and Assignee")
    public static final String ADHOC_VALIDATION_ERROR2 = "236";

    /*
     * Labels for Task Assistant
     */

    @RBEntry("Opzioni di instradamento")
    @RBComment("Task Routing Options")
    public static final String TASK_ROUTING = "237";

    @RBEntry("Opzioni di voto")
    @RBComment("Task voting options")
    public static final String TASK_VOTING = "238";

    @RBEntry("Le modifiche non salvate apportate a \"{0}\" andranno perse. Aprire il task \"{1}\"?")
    @RBComment("Task Assistant confirmation for existing tracked task assistant")
    public static final String TASKBAR_CONFIRMATION = "239";

    @RBEntry("Nome")
    public static final String WORKITEM_NAME = "250";

    @RBEntry("Contesto")
    public static final String WORKITEM_CONTEXT = "251";

    @RBEntry("Assistente task")
    @RBComment("Title of task assistant")
    public static final String TASK_WINDOW_TITLE = "TASK_WINDOW_TITLE";

    @RBEntry("Oggetto:")
    @RBComment("Label for task assistant PBO")
    public static final String TASK_WINDOW_SUBJECT = "TASK_WINDOW_SUBJECT";

    @RBEntry("Task:")
    @RBComment("Label for workitem name on task assistant")
    public static final String TASK_WINDOW_TASK = "TASK_WINDOW_TASK";

    @RBEntry("Completa task")
    @RBComment("Label for complete task button on task assistant")
    public static final String TASK_WINDOW_COMPLETE_TASK_BUTTON = "TASK_WINDOW_COMPLETE_TASK_BUTTON";

    @RBEntry("Salva")
    @RBComment("Label for save button on task assistant")
    public static final String TASK_WINDOW_SAVE_BUTTON = "TASK_WINDOW_SAVE_BUTTON";

    @RBEntry("Continua completamento task")
    @RBComment("Label for continue button on task assistant")
    public static final String TASK_WINDOW_CONTINUE_BUTTON = "TASK_WINDOW_CONTINUE_BUTTON";
    
    @RBEntry("Il commento è stato salvato.")
    @RBComment("Message after comment is saved from task assistant")
    public static final String TASK_WINDOW_SAVE_MESSAGE = "TASK_WINDOW_SAVE_MESSAGE";

    @RBEntry("Task completato.")
    @RBComment("Message after task is completed from task assistant")
    public static final String TASK_WINDOW_COMPLETE_MESSAGE = "TASK_WINDOW_COMPLETE_MESSAGE";

    @RBEntry("Modifiche non salvate. Chiudere l'assistente task?")
    @RBComment("Confirmation message to close task assistant")
    public static final String TASK_WINDOW_CLOSE_CONFIRMATION_MESSAGE = "TASK_WINDOW_CLOSE_CONFIRMATION_MESSAGE";

    /*
     * Labels for task details page
     */

    @RBEntry("Stato processo")
    public static final String workitem_routingStatus_description = "workitem.routingStatus.description";

    @RBEntry("Stato processo")
    public static final String workitem_routingStatus_tooltip = "workitem.routingStatus.tooltip";

    @RBEntry("newproject.gif")
    @RBPseudo(false)
    @RBComment("DO NOT TRANSLATE")
    public static final String workitem_routingStatus_icon = "workitem.routingStatus.icon";

    @RBEntry("Dettagli")
    @RBComment("The Details tab on the info page for WorkItem")
    public static final String workitem_taskDetailsDetails_description = "object.taskDetailsDetails.description";

    @RBEntry("Vista modello")
    @RBComment("The main tab on the info page for WorkItem which will render task form template")
    public static final String workitem_taskFormTemplateDetails_description = "object.taskFormTemplateDetails.description";

    @RBEntry("Correlato")
    @RBComment("The Others tab on the info page for WorkItem")
    public static final String workitem_taskDetailsOthers_description = "object.taskDetailsOthers.description";

    @RBEntry("Attributi")
    @RBComment("The Attributes menu on the info page for WorkItem")
    public static final String workitem_attributes_description = "workitem.attributes.description";

    @RBEntry("Blocco note")
    @RBComment("The Notebook menu on the info page for WorkItem")
    public static final String workitem_notebook_description = "workitem.notebook.description";

    @RBEntry("Discussioni")
    @RBComment("The Discussions menu on the info page for WorkItem")
    public static final String workitem_discussions_description = "workitem.discussions.description";

    @RBEntry("Imposta partecipanti")
    @RBComment("The Set Up Participant tab on the info page for WorkItem")
    public static final String workitem_taskDetailsSetupParticipant_description = "object.taskDetailsSetupParticipant.description";

    @RBEntry("Attività ad hoc")
    @RBComment("The Adhoc Activities tab on the info page for WorkItem")
    public static final String workitem_taskDetailsAdhoc_description = "object.taskDetailsAdhoc.description";

    @RBEntry("Attività ad hoc")
    @RBComment("The Adhoc Activities customization action")
    public static final String workitem_adhocActivities_description = "workitem.adhocActivities.description";

    @RBEntry("Imposta partecipanti")
    @RBComment("Set Up PArticipant customization action")
    public static final String workitem_setupParticipant_description = "workitem.setupParticipant.description";

    @RBEntry("insert_multi_rows_below.gif")
    @RBPseudo(false)
    public static final String workitem_addMultipleObjects_icon = "workitem.addMultipleObjects.icon";
    
    @RBEntry("Aggiungi 5 righe")
    @RBComment("Action to add 5 empty rows")
    public static final String workitem_addMultipleObjects_description = "workitem.addMultipleObjects.description";

    @RBEntry("Istruzioni speciali")
    @RBComment("Special instructions is a special Workflow veriable which is used in Change related Workflow.")
    public static final String SPECIAL_INSTRUCTIONS_LABEL = "special_instructions_label";

    @RBEntry("Vista modello")
    @RBComment("Label for Task Form Template action")
    public static final String workitem_taskFormTemplate_description = "workitem.taskFormTemplate.description";

    @RBEntry("Questo è un task \"Attività ad hoc\". Utilizzare la scheda \"Attività ad hoc\" per creare e assegnare nuove attività ad hoc.")
    @RBComment("Inline help contents for ad hoc type of activity.")
    public static final String ADHOC_INLINE_HELP = "ADHOC_INLINE_HELP";

    @RBEntry("Questo è un task \"Imposta partecipanti\". Utilizzare la scheda Imposta partecipanti per assegnare partecipanti ai vari ruoli del processo.")
    @RBComment("Inline help contents for set up participant type of activity.")
    public static final String SETUP_INLINE_HELP = "SETUP_INLINE_HELP";

    @RBEntry("Questo task viene sottoposto a rendering utilizzando il modello di modulo task definito per questo tipo di task e il tipo di business object primario.")
    @RBComment("Inline help contents if task form template preference is set to true.")
    public static final String TEMPLATE_VIEW_INLINE_HELP = "TEMPLATE_VIEW_INLINE_HELP";

    @RBEntry("Questo task è stato impostato come task \"Attività ad hoc\" e task \"Imposta partecipanti\". Utilizzare la scheda \"Attività ad hoc\" per creare e assegnare nuove attività ad hoc. Utilizzare la scheda Imposta partecipanti per assegnare partecipanti ai vari ruoli del processo.")
    @RBComment("Inline help contents for set up as well as ad hoc type of activity.")
    public static final String SETUP_ADHOC_INLINE_HELP = "SETUP_ADHOC_INLINE_HELP";
}
