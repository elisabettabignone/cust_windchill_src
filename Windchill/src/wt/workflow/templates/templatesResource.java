/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.workflow.templates;

import wt.util.resource.*;

@RBUUID("wt.workflow.templates.templatesResource")
public final class templatesResource extends WTListResourceBundle {
   @RBEntry("Cannot import into a container that is not yet persisted.")
   public static final String CANT_IMPORT_INTO_UNPERSISTED_CONTAINER = "10";

   @RBEntry("{0} is not a subclass of {1}")
   public static final String UNSUPPORTED_TEMPLATE_TYPE = "20";

   @RBEntry("Cannot operation on a null container template")
   public static final String CONTAINER_TEMPLATE_CANNOT_BE_NULL = "30";

   @RBEntry("A template has already been assigned to the container {0}")
   public static final String CONTAINER_ALREADY_HAS_A_TEMPLATE = "40";

   @RBEntry("Template {0} is already in use by {1} containers, it cannot be deleted")
   public static final String TEMPLATE_IN_USE = "50";

   @RBEntry("There is more than a single latest iteration contact your administrator")
   public static final String MORE_THAN_ONE_LATEST = "60";

   @RBEntry("Cannot get template from a null definition")
   public static final String CANT_GET_TEMPLATE_FROM_NULL_DEFN = "70";

   @RBEntry("Illegal definition class: {0}")
   public static final String ILLEGAL_DEFN_CLASS = "80";

   @RBEntry("A non-null template reference is required")
   public static final String CANT_TAKE_NULL_TEMPLATE_REF = "90";

   @RBEntry("Cannot upload a null request")
   public static final String CANT_UPLOAD_NULL_BUSINESS_REQUEST = "100";

   @RBEntry("Template already has business xml assigned to it, please use updateBusinessXML to merge XML.")
   public static final String THIS_METHOD_WONT_CLOBBER_EXISTING_XML = "110";

   @RBEntry("{0} has already been persisted, it cannot be created again")
   public static final String TEMPLATE_ALREADY_PERSISTED = "120";

   @RBEntry("Template reference holds a null template instance")
   public static final String NULL_TEMPLATE_NON_NULL_TEMPLATE_REF = "130";

   @RBEntry("Template name cannot be null")
   public static final String NAME_CANNOT_BE_NULL = "140";

   @RBEntry("Target container class cannot be null")
   public static final String TARGET_CONTAINER_CLASS_CANNOT_BE_NULL = "150";

   @RBEntry("Found more than one template with name: {0} containerClass {1} in container {2} please contact your administrator")
   public static final String TOO_MANY_TEMPLATES_OF_NAME_AND_CONTCLASS_IN_CONTAINER_CONTACT_ADMIN = "160";

   @RBEntry("Cannot find a template with a null class object")
   public static final String CANNOT_FIND_TEMPLATE_WITHOUT_TARGET_CLASS = "170";

   @RBEntry("Cannot get template from a null definition")
   public static final String CANNOT_GET_TEMPLATE_FROM_NULL_DEFINITION = "180";

   @RBEntry("Unknown container definition class: {0}")
   public static final String UNSUPPORTED_TYPE_OF_DEFINITION = "190";

   @RBEntry("Illegal system xml category: {0}")
   public static final String ILLEGAL_SYSTEM_XML_CATEGORY = "200";

   @RBEntry("{0} is not a valid instance of {1}")
   public static final String NOT_A_VALID_TASK_FORM_TEMPLATE_CLASS = "210";

   @RBEntry("{0} is not in the codebase")
   public static final String CLASS_NOT_IN_CODEBASE = "220";

   @RBEntry("Merging of lookupspec queryspec not supported")
   public static final String MERGE_OF_QUERYSPEC_NOT_SUPPORTED = "230";

   @RBEntry("Illegal business request, encoding must be specified")
   public static final String XML_REQUEST_NEEDS_ENCODING = "240";

   @RBEntry("BUSINESS_JAR is not ApplicationData object, notify your administrator")
   public static final String BUSSINESS_JAR_NOT_APP_DATA = "250";

   @RBEntry("{0} is not present in the codebase, contact your administrator")
   public static final String DTD_NOT_FOUND = "260";

   @RBEntry("The business XML for the template {0}")
   public static final String BUSS_JAR_CONTENT_DESCR = "270";

   @RBEntry("Added business jar")
   public static final String BUSS_JAR_CHECKIN_COMMENT = "280";

   @RBEntry("The classname of the WTContainer is a required attribute for a Context template")
   public static final String CONTAINER_CLASSNAME_CANNOTBENULL = "290";

   @RBEntry("Templates cannot be physically deleted.")
   public static final String TEMPLATES_CANT_BE_PHYSICALLY_DELETED = "300";

   @RBEntry("Parent container cannot be null")
   public static final String PARENT_CONTAINER_CANNOT_BE_NULL = "310";

   @RBEntry("A container reference pointed to a null container, contact your administrator")
   public static final String NON_NULL_CONT_REF_POINTING_TO_A_NULL_CONTAINER = "320";

   @RBEntry("A non persistent container was found, contact your administrator")
   public static final String NON_PERSISTENT_CONTAINER = "330";

   @RBEntry("Template locale cannot be null")
   public static final String LOCALE_CANNOT_BE_NULL = "340";

   @RBEntry("Master has no iterations")
   public static final String MASTER_HAS_NO_ITERATIONS = "350";

   @RBEntry("Cannot update a non-working copy of a checked out template")
   public static final String NON_WORKING_COPY_CANNOT_BE_UPDATED = "360";

   @RBEntry("Cannot operate with a null container reference")
   public static final String CONTAINER_REF_CANNOT_BE_NULL = "370";

   @RBEntry("Unable to find generator for context class {0}, contact your administrator")
   public static final String UNABLE_TO_FIND_TEMPLATE_GENERATOR = "390";

   @RBEntry("Unable to find copier for context class {0}, contact your administrator")
   public static final String UNABLE_TO_FIND_CONTAINER_COPIER = "400";

   @RBEntry("Unable to find default values applier for context class {0}, contact your administrator")
   public static final String UNABLE_TO_FIND_VALUES_DELEGATE = "410";

   @RBEntry("Unable to find template request factory for context class {0}, contact your administrator")
   public static final String UNABLE_FIND_TEMPLATE_REQUEST_FACTORY = "420";

   @RBEntry("Unable to find template request processor factory for context class {0}, contact your administrator")
   public static final String UNABLE_TO_FIND_TMPL_REQ_PROCESSOR_FACTORY = "430";

   @RBEntry("The Context name {0} is invalid for Context Copy.  Context names cannot contain any of the following characters:  {1}")
   public static final String INVALID_CHARACTERS_IN_CONTEXT_COPY_NAME = "440";

   @RBEntry("Cannot replace XML unless the template is checked out")
   public static final String TEMPLATE_NEEDS_2_B_CHKED_OUT_TO_RPLC_XML = "450";

   @RBEntry("Illegal file passed to load container template, this requires an XML file of doctype NMLoader with a tag of csvCreateContainerTemplate")
   public static final String ILLEGAL_LOAD_FILE = "460";

   @RBEntry("{0}.zip")
   public static final String BUSINESS_JAR_DOWNLOAD_NAME = "470";

   @RBEntry("Unable to fill in the attributes for the Task Form Template in the object.")
   public static final String INFLATE_FAILED = "89";

   @RBEntry("Unable to get input stream for template, contact your administrator")
   public static final String UNABLE_TO_GET_INPUTSTREAM = "480";

   @RBEntry("Template is not ApplicationData object, notify your administrator")
   public static final String NOT_APPLICATION_DATA = "490";
}
