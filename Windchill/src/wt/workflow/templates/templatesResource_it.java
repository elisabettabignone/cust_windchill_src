/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.workflow.templates;

import wt.util.resource.*;

@RBUUID("wt.workflow.templates.templatesResource")
public final class templatesResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile importare in un contenitore non reso persistente.")
   public static final String CANT_IMPORT_INTO_UNPERSISTED_CONTAINER = "10";

   @RBEntry("{0} non è una sottoclasse di {1}")
   public static final String UNSUPPORTED_TEMPLATE_TYPE = "20";

   @RBEntry("Impossibile operare su un modello di contenitore nullo")
   public static final String CONTAINER_TEMPLATE_CANNOT_BE_NULL = "30";

   @RBEntry("Un modello è già stato assegnato al contenitore {0}")
   public static final String CONTAINER_ALREADY_HAS_A_TEMPLATE = "40";

   @RBEntry("Il modello {0} viene usato da {1} contenitori. Impossibile eliminarlo")
   public static final String TEMPLATE_IN_USE = "50";

   @RBEntry("Esiste più di una utlima iterazione. Contattare l'amministratore.")
   public static final String MORE_THAN_ONE_LATEST = "60";

   @RBEntry("Impossibile ottenere un modello da una definizione nulla")
   public static final String CANT_GET_TEMPLATE_FROM_NULL_DEFN = "70";

   @RBEntry("Classe di definizione non valida: {0}")
   public static final String ILLEGAL_DEFN_CLASS = "80";

   @RBEntry("Un riferimento di modello non nullo è obbligatorio")
   public static final String CANT_TAKE_NULL_TEMPLATE_REF = "90";

   @RBEntry("Impossibile caricare una richiesta nulla")
   public static final String CANT_UPLOAD_NULL_BUSINESS_REQUEST = "100";

   @RBEntry("Al modello è già stato assegnato un xml aziendale. Utilizzare updateBusinessXML per unire i file XML.")
   public static final String THIS_METHOD_WONT_CLOBBER_EXISTING_XML = "110";

   @RBEntry("{0} è già stato reso persistente. Non può essere creato nuovamente.")
   public static final String TEMPLATE_ALREADY_PERSISTED = "120";

   @RBEntry("Il riferimento del modello contiene un'istanza di modello nulla")
   public static final String NULL_TEMPLATE_NON_NULL_TEMPLATE_REF = "130";

   @RBEntry("Il nome del modello non può essere nullo")
   public static final String NAME_CANNOT_BE_NULL = "140";

   @RBEntry("Il contenitore di destinazione non può essere nullo")
   public static final String TARGET_CONTAINER_CLASS_CANNOT_BE_NULL = "150";

   @RBEntry("È stato trovato più di un modello denominato {0} di classe {1} nel contenitore {2}. Contattare l'amministratore")
   public static final String TOO_MANY_TEMPLATES_OF_NAME_AND_CONTCLASS_IN_CONTAINER_CONTACT_ADMIN = "160";

   @RBEntry("Impossibile trovare un modello con un oggetto di classe nulla")
   public static final String CANNOT_FIND_TEMPLATE_WITHOUT_TARGET_CLASS = "170";

   @RBEntry("Impossibile ottenere un modello da una definizione nulla")
   public static final String CANNOT_GET_TEMPLATE_FROM_NULL_DEFINITION = "180";

   @RBEntry("Classe di definizione contenitore sconosciuta: {0}")
   public static final String UNSUPPORTED_TYPE_OF_DEFINITION = "190";

   @RBEntry("Categoria non valida di xml nel sistema: {0}")
   public static final String ILLEGAL_SYSTEM_XML_CATEGORY = "200";

   @RBEntry("{0} non è un'istanza valida di {1}")
   public static final String NOT_A_VALID_TASK_FORM_TEMPLATE_CLASS = "210";

   @RBEntry("{0} non presente nel codebase")
   public static final String CLASS_NOT_IN_CODEBASE = "220";

   @RBEntry("L'unione della specifica di ricerca e della specifica di interrogazione non è supportata")
   public static final String MERGE_OF_QUERYSPEC_NOT_SUPPORTED = "230";

   @RBEntry("Richiesta non valida. Specificare la codifica.")
   public static final String XML_REQUEST_NEEDS_ENCODING = "240";

   @RBEntry("BUSINESS_JAR non è un oggetto ApplicationData. Notificare l'amministratore")
   public static final String BUSSINESS_JAR_NOT_APP_DATA = "250";

   @RBEntry("{0} non presente nel codebase. Contattare l'amministratore")
   public static final String DTD_NOT_FOUND = "260";

   @RBEntry("XML aziendale per il modello {0}")
   public static final String BUSS_JAR_CONTENT_DESCR = "270";

   @RBEntry("Jar aziendale aggiunto")
   public static final String BUSS_JAR_CHECKIN_COMMENT = "280";

   @RBEntry("Il nome di classe del WTContainer è un attributo obbligatorio per il modello di contesto")
   public static final String CONTAINER_CLASSNAME_CANNOTBENULL = "290";

   @RBEntry("I modelli non possono essere fisicamente eliminati.")
   public static final String TEMPLATES_CANT_BE_PHYSICALLY_DELETED = "300";

   @RBEntry("Il contenitore padre non può essere nullo.")
   public static final String PARENT_CONTAINER_CANNOT_BE_NULL = "310";

   @RBEntry("Il riferimento di un contenitore punta ad un contenitore nullo. Contattare l'amministratore.")
   public static final String NON_NULL_CONT_REF_POINTING_TO_A_NULL_CONTAINER = "320";

   @RBEntry("È stato trovato un contenitore non persistente. Contattare l'amministratore.")
   public static final String NON_PERSISTENT_CONTAINER = "330";

   @RBEntry("La lingua del modello deve essere specificata")
   public static final String LOCALE_CANNOT_BE_NULL = "340";

   @RBEntry("Il master non ha iterazioni")
   public static final String MASTER_HAS_NO_ITERATIONS = "350";

   @RBEntry("Impossibile aggiornare una copia non in modifica di un modello sottoposto a Check-Out.")
   public static final String NON_WORKING_COPY_CANNOT_BE_UPDATED = "360";

   @RBEntry("Funzionamento impossibile dovuto al riferimento nullo del contenitore")
   public static final String CONTAINER_REF_CANNOT_BE_NULL = "370";

   @RBEntry("Impossibile trovare il generatore per la classe di contesto {0}. Contattare l'amministratore")
   public static final String UNABLE_TO_FIND_TEMPLATE_GENERATOR = "390";

   @RBEntry("Impossibile trovare il copiatore per la classe di contesto {0}. Contattare l'amministratore")
   public static final String UNABLE_TO_FIND_CONTAINER_COPIER = "400";

   @RBEntry("Impossibile trovare l'applicatore dei valori predefiniti per la classe di contesto {0}. Contattare l'amministratore")
   public static final String UNABLE_TO_FIND_VALUES_DELEGATE = "410";

   @RBEntry("Impossibile trovare la factory di richiesta del modello per la classe di contesto {0}. Contattare l'amministratore")
   public static final String UNABLE_FIND_TEMPLATE_REQUEST_FACTORY = "420";

   @RBEntry("Impossibile trovare la factory per l'elaboratore della richiesta di modello per la classe di contesto {0}. Contattare l'amministratore")
   public static final String UNABLE_TO_FIND_TMPL_REQ_PROCESSOR_FACTORY = "430";

   @RBEntry("{0} non è un nome di contesto valido per la copia. I nomi di contesto non possono contenere i seguenti caratteri:  {1}")
   public static final String INVALID_CHARACTERS_IN_CONTEXT_COPY_NAME = "440";

   @RBEntry("Impossibile sostituire XML se non si esegue il Check-Out del modello")
   public static final String TEMPLATE_NEEDS_2_B_CHKED_OUT_TO_RPLC_XML = "450";

   @RBEntry("File non valido inviato al modello di contenitore. È necessario caricare un file XML di tipo doctype NMLoader con un tag di csvCreateContainerTemplate")
   public static final String ILLEGAL_LOAD_FILE = "460";

   @RBEntry("{0}.zip")
   public static final String BUSINESS_JAR_DOWNLOAD_NAME = "470";

   @RBEntry("Impossibile completare gli attributi per il modello di modulo task nell'oggetto.")
   public static final String INFLATE_FAILED = "89";

   @RBEntry("Impossibile recuperare il flusso di input per il modello. Contattare l'amministratore")
   public static final String UNABLE_TO_GET_INPUTSTREAM = "480";

   @RBEntry("Il modello non è un oggetto ApplicationData. Avvisare l'amministratore")
   public static final String NOT_APPLICATION_DATA = "490";
}
