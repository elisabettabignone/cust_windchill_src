/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.workflow.definer;

import wt.util.resource.*;

@RBUUID("wt.workflow.definer.definerResource")
public final class definerResource_it extends WTListResourceBundle {
   /**
    * -- Workflow Definer messages: starts at 0
    * -- Workflow Analysis Report messages: starts at 500
    * -- Workflow Loader Messages: starts at 1000
    * Workflow Definer Error Messages --------------------------------------------
    **/
   @RBEntry("L'operazione {0} non è riuscita.")
   public static final String OPERATION_FAILED = "0";

   @RBEntry("Impossibile trovare il connettore iniziale per il modello di processo: {0}")
   public static final String NO_START_CONNECTOR = "1";

   @RBEntry("Evento non valido: {0}")
   public static final String INVALID_EVENT = "2";

   @RBEntry("Azione di destinazione non valida: {0}")
   public static final String INVALID_ACTION = "3";

   @RBEntry("Modello di link al nodo di tipo sconosciuto")
   public static final String UNKNOWN_LINK_TYPE = "4";

   @RBEntry("Oggetto di link al nodo di tipo sconosciuto")
   public static final String UNKNOWN_OBJECT_TYPE = "5";

   @RBEntry("Impossibile collegare nodi con processi padre differenti")
   public static final String DIFFERENT_PARENTS = "6";

   @RBEntry("Origine connettore non valida per un modello di link: {0}")
   public static final String INVALID_SOURCE = "7";

   @RBEntry("Modello non attivato: {0}")
   public static final String NOT_ENABLED = "8";

   @RBEntry("Impossibile modificare il tipo di un connettore {0}")
   public static final String START_CONNECTOR = "9";

   @RBEntry("Destinazione connettore non valida per un modello di link: {0}")
   public static final String INVALID_DESTINATION = "10";

   @RBEntry("Impossibile assegnare utente/gruppo/ruolo all'operazione: {0}")
   public static final String INVALID_PRINCIPAL = "11";

   @RBEntry("Copia di ")
   public static final String COPY_OF = "12";

   @RBEntry("Informazioni sulle variabili non valide")
   public static final String INVALID_VARIABLE_INFO = "13";

   @RBEntry("La variabile di origine {0} non è compatibile con la variabile di destinazione {1}")
   public static final String INCOMPATIBLE_TYPES = "14";

   @RBEntry("Impossibile trovare il metodo {0} per l'espressione")
   public static final String NO_METHOD = "15";

   @RBEntry("Rilevata eccezione di accesso non valida nell'esecuzione del metodo {0}")
   public static final String ILLEGAL_ACCESS = "16";

   @RBEntry("Impossibile trovare la classe {0} per l'espressione")
   public static final String NO_CLASS = "17";

   @RBEntry("Rilevata eccezione di argomento non valida nell'esecuzione del metodo {0}")
   public static final String ILLEGAL_ARGUMENT = "18";

   @RBEntry("Esecuzione interrotta. Espressione di workflow non valida.")
   public static final String BAD_EXPRESSION = "19";

   @RBEntry("Condizione non valida per il contenitore delle transazioni")
   public static final String INVALID_ASSERTION = "20";

   @RBEntry("Oggetto non valido rilevato in BatchContainer")
   public static final String INVALID_BC_OBJECT = "21";

   @RBEntry("Oggetto non valido rilevato in RoleBatchContainer")
   public static final String INVALID_RBC_OBJECT = "22";

   @RBEntry("Impossibile eliminare un modello di workflow")
   public static final String CANNOT_DELETE_WFT = "23";

   @RBEntry("Il modello \"{0}\" è già presente in un modello di processo")
   public static final String ALREADY_CONTAINED = "24";

   @RBEntry("Modello \"{0}\" non trovato")
   public static final String TEMPLATE_NOT_FOUND = "25";

   @RBEntry("Impossibile modificare il nome di un modello di processo: {0}")
   public static final String CANT_CHANGE_NAME = "26";

   @RBEntry("Impossibile modificare o eliminare un modello di processo: {0}. Sono presenti istanze aperte")
   public static final String IN_USE_BY_PROCESS = "27";

   @RBEntry("Impossibile eliminare file di espressione: {0}")
   public static final String CANT_DELETE_FILE = "28";

   @RBEntry("La variabile {0} non può essere copiata nella variabile {1} perché {2} copia in {1}.")
   public static final String MULTIPLE_TARGET = "29";

   @RBEntry("Impossibile salvare l'espressione poiché l'oggetto modello associato non è persistente.")
   public static final String TEMPLATE_NOT_PERSISTENT = "30";

   @RBEntry("Rifermento invalido: manca il modello di destinazione.")
   public static final String NULL_PROXY = "31";

   @RBEntry("Riferimento invalido: riferimento circolare.")
   public static final String CIRCULAR_PROXY = "32";

   @RBEntry("Riferimento invalido: modello di destinazione non supportato: {0}")
   public static final String INVALID_TARGET_PROXY = "33";

   @RBEntry("Tipo di definizione di processo invalido: {0}")
   public static final String INVALID_PROCESS_DEF = "34";

   @RBEntry("Definizione invalida di blocco: manca un processo padre: {0}")
   public static final String INVALID_BLOCK_TEMPLATE = "35";

   @RBEntry("Trasformazione invalida del blocco: collegamenti iniziali multipli")
   public static final String MULTIPLE_INITIAL_LINKS = "36";

   @RBEntry(" ")
   @RBPseudo(false)
   public static final String MULTIPLE_FINAL_LINKS = "37";

   @RBEntry(" ")
   @RBPseudo(false)
   public static final String NO_INITIAL_LINK = "38";

   @RBEntry(" ")
   @RBPseudo(false)
   public static final String NO_FINAL_LINK = "39";

   @RBEntry("Trasformazione invalida del blocco: set di nodi vuoto")
   public static final String EMPTY_SET = "40";

   @RBEntry("Trasformazione invalida del blocco: il collegamento input ha più di una coppia evento-azione")
   public static final String MULTIPLE_MAPPINGS = "41";

   @RBEntry("Impossibile disattivare il modello del processo perché è in uso: {0}")
   public static final String CANT_DISABLE_TEMPLATE = "42";

   @RBEntry("Impossibile eliminare il modello di processo master: {0}")
   public static final String CANT_DELETE_MASTER = "43";

   @RBEntry("Un modello di processo deve essere memorizzato in uno schedario personale o nello schedario System.")
   public static final String INVALID_CABINET = "44";

   @RBEntry("Impossibile trovare o creare {0}.  ")
   @RBArgComment0("the name of the file or directory that we are trying to create")
   public static final String UNABLE_TO_CREATE_DIR = "45";

   @RBEntry("Impossibile eliminare il modello del processo {0} perché è impiegato da un altro modello.")
   public static final String IN_USE_BY_TEMPLATE = "46";

   @RBEntry("Impossibile accedere alle variabili: il modello non ha un modello padre.")
   public static final String NO_PARENT_TEMPLATE = "47";

   @RBEntry("Impossibile creare l'espressione perché il tipo non è corretto o è incompatibile: {0}.")
   public static final String BAD_EXPRESSION_TYPE = "48";

   @RBEntry("Non si dispone di autorizzazione alla modifica di {0}")
   @RBArgComment0("is the Wf Process template object that the user is trying to modify ")
   public static final String MODIFY_NOT_ALLOWED = "49";

   @RBEntry("Non si dispone di autorizzazione alla creazione di un modello di processo di workflow.")
   public static final String CREATE_WFPT_NOT_ALLOWED = "50";

   @RBEntry("È necessario sottoporre a Check-In i modelli di workflow prima di eseguire l'operazione.")
   public static final String CANT_DELETE_CHECKOUT_TEMPLATE = "51";

   /**
    * Template Analysis Report --------------------------------------------------
    **/
   @RBEntry("Link non validi")
   public static final String INVALID_LINKS = "500";

   @RBEntry("Passi non raggiungibili")
   public static final String CANT_REACH_STEPS = "501";

   @RBEntry("Passi che non consentono di raggiungere il connettore finale o la messa a terra")
   @RBComment("If you translate END, GROUND keep it consistent with WfConnectorRB definitions")
   public static final String CANT_FINISH_STEPS = "502";

   @RBEntry("Connettori non raggiungibili")
   public static final String CANT_REACH_CONNECTORS = "503";

   @RBEntry("Connettori che non consentono di raggiungere il connettore finale o la messa a terra")
   public static final String CANT_FINISH_CONNECTORS = "504";

   @RBEntry("Flusso di dati non valido")
   public static final String DATA_FLOW_DEFECTS = "505";

   @RBEntry("Eventi definiti ma non utilizzati")
   public static final String UNUSED_EVENTS = "506";

   @RBEntry("Variabili di input/output non utilizzate correttamente")
   @RBComment("If you translate INPUT, OUTPUT keep it consistent with WfDestinationRB definitions")
   public static final String UNMAPPED_VARIABLES = "507";

   @RBEntry("processo")
   public static final String PROCESS = "508";

   @RBEntry("attività")
   public static final String ACTIVITY = "509";

   @RBEntry("origine")
   public static final String SOURCE = "510";

   @RBEntry("destinazione")
   public static final String DESTINATION = "511";

   @RBEntry("in")
   public static final String IN = "512";

   /**
    * Workflow Loader Messages ------------------------------------------------
    **/
   @RBEntry("Il task \"{0}\" non è valido. I task validi sono disponibili in WfDefiner.properties.")
   @RBArgComment0("is the name of the workflow task")
   public static final String INVALID_TASK = "1000";

   @RBEntry("Il tipo di connettore \"{0}\" non è valido.  I tipi validi sono INIZIALE, FINALE, E, O, MESSA A TERRA")
   @RBArgComment0("is the connector type")
   public static final String LOAD_INVALID_CONNECTOR = "1001";

   @RBEntry("Uno o entrambi i WfTemplateNodes non sono stati precedentemente creati.  Assicurarsi che i nodi di origine e di destinazione siano visualizzati prima di questo link di nodo nel file di caricamento.  Nodo di origine: {0} --- Nodo di destinazione: {1}")
   @RBArgComment0("is the name of source node link.")
   @RBArgComment1("is the name of destination node link.")
   public static final String LOAD_INVALID_LINK = "1002";

   @RBEntry("\"{0}\" non è un ruolo valido.  I ruoli validi sono le chiavi disponibili in wt.project.RoleRB.")
   public static final String INVALID_ROLE = "1003";

   @RBEntry("Assegnare ruoli o utenti/gruppi/ruoli ad un WfAssignedActivityTemplate.  Assicurari che questo file di caricamento si trovi tra gli elementi WfAssignedActivityTemplateBegin e WfAssignedActivityTemplateEnd.")
   public static final String LOAD_CANNOT_ADD_PARTICIPANT = "1004";

   @RBEntry("\"{0}\" è un utente/gruppo/ruolo non valido.  Verificare che l'utente o il gruppo esista.")
   public static final String LOAD_INVALID_PRINCIPAL = "1005";

   @RBEntry("Non esiste alcun modello di processo.  Tutti gli oggetti di workflow devono essere creati come parte di un WfProcessTemplate.  Assicurarsi che nel file di caricamento sia visualizzato un elemento WfProcessTemplate prima di questo elemento.")
   public static final String LOAD_INVALID_PROCESS = "1006";

   @RBEntry("Valore variabile predefinito non valido, {0}: {1}")
   @RBArgComment0("is the default value")
   @RBArgComment1("is the type of the default value")
   public static final String LOAD_INVALID_DEFAULT_VALUE = "1007";

   @RBEntry("Coppia evento/azione non valida: '{0}/{1}'")
   @RBArgComment0("is the name of the event")
   @RBArgComment1("is the name of the action")
   public static final String LOAD_INVALID_EVENT_ACTION = "1008";

   @RBEntry("Impossibile creare questo oggetto, poiché dipendente dall' esistenza di un altro oggetto.  Gli elementi nel file di caricamento devono avere un ordine valido.")
   public static final String LOAD_MISSING_DEPENDENCY = "1009";

   @RBEntry("Il progetto {0} è correntemente in uso. Prima di eliminare il progetto {0} è necessario rimuovere tutti i componenti.")
   @RBArgComment0("the identity of the project that the user is trying to delete")
   public static final String DELETE_PROJECT_PROHIBITED = "1010";

   @RBEntry("Avvertenza: è stata creata una nuova iterazione del seguente modello di workflow: {0}")
   public static final String NEW_ITERATION_CREATED = "1011";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Il modello di team {0} è in uso al momento. Prima di eliminare il modello di team {0} è necessario rimuovere tutti i componenti.")
   public static final String DELETE_TEAM_TEMPLATE_PROHIBITED = "1012";

   @RBEntry("Il team {0} è in uso al momento. Prima di eliminare il team {0} è necessario rimuovere tutti i componenti.")
   public static final String DELETE_TEAM_PROHIBITED = "1013";

   @RBEntry("Avvertenza: l'importazione del workflow {0} non è riuscita. Impossibile importare un'iterazione nuova di un modello di workflow sottoposto a Check-Out.")
   public static final String WORKFLOW_CHECKED_OUT = "1014";

   @RBEntry("Problema durante la creazione dell'oggetto {0} \"{1}\" per il nodo \"{2}\" nel modello \"{3}\". Consultare il log del method server.")
   @RBArgComment1("{0} The object type that reported the problem.")
   @RBArgComment2("{1} The name of the object type.")
   @RBArgComment3("{2} The name of the node in the Workflow/Block template.")
   @RBArgComment4("{3} The name of the Workflow/Block template.")
   public static final String FAILURE_MESSAGE_1 = "1015";

   @RBEntry("Problema durante la creazione dell'oggetto {0} \"{1}\" nel modello \"{2}\". Consultare il log del method server.")
   @RBArgComment1("{0} The object type that reported the problem.")
   @RBArgComment2("{1} The name of the object type.")
   @RBArgComment3("{2} The name of the Workflow/Block template.")
   public static final String FAILURE_MESSAGE_2 = "1016";

   @RBEntry("Attributi per {0}: {1}.")
   @RBArgComment1("{0} The name of the xmlEmelent eor eg. csvAddVariable.")
   @RBArgComment2("{1} The list of attr for above xml element.")
   public static final String FAILURE_MESSAGE_3 = "1017";

   @RBEntry("ERRORE: impossibile importare workflow. Non è possibile importare una nuova iterazione di un modello di workflow di cui sia stato effettuato il Check-Out.")
   public static final String IMPORT_ERROR_WORKFLOW_CHECKED_OUT = "1018";

   @RBEntry("Valore di default")
   public static final String WF_VARIABLE_INFO_DEFAULT_VALUE = "defaultValue";

   @RBEntry("Nome")
   public static final String WF_VARIABLE_INFO_NAME = "name";

   @RBEntry("Sola lettura")
   public static final String WF_VARIABLE_INFO_READ_ONLY = "readOnly";

   @RBEntry("Obbligatoria")
   public static final String WF_VARIABLE_INFO_REQUIRED = "required";

   @RBEntry("Reimpostabile")
   public static final String WF_VARIABLE_INFO_RESETABLE = "resetable";

   @RBEntry("Nome tipo")
   public static final String WF_VARIABLE_INFO_TYPE_NAME = "typeName";

   @RBEntry("Visibile")
   public static final String WF_VARIABLE_INFO_VISIBLE = "visible";
   
   @RBEntry("Un utente non appartenente all'organizzazione non può importare o esportare il modello con il gruppo di questa organizzazione nel pool di risorse.")
   public static final String THIS_ORG_EXPORTIMPORT_MSG = "THIS_ORG_EXPORTIMPORT_MSG";

   @RBEntry("La tabella non esiste.")
   public static final String UNKNOWN_TABLE = "UNKNOWN_TABLE";
   
   @RBEntry("La colonna della tabella non esiste.")
   public static final String UNKNOWN_DB_COLUMN = "UNKNOWN_DB_COLUMN";
   
   @RBEntry("{0} di {1} modelli selezionati sono stati esportati.\\nAlmeno uno dei modelli selezionati è sottoposto a Check-Out. Per tutti i modelli selezionati sottoposti a Check-Out, se esiste una versione sottoposta a Check-In, questa è stata esportata.")
   public static final String EXPORT_MGS_TEMPLATES = "EXPORT_MGS_TEMPLATES";
   
   @RBEntry("Il modello selezionato è stato sottoposto a Check-Out. È stata esportata l'ultima versione sottoposta a Check-in del modello.")
   public static final String EXPORT_MGS_FOR_CHECKEDOUT_TEMPLATES = "EXPORT_MGS_FOR_CHECKEDOUT_TEMPLATES";
   
   @RBEntry("Il modello selezionato non è stato ancora sottoposto a Check-In. Nessun oggetto esportato")
   public static final String EXPORT_MGS_FOR_NEW_NOTCHECKED_IN_TEMPLATES = "EXPORT_MGS_FOR_NEW_NOTCHECKED_IN_TEMPLATES";
   
   @RBEntry("ATTENZIONE: il modello di workflow {0} è stato sottoposto a Check-Out da {1}. Non è possibile importare un'iterazione più recente se non si esegue il Check-In del modello.")
   public static final String WORKFLOW_CHECKED_OUT_MSG = "WORKFLOW_CHECKED_OUT_MSG";
   
   
}
