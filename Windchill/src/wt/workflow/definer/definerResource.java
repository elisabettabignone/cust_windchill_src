/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.workflow.definer;

import wt.util.resource.*;

@RBUUID("wt.workflow.definer.definerResource")
public final class definerResource extends WTListResourceBundle {
   /**
    * -- Workflow Definer messages: starts at 0
    * -- Workflow Analysis Report messages: starts at 500
    * -- Workflow Loader Messages: starts at 1000
    * Workflow Definer Error Messages --------------------------------------------
    **/
   @RBEntry("The operation: {0} failed.")
   public static final String OPERATION_FAILED = "0";

   @RBEntry("Can't find start connector for process template: {0}")
   public static final String NO_START_CONNECTOR = "1";

   @RBEntry("Invalid event: {0}")
   public static final String INVALID_EVENT = "2";

   @RBEntry("Invalid destination action: {0}")
   public static final String INVALID_ACTION = "3";

   @RBEntry("Unknown type for a node link template")
   public static final String UNKNOWN_LINK_TYPE = "4";

   @RBEntry("Unknown type for a node link object")
   public static final String UNKNOWN_OBJECT_TYPE = "5";

   @RBEntry("Can't link nodes with different parent processes")
   public static final String DIFFERENT_PARENTS = "6";

   @RBEntry("Invalid connector source for a link template: {0}")
   public static final String INVALID_SOURCE = "7";

   @RBEntry("Template is not enabled: {0}")
   public static final String NOT_ENABLED = "8";

   @RBEntry("Can't change the type of a {0} connector")
   public static final String START_CONNECTOR = "9";

   @RBEntry("Invalid connector destination for a link template: {0}")
   public static final String INVALID_DESTINATION = "10";

   @RBEntry("Can't assign principal to activity: {0}")
   public static final String INVALID_PRINCIPAL = "11";

   @RBEntry("Copy of ")
   public static final String COPY_OF = "12";

   @RBEntry("Invalid variable information")
   public static final String INVALID_VARIABLE_INFO = "13";

   @RBEntry("Source variable {0} is not type compatible with destination variable {1}")
   public static final String INCOMPATIBLE_TYPES = "14";

   @RBEntry("Expression method {0} couldn't be found")
   public static final String NO_METHOD = "15";

   @RBEntry("Illegal access exception caught in {0} method execution")
   public static final String ILLEGAL_ACCESS = "16";

   @RBEntry("Expression class {0} couldn't be found")
   public static final String NO_CLASS = "17";

   @RBEntry("Illegal argument exception caught in {0} method execution")
   public static final String ILLEGAL_ARGUMENT = "18";

   @RBEntry("Can't execute: bad workflow expression")
   public static final String BAD_EXPRESSION = "19";

   @RBEntry("Invalid Assertion for this transaction container")
   public static final String INVALID_ASSERTION = "20";

   @RBEntry("Invalid Object found in BatchContainer")
   public static final String INVALID_BC_OBJECT = "21";

   @RBEntry("Invalid Object found in RoleBatchContainer")
   public static final String INVALID_RBC_OBJECT = "22";

   @RBEntry("Cannot delete a Workflow Template")
   public static final String CANNOT_DELETE_WFT = "23";

   @RBEntry("Template {0} is already contained in a process template")
   public static final String ALREADY_CONTAINED = "24";

   @RBEntry("Template {0} not found")
   public static final String TEMPLATE_NOT_FOUND = "25";

   @RBEntry("Can't change name of process template: {0}")
   public static final String CANT_CHANGE_NAME = "26";

   @RBEntry("Can't modify or delete process template: {0}; there are open instances")
   public static final String IN_USE_BY_PROCESS = "27";

   @RBEntry("Can't delete expression file: {0}")
   public static final String CANT_DELETE_FILE = "28";

   @RBEntry("Variable {0} can't be copied into variable {1} because {2} already copies into {1}.")
   public static final String MULTIPLE_TARGET = "29";

   @RBEntry("Can't save expression because associated template object is not persistent.")
   public static final String TEMPLATE_NOT_PERSISTENT = "30";

   @RBEntry("Invalid reference: no target template.")
   public static final String NULL_PROXY = "31";

   @RBEntry("Invalid reference: circular reference.")
   public static final String CIRCULAR_PROXY = "32";

   @RBEntry("Invalid reference: target template not supported: {0}")
   public static final String INVALID_TARGET_PROXY = "33";

   @RBEntry("Invalid process definition type: {0}")
   public static final String INVALID_PROCESS_DEF = "34";

   @RBEntry("Invalid block definition: no parent process: {0}")
   public static final String INVALID_BLOCK_TEMPLATE = "35";

   @RBEntry("Invalid block transformation: multiple initial links")
   public static final String MULTIPLE_INITIAL_LINKS = "36";

   @RBEntry(" ")
   @RBPseudo(false)
   public static final String MULTIPLE_FINAL_LINKS = "37";

   @RBEntry(" ")
   @RBPseudo(false)
   public static final String NO_INITIAL_LINK = "38";

   @RBEntry(" ")
   @RBPseudo(false)
   public static final String NO_FINAL_LINK = "39";

   @RBEntry("Invalid block transformation: empty set of nodes")
   public static final String EMPTY_SET = "40";

   @RBEntry("Invalid block transformation: input link has multiple event-action pairs")
   public static final String MULTIPLE_MAPPINGS = "41";

   @RBEntry("Can't disable process template because it is in use: {0}")
   public static final String CANT_DISABLE_TEMPLATE = "42";

   @RBEntry("Can't delete process template master: {0}")
   public static final String CANT_DELETE_MASTER = "43";

   @RBEntry("A process template must be stored in a personal cabinet or in the System cabinet.")
   public static final String INVALID_CABINET = "44";

   @RBEntry("Unable to find or create {0}.  ")
   @RBArgComment0("the name of the file or directory that we are trying to create")
   public static final String UNABLE_TO_CREATE_DIR = "45";

   @RBEntry("Can't delete process template {0}; it is being used by other template.")
   public static final String IN_USE_BY_TEMPLATE = "46";

   @RBEntry("Can't access variables: template has no parent template.")
   public static final String NO_PARENT_TEMPLATE = "47";

   @RBEntry("Can't create expression because of wrong or incompatible type: {0}.")
   public static final String BAD_EXPRESSION_TYPE = "48";

   @RBEntry("You are not authorized to modify {0}")
   @RBArgComment0("is the Wf Process template object that the user is trying to modify ")
   public static final String MODIFY_NOT_ALLOWED = "49";

   @RBEntry("You are not authorized to create a Wf Process Template.")
   public static final String CREATE_WFPT_NOT_ALLOWED = "50";

   @RBEntry("Workflow templates should be checked in before performing this operation.")
   public static final String CANT_DELETE_CHECKOUT_TEMPLATE = "51";

   /**
    * Template Analysis Report --------------------------------------------------
    **/
   @RBEntry("Invalid links")
   public static final String INVALID_LINKS = "500";

   @RBEntry("Steps that can't be reached")
   public static final String CANT_REACH_STEPS = "501";

   @RBEntry("Steps that don't reach END or GROUND connector")
   @RBComment("If you translate END, GROUND keep it consistent with WfConnectorRB definitions")
   public static final String CANT_FINISH_STEPS = "502";

   @RBEntry("Connectors that can't be reached")
   public static final String CANT_REACH_CONNECTORS = "503";

   @RBEntry("Connectors that don't reach END or GROUND connector")
   public static final String CANT_FINISH_CONNECTORS = "504";

   @RBEntry("Data flow defects")
   public static final String DATA_FLOW_DEFECTS = "505";

   @RBEntry("Events defined but not used")
   public static final String UNUSED_EVENTS = "506";

   @RBEntry("INPUT/OUTPUT variables not used as such")
   @RBComment("If you translate INPUT, OUTPUT keep it consistent with WfDestinationRB definitions")
   public static final String UNMAPPED_VARIABLES = "507";

   @RBEntry("process")
   public static final String PROCESS = "508";

   @RBEntry("activity")
   public static final String ACTIVITY = "509";

   @RBEntry("source")
   public static final String SOURCE = "510";

   @RBEntry("destination")
   public static final String DESTINATION = "511";

   @RBEntry("in")
   public static final String IN = "512";

   /**
    * Workflow Loader Messages ------------------------------------------------
    **/
   @RBEntry("\"{0}\" is not a valid task.  The valid tasks are found in WfDefiner.properties.")
   @RBArgComment0("is the name of the workflow task")
   public static final String INVALID_TASK = "1000";

   @RBEntry("\"{0}\" is an invalid connector type.  Valid types are: START, END, AND, OR, GROUND")
   @RBArgComment0("is the connector type")
   public static final String LOAD_INVALID_CONNECTOR = "1001";

   @RBEntry("One or both of the WfTemplateNodes wasn't previously created.  Make sure the source and destination nodes appear before this node link in the load file.  Source Node: {0} --- Destination Node: {1}")
   @RBArgComment0("is the name of source node link.")
   @RBArgComment1("is the name of destination node link.")
   public static final String LOAD_INVALID_LINK = "1002";

   @RBEntry("\"{0}\" is an invalid Role.  Valid roles are the keys found in wt.project.RoleRB.")
   public static final String INVALID_ROLE = "1003";

   @RBEntry("Roles or Principals must be assigned to a WfAssignedActivityTemplate.  Make sure this load file entry is between a pair of WfAssignedActivityTemplateBegin and WfAssignedActivityTemplateEnd entries.")
   public static final String LOAD_CANNOT_ADD_PARTICIPANT = "1004";

   @RBEntry("\"{0}\" is an invalid Principal.  Verify that this user or group exists.")
   public static final String LOAD_INVALID_PRINCIPAL = "1005";

   @RBEntry("No process template exists.  All workflow objects must be created as part of a WfProcessTemplate.  Make sure a WfProcessTemplate entry appears in the load file before this entry.")
   public static final String LOAD_INVALID_PROCESS = "1006";

   @RBEntry("Invalid default variable value, {0} : {1}")
   @RBArgComment0("is the default value")
   @RBArgComment1("is the type of the default value")
   public static final String LOAD_INVALID_DEFAULT_VALUE = "1007";

   @RBEntry("Invalid event/action pair: '{0}/{1}'")
   @RBArgComment0("is the name of the event")
   @RBArgComment1("is the name of the action")
   public static final String LOAD_INVALID_EVENT_ACTION = "1008";

   @RBEntry("It is invalid to create this object because it is dependent on the existence of another object.  Entries in the load file must be in a valid order.")
   public static final String LOAD_MISSING_DEPENDENCY = "1009";

   @RBEntry("The {0} project is in use.  All uses must be removed before the {0} project can be deleted.")
   @RBArgComment0("the identity of the project that the user is trying to delete")
   public static final String DELETE_PROJECT_PROHIBITED = "1010";

   @RBEntry("Warning:  A new iteration was created for the following Workflow template: {0}")
   public static final String NEW_ITERATION_CREATED = "1011";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("The {0} team template is in use.  All uses must be removed before the {0} team template can be deleted.")
   public static final String DELETE_TEAM_TEMPLATE_PROHIBITED = "1012";

   @RBEntry("The {0} team is in use.  All uses must be removed before the {0} team can be deleted.")
   public static final String DELETE_TEAM_PROHIBITED = "1013";

   @RBEntry("Warning:  Import of {0} Workflow was unsuccessful.  You cannot import a new iteration of a checked out Workflow template.")
   public static final String WORKFLOW_CHECKED_OUT = "1014";

   @RBEntry("Problem creating {0} object \"{1}\" for node \"{2}\" in \"{3}\" template. Check Method Server log.")
   @RBArgComment1("{0} The object type that reported the problem.")
   @RBArgComment2("{1} The name of the object type.")
   @RBArgComment3("{2} The name of the node in the Workflow/Block template.")
   @RBArgComment4("{3} The name of the Workflow/Block template.")
   public static final String FAILURE_MESSAGE_1 = "1015";

   @RBEntry("Problem creating {0} object \"{1}\" in \"{2}\" template. Check Method Server log.")
   @RBArgComment1("{0} The object type that reported the problem.")
   @RBArgComment2("{1} The name of the object type.")
   @RBArgComment3("{2} The name of the Workflow/Block template.")
   public static final String FAILURE_MESSAGE_2 = "1016";

   @RBEntry("Attributes for {0}: {1}.")
   @RBArgComment1("{0} The name of the xmlEmelent eor eg. csvAddVariable.")
   @RBArgComment2("{1} The list of attr for above xml element.")
   public static final String FAILURE_MESSAGE_3 = "1017";

   @RBEntry("ERROR:  Import of Workflow(s) was unsuccessful.  You cannot import a new iteration of a checked out Workflow template.")
   public static final String IMPORT_ERROR_WORKFLOW_CHECKED_OUT = "1018";

   @RBEntry("Default Value")
   public static final String WF_VARIABLE_INFO_DEFAULT_VALUE = "defaultValue";

   @RBEntry("Name")
   public static final String WF_VARIABLE_INFO_NAME = "name";

   @RBEntry("Read Only")
   public static final String WF_VARIABLE_INFO_READ_ONLY = "readOnly";

   @RBEntry("Required")
   public static final String WF_VARIABLE_INFO_REQUIRED = "required";

   @RBEntry("Resetable")
   public static final String WF_VARIABLE_INFO_RESETABLE = "resetable";

   @RBEntry("Type Name")
   public static final String WF_VARIABLE_INFO_TYPE_NAME = "typeName";

   @RBEntry("Visible")
   public static final String WF_VARIABLE_INFO_VISIBLE = "visible";
   
   @RBEntry("Non organization user can not import/export template having 'This Org' group in resource pool.")
   public static final String THIS_ORG_EXPORTIMPORT_MSG = "THIS_ORG_EXPORTIMPORT_MSG";

   @RBEntry("Table does not exist.")
   public static final String UNKNOWN_TABLE = "UNKNOWN_TABLE";
   
   @RBEntry("Table Column does not exist.")
   public static final String UNKNOWN_DB_COLUMN = "UNKNOWN_DB_COLUMN";
   
   @RBEntry("{0} of {1} selected templates exported.\\nOne or more of the selected templates are checked out. For all selected templates that are in checked out state, if a checked in version exists, the checked in version has been exported.")
   public static final String EXPORT_MGS_TEMPLATES = "EXPORT_MGS_TEMPLATES";
   
   @RBEntry("Selected template was checked out. The latest checked-in version has been exported for this template.")
   public static final String EXPORT_MGS_FOR_CHECKEDOUT_TEMPLATES = "EXPORT_MGS_FOR_CHECKEDOUT_TEMPLATES";
   
   @RBEntry("Selected template has not been checked in yet. No objects have been exported")
   public static final String EXPORT_MGS_FOR_NEW_NOTCHECKED_IN_TEMPLATES = "EXPORT_MGS_FOR_NEW_NOTCHECKED_IN_TEMPLATES";
   
   @RBEntry("ATTENTION: Workflow template {0} has been checked out by {1}. You cannot import a newer iteration unless this template is checked in.")
   public static final String WORKFLOW_CHECKED_OUT_MSG = "WORKFLOW_CHECKED_OUT_MSG";
   
   
}
