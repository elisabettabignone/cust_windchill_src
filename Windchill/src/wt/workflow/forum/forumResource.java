/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.workflow.forum;

import wt.util.resource.*;

@RBUUID("wt.workflow.forum.forumResource")
public final class forumResource extends WTListResourceBundle {
   @RBEntry("The operation: {0} failed.")
   public static final String OPERATION_FAILED = "0";

   @RBEntry("{0} does not have privilleges to perform this action")
   public static final String NOT_AUTHORIZED = "1";

   @RBEntry("The parameter {0} cannot be null")
   public static final String NULL_PARAMETER = "2";

   @RBEntry("The attribute {0} is required. Please enter a value for this attribute and resubmit")
   public static final String NULL_ATTRIBUTE = "3";

   @RBEntry("Forums based on this template exist. This template cannot be deleted.")
   public static final String TEMPLATE_HAS_FORUMS = "4";

   @RBEntry("No Discussion Forum Template called {0} exists. Please try another template name.")
   public static final String NO_SUCH_TEMPLATE = "5";

   @RBEntry("Cannot unsubscribe from object : {0} because the user is subscribed to its parent. Please try again by first unsubscribing from the parent discusion.")
   public static final String UNSUBSCRIBE_NOT_ALLOWED = "6";

   @RBEntry("New Comment in Discussion")
   public static final String SUBSCRIPTION_NOTICE_SUBJECT = "7";

   @RBEntry("A new comment has been made to a discussion you are subscribed to. You can respond by accessing the reply link.")
   public static final String SUBSCRIPTION_POSTING_MSG = "8";

   @RBEntry("Forum {0} already has a primary. Attempt to assign another primary unsuccessful.")
   public static final String ONE_PRIMARY_ONLY = "9";

   @RBEntry("Name")
   public static final String NAME_LABEL = "10";

   @RBEntry("Template Structure")
   public static final String TEMPLATE_STRUCTURE_LABEL = "11";

   @RBEntry("Description")
   public static final String DESCRIPTION_LABEL = "12";

   @RBEntry("Discussion Forum Template")
   public static final String TEMPLATE_NAME_HEADER = "13";

   @RBEntry("Created On")
   public static final String CREATED_HEADER = "14";

   @RBEntry("Description")
   public static final String DESCRIPTION_HEADER = "15";

   @RBEntry("Select Discussion Forum Template")
   public static final String SELECT_TEMPLATE_TITLE = "16";

   @RBEntry("OK")
   public static final String OK_BUTTON = "17";

   @RBEntry("Cancel")
   public static final String CANCEL_BUTTON = "18";

   @RBEntry("Create Discussion Forum Template")
   public static final String CREATE_TEMPLATE_TITLE = "19";

   @RBEntry("Create Discussion Forum")
   public static final String CREATE_DISCUSSION_FORUM_TITLE = "20";

   @RBEntry("Copy structure from template")
   public static final String COPY_TEMPLATE_LINK = "21";

   @RBEntry("The XML structure specified had the following error ")
   public static final String SAXEXCEPTION = "22";

   @RBEntry("Discussion Forum Template created successfully")
   public static final String CREATE_TEMPLATE_SUCCESS = "23";

   @RBEntry("Please enter a name for the template")
   public static final String NAME_EMPTY_ERROR = "24";

   @RBEntry("Problem parsing xml (line {0}, column {1}): {2} ")
   public static final String PARSE_ERROR = "25";

   @RBEntry("Error initializing \"{0}\".")
   public static final String ERROR_INITIALIZING = "26";

   @RBEntry("Discuss")
   public static final String DISCUSS_URL_LABEL = "27";

   @RBEntry("Error")
   public static final String ERROR = "28";

   @RBEntry("Warning")
   public static final String WARNING = "29";

   @RBEntry("At line")
   public static final String AT_LINE = "30";

   @RBEntry("Column")
   public static final String COLUMN = "31";

   @RBEntry("Template")
   public static final String TEMPLATE_LABEL = "32";

   @RBEntry("There are no forums for this object yet")
   public static final String NO_FORUMS_FOR_OBJECT = "33";

   @RBEntry("Forums for ")
   public static final String FORUMS_FOR_OBJECT = "34";

   @RBEntry("Available Forums")
   public static final String AVAILABLE_FORUMS = "35";

   @RBEntry("Add Forum")
   public static final String ADD_FORUM = "36";

   @RBEntry("Remove Forum")
   public static final String REMOVE_FORUM = "37";

   @RBEntry("Discussion Forum")
   public static final String DISCUSSION_FORUM_HEADER = "38";

   @RBEntry("Object passed in was expected to be an instance of SubjectOfForum")
   public static final String SUBJECT_OF_FORUM_EXPECTED = "39";

   @RBEntry("New Discussion Forum")
   public static final String CREATE_DISCUSSION_FORUM_LINK = "40";

   @RBEntry("Discussion Forum has been successfully created for this object")
   public static final String CREATE_FORUM_SUCCESS = "41";

   @RBEntry("Discussion Forum has been successfully added to this object")
   public static final String ADD_FORUM_SUCCESS = "42";

   @RBEntry("Discussion Forum has been successfully removed from this object")
   public static final String REMOVE_FORUM_SUCCESS = "43";

   @RBEntry("Properties of {0} (for ")
   public static final String FORUM_PROPERTIES_HEADER = "44";

   @RBEntry("Topics for \"{0}\"")
   public static final String TOPICS_FOR_FORUM = "45";

   @RBEntry("Unsubscribe")
   public static final String UNSUBSCRIBE_LABEL = "46";

   @RBEntry("Subscribe")
   public static final String SUBSCRIBE_LABEL = "47";

   @RBEntry("Reply to Comment")
   public static final String REPLY_LABEL = "48";

   @RBEntry("Expand All")
   public static final String EXPAND_ALL = "49";

   @RBEntry("Collapse All")
   public static final String COLLAPSE_ALL = "50";

   @RBEntry("New Topic")
   public static final String NEW_TOPIC = "52";

   @RBEntry("Creator")
   public static final String CREATOR_LABEL = "53";

   @RBEntry("Message:")
   public static final String MESSAGE_LABEL = "54";

   @RBEntry("Topic for {0}")
   public static final String TOPIC_PROPERTIES_HEADER = "55";

   @RBEntry("Top")
   public static final String TOP_LABEL = "56";

   @RBEntry("Previous")
   public static final String PREVIOUS_LABEL = "57";

   @RBEntry("Next")
   public static final String NEXT_LABEL = "58";

   @RBEntry("Reply To")
   public static final String REPLY_TO_POSTING_TITLE = "59";

   @RBEntry("Notify me about all messages posted to this thread")
   public static final String SUBSCRIBE_TO_REPLY = "60";

   @RBEntry("Enter Plain Text or HTML tags")
   public static final String HTML_AWARE_PROMPT = "61";

   @RBEntry("Re")
   public static final String REPLY_PREFIX = "62";

   @RBEntry("Subject")
   public static final String SUBJECT_LABEL = "63";

   @RBEntry("Reply has been successfully created")
   public static final String REPLY_SUCCESS = "64";

   @RBEntry("Discuss")
   public static final String DISCUSS = "65";

   @RBEntry("Search")
   public static final String SEARCH = "66";

   @RBEntry("New Comment")
   public static final String CREATE_POSTING_LABEL = "67";

   @RBEntry("Enter plain text or HTML")
   public static final String MESSAGE_INSTRUCTIONS = "68";

   @RBEntry("RE: {0}")
   public static final String REPLY_SUBJECT_PREFIX = "69";

   @RBEntry("Reply &amp; Post")
   public static final String REPLY_AND_POST_BUTTON = "70";

   @RBEntry("Post")
   public static final String POST = "71";

   @RBEntry("New Discussion:")
   public static final String NEW_DISCUSSION_LABEL = "72";

   @RBEntry("Project:")
   public static final String PROJECT_LABEL = "73";

   @RBEntry("Discussion:")
   public static final String FORUM_LABEL = "74";

   @RBEntry("Comment Body:")
   public static final String DISCUSSION_BODY_LABEL = "75";

   @RBEntry("Sender:")
   public static final String SENDER_LABEL = "76";

   @RBEntry("Post")
   public static final String POST_BUTTON = "77";

   @RBEntry("No Subject")
   public static final String NO_SUBJECT_TEXT = "78";

   @RBEntry("Reply to Sender")
   public static final String REPLY_TO_SENDER_LABEL = "79";

   @RBEntry("Forum Notification")
   public static final String FORUM_NOTIFICATION = "80";

   @RBEntry("Discussion Forum")
   public static final String DEFAULT_FORUM_NAME = "81";

   /**
    * Additional notification labels, etc
    **/
   @RBEntry("Project Creator:")
   public static final String PROJECT_CREATOR_LABEL = "90";

   @RBEntry("Project Owner:")
   public static final String PROJECT_OWNER_LABEL = "90a";

   @RBEntry("Host Organization:")
   public static final String PROJECT_HOST_LABEL = "91";

   @RBEntry("Project Description:")
   public static final String PROJECT_DESC_LABEL = "92";

   @RBEntry("Reply to Comment")
   public static final String REPLY_TO_POSTING = "93";

   @RBEntry("None")
   public static final String PROJECT_CREATOR_NONE = "94";

   @RBEntry("A new topic has been created in the discussion you are subscribed to.")
   public static final String SUBSCRIPTION_TOPIC_MSG = "95";

   @RBEntry("A new topic has been created for a discussion you are subscribed to. You can post a message by accessing the New Comment link below.")
   public static final String NEW_TOPIC_MESSAGE = "96";

   @RBEntry("New Topic:")
   public static final String NEW_TOPIC_LABEL = "97";

   @RBEntry("New Topic in Discussion")
   public static final String SUBSCRIPTION_TOPIC_SUBJECT = "98";

   @RBEntry("Project Discussion ")
   @RBComment("Title for a Project's discussion forum table ")
   public static final String PROJECT_DISCUSSION_FORUM = "99";

   @RBEntry("Program Discussion Forum")
   @RBComment("Title for a Program's discussion forum table ")
   public static final String PROGRAM_DISCUSSION_FORUM = "99_prg";

   @RBEntry("{0} Discussion Forum")
   @RBComment("(object that is being discussed eg. part or document) for (subject)")
   public static final String FORUM_FOR_SUBJECT = "100";

   @RBEntry("{0}- Discussion Forum")
   @RBComment("Name for subscription to a DiscussionForum, for cases where the SubjectOfForum doesnt implement getName().")
   public static final String SUBSCRIPTION_IDENTITY = "101";

   @RBEntry("{0}- Discussion Forum")
   @RBComment("Name for subscription to a DiscussionForum, for cases where the SubjectOfForum implements getName() but not getVersionDisplayIdentifier().")
   public static final String NAMED_SUBSCRIPTION_ID = "102";

   @RBEntry("{0} {1}- Discussion Forum")
   @RBComment("Name for subscription to a DiscussionForum, for cases where the SubjectOfForum implements getName() and getVersionDisplayIdentifier(). 0=name,1=version.")
   public static final String NAMED_VERSIONED_SUBSCRIPTION_ID = "103";

   @RBEntry("Topic '{0}' is already closed")
   @RBComment("Message stating that specified topic is  already  closed")
   public static final String TOPIC_ALREADY_CLOSED = "TOPIC_ALREADY_CLOSED";   
}
