/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.workflow.forum;

import wt.util.resource.*;

@RBUUID("wt.workflow.forum.forumResource")
public final class forumResource_it extends WTListResourceBundle {
   @RBEntry("L'operazione {0} non è riuscita.")
   public static final String OPERATION_FAILED = "0";

   @RBEntry("{{0} non dispone del permesso di effettuare questa azione")
   public static final String NOT_AUTHORIZED = "1";

   @RBEntry("Il parametro {0} non può essere nullo")
   public static final String NULL_PARAMETER = "2";

   @RBEntry("L'attributo {0} è obbligatorio. Immettere un valore per l'attributo e inviare nuovamente")
   public static final String NULL_ATTRIBUTE = "3";

   @RBEntry("Esistono dei forum creati in base a questo modello. Impossibile eliminare il modello.")
   public static final String TEMPLATE_HAS_FORUMS = "4";

   @RBEntry("Non esiste alcun modello di forum discussioni di nome {0}. Provare un altro nome di modello.")
   public static final String NO_SUCH_TEMPLATE = "5";

   @RBEntry("Impossibile annullare la sottoscrizione all'oggetto {0} perché l'utente è sottoscrittore dell'oggetto padre. Riprovare cominciando dall'annullamento della sottoscrizione alla discussione padre.")
   public static final String UNSUBSCRIBE_NOT_ALLOWED = "6";

   @RBEntry("Nuovo commento nella discussione")
   public static final String SUBSCRIPTION_NOTICE_SUBJECT = "7";

   @RBEntry("È stato creato un nuovo commento nella discussione cui l'utente è sottoscritto. Per rispondere fare clic sul link Rispondi.")
   public static final String SUBSCRIPTION_POSTING_MSG = "8";

   @RBEntry("Il forum {0} ha già un pricipale. Il tentaivo di assegnare un altro principale è fallito.")
   public static final String ONE_PRIMARY_ONLY = "9";

   @RBEntry("Nome")
   public static final String NAME_LABEL = "10";

   @RBEntry("Struttura modello")
   public static final String TEMPLATE_STRUCTURE_LABEL = "11";

   @RBEntry("Descrizione")
   public static final String DESCRIPTION_LABEL = "12";

   @RBEntry("Modello forum")
   public static final String TEMPLATE_NAME_HEADER = "13";

   @RBEntry("Data creazione")
   public static final String CREATED_HEADER = "14";

   @RBEntry("Descrizione")
   public static final String DESCRIPTION_HEADER = "15";

   @RBEntry("Modello forum")
   public static final String SELECT_TEMPLATE_TITLE = "16";

   @RBEntry("OK")
   public static final String OK_BUTTON = "17";

   @RBEntry("Annulla")
   public static final String CANCEL_BUTTON = "18";

   @RBEntry("Modello forum")
   public static final String CREATE_TEMPLATE_TITLE = "19";

   @RBEntry("Mostra forum discussioni")
   public static final String CREATE_DISCUSSION_FORUM_TITLE = "20";

   @RBEntry("Copiare la struttura dal modello")
   public static final String COPY_TEMPLATE_LINK = "21";

   @RBEntry("La struttura XML specificata conteneva l'errore che segue.")
   public static final String SAXEXCEPTION = "22";

   @RBEntry("Il modello di forum discussione è stato creato.")
   public static final String CREATE_TEMPLATE_SUCCESS = "23";

   @RBEntry("Immettere un nome per il modello")
   public static final String NAME_EMPTY_ERROR = "24";

   @RBEntry("Problema verificatosi durante il parsing di xml (riga {0}, colonna {1}): {2}")
   public static final String PARSE_ERROR = "25";

   @RBEntry("Errore durante l'inizializzazione di \"{0}\".")
   public static final String ERROR_INITIALIZING = "26";

   @RBEntry("Discuti")
   public static final String DISCUSS_URL_LABEL = "27";

   @RBEntry("Errore")
   public static final String ERROR = "28";

   @RBEntry("Avvertenza")
   public static final String WARNING = "29";

   @RBEntry("Alla riga")
   public static final String AT_LINE = "30";

   @RBEntry("Colonna")
   public static final String COLUMN = "31";

   @RBEntry("Modello")
   public static final String TEMPLATE_LABEL = "32";

   @RBEntry("Non esistono ancora forum per quest'oggetto")
   public static final String NO_FORUMS_FOR_OBJECT = "33";

   @RBEntry("Forum per")
   public static final String FORUMS_FOR_OBJECT = "34";

   @RBEntry("Forum disponibili")
   public static final String AVAILABLE_FORUMS = "35";

   @RBEntry("Aggiungi forum")
   public static final String ADD_FORUM = "36";

   @RBEntry("Rimuovi forum")
   public static final String REMOVE_FORUM = "37";

   @RBEntry("Forum di discussione")
   public static final String DISCUSSION_FORUM_HEADER = "38";

   @RBEntry("L'oggetto trasmesso doveva essere un'istanza di SubjectOfForum.")
   public static final String SUBJECT_OF_FORUM_EXPECTED = "39";

   @RBEntry("Mostra forum discussioni")
   public static final String CREATE_DISCUSSION_FORUM_LINK = "40";

   @RBEntry("Il forum discussioni è stato creato per quest'oggetto")
   public static final String CREATE_FORUM_SUCCESS = "41";

   @RBEntry("Il forum discussioni è stato aggiunto per quest'oggetto")
   public static final String ADD_FORUM_SUCCESS = "42";

   @RBEntry("Il forum discussioni è stato rimosso per quest'oggetto")
   public static final String REMOVE_FORUM_SUCCESS = "43";

   @RBEntry("Proprietà di {0} per")
   public static final String FORUM_PROPERTIES_HEADER = "44";

   @RBEntry("Argomenti per \"{0}\"")
   public static final String TOPICS_FOR_FORUM = "45";

   @RBEntry("Annulla sottoscrizione")
   public static final String UNSUBSCRIBE_LABEL = "46";

   @RBEntry("Sottoscrivi")
   public static final String SUBSCRIBE_LABEL = "47";

   @RBEntry("Risposta al commento")
   public static final String REPLY_LABEL = "48";

   @RBEntry("Espandi tutto")
   public static final String EXPAND_ALL = "49";

   @RBEntry("Comprimi tutto")
   public static final String COLLAPSE_ALL = "50";

   @RBEntry("Nuovo argomento")
   public static final String NEW_TOPIC = "52";

   @RBEntry("Autore")
   public static final String CREATOR_LABEL = "53";

   @RBEntry("Messaggio:")
   public static final String MESSAGE_LABEL = "54";

   @RBEntry("Argomento per {0}")
   public static final String TOPIC_PROPERTIES_HEADER = "55";

   @RBEntry("In alto")
   public static final String TOP_LABEL = "56";

   @RBEntry("Indietro")
   public static final String PREVIOUS_LABEL = "57";

   @RBEntry("Avanti")
   public static final String NEXT_LABEL = "58";

   @RBEntry("Rispondi a ")
   public static final String REPLY_TO_POSTING_TITLE = "59";

   @RBEntry("Notificami i messaggi riguardanti il thread")
   public static final String SUBSCRIBE_TO_REPLY = "60";

   @RBEntry("Immettere testo semplice o tag HTML")
   public static final String HTML_AWARE_PROMPT = "61";

   @RBEntry("Ri")
   public static final String REPLY_PREFIX = "62";

   @RBEntry("Oggetto")
   public static final String SUBJECT_LABEL = "63";

   @RBEntry("Risposta creata")
   public static final String REPLY_SUCCESS = "64";

   @RBEntry("Discuti")
   public static final String DISCUSS = "65";

   @RBEntry("Cerca")
   public static final String SEARCH = "66";

   @RBEntry("Nuovo commento")
   public static final String CREATE_POSTING_LABEL = "67";

   @RBEntry("Immettere testo semplice o tag HTML")
   public static final String MESSAGE_INSTRUCTIONS = "68";

   @RBEntry("R: {0}")
   public static final String REPLY_SUBJECT_PREFIX = "69";

   @RBEntry("Rispondi e inserisci")
   public static final String REPLY_AND_POST_BUTTON = "70";

   @RBEntry("Inserisci annuncio")
   public static final String POST = "71";

   @RBEntry("Nuova discussione:")
   public static final String NEW_DISCUSSION_LABEL = "72";

   @RBEntry("Progetto:")
   public static final String PROJECT_LABEL = "73";

   @RBEntry("Discussione:")
   public static final String FORUM_LABEL = "74";

   @RBEntry("Testo commento:")
   public static final String DISCUSSION_BODY_LABEL = "75";

   @RBEntry("Mittente:")
   public static final String SENDER_LABEL = "76";

   @RBEntry("Inserisci annuncio")
   public static final String POST_BUTTON = "77";

   @RBEntry("nessun oggetto")
   public static final String NO_SUBJECT_TEXT = "78";

   @RBEntry("Rispondi al mittente")
   public static final String REPLY_TO_SENDER_LABEL = "79";

   @RBEntry("Notifica forum")
   public static final String FORUM_NOTIFICATION = "80";

   @RBEntry("Forum di discussione")
   public static final String DEFAULT_FORUM_NAME = "81";

   /**
    * Additional notification labels, etc
    **/
   @RBEntry("Autore progetto:")
   public static final String PROJECT_CREATOR_LABEL = "90";

   @RBEntry("Proprietario progetto:")
   public static final String PROJECT_OWNER_LABEL = "90a";

   @RBEntry("Organizzazione host:")
   public static final String PROJECT_HOST_LABEL = "91";

   @RBEntry("Descrizione progetto:")
   public static final String PROJECT_DESC_LABEL = "92";

   @RBEntry("Risposta al commento")
   public static final String REPLY_TO_POSTING = "93";

   @RBEntry("Nessuno")
   public static final String PROJECT_CREATOR_NONE = "94";

   @RBEntry("È stato creato un nuovo argomento nella discussione cui l'utente è sottoscritto.")
   public static final String SUBSCRIPTION_TOPIC_MSG = "95";

   @RBEntry("È stato creato un nuovo argomento in una discussione cui l'utente è sottoscritto. È possibile inserire un messaggio tramite il link Nuovo commento sottostante. ")
   public static final String NEW_TOPIC_MESSAGE = "96";

   @RBEntry("Nuovo argomento:")
   public static final String NEW_TOPIC_LABEL = "97";

   @RBEntry("Nuovo argomento nella discussione")
   public static final String SUBSCRIPTION_TOPIC_SUBJECT = "98";

   @RBEntry("Discussione del progetto")
   @RBComment("Title for a Project's discussion forum table ")
   public static final String PROJECT_DISCUSSION_FORUM = "99";

   @RBEntry("Forum di discussione del programma")
   @RBComment("Title for a Program's discussion forum table ")
   public static final String PROGRAM_DISCUSSION_FORUM = "99_prg";

   @RBEntry("Forum di discussione su {0}")
   @RBComment("(object that is being discussed eg. part or document) for (subject)")
   public static final String FORUM_FOR_SUBJECT = "100";

   @RBEntry("Forum di discussione {0}")
   @RBComment("Name for subscription to a DiscussionForum, for cases where the SubjectOfForum doesnt implement getName().")
   public static final String SUBSCRIPTION_IDENTITY = "101";

   @RBEntry("Forum di discussione {0}")
   @RBComment("Name for subscription to a DiscussionForum, for cases where the SubjectOfForum implements getName() but not getVersionDisplayIdentifier().")
   public static final String NAMED_SUBSCRIPTION_ID = "102";

   @RBEntry("Forum di discussione {0} {1}")
   @RBComment("Name for subscription to a DiscussionForum, for cases where the SubjectOfForum implements getName() and getVersionDisplayIdentifier(). 0=name,1=version.")
   public static final String NAMED_VERSIONED_SUBSCRIPTION_ID = "103";

   @RBEntry("Argomento '{0}' già chiuso")
   @RBComment("Message stating that specified topic is  already  closed")
   public static final String TOPIC_ALREADY_CLOSED = "TOPIC_ALREADY_CLOSED";   
}
