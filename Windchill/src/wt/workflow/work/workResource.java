/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.workflow.work;

import wt.util.resource.*;

@RBUUID("wt.workflow.work.workResource")
public final class workResource extends WTListResourceBundle {
   @RBEntry("Unknown user")
   public static final String UNKNOWN_USER = "0";

   @RBEntry("Cannot retrieve detailed data because current user not found")
   public static final String USER_NOT_FOUND = "1";

   @RBEntry("** Error **")
   public static final String ERROR_ALERT = "2";

   @RBEntry("Error reading wt.workflow.* properties")
   public static final String PROPERTIES_ERROR = "3";

   @RBEntry("Yes")
   public static final String COMPLETED_INDICATOR = "4";

   @RBEntry(" ")
   @RBPseudo(false)
   public static final String NOT_COMPLETED_INDICATOR = "5";

   @RBEntry("No Data Found")
   public static final String NO_DATA_FOUND = "6";

   @RBEntry("{0}")
   @RBArgComment0(" Notification subject.")
   public static final String NOTIFICATION_SUBJECT = "7";

   @RBEntry("The workflow template parameter was not found.")
   public static final String NO_TEMPLATE_PARAMETER = "8";

   @RBEntry("Initiate {0}")
   @RBArgComment0(" The name of the workflow process template")
   public static final String INITIATE_HEADLINE = "9";

   @RBEntry("Unable to build table of available workflow templates.")
   public static final String TEMPLATE_TABLE_FAILED = "10";

   @RBEntry("The work item has already been accepted.")
   public static final String ALREADY_ACCEPTED = "11";

   @RBEntry("The workitem parameter was not found.")
   public static final String MISSING_PARAMETER = "12";

   @RBEntry("Instructions")
   public static final String INSTRUCTIONS = "13";

   @RBEntry(":")
   @RBPseudo(false)
   public static final String COLON = "14";

   @RBEntry("Variable Name")
   public static final String VARIABLE_NAME = "15";

   @RBEntry("Value")
   public static final String VALUE = "16";

   @RBEntry("Workflow Task")
   public static final String WORKFLOW_TASK_TITLE = "17";

   @RBEntry("A POST request was expected but not received.")
   public static final String EXPECTED_POST_REQUEST = "18";

   @RBEntry("Work item has been successfully completed.")
   public static final String WORKITEM_COMPLETE = "19";

   @RBEntry("The workitem could not be completed because of the following error: ")
   public static final String WORKITEM_NOT_COMPLETE = "20";

   @RBEntry("The target object is not available.")
   public static final String TARGET_OBJECT_GONE = "21";

   @RBEntry("This object is not available.")
   public static final String PROCESS_VARIABLE_GONE = "22";

   @RBEntry("Task Complete")
   public static final String TASK_COMPLETE_BUTTON = "23";

   @RBEntry("Process Name")
   public static final String PROCESS_NAME = "24";

   @RBEntry("Due Date")
   public static final String DUE_DATE = "25";

   @RBEntry("Process Initiator")
   public static final String PROCESS_INITIATOR = "26";

   @RBEntry("Assignee")
   public static final String ASSIGNEE = "27";

   @RBEntry("Target Object")
   public static final String TARGET_OBJECT = "28";

   @RBEntry("Update")
   public static final String UPDATE_LABEL = "29";

   @RBEntry("Content")
   public static final String CONTENT_LABEL = "30";

   @RBEntry("Process Instructions")
   public static final String PROCESS_INSTRUCTIONS = "31";

   @RBEntry("{0} role could not be resolved from process {1}")
   @RBArgComment0("role name")
   @RBArgComment1("workflow process name")
   public static final String UNRESOLVABLE_ROLE = "32";

   @RBEntry("NOTICE: This activity is not assigned to anyone.  Reassign it to someone who can complete the work.")
   public static final String NO_ASSIGNEE_INSTRUCTIONS = "33";

   @RBEntry("This task is no longer valid because it has been completed")
   public static final String OBSOLETE_WORKITEM = "34";

   @RBEntry("{0} data must be of type {1}")
   @RBArgComment0("field name")
   @RBArgComment1("expected data type")
   public static final String INVALID_VARIABLE_TYPE = "35";

   @RBEntry("You must select at least one route to complete this task.  Page back and make a routing selection.")
   public static final String NO_ROUTE_SELECTED = "36";

   @RBEntry("Cannot reassign activity: no alternative principal defined")
   public static final String NO_ALTERNATIVE_PRINCIPAL = "37";

   @RBEntry("Cannot reassign activity: activity has multiple assignments")
   public static final String MULTIPLE_ASSIGNMENTS = "38";

   @RBEntry("Cannot reassign activity: new principal is the current assignee: \"{0}\"")
   @RBArgComment0("name of the user to which the activity is reassigned")
   public static final String SAME_ASSIGNEE = "39";

   @RBEntry("Cannot reassign activity: assignment has multiple assignees")
   public static final String MULTIPLE_ASSIGNEES = "40";

   @RBEntry("Cannot reassign activity: assignment has multiple work items")
   public static final String MULTIPLE_WORK_ITEMS = "41";

   @RBEntry("Cannot make assignment to deleted principal not allowed.")
   public static final String DELETED_PRINCIPAL = "42";

   @RBEntry("Cannot make assignment to deleted CREATOR not allowed.")
   public static final String DELETED_CREATOR = "43";

   @RBEntry("Discuss")
   @RBComment("Tool tip for the Discuss icon related to a work item")
   public static final String DISCUSS = "44";

   @RBEntry("View Information")
   @RBComment("Tool tip for the Details icon related to a work item")
   public static final String DETAILS = "45";

   @RBEntry("This task is no longer valid because it has been completed")
   public static final String WORKITEM_ALREADY_COMPLETED = "46";

   @RBEntry("** Error ** : Task may have been completed or does not exist")
   public static final String ERROR_ALERT_TASK = "137";

   /**
    * Buttons --------------------------------------------------------
    **/
   @RBEntry("Help")
   public static final String HELP_BUTTON = "50";

   @RBEntry("Submit")
   public static final String SUBMIT_BUTTON = "51";

   /**
    * Labels --------------------------------------------------------
    **/
   @RBEntry("Sort By")
   public static final String SORT_BY = "55";

   @RBEntry("Group By")
   public static final String GROUP_BY = "56";

   @RBEntry("Configuration")
   @RBComment("The label for the configuration section of related processes.  The translation can be found in the 7.0 template srcwtworkflowhtmltmplAssociatedProcess*.html")
   public static final String CONFIGURATION = "57";

   /**
    * Titles --------------------------------------------------------
    **/
   @RBEntry("Processes associated with {0}")
   public static final String ASSOCIATED_PROCESS_HEADLINE = "60";

   @RBEntry("Associated Workflow Processes")
   public static final String ASSOCIATED_PROCESSES_TITLE = "61";

   @RBEntry("Select Workflow")
   public static final String SELECT_WORKFLOW_TITLE = "62";

   /**
    * Localization: This value is originally translated in the
    * <TITLE></TITLE> tag of the InitiateWorkflow HTML template
    **/
   @RBEntry("Initiate Workflow")
   public static final String INITIATE_WORKFLOW_TITLE = "63";

   /**
    * Column Headers --------------------------------------------------------
    **/
   @RBEntry("Process")
   public static final String ASSOCIATED_PROCESS_COLUMN = "65";

   @RBEntry("Owner")
   public static final String ASSOCIATED_OWNER_COLUMN = "66";

   @RBEntry("State")
   @RBComment("Translator--\"State\" is the lifecycle state of an object")
   public static final String ASSOCIATED_STATE_COLUMN = "67";

   @RBEntry("Template Name")
   public static final String TEMPLATE_NAME_HEADER = "68";

   @RBEntry("Category")
   public static final String CATEGORY_HEADER = "69";

   @RBEntry("Description")
   public static final String DESCRIPTION_HEADER = "70";

   /**
    * Text & Messages -------------------------------------------------------
    **/
   @RBEntry("Reassign")
   public static final String REASSIGN = "75";

   @RBEntry("Accept work item {0} ?")
   @RBArgComment0(" identifying attribute of the workitem to be accepted")
   public static final String ACCEPT_WORKITEM = "76";

   @RBEntry("There are no objects to display")
   public static final String NO_ITEMS_TO_DISPLAY = "77";

   @RBEntry("There are no events to tally")
   public static final String NO_EVENTS_TO_TALLY = "78";

   /**
    * Attribute table headers ------------------------------------------------
    **/
   @RBEntry("Not Available")
   public static final String NOT_AVAILABLE = "85";

   @RBEntry("Select Workflow Process")
   public static final String SELECT_WORKFLOW_PROCESS_LABEL = "86";

   /**
    * Workitem priorities ---------------------------------------------------
    **/
   @RBEntry("Highest")
   public static final String PRIORITY_ONE = "90";

   @RBEntry("High")
   public static final String PRIORITY_TWO = "91";

   @RBEntry("Normal")
   public static final String PRIORITY_THREE = "92";

   @RBEntry("Low")
   public static final String PRIORITY_FOUR = "93";

   @RBEntry("Lowest")
   public static final String PRIORITY_FIVE = "94";

   /**
    * Online help   ------------------------------------------------------------
    * Help/WorkNotification/MainHelp.value=wt/clients/workflow/worklist/help_en/WorkFlowTaskHelp.html
    **/
   @RBEntry("WFTaskOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_0 = "Help/WorkNotification/MainHelp";

   /**
    * --------------------------------------------------------------------------
    **/
   @RBEntry("Check out and edit")
   public static final String CHECKOUT_EDIT = "100";

   @RBEntry("IgnoreUnresolvedRole Notification")
   public static final String IGNORE_UNRESOLVED_ROLE_SUBJECT = "101";

   @RBEntry("In process {1}, the {2} activity may be hung because the {0} role was not resolved, and the wt.properties IgnoreUnresolvedRole flag is true.")
   @RBArgComment0(" {0} - role name")
   @RBArgComment1(" {1} - workflow process name")
   @RBArgComment2(" {2} - activity name")
   public static final String IGNORE_UNRESOLVED_ROLE_BODY = "102";

   /**
    * Notifications
    **/
   @RBEntry("Project Name:")
   public static final String NOTIFICATION_PROJECT_NAME = "110";

   @RBEntry("Project Creator:")
   public static final String NOTIFICATION_PROJECT_CREATOR = "111";

   @RBEntry("Project Owner:")
   public static final String NOTIFICATION_PROJECT_OWNER = "111a";

   @RBEntry("Host Organization:")
   public static final String NOTIFICATION_PROJECT_HOST = "112";

   @RBEntry("Project Description:")
   public static final String NOTIFICATION_PROJECT_DESC = "113";

   @RBEntry("None")
   public static final String NOTIFICATION_NONE = "114";

   @RBEntry("The selected user does not have sufficient access to accept the task.  Select a different user.")
   public static final String INSUFFICIENT_RIGHTS = "115";

   @RBEntry("Product Name:")
   public static final String NOTIFICATION_PRODUCT_NAME = "116";

   @RBEntry("Product Creator:")
   public static final String NOTIFICATION_PRODUCT_CREATOR = "117";

   @RBEntry("Host Organization:")
   public static final String NOTIFICATION_PRODUCT_HOST = "118";

   @RBEntry("Product Description:")
   public static final String NOTIFICATION_PRODUCT_DESC = "119";

   @RBEntry("Library Name:")
   public static final String NOTIFICATION_LIBRARY_NAME = "120";

   @RBEntry("Library Creator:")
   public static final String NOTIFICATION_LIBRARY_CREATOR = "121";

   @RBEntry("Host Organization:")
   public static final String NOTIFICATION_LIBRARY_HOST = "122";

   @RBEntry("Library Description:")
   public static final String NOTIFICATION_LIBRARY_DESC = "123";

   @RBEntry("{0}")
   @RBArgComment0(" The workflow activity name")
   public static final String TASK_NOTIFICATION_SUBJECT = "124";

   @RBEntry("You have been assigned a task -")
   public static final String ASSIGNMENT_DESC = "133";

   @RBEntry("To take action click on the following url:")
   public static final String ACTIONLINK_DESC = "134";

   @RBEntry("Detail information of this workflow task:")
   public static final String DETAILED_INFO_DESC = "135";

   @RBEntry("For help see:")
   public static final String HELP_DESC = "136";

   @RBEntry("Primary Business Object")
   public static final String PBO = "138";

   @RBEntry("Task Notification")
   public static final String TASK_NOTIFICATION_TITLE = "139";

   @RBEntry("Variable")
   public static final String VARIABLE = "140";

   @RBEntry("Quality Name:")
   public static final String NOTIFICATION_QMS_LIBRARY_NAME = "166";

   @RBEntry("Quality Creator:")
   public static final String NOTIFICATION_QMS_LIBRARY_CREATOR = "167";

   @RBEntry("Quality Description:")
   public static final String NOTIFICATION_QMS_LIBRARY_DESC = "168";

   /**
    * ---------------------------------------------------------------------------------
    * Labels for email Notifications generated by Notification Robots.
    * These Strings are coming from wt/workflow/htmltmpl/NotificationRobot.html
    * ----------------------------------------------------------------------------------
    **/
   @RBEntry("Process Notification")
   public static final String PROC_NOTIF = "125";

   @RBEntry("Process:")
   public static final String PROC = "126";

   @RBEntry("Process description:")
   public static final String PROC_DESC = "127";

   @RBEntry("Activity name:")
   public static final String ACT_NAME = "128";

   @RBEntry("Activity description:")
   public static final String ACT_DESC = "129";

   @RBEntry("Subject object:")
   public static final String SUB_OBJ = "130";

   @RBEntry("Business Object:")
   public static final String BUSINESS_OBJECT_COLON = "131";

   @RBEntry("Go to Process Manager:")
   public static final String GOTO_PROCMANAGER_COLON = "132";

   @RBEntry("Process notification: Generated by robot {0} in process {1}")
   @RBArgComment0(" {0} - activity name")
   @RBArgComment1(" {1} - workflow process name")
   public static final String NOTIFICATION_DEFAULT_SUBJECT = "141";

   /**
    * -------------------------------------------------------------------------
    * 
    * Display names for the preference categories and basic definitions
    * that are used in Workflow/work folder.
    * 
    * -------------------------------------------------------------------------
    **/
   @RBEntry("Workflow")
   @RBComment("Workflow Preferences.")
   public static final String WORKFLOW_CAT_NAME = "WORKFLOW_CAT_NAME";

   @RBEntry("Workflow preferences")
   public static final String WORKFLOW_CAT_DESCR = "WORKFLOW_CAT_DESCR";

   @RBEntry("Work")
   @RBComment("Workflow Preferences for work specific operations.")
   public static final String WORK_CAT_NAME = "WORK_CAT_NAME";

   @RBEntry("Workflow Preferences for work specific operations.")
   public static final String WORK_CAT_DESCR = "WORK_CAT_DESCR";

   @RBEntry("Delete Completed WorkItems preference")
   @RBComment("Indicates the number of completed WorkItems to delete.")
   public static final String DELETE_COMPLETED_WORKITEMS_PREF = "DELETE_COMPLETED_WORKITEMS_PREF";

   @RBEntry("Indicates the number of completed WorkItems to be deleted.")
   public static final String DELETE_COMPLETED_WORKITEMS_PREF_DESCR = "DELETE_COMPLETED_WORKITEMS_PREF_DESCR";

   @RBEntry("Deletes the completed workitems for a specific principal in a specific container. Reads the preferences before deleting the workitems. The preference value can be a number or 'ALL' or 'NO'. If it is 'NO', then completed workitems are not deleted. If 'ALL', then all completed workitems are deleted. If it is a number then the latest n completed WorkItems will not be deleted. The rest will be deleted. The deletion of completed workitems is spawned only when the number of excess completed workitems is twice the specified preference. For. eg. if the preference is 10 then the deletion of workitems is spawned only when the no. of excess workitems is 20. Note that a workitem is deleted only if the parent process for it is 'CLOSED'. The completed ProjectWorkItems are not deleted.")
   public static final String DELETE_COMPLETED_WORKITEMS_PREF_LONG_DESCR = "DELETE_COMPLETED_WORKITEMS_PREF_LONG_DESCR";

   @RBEntry("Workflow Process Manager Link Display")
   @RBComment("Expose Process Manager comment")
   public static final String EXPOSE_PROCESS_MANAGER_PREF = "EXPOSE_PROCESS_MANAGER_PREF";

   @RBEntry("Enables workflow process manager link on workflow task information pages.")
   public static final String EXPOSE_PROCESS_MANAGER_PREF_DESCR = "EXPOSE_PROCESS_MANAGER_PREF_DESCR";

   @RBEntry("Determines if the workflow process manager applet link on workflow task information pages is shown. If set to Administrators only, the link will display only for members of the Administrators group or Organization Administrators. If set to All internal users, the link will display for all internal users.  The default is Administrators only.")
   public static final String EXPOSE_PROCESS_MANAGER_PREF_LONG_DESCR = "EXPOSE_PROCESS_MANAGER_PREF_LONG_DESCR";

   @RBEntry("Administrators only")
   @RBComment("Value for Expose Process Manager preferences")
   public static final String ADMINISTRATOR_PREF_VALUE = "ADMINISTRATOR_PREF_VALUE";

   @RBEntry("All internal users")
   @RBComment("Value for Expose Process Manager preferences")
   public static final String ALL_PREF_VALUE = "ALL_PREF_VALUE";
   
   @RBEntry("Use task form template to generate the task details page")
   @RBComment("Flag to display task form template in task details page.")
   public static final String TASK_FORM_TEMPLATE_PREF="TASK_FORM_TEMPLATE_PREF";
   
   @RBEntry("Determines if task form templates are used to render a task")
   public static final String TASK_FORM_TEMPLATE_PREF_DESC="TASK_FORM_TEMPLATE_PREF_DESC";
   
   @RBEntry("If a task form template has been defined for the task type and the primary business object type of a task, uses the template to render the task details page for the task. If no template has been defined, the task defaults to using multiple tabs to display sections of the task details page.")
   public static final String TASK_FORM_TEMPLATE_PREF_LONG_DESC="TASK_FORM_TEMPLATE_PREF_LONG_DESC";
   
   @RBEntry("WTKEY_SEQ's current value is lesser than wtkey values used in the system. Correct the sequence appropriately.")
   public static final String DUPLICATE_WTKEY_SEQ_ERROR = "DUPLICATE_WTKEY_SEQ_ERROR";
   
}
