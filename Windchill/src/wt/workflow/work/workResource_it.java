/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.workflow.work;

import wt.util.resource.*;

@RBUUID("wt.workflow.work.workResource")
public final class workResource_it extends WTListResourceBundle {
   @RBEntry("Utente sconosciuto")
   public static final String UNKNOWN_USER = "0";

   @RBEntry("L'utente attuale non è stato trovato. Impossibile recuperare i dati dettagliati.")
   public static final String USER_NOT_FOUND = "1";

   @RBEntry("** Errore **")
   public static final String ERROR_ALERT = "2";

   @RBEntry("Errore durante la lettura delle proprietà wt.workflow.*")
   public static final String PROPERTIES_ERROR = "3";

   @RBEntry("Sì")
   public static final String COMPLETED_INDICATOR = "4";

   @RBEntry(" ")
   @RBPseudo(false)
   public static final String NOT_COMPLETED_INDICATOR = "5";

   @RBEntry("Nessun dato individuato")
   public static final String NO_DATA_FOUND = "6";

   @RBEntry("{0}")
   @RBArgComment0(" Notification subject.")
   public static final String NOTIFICATION_SUBJECT = "7";

   @RBEntry("Il parametro del modello di workflow non è stato trovato.")
   public static final String NO_TEMPLATE_PARAMETER = "8";

   @RBEntry("Avvia {0}")
   @RBArgComment0(" The name of the workflow process template")
   public static final String INITIATE_HEADLINE = "9";

   @RBEntry("Impossibile compilare la tabella dei modelli di workflow disponibili.")
   public static final String TEMPLATE_TABLE_FAILED = "10";

   @RBEntry("Il task è già stato accettato.")
   public static final String ALREADY_ACCEPTED = "11";

   @RBEntry("Il parametro relativo al task non è stato trovato.")
   public static final String MISSING_PARAMETER = "12";

   @RBEntry("Istruzioni")
   public static final String INSTRUCTIONS = "13";

   @RBEntry(":")
   @RBPseudo(false)
   public static final String COLON = "14";

   @RBEntry("Nome variabile")
   public static final String VARIABLE_NAME = "15";

   @RBEntry("Valore")
   public static final String VALUE = "16";

   @RBEntry("Task di workflow")
   public static final String WORKFLOW_TASK_TITLE = "17";

   @RBEntry("Richiesta POST attesa non ricevuta.")
   public static final String EXPECTED_POST_REQUEST = "18";

   @RBEntry("Il task è stato completato.")
   public static final String WORKITEM_COMPLETE = "19";

   @RBEntry("Impossibile completare il task. Si è verificato il seguente errore: ")
   public static final String WORKITEM_NOT_COMPLETE = "20";

   @RBEntry("L'oggetto di destinazione non è disponibile.")
   public static final String TARGET_OBJECT_GONE = "21";

   @RBEntry("L'oggetto non è disponibile.")
   public static final String PROCESS_VARIABLE_GONE = "22";

   @RBEntry("Completa task")
   public static final String TASK_COMPLETE_BUTTON = "23";

   @RBEntry("Nome processo")
   public static final String PROCESS_NAME = "24";

   @RBEntry("Data di scadenza")
   public static final String DUE_DATE = "25";

   @RBEntry("Iniziatore processo")
   public static final String PROCESS_INITIATOR = "26";

   @RBEntry("Assegnatario")
   public static final String ASSIGNEE = "27";

   @RBEntry("Oggetto di destinazione")
   public static final String TARGET_OBJECT = "28";

   @RBEntry("Aggiornamento")
   public static final String UPDATE_LABEL = "29";

   @RBEntry("Contenuto")
   public static final String CONTENT_LABEL = "30";

   @RBEntry("Istruzioni del processo")
   public static final String PROCESS_INSTRUCTIONS = "31";

   @RBEntry("{0} impossibile ricavare il ruolo dal processo {1}")
   @RBArgComment0("role name")
   @RBArgComment1("workflow process name")
   public static final String UNRESOLVABLE_ROLE = "32";

   @RBEntry("AVVISO: L'attività non ha assegnatari. Riassegnare l'attività a un utente che possa completare il lavoro.")
   public static final String NO_ASSIGNEE_INSTRUCTIONS = "33";

   @RBEntry("Il task è stato completato e non è più valido")
   public static final String OBSOLETE_WORKITEM = "34";

   @RBEntry("I dati {0} devono essere di tipo {1}")
   @RBArgComment0("field name")
   @RBArgComment1("expected data type")
   public static final String INVALID_VARIABLE_TYPE = "35";

   @RBEntry("Per completare il task è necessario selezionare almeno un instradamento. Andare alla pagina precedente e selezionare un instradamento.")
   public static final String NO_ROUTE_SELECTED = "36";

   @RBEntry("Impossibile riassegnare l'attività: non è stato definito alcun utente/gruppo/ruolo alternativo")
   public static final String NO_ALTERNATIVE_PRINCIPAL = "37";

   @RBEntry("Impossibile riassegnare l'attività: l'attività comprende più assegnazioni")
   public static final String MULTIPLE_ASSIGNMENTS = "38";

   @RBEntry("Impossibile riassegnare l'attività: il nuovo utente/gruppo/ruolo è l'assegnatario attuale: \"{0}\"")
   @RBArgComment0("name of the user to which the activity is reassigned")
   public static final String SAME_ASSIGNEE = "39";

   @RBEntry("Impossibile riassegnare l'attività: l'assegnazione ha più assegnatari")
   public static final String MULTIPLE_ASSIGNEES = "40";

   @RBEntry("Impossibile riassegnare l'attività: l'assegnazione ha più task")
   public static final String MULTIPLE_WORK_ITEMS = "41";

   @RBEntry("Impossibile eseguire un'assegnazione a un utente/gruppo/ruolo eliminato.")
   public static final String DELETED_PRINCIPAL = "42";

   @RBEntry("Impossibile eseguire un'assegnazione all'AUTORE se è stato eliminato.")
   public static final String DELETED_CREATOR = "43";

   @RBEntry("Discuti")
   @RBComment("Tool tip for the Discuss icon related to a work item")
   public static final String DISCUSS = "44";

   @RBEntry("Visualizza informazioni")
   @RBComment("Tool tip for the Details icon related to a work item")
   public static final String DETAILS = "45";

   @RBEntry("Il task è stato completato e non è più valido")
   public static final String WORKITEM_ALREADY_COMPLETED = "46";

   @RBEntry("** Errore ** : è possibile che il task sia già completato o che non esista")
   public static final String ERROR_ALERT_TASK = "137";

   /**
    * Buttons --------------------------------------------------------
    **/
   @RBEntry("Guida")
   public static final String HELP_BUTTON = "50";

   @RBEntry("Invia")
   public static final String SUBMIT_BUTTON = "51";

   /**
    * Labels --------------------------------------------------------
    **/
   @RBEntry("Ordina per ")
   public static final String SORT_BY = "55";

   @RBEntry("Raggruppa per ")
   public static final String GROUP_BY = "56";

   @RBEntry("Configurazione")
   @RBComment("The label for the configuration section of related processes.  The translation can be found in the 7.0 template srcwtworkflowhtmltmplAssociatedProcess*.html")
   public static final String CONFIGURATION = "57";

   /**
    * Titles --------------------------------------------------------
    **/
   @RBEntry("Processi associati a {0}")
   public static final String ASSOCIATED_PROCESS_HEADLINE = "60";

   @RBEntry("Processi di workflow associati")
   public static final String ASSOCIATED_PROCESSES_TITLE = "61";

   @RBEntry("Seleziona workflow")
   public static final String SELECT_WORKFLOW_TITLE = "62";

   /**
    * Localization: This value is originally translated in the
    * <TITLE></TITLE> tag of the InitiateWorkflow HTML template
    **/
   @RBEntry("Inizia workflow")
   public static final String INITIATE_WORKFLOW_TITLE = "63";

   /**
    * Column Headers --------------------------------------------------------
    **/
   @RBEntry("Processo")
   public static final String ASSOCIATED_PROCESS_COLUMN = "65";

   @RBEntry("Proprietario")
   public static final String ASSOCIATED_OWNER_COLUMN = "66";

   @RBEntry("Stato del ciclo di vita")
   @RBComment("Translator--\"State\" is the lifecycle state of an object")
   public static final String ASSOCIATED_STATE_COLUMN = "67";

   @RBEntry("Nome modello")
   public static final String TEMPLATE_NAME_HEADER = "68";

   @RBEntry("Categoria")
   public static final String CATEGORY_HEADER = "69";

   @RBEntry("Descrizione")
   public static final String DESCRIPTION_HEADER = "70";

   /**
    * Text & Messages -------------------------------------------------------
    **/
   @RBEntry("Riassegna")
   public static final String REASSIGN = "75";

   @RBEntry("Accettare il task {0}?")
   @RBArgComment0(" identifying attribute of the workitem to be accepted")
   public static final String ACCEPT_WORKITEM = "76";

   @RBEntry("Nessun oggetto da visualizzare")
   public static final String NO_ITEMS_TO_DISPLAY = "77";

   @RBEntry("Nessun evento da conteggiare")
   public static final String NO_EVENTS_TO_TALLY = "78";

   /**
    * Attribute table headers ------------------------------------------------
    **/
   @RBEntry("Non disponibile")
   public static final String NOT_AVAILABLE = "85";

   @RBEntry("Seleziona processo workflow")
   public static final String SELECT_WORKFLOW_PROCESS_LABEL = "86";

   /**
    * Workitem priorities ---------------------------------------------------
    **/
   @RBEntry("Massima")
   public static final String PRIORITY_ONE = "90";

   @RBEntry("Alta")
   public static final String PRIORITY_TWO = "91";

   @RBEntry("Normale")
   public static final String PRIORITY_THREE = "92";

   @RBEntry("Bassa")
   public static final String PRIORITY_FOUR = "93";

   @RBEntry("Minima")
   public static final String PRIORITY_FIVE = "94";

   /**
    * Online help   ------------------------------------------------------------
    * Help/WorkNotification/MainHelp.value=wt/clients/workflow/worklist/help_en/WorkFlowTaskHelp.html
    **/
   @RBEntry("WFTaskOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_0 = "Help/WorkNotification/MainHelp";

   /**
    * --------------------------------------------------------------------------
    **/
   @RBEntry("Check-Out e modifica")
   public static final String CHECKOUT_EDIT = "100";

   @RBEntry("Notifica di IgnoreUnresolvedRole")
   public static final String IGNORE_UNRESOLVED_ROLE_SUBJECT = "101";

   @RBEntry("Nel processo {1}, l'attività {2} può essere sospesa perché il ruolo {0} non è stato definito, e IgnoreUnresolvedRole di wt.prperties è impostato a true.")
   @RBArgComment0(" {0} - role name")
   @RBArgComment1(" {1} - workflow process name")
   @RBArgComment2(" {2} - activity name")
   public static final String IGNORE_UNRESOLVED_ROLE_BODY = "102";

   /**
    * Notifications
    **/
   @RBEntry("Nome progetto:")
   public static final String NOTIFICATION_PROJECT_NAME = "110";

   @RBEntry("Autore progetto:")
   public static final String NOTIFICATION_PROJECT_CREATOR = "111";

   @RBEntry("Proprietario progetto:")
   public static final String NOTIFICATION_PROJECT_OWNER = "111a";

   @RBEntry("Organizzazione host:")
   public static final String NOTIFICATION_PROJECT_HOST = "112";

   @RBEntry("Descrizione progetto:")
   public static final String NOTIFICATION_PROJECT_DESC = "113";

   @RBEntry("Nessuno")
   public static final String NOTIFICATION_NONE = "114";

   @RBEntry("L'utente selezionato non dispone di sufficienti diritti d'accesso per accettare il task. Selezionare un utente diverso.")
   public static final String INSUFFICIENT_RIGHTS = "115";

   @RBEntry("Nome prodotto:")
   public static final String NOTIFICATION_PRODUCT_NAME = "116";

   @RBEntry("Autore prodotto:")
   public static final String NOTIFICATION_PRODUCT_CREATOR = "117";

   @RBEntry("Organizzazione host:")
   public static final String NOTIFICATION_PRODUCT_HOST = "118";

   @RBEntry("Descrizione prodotto:")
   public static final String NOTIFICATION_PRODUCT_DESC = "119";

   @RBEntry("Nome libreria:")
   public static final String NOTIFICATION_LIBRARY_NAME = "120";

   @RBEntry("Autore libreria:")
   public static final String NOTIFICATION_LIBRARY_CREATOR = "121";

   @RBEntry("Organizzazione host:")
   public static final String NOTIFICATION_LIBRARY_HOST = "122";

   @RBEntry("Descrizione libreria:")
   public static final String NOTIFICATION_LIBRARY_DESC = "123";

   @RBEntry("{0}")
   @RBArgComment0(" The workflow activity name")
   public static final String TASK_NOTIFICATION_SUBJECT = "124";

   @RBEntry("È stata ricevuta l'assegnazione di un task -")
   public static final String ASSIGNMENT_DESC = "133";

   @RBEntry("Per eseguire l'azione, fare clic sul seguente URL:")
   public static final String ACTIONLINK_DESC = "134";

   @RBEntry("Informazioni dettagliate su questo task di workflow:")
   public static final String DETAILED_INFO_DESC = "135";

   @RBEntry("Per informazioni, vedere:")
   public static final String HELP_DESC = "136";

   @RBEntry("Business object principale")
   public static final String PBO = "138";

   @RBEntry("Notifica task")
   public static final String TASK_NOTIFICATION_TITLE = "139";

   @RBEntry("Variabile")
   public static final String VARIABLE = "140";

   @RBEntry("Nome qualità:")
   public static final String NOTIFICATION_QMS_LIBRARY_NAME = "166";

   @RBEntry("Autore qualità:")
   public static final String NOTIFICATION_QMS_LIBRARY_CREATOR = "167";

   @RBEntry("Descrizione qualità:")
   public static final String NOTIFICATION_QMS_LIBRARY_DESC = "168";

   /**
    * ---------------------------------------------------------------------------------
    * Labels for email Notifications generated by Notification Robots.
    * These Strings are coming from wt/workflow/htmltmpl/NotificationRobot.html
    * ----------------------------------------------------------------------------------
    **/
   @RBEntry("Notifica di processo")
   public static final String PROC_NOTIF = "125";

   @RBEntry("Processo:")
   public static final String PROC = "126";

   @RBEntry("Descrizione del processo:")
   public static final String PROC_DESC = "127";

   @RBEntry("Nome attività:")
   public static final String ACT_NAME = "128";

   @RBEntry("Descrizione attività:")
   public static final String ACT_DESC = "129";

   @RBEntry("Oggetto discussione:")
   public static final String SUB_OBJ = "130";

   @RBEntry("Business object:")
   public static final String BUSINESS_OBJECT_COLON = "131";

   @RBEntry("Vai a Gestione processi:")
   public static final String GOTO_PROCMANAGER_COLON = "132";

   @RBEntry("Notifica di processo generata automaticamente da {0} nel processo {1}")
   @RBArgComment0(" {0} - activity name")
   @RBArgComment1(" {1} - workflow process name")
   public static final String NOTIFICATION_DEFAULT_SUBJECT = "141";

   /**
    * -------------------------------------------------------------------------
    * 
    * Display names for the preference categories and basic definitions
    * that are used in Workflow/work folder.
    * 
    * -------------------------------------------------------------------------
    **/
   @RBEntry("Workflow")
   @RBComment("Workflow Preferences.")
   public static final String WORKFLOW_CAT_NAME = "WORKFLOW_CAT_NAME";

   @RBEntry("Preferenze workflow")
   public static final String WORKFLOW_CAT_DESCR = "WORKFLOW_CAT_DESCR";

   @RBEntry("Lavoro")
   @RBComment("Workflow Preferences for work specific operations.")
   public static final String WORK_CAT_NAME = "WORK_CAT_NAME";

   @RBEntry("Preferenze workflow per operazioni specifiche dell'attività.")
   public static final String WORK_CAT_DESCR = "WORK_CAT_DESCR";

   @RBEntry("Preferenza eliminazione task completati")
   @RBComment("Indicates the number of completed WorkItems to delete.")
   public static final String DELETE_COMPLETED_WORKITEMS_PREF = "DELETE_COMPLETED_WORKITEMS_PREF";

   @RBEntry("Indica il numero di task completati da eliminare.")
   public static final String DELETE_COMPLETED_WORKITEMS_PREF_DESCR = "DELETE_COMPLETED_WORKITEMS_PREF_DESCR";

   @RBEntry("Elimina i task completati da un utente/gruppo/ruolo specifico in un determinato contesto. Prima di eliminare i task, esamina le preferenze. Il valore delle preferenze può essere 'TUTTI', 'NO' oppure un numero. Se il valore impostato è 'NO', i task completati non vengono eliminati. Se il valore è 'TUTTI', tutti i task completati vengono eliminati. Se invece è impostato un valore numerico, vengono eliminati tutti i task ad eccezione degli ultimi n task completati (dove n è il numero immesso). L'eliminazione dei task avviene solo quando il numero di task completati in eccesso è doppio rispetto al valore indicato dalla preferenza. Ad esempio, se il valore assegnato alla preferenza è 10, l'eliminazione dei task si verifica solo quando il numero di task in eccesso è 20. Si noti che un task viene eliminati solo se il relativo processo padre è 'CHIUSO'. I task di progetto completati non vengono eliminati.")
   public static final String DELETE_COMPLETED_WORKITEMS_PREF_LONG_DESCR = "DELETE_COMPLETED_WORKITEMS_PREF_LONG_DESCR";

   @RBEntry("Visibilità link Gestione processi di workflow")
   @RBComment("Expose Process Manager comment")
   public static final String EXPOSE_PROCESS_MANAGER_PREF = "EXPOSE_PROCESS_MANAGER_PREF";

   @RBEntry("Visualizza li link a Gestione processi di workflow nelle pagine di informazioni sui task di workflow")
   public static final String EXPOSE_PROCESS_MANAGER_PREF_DESCR = "EXPOSE_PROCESS_MANAGER_PREF_DESCR";

   @RBEntry("Determina se il link all'applet Gestione processi di workflow è visibile. Se impostata su Solo amministratori, il link è visibile solo ai membri del gruppo Amministratori o Aministratori organizzazione. Se impostata su Tutti gli utenti interni, il link sarà visibile a tutti gli utenti interni.  Il default è Solo amministratori.")
   public static final String EXPOSE_PROCESS_MANAGER_PREF_LONG_DESCR = "EXPOSE_PROCESS_MANAGER_PREF_LONG_DESCR";

   @RBEntry("Solo amministratori")
   @RBComment("Value for Expose Process Manager preferences")
   public static final String ADMINISTRATOR_PREF_VALUE = "ADMINISTRATOR_PREF_VALUE";

   @RBEntry("Tutti gli utenti interni")
   @RBComment("Value for Expose Process Manager preferences")
   public static final String ALL_PREF_VALUE = "ALL_PREF_VALUE";
   
   @RBEntry("Utilizzare il modello di modulo task per generare la pagina dettagli task")
   @RBComment("Flag to display task form template in task details page.")
   public static final String TASK_FORM_TEMPLATE_PREF="TASK_FORM_TEMPLATE_PREF";
   
   @RBEntry("Determina se debbano essere utilizzati modelli di modulo task per eseguire il rendering di un task.")
   public static final String TASK_FORM_TEMPLATE_PREF_DESC="TASK_FORM_TEMPLATE_PREF_DESC";
   
   @RBEntry("Se è stato definito un modello di modulo task per il tipo di task e il tipo di business object principale di un task, viene utilizzato il modello per eseguire il rendering della pagina dettagli task per il task. Se non è stato definito alcun modello, per default il task utilizza varie schede per visualizzare sezioni della pagina dettagli task.")
   public static final String TASK_FORM_TEMPLATE_PREF_LONG_DESC="TASK_FORM_TEMPLATE_PREF_LONG_DESC";
   
   @RBEntry("Il valore corrente di WTKEY_SEQ è inferiore ai valori wtkey utilizzati nel sistema. Correggere la sequenza.")
   public static final String DUPLICATE_WTKEY_SEQ_ERROR = "DUPLICATE_WTKEY_SEQ_ERROR";
   
}
