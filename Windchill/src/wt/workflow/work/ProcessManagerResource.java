/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.workflow.work;

import wt.util.resource.*;

@RBUUID("wt.workflow.work.ProcessManagerResource")
public final class ProcessManagerResource extends WTListResourceBundle {
   /**
    * field messages -------------------------------------------------------
    **/
   @RBEntry("Not Available")
   public static final String NOT_AVAILABLE_MSG = "1";

   @RBEntry("--")
   public static final String NOT_APPLICABLE_MSG = "2";

   @RBEntry("--")
   public static final String NO_ASSIGNMENTS_MSG = "3";

   @RBEntry("--")
   public static final String NO_VOTES_MSG = "4";

   @RBEntry("Actor")
   public static final String ACTOR_MSG = "6";

   @RBEntry("Principal")
   public static final String PRINCIPAL_MSG = "7";

   @RBEntry("Project")
   public static final String PROJECT_MSG = "8";

   @RBEntry("Variable")
   public static final String VARIABLE_MSG = "9";

   @RBEntry("Role")
   public static final String ROLE_MSG = "10";

   @RBEntry("Worklist")
   public static final String WORKLIST_MSG = "11";

   @RBEntry("yes")
   @RBComment("states whether the activity is overdue or not")
   public static final String ACTIVITY_IS_OVERDUE_MSG = "12";

   @RBEntry("no")
   @RBComment("states whether the activity is overdue or not")
   public static final String ACTIVITY_NOT_OVERDUE_MSG = "13";

   @RBEntry("yes")
   @RBComment("states whether the variable can be changed (modifiable)")
   public static final String VARIABLE_IS_MUTABLE_MSG = "14";

   @RBEntry("no")
   @RBComment("states whether the variable can be changed (modifiable)")
   public static final String VARIABLE_NOT_MUTABLE_MSG = "15";

   @RBEntry("yes")
   @RBComment("states whether the variable is required or not")
   public static final String VARIABLE_IS_REQUIRED_MSG = "16";

   @RBEntry("no")
   @RBComment("states whether the variable is required or not")
   public static final String VARIABLE_NOT_REQUIRED_MSG = "17";

   @RBEntry("yes")
   @RBComment("states whether the variable is reset to its default value each time the process or activity is started")
   public static final String VARIABLE_IS_RESETTABLE_MSG = "18";

   @RBEntry("no")
   @RBComment("states whether the variable is reset to its default value each time the process or activity is started")
   public static final String VARIABLE_NOT_RESETTABLE_MSG = "19";

   @RBEntry("yes")
   @RBComment("states whether the variable is visible to the user")
   public static final String VARIABLE_IS_VISIBLE_MSG = "20";

   @RBEntry("no")
   @RBComment("states whether the variable is visible to the user")
   public static final String VARIABLE_NOT_VISIBLE_MSG = "21";

   @RBEntry("yes")
   @RBComment("states whether the assignment has been completed")
   public static final String ASSIGNMENT_IS_COMPLETED_MSG = "22";

   @RBEntry("no")
   @RBComment("states whether the assignment has been completed")
   public static final String ASSIGNMENT_NOT_COMPLETED_MSG = "23";

   @RBEntry("yes")
   @RBComment("states whether the assignment has been completed")
   public static final String ASSIGNMENT_IS_REQUIRED_MSG = "24";

   @RBEntry("no")
   @RBComment("states whether the assignment has been completed")
   public static final String ASSIGNMENT_NOT_REQUIRED_MSG = "25";

   @RBEntry(", ")
   @RBPseudo(false)
   public static final String LIST_SEPARATOR = "26";

   @RBEntry("Process Manager <{0}>")
   public static final String TITLE_PAGE_MSG = "5";

   @RBEntry("Local Search")
   public static final String LOCAL_SEARCH_MSG = "27";

   @RBEntry("Report on Processes Behind Schedule")
   public static final String PROCESSES_BEHIND_SCHEDULE_REPORT = "28";

   @RBEntry("Report on Processes Initiated By This User")
   public static final String PROCESSES_INITIATED_REPORT = "29";

   @RBEntry("yes")
   @RBComment("states whether the activity has been escalated")
   public static final String ACTIVITY_IS_ESCALATED_MSG = "30";

   @RBEntry("no")
   @RBComment("states whether the activity has been escalated")
   public static final String ACTIVITY_NOT_ESCALATED_MSG = "31";

   @RBEntry("Graphical View")
   public static final String GRAPHICAL_LINK = "32";

   @RBEntry("Team")
   public static final String TEAM_MSG = "33";

   @RBEntry("Process:")
   public static final String PM_PROCESS_LABEL = "130";

   @RBEntry("Category")
   public static final String PM_CATEGORY_LABEL = "131";

   @RBEntry("Deadline")
   public static final String PM_DEADLINE_LABEL = "132";

   @RBEntry("Team")
   public static final String PM_TEAM_LABEL = "133";

   @RBEntry("State")
   public static final String PM_STATE_LABEL = "134";

   @RBEntry("Start Time")
   public static final String PM_START_TIME_LABEL = "135";

   @RBEntry("End Time")
   public static final String PM_END_TIME_LABEL = "136";

   @RBEntry("Priority")
   public static final String PM_PRIORITY_LABEL = "137";

   @RBEntry("Process Initiator")
   public static final String PM_PROCESS_INITIATOR_LABEL = "138";

   @RBEntry("Template Name")
   public static final String PM_TEMPLATE_NAME_LABEL = "139";

   @RBEntry("Primary Object")
   public static final String PM_PRIMARY_OBJECT_LABEL = "140";

   @RBEntry("Description")
   public static final String PM_DESCRIPTION_LABEL = "141";

   @RBEntry("Project:")
   public static final String PM_PROJECT_LABEL = "142";

   @RBEntry("Instructions")
   public static final String PM_INSTRUCTIONS_LABEL = "143";

   @RBEntry("Possible Votes")
   public static final String PM_POSSIBLE_VOTES_LABEL = "144";

   @RBEntry("Overdue")
   public static final String PM_OVERDUE_LABEL = "145";

   @RBEntry("Escalated")
   public static final String PM_ESCALATED_LABEL = "146";

   @RBEntry("Iteration")
   public static final String PM_ITERATION_LABEL = "147";

   @RBEntry("Responsible Role")
   public static final String PM_RESPONSIBLE_ROLE_LABEL = "148";

   @RBEntry("Errors")
   public static final String PM_ERRORS_LABEL = "149";

   @RBEntry("Process Content:")
   public static final String PM_CONTENT_LABEL = "150";

   @RBEntry("Lifecycle")
   public static final String PM_LIFECYCLE_LABEL = "151";

   @RBEntry("ID")
   public static final String PM_ID_HEADER = "161";

   @RBEntry("Activity Name")
   public static final String PM_ACTIVITY_NAME_HEADER = "162";

   @RBEntry("State")
   public static final String PM_STATE_HEADER = "163";

   @RBEntry("Deadline")
   public static final String PM_DEADLINE_HEADER = "164";

   @RBEntry("Start Time")
   public static final String PM_START_TIME_HEADER = "165";

   @RBEntry("End Time")
   public static final String PM_END_TIME_HEADER = "166";

   @RBEntry("Time Until Start")
   public static final String PM_TIME_UNTIL_START_HEADER = "167";

   @RBEntry("Priority")
   public static final String PM_PRIORITY_HEADER = "168";

   @RBEntry("Participant")
   public static final String PM_PARTICIPANT_HEADER = "169";

   @RBEntry("Participant Name")
   public static final String PM_PARTICIPANT_NAME_HEADER = "170";

   @RBEntry("Role")
   public static final String PM_ROLE_HEADER = "171";

   @RBEntry("Required")
   public static final String PM_REQUIRED_HEADER = "172";

   @RBEntry("Completion Status")
   public static final String PM_COMPLETION_STATUS_HEADER = "173";

   @RBEntry("Vote")
   public static final String PM_VOTE_HEADER = "174";

   @RBEntry("Variable Name")
   public static final String PM_VARIABLE_NAME_HEADER = "175";

   @RBEntry("Value")
   public static final String PM_VALUE_HEADER = "176";

   @RBEntry("Type")
   public static final String PM_TYPE_HEADER = "177";

   @RBEntry("Default Value")
   public static final String PM_DEFAULT_VALUE_HEADER = "178";

   @RBEntry("Initialized From")
   public static final String PM_INITIALIZED_FROM_HEADER = "179";

   @RBEntry("Copied Into")
   public static final String PM_COPIED_INTO_HEADER = "180";

   @RBEntry("Process Role")
   public static final String PM_PROCESS_ROLE_HEADER = "181";

   @RBEntry("Process Principals")
   public static final String PM_PROCESS_PRINCIPALS_HEADER = "182";

   @RBEntry("Completed")
   public static final String PM_COMPLETED_HEADER = "183";

   @RBEntry("Variable Display Name")
   public static final String PM_VARIABLE_DISPLAYNAME_HEADER = "184";

   /**
    * Localization: This value is originally localized in the body of
    * the ProcessManager.html HTML template
    **/
   @RBEntry("A frame-capable browser is necessary in order to view the Process Manager.")
   public static final String PM_FRAME_BROWSER_REQUIRED = "185";

   @RBEntry("Assignments")
   public static final String ASSIGNMENT_MSG = "186";

   @RBEntry("Process Details")
   public static final String PM_PROCESSDETAILS_TITLE = "200";

   @RBEntry("Process Participants")
   public static final String PM_PROCESSPARTICIPANTS_TITLE = "201";

   @RBEntry("Process Variables")
   public static final String PM_PROCESSVARIABLES_TITLE = "202";

   @RBEntry("Activity Details")
   public static final String PM_ACTIVITYDETAILS_TITLE = "203";

   @RBEntry("Activity Participants")
   public static final String PM_ACTIVITYPARTICIPANTS_TITLE = "204";

   @RBEntry("Activity Variables")
   public static final String PM_ACTIVITYVARIABLES_TITLE = "205";

   @RBEntry("Activity Properties")
   public static final String PM_ACTIVITYPROPERTY_TITLE = "206";

   @RBEntry("Completed On")
   public static final String PM_COMPLETED_ON_HEADER = "207";
   @RBEntry("Health")
   public static final String PM_PROCESS_HEALTH = "208";
   @RBEntry("Error: Invalid PBO reference")
   public static final String PROC_INVALID_PBO = "209";
   
   @RBEntry("Error: Invalid team reference")
   public static final String PROC_INVALID_TEAM_REF = "210";
   
   @RBEntry("Error: Invalid workflow template reference")
   public static final String PROC_INVALID_WORKFLOWTEMP_REF = "211";
   
    
   @RBEntry("Warning: Overdue process")
   public static final String PROC_WARNING_OVERDUE_PROCESS = "212";
   
   @RBEntry("Warning: Overdue task(s)")
   public static final String PROC_WARNING_OVERDUE_TASK = "213";
   
   @RBEntry("Warning: Process has large update count")
   public static final String PROC_LARGEUPDATECOUNT = "214";
   
   @RBEntry("Warning: Large number of associated synch robots")
   public static final String PROC_ASSOCIATED_SYNC_ROBOT = "215";
   
   @RBEntry("Error: Orphaned Workitems")
   public static final String PROC_WORKITEM_ORPHANED = "216";
   
   @RBEntry("Error: Stalled (Failed queue entry)")
   public static final String PROC_STALLED = "217";
   
   @RBEntry("Warning: Suspended")
   public static final String PROC_WARNING_SUSPENDED = "218";
   
   @RBEntry("Error: Terminated with running nodes")
   public static final String  PROC_INVALID_RUNNING_COMPONENT="219";
   
   @RBEntry("Warning:Terminated")
   public static final String PROC_TERMINATED  = "220";
   
   @RBEntry("Error: Aborted")
   public static final String  PROC_ABORTED = "221";
   
   @RBEntry("Running with no identified errors or warnings")
   public static final String PROC_RUNNING = "222";   
   
   @RBEntry("INVALID PROCESS")
   public static final String INVALID_PROCESS = "223";
   
   @RBEntry("Warning: Node(s) in process with large update count")
   public static final String PROC_LARGEUPDATECOUNT_NODE = "224";
   
   @RBEntry("Warning: Suspended node(s)")
   public static final String PROC_WARNING_SUSPENDED_NODE = "225";
   
   @RBEntry("Error: Aborted node(s)")
   public static final String PROC_ABORTED_NODE = "226";

   @RBEntry("Executed with no identified errors or warnings")
   public static final String PROC_CLOSED = "227";    

   @RBEntry("Name")
   public static final String FILE_NAME = "228";
   
   @RBEntry("Format")
   public static final String  FILE_FORMAT = "229";
   
   @RBEntry("File Size")
   public static final String FILE_SIZE  = "230";
   
   @RBEntry("Last Modified")
   public static final String LAST_MODIFIED  = "231";
   
   @RBEntry("Modified By")
   public static final String  MODIFIED_BY = "232";
   
   @RBEntry("Sub-Process/Block has not yet started.")
   public static final String  BLOCK_PROCESS_NOT_STARTED = "233";
   
   
   @RBEntry("URL")
   public static final String  URL_FORMAT = "234";
   
   
   @RBEntry("Expand All")
   public static final String EXPAND_ALL_ACTIVITY  = "235";
   
   @RBEntry("Collapse All")
   public static final String COLLAPSE_ALL_ACTIVITY  = "236";
   
   @RBEntry("Activity:")
   public static final String ACTIVITY_LABEL  = "237";
   
   @RBEntry("Activities")
   public static final String ACTIVITIES_LABEL  = "238";
   
   @RBEntry("Process:")
   public static final String PROCESS_LABEL  = "239";
   
   
   
   
}
