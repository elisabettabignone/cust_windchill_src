/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.workflow.work;

import wt.util.resource.*;

@RBUUID("wt.workflow.work.ProcessManagerResource")
public final class ProcessManagerResource_it extends WTListResourceBundle {
   /**
    * field messages -------------------------------------------------------
    **/
   @RBEntry("Non disponibile")
   public static final String NOT_AVAILABLE_MSG = "1";

   @RBEntry("--")
   public static final String NOT_APPLICABLE_MSG = "2";

   @RBEntry("--")
   public static final String NO_ASSIGNMENTS_MSG = "3";

   @RBEntry("--")
   public static final String NO_VOTES_MSG = "4";

   @RBEntry("Attore")
   public static final String ACTOR_MSG = "6";

   @RBEntry("Utente/gruppo/ruolo")
   public static final String PRINCIPAL_MSG = "7";

   @RBEntry("Progetto")
   public static final String PROJECT_MSG = "8";

   @RBEntry("Variabile")
   public static final String VARIABLE_MSG = "9";

   @RBEntry("Ruolo")
   public static final String ROLE_MSG = "10";

   @RBEntry("Elenco incarichi")
   public static final String WORKLIST_MSG = "11";

   @RBEntry("sì")
   @RBComment("states whether the activity is overdue or not")
   public static final String ACTIVITY_IS_OVERDUE_MSG = "12";

   @RBEntry("no")
   @RBComment("states whether the activity is overdue or not")
   public static final String ACTIVITY_NOT_OVERDUE_MSG = "13";

   @RBEntry("sì")
   @RBComment("states whether the variable can be changed (modifiable)")
   public static final String VARIABLE_IS_MUTABLE_MSG = "14";

   @RBEntry("no")
   @RBComment("states whether the variable can be changed (modifiable)")
   public static final String VARIABLE_NOT_MUTABLE_MSG = "15";

   @RBEntry("sì")
   @RBComment("states whether the variable is required or not")
   public static final String VARIABLE_IS_REQUIRED_MSG = "16";

   @RBEntry("no")
   @RBComment("states whether the variable is required or not")
   public static final String VARIABLE_NOT_REQUIRED_MSG = "17";

   @RBEntry("sì")
   @RBComment("states whether the variable is reset to its default value each time the process or activity is started")
   public static final String VARIABLE_IS_RESETTABLE_MSG = "18";

   @RBEntry("no")
   @RBComment("states whether the variable is reset to its default value each time the process or activity is started")
   public static final String VARIABLE_NOT_RESETTABLE_MSG = "19";

   @RBEntry("sì")
   @RBComment("states whether the variable is visible to the user")
   public static final String VARIABLE_IS_VISIBLE_MSG = "20";

   @RBEntry("no")
   @RBComment("states whether the variable is visible to the user")
   public static final String VARIABLE_NOT_VISIBLE_MSG = "21";

   @RBEntry("sì")
   @RBComment("states whether the assignment has been completed")
   public static final String ASSIGNMENT_IS_COMPLETED_MSG = "22";

   @RBEntry("no")
   @RBComment("states whether the assignment has been completed")
   public static final String ASSIGNMENT_NOT_COMPLETED_MSG = "23";

   @RBEntry("sì")
   @RBComment("states whether the assignment has been completed")
   public static final String ASSIGNMENT_IS_REQUIRED_MSG = "24";

   @RBEntry("no")
   @RBComment("states whether the assignment has been completed")
   public static final String ASSIGNMENT_NOT_REQUIRED_MSG = "25";

   @RBEntry(", ")
   @RBPseudo(false)
   public static final String LIST_SEPARATOR = "26";

   @RBEntry("Gestione processi <{0}>")
   public static final String TITLE_PAGE_MSG = "5";

   @RBEntry("Ricerca locale")
   public static final String LOCAL_SEARCH_MSG = "27";

   @RBEntry("Report dei processi in ritardo")
   public static final String PROCESSES_BEHIND_SCHEDULE_REPORT = "28";

   @RBEntry("Report dei processi avviati dall'utente")
   public static final String PROCESSES_INITIATED_REPORT = "29";

   @RBEntry("sì")
   @RBComment("states whether the activity has been escalated")
   public static final String ACTIVITY_IS_ESCALATED_MSG = "30";

   @RBEntry("no")
   @RBComment("states whether the activity has been escalated")
   public static final String ACTIVITY_NOT_ESCALATED_MSG = "31";

   @RBEntry("Visualizzazione grafica")
   public static final String GRAPHICAL_LINK = "32";

   @RBEntry("Team")
   public static final String TEAM_MSG = "33";

   @RBEntry("Processo:")
   public static final String PM_PROCESS_LABEL = "130";

   @RBEntry("Categoria")
   public static final String PM_CATEGORY_LABEL = "131";

   @RBEntry("Scadenza")
   public static final String PM_DEADLINE_LABEL = "132";

   @RBEntry("Team")
   public static final String PM_TEAM_LABEL = "133";

   @RBEntry("Stato del ciclo di vita")
   public static final String PM_STATE_LABEL = "134";

   @RBEntry("Ora d'inizio")
   public static final String PM_START_TIME_LABEL = "135";

   @RBEntry("Ora di fine")
   public static final String PM_END_TIME_LABEL = "136";

   @RBEntry("Priorità")
   public static final String PM_PRIORITY_LABEL = "137";

   @RBEntry("Iniziatore processo")
   public static final String PM_PROCESS_INITIATOR_LABEL = "138";

   @RBEntry("Nome modello")
   public static final String PM_TEMPLATE_NAME_LABEL = "139";

   @RBEntry("Oggetto principale")
   public static final String PM_PRIMARY_OBJECT_LABEL = "140";

   @RBEntry("Descrizione")
   public static final String PM_DESCRIPTION_LABEL = "141";

   @RBEntry("Progetto:")
   public static final String PM_PROJECT_LABEL = "142";

   @RBEntry("Istruzioni")
   public static final String PM_INSTRUCTIONS_LABEL = "143";

   @RBEntry("Voti possibili")
   public static final String PM_POSSIBLE_VOTES_LABEL = "144";

   @RBEntry("In ritardo")
   public static final String PM_OVERDUE_LABEL = "145";

   @RBEntry("Notificato")
   public static final String PM_ESCALATED_LABEL = "146";

   @RBEntry("Iterazione")
   public static final String PM_ITERATION_LABEL = "147";

   @RBEntry("Ruolo responsabile")
   public static final String PM_RESPONSIBLE_ROLE_LABEL = "148";

   @RBEntry("Errori")
   public static final String PM_ERRORS_LABEL = "149";

   @RBEntry("Contenuto processo:")
   public static final String PM_CONTENT_LABEL = "150";

   @RBEntry("Ciclo di vita")
   public static final String PM_LIFECYCLE_LABEL = "151";

   @RBEntry("ID")
   public static final String PM_ID_HEADER = "161";

   @RBEntry("Nome attività")
   public static final String PM_ACTIVITY_NAME_HEADER = "162";

   @RBEntry("Stato del ciclo di vita")
   public static final String PM_STATE_HEADER = "163";

   @RBEntry("Scadenza")
   public static final String PM_DEADLINE_HEADER = "164";

   @RBEntry("Ora d'inizio")
   public static final String PM_START_TIME_HEADER = "165";

   @RBEntry("Ora di fine")
   public static final String PM_END_TIME_HEADER = "166";

   @RBEntry("Tempo prima dell'inizio")
   public static final String PM_TIME_UNTIL_START_HEADER = "167";

   @RBEntry("Priorità")
   public static final String PM_PRIORITY_HEADER = "168";

   @RBEntry("Partecipante")
   public static final String PM_PARTICIPANT_HEADER = "169";

   @RBEntry("Nome partecipante")
   public static final String PM_PARTICIPANT_NAME_HEADER = "170";

   @RBEntry("Ruolo")
   public static final String PM_ROLE_HEADER = "171";

   @RBEntry("Obbligatorio")
   public static final String PM_REQUIRED_HEADER = "172";

   @RBEntry("Stato di completamento")
   public static final String PM_COMPLETION_STATUS_HEADER = "173";

   @RBEntry("Voto")
   public static final String PM_VOTE_HEADER = "174";

   @RBEntry("Nome variabile")
   public static final String PM_VARIABLE_NAME_HEADER = "175";

   @RBEntry("Valore")
   public static final String PM_VALUE_HEADER = "176";

   @RBEntry("Tipo")
   public static final String PM_TYPE_HEADER = "177";

   @RBEntry("Valore di default")
   public static final String PM_DEFAULT_VALUE_HEADER = "178";

   @RBEntry("Inizializzato da")
   public static final String PM_INITIALIZED_FROM_HEADER = "179";

   @RBEntry("Copiato in")
   public static final String PM_COPIED_INTO_HEADER = "180";

   @RBEntry("Ruolo processo")
   public static final String PM_PROCESS_ROLE_HEADER = "181";

   @RBEntry("Utenti/gruppi/ruoli del processo")
   public static final String PM_PROCESS_PRINCIPALS_HEADER = "182";

   @RBEntry("Completato")
   public static final String PM_COMPLETED_HEADER = "183";

   @RBEntry("Nome visualizzato variabile")
   public static final String PM_VARIABLE_DISPLAYNAME_HEADER = "184";

   /**
    * Localization: This value is originally localized in the body of
    * the ProcessManager.html HTML template
    **/
   @RBEntry("È necessario che il browser utilizzato offra il supporto per i frame per visualizzare la Gestione processi.")
   public static final String PM_FRAME_BROWSER_REQUIRED = "185";

   @RBEntry("Assegnazioni")
   public static final String ASSIGNMENT_MSG = "186";

   @RBEntry("Dettagli processo")
   public static final String PM_PROCESSDETAILS_TITLE = "200";

   @RBEntry("Partecipanti processo")
   public static final String PM_PROCESSPARTICIPANTS_TITLE = "201";

   @RBEntry("Variabili processo")
   public static final String PM_PROCESSVARIABLES_TITLE = "202";

   @RBEntry("Dettagli attività")
   public static final String PM_ACTIVITYDETAILS_TITLE = "203";

   @RBEntry("Partecipanti attività")
   public static final String PM_ACTIVITYPARTICIPANTS_TITLE = "204";

   @RBEntry("Variabili attività")
   public static final String PM_ACTIVITYVARIABLES_TITLE = "205";

   @RBEntry("Proprietà attività")
   public static final String PM_ACTIVITYPROPERTY_TITLE = "206";

   @RBEntry("Data completamento")
   public static final String PM_COMPLETED_ON_HEADER = "207";
   @RBEntry("Stato")
   public static final String PM_PROCESS_HEALTH = "208";
   @RBEntry("Errore: riferimento business object principale non valido")
   public static final String PROC_INVALID_PBO = "209";
   
   @RBEntry("Errore: riferimento team non valido")
   public static final String PROC_INVALID_TEAM_REF = "210";
   
   @RBEntry("Errore: riferimento modello di workflow non valido")
   public static final String PROC_INVALID_WORKFLOWTEMP_REF = "211";
   
    
   @RBEntry("Avvertenza: processo in ritardo")
   public static final String PROC_WARNING_OVERDUE_PROCESS = "212";
   
   @RBEntry("Avvertenza: task in ritardo")
   public static final String PROC_WARNING_OVERDUE_TASK = "213";
   
   @RBEntry("Avvertenza: numero elevato di aggiornamenti processo")
   public static final String PROC_LARGEUPDATECOUNT = "214";
   
   @RBEntry("Avvertenza: numero elevato di robot sinc associati")
   public static final String PROC_ASSOCIATED_SYNC_ROBOT = "215";
   
   @RBEntry("Errore: task isolati")
   public static final String PROC_WORKITEM_ORPHANED = "216";
   
   @RBEntry("Errore: processo bloccato (errore elemento coda)")
   public static final String PROC_STALLED = "217";
   
   @RBEntry("Avvertenza: processo sospeso")
   public static final String PROC_WARNING_SUSPENDED = "218";
   
   @RBEntry("Errore: processo terminato con nodi in esecuzione")
   public static final String  PROC_INVALID_RUNNING_COMPONENT="219";
   
   @RBEntry("Avvertenza: processo terminato")
   public static final String PROC_TERMINATED  = "220";
   
   @RBEntry("Errore: processo interrotto")
   public static final String  PROC_ABORTED = "221";
   
   @RBEntry("Processo in esecuzione senza avvertenze o errori identificati")
   public static final String PROC_RUNNING = "222";   
   
   @RBEntry("PROCESSO NON VALIDO")
   public static final String INVALID_PROCESS = "223";
   
   @RBEntry("Avvertenza: nodi del processo con numero elevato di aggiornamenti")
   public static final String PROC_LARGEUPDATECOUNT_NODE = "224";
   
   @RBEntry("Avvertenza: nodi sospesi")
   public static final String PROC_WARNING_SUSPENDED_NODE = "225";
   
   @RBEntry("Errore: nodi interrotti")
   public static final String PROC_ABORTED_NODE = "226";

   @RBEntry("Eseguito senza avvertenze o errori identificati")
   public static final String PROC_CLOSED = "227";    

   @RBEntry("Nome")
   public static final String FILE_NAME = "228";
   
   @RBEntry("Formato")
   public static final String  FILE_FORMAT = "229";
   
   @RBEntry("Dimensione file")
   public static final String FILE_SIZE  = "230";
   
   @RBEntry("Data ultima modifica")
   public static final String LAST_MODIFIED  = "231";
   
   @RBEntry("Autore modifiche")
   public static final String  MODIFIED_BY = "232";
   
   @RBEntry("Sottoprocesso/blocco non ancora avviato.")
   public static final String  BLOCK_PROCESS_NOT_STARTED = "233";
   
   
   @RBEntry("URL")
   public static final String  URL_FORMAT = "234";
   
   
   @RBEntry("Espandi tutto")
   public static final String EXPAND_ALL_ACTIVITY  = "235";
   
   @RBEntry("Comprimi tutto")
   public static final String COLLAPSE_ALL_ACTIVITY  = "236";
   
   @RBEntry("Attività:")
   public static final String ACTIVITY_LABEL  = "237";
   
   @RBEntry("Attività")
   public static final String ACTIVITIES_LABEL  = "238";
   
   @RBEntry("Processo:")
   public static final String PROCESS_LABEL  = "239";
   
   
   
   
}
