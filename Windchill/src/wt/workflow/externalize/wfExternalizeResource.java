/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.workflow.externalize;

import wt.util.resource.*;

@RBUUID("wt.workflow.externalize.wfExternalizeResource")
public final class wfExternalizeResource extends WTListResourceBundle {
   @RBEntry(" Externalization initiated at ")
   public static final String EXTERINALIZATION_INITIATE = "0";

   @RBEntry(" Externalization completed successfully at ")
   public static final String EXTERINALIZATION_COMPLETED_SUCCESSFULLY = "1";

   @RBEntry(" File creation failed. ")
   public static final String EXTERINALIZATION_FILE_CREATION_FAILED = "2";

   @RBEntry(" BlobWrite Status : ")
   public static final String BLOBWRITE_STATUS = "3";

   @RBEntry(" Compilation Status : ")
   public static final String COMPILATION_STATUS = "4";

   @RBEntry(" Successful ")
   public static final String STATUS_SUCCESSFUL = "5";

   @RBEntry(" Skipped ")
   public static final String STATUS_SKIPPED = "6";

   @RBEntry(" Failure ")
   public static final String STATUS_FAILED = "7";

   @RBEntry(" Externalization completed with failure at ")
   public static final String EXTERINALIZATION_COMPLETED_WITH_FAILURE = "8";

   @RBEntry(" Unable to make a backup of one or more externalized java files. If the externalized files are deleted, it will not be possible to recover the expressions.")
   public static final String BLOBWRITE_STATUS_FAILURE_REASON = "9";

   @RBEntry(" Workflow template : ")
   public static final String WORKFLOW_TEMPLATE = "10";

   @RBEntry(" Iteration ")
   public static final String WORKFLOW_ITERATION  = "11";

   @RBEntry(" ================== ")
   public static final String LEADING_TRAIL  = "12";

   @RBEntry(" Organization - ")
   public static final String ORGANIZATION = "13";

   @RBEntry(" Generated java source file : ")
   public static final String GENERATED_JAVA_FILE_SOURCE = "14";

   @RBEntry(" Block template : ")
   public static final String BLOCK_TEMPLATE = "15";

   @RBEntry(" The exceptioon message is ")
   public static final String EXCEPTIOON_MESSAGE = "16";

   @RBEntry(" File Creation Status : ")
   public static final String FILE_CREATION_STATUS = "17";

   @RBEntry(" Context - ")
   public static final String CONTEXT = "18";
}