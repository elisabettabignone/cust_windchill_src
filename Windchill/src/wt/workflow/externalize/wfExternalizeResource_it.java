/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.workflow.externalize;

import wt.util.resource.*;

@RBUUID("wt.workflow.externalize.wfExternalizeResource")
public final class wfExternalizeResource_it extends WTListResourceBundle {
   @RBEntry(" Inizio esternalizzazione: ")
   public static final String EXTERINALIZATION_INITIATE = "0";

   @RBEntry(" Fine esternalizzazione: ")
   public static final String EXTERINALIZATION_COMPLETED_SUCCESSFULLY = "1";

   @RBEntry(" Creazione file non riuscita. ")
   public static final String EXTERINALIZATION_FILE_CREATION_FAILED = "2";

   @RBEntry(" Stato scrittura blob: ")
   public static final String BLOBWRITE_STATUS = "3";

   @RBEntry(" Stato compilazione: ")
   public static final String COMPILATION_STATUS = "4";

   @RBEntry(" Completato ")
   public static final String STATUS_SUCCESSFUL = "5";

   @RBEntry(" Ignorato ")
   public static final String STATUS_SKIPPED = "6";

   @RBEntry(" Errore ")
   public static final String STATUS_FAILED = "7";

   @RBEntry(" Esternalizzazione completata con errore: ")
   public static final String EXTERINALIZATION_COMPLETED_WITH_FAILURE = "8";

   @RBEntry(" Impossibile eseguire il backup di uno o più file Java esternalizzati. Se i file esternalizzati vengono eliminati, non sarà possibile recuperare le espressioni.")
   public static final String BLOBWRITE_STATUS_FAILURE_REASON = "9";

   @RBEntry(" Modello workflow: ")
   public static final String WORKFLOW_TEMPLATE = "10";

   @RBEntry(" Iterazione")
   public static final String WORKFLOW_ITERATION  = "11";

   @RBEntry(" ================== ")
   public static final String LEADING_TRAIL  = "12";

   @RBEntry(" Organizzazione -")
   public static final String ORGANIZATION = "13";

   @RBEntry(" File di origine Java generato:")
   public static final String GENERATED_JAVA_FILE_SOURCE = "14";

   @RBEntry(" Modello blocco:")
   public static final String BLOCK_TEMPLATE = "15";

   @RBEntry("Messaggio di eccezione:")
   public static final String EXCEPTIOON_MESSAGE = "16";

   @RBEntry("Stato creazione file:")
   public static final String FILE_CREATION_STATUS = "17";

   @RBEntry("Contesto - ")
   public static final String CONTEXT = "18";
}
