/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.workflow.notebook;

import wt.util.resource.*;

@RBUUID("wt.workflow.notebook.notebookResource")
public final class notebookResource extends WTListResourceBundle {
   @RBEntry("Notebook")
   public static final String NOTEBOOK_URL_LABEL = "0";

   @RBEntry("My Notebook")
   public static final String NOTEBOOKNAME = "1";

   /**
    * The translation for the following string
    * must match the string in the notebookTemplate.xml file
    **/
   @RBEntry("My Current Hotlinks")
   public static final String NOTEBOOK_CURRENT_HOTLINKS = "2";
}
