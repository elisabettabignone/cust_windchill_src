/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.generic;

import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.generic.genericResource")
public final class genericResource_it extends WTListResourceBundle {
   @RBEntry("Il riferimento non può essere vuoto.")
   @RBComment("An error message stating that the reference on an ExternalLogicLink cannot be empty.")
   public static final String REFERENCE_CANNOT_BE_EMPTY = "100";

   @RBEntry("Il padre deve essere una parte configurabile.")
   @RBComment("An error message stating that the parent (referencedby) on an ExternalLogicLink must be a configurable part.")
   public static final String PARENT_MUST_BE_ADVANCED_CONFIGRABLE = "110";

   @RBEntry("Il figlio deve essere una parte configurabile o un insieme di opzioni.")
   @RBComment("An error message stating that the child (references) on an ExternalLogicLink must be a configurable part or option set.")
   public static final String CHILD_MUST_BE_ADVANCED_CONFIGRABLE = "120";

   @RBEntry("Il figlio deve essere univoco nel padre.")
   @RBComment("An error message stating that the child (references) on an ExternalLogicLink must be unique under the parent (referencedby).")
   public static final String CHILD_IS_ALREADY_LINKED = "130";

   @RBEntry("Rilevata dipendenza circolare.")
   @RBComment("An error message stating that a circular dependency has been detected.")
   public static final String CIRCULAR_DEPENDENCY = "140";

   @RBEntry("Impossibile creare un riferimento logico tra una parte configurabile e se stessa.")
   @RBComment("An error message stating that a logic reference cannot be created between a configurable part and itself.")
   public static final String CANT_CREATE_LINK_TO_ITSELF = "150";

   @RBEntry("Impossibile creare una parte comprimibile non configurabile.")
   @RBComment("An error message stating that a part cannot be collapsible unless it is also configurable.")
   public static final String CANT_CREATE_NON_GENERIC_COLLAPSIBLE_PART = "160";
}
