/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.generic.rule;

import wt.util.resource.*;

@RBUUID("wt.generic.rule.ruleResource")
public final class ruleResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile assegnare un nome alla variante. Il prefisso del nome non è definito.")
   public static final String VARIANTNAMEPREFIX_NOTFOUND = "0";
}
