/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.fc.delete;

import wt.util.resource.*;

@RBUUID("wt.fc.delete.deleteResource")
public final class deleteResource_it extends WTListResourceBundle {
   @RBEntry("L'oggetto \"{0}\" deve essere contrassegnato per l'eliminazione perché diventi irripristinabile.")
   @RBArgComment0("The display identity of the object that the user tried to make unrestorable")
   public static final String MUST_BE_M4D_TO_MAKE_UNRESTORABLE = "0";

   @RBEntry("Impossibile ripristinare l'oggetto \"{0}\".")
   @RBArgComment0("The display identity of the object that cannot be restored")
   public static final String OBJECT_UNRESTORABLE = "1";

   @RBEntry("L'oggetto \"{0}\" deve essere contrassegnato per l'eliminazione perché diventi ripristinabile.")
   @RBArgComment0("The display identity of the object that the user tried to make restorable")
   public static final String MUST_BE_M4D_TO_MAKE_RESTORABLE = "2";
}
