/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.fc.batch;

import wt.util.resource.*;

@RBUUID("wt.fc.batch.batchResource")
public final class batchResource_it extends WTListResourceBundle {
   @RBEntry("La classe \"{0}\" per l'attributo \"{1}\" non è assegnabile dalla classe di destinazione \"{2}\".")
   @RBArgComment0(" {0} is the attribute class name")
   @RBArgComment1(" {1} is the attribute name")
   @RBArgComment2(" {2} is the target class name:")
   public static final String ATTR_CLASS_IN_WHERE_NOT_ASSIGNABLE_FROM_TARGET_CLASS = "0";

   @RBEntry("La classe \"{0}\" per l'attributo \"{1}\" non è assegnabile dalla classe di destinazione \"{2}\".")
   @RBArgComment0(" {0} is the attribute class name")
   @RBArgComment1(" {1} is the attribute name")
   @RBArgComment2(" {2} is the target class name:")
   public static final String ATTR_CLASS_IN_UPDATE_COLUMNS_NOT_ASSIGNABLE_FROM_TARGET_CLASS = "1";

   @RBEntry("L'attributo \"{0}\" non è consentito nelle colonne di aggiornamento.")
   @RBArgComment0(" {0} is the attribute name")
   public static final String ATTR_NOT_ALLOWED_IN_UPDATE_COLUMNS = "2";
}
