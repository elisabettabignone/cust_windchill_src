/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.fc.batch;

import wt.util.resource.*;

@RBUUID("wt.fc.batch.batchResource")
public final class batchResource extends WTListResourceBundle {
   @RBEntry("The class \"{0}\" for attribute \"{1}\" is not assignable from target class \"{2}\".")
   @RBArgComment0(" {0} is the attribute class name")
   @RBArgComment1(" {1} is the attribute name")
   @RBArgComment2(" {2} is the target class name:")
   public static final String ATTR_CLASS_IN_WHERE_NOT_ASSIGNABLE_FROM_TARGET_CLASS = "0";

   @RBEntry("The class \"{0}\" for attribute \"{1}\" is not assignable from target class \"{2}\".")
   @RBArgComment0(" {0} is the attribute class name")
   @RBArgComment1(" {1} is the attribute name")
   @RBArgComment2(" {2} is the target class name:")
   public static final String ATTR_CLASS_IN_UPDATE_COLUMNS_NOT_ASSIGNABLE_FROM_TARGET_CLASS = "1";

   @RBEntry("Attribute \"{0}\" is not allowed in update columns.")
   @RBArgComment0(" {0} is the attribute name")
   public static final String ATTR_NOT_ALLOWED_IN_UPDATE_COLUMNS = "2";
}
