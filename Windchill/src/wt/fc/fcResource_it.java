/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.fc;

import wt.util.resource.*;

@RBUUID("wt.fc.fcResource")
public final class fcResource_it extends WTListResourceBundle {
   @RBEntry("L'operazione \"{0}\" non è riuscita.")
   @RBArgComment0(" {0} name of operation")
   public static final String OPERATION_FAILURE = "0";

   @RBEntry("Impossibile creare l'oggetto {0}.")
   @RBComment("Object type.")
   public static final String UNABLE_TO_INSTANTIATE = "2";

   @RBEntry("L'oggetto \"{0}\" è già persistente.")
   public static final String ALREADY_PERSISTENT = "5";

   @RBEntry("L'oggetto \"{0}\" non è persistente.")
   public static final String NOT_PERSISTENT = "7";

   @RBEntry("Uno o entrambi gli oggetti correlati non sono persistenti")
   public static final String RELATED_NOT_PERSISTENT = "9";

   @RBEntry("L'oggetto \"{0}\" non è valido per la navigazione perché è federato o non persistente.")
   public static final String INVALID_NAVIGATE_SOURCE = "10";

   @RBEntry("Si è verificato un problema durante la creazione dell'oggetto \"{0}\".")
   public static final String CANNOT_CREATE = "11";

   @RBEntry("Uno o entrambi gli oggetti specificati non sono persistenti")
   public static final String SPECIFIED_NOT_PERSISTENT = "16";

   @RBEntry("Il ruolo \"{0}\" non è valido per la classe link \"{1}\".")
   public static final String INVALID_ROLE = "17";

   @RBEntry("URL errato. \"{0}\". Il formato deve essere: \"nomeclasse:valoreID\"")
   public static final String MALFORMED_URL = "18";

   @RBEntry("L'oggetto \"{0}\" non è membro della classe link \"{1}\".")
   public static final String NOT_LINK_CLASS_MEMBER = "19";

   @RBEntry("Il valore supera il limite superiore di \"{1}\" per \"{0}\".")
   @RBArgComment0("  {0} name of attribute")
   @RBArgComment1("  {1} number, which is the limit")
   public static final String UPPER_LIMIT = "20";

   @RBEntry("Il valore è inferiore al limite minimo \"{1}\" di \"{0}\"")
   @RBArgComment0("  {0} name of attribute")
   @RBArgComment1("  {1} number, which is the limit")
   public static final String LOWER_LIMIT = "21";

   @RBEntry("Il valore di \"{0}\" non può essere nullo poiché è un attributo \"obbligatorio\".")
   @RBArgComment0("  {0} name of attribute")
   public static final String REQUIRED_ATTRIBUTE = "22";

   @RBEntry("Impossibile trovare l'identificativo \"{0}\".")
   public static final String IDENTITY_NOT_FOUND = "23";

   @RBEntry("L'identificativo \"{0}\" è persistente.")
   public static final String PERSISTENT_NAME_CHANGE = "24";

   @RBEntry("Non esiste alcun tipo enumerativo con \"{0}\" come valore interno.")
   @RBArgComment0("  {0} internal (persistent) name (key)")
   public static final String INVALID_ENUM_VALUE = "25";

   @RBEntry("Impossibile trovare la classe chiave semantica \"{0}\".")
   public static final String CLASS_NOT_FOUND = "26";

   @RBEntry("Impossibile trovare il costruttore \"{0}\".")
   public static final String CONSTRUCTOR_NOT_FOUND = "27";

   @RBEntry("Il costruttore \"{0}\" non è pubblico.")
   public static final String CONSTRUCTOR_NOT_PUBLIC = "28";

   @RBEntry("Impossibile creare l'oggetto \"{0}.{1}\". L'identificativo non è univoco.")
   @RBArgComment0("The object's type (such as a part, document, or folder).  This is the first part of an object's identity which consists of its type and an identifier.")
   @RBArgComment1("The object's identifier (such as a part number, document name, or folder name).  This is the second part of an object's identiy which consists of its type and an identifier.")
   public static final String DUPLICATE_IDENTITY = "29";

   @RBEntry("Impossibile modificare l'identificativo dell'oggetto \"{0}.{1}\" in \"{2}\". Il nuovo identificativo non è univoco.")
   public static final String DUPLICATE_NEW_IDENTITY = "30";

   @RBEntry("Impossibile {0} \"{1}.{2}\". L'identificativo non è univoco.")
   public static final String UNIQUENESS_EXCEPTION = "31";

   @RBEntry("Impossibile {0} \"{1}.{2}\" perché non esiste più.")
   public static final String DELETED_OBJECT_EXCEPTION = "32";

   @RBEntry("Impossibile generare il selettore icona. La classe e l'oggetto IconDelegate non sono stati impostati.")
   public static final String CANNOT_GET_ICON = "33";

   @RBEntry("Accessibile solo a")
   @RBComment(" name of class to follow")
   public static final String FRIEND_ACCESS = "34";

   @RBEntry("Il valore assegnato a \"{0}\" deve essere di tipo  \"{1}\".")
   @RBArgComment0("  {0} name of attribute")
   @RBArgComment1("  {1} name of correct class (type)")
   public static final String WRONG_TYPE = "35";

   @RBEntry("Dopo che l'oggetto è stato reso persistente, le modifiche a \"{0}\" vengono limitate.")
   @RBArgComment0("  {0} name of attribute")
   public static final String CHANGE_RESTRICTION = "36";

   @RBEntry("l criteri di chiave esterna {0} consentono di trovare {1} oggetti, ma solo 1 è applicabile")
   public static final String MANY_OBJECTS_FOUND = "37";

   @RBEntry("Impossibile trovare il nome della classe {0}.")
   public static final String RELFECTED_CLASS_NOT_FOUND = "38";

   @RBEntry("Errore durante l'inizializzazione di \"{0}\".")
   public static final String ERROR_INITIALIZING = "39";

   @RBEntry("Nessun servizio di applicazione registrato per: \"{0}\".")
   @RBArgComment0("  {0} fully qualified class name")
   public static final String UNREGISTERED_SERVICE = "40";

   @RBEntry("Impossibile creare \"{0}\". L'identificativo non è univoco.")
   public static final String CREATE_UNIQUENESS_EXCEPTION = "41";

   @RBEntry("Impossibile aggiornare \"{0}\". L'identificativo non è univoco.")
   public static final String UPDATE_UNIQUENESS_EXCEPTION = "42";

   @RBEntry("Un IteratedObjectVector può preparare gli oggetti iterati all'esternalizzazione.")
   @RBComment("This message indicates that an object that does not implement the Iterated interface was unexpectedly found in an instance of IteratedObjectVector when the prepareForExternalization() method was called.")
   public static final String ITERATED_OBJECT_VECTOR_CLASS_CAST = "43";

   @RBEntry("La pubblicazione evento per singoli oggetti non è supportata per questo evento di tipo = {0}.")
   @RBArgComment0("The event type that is not supported.")
   public static final String SINGLE_OBJECT_EVENT_DISPATCH_NOT_SUPPORTED = "44";

   @RBEntry("Si è verificata una violazione di integrità dei riferimenti per questa operazione:")
   public static final String REFERENIAL_INTEGRITY_VOILATION = "45";

   @RBEntry("Si sono verificate violazioni di integrità dei riferimenti per questa operazione:")
   public static final String REFERENIAL_INTEGRITY_VOILATIONS = "46";

   @RBEntry("L'oggetto \"{0}\" fa riferimento all'oggetto \"{1}\" che è stato rimosso dal sistema.")
   @RBArgComment0("The object that contains the reference for the referential integrity violation.")
   @RBArgComment1("The target object of the referential integrity violation.")
   public static final String REFERENIAL_INTEGRITY_LINK_VOILATION = "47";

   @RBEntry("Il ruolo \"{0}\" per la classe link \"{1}\" specifica il tipo di riferimento \"{2}\" che non è supportato per la navigazione basata su raccolta.")
   @RBArgComment0("Name of a link role")
   @RBArgComment1("Link class")
   @RBArgComment2("Link reference type ")
   public static final String INVALID_ROLE_REFERENCE_TYPE = "48";

   @RBEntry("Dati di allocazione oggetti per una singola classe oggetto")
   public static final String PER_CLASS_DATA_ROW_TYPE_DESCR = "49";

   @RBEntry("Nome classe oggetto")
   public static final String OBJECT_CLASS_ITEM_DESCR = "50";

   @RBEntry("Totale oggetti creati")
   public static final String INITIALIZED_ITEM_DESCR = "51";

   @RBEntry("Numero di oggetto creati che non sono stati finalizzati")
   public static final String FINALIZED_ITEM_DESCR = "52";

   @RBEntry("Numero di riferimenti oggetti attivi")
   public static final String LIVE_REFS_ITEM_DESCR = "53";

   @RBEntry("Dati di allocazione oggetti raggruppati in base alla classe oggetto")
   public static final String PER_CLASS_DATA_TYPE_DESCR = "54";

   @RBEntry("Dati di allocazione oggetti raggruppati in base alla classe oggetto")
   public static final String PER_CLASS_DATA_ITEM_DESCR = "55";

   @RBEntry("Dati di allocazione oggetti")
   public static final String OVERALL_DATA_TYPE_DESCR = "56";

   @RBEntry("L'oggetto \"{0}\" non può essere eliminato perché è referenziato dall'oggetto \"{1}\".")
   @RBArgComment0("The target object of the referential integrity violation.")
   @RBArgComment1("The object that references the target object of the referential integrity violation.")
   public static final String REFERENIAL_INTEGRITY_OWNER_VOILATION = "70";

   @RBEntry("Durante l'elaborazione dei risultati è stato superato un limite.")
   public static final String RESULT_PROCESSOR_LIMIT = "80";
   
   @RBEntry("Lingua di creazione \"{0}\" non valida")
   @RBArgComment0("Value of authoring language attribute")
   @RBComment("Exception message when value of authoring language attribute is invalid")
   public static final String INVALID_LANGAUAGE = "81";
}
