/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.fc;

import wt.util.resource.*;

@RBUUID("wt.fc.fcResource")
public final class fcResource extends WTListResourceBundle {
   @RBEntry("The operation:   \"{0}\" failed.")
   @RBArgComment0(" {0} name of operation")
   public static final String OPERATION_FAILURE = "0";

   @RBEntry("Unable to create object {0}.")
   @RBComment("Object type.")
   public static final String UNABLE_TO_INSTANTIATE = "2";

   @RBEntry("Object \"{0}\" is already persistent.")
   public static final String ALREADY_PERSISTENT = "5";

   @RBEntry("Object \"{0}\" is not persistent.")
   public static final String NOT_PERSISTENT = "7";

   @RBEntry("One or both of the related objects are not persistent")
   public static final String RELATED_NOT_PERSISTENT = "9";

   @RBEntry("Object \"{0}\" is not valid navigate source because either it is federated or not persistence")
   public static final String INVALID_NAVIGATE_SOURCE = "10";

   @RBEntry("Problem occurred trying to create \"{0}\" object.")
   public static final String CANNOT_CREATE = "11";

   @RBEntry("One or both of the specified objects are not persistent")
   public static final String SPECIFIED_NOT_PERSISTENT = "16";

   @RBEntry("Role \"{0}\" is not a valid role for the \"{1}\" link class.")
   public static final String INVALID_ROLE = "17";

   @RBEntry("Malformed URL: \"{0}\". Format must be: \"classname:idValue\" ")
   public static final String MALFORMED_URL = "18";

   @RBEntry("\"{0}\" object is not a member of the given \"{1}\" Link class.")
   public static final String NOT_LINK_CLASS_MEMBER = "19";

   @RBEntry("The value exceeds the upper limit of \"{1}\" for \"{0}\".")
   @RBArgComment0("  {0} name of attribute")
   @RBArgComment1("  {1} number, which is the limit")
   public static final String UPPER_LIMIT = "20";

   @RBEntry("The value is less than \"{0}\"s lower limit of \"{1}\".")
   @RBArgComment0("  {0} name of attribute")
   @RBArgComment1("  {1} number, which is the limit")
   public static final String LOWER_LIMIT = "21";

   @RBEntry("The value of \"{0}\" cannot be set to null, since it is a \"required\" attribute.")
   @RBArgComment0("  {0} name of attribute")
   public static final String REQUIRED_ATTRIBUTE = "22";

   @RBEntry("The identity \"{0}\" was not found.")
   public static final String IDENTITY_NOT_FOUND = "23";

   @RBEntry("Can't change identity \"{0}\" is persistent.")
   public static final String PERSISTENT_NAME_CHANGE = "24";

   @RBEntry("No EnumeratedType exists with \"{0}\" as its internal value.")
   @RBArgComment0("  {0} internal (persistent) name (key)")
   public static final String INVALID_ENUM_VALUE = "25";

   @RBEntry("Semantic key class \"{0}\" not found.")
   public static final String CLASS_NOT_FOUND = "26";

   @RBEntry("Constructor \"{0}\" not found.")
   public static final String CONSTRUCTOR_NOT_FOUND = "27";

   @RBEntry("Constructor \"{0}\" not public.")
   public static final String CONSTRUCTOR_NOT_PUBLIC = "28";

   @RBEntry("Cannot create object \"{0}.{1}\" because identity is not unique.")
   @RBArgComment0("The object's type (such as a part, document, or folder).  This is the first part of an object's identity which consists of its type and an identifier.")
   @RBArgComment1("The object's identifier (such as a part number, document name, or folder name).  This is the second part of an object's identiy which consists of its type and an identifier.")
   public static final String DUPLICATE_IDENTITY = "29";

   @RBEntry("Cannot change identity of object \"{0}.{1}\" to \"{2}\" because new identity is not unique.")
   public static final String DUPLICATE_NEW_IDENTITY = "30";

   @RBEntry("Cannot {0} \"{1}.{2}\" because its identity is not unique.")
   public static final String UNIQUENESS_EXCEPTION = "31";

   @RBEntry("Cannot {0} \"{1}.{2}\" because it no longer exists.")
   public static final String DELETED_OBJECT_EXCEPTION = "32";

   @RBEntry("IconDelegate object and class not set - cannot generate icon selector")
   public static final String CANNOT_GET_ICON = "33";

   @RBEntry("Only accessible to ")
   @RBComment(" name of class to follow")
   public static final String FRIEND_ACCESS = "34";

   @RBEntry("The value assigned to \"{0}\" must be of the type \"{1}\".")
   @RBArgComment0("  {0} name of attribute")
   @RBArgComment1("  {1} name of correct class (type)")
   public static final String WRONG_TYPE = "35";

   @RBEntry("Changes to \"{0}\" are restricted, once the object has been persisted.")
   @RBArgComment0("  {0} name of attribute")
   public static final String CHANGE_RESTRICTION = "36";

   @RBEntry("The {0} foreign key criteria results in {1} objects found where only 1 is applicable")
   public static final String MANY_OBJECTS_FOUND = "37";

   @RBEntry("The {0} class name could not be reflected upon (i.e., it was not found)")
   public static final String RELFECTED_CLASS_NOT_FOUND = "38";

   @RBEntry("Error initializing \"{0}\".")
   public static final String ERROR_INITIALIZING = "39";

   @RBEntry("No application service registered for: \"{0}\".")
   @RBArgComment0("  {0} fully qualified class name")
   public static final String UNREGISTERED_SERVICE = "40";

   @RBEntry("Cannot create \"{0}\" because its identity is not unique.")
   public static final String CREATE_UNIQUENESS_EXCEPTION = "41";

   @RBEntry("Cannot update \"{0}\" because its identity is not unique.")
   public static final String UPDATE_UNIQUENESS_EXCEPTION = "42";

   @RBEntry("An IteratedObjectVector can only prepare Iterated objects for externalization.")
   @RBComment("This message indicates that an object that does not implement the Iterated interface was unexpectedly found in an instance of IteratedObjectVector when the prepareForExternalization() method was called.")
   public static final String ITERATED_OBJECT_VECTOR_CLASS_CAST = "43";

   @RBEntry("Single object event dispatch is not supported for this event: type={0}.")
   @RBArgComment0("The event type that is not supported.")
   public static final String SINGLE_OBJECT_EVENT_DISPATCH_NOT_SUPPORTED = "44";

   @RBEntry("A referential integrity violation occurred for this operation:")
   public static final String REFERENIAL_INTEGRITY_VOILATION = "45";

   @RBEntry("Referential integrity violations occurred for this operation:")
   public static final String REFERENIAL_INTEGRITY_VOILATIONS = "46";

   @RBEntry("Object \"{0}\" references the object \"{1}\" that was removed from the system.")
   @RBArgComment0("The object that contains the reference for the referential integrity violation.")
   @RBArgComment1("The target object of the referential integrity violation.")
   public static final String REFERENIAL_INTEGRITY_LINK_VOILATION = "47";

   @RBEntry("Role \"{0}\" for the \"{1}\" link class specifies a reference type \"{2}\" that is not supported for Collection based navigation.")
   @RBArgComment0("Name of a link role")
   @RBArgComment1("Link class")
   @RBArgComment2("Link reference type ")
   public static final String INVALID_ROLE_REFERENCE_TYPE = "48";

   @RBEntry("Object allocation data for a single object class")
   public static final String PER_CLASS_DATA_ROW_TYPE_DESCR = "49";

   @RBEntry("Object class name")
   public static final String OBJECT_CLASS_ITEM_DESCR = "50";

   @RBEntry("Count of objects created")
   public static final String INITIALIZED_ITEM_DESCR = "51";

   @RBEntry("Count of objects created that have been finalized")
   public static final String FINALIZED_ITEM_DESCR = "52";

   @RBEntry("Count of live object references")
   public static final String LIVE_REFS_ITEM_DESCR = "53";

   @RBEntry("Object allocation data grouped by object class")
   public static final String PER_CLASS_DATA_TYPE_DESCR = "54";

   @RBEntry("Object allocation data grouped by object class")
   public static final String PER_CLASS_DATA_ITEM_DESCR = "55";

   @RBEntry("Object allocation data")
   public static final String OVERALL_DATA_TYPE_DESCR = "56";

   @RBEntry("Object \"{0}\" cannot be deleted because it is referenced by the object \"{1}\".")
   @RBArgComment0("The target object of the referential integrity violation.")
   @RBArgComment1("The object that references the target object of the referential integrity violation.")
   public static final String REFERENIAL_INTEGRITY_OWNER_VOILATION = "70";

   @RBEntry("A limit has been execeeded when processing results.")
   public static final String RESULT_PROCESSOR_LIMIT = "80";
   
   @RBEntry("Authoring language \"{0}\" is invalid")
   @RBArgComment0("Value of authoring language attribute")
   @RBComment("Exception message when value of authoring language attribute is invalid")
   public static final String INVALID_LANGAUAGE = "81";
}
