/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.fc.collections;

import wt.util.resource.*;

@RBUUID("wt.fc.collections.collectionsResource")
public final class collectionsResource extends WTListResourceBundle {
   @RBEntry("The object was stale: \"{0}\"")
   @RBComment("Exception message when a collection contains stale objects and is refreshed with the VALIDATE_STALE option specified.")
   @RBArgComment0("The display identity of the stale object")
   public static final String STALE_OBJECTS = "0";

   @RBEntry("The object with the following key was deleted or inaccessible: \"{0}\"")
   @RBComment("Exception message when a collection contains deleted objects and is refreshed with the VALIDATE_DELETE option specified.")
   @RBArgComment0("The String value of the QueryKey for the deleted or inaccessible object. The QueryKey is typically an ObjectIdentifier or VersionForeignKey. Note that the collections refresh treats objects that the user doesn't have access to as if they were deleted")
   public static final String DELETED_OBJECTS = "1";
}
