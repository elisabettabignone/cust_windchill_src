/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.fc.jmx;

import wt.util.resource.*;

@RBUUID("wt.fc.jmx.fcJmxResource")
public final class fcJmxResource extends WTListResourceBundle {
   @RBEntry("Deadlock is detected.")
   public static final String DEADLOCK_DETECTED = "0";

   @RBEntry("Deadlock is detected on database connection with session id \"{0}\". ")
   public static final String DEADLOCK_DETECTED_WITH_SESSION = "1";

   @RBEntry("Long SQL statement is executed.")
   public static final String LONG_SQLSTATEMENT = "2";

   @RBEntry("Long SQL statement is executed: start={0} end={1}.")
   public static final String LONG_SQLSTATEMENT_WITH_TIME = "3";

   @RBEntry("Long SQL statement is executed on database connection with session id \"{0}\": start={1} end={2}.")
   public static final String LONG_SQLSTATEMENT_WITH_SESSION = "4";

   @RBEntry("SQL statement usage information")
   public static final String SQL_USAGE_TYPE_DESCR = "5";

   @RBEntry("SQL statement")
   public static final String SQL_ITEM_DESCR = "6";

   @RBEntry("Total cumulative duration of SQL statement executions")
   public static final String SQL_EXECUTION_SECONDS_ITEM_DESCR = "7";

   @RBEntry("Total number of SQL statement executions")
   public static final String SQL_EXECUTION_CALLS_ITEM_DESCR = "8";

   @RBEntry("Representative stack trace where SQL statement was excecuted")
   public static final String SQL_STACK_TRACE_ITEM_DESCR = "9";

   @RBEntry("'NumberOfTopStatementsToReport' must be positive")
   public static final String NUM_TOP_STATEMENTS_MUST_BE_POSITIVE = "10";

   @RBEntry("SQL usage statistics for a time interval")
   public static final String INTERVAL_STATS_TYPE_DESCR = "11";

   @RBEntry("Start of time interval")
   public static final String START_TIME_ITEM_DESCR = "12";

   @RBEntry("End of time interval")
   public static final String END_TIME_ITEM_DESCR = "13";

   @RBEntry("Servlet request id for representative SQL statement execution")
   public static final String SQL_SERVLET_REQUEST_ID_ITEM_DESCR = "14";

   @RBEntry("Method context id for representative SQL statement execution")
   public static final String SQL_METHOD_CONTEXT_ID_ITEM_DESCR = "15";

   @RBEntry("JDBC session id for representative SQL statement execution")
   public static final String SQL_JDBC_SESSION_ID_ITEM_DESCR = "16";

   @RBEntry("Total cumulative duration of SQL statement preparations")
   public static final String SQL_PREPARATION_SECONDS_ITEM_DESCR = "17";

   @RBEntry("Total number of SQL statement preparations")
   public static final String SQL_PREPARATION_CALLS_ITEM_DESCR = "18";

   @RBEntry("Total cumulative duration of SQL statement preparations and executions")
   public static final String SQL_TOTAL_ELAPSED_SECONDS_ITEM_DESCR = "19";

   @RBEntry("Total number of SQL statement preparations and executions")
   public static final String SQL_TOTAL_CALLS_ITEM_DESCR = "20";

   @RBEntry("Bind parameters for representative SQL statement execution")
   public static final String SQL_BIND_PARAMETERS_ITEM_DESCR = "21";
}
