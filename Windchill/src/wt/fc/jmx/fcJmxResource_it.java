/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.fc.jmx;

import wt.util.resource.*;

@RBUUID("wt.fc.jmx.fcJmxResource")
public final class fcJmxResource_it extends WTListResourceBundle {
   @RBEntry("Rilevato blocco critico.")
   public static final String DEADLOCK_DETECTED = "0";

   @RBEntry("È stato rilevato un blocco critico sulla connessione di database con ID sessione \"{0}\". ")
   public static final String DEADLOCK_DETECTED_WITH_SESSION = "1";

   @RBEntry("Istruzione SQL long eseguita.")
   public static final String LONG_SQLSTATEMENT = "2";

   @RBEntry("Istruzione SQL long eseguita: inizio={0} fine={1}.")
   public static final String LONG_SQLSTATEMENT_WITH_TIME = "3";

   @RBEntry("Istruzione SQL long eseguita su connessione di database con ID sessione \"{0}\": inizio={1} fine={2}.")
   public static final String LONG_SQLSTATEMENT_WITH_SESSION = "4";

   @RBEntry("Informazioni sull'utilizzo dell'istruzione SQL")
   public static final String SQL_USAGE_TYPE_DESCR = "5";

   @RBEntry("Istruzione SQL")
   public static final String SQL_ITEM_DESCR = "6";

   @RBEntry("Durata cumulativa totale di esecuzione delle istruzioni SQL")
   public static final String SQL_EXECUTION_SECONDS_ITEM_DESCR = "7";

   @RBEntry("Numero complessivo di esecuzioni delle istruzioni SQL")
   public static final String SQL_EXECUTION_CALLS_ITEM_DESCR = "8";

   @RBEntry("Lo stack rappresentativo traccia il punto in cui è stata eseguita l'istruzione SQL")
   public static final String SQL_STACK_TRACE_ITEM_DESCR = "9";

   @RBEntry("'NumberOfTopStatementsToReport' deve essere positivo")
   public static final String NUM_TOP_STATEMENTS_MUST_BE_POSITIVE = "10";

   @RBEntry("Statistiche utilizzo SQL per intervallo di tempo")
   public static final String INTERVAL_STATS_TYPE_DESCR = "11";

   @RBEntry("Inizio intervallo di tempo")
   public static final String START_TIME_ITEM_DESCR = "12";

   @RBEntry("Fine intervallo di tempo")
   public static final String END_TIME_ITEM_DESCR = "13";

   @RBEntry("ID richiesta servlet per esecuzione istruzione SQL rappresentativa")
   public static final String SQL_SERVLET_REQUEST_ID_ITEM_DESCR = "14";

   @RBEntry("ID contesto di metodo per esecuzione istruzione SQL rappresentativa")
   public static final String SQL_METHOD_CONTEXT_ID_ITEM_DESCR = "15";

   @RBEntry("ID sessione JDBC per esecuzione istruzione SQL rappresentativa")
   public static final String SQL_JDBC_SESSION_ID_ITEM_DESCR = "16";

   @RBEntry("Durata cumulativa totale di preparazione delle istruzioni SQL")
   public static final String SQL_PREPARATION_SECONDS_ITEM_DESCR = "17";

   @RBEntry("Numero complessivo di preparazioni delle istruzioni SQL")
   public static final String SQL_PREPARATION_CALLS_ITEM_DESCR = "18";

   @RBEntry("Durata cumulativa totale di preparazione ed esecuzione delle istruzioni SQL")
   public static final String SQL_TOTAL_ELAPSED_SECONDS_ITEM_DESCR = "19";

   @RBEntry("Numero complessivo di preparazioni ed esecuzioni delle istruzioni SQL")
   public static final String SQL_TOTAL_CALLS_ITEM_DESCR = "20";

   @RBEntry("Parametri di collegamento per esecuzione istruzione SQL rappresentativa")
   public static final String SQL_BIND_PARAMETERS_ITEM_DESCR = "21";
}
