/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.project;

import wt.util.resource.*;

@RBUUID("wt.project.projectResource")
public final class projectResource extends WTListResourceBundle {
   @RBEntry("Could not get Cabinet Reference")
   public static final String NO_CAB_REF = "0";

   @RBEntry("Could not get folder path")
   public static final String NO_FOLDER_PATH = "1";

   @RBEntry("Could not get location")
   public static final String NO_LOCATION = "2";

   @RBEntry("No folder has been assigned in which to store the Project.  Project not saved.")
   public static final String NO_PROJECT_FOLDER = "3";

   @RBEntry("A project cannot be stored in a personal cabinet.  Choose another location.  ")
   public static final String PROJECT_IN_USER_LOCATION = "4";

   @RBEntry("Projects cannot be deleted from the system.")
   public static final String PROHIBIT_DELETE_PROJECT = "5";

   @RBEntry("You can not change the identity of a project.")
   public static final String PROHIBIT_IDCHANGE_PROJECT = "6";

   @RBEntry("Unable to fill in the attributes for the project in the object.")
   public static final String INFLATE_FAILED = "7";

   @RBEntry("Unable to set search criteria to project = {0}.")
   @RBArgComment0(" the project object.")
   public static final String SEARCH_CRITERIA = "8";

   @RBEntry("A project \"{0}\" does not exist in the administrative domain: \"{1}\".  The project has not been set for this object.")
   @RBArgComment0(" refers to a project name")
   @RBArgComment1("  refers to the name of an administrative domain")
   public static final String NONEXISTENT_PROJECT = "9";

   @RBEntry("The administrative domain: \"{0}\" has multiple projects with name: \"{1}\".  The project has not been set for this object.")
   @RBArgComment0(" refers to the name of the administrative domain ")
   @RBArgComment1(" refers to the project name")
   public static final String MULTIPLE_PROJECTS = "10";

   @RBEntry("The value passed for parameter - \"{0}\" was null.")
   @RBArgComment0(" refers to the parameters that is null.")
   public static final String NULL_PARAMETER = "11";

   @RBEntry("The project \"{0}\" was not found in the system, cannot continue search.")
   @RBArgComment0(" refers to the project string the user tried to query on.")
   public static final String NO_PROJECT_FOR_SEARCH = "12";

   @RBEntry("\"{0}\" is an invalid role.  Valid roles are found in the following resource bundles: wt.project.RoleRB.java or wt.project.ActorRoleRB.java")
   @RBArgComment0(" is the role")
   public static final String INVALID_ROLE = "13";

   @RBEntry("You cannot set the Project because the {0} object is already persisted.  Use the Reassign Project option.  ")
   @RBArgComment0("  the Project Managed object")
   public static final String CANT_SET_PROJ_WHEN_PERSISTENT = "14";

   @RBEntry("Reproject")
   public static final String REPROJECT = "15";

   @RBEntry("You are not authorized to {0}.  You need modify rights on the object to complete this task.")
   @RBArgComment0("action the user is not authorized to complete")
   public static final String NOT_AUTHORIZED_TO_MODIFY = "16";

   @RBEntry("You are not allowed to reproject a working copy.  Check-in the object before proceeding.")
   public static final String WORKING_COPY = "17";

   @RBEntry("The project is set for this object, but it does not reference an actual project.")
   public static final String PROJECT_REF_NOT_SET = "18";

   @RBEntry("{0} not allowed because the object is not the latest iteration.  Refresh the data and try again.  ")
   @RBArgComment0(" Action (Submit, Promote, Demote, Drop, etc) that is not allowed")
   public static final String OBJECT_NOT_LATEST_ITERATION = "19";

   @RBEntry("You may not {0} a checked out object.")
   @RBArgComment0(" refers to the name of the task")
   public static final String CHECKED_OUT_OBJECT = "20";

   @RBEntry("Copy Of")
   public static final String COPY_OF = "21";

   @RBEntry("The {0} project associated with the {1} object is not enabled.  Select a project that is enabled.")
   @RBArgComment0(" the disabled Project")
   @RBArgComment1(" the object that is being updated/created ")
   public static final String ASSOC_PROJECT_NOT_ENABLED = "22";

   @RBEntry(", ")
   public static final String IDENTITY_STRING_DELIMITER = "23";

   @RBEntry("Multiple projects were found:  {0}.  Clarify the project identity and try again.  ")
   @RBArgComment0(" a list of all project identities that were found")
   public static final String CLARIFY_PROJECT_IDENTITY = "24";

   @RBEntry("The {0} project is in use.  All uses must be removed before the {0} project can be deleted.")
   @RBArgComment0(" the identity of the project that the user is trying to delete")
   public static final String DELETE_PROJECT_PROHIBITED = "25";

   @RBEntry("<B>")
   @RBPseudo(false)
   @RBComment("-do not translate!")
   public static final String LABEL_BEGIN = "26";

   @RBEntry(": </B>")
   @RBPseudo(false)
   @RBComment("-do not translate!")
   public static final String LABEL_END = "27";

   @RBEntry("</TD></TR><TR><TD align=right VALIGN=TOP>")
   @RBPseudo(false)
   @RBComment("-do not translate!")
   public static final String ROLE_BEGIN = "28";

   @RBEntry(": </TD><TD colspan=3 wrap>")
   @RBPseudo(false)
   @RBComment("-do not translate!")
   public static final String ROLE_END = "29";

   @RBEntry(", ")
   @RBComment("-do not translate!")
   public static final String PARTICIPANT_SEPARATOR = "30";

   @RBEntry("Role Participants:")
   public static final String ROLE_PARTICIPANTS_LABEL = "31";

   @RBEntry("You are not authorized to update the role participants list.  To complete this action, you must have modify permissions on {0}. ")
   public static final String ROLE_PARTICIPANT_MODIFY_NOT_ALLOWED = "32";

   @RBEntry("Role Participants")
   public static final String ROLE_PARTICIPANTS = "33";

   @RBEntry("for")
   public static final String FOR_LABEL = "34";
}
