/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.project;

import wt.util.resource.*;

@RBUUID("wt.project.projectResource")
public final class projectResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile ottenere i riferimenti dello schedario")
   public static final String NO_CAB_REF = "0";

   @RBEntry("Impossibile individuare il percorso della cartella")
   public static final String NO_FOLDER_PATH = "1";

   @RBEntry("Impossibile individuare la posizione")
   public static final String NO_LOCATION = "2";

   @RBEntry("Non è stata assegnata alcuna cartella in cui memorizzare il progetto. Progetto non salvato.")
   public static final String NO_PROJECT_FOLDER = "3";

   @RBEntry("Impossibile memorizzare un progetto in uno schedario personale. Selezionare un'altra posizione.")
   public static final String PROJECT_IN_USER_LOCATION = "4";

   @RBEntry("Impossibile eliminare i progetti dal sistema.")
   public static final String PROHIBIT_DELETE_PROJECT = "5";

   @RBEntry("Impossibile modificare l'identificativo di un progetto.")
   public static final String PROHIBIT_IDCHANGE_PROJECT = "6";

   @RBEntry("Impossibile completare gli attributi per il progetto nell'oggetto.")
   public static final String INFLATE_FAILED = "7";

   @RBEntry("Impossibile impostare il criterio di ricerca \"progetto = {0}\".")
   @RBArgComment0(" the project object.")
   public static final String SEARCH_CRITERIA = "8";

   @RBEntry("Il progetto \"{0}\" non esiste nel dominio amministrativo \"{1}\".  Impossibile impostare il progetto per l'oggetto.")
   @RBArgComment0(" refers to a project name")
   @RBArgComment1("  refers to the name of an administrative domain")
   public static final String NONEXISTENT_PROJECT = "9";

   @RBEntry("Dominio amministrativo \"{0}\" contiene più progetti con nome \"{1}\".  Impossibile impostare il progetto per l'oggetto.")
   @RBArgComment0(" refers to the name of the administrative domain ")
   @RBArgComment1(" refers to the project name")
   public static final String MULTIPLE_PROJECTS = "10";

   @RBEntry("Il valore trasferito per il parametro \"{0}\" era nullo.")
   @RBArgComment0(" refers to the parameters that is null.")
   public static final String NULL_PARAMETER = "11";

   @RBEntry("Il progetto \"{0}\" non è stato trovato nel sistema. Impossibile proseguire la ricerca.")
   @RBArgComment0(" refers to the project string the user tried to query on.")
   public static final String NO_PROJECT_FOR_SEARCH = "12";

   @RBEntry("\"{0}\" non è un ruolo valido.  I ruoli validi si trovano nei resource bundle seguenti:  wt.project.RoleRB.java o wt.project.ActorRoleRB.java")
   @RBArgComment0(" is the role")
   public static final String INVALID_ROLE = "13";

   @RBEntry("L'oggetto {0} è già persistente. Impossibile impostare il progetto.  Utilizzare l'opzione Assegna a un altro progetto.")
   @RBArgComment0("  the Project Managed object")
   public static final String CANT_SET_PROJ_WHEN_PERSISTENT = "14";

   @RBEntry("Assegna a un altro progetto")
   public static final String REPROJECT = "15";

   @RBEntry("Nessuna autorizzazione per {0}. È necessario modificare i diritti sull'oggetto per completare il task.")
   @RBArgComment0("action the user is not authorized to complete")
   public static final String NOT_AUTHORIZED_TO_MODIFY = "16";

   @RBEntry("Non è consentito assegnare la copia in modifica a un altro progetto.  Effettuare Check-In dell'oggetto prima di continuare.")
   public static final String WORKING_COPY = "17";

   @RBEntry("Il progetto impostato per l'oggetto non è un progetto effettivo.")
   public static final String PROJECT_REF_NOT_SET = "18";

   @RBEntry("L'oggetto non è l'ultima iterazione. L'azione {0} non è consentita.  Aggiornare i dati e riprovare.")
   @RBArgComment0(" Action (Submit, Promote, Demote, Drop, etc) that is not allowed")
   public static final String OBJECT_NOT_LATEST_ITERATION = "19";

   @RBEntry("Potrebbe non essere possibile {0} un oggetto sottoposto a Check-Out.")
   @RBArgComment0(" refers to the name of the task")
   public static final String CHECKED_OUT_OBJECT = "20";

   @RBEntry("Copia di")
   public static final String COPY_OF = "21";

   @RBEntry("Il progetto {0} asociato all'oggetto {1}non è attivato. Selezionare un progetto attivato.")
   @RBArgComment0(" the disabled Project")
   @RBArgComment1(" the object that is being updated/created ")
   public static final String ASSOC_PROJECT_NOT_ENABLED = "22";

   @RBEntry(", ")
   public static final String IDENTITY_STRING_DELIMITER = "23";

   @RBEntry("Più progetti sono stati trovati: {0}.  Specificare l'elemento identificativo dell'oggetto e riprovare.")
   @RBArgComment0(" a list of all project identities that were found")
   public static final String CLARIFY_PROJECT_IDENTITY = "24";

   @RBEntry("Il progetto {0} è correntemente in uso. Prima di eliminare il progetto {0} è necessario rimuovere tutti i componenti.")
   @RBArgComment0(" the identity of the project that the user is trying to delete")
   public static final String DELETE_PROJECT_PROHIBITED = "25";

   @RBEntry("<B>")
   @RBPseudo(false)
   @RBComment("-do not translate!")
   public static final String LABEL_BEGIN = "26";

   @RBEntry(": </B>")
   @RBPseudo(false)
   @RBComment("-do not translate!")
   public static final String LABEL_END = "27";

   @RBEntry("</TD></TR><TR><TD align=right VALIGN=TOP>")
   @RBPseudo(false)
   @RBComment("-do not translate!")
   public static final String ROLE_BEGIN = "28";

   @RBEntry(": </TD><TD colspan=3 wrap>")
   @RBPseudo(false)
   @RBComment("-do not translate!")
   public static final String ROLE_END = "29";

   @RBEntry(", ")
   @RBComment("-do not translate!")
   public static final String PARTICIPANT_SEPARATOR = "30";

   @RBEntry("Partecipanti ai ruoli:")
   public static final String ROLE_PARTICIPANTS_LABEL = "31";

   @RBEntry("L'utente non è autorizzato ad aggiornare l'elenco dei partecipanti ai ruoli. Per completare l'azione, è necessario disporre del permesso di modifica di {0}.")
   public static final String ROLE_PARTICIPANT_MODIFY_NOT_ALLOWED = "32";

   @RBEntry("Partecipanti ai ruoli")
   public static final String ROLE_PARTICIPANTS = "33";

   @RBEntry("per")
   public static final String FOR_LABEL = "34";
}
