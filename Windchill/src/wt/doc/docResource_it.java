/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.doc;

import wt.util.resource.*;

@RBUUID("wt.doc.docResource")
public final class docResource_it extends WTListResourceBundle {
   @RBEntry("Si è verificato un errore wt.doc. Messaggio di sistema:")
   @RBComment("Format for displaying document information.")
   @RBArgComment0("document number")
   @RBArgComment1("document name")
   @RBArgComment2("document version")
   public static final String GENERAL_ERROR = "0";

   @RBEntry("Impossibile individuare lo schedario personale dell'utente:  \"{0}\".")
   public static final String NO_PERSONAL_CABINET = "1";

   @RBEntry("Check-In di {0} completato")
   public static final String SUCCESS_CHECK_IN = "10";

   @RBEntry("Il valore per l'argomento \"{0}\" non può essere nullo")
   public static final String NULL_ARGUMENT = "11";

   @RBEntry("Impossibile aggiungere una relazione al documento {0} perchè non sottoposto a Check-Out dall'utente.")
   public static final String NO_RELATIONSHIP = "12";

   @RBEntry("{0} ({1}) {2}")
   public static final String DOC_DISPLAY_FORMAT = "13";

   @RBEntry("Imposta specifica di configurazione")
   public static final String SET_CONFIG_SPEC_LABEL = "14";

   @RBEntry("Documenti Riferimenti")
   public static final String DEPENDSON_SUBTITLE = "15";

   @RBEntry("Documenti Referenziato da")
   public static final String HASDEPENDENT_SUBTITLE = "16";

   @RBEntry("Parti Referenziato da")
   public static final String PART_REF_SUBTITLE = "17";

   @RBEntry("Commento")
   public static final String LINKDESCRIPTION = "18";

   @RBEntry("Componenti")
   public static final String USES_SUBTITLE = "19";

   @RBEntry("Impossibile individuare la cartella Check-Out dell'utente.")
   public static final String NO_CHECKOUT_FOLDER = "2";

   @RBEntry("Impiegato da")
   public static final String USEDBY_SUBTITLE = "20";

   @RBEntry("Aggiungi riferimenti")
   @RBComment("Hardcoded in HTML templates to 21.")
   public static final String ADD_DEPENDENCY_LINK = "21";

   @RBEntry("Trova i documenti per i riferimenti")
   public static final String FIND_WTDOCUMENT = "22";

   @RBEntry("Risultati della ricerca per l'aggiunta di documenti referenziati")
   public static final String SEARCH_RESULTS = "23";

   @RBEntry("Versione più recente")
   public static final String LATESTVERSION = "24";

   @RBEntry("Errore durante la ricerca del contenuto di {0} nella struttura del documento.")
   @RBArgComment0("document")
   public static final String STRUCTURE_CONTENT = "25";

   @RBEntry("Errore durante la ricerca nella struttura del documento {0}.")
   @RBArgComment0("document")
   public static final String STRUCTURE_WALKING = "26";

   @RBEntry("Impossibile aggiungere i criteri di ricerca per selezionare il documento in base alle informazioni della baseline. La baseline referenziata potrebbe essere stata eliminata.")
   public static final String INVALID_DOC_BASELINE_CONFIG_SPEC = "27";

   @RBEntry("XXXX")
   @RBComment("This string is not used in Windchill right now.")
   public static final String STRUCTURE_SUBTITLE = "28";

   @RBEntry("Impossibile trovare la classe di {0}.")
   @RBComment("Document type.")
   public static final String CLASS_NOT_FOUND = "29";

   @RBEntry("Il valore \"{0}\" non è un valore \"{1}\" valido.")
   public static final String INVALID_VALUE = "3";

   @RBEntry("Impossibile creare il documento {0}.")
   @RBComment("Document type.")
   public static final String UNABLE_TO_INSTANTIATE = "30";

   @RBEntry("Non è stato trovato alcun oggetto con cui creare la relazione.")
   public static final String NO_OBJECTS_FOR_RELATIONSHIP = "31";

   @RBEntry("Impossibile associare i documenti ai commenti.")
   public static final String VECTOR_MISMATCH = "32";

   @RBEntry("Aggiungi componenti")
   @RBComment("Hardcoded in HTML templates to 33.")
   public static final String ADD_STRUCTURE_LINK = "33";

   @RBEntry("Errore durante l'impostazione dei valori degli attributi.")
   public static final String SETTING_VALUES = "34";

   @RBEntry("Errore durante l'impostazione del progetto {0} sul documento.")
   @RBComment("Project")
   public static final String SETTING_PROJECT = "35";

   @RBEntry("La posizione della cartella {0} non è valida.")
   @RBComment("Folder")
   public static final String INVALID_FOLDER = "36";

   @RBEntry("Errore durante l'impostazione del ciclo di vita {0}.")
   @RBComment("Lifecycle.")
   public static final String SETTING_LIFECYCLE = "37";

   @RBEntry("XXXX")
   @RBComment("This string is not used in Windchill right now.")
   public static final String DOC_CREATE_ERROR = "38";

   @RBEntry("Errore durante la ricerca della relazione da eliminare.")
   public static final String NO_RELATIONSHIP_FOR_DELETE = "39";

   @RBEntry("La creazione di documenti di tipo {0} non è supportata")
   public static final String INVALID_DOC_TYPE = "4";

   @RBEntry("Aggiorna")
   public static final String UPDATE_URL_LABEL = "40";

   @RBEntry("Errore durante la ricerca della relazione da aggiornare.")
   public static final String NO_RELATIONSHIP_FOR_UPDATE = "41";

   @RBEntry("L'oggetto deve essere {0} o una sottoclasse perché questa funzione sia operante.")
   @RBComment("object class")
   public static final String OBJECT_WRONG_CLASS = "42";

   @RBEntry("Errore durante la personalizzazione di questa funzione.")
   public static final String DELEGATE_INSTANTIATION = "43";

   @RBEntry("Preleva il contenuto")
   public static final String GETCONTENT_URL_LABEL = "44";

   @RBEntry("Preleva il contenuto non supportato da {0}.")
   @RBComment("class of object")
   public static final String GETCONTENT_NOTSUPPORTED = "45";

   @RBEntry("Il prelievo del contenuto è stato completato.")
   public static final String GETCONTENT_SUCCESS = "46";

   @RBEntry("Il Check-In non è stato eseguito. Specificare uno schedario condiviso o effettuare il Check-Out del documento.")
   public static final String NO_CHECKIN_DONE = "47";

   @RBEntry("Riferimenti per ")
   public static final String REFERENCES_TITLE = "48";

   @RBEntry("Utilizzo della struttura per ")
   public static final String USAGE_TITLE = "49";

   @RBEntry("Per la creazione di un documento è obbligatorio almeno un file ")
   public static final String FILE_REQUIRED = "5";

   @RBEntry("Gerarchia della struttura per ")
   public static final String STRUCT_TITLE = "50";

   @RBEntry("Imposta la specifica di configurazione più recente")
   public static final String SET_STD_CSPEC = "51";

   @RBEntry("Imposta la specifica di configurazione della baseline")
   public static final String SET_BASE_CSPEC = "52";

   @RBEntry("Includi i documenti nel mio schedario personale")
   @RBComment("This string is used in Structure Hierarchy set Baseline Config Spec.")
   public static final String INCLUDE_DOCS = "53";

   @RBEntry("Stato del ciclo di vita:")
   @RBComment("This string is used in Structure Hierarchy set Std Config Spec.")
   public static final String STATE_PROMPT = "54";

   @RBEntry("Cerca...")
   @RBComment("This string is used in Structure Hierarchy set Baseline Config Spec.")
   public static final String SEARCH_BUTTON = "55";

   @RBEntry("Nome:")
   @RBComment("This string is used in Structure Hierarchy set Baseline Config Spec.")
   public static final String NAME_FIELD = "56";

   @RBEntry("Più recente")
   @RBComment("This string is used in Structure Hierarchy set Std Config Spec.")
   public static final String STD_TAB = "57";

   @RBEntry("Baseline")
   @RBComment("This string is used in Structure Hierarchy set Baseline Config Spec.")
   public static final String BASE_TAB = "58";

   @RBEntry("Usa solo per questa sessione")
   @RBComment("Structure Hierarchy set Std Config Spec.")
   public static final String THIS_SESSION = "59";

   @RBEntry("Importazione di {0} completata")
   public static final String SUCCESS_IMPORT = "6";

   @RBEntry("  OK  ")
   @RBComment("This string is used in Structure Hierarchy set Std Config Spec.")
   public static final String OKAY_BUTTON = "60";

   @RBEntry("Trova i documenti della struttura")
   @RBComment("This string is used in creating document structures.")
   public static final String FIND_FOR_STRUCT = "61";

   @RBEntry("Risultati della ricerca per l'aggiunta della struttura")
   @RBComment("This string is used in creating document structures.")
   public static final String RESULTS_FOR_STRUCT = "62";

   @RBEntry("Aggiungi gli elementi selezionati")
   public static final String ADD_SELECTED_BUTTON = "63";

   @RBEntry("È stato restituito più di un wt.doc.WTDocumentConfigSpec. Se ne attendeva solo uno.")
   public static final String WTDOCCONFIGSPEC_QTY = "64";

   @RBEntry("Scaricamento del contenuto principale in corso...")
   public static final String DOWNLOAD_PRIMARY = "65";

   @RBEntry("Contenuto principale da scaricare inesistente.")
   public static final String NO_PRIMARY_DOWNLOAD = "66";

   @RBEntry("Errore durante l'impostazione del team {0} sul documento.")
   @RBComment("Team Template x")
   public static final String SETTING_TEAMTEMPLATE = "67";

   @RBEntry("Lo script method Windchill non ha valore per il parametro \"{0}\". Questo parametro dovrebbe essere impostato ad un nome di classe valido.")
   public static final String NULL_CLASS_PARAMETER = "68";

   @RBEntry("Lo script method Windchill ha il valore \"{0}\" per il parametro \"{1}\". Non è una classe valida.")
   public static final String INVALID_CLASS_PARAMETER = "69";

   @RBEntry("Nota: il documento {0} è stato inviato")
   public static final String SUBMIT_SUCCESS = "7";

   @RBEntry("Lo script method Windchill non ha un valore per il parametro \"{0}\".")
   public static final String NULL_STRING_PARAMETER = "70";

   @RBEntry("Il tasto \"{0}\" non è valido per il resource bundle indicato nella chiamata dello script method Windchill.")
   public static final String INVALID_RESOURCEKEY_PARAMETER = "71";

   @RBEntry("Aggiornamento di {0} fallito. L'oggetto non è sottoposto a Check-Out.")
   public static final String UPDATE_FAILED = "72";

   @RBEntry("L'utente non è autorizzato a {0}.")
   public static final String NOT_PERMITTED = "73";

   @RBEntry("L'oggetto del contesto non è {0}.")
   public static final String OBJECT_NOT = "74";

   @RBEntry("Errore durante l'inizializzazione di \"{0}\".")
   public static final String ERROR_INITIALIZING = "75";

   @RBEntry("{0} e tutte le sue iterazioni sono state eliminate.")
   public static final String SUCCESS_DELETE = "76";

   @RBEntry("{0} è fallito. Azione non impostata nella chiamata dello script Windchill.")
   public static final String NO_ACTION = "77";

   @RBEntry("{0} è una copia in modifica. Impossibile spostarla.")
   public static final String NOT_MOVED = "78";

   @RBEntry("Lo spostamento di {0} in {1} non è riuscito. Si è verificato un errore. {2}")
   @RBArgComment0("Is the identity of the document.")
   @RBArgComment1("The folder or path that the document was attempted to move to.")
   @RBArgComment2("The error message that is passed up from the folder service, should be complete localized sentences.")
   public static final String MOVE_FAILED = "79";

   @RBEntry("Attenzione: il documento {0} non è stato inviato.")
   public static final String SUBMIT_FAILED = "8";

   @RBEntry("Sposta")
   public static final String MOVE_URL_LABEL = "80";

   @RBEntry("{0} è stato spostato in {1}.")
   public static final String SUCCESS_MOVE = "81";

   @RBEntry("modifica")
   public static final String MODIFY = "82";

   @RBEntry("Creazione nuova revisione di {0} non riuscita.")
   public static final String REVISE_FAILED = "83";

   @RBEntry("Creazione nuova versione di {0} completata.")
   public static final String REVISE_SUCCESS = "84";

   @RBEntry("Nuova revisione")
   public static final String REVISE_URL_LABEL = "85";

   @RBEntry("{0} è sottoposto a Check-Out.")
   public static final String CHECKED_OUT = "86";

   @RBEntry("Annulla Check-Out")
   public static final String UNDOCHECKOUT_URL_LABEL = "87";

   @RBEntry("L'annullamento del Check-Out di {0} è fallito.")
   public static final String FAILED_UNDOCHECKOUT = "88";

   @RBEntry("L'annullamento del Check-Out di {0} è riuscito.")
   public static final String SUCCESS_UNDO_CHECKOUT = "89";

   @RBEntry("{0} non è stato sottoposto a Check-Out dall'utente.")
   public static final String NOT_CHECKED_OUT = "9";

   @RBEntry("Rinomina")
   public static final String RENAME_URL_LABEL = "90";

   @RBEntry("La ridenominazione di {0} è fallita. Per ulteriori informazioni, controllare il log.")
   public static final String RENAME_FAILURE = "91";

   @RBEntry("La ridenominazione di {0} è stata completata")
   public static final String RENAME_SUCCESS = "92";

   @RBEntry("elimina")
   public static final String DELETE = "93";

   @RBEntry("Eliminazione di {0} non riuscita. L'oggetto è sottoposto a Check-Out. Annullare il Check-Out.")
   public static final String DELETE_FAILED = "94";

   @RBEntry("Elimina")
   public static final String DELETE_URL_LABEL = "95";

   @RBEntry("Il documento non è stato modificato.")
   public static final String RENAME_NO_CHANGE = "96";

   @RBEntry("Eliminare il documento?")
   public static final String WANT_TO_DELETE_DOCUMENT = "97";

   @RBEntry("?")
   public static final String QUESTION_MARK = "98";

   @RBEntry("creazione")
   public static final String CREATE = "99";

   @RBEntry("Errore durante la ricerca dei componenti del documento {0} {1}.")
   @RBComment("Error message for failing in searching for relationships.")
   @RBArgComment0("document name")
   @RBArgComment1("document number")
   public static final String ERROR_FINDING_USES = "100";

   @RBEntry("Errore durante l'impostazione dell'ordine per un link a un documento strutturato tra il documento padre ({0}) e il documento figlio ({1}).")
   @RBComment("In reordering a structured document there was an error setting the order for one of the documents.")
   @RBArgComment0("parent document name")
   @RBArgComment1("child document name")
   public static final String ERROR_STRUCTURE_ORDER = "101";

   @RBEntry("Errore durante il riordino della struttura del documento perché non è stato trovato alcun link tra il documento padre ({0}) e il documento figlio ({1}).")
   @RBComment("In reordering a structured document one of the links couldn't be found.")
   @RBArgComment0("parent document name")
   @RBArgComment1("child document name")
   public static final String MISSING_USAGE_LINK = "102";

   @RBEntry("Errore, impossibile modificare le relazioni di questo documento perché il documento {0} è stato sottoposto a Check-Out da {1}. Per poter effettuare l'operazione, è necessario effettuare il Check-Out del documento.")
   @RBComment("Error message for trying to modify a document's relationships when it is checked out to someone else.")
   @RBArgComment0("Document name.")
   @RBArgComment1("Other user that has the document checked out.")
   public static final String CHECKED_OUT_TO_OTHER = "103";

   @RBEntry("Errore, impossibile modificare le relazioni del documento perché non si dispone dei privilegi per effettuare il Check-Out di {0}. Per effettuare l'operazione, è necessario effettuare il Check-Out del documento.")
   @RBComment("Error message for trying to modify a document's relationships when the user doesn't have check out access.")
   @RBArgComment0("Document name.")
   public static final String CHECKED_OUT_ERROR = "104";

   @RBEntry("Errore durante il Check-Out di {0} prima del tentativo di modifica delle relazioni del documento. Tutte le modifiche sono state annullate.")
   @RBComment("Error message for checkout failing before modifying reference or usage links on a document.  Entire transaction rolled back.")
   @RBArgComment0("Document name.")
   public static final String ON_CHECKOUT_ERROR = "105";

   @RBEntry("Errore durante il Check-In di {0} dopo il tentativo di modifica delle relazioni del documento. Tutte le modifiche sono state annullate.")
   @RBComment("Error message for checkin failing after modifying reference or usage links on a document.  Entire transaction rolled back.")
   @RBArgComment0("Document name.")
   public static final String ON_CHECKIN_ERROR = "106";

   @RBEntry("Errore: non è stato indicato alcun documento da cui rimuovere le relazioni di riferimento.")
   @RBComment("Error message for null document passed into server method for removing references relationships.")
   public static final String NO_DOCUMENT_FOR_REMOVE_REFERENCES = "107";

   @RBEntry("Errore durante la modifica dello stato Disponibile all'uso dell'oggetto.")
   @RBComment("Error occurred while changing the enabled flag for the object.The object here is a Document Template. ")
   public static final String SETTING_ENABLED = "108";

   @RBEntry("Errore durante la modifica dello stato Disponibile all'uso dell'oggetto durante la revisione.")
   @RBComment("Error occurred while changing the enabled flag on revise for the object. The object here is a Document Template.")
   public static final String ENABLEFLAG_ON_REVISE = "109";

   @RBEntry("{0} - {1}")
   @RBPseudo(false)
   @RBComment("Used to populate the template selection drop down list.")
   @RBArgComment0("Type of the template like \"Document\" or \"Reference Document\", can be a customer defined type.")
   @RBArgComment1("Name of the document.")
   public static final String TEMPLATE_SELECTION = "110";

   @RBEntry("Errore, nome di modello di documento duplicato {0} non consentito nello stesso contesto.")
   @RBArgComment0("Document Template Name.")
   public static final String DUPNAME_PROHIBITED = "111";

   @RBEntry(" Errore durante il recupero dell'ultima iterazione della versione più recente.")
   public static final String LATESTVER_ITER = "112";

   @RBEntry("Attivato ")
   public static final String ENABLED_LABEL = "113";

   @RBEntry("Errore: i campi contrassegnati da un asterisco devono essere completati.")
   @RBComment("Message that appears when user tries to leave a page without filling in a required (asterisked) field.")
   public static final String MISSING_REQUIRED_FIELD = "114";

   @RBEntry("Specificare un nome diverso. Massimo numero di caratteri consentiti: {0} ")
   @RBComment("error message when the name field exceeds the maximum number of characters allowed.")
   @RBArgComment0("maximum number of characters allowed for the Name field.")
   public static final String LONG_NAME_ERROR = "115";

   @RBEntry("Impossibile rinominare il documento {0} ({1}) perché l'identificativo non è univoco.")
   @RBComment("error message when a duplicate number is entered.")
   @RBArgComment0("document number")
   @RBArgComment1("document name")
   public static final String NUMBER_UNIQUENESS_EXCEPTION = "116";

   @RBEntry("L'eliminazione di {0} è fallita con l'errore seguente: {1}")
   @RBComment("error message when a document cannot be deleted for a reason other than being checked out")
   @RBArgComment0("document identity string")
   @RBArgComment1("error message passed from somewhere else")
   public static final String DELETE_FAILED_MESSAGE = "117";

   @RBEntry("<-- Selezionare un tipo -->")
   @RBComment("String to be displayed as default in the type dropdown of document wizards, to prompt the user to select a type.")
   public static final String SELECT_A_TYPE = "118";

   @RBEntry("La versione di documento di livello superiore è stata modificata da {0} a {1} per riflettere la specifica di configurazione usata.")
   public static final String DOC_VERSION_HAS_CHANGED = "119";

   @RBEntry("La specifica di configurazione sopra identificata non seleziona una versione di documento di {0}. Fare clic sul link della specifica di configurazione soprastante per scegliere una nuova configurazione.")
   public static final String NO_DOC_VERSION_FOUND = "120";

   @RBEntry("In questo contesto esiste già un documento denominato {0}. Scegliere un nome o un contesto differente.")
   public static final String ERROR_DOC_DUP_NAME_CONTEXT = "ERROR_DOC_DUP_NAME_CONTEXT";

   @RBEntry("In questa cartella esiste già un documento denominato {0}. Scegliere un nome o una cartella differente.")
   public static final String ERROR_DOC_DUP_NAME_FOLDER = "ERROR_DOC_DUP_NAME_FOLDER";

   @RBEntry("Impossibile eliminare il documento \"{0}\" poiché i seguenti documenti vi fanno riferimento: {1}")
   @RBArgComment0("Display identity of the object being deleted.")
   @RBArgComment1("Display identity of the object which references the deleted object.")
   public static final String CAN_NOT_DELETE_REFERENCED_BY = "121";
}
