/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.doc;

import wt.util.resource.*;

@RBUUID("wt.doc.CreateRB")
public final class CreateRB_it extends WTListResourceBundle {
   @RBEntry("Creare documenti?")
   @RBComment("Header in create document wizzard")
   public static final String CREATE_DOCUMENT_PROMPT = "0";

   @RBEntry("OK")
   @RBComment("Text displayed in ok button")
   public static final String OK_BUTTON = "1";

   @RBEntry("Annulla")
   @RBComment("Text displayed in cancel button")
   public static final String CANCEL_BUTTON = "2";

   @RBEntry("File")
   @RBComment("Label for file field in create document form")
   public static final String FILE_LABEL = "3";

   @RBEntry("Numero")
   @RBComment("Label for number field in create document form")
   public static final String NUMBER_LABEL = "4";

   @RBEntry("Nome")
   @RBComment("Label for name field in create document form")
   public static final String NAME_LABEL = "5";

   @RBEntry("Titolo")
   @RBComment("Label for title field in create document form")
   public static final String TITLE_LABEL = "6";

   @RBEntry("Tipo")
   @RBComment("Label for type field in create document form")
   public static final String TYPE_LABEL = "7";

   @RBEntry("Reparto")
   @RBComment("Label for department field in create document form")
   public static final String DEPARTMENT_LABEL = "8";

   @RBEntry("Descrizione")
   @RBComment("Label for description field in create document form")
   public static final String DESCRIPTION_LABEL = "9";

   @RBEntry("Posizione")
   @RBComment("Label for location field in create document form")
   public static final String LOCATION_LABEL = "10";

   @RBEntry("Ciclo di vita")
   @RBComment("Label for lifecycle field in create document form")
   public static final String LIFECYCLE_LABEL = "11";

   @RBEntry("Team")
   @RBComment("Label for team field in create document form")
   public static final String TEAM_LABEL = "12";

   @RBEntry("Fine")
   @RBComment("Text displayed in finish button, user is done with wizard form")
   public static final String FINISH_BUTTON = "13";

   @RBEntry("Indietro")
   @RBComment("Text displayed in previous button, goes back to previous page in wizard")
   public static final String PREVIOUS_BUTTON = "14";

   @RBEntry("Salva")
   @RBComment("Text displayed in save button, user is done with form continue with upload")
   public static final String SAVE_BUTTON = "15";

   @RBEntry("Campi obbligatori")
   @RBComment("Message stating certain fields are required")
   public static final String REQUIRED_FIELDS = "16";

   @RBEntry("Completare tutti i campi obbligatori.")
   @RBComment("Error message displayed if required fields are not filled in")
   public static final String REQUIRED_FIELDS_WARNING = "17";

   @RBEntry("Creazione di documento in corso")
   @RBComment("Title for the page upload the files")
   public static final String CREATE_DOC_PROCESSING_TITLE = "18";

   @RBEntry("Crea documenti")
   @RBComment("Title for pages in create document wizard")
   public static final String CREATE_DOC_WIZARD_TITLE = "19";

   @RBEntry("Avanti")
   @RBComment("Text displayed in next button, procedes to form for next document")
   public static final String NEXT_BUTTON = "20";

   @RBEntry("Caricamento in corso...")
   @RBComment("Message displayed in the upload applet")
   public static final String UPLOADING_MSG = "21";

   @RBEntry("{0} byte di {1} per {2}.")
   @RBComment("Message showing the number of bytes that have been uploaded for the file compared the files size")
   @RBArgComment0("Number of bytes uploaded for a file")
   @RBArgComment1("Size of a file in bytes")
   @RBArgComment2("The files name")
   public static final String BYTE_UPLOAD_PROGRESS_MSG = "22";

   @RBEntry("Caricamento file {0} di {1}.")
   @RBComment("Message showing the current file out of total number of files that is being uploaded")
   @RBArgComment0("The number of the file currently being upload")
   @RBArgComment1("The total number of files being uploaded")
   public static final String FILE_UPLOAD_PROGRESS_MSG = "23";

   @RBEntry("Documento {0} di {1}:  {2}")
   @RBComment("Message in the wizard that shows the document that current form is for")
   @RBArgComment0("The number of the file in the list of files to be created")
   @RBArgComment1("The total number of files to be created")
   @RBArgComment2("The name of the file to be created")
   public static final String FILE_LIST_MSG = "24";

   @RBEntry("È possibile spostare solo gli oggetti in cartella.")
   @RBComment("Message displayed if a nonfoldered object is moved by drag-and-drop")
   public static final String NONFOLDERED_MOVE_ERROR_MSG = "25";

   @RBEntry("Impossibile trovare la cartella o lo schedario di destinazione.")
   @RBComment("Message displayed if an object is moved to a nonfolder object by drag-and-drop")
   public static final String TARGET_NOT_FOLDER_ERROR_MSG = "26";

   @RBEntry("Solo i documenti possono essere spostati con questo client.")
   @RBComment("Message displayed when trying to move a nonWTDocument object")
   public static final String NONDOCUMENT_MOVE_ERROR_MSG = "27";

   @RBEntry("Avviso: errore durante lo spostamento.")
   @RBComment("Title for page when an error ocurrs when attempting a move.")
   public static final String MOVE_ERROR_TITLE = "28";

   @RBEntry("Trascinare solo un file per aggiornare il documento.")
   @RBComment("Message displayed when too many files are dropped on a document")
   public static final String TOO_MANY_FILES_ERROR_MSG = "29";

   @RBEntry("Impossibile aggiornare il documento indicato.")
   @RBComment("Message displayed when a document cannot be updated")
   public static final String PRIVATE_CONSTANT_0 = "30";

   @RBEntry("Avviso: errore durante l'aggiornamento del documento")
   @RBComment("Title for page when trying to update a document by drag-and-drop is unsuccessful")
   public static final String UPDATE_DOCUMENT_ERROR_TITLE = "31";

   @RBEntry("Trascinare solo un file per aggiornare il documento.")
   @RBComment("Error message given when multiple files are dropped on a document icon though only a drop of a single file is allowed.")
   public static final String MULTI_FILE_DOC_UPDATE_DROP_ERROR = "32";

   @RBEntry("Impossibile aggiornare il documento indicato.")
   @RBComment("General error message given upon attempt to update a document via a file drop and this is found not to be possible.")
   public static final String DOC_CANNOT_BE_UPDATED_ERROR = "33";
}
