/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.doc;

import wt.util.resource.*;

@RBUUID("wt.doc.docResource")
public final class docResource extends WTListResourceBundle {
   @RBEntry("A wt.doc error occurred. System message follows:")
   @RBComment("Format for displaying document information.")
   @RBArgComment0("document number")
   @RBArgComment1("document name")
   @RBArgComment2("document version")
   public static final String GENERAL_ERROR = "0";

   @RBEntry("Could not locate Personal Cabinet for user: \"{0}\".")
   public static final String NO_PERSONAL_CABINET = "1";

   @RBEntry("{0} has been successfully checked-in")
   public static final String SUCCESS_CHECK_IN = "10";

   @RBEntry("The value for the argument \"{0}\" can not be null")
   public static final String NULL_ARGUMENT = "11";

   @RBEntry("Cannot add a relationship to document {0} because it is not checked out by you.")
   public static final String NO_RELATIONSHIP = "12";

   @RBEntry("{0} ({1}) {2}")
   public static final String DOC_DISPLAY_FORMAT = "13";

   @RBEntry("Set Configuration Specification")
   public static final String SET_CONFIG_SPEC_LABEL = "14";

   @RBEntry("References Documents")
   public static final String DEPENDSON_SUBTITLE = "15";

   @RBEntry("Referenced By Documents")
   public static final String HASDEPENDENT_SUBTITLE = "16";

   @RBEntry("Referenced By Parts")
   public static final String PART_REF_SUBTITLE = "17";

   @RBEntry("Comment")
   public static final String LINKDESCRIPTION = "18";

   @RBEntry("Uses")
   public static final String USES_SUBTITLE = "19";

   @RBEntry("Could not locate user's 'Checked Out' folder.")
   public static final String NO_CHECKOUT_FOLDER = "2";

   @RBEntry("Used By")
   public static final String USEDBY_SUBTITLE = "20";

   @RBEntry("Add References")
   @RBComment("Hardcoded in HTML templates to 21.")
   public static final String ADD_DEPENDENCY_LINK = "21";

   @RBEntry("Find Documents for References")
   public static final String FIND_WTDOCUMENT = "22";

   @RBEntry("Search Results for Adding Referenced Documents")
   public static final String SEARCH_RESULTS = "23";

   @RBEntry("Latest Version")
   public static final String LATESTVERSION = "24";

   @RBEntry("Error finding content on {0} when walking document structure.")
   @RBArgComment0("document")
   public static final String STRUCTURE_CONTENT = "25";

   @RBEntry("Error walking the structure of document {0}.")
   @RBArgComment0("document")
   public static final String STRUCTURE_WALKING = "26";

   @RBEntry("Unable to append search criteria to select document based on baseline information.  The referenced baseline may have been deleted.")
   public static final String INVALID_DOC_BASELINE_CONFIG_SPEC = "27";

   @RBEntry("XXXX")
   @RBComment("This string is not used in Windchill right now.")
   public static final String STRUCTURE_SUBTITLE = "28";

   @RBEntry("Class not found {0}.")
   @RBComment("Document type.")
   public static final String CLASS_NOT_FOUND = "29";

   @RBEntry("Value: \"{0}\" is not a valid \"{1}\" value.")
   public static final String INVALID_VALUE = "3";

   @RBEntry("Unable to create document {0}.")
   @RBComment("Document type.")
   public static final String UNABLE_TO_INSTANTIATE = "30";

   @RBEntry("No objects found to create relationship with.")
   public static final String NO_OBJECTS_FOR_RELATIONSHIP = "31";

   @RBEntry("Unable to match documents to comments.")
   public static final String VECTOR_MISMATCH = "32";

   @RBEntry("Add Uses")
   @RBComment("Hardcoded in HTML templates to 33.")
   public static final String ADD_STRUCTURE_LINK = "33";

   @RBEntry("Error setting attribute values.")
   public static final String SETTING_VALUES = "34";

   @RBEntry("Error setting the project {0} on the document.")
   @RBComment("Project")
   public static final String SETTING_PROJECT = "35";

   @RBEntry("Invalid folder location {0}.")
   @RBComment("Folder")
   public static final String INVALID_FOLDER = "36";

   @RBEntry("Error setting lifecycle {0}.")
   @RBComment("Lifecycle.")
   public static final String SETTING_LIFECYCLE = "37";

   @RBEntry("XXXX")
   @RBComment("This string is not used in Windchill right now.")
   public static final String DOC_CREATE_ERROR = "38";

   @RBEntry("Error finding relationship to delete.")
   public static final String NO_RELATIONSHIP_FOR_DELETE = "39";

   @RBEntry("Create of documents of type {0} are not supported")
   public static final String INVALID_DOC_TYPE = "4";

   @RBEntry("Update")
   public static final String UPDATE_URL_LABEL = "40";

   @RBEntry("Error finding relationship to update.")
   public static final String NO_RELATIONSHIP_FOR_UPDATE = "41";

   @RBEntry("Object must a {0} or a subclass for this function to work.")
   @RBComment("object class")
   public static final String OBJECT_WRONG_CLASS = "42";

   @RBEntry("Error doing the customized processing for this function.")
   public static final String DELEGATE_INSTANTIATION = "43";

   @RBEntry("Get Content")
   public static final String GETCONTENT_URL_LABEL = "44";

   @RBEntry("Get Content not supported on {0}.")
   @RBComment("class of object")
   public static final String GETCONTENT_NOTSUPPORTED = "45";

   @RBEntry("Get Content completed successfully.")
   public static final String GETCONTENT_SUCCESS = "46";

   @RBEntry("The checkin was not executed.  You must specify a shared cabinet or the document must be checked out.")
   public static final String NO_CHECKIN_DONE = "47";

   @RBEntry("References for ")
   public static final String REFERENCES_TITLE = "48";

   @RBEntry("Structure Usage for ")
   public static final String USAGE_TITLE = "49";

   @RBEntry("At least one file is required to create a document")
   public static final String FILE_REQUIRED = "5";

   @RBEntry("Structure Hierarchy for ")
   public static final String STRUCT_TITLE = "50";

   @RBEntry("Set Latest Configuration Specification")
   public static final String SET_STD_CSPEC = "51";

   @RBEntry("Set Baseline Configuration Specification")
   public static final String SET_BASE_CSPEC = "52";

   @RBEntry("Include documents in my personal cabinet")
   @RBComment("This string is used in Structure Hierarchy set Baseline Config Spec.")
   public static final String INCLUDE_DOCS = "53";

   @RBEntry("State:")
   @RBComment("This string is used in Structure Hierarchy set Std Config Spec.")
   public static final String STATE_PROMPT = "54";

   @RBEntry("Search...")
   @RBComment("This string is used in Structure Hierarchy set Baseline Config Spec.")
   public static final String SEARCH_BUTTON = "55";

   @RBEntry("Name:")
   @RBComment("This string is used in Structure Hierarchy set Baseline Config Spec.")
   public static final String NAME_FIELD = "56";

   @RBEntry("Latest")
   @RBComment("This string is used in Structure Hierarchy set Std Config Spec.")
   public static final String STD_TAB = "57";

   @RBEntry("Baseline")
   @RBComment("This string is used in Structure Hierarchy set Baseline Config Spec.")
   public static final String BASE_TAB = "58";

   @RBEntry("Use for this session only")
   @RBComment("Structure Hierarchy set Std Config Spec.")
   public static final String THIS_SESSION = "59";

   @RBEntry("{0} has been successfully imported")
   public static final String SUCCESS_IMPORT = "6";

   @RBEntry("  OK  ")
   @RBComment("This string is used in Structure Hierarchy set Std Config Spec.")
   public static final String OKAY_BUTTON = "60";

   @RBEntry("Find Documents for Structure")
   @RBComment("This string is used in creating document structures.")
   public static final String FIND_FOR_STRUCT = "61";

   @RBEntry("Search Results for Adding Structure")
   @RBComment("This string is used in creating document structures.")
   public static final String RESULTS_FOR_STRUCT = "62";

   @RBEntry("Add Selected Items")
   public static final String ADD_SELECTED_BUTTON = "63";

   @RBEntry("More than one wt.doc.WTDocumentConfigSpec returned, when only one was expected")
   public static final String WTDOCCONFIGSPEC_QTY = "64";

   @RBEntry("Downloading primary content...")
   public static final String DOWNLOAD_PRIMARY = "65";

   @RBEntry("No primary content to download.")
   public static final String NO_PRIMARY_DOWNLOAD = "66";

   @RBEntry("Error setting the team {0} on the document.")
   @RBComment("Team Template x")
   public static final String SETTING_TEAMTEMPLATE = "67";

   @RBEntry("The Windchill script method has no value for the \"{0}\" parameter.  This parameter should be set to a valid class name.")
   public static final String NULL_CLASS_PARAMETER = "68";

   @RBEntry("The Windchill script method has the value \"{0}\" for the \"{1}\" parameter.  This is not a valid class.")
   public static final String INVALID_CLASS_PARAMETER = "69";

   @RBEntry("Note: document {0} has been submitted")
   public static final String SUBMIT_SUCCESS = "7";

   @RBEntry("The Windchill script method has no value for the \"{0}\" parameter.")
   public static final String NULL_STRING_PARAMETER = "70";

   @RBEntry("The \"{0}\" key is not valid for the resource bundle indicated on the Windchill script method call.")
   public static final String INVALID_RESOURCEKEY_PARAMETER = "71";

   @RBEntry("Update of {0} failed.  This object is not checked out.")
   public static final String UPDATE_FAILED = "72";

   @RBEntry("User is not permitted to {0}.")
   public static final String NOT_PERMITTED = "73";

   @RBEntry("The context object is not {0}.")
   public static final String OBJECT_NOT = "74";

   @RBEntry("Error initializing \"{0}\".")
   public static final String ERROR_INITIALIZING = "75";

   @RBEntry("{0} and all of its iterations have been successfully deleted.")
   public static final String SUCCESS_DELETE = "76";

   @RBEntry("{0} failed. Action not set in Windchill script call.")
   public static final String NO_ACTION = "77";

   @RBEntry("Cannot move a working copy, {0} is a working copy.")
   public static final String NOT_MOVED = "78";

   @RBEntry("Failed Move of {0} to {1} because of the following error.  {2}")
   @RBArgComment0("Is the identity of the document.")
   @RBArgComment1("The folder or path that the document was attempted to move to.")
   @RBArgComment2("The error message that is passed up from the folder service, should be complete localized sentences.")
   public static final String MOVE_FAILED = "79";

   @RBEntry("Warning: document {0} has not been submitted.  ")
   public static final String SUBMIT_FAILED = "8";

   @RBEntry("Move")
   public static final String MOVE_URL_LABEL = "80";

   @RBEntry("{0} has been successfully moved to {1}.")
   public static final String SUCCESS_MOVE = "81";

   @RBEntry("modify")
   public static final String MODIFY = "82";

   @RBEntry("Revise of {0} failed.")
   public static final String REVISE_FAILED = "83";

   @RBEntry("{0} has been successfully revised")
   public static final String REVISE_SUCCESS = "84";

   @RBEntry("Revise")
   public static final String REVISE_URL_LABEL = "85";

   @RBEntry("{0} is checked out.")
   public static final String CHECKED_OUT = "86";

   @RBEntry("Undo Checkout")
   public static final String UNDOCHECKOUT_URL_LABEL = "87";

   @RBEntry("Failed UndoCheckout on {0}.")
   public static final String FAILED_UNDOCHECKOUT = "88";

   @RBEntry("Undo Checkout has been successfully performed on {0}.")
   public static final String SUCCESS_UNDO_CHECKOUT = "89";

   @RBEntry("{0} is not checked out to you.")
   public static final String NOT_CHECKED_OUT = "9";

   @RBEntry("Rename")
   public static final String RENAME_URL_LABEL = "90";

   @RBEntry("Rename of {0} failed.  Check log for more information.")
   public static final String RENAME_FAILURE = "91";

   @RBEntry("Rename of {0} was successful")
   public static final String RENAME_SUCCESS = "92";

   @RBEntry("delete")
   public static final String DELETE = "93";

   @RBEntry("Delete of {0} failed.  This object is checked out, undo check out first.")
   public static final String DELETE_FAILED = "94";

   @RBEntry("Delete")
   public static final String DELETE_URL_LABEL = "95";

   @RBEntry("No changes were made to document.")
   public static final String RENAME_NO_CHANGE = "96";

   @RBEntry("Are you sure you want to delete document")
   public static final String WANT_TO_DELETE_DOCUMENT = "97";

   @RBEntry("?")
   public static final String QUESTION_MARK = "98";

   @RBEntry("create")
   public static final String CREATE = "99";

   @RBEntry("Error looking for uses relationships for the following document: {0} {1}.")
   @RBComment("Error message for failing in searching for relationships.")
   @RBArgComment0("document name")
   @RBArgComment1("document number")
   public static final String ERROR_FINDING_USES = "100";

   @RBEntry("Error setting the order for a structured document link between the parent document ({0}) and the child document ({1}).")
   @RBComment("In reordering a structured document there was an error setting the order for one of the documents.")
   @RBArgComment0("parent document name")
   @RBArgComment1("child document name")
   public static final String ERROR_STRUCTURE_ORDER = "101";

   @RBEntry("Error ordering the document structure because no link was found between the parent document ({0}) and the child document ({1}).")
   @RBComment("In reordering a structured document one of the links couldn't be found.")
   @RBArgComment0("parent document name")
   @RBArgComment1("child document name")
   public static final String MISSING_USAGE_LINK = "102";

   @RBEntry("Error, can't modify this document's relationships because the document {0} is checked out to {1}.  The operation that you attempted requires you to be able to check out the document.")
   @RBComment("Error message for trying to modify a document's relationships when it is checked out to someone else.")
   @RBArgComment0("Document name.")
   @RBArgComment1("Other user that has the document checked out.")
   public static final String CHECKED_OUT_TO_OTHER = "103";

   @RBEntry("Error, can't modify this document's relationships because you do not have privileges to check out {0}.  The operation that you attempted requires you to be able to check out the document.")
   @RBComment("Error message for trying to modify a document's relationships when the user doesn't have check out access.")
   @RBArgComment0("Document name.")
   public static final String CHECKED_OUT_ERROR = "104";

   @RBEntry("Error, on the checkout of {0} before attempting to modify the document's relationships.  All changes were undone.")
   @RBComment("Error message for checkout failing before modifying reference or usage links on a document.  Entire transaction rolled back.")
   @RBArgComment0("Document name.")
   public static final String ON_CHECKOUT_ERROR = "105";

   @RBEntry("Error, on the checkin of {0} after attempting to modify the document's relationships.  All changes were undone.")
   @RBComment("Error message for checkin failing after modifying reference or usage links on a document.  Entire transaction rolled back.")
   @RBArgComment0("Document name.")
   public static final String ON_CHECKIN_ERROR = "106";

   @RBEntry("Error, no document provided to remove the references relationships from.")
   @RBComment("Error message for null document passed into server method for removing references relationships.")
   public static final String NO_DOCUMENT_FOR_REMOVE_REFERENCES = "107";

   @RBEntry("Error changing the available for use status on the object.")
   @RBComment("Error occurred while changing the enabled flag for the object.The object here is a Document Template. ")
   public static final String SETTING_ENABLED = "108";

   @RBEntry("Error changing the available for use status on the object during revise.")
   @RBComment("Error occurred while changing the enabled flag on revise for the object. The object here is a Document Template.")
   public static final String ENABLEFLAG_ON_REVISE = "109";

   @RBEntry("{0} - {1}")
   @RBPseudo(false)
   @RBComment("Used to populate the template selection drop down list.")
   @RBArgComment0("Type of the template like \"Document\" or \"Reference Document\", can be a customer defined type.")
   @RBArgComment1("Name of the document.")
   public static final String TEMPLATE_SELECTION = "110";

   @RBEntry("Error, duplicate document template name {0} not allowed within the same context.")
   @RBArgComment0("Document Template Name.")
   public static final String DUPNAME_PROHIBITED = "111";

   @RBEntry(" Error getting the latest iteration of the latest version.")
   public static final String LATESTVER_ITER = "112";

   @RBEntry("Enabled ")
   public static final String ENABLED_LABEL = "113";

   @RBEntry("Error: you need to enter values for fields with asterisks by their names.")
   @RBComment("Message that appears when user tries to leave a page without filling in a required (asterisked) field.")
   public static final String MISSING_REQUIRED_FIELD = "114";

   @RBEntry("Please enter a different name. Maximum number of characters allowed for Name: {0} ")
   @RBComment("error message when the name field exceeds the maximum number of characters allowed.")
   @RBArgComment0("maximum number of characters allowed for the Name field.")
   public static final String LONG_NAME_ERROR = "115";

   @RBEntry("Cannot rename document {0} ({1}) because its identity is not unique.")
   @RBComment("error message when a duplicate number is entered.")
   @RBArgComment0("document number")
   @RBArgComment1("document name")
   public static final String NUMBER_UNIQUENESS_EXCEPTION = "116";

   @RBEntry("Delete of {0} failed with the following error: {1}")
   @RBComment("error message when a document cannot be deleted for a reason other than being checked out")
   @RBArgComment0("document identity string")
   @RBArgComment1("error message passed from somewhere else")
   public static final String DELETE_FAILED_MESSAGE = "117";

   @RBEntry("<-- Select A Type -->")
   @RBComment("String to be displayed as default in the type dropdown of document wizards, to prompt the user to select a type.")
   public static final String SELECT_A_TYPE = "118";

   @RBEntry("The top level document version has been changed from {0} to {1} to reflect the configuration specification used.")
   public static final String DOC_VERSION_HAS_CHANGED = "119";

   @RBEntry("The configuration specification identified above did not select a document version of {0}.  Please follow the configuration specification link above to choose a new configuration.")
   public static final String NO_DOC_VERSION_FOUND = "120";

   @RBEntry("A document with the name {0} already exists in this context. Choose a different name or context.")
   public static final String ERROR_DOC_DUP_NAME_CONTEXT = "ERROR_DOC_DUP_NAME_CONTEXT";

   @RBEntry("A document with the name {0} already exists in this folder. Choose a different name or folder.")
   public static final String ERROR_DOC_DUP_NAME_FOLDER = "ERROR_DOC_DUP_NAME_FOLDER";

   @RBEntry("Cannot delete document \"{0}\" because it is referenced by following documents: {1}")
   @RBArgComment0("Display identity of the object being deleted.")
   @RBArgComment1("Display identity of the object which references the deleted object.")
   public static final String CAN_NOT_DELETE_REFERENCED_BY = "121";
}
