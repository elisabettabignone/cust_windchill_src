/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.sandbox;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBArgComment1;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.dataops.sandbox.sandboxResource")
public final class sandboxResource extends WTListResourceBundle {

   @RBEntry("Undo PDM Checkout")
   public static final String UNDO_PDM_CHECKOUT_TASK_NAME = "0";

   @RBEntry("No objects have been selected for this Undo PDM Checkout action.")
   public static final String UNDO_PDM_CHECKOUT_NO_OBJECTS = "1";

   @RBEntry("Object List")
   public static final String ADD_RELATED_OBJECTS_LABEL = "2";

   @RBEntry("Send Objects to PDM")
   public static final String CHECKIN_TITLE = "3";

   @RBEntry("Set Object Attributes")
   public static final String EDIT_OBJECT_LIST_LABEL = "4";

   @RBEntry("All Objects")
   public static final String VIEW_LABEL_ALL_OBJECTS = "6";

   @RBEntry("Problems exist. Please review status columns for more details.")
   public static final String TOP_STATUS_MESSAGE = "7";

   @RBEntry("Location")
   public static final String LOCATION = "8";

    @RBEntry("The new location was not specified for some objects.\nIf you continue, these objects will not be checked in.\nDo you want to continue?")
    @RBComment("Note the embedded \n entries to break the string into three lines.")
   public static final String NO_LOCATION_SPECIFIED_FOR_SOME = "9";

   @RBEntry("A destination PDM context and folder path has not been specified for some objects.")
   public static final String NO_DESTINATION_FOLDER = "90001";

   @RBEntry("There are no objects to be checked in to the PDM context.")
   public static final String NO_OBJECTS_TO_CHECKIN = "10";

   @RBEntry("The destination context, \"{0}\", has been specified but does not exist.")
   public static final String DEST_CONTAINER_DOESNT_EXIST = "11";

   @RBEntry("The destination folder path, \"{0}\", doesn't exist within the context, \"{1}\".")
   public static final String DEST_CONTAINER_FOLDER_PATH_DOESNT_EXIST = "12";

   @RBEntry("Set View")
   public static final String CHECKIN_SET_VIEW = "13";

   @RBEntry("Auto Numbered")
   public static final String CHECKIN_AUTO_NUMBERED = "14";

   @RBEntry("Send Objects to PDM")
   public static final String PDM_CHECKIN_TASK_NAME = "15";

   @RBEntry("No iteration of this dependent object could be retrieved because you do not have access to it.")
   public static final String OBJECT_IS_MASTER_CHECKIN = "16";

   @RBEntry("PDM check-in is not allowed for this object type.")
   public static final String CHECKIN_NOT_ALLOWED_FOR_TYPE = "17";

   @RBEntry("This object is a local working copy.")
   public static final String OBJECT_IS_WORKING_COPY = "18";

   @RBEntry("This object is checked out in its local context.")
   public static final String OBJECT_WIP_CHECKED_OUT = "19";

   @RBEntry("This object is shared to the project.")
   public static final String CHECKIN_NOT_ALLOWED_FOR_SHARED = "20";

   @RBEntry("This object has already been checked in.")
   public static final String CHECKIN_OBJECT_TERMINAL = "21";

   @RBEntry("You do not have access to the version of this object in the PDM system.")
   public static final String NO_ACCESS_TO_PDM_ITERATION = "22";

   @RBEntry("You do not have access to the PDM context to which this object belongs.")
   public static final String NO_ACCESS_TO_PDM_SYSTEM = "23";

   @RBEntry("Unable to complete PDM check-in operation because of errors.")
   public static final String GENERAL_CHECKIN_CONFLICT_MESSAGE = "24";

   @RBEntry("Undo Checkout From PDM System")
   public static final String UNDO_PDM_CHECKOUT_WINDOW_TITLE = "25";

   /**
    * --- start of Add To Project
    *
    **/
   @RBEntry("There are no objects to be added to the project.")
   public static final String NO_OBJECTS_TO_ADDTOPRJ = "26";

    @RBEntry("The new location was not specified for some objects.\nIf you continue, these objects will be added to the default directory of the project.\nDo you want to continue?")
    @RBComment("Note the embedded \n entries to break the string into three lines.")
   public static final String ADDTOPRJ_NO_LOCATION_SPECIFIED_FOR_SOME = "27";

   @RBEntry("Add Objects to a Project")
   public static final String ADD_TO_PROJ_TITLE = "28";

   @RBEntry("Select Project and Location")
   public static final String ADD_TO_PROJ_STEP1_LABEL = "29";

   @RBEntry("Collect Objects and Set Options")
   public static final String ADD_TO_PROJ_STEP2_LABEL = "30";

   /**
    * ---------------------------------------------------------------------
    * table views
    **/
   @RBEntry("Parts")
   public static final String PARTS_VIEW = "31";

   @RBEntry("Documents")
   public static final String DOCUMENTS_VIEW = "32";

   @RBEntry("CAD Documents")
   public static final String CAD_DOCUMENTS_VIEW = "33";

   @RBEntry("Notes")
   public static final String NOTES_VIEW = "330001";

   /**
    * ----------
    **/
   @RBEntry("Share")
   public static final String SHARE_BUTTON_LABEL = "34";

   @RBEntry("PDM Check-out")
   public static final String CHECKOUT_BUTTON_LABEL = "35";

   @RBEntry("You have selected more than one version of some objects. The Add to Project action automatically operates on all versions of all selected objects. Duplicate objects have been removed from the table.")
   public static final String MULTIPLE_VERSIONS_SELECTED = "36";

   @RBEntry("All versions and iterations of the objects will be added to the project.")
   public static final String TOP_INFO_MESSAGE = "37";

   @RBEntry("Add to Project Collection Table")
   public static final String ADDTOPRJ_GATHERING_TABLE = "38";

   @RBEntry("Copy")
   public static final String COPY_BUTTON_LABEL = "39";

   @RBEntry("Please select only Parts, Documents, and CAD Documents.\nOther object types are not valid for this operation.")
   public static final String ILLEGAL_OBJECT_TYPES_SELECTED = "40";

   @RBEntry("*Indicates required fields.")
   @RBComment("The star indicates that a form field is a required one and must be filled")
   public static final String FOOTNOTE_REQ_FIELDS = "41";

   @RBEntry("Action \"{0}\" could not be applied to the object(s):")
   @RBComment("This line is the header for a large message, followed by a list of object identities (name and number, if applicable) that the action can not be applied to.")
   public static final String INTEROP_BAD_OBJECT_TYPES_HEADER = "42";

   @RBEntry("Action \"{0}\" is not allowed on these objects.")
   @RBComment("This line is the footer for a large message. See INTEROP_BAD_OBJECT_TYPES_HEADER comments")
   public static final String INTEROP_BAD_OBJECT_TYPES_FOOTER = "43";

   @RBEntry("PDM Check-in")
   public static final String CHECKIN_BUTTON_LABEL = "44";

   @RBEntry("Keep Check-out")
   public static final String KEEP_CHECKOUT_BUTTON_LABEL = "45";

   @RBEntry("*Name")
   public static final String NEW_NAME_LABEL = "46";

   @RBEntry("*Number")
   public static final String NEW_NUM_LABEL = "47";

   @RBEntry("*File Name")
   @RBComment("The header text for the filename column in the Send to PDM Wizard Table Step 2.")
   public static final String NEW_CAD_LABEL = "48";

   @RBEntry("View")
   @RBComment("The header text for the new view column in the Send to PDM Wizard Table Step 2.")
   public static final String NEW_VIEW_LABEL = "49";

   @RBEntry("Name")
   @RBComment("The column title used in custom table view wizard for Send to PDM Wizard Table Step 2.")
   public static final String NAME_LABEL = "NAME_LABEL";

   @RBEntry("Number")
   @RBComment("The column title used in custom table view wizard for Send to PDM Wizard Table Step 2.")
   public static final String NUMBER_LABEL = "NUMBER_LABEL";

   @RBEntry("File Name")
   @RBComment("The column title used in custom table view wizard for Send to PDM Wizard Table Step 2.")
   public static final String FILE_NAME_LABEL = "FILE_NAME_LABEL";


   @RBEntry("Conflict")
   @RBComment("tooltip for conflict column in the Send to PDM Wizard Table Step 2.")
   public static final String SEND_TO_PDM_CONFLICT = "SEND_TO_PDM_CONFLICT";

   @RBEntry("Status Messages")
   @RBComment("Tooltip for Status messages column in the Send to PDM Wizard Table Step 2.")
   public static final String SEND_TO_PDM_STATUS_MESSAGES = "SEND_TO_PDM_STATUS_MESSAGES";

   @RBEntry("Send to PDM")
   public static final String SEND_TO_PDM_TITLE = "50";

   @RBEntry("Settings for New Objects")
   public static final String SEND_TO_PDM_STEP2_LABEL = "51";

   @RBEntry("PDM Location")
   @RBComment("This label is a header for a table column. The values will be the context and folder location of the object in PDM.")
   public static final String PDM_LOCATION_LABEL = "52";

   @RBEntry("The new location was not specified for some objects.\nPlease set the location for all objects and then try again.")
   @RBComment("Note the embedded \n entries to break the string into two lines.")
   public static final String SENDTOPDM_NO_LOCATION_SPECIFIED_FOR_SOME = "53";

   @RBEntry("This object is shared to the project and can not be checked-in to PDM.")
   public static final String SHARED_CHECKIN_NOT_ALLOWD = "54";

   @RBEntry("Exclude")
   public static final String EXCLUDE_OBJ = "55";

   @RBEntry("Set New Location is not allowed for this object.")
   public static final String SET_LOC_INVALID = "56";

   @RBEntry("The new location was not specified for some objects.\nPlease set the location for all objects and then try again.")
   @RBComment("Note the embedded \n entries to break the string into two lines.")
   public static final String DEST_LOC_DOESNT_EXIST = "57";

   @RBEntry("Set New Location")
   public static final String SET_OBJ_LOCATION = "58";

   @RBEntry("Objects successfully sent to PDM: {0}")
   public static final String OBJECTS_CHECKED_IN_MSG = "59";

   @RBEntry("Objects successfully checked-out to project {0}: {1}")
   public static final String OBJECTS_CHECKED_OUT_MSG = "60";

   @RBEntry("Objects successfully shared to project {0}: {1}")
   public static final String OBJECTS_SHARED_MSG = "61";

   @RBEntry("Objects successfully copied to project {0}: {1}")
   public static final String OBJECTS_COPIED_MSG = "62";

   @RBEntry("Objects successfully pasted to project {0}: {1}")
   public static final String OBJECTS_PASTED_MSG = "63";

   @RBEntry("Update Project")
   public static final String UPDATE_PRJ_TITLE = "64";

   @RBEntry("Updated Version")
   public static final String UPDATED_VERSION_LABEL = "65";

   @RBEntry("Project Version")
   public static final String PROJECT_VERSION_LABEL = "66";

   @RBEntry("There are no objects to update.")
   public static final String NO_OBJECTS_TO_UPDATE = "67";

   @RBEntry("Set Refresh")
   public static final String SET_REFRESH_LAB = "68";

   @RBEntry("This object is already shared to the project and can only be updated.")
   public static final String SETLOC_NOT_ALLOWED_FOR_SHARED = "69";

   @RBEntry("This action is not allowed for objects that already exist in the project.")
   public static final String SETLOC_NOT_ALLOWED_FOR_ITEM = "70";

   @RBEntry("Dynamic Documents")
   public static final String DYNAMIC_DOCUMENTS_VIEW = "71";

   @RBEntry("Find...")
   public static final String FIND_BUTTON_LABEL = "72";

   @RBEntry("Browse...")
   public static final String BROWSE_BUTTON_LABEL = "73";

   @RBEntry("An unknown error occurred.")
   public static final String SANDBOX_VALIDATION_ERROR = "74";

   @RBEntry("The following objects were ignored:")
   public static final String SANDBOX_IGNORE_ITEMS = "75";

   @RBEntry("Object with name \"{0}\" and number \"{1}\" is already shared to the project.")
   public static final String SANDBOX_IGNORE_SHARED_ITEM = "76";

   @RBEntry("Object with name \"{0}\" and number \"{1}\" is already checked out to the project.")
   public static final String SANDBOX_IGNORE_CHECKEDOUT_ITEM = "77";

   @RBEntry("Object with name \"{0}\" and number \"{1}\" is deprecated in the project.")
   public static final String SANDBOX_IGNORE_SUPERSEDED_ITEM = "77_1";

   @RBEntry("CONFIRMATION: Ineligible objects selected.\nAn object(s) you selected is ineligible for Add to Project operation, or you do not have access to the object(s). Please click OK to continue, ineligible objects will be ignored.")
   public static final String ADD_TO_PROJECT_SOME_VALID_OBJS = "78";

   @RBEntry("FAILURE: Ineligible objects selected.\nThe object(s) you selected is ineligible for Add to Project, or you do not have access to the object(s).")
   public static final String ADD_TO_PROJECT_ALL_INVALID_OBJS = "79";

   @RBEntry("CONFIRMATION: Ineligible objects selected.\nOnly accessible objects that have been checked out from PDM or newly created in the project and are not associated to any deprecated or abandoned objects are eligible for send to PDM. Please click OK to continue, ineligible objects will be ignored.")
   public static final String SEND_TO_PDM_SOME_VALID_OBJS = "80";

   @RBEntry("FAILURE: Ineligible objects selected.\nOnly accessible objects that have been checked out from PDM or newly created in the project and are not associated to any deprecated or abandoned objects are eligible for send to PDM.")
   public static final String SEND_TO_PDM_ALL_INVALID_OBJS = "81";

   @RBEntry("FAILURE: Ineligible objects selected.\nOnly one PDM checked-out version can be sent to PDM at a time.")
   public static final String CANNOT_SB_CHECKIN_MULTIPLE_ONEOFF_VERSIONS_OF_SAME_OBJECT = "CANNOT_SB_CHECKIN_MULTIPLE_ONEOFF_VERSIONS_OF_SAME_OBJECT";
   @RBEntry("The new number was not specified for some objects.\nPlease set the number for all objects and then try again.")
   public static final String SENDTOPDM_NO_NUMBER_SPECIFIED_FOR_SOME = "82";

   @RBEntry("The new name was not specified for some objects.\nPlease set the name for all objects and then try again.")
   public static final String SENDTOPDM_NO_NAME_SPECIFIED_FOR_SOME = "83";

   @RBEntry("The new file name was not specified for some objects.\nPlease set the file name for all objects and then try again.")
   public static final String SENDTOPDM_NO_CAD_FILENAME_SPECIFIED_FOR_SOME = "84";

   @RBEntry("CONFIRMATION: Undo PDM Check-out. Do you wish to undo the PDM Check-out of the selected object(s)? You will lose all the changes made in the project. ")
   public static final String SANDBOX_CONFIRM_UNDO_CHK_OUT_MSG = "85";

   @RBEntry("CONFIRMATION: Ineligible objects selected.\nDo you wish to undo the PDM Check-out of the eligible selected object(s)? You will lose all the changes made in the project. Please click OK to continue, ineligible objects will be ignored.")
   public static final String UNDO_PDM_CHECKOUT_SOME_VALID_OBJS = "86";

   @RBEntry("FAILURE: Ineligible objects selected. Only accessible objects that have been checked-out from PDM are eligible for Undo PDM Check-out.")
   public static final String UNDO_PDM_CHECKOUT_ALL_INVALID_OBJS = "87";

   @RBEntry("Checked Out from PDM")
   public static final String WORKS = "88";

   @RBEntry("CONFIRMATION: Ineligible objects selected.\nOnly accessible objects that have been shared from PDM to project are eligible for Convert to PDM Check-out. Please click OK to continue, ineligible objects will be ignored.")
   public static final String CONVERT_TO_PDM_CHECKOUT_SOME_VALID_OBJS = "91";

   @RBEntry("FAILURE: Ineligible objects selected.\nOnly accessible objects that have been shared from PDM to project are eligible for Convert to PDM Check-out.")
   public static final String CONVERT_TO_PDM_CHECKOUT_ALL_INVALID_OBJS = "92";

   @RBEntry("CONFIRMATION: Ineligible objects selected.\nOnly accessible objects that have been shared to project are eligible for Remove Share. Please click OK to continue, ineligible objects will be ignored.")
   public static final String REMOVE_SHARE_SOME_VALID_OBJS = "REMOVE_SHARE_SOME_VALID_OBJS";

   @RBEntry("FAILURE: Ineligible objects selected.\nOnly accessible objects that have been shared to project are eligible for Remove Share.")
   public static final String REMOVE_SHARE_ALL_INVALID_OBJS = "REMOVE_SHARE_ALL_INVALID_OBJS";

   @RBEntry("Removing the share of this object may cause a failure opening related objects in their applications. Do you want to proceed?")
   public static final String REMOVE_SHARE_CAD_DOC_CONFIRM_MSG = "93";

   @RBEntry("Removing the shares of the selected CAD documents may cause a failure opening related objects in their applications. Do you want to proceed?")
   public static final String REMOVE_SHARE_CAD_DOCS_CONFIRM_MSG = "94";

   @RBEntry("Nothing selected.")
   public static final String NOTHING_SELECTED = "95";

   @RBEntry("You do not have access to perform this operation.")
   public static final String NO_ACCESS_TO_OPERATION = "96";

   @RBEntry("Send to PDM Collection Table")
   @RBComment("This is the title for the Send to PDM collection of related objects wizard step")
   public static final String SENDTOPDM_GATHERING_TABLE = "97";

   @RBEntry("This object is already shared to this project; however, the baselined iteration is not the latest. You must update to the latest iteration before this object can be checked out.")
   public static final String OBJECT_IS_OUTDATED_SHARE = "98";

   /**
    * fix SPR 1421732, user can't paste an older revision of a change object
    **/
   @RBEntry("Paste not allowed for the older version of this object ")
   public static final String ADD_TO_PROJECT_INVALID_OBJ_VERSION = "99";

   @RBEntry("CONFIRMATION: Ineligible objects selected.\\nOnly checked in Deprecated objects are eligible to be Converted to Shares. Click OK to continue, ineligible objects will be ignored.")
   @RBComment("Displayed to the user when they select some invalid objects to Convert to Share. The operation may proceed for the valid objects.")
   public static final String CONVERT_TO_SHARE_SOME_VALID_OBJS = "CONVERT_TO_SHARE_SOME_VALID_OBJS";

   @RBEntry("FAILURE: Ineligible objects selected.\\nOnly checked in deprecated objects are eligible to be converted to shares.")
   @RBComment("Displayed to the user when they select all invalid objects to Convert to Share. The operation is aborted.")
   public static final String CONVERT_TO_SHARE_ALL_INVALID_OBJS = "CONVERT_TO_SHARE_ALL_INVALID_OBJS";

   @RBEntry("Convert to Share")
   @RBComment("This is the title for the Convert to Share wizard.")
   public static final String CONVERT_TO_SHARE_TITLE = "CONVERT_TO_SHARE_TITLE";

   @RBEntry("Collect Objects")
   @RBComment("This is the title for the table in the Convert to Share wizard.")
   public static final String CONVERT_TO_SHARE_TABLE_TITLE = "CONVERT_TO_SHARE_TABLE_TITLE";

   @RBEntry("Convert to Share")
   @RBComment("This is the action name displayed in the Selected Action column of the Convert to Share wizard.")
   public static final String CONVERT_TO_SHARE_OBJ = "CONVERT_TO_SHARE_OBJ";

   @RBEntry("Selected Action")
   @RBComment("This is used as a column name in the Convert to Share collector table.")
   public static final String SELECTED_ACTION = "SELECTED_ACTION";

   @RBEntry("Invalid object to be converted to share")
   @RBComment("This is used in the Convert to Share table to tell the user the object is invalid for the operation.")
   public static final String CONVERT_TO_SHARE_INVALID_OBJ = "CONVERT_TO_SHARE_INVALID_OBJ";

   @RBEntry("There are no objects to convert to shares.")
   @RBComment("This is displayed in the Convert to Share wizard when there are no objects to convert.")
   public static final String NO_OBJECTS_TO_CONVERT = "NO_OBJECTS_TO_CONVERT";

   @RBEntry("Modified deprecated objects and family table instances cannot be excluded.")
   @RBComment("This is displayed in the Convert to Share wizard when a user tries to exclude an invalid object.")
   public static final String CONVERT_TO_SHARE_INVALID_TO_EXCLUDE = "CONVERT_TO_SHARE_INVALID_TO_EXCLUDE";

   @RBEntry("Convert Objects to Shares")
   @RBComment("This is the task name for the Convert to Share operation.")
   public static final String CONVERT_TO_SHARE_TASK_NAME = "CONVERT_TO_SHARE_TASK_NAME";

   @RBEntry("Objects successfully converted and shared back to the project: {0}")
   public static final String OBJECTS_CONVERTED_MSG = "OBJECTS_CONVERTED_MSG";

   @RBEntry("WARNING:  All accessible objects are in sync with the project filter.")
   @RBComment("Displayed to the user when there are no out-of-sync shared objects in the project.")
   public static final String UPDATE_PROJECT_NO_SEEDS_AVAILABLE = "UPDATE_PROJECT_NO_SEEDS_AVAILABLE";

   @RBEntry("CONFIRMATION: Ineligible objects selected.\nOnly shared objects that are in the project's sandbox baseline are eligible to be updated. Click OK to continue, ineligible objects will be ignored.")
   @RBComment("Displayed to the user when they select some invalid objects to update. The operation may proceed for the valid objects.")
   public static final String UPDATE_SHARE_SOME_VALID_OBJS = "UPDATE_SHARE_SOME_VALID_OBJS";

   @RBEntry("FAILURE: Ineligible objects selected.\nOnly shared objects that are out-of-sync with the project filter are eligible to be updated.")
   @RBComment("Displayed to the user when they select all invalid objects to update. The operation is aborted.")
   public static final String UPDATE_SHARE_ALL_INVALID_OBJS = "UPDATE_SHARE_ALL_INVALID_OBJS";

   @RBEntry("Update Selected Shares")
   @RBComment("This is the title for the Update Selected Shares wizard and the table.")
   public static final String UPDATE_SHARE_TITLE = "UPDATE_SHARE_TITLE";

   @RBEntry("Cannot convert {0} to share. It requires {1}.")
   @RBComment("Error message shown when a required modified dependent exists, but is not included for conversion to share.")
   @RBArgComment0("Identity of the source object")
   @RBArgComment1("Identity of the required dependent object")
   public static final String MUST_CONVERT_MODIFIED_REQUIRED_DEPENDENT = "MUST_CONVERT_MODIFIED_REQUIRED_DEPENDENT";

   @RBEntry("Collect Objects and Set Options")
   @RBComment("The name of the Send TO PDM Step 1 table.")
   public static final String SEND_TO_PDM_STEP1_TABLE_NAME = "SEND_TO_PDM_STEP1_TABLE_NAME";

   /**
    * ---------------------------------
    **/
   @RBEntry("Send To PDM Step 2")
   @RBComment("The name of the Send TO PDM Step 2 table.")
   public static final String SEND_TO_PDM_STEP2_TABLE_NAME = "SEND_TO_PDM_STEP2_TABLE_NAME";

   /**
    * --------------------------------
    * Project Revision table view
    **/
   @RBEntry("All")
   public static final String PRIVATE_CONSTANT_0 = "ALL";

   @RBEntry("All Objects")
   public static final String PRIVATE_CONSTANT_1 = "ALL_VIEW_DESC";

   @RBEntry("Version")
   public static final String PRIVATE_CONSTANT_2 = "IOP_VERSION_COLUMN";

   @RBEntry("Status")
   public static final String PRIVATE_CONSTANT_3 = "IOP_STATUS_COLUMN";

   @RBEntry("Last Modified")
   public static final String PRIVATE_CONSTANT_4 = "MODIFIED_ON_COLUMN";

   @RBEntry("Project Name")
   public static final String PRIVATE_CONSTANT_5 = "PROJECT_COLUMN";

   @RBEntry("Folder Path")
   public static final String PRIVATE_CONSTANT_6 = "FOLDER_COLUMN";

   @RBEntry("View Information")
   public static final String PRIVATE_CONSTANT_7 = "INFO_COLUMN";

   @RBEntry("Status Icon")
   public static final String PRIVATE_CONSTANT_8 = "ICON_COLUMN";

   @RBEntry("Actions")
   public static final String PRIVATE_CONSTANT_9 = "IOP_ACTIONS_COLUMN";

   @RBEntry("New Location")
   @RBComment("The header text for the new location column in the Send to PDM Wizard Table Step 2.")
   public static final String SENDTOPDMSTEP2_LOCATION = "SENDTOPDMSTEP2_LOCATION";

   @RBEntry("Set Object Attributes")
   @RBComment("Send to PDM Wizard Table Step 2.")
   public static final String SENDTOPDMSTEP2_TABLE_NAME = "SENDTOPDMSTEP2_TABLE_NAME";

   @RBEntry("Dependent")
   @RBComment("Send To PDM Step 1 Collection Rule column/cell value ")
   public static final String SEND_TO_PDM_DEPENDANT_COLLECTION_RULE = "SEND_TO_PDM_DEPENDANT_COLLECTION_RULE";

   @RBEntry("Initially Selected")
   @RBComment("Send To PDM Step 1 Collection Rule column/cell value ")
   public static final String SEND_TO_PDM_INITIAL_SEL_COLLECTION_RULE = "SEND_TO_PDM_INITIAL_SEL_COLLECTION_RULE";

   @RBEntry("Related Objects ({0}) - ")
   @RBComment("Send To PDM Step 1 Collection Rule tooltip prefix")
   public static final String SEND_TO_PDM_COLLECTION_RULE_TOOLTIP_PREFIX = "SEND_TO_PDM_COLLECTION_RULE_TOOLTIP_PREFIX";

   @RBEntry("\n Included as dependent for: ")
   @RBComment("Send To PDM Step 1 Collection Rule tooltip text")
   public static final String SEND_TO_PDM_COLLECTION_RULE_TOOLTIP_TEXT = "SEND_TO_PDM_COLLECTION_RULE_TOOLTIP_TEXT";

   @RBEntry("Collection Rule")
   @RBComment("Send To PDM Step 1 Collection Rule column title/heading")
   public static final String SEND_TO_PDM_COLLECTION_RULE = "SEND_TO_PDM_COLLECTION_RULE";

   @RBEntry("PDM Location")
   @RBComment("Send To PDM Step 1 PDM Location column title/heading")
   public static final String SEND_TO_PDM_PDM_LOCATION = "SEND_TO_PDM_PDM_LOCATION";

   @RBEntry("CAD Documents in conflict")
   @RBComment("The title for the CAD Documents identity conflicts report table")
   public static final String IDENTITY_CONFLICTS_REPORT_TITLE = "IDENTITY_CONFLICTS_REPORT_TITLE";

    @RBEntry("This table lists the CAD documents that are in your project for which a CAD document of the same category with the same file name exists in PDM. ")
    @RBComment("This is the message that shows above the table in the identify conflict UI.")
    public static final String IDENTITY_CONFLICTS_REPORT_HELP_MESSAGE = "IDENTITY_CONFLICTS_REPORT_HELP_MESSAGE";

   @RBEntry("Object Identity")
   @RBComment("The title for the object identity column in the identity conflicts report table for CAD Documents")
   public static final String IDENTITY_CONFLICTS_OBJECT_IDENTITY = "IDENTITY_CONFLICTS_OBJECT_IDENTITY";

   @RBEntry("waiting for input from PD")
   public static final String FOUND_CONFLICTS = "FOUND_CONFLICTS";

   @RBEntry("Warning: If a project object is replaced with a PDM object, the CAD structure using the project object may not open correctly in the CAD application.\n\nAre you sure you want to replace the selected object(s)?")
   @RBComment("This warning message is shown when the user invokes the Replace Project Object With PDM Object action.")
   public static final String REPLACE_PROJ_OBJ_CONFIRMATION = "REPLACE_PROJ_OBJ_CONFIRMATION";

   @RBEntry("CONFIRMATION: Ineligible objects selected.\nOnly stand-alone CAD Parts and CAD Formats are eligible for replacement. Please click OK to continue, ineligible objects will be ignored.")
   @RBComment("This message is displayed to the user when they select some valid objects for the Replace Project Object action. The operation proceeded for the valid objects.")
   public static final String REPLACE_PROJ_OBJ_SOME_INVALID_OBJS = "REPLACE_PROJ_OBJ_SOME_INVALID_OBJS";

   @RBEntry("FAILURE: Ineligible objects selected. Only stand-alone CAD Parts and CAD Formats are eligible for replacement.")
   @RBComment("This message is displayed to the user when they select no valid objects for the Replace Project Object action. The operation is aborted.")
   public static final String REPLACE_PROJ_OBJ_ALL_INVALID_OBJS = "REPLACE_PROJ_OBJ_ALL_INVALID_OBJS";

   @RBEntry("Failure: You do not have proper permission to perform this action.")
   public static final String INSUFFICIENT_PERMISSIONS_MSG = "INSUFFICIENT_PERMISSIONS_MSG";

   @RBEntry("An object with the same file name exists in PDM.")
   @RBComment("Send To PDM CAD name conflict tool tip message.")
   public static final String SEND_TO_PDM_CAD_NAME_CONFLICT = "SEND_TO_PDM_CAD_NAME_CONFLICT";

   @RBEntry("An object with the same number already exist in PDM.")
   @RBComment("Send To PDM CAD number conflict tool tip message.")
   public static final String SEND_TO_PDM_NUMBER_CONFLICT = "SEND_TO_PDM_NUMBER_CONFLICT";

   @RBEntry("An object with the same number and file name exists in PDM.")
   @RBComment("Send To PDM Number and CAD name conflict tool tip message.")
   public static final String SEND_TO_PDM_NUMBER_CAD_NAME_CONFLICT = "SEND_TO_PDM_NUMBER_CAD_NAME_CONFLICT";

   @RBEntry("You don't have proper permission to send this object to PDM")
   @RBComment("Send To PDM improper permission tool tip message.")
   public static final String SEND_TO_PDM_IMPROPER_PERMISION = "SEND_TO_PDM_IMPROPER_PERMISION";

   @RBEntry("The destination has not been set.")
   @RBComment("Send To PDM missing destination error message.")
   public static final String SEND_TO_PDM_MISSING_DESTINATION = "SEND_TO_PDM_MISSING_DESTINATION";

   @RBEntry("Failure: The Following required dependents cannot be removed without removing their parents.")
   @RBComment("Send To PDM error message.")
   public static final String SEND_TO_PDM_CANNOT_REMOVE_FROM_TABLE = "SEND_TO_PDM_CANNOT_REMOVE_FROM_TABLE";

   @RBEntry("File name \'{0}\' has an invalid extention \'{1}\'. It should be \'{2}\' ")
   @RBComment("Send To PDM  invalid file extention error message.")
   public static final String SEND_TO_PDM_INVALID_CAD_NAME_EXTENSION = "SEND_TO_PDM_INVALID_CAD_NAME_EXTENSION";

   @RBEntry("File name \'{0}\' contains more than one \'.\' characters.")
   @RBComment("Send To PDM  invalid file extention error message.")
   public static final String SEND_TO_PDM_CAD_NAME_MULTI_EXTENSION_FOUND = "SEND_TO_PDM_CAD_NAME_MULTI_EXTENSION_FOUND";

   @RBEntry("Failure: The new location was not specified for some items. \\n\\nPlease correct the issues for all objects and then try again.")
   @RBComment("Send To PDM destination not set summary error message.")
   public static final String SEND_TO_PDM_MISSING_DESTINATION_SUMMARY = "SEND_TO_PDM_MISSING_DESTINATION_SUMMARY";

   @RBEntry("The new location could not be found for this item. \n\nMore information found in log files.")
   @RBComment("Send To PDM destination not set row error message.")
   public static final String SEND_TO_PDM_FOLDER_COULD_NOT_BE_DETERMINED = "SEND_TO_PDM_FOLDER_COULD_NOT_BE_DETERMINED";

   @RBEntry("Uniqueness exception found for some items\\n\\nPlease correct the issues for all objects and then try again.")
   @RBComment("Send To PDM general error message.")
   public static final String SEND_TO_PDM_UNIQUENESS_ERROR = "SEND_TO_PDM_UNIQUENESS_ERROR";

   @RBEntry("Error processing item.\n\nAdditional information can be found in log files.")
   @RBComment("Send To PDM general row error message.")
   public static final String SEND_TO_PDM_GENERAL_ROW_PROCESSING_ERROR = "SEND_TO_PDM_GENERAL_ROW_PROCESSING_ERROR";

   @RBEntry("Error processing item(s).\\n\\nAdditional information can be found in log files.")
   @RBComment("Send To PDM general error message.")
   public static final String SEND_TO_PDM_GENERAL_FORM_PROCESSING_ERROR = "SEND_TO_PDM_GENERAL_FORM_PROCESSING_ERROR";

   @RBEntry("Invalid item type for Send To PDM action. ")
   @RBComment("Send To PDM invalid object type error message.")
   public static final String SEND_TO_PDM_INVALID_OBJECT_TYPE = "SEND_TO_PDM_INVALID_OBJECT_TYPE";

   @RBEntry("The checkout destination folder could not be found. \n\nMore information found in log files.")
   @RBComment("Send To PDM destination folder not found for the keep checked out option.")
   public static final String SEND_TO_PDM_FOLDER_NOT_FOUND = "SEND_TO_PDM_FOLDER_NOT_FOUND";

    @RBEntry("View Information")
    @RBComment("Tool tip for the information column in the Manage Identity Conflicts table.")
    public static final String INFO_COLUMN_HEADER                           = "INFO_COLUMN_HEADER";

    @RBEntry("Modified By")
    @RBComment("Column header for the modified-by column in the Manage Identity Conflicts table.")
    public static final String MODIFIED_BY_COLUMN_HEADER                    = "MODIFIED_BY_COLUMN_HEADER";

    @RBEntry("ATTENTION: The selected objects must be from the same container for this action.")
    @RBComment("Error message displayed to the user when they select objects from different containers for the Undo PDM Checkout action.")
    public static final String NOT_SAME_CONTAINER = "NOT_SAME_CONTAINER";

    @RBEntry("Remove Shares")
    public static final String REMOVE_SHARE_TASK_NAME = "REMOVE_SHARE_TASK_NAME";

    @RBEntry("No objects have been selected for this Remove Shares action.")
    public static final String REMOVE_SHARE_NO_OBJECTS = "REMOVE_SHARE_NO_OBJECTS";

    @RBEntry("Cannot add one or more of the WTDocument or WTParts  to {0}  because their names and numbers are not unique in {0}. The duplicate names are {1}")
    @RBComment("AddToProject or PDM checkout,failed because  the target project already has WTDocument/WTPart with the same name and number as the objects to be shared/PDM checkout")
    public static final String DUP_NAME_NUMBER_IN_PROJ = "DUP_NAME_NUMBER_IN_PROJ";
}