/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.sandbox;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBArgComment1;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.dataops.sandbox.sandboxResource")
public final class sandboxResource_it extends WTListResourceBundle {

   @RBEntry("Annulla Check-Out PDM")
   public static final String UNDO_PDM_CHECKOUT_TASK_NAME = "0";

   @RBEntry("Nessun oggetto selezionato per l'azione di Annulla Check-Out PDM.")
   public static final String UNDO_PDM_CHECKOUT_NO_OBJECTS = "1";

   @RBEntry("Elenco oggetti")
   public static final String ADD_RELATED_OBJECTS_LABEL = "2";

   @RBEntry("Invia oggetti al PDM")
   public static final String CHECKIN_TITLE = "3";

   @RBEntry("Imposta attributi oggetto")
   public static final String EDIT_OBJECT_LIST_LABEL = "4";

   @RBEntry("Tutti gli oggetti")
   public static final String VIEW_LABEL_ALL_OBJECTS = "6";

   @RBEntry("È stato rilevato un problema. Consultare le colonne di stato per ulteriori dettagli.")
   public static final String TOP_STATUS_MESSAGE = "7";

   @RBEntry("Posizione")
   public static final String LOCATION = "8";

    @RBEntry("Nuova posizione non specificata per alcuni oggetti.\nSe si continua, tali oggetti non saranno sottoposti a Check-In.\nContinuare?")
    @RBComment("Note the embedded \n entries to break the string into three lines.")
   public static final String NO_LOCATION_SPECIFIED_FOR_SOME = "9";

   @RBEntry("Non sono stati specificati un contesto PDM di destinazione e un percorso di cartella per alcuni oggetti.")
   public static final String NO_DESTINATION_FOLDER = "90001";

   @RBEntry("Nessun oggetto da sottoporre a Check-In nel contesto PDM.")
   public static final String NO_OBJECTS_TO_CHECKIN = "10";

   @RBEntry("Il contesto di destinazione specificato \"{0}\" non esiste.")
   public static final String DEST_CONTAINER_DOESNT_EXIST = "11";

   @RBEntry("Cartella di destinazione \"{0}\" inesistente nel contesto \"{1}\".")
   public static final String DEST_CONTAINER_FOLDER_PATH_DOESNT_EXIST = "12";

   @RBEntry("Imposta vista")
   public static final String CHECKIN_SET_VIEW = "13";

   @RBEntry("Numerato automaticamente")
   public static final String CHECKIN_AUTO_NUMBERED = "14";

   @RBEntry("Invia oggetti al PDM")
   public static final String PDM_CHECKIN_TASK_NAME = "15";

   @RBEntry("Impossibile recuperare iterazioni dell'oggetto dipendente. Non si dispone dei diritti di accesso richiesti.")
   public static final String OBJECT_IS_MASTER_CHECKIN = "16";

   @RBEntry("Check-In PDM non consentito per questo tipo di oggetto.")
   public static final String CHECKIN_NOT_ALLOWED_FOR_TYPE = "17";

   @RBEntry("L'oggetto è una copia in modifica locale.")
   public static final String OBJECT_IS_WORKING_COPY = "18";

   @RBEntry("L'oggetto è stato sottoposto a Check-Out nel relativo contesto locale.")
   public static final String OBJECT_WIP_CHECKED_OUT = "19";

   @RBEntry("L'oggetto è condiviso con il progetto.")
   public static final String CHECKIN_NOT_ALLOWED_FOR_SHARED = "20";

   @RBEntry("L'oggetto è già stato sottoposto a Check-In.")
   public static final String CHECKIN_OBJECT_TERMINAL = "21";

   @RBEntry("L'utente non dispone di diritto d'accesso alla versione di questo oggetto nel sistema PDM.")
   public static final String NO_ACCESS_TO_PDM_ITERATION = "22";

   @RBEntry("Non si dispone dei permessi di accesso per il contesto PDM a cui appartiene l'oggetto.")
   public static final String NO_ACCESS_TO_PDM_SYSTEM = "23";

   @RBEntry("Impossibile completare l'operazione di Check-In in PDM a causa di errori.")
   public static final String GENERAL_CHECKIN_CONFLICT_MESSAGE = "24";

   @RBEntry("Annulla Check-Out dal sistema PDM")
   public static final String UNDO_PDM_CHECKOUT_WINDOW_TITLE = "25";

   /**
    * --- start of Add To Project
    *
    **/
   @RBEntry("Nessun oggetto da aggiungere al progetto.")
   public static final String NO_OBJECTS_TO_ADDTOPRJ = "26";

    @RBEntry("Non è stata specificata la nuova posizione di alcuni oggetti.\nSe si continua, questi oggetti verranno aggiunti alla directory di default del progetto.\nContinuare?")
    @RBComment("Note the embedded \n entries to break the string into three lines.")
   public static final String ADDTOPRJ_NO_LOCATION_SPECIFIED_FOR_SOME = "27";

   @RBEntry("Aggiungi oggetti a un progetto")
   public static final String ADD_TO_PROJ_TITLE = "28";

   @RBEntry("Seleziona progetto e posizione")
   public static final String ADD_TO_PROJ_STEP1_LABEL = "29";

   @RBEntry("Raccogli oggetti e imposta opzioni")
   public static final String ADD_TO_PROJ_STEP2_LABEL = "30";

   /**
    * ---------------------------------------------------------------------
    * table views
    **/
   @RBEntry("Parti")
   public static final String PARTS_VIEW = "31";

   @RBEntry("Documenti")
   public static final String DOCUMENTS_VIEW = "32";

   @RBEntry("Documenti CAD")
   public static final String CAD_DOCUMENTS_VIEW = "33";

   @RBEntry("Note")
   public static final String NOTES_VIEW = "330001";

   /**
    * ----------
    **/
   @RBEntry("Condividi")
   public static final String SHARE_BUTTON_LABEL = "34";

   @RBEntry("Check-Out PDM")
   public static final String CHECKOUT_BUTTON_LABEL = "35";

   @RBEntry("Sono state selezionate più versioni di alcuni oggetti. L'azione Aggiungi a progetto ha effetto automaticamente su tutte le versioni degli oggetti selezionati. Gli oggetti duplicati sono stati rimossi dalla tabella.")
   public static final String MULTIPLE_VERSIONS_SELECTED = "36";

   @RBEntry("Tutte le versioni e iterazioni degli oggetti verranno aggiunti al progetto.")
   public static final String TOP_INFO_MESSAGE = "37";

   @RBEntry("Aggiungi a tabella di raccolta progetto")
   public static final String ADDTOPRJ_GATHERING_TABLE = "38";

   @RBEntry("Copia")
   public static final String COPY_BUTTON_LABEL = "39";

   @RBEntry("Selezionare solo parti, documenti e documenti CAD.\nGli altri tipi di oggetto non sono validi per questa operazione.")
   public static final String ILLEGAL_OBJECT_TYPES_SELECTED = "40";

   @RBEntry("* Indica i campi obbligatori.")
   @RBComment("The star indicates that a form field is a required one and must be filled")
   public static final String FOOTNOTE_REQ_FIELDS = "41";

   @RBEntry("Impossibile applicare l'azione \"{0}\" a uno o più oggetti:")
   @RBComment("This line is the header for a large message, followed by a list of object identities (name and number, if applicable) that the action can not be applied to.")
   public static final String INTEROP_BAD_OBJECT_TYPES_HEADER = "42";

   @RBEntry("Azione \"{0}\" non consentita su questi oggetti.")
   @RBComment("This line is the footer for a large message. See INTEROP_BAD_OBJECT_TYPES_HEADER comments")
   public static final String INTEROP_BAD_OBJECT_TYPES_FOOTER = "43";

   @RBEntry("Check-In PDM")
   public static final String CHECKIN_BUTTON_LABEL = "44";

   @RBEntry("Mantieni in stato di Check-Out")
   public static final String KEEP_CHECKOUT_BUTTON_LABEL = "45";

   @RBEntry("*Nome")
   public static final String NEW_NAME_LABEL = "46";

   @RBEntry("*Numero")
   public static final String NEW_NUM_LABEL = "47";

   @RBEntry("*Nome file")
   @RBComment("The header text for the filename column in the Send to PDM Wizard Table Step 2.")
   public static final String NEW_CAD_LABEL = "48";

   @RBEntry("Visualizza")
   @RBComment("The header text for the new view column in the Send to PDM Wizard Table Step 2.")
   public static final String NEW_VIEW_LABEL = "49";

   @RBEntry("Nome")
   @RBComment("The column title used in custom table view wizard for Send to PDM Wizard Table Step 2.")
   public static final String NAME_LABEL = "NAME_LABEL";

   @RBEntry("Numero")
   @RBComment("The column title used in custom table view wizard for Send to PDM Wizard Table Step 2.")
   public static final String NUMBER_LABEL = "NUMBER_LABEL";

   @RBEntry("Nome file")
   @RBComment("The column title used in custom table view wizard for Send to PDM Wizard Table Step 2.")
   public static final String FILE_NAME_LABEL = "FILE_NAME_LABEL";


   @RBEntry("Conflitto")
   @RBComment("tooltip for conflict column in the Send to PDM Wizard Table Step 2.")
   public static final String SEND_TO_PDM_CONFLICT = "SEND_TO_PDM_CONFLICT";

   @RBEntry("Messaggi di stato")
   @RBComment("Tooltip for Status messages column in the Send to PDM Wizard Table Step 2.")
   public static final String SEND_TO_PDM_STATUS_MESSAGES = "SEND_TO_PDM_STATUS_MESSAGES";

   @RBEntry("Invia a PDM")
   public static final String SEND_TO_PDM_TITLE = "50";

   @RBEntry("Impostazioni per nuovi oggetti")
   public static final String SEND_TO_PDM_STEP2_LABEL = "51";

   @RBEntry("Posizione PDM")
   @RBComment("This label is a header for a table column. The values will be the context and folder location of the object in PDM.")
   public static final String PDM_LOCATION_LABEL = "52";

   @RBEntry("Non è stata specificata la nuova posizione per alcuni oggetti.\nDefinire la posizione per tutti gli oggetti e riprovare.")
   @RBComment("Note the embedded \n entries to break the string into two lines.")
   public static final String SENDTOPDM_NO_LOCATION_SPECIFIED_FOR_SOME = "53";

   @RBEntry("Questo oggetto è condiviso nel progetto e non può essere sottoposto a Check-In nel PDM.")
   public static final String SHARED_CHECKIN_NOT_ALLOWD = "54";

   @RBEntry("Escludi")
   public static final String EXCLUDE_OBJ = "55";

   @RBEntry("Non è consentita l'impostazione di una nuova posizione per l'oggetto.")
   public static final String SET_LOC_INVALID = "56";

   @RBEntry("Non è stata specificata la nuova posizione per alcuni oggetti.\nDefinire la posizione per tutti gli oggetti e riprovare.")
   @RBComment("Note the embedded \n entries to break the string into two lines.")
   public static final String DEST_LOC_DOESNT_EXIST = "57";

   @RBEntry("Imposta nuova posizione")
   public static final String SET_OBJ_LOCATION = "58";

   @RBEntry("Oggetti inviati al PDM: {0}")
   public static final String OBJECTS_CHECKED_IN_MSG = "59";

   @RBEntry("Oggetti sottoposti a Check-Out nel progetto {0}: {1}")
   public static final String OBJECTS_CHECKED_OUT_MSG = "60";

   @RBEntry("Oggetti condivisi nel progetto {0}: {1}")
   public static final String OBJECTS_SHARED_MSG = "61";

   @RBEntry("Oggetti copiati nel progetto {0}: {1}")
   public static final String OBJECTS_COPIED_MSG = "62";

   @RBEntry("Oggetti incollati nel progetto {0}: {1}")
   public static final String OBJECTS_PASTED_MSG = "63";

   @RBEntry("Aggiorna progetto")
   public static final String UPDATE_PRJ_TITLE = "64";

   @RBEntry("Versione aggiornata")
   public static final String UPDATED_VERSION_LABEL = "65";

   @RBEntry("Versione progetto")
   public static final String PROJECT_VERSION_LABEL = "66";

   @RBEntry("Nessun oggetto da aggiornare.")
   public static final String NO_OBJECTS_TO_UPDATE = "67";

   @RBEntry("Imposta aggiornamento")
   public static final String SET_REFRESH_LAB = "68";

   @RBEntry("Questo oggetto è già condiviso nel progetto e può essere solo aggiornato.")
   public static final String SETLOC_NOT_ALLOWED_FOR_SHARED = "69";

   @RBEntry("Questa azione non è consentita per gli oggetti già presenti nel progetto.")
   public static final String SETLOC_NOT_ALLOWED_FOR_ITEM = "70";

   @RBEntry("Documenti dinamici")
   public static final String DYNAMIC_DOCUMENTS_VIEW = "71";

   @RBEntry("Trova...")
   public static final String FIND_BUTTON_LABEL = "72";

   @RBEntry("Sfoglia...")
   public static final String BROWSE_BUTTON_LABEL = "73";

   @RBEntry("Si è verificato un errore sconosciuto")
   public static final String SANDBOX_VALIDATION_ERROR = "74";

   @RBEntry("I seguenti oggetti sono stati ignorati:")
   public static final String SANDBOX_IGNORE_ITEMS = "75";

   @RBEntry("L'oggetto con nome \"{0}\" e numero \"{1}\" è già condiviso nel progetto.")
   public static final String SANDBOX_IGNORE_SHARED_ITEM = "76";

   @RBEntry("L'oggetto con nome \"{0}\" e numero \"{1}\" è già sottoposto a Check-Out nel progetto.")
   public static final String SANDBOX_IGNORE_CHECKEDOUT_ITEM = "77";

   @RBEntry("L'oggetto con nome \"{0}\" e numero \"{1}\" è obsoleto nel progetto.")
   public static final String SANDBOX_IGNORE_SUPERSEDED_ITEM = "77_1";

   @RBEntry("CONFERMA: sono stati selezionati oggetti non validi.\nUno o più fra gli oggetti selezionati non sono validi per l'operazione Aggiungi a progetto, o non si dispone del livello di accesso necessario agli oggetti. Fare clic su OK per continuare. Gli oggetti non validi verranno ignorati.")
   public static final String ADD_TO_PROJECT_SOME_VALID_OBJS = "78";

   @RBEntry("ERRORE: sono stati selezionati oggetti non validi.\nGli oggetti selezionati non sono validi per l'operazione Aggiungi a progetto, o non si dispone del livello di accesso necessario agli oggetti.")
   public static final String ADD_TO_PROJECT_ALL_INVALID_OBJS = "79";

   @RBEntry("CONFERMA: sono stati selezionati oggetti non validi.\nSoltanto gli oggetti accessibili sottoposti a Check-Out da PDM o appena creati nel progetto e non associati a oggetti obsoleti o abbandonati sono validi per l'azione Invia a PDM. Fare clic su OK per continuare. Gli oggetti non validi verranno ignorati.")
   public static final String SEND_TO_PDM_SOME_VALID_OBJS = "80";

   @RBEntry("ERRORE: sono stati selezionati oggetti non validi.\nSoltanto gli oggetti accessibili sottoposti a Check-Out da PDM o appena creati nel progetto e non associati a oggetti obsoleti o abbandonati sono validi per l'azione Invia a PDM. ")
   public static final String SEND_TO_PDM_ALL_INVALID_OBJS = "81";

   @RBEntry("ERRORE: sono stati selezionati oggetti non validi.\nÈ possibile inviare a PDM una sola Versione sottoposta a Check-Out da PDM alla volta.")
   public static final String CANNOT_SB_CHECKIN_MULTIPLE_ONEOFF_VERSIONS_OF_SAME_OBJECT = "CANNOT_SB_CHECKIN_MULTIPLE_ONEOFF_VERSIONS_OF_SAME_OBJECT";
   @RBEntry("Per alcuni oggetti non è stato specificato il nuovo numero.\nImpostare il numero per tutti gli oggetti e riprovare.")
   public static final String SENDTOPDM_NO_NUMBER_SPECIFIED_FOR_SOME = "82";

   @RBEntry("Per alcuni oggetti non è stato specificato il nuovo nome.\nImpostare il nome per tutti gli oggetti e riprovare.")
   public static final String SENDTOPDM_NO_NAME_SPECIFIED_FOR_SOME = "83";

   @RBEntry("Per alcuni oggetti non è stato specificato il nuovo nome file.\nImpostare il nome file per tutti gli oggetti e riprovare.")
   public static final String SENDTOPDM_NO_CAD_FILENAME_SPECIFIED_FOR_SOME = "84";

   @RBEntry("CONFERMA: annullamento del Check-Out PDM. Annullare il Check-Out PDM degli oggetti selezionati? In caso affermativo, verranno perse tutte le modifiche apportate al progetto.")
   public static final String SANDBOX_CONFIRM_UNDO_CHK_OUT_MSG = "85";

   @RBEntry("CONFERMA: sono stati selezionati oggetti non validi.\nAnnullare il Check-Out PDM degli oggetti selezionati? In caso affermativo, verranno perse tutte le modifiche apportate al progetto. Fare clic su OK per continuare. Gli oggetti non validi verranno ignorati.")
   public static final String UNDO_PDM_CHECKOUT_SOME_VALID_OBJS = "86";

   @RBEntry("ERRORE: sono stati selezionati oggetti non validi. Soltanto gli oggetti accessibili sottoposti a Check-Out da PDM sono validi per l'azione Annulla Check-Out PDM.")
   public static final String UNDO_PDM_CHECKOUT_ALL_INVALID_OBJS = "87";

   @RBEntry("Sottoposto a Check-Out da PDM")
   public static final String WORKS = "88";

   @RBEntry("CONFERMA: sono stati selezionati oggetti non validi.\nSoltanto gli oggetti accessibili condivisi da PDM nel progetto sono validi per la conversione a Check-Out da PDM. Fare clic su OK per continuare. Gli oggetti non validi verranno ignorati.")
   public static final String CONVERT_TO_PDM_CHECKOUT_SOME_VALID_OBJS = "91";

   @RBEntry("ERRORE: sono stati selezionati oggetti non validi.\nSoltanto gli oggetti accessibili condivisi da PDM nel progetto sono validi per la conversione a Check-Out da PDM.")
   public static final String CONVERT_TO_PDM_CHECKOUT_ALL_INVALID_OBJS = "92";

   @RBEntry("CONFERMA: sono stati selezionati oggetti non validi.\nSoltanto gli oggetti accessibili condivisi nel progetto sono validi per l'azione Rimuovi condivisione. Fare clic su OK per continuare. Gli oggetti non validi verranno ignorati.")
   public static final String REMOVE_SHARE_SOME_VALID_OBJS = "REMOVE_SHARE_SOME_VALID_OBJS";

   @RBEntry("ERRORE: sono stati selezionati oggetti non validi.\nSoltanto gli oggetti accessibili condivisi nel progetto sono validi per l'azione Rimuovi condivisione.")
   public static final String REMOVE_SHARE_ALL_INVALID_OBJS = "REMOVE_SHARE_ALL_INVALID_OBJS";

   @RBEntry("La rimozione della condivisione dell'oggetto può impedire l'apertura degli oggetti correlati nelle rispettive applicazioni. Continuare?")
   public static final String REMOVE_SHARE_CAD_DOC_CONFIRM_MSG = "93";

   @RBEntry("La rimozione delle condivisioni dei documenti CAD selezionati potrebbe impedire l'apertura degli oggetti correlati nelle rispettive applicazioni. Continuare?")
   public static final String REMOVE_SHARE_CAD_DOCS_CONFIRM_MSG = "94";

   @RBEntry("Nessun elemento selezionato.")
   public static final String NOTHING_SELECTED = "95";

   @RBEntry("L'utente non dispone dei permessi per eseguire l'operazione.")
   public static final String NO_ACCESS_TO_OPERATION = "96";

   @RBEntry("Tabella di raccolta invio a PDM")
   @RBComment("This is the title for the Send to PDM collection of related objects wizard step")
   public static final String SENDTOPDM_GATHERING_TABLE = "97";

   @RBEntry("L'oggetto è già condiviso nel progetto ma l'iterazione utilizzata come baseline non è la più recente. Per effettuare il Check-Out dell'oggetto è prima necessario eseguire l'aggiornamento all'iterazione più recente.")
   public static final String OBJECT_IS_OUTDATED_SHARE = "98";

   /**
    * fix SPR 1421732, user can't paste an older revision of a change object
    **/
   @RBEntry("Operazione Incolla non consentita per le versioni precedenti di questo oggetto.")
   public static final String ADD_TO_PROJECT_INVALID_OBJ_VERSION = "99";

   @RBEntry("CONFERMA: sono stati selezionati oggetti non validi.\\nÈ possibile convertire in oggetti condivisi soltanto gli oggetti obsoleti sottoposti a Check-In. Fare clic su OK per continuare. Gli oggetti non validi verranno ignorati.")
   @RBComment("Displayed to the user when they select some invalid objects to Convert to Share. The operation may proceed for the valid objects.")
   public static final String CONVERT_TO_SHARE_SOME_VALID_OBJS = "CONVERT_TO_SHARE_SOME_VALID_OBJS";

   @RBEntry("ERRORE: sono stati selezionati oggetti non validi.\\nÈ possibile convertire in oggetti condivisi soltanto gli oggetti obsoleti sottoposti a Check-In.")
   @RBComment("Displayed to the user when they select all invalid objects to Convert to Share. The operation is aborted.")
   public static final String CONVERT_TO_SHARE_ALL_INVALID_OBJS = "CONVERT_TO_SHARE_ALL_INVALID_OBJS";

   @RBEntry("Converti in oggetto condiviso")
   @RBComment("This is the title for the Convert to Share wizard.")
   public static final String CONVERT_TO_SHARE_TITLE = "CONVERT_TO_SHARE_TITLE";

   @RBEntry("Raccogli oggetti")
   @RBComment("This is the title for the table in the Convert to Share wizard.")
   public static final String CONVERT_TO_SHARE_TABLE_TITLE = "CONVERT_TO_SHARE_TABLE_TITLE";

   @RBEntry("Converti in oggetto condiviso")
   @RBComment("This is the action name displayed in the Selected Action column of the Convert to Share wizard.")
   public static final String CONVERT_TO_SHARE_OBJ = "CONVERT_TO_SHARE_OBJ";

   @RBEntry("Azione selezionata")
   @RBComment("This is used as a column name in the Convert to Share collector table.")
   public static final String SELECTED_ACTION = "SELECTED_ACTION";

   @RBEntry("Oggetto non valido per la conversione in oggetto condiviso")
   @RBComment("This is used in the Convert to Share table to tell the user the object is invalid for the operation.")
   public static final String CONVERT_TO_SHARE_INVALID_OBJ = "CONVERT_TO_SHARE_INVALID_OBJ";

   @RBEntry("Nessun oggetto da convertire in oggetto condiviso.")
   @RBComment("This is displayed in the Convert to Share wizard when there are no objects to convert.")
   public static final String NO_OBJECTS_TO_CONVERT = "NO_OBJECTS_TO_CONVERT";

   @RBEntry("Impossibile escludere istanze di family table e oggetti obsoleti modificati.")
   @RBComment("This is displayed in the Convert to Share wizard when a user tries to exclude an invalid object.")
   public static final String CONVERT_TO_SHARE_INVALID_TO_EXCLUDE = "CONVERT_TO_SHARE_INVALID_TO_EXCLUDE";

   @RBEntry("Converti oggetti in oggetti condivisi")
   @RBComment("This is the task name for the Convert to Share operation.")
   public static final String CONVERT_TO_SHARE_TASK_NAME = "CONVERT_TO_SHARE_TASK_NAME";

   @RBEntry("Oggetti convertiti e condivisi nel progetto: {0}")
   public static final String OBJECTS_CONVERTED_MSG = "OBJECTS_CONVERTED_MSG";

   @RBEntry("AVVERTENZA: tutti gli oggetti accessibili sono sincronizzati con il filtro del progetto.")
   @RBComment("Displayed to the user when there are no out-of-sync shared objects in the project.")
   public static final String UPDATE_PROJECT_NO_SEEDS_AVAILABLE = "UPDATE_PROJECT_NO_SEEDS_AVAILABLE";

   @RBEntry("CONFERMA: sono stati selezionati oggetti non validi.\nÈ possibile aggiornare soltanto gli oggetti condivisi presenti nella baseline del sandbox del progetto. Fare clic su OK per continuare. Gli oggetti non validi verranno ignorati.")
   @RBComment("Displayed to the user when they select some invalid objects to update. The operation may proceed for the valid objects.")
   public static final String UPDATE_SHARE_SOME_VALID_OBJS = "UPDATE_SHARE_SOME_VALID_OBJS";

   @RBEntry("ERRORE: sono stati selezionati oggetti non validi.\nÈ possibile aggiornare soltanto gli oggetti condivisi non sincronizzati con il filtro del progetto.")
   @RBComment("Displayed to the user when they select all invalid objects to update. The operation is aborted.")
   public static final String UPDATE_SHARE_ALL_INVALID_OBJS = "UPDATE_SHARE_ALL_INVALID_OBJS";

   @RBEntry("Aggiorna oggetti condivisi selezionati")
   @RBComment("This is the title for the Update Selected Shares wizard and the table.")
   public static final String UPDATE_SHARE_TITLE = "UPDATE_SHARE_TITLE";

   @RBEntry("Impossibile convertire {0} in un oggetto condiviso. È necessario {1}.")
   @RBComment("Error message shown when a required modified dependent exists, but is not included for conversion to share.")
   @RBArgComment0("Identity of the source object")
   @RBArgComment1("Identity of the required dependent object")
   public static final String MUST_CONVERT_MODIFIED_REQUIRED_DEPENDENT = "MUST_CONVERT_MODIFIED_REQUIRED_DEPENDENT";

   @RBEntry("Raccogli oggetti e imposta opzioni")
   @RBComment("The name of the Send TO PDM Step 1 table.")
   public static final String SEND_TO_PDM_STEP1_TABLE_NAME = "SEND_TO_PDM_STEP1_TABLE_NAME";

   /**
    * ---------------------------------
    **/
   @RBEntry("Invia a PDM - Passo 2")
   @RBComment("The name of the Send TO PDM Step 2 table.")
   public static final String SEND_TO_PDM_STEP2_TABLE_NAME = "SEND_TO_PDM_STEP2_TABLE_NAME";

   /**
    * --------------------------------
    * Project Revision table view
    **/
   @RBEntry("Tutto")
   public static final String PRIVATE_CONSTANT_0 = "ALL";

   @RBEntry("Tutti gli oggetti")
   public static final String PRIVATE_CONSTANT_1 = "ALL_VIEW_DESC";

   @RBEntry("Versione")
   public static final String PRIVATE_CONSTANT_2 = "IOP_VERSION_COLUMN";

   @RBEntry("Stato")
   public static final String PRIVATE_CONSTANT_3 = "IOP_STATUS_COLUMN";

   @RBEntry("Data ultima modifica")
   public static final String PRIVATE_CONSTANT_4 = "MODIFIED_ON_COLUMN";

   @RBEntry("Nome progetto")
   public static final String PRIVATE_CONSTANT_5 = "PROJECT_COLUMN";

   @RBEntry("Percorso della cartella")
   public static final String PRIVATE_CONSTANT_6 = "FOLDER_COLUMN";

   @RBEntry("Visualizza informazioni")
   public static final String PRIVATE_CONSTANT_7 = "INFO_COLUMN";

   @RBEntry("Icona di stato")
   public static final String PRIVATE_CONSTANT_8 = "ICON_COLUMN";

   @RBEntry("Azioni")
   public static final String PRIVATE_CONSTANT_9 = "IOP_ACTIONS_COLUMN";

   @RBEntry("Nuova posizione")
   @RBComment("The header text for the new location column in the Send to PDM Wizard Table Step 2.")
   public static final String SENDTOPDMSTEP2_LOCATION = "SENDTOPDMSTEP2_LOCATION";

   @RBEntry("Imposta attributi oggetto")
   @RBComment("Send to PDM Wizard Table Step 2.")
   public static final String SENDTOPDMSTEP2_TABLE_NAME = "SENDTOPDMSTEP2_TABLE_NAME";

   @RBEntry("Dipendente")
   @RBComment("Send To PDM Step 1 Collection Rule column/cell value ")
   public static final String SEND_TO_PDM_DEPENDANT_COLLECTION_RULE = "SEND_TO_PDM_DEPENDANT_COLLECTION_RULE";

   @RBEntry("Selezionato in origine")
   @RBComment("Send To PDM Step 1 Collection Rule column/cell value ")
   public static final String SEND_TO_PDM_INITIAL_SEL_COLLECTION_RULE = "SEND_TO_PDM_INITIAL_SEL_COLLECTION_RULE";

   @RBEntry("Oggetti correlati ({0}) - ")
   @RBComment("Send To PDM Step 1 Collection Rule tooltip prefix")
   public static final String SEND_TO_PDM_COLLECTION_RULE_TOOLTIP_PREFIX = "SEND_TO_PDM_COLLECTION_RULE_TOOLTIP_PREFIX";

   @RBEntry("\n Inclusa come dipendente per: ")
   @RBComment("Send To PDM Step 1 Collection Rule tooltip text")
   public static final String SEND_TO_PDM_COLLECTION_RULE_TOOLTIP_TEXT = "SEND_TO_PDM_COLLECTION_RULE_TOOLTIP_TEXT";

   @RBEntry("Regola di raccolta")
   @RBComment("Send To PDM Step 1 Collection Rule column title/heading")
   public static final String SEND_TO_PDM_COLLECTION_RULE = "SEND_TO_PDM_COLLECTION_RULE";

   @RBEntry("Posizione PDM")
   @RBComment("Send To PDM Step 1 PDM Location column title/heading")
   public static final String SEND_TO_PDM_PDM_LOCATION = "SEND_TO_PDM_PDM_LOCATION";

   @RBEntry("Documenti CAD in conflitto")
   @RBComment("The title for the CAD Documents identity conflicts report table")
   public static final String IDENTITY_CONFLICTS_REPORT_TITLE = "IDENTITY_CONFLICTS_REPORT_TITLE";

    @RBEntry("Questa tabella elenca i documenti CAD nel progetto per cui esiste già in PDM un documento CAD della stessa categoria e con lo stesso nome.")
    @RBComment("This is the message that shows above the table in the identify conflict UI.")
    public static final String IDENTITY_CONFLICTS_REPORT_HELP_MESSAGE = "IDENTITY_CONFLICTS_REPORT_HELP_MESSAGE";

   @RBEntry("Identificativo oggetto")
   @RBComment("The title for the object identity column in the identity conflicts report table for CAD Documents")
   public static final String IDENTITY_CONFLICTS_OBJECT_IDENTITY = "IDENTITY_CONFLICTS_OBJECT_IDENTITY";

   @RBEntry("waiting for input from PD")
   public static final String FOUND_CONFLICTS = "FOUND_CONFLICTS";

   @RBEntry("Avvertenza: se un oggetto del progetto viene sostituito con un oggetto PDM, è possibile che la struttura CAD, che utilizza l'oggetto del progetto, non venga aperta correttamente nell'applicazione CAD.\n\nSostituire gli oggetti selezionati?")
   @RBComment("This warning message is shown when the user invokes the Replace Project Object With PDM Object action.")
   public static final String REPLACE_PROJ_OBJ_CONFIRMATION = "REPLACE_PROJ_OBJ_CONFIRMATION";

   @RBEntry("CONFERMA: sono stati selezionati oggetti non validi.\nÈ possibile sostituire soltanto formati e parti CAD indipendenti. Fare clic su OK per continuare. Gli oggetti non validi verranno ignorati.")
   @RBComment("This message is displayed to the user when they select some valid objects for the Replace Project Object action. The operation proceeded for the valid objects.")
   public static final String REPLACE_PROJ_OBJ_SOME_INVALID_OBJS = "REPLACE_PROJ_OBJ_SOME_INVALID_OBJS";

   @RBEntry("ERRORE: sono stati selezionati oggetti non validi. È possibile sostituire soltanto formati e parti CAD indipendenti.")
   @RBComment("This message is displayed to the user when they select no valid objects for the Replace Project Object action. The operation is aborted.")
   public static final String REPLACE_PROJ_OBJ_ALL_INVALID_OBJS = "REPLACE_PROJ_OBJ_ALL_INVALID_OBJS";

   @RBEntry("Errore: l'utente non dispone dei permessi appropriati per eseguire l'azione.")
   public static final String INSUFFICIENT_PERMISSIONS_MSG = "INSUFFICIENT_PERMISSIONS_MSG";

   @RBEntry("Esiste già un oggetto con lo stesso nome file in PDM.")
   @RBComment("Send To PDM CAD name conflict tool tip message.")
   public static final String SEND_TO_PDM_CAD_NAME_CONFLICT = "SEND_TO_PDM_CAD_NAME_CONFLICT";

   @RBEntry("Esiste già un oggetto con lo stesso numero in PDM.")
   @RBComment("Send To PDM CAD number conflict tool tip message.")
   public static final String SEND_TO_PDM_NUMBER_CONFLICT = "SEND_TO_PDM_NUMBER_CONFLICT";

   @RBEntry("Esiste già un oggetto con lo stesso numero e lo stesso nome file in PDM.")
   @RBComment("Send To PDM Number and CAD name conflict tool tip message.")
   public static final String SEND_TO_PDM_NUMBER_CAD_NAME_CONFLICT = "SEND_TO_PDM_NUMBER_CAD_NAME_CONFLICT";

   @RBEntry("Non si dispone dei permessi necessari per inviare l'oggetto a PDM")
   @RBComment("Send To PDM improper permission tool tip message.")
   public static final String SEND_TO_PDM_IMPROPER_PERMISION = "SEND_TO_PDM_IMPROPER_PERMISION";

   @RBEntry("Destinazione non impostata.")
   @RBComment("Send To PDM missing destination error message.")
   public static final String SEND_TO_PDM_MISSING_DESTINATION = "SEND_TO_PDM_MISSING_DESTINATION";

   @RBEntry("Errore: impossibile rimuovere i dipendenti necessari che seguono senza rimuovere i relativi padri.")
   @RBComment("Send To PDM error message.")
   public static final String SEND_TO_PDM_CANNOT_REMOVE_FROM_TABLE = "SEND_TO_PDM_CANNOT_REMOVE_FROM_TABLE";

   @RBEntry("L'estensione \\'{1}\\' nel nome file \\'{0}\\' non è valida. Versione corretta: \\'{2}\\'.")
   @RBComment("Send To PDM  invalid file extention error message.")
   public static final String SEND_TO_PDM_INVALID_CAD_NAME_EXTENSION = "SEND_TO_PDM_INVALID_CAD_NAME_EXTENSION";

   @RBEntry("Il nome file \\'{0}\\' contiene più di un carattere \\'.\\'.")
   @RBComment("Send To PDM  invalid file extention error message.")
   public static final String SEND_TO_PDM_CAD_NAME_MULTI_EXTENSION_FOUND = "SEND_TO_PDM_CAD_NAME_MULTI_EXTENSION_FOUND";

   @RBEntry("Errore: la nuova posizione non è stata specificata per alcuni elementi. \\n\\nRisolvere il problema per tutti gli oggetti e riprovare.")
   @RBComment("Send To PDM destination not set summary error message.")
   public static final String SEND_TO_PDM_MISSING_DESTINATION_SUMMARY = "SEND_TO_PDM_MISSING_DESTINATION_SUMMARY";

   @RBEntry("Impossibile trovare la nuova posizione per l'elemento. \n\nUlteriori informazioni sono disponibili nei file di log.")
   @RBComment("Send To PDM destination not set row error message.")
   public static final String SEND_TO_PDM_FOLDER_COULD_NOT_BE_DETERMINED = "SEND_TO_PDM_FOLDER_COULD_NOT_BE_DETERMINED";

   @RBEntry("Rilevata eccezione di univocità per alcuni elementi.\\n\\nRisolvere il problema per tutti gli oggetti e riprovare.")
   @RBComment("Send To PDM general error message.")
   public static final String SEND_TO_PDM_UNIQUENESS_ERROR = "SEND_TO_PDM_UNIQUENESS_ERROR";

   @RBEntry("Errore durante l'elaborazione dell'elemento.\n\nUlteriori informazioni sono disponibili nei file di log.")
   @RBComment("Send To PDM general row error message.")
   public static final String SEND_TO_PDM_GENERAL_ROW_PROCESSING_ERROR = "SEND_TO_PDM_GENERAL_ROW_PROCESSING_ERROR";

   @RBEntry("Errore durante l'elaborazione degli elementi.\\n\\nUlteriori informazioni sono disponibili nei file di log.")
   @RBComment("Send To PDM general error message.")
   public static final String SEND_TO_PDM_GENERAL_FORM_PROCESSING_ERROR = "SEND_TO_PDM_GENERAL_FORM_PROCESSING_ERROR";

   @RBEntry("Tipo di elemento non valido per l'azione di invio a PDM.")
   @RBComment("Send To PDM invalid object type error message.")
   public static final String SEND_TO_PDM_INVALID_OBJECT_TYPE = "SEND_TO_PDM_INVALID_OBJECT_TYPE";

   @RBEntry("Impossibile trovare la cartella di destinazione del Check-Out. \n\nUlteriori informazioni sono disponibili nei file di log.")
   @RBComment("Send To PDM destination folder not found for the keep checked out option.")
   public static final String SEND_TO_PDM_FOLDER_NOT_FOUND = "SEND_TO_PDM_FOLDER_NOT_FOUND";

    @RBEntry("Visualizza informazioni")
    @RBComment("Tool tip for the information column in the Manage Identity Conflicts table.")
    public static final String INFO_COLUMN_HEADER                           = "INFO_COLUMN_HEADER";

    @RBEntry("Autore modifiche")
    @RBComment("Column header for the modified-by column in the Manage Identity Conflicts table.")
    public static final String MODIFIED_BY_COLUMN_HEADER                    = "MODIFIED_BY_COLUMN_HEADER";

    @RBEntry("ATTENZIONE: per questa azione selezionare oggetti dello stesso contenitore.")
    @RBComment("Error message displayed to the user when they select objects from different containers for the Undo PDM Checkout action.")
    public static final String NOT_SAME_CONTAINER = "NOT_SAME_CONTAINER";

    @RBEntry("Rimuovi condivisioni")
    public static final String REMOVE_SHARE_TASK_NAME = "REMOVE_SHARE_TASK_NAME";

    @RBEntry("Nessun oggetto selezionato per l'azione Rimuovi condivisioni.")
    public static final String REMOVE_SHARE_NO_OBJECTS = "REMOVE_SHARE_NO_OBJECTS";

    @RBEntry("Impossibile aggiungere uno o più WTDocument o WTPart a {0}, poiché i rispettivi nomi e numeri non sono univoci in {0}. I nomi duplicati sono {1}")
    @RBComment("AddToProject or PDM checkout,failed because  the target project already has WTDocument/WTPart with the same name and number as the objects to be shared/PDM checkout")
    public static final String DUP_NAME_NUMBER_IN_PROJ = "DUP_NAME_NUMBER_IN_PROJ";
}
