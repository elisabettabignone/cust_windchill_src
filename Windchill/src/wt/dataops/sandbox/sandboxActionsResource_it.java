/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.sandbox;

import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBNameException;
import wt.util.resource.RBPseudo;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.dataops.sandbox.sandboxActionsResource")
@RBNameException //Grandfathered by conversion
public final class sandboxActionsResource_it extends WTListResourceBundle {
   /**
    * The format of the entries in the rbinfo file are almost identical to those in the
    * action.properties. The only thing to change is that all the keys MUST end with .value.
    * Also comments are extremely important for each entry so that the localizers know
    * a) where and how the text will be used and b)if the text should be translated at all.
    * In the case of the moreurlinfo and the icon, we would not want the localizers to translate
    * the values for those entries.
    * SB actions
    * Entries for Sandbox Add to Project
    **/
   @RBEntry("Aggiungi a progetto")
   @RBComment("Add to Project")
   public static final String PRIVATE_CONSTANT_0 = "sandbox.SBAddToPrj.description";

   @RBEntry("Aggiunge gli oggetti al progetto")
   @RBComment("Add Objects to the Project")
   public static final String PRIVATE_CONSTANT_1 = "sandbox.SBAddToPrj.tooltip";

   @RBEntry("proj_addto.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_2 = "sandbox.SBAddToPrj.icon";

   /**
    * Entries for Sandbox Send to PDM row-level action
    **/
   @RBEntry("Invia a PDM")
   @RBComment("Send to PDM row-level action")
   public static final String SENDTOPDM_ROW_TITLE = "sandbox.SBSendToPdm.title";

   @RBEntry("Invia a PDM")
   @RBComment("Send to PDM row-level action")
   public static final String PRIVATE_CONSTANT_3 = "sandbox.SBSendToPdm.description";

   @RBEntry("Invia gli oggetti selezionati al sistema PDM")
   @RBComment("Send Selected Objects to PDM System")
   public static final String PRIVATE_CONSTANT_4 = "sandbox.SBSendToPdm.tooltip";

   @RBEntry("projcheckin.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_5 = "sandbox.SBSendToPdm.icon";

   @RBEntry("width=1000,height=600")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE")
   public static final String SENDTOPDM_ROW_WINDOW_SIZE = "sandbox.SBSendToPdm.moreurlinfo";

   /**
    * Entries for Sandbox Update Project
    **/
   @RBEntry("Aggiorna progetto")
   @RBComment("Update Project")
   public static final String PRIVATE_CONSTANT_9 = "sandbox.SBUpdatePrj.description";

   @RBEntry("Aggiorna tutti gli oggetti condivisi nel progetto")
   @RBComment("Update All Objects Shared to the Project")
   public static final String PRIVATE_CONSTANT_10 = "sandbox.SBUpdatePrj.tooltip";

   @RBEntry("proj_refresh2.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_11 = "sandbox.SBUpdatePrj.icon";

   /**
    * Entries for Sandbox Update Selected Shares -- File menu action
    **/
   @RBEntry("Aggiorna oggetti condivisi selezionati")
   @RBComment("Update Selected Shares")
   public static final String PRIVATE_CONSTANT_12 = "sandbox.updateShareMultiSelect.description";

   @RBEntry("Aggiorna gli oggetti selezionati condivisi nel progetto")
   @RBComment("Update Selected Objects Shared to the Project")
   public static final String PRIVATE_CONSTANT_13 = "sandbox.updateShareMultiSelect.tooltip";

   @RBEntry("shared_object_update.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String UPDATE_SHARE_MULTI_SELECT_ICON = "sandbox.updateShareMultiSelect.icon";

   /**
    * Entries for Sandbox Update Selected Shares -- Row-level action
    **/
   @RBEntry("Aggiorna")
   @RBComment("Update")
   public static final String PRIVATE_CONSTANT_14 = "sandbox.updateShare.description";

   @RBEntry("Aggiorna gli oggetti selezionati condivisi nel progetto")
   @RBComment("Update Selected Objects Shared to the Project")
   public static final String PRIVATE_CONSTANT_15 = "sandbox.updateShare.tooltip";

   @RBEntry("shared_object_update.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String UPDATE_SHARE_ICON = "sandbox.updateShare.icon";

   /**
    * Entries for Sandbox Undo Checkout
    **/
   @RBEntry("projcheckout_undo.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_16 = "object.sandboxUndoCheckout.icon";

   @RBEntry("Annulla Check-Out PDM")
   @RBComment("Undo PDM Checkout")
   public static final String PRIVATE_CONSTANT_17 = "object.sandboxUndoCheckout.description";

   @RBEntry("Annulla il Check-Out PDM degli oggetti selezionati")
   @RBComment("Undo PDM Checkout of Selected Objects")
   public static final String PRIVATE_CONSTANT_18 = "object.sandboxUndoCheckout.tooltip";

   /**
    * Entries for Sandbox Undo Checkout Info page
    **/
   @RBEntry("projcheckout_undo.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_19 = "object.sandboxUndoCheckoutDetails.icon";

   @RBEntry("Annulla Check-Out PDM")
   @RBComment("Undo PDM Checkout")
   public static final String PRIVATE_CONSTANT_20 = "object.sandboxUndoCheckoutDetails.description";

   @RBEntry("Annulla il Check-Out PDM degli oggetti selezionati")
   @RBComment("Undo PDM Checkout of Selected Objects")
   public static final String PRIVATE_CONSTANT_21 = "object.sandboxUndoCheckoutDetails.tooltip";

   /**
    * Entries for Sandbox Convert To PDM Checkout
    **/
   @RBEntry("Converti in Check-Out PDM")
   @RBComment("Convert To PDM Check Out")
   public static final String PRIVATE_CONSTANT_22 = "object.sandboxCheckoutShare.description";

   @RBEntry("Converte gli oggetti condivisi in elementi sottoposti a Check-Out PDM")
   @RBComment("Convert Shared Object to PDM Checked Out")
   public static final String PRIVATE_CONSTANT_23 = "object.sandboxCheckoutShare.tooltip";

   @RBEntry("pdm_checkout.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_24 = "object.sandboxCheckoutShare.icon";

   /**
    * Entries for Sandbox Convert To PDM Checkout multiple objects
    **/
   @RBEntry("Converti in Check-Out PDM")
   @RBComment("Convert To PDM Check Out")
   public static final String PRIVATE_CONSTANT_25 = "object.sandboxCheckoutShareMultiSelect.description";

   @RBEntry("Converte gli oggetti condivisi in elementi sottoposti a Check-Out PDM")
   @RBComment("Convert Shared Objects to PDM Checked Out")
   public static final String PRIVATE_CONSTANT_26 = "object.sandboxCheckoutShareMultiSelect.tooltip";

   @RBEntry("pdm_checkout.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_27 = "object.sandboxCheckoutShareMultiSelect.icon";

   /**
    * Entries for Sandbox Remove Share
    **/
   @RBEntry("Rimuovi condivisione")
   @RBComment("Remove Share")
   public static final String PRIVATE_CONSTANT_28 = "object.removeShare.description";

   @RBEntry("Rimuovi condivisione")
   @RBComment("Remove Share")
   public static final String PRIVATE_CONSTANT_29 = "object.removeShare.tooltip";

   @RBEntry("share_remove.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_30 = "object.removeShare.icon";

   /**
    * Entries for Sandbox Remove Share
    **/
   @RBEntry("Rimuovi condivisione")
   @RBComment("Remove Share")
   public static final String PRIVATE_CONSTANT_31 = "object.removeShareTB.description";

   @RBEntry("Rimuovi condivisione")
   @RBComment("Remove Share")
   public static final String PRIVATE_CONSTANT_32 = "object.removeShareTB.tooltip";

   @RBEntry("share_remove.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_33 = "object.removeShareTB.icon";

   /**
    * Entries for Sandbox Convert to Share - row level
    **/
   @RBEntry("Converti in oggetto condiviso")
   @RBComment("Converts Deprecated objects to Abandoned and shares back the latest PDM version.")
   public static final String PRIVATE_CONSTANT_34 = "sandbox.convertToShare.description";

   @RBEntry("Converte gli oggetti obsoleti in oggetti condivisi")
   @RBComment("Convert Deprecated Objects to Shares")
   public static final String PRIVATE_CONSTANT_35 = "sandbox.convertToShare.tooltip";

   @RBEntry("share_convert.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_36 = "sandbox.convertToShare.icon";

   /**
    * Send to PDM Wizard Action -- toolbar action
    **/
   @RBEntry("Invia a PDM")
   @RBComment("Send to PDM Wizard Action")
   public static final String PRIVATE_CONSTANT_40 = "sandbox.sendToPDM.title";

   @RBEntry("Invia a PDM")
   @RBComment("Send to PDM Wizard Action")
   public static final String PRIVATE_CONSTANT_41 = "sandbox.sendToPDM.description";

   @RBEntry("Invia gli oggetti selezionati al sistema PDM")
   @RBComment("Send Selected Objects to PDM System")
   public static final String PRIVATE_CONSTANT_42 = "sandbox.sendToPDM.tooltip";

   @RBEntry("projcheckin.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String SENDTOPDM_ICON = "sandbox.sendToPDM.icon";

   @RBEntry("width=1000,height=600")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE")
   public static final String SENDTOPDM_WINDOW_SIZE = "sandbox.sendToPDM.moreurlinfo";

   /**
    * Send to PDM Wizard -- Step 1: Collect Objects and Set Options
    **/
   @RBEntry("Raccogli oggetti e imposta opzioni")
   @RBComment("Send to PDM Wizard Step 1")
   public static final String PRIVATE_CONSTANT_43 = "sandbox.sendToPDMCollectObjectsWizStep.title";

   @RBEntry("Raccogli oggetti e imposta opzioni")
   @RBComment("Send to PDM Wizard Step 1 label")
   public static final String PRIVATE_CONSTANT_44 = "sandbox.sendToPDMCollectObjectsWizStep.description";

   @RBEntry("Raccogli oggetti e imposta opzioni")
   @RBComment("Collect Objects and Set Options")
   public static final String PRIVATE_CONSTANT_45 = "sandbox.sendToPDMCollectObjectsWizStep.tooltip";

   /**
    * Send to PDM Wizard -- Step 2: Set Object Attributes
    **/
   @RBEntry("Imposta attributi oggetto")
   @RBComment("Send to PDM Wizard Step 2")
   public static final String PRIVATE_CONSTANT_46 = "sandbox.sendToPDMSetObjectAttributesWizStep.title";

   @RBEntry("Imposta attributi oggetto")
   @RBComment("Send to PDM Wizard Step 2 label")
   public static final String PRIVATE_CONSTANT_47 = "sandbox.sendToPDMSetObjectAttributesWizStep.description";

   @RBEntry("Imposta attributi oggetto")
   @RBComment("Set Object Attributes")
   public static final String PRIVATE_CONSTANT_48 = "sandbox.sendToPDMSetObjectAttributesWizStep.tooltip";

   /**
    * Entries for Sandbox Keep Checkout
    **/
   @RBEntry("Mantieni in stato di Check-Out")
   @RBComment("Checkout the objects after check in ")
   public static final String PRIVATE_CONSTANT_49 = "sandbox.keepcheckout.description";

   @RBEntry("Mantieni in stato di Check-Out")
   @RBComment("Keep Check-out")
   public static final String PRIVATE_CONSTANT_50 = "sandbox.keepcheckout.tooltip";

   @RBEntry("projcheckout.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_51 = "sandbox.keepcheckout.icon";

   /**
    * Entries for Sandbox Check in only
    **/
   @RBEntry("Check-In PDM")
   @RBComment("Do not checkout the objects after check in ")
   public static final String PRIVATE_CONSTANT_52 = "sandbox.checkinonly.description";

   @RBEntry("Check-In PDM")
   @RBComment("PDM Check-in")
   public static final String PRIVATE_CONSTANT_53 = "sandbox.checkinonly.tooltip";

   @RBEntry("projcheckin.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_54 = "sandbox.checkinonly.icon";

   /**
    * Entries for Collect Objects action on Send To PDM Wizard Step 1 Toolbar
    **/
   @RBEntry("Raccogli oggetti correlati")
   @RBComment("Collect Related Objects to Send To PDM")
   public static final String PRIVATE_CONSTANT_55 = "sandbox.collectObjects.description";

   @RBEntry("Raccogli oggetti correlati")
   @RBComment("Collect Related Objects")
   public static final String PRIVATE_CONSTANT_56 = "sandbox.collectObjects.tooltip";

   @RBEntry("collect.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_57 = "sandbox.collectObjects.icon";

   /**
    * SetView action for Send to PDM Wizard Toolbar
    **/
   @RBEntry("Imposta vista")
   @RBComment("Sets the view for the object when it is sent to PDM")
   public static final String PRIVATE_CONSTANT_58 = "sandbox.sendToPDMSetView.title";

   @RBEntry("Imposta vista")
   @RBComment("Sets the view for the object when it is sent to PDM")
   public static final String PRIVATE_CONSTANT_59 = "sandbox.sendToPDMSetView.description";

   @RBEntry("Imposta vista")
   @RBComment("Set Object View")
   public static final String PRIVATE_CONSTANT_60 = "sandbox.sendToPDMSetView.tooltip";

   @RBEntry("view_set.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_61 = "sandbox.sendToPDMSetView.icon";

   /**
    * SetLocation action for Send to PDM Wizard Toolbar
    **/
   @RBEntry("Imposta posizione")
   @RBComment("Sets the location for the object when it is sent to PDM")
   public static final String PRIVATE_CONSTANT_62 = "sandbox.sendToPDMSetLocation.title";

   @RBEntry("Imposta posizione")
   @RBComment("Sets the location for the object when it is sent to PDM")
   public static final String PRIVATE_CONSTANT_63 = "sandbox.sendToPDMSetLocation.description";

   @RBEntry("Imposta posizione")
   @RBComment("Set Object Location")
   public static final String PRIVATE_CONSTANT_64 = "sandbox.sendToPDMSetLocation.tooltip";

   @RBEntry("location_set.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_65 = "sandbox.sendToPDMSetLocation.icon";

   /**
    * Entries for Manage Identity Conflicts action
    **/
   @RBEntry("Risolvi conflitto di identificativi")
   @RBComment("Resolve Identity Conflicts Action")
   public static final String PRIVATE_CONSTANT_66 = "sandbox.manageIdentityConflicts.title";

   @RBEntry("Risolvi conflitto di identificativi")
   @RBComment("Reports CAD Documents in a project which have name conflict with those in PDM ")
   public static final String PRIVATE_CONSTANT_67 = "sandbox.manageIdentityConflicts.description";

   @RBEntry("Segnala i documenti CAD con un conflitto di univocità del nome")
   @RBComment("Report CAD Documents having name uniqueness conflict")
   public static final String PRIVATE_CONSTANT_68 = "sandbox.manageIdentityConflicts.tooltip";

    @RBEntry("cad_id_conflict_4.gif")
    @RBComment("DO NOT TRANSLATE")
    public static final String PRIVATE_CONSTANT_69 = "sandbox.manageIdentityConflicts.icon";

   /**
    * Entries for Replace action
    **/
   @RBEntry("Sostituisci")
   @RBComment("Replaces a project object with a PDM object ")
   public static final String PRIVATE_CONSTANT_70 = "sandbox.replaceWithPDMObj.title";

   @RBEntry("Sostituisci")
   @RBComment("Replaces a project object with a PDM object  ")
   public static final String PRIVATE_CONSTANT_71 = "sandbox.replaceWithPDMObj.description";

   @RBEntry("Sostituisce un oggetto del progetto con un oggetto PDM")
   @RBComment("Replace a project object with a PDM object")
   public static final String PRIVATE_CONSTANT_72 = "sandbox.replaceWithPDMObj.tooltip";

   @RBEntry("override_project.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_73 = "sandbox.replaceWithPDMObj.icon";

   /**
    * Entries for Compare action (Wraps Info Compare action)
    **/
   @RBEntry("Confronta")
   @RBComment("Compares a project object with a PDM object ")
   public static final String PRIVATE_CONSTANT_74 = "sandbox.compareWithPDMObj.title";

   @RBEntry("Confronta")
   @RBComment("This compare action wraps the existing Information Compare action for the comparison  ")
   public static final String PRIVATE_CONSTANT_75 = "sandbox.compareWithPDMObj.description";

   @RBEntry("Confronta un oggetto del progetto con un oggetto PDM")
   @RBComment("Compare a project object with a PDM object")
   public static final String PRIVATE_CONSTANT_76 = "sandbox.compareWithPDMObj.tooltip";

   @RBEntry("iconCompare.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_77 = "sandbox.compareWithPDMObj.icon";

   /**
    * Entries for Remove Objects action on Send To PDM Wizard Step 1 Toolbar
    **/
   @RBEntry("Rimuovi oggetti selezionati")
   @RBComment("Remove Selected Objects")
   public static final String PRIVATE_CONSTANT_78 = "sandbox.remove.description";

   @RBEntry("Rimuovi oggetti selezionati")
   @RBComment("Remove Selected Objects")
   public static final String PRIVATE_CONSTANT_79 = "sandbox.remove.tooltip";

   @RBEntry("remove.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_80 = "sandbox.remove.icon";

   /**
    * Entries for toolbar Replace action
    **/
   @RBEntry("Sostituisci")
   @RBComment("Replaces a project object with a PDM object ")
   public static final String PRIVATE_CONSTANT_81 = "sandbox.replaceWithPDMObjTB.title";

   @RBEntry("Sostituisci")
   @RBComment("Replaces a project object with a PDM object  ")
   public static final String PRIVATE_CONSTANT_82 = "sandbox.replaceWithPDMObjTB.description";

   @RBEntry("Sostituisce un oggetto del progetto con un oggetto PDM")
   @RBComment("Replace a project object with a PDM object")
   public static final String PRIVATE_CONSTANT_83 = "sandbox.replaceWithPDMObjTB.tooltip";

   @RBEntry("override_project.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_84 = "sandbox.replaceWithPDMObjTB.icon";

   /**
    * Entries for Sandbox Convert to Share - Toolbar
    **/
   @RBEntry("Converti in oggetto condiviso")
   @RBComment("Converts Deprecated objects to Abandoned and shares back the latest PDM version.")
   public static final String PRIVATE_CONSTANT_85 = "sandbox.convertToShareTB.description";

   @RBEntry("Converte gli oggetti obsoleti in oggetti condivisi")
   @RBComment("Convert Deprecated Objects to Shares")
   public static final String PRIVATE_CONSTANT_86 = "sandbox.convertToShareTB.tooltip";

   @RBEntry("share_convert.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_87 = "sandbox.convertToShareTB.icon";

   /**
    * Entries for Project Status Table Send to PDM Action - Toolbar
    **/
   @RBEntry("Invia a PDM")
   @RBComment("Send to PDM Wizard title")
   public static final String PST_SEND_TO_PDM_TOOLBAR_ACTION_TITLE = "sandbox.pstSendToPdmTB.title";

   @RBEntry("Invia a PDM")
   @RBComment("Send to PDM Wizard Action")
   public static final String PRIVATE_CONSTANT_91 = "sandbox.pstSendToPdmTB.description";

   @RBEntry("Invia gli oggetti selezionati al sistema PDM")
   @RBComment("Send Selected Objects to PDM System")
   public static final String PRIVATE_CONSTANT_92 = "sandbox.pstSendToPdmTB.tooltip";

   @RBEntry("projcheckin.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_93 = "sandbox.pstSendToPdmTB.icon";

   /**
    * Entries for Project Status Table Convert to PDM Checkout Action - Toolbar
    **/
   @RBEntry("Converti in Check-Out PDM")
   @RBComment("Convert To PDM Check Out")
   public static final String PRIVATE_CONSTANT_94 = "object.pstConvertToPdmCheckoutTB.description";

   @RBEntry("Converte gli oggetti condivisi in elementi sottoposti a Check-Out PDM")
   @RBComment("Convert Shared Objects to PDM Checked Out")
   public static final String PRIVATE_CONSTANT_95 = "object.pstConvertToPdmCheckoutTB.tooltip";

   @RBEntry("pdm_checkout.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_96 = "object.pstConvertToPdmCheckoutTB.icon";

   /**
    * Entries for Project Status Table Remove Share Action - Toolbar
    **/
   @RBEntry("Rimuovi condivisione")
   @RBComment("Remove Share")
   public static final String PRIVATE_CONSTANT_97 = "object.pstRemoveShareTB.description";

   @RBEntry("Rimuovi condivisione")
   @RBComment("Remove Share")
   public static final String PRIVATE_CONSTANT_98 = "object.pstRemoveShareTB.tooltip";

   @RBEntry("share_remove.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_99 = "object.pstRemoveShareTB.icon";

   /**
    * Entries for Project Status Table Undo PDM Checkout Action - Toolbar
    **/
   @RBEntry("projcheckout_undo.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_100 = "object.pstUndoPdmCheckoutTB.icon";

   @RBEntry("Annulla Check-Out PDM")
   @RBComment("Undo PDM Checkout")
   public static final String PRIVATE_CONSTANT_101 = "object.pstUndoPdmCheckoutTB.description";

   @RBEntry("Annulla il Check-Out PDM degli oggetti selezionati")
   @RBComment("Undo PDM Checkout of Selected Objects")
   public static final String PRIVATE_CONSTANT_102 = "object.pstUndoPdmCheckoutTB.tooltip";


   /**
    * Entries for Project Status Table Convert to Share Action - Row level
    **/
   @RBEntry("Converti in oggetto condiviso")
   @RBComment("Converts Deprecated objects to Abandoned and shares back the latest PDM version.")
   public static final String PRIVATE_CONSTANT_103 = "sandbox.pstConvertToShare.description";

   @RBEntry("Converte gli oggetti obsoleti in oggetti condivisi")
   @RBComment("Convert Deprecated Objects to Shares")
   public static final String PRIVATE_CONSTANT_104 = "sandbox.pstConvertToShare.tooltip";

   @RBEntry("share_convert.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_105 = "sandbox.pstConvertToShare.icon";

   /**
    * Entries for Project Status Table Send to PDM Action - Row level
    **/
   @RBEntry("Invia a PDM")
   @RBComment("Send to PDM row-level action")
   public static final String PRIVATE_CONSTANT_106 = "sandbox.pstSendToPdm.title";

   @RBEntry("Invia a PDM")
   @RBComment("Send to PDM row-level action")
   public static final String PRIVATE_CONSTANT_107 = "sandbox.pstSendToPdm.description";

   @RBEntry("Invia gli oggetti selezionati al sistema PDM")
   @RBComment("Send Selected Objects to PDM System")
   public static final String PRIVATE_CONSTANT_108 = "sandbox.pstSendToPdm.tooltip";

   @RBEntry("projcheckin.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_109 = "sandbox.pstSendToPdm.icon";

   /**
    * Entries for Project Status Table Convert to PDM Checkout Action - Row level
    **/
   @RBEntry("Converti in Check-Out PDM")
   @RBComment("Convert To PDM Check Out")
   public static final String PRIVATE_CONSTANT_110 = "object.pstConvertToPdmCheckout.description";

   @RBEntry("Converte gli oggetti condivisi in elementi sottoposti a Check-Out PDM")
   @RBComment("Convert Shared Object to PDM Checked Out")
   public static final String PRIVATE_CONSTANT_111 = "object.pstConvertToPdmCheckout.tooltip";

   @RBEntry("pdm_checkout.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_112 = "object.pstConvertToPdmCheckout.icon";

   /**
    * Entries for Project Status Table Remove Share Action - Row level
    **/
   @RBEntry("Rimuovi condivisione")
   @RBComment("Remove Share")
   public static final String PRIVATE_CONSTANT_113 = "object.pstRemoveShare.description";

   @RBEntry("Rimuovi condivisione")
   @RBComment("Remove Share")
   public static final String PRIVATE_CONSTANT_114 = "object.pstRemoveShare.tooltip";

   @RBEntry("share_remove.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_115 = "object.pstRemoveShare.icon";

   /**
    * Entries for Project Status Table Undo Pdm Checkout Action - Row level
    **/
   @RBEntry("projcheckout_undo.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_116 = "object.pstUndoPdmCheckout.icon";

   @RBEntry("Annulla Check-Out PDM")
   @RBComment("Undo PDM Checkout")
   public static final String PRIVATE_CONSTANT_117 = "object.pstUndoPdmCheckout.description";

   @RBEntry("Annulla il Check-Out PDM degli oggetti selezionati")
   @RBComment("Undo PDM Checkout of Selected Objects")
   public static final String PRIVATE_CONSTANT_118 = "object.pstUndoPdmCheckout.tooltip";

   /**
    * Entries for Project Status Table Update Action - Row level
    **/
   @RBEntry("Aggiorna")
   @RBComment("Update")
   public static final String PRIVATE_CONSTANT_119 = "sandbox.pstUpdateShare.description";

   @RBEntry("Aggiorna gli oggetti selezionati condivisi nel progetto")
   @RBComment("Update Selected Objects Shared to the Project")
   public static final String PRIVATE_CONSTANT_120 = "sandbox.pstUpdateShare.tooltip";

   @RBEntry("shared_object_update.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String UPDATE_SHARE_PST_ICON = "sandbox.pstUpdateShare.icon";

   /**
    * Entries for Project Status Table Delete Action - Row level
    **/
   @RBEntry("delete.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_121 = "object.pstDelete.icon";

   @RBEntry("Elimina")
   @RBComment("Delete")
   public static final String PRIVATE_CONSTANT_122 = "object.pstDelete.description";

   @RBEntry("Elimina gli oggetti selezionati")
   @RBComment("Delete selected objects")
   public static final String PRIVATE_CONSTANT_123 = "object.pstDelete.tooltip";

   /**
    * Entries for Project Status Table Delete Action - Row level
    **/
   @RBEntry("delete.gif")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_124 = "object.pstDeleteTB.icon";

   @RBEntry("Elimina")
   @RBComment("Delete")
   public static final String PRIVATE_CONSTANT_125 = "object.pstDeleteTB.description";

   @RBEntry("Elimina gli oggetti selezionati")
   @RBComment("Delete selected objects")
   public static final String PRIVATE_CONSTANT_126 = "object.pstDeleteTB.tooltip";
}
