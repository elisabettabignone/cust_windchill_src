/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.containermove;

import wt.util.resource.*;

@RBUUID("wt.dataops.containermove.containermoveResource")
public final class containermoveResource extends WTListResourceBundle {
   @RBEntry("Some selected objects are associated with a project context. Objects in a project cannot be moved.")
   public static final String SOURCE_PROJECT_CONTEXT = "0";

   @RBEntry("The target folder for some selected objects is in a project context. Objects cannot be moved to a project context.")
   public static final String DESTINATION_PROJECT_CONTEXT = "1";

   @RBEntry("Object owned by organization \"{0}\" is being moved to organization \"{1}\". Objects cannot be moved between contexts in different organizations.")
   public static final String SOURCE_DESTINATION_DIFFERENT_ORGS = "2";

   @RBEntry("You do not have the necessary permission to modify the source folders for the objects {0}.")
   public static final String SOURCE_FOLDER_NO_ACCESS = "3";

   @RBEntry("You do not have the necessary permission to modify the target folders for the objects {0}.")
   public static final String DESTINATION_FOLDER_NO_ACCESS = "4";

   @RBEntry("You do not have permission to create these object types in the target folders.\n{0}")
   public static final String DESTINATION_FOLDER_NO_CREATE_ACCESS = "40001";

   @RBEntry("You do not have permission to create these object types in the selected lifecycle states and target folders.\n{0}")
   public static final String DEST_FOLDER_NO_ACCESS_LC_STATE = "40002";

   @RBEntry("The following object types cannot be created. The associated lifecycle templates are not valid in the target folders.\n{0}")
   public static final String DEST_FOLDER_NO_ACCESS_LC_TEMPLATE = "40003";

   @RBEntry("Target:")
   public static final String TARGET_FOR_ERROR = "TARGET_FOR_ERROR";

   @RBEntry("Template:")
   public static final String TEMPLATE_FOR_ERROR = "TEMPLATE_FOR_ERROR";

   @RBEntry("State:")
   public static final String STATE_FOR_ERROR = "STATE_FOR_ERROR";
   
   @RBEntry("Product - ")
   public static final String PRODUCT_FOR_ERROR = "PRODUCT_FOR_ERROR";

   @RBEntry("Library - ")
   public static final String LIBRARY_FOR_ERROR = "LIBRARY_FOR_ERROR";

   @RBEntry("Organization - ")
   public static final String ORGANIZATION_FOR_ERROR = "ORGANIZATION_FOR_ERROR";
   
   @RBEntry("Object \"{0}\" is checked out by \"{1}\" and cannot be moved.")
   public static final String OBJECT_WIP_CHECKED_OUT = "5";

   @RBEntry("Object \"{0}\" is checked out to project \"{1}\" and cannot be moved.")
   public static final String OBJECT_SANDBOX_CHECKED_OUT = "6";

   @RBEntry("Unable to complete move operation because of conflicts.")
   public static final String GENERAL_MOVE_CONFLICT_MESSAGE = "7";

   @RBEntry("Illegal object types for move operation.")
   public static final String ILLEGAL_OBJECT_TYPE = "8";

   @RBEntry("You cannot move these objects because you do not have the necessary permission to delete one or more of its versions.")
   public static final String NO_DELETE_PERMISSION = "9";

   @RBEntry("You cannot move these objects because you do not have the necessary permission to modify one or more of its versions.")
   public static final String NO_MODIFY_PERMISSION = "90001";

   @RBEntry("You cannot move these objects because you do not have permission to change the domain for one or more of their versions.\n{0}")
   public static final String NO_CHANGE_DOMAIN_PERMISSION = "90002";

   @RBEntry("You cannot move these objects because you do not have permission to change the context for one or more of their versions.\n{0}")
   public static final String NO_CHANGE_CONTAINER_PERMISSION = "90003";

   @RBEntry("The target context, \"{0}\", has been specified but does not exist.")
   public static final String DEST_CONTAINER_DOESNT_EXIST = "10";

   @RBEntry("The target folder path, \"{0}\", doesn't exist within the context, \"{1}\".")
   public static final String DEST_CONTAINER_FOLDER_PATH_DOESNT_EXIST = "11";

   @RBEntry("A target context and folder path has not been specified for some objects.")
   public static final String NO_DESTINATION_FOLDER = "12";

   @RBEntry("Objects:")
   public static final String OBJECTS_FOR_ERROR = "13";

   @RBEntry("Some selected objects are in a personal cabinet. Objects in a personal cabinet must be checked in before they can be moved.")
   public static final String IN_PERSONAL_CABINET = "14";

   @RBEntry("Moving foldered objects between contexts is not permitted.")
   public static final String ILLEGAL_FOLDER_CHANGE_EVENT = "15";

   @RBEntry("Object \"{0}\" has the life cycle template \"{1}\" which is associated with the local context.\nThe object cannot be moved to a new context.")
   public static final String LOCAL_LIFECYCLE_TEMPLATE = "16";

   @RBEntry("Move Objects")
   public static final String TASK_NAME = "17";

   @RBEntry("The primary End Item for a Product cannot be moved.")
   public static final String OBJECT_IS_PRIMARY_ENDITEM = "18";

   @RBEntry("There are no objects to be moved.")
   public static final String NO_OBJECTS_TO_MOVE = "19";

   @RBEntry("Moving foldered objects between contexts is not permitted.")
   public static final String VETO_FOLDER_CHANGE_MESSAGE = "20";

   @RBEntry("A life cycle template initialization rule does not exist for objects of type \"{0}\" in context \"{1}\".")
   public static final String NO_LIFECYCLE_INIT_RULE = "21";

   @RBEntry("The context \"{0}\" selected is not a valid target context.")
   public static final String WRONG_DESTINATION_CONTEXT = "22";

   @RBEntry("The object \"{0}\" has been shared to the project to which you want to move it. The share must be removed first from the target project.")
   public static final String WRONG_DESTINATION_PROJECT = "23";

   @RBEntry("The object \"{0}\" is either checked out or checked out from PDM.")
   public static final String INELIGIBLE_OBJECTS = "24";

   @RBEntry("No target location has been specified for the object \"{0}\".")
   public static final String NO_TARGET_LOCATION = "25";

   @RBEntry("The type definition \"{0}\" does not exist in the target context.")
   public static final String TYPE_DEFINITION_DOES_NOT_EXISTS = "26";

   @RBEntry("An object with the same number or file name already exists in the target context.")
   public static final String UNIQUENESS = "27";

   @RBEntry("Life cycle template of object \"{0}\" does not exist in the target context.")
   public static final String LIFE_CYCLE_TEMPLATE_NOT_FOUND = "28";

   @RBEntry("The user or role defined in the following object team does not exist in the target context team. \"{0}\" \"{1}\"")
   public static final String ROLE_OR_USER_DOES_NOT_EXISTS = "29";

   @RBEntry("The classification definition for the object \"{0}\" does not exist in the target context.")
   public static final String CLASSIFICATION_DEFINITION_DOES_NOT_EXIST = "30";

   @RBEntry("The workflow template \"{0}\" of object \"{1}\" does not exist in the target context.")
   public static final String WORKFLOW_TEMPLATE_NOT_FOUND = "31";

   @RBEntry("Different versions of the objects \"{0}\" are located in different folders that are not moved at the same time.")
   public static final String VERSIONS_LEFT_BEHIND = "32";

   @RBEntry("A multiple domain stucture is associated to the folder structure being moved. Domain structure and access control policies can be either moved or duplicated. Select which action will be performed.")
   public static final String MULTIPLE_DOMAIN_STRUCTURE_EXIST = "33";

   @RBEntry("You do not have the proper authorization for the objects \"{0}\" in the target location to perform this operation.")
   public static final String SECURE_OPERATION = "34";

   @RBEntry("Distribution target of object \"{0}\" does not exist in the target context.")
   public static final String ESITARGET_NOT_FOUND = "35";

   @RBEntry("Subfolder \"{0}\" was also selected to be moved along with parent folder \"{1}\". Choose either one for move.")
   public static final String SUBFOLDER_IN_FOLDER_SEEDS = "36";

   @RBEntry("The soft attribute definition \"{0}\" does not exist in the target context.")
   public static final String IBA_DEFINITION_DOES_NOT_EXISTS = "37";

   @RBEntry("Objects and associated domains are in different contexts.")
   public static final String OBJECT_DOMAIN_IN_DIFFERENT_CONTEXT = "38";

   @RBEntry("\"{0}\": The target context must be the same for all versions of an object being moved.")
   public static final String VERSIONS_IN_DIFFERENT_CONTEXT = "39";

   @RBEntry("Team template of object \"{0}\" does not exist in the target context.")
   public static final String TEAM_TEMPLATE_NOT_FOUND = "40";

   @RBEntry("(Secured information)")
   public static final String SECURED_INFORMATION = "41";
  
   @RBEntry("Not all objects are moved.")
   public static final String NO_ALL_OBJECTS_TO_MOVE = "42";
   
   @RBEntry("Same domain paths of objects' does not exist in the target context.")
   public static final String SAME_DOMAIN_PATH_DOES_NOT_EXIST_IN_TARGET_CONTEXT = "43";
}
















