/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.containermove.conflictResolution;

import wt.util.resource.*;

@RBUUID("wt.dataops.containermove.conflictResolution.conflictResolutionResource")
public final class conflictResolutionResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile trovare un risolutore per il tipo di risoluzione \"{0}\".")
   public static final String RESOLVER_NOT_FOUND = "0";

   @RBEntry("Risoluzione non valida. Deve essere del tipo MoveConflictResolution.")
   public static final String INVALID_RESOLUTION = "1";
}
