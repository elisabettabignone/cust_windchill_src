/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.containermove.conflictResolution;

import wt.util.resource.*;

@RBUUID("wt.dataops.containermove.conflictResolution.conflictResolutionResource")
public final class conflictResolutionResource extends WTListResourceBundle {
   @RBEntry("Resolver not found for resolution type \"{0}\".")
   public static final String RESOLVER_NOT_FOUND = "0";

   @RBEntry("Invalid resolution. Resolution should be of type MoveConflictResolution.")
   public static final String INVALID_RESOLUTION = "1";
}
