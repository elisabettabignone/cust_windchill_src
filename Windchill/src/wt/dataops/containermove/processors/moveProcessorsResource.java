/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.containermove.processors;

import wt.util.resource.*;

@RBUUID("wt.dataops.containermove.processors.moveProcessorsResource")
public final class moveProcessorsResource extends WTListResourceBundle {
   @RBEntry("Collect Objects")
   public static final String ADD_RELATED_OBJECTS_LABEL = "0";

   @RBEntry("Set New Location")
   public static final String EDIT_OBJECT_LIST_LABEL = "1";

   @RBEntry("Gather objects within the same Product(s) and/or Library(ies) as the orginally selected objects only.")
   public static final String GATHER_FROM_SAME_CONTAINERS_ONLY = "2";

   @RBEntry("Move")
   public static final String MOVE_TITLE = "3";

   @RBEntry("Set the new context and folder location for the selected objects")
   public static final String LOCATION = "4";

   @RBEntry("Include/Exclude")
   public static final String INCLUDE_EXCLUDE = "5";

   @RBEntry("All Objects")
   public static final String VIEW_LABEL_ALL_OBJECTS = "6";

   @RBEntry("Parts/Products Only")
   public static final String VIEW_LABEL_PARTS_ONLY = "60001";

   @RBEntry("Documents Only")
   public static final String VIEW_LABEL_DOCS_ONLY = "60002";

   @RBEntry("CAD Documents Only")
   public static final String VIEW_LABEL_CAD_DOCS_ONLY = "60003";

   @RBEntry("Move Gathering Table")
   public static final String MOVE_GATHERING_TABLE = "7";

   @RBEntry("Move Edit Table")
   public static final String MOVE_EDIT_TABLE = "8";

   @RBEntry("Move")
   public static final String MOVE_LABEL = "9";

   @RBEntry("Move objects to a new context and folder.")
   public static final String MOVE_TOOLTIP = "10";

   @RBEntry("Objects of this type can not be moved.")
   public static final String ILLEGAL_OBJECT_TYPE = "11";

   @RBEntry("You are not allowed to modify this object.")
   public static final String NO_MOVE_PERMISSION = "12";

   @RBEntry("One or more versions of this object are checked out. Checked out objects cannot be moved.")
   public static final String OBJECT_CHECKED_OUT = "13";

   @RBEntry("This object is in a library.")
   public static final String ITEM_IN_LIBRARY = "14";

   @RBEntry("Problems exist. Please review status columns for more details.")
   public static final String TOP_STATUS_MESSAGE = "15";

   @RBEntry("You have selected more than one version of some objects. The Move action automatically operates on all versions of all selected objects. Duplicate objects have been removed from the table.")
   public static final String MULTIPLE_VERSIONS_SELECTED = "16";

   @RBEntry("All versions and iterations of the objects will be moved together.")
   public static final String TOP_INFO_MESSAGE = "17";

   @RBEntry("Attention: Ineligible object selected.\\nThe object(s) you selected is ineligible for Move.")
   public static final String ILLEGAL_OBJECT_TYPES_SELECTED = "18";

   @RBEntry("The object is associated with a project context. Objects in a project currently cannot be moved.")
   public static final String OBJECT_IN_PROJECT_CONTEXT = "19";

   @RBEntry("Document templates can not currently be moved between contexts.")
   public static final String DOC_TEMPLATE_NOT_MOVABLE = "20";

   @RBEntry("The object is in a personal cabinet. Objects in a personal cabinet must be checked in before they can be moved.")
   public static final String ITEM_IN_PERSONAL_CABINET = "21";

   @RBEntry("There are no objects to be moved.")
   public static final String NEXT_STEP_INVALID = "22";

   @RBEntry("The new location was not specified for some objects.\\nIf you continue, these objects will not be moved.\\nDo you want to continue?")
   @RBComment("Note the embedded \\n entries to break the string into three lines.")
   public static final String NO_LOCATION_SPECIFIED_FOR_SOME = "23";

   @RBEntry("Context")
   public static final String CONTEXT_LABEL = "24";

   @RBEntry("Objects associated with a site or organization context cannot be moved.")
   public static final String OBJECT_IN_SITE_ORG_CONTEXT = "25";
   
   @RBEntry("Moving specific versions of an object across containers is not allowed.")
   public static final String CROSS_CONTAINER_SPECIFIC_VERSION_ERROR = "26";
   
   @RBEntry("Moving all view versions of an object across containers is not allowed.")
   public static final String CROSS_CONTAINER_ALL_VIEW_VERSION_ERROR = "27";
   
   @RBEntry("Move all versions of displayed objects.")
   public static final String MOVE_OPTION_ALL_VERSIONS = "28";
   
   @RBEntry("Only Move versions of displayed view.")
   public static final String MOVE_OPTION_ALL_VIEW_VERSIONS = "29";
   
   @RBEntry("Only Move displayed versions.")
   public static final String MOVE_OPTION_CURRENT_VERSIONS = "30";
}
