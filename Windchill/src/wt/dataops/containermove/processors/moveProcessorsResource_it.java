/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.containermove.processors;

import wt.util.resource.*;

@RBUUID("wt.dataops.containermove.processors.moveProcessorsResource")
public final class moveProcessorsResource_it extends WTListResourceBundle {
   @RBEntry("Raccogli oggetti")
   public static final String ADD_RELATED_OBJECTS_LABEL = "0";

   @RBEntry("Imposta nuova posizione")
   public static final String EDIT_OBJECT_LIST_LABEL = "1";

   @RBEntry("Raccogli solo oggetti nell'ambito degli stessi prodotti o librerie degli oggetti selezionati inizialmente.")
   public static final String GATHER_FROM_SAME_CONTAINERS_ONLY = "2";

   @RBEntry("Sposta")
   public static final String MOVE_TITLE = "3";

   @RBEntry("Imposta il nuovo contesto e posizione di cartella per gli oggetti selezionati.")
   public static final String LOCATION = "4";

   @RBEntry("Includi/Escludi")
   public static final String INCLUDE_EXCLUDE = "5";

   @RBEntry("Tutti gli oggetti")
   public static final String VIEW_LABEL_ALL_OBJECTS = "6";

   @RBEntry("Solo parti/prodotti")
   public static final String VIEW_LABEL_PARTS_ONLY = "60001";

   @RBEntry("Solo documenti")
   public static final String VIEW_LABEL_DOCS_ONLY = "60002";

   @RBEntry("Solo documenti CAD")
   public static final String VIEW_LABEL_CAD_DOCS_ONLY = "60003";

   @RBEntry("Sposta tabella di raccolta")
   public static final String MOVE_GATHERING_TABLE = "7";

   @RBEntry("Sposta tabella di modifica")
   public static final String MOVE_EDIT_TABLE = "8";

   @RBEntry("Sposta")
   public static final String MOVE_LABEL = "9";

   @RBEntry("Sposta gli oggetti in un nuovo contesto e cartella.")
   public static final String MOVE_TOOLTIP = "10";

   @RBEntry("Impossibile muovere gli oggetti di questo tipo.")
   public static final String ILLEGAL_OBJECT_TYPE = "11";

   @RBEntry("Non si dispone dei permessi necessari per modificare l'oggetto.")
   public static final String NO_MOVE_PERMISSION = "12";

   @RBEntry("Una o più versioni dell'oggetto sono sottoposte a Check-Out. Impossibile spostare gli oggetti sottoposti a Check-Out.")
   public static final String OBJECT_CHECKED_OUT = "13";

   @RBEntry("L'oggetto è in una libreria.")
   public static final String ITEM_IN_LIBRARY = "14";

   @RBEntry("È stato rilevato un problema. Consultare le colonne di stato per ulteriori dettagli.")
   public static final String TOP_STATUS_MESSAGE = "15";

   @RBEntry("Sono state selezionate più versioni di alcuni oggetti. L'azione Sposta ha effetto automaticamente su tutte le versioni degli oggetti selezionati. Gli oggetti duplicati sono stati rimossi dalla tabella.")
   public static final String MULTIPLE_VERSIONS_SELECTED = "16";

   @RBEntry("Tutte le versioni e iterazioni degli oggetti saranno spostate assieme.")
   public static final String TOP_INFO_MESSAGE = "17";

   @RBEntry("Attenzione: selezionati oggetti non validi.\\n Gli oggetti selezionati non sono validi per l'operazione di spostamento.")
   public static final String ILLEGAL_OBJECT_TYPES_SELECTED = "18";

   @RBEntry("L'oggetto è associato a un contesto di progetto. Impossibile spostare gli oggetti che si trovano in un progetto in questo momento.")
   public static final String OBJECT_IN_PROJECT_CONTEXT = "19";

   @RBEntry("Al momento non è possibile spostare modelli di documento fra contesti.")
   public static final String DOC_TEMPLATE_NOT_MOVABLE = "20";

   @RBEntry("L'oggetto si trova in uno schedario personale ed è necessario sottoporlo a Check-In prima di poterlo spostare.")
   public static final String ITEM_IN_PERSONAL_CABINET = "21";

   @RBEntry("Non esistono oggetti da spostare.")
   public static final String NEXT_STEP_INVALID = "22";

   @RBEntry("Nuova posizione non specificata per alcuni oggetti.\\nSe si continua, tali oggetti non saranno spostati.\\nContinuare?")
   @RBComment("Note the embedded \\n entries to break the string into three lines.")
   public static final String NO_LOCATION_SPECIFIED_FOR_SOME = "23";

   @RBEntry("Contesto")
   public static final String CONTEXT_LABEL = "24";

   @RBEntry("Impossibile spostare oggetti associati a un contesto sito o organizzazione.")
   public static final String OBJECT_IN_SITE_ORG_CONTEXT = "25";
   
   @RBEntry("Spostamento di versioni specifiche di un oggetto tra contenitori diversi non consentito.")
   public static final String CROSS_CONTAINER_SPECIFIC_VERSION_ERROR = "26";
   
   @RBEntry("Spostamento di tutte le versioni della vista di un oggetto tra contenitori diversi non consentito.")
   public static final String CROSS_CONTAINER_ALL_VIEW_VERSION_ERROR = "27";
   
   @RBEntry("Spostare tutte le versioni degli oggetti visualizzati.")
   public static final String MOVE_OPTION_ALL_VERSIONS = "28";
   
   @RBEntry("Spostare solo le versioni della vista visualizzata.")
   public static final String MOVE_OPTION_ALL_VIEW_VERSIONS = "29";
   
   @RBEntry("Spostare solo le versioni visualizzate.")
   public static final String MOVE_OPTION_CURRENT_VERSIONS = "30";
}
