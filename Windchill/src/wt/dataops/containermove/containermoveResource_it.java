/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.containermove;

import wt.util.resource.*;

@RBUUID("wt.dataops.containermove.containermoveResource")
public final class containermoveResource_it extends WTListResourceBundle {
   @RBEntry("Alcuni degli oggetti selezionati sono associati a un contesto di progetto. Impossibile spostare gli oggetti che si trovano in un progetto.")
   public static final String SOURCE_PROJECT_CONTEXT = "0";

   @RBEntry("La cartella di destinazione per alcuni degli oggetti selezionati è in un contesto di progetto. Impossibile spostare gli oggetti in un contesto di progetto.")
   public static final String DESTINATION_PROJECT_CONTEXT = "1";

   @RBEntry("Si sta spostando un oggetto appartenente all'organizzazione \"{0}\" nell'organizzazione \"{1}\". Impossibile spostare gli oggetti fra contesti in organizzazioni differenti.")
   public static final String SOURCE_DESTINATION_DIFFERENT_ORGS = "2";

   @RBEntry("Non si dispone dei permessi di modifica necessari sulle cartelle di origine relative agli oggetti che seguono: {0}.")
   public static final String SOURCE_FOLDER_NO_ACCESS = "3";

   @RBEntry("Non si dispone dei permessi di modifica necessari sulle cartelle di destinazione relative agli oggetti che seguono: {0}.")
   public static final String DESTINATION_FOLDER_NO_ACCESS = "4";

   @RBEntry("Non si dispone dei permessi per creare oggetti di questi tipi nelle cartelle di destinazione.\n{0}")
   public static final String DESTINATION_FOLDER_NO_CREATE_ACCESS = "40001";

   @RBEntry("Non si dispone dei permessi per creare oggetti di questi tipi negli stati del ciclo di vita selezionati e nelle cartelle di destinazione.\n{0}")
   public static final String DEST_FOLDER_NO_ACCESS_LC_STATE = "40002";

   @RBEntry("Impossibile creare i seguenti tipi di oggetti. I modelli del ciclo di vita associati non sono validi nelle cartelle di destinazione.\n{0}")
   public static final String DEST_FOLDER_NO_ACCESS_LC_TEMPLATE = "40003";

   @RBEntry("Destinazione:")
   public static final String TARGET_FOR_ERROR = "TARGET_FOR_ERROR";

   @RBEntry("Modello:")
   public static final String TEMPLATE_FOR_ERROR = "TEMPLATE_FOR_ERROR";

   @RBEntry("Stato del ciclo di vita:")
   public static final String STATE_FOR_ERROR = "STATE_FOR_ERROR";
   
   @RBEntry("Prodotto - ")
   public static final String PRODUCT_FOR_ERROR = "PRODUCT_FOR_ERROR";

   @RBEntry("Libreria - ")
   public static final String LIBRARY_FOR_ERROR = "LIBRARY_FOR_ERROR";

   @RBEntry("Organizzazione - ")
   public static final String ORGANIZATION_FOR_ERROR = "ORGANIZATION_FOR_ERROR";
   
   @RBEntry("Impossibile spostare l'oggetto \"{0}\". È sottoposto a Check-Out dall'utente \"{1}\".")
   public static final String OBJECT_WIP_CHECKED_OUT = "5";

   @RBEntry("Impossibile spostare l'oggetto \"{0}\". È sottoposto a Check-Out nel progetto \"{1}\".")
   public static final String OBJECT_SANDBOX_CHECKED_OUT = "6";

   @RBEntry("Impossibile completare l'operazione di spostamento. Si sono verificati dei conflitti.")
   public static final String GENERAL_MOVE_CONFLICT_MESSAGE = "7";

   @RBEntry("Tipi di elemento non validi per l'operazione di spostamento.")
   public static final String ILLEGAL_OBJECT_TYPE = "8";

   @RBEntry("Impossibile spostare gli oggetti. Non si dispone dei permessi necessari a eliminarne una o più versioni.")
   public static final String NO_DELETE_PERMISSION = "9";

   @RBEntry("Impossibile spostare gli oggetti. Non si dispone dei permessi necessari a modificarne una o più versioni.")
   public static final String NO_MODIFY_PERMISSION = "90001";

   @RBEntry("Impossibile spostare gli oggetti. Non si dispone dei permessi per cambiare il dominio per una o più versioni.\n{0}")
   public static final String NO_CHANGE_DOMAIN_PERMISSION = "90002";

   @RBEntry("Impossibile spostare gli oggetti. Non si dispone dei permessi per cambiare il contesto per una o più versioni.\n{0}")
   public static final String NO_CHANGE_CONTAINER_PERMISSION = "90003";

   @RBEntry("Il contesto di destinazione specificato \"{0}\" non esiste.")
   public static final String DEST_CONTAINER_DOESNT_EXIST = "10";

   @RBEntry("Cartella di destinazione \"{0}\" inesistente nel contesto \"{1}\".")
   public static final String DEST_CONTAINER_FOLDER_PATH_DOESNT_EXIST = "11";

   @RBEntry("Non sono stati specificati un contesto di destinazione e un percorso di cartella per alcuni oggetti.")
   public static final String NO_DESTINATION_FOLDER = "12";

   @RBEntry("Oggetti:")
   public static final String OBJECTS_FOR_ERROR = "13";

   @RBEntry("Alcuni degli oggetti selezionati si trovano in uno schedario personale ed è necessario sottoporli a Check-In prima di poterli spostare.")
   public static final String IN_PERSONAL_CABINET = "14";

   @RBEntry("Lo spostamento fra contesti degli oggetti in cartella non è consentito.")
   public static final String ILLEGAL_FOLDER_CHANGE_EVENT = "15";

   @RBEntry("Il modello di ciclo di vita per l'oggetto \"{0}\", \"{1}\", è associato al contesto locale.\nImpossibile spostare l'oggetto in un nuovo contesto.")
   public static final String LOCAL_LIFECYCLE_TEMPLATE = "16";

   @RBEntry("Sposta oggetti")
   public static final String TASK_NAME = "17";

   @RBEntry("Impossibile spostare il prodotto finale principale per un prodotto.")
   public static final String OBJECT_IS_PRIMARY_ENDITEM = "18";

   @RBEntry("Non esistono oggetti da spostare.")
   public static final String NO_OBJECTS_TO_MOVE = "19";

   @RBEntry("Lo spostamento fra contesti degli oggetti in cartella non è consentito.")
   public static final String VETO_FOLDER_CHANGE_MESSAGE = "20";

   @RBEntry("Non esiste una regola di inizializzazione del modello di ciclo di vita per oggetti di tipo \"{0}\" nel contesto \"{1}\".")
   public static final String NO_LIFECYCLE_INIT_RULE = "21";

   @RBEntry("Il contesto \"{0}\" selezionato non è un contesto di destinazione valido.")
   public static final String WRONG_DESTINATION_CONTEXT = "22";

   @RBEntry("L'oggetto \"{0}\" è condiviso nel progetto in cui si desidera spostarlo. È necessario rimuovere la condivisione dal progetto di destinazione prima che sia possibile eseguire lo spostamento.")
   public static final String WRONG_DESTINATION_PROJECT = "23";

   @RBEntry("L'oggetto \"{0}\" è sottoposto a Check-Out o sottoposto a Check-Out da PDM.")
   public static final String INELIGIBLE_OBJECTS = "24";

   @RBEntry("Nessuna posizione di destinazione specificata per l'oggetto \"{0}\".")
   public static final String NO_TARGET_LOCATION = "25";

   @RBEntry("La definizione di tipo \"{0}\" non esiste nel contesto di destinazione.")
   public static final String TYPE_DEFINITION_DOES_NOT_EXISTS = "26";

   @RBEntry("Un oggetto con lo stesso numero o nome file esiste già nel contesto di destinazione.")
   public static final String UNIQUENESS = "27";

   @RBEntry("Il modello di ciclo di vita \"{0}\" non esiste nel contesto di destinazione.")
   public static final String LIFE_CYCLE_TEMPLATE_NOT_FOUND = "28";

   @RBEntry("Il ruolo o utente definito nel team di oggetto che segue non esiste nel team del contesto di destinazione. \"{0}\" \"{1}\"")
   public static final String ROLE_OR_USER_DOES_NOT_EXISTS = "29";

   @RBEntry("La definizione di classificazione per l'oggetto \"{0}\" non esiste nel contesto di destinazione.")
   public static final String CLASSIFICATION_DEFINITION_DOES_NOT_EXIST = "30";

   @RBEntry("Il modello di workflow \"{0}\" dell'oggetto \"{1}\" non esiste nel contesto di destinazione.")
   public static final String WORKFLOW_TEMPLATE_NOT_FOUND = "31";

   @RBEntry("Versioni differenti degli oggetti \"{0}\" si trovano in cartelle differenti che non è possibile spostare simultaneamente.")
   public static final String VERSIONS_LEFT_BEHIND = "32";

   @RBEntry("Una struttura a più domini è associata alla struttura di cartelle che si sta spostando. La struttura di dominio e le regole di controllo di accesso possono essere spostate o duplicate. Selezionare l'azione da eseguire.")
   public static final String MULTIPLE_DOMAIN_STRUCTURE_EXIST = "33";

   @RBEntry("Non si dispone dei permessi necessari per gli oggetti \"{0}\" nella posizione di destinazione per eseguire l'operazione.")
   public static final String SECURE_OPERATION = "34";

   @RBEntry("Il target di distribuzione per l'oggetto \"{0}\" non esiste nel contesto di destinazione.")
   public static final String ESITARGET_NOT_FOUND = "35";

   @RBEntry("La cartella secondaria \"{0}\" è stata scelta assieme alla relativa cartella padre \"{1}\" per lo spostamento. Scegliere quale delle due spostare.")
   public static final String SUBFOLDER_IN_FOLDER_SEEDS = "36";

   @RBEntry("La definizione di attributo soft \"{0}\" non esiste nel contesto di destinazione.")
   public static final String IBA_DEFINITION_DOES_NOT_EXISTS = "37";

   @RBEntry("Gli oggetti e i domini associati si trovano in contesti differenti.")
   public static final String OBJECT_DOMAIN_IN_DIFFERENT_CONTEXT = "38";

   @RBEntry("\"{0}\": per spostare un oggetto è necessario che il contesto di destinazione sia lo stesso per tutte le versioni dell'oggetto.")
   public static final String VERSIONS_IN_DIFFERENT_CONTEXT = "39";

   @RBEntry("Il modello di team per l'oggetto \"{0}\" non esiste nel contesto di destinazione.")
   public static final String TEAM_TEMPLATE_NOT_FOUND = "40";

   @RBEntry("(Informazioni protette)")
   public static final String SECURED_INFORMATION = "41";
  
   @RBEntry("Non sono stati spostati tutti gli oggetti.")
   public static final String NO_ALL_OBJECTS_TO_MOVE = "42";
   
   @RBEntry("Gli stessi percorsi di dominio degli oggetti non esistono nel contesto di destinazione.")
   public static final String SAME_DOMAIN_PATH_DOES_NOT_EXIST_IN_TARGET_CONTEXT = "43";
}
















