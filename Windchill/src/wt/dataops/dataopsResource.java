/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops;

import wt.util.resource.*;

@RBUUID("wt.dataops.dataopsResource")
public final class dataopsResource extends WTListResourceBundle {
   /**
    * DataOps common messages
    **/
   @RBEntry("Task \"{0}\" : Delete not permitted for Task with status \"{1}\".")
   @RBArgComment0("Task name")
   @RBArgComment1("Task status")
   public static final String TASK_DELETE_VETO = "0";

   @RBEntry("Type")
   public static final String TYPE = "1";

   @RBEntry("Identity")
   public static final String IDENTITY = "2";

   @RBEntry("Version")
   public static final String VERSION = "3";

   @RBEntry("Iteration")
   public static final String ITERATION = "4";

   @RBEntry("Context")
   public static final String CONTEXT = "5";
}
