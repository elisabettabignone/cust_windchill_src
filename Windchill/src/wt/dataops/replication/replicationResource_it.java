package wt.dataops.replication;

import wt.util.resource.*;

@RBUUID("wt.dataops.replication.replicationResource")
public final class replicationResource_it extends WTListResourceBundle {
   /**
/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
    * searchResource message resource bundle [English/US]
    * ##
    * Replication (RulesCacheManager) specific constants
    * ##
    * 
    * Error messages
    * 
    **/
   @RBEntry("Tipo di oggetto come")
   @RBComment("It will indicate Object Type values to be followed up.")
   public static final String OBJECTTYPE_AS = "1";

   @RBEntry("Contesto come")
   @RBComment("It will indicate Context values to be followed up.")
   public static final String CONTEXT_AS = "2";

   @RBEntry("Stato del ciclo di vita come")
   @RBComment("It will indicate LifeCycle state values to be followed up.")
   public static final String LIFECYCLE_STATE_AS = "3";

   @RBEntry("per archivio di replica")
   @RBComment("It will indicate ReplicaVault value to be followed up.")
   public static final String FOR_REPLICA = "4";

   @RBEntry("Impossibile creare la regola appena aggiunta perché possiede una regola duplicata")
   @RBComment("Rule can not be created as newly added rule has duplicate rule")
   public static final String RULE_CAN_NOT_BE_CREATED_MESSAGE = "5";

   @RBEntry("Impossibile creare la regola perché è stata aggiunta in archivi differenti dello stesso sito Sito={0}")
   @RBComment("Rule cannot be created. Rule is added to different vaults of the same site ")
   public static final String MULTI_VAULT_OF_SAME_SITE = "6";

   @RBEntry("Impossibile creare la regola. Esiste già una regola ({0}) con i seguenti parametri: Contesto = {1}, Tipo di oggetto = {2} e Archivio di destinazione replica = {3}.")
   @RBComment("Error Message for Rule Duplication with Object Type ")
   public static final String ERROR_RULE_DUPLICATION_OBJTYPE = "7";

   @RBEntry("Impossibile creare la regola. Esiste già una regola ({0}) con i seguenti parametri: Contesto = {1}, Tipo di oggetto = {2} Stato del ciclo di vita = {3} e Archivio di destinazione replica = {4}.")
   @RBComment("Error Message for Rule Duplication with Object Type and Lifecycle State")
   public static final String ERROR_RULE_DUPLICATION_OBJTYPE_LC = "8";

   @RBEntry("Pulizia completa")
   @RBComment("Message when task for removing inaccessible item using mbean is completed")
   public static final String NOTIFY_INACCESSIBLE_CONTENT_SUBJECT = "9";
}
