package wt.dataops.replication;

import wt.util.resource.*;

@RBUUID("wt.dataops.replication.replicationResource")
public final class replicationResource extends WTListResourceBundle {
   /**
/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
    * searchResource message resource bundle [English/US]
    * ##
    * Replication (RulesCacheManager) specific constants
    * ##
    * 
    * Error messages
    * 
    **/
   @RBEntry("ObjectType as")
   @RBComment("It will indicate Object Type values to be followed up.")
   public static final String OBJECTTYPE_AS = "1";

   @RBEntry("Context as")
   @RBComment("It will indicate Context values to be followed up.")
   public static final String CONTEXT_AS = "2";

   @RBEntry("Lifecycle state as")
   @RBComment("It will indicate LifeCycle state values to be followed up.")
   public static final String LIFECYCLE_STATE_AS = "3";

   @RBEntry("for replica vault")
   @RBComment("It will indicate ReplicaVault value to be followed up.")
   public static final String FOR_REPLICA = "4";

   @RBEntry("Rule can not be created as newly added rule has duplicate rule")
   @RBComment("Rule can not be created as newly added rule has duplicate rule")
   public static final String RULE_CAN_NOT_BE_CREATED_MESSAGE = "5";

   @RBEntry("Rule cannot be created. Rule is added to different vaults of the same site Site={0}")
   @RBComment("Rule cannot be created. Rule is added to different vaults of the same site ")
   public static final String MULTI_VAULT_OF_SAME_SITE = "6";

   @RBEntry("Rule cannot be created. A Rule ({0}) already exists with parameters [Context={1}, Object Type={2} and Target Replica Vault={3}].")
   @RBComment("Error Message for Rule Duplication with Object Type ")
   public static final String ERROR_RULE_DUPLICATION_OBJTYPE = "7";

   @RBEntry("Rule cannot be created. A Rule ({0}) already exists with parameters [Context={1}, Object Type={2}, Lifecycle State={3} and Target Replica Vault={4}].")
   @RBComment("Error Message for Rule Duplication with Object Type and Lifecycle State")
   public static final String ERROR_RULE_DUPLICATION_OBJTYPE_LC = "8";

   @RBEntry("The cleanup is completed")
   @RBComment("Message when task for removing inaccessible item using mbean is completed")
   public static final String NOTIFY_INACCESSIBLE_CONTENT_SUBJECT = "9";
}