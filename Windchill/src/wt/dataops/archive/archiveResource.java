/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.archive;

import wt.util.resource.*;

@RBUUID("wt.dataops.archive.archiveResource")
public final class archiveResource extends WTListResourceBundle {
   @RBEntry("{1} : Archive Operation has started : \"{0}\"")
   @RBArgComment0("The Archive operation name")
   @RBArgComment1("The start time")
   public static final String ARCHIVE_STARTED = "0";

   @RBEntry("{1} : Archive Operation has finished : \"{0}\"")
   @RBArgComment0("The Archive operation name")
   @RBArgComment1("The finish time")
   public static final String ARCHIVE_FINISHED = "1";

   @RBEntry("Archive Operation has failed : \"{0}\"")
   @RBArgComment0("The Archive operation name")
   public static final String ARCHIVE_FAILED = "2";

   @RBEntry("Successfully archived \"{0}\".")
   @RBArgComment0("The archiveable identity")
   public static final String OBJECT_ARCHIVE_SUCCESSFUL = "3";

   @RBEntry("Failed to archive \"{0}\".")
   @RBArgComment0("The archiveable identity")
   public static final String OBJECT_ARCHIVE_FAILED = "4";

   @RBEntry("\"{0}\" : Not Archiveable.")
   @RBArgComment0("The object identity")
   public static final String NOT_ARCHIVEABLE = "5";

   @RBEntry("Failed to dispatch Archive package to Archive System. Archive Operation has failed : \"{0}\" ")
   @RBArgComment0("The Archive operation name")
   public static final String ARCHIVE_DISPATCH_FAILED = "6";

   @RBEntry("Object holding archive information cannot be deleted")
   public static final String HOLDER_DELETE_PROHIBITED = "7";

   @RBEntry("Delete not allowed : \"{0}\"")
   @RBArgComment0("The Archive identity")
   public static final String ARCHIVE_DELETE_PROHIBITED = "8";

   @RBEntry("Successfully dispatched Archive package to Archive System : \"{0}\" ")
   @RBArgComment0("The Archive operation name")
   public static final String ARCHIVE_DISPATCH_SUCCESSFUL = "9";

   @RBEntry("Attempting to move container of archived version \"{0}\".")
   public static final String VERSION_CONTAINER_MOVE_WARNING = "10";

   @RBEntry("No objects were selected for archive.")
   public static final String NO_OPERANDS = "11";

   @RBEntry("{1} : Restore Operation has started for following archive : \"{0}\"")
   @RBArgComment0("The Restore operation name")
   @RBArgComment1("The start time")
   public static final String RESTORE_STARTED = "12";

   @RBEntry("{1} : Restore Operation has finished : \"{0}\"")
   @RBArgComment0("The Restore operation name")
   @RBArgComment1("The finish time")
   public static final String RESTORE_FINISHED = "13";

   @RBEntry("Restore Operation has failed : \"{0}\"")
   @RBArgComment0("The Restore operation name")
   public static final String RESTORE_FAILED = "14";

   @RBEntry("Successfully restored \"{0}\".")
   @RBArgComment0("The archiveable identity")
   public static final String OBJECT_RESTORE_SUCCESSFUL = "15";

   @RBEntry("Failed to restore \"{0}\".")
   @RBArgComment0("The archiveable identity")
   public static final String OBJECT_RESTORE_FAILED = "16";

   @RBEntry("Restore record does not have any archive reference : \"{0}\"")
   @RBArgComment0("Restore record name")
   public static final String NO_ARCHIVE_REFERENCE = "17";

   @RBEntry("Archive operands information.")
   public static final String OPERANDS_HEADER = "18";

   @RBEntry("Archive package creation failed : \"{0}\"")
   @RBArgComment0("Archive record name")
   public static final String IX_ARCHIVE_FAILED = "19";

   @RBEntry("Restore operation failed for following archive : \"{0}\"")
   @RBArgComment0("Archive name")
   public static final String IX_RESTORE_FAILED = "20";

   @RBEntry("Invalid Archive : \"{0}\"")
   @RBArgComment0("Archive name")
   public static final String INVALID_ARCHIVE = "21";

   @RBEntry("Invalid insert of archived object : \"{0}\".")
   @RBArgComment0("Object identity")
   public static final String INSERT_NOT_ALLOWED = "22";

   @RBEntry("Invalid Rollback of archived version \"{0}\"")
   @RBArgComment0("Version identity")
   public static final String ROLLBACK_NOT_ALLOWED = "23";

   @RBEntry("\"{0}\" : Delete not permitted for operation with status \"{1}\".")
   @RBArgComment0("Record Name")
   @RBArgComment1("Record status")
   public static final String RECORD_DELETE_PROHIBITED = "24";

   @RBEntry("\"{0}\"")
   @RBArgComment0("The identity of the windchill object")
   public static final String OBJECT_IDENTITY = "25";

   @RBEntry("The collection contains some objects that cannot be archived. Please contact PTC. ")
   public static final String CANNOT_ARCHIVE_NON_ARCHIVEABLES = "26";
}
