/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.archive;

import wt.util.resource.*;

@RBUUID("wt.dataops.archive.archiveResource")
public final class archiveResource_it extends WTListResourceBundle {
   @RBEntry("Operazione di archiviazione \"{0}\" iniziata alle {1}.")
   @RBArgComment0("The Archive operation name")
   @RBArgComment1("The start time")
   public static final String ARCHIVE_STARTED = "0";

   @RBEntry("Operazione di archiviazione \"{0}\" completata alle {1}.")
   @RBArgComment0("The Archive operation name")
   @RBArgComment1("The finish time")
   public static final String ARCHIVE_FINISHED = "1";

   @RBEntry("Operazione di archiviazione \"{0}\" non riuscita.")
   @RBArgComment0("The Archive operation name")
   public static final String ARCHIVE_FAILED = "2";

   @RBEntry("Archiviazione di \"{0}\" completata.")
   @RBArgComment0("The archiveable identity")
   public static final String OBJECT_ARCHIVE_SUCCESSFUL = "3";

   @RBEntry("Archiviazione di \"{0}\" non riuscita.")
   @RBArgComment0("The archiveable identity")
   public static final String OBJECT_ARCHIVE_FAILED = "4";

   @RBEntry("Impossibile archiviare \"{0}\".")
   @RBArgComment0("The object identity")
   public static final String NOT_ARCHIVEABLE = "5";

   @RBEntry("Impossibile trasmettere il pacchetto al sistema di archiviazione. Operazione di archiviazione \"{0}\" non riuscita. ")
   @RBArgComment0("The Archive operation name")
   public static final String ARCHIVE_DISPATCH_FAILED = "6";

   @RBEntry("Impossibile eliminare un oggetto contenente informazioni sull'archivio")
   public static final String HOLDER_DELETE_PROHIBITED = "7";

   @RBEntry("Impossibile eliminare \"{0}\".")
   @RBArgComment0("The Archive identity")
   public static final String ARCHIVE_DELETE_PROHIBITED = "8";

   @RBEntry("Il pacchetto di archiviazione \"{0}\" è stato trasmesso al sistema di archiviazione.")
   @RBArgComment0("The Archive operation name")
   public static final String ARCHIVE_DISPATCH_SUCCESSFUL = "9";

   @RBEntry("Tentativo di spostamento del contenitore della versione archiviata \"{0}\".")
   public static final String VERSION_CONTAINER_MOVE_WARNING = "10";

   @RBEntry("Nessun oggetto selezionato per l'archiviazione.")
   public static final String NO_OPERANDS = "11";

   @RBEntry("Operazione di ripristino dell'archivio \"{0}\" iniziata alle {1}.")
   @RBArgComment0("The Restore operation name")
   @RBArgComment1("The start time")
   public static final String RESTORE_STARTED = "12";

   @RBEntry("Operazione di ripristino \"{0}\" completata alle {1}.")
   @RBArgComment0("The Restore operation name")
   @RBArgComment1("The finish time")
   public static final String RESTORE_FINISHED = "13";

   @RBEntry("Operazione di ripristino \"{0}\" non riuscita.")
   @RBArgComment0("The Restore operation name")
   public static final String RESTORE_FAILED = "14";

   @RBEntry("Ripristino di \"{0}\" completato.")
   @RBArgComment0("The archiveable identity")
   public static final String OBJECT_RESTORE_SUCCESSFUL = "15";

   @RBEntry("Ripristino di \"{0}\" non riuscito.")
   @RBArgComment0("The archiveable identity")
   public static final String OBJECT_RESTORE_FAILED = "16";

   @RBEntry("Il record di ripristino \"{0}\" non dispone di riferimenti di archivio.")
   @RBArgComment0("Restore record name")
   public static final String NO_ARCHIVE_REFERENCE = "17";

   @RBEntry("Informazioni sugli elementi da archiviare.")
   public static final String OPERANDS_HEADER = "18";

   @RBEntry("Creazione del pacchetto di archiviazione \"{0}\" non riuscita.")
   @RBArgComment0("Archive record name")
   public static final String IX_ARCHIVE_FAILED = "19";

   @RBEntry("Operazione di ripristino dell'archivio \"{0}\" non riuscita")
   @RBArgComment0("Archive name")
   public static final String IX_RESTORE_FAILED = "20";

   @RBEntry("Archivio \"{0}\" non valido.")
   @RBArgComment0("Archive name")
   public static final String INVALID_ARCHIVE = "21";

   @RBEntry("Inserimento non valido dell'oggetto archiviato \"{0}\".")
   @RBArgComment0("Object identity")
   public static final String INSERT_NOT_ALLOWED = "22";

   @RBEntry("Rollback della versione archiviata \"{0}\" non valido.")
   @RBArgComment0("Version identity")
   public static final String ROLLBACK_NOT_ALLOWED = "23";

   @RBEntry("Attività \"{0}\": l'eliminazione delle attività con stato \"{1}\" non è consentita.")
   @RBArgComment0("Record Name")
   @RBArgComment1("Record status")
   public static final String RECORD_DELETE_PROHIBITED = "24";

   @RBEntry("\"{0}\"")
   @RBArgComment0("The identity of the windchill object")
   public static final String OBJECT_IDENTITY = "25";

   @RBEntry("La raccolta contiene oggetti che non possono essere archiviati. Contattare PTC.")
   public static final String CANNOT_ARCHIVE_NON_ARCHIVEABLES = "26";
}
