/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops;

import wt.util.resource.*;

@RBUUID("wt.dataops.dataopsPreferenceResource")
public final class dataopsPreferenceResource_it extends WTListResourceBundle {
   /**
    * DataOps common messages
    * PREF_CATEGORY_PURGE_ARCHIVE_RESTORE.constant=PREF_CATEGORY_PURGE_ARCHIVE_RESTORE
    **/
   @RBEntry("Pulizia, Archiviazioni, Ripristini")
   public static final String PRIVATE_CONSTANT_0 = "PREF_CATEGORY_PURGE_ARCHIVE_RESTORE";

   /**
    * PREF_CHUNK_SIZE.constant=PREF_CHUNK_SIZE
    **/
   @RBEntry("Dimensione frammento per eliminazione")
   public static final String PRIVATE_CONSTANT_1 = "PREF_CHUNK_SIZE";

   /**
    * PREF_CHUNK_SIZE_DESC.constant=PREF_CHUNK_SIZE_DESC
    **/
   @RBEntry("Numero di oggetti da eliminare in una volta sola")
   public static final String PRIVATE_CONSTANT_2 = "PREF_CHUNK_SIZE_DESC";

   /**
    * PREF_CHUNK_SIZE_LONG_DESC.constant=PREF_CHUNK_SIZE_LONG_DESC
    **/
   @RBEntry("Gli oggetti da eliminare vengono suddivisi in frammenti che contengono al massimo il numero di oggetti specificati dal valore della preferenza chunksize. Ciascun frammento viene eliminato separatamente. Impostare il valore su -1 per disattivare l'uso dei frammenti durante le operazioni di pulizia.")
   public static final String PRIVATE_CONSTANT_3 = "PREF_CHUNK_SIZE_LONG_DESC";

   /**
    * JOB_COUNT_DISPLAY.constant=JOB_COUNT_DISPLAY
    **/
   @RBEntry("Numero di operazioni")
   public static final String PRIVATE_CONSTANT_4 = "JOB_COUNT_DISPLAY";

   /**
    * JOB_COUNT_DESCRIPTION.constant=JOB_COUNT_DESCRIPTION
    **/
   @RBEntry("Numero massimo di operazioni/programmazioni elencate in una pagina.")
   public static final String PRIVATE_CONSTANT_5 = "JOB_COUNT_DESCRIPTION";

   /**
    * JOB_COUNT_LONG_DESCRIPTION.constant=JOB_COUNT_LONG_DESCRIPTION
    **/
   @RBEntry("Questa proprietà viene utilizzata dal sistema di archiviazione. Il valore di default è 50 e può essere modificato dall'utente.")
   public static final String PRIVATE_CONSTANT_6 = "JOB_COUNT_LONG_DESCRIPTION";

   /**
    * PREF_PREVIEW_LIMIT.constant=PREF_PREVIEW_LIMIT
    **/
   @RBEntry("Limite anteprima")
   public static final String PRIVATE_CONSTANT_7 = "PREF_PREVIEW_LIMIT";

   /**
    * PREF_PREVIEW_LIMIT_DESC.constant=PREF_PREVIEW_LIMIT_DESC
    **/
   @RBEntry("Numero massimo di oggetti nella tabella di anteprima HTML.")
   public static final String PRIVATE_CONSTANT_8 = "PREF_PREVIEW_LIMIT_DESC";

   /**
    * PREF_PREVIEW_LIMIT_LONG_DESC.constant=PREF_PREVIEW_LIMIT_LONG_DESC
    **/
   @RBEntry("Controlla il numero massimo di oggetti raccolti nella tabella di anteprima HTML nella finestra Nuova operazione. Il valore di default è 500.")
   public static final String PRIVATE_CONSTANT_9 = "PREF_PREVIEW_LIMIT_LONG_DESC";
}
