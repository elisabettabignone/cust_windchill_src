/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops;

import wt.util.resource.*;

@RBUUID("wt.dataops.dataopsPreferenceResource")
public final class dataopsPreferenceResource extends WTListResourceBundle {
   /**
    * DataOps common messages
    * PREF_CATEGORY_PURGE_ARCHIVE_RESTORE.constant=PREF_CATEGORY_PURGE_ARCHIVE_RESTORE
    **/
   @RBEntry("Purge, Archive, Restore")
   public static final String PRIVATE_CONSTANT_0 = "PREF_CATEGORY_PURGE_ARCHIVE_RESTORE";

   /**
    * PREF_CHUNK_SIZE.constant=PREF_CHUNK_SIZE
    **/
   @RBEntry("Chunk Size Purge")
   public static final String PRIVATE_CONSTANT_1 = "PREF_CHUNK_SIZE";

   /**
    * PREF_CHUNK_SIZE_DESC.constant=PREF_CHUNK_SIZE_DESC
    **/
   @RBEntry("Number of objects to purge at one time.")
   public static final String PRIVATE_CONSTANT_2 = "PREF_CHUNK_SIZE_DESC";

   /**
    * PREF_CHUNK_SIZE_LONG_DESC.constant=PREF_CHUNK_SIZE_LONG_DESC
    **/
   @RBEntry("All objects to purge are divided into sets of objects containing, at most, the number of objects specified by the chunksize preference. Each chunk is purged separately. Set this to -1 to disable the use of chunks for a purge.")
   public static final String PRIVATE_CONSTANT_3 = "PREF_CHUNK_SIZE_LONG_DESC";

   /**
    * JOB_COUNT_DISPLAY.constant=JOB_COUNT_DISPLAY
    **/
   @RBEntry("Job Count")
   public static final String PRIVATE_CONSTANT_4 = "JOB_COUNT_DISPLAY";

   /**
    * JOB_COUNT_DESCRIPTION.constant=JOB_COUNT_DESCRIPTION
    **/
   @RBEntry("The maximum number of jobs/schedules listed on a page.")
   public static final String PRIVATE_CONSTANT_5 = "JOB_COUNT_DESCRIPTION";

   /**
    * JOB_COUNT_LONG_DESCRIPTION.constant=JOB_COUNT_LONG_DESCRIPTION
    **/
   @RBEntry("This property is used by the archive system. The default value is 50 and can be changed by the user.")
   public static final String PRIVATE_CONSTANT_6 = "JOB_COUNT_LONG_DESCRIPTION";

   /**
    * PREF_PREVIEW_LIMIT.constant=PREF_PREVIEW_LIMIT
    **/
   @RBEntry("Preview Limit")
   public static final String PRIVATE_CONSTANT_7 = "PREF_PREVIEW_LIMIT";

   /**
    * PREF_PREVIEW_LIMIT_DESC.constant=PREF_PREVIEW_LIMIT_DESC
    **/
   @RBEntry("The maximum number of objects in the HTML preview table.")
   public static final String PRIVATE_CONSTANT_8 = "PREF_PREVIEW_LIMIT_DESC";

   /**
    * PREF_PREVIEW_LIMIT_LONG_DESC.constant=PREF_PREVIEW_LIMIT_LONG_DESC
    **/
   @RBEntry("Controls the maximum number of objects collected in the HTML preview table in the New Job window. The default is 500.")
   public static final String PRIVATE_CONSTANT_9 = "PREF_PREVIEW_LIMIT_LONG_DESC";
}
