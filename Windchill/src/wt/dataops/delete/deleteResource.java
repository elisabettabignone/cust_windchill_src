/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.delete;

import wt.util.resource.*;

@RBUUID("wt.dataops.delete.deleteResource")
public final class deleteResource extends WTListResourceBundle {
   @RBEntry("Delete Objects")
   public static final String TASK_NAME = "0";

   @RBEntry("There are no items to be deleted.")
   public static final String NO_ITEMS_FOR_TASK = "1";

   @RBEntry("Delete task cannot process a combination of project and non-project objects.")
   public static final String OBJECTS_FROM_MULTIPLE_PROJECTS_ERROR = "2";

   @RBEntry("Cannot delete the latest iteration of object(s) '{0}' because it is the only remaining iteration for revision")
   public static final String ONLY_ITERATION_OF_VERSION_CONFLICT = "3";
}
