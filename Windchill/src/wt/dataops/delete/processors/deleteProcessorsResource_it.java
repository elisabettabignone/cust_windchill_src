/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.delete.processors;

import wt.util.resource.*;

@RBUUID("wt.dataops.delete.processors.deleteProcessorsResource")
public final class deleteProcessorsResource_it extends WTListResourceBundle {
   @RBEntry("Elenco oggetti")
   public static final String ADD_RELATED_OBJECTS_LABEL = "0";

   @RBEntry("Modifica lista oggetti")
   public static final String EDIT_OBJECT_LIST_LABEL = "1";

   @RBEntry("Elimina")
   public static final String DELETE_TITLE = "3";

   @RBEntry("Elimina tutte le versioni")
   public static final String DELETE_ALL_VERSIONS = "4";

   @RBEntry("Elimina elementi")
   public static final String DELETE_ITEMS_LABEL = "5";

   @RBEntry("Includi/Escludi")
   public static final String INCLUDE_EXCLUDE = "6";

   @RBEntry("Tutti gli elementi")
   public static final String VIEW_LABEL_ALL_OBJECTS = "7";

   @RBEntry("Esegui azione in background")
   public static final String RUN_IN_BACKGROUND = "8";

   @RBEntry("Non si dispone dei permessi per eliminare questo elemento.")
   public static final String NO_DELETE_PERMISSION = "9";

   @RBEntry("È stato rilevato un problema. Consultare le colonne di stato per ulteriori dettagli.")
   public static final String TOP_STATUS_MESSAGE = "10";

   @RBEntry("Contesto")
   public static final String CONTEXT_LABEL = "11";

   @RBEntry("Elimina tutte le iterazioni di ciascun oggetto incluso nella tabella")
   public static final String DELETE_SINGLE_REVISION = "12";

   @RBEntry("Elimina tutte le revisioni di ciascun oggetto incluso nella tabella")
   public static final String DELETE_ALL_REVISIONS = "13";

   @RBEntry("Elimina le iterazioni più recenti di ciascun oggetto incluso nella tabella")
   public static final String DELETE_LATEST_ITERATION = "14";
}
