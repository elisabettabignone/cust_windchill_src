/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.delete.processors;

import wt.util.resource.*;

@RBUUID("wt.dataops.delete.processors.deleteProcessorsResource")
public final class deleteProcessorsResource extends WTListResourceBundle {
   @RBEntry("Object List")
   public static final String ADD_RELATED_OBJECTS_LABEL = "0";

   @RBEntry("Edit Object List")
   public static final String EDIT_OBJECT_LIST_LABEL = "1";

   @RBEntry("Delete")
   public static final String DELETE_TITLE = "3";

   @RBEntry("Delete All Versions")
   public static final String DELETE_ALL_VERSIONS = "4";

   @RBEntry("Delete Items")
   public static final String DELETE_ITEMS_LABEL = "5";

   @RBEntry("Include/Exclude")
   public static final String INCLUDE_EXCLUDE = "6";

   @RBEntry("All Items")
   public static final String VIEW_LABEL_ALL_OBJECTS = "7";

   @RBEntry("Run action in the background")
   public static final String RUN_IN_BACKGROUND = "8";

   @RBEntry("You do not have permission to delete this item.")
   public static final String NO_DELETE_PERMISSION = "9";

   @RBEntry("Problems exist. Please review the status columns for more details.")
   public static final String TOP_STATUS_MESSAGE = "10";

   @RBEntry("Context")
   public static final String CONTEXT_LABEL = "11";

   @RBEntry("Delete all iterations of each object included in the table")
   public static final String DELETE_SINGLE_REVISION = "12";

   @RBEntry("Delete all revisions of each object included in the table")
   public static final String DELETE_ALL_REVISIONS = "13";

   @RBEntry("Delete latest iterations of each object included in the table")
   public static final String DELETE_LATEST_ITERATION = "14";
}
