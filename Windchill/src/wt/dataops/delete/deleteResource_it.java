/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.delete;

import wt.util.resource.*;

@RBUUID("wt.dataops.delete.deleteResource")
public final class deleteResource_it extends WTListResourceBundle {
   @RBEntry("Elimina oggetti")
   public static final String TASK_NAME = "0";

   @RBEntry("Non esistono elementi da eliminare.")
   public static final String NO_ITEMS_FOR_TASK = "1";

   @RBEntry("Impossibile elaborare una combinazione di oggetti di progetto e non di progetti in un unico task di eliminazione.")
   public static final String OBJECTS_FROM_MULTIPLE_PROJECTS_ERROR = "2";

   @RBEntry("Impossibile eliminare l'ultima iterazione degli oggetti '{0}' perché si tratta dell'unica iterazione rimanente per la revisione")
   public static final String ONLY_ITERATION_OF_VERSION_CONFLICT = "3";
}
