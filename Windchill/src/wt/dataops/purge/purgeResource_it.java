/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.purge;

import wt.util.resource.*;

@RBUUID("wt.dataops.purge.purgeResource")
public final class purgeResource_it extends WTListResourceBundle {
   @RBEntry("ERRORE GRAVE: si è verificata un'eccezione non gestita durante l'elaborazione. Impossibile proseguire con l'operazione di pulizia.\n {0}")
   @RBArgComment0("Exception message")
   public static final String EXCEPTION_OCCURRED = "0";

   @RBEntry("Operazione di pulizia \"{0}\" iniziata alle {1}.")
   @RBArgComment0("The purge operation name")
   @RBArgComment1("The start time")
   public static final String PURGE_STARTED = "1";

   @RBEntry("Operazione di pulizia \"{0}\" completata alle {1}.")
   @RBArgComment0("The purge operation name")
   @RBArgComment1("The finish time")
   public static final String PURGE_FINISHED = "2";

   @RBEntry("Impossibile eliminare gli oggetti. Operazione di pulizia \"{0}\" non riuscita.")
   @RBArgComment0("The purge operation name")
   public static final String PURGE_FAILED = "3";

   @RBEntry("AVVERTENZA: l'operazione di pulizia si è conclusa con i seguenti errori:")
   public static final String PURGE_ERROR = "4";

   @RBEntry("Tentativo di eliminazione permanente di {0} oggetto/i in corso.")
   @RBArgComment0("number of objects")
   public static final String PURGE_ATTEMPT = "5";

   @RBEntry("Messaggio di conflitto: {0}")
   @RBArgComment0("the conflict message")
   public static final String PURGE_CONFLICT_MESSAGE = "6";

   @RBEntry("Nessuno degli oggetti che provocano il conflitto è fra gli oggetti da eliminare. Impossibile proseguire con la pulizia.")
   public static final String CANNOT_RETRY = "7";

   @RBEntry("Attività \"{0}\": l'eliminazione delle attività con stato \"{1}\" non è consentita.")
   @RBArgComment0("Record Name")
   @RBArgComment1("Record status")
   public static final String RECORD_DELETE_PROHIBITED = "8";

   @RBEntry("Informazioni operando pulizia")
   public static final String PURGE_OPERAND_HEADER = "9";

   @RBEntry("Impossibile eliminare gli oggetti. Vedere il log di pulizia per i dettagli.")
   public static final String COULD_NOT_DELETE_ANY_OBJECT = "10";

   @RBEntry("Impossibile eliminare gli oggetti che seguono in quanto sono l'iterazione punto di diramazione.")
   public static final String CANNOT_DELETE_BRANCH_POINTS = "11";
   
   @RBEntry("Impossibile trovare oggetti per l'eliminazione.")
   public static final String NO_OBJECTS_TO_PURGE = "12";
}
