/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.purge;

import wt.util.resource.*;

@RBUUID("wt.dataops.purge.purgeResource")
public final class purgeResource extends WTListResourceBundle {
   @RBEntry("SEVERE ERROR: Unhandled exception occurred during processing. Cannot continue Purge operation.\n {0}")
   @RBArgComment0("Exception message")
   public static final String EXCEPTION_OCCURRED = "0";

   @RBEntry("The Purge operation \"{0}\" has started on {1}.")
   @RBArgComment0("The purge operation name")
   @RBArgComment1("The start time")
   public static final String PURGE_STARTED = "1";

   @RBEntry("The Purge operation \"{0}\" has finished on {1}.")
   @RBArgComment0("The purge operation name")
   @RBArgComment1("The finish time")
   public static final String PURGE_FINISHED = "2";

   @RBEntry("No objects could be purged. The Purge operation \"{0}\" has failed.")
   @RBArgComment0("The purge operation name")
   public static final String PURGE_FAILED = "3";

   @RBEntry("WARNING: Purge resulted in following errors :")
   public static final String PURGE_ERROR = "4";

   @RBEntry("Attempting to purge {0} object(s).")
   @RBArgComment0("number of objects")
   public static final String PURGE_ATTEMPT = "5";

   @RBEntry("Conflict Message : {0}")
   @RBArgComment0("the conflict message")
   public static final String PURGE_CONFLICT_MESSAGE = "6";

   @RBEntry("None of the objects causing conflicts found in objects to be purged. Cannot continue Purge operation.")
   public static final String CANNOT_RETRY = "7";

   @RBEntry("\"{0}\" : Delete not permitted for operation with status \"{1}\".")
   @RBArgComment0("Record Name")
   @RBArgComment1("Record status")
   public static final String RECORD_DELETE_PROHIBITED = "8";

   @RBEntry("Purge Operand Information")
   public static final String PURGE_OPERAND_HEADER = "9";

   @RBEntry("None of the objects could be purged. See purge log for more details.")
   public static final String COULD_NOT_DELETE_ANY_OBJECT = "10";

   @RBEntry("Unable to delete following objects as they are branch point iteration.")
   public static final String CANNOT_DELETE_BRANCH_POINTS = "11";
   
   @RBEntry("No objects were found for purge.")
   public static final String NO_OBJECTS_TO_PURGE = "12";
}
