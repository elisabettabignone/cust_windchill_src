/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.archivesystem.cyaimpl;

import wt.util.resource.*;

@RBUUID("wt.dataops.archivesystem.cyaimpl.cyaimplResource")
public final class cyaimplResource extends WTListResourceBundle {
   /**
    * CYA Archive System related messages
    * CYAArchiveResident
    **/
   @RBEntry("Error constructing object of class {0}.")
   @RBArgComment0("classname")
   public static final String ERROR_CONSTRUCT_OBJ = "0";

   /**
    * CYAArchiveSystem
    **/
   @RBEntry("Not Connected.")
   public static final String NOT_CONNECTED = "1";

   @RBEntry("Empty input data.")
   public static final String EMPTY_INPUT = "2";

   @RBEntry("Query not initialized.")
   public static final String QUERY_NOT_INIT = "3";

   @RBEntry("Query already finalized - use newQuery().")
   public static final String QUERY_FINAL = "4";

   @RBEntry("No type information - use addType(String).")
   public static final String NO_TYPE_INFO = "5";

   @RBEntry("Not able to connect to Windchill Archive Server.")
   public static final String CONNECT_FAILED = "6";
   
   @RBEntry("Unable to open volume.")
   public static final String UNABLE_TO_OPEN_VOLUME = "7";
}
