/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.archivesystem.cyaimpl;

import wt.util.resource.*;

@RBUUID("wt.dataops.archivesystem.cyaimpl.cyaimplResource")
public final class cyaimplResource_it extends WTListResourceBundle {
   /**
    * CYA Archive System related messages
    * CYAArchiveResident
    **/
   @RBEntry("Errore durante la costruzione dell'oggetto di classe {0}.")
   @RBArgComment0("classname")
   public static final String ERROR_CONSTRUCT_OBJ = "0";

   /**
    * CYAArchiveSystem
    **/
   @RBEntry("Nessuna connessione.")
   public static final String NOT_CONNECTED = "1";

   @RBEntry("Dati di input vuoti.")
   public static final String EMPTY_INPUT = "2";

   @RBEntry("Interrogazione non inizializzata.")
   public static final String QUERY_NOT_INIT = "3";

   @RBEntry("Interrogazione già conclusa, utilizzare l'istruzione newQuery().")
   public static final String QUERY_FINAL = "4";

   @RBEntry("Nessuna informazione sul tipo, utilizzare l'istruzione addType(stringa).")
   public static final String NO_TYPE_INFO = "5";

   @RBEntry("Impossibile effettuare la connessione al server di Windchill Archive")
   public static final String CONNECT_FAILED = "6";
   
   @RBEntry("Impossibile aprire il volume.")
   public static final String UNABLE_TO_OPEN_VOLUME = "7";
}
