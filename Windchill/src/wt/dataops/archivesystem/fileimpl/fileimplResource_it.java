/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.archivesystem.fileimpl;

import wt.util.resource.*;

@RBUUID("wt.dataops.archivesystem.fileimpl.fileimplResource")
public final class fileimplResource_it extends WTListResourceBundle {
   /**
    * File Archive System related messages
    * FileArchiveSystem
    **/
   @RBEntry("Il file da archiviare è 'nullo'.")
   public static final String NULL_FILE = "0";

   @RBEntry("{0} non rappresenta un file.")
   @RBArgComment0("file with path")
   public static final String NOT_A_FILE = "1";

   @RBEntry("{0} non rappresenta una directory.")
   @RBArgComment0("directory with path")
   public static final String NOT_A_DIR = "2";

   @RBEntry("Impossibile scrivere in {0}.")
   @RBArgComment0("file or directory with path")
   public static final String CANNOT_WRITE = "3";

   @RBEntry("Impossibile leggere da {0}.")
   @RBArgComment0("file or directory with path")
   public static final String CANNOT_READ = "4";

   @RBEntry("Errore durante la creazione del nuovo oggetto archiviato.")
   public static final String ERROR_NEW_ARCHIVE = "5";

   @RBEntry("Nessuna connessione a un sistema di archivio.")
   public static final String NOT_CONNECTED = "6";

   @RBEntry("Interrogazione già conclusa, utilizzare l'istruzione newQuery().")
   public static final String QUERY_FINALIZED = "7";

   @RBEntry("Non supportato dalla classe di implementazione sistema di archiviazione {0}.")
   @RBArgComment0("classname")
   public static final String NOT_SUPPORTED = "8";

   @RBEntry("Impossibile aggiornare l'indice.")
   public static final String UNABLE_TO_UPDATE_INDEX = "9";

   /**
    * FileArchiveResident
    **/
   @RBEntry("Errore durante la costruzione dell'oggetto di classe {0}.")
   @RBArgComment0("classname")
   public static final String ERROR_CONSTRUCT_OBJ = "10";
}
