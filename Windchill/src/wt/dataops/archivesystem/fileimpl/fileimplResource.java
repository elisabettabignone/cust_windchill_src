/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.archivesystem.fileimpl;

import wt.util.resource.*;

@RBUUID("wt.dataops.archivesystem.fileimpl.fileimplResource")
public final class fileimplResource extends WTListResourceBundle {
   /**
    * File Archive System related messages
    * FileArchiveSystem
    **/
   @RBEntry("File to be archived is 'null'.")
   public static final String NULL_FILE = "0";

   @RBEntry("{0} does not represent a file.")
   @RBArgComment0("file with path")
   public static final String NOT_A_FILE = "1";

   @RBEntry("{0} does not represent a directory.")
   @RBArgComment0("directory with path")
   public static final String NOT_A_DIR = "2";

   @RBEntry("Cannot write to {0}.")
   @RBArgComment0("file or directory with path")
   public static final String CANNOT_WRITE = "3";

   @RBEntry("Cannot read from {0}.")
   @RBArgComment0("file or directory with path")
   public static final String CANNOT_READ = "4";

   @RBEntry("Error creating new Archive Object.")
   public static final String ERROR_NEW_ARCHIVE = "5";

   @RBEntry("Not connected to an Archive System.")
   public static final String NOT_CONNECTED = "6";

   @RBEntry("Query already finalized - use newQuery().")
   public static final String QUERY_FINALIZED = "7";

   @RBEntry("Not Supported by Archive System Implementation class {0}.")
   @RBArgComment0("classname")
   public static final String NOT_SUPPORTED = "8";

   @RBEntry("Unable to update index")
   public static final String UNABLE_TO_UPDATE_INDEX = "9";

   /**
    * FileArchiveResident
    **/
   @RBEntry("Error constructing object of class {0}.")
   @RBArgComment0("classname")
   public static final String ERROR_CONSTRUCT_OBJ = "10";
}
