/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.archivesystem;

import wt.util.resource.*;

@RBUUID("wt.dataops.archivesystem.prefArchivesystemResource")
public final class prefArchivesystemResource extends WTListResourceBundle {
   /**
    * Entry
    **/
   @RBEntry("Archive System")
   public static final String PREFERENCE_ARCHIVESYSTEM_CATEGORY = "PREFERENCE_ARCHIVESYSTEM_CATEGORY";

   @RBEntry("Archive Volume")
   @RBComment("Display name for Archive Volume option in Preferences client")
   public static final String ARCHIVE_VOLUME_DISPLAY = "ARCHIVE_VOLUME_DISPLAY";

   @RBEntry("The name of the volume where the archive is stored.")
   @RBComment("Description for Archive Volume option in the Preferences client")
   public static final String ARCHIVE_VOLUME_DESCRIPTION = "ARCHIVE_VOLUME_DESCRIPTION";

   @RBEntry("This property is used by the archive system. The value and format of this data depends on the archive system being configured.")
   @RBComment("Long description for Archive Volume option in the Preferences client")
   public static final String ARCHIVE_VOLUME_LONG_DESCRIPTION = "ARCHIVE_VOLUME_LONG_DESCRIPTION";

   @RBEntry("Archive Retention Period")
   @RBComment("Display name for Archive Retention Period option in Preferences client.")
   public static final String ARCHIVE_RETENTIONPERIOD_DISPLAY = "ARCHIVE_RETENTIONPERIOD_DISPLAY";

   @RBEntry("Number of years the archive is retained.")
   @RBComment("Description for Archive Retention Period option in the Preferences client.")
   public static final String ARCHIVE_RETENTIONPERIOD_DESCRIPTION = "ARCHIVE_RETENTIONPERIOD_DESCRIPTION";

   @RBEntry("This property is used by the archive system. This value is the number of years the archives are retained in the archive system. The archive system does not allow deletion of the archives prior to the retention period, nor does it allow the retention period to be curtailed by a newer, shorter setting.")
   @RBComment("Long description for Archive Retention Period in the Preferences client")
   public static final String ARCHIVE_RETENTIONPERIOD_LONG_DESCRIPTION = "ARCHIVE_RETENTIONPERIOD_LONG_DESCRIPTION";

   @RBEntry("Archive Search Volumes")
   @RBComment("Display name for Archive Serach Volumes option in Preferences client.")
   public static final String ARCHIVE_SERACHVOLUMES_DISPLAY = "ARCHIVE_SERACHVOLUMES_DISPLAY";

   @RBEntry("List of archive volumes to search for archives.")
   @RBComment("Description for Archive Serach Volumes option in the Preferences client.")
   public static final String ARCHIVE_SERACHVOLUMES_DESCRIPTION = "ARCHIVE_SERACHVOLUMES_DESCRIPTION";

   @RBEntry("This property is used by the archive system. Commas separate the list of archive volumes. A search for the archives is performed on each of the volumes defined in this list. The name of the archive volume depends on the archive system being configured.")
   @RBComment("Long description for Archive Serach Volumes option in the Preferences client")
   public static final String ARCHIVE_SERACHVOLUMES_LONG_DESCRIPTION = "ARCHIVE_SERACHVOLUMES_LONG_DESCRIPTION";
}
