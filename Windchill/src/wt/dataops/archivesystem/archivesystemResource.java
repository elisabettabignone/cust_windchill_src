/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.archivesystem;

import wt.util.resource.*;

@RBUUID("wt.dataops.archivesystem.archivesystemResource")
public final class archivesystemResource extends WTListResourceBundle {
   /**
    * Archive System related messages
    * WTArchiveSystemFactory
    **/
   @RBEntry("Could not instantiate archive system for {0}.")
   @RBArgComment0("The Archive System classname")
   public static final String COULD_NOT_INSTANTIATE_AS = "0";

   /**
    * WTArchiveSystemHelper
    **/
   @RBEntry("Preference {0} not found.")
   @RBArgComment0("archiveVolume")
   public static final String PREF_NOT_FOUND = "1";

   /**
    * WTArchiveSystemFactory
    **/
   @RBEntry("No implementation class specified for {0}.")
   @RBArgComment0("The Archive Type")
   public static final String NO_IMPL_CLASS = "2";

   /**
    * archivesearch.jjt
    **/
   @RBEntry("Encountered null query string.")
   public static final String ERROR_NULL_QUERY_STRING = "3";

   @RBEntry("Encountered erros parsing query string:  {0}.")
   @RBArgComment0("The query string")
   public static final String ERROR_PARSE_QUERY = "4";

   /**
    * ASTClauseExpr
    **/
   @RBEntry("Found unmatched attribute name in parse string:  {0}.")
   @RBArgComment0("The unmatched attr")
   public static final String UNMATCHED_ATTR = "5";

   @RBEntry("No matching parent found in the dictionary:  {0}.")
   @RBArgComment0("The unmatched parent")
   public static final String UNMATCHED_PARENT = "6";

   /**
    * ASTTopNode
    **/
   @RBEntry("Type clause absent in query string.")
   public static final String NO_TYPE_CLAUSE = "7";

   /**
    * WTArchiveSystemHelper
    **/
   @RBEntry("Error creating search hints file.")
   public static final String ERROR_CREATE_SHINTS = "8";

   /**
    * WTArchiveSystemQUERY
    **/
   @RBEntry("Incorrectly built query.")
   public static final String INCORRECT_QUERY = "9";

   /**
    * WTArchiveSystemHelper
    **/
   @RBEntry("File not found: {0}.")
   @RBArgComment0("filename")
   public static final String FILE_NOT_FOUND = "10";

   @RBEntry("Insufficient privileges to read file: {0}.")
   @RBArgComment0("filename")
   public static final String CANT_READ_FILE = "11";

   @RBEntry("Failed to load preferences.")
   public static final String FAILURE_LOADING_PREF = "12";
}
