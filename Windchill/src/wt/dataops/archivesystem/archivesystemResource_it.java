/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.archivesystem;

import wt.util.resource.*;

@RBUUID("wt.dataops.archivesystem.archivesystemResource")
public final class archivesystemResource_it extends WTListResourceBundle {
   /**
    * Archive System related messages
    * WTArchiveSystemFactory
    **/
   @RBEntry("Impossibile istanziare il sistema di archiviazione per {0}.")
   @RBArgComment0("The Archive System classname")
   public static final String COULD_NOT_INSTANTIATE_AS = "0";

   /**
    * WTArchiveSystemHelper
    **/
   @RBEntry("Preferenza {0} non trovata")
   @RBArgComment0("archiveVolume")
   public static final String PREF_NOT_FOUND = "1";

   /**
    * WTArchiveSystemFactory
    **/
   @RBEntry("Classe di implementazione non specificata per {0}.")
   @RBArgComment0("The Archive Type")
   public static final String NO_IMPL_CLASS = "2";

   /**
    * archivesearch.jjt
    **/
   @RBEntry("Rilevata stringa di interrogazione vuota.")
   public static final String ERROR_NULL_QUERY_STRING = "3";

   @RBEntry("Errori rilevati durante l'analisi della stringa di interrogazione: {0}.")
   @RBArgComment0("The query string")
   public static final String ERROR_PARSE_QUERY = "4";

   /**
    * ASTClauseExpr
    **/
   @RBEntry("Nome di attributo non corrispondente rilevato nella stringa di analisi: {0}.")
   @RBArgComment0("The unmatched attr")
   public static final String UNMATCHED_ATTR = "5";

   @RBEntry("Impossibile trovare padri corrispondenti nel dizionario: {0}.")
   @RBArgComment0("The unmatched parent")
   public static final String UNMATCHED_PARENT = "6";

   /**
    * ASTTopNode
    **/
   @RBEntry("Tipo di proposizione mancante nella stringa di interrogazione.")
   public static final String NO_TYPE_CLAUSE = "7";

   /**
    * WTArchiveSystemHelper
    **/
   @RBEntry("Errore durante la creazione del file degli hint di ricerca.")
   public static final String ERROR_CREATE_SHINTS = "8";

   /**
    * WTArchiveSystemQUERY
    **/
   @RBEntry("Interrogazione formulata in modo non corretto.")
   public static final String INCORRECT_QUERY = "9";

   /**
    * WTArchiveSystemHelper
    **/
   @RBEntry("Impossibile trovare il file: {0}.")
   @RBArgComment0("filename")
   public static final String FILE_NOT_FOUND = "10";

   @RBEntry("Non si dispone dei privilegi per la lettura del file: {0}.")
   @RBArgComment0("filename")
   public static final String CANT_READ_FILE = "11";

   @RBEntry("Caricamento delle preferenze non riuscito.")
   public static final String FAILURE_LOADING_PREF = "12";
}
