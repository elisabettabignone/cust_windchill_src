/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.archivesystem;

import wt.util.resource.*;

@RBUUID("wt.dataops.archivesystem.prefArchivesystemResource")
public final class prefArchivesystemResource_it extends WTListResourceBundle {
   /**
    * Entry
    **/
   @RBEntry("Sistema di archiviazione")
   public static final String PREFERENCE_ARCHIVESYSTEM_CATEGORY = "PREFERENCE_ARCHIVESYSTEM_CATEGORY";

   @RBEntry("Volume archivio")
   @RBComment("Display name for Archive Volume option in Preferences client")
   public static final String ARCHIVE_VOLUME_DISPLAY = "ARCHIVE_VOLUME_DISPLAY";

   @RBEntry("Nome del volume in cui è memorizzato l'archivio.")
   @RBComment("Description for Archive Volume option in the Preferences client")
   public static final String ARCHIVE_VOLUME_DESCRIPTION = "ARCHIVE_VOLUME_DESCRIPTION";

   @RBEntry("Questa proprietà viene utilizzata dal sistema di archiviazione. Il valore e il formato di questi dati dipendono dal sistema di archiviazione configurato.")
   @RBComment("Long description for Archive Volume option in the Preferences client")
   public static final String ARCHIVE_VOLUME_LONG_DESCRIPTION = "ARCHIVE_VOLUME_LONG_DESCRIPTION";

   @RBEntry("Periodo di ritenzione archivio")
   @RBComment("Display name for Archive Retention Period option in Preferences client.")
   public static final String ARCHIVE_RETENTIONPERIOD_DISPLAY = "ARCHIVE_RETENTIONPERIOD_DISPLAY";

   @RBEntry("Numero di anni per i quali verrà conservato l'archivio.")
   @RBComment("Description for Archive Retention Period option in the Preferences client.")
   public static final String ARCHIVE_RETENTIONPERIOD_DESCRIPTION = "ARCHIVE_RETENTIONPERIOD_DESCRIPTION";

   @RBEntry("Questa proprietà viene utilizzata dal sistema di archiviazione. Questo valore indica il numero di anni che verranno conservati gli archivi nel sistema di archiviazione. Il sistema di archiviazione non consente l'eliminazione degli archivi prima del periodo di ritenzione né la riduzione del periodo di ritenzione.")
   @RBComment("Long description for Archive Retention Period in the Preferences client")
   public static final String ARCHIVE_RETENTIONPERIOD_LONG_DESCRIPTION = "ARCHIVE_RETENTIONPERIOD_LONG_DESCRIPTION";

   @RBEntry("Volumi archivio di ricerca")
   @RBComment("Display name for Archive Serach Volumes option in Preferences client.")
   public static final String ARCHIVE_SERACHVOLUMES_DISPLAY = "ARCHIVE_SERACHVOLUMES_DISPLAY";

   @RBEntry("Elenco di volumi archivio in cui ricercare gli archivi.")
   @RBComment("Description for Archive Serach Volumes option in the Preferences client.")
   public static final String ARCHIVE_SERACHVOLUMES_DESCRIPTION = "ARCHIVE_SERACHVOLUMES_DESCRIPTION";

   @RBEntry("Questa proprietà viene utilizzata dal sistema di archiviazione. I volumi di archivio nell'elenco sono separati da virgole. La ricerca degli archivi viene eseguita su ognuno dei volumi definiti nell'elenco. Il nome del volume dipende dal sistema di archiviazione configurato.")
   @RBComment("Long description for Archive Serach Volumes option in the Preferences client")
   public static final String ARCHIVE_SERACHVOLUMES_LONG_DESCRIPTION = "ARCHIVE_SERACHVOLUMES_LONG_DESCRIPTION";
}
