/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.objectcol;

import wt.util.resource.*;

@RBUUID("wt.dataops.objectcol.objectcolResource")
public final class objectcolResource extends WTListResourceBundle {
   /**
    * Object collection related messages
    **/
   @RBEntry("Invalid Input Type: {0}")
   @RBArgComment0("The type of InputValue")
   public static final String INVALID_INPUT_TYPE = "0";

   @RBEntry("Cannot create generic collection query processor.")
   public static final String CANNOT_CREATE_GENERIC_COL_QUERY_PROC = "1";

   @RBEntry("Cannot add criterion object to persistent, non-editable criteria: {0}")
   @RBArgComment0("Criteria name")
   public static final String CANNOT_ADD_CRITERION_TO_PERSISTENT_CRITERIA = "2";

   @RBEntry("Object is already persistent: {0}")
   @RBArgComment0("Object identity")
   public static final String OBJECT_IS_ALREADY_PERSISTENT = "3";

   @RBEntry("Multiple criteria found for name: {0}")
   @RBArgComment0("name of criteria")
   public static final String MULTIPLE_CRITERIA_FOR_NAME = "4";

   @RBEntry("Criteria does not contain any criterion objects: {0}")
   @RBArgComment0("Criteria name")
   public static final String CRITERIA_CONTAINS_NO_CRITERION_OBJECTS = "5";

   @RBEntry("Non supported \"condition\" type. Supported types are: \"relationship\", \"attr\" and \"objtype\"")
   public static final String NON_SUPPORTED_CONDITION_TYPE = "6";

   @RBEntry("Non supported \"content\" type for criterion definition. Supported types are: \"condition\" and \"filter\"")
   public static final String NON_SUPPORTED_CONTENT_TYPE_FOR_DEF = "7";

   @RBEntry("Context is not of type WTContainer: {0}")
   @RBArgComment0("Type of context ")
   public static final String WRONG_CLASS_FOR_CONTAINER_OBJ = "8";

   @RBEntry("Context reference resolves to a NULL object.")
   public static final String CONTEXT_REFERENCE_RESOLVES_TO_NULL = "9";

   @RBEntry("Criteria does not contain any context reference")
   public static final String CRITERIA_CONTAINS_NO_CONTEXT = "10";

   @RBEntry("The criterion object array is null")
   public static final String CRITERION_OBJECT_ARRAY_NULL = "11";

   @RBEntry("The number of criterion objects and inputs do not match")
   public static final String MISMATCH_IN_NUMBER_OF_CRITERION_OBJECTS_AND_INPUTS = "12";

   @RBEntry("Invalid inputs for criterion definition: {0}")
   @RBArgComment0("the name of criterion definition that the inputs are invalid for")
   public static final String INVALID_INPUTS_FOR_CRITERION_DEFINITION = "13";

   @RBEntry("Cannot delete object because it has been used: {0}")
   @RBArgComment0("the object that cannot be deleted since it is used")
   public static final String CANNOT_DELETE_USED_OBJECT = "14";

   @RBEntry("The current principal is not authorized to create the criteria in default domain of: {0}")
   @RBArgComment0("container that user is not authorized to create criteria in.")
   public static final String NOT_AUTHORIZED_EXCEPTION = "15";

   @RBEntry("Cannot remove criterion object from persistent, non-editable criteria: {0}")
   @RBArgComment0("criteria name")
   public static final String CANNOT_REMOVE_CRITERION_FRM_PERSISTENT_CRITERIA = "16";

   @RBEntry("Multiple criterion definitions found for name: {0}")
   @RBArgComment0("name of criterion definition")
   public static final String MULTIPLE_CRITERION_DEFINITIONS_FOR_NAME = "17";

   @RBEntry("Multiple relationship maps found for name: {0}")
   @RBArgComment0("name of relationship map")
   public static final String MULTIPLE_RELATIONSHIP_MAPS_FOR_NAME = "18";

   @RBEntry("Non supported \"related\" type. Supported types are: \"by-method\" and \"by-relationship\"")
   public static final String NON_SUPPORTED_RELATED_TYPE = "19";

   @RBEntry("The input value array is null")
   public static final String INPUT_VALUE_ARRAY_NULL = "20";

   @RBEntry("Invalid inputs for relationship map definition: {0}")
   @RBArgComment0("the name of relationship map definition that the inputs are invalid for")
   public static final String INVALID_INPUTS_FOR_RELATIONSHIP_MAP_DEFINITION = "21";

   @RBEntry("Cannot add criterion object to null criteria.")
   public static final String CANNOT_ADD_CRITERION_TO_NULL_CRITERIA = "22";

   @RBEntry("Criteria must have a valid object type criterion object.")
   public static final String CRITERIA_MUST_HAVE_OBJECTTYPE_CRITERION = "23";

   @RBEntry("Cannot use null container reference to find collection criteria.")
   public static final String CANNOT_FIND_CRITERIA_IN_NULL_CONTAINER = "24";

   @RBEntry("Cannot use null container reference to find relationship map.")
   public static final String CANNOT_FIND_RELATIONSHIPMAP_IN_NULL_CONTAINER = "25";

   @RBEntry("The string is not in the localizable format: {0}")
   @RBArgComment0("the string that is not in the correct format for localization")
   public static final String NOT_LOCALIZABLE_FORMAT = "26";

   @RBEntry("The string is not in the <resource bundle>:<key> format: {0}")
   @RBArgComment0("the string that is not in the correct format for parsing into resource bundle and key")
   public static final String NOT_RB_KEY_FORMAT = "27";

   @RBEntry("Cannot export null collection criteria and relationship map.")
   public static final String CANNOT_EXPORT_NULL_CRITERIA_N_RELATIONSHIP_MAP = "28";

   @RBEntry("{0} : Cleanup schedule queue entry to run at : {1}")
   public static final String RUN_QUEUE = "29";

   @RBEntry("Cannot update non persisted criteria : {0}")
   public static final String CANNOT_UPDATE_NON_PERSISTED_OBJECT = "30";

   @RBEntry("Invalid Collection Criteria. Criterion {0} is not valid with criterion {1}")
   public static final String CRITERIONS_NOT_VALID_WITH = "31";

   @RBEntry("Invalid Collection Criteria. Criterion {0} is duplicated")
   public static final String CRITERIONS_DUPLICATE = "32";

   @RBEntry(" {0} is disabled.")
   public static final String CRITERIA_DISABLED = "33";

   @RBEntry("Input values are not expected in the file")
   public static final String INPUT_VALUE_NOT_EXPECTED = "34";

   @RBEntry("{0} not found")
   public static final String FILE_NOT_PRESENT = "35";

   @RBEntry("Context Id")
   public static final String PRIVATE_CONSTANT_0 = "ContextCriterionDef_name";

   @RBEntry("Select Context Id to search in")
   public static final String PRIVATE_CONSTANT_1 = "ContextCriterionDef_desc";

   @RBEntry("Create Date")
   public static final String PRIVATE_CONSTANT_2 = "CreateDateCriterionDef_name";

   @RBEntry("Find object created before, between or after the input date(s)")
   public static final String PRIVATE_CONSTANT_3 = "CreateDateCriterionDef_desc";

   @RBEntry("Last Modified")
   public static final String PRIVATE_CONSTANT_4 = "ModifiedDateCriterionDef_name";

   @RBEntry("Find object modified before, between or after the input date(s)")
   public static final String PRIVATE_CONSTANT_5 = "ModifiedDateCriterionDef_desc";

   @RBEntry("Created By")
   public static final String PRIVATE_CONSTANT_6 = "CreatedByCriterionDef_name";

   @RBEntry("Select objects created by")
   public static final String PRIVATE_CONSTANT_7 = "CreatedByCriterionDef_desc";

   @RBEntry("Modified By")
   public static final String PRIVATE_CONSTANT_8 = "ModifiedByCriterionDef_name";

   @RBEntry("Select objects modified by")
   public static final String PRIVATE_CONSTANT_9 = "ModifiedByCriterionDef_desc";

   @RBEntry("Folder Membership")
   public static final String PRIVATE_CONSTANT_10 = "FolderReferenceCriterionDef_name";

   @RBEntry("Find objects that are members of a folder")
   public static final String PRIVATE_CONSTANT_11 = "FolderReferenceCriterionDef_desc";

   @RBEntry("Iteration")
   public static final String PRIVATE_CONSTANT_12 = "IterationCriterionDef_name";

   @RBEntry("Select the following iterations")
   public static final String PRIVATE_CONSTANT_13 = "IterationCriterionDef_desc";

   @RBEntry("Lifecycle State")
   public static final String PRIVATE_CONSTANT_14 = "LifecycleStateCriterionDef_name";

   @RBEntry("Find object in lifecycle state")
   public static final String PRIVATE_CONSTANT_15 = "LifecycleStateCriterionDef_desc";

   @RBEntry("Object Type")
   public static final String PRIVATE_CONSTANT_16 = "ObjectTypeCriterionDef_name";

   @RBEntry("Select objects of these types")
   public static final String PRIVATE_CONSTANT_17 = "ObjectTypeCriterionDef_desc";

   @RBEntry("Archive Dependency Map")
   public static final String PRIVATE_CONSTANT_18 = "ArchiveRelationshipMap_name";

   @RBEntry("Collect objects that are required for a complete archive")
   public static final String PRIVATE_CONSTANT_19 = "ArchiveRelationshipMap_desc";

   @RBEntry("Version at Maturity")
   public static final String PRIVATE_CONSTANT_20 = "VersionAtMaturityCriterionDef_name";

   @RBEntry("Find object version at following state")
   public static final String PRIVATE_CONSTANT_21 = "VersionAtMaturityCriterionDef_desc";

   @RBEntry("Version")
   public static final String PRIVATE_CONSTANT_22 = "VersionCriterionDef_name";

   @RBEntry("Select the following versions")
   public static final String PRIVATE_CONSTANT_23 = "VersionCriterionDef_desc";

   @RBEntry("View")
   public static final String PRIVATE_CONSTANT_24 = "ViewCriterionDef_name";

   @RBEntry("Find object in a given view")
   public static final String PRIVATE_CONSTANT_25 = "ViewCriterionDef_desc";

   @RBEntry("Iteration")
   public static final String PRIVATE_CONSTANT_26 = "IterationSelectCriterionDef_name";

   @RBEntry("Select all or latest or all but the latest iterations of the objects.")
   public static final String PRIVATE_CONSTANT_27 = "IterationSelectCriterionDef_desc";

   @RBEntry("Domain Membership")
   public static final String PRIVATE_CONSTANT_28 = "DomainCriterionDef_name";

   @RBEntry("Find objects belonging to this domain")
   public static final String PRIVATE_CONSTANT_29 = "DomainCriterionDef_desc";

   @RBEntry("Note holder note dependency map")
   public static final String PRIVATE_CONSTANT_30 = "NoteHolderNoteRelationshipMap_name";

   @RBEntry("Collect objects that are required for a complete archive")
   public static final String PRIVATE_CONSTANT_31 = "NoteHolderNoteRelationshipMap_desc";

   @RBEntry("publish content dependency map")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_32 = "publishContentRelationshipMap_name";

   @RBEntry("Collect representable objects that are required for a complete archive if Arbortext Content Manager is installed")
   public static final String PRIVATE_CONSTANT_33 = "publishContentRelationshipMap_desc";

   @RBEntry("configurable links dependency map")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_34 = "configurableLinksRelationshipMap_name";

   @RBEntry("Collect configurable links that are required for a complete archive")
   public static final String PRIVATE_CONSTANT_35 = "configurableLinksRelationshipMap_desc";

   @RBEntry("Changeable to change object relationship map.")
   public static final String PRIVATE_CONSTANT_36 = "ChangeableChangeObjectRelationshipMap_name";

   @RBEntry("Collect objects that are required for a complete archive.")
   public static final String PRIVATE_CONSTANT_37 = "ChangeableChangeObjectRelationshipMap_desc";
}
