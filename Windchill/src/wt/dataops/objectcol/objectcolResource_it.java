/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.objectcol;

import wt.util.resource.*;

@RBUUID("wt.dataops.objectcol.objectcolResource")
public final class objectcolResource_it extends WTListResourceBundle {
   /**
    * Object collection related messages
    **/
   @RBEntry("Tipo di input non valido: {0}")
   @RBArgComment0("The type of InputValue")
   public static final String INVALID_INPUT_TYPE = "0";

   @RBEntry("Impossibile creare elaboratore di interrogazione per raccolta generica.")
   public static final String CANNOT_CREATE_GENERIC_COL_QUERY_PROC = "1";

   @RBEntry("Impossibile aggiungere oggetto criterio a criteri persistenti e non modificabili: {0}")
   @RBArgComment0("Criteria name")
   public static final String CANNOT_ADD_CRITERION_TO_PERSISTENT_CRITERIA = "2";

   @RBEntry("L'oggetto è già persistente: {0}")
   @RBArgComment0("Object identity")
   public static final String OBJECT_IS_ALREADY_PERSISTENT = "3";

   @RBEntry("Trovati più criteri per il nome: {0}")
   @RBArgComment0("name of criteria")
   public static final String MULTIPLE_CRITERIA_FOR_NAME = "4";

   @RBEntry("I criteri non contengono oggetti criterio: {0}")
   @RBArgComment0("Criteria name")
   public static final String CRITERIA_CONTAINS_NO_CRITERION_OBJECTS = "5";

   @RBEntry("Tipo \"condition\" non supportato. I tipi supportati sono: \"relationship\", \"attr\" e \"objtype\"")
   public static final String NON_SUPPORTED_CONDITION_TYPE = "6";

   @RBEntry("Tipo \"content\" non supportato per la definizione di criterio. I tipi supportati sono: \"condition\" e \"filter\"")
   public static final String NON_SUPPORTED_CONTENT_TYPE_FOR_DEF = "7";

   @RBEntry("Il contesto non è di tipo WTContainer: {0}")
   @RBArgComment0("Type of context ")
   public static final String WRONG_CLASS_FOR_CONTAINER_OBJ = "8";

   @RBEntry("Il riferimento del contesto corrisponde a un oggetto vuoto.")
   public static final String CONTEXT_REFERENCE_RESOLVES_TO_NULL = "9";

   @RBEntry("I criteri non contengono riferimenti a contesto")
   public static final String CRITERIA_CONTAINS_NO_CONTEXT = "10";

   @RBEntry("L'array degli oggetti criterio è vuoto")
   public static final String CRITERION_OBJECT_ARRAY_NULL = "11";

   @RBEntry("Numero di oggetti criterio e di input non corrispondente")
   public static final String MISMATCH_IN_NUMBER_OF_CRITERION_OBJECTS_AND_INPUTS = "12";

   @RBEntry("Input non validi per la definizione di criterio: {0}")
   @RBArgComment0("the name of criterion definition that the inputs are invalid for")
   public static final String INVALID_INPUTS_FOR_CRITERION_DEFINITION = "13";

   @RBEntry("Impossibile eliminare l'oggetto che segue perché utilizzato: {0}")
   @RBArgComment0("the object that cannot be deleted since it is used")
   public static final String CANNOT_DELETE_USED_OBJECT = "14";

   @RBEntry("L'utente/gruppo/ruolo corrente non dispone dei permessi per creare i criteri nel dominio di default di: {0}")
   @RBArgComment0("container that user is not authorized to create criteria in.")
   public static final String NOT_AUTHORIZED_EXCEPTION = "15";

   @RBEntry("Impossibile rimuovere oggetto criterio da criteri persistenti e non modificabili: {0}")
   @RBArgComment0("criteria name")
   public static final String CANNOT_REMOVE_CRITERION_FRM_PERSISTENT_CRITERIA = "16";

   @RBEntry("Trovate più definizioni di criterio per il nome: {0}")
   @RBArgComment0("name of criterion definition")
   public static final String MULTIPLE_CRITERION_DEFINITIONS_FOR_NAME = "17";

   @RBEntry("Trovate più mappature di relazione per il nome: {0}")
   @RBArgComment0("name of relationship map")
   public static final String MULTIPLE_RELATIONSHIP_MAPS_FOR_NAME = "18";

   @RBEntry("Tipo \"related\" non supportato. I tipi supportati sono: \"by-method\" e \"by-relationship\"")
   public static final String NON_SUPPORTED_RELATED_TYPE = "19";

   @RBEntry("L'array dei valori di input è vuoto")
   public static final String INPUT_VALUE_ARRAY_NULL = "20";

   @RBEntry("Input non validi per la definizione di mappature di relazione: {0}")
   @RBArgComment0("the name of relationship map definition that the inputs are invalid for")
   public static final String INVALID_INPUTS_FOR_RELATIONSHIP_MAP_DEFINITION = "21";

   @RBEntry("Impossibile aggiungere oggetto criterio a criteri nulli.")
   public static final String CANNOT_ADD_CRITERION_TO_NULL_CRITERIA = "22";

   @RBEntry("I criteri devono avere oggetti criterio con tipo oggetto valido.")
   public static final String CRITERIA_MUST_HAVE_OBJECTTYPE_CRITERION = "23";

   @RBEntry("Impossibile utilizzare riferimenti contenitore nulli per individuare i criteri di raccolta.")
   public static final String CANNOT_FIND_CRITERIA_IN_NULL_CONTAINER = "24";

   @RBEntry("Impossibile utilizzare riferimenti contenitore nulli per individuare la mappatura delle relazioni.")
   public static final String CANNOT_FIND_RELATIONSHIPMAP_IN_NULL_CONTAINER = "25";

   @RBEntry("Stringa non in formato localizzabile: {0}")
   @RBArgComment0("the string that is not in the correct format for localization")
   public static final String NOT_LOCALIZABLE_FORMAT = "26";

   @RBEntry("Stringa non in formato <resource bundle>:<chiave>: {0}")
   @RBArgComment0("the string that is not in the correct format for parsing into resource bundle and key")
   public static final String NOT_RB_KEY_FORMAT = "27";

   @RBEntry("Impossibile esportare criteri di raccolta e mappatura delle relazioni nulli.")
   public static final String CANNOT_EXPORT_NULL_CRITERIA_N_RELATIONSHIP_MAP = "28";

   @RBEntry("{0}: elemento coda pulizia programmata da eseguire alle  {1}")
   public static final String RUN_QUEUE = "29";

   @RBEntry("Impossibile aggiornare i criteri non persistenti: {0}")
   public static final String CANNOT_UPDATE_NON_PERSISTED_OBJECT = "30";

   @RBEntry("Criteri di raccolta non validi. Il criterio {0} non può essere specificato unitamente al criterio {1}")
   public static final String CRITERIONS_NOT_VALID_WITH = "31";

   @RBEntry("Criteri di raccolta non validi. Il criterio {0} è duplicato")
   public static final String CRITERIONS_DUPLICATE = "32";

   @RBEntry(" {0} è disattivato.")
   public static final String CRITERIA_DISABLED = "33";

   @RBEntry("Non è previsto l'input di valori nel file")
   public static final String INPUT_VALUE_NOT_EXPECTED = "34";

   @RBEntry("Impossibile trovare {0}")
   public static final String FILE_NOT_PRESENT = "35";

   @RBEntry("Id contesto")
   public static final String PRIVATE_CONSTANT_0 = "ContextCriterionDef_name";

   @RBEntry("Seleziona l'identificatore del contesto in cui effettuare la ricerca")
   public static final String PRIVATE_CONSTANT_1 = "ContextCriterionDef_desc";

   @RBEntry("Data di creazione")
   public static final String PRIVATE_CONSTANT_2 = "CreateDateCriterionDef_name";

   @RBEntry("Trova oggetti creati prima o dopo la data di input oppure fra le date di input")
   public static final String PRIVATE_CONSTANT_3 = "CreateDateCriterionDef_desc";

   @RBEntry("Data ultima modifica")
   public static final String PRIVATE_CONSTANT_4 = "ModifiedDateCriterionDef_name";

   @RBEntry("Trova oggetti modificati prima o dopo la data di input oppure fra le date di input")
   public static final String PRIVATE_CONSTANT_5 = "ModifiedDateCriterionDef_desc";

   @RBEntry("Autore")
   public static final String PRIVATE_CONSTANT_6 = "CreatedByCriterionDef_name";

   @RBEntry("Seleziona gli oggetti di un dato autore")
   public static final String PRIVATE_CONSTANT_7 = "CreatedByCriterionDef_desc";

   @RBEntry("Autore modifiche")
   public static final String PRIVATE_CONSTANT_8 = "ModifiedByCriterionDef_name";

   @RBEntry("Seleziona gli oggetti di un dato autore modifiche")
   public static final String PRIVATE_CONSTANT_9 = "ModifiedByCriterionDef_desc";

   @RBEntry("Appartenenza cartella")
   public static final String PRIVATE_CONSTANT_10 = "FolderReferenceCriterionDef_name";

   @RBEntry("Trova oggetti che appartengono a una data cartella")
   public static final String PRIVATE_CONSTANT_11 = "FolderReferenceCriterionDef_desc";

   @RBEntry("Iterazione")
   public static final String PRIVATE_CONSTANT_12 = "IterationCriterionDef_name";

   @RBEntry("Seleziona le iterazioni che seguono")
   public static final String PRIVATE_CONSTANT_13 = "IterationCriterionDef_desc";

   @RBEntry("Stato del ciclo di vita")
   public static final String PRIVATE_CONSTANT_14 = "LifecycleStateCriterionDef_name";

   @RBEntry("Trova oggetti con un dato stato del ciclo di vita")
   public static final String PRIVATE_CONSTANT_15 = "LifecycleStateCriterionDef_desc";

   @RBEntry("Tipo di oggetto")
   public static final String PRIVATE_CONSTANT_16 = "ObjectTypeCriterionDef_name";

   @RBEntry("Seleziona oggetti di un dato tipo")
   public static final String PRIVATE_CONSTANT_17 = "ObjectTypeCriterionDef_desc";

   @RBEntry("Mappatura dipendenza archivio")
   public static final String PRIVATE_CONSTANT_18 = "ArchiveRelationshipMap_name";

   @RBEntry("Raccoglie gli oggetti necessari per un archivio completo")
   public static final String PRIVATE_CONSTANT_19 = "ArchiveRelationshipMap_desc";

   @RBEntry("Versione a maturità")
   public static final String PRIVATE_CONSTANT_20 = "VersionAtMaturityCriterionDef_name";

   @RBEntry("Trova la versione degli oggetti al livello di stato del ciclo di vita che segue")
   public static final String PRIVATE_CONSTANT_21 = "VersionAtMaturityCriterionDef_desc";

   @RBEntry("Versione")
   public static final String PRIVATE_CONSTANT_22 = "VersionCriterionDef_name";

   @RBEntry("Seleziona le versioni che seguono")
   public static final String PRIVATE_CONSTANT_23 = "VersionCriterionDef_desc";

   @RBEntry("Vista")
   public static final String PRIVATE_CONSTANT_24 = "ViewCriterionDef_name";

   @RBEntry("Trova gli oggetti in una data vista")
   public static final String PRIVATE_CONSTANT_25 = "ViewCriterionDef_desc";

   @RBEntry("Iterazione")
   public static final String PRIVATE_CONSTANT_26 = "IterationSelectCriterionDef_name";

   @RBEntry("Seleziona tutte le iterazioni di oggetti o tutte tranne la più recente")
   public static final String PRIVATE_CONSTANT_27 = "IterationSelectCriterionDef_desc";

   @RBEntry("Appartenenza dominio")
   public static final String PRIVATE_CONSTANT_28 = "DomainCriterionDef_name";

   @RBEntry("Trova gli oggetti che appartengono a questo dominio")
   public static final String PRIVATE_CONSTANT_29 = "DomainCriterionDef_desc";

   @RBEntry("Mappa di dipendenza fra nota ed oggetto annotato")
   public static final String PRIVATE_CONSTANT_30 = "NoteHolderNoteRelationshipMap_name";

   @RBEntry("Raccoglie gli oggetti necessari per un archivio completo")
   public static final String PRIVATE_CONSTANT_31 = "NoteHolderNoteRelationshipMap_desc";

   @RBEntry("publish content dependency map")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_32 = "publishContentRelationshipMap_name";

   @RBEntry("Raccoglie gli oggetti rappresentabili necessari per un archivio completo se è installato Arbortext Content Manager.")
   public static final String PRIVATE_CONSTANT_33 = "publishContentRelationshipMap_desc";

   @RBEntry("configurable links dependency map")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_34 = "configurableLinksRelationshipMap_name";

   @RBEntry("Raccoglie i link configurabili necessari per un archivio completo")
   public static final String PRIVATE_CONSTANT_35 = "configurableLinksRelationshipMap_desc";

   @RBEntry("Mappatura relazioni oggetti modificabili e di modifica.")
   public static final String PRIVATE_CONSTANT_36 = "ChangeableChangeObjectRelationshipMap_name";

   @RBEntry("Raccoglie gli oggetti necessari per un archivio completo.")
   public static final String PRIVATE_CONSTANT_37 = "ChangeableChangeObjectRelationshipMap_desc";
}
