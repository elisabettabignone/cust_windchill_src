/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.objectcol.dependency;

import wt.util.resource.*;

@RBUUID("wt.dataops.objectcol.dependency.dependencyResource")
public final class dependencyResource_it extends WTListResourceBundle {
   /**
    * Object collection dependency classes related messages
    **/
   @RBEntry("Il metodo ha restituito un risultato di tipo errato. Previsto: {0}. Ottenuto: {1}.")
   @RBArgComment0("expected return type")
   @RBArgComment1("got return type")
   public static final String WRONG_RETURN_TYPE = "0";
}
