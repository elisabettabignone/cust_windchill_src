/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.objectcol.xmldef;

import wt.util.resource.*;

@RBUUID("wt.dataops.objectcol.xmldef.xmldefResource")
public final class xmldefResource_it extends WTListResourceBundle {
   /**
    * Object collection xmldef classes related messages
    **/
   @RBEntry("Il tipo di base iniziale deve essere del tipo \"class\": {0}")
   @RBArgComment0("type of object")
   public static final String INITIAL_BASETYPE_MUST_BE_CLASS = "0";

   @RBEntry("Tipo di campo errato per la classe. Nome classe: {0}, nome campo: {1}, tipo di campo previsto: {2}, tipo di campo restituito: {3}")
   @RBArgComment0("class name")
   @RBArgComment1("field name")
   @RBArgComment2("expected type")
   @RBArgComment3("got type")
   public static final String WRONG_FIELD_TYPE = "1";

   @RBEntry("Tipo \"related\" non supportato. I tipi supportati sono: \"by-method\" e \"by-relationship\"")
   public static final String NON_SUPPORTED_RELATED_TYPE = "2";

   @RBEntry("Impossibile utilizzare contemporaneamente i tag di inclusione ed esclusione link")
   public static final String NON_SUPPORTED_EXCLUDE_INCLUDE_IN_A_TAG = "3";

   @RBEntry("Attributo \"role\" non definito")
   public static final String ATTRIBUTE_ROLE_UNDEFINED = "4";

   @RBEntry("Tag \"includeLinks\" non definito")
   public static final String INCLUDELINK_TAG_UNDEFINED = "5";
}
