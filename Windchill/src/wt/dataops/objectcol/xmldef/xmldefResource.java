/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.objectcol.xmldef;

import wt.util.resource.*;

@RBUUID("wt.dataops.objectcol.xmldef.xmldefResource")
public final class xmldefResource extends WTListResourceBundle {
   /**
    * Object collection xmldef classes related messages
    **/
   @RBEntry("Initial base type must be of type \"class\": {0}")
   @RBArgComment0("type of object")
   public static final String INITIAL_BASETYPE_MUST_BE_CLASS = "0";

   @RBEntry("Wrong field type on class. Class name: {0}, Field name: {1}, Expected Field Type: {2}, Got Field Type: {3}")
   @RBArgComment0("class name")
   @RBArgComment1("field name")
   @RBArgComment2("expected type")
   @RBArgComment3("got type")
   public static final String WRONG_FIELD_TYPE = "1";

   @RBEntry("Non supported \"related\" type. Supported types are: \"by-method\" and \"by-relationship\"")
   public static final String NON_SUPPORTED_RELATED_TYPE = "2";

   @RBEntry("Include Link and Exclude Link tags cannot be present at a time")
   public static final String NON_SUPPORTED_EXCLUDE_INCLUDE_IN_A_TAG = "3";

   @RBEntry("Attribute \"role\" is undefined")
   public static final String ATTRIBUTE_ROLE_UNDEFINED = "4";

   @RBEntry("Tag \"includeLinks\" is undefined")
   public static final String INCLUDELINK_TAG_UNDEFINED = "5";
}