/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.objectcol.delegate;

import wt.util.resource.*;

@RBUUID("wt.dataops.objectcol.delegate.delegateResource")
public final class delegateResource_it extends WTListResourceBundle {
   /**
    * Object collection delegate classes related messages
    **/
   @RBEntry("Valore versione filtro non supportato: {0}")
   @RBArgComment0("version filter value")
   public static final String NOT_SUPPORTED_VERSION_FILTER_VALUE = "0";

   @RBEntry("Operatore logico non supportato: {0}")
   @RBArgComment0("version filter value")
   public static final String NOT_SUPPORTED_LOGICAL_OPERATOR = "1";
}
