/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.objectcol.ixb;

import wt.util.resource.*;

@RBUUID("wt.dataops.objectcol.ixb.ixbResource")
public final class ixbResource extends WTListResourceBundle {
   /**
    * Export-Import related messages
    **/
   @RBEntry("Object found on the target system.")
   public static final String OBJECT_FOUND = "0";

   @RBEntry("Object not found on the target system. ")
   public static final String OBJECT_NOT_FOUND = "1";

   @RBEntry("Import failed. No matching contexts found on the target system. ")
   public static final String NO_MATCHING_CONTEXTS = "2";

   @RBEntry(" Export failed. No valid contexts found.")
   public static final String NO_VALID_CONTEXTS = "3";

   @RBEntry("Import failed. No matching vaults found on the target system. ")
   public static final String NO_MATCHING_VAULTS = "4";

   @RBEntry("Target Vault")
   public static final String TARGET_VAULT = "5";

   @RBEntry("The import jar does not contain any vaults.")
   public static final String NO_VAULTS_IN_IMPORT_FILE = "6";
}
