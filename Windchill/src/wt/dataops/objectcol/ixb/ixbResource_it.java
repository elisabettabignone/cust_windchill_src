/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.objectcol.ixb;

import wt.util.resource.*;

@RBUUID("wt.dataops.objectcol.ixb.ixbResource")
public final class ixbResource_it extends WTListResourceBundle {
   /**
    * Export-Import related messages
    **/
   @RBEntry("Oggetto trovato nel sistema di destinazione.")
   public static final String OBJECT_FOUND = "0";

   @RBEntry("Oggetto non trovato nel sistema di destinazione.")
   public static final String OBJECT_NOT_FOUND = "1";

   @RBEntry("Importazione non riuscita. Nessun contesto corrispondente sul sistema di destinazione.")
   public static final String NO_MATCHING_CONTEXTS = "2";

   @RBEntry("Esportazione non riuscita. Nessun contesto valido trovato.")
   public static final String NO_VALID_CONTEXTS = "3";

   @RBEntry("Importazione non riuscita. Nessun archivio corrispondente sul sistema di destinazione.")
   public static final String NO_MATCHING_VAULTS = "4";

   @RBEntry("Archivio di destinazione")
   public static final String TARGET_VAULT = "5";

   @RBEntry("Il file jar importato non contiene archivi.")
   public static final String NO_VAULTS_IN_IMPORT_FILE = "6";
}
