/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.objectcol.resolution;

import wt.util.resource.*;

@RBUUID("wt.dataops.objectcol.resolution.resolutionResource")
public final class resolutionResource extends WTListResourceBundle {
   /**
    * Object collection resolution classes related messages
    **/
   @RBEntry("Cannot add attribute condition to null or empty specs")
   public static final String CANNOT_ADD_ATTR_CONDITION_TO_NULL_OR_EMPTY_SPECS = "0";

   @RBEntry("Unsupported logical operator: {0}")
   @RBArgComment0("value of logical operator")
   public static final String UNSUPPORTED_LOGICAL_OPERATOR = "1";

   @RBEntry("No values specified in attribute criterion for attribute: {0}")
   @RBArgComment0("attribute name")
   public static final String NO_VALUES_IN_ATTR_CRITERION = "2";

   @RBEntry("Wrong return type from method. Expected: {0}. Got: {1}.")
   @RBArgComment0("expected return type")
   @RBArgComment1("got return type")
   public static final String WRONG_RETURN_TYPE = "3";

   @RBEntry("Cannot add relationship condition if there are multiple classes in the from clause of the query.")
   public static final String CANNOT_ADD_REL_IF_MULTIPLE_FROM_CLASSES = "4";

   @RBEntry("Unsupported type of statement spec: {0}")
   @RBArgComment0("type of statement spec")
   public static final String UNSUPPORTED_TYPE_OF_STATEMENT_SPEC = "5";

   @RBEntry("Unsupported value type for parameter. Value Type: {0}. Parameter Type: {1}")
   @RBArgComment0("value type")
   @RBArgComment1("param type")
   public static final String UNSUPPORTED_VALUE_FOR_PARAM_TYPE = "6";

   @RBEntry("Object cannot be resolved to type. Object: {0}. Type: {1}")
   @RBArgComment0("object")
   @RBArgComment1("type")
   public static final String OBJECT_CANNOT_BE_RESOLVED_TO_TYPE = "7";

   @RBEntry("Cannot add relationship condition to null or empty specs")
   public static final String CANNOT_ADD_REL_CONDITION_TO_NULL_OR_EMPTY_SPECS = "8";

   @RBEntry("Unsupported type of attribute value: {0}")
   @RBArgComment0("type of attribute value")
   public static final String UNSUPPORTED_TYPE_OF_ATTR_VALUE = "9";
}
