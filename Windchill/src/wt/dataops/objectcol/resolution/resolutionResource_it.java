/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.dataops.objectcol.resolution;

import wt.util.resource.*;

@RBUUID("wt.dataops.objectcol.resolution.resolutionResource")
public final class resolutionResource_it extends WTListResourceBundle {
   /**
    * Object collection resolution classes related messages
    **/
   @RBEntry("Impossibile aggiungere condizione attributo a spec nulle o vuote")
   public static final String CANNOT_ADD_ATTR_CONDITION_TO_NULL_OR_EMPTY_SPECS = "0";

   @RBEntry("Operatore logico non supportato: {0}")
   @RBArgComment0("value of logical operator")
   public static final String UNSUPPORTED_LOGICAL_OPERATOR = "1";

   @RBEntry("Nessun valore specificato nel criterio di attributo per l'attributo: {0}")
   @RBArgComment0("attribute name")
   public static final String NO_VALUES_IN_ATTR_CRITERION = "2";

   @RBEntry("Il metodo ha restituito un risultato di tipo errato. Previsto: {0}. Ottenuto: {1}.")
   @RBArgComment0("expected return type")
   @RBArgComment1("got return type")
   public static final String WRONG_RETURN_TYPE = "3";

   @RBEntry("Impossibile aggiungere una condizione di relazione in presenza di più classi nella proposizione from dell'interrogazione.")
   public static final String CANNOT_ADD_REL_IF_MULTIPLE_FROM_CLASSES = "4";

   @RBEntry("Spec di istruzione di tipo non supportato: {0}")
   @RBArgComment0("type of statement spec")
   public static final String UNSUPPORTED_TYPE_OF_STATEMENT_SPEC = "5";

   @RBEntry("Tipo di valore non supportato per il parametro. Tipo di valore: {0}. Tipo di parametro: {1}")
   @RBArgComment0("value type")
   @RBArgComment1("param type")
   public static final String UNSUPPORTED_VALUE_FOR_PARAM_TYPE = "6";

   @RBEntry("L'oggetto non corrisponde al tipo. Oggetto: {0}. Tipo: {1}")
   @RBArgComment0("object")
   @RBArgComment1("type")
   public static final String OBJECT_CANNOT_BE_RESOLVED_TO_TYPE = "7";

   @RBEntry("Impossibile aggiungere una condizione di relazione a specifiche nulle o vuote")
   public static final String CANNOT_ADD_REL_CONDITION_TO_NULL_OR_EMPTY_SPECS = "8";

   @RBEntry("Valore attributo di tipo non supportato: {0}")
   @RBArgComment0("type of attribute value")
   public static final String UNSUPPORTED_TYPE_OF_ATTR_VALUE = "9";
}
