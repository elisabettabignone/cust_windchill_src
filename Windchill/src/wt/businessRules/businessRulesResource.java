/* bcwti
 *
 * Copyright (c) 2011 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.businessRules;

import wt.util.resource.RBEntry;
import wt.util.resource.RBComment;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.businessRules.businessRulesResource")
public class businessRulesResource extends WTListResourceBundle {

    @RBEntry("Unable to save \"{0}\" because (parameter \"{1}\") in \"{2}\" is null")
    public static final String NULL_ARGUMENT = "0";

    @RBEntry("Unable to delete \"{0}\" because (parameter \"{1}\") in \"{2}\" is null")
    public static final String CANNOT_DELETE_NULL_OBJECT = "1";

    @RBEntry("Invalid business rule configuration.")
    @RBComment("Error message displayed when a business rule in a business rule set is misconfigured.")
    public static final String INVALID_BUSINESS_RULE_CONFIG = "INVALID_BUSINESS_RULE_CONFIG";

    @RBEntry("The business rule set key was null.")
    @RBComment("Error message displayed when a business rule set key is null.")
    public static final String NULL_BUSINESS_RULE_SET_KEY = "NULL_BUSINESS_RULE_SET_KEY";

    @RBEntry("Unable to create a Business Rule link. \"{0}\" cannot be used in \"{1}\"")
    public static final String INVALID_RULES_ASSOCIATION = "2";

    @RBEntry("Cannot find the business rule set or it is disabled.")
    public static final String INVALID_RULESET = "3";
}
