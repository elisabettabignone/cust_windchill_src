/* bcwti
 *
 * Copyright (c) 2011 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.businessRules;

import wt.util.resource.RBEntry;
import wt.util.resource.RBComment;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.businessRules.businessRulesResource")
public class businessRulesResource_it extends WTListResourceBundle {

    @RBEntry("Impossibile salvare \"{0}\" poiché (il parametro \"{1}\") in \"{2}\" è nullo")
    public static final String NULL_ARGUMENT = "0";

    @RBEntry("Impossibile eliminare \"{0}\" poiché (il parametro \"{1}\") in \"{2}\" è nullo")
    public static final String CANNOT_DELETE_NULL_OBJECT = "1";

    @RBEntry("Configurazione regola aziendale non valida.")
    @RBComment("Error message displayed when a business rule in a business rule set is misconfigured.")
    public static final String INVALID_BUSINESS_RULE_CONFIG = "INVALID_BUSINESS_RULE_CONFIG";

    @RBEntry("Chiave insieme regole aziendali non valida.")
    @RBComment("Error message displayed when a business rule set key is null.")
    public static final String NULL_BUSINESS_RULE_SET_KEY = "NULL_BUSINESS_RULE_SET_KEY";

    @RBEntry("Impossibile creare un link regola aziendale. Impossibile utilizzare \"{0}\" in \"{1}\"")
    public static final String INVALID_RULES_ASSOCIATION = "2";

    @RBEntry("Insieme di regole aziendali non trovato o disattivato.")
    public static final String INVALID_RULESET = "3";
}
