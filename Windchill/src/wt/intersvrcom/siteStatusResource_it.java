/**
 * 
 */
package wt.intersvrcom;
import wt.util.resource.*;

/**

 *
 */
@RBUUID("wt.intersvrcom.siteStatusResource")
public class siteStatusResource_it extends WTListResourceBundle{
   @RBEntry("Il sito \"{0}\" è attivo.")
   public static final String SITE_OK = "0";
   
   @RBEntry("Il sito \"{0}\" non è in linea.")
   public static final String SITE_OFFLINE = "1";
   
   @RBEntry("Il sito \"{0}\" è di sola lettura")
   public static final String SITE_READONLY = "2";
   
   @RBEntry("Il sito \"{0}\" non risponde.")
   public static final String SITE_NOT_RESPONDING = "3";
   
   @RBEntry("Il motore servlet del sito \"{0}\" non risponde")
   public static final String SERVLET_ENGINE_NOT_RESPONDING = "4";
   
   @RBEntry("Il method server del sito \"{0}\" non risponde")
   public static final String MS_NOT_RESPONDING = "5";
   
   @RBEntry("Impossibile trasmettere la configurazione al sito \"{0}\"")
   public static final String BROADCAST_FAILED = "6";   
   
}
