/**
 * 
 */
package wt.intersvrcom;
import wt.util.resource.*;

/**

 *
 */
@RBUUID("wt.intersvrcom.siteStatusResource")
public class siteStatusResource extends WTListResourceBundle{
   @RBEntry("The site \"{0}\" is ok.")
   public static final String SITE_OK = "0";
   
   @RBEntry("The site \"{0}\" is offline.")
   public static final String SITE_OFFLINE = "1";
   
   @RBEntry("The site \"{0}\" is read-only")
   public static final String SITE_READONLY = "2";
   
   @RBEntry("The site \"{0}\" is not responding.")
   public static final String SITE_NOT_RESPONDING = "3";
   
   @RBEntry("The servlet engine at site \"{0}\" is not responding")
   public static final String SERVLET_ENGINE_NOT_RESPONDING = "4";
   
   @RBEntry("The method server at site \"{0}\" is not responding")
   public static final String MS_NOT_RESPONDING = "5";
   
   @RBEntry("Broadcast configuration to site \"{0}\" failed")
   public static final String BROADCAST_FAILED = "6";   
   
}
