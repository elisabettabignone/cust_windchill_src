/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.intersvrcom;

import wt.util.resource.*;

@RBUUID("wt.intersvrcom.intersvrcomResource")
public final class intersvrcomResource_it extends WTListResourceBundle {
   @RBEntry("L'algoritmo di sicurezza \"{0}\" non esiste o non è usato correttamente.")
   public static final String ALGORITHM_MISUSED = "0";

   @RBEntry("Non sono state ancora generate le chiavi di protezione del sito \"{0}\".")
   public static final String NO_SECURITY = "1";

   @RBEntry("La firma di \"{0}\", effettuata alle \"{1}\", ricevuta alle \"{2}\", è stata apposta oltre il tempo limite. Riprovare. Se l'errore persiste, contattare l'amministratore di sistema.")
   public static final String SIGNATURE_OUT_OF_TIME = "10";

   @RBEntry("Impossibile trovare la chiave di protezione per il sito \"{0}\".")
   public static final String NO_SECURITY_FOR_TARGET_SITE = "11";

   @RBEntry("L'oggetto importato non è una chiave valida.")
   public static final String ILLEGAL_IMPORTED_OBJECT = "2";

   @RBEntry("L'oggetto per il sito locale non è stato trovato. Verificare l'attributo URL del sito.")
   public static final String LOCAL_SITE_NOT_FOUND = "3";

   @RBEntry("L'oggetto per il sito dell'inviante \"{0}\" non è stato trovato. Verificare l'attributo URL del sito.")
   public static final String SENDER_SITE_NOT_FOUND = "4";

   @RBEntry("Il nome di sito \"{0}\" è già in uso. Scegliere un altro nome.")
   public static final String DUPLICATE_SITE_NAME = "5";

   @RBEntry("L'URL del sito \"{0}\" è già associato al sito \"{1}\". Due siti non possono avere lo stesso URL.")
   public static final String DUPLICATE_SITE_URL = "6";

   @RBEntry("\"{0}\" è il sito principale. Non è consentito eliminare il sito principale.")
   public static final String NO_MASTER_SITE_DELETE = "7";

   @RBEntry("Il nome dell'host sul client non è stato trovato durante la preparazione per la firma di autenticazione all'indirizzo \"{0}\".")
   public static final String NULL_CLIENT_FOR_SIGN = "8";

   @RBEntry("Il nome dell'host sul client non è stato trovato durante la preparazione della verifica dell'autenticazione dall'indirizzo \"{0}\".")
   public static final String NULL_CLIENT_FOR_VERIFY = "9";

   @RBEntry("master")
   public static final String MASTER_SITE_NAME = "12";

   @RBEntry("Il sito di archiviazione ha appena iniziato o appena smesso di rispondere alle richieste ping.")
   public static final String SITE_MONITOR_NOTIF_INFO_DESCR = "13";

   @RBEntry("Il sito \"{0}\" non risponde")
   public static final String SITE_DOWN_NOTIF_MSG = "14";

   @RBEntry("Stato e statistiche del sito per URL del sito")
   public static final String TABULAR_SITE_DATA_TYPE_DESCR = "15";

   @RBEntry("Stato e statistiche per un unico URL di sito")
   public static final String SITE_DATA_TYPE_DESCR = "16";

   @RBEntry("URL sito")
   public static final String SITE_DATA_URL_FIELD_DESCR = "17";

   @RBEntry("Nomi di riferimento del sito")
   public static final String SITE_DATA_NAMES_FIELD_DESCR = "18";

   @RBEntry("Ora a cui è stato eseguito un ping al sito per rilevarne lo stato")
   public static final String SITE_DATA_LAST_PING_FIELD_DESCR = "19";

   @RBEntry("Stato ottenuto dall'ultima operazione di ping")
   public static final String SITE_DATA_LAST_STATUS_FIELD_DESCR = "20";

   @RBEntry("Tempo impiegato dalle richieste ping (in secondi)")
   public static final String SITE_DATA_LAST_RESP_TIME_FIELD_DESCR = "21";

   @RBEntry("Percentuale stimata del tempo di attività e disponibilità del sito")
   public static final String SITE_DATA_PERC_UPTIME_FIELD_DESCR = "22";

   @RBEntry("Tempo di risposta medio dei ping riusciti al sito (in secondo)")
   public static final String SITE_DATA_AVG_RESP_TIME_FIELD_DESCR = "23";

   @RBEntry("Nomi di tutti i siti attualmente noti")
   public static final String SITE_NAMES_FIELD_DESCR = "24";

   @RBEntry("Nomi dei siti attualmente non raggiungibili")
   public static final String UNREACHABLE_SITE_NAMES_FIELD_DESCR = "25";

   @RBEntry("Dati dettagliati di stato per i siti")
   public static final String SITE_STATUS_DATA_FIELD_DESCR = "26";

   @RBEntry("Stato e statistiche globali per i siti")
   public static final String OVERALL_SITE_COMP_DATA_TYPE_DESCR = "27";

   @RBEntry("Numero di ping su cui le statistiche sono basate")
   public static final String SITE_DATA_NUMBER_OF_PINGS_FIELD_DESCR = "28";

   @RBEntry("Il programma di monitoraggio del sito non può essere eseguito su un nodo cluster senza method server in primo piano a meno che non venga eseguito nell'ambito di un method server in background.")
   public static final String SITE_MONITOR_NEEDS_A_LOCAL_FG_METHOD_SERVER = "29";

   @RBEntry("Il sito \"{0}\" ha ripreso a rispondere.")
   public static final String SITE_RESPONDING_AGAIN_NOTIF_MSG = "30";

   @RBEntry("Registrazione di CCS non riuscita.")
   public static final String REGISTER_CCS_FAILED = "31";

   @RBEntry("Le chiavi di protezione sono memorizzate in un formato sconosciuto. Il tipo delle chiavi private è \"{0}\". Il tipo delle chiavi pubbliche è \"{1}\".")
   public static final String UNRECOGNIZED_KEY_FORMAT = "32";

   @RBEntry("Le chiavi di protezione sono state rigenerate perché potrebbero essere danneggiate. Distribuire la chiave pubblica a tutti i server cache.")
   public static final String MAIL_BODY = "33";

   @RBEntry("Chiavi di protezione rigenerate")
   public static final String MAIL_SUBJECT = "34";

   @RBEntry("Creazione del sito {0} in corso...")
   @RBArgComment0("Name of the site")
   public static final String CREATING_SITE = "35";

   @RBEntry("Creazione del server cache contenuto {0} in corso...")
   @RBComment("Informational message, CCS stands for Content Cache Server.")
   @RBArgComment0("Name of site")
   public static final String CREATING_CCS = "36";

   @RBEntry("Errore durante la scrittura nella cartella nel percorso [{0}]. È possibile che lo spazio su disco non sia sufficiente.")
   public static final String ERROR_WRITE_PATH = "37";

   @RBEntry("Errore durante la scrittura nello stream di output a file per {0}")
   @RBArgComment0("File name with path")
   public static final String ERROR_WRITE_FILE = "38";

   @RBEntry("Errore durante la chiusura dello stream di output a file per {0}")
   @RBArgComment0("File name with path")
   public static final String ERROR_CLOSE_FILE = "39";

   @RBEntry("Impossibile completare lo streaming dei file al file server. Riavviare il processo. URL file server: {0}")
   @RBArgComment0("URL of the File Server")
   public static final String STREAM_TO_REPL_INCOMPLETE = "40";

   @RBEntry("{0} non in linea.")
   @RBComment("{Site Name} is offline")
   public static final String SITE_OFFLINE = "41";

   @RBEntry("{0} in linea.")
   @RBComment("{Site Name} is online")
   public static final String SITE_ONLINE = "42";

   @RBEntry("{0} impostato su sola lettura.")
   @RBComment("{Site Name} has been set to read only.")
   public static final String SITE_READ_ONLY = "43";

   @RBEntry("{0} richiede un riavvio.")
   @RBComment("{Site Name} requires a restart.")
   public static final String SITE_RESTART_REQD = "44";

   @RBEntry("Aggiurnamento in corso per il sito {0}.")
   @RBComment("Update is in process for the site {Site Name}.")
   public static final String SITE_UPDATE_IN_PROCESS = "45";

   @RBEntry("La firma di \"{0}\", effettuata alle \"{1}\", ricevuta alle \"{2}\", è già in uso. Riprovare. Se l'errore persiste, contattare l'amministratore di sistema.")
   @RBComment("The signature from {url}, signed at { time }, received at {time}, is already used. Please retry the operation. If the error persists please contact the system administrator.")
   public static final String CONTENT_URL_ALREADY_USED = "46";

   @RBEntry("Impossibile impostare il dominio di destinazione per il link a utente/gruppo/ruolo del sito \"{0}\" in quanto il contesto era nullo.")
   @RBComment("Could not set target domain for principal link of site \"{site}\" as context was empty.")
   public static final String ERROR_SET_TARGET_DOMAIN = "47";

   @RBEntry("Eventuali informazioni aggiuntive sugli archivi contenuti nel sito")
   public static final String SITE_DATA_ADDITIONAL_INFO = "48";

   @RBEntry("Gli archivi \"{0}\" contenuti nel sito non sono accessibili perché lo stato del sito è \"{1}\".")
   public static final String SITE_DATA_ADDITIONAL_INFO_VALUE = "49";
   
   @RBEntry("Impossibile trovare la chiave di protezione per il sito principale.")
   public static final String NO_SECURITY_FOR_MAIN_SITE = "50";
   
   @RBEntry("Nome \"{0}\" non valido. Un nome può contenere solo caratteri alfanumerici, trattini o tratti di sottolineatura.")
   @RBArgComment0("Name")
   public static final String NAME_INVALID = "NAME_INVALID";
   
   @RBEntry("La sintassi dell'URL \"{0}\" per questo sito non è valida. Immettere nuovamente l'URL.")
   @RBArgComment0("URL of the site")
   public static final String SITE_URL_INVALID = "SITE_URL_INVALID";
}
