/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.intersvrcom;

import wt.util.resource.*;

@RBUUID("wt.intersvrcom.intersvrcomResource")
public final class intersvrcomResource extends WTListResourceBundle {
   @RBEntry("The security algorithm \"{0}\" does not exist or misused.")
   public static final String ALGORITHM_MISUSED = "0";

   @RBEntry("The site \"{0}\" does not have security keys generated yet.")
   public static final String NO_SECURITY = "1";

   @RBEntry("The signature from \"{0}\", signed at \"{1}\", received at \"{2}\", was out of date.  Please retry the operation. If the error persists please contact the system administrator.")
   public static final String SIGNATURE_OUT_OF_TIME = "10";

   @RBEntry("The security key is not found for site \"{0}\".")
   public static final String NO_SECURITY_FOR_TARGET_SITE = "11";

   @RBEntry("The Imported object does not represent a valid key.")
   public static final String ILLEGAL_IMPORTED_OBJECT = "2";

   @RBEntry("The object for the local site not found. Check the site's URL attribute.")
   public static final String LOCAL_SITE_NOT_FOUND = "3";

   @RBEntry("The object for the sender site \"{0}\" not found. Check the site's URL attribute.")
   public static final String SENDER_SITE_NOT_FOUND = "4";

   @RBEntry("The site name \"{0}\" is already used by another site. Please select a different name.")
   public static final String DUPLICATE_SITE_NAME = "5";

   @RBEntry("The site URL\"{0}\" is already associated with site \"{1}\". Having two sites with the same URL is not allowed.")
   public static final String DUPLICATE_SITE_URL = "6";

   @RBEntry("Site \"{0}\" is the main site. Deleting the main site is not allowed.")
   public static final String NO_MASTER_SITE_DELETE = "7";

   @RBEntry("Client host name not found while preparing for authentication signature to address \"{0}\".")
   public static final String NULL_CLIENT_FOR_SIGN = "8";

   @RBEntry("Client host name not found while preparing for authentication verification from address \"{0}\".")
   public static final String NULL_CLIENT_FOR_VERIFY = "9";

   @RBEntry("master")
   public static final String MASTER_SITE_NAME = "12";

   @RBEntry("Vaulting site has either just started or just stopped responding to pings.")
   public static final String SITE_MONITOR_NOTIF_INFO_DESCR = "13";

   @RBEntry("No response from site \"{0}\"")
   public static final String SITE_DOWN_NOTIF_MSG = "14";

   @RBEntry("Site status and statistics by site URL")
   public static final String TABULAR_SITE_DATA_TYPE_DESCR = "15";

   @RBEntry("Status and statistics for a single site URL")
   public static final String SITE_DATA_TYPE_DESCR = "16";

   @RBEntry("URL of site")
   public static final String SITE_DATA_URL_FIELD_DESCR = "17";

   @RBEntry("Reference name(s) of site")
   public static final String SITE_DATA_NAMES_FIELD_DESCR = "18";

   @RBEntry("Time site was last pinged for status")
   public static final String SITE_DATA_LAST_PING_FIELD_DESCR = "19";

   @RBEntry("Status of site obtained from last ping")
   public static final String SITE_DATA_LAST_STATUS_FIELD_DESCR = "20";

   @RBEntry("Elapsed time of last ping request (in seconds)")
   public static final String SITE_DATA_LAST_RESP_TIME_FIELD_DESCR = "21";

   @RBEntry("Estimated percentage of time site has been up and available")
   public static final String SITE_DATA_PERC_UPTIME_FIELD_DESCR = "22";

   @RBEntry("Average response time of successful pings of the site (in seconds)")
   public static final String SITE_DATA_AVG_RESP_TIME_FIELD_DESCR = "23";

   @RBEntry("Names of all currently known sites")
   public static final String SITE_NAMES_FIELD_DESCR = "24";

   @RBEntry("Names of sites which are currently unreachable")
   public static final String UNREACHABLE_SITE_NAMES_FIELD_DESCR = "25";

   @RBEntry("Detailed status data on sites")
   public static final String SITE_STATUS_DATA_FIELD_DESCR = "26";

   @RBEntry("Overall status and statistics data for sites")
   public static final String OVERALL_SITE_COMP_DATA_TYPE_DESCR = "27";

   @RBEntry("Number of pings which statistics are based upon")
   public static final String SITE_DATA_NUMBER_OF_PINGS_FIELD_DESCR = "28";

   @RBEntry("Site monitor cannot run on a cluster node without any foreground method servers unless it is run within a background method server.")
   public static final String SITE_MONITOR_NEEDS_A_LOCAL_FG_METHOD_SERVER = "29";

   @RBEntry("Site \"{0}\" is responding again.")
   public static final String SITE_RESPONDING_AGAIN_NOTIF_MSG = "30";

   @RBEntry("Registration of CCS failed.")
   public static final String REGISTER_CCS_FAILED = "31";

   @RBEntry("Security keys are stored in an unrecognized format. Type of private is \"{0}\".  Type of public is \"{1}\".")
   public static final String UNRECOGNIZED_KEY_FORMAT = "32";

   @RBEntry("Security keys are regenerated because it might have got corrupted. Please Distribute public key to all cache servers.")
   public static final String MAIL_BODY = "33";

   @RBEntry("Security keys regenerated")
   public static final String MAIL_SUBJECT = "34";

   @RBEntry("Creating site ...{0}")
   @RBArgComment0("Name of the site")
   public static final String CREATING_SITE = "35";

   @RBEntry("Creating CCS ...{0}")
   @RBComment("Informational message, CCS stands for Content Cache Server.")
   @RBArgComment0("Name of site")
   public static final String CREATING_CCS = "36";

   @RBEntry("Error while writing to folder with path [{0}]. Disk might not have enough space.")
   public static final String ERROR_WRITE_PATH = "37";

   @RBEntry("Error while writing to file output stream for {0}")
   @RBArgComment0("File name with path")
   public static final String ERROR_WRITE_FILE = "38";

   @RBEntry("Error while closing file output stream for {0}")
   @RBArgComment0("File name with path")
   public static final String ERROR_CLOSE_FILE = "39";

   @RBEntry("Streaming of all files to file server could not be completed. Need to restart the process. File Server URL: {0}")
   @RBArgComment0("URL of the File Server")
   public static final String STREAM_TO_REPL_INCOMPLETE = "40";

   @RBEntry("{0} is offline.")
   @RBComment("{Site Name} is offline")
   public static final String SITE_OFFLINE = "41";

   @RBEntry("{0} is online.")
   @RBComment("{Site Name} is online")
   public static final String SITE_ONLINE = "42";

   @RBEntry("{0} has been set to read only.")
   @RBComment("{Site Name} has been set to read only.")
   public static final String SITE_READ_ONLY = "43";

   @RBEntry("{0} requires a restart.")
   @RBComment("{Site Name} requires a restart.")
   public static final String SITE_RESTART_REQD = "44";

   @RBEntry("Update is in process for the site {0}.")
   @RBComment("Update is in process for the site {Site Name}.")
   public static final String SITE_UPDATE_IN_PROCESS = "45";

   @RBEntry("The signature from \"{0}\", signed at \"{1}\", received at \"{2}\", is already used. Please retry the operation. If the error persists please contact the system administrator.")
   @RBComment("The signature from {url}, signed at { time }, received at {time}, is already used. Please retry the operation. If the error persists please contact the system administrator.")
   public static final String CONTENT_URL_ALREADY_USED = "46";

   @RBEntry("Could not set target domain for principal link of site \"{0}\" as context was empty.")
   @RBComment("Could not set target domain for principal link of site \"{site}\" as context was empty.")
   public static final String ERROR_SET_TARGET_DOMAIN = "47";

   @RBEntry("Additional information, if any, about vaults contained in the site")
   public static final String SITE_DATA_ADDITIONAL_INFO = "48";

   @RBEntry("Vaults \"{0}\" contained in this site are inaccessible because site status is \"{1}\".")
   public static final String SITE_DATA_ADDITIONAL_INFO_VALUE = "49";
   
   @RBEntry("The security key is not found for main site.")
   public static final String NO_SECURITY_FOR_MAIN_SITE = "50";
   
   @RBEntry("The name \"{0}\" is not valid. Only alphabets, numbers, hyphen and underscores are allowed in name.")
   @RBArgComment0("Name")
   public static final String NAME_INVALID = "NAME_INVALID";
   
   @RBEntry("The URL \"{0}\" for this site has an invalid syntax. Please retype the URL.")
   @RBArgComment0("URL of the site")
   public static final String SITE_URL_INVALID = "SITE_URL_INVALID";
}
