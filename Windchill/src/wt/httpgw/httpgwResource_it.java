/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.httpgw;

import wt.util.resource.*;

@RBUUID("wt.httpgw.httpgwResource")
public final class httpgwResource_it extends WTListResourceBundle {
   @RBEntry("Costrutto non valido.  Specificare \"q = \" qualityValue.")
   public static final String CONSTRUCT_ERROR = "0";

   @RBEntry("Specificare il nome utente e la password")
   public static final String PASSWORD_REALM_REQUEST = "1";

   @RBEntry("Host:")
   public static final String HOST = "2";

   @RBEntry("Schema di autenticazione: ")
   public static final String AUTHENTICATION_SCHEME = "3";

   @RBEntry("Specificare il nome utente e la password")
   public static final String PASSWORD_SOCKS_REQUEST = "4";

   @RBEntry("Server SOCKS:")
   public static final String SERVER_HOST = "5";

   @RBEntry("Metodo di autenticazione: nome utente/password")
   public static final String AUTHENTICATION_METHOD = "6";

   @RBEntry("Autenticazione HTTP non attivata")
   public static final String HTTP_AUTH_NOT_ENABLED = "7";

   @RBEntry("Accesso HTTP non riuscito.")
   public static final String HTTP_LOGIN_FAILED = "8";

   @RBEntry("Flusso di input esaurito")
   public static final String INPUT_STREAM_CONSUMED = "9";

   @RBEntry("{0}: risposta HTTP {1}, {2}")
   @RBArgComment0(" {0} refers to the URL")
   @RBArgComment1(" {1} refers to the HTTP code")
   @RBArgComment2(" {2} refers to the error message this is being prepended to")
   public static final String CONNECT_FAILED = "10";

   @RBEntry("accesso negato")
   public static final String ACCESS_DENIED = "11";

   @RBEntry("Acquisizione flusso di output prematura.")
   public static final String EARLY_OUTPUT_ACQUISITION = "12";

   @RBEntry("Benvenuti in Windchill")
   public static final String FORM_WELCOME = "13";

   @RBEntry("Impossibile recuperare le proprietà del server")
   public static final String SERVER_PROPERTIES_ERROR = "14";

   @RBEntry("u8 è un nome di parametro riservato")
   @RBComment("the 'u8' should remain in any translated strings.  ")
   public static final String U8_ENCODE_PARAMETER = "15";

   @RBEntry("{1} è una stringa codificata URL non valida")
   @RBArgComment1("The string that was to be encoded.")
   public static final String INVALID_ENCODE_STRING = "16";

   @RBEntry("L'URI relativo di richiesta specificato non è valido in relazione all'URI di richiesta attuale.")
   public static final String PRIVATE_CONSTANT_0 = "URLFactory_invalidRequestURI";

   @RBEntry("L'URI relativo di richiesta specificato non può essere impostato in caso di URI di richiesta nullo.")
   public static final String PRIVATE_CONSTANT_1 = "URLFactory_nullRequestURI";

   /**
    * Labels ------------------------------------------------------------------
    * 
    **/
   @RBEntry("Password:")
   public static final String PRIVATE_CONSTANT_2 = "LPassword";

   @RBEntry("Nome utente:")
   public static final String PRIVATE_CONSTANT_3 = "LUsername";

   /**
    * Button Labels -----------------------------------------------------------
    * 
    **/
   @RBEntry("Annulla")
   public static final String PRIVATE_CONSTANT_4 = "BLCancel";

   @RBEntry("Reimposta")
   public static final String PRIVATE_CONSTANT_5 = "BLClear";

   @RBEntry("OK")
   public static final String PRIVATE_CONSTANT_6 = "BLOK";

   @RBEntry("Area autenticazione:")
   public static final String REALM_VALUE = "17";

   @RBEntry("Richiesta di autorizzazione")
   public static final String PRIVATE_CONSTANT_7 = "AUTH_TITLE";
}
