/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.httpgw;

import wt.util.resource.*;

@RBUUID("wt.httpgw.httpgwResource")
public final class httpgwResource extends WTListResourceBundle {
   @RBEntry("Bad construct.  Expecting \"q = \" qualityValue")
   public static final String CONSTRUCT_ERROR = "0";

   @RBEntry("Enter username and password   ")
   public static final String PASSWORD_REALM_REQUEST = "1";

   @RBEntry("Host:")
   public static final String HOST = "2";

   @RBEntry("Authentication Scheme: ")
   public static final String AUTHENTICATION_SCHEME = "3";

   @RBEntry("Enter username and password ")
   public static final String PASSWORD_SOCKS_REQUEST = "4";

   @RBEntry("SOCKS Server:")
   public static final String SERVER_HOST = "5";

   @RBEntry("Authentication Method: username/password")
   public static final String AUTHENTICATION_METHOD = "6";

   @RBEntry("HTTP Authentication not enabled")
   public static final String HTTP_AUTH_NOT_ENABLED = "7";

   @RBEntry("HTTP login failed.")
   public static final String HTTP_LOGIN_FAILED = "8";

   @RBEntry("Input stream already consumed")
   public static final String INPUT_STREAM_CONSUMED = "9";

   @RBEntry("{0}: HTTP response {1}, {2}")
   @RBArgComment0(" {0} refers to the URL")
   @RBArgComment1(" {1} refers to the HTTP code")
   @RBArgComment2(" {2} refers to the error message this is being prepended to")
   public static final String CONNECT_FAILED = "10";

   @RBEntry("access denied")
   public static final String ACCESS_DENIED = "11";

   @RBEntry("Premature output stream acquisition.")
   public static final String EARLY_OUTPUT_ACQUISITION = "12";

   @RBEntry("Welcome to Windchill")
   public static final String FORM_WELCOME = "13";

   @RBEntry("The Server Properties could not be retrieved.")
   public static final String SERVER_PROPERTIES_ERROR = "14";

   @RBEntry("u8 is a reserved parameter name")
   @RBComment("the 'u8' should remain in any translated strings.  ")
   public static final String U8_ENCODE_PARAMETER = "15";

   @RBEntry("{1} is an invalid URL Encoded String")
   @RBArgComment1("The string that was to be encoded.")
   public static final String INVALID_ENCODE_STRING = "16";

   @RBEntry("The specified relative Request URI is invalid relative to the current Request URI.")
   public static final String PRIVATE_CONSTANT_0 = "URLFactory_invalidRequestURI";

   @RBEntry("The specified relative Request URI can not be set relative to a null Request URI.")
   public static final String PRIVATE_CONSTANT_1 = "URLFactory_nullRequestURI";

   /**
    * Labels ------------------------------------------------------------------
    * 
    **/
   @RBEntry("Password:")
   public static final String PRIVATE_CONSTANT_2 = "LPassword";

   @RBEntry("Username:")
   public static final String PRIVATE_CONSTANT_3 = "LUsername";

   /**
    * Button Labels -----------------------------------------------------------
    * 
    **/
   @RBEntry("Cancel")
   public static final String PRIVATE_CONSTANT_4 = "BLCancel";

   @RBEntry("Clear")
   public static final String PRIVATE_CONSTANT_5 = "BLClear";

   @RBEntry("OK")
   public static final String PRIVATE_CONSTANT_6 = "BLOK";

   @RBEntry("Realm:")
   public static final String REALM_VALUE = "17";

   @RBEntry("Authorization Request")
   public static final String PRIVATE_CONSTANT_7 = "AUTH_TITLE";
}
