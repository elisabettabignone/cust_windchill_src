/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.ufid;

import wt.util.resource.*;

@RBUUID("wt.ufid.ufidResource")
public final class ufidResource_it extends WTListResourceBundle {
   @RBEntry("Le informazioni di identificazione dell'oggetto {0} \"{1}\" sono danneggiate. Contattare l'amministratore.")
   @RBComment("This is the main error message for errors related to UFID corruption when a specific object is identified")
   @RBArgComment0("The class/type of object being reported on")
   @RBArgComment1("Name or Number of the object")
   public static final String UFID_DATA_CORRUPTED = "UFID_DATA_CORRUPTED";

   @RBEntry("Le informazioni di identificazione degli oggetti in corso di elaborazione sono danneggiate.")
   @RBComment("This is the main error message for errors related to UFID corruption when no specific object is identified")
   public static final String UFID_DATA_CORRUPTED_GENERIC = "UFID_DATA_CORRUPTED_GENERIC";

   @RBEntry("È stato rilevato un problema di configurazione. È possibile configurare solo un repository come locale. Verificare la configurazione e accertarsi che solo un repository sia contrassegnato come locale.")
   @RBComment("**NOTE: This message is logged to the server log and is not displayed in UI. It is intentionally kept technical to help the administrators fix the underlying data issue.")
   public static final String MORE_THAN_ONE_LOCAL_REPOSITORY = "0";

   @RBEntry("{0} appartiene a più repository. Un oggetto può solo essere registrato come appartenente a un repository in Windchill.")
   @RBComment("**NOTE: This message is logged to the server log and is not displayed in UI. It is intentionally kept technical to help the administrators fix the underlying data issue.")
   @RBArgComment0("Identifying information of the object with the problem")
   public static final String MORE_THAN_ONE_OWNER = "1";

   @RBEntry("È stato rilevato un problema di configurazione. Più repository condividono il GUID {0}. Verificare la configurazione e accertarsi che ogni repository abbia un GUID distinto.")
   @RBComment("**NOTE: This message is logged to the server log and is not displayed in UI. It is intentionally kept technical to help the administrators fix the underlying data issue.")
   @RBArgComment0("Globally Unique Identifier (GUID) of a repository with the problem")
   public static final String GUID_MATCHES_MULTIPLE_REPOSITORIES = "2";

   @RBEntry("L'UFID (Unique Federation Identifier) dell'oggetto {0} è danneggiato. Esistono più record rappresentanti la proprietà di Windchill di origine e di destinazione dell'oggetto.")
   @RBComment("**NOTE: This message is logged to the server log and is not displayed in UI. It is intentionally kept technical to help the administrators fix the underlying data issue.")
   @RBArgComment0("Identifying information of the object with the problem")
   public static final String MORE_THAN_ONE_REMOTE_REF = "4";

   @RBEntry("L'UFID (Unique Federation Identifier) dell'oggetto è danneggiato. Esistono più record rappresentanti la proprietà di Windchill di origine e di destinazione dell'oggetto con il nome distinto \"{0}\".")
   @RBComment("**NOTE: This message is logged to the server log and is not displayed in UI. It is intentionally kept technical to help the administrators fix the underlying data issue.")
   @RBArgComment0("Distinguished name of the object with the problem")
   public static final String DN_MATCHES_MULTIPLE_ROIDS = "5";

   @RBEntry("L'oggetto {0} è già contrassegnato come di proprietà di un altro repository di Windchill.")
   @RBComment("**NOTE: This message is logged to the server log and is not displayed in UI. It is intentionally kept technical to help the administrators fix the underlying data issue.")
   @RBArgComment0("Identifying information of the object with the problem")
   public static final String OBJECT_ALREADY_REMOTE = "6";

   @RBEntry("Un repository proprietario esiste già per l'oggetto {0}.")
   @RBComment("**NOTE: This message is logged to the server log and is not displayed in UI. It is intentionally kept technical to help the administrators fix the underlying data issue.")
   @RBArgComment0("Identifying information of the object with the problem")
   public static final String OWNER_ALREADY_EXISTS = "7";

   @RBEntry("Impossibile aggiornare o eliminare le informazioni sul repository proprietario per uno o più oggetti in corso di elaborazione. L'operazione è supportata solo per gli oggetti con master.")
   @RBComment("**NOTE: This message is logged to the server log and is not displayed in UI. It is intentionally kept technical to help the administrators fix the underlying data issue.")
   public static final String CAN_NOT_PROCESS_ITERATED_OBJECT = "8";

   @RBEntry("Le informazioni sul repository proprietario dell'oggetto {0} sono danneggiate.")
   @RBComment("**NOTE: This message is logged to the server log and is not displayed in UI. It is intentionally kept technical to help the administrators fix the underlying data issue.")
   @RBArgComment0("Identifying information of the object with the problem")
   public static final String OWNING_INFORMATION_INCONSISTENT = "9";

   @RBEntry("L'UFID (Unique Federation Identifier) di uno o più oggetti è danneggiato. Esistono più record rappresentanti la proprietà di Windchill di origine e di destinazione di uno o più oggetti in corso di elaborazione.")
   @RBComment("**NOTE: This message is logged to the server log and is not displayed in UI. It is intentionally kept technical to help the administrators fix the underlying data issue.")
   public static final String REMOTEID_DOES_NOT_MATCH_REMOTEOBJECTID = "10";

   @RBEntry("Informazioni mancanti sul repository proprietario di uno o più oggetti.")
   @RBComment("**NOTE: This message is logged to the server log and is not displayed in UI. It is intentionally kept technical to help the administrators fix the underlying data issue.")
   public static final String OWNERSHIP_NOT_FOUND = "11";

   @RBEntry("Uno o più oggetti sono contrassegnati come appartenenti a più repository. Un oggetto può solo essere registrato come appartenente a un repository in Windchill.")
   @RBComment("**NOTE: This message is logged to the server log and is not displayed in UI. It is intentionally kept technical to help the administrators fix the underlying data issue.")
   public static final String MORE_OWNERS_THAN_OBJECTS = "12";

   @RBEntry("L'UFID (Unique Federation Identifier) di uno o più oggetti è danneggiato. Esistono più record rappresentanti la proprietà di Windchill di origine e di destinazione di uno o più oggetti in corso di elaborazione.")
   @RBComment("**NOTE: This message is logged to the server log and is not displayed in UI. It is intentionally kept technical to help the administrators fix the underlying data issue.")
   public static final String MORE_REMOTE_ENTRIES_THAN_OBJECTS = "13";
}
