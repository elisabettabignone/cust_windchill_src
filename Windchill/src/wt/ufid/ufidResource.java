/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.ufid;

import wt.util.resource.*;

@RBUUID("wt.ufid.ufidResource")
public final class ufidResource extends WTListResourceBundle {
   @RBEntry("The identifying information for the {0} \"{1}\" is corrupted. Contact your administrator.")
   @RBComment("This is the main error message for errors related to UFID corruption when a specific object is identified")
   @RBArgComment0("The class/type of object being reported on")
   @RBArgComment1("Name or Number of the object")
   public static final String UFID_DATA_CORRUPTED = "UFID_DATA_CORRUPTED";

   @RBEntry("The identifying information for the objects being processed is corrupted.")
   @RBComment("This is the main error message for errors related to UFID corruption when no specific object is identified")
   public static final String UFID_DATA_CORRUPTED_GENERIC = "UFID_DATA_CORRUPTED_GENERIC";

   @RBEntry("A configuration error has been detected. Only one repository can be configured as \"local\". Check your configuration and ensure that only one repository is marked as \"local\".")
   @RBComment("**NOTE: This message is logged to the server log and is not displayed in UI. It is intentionally kept technical to help the administrators fix the underlying data issue.")
   public static final String MORE_THAN_ONE_LOCAL_REPOSITORY = "0";

   @RBEntry("{0} is owned by more than one repository. An object can only be registered as belonging to one repository in Windchill.")
   @RBComment("**NOTE: This message is logged to the server log and is not displayed in UI. It is intentionally kept technical to help the administrators fix the underlying data issue.")
   @RBArgComment0("Identifying information of the object with the problem")
   public static final String MORE_THAN_ONE_OWNER = "1";

   @RBEntry("A configuration error has been detected. More than one repository shares the following GUID: {0}. Check your configuration and ensure that each repository has a unique GUID.")
   @RBComment("**NOTE: This message is logged to the server log and is not displayed in UI. It is intentionally kept technical to help the administrators fix the underlying data issue.")
   @RBArgComment0("Globally Unique Identifier (GUID) of a repository with the problem")
   public static final String GUID_MATCHES_MULTIPLE_REPOSITORIES = "2";

   @RBEntry("The UFID (Unique Federation Identifier) for {0} is corrupted. There are multiple records representing the source and target Windchill ownership for the object.")
   @RBComment("**NOTE: This message is logged to the server log and is not displayed in UI. It is intentionally kept technical to help the administrators fix the underlying data issue.")
   @RBArgComment0("Identifying information of the object with the problem")
   public static final String MORE_THAN_ONE_REMOTE_REF = "4";

   @RBEntry("The UFID (Unique Federation Identifier) for the object is corrupted. There are multiple records representing the source and target Windchill ownership for the object with the distinguished name \"{0}\".")
   @RBComment("**NOTE: This message is logged to the server log and is not displayed in UI. It is intentionally kept technical to help the administrators fix the underlying data issue.")
   @RBArgComment0("Distinguished name of the object with the problem")
   public static final String DN_MATCHES_MULTIPLE_ROIDS = "5";

   @RBEntry("{0} is already marked as being owned by another Windchill repository.")
   @RBComment("**NOTE: This message is logged to the server log and is not displayed in UI. It is intentionally kept technical to help the administrators fix the underlying data issue.")
   @RBArgComment0("Identifying information of the object with the problem")
   public static final String OBJECT_ALREADY_REMOTE = "6";

   @RBEntry("An owning repository already exists for {0}.")
   @RBComment("**NOTE: This message is logged to the server log and is not displayed in UI. It is intentionally kept technical to help the administrators fix the underlying data issue.")
   @RBArgComment0("Identifying information of the object with the problem")
   public static final String OWNER_ALREADY_EXISTS = "7";

   @RBEntry("The information about the owning repository cannot be updated or deleted for one or more objects being processed because this operation is only supported for Mastered objects.")
   @RBComment("**NOTE: This message is logged to the server log and is not displayed in UI. It is intentionally kept technical to help the administrators fix the underlying data issue.")
   public static final String CAN_NOT_PROCESS_ITERATED_OBJECT = "8";

   @RBEntry("The information about the owning repository for {0} is corrupted.")
   @RBComment("**NOTE: This message is logged to the server log and is not displayed in UI. It is intentionally kept technical to help the administrators fix the underlying data issue.")
   @RBArgComment0("Identifying information of the object with the problem")
   public static final String OWNING_INFORMATION_INCONSISTENT = "9";

   @RBEntry("The UFID (Unique Federation Identifier) for one or more objects is corrupted. There are multiple records representing the source and target Windchill ownership for one or more of the objects being processed.")
   @RBComment("**NOTE: This message is logged to the server log and is not displayed in UI. It is intentionally kept technical to help the administrators fix the underlying data issue.")
   public static final String REMOTEID_DOES_NOT_MATCH_REMOTEOBJECTID = "10";

   @RBEntry("The information about the owning repository for one or more objects is missing.")
   @RBComment("**NOTE: This message is logged to the server log and is not displayed in UI. It is intentionally kept technical to help the administrators fix the underlying data issue.")
   public static final String OWNERSHIP_NOT_FOUND = "11";

   @RBEntry("One or more objects are marked as being owned by more than one repository. An object can only be registered as belonging to one repository in Windchill.")
   @RBComment("**NOTE: This message is logged to the server log and is not displayed in UI. It is intentionally kept technical to help the administrators fix the underlying data issue.")
   public static final String MORE_OWNERS_THAN_OBJECTS = "12";

   @RBEntry("The UFID (Unique Federation Identifier) for one or more objects is corrupted. There are multiple records representing the source and target Windchill ownership for one or more of the objects being processed.")
   @RBComment("**NOTE: This message is logged to the server log and is not displayed in UI. It is intentionally kept technical to help the administrators fix the underlying data issue.")
   public static final String MORE_REMOTE_ENTRIES_THAN_OBJECTS = "13";
}
