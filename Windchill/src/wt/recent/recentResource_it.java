/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.recent;

import wt.util.resource.*;

@RBUUID("wt.recent.recentResource")
public final class recentResource_it extends WTListResourceBundle {
   @RBEntry("\"{0}\" non è un tipo di contenitore supportato.")
   @RBArgComment0("refers to container name")
   public static final String UNSUPPORTED_TYPE = "0";

   @RBEntry("ObjectIdentifier \"{0}\" non corrisponde all'istanza di WTContainer.")
   @RBArgComment0("refers to objectIdentifier")
   public static final String CONTAINER_NOT_FOUND = "1";

   @RBEntry("ObjectIdentifier \"{0}\" non corrisponde all'istanza di WTContainer.")
   @RBArgComment0("refers to objectIdentifier")
   public static final String OBJECT_NOT_FOUND = "2";

   @RBEntry("L'elenco recente \"{0}\" non esiste.")
   @RBArgComment0("refers to recent list name")
   public static final String LIST_NOT_FOUND = "3";

   @RBEntry("Uso non valido del comando Rimuovi per un oggetto che implementa l'interfaccia RecentlyVisited.")
   public static final String INVALID_RECENT_REMOVE = "4";
}
