/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.recent;

import wt.util.resource.*;

@RBUUID("wt.recent.recentResource")
public final class recentResource extends WTListResourceBundle {
   @RBEntry("\"{0}\" is not supported container type.")
   @RBArgComment0("refers to container name")
   public static final String UNSUPPORTED_TYPE = "0";

   @RBEntry("ObjectIdentifier \"{0}\" doesn't corresponds to instance of WTContainer.")
   @RBArgComment0("refers to objectIdentifier")
   public static final String CONTAINER_NOT_FOUND = "1";

   @RBEntry("ObjectIdentifier \"{0}\" doesn't corresponds to instance of Windchill Object.")
   @RBArgComment0("refers to objectIdentifier")
   public static final String OBJECT_NOT_FOUND = "2";

   @RBEntry("Recent list \"{0}\" does not exist.")
   @RBArgComment0("refers to recent list name")
   public static final String LIST_NOT_FOUND = "3";

   @RBEntry("Invalid use of remove for an object implementing the RecentlyVisited Interface.")
   public static final String INVALID_RECENT_REMOVE = "4";
}
