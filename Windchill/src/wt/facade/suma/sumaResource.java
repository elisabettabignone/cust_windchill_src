/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.facade.suma;

import wt.util.resource.*;

@RBUUID("wt.facade.suma.sumaResource")
public final class sumaResource extends WTListResourceBundle {
   @RBEntry("Supplier Management module is not installed")
   public static final String MSG_SUMA_NOT_INSTALLED = "100";

   @RBEntry("Sourcing Status")
   public static final String LBL_SOURCING_STATUS = "110";

   @RBEntry("Single Source")
   public static final String LBL_SINGLE_SOURCE = "120";

   @RBEntry("View Manufacturer")
   public static final String VIEW_MFR_URL_LABEL = "200";

   @RBEntry("View Vendor")
   public static final String VIEW_VDR_URL_LABEL = "210";
}
