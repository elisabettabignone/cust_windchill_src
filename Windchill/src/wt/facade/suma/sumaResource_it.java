/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.facade.suma;

import wt.util.resource.*;

@RBUUID("wt.facade.suma.sumaResource")
public final class sumaResource_it extends WTListResourceBundle {
   @RBEntry("Il modulo Gestione fornitori non è installato")
   public static final String MSG_SUMA_NOT_INSTALLED = "100";

   @RBEntry("Stato Acquisti")
   public static final String LBL_SOURCING_STATUS = "110";

   @RBEntry("Fornitore unico")
   public static final String LBL_SINGLE_SOURCE = "120";

   @RBEntry("Visualizza produttore")
   public static final String VIEW_MFR_URL_LABEL = "200";

   @RBEntry("Visualizza fornitore")
   public static final String VIEW_VDR_URL_LABEL = "210";
}
