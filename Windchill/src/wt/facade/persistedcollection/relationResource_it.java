/* bcwti
 *
 * Copyright (c) 2012 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.facade.persistedcollection;

import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;


/**
 * This is a default resource bundle containing entries for localized displays for the relations recorded from the
 * collector when packages are saved with soft filtering enabled.  Relationship information stored in the internal
 * graph extracted from the collector results will contain two pieces of information:<p>
 * <ol>
 * <li>Specific object identifier of the link class (where there is a link returned from the collector tool)</li>
 * <li>The integer key of the collection tool which collected the associated object (a.k.a. 'collected as key')</li>
 * </ol>
 * <p>
 * Relationship display values can be specified by bundle entries mapping by the 'collected as key', the link
 * class name, or a combination of both.  The lookup for a custom bundle entry will first look for an entry
 * for both the 'collected as key' and link class name (i.e. "CAK-10|wt.part.WTPartDescribeLink").  This allows for
 * the most specific labeling of the association.  If no entry matching this format is found, an entry matching only
 * the link class name will be looked up (i.e. "wt.note.NoteHolderNoteLink").  If still no entry is found, then a
 * lookup will be performed by the 'collected as key' (i.e. "CAK-2").<p>
 *
 * When specifying a custom bundle entry by link name only (i.e. <code>"wt.part.WTPartUsageLink"</code>), the
 * bundle entry itself can specify the actual display value in one of three ways:<p>
 * <ol>
 * <li>Specifying the localized display text directly.  Example:<br>
 * <br>
 * <code>
 *   @RBEntry("Uses")<br>
 *   public static final String WTPART_USAGE_LINK = "wt.part.WTPartUsageLink";<br>
 * </code>
 * </li>
 * <br>
 * <li>Specifying to use the localized display value from the class introspection, by specifying a bundle entry
 * value of "USE_MODEL_CLASS".  Example:<br>
 * <br>
 * <code>
 *   <i>// This will specify to use the display value from model obtained via introspection information for the link class</i><br>
 *   @RBComment("DO NOT TRANSLATE")<br>
 *   @RBEntry("USE_MODEL_CLASS")<br>
 *   public static final String WTPART_USAGE_LINK = "wt.part.WTPartUsageLink";<br>
 * </code>
 * </li>
 * <br>
 * <li>Specifying to use another bundle entry, by specifying a bundle entry value in the format
 * "USE_OTHER_ENTRY-<bundle name>|<bundle key>".  In this case, the specified entry will be looked up for the
 * specified bundle and used instead.   Example:<br>
 * <br>
 * <code>
 *   <i>// This will specify to use the display value from another bundle entry</i><br>
 *   @RBComment("DO NOT TRANSLATE")<br>
 *   @RBEntry("USE_OTHER_ENTRY-com.myCompany.myCustomBundle|customUsageLinkDisplayEntry")<br>
 *   public static final String WTPART_USAGE_LINK = "wt.part.WTPartUsageLink";<br>
 * </code>
 * </li>
 * </ol>
 * <p>
 * When specifying the bundle entry by both combination of the 'collected as key' and link class name, or the
 * 'collected as key' only, the bundle entry value can be specified in one of two ways:
 * <ol>
 * <li>Specifying the localized display text directly.  Example:<br>
 * <br>
 * <code>
 *   @RBEntry("Uses")<br>
 *   public static final String WTPART_USAGE_LINK = "CAK-2";<br>
 * </code>
 * </li>
 * <br>
 * <li>Specifying to use another bundle entry, by specifying a bundle entry value in the format
 * "USE_OTHER_ENTRY-<bundle name>|<bundle key>".  In this case, the specified entry will be looked up for the
 * specified bundle and used instead.   Example:<br>
 * <br>
 * <code>
 *   <i>// This will specify to use the display value from another bundle entry</i><br>
 *   @RBComment("DO NOT TRANSLATE")<br>
 *   @RBEntry("USE_OTHER_ENTRY-com.myCompany.myCustomBundle|customUsageLinkDisplayEntry")<br>
 *   public static final String WTPART_USAGE_LINK = "CAK-2";<br>
 * </code>
 * </li>
 * </ol>
 * <p>
 * If no entry is found, the relation key information will be returned as it was stored internally.  This will
 * be in format: "CAK-&lt;collected as key number&gt;|&lt;link OID, or blank if not available&gt;"
 * (i.e. <code>"CAK-123|com.myCompany.SomeLink:12345"</code>).  The oid, in particular, can be used if any
 * special processing is needed.<p>
 *
 * <BR><BR><B>Supported API: </B>true
 * <BR><BR><B>Extendable: </B>false
 */
@RBUUID("wt.facade.persistedcollection.relationResource")
public final class relationResource_it extends WTListResourceBundle {


   // ****************************************************************
   // Common entries
   // ****************************************************************

   @RBEntry("Componenti")
   @RBComment("This value would be returned by a service method providing information for customizations wanting to display package members and include the relationship as to why a member was collected and included in the package.  (i.e. Suppose Part1 uses Part2, and Part2 is collected into a package; a custom report might display Part2 with a value of 'Uses' in a relationship column to denote why it was collected.)")
   public static final String COMMON_USES = "COMMON_USES";

   @RBEntry("Descritto con")
   @RBComment("This value would be returned by a service method providing information for customizations wanting to display package members and include the relationship as to why a member was collected and included in the package.  (i.e. Suppose Part1 has described by document Doc1, and Doc1 is collected into a package; a custom report might display Doc1 with a value of 'Described By' in a relationship column to denote why it was collected.)")
   public static final String COMMON_DESCRIBED_BY = "COMMON_DESCRIBED_BY";

   @RBEntry("Riferimenti")
   @RBComment("This value would be returned by a service method providing information for customizations wanting to display package members and include the relationship as to why a member was collected and included in the package.  (i.e. Suppose Part1 has references document Doc1, and Doc1 is collected into a package; a custom report might display Doc1 with a value of 'References' in a relationship column to denote why it was collected.)")
   public static final String COMMON_REFERENCES = "COMMON_REFERENCES";



   // ****************************************************************
   // Collected as key entries
   // ****************************************************************


   // CADDOC_DEPENDENTS
   // ----------------------------------------------------------------

   @RBEntry("USE_OTHER_ENTRY-wt.facade.persistedcollection.relationResource|COMMON_USES")
   @RBComment("DO NOT TRANSLATE")
   public static final String CADDOC_REQUIRED_DEPENDENTS = "CAK-1";

   @RBEntry("USE_OTHER_ENTRY-wt.facade.persistedcollection.relationResource|COMMON_USES")
   @RBComment("DO NOT TRANSLATE")
   public static final String CADDOC_REQUIRED_MEMBER_DEPENDENTS = "CAK-101";

   @RBEntry("USE_OTHER_ENTRY-wt.facade.persistedcollection.relationResource|COMMON_USES")
   @RBComment("DO NOT TRANSLATE")
   public static final String CADDOC_REQUIRED_REF_DEPENDENTS = "CAK-102";

   @RBEntry("USE_OTHER_ENTRY-wt.facade.persistedcollection.relationResource|COMMON_USES")
   @RBComment("DO NOT TRANSLATE")
   public static final String CADDOC_REQUIRED_VARIANT_DEPENDENTS = "CAK-103";

   @RBEntry("USE_OTHER_ENTRY-wt.facade.persistedcollection.relationResource|COMMON_USES")
   @RBComment("DO NOT TRANSLATE")
   public static final String CADDOC_OPTIONAL_DEPENDENTS = "CAK-3";

   @RBEntry("USE_OTHER_ENTRY-wt.facade.persistedcollection.relationResource|COMMON_USES")
   @RBComment("DO NOT TRANSLATE")
   public static final String CADDOC_MEMBER_DEPENDENTS = "CAK-3001";

   @RBEntry("USE_OTHER_ENTRY-wt.facade.persistedcollection.relationResource|COMMON_USES")
   @RBComment("DO NOT TRANSLATE")
   public static final String CADDOC_OPTIONAL_REF_DEPENDENTS = "CAK-3002";

   @RBEntry("USE_OTHER_ENTRY-wt.facade.persistedcollection.relationResource|COMMON_USES")
   @RBComment("DO NOT TRANSLATE")
   public static final String CADDOC_OPTIONAL_VARIANT_DEPENDENTS = "CAK-3003";


   // WTPART_DEPENDENTS
   // ----------------------------------------------------------------

   @RBEntry("USE_OTHER_ENTRY-wt.facade.persistedcollection.relationResource|COMMON_USES")
   @RBComment("DO NOT TRANSLATE")
   public static final String PART_DEPENDENTS = "CAK-2";


   // WTDOC_DEPENDENTS
   // ----------------------------------------------------------------

   @RBEntry("USE_OTHER_ENTRY-wt.facade.persistedcollection.relationResource|COMMON_USES")
   @RBComment("DO NOT TRANSLATE")
   public static final String DOC_DEPENDENTS = "CAK-0";


   // DRAWINGS
   // ----------------------------------------------------------------

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|collected_as_drawing")
   @RBComment("DO NOT TRANSLATE")
   public static final String CADDOC_RELATED_DRAWING = "CAK-4";


   // FAMILY and GENERICS
   // ----------------------------------------------------------------

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|collected_as_family")
   @RBComment("DO NOT TRANSLATE")
   public static final String CADDOC_RELATED_FAMILY = "CAK-11";

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|collected_as_generic")
   @RBComment("DO NOT TRANSLATE")
   public static final String CADDOC_RELATED_GENERIC = "CAK-6";

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|collected_as_instance")
   @RBComment("DO NOT TRANSLATE")
   public static final String CADDOC_RELATED_INSTANCE = "CAK-5";


   // ASSOCIATED_EPMDOC
   // ----------------------------------------------------------------

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|collected_as_associated_caddoc")
   @RBComment("DO NOT TRANSLATE")
   public static final String CADDOC_RELATED_CADDOC = "CAK-9";

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|collected_as_associated_caddoc")
   @RBComment("DO NOT TRANSLATE")
   public static final String CADDOC_MPMLINK_RELATED_CADDOC = "CAK-501";


   // ASSOCIATED_WTPART
   // ----------------------------------------------------------------

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|collected_as_associated_wtpart")
   @RBComment("DO NOT TRANSLATE")
   public static final String PART_CADDOC_RELATED_PARTS = "CAK-8";

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|collected_as_associated_wtpart")
   @RBComment("DO NOT TRANSLATE")
   public static final String PART_DOC_RELATED_PARTS = "CAK-801";


   // PACKAGE_WHERE_USED
   // ----------------------------------------------------------------

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|collected_as_used_by")
   @RBComment("DO NOT TRANSLATE")
   public static final String CADDOC_WHERE_USED = "CAK-112";

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|collected_as_used_by")
   @RBComment("DO NOT TRANSLATE")
   public static final String PART_WHERE_USED = "CAK-113";

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|collected_as_used_by")
   @RBComment("DO NOT TRANSLATE")
   public static final String DOC_WHERE_USED = "CAK-114";


   // ASSOCIATED_WTDOCS
   // ----------------------------------------------------------------

   @RBEntry("USE_OTHER_ENTRY-wt.facade.persistedcollection.relationResource|COMMON_DESCRIBED_BY")
   @RBComment("DO NOT TRANSLATE")
   public static final String DOC_RELATED_DESCRIBE_DOC = "CAK-10|wt.part.WTPartDescribeLink";

   @RBEntry("USE_OTHER_ENTRY-wt.facade.persistedcollection.relationResource|COMMON_REFERENCES")
   @RBComment("DO NOT TRANSLATE")
   public static final String DOC_RELATED_REFERENCE_DOC = "CAK-10|wt.part.WTPartReferenceLink";

   @RBEntry("USE_OTHER_ENTRY-wt.facade.persistedcollection.relationResource|COMMON_DESCRIBED_BY")
   @RBComment("DO NOT TRANSLATE")
   public static final String DOC_MPMLINK_RELATED_DESCRIBE_DOC = "CAK-505|wt.part.WTPartDescribeLink";

   @RBEntry("USE_OTHER_ENTRY-wt.facade.persistedcollection.relationResource|COMMON_REFERENCES")
   @RBComment("DO NOT TRANSLATE")
   public static final String DOC_MPMLINK_RELATED_REF_DOC = "CAK-518|wt.part.WTPartReferenceLink";

   @RBEntry("USE_OTHER_ENTRY-wt.facade.persistedcollection.relationResource|COMMON_REFERENCES")
   @RBComment("DO NOT TRANSLATE")
   public static final String DOC_DOC_RELATED_DOC = "CAK-811|wt.doc.WTDocumentDependencyLink";


   // CHANGE_OBJECTS_AFFECTED_DATA
   // ----------------------------------------------------------------

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|collected_as_affected_data")
   @RBComment("DO NOT TRANSLATE")
   public static final String CHANGE_PROBLEM_REPORT_RELATED_CHANGEABLE = "CAK-12";

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|collected_as_affected_data")
   @RBComment("DO NOT TRANSLATE")
   public static final String CHANGE_VARIANCE_RELATED_CHANGEABLE = "CAK-13";

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|collected_as_affected_data")
   @RBComment("DO NOT TRANSLATE")
   public static final String CHANGE_REQUEST_RELATED_CHANGEABLE = "CAK-14";

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|collected_as_affected_data")
   @RBComment("DO NOT TRANSLATE")
   public static final String CHANGE_ACTIVITY_RELATED_CHANGEABLE = "CAK-15";


   // CHANGE_ACTIVITY_RESULTING_DATA
   // ----------------------------------------------------------------

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|collected_as_resulting_data")
   @RBComment("DO NOT TRANSLATE")
   public static final String CHANGE_ACTIVITY_RESULTING_DATA = "CAK-41";

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|collected_as_resulting_data")
   @RBComment("DO NOT TRANSLATE")
   public static final String CHANGE_ACTIVITY_RESULTING_DATA_INCL_HANGING_CHANGE = "CAK-42";

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|collected_as_resulting_data")
   @RBComment("DO NOT TRANSLATE")
   public static final String CHANGE_ACTIVITY_HANGING_CHANGE = "CAK-43";


   // RELATED_NOTES
   // ----------------------------------------------------------------

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|related_note")
   @RBComment("DO NOT TRANSLATE")
   public static final String NOTES_PART_RELATED_NOTES = "CAK-300";

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|related_usagelink_note")
   @RBComment("DO NOT TRANSLATE")
   public static final String NOTES_USAGE_LINK_RELATED_NOTES = "CAK-301";


   // PUBLISHED_CONTENT
   // ----------------------------------------------------------------

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|collected_as_associated_published_content")
   @RBComment("DO NOT TRANSLATE")
   public static final String CADDOC_PUBLISHED_CONTENT = "CAK-601";


   // PROBLEM_REPORT
   // ----------------------------------------------------------------

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|related_problem_report")
   @RBComment("DO NOT TRANSLATE")
   public static final String CHANGE_REQUEST_RELATED_PROBLEM_REPORT = "CAK-16";

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|related_problem_report")
   @RBComment("DO NOT TRANSLATE")
   public static final String CHANGE_CHANGEABLE_RELATED_PROBLEM_REPORT = "CAK-17";

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|related_problem_report")
   @RBComment("DO NOT TRANSLATE")
   public static final String CHANGE_NOTICE_RELATED_PROBLEM_REPORT = "CAK-38";


   // VARIANCE
   // ----------------------------------------------------------------

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|related_variance")
   @RBComment("DO NOT TRANSLATE")
   public static final String CHANGE_REQUEST_RELATED_VARIANCE = "CAK-18";

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|related_variance")
   @RBComment("DO NOT TRANSLATE")
   public static final String CHANGE_CHANGEABLE_RELATED_VARIANCE = "CAK-19";

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|related_variance")
   @RBComment("DO NOT TRANSLATE")
   public static final String CHANGE_NOTICE_RELATED_VARIANCE = "CAK-39";


   // CHANGE_REQUEST
   // ----------------------------------------------------------------

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|related_change_request")
   @RBComment("DO NOT TRANSLATE")
   public static final String CHANGE_NOTICE_RELATED_REQUEST = "CAK-20";

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|related_change_request")
   @RBComment("DO NOT TRANSLATE")
   public static final String CHANGE_PROBLEM_REPORT_RELATED_REQUEST = "CAK-22";

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|related_change_request")
   @RBComment("DO NOT TRANSLATE")
   public static final String CHANGE_VARIANCE_RELATED_REQUEST = "CAK-23";

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|related_change_request")
   @RBComment("DO NOT TRANSLATE")
   public static final String CHANGE_CHANGEABLE_RELATED_REQUEST = "CAK-24";


   // CHANGE_ACTIVITY
   // ----------------------------------------------------------------

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|related_change_activity")
   @RBComment("DO NOT TRANSLATE")
   public static final String CHANGE_NOTCIE_RELATED_REQUEST = "CAK-33";

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|related_change_activity")
   @RBComment("DO NOT TRANSLATE")
   public static final String CHANGE_CHANGEABLE_RELATED_CHANGE_ACTIVITY = "CAK-34";


   // CHANGE_NOTICE
   // ----------------------------------------------------------------

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|related_change_notice")
   @RBComment("DO NOT TRANSLATE")
   public static final String CHANGE_ACTIVITY_RELATED_CHANGE_NOTCE = "CAK-35";

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|related_change_notice")
   @RBComment("DO NOT TRANSLATE")
   public static final String CHANGE_REQUEST_RELATED_CHANGE_NOTICE = "CAK-36";

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|related_change_notice")
   @RBComment("DO NOT TRANSLATE")
   public static final String CHANGE_CHANGEABLE_RELATED_CHANGE_NOTICE = "CAK-37";

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|related_change_notice")
   @RBComment("DO NOT TRANSLATE")
   public static final String CHANGE_NOTICE_RELATED_CHANGE_NOTICE = "CAK-40";


   // HANGING_CHANGE
   // ----------------------------------------------------------------

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|related_released_change_notice")
   @RBComment("DO NOT TRANSLATE")
   public static final String CHANGE_CHANGEABLE_RELATED_RELEASED_CHANGE_NOTICE_ARC = "CAK-44";

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|related_hanging_change")
   @RBComment("DO NOT TRANSLATE")
   public static final String CHANGE_CHANGEABLE_RELATED_HANGING_CHANGE_AHC = "CAK-45";

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|related_hanging_change")
   @RBComment("DO NOT TRANSLATE")
   public static final String CHANGE_CHANGEABLE_RELATED_HANGING_CHANGE = "CAK-46";

   @RBEntry("USE_OTHER_ENTRY-com.ptc.core.htmlcomp.collection.collectionResource|related_released_change_notice")
   @RBComment("DO NOT TRANSLATE")
   public static final String CHANGE_CHANGEABLE_RELATED_RELEASED_CHANGE_NOTICE = "CAK-47";


   // SPECIFICATION_MEMBERS_PKG
   // ----------------------------------------------------------------

   @RBEntry("Membro specifica")
   @RBComment("This value would be returned by a service method providing information for customizations wanting to display package members and include the relationship as to why a member was collected and included in the package.  (i.e. Suppose specification Spec1 has member specification Spec2 and Spec2 is collected into a package; a custom report might display Spec2 with a value of 'Specification Member' in a relationship column to denote why it was collected.)")
   public static final String SPECIFICATION_MEMBERS_ALL = "CAK-705";

   @RBEntry("Membro sezione specifica")
   @RBComment("This value would be returned by a service method providing information for customizations wanting to display package members and include the relationship as to why a member was collected and included in the package.  (i.e. Suppose specification Spec1 has a section member specification Spec2 and Spec2 is collected into a package; a custom report might display Spec2 with a value of 'Specification Section Member' in a relationship column to denote why it was collected.)")
   public static final String SPECIFICATION_MEMBERS_SECTION = "CAK-706";

}
