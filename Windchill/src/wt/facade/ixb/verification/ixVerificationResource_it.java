/* bcwti
 *
 * Copyright (c) 2012 Parametric Technology Corporation (PTC). All Rights
 * Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.facade.ixb.verification;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

/**
 * This is a resource file for IX data collection and verification framework.
 * <BR>
 * Resource key values are kept in Capital Case as it is being used as Element tags in XML.
 * Please don't change it to standard format (with '_').
 *
 * <BR>
 * <BR>
 * <B>Supported API: </B>false <BR>
 * <BR>
 * <B>Extendable: </B>false
 */
@RBUUID("wt.facade.ixb.verification.ixVerificationResource")
public final class ixVerificationResource_it extends WTListResourceBundle {

    @RBEntry("Report verifica operazione di importazione")
    public static final String IMPORT_REPORT_TITLE = "IMPORT_REPORT_TITLE";

    @RBEntry("Report verifica operazione di esportazione")
    public static final String EXPORT_REPORT_TITLE = "EXPORT_REPORT_TITLE";

    @RBEntry("Report di riepilogo importazione per consegna ricevuta - {0}")
    @RBArgComment0("Received Delivery Identity")
    public static final String RD_SUMMARY_REPORT_TITLE = "RD_SUMMARY_REPORT_TITLE";

    @RBEntry("Numero totale di oggetti elaborati")
    public static final String OBJECTS_PROCESSED = "ObjectsProcessed";
    // Don't change 'ObjectsProcessed' to 'OBJECTS_PROCESSED' above & elsewhere as per standard format. This is done intentionally as keys are being used as XML element tags.

    @RBEntry("Numero totale di business object")
    public static final String BUSINESS_OBJECTS = "BusinessObjects";

    @RBEntry("Numero totale di business object saltati")
    public static final String BUSINESS_OBJECTS_SKIPPED = "BusinessObjectsSkipped";

    @RBEntry("Numero totale di business object")
    public static final String BUSINESS_OBJECTS_IGNORED = "BusinessObjectsIgnored";

    @RBEntry("Numero totale di file di contenuto")
    public static final String CONTENTS = "Contents";

    @RBEntry("Numero totale di file di contenuto saltati")
    public static final String CONTENTS_SKIPPED = "ContentsSkipped";

    @RBEntry("Numero totale di file di contenuto ignorati")
    public static final String CONTENTS_IGNORED = "ContentsIgnored";

    @RBEntry("Numero di business object basati sul tipo")
    public static final String BUSINESS_OBJECTS_BY_TYPE = "BusinessObjectsByType";

    @RBEntry("Numero totale di oggetti nuovi creati")
    public static final String NEW_OBJECTS = "NewObjects";

    @RBEntry("Numero totale di oggetti esistenti aggiornati")
    public static final String UPDATED_OBJECTS = "UpdatedObjects";

    @RBEntry("Numero di oggetti ignorati a causa di conflitti")
    public static final String SKIPPED_OBJECTS = "SkippedObjects";

    @RBEntry("Numero di oggetti non supportati per l'importazione")
    public static final String IGNORED_OBJECTS = "IgnoredObjects";

    @RBEntry("Non supportati per l'importazione")
    public static final String OBJECTS_IGNORED_CATEGORY = "IgnoredObjectsByType";

    @RBEntry("Risoluzione conflitto da ignorare")
    public static final String OBJECTS_SKIPPED_CATEGORY = "SkippedObjectsByType";

    @RBEntry("Esportazione")
    public static final String OBJECTS_EXPORT_CATEGORY = "ExportedObjectsByType";

    @RBEntry("Importazione")
    public static final String OBJECTS_IMPORT_CATEGORY = "ImportedObjectsByType";

    @RBEntry("Numero di oggetti elaborati per esportazione")
    public static final String OBJECTS_PROCESSED_BY_EXPORT = "ObjectsProcessedByExport";

    @RBEntry("Numero di oggetti elaborati per importazione")
    public static final String OBJECTS_PROCESSED_BY_IMPORT = "ObjectsProcessedByImport";

    @RBEntry("Numero di oggetti non più inclusi nella consegna ricevuta")
    public static final String ABSENT_OBJECTS = "AbsentObjects";

    @RBEntry("Numero di oggetti di cui si è tentata l'eliminazione")
    public static final String DELETED_OBJECTS = "DeletedObjects";

    @RBEntry("È possibile che alcuni oggetti non siano stati importati in Windchill. Alcune cause possibili sono la risoluzione di un conflitto o un tipo di oggetto non supportato. Per informazioni dettagliate, vedere il log di importazione.")
    public static final String REFER_IMPORT_LOGS_MSG = "REFER_IMPORT_LOGS_MSG";
}
