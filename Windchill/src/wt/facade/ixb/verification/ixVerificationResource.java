/* bcwti
 *
 * Copyright (c) 2012 Parametric Technology Corporation (PTC). All Rights
 * Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.facade.ixb.verification;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

/**
 * This is a resource file for IX data collection and verification framework.
 * <BR>
 * Resource key values are kept in Capital Case as it is being used as Element tags in XML.
 * Please don't change it to standard format (with '_').
 *
 * <BR>
 * <BR>
 * <B>Supported API: </B>false <BR>
 * <BR>
 * <B>Extendable: </B>false
 */
@RBUUID("wt.facade.ixb.verification.ixVerificationResource")
public final class ixVerificationResource extends WTListResourceBundle {

    @RBEntry("Verification Report of Import Operation")
    public static final String IMPORT_REPORT_TITLE = "IMPORT_REPORT_TITLE";

    @RBEntry("Verification Report of Export Operation")
    public static final String EXPORT_REPORT_TITLE = "EXPORT_REPORT_TITLE";

    @RBEntry("Import Summary Report for Received Delivery - {0}")
    @RBArgComment0("Received Delivery Identity")
    public static final String RD_SUMMARY_REPORT_TITLE = "RD_SUMMARY_REPORT_TITLE";

    @RBEntry("Total number of objects processed")
    public static final String OBJECTS_PROCESSED = "ObjectsProcessed";
    // Don't change 'ObjectsProcessed' to 'OBJECTS_PROCESSED' above & elsewhere as per standard format. This is done intentionally as keys are being used as XML element tags.

    @RBEntry("Total number of business objects")
    public static final String BUSINESS_OBJECTS = "BusinessObjects";

    @RBEntry("Total number of business objects skipped")
    public static final String BUSINESS_OBJECTS_SKIPPED = "BusinessObjectsSkipped";

    @RBEntry("Total number of business objects")
    public static final String BUSINESS_OBJECTS_IGNORED = "BusinessObjectsIgnored";

    @RBEntry("Total number of content files")
    public static final String CONTENTS = "Contents";

    @RBEntry("Total number of content files skipped")
    public static final String CONTENTS_SKIPPED = "ContentsSkipped";

    @RBEntry("Total number of content files ignored")
    public static final String CONTENTS_IGNORED = "ContentsIgnored";

    @RBEntry("Number of business objects based on type")
    public static final String BUSINESS_OBJECTS_BY_TYPE = "BusinessObjectsByType";

    @RBEntry("Total number of objects newly created")
    public static final String NEW_OBJECTS = "NewObjects";

    @RBEntry("Total number of existing objects updated")
    public static final String UPDATED_OBJECTS = "UpdatedObjects";

    @RBEntry("Number of objects skipped due to conflict")
    public static final String SKIPPED_OBJECTS = "SkippedObjects";

    @RBEntry("Number of objects not supported for Import")
    public static final String IGNORED_OBJECTS = "IgnoredObjects";

    @RBEntry("Not Supported for Import")
    public static final String OBJECTS_IGNORED_CATEGORY = "IgnoredObjectsByType";

    @RBEntry("Conflict Resolution to Skip")
    public static final String OBJECTS_SKIPPED_CATEGORY = "SkippedObjectsByType";

    @RBEntry("Export")
    public static final String OBJECTS_EXPORT_CATEGORY = "ExportedObjectsByType";

    @RBEntry("Import")
    public static final String OBJECTS_IMPORT_CATEGORY = "ImportedObjectsByType";

    @RBEntry("Number of Objects Processed by Export")
    public static final String OBJECTS_PROCESSED_BY_EXPORT = "ObjectsProcessedByExport";

    @RBEntry("Number of Objects Processed by Import")
    public static final String OBJECTS_PROCESSED_BY_IMPORT = "ObjectsProcessedByImport";

    @RBEntry("Number of Objects that are no longer included in the received delivery")
    public static final String ABSENT_OBJECTS = "AbsentObjects";

    @RBEntry("Number of Objects for which deletion was attempted")
    public static final String DELETED_OBJECTS = "DeletedObjects";

    @RBEntry("Some objects may not have been imported into Windchill. Possible reasons for skipping import include a conflict resolution or an unsupported object type. Refer to the Import Log for detailed information.")
    public static final String REFER_IMPORT_LOGS_MSG = "REFER_IMPORT_LOGS_MSG";
}
