/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.facade.scm;

import wt.util.resource.*;

@RBUUID("wt.facade.scm.scmResource")
public final class scmResource_it extends WTListResourceBundle {
   @RBEntry("Il modulo ClearCase Integration non è installato")
   public static final String MSG_SCMI_NOT_INSTALLED = "100";

   @RBEntry("Attributi aggiuntivi")
   @RBComment("Title of the table that shows additional SCM attributes.")
   public static final String SCM_ATTRIBUTES = "110";

   @RBEntry("Nome")
   @RBComment("Title of the \"Name\" (1st) column in the additional attributes table.")
   public static final String SCM_ATTR_NAME = "120";

   @RBEntry("Valore")
   @RBComment("Title of the \"Value\" (2nd) column in the additional attributes table.")
   public static final String SCM_ATTR_VALUE = "130";

   @RBEntry("Versione elemento")
   @RBComment("SCM version id of SCM file attribute.")
   public static final String SCM_ELEMENT_VERSION = "140";

   @RBEntry("Posizione di ClearCase")
   @RBComment("The file path within the SCM repository.")
   public static final String SCM_LOCATION = "150";

   @RBEntry("Repository elementi")
   @RBComment("SCM respository. For ClearCase this corresponds to the VOB (\"Versioned Object Base\") name.")
   public static final String SCM_REPOSITORY = "160";

   @RBEntry("Data ultima modifica")
   @RBComment("Date the file version was last modified. ")
   public static final String SCM_LAST_MODIFIED_DATE = "170";

   @RBEntry("Autore modifiche")
   @RBComment("User who last modified the file.")
   public static final String SCM_MODIFIED_BY = "180";

   @RBEntry("Applicazione proprietaria")
   @RBComment("The type of SCM application to which the element belongs.")
   public static final String SCM_OWNER_APPLICATION = "190";

   @RBEntry("ClearCase")
   @RBComment("Name of the software configuration management application.")
   public static final String CLEARCASE_NAME = "200";

   @RBEntry("Vista di ClearCase di default:")
   @RBComment("Label in the project info page for the ClearCase view of the project.")
   public static final String DEFAULT_CLEARCASE_VIEW_LABEL = "210";

   @RBEntry("File di ClearCase")
   @RBComment("Label for the Tooltip of a ClearCase File")
   public static final String CLEARCASE_FILE_TOOLTIP = "220";

   @RBEntry("Directory di ClearCase")
   @RBComment("Label for the Tooltip of a ClearCase Directory")
   public static final String CLEARCASE_DIRECTORY_TOOLTIP = "230";

   @RBEntry("Adattatore ClearCase")
   @RBComment("Label for the Tooltip of an Scm Adapter")
   public static final String SCM_ADAPTER_TOOLTIP = "240";

   @RBEntry("Scarica il contenuto del file di ClearCase - dimensione file: {0} KB")
   @RBComment("Tool tip for format icon that download attachment with file size information.")
   public static final String TOOL_TIP_DOWNLOAD_1 = "250";

   @RBEntry("Scarica il contenuto della directory di ClearCase.")
   @RBComment("Tool tip for format icon that download ClearCase attachment that happens to be a directory.")
   public static final String TOOL_TIP_DOWNLOAD_2 = "260";
}
