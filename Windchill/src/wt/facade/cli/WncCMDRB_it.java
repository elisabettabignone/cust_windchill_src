/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights
 * Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.facade.cli;

import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.facade.cli.WncCMDRB")
public class WncCMDRB_it extends WTListResourceBundle {
	
	@RBEntry("Il comando seguente non è valido: \"{0}\"")
	public static final String INVALID_COMMAND = "INVALID_COMMAND";
		
	@RBEntry("Stampa le informazioni sul comando.")
	public static final String HELP_OPTION_DESCRIPTION = "HELP_OPTION_DESCRIPTION";

	@RBEntry("Nome utente Windchill.")
	public static final String USER_NAME_OPTION_DESCRIPTION = "USER_NAME_OPTION_DESCRIPTION";

	@RBEntry("L'estensione del file seguente non è valida: \\'{0}\\'.")
	public static final String INVALID_FILE_EXT = "INVALID_FILE_EXT";
	
	@RBEntry("Il file seguente non esiste: \\'{0}\\'.")
	public static final String FILE_DOES_NOT_EXIST = "FILE_DOES_NOT_EXIST";
	
	@RBEntry("La posizione seguente non è valida o non è accessibile: \\'{0}\\'.")
	public static final String INVALID_FILE_PATH = "INVALID_REMOTE_PATH";
	
	@RBEntry("Errore durante la lettura del file seguente: \\'{0}\\'.")
	public static final String FILE_IO_ERROR = "FILE_IO_ERROR";
	
	@RBEntry("Nessun argomento deve essere specificato per l'opzione seguente: ")
	public static final String OPTION_NOT_ACCEPT_ARGUMENT = "OPTION_NOT_ACCEPT_ARGUMENT ";
	
	@RBEntry("È necessario specificare un argomento per l'opzione seguente: \\'{0}\\'")
	public static final String ARGUMENT_NOT_PROVIDED = "ARGUMENT_NOT_PROVIDED";
	
	@RBEntry("È necessario specificare un argomento per l'opzione seguente: \\'{0}\\'")
	public static final String ARGUMENT_IS_MISSING = "ARGUMENT_IS_MISSING";
	
	@RBEntry("L'opzione seguente non è valida per il comando: \\'{0}\\'.")
	public static final String INVALID_OPTION = "INVALID_OPTION";
	
	@RBEntry("Impossibile utilizzare le opzioni seguenti in contemporanea: \\'{0}\\', \\'{1}\\'.")
	public static final String MUTUALLY_EXCLUSIVE = "MUTUALLY_EXCLUSIVE";

	@RBEntry("L'utente non è un utente Windchill valido.")
	public static final String INVALID_USER_NAME = "INVALID_USER_NAME";
	
	@RBEntry("Impossibile accedere al server. Contattare l'amministratore.")
	public static final String SERVER_NOT_ACCESSIBLE = "SERVER_NOT_ACCESSIBLE";
	
	
}
