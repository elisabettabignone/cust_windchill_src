/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights
 * Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.facade.cli;

import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.facade.cli.WncCMDRB")
public class WncCMDRB extends WTListResourceBundle {
	
	@RBEntry("The following command is invalid: \"{0}\".")
	public static final String INVALID_COMMAND = "INVALID_COMMAND";
		
	@RBEntry("Prints help of the command.")
	public static final String HELP_OPTION_DESCRIPTION = "HELP_OPTION_DESCRIPTION";

	@RBEntry("Windchill user name.")
	public static final String USER_NAME_OPTION_DESCRIPTION = "USER_NAME_OPTION_DESCRIPTION";

	@RBEntry("The following file has an invalid file extension:\'{0}\'.")
	public static final String INVALID_FILE_EXT = "INVALID_FILE_EXT";
	
	@RBEntry("The following file  does not exist:\'{0}\'.") 
	public static final String FILE_DOES_NOT_EXIST = "FILE_DOES_NOT_EXIST";
	
	@RBEntry("The following location is invalid or not accessible:\'{0}\'.")
	public static final String INVALID_FILE_PATH = "INVALID_REMOTE_PATH";
	
	@RBEntry("An error occurred while reading the following file:\'{0}\'.")
	public static final String FILE_IO_ERROR = "FILE_IO_ERROR";
	
	@RBEntry("An argument should not be specified for the following option: ")
	public static final String OPTION_NOT_ACCEPT_ARGUMENT = "OPTION_NOT_ACCEPT_ARGUMENT ";
	
	@RBEntry("An argument should be specified for the following option: \'{0}\'")
	public static final String ARGUMENT_NOT_PROVIDED = "ARGUMENT_NOT_PROVIDED";
	
	@RBEntry("An argument should be specified for the following option: \'{0}\'")
	public static final String ARGUMENT_IS_MISSING = "ARGUMENT_IS_MISSING";
	
	@RBEntry("The following option is not valid for the command: \'{0}\'.")
	public static final String INVALID_OPTION = "INVALID_OPTION";
	
	@RBEntry("The following options cannot be used concurrently: \'{0}\', \'{1}\'.")
	public static final String MUTUALLY_EXCLUSIVE = "MUTUALLY_EXCLUSIVE";

	@RBEntry("The user is not a valid Windchill user.")
	public static final String INVALID_USER_NAME = "INVALID_USER_NAME";
	
	@RBEntry("Server was not accessible. Contact your administrator.")
	public static final String SERVER_NOT_ACCESSIBLE = "SERVER_NOT_ACCESSIBLE";
	
	
}
