/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.admin;

import wt.util.resource.*;

@RBUUID("wt.admin.adminEventResource")
public final class adminEventResource extends WTListResourceBundle {
   @RBEntry("ALL")
   public static final String PRIVATE_CONSTANT_0 = "ALL";

   @RBEntry("CHECKIN")
   public static final String PRIVATE_CONSTANT_1 = "*/wt.vc.wip.WorkInProgressServiceEvent/POST_CHECKIN";

   @RBEntry("CHECKOUT")
   public static final String PRIVATE_CONSTANT_2 = "*/wt.vc.wip.WorkInProgressServiceEvent/POST_CHECKOUT";

   @RBEntry("CREATE")
   public static final String PRIVATE_CONSTANT_3 = "*/wt.fc.PersistenceManagerEvent/POST_STORE";

   @RBEntry("DELETE")
   public static final String PRIVATE_CONSTANT_4 = "*/wt.fc.PersistenceManagerEvent/POST_DELETE";

   @RBEntry("MODIFY")
   public static final String PRIVATE_CONSTANT_5 = "*/wt.fc.PersistenceManagerEvent/POST_MODIFY";

   @RBEntry("LOCK")
   public static final String PRIVATE_CONSTANT_6 = "*/wt.locks.LockServiceEvent/POST_LOCK";

   @RBEntry("UNLOCK")
   public static final String PRIVATE_CONSTANT_7 = "*/wt.locks.LockServiceEvent/POST_UNLOCK";

   @RBEntry("STATE CHANGE")
   public static final String PRIVATE_CONSTANT_8 = "*/wt.lifecycle.LifeCycleServiceEvent/STATE_CHANGE";

   @RBEntry("SEND TO PDM")
   public static final String PRIVATE_CONSTANT_9 = "*/wt.sandbox.SandboxServiceCheckinEvent/POST_SB_CHECKIN_EVENT";

   @RBEntry("UNDO CHECKOUT")
   public static final String PRIVATE_CONSTANT_10 = "*/wt.vc.wip.WorkInProgressServiceEvent/POST_UNDO_CHECKOUT";

   @RBEntry("NEW VERSION")
   public static final String PRIVATE_CONSTANT_11 = "*/wt.vc.VersionControlServiceEvent/NEW_VERSION";

   @RBEntry("MERGE")
   public static final String PRIVATE_CONSTANT_12 = "*/wt.vc.VersionControlServiceEvent/POST_MERGE";

   @RBEntry("CHANGE DOMAIN")
   public static final String PRIVATE_CONSTANT_13 = "*/wt.admin.AdministrativeDomainManagerEvent/POST_CHANGE_DOMAIN";

   @RBEntry("CHANGE LOCATION")
   public static final String PRIVATE_CONSTANT_14 = "*/wt.folder.FolderServiceEvent/POST_CHANGE_FOLDER";

   @RBEntry("OVERFLOW")
   public static final String PRIVATE_CONSTANT_15 = "*/wt.fv.FvServiceEvent/OVERFLOW";

   @RBEntry("PROBLEM REPORT FORMALIZED")
   public static final String PRIVATE_CONSTANT_16 = "*/wt.change2.ChangeService2Event/ISSUE_FORMALIZED";

   @RBEntry("PROBLEM REPORT UNFORMALIZED")
   public static final String PRIVATE_CONSTANT_17 = "*/wt.change2.ChangeService2Event/ISSUE_UNFORMALIZED";

   @RBEntry("CHANGE ACTIVITY STATE CHANGE")
   public static final String PRIVATE_CONSTANT_18 = "*/wt.change2.ChangeService2Event/CA_STATE_CHANGED";

   @RBEntry("CHANGE NOTICE STATE CHANGE")
   public static final String PRIVATE_CONSTANT_19 = "*/wt.change2.ChangeService2Event/CN_STATE_CHANGED";

   @RBEntry("SAVE NEW SELECTOR")
   public static final String PRIVATE_CONSTANT_20 = "*/com.ptc.gateway.ilpdm.mapping.I2WServiceEvent/SAVE_NEW_SELECTOR";

   @RBEntry("PUBLISH IS DONE SUCCESSFULLY")
   public static final String PRIVATE_CONSTANT_21 = "*/com.ptc.gateway.ilpdm.mapping.I2WServiceEvent/PUBLISH_SUCCESS";

   @RBEntry("PUBLISH IS DONE WITH ERRORS")
   public static final String PRIVATE_CONSTANT_22 = "*/com.ptc.gateway.ilpdm.mapping.I2WServiceEvent/PUBLISH_NOT_SUCCESS";

   @RBEntry("RESOLVE ACTION ITEM")
   public static final String PRIVATE_CONSTANT_23 = "*/wt.meeting.actionitem.ActionItemEvent/RESOLVE";

   @RBEntry("UPDATE ACTION ITEM")
   public static final String PRIVATE_CONSTANT_24 = "*/wt.meeting.actionitem.ActionItemEvent/UPDATE";

   @RBEntry("DELETE ACTION ITEM")
   public static final String PRIVATE_CONSTANT_25 = "*/wt.meeting.actionitem.ActionItemEvent/DELETE";

   @RBEntry("VISUALIZATION REPRESENTATION SAVED")
   public static final String PRIVATE_CONSTANT_26 = "*/com.ptc.wvs.server.loader.GraphicsServerLoaderServiceEvent/REPRESENTATION_SAVED";

   @RBEntry("VISUALIZATION MARKUP SAVED")
   public static final String PRIVATE_CONSTANT_27 = "*/com.ptc.wvs.server.loader.GraphicsServerLoaderServiceEvent/MARKUP_SAVED";

   @RBEntry("VISUALIZATION PUBLISH SUCCESSFUL")
   public static final String PRIVATE_CONSTANT_28 = "*/com.ptc.wvs.server.publish.PublishServiceEvent/PUBLISH_SUCCESSFUL";

   @RBEntry("VISUALIZATION PUBLISH NOT SUCCESSFUL")
   public static final String PRIVATE_CONSTANT_29 = "*/com.ptc.wvs.server.publish.PublishServiceEvent/PUBLISH_NOT_SUCCESSFUL";

   @RBEntry("ADD AML")
   public static final String PRIVATE_CONSTANT_30 = "*/com.ptc.windchill.suma.axl.AXLServiceEvent/ADD_AML";

   @RBEntry("ADD AVL")
   public static final String PRIVATE_CONSTANT_31 = "*/com.ptc.windchill.suma.axl.AXLServiceEvent/ADD_AVL";

   @RBEntry("REMOVE AML")
   public static final String PRIVATE_CONSTANT_32 = "*/com.ptc.windchill.suma.axl.AXLServiceEvent/REMOVE_AML";

   @RBEntry("REMOVE AVL")
   public static final String PRIVATE_CONSTANT_33 = "*/com.ptc.windchill.suma.axl.AXLServiceEvent/REMOVE_AVL";

   @RBEntry("MODIFY AML PREFERENCE")
   public static final String PRIVATE_CONSTANT_34 = "*/com.ptc.windchill.suma.axl.AXLServiceEvent/MODIFY_AML";

   @RBEntry("MODIFY AVL PREFERENCE")
   public static final String PRIVATE_CONSTANT_35 = "*/com.ptc.windchill.suma.axl.AXLServiceEvent/MODIFY_AVL";

   @RBEntry("CREATED")
   public static final String PRIVATE_CONSTANT_36 = "*/com.ptc.windchill.ixb.importer.ImportJobEvent/CREATED";

   @RBEntry("SUBMITTED")
   public static final String PRIVATE_CONSTANT_37 = "*/com.ptc.windchill.ixb.importer.ImportJobEvent/SUBMITTED";

   @RBEntry("STARTED")
   public static final String PRIVATE_CONSTANT_38 = "*/com.ptc.windchill.ixb.importer.ImportJobEvent/STARTED";

   @RBEntry("COMPLETED")
   public static final String PRIVATE_CONSTANT_39 = "*/com.ptc.windchill.ixb.importer.ImportJobEvent/COMPLETED";

   @RBEntry("CANCELED")
   public static final String PRIVATE_CONSTANT_40 = "*/com.ptc.windchill.ixb.importer.ImportJobEvent/CANCELED";

   @RBEntry("FAILED")
   public static final String PRIVATE_CONSTANT_41 = "*/com.ptc.windchill.ixb.importer.ImportJobEvent/FAILED";
}
