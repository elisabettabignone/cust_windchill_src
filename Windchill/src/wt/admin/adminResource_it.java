/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.admin;

import wt.util.resource.*;

@RBUUID("wt.admin.adminResource")
public final class adminResource_it extends WTListResourceBundle {
   @RBEntry("L'operazione \"{0}\" non è riuscita.")
   @RBComment("WTException: A specified operation failed during installation")
   @RBArgComment0("Name of operation that failed")
   public static final String OPERATION_FAILED = "0";

   @RBEntry("Operazione non valida: impossibile eliminare \"{0}\".")
   @RBComment("Message stating that specified user (intended for admin) cannot be deleted")
   @RBArgComment0("Name of object that cannot be deleted")
   public static final String ADMIN_DELETE = "1";

   @RBEntry("Impossibile modificare il nome del dominio predefinito \"{0}\".")
   @RBComment("Message stating that a specified predefined domain's name cannot be changed")
   @RBArgComment0("Name of domain whose name cannot be changed")
   public static final String CANT_CHANGE_NAME = "2";

   @RBEntry("Impossibile inizializzare il dominio amministrativo.")
   @RBComment("Message stating that there was a failure to initialize administrative domain")
   public static final String MISSING_DOMAIN = "3";

   @RBEntry("L'evento \"{0}\" non esiste.")
   @RBComment("AdministrativeDomainException: An event with the specified name does not exist")
   @RBArgComment0("Name of event that does not exist")
   public static final String INVALID_EVENT = "4";

   @RBEntry("Impossibile eliminare i domini.")
   @RBComment("Message stating that domains cannot be deleted")
   public static final String CANT_DELETE_DOMAIN = "5";

   @RBEntry("Impossibile utilizzare \"{0}\" per eliminare il dominio di oggetti persistenti.")
   @RBComment("WTRuntimeException: Persistent objects cannot be changed using a specified means")
   @RBArgComment0("Method name: DomainAdministered.setAdminDomain")
   public static final String CANT_CHANGE_DOMAIN = "6";

   @RBEntry("Numero di attributi errato.")
   @RBComment("WTException: The number of arguments provided to the doInstall API is incorrect")
   public static final String WRONG_NUMBER_ATTRS = "7";

   @RBEntry("Dominio dell'oggetto nullo. Impossibile inizializzare.")
   @RBComment("WTRuntimeException: The domain specified for a new AdminDomainRef is null so the domain reference cannot be initialized")
   public static final String NULL_DOMAIN = "8";

   @RBEntry("Nessun dominio assegnato all'oggetto amministrato. Impossibile verificare la regola di accesso. Classe di oggetti  \"{0}\", identificatore di visualizzazione dell'oggetto \"{1}\", identificatore dell'oggetto \"{2}\", tipo di visualizzazione dell'oggetto \"{3}\"")
   @RBComment("WTRuntimeException: An object is DomainAdministered but its domain reference is null")
   @RBArgComment0("Object class")
   @RBArgComment1("Object Display Identity")
   @RBArgComment2("Object Identifier")
   @RBArgComment3("Object Display Type")
   public static final String NO_DOMAIN = "9";

   @RBEntry("Non è consentito eliminare il dominio predefinito \"{0}\".")
   @RBComment("AdministrativeDomainException: Deleting a predefined domain is not allowed")
   @RBArgComment0("Name of the predefined domain")
   public static final String CANT_DELETE_PREDEFINED_DOMAIN = "10";

   @RBEntry("Impossibile eliminare il dominio \"{0}\" perché è referenziato da altri oggetti.")
   @RBComment("AdministrativeDomainException: A specified domain cannot be deleted because other Windchill objects have references to it")
   @RBArgComment0("Name of the domain")
   public static final String CANT_DELETE_REFERENCED_DOMAIN = "11";

   @RBEntry("Non è consentito modificare il padre del dominio \"{0}\" al dominio discendente \"{1}\".")
   @RBComment("AdministrativeDomainException: Changing the parent of a domain to one of its descendent domains is not allowed.")
   @RBArgComment0("Name of the domain to reparent")
   @RBArgComment1("Name of new parent domain")
   public static final String CANT_REPARENT_DOMAIN_TO_DESCENDANT = "12";

   @RBEntry("Non è consentito modificare il padre del dominio predefinito \"{0}\".")
   @RBComment("AdministrativeDomainException: Changing the parent of a predefined domain is not allowed; the parent domain must be Root")
   @RBArgComment0("Name of the predefined domain")
   public static final String CANT_REPARENT_PREDEFINED_DOMAIN = "13";

   @RBEntry("Non è consentito aggiornare le proprietà del dominio predefinito \"{0}\".")
   @RBComment("AdministrativeDomainException: Updating properties of a predefined domain is not allowed")
   @RBArgComment0("Name of the predefined domain")
   public static final String CANT_UPDATE_PREDEFINED_DOMAIN = "14";

   @RBEntry("Impossibile trovare il dominio \"{0}\".")
   @RBComment("AdministrativeDomainException: A domain with the specified path name could not be found.")
   @RBArgComment0("Domain path name")
   public static final String DOMAIN_NOT_FOUND = "15";

   @RBEntry("Tutti")
   @RBComment("Used in place of a State EnumeratedType string to represent all lifecycle states (e.g., State: \"All\")")
   public static final String ALL_STATES = "16";

   @RBEntry("Il valore assegnato a \"{0}\" contiene il carattere riservato \"{1}\".")
   @RBComment("WTPropertyVetoException: An attribute value contains a reserved character")
   @RBArgComment0("name of attribute")
   @RBArgComment1("reserved character")
   public static final String RESERVED_CHARACTER = "17";

   @RBEntry("Il valore \"{1}\" del parametro \"{0}\" non è valido.")
   @RBComment("WTInvalidParameterException: An invalid parameter value is passed to a method.")
   @RBArgComment0("Name of parameter")
   @RBArgComment1("Value of parameter that is invalid")
   public static final String INVALID_PARAMETER = "18";

   @RBEntry("Il valore del parametro \"{0}\" non può essere nullo.")
   @RBComment("WTInvalidParameterException: A null value is passed to a method for a required parameter.")
   @RBArgComment0("Name of parameter")
   public static final String NULL_PARAMETER = "19";

   @RBEntry("La creazione del dominio predefinito \"{0}\" nel contenitore Windchill PDM non è consentita.")
   @RBComment("AdministrativeDomainException: Creating a predefined domain in the WIndchill PDM container is not allowed.")
   @RBArgComment0("Name of the predefined domain")
   public static final String CANT_CREATE_PREDEFINED_DOMAIN_IN_WINDCHILL_PDM_CONTAINER = "20";

   @RBEntry(" Nessun dominio ritrovato per l'identificatore oggetto \"{0}\"")
   @RBComment("AdministrativeDomainException: A domain was not found for the specified ObjectIdentifier.")
   @RBArgComment0("Domain path name")
   public static final String NO_DOMAIN_FOUND_FOR_OID = "21";

   @RBEntry("L'identificatore oggetto \"{0}\" non fa riferimento ad un dominio.")
   @RBComment("AdministrativeDomainException: The specified ObjectIdentifier does not reference an AdministrativeDomain object.")
   @RBArgComment0("Domain path name")
   public static final String OID_NOT_A_DOMAIN_OID = "22";

   @RBEntry(" Impossibile analizzare il percorso del dominio \"{0}\" e del contenitore \"{1}\".")
   @RBComment("AdministrativeDomainException: Failed to compute a domain path for the specified domain and container.")
   @RBArgComment0("Name of the domain")
   @RBArgComment1("Name of the container")
   public static final String FAILED_TO_GEN_DOMAIN_PATH = "23";

   @RBEntry("Il dominio \"{0}\" non è un dominio predefinito.")
   @RBComment("AdministrativeDomainException: The specified domain is not one of the special predefined domains")
   @RBArgComment0("Name of the domain")
   public static final String DOMAIN_NOT_A_PREDEFINED_DOMAIN = "24";

   @RBEntry("Non è consentito trasformare il padre in radice per un dominio con nome predefinito \"{0}\" nel contenitore Windchill PDM.")
   @RBComment("AdministrativeDomainException: Changing the parent domain to Root for a domain that has one of the predefined domain names is not allowed for domains in the Windchill PDM container because they would conflict with the system defined special domains that are assigned those predefined names.")
   @RBArgComment0("Name of the domain")
   public static final String CANT_REPARENT_DOMAIN_WITH_PREDEFINED_NAME = "25";

   @RBEntry("Non è consentito modificare il nome di dominio nel nome predefinito \"{0}\" per i domini del contenitore Windchill PDM con il dominio radice come padre.")
   @RBComment("AdministrativeDomainException: Changing the domain name to on of the predefined names is not allowed for domains in the Windchill PDM container with parent Root because they would conflict with the system defined special domains that are assigned those predefined names")
   @RBArgComment0("The new name that we are trying to assign to the domain")
   public static final String CANT_RENAME_DOMAIN_TO_PREDEFINED_DOMAIN = "26";

   @RBEntry("La modifica del dominio non è consentita per più oggetti amministrati quando uno degli oggetti è un oggetto dominio.")
   @RBComment("AdministrativeDomainException: Multi-object change and replace domain operations are not allowed if one of the objects is an AdministrativeDomain object. If you want to change the domain for an AdministrativeDomain object, it must be done as a single object operation.")
   public static final String MULTI_OBJECT_DOMAIN_CHANGE_NOT_ALLOWED_FOR_ADMIN_DOMAINS = "27";
}
