/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.admin;

import wt.util.resource.*;

@RBUUID("wt.admin.adminEventResource")
public final class adminEventResource_it extends WTListResourceBundle {
   @RBEntry("TUTTI")
   public static final String PRIVATE_CONSTANT_0 = "ALL";

   @RBEntry("CHECK-IN")
   public static final String PRIVATE_CONSTANT_1 = "*/wt.vc.wip.WorkInProgressServiceEvent/POST_CHECKIN";

   @RBEntry("CHECK-OUT")
   public static final String PRIVATE_CONSTANT_2 = "*/wt.vc.wip.WorkInProgressServiceEvent/POST_CHECKOUT";

   @RBEntry("CREA")
   public static final String PRIVATE_CONSTANT_3 = "*/wt.fc.PersistenceManagerEvent/POST_STORE";

   @RBEntry("ELIMINA")
   public static final String PRIVATE_CONSTANT_4 = "*/wt.fc.PersistenceManagerEvent/POST_DELETE";

   @RBEntry("MODIFICA")
   public static final String PRIVATE_CONSTANT_5 = "*/wt.fc.PersistenceManagerEvent/POST_MODIFY";

   @RBEntry("BLOCCA")
   public static final String PRIVATE_CONSTANT_6 = "*/wt.locks.LockServiceEvent/POST_LOCK";

   @RBEntry("SBLOCCA")
   public static final String PRIVATE_CONSTANT_7 = "*/wt.locks.LockServiceEvent/POST_UNLOCK";

   @RBEntry("MODIFICA DI STATO DEL CICLO DI VITA")
   public static final String PRIVATE_CONSTANT_8 = "*/wt.lifecycle.LifeCycleServiceEvent/STATE_CHANGE";

   @RBEntry("INVIA AL PDM")
   public static final String PRIVATE_CONSTANT_9 = "*/wt.sandbox.SandboxServiceCheckinEvent/POST_SB_CHECKIN_EVENT";

   @RBEntry("ANNULLA CHECK-OUT")
   public static final String PRIVATE_CONSTANT_10 = "*/wt.vc.wip.WorkInProgressServiceEvent/POST_UNDO_CHECKOUT";

   @RBEntry("NUOVA VERSIONE")
   public static final String PRIVATE_CONSTANT_11 = "*/wt.vc.VersionControlServiceEvent/NEW_VERSION";

   @RBEntry("UNISCI")
   public static final String PRIVATE_CONSTANT_12 = "*/wt.vc.VersionControlServiceEvent/POST_MERGE";

   @RBEntry("MODIFICA DOMINIO")
   public static final String PRIVATE_CONSTANT_13 = "*/wt.admin.AdministrativeDomainManagerEvent/POST_CHANGE_DOMAIN";

   @RBEntry("MODIFICA POSIZIONE")
   public static final String PRIVATE_CONSTANT_14 = "*/wt.folder.FolderServiceEvent/POST_CHANGE_FOLDER";

   @RBEntry("OVERFLOW")
   public static final String PRIVATE_CONSTANT_15 = "*/wt.fv.FvServiceEvent/OVERFLOW";

   @RBEntry("REPORT DI PROBLEMA FORMALIZZATO")
   public static final String PRIVATE_CONSTANT_16 = "*/wt.change2.ChangeService2Event/ISSUE_FORMALIZED";

   @RBEntry("REPORT DI PROBLEMA NON FORMALIZZATO")
   public static final String PRIVATE_CONSTANT_17 = "*/wt.change2.ChangeService2Event/ISSUE_UNFORMALIZED";

   @RBEntry("MODIFICA STATO OPERAZIONE DI MODIFICA")
   public static final String PRIVATE_CONSTANT_18 = "*/wt.change2.ChangeService2Event/CA_STATE_CHANGED";

   @RBEntry("MODIFICA STATO NOTIFICA DI MODIFICA")
   public static final String PRIVATE_CONSTANT_19 = "*/wt.change2.ChangeService2Event/CN_STATE_CHANGED";

   @RBEntry("SALVA NUOVO SELETTORE")
   public static final String PRIVATE_CONSTANT_20 = "*/com.ptc.gateway.ilpdm.mapping.I2WServiceEvent/SAVE_NEW_SELECTOR";

   @RBEntry("PUBBLICAZIONE COMPLETATA")
   public static final String PRIVATE_CONSTANT_21 = "*/com.ptc.gateway.ilpdm.mapping.I2WServiceEvent/PUBLISH_SUCCESS";

   @RBEntry("PUBBLICAZIONE COMPLETATA CON ERRORI")
   public static final String PRIVATE_CONSTANT_22 = "*/com.ptc.gateway.ilpdm.mapping.I2WServiceEvent/PUBLISH_NOT_SUCCESS";

   @RBEntry("ESEGUI AZIONE")
   public static final String PRIVATE_CONSTANT_23 = "*/wt.meeting.actionitem.ActionItemEvent/RESOLVE";

   @RBEntry("AGGIORNA AZIONE")
   public static final String PRIVATE_CONSTANT_24 = "*/wt.meeting.actionitem.ActionItemEvent/UPDATE";

   @RBEntry("ELIMINA AZIONE")
   public static final String PRIVATE_CONSTANT_25 = "*/wt.meeting.actionitem.ActionItemEvent/DELETE";

   @RBEntry("RAPPRESENTAZIONE DI VISUALIZATION SALVATA")
   public static final String PRIVATE_CONSTANT_26 = "*/com.ptc.wvs.server.loader.GraphicsServerLoaderServiceEvent/REPRESENTATION_SAVED";

   @RBEntry("MARKUP DI VISUALIZATION SALVATO")
   public static final String PRIVATE_CONSTANT_27 = "*/com.ptc.wvs.server.loader.GraphicsServerLoaderServiceEvent/MARKUP_SAVED";

   @RBEntry("PUBBLICAZIONE VISUALIZATION COMPLETATA")
   public static final String PRIVATE_CONSTANT_28 = "*/com.ptc.wvs.server.publish.PublishServiceEvent/PUBLISH_SUCCESSFUL";

   @RBEntry("PUBBLICAZIONE VISUALIZATION FALLITA")
   public static final String PRIVATE_CONSTANT_29 = "*/com.ptc.wvs.server.publish.PublishServiceEvent/PUBLISH_NOT_SUCCESSFUL";

   @RBEntry("AGGIUNGI ELENCO PARTI PRODUTTORE APPROVATE")
   public static final String PRIVATE_CONSTANT_30 = "*/com.ptc.windchill.suma.axl.AXLServiceEvent/ADD_AML";

   @RBEntry("AGGIUNGI ELENCO PARTI FORNITORE APPROVATE")
   public static final String PRIVATE_CONSTANT_31 = "*/com.ptc.windchill.suma.axl.AXLServiceEvent/ADD_AVL";

   @RBEntry("RIMUOVI ELENCO PARTI PRODUTTORE APPROVATE")
   public static final String PRIVATE_CONSTANT_32 = "*/com.ptc.windchill.suma.axl.AXLServiceEvent/REMOVE_AML";

   @RBEntry("RIMUOVI ELENCO PARTI FORNITORE APPROVATE")
   public static final String PRIVATE_CONSTANT_33 = "*/com.ptc.windchill.suma.axl.AXLServiceEvent/REMOVE_AVL";

   @RBEntry("MODIFICA PREFERENZA ELENCO PARTI PRODUTTORE APPROVATE")
   public static final String PRIVATE_CONSTANT_34 = "*/com.ptc.windchill.suma.axl.AXLServiceEvent/MODIFY_AML";

   @RBEntry("MODIFICA PREFERENZA ELENCO PARTI FORNITORE APPROVATE")
   public static final String PRIVATE_CONSTANT_35 = "*/com.ptc.windchill.suma.axl.AXLServiceEvent/MODIFY_AVL";

   @RBEntry("CREATO")
   public static final String PRIVATE_CONSTANT_36 = "*/com.ptc.windchill.ixb.importer.ImportJobEvent/CREATED";

   @RBEntry("INVIATO")
   public static final String PRIVATE_CONSTANT_37 = "*/com.ptc.windchill.ixb.importer.ImportJobEvent/SUBMITTED";

   @RBEntry("AVVIATO")
   public static final String PRIVATE_CONSTANT_38 = "*/com.ptc.windchill.ixb.importer.ImportJobEvent/STARTED";

   @RBEntry("COMPLETATO")
   public static final String PRIVATE_CONSTANT_39 = "*/com.ptc.windchill.ixb.importer.ImportJobEvent/COMPLETED";

   @RBEntry("ANNULLATO")
   public static final String PRIVATE_CONSTANT_40 = "*/com.ptc.windchill.ixb.importer.ImportJobEvent/CANCELED";

   @RBEntry("NON RIUSCITO")
   public static final String PRIVATE_CONSTANT_41 = "*/com.ptc.windchill.ixb.importer.ImportJobEvent/FAILED";
}
