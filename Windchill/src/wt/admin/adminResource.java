/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.admin;

import wt.util.resource.*;

@RBUUID("wt.admin.adminResource")
public final class adminResource extends WTListResourceBundle {
   @RBEntry("The operation:   \"{0}\" failed.")
   @RBComment("WTException: A specified operation failed during installation")
   @RBArgComment0("Name of operation that failed")
   public static final String OPERATION_FAILED = "0";

   @RBEntry("Invalid operation: cannot delete: \"{0}\".")
   @RBComment("Message stating that specified user (intended for admin) cannot be deleted")
   @RBArgComment0("Name of object that cannot be deleted")
   public static final String ADMIN_DELETE = "1";

   @RBEntry("Cannot change name of predefined domain: \"{0}\".")
   @RBComment("Message stating that a specified predefined domain's name cannot be changed")
   @RBArgComment0("Name of domain whose name cannot be changed")
   public static final String CANT_CHANGE_NAME = "2";

   @RBEntry("Failed to initialize administrative domain.")
   @RBComment("Message stating that there was a failure to initialize administrative domain")
   public static final String MISSING_DOMAIN = "3";

   @RBEntry("The event \"{0}\" does not exist.")
   @RBComment("AdministrativeDomainException: An event with the specified name does not exist")
   @RBArgComment0("Name of event that does not exist")
   public static final String INVALID_EVENT = "4";

   @RBEntry("Domains can't be deleted.")
   @RBComment("Message stating that domains cannot be deleted")
   public static final String CANT_DELETE_DOMAIN = "5";

   @RBEntry("The domain of persistent objects cannot be changed using \"{0}\".")
   @RBComment("WTRuntimeException: Persistent objects cannot be changed using a specified means")
   @RBArgComment0("Method name: DomainAdministered.setAdminDomain")
   public static final String CANT_CHANGE_DOMAIN = "6";

   @RBEntry("Wrong number of attributes.")
   @RBComment("WTException: The number of arguments provided to the doInstall API is incorrect")
   public static final String WRONG_NUMBER_ATTRS = "7";

   @RBEntry("Domain of the object is null: can't initialize.")
   @RBComment("WTRuntimeException: The domain specified for a new AdminDomainRef is null so the domain reference cannot be initialized")
   public static final String NULL_DOMAIN = "8";

   @RBEntry("The domain administered object has no assigned domain.  Cannot check policy access.  Object class:  \"{0}\", Object display identity: \"{1}\", Object identifier: \"{2}\", Object display type: \"{3}\"")
   @RBComment("WTRuntimeException: An object is DomainAdministered but its domain reference is null")
   @RBArgComment0("Object class")
   @RBArgComment1("Object Display Identity")
   @RBArgComment2("Object Identifier")
   @RBArgComment3("Object Display Type")
   public static final String NO_DOMAIN = "9";

   @RBEntry("A delete of the predefined domain \"{0}\" is not allowed.")
   @RBComment("AdministrativeDomainException: Deleting a predefined domain is not allowed")
   @RBArgComment0("Name of the predefined domain")
   public static final String CANT_DELETE_PREDEFINED_DOMAIN = "10";

   @RBEntry("The domain \"{0}\" is referenced by other objects and cannot be deleted.")
   @RBComment("AdministrativeDomainException: A specified domain cannot be deleted because other Windchill objects have references to it")
   @RBArgComment0("Name of the domain")
   public static final String CANT_DELETE_REFERENCED_DOMAIN = "11";

   @RBEntry(" Changing the parent of domain \"{0}\" to descendent domain \"{1}\" is not allowed.")
   @RBComment("AdministrativeDomainException: Changing the parent of a domain to one of its descendent domains is not allowed.")
   @RBArgComment0("Name of the domain to reparent")
   @RBArgComment1("Name of new parent domain")
   public static final String CANT_REPARENT_DOMAIN_TO_DESCENDANT = "12";

   @RBEntry("Changing the parent of the predefined domain \"{0}\" is not allowed.")
   @RBComment("AdministrativeDomainException: Changing the parent of a predefined domain is not allowed; the parent domain must be Root")
   @RBArgComment0("Name of the predefined domain")
   public static final String CANT_REPARENT_PREDEFINED_DOMAIN = "13";

   @RBEntry("An update of properties for the predefined domain \"{0}\" is not allowed.")
   @RBComment("AdministrativeDomainException: Updating properties of a predefined domain is not allowed")
   @RBArgComment0("Name of the predefined domain")
   public static final String CANT_UPDATE_PREDEFINED_DOMAIN = "14";

   @RBEntry(" Domain \"{0}\" not found.")
   @RBComment("AdministrativeDomainException: A domain with the specified path name could not be found.")
   @RBArgComment0("Domain path name")
   public static final String DOMAIN_NOT_FOUND = "15";

   @RBEntry("All")
   @RBComment("Used in place of a State EnumeratedType string to represent all lifecycle states (e.g., State: \"All\")")
   public static final String ALL_STATES = "16";

   @RBEntry("The value assigned to \"{0}\" contains the reserved character \"{1}\".")
   @RBComment("WTPropertyVetoException: An attribute value contains a reserved character")
   @RBArgComment0("name of attribute")
   @RBArgComment1("reserved character")
   public static final String RESERVED_CHARACTER = "17";

   @RBEntry("The \"{0}\" parameter value \"{1}\" is invalid. ")
   @RBComment("WTInvalidParameterException: An invalid parameter value is passed to a method.")
   @RBArgComment0("Name of parameter")
   @RBArgComment1("Value of parameter that is invalid")
   public static final String INVALID_PARAMETER = "18";

   @RBEntry("The \"{0}\" parameter value cannot be null.")
   @RBComment("WTInvalidParameterException: A null value is passed to a method for a required parameter.")
   @RBArgComment0("Name of parameter")
   public static final String NULL_PARAMETER = "19";

   @RBEntry("Creation of the predefined domain \"{0}\" in the Windchill PDM container is not allowed.")
   @RBComment("AdministrativeDomainException: Creating a predefined domain in the WIndchill PDM container is not allowed.")
   @RBArgComment0("Name of the predefined domain")
   public static final String CANT_CREATE_PREDEFINED_DOMAIN_IN_WINDCHILL_PDM_CONTAINER = "20";

   @RBEntry(" No domain was found for the ObjectIdentifier \"{0}\"")
   @RBComment("AdministrativeDomainException: A domain was not found for the specified ObjectIdentifier.")
   @RBArgComment0("Domain path name")
   public static final String NO_DOMAIN_FOUND_FOR_OID = "21";

   @RBEntry(" The ObjectIdentifier \"{0}\" does not reference a domain.")
   @RBComment("AdministrativeDomainException: The specified ObjectIdentifier does not reference an AdministrativeDomain object.")
   @RBArgComment0("Domain path name")
   public static final String OID_NOT_A_DOMAIN_OID = "22";

   @RBEntry(" Could not compute the domain path for domain \"{0}\" and container \"{1}\".")
   @RBComment("AdministrativeDomainException: Failed to compute a domain path for the specified domain and container.")
   @RBArgComment0("Name of the domain")
   @RBArgComment1("Name of the container")
   public static final String FAILED_TO_GEN_DOMAIN_PATH = "23";

   @RBEntry("The domain \"{0}\" is not one of the predefined domains.")
   @RBComment("AdministrativeDomainException: The specified domain is not one of the special predefined domains")
   @RBArgComment0("Name of the domain")
   public static final String DOMAIN_NOT_A_PREDEFINED_DOMAIN = "24";

   @RBEntry("Changing the parent to Root for a domain with the predefined name \"{0}\" is not allowed in the Windchill PDM container.")
   @RBComment("AdministrativeDomainException: Changing the parent domain to Root for a domain that has one of the predefined domain names is not allowed for domains in the Windchill PDM container because they would conflict with the system defined special domains that are assigned those predefined names.")
   @RBArgComment0("Name of the domain")
   public static final String CANT_REPARENT_DOMAIN_WITH_PREDEFINED_NAME = "25";

   @RBEntry("Changing the domain name to the predefined name \"{0}\" is not allowed for domains in the Windchill PDM container with parent Root.")
   @RBComment("AdministrativeDomainException: Changing the domain name to on of the predefined names is not allowed for domains in the Windchill PDM container with parent Root because they would conflict with the system defined special domains that are assigned those predefined names")
   @RBArgComment0("The new name that we are trying to assign to the domain")
   public static final String CANT_RENAME_DOMAIN_TO_PREDEFINED_DOMAIN = "26";

   @RBEntry("Changing the domain for multiple domain administered objects is not allowed when one of the objects is a domain object.")
   @RBComment("AdministrativeDomainException: Multi-object change and replace domain operations are not allowed if one of the objects is an AdministrativeDomain object. If you want to change the domain for an AdministrativeDomain object, it must be done as a single object operation.")
   public static final String MULTI_OBJECT_DOMAIN_CHANGE_NOT_ALLOWED_FOR_ADMIN_DOMAINS = "27";
}
