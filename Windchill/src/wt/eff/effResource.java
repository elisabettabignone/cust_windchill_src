/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.eff;

import wt.util.resource.*;

@RBUUID("wt.eff.effResource")
public final class effResource extends WTListResourceBundle {
   @RBEntry("-")
   @RBPseudo(false)
   public static final String DASH = "0";

   @RBEntry(",")
   @RBPseudo(false)
   public static final String SEPARATOR = "1";

   @RBEntry("Cannot delete \"{0}\" because there are effectivities associated to it")
   public static final String CAN_NOT_DELETE_EFF_CONTEXT = "10";

   @RBEntry("Cannot delete \"{0}\" because the product instance \"{1}\" associated with it could not be deleted")
   public static final String CAN_NOT_DELETE_EFF_CONFIGURATION_ITEM = "11";

   @RBEntry("Cannot delete \"{0}\" because it is a solution for \"{1}\", and an error occured when trying to remove the association")
   public static final String CAN_NOT_DELETE_PRODUCT_SOLUTION = "12";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String OK_LABEL = "13";

   @RBEntry("Effectivity cannot be added unless an object to add the effectivity to has been supplied.")
   public static final String EFF_MANAGED_VERSION_NULL = "14";

   @RBEntry("Effectivity {0} was successfully added to {1}")
   public static final String EFF_ADDED_SUCCESSFULLY = "15";

   @RBEntry("Effectivity")
   public static final String EFF_LABEL = "16";

   @RBEntry("New Value")
   public static final String EFF_NEW_RANGE = "17";

   @RBEntry("Delete")
   public static final String DELETE_EFF_LABEL = "18";

   @RBEntry("Effectivity Context")
   public static final String EFF_CONTEXT_LABEL = "19";

   @RBEntry("Unable to persist the effectivity changes:  some of versions were stale")
   public static final String STALE_VERSIONS_FOUND = "2";

   @RBEntry("Type")
   public static final String EFF_TYPE_LABEL = "20";

   @RBEntry("Value")
   public static final String EFF_VALUE_LABEL = "21";

   @RBEntry("The effectivity \"{0}\" requires a non-null context")
   public static final String INVALID_TYPE_NULL_CONTEXT = "22";

   @RBEntry("The effectivity \"{0}\" is not valid for the context \"{1}\"")
   public static final String INVALID_TYPE_NON_NULL_CONTEXT = "23";

   @RBEntry("The effectivity cannot be added because no type has been given. Please specify a type for the effectivity.")
   public static final String NO_TYPE_GIVEN = "24";

   @RBEntry("The effectivity cannot be added because no value has been given. Please give a value for the effectivity.")
   public static final String NO_VALUE_GIVEN = "25";

   @RBEntry("Add Effectivity")
   @RBComment("Text that appears as the label in Set Effectivity (SetEff.html) Page")
   public static final String ADD_EFFECTIVITY_LABEL = "26";

   @RBEntry("Appended effectivities to \"{0}\"")
   public static final String VISIT_FEEDBACK = "27";

   @RBEntry("No Configuration Item with name \"{0}\" was found.")
   public static final String NAMED_CONFIG_ITEM_NOT_FOUND = "28";

   @RBEntry("View Effectivity")
   public static final String VIEW_EFFECTIVITY_LABEL = "29";

   @RBEntry("The range is invalid:  the start value \"{0}\" is greater than the end value \"{1}\"")
   public static final String START_AFTER_END = "3";

   @RBEntry("{0} was successfully added.")
   public static final String ADD_EFFECTIVITY_CHANGE_ORDER_SUCCESSFUL = "30";

   @RBEntry("Can not modify the effectivities of \"{0}\" because it is checked out")
   public static final String CAN_NOT_MODIFY_CHECKED_OUT = "31";

   @RBEntry("Can not set an incorporation date for \"{0}\" because it has not been built.")
   public static final String CAN_NOT_SET_INCORPORATION_DATE_TO_UNBUILT_PI = "32";

   @RBEntry("Can not set an incorporation date for \"{0}\" prior to its build date of \"{1}\".")
   public static final String CAN_NOT_SET_INCORPORATION_DATE_PRIOR_TO_BUILD = "33";

   @RBEntry("Serial Number")
   public static final String SERIAL_NUMBER = "34";

   @RBEntry("Build Date")
   public static final String BUILD_DATE = "35";

   @RBEntry("{0} ({1})")
   public static final String PRODUCT_INSTANCE_IDENTIFIER = "4";

   @RBEntry("{0} is valid in the context of {1} and the range {2}")
   public static final String EFF_GROUP_IDENTIFIER = "5";

   @RBEntry("{0} is valid in the range {1}")
   public static final String EFF_GROUP_NULL_CONTEXT_IDENTIFIER = "6";

   @RBEntry("Effectivity for context {0} and range {1}")
   public static final String EFF_GROUP_NULL_TARGET_IDENTIFIER = "7";

   @RBEntry("Effectivity for range {0}")
   public static final String EFF_GROUP_NULL_TARGET_NULL_CONTEXT_IDENTIFIER = "8";

   @RBEntry("Unable to delete \"{0}\" because an error occured when attempting to delete one of its effectivities.  Please try again; if the problem persists, contact your system administrator")
   public static final String CAN_NOT_DELETE_EFF_MANAGED_VERSION = "9";

   @RBEntry("Effectivity History for {0}")
   @RBComment("Parameterized effectivity history label to introduce the wt.eff.EffManagedVersion whose effectivity history is being presented, for example, 'Effectivity History for Part 123 (ABC) A.'")
   public static final String EFF_HISTORY_FOR = "36";

   @RBEntry("GLOBAL CONTEXT")
   @RBComment("Label for HTML choice denoting global effectivity context.  This label is in all caps to distinguish it from contexts that are specific business objects.")
   public static final String GLOBAL_CONTEXT_CHOICE = "37";

   @RBEntry("Modified By")
   @RBComment("Override label for the heading of the table column containing the identities of the principals who changed effectivities.")
   public static final String MODIFIER_LABEL = "38";

   @RBEntry("Updated On")
   @RBComment("Override label for the heading of the table column containing the timestamps of effectivity changes.")
   public static final String MODIFY_STAMP_LABEL = "39";

   @RBEntry("Authorized By")
   @RBComment("Override label for the heading of the table column containing the references to the change activities 'authorizing' effectivity changes.")
   public static final String AUTHORIZED_BY_LABEL = "40";

   @RBEntry("Effectivity History")
   @RBComment("Label for effectivity history page URL.")
   public static final String EFF_HISTORY_LABEL = "41";

   @RBEntry("The recording of effectivity history may not be disabled once established.  Either delete all existing effectivity histories, or re-enable effectivity history recording.  Refer to the Windchill Administrator's Guide for details.")
   @RBComment("Error message advising that once any effectivity histories have been recorded, the recording of effectivity history may not be disabled.")
   public static final String HISTORY_REQUIRED_ERROR = "42";

   @RBEntry("\"{0}\" is not a valid target effectivity class.  The Class must be a concrete implementation of the wt.eff.Eff interface.")
   @RBComment("Error message advising that the java.lang.Class parameter passed to an implementation of the wt.eff.EffService.getEffectivities() method does not conform to the method's pre-condition:  it may not be null, and must represent a concrete implementation of the wt.eff.Eff interface.")
   public static final String BAD_EFF_TYPE_ERROR = "43";

   @RBEntry("\"{0}\" has never been assigned an effectivity value.")
   @RBComment("Parameterized message displayed on the Effectivity History page indicating that the version in question has no effectivity history.")
   public static final String NO_HISTORY_MSG = "44";

   @RBEntry("Effectivity Context:")
   @RBComment("Text that appears as a table row header in AddEffNoForm.html X-05.0.0.00.50")
   public static final String EFFECTIVITY_CONTEXT = "46";

   @RBEntry("Range start must be specified for this effectivity type.")
   @RBComment("Text that appears when user enters a serial number or lot effectivity which does not have a range start value (i.e. \" - 20000000\").")
   public static final String NO_RANGE_START = "56";

   @RBEntry("Invalid date specified: \"{0}\".")
   @RBComment("Text that appears when user enters an invalid date in a date effectivity statement.")
   public static final String INVALID_DATE = "57";

   @RBEntry("Effectivities for")
   @RBComment("(Used Windchill PDM/Classic only) Used on a page accessed when clicking \"View Effectivity\" from a Change Order. The label is used in contexts such as \"Effectivities for Change Activity 00001 - ssca\" and \"Effectivities for Change Order 00001 - ssco\", where \"Effectivities For\" is the localizable text")
   public static final String EFFECTIVITIES_FOR = "58";

   /**
    * --------------------------------------------------------------------
    * General exceptions
    * --------------------------------------------------------------------
    **/
   @RBEntry("Parameter passed in is null.")
   @RBComment("Message for exception thrown when null is passed as an argument where a value is required.")
   public static final String NULL_PARAMETER = "60";

   @RBEntry("Invalid reference.  Expected reference to instance of type: \"{0}\".")
   @RBComment("Message for exception thrown when a reference is not to the expected type of object.")
   public static final String INVALID_REFERENCE = "61";

   @RBEntry("Required data not specified: \"{0}\".")
   @RBComment("Message for exception thrown information in a group object is missing.")
   public static final String GROUP_DATA_MISSING = "62";

   @RBEntry("Changeable reference must be to instance of type: {0}.")
   @RBComment("Message for exception thrown when a changeable reference in a group object is not to the expected type of object.")
   public static final String GROUP_DATA_CHANGEABLE_REFERENCE_INVALID = "63";

   @RBEntry("Link reference must be to instance of type: {0}.")
   @RBComment("Message for exception thrown when a link reference in a group object is not to the expected type of object.")
   public static final String GROUP_DATA_LINK_REFERENCE_INVALID = "64";

   /**
    * --------------------------------------------------------------------
    * Pending and propagtion-related exceptions
    * --------------------------------------------------------------------
    **/
   @RBEntry("A pending effectivity statement is assigned to a change record which associates a changeable not implementing EffManagedVersion.")
   @RBComment("Error message when a pending effectivity statement is assigned to a change record which is associating a non-EffManagedVersion changeable.")
   public static final String PENDING_TARGET_NOT_EFF_MANAGED_VERSION = "70";

   @RBEntry("Unrecognized pending effectivity type: \"{0}\".")
   @RBComment("Error message when attempting to instantiate an actual effectivity from an unknown pending effectivity statement.")
   public static final String UNKNOWN_PENDING_EFF_TYPE = "71";

   @RBEntry("Target must implement WTContained interface.")
   @RBComment("Error message in the event the target is not WTContained.")
   public static final String TARGET_NOT_WTCONTAINED = "72";

   @RBEntry("Target is not assigned to a container.")
   @RBComment("Error message in the event the target is not assigned a container.")
   public static final String TARGET_HAS_NO_CONTAINER = "73";

   /**
    * --------------------------------------------------------------------
    * ValidEffsDelegate exceptions
    * --------------------------------------------------------------------
    **/
   @RBEntry("Unrecognized trace code: {0}")
   @RBComment("Error message when a part master contains a trace code not recognized by the delegate.")
   public static final String UNKNOWN_TRACE_CODE = "80";

   @RBEntry("Trace code is null.")
   @RBComment("Error message when a part master's trace code is null.")
   public static final String NULL_TRACE_CODE = "81";

   /**
    * --------------------------------------------------------------------
    * Preference exceptions
    * --------------------------------------------------------------------
    **/
   @RBEntry("Unexpected preference value type.")
   @RBComment("Message of exception thrown when type of preference value returned is not what is expected.")
   public static final String UNEXPECTED_PREF_TYPE = "90";

   /**
    * --------------------------------------------------------------------
    * Validation exceptions
    * --------------------------------------------------------------------
    **/
   @RBEntry("Target not specified.")
   @RBComment("Message for exception thrown when client eff group is validated and found to not have its changeable target specified.")
   public static final String VALIDATE_TARGET_NOT_SPECIFIED = "100";

   @RBEntry("Target must be an instance of type EffManagedVersion.")
   @RBComment("Message for exception thrown when client eff group is validated and found to be set to a changeable target of an invalid type.")
   public static final String VALIDATE_TARGET_INVALID = "101";

   @RBEntry("Effectivity type not specified.")
   @RBComment("Message for exception thrown when client eff group is validated and found to not have its effectivity type specified.")
   public static final String VALIDATE_FORM_NOT_SPECIFIED = "102";

   @RBEntry("Effectivity type must be a subinterface of EffForm.")
   @RBComment("Message for exception thrown when client eff group is validated and found to be set to an invalid effectivity type.")
   public static final String VALIDATE_FORM_INVALID = "103";

   @RBEntry("Effectivity context is required for this effectivity type.")
   @RBComment("Message for exception thrown when client eff group is validated and found to not have its effectivity context specified for an effectivity type requiring one.")
   public static final String VALIDATE_EFF_CONTEXT_NOT_SPECIFIED = "104";

   @RBEntry("Effectivity context must be an instance of type EffContext.")
   @RBComment("Message for exception thrown when client eff group is validated and found to be set to an eff context of an invalid type.")
   public static final String VALIDATE_EFF_CONTEXT_INVALID = "105";

   @RBEntry("Range not specified.")
   @RBComment("Message for exception thrown when client eff group is validated and found to not have its range specified.")
   public static final String VALIDATE_RANGE_NOT_SPECIFIED = "106";
  
   @RBEntry("Effectivity ranges for versions {0} and {1} of target '{2}' overlap and cannot be applied simultaneously.")
   @RBComment("Message for exception thrown when client eff groups are validated and two found to have overlapping ranges for different versions for same context and effectivity type.")
   public static final String VALIDATE_RANGES_OVERLAP = "107";

   @RBEntry("A pending effectivity statement already exists for this target for same effectivity type and context in {0}.")
   @RBComment("Message for exception thrown when client eff groups are validated and a pending effectivity statement already exists for the target for same effectivity type and context.")
   public static final String VALIDATE_PENDING_EFF_ALREADY_EXISTS = "108";

   @RBEntry("Invalid range format--see method server log for details")
   @RBComment("Message for exception thrown when client eff group is validated and found that the format of range specified is invalid.")
   public static final String VALIDATE_RANGE_INVALID = "109";
   
   @RBEntry("The selected objects cannot be removed from the Effectivity table because an administrative lock is applied.")
   @RBComment("Message for when administratively locked effectivities are trying to be removed.")
   public static final String VALIDATE_ADMIN_LOCKED_EFFS_ALL = "VALIDATE_ADMIN_LOCKED_EFFS_ALL";

   @RBEntry("One or more of the selected objects cannot be removed from the Effectivity table because an administrative lock is applied.")
   @RBComment("Message for when administratively locked effectivities are trying to be removed.")
   public static final String VALIDATE_ADMIN_LOCKED_EFFS_SOME = "VALIDATE_ADMIN_LOCKED_EFFS_SOME";
   
   @RBEntry("This object is administratively locked. To enable the input fields, set the Effectivity Context.")
   @RBComment("Message for when effectivities on administratively locked objects are trying to be created.")
   public static final String VALIDATE_ADMIN_LOCKED_EFFS_INPUTS = "VALIDATE_ADMIN_LOCKED_EFFS_INPUTS";
   
   @RBEntry("New effectivities for administratively locked objects require a non-locked Effectivity Context.")
   @RBComment("Message for when effectivities on administratively locked objects are trying to be created.")
   public static final String VALIDATE_ADMIN_LOCKED_EFFS_CONTEXT = "VALIDATE_ADMIN_LOCKED_EFFS_CONTEXT";
   
   /**
    * --------------------------------------------------------------------
    * Miscellaneous exceptions
    * --------------------------------------------------------------------
    **/
   @RBEntry("Master reference passed is not master to effectivity managed versions.")
   @RBComment("Message for exception thrown the master reference passed is not for EffManagedVersion versions.")
   public static final String MASTER_REFERENCE_NOT_EFF_MANAGED = "120";

   /**
    * --------------------------------------------------------------------
    * Loader exceptions
    * --------------------------------------------------------------------
    **/
   @RBEntry("Change directive not found: {0}")
   @RBComment("An error that is displayed when effectivity associated to a change directive is loaded and the change directive specified cannot be found.  The parameter will be the change directive number.")
   public static final String CHANGE_DIRECTIVE_NOT_FOUND_BY_NUMBER = "150";

   @RBEntry("Effectivity context not found: {0}")
   @RBComment("An error that is displayed when effectivity associated to a change directive is loaded and the effectivity context (part) specified by the number cannot be found.  The parameter will be the effectivity context (part) number.")
   public static final String EFF_CONTEXT_NOT_FOUND_BY_NUMBER = "151";

   @RBEntry("Structure Propagation failed because circular references is detected in the structure with {0}.")
   @RBComment("Error message in the event when circular references is detected in the structure.")
   public static final String STRUCTURE_RECURSION_DETECTED = "160";

}
