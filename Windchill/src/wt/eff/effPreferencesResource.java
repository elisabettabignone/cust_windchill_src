package wt.eff;

import wt.util.resource.*;

@RBUUID("wt.eff.effPreferencesResource")
public final class effPreferencesResource extends WTListResourceBundle {
   /**
/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
    **/
   @RBEntry("Enable Sibling Propagation")
   @RBComment("Display name for this preference.")
   public static final String PRIVATE_CONSTANT_0 = "SiblingPropagationEnabled.displayName";

   @RBEntry("Enables propagation of effectivity statements to siblings.")
   @RBComment("Short description for this preference.")
   public static final String PRIVATE_CONSTANT_1 = "SiblingPropagationEnabled.description";

   @RBEntry("This preference determines whether effectivity of siblings will be adjusted based on an effectivity statement being applied through an approved change process.")
   public static final String PRIVATE_CONSTANT_2 = "SiblingPropagationEnabled.longDescription";

   @RBEntry("Enable Structure Propagation")
   @RBComment("Display name for this preference.")
   public static final String PRIVATE_CONSTANT_3 = "StructurePropagationEnabled.displayName";

   @RBEntry("Enables propagation of effectivity statements down a product stucture.")
   @RBComment("Short description for this preference.")
   public static final String PRIVATE_CONSTANT_4 = "StructurePropagationEnabled.description";

   @RBEntry("This preference determines if effectivity statements will be propagated down the product structure when applied through an approved change process.  When enabled, a part configuration specification (based on the other effectivity preferences) will be used to identify the child parts considered for the effectivity statement.")
   @RBComment("Long description for this preference.")
   public static final String PRIVATE_CONSTANT_5 = "StructurePropagationEnabled.longDescription";

   @RBEntry("Enable Recursive Sibling Propagation")
   @RBComment("Display name for this preference.")
   public static final String PRIVATE_CONSTANT_6 = "RecursiveSiblingPropagationEnabled.displayName";

   @RBEntry("Enables propagation of effectivity statements down a product structure for statements which result from a sibling propagation operation. ")
   @RBComment("Short description for this preference.")
   public static final String PRIVATE_CONSTANT_7 = "RecursiveSiblingPropagationEnabled.description";

   @RBEntry("This preference determines whether an effectivity statement adjusted as a result of an effectivity operation will be propagated down the product structure.  If this preference is enabled, the closure of effectivity statements of siblings will be propagated down the structure, ensuring no child has greater effectivity than the sum effectivity of its parents.")
   public static final String PRIVATE_CONSTANT_8 = "RecursiveSiblingPropagationEnabled.longDescription";

   @RBEntry("Lifecycle State of Part Configuration Specification for Structure Propagation")
   @RBComment("Display name for this preference.")
   public static final String PRIVATE_CONSTANT_9 = "StructurePropagationPartConfigSpecLifecycleState.displayName";

   @RBEntry("Lifecycle state of the part configuration specification used in structure propagation of effectivity statements.")
   @RBComment("Short description for this preference.")
   public static final String PRIVATE_CONSTANT_10 = "StructurePropagationPartConfigSpecLifecycleState.description";

   @RBEntry("This preference defines the lifecycle state of the part configuration specification used to identify child parts when propagating effectivity down the product structure.")
   @RBComment("Long description for this preference.")
   public static final String PRIVATE_CONSTANT_11 = "StructurePropagationPartConfigSpecLifecycleState.longDescription";

   @RBEntry("View of Part Configuration Specification for Structure Propagation")
   @RBComment("Display name for this preference.")
   public static final String PRIVATE_CONSTANT_12 = "StructurePropagationPartConfigSpecView.displayName";

   @RBEntry("View of the part configuration specification used in structure propagation of effectivity statements.")
   @RBComment("Short description for this preference.")
   public static final String PRIVATE_CONSTANT_13 = "StructurePropagationPartConfigSpecView.description";

   @RBEntry("This preference defines the view of the part configuration specification used in structure propagation of effectivity statements.")
   @RBComment("Long description for this preference.")
   public static final String PRIVATE_CONSTANT_14 = "StructurePropagationPartConfigSpecView.longDescription";

   @RBEntry("Include Work in Process Parts for Part Configuration Specification for Structure Propagation")
   @RBComment("Display name for this preference.")
   public static final String PRIVATE_CONSTANT_15 = "StructurePropagationPartConfigSpecIncludeWorking.displayName";

   @RBEntry("Includes work in process parts for the part configuration specification used in structure propagation of effectivity statements.")
   @RBComment("Short description for this preference.")
   public static final String PRIVATE_CONSTANT_16 = "StructurePropagationPartConfigSpecIncludeWorking.description";

   @RBEntry("This preference specifies the option for including work in process parts for the part configuration specification used in structure propagation of effectivity statements.")
   @RBComment("Long description for this preference.")
   public static final String PRIVATE_CONSTANT_17 = "StructurePropagationPartConfigSpecIncludeWorking.longDescription";


   @RBEntry("Include Time in Date Effectivity")
   @RBComment("Display name for this preference.")
   public static final String INCLUDE_TIME_IN_DATE_EFFECTIVITY_NAME = "INCLUDE_TIME_IN_DATE_EFFECTIVITY_NAME";

   @RBEntry("Allows date effectivity to be defined using date and time.")
   @RBComment("Short description for this preference.")
   public static final String INCLUDE_TIME_IN_DATE_EFFECTIVITY_DESC = "INCLUDE_TIME_IN_DATE_EFFECTIVITY_DESC";

   @RBEntry("When set to No, date effectivity is defined by specifying a Date. Assumed time is 12:00 am.  When set to Yes, date effectivity is defined by specifying Date and Time.")
   @RBComment("Long description for this preference.")
   public static final String INCLUDE_TIME_IN_DATE_EFFECTIVITY_LONG_DESC = "INCLUDE_TIME_IN_DATE_EFFECTIVITY_LONG_DESC";
}
