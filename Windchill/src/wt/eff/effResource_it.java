/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.eff;

import wt.util.resource.*;

@RBUUID("wt.eff.effResource")
public final class effResource_it extends WTListResourceBundle {
   @RBEntry("-")
   @RBPseudo(false)
   public static final String DASH = "0";

   @RBEntry(",")
   @RBPseudo(false)
   public static final String SEPARATOR = "1";

   @RBEntry("Impossibile eliminare \"{0}\" perché esistono delle effettività associate ad esso.")
   public static final String CAN_NOT_DELETE_EFF_CONTEXT = "10";

   @RBEntry("Impossibile eliminare \"{0}\" perché l'istanza di prodotto \"{1}\" associata ad esso non è stata eliminata")
   public static final String CAN_NOT_DELETE_EFF_CONFIGURATION_ITEM = "11";

   @RBEntry("Impossibile eliminare \"{0}\" perché è una soluzione per \"{1}\", e si è verificato un errore durante la rimozione dell'associazione.")
   public static final String CAN_NOT_DELETE_PRODUCT_SOLUTION = "12";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String OK_LABEL = "13";

   @RBEntry("Non è possibile aggiungere l'effettività a meno che venga fornito un oggetto cui aggiungere l'effettività.")
   public static final String EFF_MANAGED_VERSION_NULL = "14";

   @RBEntry("L'effettività {0} è stata aggiunta a {1}")
   public static final String EFF_ADDED_SUCCESSFULLY = "15";

   @RBEntry("Effettività")
   public static final String EFF_LABEL = "16";

   @RBEntry("Nuovo valore")
   public static final String EFF_NEW_RANGE = "17";

   @RBEntry("Elimina")
   public static final String DELETE_EFF_LABEL = "18";

   @RBEntry("Contesto di effettività")
   public static final String EFF_CONTEXT_LABEL = "19";

   @RBEntry("Impossibile rendere persistenti le modifiche di effettività: alcune versioni sono obsolete.")
   public static final String STALE_VERSIONS_FOUND = "2";

   @RBEntry("Tipo")
   public static final String EFF_TYPE_LABEL = "20";

   @RBEntry("Valore")
   public static final String EFF_VALUE_LABEL = "21";

   @RBEntry("L'effettività \"{0}\" richiede un contesto non nullo")
   public static final String INVALID_TYPE_NULL_CONTEXT = "22";

   @RBEntry("L'effettività \"{0}\" non è valida per il contesto \"{1}\"")
   public static final String INVALID_TYPE_NON_NULL_CONTEXT = "23";

   @RBEntry("L'effettività non può essere aggiunta perché non è stato indicato il tipo. Specificare un tipo di effettività.")
   public static final String NO_TYPE_GIVEN = "24";

   @RBEntry("L'effettività non può essere aggiunta perché non è stato indicato un valore. Specificare un valore per l'effettività.")
   public static final String NO_VALUE_GIVEN = "25";

   @RBEntry("Aggiungi effettività")
   @RBComment("Text that appears as the label in Set Effectivity (SetEff.html) Page")
   public static final String ADD_EFFECTIVITY_LABEL = "26";

   @RBEntry("Effettività aggiunte a \"{0}\"")
   public static final String VISIT_FEEDBACK = "27";

   @RBEntry("Non è stato trovato alcun configuration item con nome \"{0}\".")
   public static final String NAMED_CONFIG_ITEM_NOT_FOUND = "28";

   @RBEntry("Visualizza effettività")
   public static final String VIEW_EFFECTIVITY_LABEL = "29";

   @RBEntry("Intervallo invalido: il valore iniziale \"{0}\" è maggiore del valore finale \"{1}\"")
   public static final String START_AFTER_END = "3";

   @RBEntry("{0} è stato aggiunto.")
   public static final String ADD_EFFECTIVITY_CHANGE_ORDER_SUCCESSFUL = "30";

   @RBEntry("Impossibile modificare le effettività di \"{0}\" perché è sottoposto a Check-Out")
   public static final String CAN_NOT_MODIFY_CHECKED_OUT = "31";

   @RBEntry("Impossibile impostare la data di incorporazione per \"{0}\" perché non è stato costruito.")
   public static final String CAN_NOT_SET_INCORPORATION_DATE_TO_UNBUILT_PI = "32";

   @RBEntry("Impossibile impostare la data di incorporazione per \"{0}\" prima della data di costruzione di \"{1}\".")
   public static final String CAN_NOT_SET_INCORPORATION_DATE_PRIOR_TO_BUILD = "33";

   @RBEntry("Numero di serie")
   public static final String SERIAL_NUMBER = "34";

   @RBEntry("Data build")
   public static final String BUILD_DATE = "35";

   @RBEntry("{0} ({1})")
   public static final String PRODUCT_INSTANCE_IDENTIFIER = "4";

   @RBEntry("{0} è valido nel contesto di {1} e nell'intervallo {2}")
   public static final String EFF_GROUP_IDENTIFIER = "5";

   @RBEntry("{0} è valido nell'intervallo {1}")
   public static final String EFF_GROUP_NULL_CONTEXT_IDENTIFIER = "6";

   @RBEntry("Effettività per il contesto {0} e l'intervallo {1}")
   public static final String EFF_GROUP_NULL_TARGET_IDENTIFIER = "7";

   @RBEntry("Effettività per l'intervallo {0}")
   public static final String EFF_GROUP_NULL_TARGET_NULL_CONTEXT_IDENTIFIER = "8";

   @RBEntry("Impossibile eliminare \"{0}\" perché si è verificato un errore durante l'eliminazione delle sue effettività. Riprovare e se il problema persiste, contattare l'amministratore del sistema")
   public static final String CAN_NOT_DELETE_EFF_MANAGED_VERSION = "9";

   @RBEntry("Cronologia effettività per {0}")
   @RBComment("Parameterized effectivity history label to introduce the wt.eff.EffManagedVersion whose effectivity history is being presented, for example, 'Effectivity History for Part 123 (ABC) A.'")
   public static final String EFF_HISTORY_FOR = "36";

   @RBEntry("CONTESTO GLOBALE")
   @RBComment("Label for HTML choice denoting global effectivity context.  This label is in all caps to distinguish it from contexts that are specific business objects.")
   public static final String GLOBAL_CONTEXT_CHOICE = "37";

   @RBEntry("Autore modifiche")
   @RBComment("Override label for the heading of the table column containing the identities of the principals who changed effectivities.")
   public static final String MODIFIER_LABEL = "38";

   @RBEntry("Data aggiornamento")
   @RBComment("Override label for the heading of the table column containing the timestamps of effectivity changes.")
   public static final String MODIFY_STAMP_LABEL = "39";

   @RBEntry("Autorizzazione di")
   @RBComment("Override label for the heading of the table column containing the references to the change activities 'authorizing' effectivity changes.")
   public static final String AUTHORIZED_BY_LABEL = "40";

   @RBEntry("Cronologia effettività")
   @RBComment("Label for effectivity history page URL.")
   public static final String EFF_HISTORY_LABEL = "41";

   @RBEntry("La registrazione della cronologia effettività non può essere disattivata una volta impostata. Eliminare tutte le cronologie effettività esistenti o riattivare la registrazione della cronologia effettività. Per ulteriori informazioni, fare riferimento alla Guida dell'amministratore Windchill.")
   @RBComment("Error message advising that once any effectivity histories have been recorded, the recording of effectivity history may not be disabled.")
   public static final String HISTORY_REQUIRED_ERROR = "42";

   @RBEntry("\"{0}\" non è una classe di effettività di destinazione valida. La classe deve essere un'implementazione concreta dell'interfaccia wt.eff.Eff.")
   @RBComment("Error message advising that the java.lang.Class parameter passed to an implementation of the wt.eff.EffService.getEffectivities() method does not conform to the method's pre-condition:  it may not be null, and must represent a concrete implementation of the wt.eff.Eff interface.")
   public static final String BAD_EFF_TYPE_ERROR = "43";

   @RBEntry("A \"{0}\" non è mai stato assegnato un valore di effettività.")
   @RBComment("Parameterized message displayed on the Effectivity History page indicating that the version in question has no effectivity history.")
   public static final String NO_HISTORY_MSG = "44";

   @RBEntry("Contesto di effettività:")
   @RBComment("Text that appears as a table row header in AddEffNoForm.html X-05.0.0.00.50")
   public static final String EFFECTIVITY_CONTEXT = "46";

   @RBEntry("È necessario specificare l'intervallo per questo tipo di effettività.")
   @RBComment("Text that appears when user enters a serial number or lot effectivity which does not have a range start value (i.e. \" - 20000000\").")
   public static final String NO_RANGE_START = "56";

   @RBEntry("Data specificata non valida: \"{0}\"")
   @RBComment("Text that appears when user enters an invalid date in a date effectivity statement.")
   public static final String INVALID_DATE = "57";

   @RBEntry("Effettività per")
   @RBComment("(Used Windchill PDM/Classic only) Used on a page accessed when clicking \"View Effectivity\" from a Change Order. The label is used in contexts such as \"Effectivities for Change Activity 00001 - ssca\" and \"Effectivities for Change Order 00001 - ssco\", where \"Effectivities For\" is the localizable text")
   public static final String EFFECTIVITIES_FOR = "58";

   /**
    * --------------------------------------------------------------------
    * General exceptions
    * --------------------------------------------------------------------
    **/
   @RBEntry("Il parametro trasmesso è nullo.")
   @RBComment("Message for exception thrown when null is passed as an argument where a value is required.")
   public static final String NULL_PARAMETER = "60";

   @RBEntry("Riferimento non valido. Atteso riferimento a un istanza di tipo: \"{0}\".")
   @RBComment("Message for exception thrown when a reference is not to the expected type of object.")
   public static final String INVALID_REFERENCE = "61";

   @RBEntry("Dato obbligatorio non specificato: \"{0}\".")
   @RBComment("Message for exception thrown information in a group object is missing.")
   public static final String GROUP_DATA_MISSING = "62";

   @RBEntry("Il riferimento modificabile deve essere a un istanza del tipo: {0}.")
   @RBComment("Message for exception thrown when a changeable reference in a group object is not to the expected type of object.")
   public static final String GROUP_DATA_CHANGEABLE_REFERENCE_INVALID = "63";

   @RBEntry("Il riferimento link deve essere a un istanza del tipo: {0}.")
   @RBComment("Message for exception thrown when a link reference in a group object is not to the expected type of object.")
   public static final String GROUP_DATA_LINK_REFERENCE_INVALID = "64";

   /**
    * --------------------------------------------------------------------
    * Pending and propagtion-related exceptions
    * --------------------------------------------------------------------
    **/
   @RBEntry("Una dichiarazione di effettività in sospeso è assegnata a un record di modifica associato a un elemento modificabile non di tipo EffManagedVersion.")
   @RBComment("Error message when a pending effectivity statement is assigned to a change record which is associating a non-EffManagedVersion changeable.")
   public static final String PENDING_TARGET_NOT_EFF_MANAGED_VERSION = "70";

   @RBEntry("Tipo di effettività in sospeso non riconosciuto: \"{0}\".")
   @RBComment("Error message when attempting to instantiate an actual effectivity from an unknown pending effectivity statement.")
   public static final String UNKNOWN_PENDING_EFF_TYPE = "71";

   @RBEntry("La destinazione deve implementare l'interfaccia WTContained.")
   @RBComment("Error message in the event the target is not WTContained.")
   public static final String TARGET_NOT_WTCONTAINED = "72";

   @RBEntry("La destinazione non è assegnata a un contenitore.")
   @RBComment("Error message in the event the target is not assigned a container.")
   public static final String TARGET_HAS_NO_CONTAINER = "73";

   /**
    * --------------------------------------------------------------------
    * ValidEffsDelegate exceptions
    * --------------------------------------------------------------------
    **/
   @RBEntry("Codice traccia non riconosciuto: {0}")
   @RBComment("Error message when a part master contains a trace code not recognized by the delegate.")
   public static final String UNKNOWN_TRACE_CODE = "80";

   @RBEntry("Il codice traccia è nullo.")
   @RBComment("Error message when a part master's trace code is null.")
   public static final String NULL_TRACE_CODE = "81";

   /**
    * --------------------------------------------------------------------
    * Preference exceptions
    * --------------------------------------------------------------------
    **/
   @RBEntry("Tipo di valore preferenza non previsto.")
   @RBComment("Message of exception thrown when type of preference value returned is not what is expected.")
   public static final String UNEXPECTED_PREF_TYPE = "90";

   /**
    * --------------------------------------------------------------------
    * Validation exceptions
    * --------------------------------------------------------------------
    **/
   @RBEntry("Destinazione non specificata.")
   @RBComment("Message for exception thrown when client eff group is validated and found to not have its changeable target specified.")
   public static final String VALIDATE_TARGET_NOT_SPECIFIED = "100";

   @RBEntry("La destinazione deve essere un'istanza di tipo EffManagedVersion.")
   @RBComment("Message for exception thrown when client eff group is validated and found to be set to a changeable target of an invalid type.")
   public static final String VALIDATE_TARGET_INVALID = "101";

   @RBEntry("Tipo di effettività non specificato.")
   @RBComment("Message for exception thrown when client eff group is validated and found to not have its effectivity type specified.")
   public static final String VALIDATE_FORM_NOT_SPECIFIED = "102";

   @RBEntry("Il tipo di effettività deve essere un'interfaccia secondaria di EffForm.")
   @RBComment("Message for exception thrown when client eff group is validated and found to be set to an invalid effectivity type.")
   public static final String VALIDATE_FORM_INVALID = "103";

   @RBEntry("Il contesto di effettività è obbligatorio per questo tipo di effettività.")
   @RBComment("Message for exception thrown when client eff group is validated and found to not have its effectivity context specified for an effectivity type requiring one.")
   public static final String VALIDATE_EFF_CONTEXT_NOT_SPECIFIED = "104";

   @RBEntry("Il contesto di effettività deve essere un'istanza di forma EffContext.")
   @RBComment("Message for exception thrown when client eff group is validated and found to be set to an eff context of an invalid type.")
   public static final String VALIDATE_EFF_CONTEXT_INVALID = "105";

   @RBEntry("Intervallo non specificato")
   @RBComment("Message for exception thrown when client eff group is validated and found to not have its range specified.")
   public static final String VALIDATE_RANGE_NOT_SPECIFIED = "106";
  
   @RBEntry("Gli intervalli di effettività per le versioni {0} e {1} della destinazione '{2}' si sovrappongono e non possono essere applicati contemporaneamente.")
   @RBComment("Message for exception thrown when client eff groups are validated and two found to have overlapping ranges for different versions for same context and effectivity type.")
   public static final String VALIDATE_RANGES_OVERLAP = "107";

   @RBEntry("Esiste già in {0} una dichiarazione di effettività in sospeso per questa destinazione per lo stesso tipo e contesto.")
   @RBComment("Message for exception thrown when client eff groups are validated and a pending effectivity statement already exists for the target for same effectivity type and context.")
   public static final String VALIDATE_PENDING_EFF_ALREADY_EXISTS = "108";

   @RBEntry("Formato intervallo non valido. Per informazioni dettagliate, vedere il log del method server")
   @RBComment("Message for exception thrown when client eff group is validated and found that the format of range specified is invalid.")
   public static final String VALIDATE_RANGE_INVALID = "109";
   
   @RBEntry("Impossibile rimuovere gli oggetti selezionati dalla tabella Effettività a causa di un blocco amministrativo.")
   @RBComment("Message for when administratively locked effectivities are trying to be removed.")
   public static final String VALIDATE_ADMIN_LOCKED_EFFS_ALL = "VALIDATE_ADMIN_LOCKED_EFFS_ALL";

   @RBEntry("Impossibile rimuovere uno o più oggetti selezionati dalla tabella Effettività a causa di un blocco amministrativo.")
   @RBComment("Message for when administratively locked effectivities are trying to be removed.")
   public static final String VALIDATE_ADMIN_LOCKED_EFFS_SOME = "VALIDATE_ADMIN_LOCKED_EFFS_SOME";
   
   @RBEntry("L'oggetto è bloccato a livello amministrativo. Per attivare i campi di input, impostare il contesto di effettività.")
   @RBComment("Message for when effectivities on administratively locked objects are trying to be created.")
   public static final String VALIDATE_ADMIN_LOCKED_EFFS_INPUTS = "VALIDATE_ADMIN_LOCKED_EFFS_INPUTS";
   
   @RBEntry("Per creare nuove effettività per gli oggetti bloccati a livello amministrativo, è necessario un contesto di effettività non bloccato.")
   @RBComment("Message for when effectivities on administratively locked objects are trying to be created.")
   public static final String VALIDATE_ADMIN_LOCKED_EFFS_CONTEXT = "VALIDATE_ADMIN_LOCKED_EFFS_CONTEXT";
   
   /**
    * --------------------------------------------------------------------
    * Miscellaneous exceptions
    * --------------------------------------------------------------------
    **/
   @RBEntry("Il riferimento master passato non è master per le versioni gestite da effettività.")
   @RBComment("Message for exception thrown the master reference passed is not for EffManagedVersion versions.")
   public static final String MASTER_REFERENCE_NOT_EFF_MANAGED = "120";

   /**
    * --------------------------------------------------------------------
    * Loader exceptions
    * --------------------------------------------------------------------
    **/
   @RBEntry("Impossibile trovare la direttiva di modifica: {0}")
   @RBComment("An error that is displayed when effectivity associated to a change directive is loaded and the change directive specified cannot be found.  The parameter will be the change directive number.")
   public static final String CHANGE_DIRECTIVE_NOT_FOUND_BY_NUMBER = "150";

   @RBEntry("Impossibile trovare il contesto di effettività: {0}")
   @RBComment("An error that is displayed when effectivity associated to a change directive is loaded and the effectivity context (part) specified by the number cannot be found.  The parameter will be the effectivity context (part) number.")
   public static final String EFF_CONTEXT_NOT_FOUND_BY_NUMBER = "151";

   @RBEntry("Propagazione struttura non riuscita perché sono stati rilevati dei riferimenti circolari nella struttura con {0}.")
   @RBComment("Error message in the event when circular references is detected in the structure.")
   public static final String STRUCTURE_RECURSION_DETECTED = "160";

}
