/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.eff.form;

import wt.util.resource.*;

@RBUUID("wt.eff.form.formResource")
public final class formResource_it extends WTListResourceBundle {
   /**
    * --------------------------------------------------------------------
    * General exceptions
    * --------------------------------------------------------------------
    **/
   @RBEntry("Il valore trasmesso non può essere nullo.")
   @RBComment("Exception message when null parameter is passed to method requiring non-null value.")
   public static final String NULL_ARG = "0";
}
