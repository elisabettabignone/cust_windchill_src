/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.eff.format;

import wt.util.resource.*;

@RBUUID("wt.eff.format.formatResource")
public final class formatResource_it extends WTListResourceBundle {
   /**
    * Unit Number Effectivity Format Entries
    **/
   @RBEntry("Formato unità di default")
   @RBComment("This value is used for display in the UI.")
   public static final String DEFAULT_UNIT_FORMAT_NAME = "DEFAULT_UNIT_FORMAT_NAME";

   @RBEntry("Formato unità di default per l'effettività utilizzato da Windchill quando non è specificato un formato di effettività. Fornisce implementazioni vuote delle varie operazioni relative al formato.")
   @RBComment("This description string is displayed if more information about an effectivity format is needed.")
   public static final String DEFAULT_UNIT_FORMAT_DESCRIPTION = "DEFAULT_UNIT_FORMAT_DESCRIPTION";

   @RBEntry("Formato unità alfanumeriche")
   @RBComment("This value is used for display in the UI.")
   public static final String ALPHA_NUMERIC_UNIT_FORMAT_NAME = "ALPHA_NUMERIC_UNIT_FORMAT_NAME";

   @RBEntry("Questo formato supporta dichiarazioni di effettività unità numerica che contengono sia un prefisso/suffisso alfabetico che uno numerico.")
   @RBComment("This description string is displayed if more information about an effectivity format is needed.")
   public static final String ALPHA_NUMERIC_UNIT_FORMAT_DESCRIPTION = "ALPHA_NUMERIC_UNIT_FORMAT_DESCRIPTION";

   @RBEntry("Formato unità numeriche")
   @RBComment("This value is used for display in the UI.")
   public static final String NUMERIC_UNIT_FORMAT_NAME = "NUMERIC_UNIT_FORMAT_NAME";

   @RBEntry("Questo formato supporta dichiarazioni di effettività unità numerica di lunghezza fissa che contengono cifre numeriche.")
   @RBComment("This description string is displayed if more information about an effectivity format is needed.")
   public static final String NUMERIC_UNIT_FORMAT_DESCRIPTION = "NUMERIC_UNIT_FORMAT_DESCRIPTION";

   /**
    * Date Effectivity Format Entries
    **/
   @RBEntry("Formato data di default")
   @RBComment("This value is used for display in the UI.")
   public static final String DEFAULT_DATE_FORMAT_NAME = "DEFAULT_DATE_FORMAT_NAME";

   @RBEntry("Formato data di default per l'effettività utilizzato da Windchill quando non è specificato un formato di effettività. Il presupposto è che tutte le dichiarazioni di effettività siano conformi al formato data delle impostazioni della lingua del sistema.")
   @RBComment("This description string is displayed if more information about an effectivity format is needed.")
   public static final String DEFAULT_DATE_FORMAT_DESCRIPTION = "DEFAULT_DATE_FORMAT_DESCRIPTION";

   /**
    * Effectivity Formats for various effectivity forms/types - used in the preference manager UI
    **/
   @RBEntry("Formato numero di serie")
   @RBComment("This value is used for display in the preference manager UI.")
   public static final String EFF_FORMAT_SERIAL_NUMBER = "EFF_FORMAT_SERIAL_NUMBER_DISPLAY_NAME";

   @RBEntry("Classe utilizzata per convalidare il formato dei numeri di serie")
   @RBComment("This value is used for display in the preference manager UI.")
   public static final String EFF_FORMAT_SERIAL_NUMBER_SHORT_DESCRIPTION = "EFF_FORMAT_SERIAL_NUMBER_SHORT_DESCRIPTION";

   @RBEntry("Specifica il formato utilizzato per la convalida dei numeri di serie. La sintassi del valore della preferenza è il nome della classe utilizzato per la convalida e i relativi parametri separati dal carattere \"|\". Il valore di default è wt.eff.format.DefaultUnitEffFormat. Per informazioni dettagliate, vedere la documentazione di Java.\n<BR><B>Attenzione:</B>\n non cambiare il valore della preferenza se il formato specificato è stato utilizzato in produzione, in particolare qualora le effettività siano state propagate in una struttura.")
   @RBComment("This value is used for display in the preference manager UI.")
   public static final String EFF_FORMAT_SERIAL_NUMBER_LONG_DESCRIPTION = "EFF_FORMAT_SERIAL_NUMBER_LONG_DESCRIPTION";

   @RBEntry("Formato numero di lotto")
   @RBComment("This value is used for display in the preference manager UI.")
   public static final String EFF_FORMAT_LOT_NUMBER_DISPLAY_NAME = "EFF_FORMAT_LOT_NUMBER_DISPLAY_NAME";

   @RBEntry("Classe utilizzata per convalidare il formato dei numeri di lotto")
   @RBComment("This value is used for display in the preference manager UI.")
   public static final String EFF_FORMAT_LOT_NUMBER_SHORT_DESCRIPTION = "EFF_FORMAT_LOT_NUMBER_SHORT_DESCRIPTION";

    @RBEntry("Specifica il formato utilizzato per la convalida dei numeri di lotto. La sintassi del valore della preferenza è il nome della classe utilizzato per la convalida e i relativi parametri separati dal carattere \"|\". Il valore di default è wt.eff.format.DefaultUnitEffFormat. Per informazioni dettagliate, vedere la documentazione di Java.\n<BR><B>Attenzione</B>\n Non cambiare il valore della preferenza se il formato specificato è stato utilizzato in produzione, in particolare qualora le effettività siano state propagate in una struttura.")
    @RBComment("This value is used for display in the preference manager UI.")
   public static final String EFF_FORMAT_LOT_NUMBER = "EFF_FORMAT_LOT_NUMBER_LONG_DESCRIPTION";

   /**
    * Entries for Exceptions/Errors
    **/
   @RBEntry("Il valore di inizio intervallo {0} non è conforme al formato specificato per il contesto.")
   @RBComment("Error message displayed in the client, when a range value of an effectivity statement does not conform to the eff. context object's format.")
   @RBArgComment0("Effectivity range value entered by user")
   public static final String INVALID_START_VALUE = "INVALID_START_VALUE";

   @RBEntry("Il valore di fine intervallo {0} non è conforme al formato specificato per il contesto")
   @RBComment("Error message displayed in the client, when a range value of an effectivity statement does not conform to the eff. context object's format.")
   @RBArgComment0("Effectivity range value entered by user")
   public static final String INVALID_END_VALUE = "INVALID_END_VALUE";

   @RBEntry("Il valore di inizio intervallo {0} e il valore di fine intervallo {1} non sono conformi al formato specificato per il contesto.")
   @RBComment("Error message displayed in the client, when the start and end range values of an effectivity statement do not conform to the eff. context object's format.")
   @RBArgComment0("Effectivity range value entered by user")
   @RBArgComment1("Effectivity range value entered by user")
   public static final String INVALID_START_END_VALUES = "INVALID_START_END_VALUES";

   @RBEntry("Il valore di inizio intervallo {0} è maggiore del valore di fine intervallo {1}.")
   @RBComment("Error message displayed in the client, when the start range value is greater than the end range value of an effectivity statement entered by the user.")
   @RBArgComment0("Effectivity range value entered by user")
   @RBArgComment1("Effectivity range value entered by user")
   public static final String START_GREATER_THAN_END = "START_GREATER_THAN_END";

   @RBEntry("Il valore di intervallo {0} contiene caratteri non validi.")
   @RBComment("Error message displayed in the client, when a range value of an effectivity statement does not conform to the eff. context object's format.")
   @RBArgComment0("Effectivity range value entered by user")
   public static final String INVALID_RANGE_VALUE = "INVALID_RANGE_VALUE";

   @RBEntry("Il valore di intervallo {0} contiene caratteri non numerici non consentiti dal formato specificato nel contesto.")
   @RBComment("Error message displayed in the client, when a range value of an effectivity statement does not conform to the eff. context object's format.")
   @RBArgComment0("Effectivity range value entered by user")
   public static final String INVALID_NUMERICAL_RANGE_VALUE = "INVALID_NUMERICAL_RANGE_VALUE";

   @RBEntry("Lunghezza intervallo non valida.")
   public static final String INVALID_LENGTH = "INVALID_LENGTH";

   @RBEntry("Prefisso non valido.")
   public static final String INVALID_PREFIX = "INVALID_PREFIX";

   @RBEntry("Caratteri non validi.")
   public static final String INVALID_CHARS = "INVALID_CHARS";

   @RBEntry("Suffisso non valido.")
   public static final String INVALID_SUFFIX = "INVALID_SUFFIX";

   @RBEntry("Il valore intervallo è vuoto.")
   public static final String EMPTY_RANGE = "EMPTY_RANGE";

   @RBEntry("Formato data non valido.")
   public static final String INVALID_DATE = "INVALID_DATE";

   @RBEntry("Il valore data è vuoto.")
   public static final String EMPTY_DATE = "EMPTY_DATE";

   @RBEntry("Il valore intervallo contiene uno spazio.")
   public static final String INVALID_WHITE_SPACE = "INVALID_WHITE_SPACE";

   @RBEntry("Impossibile confrontare i valori intervallo di effettività non validi.")
   public static final String INVALID_COMPARE = "INVALID_COMPARE";

   @RBEntry("I valori dell'intervallo di effettività non sono validi e non possono essere utilizzati per il calcolo dell'intervallo.")
   public static final String RANGE_ERR = "RANGE_ERR";

   @RBEntry("L'operazione ha restituito un valore intervallo di lunghezza non valida.")
   public static final String RESULT_RANGE_ERR = "RESULT_RANGE_ERR";

   @RBEntry("La lunghezza specificata deve essere maggiore di almeno un'unità rispetto alla somma delle lunghezze di prefisso e suffisso.")
   public static final String PREFERENCE_LENGTH_ERR = "PREFERENCE_LENGTH_ERR";

   @RBEntry("È necessario specificare il valore di inizio intervallo.")
   public static final String EMPTY_START_RANGE = "EMPTY_START_RANGE";
}
