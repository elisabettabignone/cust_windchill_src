/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.eff.format;

import wt.util.resource.*;

@RBUUID("wt.eff.format.formatResource")
public final class formatResource extends WTListResourceBundle {
   /**
    * Unit Number Effectivity Format Entries
    **/
   @RBEntry("Default Unit Format")
   @RBComment("This value is used for display in the UI.")
   public static final String DEFAULT_UNIT_FORMAT_NAME = "DEFAULT_UNIT_FORMAT_NAME";

   @RBEntry("This is the default unit number effectivity format used by Windchill if no other effectivity format is specified. It provides empty implementations of the various format operations.")
   @RBComment("This description string is displayed if more information about an effectivity format is needed.")
   public static final String DEFAULT_UNIT_FORMAT_DESCRIPTION = "DEFAULT_UNIT_FORMAT_DESCRIPTION";

   @RBEntry("Alpha-Numeric Unit Format")
   @RBComment("This value is used for display in the UI.")
   public static final String ALPHA_NUMERIC_UNIT_FORMAT_NAME = "ALPHA_NUMERIC_UNIT_FORMAT_NAME";

   @RBEntry("This format supports fixed-length unit number effectivity statements that contain both an alphabet prefix/suffix and a numerical suffix/prefix.")
   @RBComment("This description string is displayed if more information about an effectivity format is needed.")
   public static final String ALPHA_NUMERIC_UNIT_FORMAT_DESCRIPTION = "ALPHA_NUMERIC_UNIT_FORMAT_DESCRIPTION";

   @RBEntry("Numeric Unit Format")
   @RBComment("This value is used for display in the UI.")
   public static final String NUMERIC_UNIT_FORMAT_NAME = "NUMERIC_UNIT_FORMAT_NAME";

   @RBEntry("This format supports fixed-length unit number effectivity statements that contain numeric digits.")
   @RBComment("This description string is displayed if more information about an effectivity format is needed.")
   public static final String NUMERIC_UNIT_FORMAT_DESCRIPTION = "NUMERIC_UNIT_FORMAT_DESCRIPTION";

   /**
    * Date Effectivity Format Entries
    **/
   @RBEntry("Default Date Format")
   @RBComment("This value is used for display in the UI.")
   public static final String DEFAULT_DATE_FORMAT_NAME = "DEFAULT_DATE_FORMAT_NAME";

   @RBEntry("This is the default date effectivity format used by Windchill if no other effectivity format is specified. It expects all effectivity statements adhere to the date format of the system's locale.")
   @RBComment("This description string is displayed if more information about an effectivity format is needed.")
   public static final String DEFAULT_DATE_FORMAT_DESCRIPTION = "DEFAULT_DATE_FORMAT_DESCRIPTION";

   /**
    * Effectivity Formats for various effectivity forms/types - used in the preference manager UI
    **/
   @RBEntry("Serial Number Format")
   @RBComment("This value is used for display in the preference manager UI.")
   public static final String EFF_FORMAT_SERIAL_NUMBER = "EFF_FORMAT_SERIAL_NUMBER_DISPLAY_NAME";

   @RBEntry("The class used to validate the format of serial number input")
   @RBComment("This value is used for display in the preference manager UI.")
   public static final String EFF_FORMAT_SERIAL_NUMBER_SHORT_DESCRIPTION = "EFF_FORMAT_SERIAL_NUMBER_SHORT_DESCRIPTION";

   @RBEntry("Specify the format used to validate serial number input. The syntax for this preference value is the class name used for validation plus its parameters separated by the \"|\" character. The default value is wt.eff.format.DefaultUnitEffFormat. See the JavaDoc for details.\n<BR><B>Caution:</B>\n Do not change the value of this preference if the currently specified format has been used in production, especially when effectivities have been propagated throughout a structure.")
   @RBComment("This value is used for display in the preference manager UI.")
   public static final String EFF_FORMAT_SERIAL_NUMBER_LONG_DESCRIPTION = "EFF_FORMAT_SERIAL_NUMBER_LONG_DESCRIPTION";

   @RBEntry("Lot Number Format")
   @RBComment("This value is used for display in the preference manager UI.")
   public static final String EFF_FORMAT_LOT_NUMBER_DISPLAY_NAME = "EFF_FORMAT_LOT_NUMBER_DISPLAY_NAME";

   @RBEntry("The class used to validate the format of lot number input")
   @RBComment("This value is used for display in the preference manager UI.")
   public static final String EFF_FORMAT_LOT_NUMBER_SHORT_DESCRIPTION = "EFF_FORMAT_LOT_NUMBER_SHORT_DESCRIPTION";

    @RBEntry("Specify the format used to validate lot number input. The syntax for this preference value is the class name used for validation plus its parameters separated by the \"|\" character. The default value is wt.eff.format.DefaultUnitEffFormat. See the JavaDoc for details.\n<BR><B>Caution:</B>\n Do not change the value of this preference if the currently specified format has been used in production, especially when effectivities have been propagated throughout a structure.")
    @RBComment("This value is used for display in the preference manager UI.")
   public static final String EFF_FORMAT_LOT_NUMBER = "EFF_FORMAT_LOT_NUMBER_LONG_DESCRIPTION";

   /**
    * Entries for Exceptions/Errors
    **/
   @RBEntry("The start range value {0} does not conform to the format specified on the context.")
   @RBComment("Error message displayed in the client, when a range value of an effectivity statement does not conform to the eff. context object's format.")
   @RBArgComment0("Effectivity range value entered by user")
   public static final String INVALID_START_VALUE = "INVALID_START_VALUE";

   @RBEntry("The end range value {0} does not conform to the format specified on the context.")
   @RBComment("Error message displayed in the client, when a range value of an effectivity statement does not conform to the eff. context object's format.")
   @RBArgComment0("Effectivity range value entered by user")
   public static final String INVALID_END_VALUE = "INVALID_END_VALUE";

   @RBEntry("The start range value {0} and the end range value {1} do not conform to the format specified on the context.")
   @RBComment("Error message displayed in the client, when the start and end range values of an effectivity statement do not conform to the eff. context object's format.")
   @RBArgComment0("Effectivity range value entered by user")
   @RBArgComment1("Effectivity range value entered by user")
   public static final String INVALID_START_END_VALUES = "INVALID_START_END_VALUES";

   @RBEntry("The start range value {0} is greater than the end range value {1}.")
   @RBComment("Error message displayed in the client, when the start range value is greater than the end range value of an effectivity statement entered by the user.")
   @RBArgComment0("Effectivity range value entered by user")
   @RBArgComment1("Effectivity range value entered by user")
   public static final String START_GREATER_THAN_END = "START_GREATER_THAN_END";

   @RBEntry("The range value {0} contains invalid characters.")
   @RBComment("Error message displayed in the client, when a range value of an effectivity statement does not conform to the eff. context object's format.")
   @RBArgComment0("Effectivity range value entered by user")
   public static final String INVALID_RANGE_VALUE = "INVALID_RANGE_VALUE";

   @RBEntry("The range value {0} contains non-numerical characters which is disallowed by the format specified on the context.")
   @RBComment("Error message displayed in the client, when a range value of an effectivity statement does not conform to the eff. context object's format.")
   @RBArgComment0("Effectivity range value entered by user")
   public static final String INVALID_NUMERICAL_RANGE_VALUE = "INVALID_NUMERICAL_RANGE_VALUE";

   @RBEntry("The length of the range is invalid.")
   public static final String INVALID_LENGTH = "INVALID_LENGTH";

   @RBEntry("Invalid prefix.")
   public static final String INVALID_PREFIX = "INVALID_PREFIX";

   @RBEntry("Invalid characters.")
   public static final String INVALID_CHARS = "INVALID_CHARS";

   @RBEntry("Invalid suffix.")
   public static final String INVALID_SUFFIX = "INVALID_SUFFIX";

   @RBEntry("The range value is empty.")
   public static final String EMPTY_RANGE = "EMPTY_RANGE";

   @RBEntry("Invalid date format.")
   public static final String INVALID_DATE = "INVALID_DATE";

   @RBEntry("The date value is empty.")
   public static final String EMPTY_DATE = "EMPTY_DATE";

   @RBEntry("Range value contains white space.")
   public static final String INVALID_WHITE_SPACE = "INVALID_WHITE_SPACE";

   @RBEntry("Invalid effectivity range values can not be compared.")
   public static final String INVALID_COMPARE = "INVALID_COMPARE";

   @RBEntry("Invalid effectivity range values can not be used for range arithmetic.")
   public static final String RANGE_ERR = "RANGE_ERR";

   @RBEntry("Operation resulted in a range value that is of invalid length.")
   public static final String RESULT_RANGE_ERR = "RESULT_RANGE_ERR";

   @RBEntry("Length specified should be at least one greater than the combined length of prefix and suffix.")
   public static final String PREFERENCE_LENGTH_ERR = "PREFERENCE_LENGTH_ERR";

   @RBEntry("The start range value must be specified.")
   public static final String EMPTY_START_RANGE = "EMPTY_START_RANGE";
}
