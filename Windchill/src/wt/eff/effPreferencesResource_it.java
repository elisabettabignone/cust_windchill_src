package wt.eff;

import wt.util.resource.*;

@RBUUID("wt.eff.effPreferencesResource")
public final class effPreferencesResource_it extends WTListResourceBundle {
   /**
/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
    **/
   @RBEntry("Attiva propagazione a elementi di pari livello")
   @RBComment("Display name for this preference.")
   public static final String PRIVATE_CONSTANT_0 = "SiblingPropagationEnabled.displayName";

   @RBEntry("Attiva la propagazione delle dichiarazioni di effettività agli elementi di pari livello.")
   @RBComment("Short description for this preference.")
   public static final String PRIVATE_CONSTANT_1 = "SiblingPropagationEnabled.description";

   @RBEntry("Questa preferenza determina se l'effettività degli elementi di pari livello è regolata in base a una dichiarazione di effettività applicata tramite un processo di modifica approvato.")
   public static final String PRIVATE_CONSTANT_2 = "SiblingPropagationEnabled.longDescription";

   @RBEntry("Attiva propagazione struttura")
   @RBComment("Display name for this preference.")
   public static final String PRIVATE_CONSTANT_3 = "StructurePropagationEnabled.displayName";

   @RBEntry("Attiva la propagazione delle dichiarazioni di effettività agli elementi di livello inferiore nella struttura di prodotto.")
   @RBComment("Short description for this preference.")
   public static final String PRIVATE_CONSTANT_4 = "StructurePropagationEnabled.description";

   @RBEntry("La preferenza determina se le dichiarazioni di effettività sono propagate ai livelli inferiori della struttura di prodotto se attivate tramite un processo di modifica approvato. Se la preferenza è attivata, viene utilizzata una specifica di configurazione parte (in base alle altre preferenze di effettività) per identificare le parti figlio a cui applicare la dichiarazione di effettività.")
   @RBComment("Long description for this preference.")
   public static final String PRIVATE_CONSTANT_5 = "StructurePropagationEnabled.longDescription";

   @RBEntry("Attiva propagazione a elementi di pari livello ricorsiva")
   @RBComment("Display name for this preference.")
   public static final String PRIVATE_CONSTANT_6 = "RecursiveSiblingPropagationEnabled.displayName";

   @RBEntry("Attiva la propagazione delle dichiarazioni di effettività verso i livelli inferiori di una struttura di prodotto per le dichiarazioni che risultano da un'operazione di propagazione agli elementi di pari livello.")
   @RBComment("Short description for this preference.")
   public static final String PRIVATE_CONSTANT_7 = "RecursiveSiblingPropagationEnabled.description";

   @RBEntry("La preferenza determina se una dichiarazione di effettività regolata in risultanza di un'operazione di effettività è propagata ai livelli inferiori della struttura di prodotto. Se la preferenza è attivata, la chiusura delle dichiarazioni di effettività degli elementi di pari livello verrà propagata in basso nella struttura, garantendo che nessun figlio abbia un'effettività maggiore della somma dell'effettività dei padri.")
   public static final String PRIVATE_CONSTANT_8 = "RecursiveSiblingPropagationEnabled.longDescription";

   @RBEntry("Stato del ciclo di vita della specifica di configurazione parte per propagazione a struttura")
   @RBComment("Display name for this preference.")
   public static final String PRIVATE_CONSTANT_9 = "StructurePropagationPartConfigSpecLifecycleState.displayName";

   @RBEntry("Stato del ciclo di vita della specifica di configurazione parte utilizzata per la propagazione a struttura delle dichiarazioni di effettività.")
   @RBComment("Short description for this preference.")
   public static final String PRIVATE_CONSTANT_10 = "StructurePropagationPartConfigSpecLifecycleState.description";

   @RBEntry("La preferenza definisce lo stato del ciclo di vita della specifica di configurazione parte utilizzata per identificare le parti figlio durante la propagazione ai livelli inferiori di una struttura di prodotto.")
   @RBComment("Long description for this preference.")
   public static final String PRIVATE_CONSTANT_11 = "StructurePropagationPartConfigSpecLifecycleState.longDescription";

   @RBEntry("Vista della specifica di configurazione parte per propagazione struttura")
   @RBComment("Display name for this preference.")
   public static final String PRIVATE_CONSTANT_12 = "StructurePropagationPartConfigSpecView.displayName";

   @RBEntry("Vista della specifica di configurazione parte utilizzata per la propagazione a struttura delle dichiarazioni di effettività.")
   @RBComment("Short description for this preference.")
   public static final String PRIVATE_CONSTANT_13 = "StructurePropagationPartConfigSpecView.description";

   @RBEntry("La preferenza definisce la vista della specifica di configurazione parte utilizzata per la propagazione a struttura delle dichiarazioni di effettività.")
   @RBComment("Long description for this preference.")
   public static final String PRIVATE_CONSTANT_14 = "StructurePropagationPartConfigSpecView.longDescription";

   @RBEntry("Includi parti degli elementi in corso per specifica di configurazione parte per propagazione a struttura")
   @RBComment("Display name for this preference.")
   public static final String PRIVATE_CONSTANT_15 = "StructurePropagationPartConfigSpecIncludeWorking.displayName";

   @RBEntry("Include le parti degli elementi in corso nella specifica di configurazione parte utilizzata per la propagazione a struttura delle dichiarazioni di effettività.")
   @RBComment("Short description for this preference.")
   public static final String PRIVATE_CONSTANT_16 = "StructurePropagationPartConfigSpecIncludeWorking.description";

   @RBEntry("La preferenza attiva l'inclusione delle parti degli elementi in corso nella specifica di configurazione parte utilizzata per la propagazione a struttura delle dichiarazioni di effettività. ")
   @RBComment("Long description for this preference.")
   public static final String PRIVATE_CONSTANT_17 = "StructurePropagationPartConfigSpecIncludeWorking.longDescription";


   @RBEntry("Includi ora in effettività per data")
   @RBComment("Display name for this preference.")
   public static final String INCLUDE_TIME_IN_DATE_EFFECTIVITY_NAME = "INCLUDE_TIME_IN_DATE_EFFECTIVITY_NAME";

   @RBEntry("Consente di definire l'effettività per data utilizzando data e ora.")
   @RBComment("Short description for this preference.")
   public static final String INCLUDE_TIME_IN_DATE_EFFECTIVITY_DESC = "INCLUDE_TIME_IN_DATE_EFFECTIVITY_DESC";

   @RBEntry("Se impostata su No, l'effettività per data viene definita specificando una data. Come ora viene utilizzata la mezzanotte (00.00). Se impostata su Sì, l'effettività per data viene definita specificando sia la data che l'ora.")
   @RBComment("Long description for this preference.")
   public static final String INCLUDE_TIME_IN_DATE_EFFECTIVITY_LONG_DESC = "INCLUDE_TIME_IN_DATE_EFFECTIVITY_LONG_DESC";
}
