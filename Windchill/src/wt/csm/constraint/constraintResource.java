/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.csm.constraint;

import wt.util.resource.*;

@RBUUID("wt.csm.constraint.constraintResource")
public final class constraintResource extends WTListResourceBundle {
   @RBEntry("Ranking attribute")
   public static final String RNKSHORTDESCRIPTION = "rnkmsg0";

   @RBEntry("Attribute values correspond to ranking values.")
   public static final String RNKLONGDESCRIPTION = "rnkmsg1";

   @RBEntry("Ranking attribute")
   public static final String RNKDISPLYNAME = "rnkmsg2";

   @RBEntry("Cannot search \"{0}\"")
   public static final String SEARCH_ERROR = "rnkmsg3";

   @RBEntry("Cannot construct \"{0}\"")
   public static final String CONSTRUCT_ERROR = "rnkmsg4";
}
