/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.csm.constraint;

import wt.util.resource.*;

@RBUUID("wt.csm.constraint.constraintResource")
public final class constraintResource_it extends WTListResourceBundle {
   @RBEntry("Attributo di classificazione")
   public static final String RNKSHORTDESCRIPTION = "rnkmsg0";

   @RBEntry("I valori degli attributi corrispondono ai valori di classificazione.")
   public static final String RNKLONGDESCRIPTION = "rnkmsg1";

   @RBEntry("Attributo di classificazione")
   public static final String RNKDISPLYNAME = "rnkmsg2";

   @RBEntry("Impossibile cercare \"{0}\"")
   public static final String SEARCH_ERROR = "rnkmsg3";

   @RBEntry("Impossibile costruire \"{0}\"")
   public static final String CONSTRUCT_ERROR = "rnkmsg4";
}
