/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.csm.businessentity.service;

import wt.util.resource.*;

@RBUUID("wt.csm.businessentity.service.serviceResource")
public final class serviceResource_it extends WTListResourceBundle {
   @RBEntry("Entità aziendale \"{0}\" obsoleta.")
   public static final String OBSELETE_BUS_ENTITY = "0";

   @RBEntry("L'oggetto \"{0}\" referenziato non è valido.")
   public static final String INVALID_REF_OBJ = "1";

   @RBEntry("Impossibile aggiornare il database: \"{0}\"")
   public static final String CANNOT_REFRESH_DATABASE = "2";

   @RBEntry("Errore durante la creazione dell'entità aziendale: \"{0}\"")
   public static final String CANNOT_CONSTRUCT_LITEBUSENTITY = "3";

   @RBEntry("Errore durante la creazione dell'entità aziendale: \"{0}\"")
   public static final String CANNOT_CONSTRUCT_BUSINESSENTITY = "4";

   @RBEntry("Impossibile aggiornare il contenitore degli attributi: \"{0}\"")
   public static final String CANNOT_REFRESH_ATTR_CONTAINER = "5";

   @RBEntry("Il campo Nome è obbligatorio.")
   public static final String NULL_NAME_VALUE = "6";
}
