/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.csm.html.query;

import wt.util.resource.*;

@RBUUID("wt.csm.html.query.queryResource")
public final class queryResource extends WTListResourceBundle {
   @RBEntry("Graphics Off")
   public static final String GRAPHICS_OFF = "0";

   @RBEntry("Graphics On")
   public static final String GRAPHICS_ON = "1";

   @RBEntry("Re-Use Manager")
   public static final String PAGE_DESCRIPTION = "10";

   @RBEntry("No children under current node")
   public static final String NO_CHILDREN = "100";

   @RBEntry("No Image Found")
   public static final String NO_IMAGE_FOUND = "101";

   @RBEntry("No Query Form For Root Node")
   public static final String NO_QF_FOR_ROOT = "102";

   @RBEntry("wt/clients/images/changeunits.gif")
   @RBPseudo(false)
   @RBComment("do not translate")
   public static final String CHANGE_UNITS_IMAGE = "103";

   @RBEntry("Change Units")
   public static final String CHANGE_UNITS_ALTERNATIVE = "104";

   @RBEntry("Contains")
   public static final String LIKE_CONTAINS = "105";

   @RBEntry("Begins with")
   public static final String LIKE_STARTS_WITH = "106";

   @RBEntry("Ends with")
   public static final String LIKE_ENDS_WITH = "107";

   @RBEntry("Exact")
   public static final String LIKE_EXACT = "108";

   @RBEntry("Search Pattern: ")
   public static final String SEARCH_PATTERN = "109";

   @RBEntry("Next")
   public static final String NEXT = "11";

   @RBEntry("No nodes found.")
   public static final String NO_NODES_FOUND = "110";

   @RBEntry("Previous")
   public static final String PREVIOUS = "12";

   @RBEntry("Change Units")
   public static final String CHANGE_UNITS = "13";

   @RBEntry("View")
   public static final String VIEW = "14";

   @RBEntry("Functional Equivalence")
   public static final String FUNC_EQ = "15";

   @RBEntry("Sort Ascending")
   public static final String SORT_ASCEND = "16";

   @RBEntry("Sort Descending")
   public static final String SORT_DESCEND = "17";

   @RBEntry("No Results Found")
   public static final String RESULTS_NOT_FOUND = "18";

   @RBEntry("Change Ranking")
   public static final String CHANGE_RANKING = "19";

   @RBEntry("Query Form Off")
   public static final String QUERYFORM_OFF = "2";

   @RBEntry("ALL")
   public static final String ALL = "20";

   @RBEntry("Find Navigation Node:")
   public static final String FIND_NAVIGATION_NODE_LABEL = "201";

   @RBEntry("wt/clients/images/findnavnode.gif")
   @RBPseudo(false)
   @RBComment("do not translate")
   public static final String FIND_NAVIGATION_NODE_IMAGE = "202";

   @RBEntry("Find Navigation Node")
   public static final String FIND_NAVIGATION_NODE_ALTERNATIVE = "203";

   @RBEntry("Search Results")
   public static final String SEARCH_RESULTS = "21";

   @RBEntry("Functional Equivalence Search")
   public static final String FUNCTIONAL_EQUIVALENCE_SEARCH = "22";

   @RBEntry("HTML")
   public static final String RESULTSOBJECT_HTML_LABEL = "23";

   @RBEntry("Text")
   public static final String RESULTSOBJECT_TEXT_LABEL = "24";

   @RBEntry("True")
   public static final String TRUE_LABEL = "25";

   @RBEntry("False")
   public static final String FALSE_LABEL = "26";

   @RBEntry("From:")
   public static final String FROM_LABEL = "27";

   @RBEntry("To:")
   public static final String TO_LABEL = "28";

   @RBEntry("Invalid number format:")
   public static final String NUMBERFORMAT_EX = "29";

   @RBEntry("Query Form On")
   public static final String QUERYFORM_ON = "3";

   @RBEntry("Units:")
   public static final String UNITS_LABEL = "4";

   @RBEntry("Ranking Context:")
   public static final String RANKINGCONTEXT_LABEL = "5";

   @RBEntry("Display:")
   public static final String RESULTSCOUNT_LABEL = "6";

   @RBEntry("Display Results:")
   public static final String DISPLAYRESULTS_LABEL = "7";

   @RBEntry("Results Object:")
   public static final String RESULTSOBJECT_LABEL = "8";

   @RBEntry("Search")
   public static final String SEARCH_LABEL = "9";

   @RBEntry("Absolute")
   public static final String CRITERIA_ABSOLUTE = "cr01";

   @RBEntry("Percent")
   public static final String CRITERIA_PERCENT = "cr02";

   @RBEntry("Attribute")
   public static final String HDR_ATTRIBUTE = "hdr00";

   @RBEntry("Include")
   public static final String HDR_INCLUDE = "hdr01";

   @RBEntry("Value")
   public static final String HDR_VALUE = "hdr02";

   @RBEntry("Criteria")
   public static final String HDR_CRITERIA = "hdr03";

   @RBEntry("wt/clients/images/lite_nexttl.gif")
   @RBPseudo(false)
   @RBComment("do not translate")
   public static final String LITE_NEXT_IMAGE = "im01";

   @RBEntry("wt/clients/images/lite_prevtl.gif")
   @RBPseudo(false)
   @RBComment("do not translate")
   public static final String LITE_PREV_IMAGE = "im02";

   @RBEntry("Go to Windchill Home")
   public static final String WINDCHILL_HOME_TOOLTIP = "tt01";

   @RBEntry("Search")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String SEARCH_HELP = "tt02";

   @RBEntry("Results")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String RESULTS_HELP = "tt03";

   @RBEntry("Help")
   public static final String HELP_TOOLTIP = "tt04";
}
