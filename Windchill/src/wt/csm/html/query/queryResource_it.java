/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.csm.html.query;

import wt.util.resource.*;

@RBUUID("wt.csm.html.query.queryResource")
public final class queryResource_it extends WTListResourceBundle {
   @RBEntry("Grafici disattivati")
   public static final String GRAPHICS_OFF = "0";

   @RBEntry("Grafici attivati")
   public static final String GRAPHICS_ON = "1";

   @RBEntry("Gestione riutilizzo")
   public static final String PAGE_DESCRIPTION = "10";

   @RBEntry("Nessun figlio nel nodo corrente")
   public static final String NO_CHILDREN = "100";

   @RBEntry("Nessuna immagine")
   public static final String NO_IMAGE_FOUND = "101";

   @RBEntry("Nessun formulario di interrogazione per il nodo della radice")
   public static final String NO_QF_FOR_ROOT = "102";

   @RBEntry("wt/clients/images/changeunits.gif")
   @RBPseudo(false)
   @RBComment("do not translate")
   public static final String CHANGE_UNITS_IMAGE = "103";

   @RBEntry("Modifica unità")
   public static final String CHANGE_UNITS_ALTERNATIVE = "104";

   @RBEntry("Contiene")
   public static final String LIKE_CONTAINS = "105";

   @RBEntry("Inizia con")
   public static final String LIKE_STARTS_WITH = "106";

   @RBEntry("Termina con")
   public static final String LIKE_ENDS_WITH = "107";

   @RBEntry("Esatto")
   public static final String LIKE_EXACT = "108";

   @RBEntry("Espressione di ricerca:")
   public static final String SEARCH_PATTERN = "109";

   @RBEntry("Avanti")
   public static final String NEXT = "11";

   @RBEntry("Nessun nodo trovato.")
   public static final String NO_NODES_FOUND = "110";

   @RBEntry("Indietro")
   public static final String PREVIOUS = "12";

   @RBEntry("Modifica unità")
   public static final String CHANGE_UNITS = "13";

   @RBEntry("Visualizza")
   public static final String VIEW = "14";

   @RBEntry("Equivalenza funzionale")
   public static final String FUNC_EQ = "15";

   @RBEntry("Ordine crescente")
   public static final String SORT_ASCEND = "16";

   @RBEntry("Ordine decrescente")
   public static final String SORT_DESCEND = "17";

   @RBEntry("Nessun risultato")
   public static final String RESULTS_NOT_FOUND = "18";

   @RBEntry("Modifica classificazione")
   public static final String CHANGE_RANKING = "19";

   @RBEntry("Disattiva formulario d'interrogazione")
   public static final String QUERYFORM_OFF = "2";

   @RBEntry("TUTTI")
   public static final String ALL = "20";

   @RBEntry("Trova il nodo di navigazione:")
   public static final String FIND_NAVIGATION_NODE_LABEL = "201";

   @RBEntry("wt/clients/images/findnavnode.gif")
   @RBPseudo(false)
   @RBComment("do not translate")
   public static final String FIND_NAVIGATION_NODE_IMAGE = "202";

   @RBEntry("Trova il nodo di navigazione")
   public static final String FIND_NAVIGATION_NODE_ALTERNATIVE = "203";

   @RBEntry("Risultati della ricerca")
   public static final String SEARCH_RESULTS = "21";

   @RBEntry("Ricerca dell'equivalenza funzionale:")
   public static final String FUNCTIONAL_EQUIVALENCE_SEARCH = "22";

   @RBEntry("HTML")
   public static final String RESULTSOBJECT_HTML_LABEL = "23";

   @RBEntry("Testo")
   public static final String RESULTSOBJECT_TEXT_LABEL = "24";

   @RBEntry("True")
   public static final String TRUE_LABEL = "25";

   @RBEntry("False")
   public static final String FALSE_LABEL = "26";

   @RBEntry("Da:")
   public static final String FROM_LABEL = "27";

   @RBEntry("A:")
   public static final String TO_LABEL = "28";

   @RBEntry("Formato numero non valido:")
   public static final String NUMBERFORMAT_EX = "29";

   @RBEntry("Formulario d'interrogazione attivato")
   public static final String QUERYFORM_ON = "3";

   @RBEntry("Unità:")
   public static final String UNITS_LABEL = "4";

   @RBEntry("Contesto di classificazione:")
   public static final String RANKINGCONTEXT_LABEL = "5";

   @RBEntry("Numero di risultati:")
   public static final String RESULTSCOUNT_LABEL = "6";

   @RBEntry("Formato dei risultati:")
   public static final String DISPLAYRESULTS_LABEL = "7";

   @RBEntry("Risultati per:")
   public static final String RESULTSOBJECT_LABEL = "8";

   @RBEntry("Cerca")
   public static final String SEARCH_LABEL = "9";

   @RBEntry("Assoluto")
   public static final String CRITERIA_ABSOLUTE = "cr01";

   @RBEntry("Percentuale")
   public static final String CRITERIA_PERCENT = "cr02";

   @RBEntry("Attributo")
   public static final String HDR_ATTRIBUTE = "hdr00";

   @RBEntry("Includi")
   public static final String HDR_INCLUDE = "hdr01";

   @RBEntry("Valore")
   public static final String HDR_VALUE = "hdr02";

   @RBEntry("Criteri")
   public static final String HDR_CRITERIA = "hdr03";

   @RBEntry("wt/clients/images/lite_nexttl.gif")
   @RBPseudo(false)
   @RBComment("do not translate")
   public static final String LITE_NEXT_IMAGE = "im01";

   @RBEntry("wt/clients/images/lite_prevtl.gif")
   @RBPseudo(false)
   @RBComment("do not translate")
   public static final String LITE_PREV_IMAGE = "im02";

   @RBEntry("Vai alla home page Windchill")
   public static final String WINDCHILL_HOME_TOOLTIP = "tt01";

   @RBEntry("Search")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String SEARCH_HELP = "tt02";

   @RBEntry("Results")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String RESULTS_HELP = "tt03";

   @RBEntry("Guida")
   public static final String HELP_TOOLTIP = "tt04";
}
