/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.csm.navigation.service;

import wt.util.resource.*;

@RBUUID("wt.csm.navigation.service.serviceResource")
public final class serviceResource_it extends WTListResourceBundle {
   @RBEntry("La struttura di classificazione seguente è obsoleta: \"{0}\".")
   public static final String CLASSIFICATION_STRUCT_OBSOLETE = "0";

   @RBEntry("Violazione dell'univocità del nome. Il nome seguente esiste già: \"{0}\".")
   public static final String NAME_UNIQUENESS_VIOLATION = "1";

   @RBEntry("Impossibile recuperare il valore dell'attributo mappato per la classe IBAHolder: \"{0}\" .")
   public static final String LISTENER_CANNOT_READ_MAPPED_ATTR_VALUE = "10";

   @RBEntry("Impossibile recuperare il valore dell'attributo mappato per la classe IBAHolder: \"{0}\".")
   public static final String LISTENER_CANNOT_READ_MAPPED_ATTR_DEF = "11";

   @RBEntry("L'errore seguente si è verificato durante la lettura delle mappature degli attributi modellati in base agli attributi d'istanza attesi: \"{0}\" .")
   public static final String LISTENER_INITIALIZATION = "12";

   @RBEntry("Il nodo di classificazione è attualmente in uso: \"{0}\".")
   public static final String CLASSIFICATION_NODE__IN_USE = "13";

   @RBEntry("L'attributo \"{0}\" è stato usato in un'altra struttura di classificazione.")
   public static final String CLASSIFICATION_ATTRIBUTE_UNIQUENESS = "14";

   @RBEntry("La classe \"{0}\" è già stata classificata. ")
   public static final String CLASS_ALREADY_CLASSIFIED = "15";

   @RBEntry("Il campo Nome è obbligatorio.")
   public static final String NULL_NAME_VALUE = "16";

   @RBEntry("Etichetta obbligatoria.")
   public static final String NULL_LABEL_VALUE = "17";

   @RBEntry("La struttura di classificazione per \"{0}\" sta già venendo utilizzata.")
   public static final String CLASSIFICATION_STRUCT_IN_USE = "18";

   @RBEntry("3")
   @RBPseudo(false)
   @RBComment("do not translate")
   public static final String SEARCH_NAV_NODE_NAME_EXACT = "19";

   @RBEntry("Padre non valido.")
   public static final String INVALID_PARENT = "2";

   @RBEntry("0")
   @RBPseudo(false)
   @RBComment("do not translate")
   public static final String SEARCH_NAV_NODE_NAME_CONTAINS = "20";

   @RBEntry("1")
   @RBPseudo(false)
   @RBComment("do not translate")
   public static final String SEARCH_NAV_NODE_NAME_BEGINSWITH = "21";

   @RBEntry("2")
   @RBPseudo(false)
   @RBComment("do not translate")
   public static final String SEARCH_NAV_NODE_NAME_ENDSWITH = "22";

   @RBEntry("Impossibile eliminare l'attributo \"{0}\". È utilizzato dalla struttura di navigazione di Sourcing Factor")
   public static final String ATTRIBUTE_DEF_IN_USE = "23";

   @RBEntry("Il nodo seguente è obsoleto: \"{0}\".")
   public static final String NODE_OBSOLETE = "3";

   @RBEntry("Impossibile aggiornare il database. Messaggio della Gestione persistenza: \"{0}\".")
   public static final String CANNOT_REFRESH_DATABASE = "4";

   @RBEntry("Formulario interrogazione non valido.")
   public static final String INVALID_QUERY_FORM = "5";

   @RBEntry("Attribute Definition \"{0}\" is not supported. Only String listener is available.")
   public static final String LISTENER_ATTR_DEF_NOT_SUPPORTED = "6";

   @RBEntry("Impossibile invocare il metodo \"{0}\" per ottenere l'attributo modellato mappato. Il tipo di parametro deve essere vuoto.")
   public static final String LISTENER_CANNOT_GET_MODELED_ATTR1 = "7";

   @RBEntry("Impossibile invocare il metodo \"{0}\" per ottenere l'attributo modellato mappato. Il tipo restituito deve essere Stringa.")
   public static final String LISTENER_CANNOT_GET_MODELED_ATTR2 = "8";

   @RBEntry("Impossibile leggere il nome dell'attributo mappato per la classe  IBAHolder \"{0}\".")
   public static final String LISTENER_CANNOT_READ_MAPPED_ATTR_NAME = "9";
}
