/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.csm.navigation.service;

import wt.util.resource.*;

@RBUUID("wt.csm.navigation.service.serviceResource")
public final class serviceResource extends WTListResourceBundle {
   @RBEntry("The following classification structure is obsolete: \"{0}\".")
   public static final String CLASSIFICATION_STRUCT_OBSOLETE = "0";

   @RBEntry("Name uniqueness violation. The following name already exists: \"{0}\".")
   public static final String NAME_UNIQUENESS_VIOLATION = "1";

   @RBEntry("Cannot retrieve mapped attribute value for IBAHolder class: \"{0}\" .")
   public static final String LISTENER_CANNOT_READ_MAPPED_ATTR_VALUE = "10";

   @RBEntry("Cannot retrieve mapped attribute definition for IBAHolder class: \"{0}\" .")
   public static final String LISTENER_CANNOT_READ_MAPPED_ATTR_DEF = "11";

   @RBEntry("The following error occurred while reading modeled-to-IBA attributes mappings to be listened for: \"{0}\" .")
   public static final String LISTENER_INITIALIZATION = "12";

   @RBEntry("The following classification node is currently in use: \"{0}\".")
   public static final String CLASSIFICATION_NODE__IN_USE = "13";

   @RBEntry("The attribute \"{0}\" has been used in another classification structure.")
   public static final String CLASSIFICATION_ATTRIBUTE_UNIQUENESS = "14";

   @RBEntry("The class \"{0}\" has already been classified. ")
   public static final String CLASS_ALREADY_CLASSIFIED = "15";

   @RBEntry("Name is required.")
   public static final String NULL_NAME_VALUE = "16";

   @RBEntry("Label is required.")
   public static final String NULL_LABEL_VALUE = "17";

   @RBEntry("The classification structure for \"{0}\" is currently in use.")
   public static final String CLASSIFICATION_STRUCT_IN_USE = "18";

   @RBEntry("3")
   @RBPseudo(false)
   @RBComment("do not translate")
   public static final String SEARCH_NAV_NODE_NAME_EXACT = "19";

   @RBEntry("Invalid Parent.")
   public static final String INVALID_PARENT = "2";

   @RBEntry("0")
   @RBPseudo(false)
   @RBComment("do not translate")
   public static final String SEARCH_NAV_NODE_NAME_CONTAINS = "20";

   @RBEntry("1")
   @RBPseudo(false)
   @RBComment("do not translate")
   public static final String SEARCH_NAV_NODE_NAME_BEGINSWITH = "21";

   @RBEntry("2")
   @RBPseudo(false)
   @RBComment("do not translate")
   public static final String SEARCH_NAV_NODE_NAME_ENDSWITH = "22";

   @RBEntry("The Attribute: \"{0}\" cannot be deleted.  It is currently in use by a Sourcing Factor Navigation Structure")
   public static final String ATTRIBUTE_DEF_IN_USE = "23";

   @RBEntry("The following node is obsolete: \"{0}\".")
   public static final String NODE_OBSOLETE = "3";

   @RBEntry("Cannot refresh database.  Message from Persistance Manager: \"{0}\".")
   public static final String CANNOT_REFRESH_DATABASE = "4";

   @RBEntry("Invalid Query Form.")
   public static final String INVALID_QUERY_FORM = "5";

   @RBEntry("Attribute Definition \"{0}\" is not supported. Only String listener is available.")
   public static final String LISTENER_ATTR_DEF_NOT_SUPPORTED = "6";

   @RBEntry("Cannot invoke method \"{0}\" to get mapped modeled attribute. The parameter type must be void.")
   public static final String LISTENER_CANNOT_GET_MODELED_ATTR1 = "7";

   @RBEntry("Cannot invoke method \"{0}\" to get mapped modeled attribute. The return type must be String.")
   public static final String LISTENER_CANNOT_GET_MODELED_ATTR2 = "8";

   @RBEntry("Cannot read mapped attribute name for IBAHolder class \"{0}\" .")
   public static final String LISTENER_CANNOT_READ_MAPPED_ATTR_NAME = "9";
}
