/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.csm.navigation;

import wt.util.resource.*;

@RBUUID("wt.csm.navigation.navigationResource")
public final class navigationResource extends WTListResourceBundle {
   @RBEntry("Incompatible Attribute Definition")
   public static final String INCOMPATIBLE_ATTRIBUTE_DEFINITION = "0";
}
