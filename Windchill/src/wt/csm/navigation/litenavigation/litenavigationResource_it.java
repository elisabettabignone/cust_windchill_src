/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.csm.navigation.litenavigation;

import wt.util.resource.*;

@RBUUID("wt.csm.navigation.litenavigation.litenavigationResource")
public final class litenavigationResource_it extends WTListResourceBundle {
   @RBEntry("Oggetto già esistente")
   public static final String OBJECT_ALREADY_EXISTS = "0";

   @RBEntry("Stato del ciclo di vita non valido: impossibile aggiungere.")
   public static final String INVALID_STATE_FOR_ADD = "1";

   @RBEntry("Oggetto inesistente: impossibile aggiornare.")
   public static final String OBJECT_DOES_NOT_EXIST_FOR_UPDATE = "2";

   @RBEntry("Aggiornamento non riuscito. Le classi dell'oggetto non sono compatibili. Classe 1: \"{0}\" Classe 2: \"{1}\"")
   public static final String UPDATE_FAILED_INCOMPATABLE_CLASSES = "3";

   @RBEntry("Il formulario d'interrogazione è nullo. Impossibile aggiungere.")
   public static final String CANNOT_ADD_NULL_QUERY_FORM = "4";

   @RBEntry("Le definizioni degli attributi non sono equivalenti.")
   public static final String ATTRIBUTE_DEFINITIONS_NOT_EQUAL = "5";

   @RBEntry("Il vincolo non può essere nullo")
   public static final String ERROR_NULL_CONSTRAINT = "6";
}
