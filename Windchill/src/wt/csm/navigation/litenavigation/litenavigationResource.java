/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.csm.navigation.litenavigation;

import wt.util.resource.*;

@RBUUID("wt.csm.navigation.litenavigation.litenavigationResource")
public final class litenavigationResource extends WTListResourceBundle {
   @RBEntry("Object already exists")
   public static final String OBJECT_ALREADY_EXISTS = "0";

   @RBEntry("Invalid state; cannot add.")
   public static final String INVALID_STATE_FOR_ADD = "1";

   @RBEntry("Object does not exist; cannot update.")
   public static final String OBJECT_DOES_NOT_EXIST_FOR_UPDATE = "2";

   @RBEntry("Update failed.  Object classes are not compatable.  Class 1: \"{0}\" Class 2: \"{1}\"")
   public static final String UPDATE_FAILED_INCOMPATABLE_CLASSES = "3";

   @RBEntry("Query Form is null; cannot add.")
   public static final String CANNOT_ADD_NULL_QUERY_FORM = "4";

   @RBEntry("Attribute Definitions are not equal.")
   public static final String ATTRIBUTE_DEFINITIONS_NOT_EQUAL = "5";

   @RBEntry("Constraint cannot be null")
   public static final String ERROR_NULL_CONSTRAINT = "6";
}
