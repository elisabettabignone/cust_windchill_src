/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.csm.ranking.service;

import wt.util.resource.*;

@RBUUID("wt.csm.ranking.service.serviceResource")
public final class serviceResource_it extends WTListResourceBundle {
   @RBEntry("Il contesto di classificazione seguente è obsoleto: \"{0}\".")
   public static final String RANKING_CONTEXT_OBSOLETE = "0";

   @RBEntry("Impossibile aggiornare il database. Messaggio della Gestione persistenza: \"{0}\".")
   public static final String CANNOT_REFRESH_DATABASE = "1";

   @RBEntry("Check-Out impossibile: \"{0}\".")
   public static final String CHECKOUT_FAILED = "2";

   @RBEntry("Il contesto di classificazione seguente è correntemente in uso: \"{0}\".")
   public static final String RANKING_CONTEXT_IN_USE = "3";
}
