/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */

package wt.csm.ranking.literanking;

import java.util.ListResourceBundle;

/**
 * Default literankingResource message resource bundle [English/US]
 *
 */
public class literankingResource_en_US extends ListResourceBundle {



    public Object[][] getContents() {
        return contents;
    }

    static final Object[][] contents =
    {
        {literankingResource.UPDATE_FAILED_INCOMPATABLE_CLASSES,  "Update failed.  Object classes are not compatable.  Class 1: \"{0}\" Class 2: \"{1}\"" },
        {literankingResource.OBJECT_ALREADY_EXISTS,               "Object already exists"                                                                 },
        {literankingResource.INVALID_STATE_FOR_ADD,               "Invalid state; cannot add."                                                            },
        {literankingResource.OBJECT_DOES_NOT_EXIST_FOR_UPDATE,    "Object does not exist; cannot update."},
        {literankingResource.SIMPLE_CRITERIA_LABELS, new String[] {"Never Use", "Not Approved", "Approved", "Preferred"}},
        {literankingResource.SIMPLE_CRITERIA_VALUES, new String[] {"25", "50", "75", "100"}},
  
        {literankingResource.WEIGHTED_AVERAGE_CRITERIA_LABELS, new String[] {"Test1", "Test2", "Test3", "Test4"}},
        {literankingResource.WEIGHTED_AVERAGE_CRITERIA_VALUES, new String[] {"10", "20", "30", "40"}},
        
        {literankingResource.WEIGHTED_AVERAGE_CRITERIA_NORMALIZATION_LABEL, "Normalization"},
        {literankingResource.WEIGHTED_AVERAGE_CRITERIA_NORMALIZATION_VALUE, "100"},
    
    
    
    
    };
}
