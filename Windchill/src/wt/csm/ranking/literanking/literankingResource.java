/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */

package wt.csm.ranking.literanking;

import java.util.ListResourceBundle;

/**
 * Default literankingResource message resource bundle [English/US]
 * 
 */
public class literankingResource extends ListResourceBundle {


    public static final String UPDATE_FAILED_INCOMPATABLE_CLASSES = "0";
    public static final String OBJECT_ALREADY_EXISTS              = "1";
    public static final String INVALID_STATE_FOR_ADD              = "2";
    public static final String OBJECT_DOES_NOT_EXIST_FOR_UPDATE   = "3";
    //choose ranks
    public static final String SIMPLE_CRITERIA_LABELS = "4";
    public static final String SIMPLE_CRITERIA_VALUES = "5";
    public static final String WEIGHTED_AVERAGE_CRITERIA_LABELS = "6";
    public static final String WEIGHTED_AVERAGE_CRITERIA_VALUES = "7";
    public static final String WEIGHTED_AVERAGE_CRITERIA_NORMALIZATION_LABEL = "8";
    public static final String WEIGHTED_AVERAGE_CRITERIA_NORMALIZATION_VALUE = "9";

    public Object[][] getContents() {
        return contents;
    }

    static final Object[][] contents =
    {
        {UPDATE_FAILED_INCOMPATABLE_CLASSES,  "Update failed.  Object classes are not compatable.  Class 1: \"{0}\" Class 2: \"{1}\"" },
        {OBJECT_ALREADY_EXISTS,               "Object already exists"                                                                 },
        {INVALID_STATE_FOR_ADD,               "Invalid state; cannot add."                                                            },
        {OBJECT_DOES_NOT_EXIST_FOR_UPDATE,    "Object does not exist; cannot update."},
        {SIMPLE_CRITERIA_LABELS, new String[] {"Never Use", "Not Approved", "Approved", "Preferred"}},
        {SIMPLE_CRITERIA_VALUES, new String[] {"25", "50", "75", "100"}},
        {WEIGHTED_AVERAGE_CRITERIA_LABELS, new String[] {"Test1", "Test2", "Test3", "Test4"}},
        {WEIGHTED_AVERAGE_CRITERIA_VALUES, new String[] {"10", "20", "30", "40"}},
        
        {WEIGHTED_AVERAGE_CRITERIA_NORMALIZATION_LABEL, "Normalization"},
        {WEIGHTED_AVERAGE_CRITERIA_NORMALIZATION_VALUE, "100"},
        
    };
}
