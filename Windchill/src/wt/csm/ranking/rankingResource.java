/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.csm.ranking;

import wt.util.resource.*;

@RBUUID("wt.csm.ranking.rankingResource")
public final class rankingResource extends WTListResourceBundle {
   @RBEntry("Invalid name: \"{0}\"")
   public static final String INITIALIZE_RC_FAILED = "0";

   @RBEntry("\"{0}\" is required.")
   public static final String MISSING_REQUIRED = "1";
}
