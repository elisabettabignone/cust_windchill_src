/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.csm.query;

import wt.util.resource.*;

@RBUUID("wt.csm.query.QueryRB")
public final class QueryRB extends WTListResourceBundle {
   @RBEntry("Query object is null")
   public static final String EXCEPTION_QUERY_IS_NULL = "ex00";

   @RBEntry("Non string value type for LIKE query")
   public static final String EXCEPTION_LIKE_QUERY_IS_NOT_STRING = "ex01";
}
