/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.fv;

import wt.util.resource.*;

@RBUUID("wt.fv.fvResource")
public final class fvResource extends WTListResourceBundle {
   @RBEntry("Selector already exists")
   public static final String DUPLICATE_SELECTOR = "0";

   @RBEntry("Cannot get vaulting policy")
   public static final String CANT_GET_POLICYITEM = "1";

   @RBEntry("Cannot save: Local mount does not exist")
   public static final String CANT_SAVE_NO_LOC_MOUNT = "2";

   @RBEntry("Cannot save: Cannot find Content Operation Identifier")
   public static final String CANT_SAVE_NO_CONTENT_OP = "3";

   @RBEntry("Cannot save: Vault is read-only")
   public static final String CANT_SAVE_VAULT_RO = "4";

   @RBEntry("Cannot read: Local mount does not exist")
   public static final String CANT_READ_NO_LOC_MOUNT = "5";

   @RBEntry("Cannot read: File \"{0}\" does not exist")
   public static final String CANT_READ_NO_FILE = "6";

   @RBEntry("Cannot delete: Folder contains one or more items")
   public static final String CANT_DELETE_FOLD_HAS_FILES = "7";

   @RBEntry("Cannot mount: Local Mount for folder already exists")
   public static final String CANT_MOUNT_LOC_EXISTS = "8";

   @RBEntry("Cannot mount: Mount for folder already exists on this host")
   public static final String CANT_MOUNT_EXISTS = "9";

   @RBEntry("Cannot unmount: No mounts of folder exist on host")
   public static final String CANT_UNMOUNT_NO_MOUNT = "10";

   @RBEntry("Cannot save: No available Folder found")
   public static final String CANT_SAVE_NO_ACT_FOLDER = "11";

   @RBEntry("Local host is not found in Data Base")
   public static final String CANT_FIND_LOCAL_HOST = "12";

   @RBEntry("IOException loading file vault properties")
   public static final String CANT_LOAD_FV_PROP = "13";

   @RBEntry("Cannot clean up folder: local mount not found")
   public static final String CANT_CLEANUP_NO_MOUNT = "14";

   @RBEntry("Cannot clean up folder: illegal mount")
   public static final String CANT_CLEANUP_ILLEGAL_MOUNT = "15";

   @RBEntry("Cannot perform operation: illegal mount")
   public static final String CANT_ILLEGAL_MOUNT = "16";

   @RBEntry("Cannot save: Folder is probably full")
   public static final String CANT_SAVE_FOLDER_FULL = "17";

   @RBEntry("CleanUp operation failed: check temporary directory permissions")
   public static final String CANT_CLEANUP_IOERROR = "18";

   @RBEntry("Policy Rule for the same content already exists")
   public static final String DUPLICATE_POLICY_RULE = "19";

   @RBEntry("Folder with this name already exists")
   public static final String FOLDER_NAME_IS_NOT_UNIQUE = "20";

   @RBEntry("Vault  with this name already exists")
   public static final String VAULT_NAME_IS_NOT_UNIQUE = "21";

   @RBEntry("Could not find specified host")
   public static final String CANT_FIND_SPEC_HOST = "22";

   @RBEntry("Site with this name already exists")
   public static final String SITE_NAME_IS_NOT_UNIQUE = "23";

   @RBEntry("The local site cannot be removed")
   public static final String CANT_REM_LOCAL_SITE = "24";

   @RBEntry("Cannot remove site with hosts")
   public static final String CANT_REM_SITE_WITH_HOSTS = "25";

   @RBEntry("Cannot remove site with vaults")
   public static final String CANT_REM_SITE_WITH_VAULTS = "26";

   @RBEntry("Host name is not valid")
   public static final String INVALID_HOST_NAME = "27";

   @RBEntry("master")
   public static final String MASTER_SITE_NAME = "28";

   @RBEntry("Can not update the site which has hosts or vaults to a non-replica site.")
   public static final String INVALID_REPLICA_CHANGE = "29";

   @RBEntry("Can not create designated vault for content cache on main site.")
   public static final String CANT_CREATE_MASTERED_VAULT_ON_MASTER = "30";

   @RBEntry("Can not create more than one designated vaults for content cache.")
   public static final String CANT_CREATE_TWO_MASTERED_VAULTS = "31";

   @RBEntry("Cannot perform operation: File Vault is unavailable.")
   public static final String FILE_VAULT_UNAVAILABLE = "32";

   @RBEntry("System property wt.fv.forceContentToVault is set to TRUE. No more than one vault is allowed across all sites.")
   public static final String SINGLE_VAULT_WHEN_FORCED = "33";

   @RBEntry("System property wt.fv.forceContentToVault is set to TRUE. This property may not be set to TRUE if there is more than one local vault across all sites. Please set the property to FALSE or remove the extra vaults.")
   public static final String SINGLE_VAULT_WHEN_CONTENT_FORCED = "34";

   @RBEntry("Default cache vault created")
   public static final String DEFAULT_CACHE_VAULT_CREATED_NAME = "35";

   @RBEntry("Boolean flag to indicate whether default cache vault is created or not")
   public static final String DEFAULT_CACHE_VAULT_CREATED_DESCRIPTION = "36";

   @RBEntry("The system uses this flag to determine whether default cache vault should be created or not. User is not supposed to change the value of this preference.")
   public static final String DEFAULT_CACHE_VAULT_CREATED_LONG_DESCRIPTION = "37";

   @RBEntry("Cannot add a new root folder. Site {0} is not online.")
   @RBArgComment0("Data type=String, Value=name of site")
   public static final String CANT_CREATE_ROOT_FOLDER_SITE_OFFLINE = "38";

   @RBEntry("Cannot add a new root folder. Site {0} is set to read only.")
   @RBArgComment0("Data type=String, Value=name of site")
   public static final String CANT_CREATE_ROOT_FOLDER_SITE_READONLY = "39";

   @RBEntry("Root folder creation failed.")
   public static final String CANT_CREATE_ROOT_FOLDER = "40";

   @RBEntry("Creation of folder for vault {0} failed. Physical file cannot be created.")
   @RBArgComment0("Data type=String, Value=name of vault")
   public static final String CANT_CREATE_AUTO_FOLDER = "41";

   @RBEntry("No vaults found designated for automatic folder creation.")
   public static final String AUTO_VAULTS_NOT_FOUND = "42";

   @RBEntry("More than one vault are designated for automatic folder creation.")
   public static final String SINGLE_AUTO_VAULT_ERROR = "43";

   @RBEntry("The mount folder '{0}' does not exist, or is not a directory.")
   @RBArgComment0("Data type=String, Value=mount path for folder")
   public static final String MOUNT_DOES_NOT_EXIST = "44";

   @RBEntry("New replica root folder not created")
   public static final String REPL_ROOT_FLDR_NOT_CREATED = "45";

   /**
    * Intentionally introducing constant same both on left and right side since the preferences xml file reader for category does not read values in node <csvdisplayName> properly.
    **/
   @RBEntry("Vaulting and Replication")
   @RBComment("This is preference category name for Vaulting and Replication")
   public static final String VAULTING_AND_REPLICATION = "VAULTING_AND_REPLICATION";

   @RBEntry("Vaulting and Replication")
   @RBComment("This is a preference category to groups the preferences related to the Vaulting and Replication")
   public static final String VAULTING_AND_REPLICATION_CATEGORY = "VAULTING_AND_REPLICATION_CATEGORY";

   @RBEntry("Some of the mounts are not valid")
   public static final String INVALID_MOUNT_STATUS = "46";

   @RBEntry("Can not read : Mount is not valid")
   public static final String CAN_NOT_READ_MOUNT_NOT_VALID = "47";

   @RBEntry("System marked one or more mounts as not valid!")
   public static final String NOTIFY_MOUNT_VALIDATION_SUBJECT = "48";

   @RBEntry("System marked one or more mounts as not valid. For more information please see Windchill Administrator's Guide.")
   public static final String NOTIFY_MOUNT_VALIDATION_MSG_BODY = "49";

   @RBEntry("Creation of subfolder below root folder \"{0}\" failed. A physical folder corresponding to the root folder mount path \"{1}\" does not exist.")
   @RBArgComment0("Data type=String, Value=name of Root folder")
   @RBArgComment1("Data type=String, Value=The root folder mount path")
   public static final String CREATE_AUTO_FOLDER_FAILED_NO_ROOT_MT = "50";

   @RBEntry("Updation of mount for root folder \"{0}\" failed. ")
   @RBArgComment0("Data type=String, Value=name of Root folder")
   public static final String UPDATE_ROOT_FOLDER_MOUNT_FAILED = "51";

   @RBEntry("A physical folder corresponding to the root folder mount path \"{0}\" does not exist")
   @RBArgComment0("Data type=String, Value=The root folder mount path")
   public static final String ROOT_FOLDER_MOUNT_FAIL_REASON = "52";

   @RBEntry("Updation of mount for subfolder \"{0}\" failed. A physical folder corresponding to the subfolder mount path \"{1}\" does not exist. ")
   @RBArgComment0("Data type=String,Value=Name of Subfolder")
   @RBArgComment1("Data type=String,Value=The subfolder mount path")
   public static final String UPDATE_AUTO_FOLDER_MOUNT_FAILED = "53";

   @RBEntry("Create mount for root folder \"{0}\" failed.")
   @RBArgComment0("Data type=String,Value=name of Root folder")
   public static final String CREATE_ROOT_MOUNT_FAILED = "54";

   @RBEntry("Creation of subfolder below the root folder mount path \"{0}\" failed.")
   @RBArgComment0("Data type=String,Value=Root folder mount path")
   public static final String CREATE_AUTO_FOLDER_FAILED = "55";

   @RBEntry("Mount path does not exist")
   public static final String MOUNT_PATH_DOES_NOT_EXIST = "56";

   @RBEntry("Mount is duplicate(already in use)")
   public static final String MOUNT_PATH_IS_DUPLICATE = "57";

   @RBEntry("Mount path is invalid")
   public static final String MOUNT_PATH_IS_INVALID = "58";

   @RBEntry("Mount path is not a directory")
   public static final String MOUNT_PATH_IS_NOT_A_DIRECTORY = "59";

   @RBEntry("Mount status cannot be determined")
   public static final String MOUNT_STATUS_CANNOT_BE_DETERMINED = "60";

   @RBEntry("Mount path is read-only")
   public static final String MOUNT_PATH_IS_READ_ONLY = "61";

   @RBEntry("Either there is no primary content, or the primary content is not stored in this system.")
   public static final String NO_PRIMARY_CONTENT_WITHIN_SYSTEM = "62";

   @RBEntry("This file cannot be saved because a file named \"{0}\" already exists.")
   @RBArgComment0("Data type=String,Value=Name of content file in vault")
   public static final String CANT_SAVE_FILE_EXISTS = "63";

   @RBEntry("This file cannot be saved because a file named \"{0}\" does not exist or it is not of an expected size during chunk upload.")
   @RBArgComment0("Data type=String,Value=Name of content file in vault")
   public static final String CANT_SAVE_CHUNK_FILE_DOESNT_EXIST = "64";

   @RBEntry("Cannot save because there was an error while reading the input stream during the save.")
   public static final String CANT_SAVE_IS_READ_ERROR = "65";

   @RBEntry("This file cannot be saved because folder \"{0}\" is read-only.")
   @RBArgComment0("Data type=String,Value=Name of folder")
   public static final String CANT_SAVE_FOLDER_RO = "66";

   @RBEntry("Invalid Rule. Content from the selected domain \"{0}\" cannot be revaulted to the target vault \"{1}\".")
   @RBArgComment0("Data type=String,Value=Domain path")
   @RBArgComment1("Data type=String,Value=Name of vault")
   public static final String INCORRECT_DOMAIN_IN_RULE = "67";

   @RBEntry("Name of the vault")
   public static final String VAULT_DATA_NAME = "68";

   @RBEntry("Status of the vault")
   public static final String VAULT_DATA_STATUS = "69";

   @RBEntry("Name of the vault site")
   public static final String VAULT_DATA_SITE_NAME = "70";

   @RBEntry("Status of the vault site")
   public static final String VAULT_DATA_SITE_STATUS = "71";

   @RBEntry("Vault mount")
   public static final String VAULT_DATA_MOUNT_PATH = "72";

   @RBEntry("Composite type for vault status notification")
   public static final String VAULT_DATA_DESCR = "73";

   @RBEntry("No active folder exists for this vault")
   public static final String NO_ACTIVE_FOLDER = "74";

   @RBEntry("Minimum free space threshold can have a value in the range of 0 to 100")
   public static final String ERR_MIN_DISK_SPACE_THRESHOLD = "75";

   @RBEntry("Free space threshold notification frequency must be greater than zero")
   public static final String ERR_FREQ_HRS = "76";

   @RBEntry("Vault status notification frequency must be greater than zero")
   public static final String ERR_FREQ_MINS = "77";

   @RBEntry("No mounts found for active folder on this vault")
   public static final String NO_MOUNT = "78";

   @RBEntry("An active folder mount for this vault is not valid")
   public static final String INVALID_MOUNT = "79";

   @RBEntry("Some of the mounts are not valid")
   public static final String MOUNTS_NOT_VALID = "80";

   @RBEntry("Could not set site level threshold since it could not be communicated to remote site.")
   public static final String ERR_MSG_SITE_THRESHOLD = "81";

   @RBEntry("Config cache not initialized. Broadcast configuration from main site first")
   public static final String ERR_MSG_MASTER_NOT_FOUND = "82";

   @RBEntry("Alert! Status of vault \"{0}\" is now \"{1}\"")
   @RBArgComment0("Data type=String,Value=Name of vault")
   @RBArgComment1("Data type=String,Value=Status of vault")
   public static final String VAULT_STATUS_NOTIF_MESSAGE = "83";

   @RBEntry("Alert! Free disk space for vault \"{0}\" below threshold")
   @RBArgComment0("Data type=String,Value=Name of vault")
   public static final String VAULT_THRESHOLD_NOTIF_MESSAGE = "84";

   @RBEntry("Composite type for vault threshold reached notification")
   public static final String VAULT_THRESHOLD_REACHED_NOTIF_DATA_DESCR = "85";

   @RBEntry("Percent free disk space threshold")
   public static final String PCT_DISK_FREE_SPACE_THRESH = "86";

   @RBEntry("Percent free disk space on mount")
   public static final String MOUNT_PCT_FREE_DISK_SPACE = "87";

   @RBEntry("Absolute free disk space in GB")
   public static final String MOUNT_ABS_FREE_DISK_SPACE = "88";

   @RBEntry("Total free disk space in GB")
   public static final String MOUNT_ABS_TOTAL_DISK_SPACE = "89";

   @RBEntry("Could not get data from remote site")
   public static final String REMOTE_COMM_FAILED = "90";

   @RBEntry("Mount status")
   public static final String FV_MOUNT_STATUS = "91";

   @RBEntry("Cannot execute immediate run of automatic vault cleanup now, as a scheduled run is already executing.")
   public static final String AUTO_CLEANUP_CANNOT_RUN_IMMEDIATELY = "92";

   @RBEntry("Cleanup started on {0} at {1}:{2} {3}, status ")
   @RBArgComment0("Data type=String,Value=Date in format yyyy-MM-dd")
   @RBArgComment1("Data type=Number,Value=Hours")
   @RBArgComment2("Data type=Number,Value=Minutes")
   @RBArgComment3("Data type=String,Value=Time Zone")
   public static final String CLEANUP_STARTED_LBL1 = "504";

   @RBEntry("Cleanup scheduled for {0} at {1} {2}, status ")
   @RBArgComment0("Data type=String,Value=Date in format yyyy-MM-dd")
   @RBArgComment1("Data type=Number,Value=Hours")
   @RBArgComment2("Data type=Number,Value=Minutes")
   public static final String CLEANUP_SCHEDULED_LBL1 = "505";


   @RBEntry("A cleanup operation is already executing for the site {0} and no new cleanup is initiated.")
   @RBArgComment0("Data type=String,Value=Name of site")
   public static final String SCHED_OP_EXECUTING_CONCURRENTLY = "509";

   @RBEntry("'Ready'")
   public static final String READY_STATE = "512";

   @RBEntry("'Executing'")
   public static final String IN_PROGRESS = "513";

   @RBEntry("'Successfully completed'")
   public static final String AVC_SUCCESS = "506";

   @RBEntry("'Failed'")
   public static final String AVC_FAILURE = "507";

   @RBEntry("ATTENTION: The Cleanup Schedule could not be saved because a cleanup operation is currently in progress.")
   public static final String AVC_CONCURRENT_QE_EXEC = "508";

   @RBEntry("Cannot get vaulting policy for CLASS [{0}], STATE [{1}], DOMAIN [{2}]")
   public static final String CANT_GET_POLICYITEM_2 = "514";

   @RBEntry("STREAMID_SEQ's current value is lesser than StreamId values used in the system. Reset the sequence appropriately.")
   public static final String DUPLICATE_STREAMID_SEQ_ERROR = "515";

   @RBEntry("FILENAMEUSN_SEQ's current value is lesser than UniqueSequenceNumber values used in the system. Reset the sequence appropriately.")
   public static final String DUPLICATE_USN_SEQ_ERROR = "516";

   @RBEntry("This file cannot be saved because the target site \"{0}\" is not online")
   @RBArgComment0("Data type=String,Value=Name of the site")
   public static final String CANT_SAVE_SITE_NOTONLINE = "517";

   @RBEntry("System property wt.fv.forceContentToVault is set to TRUE. There can be only 1 master vault and it should be on the master site. Please set the property to FALSE or add the vault.")
   public static final String SINGLE_VAULT_ON_MASTER_WHEN_CONTENT_FORCED = "518";

   @RBEntry("Some mounts are not valid")
   public static final String SOME_MOUNT_PATHS_NOT_VALID = "519";

   @RBEntry("There are no hosts in the cluster to mount the folder/rootfolder ")
   public static final String NO_CLUSTER_NODES = "520";

}
