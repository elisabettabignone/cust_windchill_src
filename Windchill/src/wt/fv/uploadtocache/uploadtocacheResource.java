/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.fv.uploadtocache;

import wt.util.resource.*;

@RBUUID("wt.fv.uploadtocache.uploadtocacheResource")
public final class uploadtocacheResource extends WTListResourceBundle {
    @RBEntry("Unexpected problem in upload to replica cache process.")
    public static final String ERROR_IN_UPLOAD = "0";

    @RBEntry("Unexpected problem while transfering content from client to replica cache.")
    public static final String ERROR_WHILE_ACCEPTING_CONTENT = "1";

    @RBEntry("Preffered Site is Not Set.")
    public static final String PREFFERED_SITE_IS_NOT_SET = "2";

    @RBEntry("Preffered Site is Not a Replica Site.")
    public static final String PREFFERED_SITE_IS_NOT_REPLICA = "3";

    @RBEntry("Cache Site doesn't have URL.")
    public static final String CACHE_SITE_DOESNT_HAVE_URL = "4";

    @RBEntry("Can not read WTProperties.")
    public static final String CANT_READ_WTPROPERTIES = "5";

    @RBEntry("Can not find My Site URL.")
    public static final String CANT_FIND_MY_SITE_URL = "6";

    @RBEntry("There is no Cache designated vault on preffered Site.")
    public static final String CACHE_VAULT_DOESNT_EXIST = "7";

    @RBEntry("No active folders.")
    public static final String NO_ACTIVE_FOLDER = "8";

    @RBEntry("Local Main Site doesn't exist.")
    public static final String LOCAL_MASTER_SITE_DOESNT_EXIST = "9";

    @RBEntry("There is no Cache designated vault or active folder for Cache designated vault on Local Main Site.")
    public static final String CACHE_VAULT_OR_FOLDER_DOESNT_EXIST_ON_LOCAL_MASTER = "10";

    @RBEntry("Cannot save: Local mount does not exist")
    public static final String CANT_SAVE_NO_LOC_MOUNT = "11";

    @RBEntry("Cannot save: Error while writing file to the folder. ")
    public static final String CANT_SAVE_FOL_WRITE_ERROR = "12";

    @RBEntry("Problem with content file [{0}] detected. Requested main site to place content in quarantine.")
    public static final String FILE_ERROR_REQ_QUARANTINE = "13";

    @RBEntry("Preferred Master Vault is not set or Vault is not Enabled.")
    public static final String PREFFERED_VAULT_IS_NOT_SET = "14";

    @RBEntry("Master Vault [{0}] does not belong to preferred site [{1}].")
    public static final String PREFERRED_VAULT_DO_NOT_BELONG_TO_PREFERRED_SITE = "15";

    @RBEntry("Check sum array length is not correct.")
    public static final String CHECK_SUM_ARR_LEN_INCORRECT = "CHECK_SUM_ARR_LEN_INCORRECT";

    @RBEntry("File sizes array length is not correct.")
    public static final String FILE_SIZES_ARR_LEN_INCORRECT = "FILE_SIZES_ARR_LEN_INCORRECT";

    @RBEntry("Infos array length is not correct.")
    public static final String INFOS_ARR_LEN_INCORRECT = "INFOS_ARR_LEN_INCORRECT";

    @RBEntry("Attempt to access protected previous content without credentials")
    public static final String PREV_CONTENT_DATA_AUTH_FAILED = "PREV_CONTENT_DATA_AUTH_FAILED";
    
    @RBEntry("Unexpected problem in upload.")
    public static final String ERROR_IN_PREV_DATA_FETCH = "ERROR_IN_PREV_DATA_FETCH";
    
    @RBEntry("Site principal does not have access to previous content data. Please contact system administrator.")
    public static final String ACCESS_DENIED_SITE_PRINCIPAL = "ACCESS_DENIED_SITE_PRINCIPAL";
}
