/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.fv.uploadtocache;

import wt.util.resource.*;

@RBUUID("wt.fv.uploadtocache.uploadtocacheResource")
public final class uploadtocacheResource_it extends WTListResourceBundle {
    @RBEntry("Problema durante il processo di caricamento sulla cache di replica")
    public static final String ERROR_IN_UPLOAD = "0";

    @RBEntry("Problema durante il trasferimento di dati dal client alla cache di replica.")
    public static final String ERROR_WHILE_ACCEPTING_CONTENT = "1";

    @RBEntry("Il sito preferito non è stato impostato.")
    public static final String PREFFERED_SITE_IS_NOT_SET = "2";

    @RBEntry("Il sito preferito non è un sito di replica.")
    public static final String PREFFERED_SITE_IS_NOT_REPLICA = "3";

    @RBEntry("Il sito di cache non ha un URL.")
    public static final String CACHE_SITE_DOESNT_HAVE_URL = "4";

    @RBEntry("Impossibile leggere WTProperties.")
    public static final String CANT_READ_WTPROPERTIES = "5";

    @RBEntry("Impossibile trovare l'URL del sito personale.")
    public static final String CANT_FIND_MY_SITE_URL = "6";

    @RBEntry("Non esiste alcun archivio designato per la cache sul sito preferito.")
    public static final String CACHE_VAULT_DOESNT_EXIST = "7";

    @RBEntry("Non esistono cartelle attive.")
    public static final String NO_ACTIVE_FOLDER = "8";

    @RBEntry("Il sito principale locale non esiste.")
    public static final String LOCAL_MASTER_SITE_DOESNT_EXIST = "9";

    @RBEntry("Non esiste alcun archivio designato per la cache o cartella attiva per l'archivio designato per la cache nel sito principale locale.")
    public static final String CACHE_VAULT_OR_FOLDER_DOESNT_EXIST_ON_LOCAL_MASTER = "10";

    @RBEntry("Impossibile salvare: mount locale inesistente.")
    public static final String CANT_SAVE_NO_LOC_MOUNT = "11";

    @RBEntry("Impossibile salvare. Errore durante la scrittura del file nella cartella.")
    public static final String CANT_SAVE_FOL_WRITE_ERROR = "12";

    @RBEntry("Rilevato un problema nel file di contenuto {0}. Presentata richiesta al sito principale di porre il file in quarantena.")
    public static final String FILE_ERROR_REQ_QUARANTINE = "13";

    @RBEntry("Archivio master preferito non impostato o archivio non attivato.")
    public static final String PREFFERED_VAULT_IS_NOT_SET = "14";

    @RBEntry("L'archivio master [{0}] non appartiene al sito preferito [{1}].")
    public static final String PREFERRED_VAULT_DO_NOT_BELONG_TO_PREFERRED_SITE = "15";

    @RBEntry("Lunghezza array checksum non corretta.")
    public static final String CHECK_SUM_ARR_LEN_INCORRECT = "CHECK_SUM_ARR_LEN_INCORRECT";

    @RBEntry("Lunghezza array dimensioni file non corretta.")
    public static final String FILE_SIZES_ARR_LEN_INCORRECT = "FILE_SIZES_ARR_LEN_INCORRECT";

    @RBEntry("Lunghezza array informazioni non corretta.")
    public static final String INFOS_ARR_LEN_INCORRECT = "INFOS_ARR_LEN_INCORRECT";

    @RBEntry("Tentativo di accedere al contenuto precedente protetto senza autorizzazione")
    public static final String PREV_CONTENT_DATA_AUTH_FAILED = "PREV_CONTENT_DATA_AUTH_FAILED";
    
    @RBEntry("Problema imprevisto durante il caricamento.")
    public static final String ERROR_IN_PREV_DATA_FETCH = "ERROR_IN_PREV_DATA_FETCH";
    
    @RBEntry("L'utente/gruppo/ruolo del sito non dispone dei diritti di accesso ai dati del contenuto precedente. Contattare l'amministratore di sistema.")
    public static final String ACCESS_DENIED_SITE_PRINCIPAL = "ACCESS_DENIED_SITE_PRINCIPAL";
}
