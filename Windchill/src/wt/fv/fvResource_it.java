/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.fv;

import wt.util.resource.*;

@RBUUID("wt.fv.fvResource")
public final class fvResource_it extends WTListResourceBundle {
   @RBEntry("Selettore già esistente.")
   public static final String DUPLICATE_SELECTOR = "0";

   @RBEntry("Impossibile ottenere regola di archiviazione")
   public static final String CANT_GET_POLICYITEM = "1";

   @RBEntry("Impossibile salvare: mount locale inesistente.")
   public static final String CANT_SAVE_NO_LOC_MOUNT = "2";

   @RBEntry("Impossibile salvare. L'identificatore contenuto operazione non è stato trovato.")
   public static final String CANT_SAVE_NO_CONTENT_OP = "3";

   @RBEntry("Impossibile salvare. L'archivio è di sola lettura.")
   public static final String CANT_SAVE_VAULT_RO = "4";

   @RBEntry("Impossibile leggere. Il mount locale non esiste.")
   public static final String CANT_READ_NO_LOC_MOUNT = "5";

   @RBEntry("Impossibile leggere. Il file \"{0}\" non esiste.")
   public static final String CANT_READ_NO_FILE = "6";

   @RBEntry("Impossibile eliminare: la cartella contiene uno o più elementi")
   public static final String CANT_DELETE_FOLD_HAS_FILES = "7";

   @RBEntry("Impossibile montare. Il mount locale per la cartella esiste già.")
   public static final String CANT_MOUNT_LOC_EXISTS = "8";

   @RBEntry("Impossibile montare. Il mount per la cartella esiste già su questo host.")
   public static final String CANT_MOUNT_EXISTS = "9";

   @RBEntry("Impossibile smontare. Nessun mount di cartella esistente sull'host.")
   public static final String CANT_UNMOUNT_NO_MOUNT = "10";

   @RBEntry("Impossibile salvare. Nessuna cartella disponibile. ")
   public static final String CANT_SAVE_NO_ACT_FOLDER = "11";

   @RBEntry("Impossibile trovare l'host locale nel database.")
   public static final String CANT_FIND_LOCAL_HOST = "12";

   @RBEntry("Eccezione di input/output durante il caricamento delle proprietà dell'archivio file. ")
   public static final String CANT_LOAD_FV_PROP = "13";

   @RBEntry("Impossibile eseguire la pulizia della cartella. Il mount locale non è stato trovato.")
   public static final String CANT_CLEANUP_NO_MOUNT = "14";

   @RBEntry("Impossibile eseguire la pulizia della cartella. Mount non valido")
   public static final String CANT_CLEANUP_ILLEGAL_MOUNT = "15";

   @RBEntry("Impossibile eseguire l'operazione. Mount non valido.")
   public static final String CANT_ILLEGAL_MOUNT = "16";

   @RBEntry("Impossibile salvare. La cartella è probabilmente piena.")
   public static final String CANT_SAVE_FOLDER_FULL = "17";

   @RBEntry("L'operazione di pulizia non è riuscita. Controllare i permessi di directory temporanei.")
   public static final String CANT_CLEANUP_IOERROR = "18";

   @RBEntry("Una regola per lo stesso contenuto esiste già")
   public static final String DUPLICATE_POLICY_RULE = "19";

   @RBEntry("Una cartella con questo nome esiste già")
   public static final String FOLDER_NAME_IS_NOT_UNIQUE = "20";

   @RBEntry("Un archivio con questo nome esiste già")
   public static final String VAULT_NAME_IS_NOT_UNIQUE = "21";

   @RBEntry("Impossibile trovare l'host specificato")
   public static final String CANT_FIND_SPEC_HOST = "22";

   @RBEntry("Un sito con questo nome esiste già")
   public static final String SITE_NAME_IS_NOT_UNIQUE = "23";

   @RBEntry("Il sito locale non può essere rimosso")
   public static final String CANT_REM_LOCAL_SITE = "24";

   @RBEntry("Impossibile rimuovere il sito con gli host")
   public static final String CANT_REM_SITE_WITH_HOSTS = "25";

   @RBEntry("Impossibile rimuovere il sito con gli archivi")
   public static final String CANT_REM_SITE_WITH_VAULTS = "26";

   @RBEntry("Nome host invalido")
   public static final String INVALID_HOST_NAME = "27";

   @RBEntry("master")
   public static final String MASTER_SITE_NAME = "28";

   @RBEntry("Impossibile trasformare un sito con host e archivi in un sito non-replica.")
   public static final String INVALID_REPLICA_CHANGE = "29";

   @RBEntry("Impossibile creare l'archivio designato per la cache di contenuto sul sito principale.")
   public static final String CANT_CREATE_MASTERED_VAULT_ON_MASTER = "30";

   @RBEntry("Impossibile designare più di un archivio per la cache di contenuto.")
   public static final String CANT_CREATE_TWO_MASTERED_VAULTS = "31";

   @RBEntry("Impossibile eseguire l'operazione: l'archivio file non è disponibile.")
   public static final String FILE_VAULT_UNAVAILABLE = "32";

   @RBEntry("La proprietà di sistema wt.fv.forceContentToVault è impostata su TRUE. È ammesso un solo archivio per l'insieme dei siti.")
   public static final String SINGLE_VAULT_WHEN_FORCED = "33";

   @RBEntry("La proprietà di sistema wt.fv.forceContentToVault è impostata su TRUE. L'impostazione non è valida se esiste più di un archivio locale nell'insieme dei siti. Impostare la proprietà su FALSE o rimuovere gli archivi in eccesso.")
   public static final String SINGLE_VAULT_WHEN_CONTENT_FORCED = "34";

   @RBEntry("Creazione archivio cache di default completata")
   public static final String DEFAULT_CACHE_VAULT_CREATED_NAME = "35";

   @RBEntry("Flag booleano che indica se deve essere creato l'archivio cache di default")
   public static final String DEFAULT_CACHE_VAULT_CREATED_DESCRIPTION = "36";

   @RBEntry("Il sistema utilizza questo flag per determinare se deve essere creato o meno l'archivio cache di default. Si consiglia agli utenti di non modificare il valore di questa preferenza.")
   public static final String DEFAULT_CACHE_VAULT_CREATED_LONG_DESCRIPTION = "37";

   @RBEntry("Impossibile aggiungere una nuova cartella radice. Il sito {0} non è in linea.")
   @RBArgComment0("Data type=String, Value=name of site")
   public static final String CANT_CREATE_ROOT_FOLDER_SITE_OFFLINE = "38";

   @RBEntry("Impossibile aggiungere una nuova cartella radice. Il sito {0} è di sola lettura.")
   @RBArgComment0("Data type=String, Value=name of site")
   public static final String CANT_CREATE_ROOT_FOLDER_SITE_READONLY = "39";

   @RBEntry("Creazione della cartella radice non riuscita.")
   public static final String CANT_CREATE_ROOT_FOLDER = "40";

   @RBEntry("Creazione della cartella per l'archivio {0} non riuscita. Impossibile creare il file fisico.")
   @RBArgComment0("Data type=String, Value=name of vault")
   public static final String CANT_CREATE_AUTO_FOLDER = "41";

   @RBEntry("Nessun archivio designato per la creazione automatica della cartella.")
   public static final String AUTO_VAULTS_NOT_FOUND = "42";

   @RBEntry("Più archivi designati per la creazione automatica della cartella.")
   public static final String SINGLE_AUTO_VAULT_ERROR = "43";

   @RBEntry("La cartella di mount '{0}' non esiste o non è una directory.")
   @RBArgComment0("Data type=String, Value=mount path for folder")
   public static final String MOUNT_DOES_NOT_EXIST = "44";

   @RBEntry("Creazione nuova cartella radice di replica non riuscita")
   public static final String REPL_ROOT_FLDR_NOT_CREATED = "45";

   /**
    * Intentionally introducing constant same both on left and right side since the preferences xml file reader for category does not read values in node <csvdisplayName> properly.
    **/
   @RBEntry("Vaulting and Replication")
   @RBComment("This is preference category name for Vaulting and Replication")
   public static final String VAULTING_AND_REPLICATION = "VAULTING_AND_REPLICATION";

   @RBEntry("Vaulting and Replication")
   @RBComment("This is a preference category to groups the preferences related to the Vaulting and Replication")
   public static final String VAULTING_AND_REPLICATION_CATEGORY = "VAULTING_AND_REPLICATION_CATEGORY";

   @RBEntry("Alcuni mount non sono validi")
   public static final String INVALID_MOUNT_STATUS = "46";

   @RBEntry("Impossibile leggere: il mount non è valido")
   public static final String CAN_NOT_READ_MOUNT_NOT_VALID = "47";

   @RBEntry("Il sistema ha contrassegnato uno o più mount come non validi.")
   public static final String NOTIFY_MOUNT_VALIDATION_SUBJECT = "48";

   @RBEntry("Il sistema ha contrassegnato uno o più mount come non validi. Per ulteriori informazioni consultare la Guida dell'amministratore di Windchill.")
   public static final String NOTIFY_MOUNT_VALIDATION_MSG_BODY = "49";

   @RBEntry("Creazione di una cartella secondaria sotto la cartella radice \"{0}\" non riuscita. Non esiste una cartella fisica corrispondente al percorso mount della cartella radice \"{1}\".")
   @RBArgComment0("Data type=String, Value=name of Root folder")
   @RBArgComment1("Data type=String, Value=The root folder mount path")
   public static final String CREATE_AUTO_FOLDER_FAILED_NO_ROOT_MT = "50";

   @RBEntry("Aggiornamento mount per la cartella radice \"{0}\" non riuscito. ")
   @RBArgComment0("Data type=String, Value=name of Root folder")
   public static final String UPDATE_ROOT_FOLDER_MOUNT_FAILED = "51";

   @RBEntry("Non esiste una cartella fisica corrispondente al percorso mount della cartella radice \"{0}\".")
   @RBArgComment0("Data type=String, Value=The root folder mount path")
   public static final String ROOT_FOLDER_MOUNT_FAIL_REASON = "52";

   @RBEntry("Aggiornamento mount per la cartella secondaria \"{0}\" non riuscito. Non esiste una cartella fisica corrispondente al percorso mount della cartella secondaria \"{1}\".")
   @RBArgComment0("Data type=String,Value=Name of Subfolder")
   @RBArgComment1("Data type=String,Value=The subfolder mount path")
   public static final String UPDATE_AUTO_FOLDER_MOUNT_FAILED = "53";

   @RBEntry("Creazione mount per la cartella radice \"{0}\" non riuscita. ")
   @RBArgComment0("Data type=String,Value=name of Root folder")
   public static final String CREATE_ROOT_MOUNT_FAILED = "54";

   @RBEntry("Creazione cartella secondaria sotto il percorso mount per la cartella radice \"{0}\" non riuscita. ")
   @RBArgComment0("Data type=String,Value=Root folder mount path")
   public static final String CREATE_AUTO_FOLDER_FAILED = "55";

   @RBEntry("Il percorso mount non esiste")
   public static final String MOUNT_PATH_DOES_NOT_EXIST = "56";

   @RBEntry("Mount duplicato (già in uso)")
   public static final String MOUNT_PATH_IS_DUPLICATE = "57";

   @RBEntry("Il percorso mount non è valido")
   public static final String MOUNT_PATH_IS_INVALID = "58";

   @RBEntry("Il percorso mount non è una directory.")
   public static final String MOUNT_PATH_IS_NOT_A_DIRECTORY = "59";

   @RBEntry("Impossibile determinare lo stato del mount")
   public static final String MOUNT_STATUS_CANNOT_BE_DETERMINED = "60";

   @RBEntry("Il percorso mount è di sola lettura")
   public static final String MOUNT_PATH_IS_READ_ONLY = "61";

   @RBEntry("Contenuto principale inesistente o non memorizzato sul sistema.")
   public static final String NO_PRIMARY_CONTENT_WITHIN_SYSTEM = "62";

   @RBEntry("Impossibile salvare il file. Un file di nome {0} esiste già.")
   @RBArgComment0("Data type=String,Value=Name of content file in vault")
   public static final String CANT_SAVE_FILE_EXISTS = "63";

   @RBEntry("Impossibile salvare il file. Non esiste un file di nome {0} o non è delle dimensioni attese durante il caricamento di blocco.")
   @RBArgComment0("Data type=String,Value=Name of content file in vault")
   public static final String CANT_SAVE_CHUNK_FILE_DOESNT_EXIST = "64";

   @RBEntry("Impossibile salvare. Si è verificato un errore durante la lettura del flusso di input in fase di salvataggio.")
   public static final String CANT_SAVE_IS_READ_ERROR = "65";

   @RBEntry("Impossibile salvare il file. La cartella {0} è di sola lettura.")
   @RBArgComment0("Data type=String,Value=Name of folder")
   public static final String CANT_SAVE_FOLDER_RO = "66";

   @RBEntry("Regola non valida. Il contenuto proveniente dal dominio \"{0}\" selezionato non può essere riarchiviato nell'archivio di destinazione \"{1}\".")
   @RBArgComment0("Data type=String,Value=Domain path")
   @RBArgComment1("Data type=String,Value=Name of vault")
   public static final String INCORRECT_DOMAIN_IN_RULE = "67";

   @RBEntry("Nome dell'archivio")
   public static final String VAULT_DATA_NAME = "68";

   @RBEntry("Stato dell'archivio")
   public static final String VAULT_DATA_STATUS = "69";

   @RBEntry("Nome del sito dell'archivio")
   public static final String VAULT_DATA_SITE_NAME = "70";

   @RBEntry("Stato del sito dell'archivio")
   public static final String VAULT_DATA_SITE_STATUS = "71";

   @RBEntry("Mount archivio")
   public static final String VAULT_DATA_MOUNT_PATH = "72";

   @RBEntry("Tipo composito per notifica stato archivio")
   public static final String VAULT_DATA_DESCR = "73";

   @RBEntry("Non esistono cartelle attive per questo archivio")
   public static final String NO_ACTIVE_FOLDER = "74";

   @RBEntry("La soglia dello spazio libero minimo può avere un valore compreso tra 0 e 100")
   public static final String ERR_MIN_DISK_SPACE_THRESHOLD = "75";

   @RBEntry("La frequenza di notifica della soglia di spazio libero deve essere maggiore di zero")
   public static final String ERR_FREQ_HRS = "76";

   @RBEntry("La frequenza di notifica dello stato dell'archivio deve essere maggiore di zero")
   public static final String ERR_FREQ_MINS = "77";

   @RBEntry("Non esistono mount per la cartella attiva su questo archivio")
   public static final String NO_MOUNT = "78";

   @RBEntry("Il mount di una cartella attiva per questo archivio non è valido")
   public static final String INVALID_MOUNT = "79";

   @RBEntry("Alcuni mount non sono validi")
   public static final String MOUNTS_NOT_VALID = "80";

   @RBEntry("Impossibile impostare la soglia del livello del sito perché non è stato possibile comunicare con il sito remoto.")
   public static final String ERR_MSG_SITE_THRESHOLD = "81";

   @RBEntry("Cache Config non inizializzata. Trasmettere prima la configurazione dal sito principale")
   public static final String ERR_MSG_MASTER_NOT_FOUND = "82";

   @RBEntry("Avviso: lo stato dell'archivio \"{0}\" è ora \"{1}\"")
   @RBArgComment0("Data type=String,Value=Name of vault")
   @RBArgComment1("Data type=String,Value=Status of vault")
   public static final String VAULT_STATUS_NOTIF_MESSAGE = "83";

   @RBEntry("Avviso: lo spazio libero su disco per l'archivio \"{0}\" è inferiore alla soglia")
   @RBArgComment0("Data type=String,Value=Name of vault")
   public static final String VAULT_THRESHOLD_NOTIF_MESSAGE = "84";

   @RBEntry("Notifica di raggiungimento soglia da parte del tipo composito per archivio")
   public static final String VAULT_THRESHOLD_REACHED_NOTIF_DATA_DESCR = "85";

   @RBEntry("Percentuale soglia spazio su disco libero")
   public static final String PCT_DISK_FREE_SPACE_THRESH = "86";

   @RBEntry("Percentuale spazio su disco libero su mount")
   public static final String MOUNT_PCT_FREE_DISK_SPACE = "87";

   @RBEntry("Spazio su disco libero assoluto in GB")
   public static final String MOUNT_ABS_FREE_DISK_SPACE = "88";

   @RBEntry("Spazio su disco libero totale in GB")
   public static final String MOUNT_ABS_TOTAL_DISK_SPACE = "89";

   @RBEntry("Impossibile ottenere dati dal sito remoto")
   public static final String REMOTE_COMM_FAILED = "90";

   @RBEntry("Stato mount")
   public static final String FV_MOUNT_STATUS = "91";

   @RBEntry("Impossibile avviare l'esecuzione immediata della pulizia automatica dell'archivio. Un'esecuzione programmata è già in corso.")
   public static final String AUTO_CLEANUP_CANNOT_RUN_IMMEDIATELY = "92";

   @RBEntry("Pulizia iniziata il {0} alle {1}.{2} {3}. Stato: ")
   @RBArgComment0("Data type=String,Value=Date in format yyyy-MM-dd")
   @RBArgComment1("Data type=Number,Value=Hours")
   @RBArgComment2("Data type=Number,Value=Minutes")
   @RBArgComment3("Data type=String,Value=Time Zone")
   public static final String CLEANUP_STARTED_LBL1 = "504";

   @RBEntry("Pulizia programmata per il {0} alle {1}.{2}. Stato: ")
   @RBArgComment0("Data type=String,Value=Date in format yyyy-MM-dd")
   @RBArgComment1("Data type=Number,Value=Hours")
   @RBArgComment2("Data type=Number,Value=Minutes")
   public static final String CLEANUP_SCHEDULED_LBL1 = "505";


   @RBEntry("Per il sito {0} è già in corso un'operazione di pulizia. Non è stata avviata una nuova pulizia.")
   @RBArgComment0("Data type=String,Value=Name of site")
   public static final String SCHED_OP_EXECUTING_CONCURRENTLY = "509";

   @RBEntry("'Pronto'")
   public static final String READY_STATE = "512";

   @RBEntry("'In esecuzione'")
   public static final String IN_PROGRESS = "513";

   @RBEntry("'Operazione completata'")
   public static final String AVC_SUCCESS = "506";

   @RBEntry("'Operazione non riuscita'")
   public static final String AVC_FAILURE = "507";

   @RBEntry("ATTENZIONE: operazione di pulizia in corso. Impossibile salvare la programmazione.")
   public static final String AVC_CONCURRENT_QE_EXEC = "508";

   @RBEntry("Impossibile ottenere regola di archiviazione per CLASS [{0}], STATE [{1}], DOMAIN [{2}]")
   public static final String CANT_GET_POLICYITEM_2 = "514";

   @RBEntry("Il valore corrente di STREAMID_SEQ è inferiore ai valori StreamId utilizzati nel sistema. Reimpostare la sequenza.")
   public static final String DUPLICATE_STREAMID_SEQ_ERROR = "515";

   @RBEntry("Il valore corrente di FILENAMEUSN_SEQ è inferiore ai valori UniqueSequenceNumber utilizzati nel sistema. Reimpostare la sequenza.")
   public static final String DUPLICATE_USN_SEQ_ERROR = "516";

   @RBEntry("Impossibile salvare il file. Il sito di destinazione \"{0}\" non è in linea")
   @RBArgComment0("Data type=String,Value=Name of the site")
   public static final String CANT_SAVE_SITE_NOTONLINE = "517";

   @RBEntry("La proprietà di sistema wt.fv.forceContentToVault è impostata su TRUE. È consentita la presenza di un solo archivio master nel sito master. Impostare la proprietà su FALSE o aggiungere l'archivio.")
   public static final String SINGLE_VAULT_ON_MASTER_WHEN_CONTENT_FORCED = "518";

   @RBEntry("Alcuni mount non sono validi")
   public static final String SOME_MOUNT_PATHS_NOT_VALID = "519";

   @RBEntry("Non vi sono host nel cluster per montare la cartella/cartella radice ")
   public static final String NO_CLUSTER_NODES = "520";

}
