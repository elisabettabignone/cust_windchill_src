/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.fv.replica;

import wt.util.resource.*;

@RBUUID("wt.fv.replica.replicaResource")
public final class replicaResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile trovare il file")
   public static final String FILE_NOT_FOUND = "1";

   @RBEntry("Impossibile trovare la cartella")
   public static final String FOLDER_NOT_FOUND = "2";

   @RBEntry("Impossibile trovare l'archivio")
   public static final String VAULT_NOT_FOUND = "3";

   @RBEntry("Impossibile creare la cache Config")
   public static final String NO_CONFIG = "4";

   @RBEntry("Le informazioni sulla configurazione non sono aggiornate o non sono disponibili")
   public static final String BAD_CONFIG = "5";

   @RBEntry("Tentativo di accedere al contenuto protetto senza autorizzazione")
   public static final String AUTHENTICATION_FAILED = "6";

   @RBEntry("Scrittura nel file {0} completata. Scritti {1} byte.")
   public static final String SUCCESS_WRITE = "7";

   @RBEntry("Errore durante la creazione del file nella cartella nel percorso [{0}]. La cartella viene contrassegnata per la sola scrittura.")
   public static final String ERROR_CREATING_FILE = "8";

   @RBEntry("Errore durante la scrittura nella cartella nel percorso [{0}]. È possibile che lo spazio su disco non sia sufficiente. La cartella viene contrassegnata per la sola scrittura.")
   public static final String ERROR_WRITING_TO_FILE = "9";

   @RBEntry("Errore durante la chiusura stream nella cartella nel percorso [{0}]. È possibile che lo spazio su disco non sia sufficiente. La cartella viene contrassegnata per la sola scrittura.")
   public static final String ERROR_CLOSING_STREAM = "10";

   @RBEntry("Rilevato un problema nel file di contenuto {0}. Presentata richiesta al sito principale di porre il file in quarantena.")
   public static final String FILE_ERROR_REQ_QUARANTINE = "11";
   
   @RBEntry("Impossibile trovare le informazioni sul numero di file della cartella")
   public static final String FOLDER_FILE_COUNT_INFO_NOT_FOUND = "FOLDER_FILE_COUNT_INFO_NOT_FOUND";
}
