/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.fv.replica;

import wt.util.resource.*;

@RBUUID("wt.fv.replica.replicaResource")
public final class replicaResource extends WTListResourceBundle {
   @RBEntry("File not found")
   public static final String FILE_NOT_FOUND = "1";

   @RBEntry("Folder not found")
   public static final String FOLDER_NOT_FOUND = "2";

   @RBEntry("Vault not found")
   public static final String VAULT_NOT_FOUND = "3";

   @RBEntry("Unable to create Config Cache")
   public static final String NO_CONFIG = "4";

   @RBEntry("Configuration info is either out of date or unavailable")
   public static final String BAD_CONFIG = "5";

   @RBEntry("Attempt to access protected content withouth credentials")
   public static final String AUTHENTICATION_FAILED = "6";

   @RBEntry("Successfully wrote to file {0}. Total number of bytes written: {1}")
   public static final String SUCCESS_WRITE = "7";

   @RBEntry("Error creating new file in folder with path [{0}]. Marking folder read only")
   public static final String ERROR_CREATING_FILE = "8";

   @RBEntry("Error writing to folder with path [{0}]. Disk might not have enough space. Marking folder read only")
   public static final String ERROR_WRITING_TO_FILE = "9";

   @RBEntry("Error closing stream in folder with path [{0}]. Disk might not have enough space. Marking folder read only")
   public static final String ERROR_CLOSING_STREAM = "10";

   @RBEntry("Problem with content file [{0}] detected. Requested main site to place content in quarantine.")
   public static final String FILE_ERROR_REQ_QUARANTINE = "11";
   
   @RBEntry("File count information for the folder not found")
   public static final String FOLDER_FILE_COUNT_INFO_NOT_FOUND = "FOLDER_FILE_COUNT_INFO_NOT_FOUND";
}
