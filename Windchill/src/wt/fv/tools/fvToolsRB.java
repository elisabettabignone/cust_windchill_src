/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.fv.tools;

import wt.util.resource.*;

@RBUUID("wt.fv.tools.fvToolsRB")
@RBNameException //Grandfathered by conversion
public final class fvToolsRB extends WTListResourceBundle {
   @RBEntry("All command line arguments are optional, and if no arguments are supplied, the system runs in the following way:")
   public static final String CMDLINE_ARGS_OPTIONAL = "0";

   @RBEntry("  All system content is checked for internal integrity.  All vaults and folders are checked for missing or incorrectly sized files")
   public static final String ALL_SYS_CONTENT = "1";

   @RBEntry("List of valid arguments:")
   public static final String LIST_VALID_ARGS = "2";

   @RBEntry("User Id of the Administrator user")
   public static final String USER_ID_DESC = "3";

   @RBEntry("Password of the Administrator user")
   public static final String PASSWD_DESC = "4";

   @RBEntry("Operate in repair mode. Needs inputFile parameter to be specified as well")
   public static final String FIX_DESC = "5";

   @RBEntry("Use the file specified in inputFile parameter to delete duplicates")
   public static final String INPUT_FILE_DESC = "6";

   @RBEntry("Use the directory path specified in outputPath parameter to save the xml output files")
   public static final String OP_PATH_DESC = "7";

   @RBEntry("Asks user for confirmation before deleting each file")
   public static final String CONFIRM_DESC = "8";

   @RBEntry("Print list of valid arguments and exit")
   public static final String USAGE_DESC = "9";

   @RBEntry("This item has not been deleted. At least one primary content item needs to be associated with a content holder")
   public static final String AT_LEAST_ONE_PRIMARY = "10";

   @RBEntry("Argument is invalid: {0}")
   public static final String ARG_INVALID = "11";

   @RBEntry("Please run with -usage option for a list of valid command line arguments.")
   public static final String RUN_USAGE = "12";

   @RBEntry("If the parameter -fix is specified, the inputFile must also be specified")
   public static final String FIX_PARAM_DEPENDENCY = "13";

   @RBEntry("Setting output directory path.  Cannot determine path to <WT_HOME>\\logs directory!")
   public static final String OP_DIR_PATH_NOT_FOUND = "14";

   @RBEntry("Output storage path: {0}")
   public static final String OP_STORAGE_PATH = "15";

   @RBEntry("Please enter a valid administrator username/password combination")
   public static final String VALID_ADMIN_USER_PASS = "16";

   @RBEntry("Cannot access Windchill server, please ensure that a Windchill server is running")
   public static final String CANNOT_ACCESS_WC_SERVER = "17";

   @RBEntry("Output directory path specified in the arguments does not exist: {0}")
   public static final String OP_DIR_DOES_NOT_EXIST = "18";

   @RBEntry("Output directory path specified in the arguments does not point to a valid directory: {0}")
   public static final String OP_DIR_NOT_VALID = "19";

   @RBEntry("Input file specified in the arguments does not exist: {0}")
   public static final String IP_FILE_DOES_NOT_EXIST = "20";

   @RBEntry("Input file specified in the arguments does not point to a valid directory: {0}")
   public static final String IP_FILE_NOT_VALID = "21";

   @RBEntry("This tool can only be run by a user in the Administrators group; please relogin with an administrator username and password")
   public static final String ONLY_BY_ADMIN = "22";
}