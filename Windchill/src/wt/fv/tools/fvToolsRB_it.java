/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.fv.tools;

import wt.util.resource.*;

@RBUUID("wt.fv.tools.fvToolsRB")
@RBNameException //Grandfathered by conversion
public final class fvToolsRB_it extends WTListResourceBundle {
   @RBEntry("Gli argomenti della riga di comando sono opzionali. Se non vengono specificati argomenti, l'esecuzione avviene secondo la modalità che segue:")
   public static final String CMDLINE_ARGS_OPTIONAL = "0";

   @RBEntry("  Il contenuto dell'intero sistema viene controllato per verificarne l'integrità interna. Tutti gli archivi e le cartelle vengono controllati per rilevare la presenza di file mancanti o di dimensione errata.")
   public static final String ALL_SYS_CONTENT = "1";

   @RBEntry("Elenco degli argomenti validi:")
   public static final String LIST_VALID_ARGS = "2";

   @RBEntry("ID utente dell'amministratore")
   public static final String USER_ID_DESC = "3";

   @RBEntry("Password dell'amministratore")
   public static final String PASSWD_DESC = "4";

   @RBEntry("Funziona in modalità riparazione. È necessario specificare anche il parametro inputFile")
   public static final String FIX_DESC = "5";

   @RBEntry("Usa il file specificato nel parametro inputFile per eliminare i duplicati")
   public static final String INPUT_FILE_DESC = "6";

   @RBEntry("Usa il percorso directory specificato nel parametro outputPath per salvare i file xml di output")
   public static final String OP_PATH_DESC = "7";

   @RBEntry("Chiede conferma all'utente prima di eliminare ciascun file.")
   public static final String CONFIRM_DESC = "8";

   @RBEntry("Stampa l'elenco degli argomenti validi ed esce")
   public static final String USAGE_DESC = "9";

   @RBEntry("L'elemento non è stato eliminato. È necessario che almeno un elemento di contenuto principale sia associato a un contenitore.")
   public static final String AT_LEAST_ONE_PRIMARY = "10";

   @RBEntry("Argomento non valido: {0}")
   public static final String ARG_INVALID = "11";

   @RBEntry("Eseguire con l'opzione -usage per l'elenco degli argomenti della riga di comando validi.")
   public static final String RUN_USAGE = "12";

   @RBEntry("Se si specifica il parametro -fix, è necessario specificare anche inputFile")
   public static final String FIX_PARAM_DEPENDENCY = "13";

   @RBEntry("Impostazione percorso directory di output. Impossibile determinare il percorso alla directory <WT_HOME>\\logs")
   public static final String OP_DIR_PATH_NOT_FOUND = "14";

   @RBEntry("Percorso di archiviazione output: {0}")
   public static final String OP_STORAGE_PATH = "15";

   @RBEntry("Immettere una combinazione nome utente/password amministratore valida")
   public static final String VALID_ADMIN_USER_PASS = "16";

   @RBEntry("Impossibile accedere al server Windchill. Assicurarsi che sia in esecuzione.")
   public static final String CANNOT_ACCESS_WC_SERVER = "17";

   @RBEntry("Il percorso directory di output specificato negli argomenti non esiste: {0}")
   public static final String OP_DIR_DOES_NOT_EXIST = "18";

   @RBEntry("Il percorso directory di output specificato negli argomenti non punta a una directory valida: {0}")
   public static final String OP_DIR_NOT_VALID = "19";

   @RBEntry("Il file di input specificato negli argomenti non esiste: {0}")
   public static final String IP_FILE_DOES_NOT_EXIST = "20";

   @RBEntry("Il file di input specificato negli argomenti non punta a una directory valida: {0}")
   public static final String IP_FILE_NOT_VALID = "21";

   @RBEntry("Lo strumento può essere eseguito solo da un utente del gruppo amministratori. Eseguire nuovamente il login come utente amministratore.")
   public static final String ONLY_BY_ADMIN = "22";
}
