/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.fv.master;

import wt.util.resource.*;

@RBUUID("wt.fv.master.masterResource")
public final class masterResource extends WTListResourceBundle {
   @RBEntry("Unable to determine the local address.")
   public static final String LOCAL_URL_NOT_FOUND = "0";

   @RBEntry("Try this, if no luck, try the next.")
   public static final String PREFERRED_SITE_STRING = "1";

   @RBEntry("Download from the main site.")
   public static final String MASTER_SITE_STRING = "2";
   
   @RBEntry("Multiple Choices")
   public static final String MULTIPLE_CHOICES = "MULTIPLE_CHOICES";

   @RBEntry("No main site found.")
   public static final String NO_MASTER_SITE = "3";

   @RBEntry("Site not found")
   public static final String SITE_NOT_FOUND = "4";

   @RBEntry("Vault with this name already exists")
   public static final String VAULT_NAME_IS_NOT_UNIQUE = "5";

   @RBEntry("Folder with this name already exists")
   public static final String FOLDER_NAME_IS_NOT_UNIQUE = "6";

   @RBEntry("Site with this name already exists")
   public static final String SITE_NAME_IS_NOT_UNIQUE = "7";

   @RBEntry("Could not create inbox")
   public static final String INBOX_CREATION_FAILED = "8";

   @RBEntry("Preferred Content Cache Site:")
   public static final String PREFERRED_SITE_LABEL = "9";

   @RBEntry("Windchill Master Site:")
   public static final String MASTER_SITE_LABEL = "10";

   @RBEntry("Content Cache Site Preference for ")
   public static final String PREF_CLIENT_TITLE = "11";

   @RBEntry("You succeeded in setting the preferred site as ")
   public static final String SUCCESS_MESSAGE = "12";

   @RBEntry("Success")
   public static final String SUCCESS_TITLE = "13";

   @RBEntry("Can not remove Replica folder, it contains Cached items.")
   public static final String CANT_REMOVE_REPLICA_FOLDER = "14";

   @RBEntry("**** This is probably due to the Site name change by the Site Administrator.")
   public static final String DUE_TO_SITE_ADMIN_CHANGE = "15";

   @RBEntry("**** ask this Principal to reset the Preferred site when appropriate.")
   public static final String ASK_TO_RESET_SITE = "16";

   @RBEntry("**** Preferred Site is not found for Principal {0}.")
   public static final String SITE_NOT_FOUND_FOR_PRINCIPAL = "17";

   @RBEntry("Attempt to access protected content withouth credentials")
   public static final String AUTHENTICATION_FAILED = "18";

   @RBEntry("ATTENTION: Secured Action. {0} is not authorized to perform user initiated replication. Only members of {1} can perform user initiated replication.")
   public static final String NO_ACCESS_FOR_REPL = "19";

   @RBEntry("Unreferenced Items={0}, Total Files For Cleanup={1}, Cleaned Files={2}, Failed Files={3}, Size Freed={4} KB ")
   @RBComment("Result message for Remove Unreferenced Items")
   public static final String STATASTICS_CLEANUP_FOLDER = "20";

   @RBEntry("Site Name [{0}]; Vault Name [{1}]; Folder Name [{2}]; Size Freed [{3} KB]; Number Of Unreferenced Items [{4}]; Number Of Files For Cleanup [{5}]; Number Of Cleaned Files [{6}]; Number Of Failed Files [{7}].")
   @RBComment("Result message for Remove Unreferenced Items")
   public static final String DETAILS_CLEANUP_FOLDER = "21";

   @RBEntry("Status: {0}")
   @RBComment("Status for Remove Unreferenced Items")
   @RBArgComment0("Status(COMPLETED/COMPLETED_WITH_ERRORS/FAILED/IN_PROGRESS)")
   public static final String STATUS = "22";

   @RBEntry("Completed")
   @RBComment("Completed status of Remove Unreferenced Items")
   public static final String COMPLETED = "23";

   @RBEntry("Completed with errors")
   @RBComment("Completed with errors status of Remove Unreferenced Items")
   public static final String COMPLETED_WITH_ERRORS = "24";

   @RBEntry("Failed")
   @RBComment("Failed status of Remove Unreferenced Items")
   public static final String FAILED = "25";

   @RBEntry("In progress")
   @RBComment("In progress status of Remove Unreferenced Items")
   public static final String IN_PROGRESS = "26";

   @RBEntry("Folder Name : {0}")
   @RBComment("Folder name shown in Event details table")
   public static final String FOLDER_NAME = "27";
   
   @RBEntry("Pre-cleanup:")
   public static final String AVC_ERR_PRE_CLEANUP_TAG = "514";
   
   @RBEntry("Post-cleanup:")
   public static final String AVC_ERR_POST_CLEANUP_TAG = "515";
   
   @RBEntry("Auto vault cleanup failed for site {0}")
   public static final String AVC_ERR_EMAIL_MESSAGE = "516";
   
   @RBEntry("Vault size fetch failure:")
   public static final String AVC_VAULT_SIZE_FAILURE = "517";
   
   @RBEntry("Mount statistics failure:")
   public static final String AVC_MOUNT_STATS_FAILURE = "518";
   
   @RBEntry("Dereference content failure:")
   public static final String AVC_UNREF_FAILURE = "519";
   
   @RBEntry("Delete unreferenced task failure for folder {0}:")
   public static final String AVC_REMOVE_UNREF_FAILURE = "520";
   
   @RBEntry("Exception occurred while sending to remote site")
   public static final String AVC_MOUNT_STATS_ERR_MSG = "521";
   
   @RBEntry("Problem communicating with Replica site {0}")
   public static final String AVC_SITE_COMM_ERR_MSG = "522";
   
   @RBEntry("An exception occurred while running remove unreferenced files task")
   public static final String AVC_REMOVE_UNREF_UNKNOWN_FAILURE = "523";
   
   @RBEntry("Site [{0}] is not online. Aborting vault cleanup.")   
   public static final String VAULT_CLEANUP_ERROR_SITE_OFFLINE = "VAULT_CLEANUP_ERROR_SITE_OFFLINE";
}
