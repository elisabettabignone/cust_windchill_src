/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.fv.master;

import wt.util.resource.*;

@RBUUID("wt.fv.master.masterResource")
public final class masterResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile determinare l'indirizzo locale.")
   public static final String LOCAL_URL_NOT_FOUND = "0";

   @RBEntry("Provare questo e quindi, se necessario, il successivo.")
   public static final String PREFERRED_SITE_STRING = "1";

   @RBEntry("Scaricare dal sito principale.")
   public static final String MASTER_SITE_STRING = "2";
   
   @RBEntry("Scelte multiple")
   public static final String MULTIPLE_CHOICES = "MULTIPLE_CHOICES";

   @RBEntry("Impossibile trovare il sito principale.")
   public static final String NO_MASTER_SITE = "3";

   @RBEntry("Impossibile trovare il sito")
   public static final String SITE_NOT_FOUND = "4";

   @RBEntry("Un archivio con questo nome esiste già")
   public static final String VAULT_NAME_IS_NOT_UNIQUE = "5";

   @RBEntry("Una cartella con questo nome esiste già")
   public static final String FOLDER_NAME_IS_NOT_UNIQUE = "6";

   @RBEntry("Un sito con questo nome esiste già")
   public static final String SITE_NAME_IS_NOT_UNIQUE = "7";

   @RBEntry("Impossibile creare la posta in arrivo")
   public static final String INBOX_CREATION_FAILED = "8";

   @RBEntry("Sito preferito per cache di dati:")
   public static final String PREFERRED_SITE_LABEL = "9";

   @RBEntry("Sito master Windchill:")
   public static final String MASTER_SITE_LABEL = "10";

   @RBEntry("Preferenze sito preferito per la cache di dati per")
   public static final String PREF_CLIENT_TITLE = "11";

   @RBEntry("Il sito preferito è stato impostato come ")
   public static final String SUCCESS_MESSAGE = "12";

   @RBEntry("Completato")
   public static final String SUCCESS_TITLE = "13";

   @RBEntry("Impossibile eliminare la cartella di replica, contiene elementi presenti nella cache ")
   public static final String CANT_REMOVE_REPLICA_FOLDER = "14";

   @RBEntry("**** Il problema è probabilmente dovuto al fatto che l'amministratore di competenza ha modificato il nome del sito.")
   public static final String DUE_TO_SITE_ADMIN_CHANGE = "15";

   @RBEntry("**** Richiedere all'utente/gruppo/ruolo di reimpostare il sito preferito appena possibile.")
   public static final String ASK_TO_RESET_SITE = "16";

   @RBEntry("**** Impossibile trovare il sito preferito per l'utente/gruppo/ruolo {0}.")
   public static final String SITE_NOT_FOUND_FOR_PRINCIPAL = "17";

   @RBEntry("Tentativo di accedere al contenuto protetto senza autorizzazione")
   public static final String AUTHENTICATION_FAILED = "18";

   @RBEntry("ATTENZIONE: azione protetta. {0} non dispone dell'autorizzazione ad eseguire una replica avviata dall'utente. Solo i membri del gruppo {1} possono eseguire questo tipo di operazione.")
   public static final String NO_ACCESS_FOR_REPL = "19";

   @RBEntry("Elementi privi di riferimento = {0}; file da eliminare = {1}; file eliminati = {2}; file per cui l'operazione non è riuscita = {3}; spazio su disco liberato = {4} KB ")
   @RBComment("Result message for Remove Unreferenced Items")
   public static final String STATASTICS_CLEANUP_FOLDER = "20";

   @RBEntry("Nome sito: {0}; nome archivio: {1}; nome cartella: {2}; spazio liberato: {3} KB; elementi privi di riferimento: {4}; file da eliminare: {5}; file eliminati: {6}; file per cui l'operazione non è riuscita: {7}.")
   @RBComment("Result message for Remove Unreferenced Items")
   public static final String DETAILS_CLEANUP_FOLDER = "21";

   @RBEntry("Stato: {0}")
   @RBComment("Status for Remove Unreferenced Items")
   @RBArgComment0("Status(COMPLETED/COMPLETED_WITH_ERRORS/FAILED/IN_PROGRESS)")
   public static final String STATUS = "22";

   @RBEntry("Completato")
   @RBComment("Completed status of Remove Unreferenced Items")
   public static final String COMPLETED = "23";

   @RBEntry("Completato con errori")
   @RBComment("Completed with errors status of Remove Unreferenced Items")
   public static final String COMPLETED_WITH_ERRORS = "24";

   @RBEntry("Non riuscito")
   @RBComment("Failed status of Remove Unreferenced Items")
   public static final String FAILED = "25";

   @RBEntry("In corso")
   @RBComment("In progress status of Remove Unreferenced Items")
   public static final String IN_PROGRESS = "26";

   @RBEntry("Nome cartella: {0}")
   @RBComment("Folder name shown in Event details table")
   public static final String FOLDER_NAME = "27";
   
   @RBEntry("Pre-pulizia:")
   public static final String AVC_ERR_PRE_CLEANUP_TAG = "514";
   
   @RBEntry("Post-pulizia:")
   public static final String AVC_ERR_POST_CLEANUP_TAG = "515";
   
   @RBEntry("Pulizia automatica archivio non riuscita per il sito {0}")
   public static final String AVC_ERR_EMAIL_MESSAGE = "516";
   
   @RBEntry("Recupero dimensione archivio non riuscito:")
   public static final String AVC_VAULT_SIZE_FAILURE = "517";
   
   @RBEntry("Statistiche mount errate:")
   public static final String AVC_MOUNT_STATS_FAILURE = "518";
   
   @RBEntry("Annullamento riferimenti al contenuto non riuscito:")
   public static final String AVC_UNREF_FAILURE = "519";
   
   @RBEntry("Eliminazione task senza riferimento non riuscita per la cartella {0}:")
   public static final String AVC_REMOVE_UNREF_FAILURE = "520";
   
   @RBEntry("Eccezione durante l'invio al sito remoto")
   public static final String AVC_MOUNT_STATS_ERR_MSG = "521";
   
   @RBEntry("Problema di comunicazione con il sito di replica {0}")
   public static final String AVC_SITE_COMM_ERR_MSG = "522";
   
   @RBEntry("Si è verificata un'eccezione durante il task Rimuovi file senza riferimenti")
   public static final String AVC_REMOVE_UNREF_UNKNOWN_FAILURE = "523";
   
   @RBEntry("Il sito [{0}] non è in linea. Interruzione pulizia archivio in corso.")
   public static final String VAULT_CLEANUP_ERROR_SITE_OFFLINE = "VAULT_CLEANUP_ERROR_SITE_OFFLINE";
}
