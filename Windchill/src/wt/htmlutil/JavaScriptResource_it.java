/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.htmlutil;

import wt.util.resource.*;

@RBUUID("wt.htmlutil.JavaScriptResource")
public final class JavaScriptResource_it extends WTListResourceBundle {
   @RBEntry("commonEvent")
   public static final String COMMON_EVENT = "1";

   @RBEntry("urlParser")
   public static final String URL_PARSER = "2";

   @RBEntry("urlAwareTextFieldHandler")
   public static final String URL_AWARE_TEXT_FIELD_HANDLER = "3";
}
