/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.htmlutil;

import wt.util.resource.*;

@RBUUID("wt.htmlutil.htmlutilResource")
public final class htmlutilResource_it extends WTListResourceBundle {
   @RBEntry("Il file {0} non esiste o non è un file")
   public static final String FILE_NOT_FOUND_ERROR = "2";

   @RBEntry("<P>[Si è verificato un errore durante la generazione della pagina. Pagina incompleta.] </P>")
   public static final String DYNAMIC_HTML_GEN_ERROR = "3";

   @RBEntry("Parametri invalidi.")
   public static final String INVALID_PARAMETERS = "4";

   @RBEntry("È necessario selezionare almeno un elemento prima di richiamare l'azione.")
   public static final String NO_CHECKED_ITEMS = "5";

   @RBEntry("Attenzione, la sequenza immessa contiene uno o più caratteri non validi: \"{0}\". Rimuovere i caratteri non validi e riprovare.")
   @RBArgComment0(" {0} String variable that includes the character sequences that are not allowed: <,javascript,href")
   public static final String ILLEGAL_CHARACTER_ENTERED = "6";
}
