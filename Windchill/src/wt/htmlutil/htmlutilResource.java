/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.htmlutil;

import wt.util.resource.*;

@RBUUID("wt.htmlutil.htmlutilResource")
public final class htmlutilResource extends WTListResourceBundle {
   @RBEntry("File {0} does not exist, or is not a file")
   public static final String FILE_NOT_FOUND_ERROR = "2";

   @RBEntry("<P>[Error occurred during page generation.  Page is not complete.]</P>")
   public static final String DYNAMIC_HTML_GEN_ERROR = "3";

   @RBEntry("Invalid Parameters.")
   public static final String INVALID_PARAMETERS = "4";

   @RBEntry("You must select at least one item before invoking this action.")
   public static final String NO_CHECKED_ITEMS = "5";

   @RBEntry("Attention: One or more of the characters or the sequence of characters you entered is not allowed:   \"{0}\".  Please remove them and try again.")
   @RBArgComment0(" {0} String variable that includes the character sequences that are not allowed: <,javascript,href")
   public static final String ILLEGAL_CHARACTER_ENTERED = "6";
}
