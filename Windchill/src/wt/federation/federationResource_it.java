/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.federation;

import wt.util.resource.*;

@RBUUID("wt.federation.federationResource")
public final class federationResource_it extends WTListResourceBundle {
   @RBEntry("L'operazione \"{0}\" non è riuscita.")
   @RBComment("Failed Operation message")
   @RBArgComment0("Operation Name")
   public static final String OPERATION_FAILED = "0";

   @RBEntry("L'URL \"{0}\" è stata rifiutata.  La sintassi potrebbe essere errata.")
   @RBComment("Failed URL message")
   @RBArgComment0("URL Name")
   public static final String URL_REJECTED = "1";

   @RBEntry("Il tipo di contenuto \"{0}\" restituito dal servizio \"{1}\" non è supportato.")
   @RBComment("Unsupported Content Type for service message")
   @RBArgComment0(" Content Type")
   @RBArgComment1(" Service Name")
   public static final String UNSUPPORTED_CONTENT_TYPE = "2";

   @RBEntry("Il token \"{0}\" relativo all'oggetto Windchill ricevuto da un sistema remoto non è stato riconosciuto.")
   @RBComment("Unrecognized object stream token message")
   @RBArgComment0(" Token Object")
   public static final String UNRECOGNIZED_STREAM_TOKEN = "3";

   @RBEntry("Il tipo di insieme \"{0}\" relativo all'oggetto Windchill ricevuto da un sistema remoto non è stato riconosciuto.")
   @RBComment(" Unrecognized object stream array type error message")
   @RBArgComment0(" Object stream array type")
   public static final String UNRECOGNIZED_ARRAY_TYPE = "4";

   @RBEntry("Un sistema remoto ha restituito una risorsa HTTP utilizzando una codifica di trasferimento CHUNKED errata.")
   @RBComment("Bad chunked encoding error message")
   public static final String BAD_CHUNKED_ENCODING = "5";

   @RBEntry("La fine del flusso di un oggetto Windchill ricevuto da un sistema remoto è stata rilevata in anticipo. Questo indica che il flusso dell'oggetto non è completo.")
   @RBComment("Truncated Object stream error message")
   public static final String TRUNCATED_OBJECT_STREAM = "6";

   @RBEntry("Il server Web relativo al servizio \"{0}\" ha restituito l'errore \"{1} {2}\".")
   @RBComment("HTTP error response message")
   @RBArgComment0(" Service Name")
   @RBArgComment1(" Error response")
   @RBArgComment2(" Error response")
   public static final String HTTP_ERROR_RESPONSE = "7";

   @RBEntry("Non è stata definita alcuna URL per l'azione \"{0}\" sul servizio \"{1}\".")
   @RBComment("No URL for action error message")
   @RBArgComment0(" Action")
   @RBArgComment1(" Service Name")
   public static final String NO_URL_FOR_ACTION = "8";

   @RBEntry("L'oggetto MIME ricevuto in una risposta HTTP da un sistema remoto è errato.")
   @RBComment("Bad MIME syntax error message")
   public static final String BAD_MIME_SYNTAX = "9";

   @RBEntry("Nell'oggetto MIME multiparte ricevuto in una risposta HTTP da un sistema remoto non è compreso un terminatore corretto.   Questo indica che l'oggetto multiparte non è completo.")
   @RBComment(" MIME body part truncated")
   public static final String TRUNCATED_MIME_MULTIPART = "10";

   @RBEntry("La classe di oggetti \"{0}\" non supporta l'interfaccia federata e quindi non può essere aggiornata automaticamente.")
   @RBComment(" Failed refresh message")
   @RBArgComment0("object class name")
   public static final String OBJECT_NOT_REFRESHABLE = "11";

   @RBEntry("Il servizio \"{0}\" ha restituito un oggetto della classe \"{1}\", ma tale classe non ha sottoclassi locali che supportano l'interfaccia federata.")
   @RBComment("Failed federated subclass message")
   @RBArgComment0("Service Name")
   @RBArgComment1(" Object class")
   public static final String NO_FEDERATED_SUBCLASS = "12";

   @RBEntry("La risposta HTTP ricevuta da un sistema remoto è errata. Il sistema remoto ha probabilmente chiuso la connessione senza inviare una risposta completa o corretta.")
   @RBComment("Failed HTTP protocol error message")
   public static final String HTTP_PROTOCOL_ERROR = "13";

   @RBEntry("Per accedere alla risorsa \"{0}\" è necessaria l'autorizzazione, ma non sono disponibili credenziali valide.")
   @RBComment("Failed HTTP Authorization message")
   @RBArgComment0(" Resource Name")
   public static final String HTTP_AUTHORIZATION_FAILURE = "14";

   @RBEntry("L'oggetto \"{0}\" non è un riferimento di servizio accettabile per un FederatedObjectInputStream.")
   @RBComment("Service rejected message")
   @RBArgComment0(" Service Reference Object Name")
   public static final String SERVICE_REJECTED = "15";

   @RBEntry("La richiesta non è riuscita poiché il servizio \"{0}\" non è associato a un valore per il proprio attributo \"{1}\".")
   @RBComment("Failed message due to configuration incomplete")
   @RBArgComment0(" Service Name")
   @RBArgComment1(" Attribute Name")
   public static final String CONFIGURATION_INCOMPLETE = "16";

   @RBEntry("L'autorizzazione è stata negata poiché nel servizio di elenco sono presenti più elementi che corrispondono a \"{0}={1}\".")
   @RBComment(" Ambiguous Directory Results message")
   @RBArgComment0(" Entry Name")
   @RBArgComment1(" Entry Name")
   public static final String AMBIGUOUS_DIR_RESULTS = "17";

   @RBEntry("La richiesta non è riuscita poiché il servizio \"{0}\" non è associato a una tabella di conversione per la classe \"{1}\".")
   @RBComment("Failed Translation Table request message")
   @RBArgComment0(" Service Name")
   @RBArgComment1(" Class Name")
   public static final String NO_TRANSLATION_TABLE = "18";

   @RBEntry("Il servizio \"{0}\" non supporta il metodo \"{1}\".")
   @RBComment("Unsupported Method for given service message")
   @RBArgComment0(" Service Name")
   @RBArgComment1(" Method Name")
   public static final String METHOD_NOT_SUPPORTED = "19";

   @RBEntry("La stringa \"{0}\" non è un identificatore di servizio corretto.")
   @RBComment("incorrect syntax for service Identifier message")
   @RBArgComment0(" Service Identifier")
   public static final String BAD_SERVICE_ID_SYNTAX = "20";

   @RBEntry("Impossibile aggiornare gli oggetti proxy della classe \"{0}\".")
   @RBComment("Update not allowed for specified class of Proxy Object message")
   @RBArgComment0(" Class Name")
   public static final String UPDATE_NOT_SUPPORTED = "21";

   @RBEntry("La richiesta HTTP per la risorsa con URL \"{0}\" non è riuscita. Il server ha restituito il seguente stato: {1}: {2}")
   @RBComment("Failed HTTP Request message")
   @RBArgComment0(" URL")
   @RBArgComment1(" Server status")
   @RBArgComment2(" Server status")
   public static final String HTTP_REQUEST_FAILURE = "22";

   @RBEntry("La richiesta Info*Engine per la risorsa con URL \"{0}\" non è riuscita. Info*Engine ha restituito lo stato \"{1}\"")
   @RBComment("Failed Info*Engine Request message")
   @RBArgComment0(" URL")
   @RBArgComment1(" Info*Engine status")
   public static final String IE_REQUEST_FAILURE = "23";

   @RBEntry("All'utente \"{0}\" corrispondono più elementi.")
   @RBComment("Ambiguous user message")
   @RBArgComment0(" User Name")
   public static final String AMBIGUOUS_USER = "24";

   @RBEntry("Autorizzazione obbligatoria")
   @RBComment("Required authorization title message")
   public static final String HTML_AUTHORIZATION_TITLE = "25";

   @RBEntry("<H1>Autorizzazione obbligatoria</H1><HR>Per accedere alla risorsa richiesta è necessaria l'autorizzazione.")
   @RBComment("Required authorization body message")
   public static final String HTML_AUTHORIZATION_BODY = "26";

   @RBEntry("Risorsa spostata")
   @RBComment("HTML redirection title message")
   public static final String HTML_REDIRECTION_TITLE = "27";

   @RBEntry("La risorsa richiesta si trova <A HREF=\"{0}\">qui </A>.")
   @RBComment("HTML redirection body message")
   public static final String HTML_REDIRECTION_BODY = "28";

   @RBEntry("Identificatore di servizio mancante")
   @RBComment("HTML no Service Identification title message")
   public static final String HTML_NO_SERVICE_ID_TITLE = "29";

   @RBEntry("<H1>Identificatore di servizio mancante</H1><HR>Nella richiesta non è stato specificato un parametro<I>ID servizio</I>.")
   @RBComment("HTML no service Identification body message")
   public static final String HTML_NO_SERVICE_ID_BODY = "30";

   @RBEntry("Servizio non trovato")
   @RBComment("HTML Service not found message")
   public static final String HTML_SERVICE_NOT_FOUND_TITLE = "31";

   @RBEntry("<H1>Servizio non trovato</H1><HR>Servizio: <I>{0}</I> inesistente")
   @RBComment("HTML Service not found message")
   @RBArgComment0(" Service")
   public static final String HTML_SERVICE_NOT_FOUND_BODY = "32";

   @RBEntry("Eccezione non prevista")
   @RBComment("unexpected JAVA exception message")
   public static final String HTML_JAVA_EXCEPTION_TITLE = "33";

   @RBEntry("<H1>Eccezione non prevista</H1><HR>È stata rilevata un'eccezione non prevista. Segue traccia:")
   @RBComment("unexpected JAVA exception body message")
   public static final String HTML_JAVA_EXCEPTION_BODY = "34";

   @RBEntry("Errore durante l'impostazione o l'esecuzione della ricerca.")
   @RBComment("Search execution or setup error message")
   public static final String SEARCH_SETUP_ERROR = "35";

   @RBEntry("Errore durante la formattazione della schermata di ricerca.")
   @RBComment("Search format error message")
   public static final String SEARCH_FORMAT_ERROR = "36";

   @RBEntry("Problema durante la configurazione della ricerca in {0}.")
   @RBComment(" Bad search configuration error message")
   @RBArgComment0(" Search process")
   public static final String BAD_SEARCH_CONFIGURATION = "37";

   @RBEntry("La ricerca ha restituito {0} oggetti. Impossibile formattare e visualizzare gli oggetti di classe {1}.")
   @RBComment("Failed search result message")
   @RBArgComment0(" Number of objects found")
   @RBArgComment1(" Object class")
   public static final String SEARCH_RESULT_ERROR = "38";

   @RBEntry("Impossibile recuperare dall'origine remota l'oggetto selezionato.")
   @RBComment("Unable to retrieve proxy message")
   public static final String PROXY_RETRIEVAL_ERROR = "39";

   @RBEntry("Chiave di ricerca:")
   @RBComment(" Search on message")
   public static final String SEARCH_PICKER = "40";

   @RBEntry("Servizi di ricerca:")
   @RBComment(" Search services message")
   public static final String SEARCH_SERVICES = "41";

   @RBEntry("Cerca")
   @RBComment(" Search message")
   public static final String DO_SEARCH = "42";

   @RBEntry("Risultati della ricerca per {0}")
   @RBComment(" Search results message")
   @RBArgComment0(" Searched Name")
   public static final String SEARCH_RESULTS = "43";

   @RBEntry("La ricerca \"{0}\" ha restituito {1} oggetti.")
   @RBComment("Successful search message")
   @RBArgComment0(" Searched Name")
   @RBArgComment1(" Number of Objects")
   public static final String SEARCH_SUCCESS = "44";

   @RBEntry("Crea oggetto proxy")
   @RBComment("Successful create proxy object message")
   public static final String CREATE_PROXY = "45";

   @RBEntry("OK")
   @RBComment("Successful saved proxy message")
   public static final String SAVE_PROXY = "46";

   @RBEntry("Salvataggio dell'oggetto proxy completato.  Gli attributi salvati dell'oggetto comprendono:")
   @RBComment("Successful saved proxy with object saved attributes message")
   public static final String PROXY_SAVED = "47";

   @RBEntry("Il salvataggio dell'oggetto proxy non è riuscito.")
   @RBComment("Unsuccessful save proxy error message")
   public static final String PROXY_SAVE_ERROR = "48";

   @RBEntry("Reimposta")
   @RBComment("Reset form message")
   public static final String RESET_FORM = "49";

   @RBEntry("Guida")
   @RBComment("Help message")
   public static final String HELP = "50";

   @RBEntry("Ricerca remota")
   @RBComment("Remote Search message")
   public static final String FEDERATION_SEARCH_LABEL = "51";

   @RBEntry("Tutto")
   @RBComment("All message")
   public static final String ALL = "52";

   @RBEntry("Master parti")
   @RBComment("Part Master message")
   public static final String PART_MASTER = "53";

   @RBEntry("Master documenti")
   @RBComment("Document Master message")
   public static final String DOCUMENT_MASTER = "54";

   @RBEntry("L'oggetto proxy non è persistente.")
   @RBComment("Proxy not persistent message")
   public static final String PROXY_NOT_PERSISTENT = "55";

   @RBEntry("Parte")
   @RBComment("Part message")
   public static final String PART = "56";

   @RBEntry("Documento")
   @RBComment("Document message")
   public static final String DOCUMENT = "57";

   @RBEntry("Posizione")
   @RBComment("Location message")
   public static final String LOCATION = "58";

   @RBEntry("Sfoglia...")
   @RBComment("Browse message")
   public static final String BROWSE = "59";

   @RBEntry("Ciclo di vita")
   @RBComment("Life cycle message")
   public static final String LIFE_CYCLE = "60";

   @RBEntry("Team")
   @RBComment("Team Template message")
   public static final String TEAMTEMPLATE = "61";

   @RBEntry("Per l'oggetto selezionato esiste già un proxy.")
   @RBComment("Proxy already exists error message")
   public static final String PROXY_ALREADY_EXISTS = "62";

   @RBEntry("Crea un'iterazione dell'oggetto proxy {0}")
   @RBComment(" Create proxy iteration message")
   @RBArgComment0(" Object Name")
   public static final String CREATE_PROXY_ITERATION = "63";

   @RBEntry("Non è consentito effettuare il Check-Out di un proxy")
   @RBComment(" Proxy checkout not allowed error message")
   public static final String PROXY_CHECKOUT_NOT_ALLOWED = "64";

   @RBEntry("La revisione di un proxy non è consentita")
   @RBComment(" Proxy revision not allowed error message")
   public static final String PROXY_REVISION_NOT_ALLOWED = "65";

   @RBEntry("La modifica di un proxy non è consentita")
   @RBComment(" Proxy modification not allowed error message")
   public static final String PROXY_MODIFICATION_NOT_ALLOWED = "66";

   @RBEntry("Un proxy esiste già per l'oggetto con origine \"{0}\" del servizio \"{1}\".")
   @RBComment("Proxy with same source error message")
   @RBArgComment0(" Source Name")
   @RBArgComment1(" Service Name")
   public static final String PROXY_WITH_SAME_SOURCE = "67";

   @RBEntry("Proprietà origine")
   @RBComment("Source properties message")
   public static final String SOURCE_PROPERTIES = "68";

   @RBEntry("Nome utente non corretto: formato password nelle proprietà dell'ambiente federato")
   @RBComment("Wrong federation properties config error message")
   public static final String FEDERATION_PROPERTIES_WRONG = "69";

   @RBEntry("Il valore del parametro \"{0}\" è nullo.")
   @RBComment("Input parameter value is null.")
   @RBArgComment0(" Parameter Name (e.g., feedbackObject)")
   public static final String NULL_PARAMETER = "70";

   @RBEntry("\"{0}\" mancante di valore e nome dell'argomento.")
   @RBComment("No argument name and value was specified for the argv array element.")
   @RBArgComment0(" The argv array element (e.g., argv[1])")
   public static final String ARGV_MISSING_ARG_VALUE_AND_NAME = "71";

   @RBEntry("Il nome dell'argomento \"{0}\" è una stringa vuota.")
   @RBComment("The argument name specified for the argv array element is an empty String.")
   @RBArgComment0(" The argv array element (e.g., argv[1])")
   public static final String ARGV_ARG_NAME_EMPTY_STRING = "72";

   @RBEntry("Il nome di argomento \"{0}\" non è un tipo \"{1}\".")
   @RBComment("The argument name specified for the argv array element is not the correct type.")
   @RBArgComment0(" The argv array element (e.g., argv[1])")
   @RBArgComment1(" The correct type (e.g., String)")
   public static final String ARGV_ARG_NAME_INVALID_TYPE = "73";

   @RBEntry("Il nome dell'argomento \"{0}\" è nullo.")
   @RBComment("The argument name specified for the argv array element is null.")
   @RBArgComment0(" The argv array element (e.g., argv[1])")
   public static final String ARGV_NULL_ARG_NAME = "74";

   @RBEntry("Il valore dell'argomento \"{0}\" è un array \"{1}\" vuoto.")
   @RBComment("The argument value specified for the argv array element is an empty array.")
   @RBArgComment0(" The argv array element (e.g., argv[1])")
   @RBArgComment1(" The array type (e.g., TypeInstance)")
   public static final String ARGV_EMPTY_ARRAY_ARG_VALUE = "75";

   @RBEntry("Il tipo di valore dell'argomento di \"{0}\" \"{1}\" non è supportato")
   @RBComment("The argument value specified for the argv array element is a type that isn't supported.")
   @RBArgComment0(" The argv array element (e.g., argv[1])")
   @RBArgComment1(" The argument value type (e.g., HashTable)")
   public static final String ARGV_ARG_VALUE_UNSUPPORTED_TYPE = "76";

   @RBEntry("Il valore dell'argomento \"{0}\" è nullo.")
   @RBComment("The argument value specified for the argv array element is null.")
   @RBArgComment0(" The argv array element (e.g., argv[1])")
   public static final String ARGV_NULL_ARG_VALUE = "77";

   @RBEntry("\"{0}\" deve contenere almeno un valore di argomento di tipo: istanza tipo, elemento o gruppo.")
   @RBComment("The argv array must contain at least one argument value that is of type: TypeInstance, Element, or Group.")
   @RBArgComment0(" The argv parameter name (currently, the only possible value is: argv)")
   public static final String ARGV_MISSING_REQUIRED_TYPE = "78";

   @RBEntry("Canale input/output inesistente: {0}")
   public static final String NO_SUCH_PIPE = "79";

   @RBEntry("L'oggetto \"{0}\" è inesistente.")
   @RBComment("No object with the specified UFID exits.")
   public static final String NO_SUCH_OBJECT = "102";

   @RBEntry("Impossibile trovare l'oggetto di destinazione \"{0}\".")
   @RBComment("Failed message")
   @RBArgComment0("target object")
   public static final String UNABLE_TO_FIND_TARGET_OBJECT = "109";
}
