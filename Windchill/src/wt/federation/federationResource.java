/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.federation;

import wt.util.resource.*;

@RBUUID("wt.federation.federationResource")
public final class federationResource extends WTListResourceBundle {
   @RBEntry("The operation:   \"{0}\" failed.")
   @RBComment("Failed Operation message")
   @RBArgComment0("Operation Name")
   public static final String OPERATION_FAILED = "0";

   @RBEntry("The URL \"{0}\" was rejected.  Possibly, its syntax is incorrect.")
   @RBComment("Failed URL message")
   @RBArgComment0("URL Name")
   public static final String URL_REJECTED = "1";

   @RBEntry("The content type \"{0}\" returned by the service named \"{1}\" is not supported.")
   @RBComment("Unsupported Content Type for service message")
   @RBArgComment0(" Content Type")
   @RBArgComment1(" Service Name")
   public static final String UNSUPPORTED_CONTENT_TYPE = "2";

   @RBEntry("The Windchill object stream token \"{0}\" received from a remote system is not recognized.")
   @RBComment("Unrecognized object stream token message")
   @RBArgComment0(" Token Object")
   public static final String UNRECOGNIZED_STREAM_TOKEN = "3";

   @RBEntry("The Windchill object stream array type \"{0}\" received from a remote system is not recognized.")
   @RBComment(" Unrecognized object stream array type error message")
   @RBArgComment0(" Object stream array type")
   public static final String UNRECOGNIZED_ARRAY_TYPE = "4";

   @RBEntry("A remote system returned an HTTP resource using CHUNKED transfer encoding, but the encoding was incorrect.")
   @RBComment("Bad chunked encoding error message")
   public static final String BAD_CHUNKED_ENCODING = "5";

   @RBEntry("The end of a Windchill object stream received from a remote system was encountered prematurely.  This indicates that the object stream was incomplete.")
   @RBComment("Truncated Object stream error message")
   public static final String TRUNCATED_OBJECT_STREAM = "6";

   @RBEntry("The web server on the service named \"{0}\" returned the error response \"{1} {2}\"")
   @RBComment("HTTP error response message")
   @RBArgComment0(" Service Name")
   @RBArgComment1(" Error response")
   @RBArgComment2(" Error response")
   public static final String HTTP_ERROR_RESPONSE = "7";

   @RBEntry("No URL has been defined for the action named \"{0}\" on the service named \"{1}\".")
   @RBComment("No URL for action error message")
   @RBArgComment0(" Action")
   @RBArgComment1(" Service Name")
   public static final String NO_URL_FOR_ACTION = "8";

   @RBEntry("The MIME object received in an HTTP response from a remote system was incorrectly formed.")
   @RBComment("Bad MIME syntax error message")
   public static final String BAD_MIME_SYNTAX = "9";

   @RBEntry("The multipart MIME object received in an HTTP response from a remote system did not include a proper termination boundary.  This indicates that the multipart object was incomplete.")
   @RBComment(" MIME body part truncated")
   public static final String TRUNCATED_MIME_MULTIPART = "10";

   @RBEntry("The object class \"{0}\" does not implement the Federated interface, so it can not be refreshed automatically.")
   @RBComment(" Failed refresh message")
   @RBArgComment0("object class name")
   public static final String OBJECT_NOT_REFRESHABLE = "11";

   @RBEntry("The service named \"{0}\" returned an object of class \"{1}\", but this class has no local subclass implementing the Federated interface.")
   @RBComment("Failed federated subclass message")
   @RBArgComment0("Service Name")
   @RBArgComment1(" Object class")
   public static final String NO_FEDERATED_SUBCLASS = "12";

   @RBEntry("The HTTP response received from a remote system was badly formed.  Possibly, the remote system closed the connection without sending a complete or proper response.")
   @RBComment("Failed HTTP protocol error message")
   public static final String HTTP_PROTOCOL_ERROR = "13";

   @RBEntry("Authorization is required to access resource \"{0}\", but no valid credentials are available.")
   @RBComment("Failed HTTP Authorization message")
   @RBArgComment0(" Resource Name")
   public static final String HTTP_AUTHORIZATION_FAILURE = "14";

   @RBEntry("The object \"{0}\" is not an acceptable service reference for a FederatedObjectInputStream.")
   @RBComment("Service rejected message")
   @RBArgComment0(" Service Reference Object Name")
   public static final String SERVICE_REJECTED = "15";

   @RBEntry("A request failed because the service named  \"{0}\" does not have a value for its attribute named \"{1}\".")
   @RBComment("Failed message due to configuration incomplete")
   @RBArgComment0(" Service Name")
   @RBArgComment1(" Attribute Name")
   public static final String CONFIGURATION_INCOMPLETE = "16";

   @RBEntry("Authorization failed because the directory service has more than one entry that matches \"{0}={1}\".")
   @RBComment(" Ambiguous Directory Results message")
   @RBArgComment0(" Entry Name")
   @RBArgComment1(" Entry Name")
   public static final String AMBIGUOUS_DIR_RESULTS = "17";

   @RBEntry("A request failed because the service named \"{0}\" has no translation table for the class \"{1}\".")
   @RBComment("Failed Translation Table request message")
   @RBArgComment0(" Service Name")
   @RBArgComment1(" Class Name")
   public static final String NO_TRANSLATION_TABLE = "18";

   @RBEntry("The service named \"{0}\" does not support the method \"{1}\".")
   @RBComment("Unsupported Method for given service message")
   @RBArgComment0(" Service Name")
   @RBArgComment1(" Method Name")
   public static final String METHOD_NOT_SUPPORTED = "19";

   @RBEntry("The string \"{0}\" is not a correctly formed service identifier.")
   @RBComment("incorrect syntax for service Identifier message")
   @RBArgComment0(" Service Identifier")
   public static final String BAD_SERVICE_ID_SYNTAX = "20";

   @RBEntry("Proxy objects of class \"{0}\" can not be updated.")
   @RBComment("Update not allowed for specified class of Proxy Object message")
   @RBArgComment0(" Class Name")
   public static final String UPDATE_NOT_SUPPORTED = "21";

   @RBEntry("The HTTP request for the resource with URL \"{0}\" failed.  The server returned the following status: {1}: {2}")
   @RBComment("Failed HTTP Request message")
   @RBArgComment0(" URL")
   @RBArgComment1(" Server status")
   @RBArgComment2(" Server status")
   public static final String HTTP_REQUEST_FAILURE = "22";

   @RBEntry("The Info*Engine request for the resource with URL \"{0}\" failed.  Info*Engine returned the status \"{1}\"")
   @RBComment("Failed Info*Engine Request message")
   @RBArgComment0(" URL")
   @RBArgComment1(" Info*Engine status")
   public static final String IE_REQUEST_FAILURE = "23";

   @RBEntry("More than one entry matches the user \"{0}\".")
   @RBComment("Ambiguous user message")
   @RBArgComment0(" User Name")
   public static final String AMBIGUOUS_USER = "24";

   @RBEntry("Authorization Required")
   @RBComment("Required authorization title message")
   public static final String HTML_AUTHORIZATION_TITLE = "25";

   @RBEntry("<H1>Authorization Required</H1><HR>Access to the resource you requested requires authorization.")
   @RBComment("Required authorization body message")
   public static final String HTML_AUTHORIZATION_BODY = "26";

   @RBEntry("Resource Moved")
   @RBComment("HTML redirection title message")
   public static final String HTML_REDIRECTION_TITLE = "27";

   @RBEntry("The resource you requested is located <A HREF=\"{0}\">here</A>.")
   @RBComment("HTML redirection body message")
   public static final String HTML_REDIRECTION_BODY = "28";

   @RBEntry("Service Identifier Missing")
   @RBComment("HTML no Service Identification title message")
   public static final String HTML_NO_SERVICE_ID_TITLE = "29";

   @RBEntry("<H1>Service Identifier Missing</H1><HR>Your request did not specify a <I>serviceId</I> parameter.")
   @RBComment("HTML no service Identification body message")
   public static final String HTML_NO_SERVICE_ID_BODY = "30";

   @RBEntry("Service Not Found")
   @RBComment("HTML Service not found message")
   public static final String HTML_SERVICE_NOT_FOUND_TITLE = "31";

   @RBEntry("<H1>Service Not Found</H1><HR>No such service: <I>{0}</I>")
   @RBComment("HTML Service not found message")
   @RBArgComment0(" Service")
   public static final String HTML_SERVICE_NOT_FOUND_BODY = "32";

   @RBEntry("Unexpected Exception")
   @RBComment("unexpected JAVA exception message")
   public static final String HTML_JAVA_EXCEPTION_TITLE = "33";

   @RBEntry("<H1>Unexpected Exception</H1><HR>An unexpected exception was caught.  Traceback follows:")
   @RBComment("unexpected JAVA exception body message")
   public static final String HTML_JAVA_EXCEPTION_BODY = "34";

   @RBEntry("Error setting up or executing the search.")
   @RBComment("Search execution or setup error message")
   public static final String SEARCH_SETUP_ERROR = "35";

   @RBEntry("Error formatting the search screen.")
   @RBComment("Search format error message")
   public static final String SEARCH_FORMAT_ERROR = "36";

   @RBEntry("Problem in configuring this search in {0}.")
   @RBComment(" Bad search configuration error message")
   @RBArgComment0(" Search process")
   public static final String BAD_SEARCH_CONFIGURATION = "37";

   @RBEntry("Your search found {0} objects.  But was unable to format the {1} class objects for display.")
   @RBComment("Failed search result message")
   @RBArgComment0(" Number of objects found")
   @RBArgComment1(" Object class")
   public static final String SEARCH_RESULT_ERROR = "38";

   @RBEntry("The object you chose could not be retrieved from its remote source.")
   @RBComment("Unable to retrieve proxy message")
   public static final String PROXY_RETRIEVAL_ERROR = "39";

   @RBEntry("Search On:")
   @RBComment(" Search on message")
   public static final String SEARCH_PICKER = "40";

   @RBEntry("Search Services:")
   @RBComment(" Search services message")
   public static final String SEARCH_SERVICES = "41";

   @RBEntry("Search")
   @RBComment(" Search message")
   public static final String DO_SEARCH = "42";

   @RBEntry("Search Results for {0}")
   @RBComment(" Search results message")
   @RBArgComment0(" Searched Name")
   public static final String SEARCH_RESULTS = "43";

   @RBEntry("Your search \"{0}\" found {1} objects.")
   @RBComment("Successful search message")
   @RBArgComment0(" Searched Name")
   @RBArgComment1(" Number of Objects")
   public static final String SEARCH_SUCCESS = "44";

   @RBEntry("Create Proxy Object")
   @RBComment("Successful create proxy object message")
   public static final String CREATE_PROXY = "45";

   @RBEntry("OK")
   @RBComment("Successful saved proxy message")
   public static final String SAVE_PROXY = "46";

   @RBEntry("The proxy object was saved successfully.  The saved attributes of the object include:")
   @RBComment("Successful saved proxy with object saved attributes message")
   public static final String PROXY_SAVED = "47";

   @RBEntry("The proxy object was not saved successfully.")
   @RBComment("Unsuccessful save proxy error message")
   public static final String PROXY_SAVE_ERROR = "48";

   @RBEntry("Reset")
   @RBComment("Reset form message")
   public static final String RESET_FORM = "49";

   @RBEntry("Help")
   @RBComment("Help message")
   public static final String HELP = "50";

   @RBEntry("Remote Search")
   @RBComment("Remote Search message")
   public static final String FEDERATION_SEARCH_LABEL = "51";

   @RBEntry("All")
   @RBComment("All message")
   public static final String ALL = "52";

   @RBEntry("Part_Master")
   @RBComment("Part Master message")
   public static final String PART_MASTER = "53";

   @RBEntry("Document_Master")
   @RBComment("Document Master message")
   public static final String DOCUMENT_MASTER = "54";

   @RBEntry("The proxy object is not persistent.")
   @RBComment("Proxy not persistent message")
   public static final String PROXY_NOT_PERSISTENT = "55";

   @RBEntry("Part")
   @RBComment("Part message")
   public static final String PART = "56";

   @RBEntry("Document")
   @RBComment("Document message")
   public static final String DOCUMENT = "57";

   @RBEntry("Location")
   @RBComment("Location message")
   public static final String LOCATION = "58";

   @RBEntry("Browse...")
   @RBComment("Browse message")
   public static final String BROWSE = "59";

   @RBEntry("LifeCycle")
   @RBComment("Life cycle message")
   public static final String LIFE_CYCLE = "60";

   @RBEntry("Team")
   @RBComment("Team Template message")
   public static final String TEAMTEMPLATE = "61";

   @RBEntry("A proxy already exists for the object you chose.")
   @RBComment("Proxy already exists error message")
   public static final String PROXY_ALREADY_EXISTS = "62";

   @RBEntry("Create an iteration of proxy object {0}")
   @RBComment(" Create proxy iteration message")
   @RBArgComment0(" Object Name")
   public static final String CREATE_PROXY_ITERATION = "63";

   @RBEntry("Checking out a proxy is not allowed !")
   @RBComment(" Proxy checkout not allowed error message")
   public static final String PROXY_CHECKOUT_NOT_ALLOWED = "64";

   @RBEntry("Revising a proxy is not allowed !")
   @RBComment(" Proxy revision not allowed error message")
   public static final String PROXY_REVISION_NOT_ALLOWED = "65";

   @RBEntry("Modifying a proxy is not allowed!")
   @RBComment(" Proxy modification not allowed error message")
   public static final String PROXY_MODIFICATION_NOT_ALLOWED = "66";

   @RBEntry("A proxy already exists for the object with source \"{0}\" from service \"{1}.")
   @RBComment("Proxy with same source error message")
   @RBArgComment0(" Source Name")
   @RBArgComment1(" Service Name")
   public static final String PROXY_WITH_SAME_SOURCE = "67";

   @RBEntry("Source Properties")
   @RBComment("Source properties message")
   public static final String SOURCE_PROPERTIES = "68";

   @RBEntry("Wrong username:passwd format in federation properties")
   @RBComment("Wrong federation properties config error message")
   public static final String FEDERATION_PROPERTIES_WRONG = "69";

   @RBEntry("The \"{0}\" parameter value is null.")
   @RBComment("Input parameter value is null.")
   @RBArgComment0(" Parameter Name (e.g., feedbackObject)")
   public static final String NULL_PARAMETER = "70";

   @RBEntry("\"{0}\" missing argument name and value.")
   @RBComment("No argument name and value was specified for the argv array element.")
   @RBArgComment0(" The argv array element (e.g., argv[1])")
   public static final String ARGV_MISSING_ARG_VALUE_AND_NAME = "71";

   @RBEntry("\"{0}\" argument name is an empty String.")
   @RBComment("The argument name specified for the argv array element is an empty String.")
   @RBArgComment0(" The argv array element (e.g., argv[1])")
   public static final String ARGV_ARG_NAME_EMPTY_STRING = "72";

   @RBEntry("\"{0}\" argument name is not a \"{1}\" type.")
   @RBComment("The argument name specified for the argv array element is not the correct type.")
   @RBArgComment0(" The argv array element (e.g., argv[1])")
   @RBArgComment1(" The correct type (e.g., String)")
   public static final String ARGV_ARG_NAME_INVALID_TYPE = "73";

   @RBEntry("\"{0}\" argument name is null.")
   @RBComment("The argument name specified for the argv array element is null.")
   @RBArgComment0(" The argv array element (e.g., argv[1])")
   public static final String ARGV_NULL_ARG_NAME = "74";

   @RBEntry("\"{0}\" argument value is an empty \"{1}\" array.")
   @RBComment("The argument value specified for the argv array element is an empty array.")
   @RBArgComment0(" The argv array element (e.g., argv[1])")
   @RBArgComment1(" The array type (e.g., TypeInstance)")
   public static final String ARGV_EMPTY_ARRAY_ARG_VALUE = "75";

   @RBEntry("\"{0}\" argument value type \"{1}\" is not a supported type.")
   @RBComment("The argument value specified for the argv array element is a type that isn't supported.")
   @RBArgComment0(" The argv array element (e.g., argv[1])")
   @RBArgComment1(" The argument value type (e.g., HashTable)")
   public static final String ARGV_ARG_VALUE_UNSUPPORTED_TYPE = "76";

   @RBEntry("\"{0}\" argument value is null.")
   @RBComment("The argument value specified for the argv array element is null.")
   @RBArgComment0(" The argv array element (e.g., argv[1])")
   public static final String ARGV_NULL_ARG_VALUE = "77";

   @RBEntry("\"{0}\" must contain at least one argument value of type: TypeInstance, Element, or Group.")
   @RBComment("The argv array must contain at least one argument value that is of type: TypeInstance, Element, or Group.")
   @RBArgComment0(" The argv parameter name (currently, the only possible value is: argv)")
   public static final String ARGV_MISSING_REQUIRED_TYPE = "78";

   @RBEntry("No such input/output pipe: {0}")
   public static final String NO_SUCH_PIPE = "79";

   @RBEntry("No such object: \"{0}\".")
   @RBComment("No object with the specified UFID exits.")
   public static final String NO_SUCH_OBJECT = "102";

   @RBEntry("Unable to find target object \"{0}\".")
   @RBComment("Failed message")
   @RBArgComment0("target object")
   public static final String UNABLE_TO_FIND_TARGET_OBJECT = "109";
}
