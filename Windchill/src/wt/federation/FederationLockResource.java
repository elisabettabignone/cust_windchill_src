/* bcwti
 *
 * Copyright (c) 2011 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.federation;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBArgComment1;
import wt.util.resource.RBArgComment2;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.federation.FederationLockResource")
public final class FederationLockResource extends WTListResourceBundle {

    @RBEntry("Missing required inputs : Missing input collection or repository.")
    @RBComment("The message shown when Federation Lock or Unlock request is failed when required inputs are missing.")
    public static final String MISSING_REQUIRED_INPUTS = "0";

    @RBEntry("Invalid Input : Request fails as one or more Non RepositoryInteroperables are found in the request input.")
    @RBComment("The message shown when Federation Lock or Unlock request is failed when non 'RepositoryInteroperable' type objects are found in the request.")
    public static final String INVALID_INPUT_NON_REPOSITORY_INTEROPERABLES_FOUND = "1";

    @RBEntry("Invalid Input : Request fails as one or more objects are non-latest iterations in the request input.")
    @RBComment("The message shown when Federation Lock or Unlock request is failed when non latest iterations are found in the request.")
    public static final String INVALID_INPUT_NON_LATEST_ITERATIONS_FOUND = "2";

    @RBEntry("Invalid Input : Request fails as one or more objects in the request are locally locked.")
    @RBComment("The message shown when Federation Lock or Unlock request is failed when locally locked objects are found.")
    public static final String INVALID_INPUT_OBJECTS_LOCALLY_LOCKED = "3";

    @RBEntry("Invalid Input : Request fails as the requesting Repository is not valid.")
    @RBComment("The message shown when Federation Lock or Unlock request is failed due to an invalid repository in the request.")
    public static final String INVALID_INPUT_REPOSITORY_INVALID = "4";

    @RBEntry("Invalid Input : Request cannot proceed as User - \"{0}\" does not have enough permission on one or more objects.")
    @RBComment("The message shown when Federation Lock or Unlock request is failed when user does not have enough permissions on the objects.")
    @RBArgComment0("Name of user who is trying to lock or unlock.")
    public static final String INVALID_INPUT_UNMODIFIABLE_OBJECTS = "5";

    @RBEntry("Another repository already owns the lock for one or more objects in the request.")
    @RBComment("The message shown when Federation Lock or Unlock request is failed when objects are locked by a different repository.")
    public static final String OBJECTS_FEDERATION_LOCKED_BY_DIFF_REPOSITORY = "6";

    @RBEntry("Request fails as one or more objects in the request are not locked.")
    @RBComment("The message shown when Federation Unlock request is failed when objects are not locked.")
    public static final String UNLOCKED_OBJECTS_FOUND = "7";

    @RBEntry(", Part \"{0}\" is locked by User \"{1}\" for Repository \"{2}\" ")
    @RBComment("The message shown when Federation Lock request is failed when part is locked  by user for different repository.")
    @RBArgComment0("Name of Part that is already  locked")
    @RBArgComment1("Name of the user who has locked the part")
    @RBArgComment2("Name of the repository for which the part is locked")
    public static final String FAILED_LOCK_MSG_SUB_PART = "FAILED_LOCK_MSG_SUB_PART";

    @RBEntry("Lock operation failed since  Part \"{0}\" is locked by User \"{1}\" for Repository \"{2}\" ")
    @RBComment("The message shown when Federation Lock  request is failed when part is locked  by user for different repository.")
    @RBArgComment0("Name of Part that is already  locked")
    @RBArgComment1("Name of the user who has locked the part")
    @RBArgComment2("Name of the repository for which the part is locked")
    public static final String FAILED_LOCK_MSG = "FAILED_LOCK_MSG";



}
