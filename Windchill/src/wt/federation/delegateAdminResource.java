/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.federation;

import wt.util.resource.*;

@RBUUID("wt.federation.delegateAdminResource")
public final class delegateAdminResource extends WTListResourceBundle {
   @RBEntry("Help")
   @RBComment("Label for link to help")
   public static final String HELP = "0";

   @RBEntry("Home")
   @RBComment("Label for link to Windchill home page")
   public static final String HOME = "1";

   @RBEntry("Find delegates")
   public static final String FIND_DELEGATES = "2";

   @RBEntry("Create delegate")
   public static final String CREATE_DELEGATE = "3";

   @RBEntry("Create repository type")
   public static final String CREATE_REPOSITORY_TYPE = "4";

   @RBEntry("Create repository")
   public static final String CREATE_REPOSITORY = "5";

   @RBEntry("Find by")
   public static final String FIND_BY = "6";

   @RBEntry("Repository type")
   public static final String REPOSITORY_TYPE = "7";

   @RBEntry("Repository name")
   public static final String REPOSITORY_NAME = "8";

   @RBEntry("Search")
   public static final String SEARCH = "9";

   @RBEntry("Whose name contains ")
   public static final String NAME_CONTAINS = "10";

   @RBEntry("Name")
   public static final String NAME = "11";

   @RBEntry("DN")
   public static final String DN = "12";

   @RBEntry("Actions")
   public static final String ACTIONS = "13";

   @RBEntry(" No matches.")
   public static final String No_MATCHES = "14";

   @RBEntry("A value is missing for a required field !")
   public static final String REQUIRED_FIELD_MISSING = "15";

   @RBEntry("Entry already exist.")
   public static final String ENTRY_ALREADY_EXIST = "16";

   @RBEntry("Source url")
   public static final String SOURCE_URL = "17";

   @RBEntry("Type id")
   public static final String TYPE_ID = "18";

   @RBEntry("Description")
   public static final String DESCRIPTION = "19";

   @RBEntry("Create")
   public static final String CREATE = "20";

   @RBEntry("Submit")
   public static final String SUBMIT = "21";

   @RBEntry("Super repository type")
   public static final String SUPER_REPOSITORY_TYPE = "22";

   @RBEntry("Update delegate")
   public static final String UPDATE_DELEGATE = "23";

   @RBEntry("OK")
   public static final String OK = "24";

   @RBEntry("Delete")
   public static final String DELETE = "25";

   @RBEntry("Object type")
   public static final String OBJECT_TYPE = "26";

   @RBEntry("Windchill adapter")
   public static final String WC_ADAPTER = "27";

   @RBEntry("Please go to create repository type first !")
   public static final String CREATE_REPOSITORY_TYPE_FIRST = "28";
}
