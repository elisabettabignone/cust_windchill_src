/* bcwti
 *
 * Copyright (c) 2012 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.federation.PrincipalManager;

import wt.util.resource.*;

@RBUUID("wt.federation.PrincipalManager.PrincipalManagerResource")
public final class PrincipalManagerResource_it extends WTListResourceBundle {
   @RBEntry("Errore di configurazione Info*Engine: impossibile recuperare l'URL del provider del servizio di elenco di default e/o la base di ricerca del servizio dalle proprietà di Info*Engine. Contattare l'amministratore di sistema.")
   @RBComment("Error message displayed for a JNDI adapter configuration problem where the default directory service provider cannot be reached")
   public static final String CONFIG_ERROR_INCORRECT_NAMING_PROVIDER = "CONFIG_ERROR_INCORRECT_NAMING_PROVIDER";

   @RBEntry("Errore di configurazione Info*Engine: impossibile individuare le configurazioni dell'adattatore JNDI mediante le impostazioni Info*Engine correnti. Contattare l'amministratore di sistema.")
   @RBComment("Error message displayed for a JNDI adapter configuration problem where there are no adapter configurations present")
   public static final String CONFIG_ERROR_NO_ADAPTER_DEFS = "CONFIG_ERROR_NO_ADAPTER_DEFS";

   @RBEntry("Errore di configurazione Info*Engine: base di ricerca del servizio di elenco \"{0}\" non univoca. Le seguenti configurazioni dell'adattatore JNDI sono state trovate con questo stesso valore per la base di ricerca: \"{1}\" e \"{2}\". Specificare una base di ricerca univoca per ogni adattatore o rivolgersi all'amministratore di sistema.")
   @RBComment("Error message displayed for a JNDI adapter configuration problem related to non-unique search base values")
   @RBArgComment0("The search base that is not unique")
   @RBArgComment1("Service name 1")
   @RBArgComment2("Service name 2")
   public static final String CONFIG_ERROR_SEARCH_BASE_NOT_UNIQUE = "CONFIG_ERROR_SEARCH_BASE_NOT_UNIQUE";
}
