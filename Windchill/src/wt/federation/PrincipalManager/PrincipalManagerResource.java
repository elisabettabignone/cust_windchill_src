/* bcwti
 *
 * Copyright (c) 2012 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.federation.PrincipalManager;

import wt.util.resource.*;

@RBUUID("wt.federation.PrincipalManager.PrincipalManagerResource")
public final class PrincipalManagerResource extends WTListResourceBundle {
   @RBEntry("Info*Engine configuration error -  The default Directory Service Provider URL and/or Service Search Base could not be retrieved from Info*Engine properties. Contact your system administrator.")
   @RBComment("Error message displayed for a JNDI adapter configuration problem where the default directory service provider cannot be reached")
   public static final String CONFIG_ERROR_INCORRECT_NAMING_PROVIDER = "CONFIG_ERROR_INCORRECT_NAMING_PROVIDER";

   @RBEntry("Info*Engine configuration error - No JNDI Adapter configurations could be located using the current Info*Engine settings. Contact your system administrator.")
   @RBComment("Error message displayed for a JNDI adapter configuration problem where there are no adapter configurations present")
   public static final String CONFIG_ERROR_NO_ADAPTER_DEFS = "CONFIG_ERROR_NO_ADAPTER_DEFS";

   @RBEntry("Info*Engine configuration error - Directory Service Search Base \"{0}\" is not unique. Following JNDI adapter configurations were found with this same value for search base: \"{1}\", and \"{2}\". Specify a unique search base for each adapter or contact your system administrator.")
   @RBComment("Error message displayed for a JNDI adapter configuration problem related to non-unique search base values")
   @RBArgComment0("The search base that is not unique")
   @RBArgComment1("Service name 1")
   @RBArgComment2("Service name 2")
   public static final String CONFIG_ERROR_SEARCH_BASE_NOT_UNIQUE = "CONFIG_ERROR_SEARCH_BASE_NOT_UNIQUE";
}
