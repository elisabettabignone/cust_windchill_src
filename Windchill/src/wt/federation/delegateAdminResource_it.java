/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.federation;

import wt.util.resource.*;

@RBUUID("wt.federation.delegateAdminResource")
public final class delegateAdminResource_it extends WTListResourceBundle {
   @RBEntry("Guida")
   @RBComment("Label for link to help")
   public static final String HELP = "0";

   @RBEntry("Home")
   @RBComment("Label for link to Windchill home page")
   public static final String HOME = "1";

   @RBEntry("Trova delegati")
   public static final String FIND_DELEGATES = "2";

   @RBEntry("Crea delegati")
   public static final String CREATE_DELEGATE = "3";

   @RBEntry("Crea tipo di repository")
   public static final String CREATE_REPOSITORY_TYPE = "4";

   @RBEntry("Crea repository")
   public static final String CREATE_REPOSITORY = "5";

   @RBEntry("Cerca per")
   public static final String FIND_BY = "6";

   @RBEntry("Tipo repository")
   public static final String REPOSITORY_TYPE = "7";

   @RBEntry("Nome repository")
   public static final String REPOSITORY_NAME = "8";

   @RBEntry("Cerca")
   public static final String SEARCH = "9";

   @RBEntry("il cui nome contenga")
   public static final String NAME_CONTAINS = "10";

   @RBEntry("Nome")
   public static final String NAME = "11";

   @RBEntry("DN")
   public static final String DN = "12";

   @RBEntry("Azioni")
   public static final String ACTIONS = "13";

   @RBEntry("Nessuna corrispondenza")
   public static final String No_MATCHES = "14";

   @RBEntry("Valore mancante in campo obbligatorio.")
   public static final String REQUIRED_FIELD_MISSING = "15";

   @RBEntry("La voce esiste già.")
   public static final String ENTRY_ALREADY_EXIST = "16";

   @RBEntry("Url sorgente")
   public static final String SOURCE_URL = "17";

   @RBEntry("Id tipo")
   public static final String TYPE_ID = "18";

   @RBEntry("Descrizione")
   public static final String DESCRIPTION = "19";

   @RBEntry("Crea")
   public static final String CREATE = "20";

   @RBEntry("Invia")
   public static final String SUBMIT = "21";

   @RBEntry("Tipo super repository")
   public static final String SUPER_REPOSITORY_TYPE = "22";

   @RBEntry("Aggiorna delegato")
   public static final String UPDATE_DELEGATE = "23";

   @RBEntry("OK")
   public static final String OK = "24";

   @RBEntry("Elimina")
   public static final String DELETE = "25";

   @RBEntry("Tipo di oggetto")
   public static final String OBJECT_TYPE = "26";

   @RBEntry("Adattatore Windchill")
   public static final String WC_ADAPTER = "27";

   @RBEntry("Creare il tipo di repository.")
   public static final String CREATE_REPOSITORY_TYPE_FIRST = "28";
}
