/* bcwti
 *
 * Copyright (c) 2011 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.federation;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBArgComment1;
import wt.util.resource.RBArgComment2;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.federation.FederationLockResource")
public final class FederationLockResource_it extends WTListResourceBundle {

    @RBEntry("Input obbligatori mancanti: raccolta o repository degli input mancante")
    @RBComment("The message shown when Federation Lock or Unlock request is failed when required inputs are missing.")
    public static final String MISSING_REQUIRED_INPUTS = "0";

    @RBEntry("Input non valido: richiesta non riuscita. Uno o più oggetti di tipo diverso da RepositoryInteroperables trovati nell'input della richiesta.")
    @RBComment("The message shown when Federation Lock or Unlock request is failed when non 'RepositoryInteroperable' type objects are found in the request.")
    public static final String INVALID_INPUT_NON_REPOSITORY_INTEROPERABLES_FOUND = "1";

    @RBEntry("Input non valido: richiesta non riuscita. Uno o più oggetti non corrispondono all'ultima iterazione nell'input della richiesta.")
    @RBComment("The message shown when Federation Lock or Unlock request is failed when non latest iterations are found in the request.")
    public static final String INVALID_INPUT_NON_LATEST_ITERATIONS_FOUND = "2";

    @RBEntry("Input non valido: richiesta non riuscita. Uno o più oggetti nella richiesta sono bloccati localmente.")
    @RBComment("The message shown when Federation Lock or Unlock request is failed when locally locked objects are found.")
    public static final String INVALID_INPUT_OBJECTS_LOCALLY_LOCKED = "3";

    @RBEntry("Input non valido: richiesta non riuscita. Il repository della richiesta non è valido.")
    @RBComment("The message shown when Federation Lock or Unlock request is failed due to an invalid repository in the request.")
    public static final String INVALID_INPUT_REPOSITORY_INVALID = "4";

    @RBEntry("Input non valido: impossibile completare la richiesta. L'utente \"{0}\" non dispone di permessi sufficienti per uno o più oggetti.")
    @RBComment("The message shown when Federation Lock or Unlock request is failed when user does not have enough permissions on the objects.")
    @RBArgComment0("Name of user who is trying to lock or unlock.")
    public static final String INVALID_INPUT_UNMODIFIABLE_OBJECTS = "5";

    @RBEntry("Uno o più oggetti nella richiesta sono già bloccati da un altro repository.")
    @RBComment("The message shown when Federation Lock or Unlock request is failed when objects are locked by a different repository.")
    public static final String OBJECTS_FEDERATION_LOCKED_BY_DIFF_REPOSITORY = "6";

    @RBEntry("Richiesta non riuscita. Uno o più oggetti nella richiesta non sono bloccati.")
    @RBComment("The message shown when Federation Unlock request is failed when objects are not locked.")
    public static final String UNLOCKED_OBJECTS_FOUND = "7";

    @RBEntry("La parte \"{0}\" è bloccata dall'utente \"{1}\" per il repository \"{2}\" ")
    @RBComment("The message shown when Federation Lock request is failed when part is locked  by user for different repository.")
    @RBArgComment0("Name of Part that is already  locked")
    @RBArgComment1("Name of the user who has locked the part")
    @RBArgComment2("Name of the repository for which the part is locked")
    public static final String FAILED_LOCK_MSG_SUB_PART = "FAILED_LOCK_MSG_SUB_PART";

    @RBEntry("Operazione di blocco non riuscita. La parte \"{0}\" è bloccata dall'utente \"{1}\" per il repository \"{2}\" ")
    @RBComment("The message shown when Federation Lock  request is failed when part is locked  by user for different repository.")
    @RBArgComment0("Name of Part that is already  locked")
    @RBArgComment1("Name of the user who has locked the part")
    @RBArgComment2("Name of the repository for which the part is locked")
    public static final String FAILED_LOCK_MSG = "FAILED_LOCK_MSG";



}
