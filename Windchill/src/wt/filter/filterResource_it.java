/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.filter;

import wt.util.resource.*;

@RBUUID("wt.filter.filterResource")
public final class filterResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile eliminare il modello filtro \"{0}\" in quanto è in uso presso i prodotti o librerie che seguono: {1}.")
   @RBComment("Template deletion exception message with names of products/libraries where the template is used")
   @RBArgComment0("Filter Template name")
   @RBArgComment1("List of Products and Libraries where this Filter Template is registered ")
   public static final String NFT_DELETION_VETOED_WITH_NAMES = "101";

   @RBEntry("Impossibile eliminare il modello filtro \"{0}\" in quanto è in uso.")
   @RBComment("Filter Template deletion exception message")
   @RBArgComment0("filter template name")
   public static final String NFT_DELETION_VETOED = "102";

   @RBEntry("Impossibile creare il modello filtro \"{0}\" di tipo \"{1}\" in un contesto \"{2}\".")
   @RBComment("Filter Template creation exception message")
   @RBArgComment0("filter template name")
   @RBArgComment1("filter template type")
   @RBArgComment2("type of context (container)")
   public static final String NFT_CREATION_VETOED = "103";

   @RBEntry("Impossibile registrare il modello filtro \"{0}\" di tipo \"{1}\" presso un contesto \"{2}\".")
   @RBComment("Filter Template cannot be registered to certain context types, this is the failure message")
   @RBArgComment0("Filter Template Name")
   @RBArgComment1("filter template type")
   @RBArgComment2("type of context (container)")
   public static final String NFT_REGISTRATION_FAILED = "104";

   @RBEntry("Default di sistema")
   @RBComment("Default name for navigation criteria object")
   public static final String NAV_CRITERIA_DEFAULT_NAME = "105";

   @RBEntry("Impossibile eliminare la definizione di attributo \"{0}\" fornita dal sistema.")
   @RBComment("Error message to veto deletion of system-provided attribute definitions")
   @RBArgComment0("Attribute Definition Name")
   public static final String CANNOT_DELETE_EXCLUSION_ATTR_DEFINITION = "106";

   @RBEntry("Impossibile ignorare il modello filtro \"{0}\" di tipo \"{1}\" in un contesto di tipo \"{2}\".")
   @RBComment("The failure message specifying that the filter template cannot be overridden to certain context types")
   @RBArgComment0("filter template name")
   @RBArgComment1("filter template type")
   @RBArgComment2("type of context (container)")
   public static final String NFT_OVERRIDE_FAILED = "107";

   @RBEntry("Lo stato di condivisione dei criteri di espansione non può essere commutato dall'utente poiché non ne è il proprietario.")
   @RBComment("The message used when a user tries to toggle the share status on an expansion criteria the user does not own.")
   public static final String CANNOT_TOGGLE_SHARED = "CANNOT_TOGGLE_SHARED";

   @RBEntry("Filtro non valido impostato su NavigationCriteria. È supportato solo un filtro per tipo di classe.")
   @RBComment("The message used when two NavigationCriteria objecst are compared and their set of filters contain more than one filter type of the same class.")
   public static final String INVALID_FILTER_SET = "INVALID_FILTER_SET";

   @RBEntry("Specificare almeno un argomento non nullo.")
   @RBComment("The message used when two null NavigationCriteria objecst are passed to the compare method.")
   public static final String NON_NULL_ARG_REQIRED = "NON_NULL_ARG_REQIRED";
}
