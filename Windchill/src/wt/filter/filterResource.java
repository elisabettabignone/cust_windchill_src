/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.filter;

import wt.util.resource.*;

@RBUUID("wt.filter.filterResource")
public final class filterResource extends WTListResourceBundle {
   @RBEntry("Filter template \"{0}\" cannot be deleted because it is in use by the following products or libraries: {1}")
   @RBComment("Template deletion exception message with names of products/libraries where the template is used")
   @RBArgComment0("Filter Template name")
   @RBArgComment1("List of Products and Libraries where this Filter Template is registered ")
   public static final String NFT_DELETION_VETOED_WITH_NAMES = "101";

   @RBEntry("Filter Template \"{0}\" cannot be deleted because it is in use.")
   @RBComment("Filter Template deletion exception message")
   @RBArgComment0("filter template name")
   public static final String NFT_DELETION_VETOED = "102";

   @RBEntry("Filter Template \"{0}\" of type \"{1}\" cannot be created in the context of a \"{2}\".")
   @RBComment("Filter Template creation exception message")
   @RBArgComment0("filter template name")
   @RBArgComment1("filter template type")
   @RBArgComment2("type of context (container)")
   public static final String NFT_CREATION_VETOED = "103";

   @RBEntry("Filter Template \"{0}\" of type \"{1}\" cannot be registered to a context of type \"{2}\".")
   @RBComment("Filter Template cannot be registered to certain context types, this is the failure message")
   @RBArgComment0("Filter Template Name")
   @RBArgComment1("filter template type")
   @RBArgComment2("type of context (container)")
   public static final String NFT_REGISTRATION_FAILED = "104";

   @RBEntry("System Default")
   @RBComment("Default name for navigation criteria object")
   public static final String NAV_CRITERIA_DEFAULT_NAME = "105";

   @RBEntry("System provided attribute definition \"{0}\" can not be deleted")
   @RBComment("Error message to veto deletion of system-provided attribute definitions")
   @RBArgComment0("Attribute Definition Name")
   public static final String CANNOT_DELETE_EXCLUSION_ATTR_DEFINITION = "106";

   @RBEntry("Filter template \"{0}\" of type \"{1}\" cannot be overridden in a context of type \"{2}\".")
   @RBComment("The failure message specifying that the filter template cannot be overridden to certain context types")
   @RBArgComment0("filter template name")
   @RBArgComment1("filter template type")
   @RBArgComment2("type of context (container)")
   public static final String NFT_OVERRIDE_FAILED = "107";

   @RBEntry("The user cannot toggle the share status on the expansion criteria because the user does not own it.")
   @RBComment("The message used when a user tries to toggle the share status on an expansion criteria the user does not own.")
   public static final String CANNOT_TOGGLE_SHARED = "CANNOT_TOGGLE_SHARED";

   @RBEntry("Invalid filter set on NavigationCriteria.  Only 1 filter per class type is supported.")
   @RBComment("The message used when two NavigationCriteria objecst are compared and their set of filters contain more than one filter type of the same class.")
   public static final String INVALID_FILTER_SET = "INVALID_FILTER_SET";

   @RBEntry("At least one non-null argument must be supplied.")
   @RBComment("The message used when two null NavigationCriteria objecst are passed to the compare method.")
   public static final String NON_NULL_ARG_REQIRED = "NON_NULL_ARG_REQIRED";
}
