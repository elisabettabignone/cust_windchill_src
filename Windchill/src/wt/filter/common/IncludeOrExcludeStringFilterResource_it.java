/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.filter.common;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

/**
 * IncludeOrExcludeStringFilterResource
 *
 * <BR><BR><B>Supported API: </B>false
 * <BR><BR><B>Extendable: </B>false
 */
@RBUUID("wt.filter.common.IncludeOrExcludeStringFilterResource")
public class IncludeOrExcludeStringFilterResource_it extends WTListResourceBundle {

    @RBEntry("Non si dispone dei permessi per uno più oggetti nel filtro {0}.")
    @RBArgComment0("Organization identifier. For example, Organization ID, Duns ID, or CAGE Code.")
    public static final String NO_ACCESS_ORG_FILTER_WARNING = "NO_ACCESS_ORG_FILTER_WARNING";

    @RBEntry("Non si dispone dei permessi per uno più oggetti nel filtro contesto.")
    public static final String NO_ACCESS_CONTEXT_FILTER_WARNING = "NO_ACCESS_CONTEXT_FILTER_WARNING";
}
