/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */

package wt.enterprise;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.lang.ClassNotFoundException;
import java.lang.Object;
import java.lang.String;
import java.util.Properties;
import java.util.Vector;
import wt.enterprise.TemplateProcessor;
import wt.templateutil.processor.ContextHolder;
import wt.templateutil.processor.ContextTranslator;
import wt.templateutil.processor.HTTPState;
import wt.util.Evolvable;

import java.util.Iterator;
import java.util.HashMap;
import java.util.TreeMap;

import wt.access.AccessPermission;
import wt.access.AccessControlHelper;

import wt.admin.AdministrativeDomainHelper;
import wt.admin.DomainAdministered;

import wt.change2.Changeable2;

import wt.clients.homepage.WindchillHome;
import wt.clients.vc.VersionTaskLogic;
import wt.clients.vc.CheckInOutTaskLogic;

import wt.content.ApplicationData;
import wt.content.ContentHelper;
import wt.content.ContentHolder;
import wt.content.ContentItem;
import wt.content.DataFormat;
import wt.content.URLData;
import wt.content.Aggregate;
import wt.csm.businessentity.BusinessEntity;
import wt.dndMicroApplet.DnDMicroAppletUtil;
import wt.doc.WTDocument;

import wt.enterprise.tabularresults.StdTemplateProcessor;
import wt.epm.workspaces.EPMWorkspace;
import wt.epm.workspaces.WorkspaceFolder;

import wt.facade.suma.SumaFacade;

import wt.fc.BusinessInformation;
import wt.fc.EnumeratedType;
import wt.fc.IconDelegate;
import wt.fc.IconDelegateFactory;
import wt.fc.IdentityCollationKeyFactory;
import wt.fc.ObjectIdentifier;
import wt.fc.ObjectReference;
import wt.fc.Persistable;
import wt.fc.PersistenceHelper;
import wt.fc.ReferenceDelegate;
import wt.fc.ReferenceDelegateFactory;
import wt.fc.ReferenceFactory;
import wt.fc.QueryResult;
import wt.fc.WTObject;
import wt.fc.WTReference;
import wt.fc.ObjectReferenceQueryStringDelegate;

import wt.folder.Cabinet;
import wt.folder.CabinetBased;
import wt.folder.Folder;
import wt.folder.FolderCollationKeyFactory;
import wt.folder.Foldered;
import wt.folder.FolderEntry;
import wt.folder.FolderHelper;
import wt.folder.FolderServerHelper;
import wt.folder.FolderNotFoundException;
import wt.folder.Shortcut;
import wt.folder.ShortcutIconDelegate;
import wt.folder.SubFolder;

import wt.help.HelpLinkHelper;
import wt.htmlutil.HTMLTemplate;
import wt.htmlutil.HtmlUtil;
import wt.htmlutil.UrlAwareTextFormatter;
import wt.htmlutil.ProcessTemplate;
import wt.htmlutil.TemplateName;
import wt.htmlutil.TemplateOutputStream;
import wt.htmlutil.JavaScriptManager;

import wt.httpgw.GatewayServletHelper;
import wt.httpgw.HTTPRequest;
import wt.httpgw.GatewayURL;
import wt.httpgw.HTTPResponse;
import wt.httpgw.URLFactory;
import wt.httpgw.EncodingConverter;

import wt.iba.value.DefaultAttributeContainer;
import wt.iba.value.IBAHolder;
import wt.iba.value.IBAValueUtility;
import wt.iba.value.service.IBAValueHelper;
//import wt.iba.value.litevalue.*;
import wt.iba.value.litevalue.AbstractValueView;
import wt.iba.value.litevalue.AbstractValueViewCollationKeyFactory;
import wt.iba.value.litevalue.AbstractContextualValueDefaultView;
import wt.iba.value.litevalue.BooleanValueDefaultView;
import wt.iba.value.litevalue.IntegerValueDefaultView;
import wt.iba.value.litevalue.FloatValueDefaultView;
import wt.iba.value.litevalue.ReferenceValueDefaultView;
import wt.iba.value.litevalue.StringValueDefaultView;
import wt.iba.value.litevalue.TimestampValueDefaultView;
import wt.iba.value.litevalue.UnitValueDefaultView;
import wt.iba.value.litevalue.URLValueDefaultView;

import wt.inf.container.WTContainer;

import wt.introspection.ClassInfo;
import wt.introspection.WTIntrospector;
import wt.introspection.WTIntrospectionException;

import wt.lifecycle.history.HistoryProcessor;
import wt.lifecycle.State;
import wt.lifecycle.LifeCycleHelper;
import wt.lifecycle.LifeCycleManaged;

import wt.method.MethodContext;

import wt.org.OrganizationServicesHelper;
import wt.org.WTUser;
import wt.org.WTPrincipalReference;
import wt.org.WTPrincipal;

import wt.part.WTPart;
import wt.part.WTPartMaster;
import wt.part.WTPartHelper;
import wt.part.WTPartUsageLink;

import wt.pds.PartialResultException;

import wt.prefs.Preferences;
import wt.prefs.PreferenceHelper;
import wt.prefs.WTPreferences;
import wt.prefs.PrefsUtil;

import wt.projmgmt.admin.Project2;

import wt.query.QueryException;
import wt.query.QuerySpec;
import wt.query.commonsearch.ClientSearchHelper;

import wt.series.HarvardSeries;
import wt.services.applicationcontext.implementation.UnableToLoadServiceProperties;
import wt.session.SessionHelper;
import wt.sandbox.SandboxHelper;
import wt.templateutil.processor.NavBarURLActionDelegate;

import wt.units.IncompatibleUnitsException;
import wt.units.UnitFormatException;
import wt.units.Unit;
import wt.units.display.DefaultUnitRenderer;

import wt.util.CollationKeyFactory;
import wt.util.HTMLEncoder;
import wt.util.IconSelector;
import wt.util.InstalledProperties;
import wt.util.LocalizableMessage;
import wt.util.SortedEnumeration;
import wt.util.WTException;
import wt.util.WTRuntimeException;
import wt.util.WTContext;
import wt.util.WTMessage;
import wt.util.WTProperties;
import wt.util.WTPropertyVetoException;
import wt.util.SortedEnumeration;
import wt.util.WTStandardBooleanFormat;
import wt.util.WTStandardDateFormat;

import wt.vc.struct.StructHelper;
import wt.vc.config.MultipleLatestConfigSpec;
import wt.vc.config.ConfigHelper;
import wt.vc.config.ConfigSpec;
import wt.vc.views.View;
import wt.vc.views.ViewHelper;
import wt.vc.views.ViewManageable;

import wt.vc.Mastered;
import wt.vc.Iterated;
import wt.vc.IterationInfo;
import wt.vc.OneOffVersioned;
import wt.vc.OneOffVersionIdentifier;
import wt.vc.VersionInfo;
import wt.vc.VersionControlException;
import wt.vc.VersionControlHelper;
import wt.vc.Versioned;

import wt.vc.baseline.Baselineable;
import wt.vc.baseline.Baseline;
import wt.vc.baseline.BaselineHelper;

import wt.vc.views.View;

import wt.vc.wip.Workable;
import wt.vc.wip.WorkInProgressHelper;

import wt.viewmarkup.Viewable;

import wt.wvs.VisualizationHelperIfc;
import wt.wvs.VisualizationHelperFactory;

import wt.query.QueryCollationKeyFactory;
import wt.query.SearchCondition;

import wt.identity.IdentityFactory;
import wt.inf.container.WTContained;
import wt.inf.container.WTContainerHelper;
import wt.inf.container.WTContainerRef;

/*
 *  -----------------------------------------------------------------------------
 *  Imported Java Classes
 *  ----------------------------------------------------------------------------
 */
import java.beans.PropertyDescriptor;
import java.beans.PropertyVetoException;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.InputStream;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Serializable;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.net.URL;
import java.rmi.RemoteException;
import java.sql.Timestamp;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.text.DateFormat;

import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.TimeZone;

import wt.services.applicationcontext.UnableToCreateServiceException;

import wt.templateutil.components.HTMLComponentFactory;
//import wt.templateutil.components.HTMLComponentDelegate;
import wt.templateutil.components.HTMLComponent;

import wt.templateutil.processor.SubTemplateService;
import wt.templateutil.processor.ProcessorService;
import wt.templateutil.processor.PageContext;
import wt.templateutil.processor.DefaultHTMLTemplateFactory;
import wt.templateutil.processor.ContextBasedLocalizedResourceSrv;
import wt.templateutil.processor.VisualizationRendererFactory;
import wt.templateutil.processor.VisualizationRendererIfc;

import wt.templateutil.table.WTAttribute;
import wt.templateutil.table.TemplateProcessorTableService;
import wt.util.InstalledProperties;

import wt.units.display.DefaultUnitRenderer;
import wt.org.WTOrganization;
import wt.org.OrganizationOwned;
import wt.org.WTPrincipalReference;
import wt.session.SessionServerHelper;
import wt.access.AccessPermission;

import wt.enterprise.TemplateInfo;
import wt.enterprise.Templateable;
import wt.inf.sharing.SharedContainerMap;
import wt.inf.sharing.DataSharingHelper;

import wt.enterprise.EnterpriseHelper;

import wt.epm.EPMDocument;
import wt.epm.structure.EPMStructureHelper;

/**
 * Deprecation Notice: This class is still valid for this release, however
 * this serves as advance notice that it will be removed in the future.
 *  All user interfaces built using the Windchill HTML Template Processing
 * client architecture will be rewritten using a different framework in
 * an upcoming release.
 *
 * This abstract class implements several methods useful for template processing.
 *  It is suggested that most new template processors should extend {@link
 * wt.templateutil.processor.DefaultTemplateProcessor}.
 *
 *
 * <BR><BR><B>Supported API: </B>true
 * <BR><BR><B>Extendable: </B>false
 *
 * @deprecated
 *
 * @see "'Customizing the HTML Client' in 'Customizer's Guide'"
 *
 * @version   1.0
 **/
public abstract class BasicTemplateProcessor implements TemplateProcessor, ContextTranslator, ContextHolder, Evolvable {
   private static final String RESOURCE = "wt.enterprise.enterpriseResource";
   private static final String CLASSNAME = BasicTemplateProcessor.class.getName();
   private HTTPState theState;
   private String[] contextListIn = null;
   private String[] contextListOut = null;
   static final long serialVersionUID = 1;
   public static final long EXTERNALIZATION_VERSION_UID = 957977401221134810L;
   protected static final long OLD_FORMAT_VERSION_UID = 4531491789507101297L;

   /**
    *  Description of the Field
    */
   public final static String HOMEPAGE = "homepage";

   /**
    *  Description of the Field
    */
   public final static String WORKLIST = "worklist";

   /**
    *  Description of the Field
    */
   public final static String ENTERPRISE_SEARCH = "enterpriseSearch";

   /**
    *  Description of the Field
    */
   public final static String LOCAL_SEARCH = "localSearch";

   /**
    *  Description of the Field
    */
   public final static String INTEGRATED_SEARCH = "integratedSearch";

   /**
    *  Description of the Field
    */
   public final static String FEDERATION_SEARCH = "federationSearch";

   /**
    *  Description of the Field
    */
   public final static String IMPORT = "import";

   /**
    *  Description of the Field
    */
   public final static String PERSONAL_CABINET = "personalCabinet";

   /**
    *  Description of the Field
    */
   public final static String ALL_CABINETS = "allCabinets";

   /**
    *  Description of the Field
    */
   public final static String SEARCH = "search";

   /**
    *  Description of the Field
    */
   public final static String DOCFROMTEMPLATE = "docFromTemplate";

   /**
    *  Description of the Field
    */
   public final static String PDMLINK_INSTALL_KEY = "Windchill.PDMLink";
   private static boolean PDMLinkInstalled = false;

    public final static String WINDCHILLPDM_INSTALL_KEY ="Windchill.WindchillPDM";
    private static boolean WindchillPDM =false;
        public final static String PROJECTLINK_INSTALL_KEY = "Windchill.ProjectLink";
        private static boolean ProjectLinkInstalled = false;

   /**
    *  Description of the Field
    */
   protected static String ACTION = wt.enterprise.URLProcessor.ACTION;

   /**
    *  Description of the Field
    */
   protected static String CLASS = wt.enterprise.URLProcessor.CLASS;

   /**
    *  Description of the Field
    */
   protected static String OID = wt.enterprise.URLProcessor.OID;

   /**
    *  Description of the Field
    */
   protected static String BREAK_SEPARATOR = "@";

   /**
    *  Description of the Field
    */
   protected static String BREAK_TAG = "<BR>";

   private static String CODEBASE;
   private static boolean VERBOSE;
   private StdTemplateProcessor queryService = null;

   /**
    *  Description of the Field
    */
   public final static String CONTENT_RESOURCE = "wt.content.contentResource";

   /**
    *  Description of the Field
    */
   public final static String URL_RESOURCE = "wt.enterprise.UrlLinkResource";
   private final static String FEDERATION_RESOURCE = "wt.federation.federationResource";
   private static int MAXLENGTH;

   /**
    *  Description of the Field
    */
   protected String helpContext = null;

   /**
    *  Description of the Field
    */
   public final String HELP_CONTEXT = "HelpContext";

   /**
    *  Description of the Field
    */
   public final String HELP_LABEL = "HelpLabel";

   /**
    *  Description of the Field
    */
   public final String HELP_LABEL_RESOURCE = "HelpLabelResource";

   /**
    *  Description of the Field
    */
   public final String DEFAULT_HELP_LABEL = "DEFAULT_HELP_LABEL";

   /**
    *  Description of the Field
    */
   public final String HELP_WINDOW_NAME = "HELP_WINDOW_NAME";

   /**
    *  Description of the Field
    */
   protected static boolean helpIconEnabledDefault = false;

   /**
    *  Description of the Field
    */
   protected static boolean globalNavBarHelpIconEnabledDefault = false;

   /**
    *  Description of the Field
    */
   protected boolean helpIconEnabled = helpIconEnabledDefault;

   /**
    *  Description of the Field
    */
   protected static boolean isDndMicroAppletEnabled = false;

   /**
    *  Description of the Field
    */
   protected static boolean isDndMicroAppletEnabledInitialized = false;

   /**
    *  Description of the Field
    */
   public final String ADD_HELP_ICON = "ADD_HELP_ICON";

   /**
    *  Description of the Field
    */
   public final String HELP_ICON_SELECTOR = "HELP_ICON_SELECTOR";

   /**
    *  Description of the Field
    */
   public final String DEFAULT_HELP_ICON_SELECTOR = "HELP";

   /**
    *  Description of the Field
    */
   public final String PRESENT_HELP_LABEL = "PresentHelpLabel";

   /**
    *  Description of the Field
    */
   public final String HELP_ICON_POSITION = "HELP_ICON_POSITION";

   /**
    *  Description of the Field
    */
   public static String LABEL_SEPARATOR = "&";

   /**
    *  Description of the Field
    */
   public final static String LINE_FEED_DELIMITER = "^#@";

   /**
    *  Description of the Field
    */
   public final String PREFERENCE_CONTEXT = "prefContext";

   /**
    *  Description of the Field
    */
   public final String DEFAULT_PREFERENCE_CONTEXT = "PrefsNodeContent";

   /**
    *  Description of the Field
    */
   public final String PREF_WINDOW_NAME = "prefEditor";

   /**
    *  Description of the Field
    */
   public final static String TEXT_RESOURCE_BUNDLE = "textResourceBundle";

   /**
    *  Description of the Field
    */
   protected String contextClass = null;

   /**
    *  Description of the Field
    */
   protected final static String BLANK_SPACE = "&nbsp;";

   private static ResourceBundle urlLinkResource = null;
   private static Hashtable classInfos = null;

   /**
    *  A hashtable to store unique key-value pairs while displaying attributes.
    */
   private Hashtable uniqueAttributeKeyValues = null;

   private final static String HTML_GATEWAY = "wt.enterprise.URLProcessor";
   private final static String HTML_TEMPLATE_ACTION = "URLTemplateAction";

   private TemplateProcessorTableService htmlTableService = null;

   /**
    *  Description of the Field
    */
   public static String ENABLE_HELP_ICON_PROPERTY_NAME = "wt.htmlhelp.icon.enable";

   /**
    *  Description of the Field
    */
   public static String GLOBAL_NAVIGATION_ENABLE_HELP_ICON = "wt.globalNavigationBar.htmlhelp.icon.enable";

   /**
    *  Description of the Field
    */
   public static String WTCORE_IMAGE_DIR = "wtcore/images";

   /**
    *  Description of the Field
    */
   public static String SHARE_ICON = "shared.gif";
   private static String glyphImages[][] = new String[2][6];
   private static String DND_GLYPH_IMG;

   private static String DND_WIP_WORKCOPY_AND_CHKOUT_FROM_PDM_GLYPH = WTCORE_IMAGE_DIR + "/glyph/proj_avail_workcopy_dnd.gif";
   private static String DND_WIP_CHECKED_OUT_AND_CHECKED_FROM_PDM_GLYPH = WTCORE_IMAGE_DIR + "/glyph/proj_avail_chkout_dnd.gif";
   private static String DND_WIP_CHECKED_OUT_AND_SHARED_FROM_GLYPH = WTCORE_IMAGE_DIR + "/glyph/chkout_shared_in_dnd.gif";
   private static String DND_WIP_CHECKED_OUT_AND_SHARED_TO_GLYPH = WTCORE_IMAGE_DIR + "/glyph/chkout_shared_out_dnd_gif";
   private static String DND_WIP_CHECKED_OUT_AND_CHECKED_OUT_FROM_PDM_AND_SHARED_FROM_GLYPH = WTCORE_IMAGE_DIR + "/glyph/proj_avail_chkout_shared_in_dnd.gif";
   private static String DND_CHECKED_FROM_PDM_AND_SHARED_FROM_GLYPH = WTCORE_IMAGE_DIR + "/glyph/proj_avail_shared_in_dnd.gif";
   private final static String CHECKOUT_FOLDER = "Checked Out";

   // This is initially introduced for Pro/E Trail File support.

   /**
    *  Constant for ID key property key.
    */
   public final static String IDPREFIX = "idprefix";

   /**
    *  Description of the Field
    */
   protected String linkPrefix = null;

   /**
    *  Description of the Field
    */
   protected WTAttribute wtAttribute = null;

   /**
    *  Description of the Field
    */
   protected HTMLComponentFactory componentFactory = null;

   /**
    *  Description of the Field
    */
   protected PageContext pageContext = null;
   private static boolean visualizationEnabled = false;
   private static boolean templateCommentsEnabled = false;

   private static Method isWildfireIconDelegateMethod = null;

   /**
    *  Description of the Field
    */
   public final static String DEFAULT_CHARSET = "utf-8";
   private static String SECONDARY_EXCLUDE;

   static {
      try {
         WTProperties properties = WTProperties.getLocalProperties();
         VERBOSE = properties.getProperty("wt.template.verbose", false);
         CODEBASE = properties.getProperty("wt.server.codebase", "");
         helpIconEnabledDefault = properties.getProperty(ENABLE_HELP_ICON_PROPERTY_NAME, helpIconEnabledDefault);
         globalNavBarHelpIconEnabledDefault = properties.getProperty(GLOBAL_NAVIGATION_ENABLE_HELP_ICON, globalNavBarHelpIconEnabledDefault);
         classInfos = new Hashtable();
         MAXLENGTH = properties.getProperty("wt.content.urlLength.wrapLength", 40);
         PDMLinkInstalled = InstalledProperties.isInstalled(PDMLINK_INSTALL_KEY);
         WindchillPDM = InstalledProperties.isInstalled(WINDCHILLPDM_INSTALL_KEY);
                        ProjectLinkInstalled = InstalledProperties.isInstalled(PROJECTLINK_INSTALL_KEY);
         VisualizationHelperFactory visHelperFactory = new VisualizationHelperFactory();
         VisualizationHelperIfc visHelper = visHelperFactory.getHelper();
         visualizationEnabled = visHelper.isWVSEnabled();
         templateCommentsEnabled = properties.getProperty("wt.template.enableComments", false);
         SECONDARY_EXCLUDE = properties.getProperty("wt.content.secondaryContentExcludeList", "THUMBNAIL,THUMBNAIL3D,THUMBNAIL_SMALL");
         glyphImages[0][0] = properties.getProperty("wt.clients.shortcut_glyph.gif", "wt/clients/images/shortcut_glyph.gif");
         glyphImages[1][0] = properties.getProperty("wt.clients.shortcut_dnd_glyph", "wt/clients/images/shortcut_dnd_glyph.gif");
         glyphImages[0][1] = properties.getProperty("wt.clients.workingGlyph.gif", "wt/clients/images/workingcopy_glyph.gif");
         glyphImages[1][1] = properties.getProperty("wt.clients.workingcopy_dnd_glyph", "wt/clients/images/workingcopy_dnd_glyph.gif");
         glyphImages[0][2] = properties.getProperty("wt.clients.checkOutGlyph.gif", "wt/clients/images/checkout_glyph.gif");
         glyphImages[1][2] = properties.getProperty("wt.clients.checkout_dnd_glyph", "wt/clients/images/checkout_dnd_glyph.gif");
         glyphImages[0][3] = WTCORE_IMAGE_DIR + "/glyph/proj_avail.gif";
         glyphImages[1][3] = WTCORE_IMAGE_DIR + "/glyph/proj_avail_dnd.gif";
         glyphImages[0][4] = WTCORE_IMAGE_DIR + "/glyph/shared_out.gif";
         glyphImages[1][4] = WTCORE_IMAGE_DIR + "/glyph/shared_out_dnd.gif";
         glyphImages[0][5] = WTCORE_IMAGE_DIR + "/glyph/shared_in.gif";
         glyphImages[1][5] = WTCORE_IMAGE_DIR + "/glyph/shared_in_dnd.gif";

         DND_GLYPH_IMG = properties.getProperty("wt.clients.dnd_glyph.gif", "wt/clients/images/dnd_glyph.gif");

         try {
            //Use of introspection API to avoid dependency on wildfire classes
            Class wildfireIconDelegateHelperClass = Class.forName("wt.wildfire.WildfireActionDelegateHelper");
            Class args[] = {IconDelegate.class};
            isWildfireIconDelegateMethod = wildfireIconDelegateHelperClass.getMethod("isWildfireIconDelegate", args);
         } catch (Throwable t) {
            t.printStackTrace(System.err);
         }
      } catch (Throwable t) {
         Object[] param = {BasicTemplateProcessor.class.getName()};
         System.err.println(WTMessage.getLocalizedMessage(RESOURCE,
               enterpriseResource.ERROR_INITIALIZING,
               param));
         t.printStackTrace(System.err);
         throw new ExceptionInInitializerError(t);
      }
   }

   /**
    *  Description of the Field
    */
   public final static String CONTAINER_NAME = "containerName";

   /**
    * Writes the non-transient fields of this class to an external source.
    *
    * <BR><BR><B>Supported API: </B>false
    *
    * @deprecated
    *
    * @param     output
    * @exception java.io.IOException
    **/
   public void writeExternal( ObjectOutput output )
            throws IOException {
      output.writeLong(EXTERNALIZATION_VERSION_UID);
      output.writeObject(theState);

      if (!(output instanceof wt.pds.PDSObjectOutput)) {
         // for non-persistent fields
         output.writeObject(contextListIn);
         output.writeObject(contextListOut);
      }
   }

   /**
    * Reads the non-transient fields of this class from an external source.
    *
    * <BR><BR><B>Supported API: </B>false
    *
    * @deprecated
    *
    * @param     input
    * @exception java.io.IOException
    * @exception java.lang.ClassNotFoundException
    **/
   public void readExternal( ObjectInput input )
            throws IOException, ClassNotFoundException {
      long readSerialVersionUID = input.readLong();                  // consume UID
      readVersion( this, input, readSerialVersionUID, false, false );  // read fields
   }

   /**
    * Reads the non-transient fields of this class from an external source.
    *
    * <BR><BR><B>Supported API: </B>false
    *
    * @deprecated
    *
    * @param     thisObject
    * @param     input
    * @param     readSerialVersionUID
    * @param     passThrough
    * @param     superDone
    * @return    boolean
    * @exception java.io.IOException
    * @exception java.lang.ClassNotFoundException
    **/
   protected boolean readVersion( BasicTemplateProcessor thisObject, ObjectInput input, long readSerialVersionUID, boolean passThrough, boolean superDone )
            throws IOException, ClassNotFoundException {
      boolean success = true;

      if (readSerialVersionUID == EXTERNALIZATION_VERSION_UID) {
         // if current version UID

         theState = (HTTPState) input.readObject();

         if (!(input instanceof wt.pds.PDSObjectInput)) {
            // for non-persistent fields
            contextListIn = (String[]) input.readObject();
            contextListOut = (String[]) input.readObject();
         }
      } else {
         success = readOldVersion(input, readSerialVersionUID, passThrough, superDone);
      }

      return success;
   }

   /**
    * Reads the non-transient fields of this class from an external source,
    * which is not the current version.
    *
    * @param     input
    * @param     readSerialVersionUID
    * @param     passThrough
    * @param     superDone
    * @return    boolean
    * @exception java.io.IOException
    * @exception java.lang.ClassNotFoundException
    **/
   private boolean readOldVersion( ObjectInput input, long readSerialVersionUID, boolean passThrough, boolean superDone )
            throws IOException, ClassNotFoundException {
      boolean success = true;

      if ( readSerialVersionUID == OLD_FORMAT_VERSION_UID ) {          // handle previous version
         contextListIn = (String[])input.readObject();
         contextListOut = (String[])input.readObject();
         theState = (HTTPState)input.readObject();
      }
      else
         throw new java.io.InvalidClassException( CLASSNAME, "Local class not compatible:"
                           + " stream classdesc externalizationVersionUID=" + readSerialVersionUID
                           + " local class externalizationVersionUID=" + EXTERNALIZATION_VERSION_UID );

      return success;
   }

   /**
    * Gets the object for the association that plays role: theState.
    *
    * <BR><BR><B>Supported API: </B>false
    *
    * @deprecated
    *
    * @return    HTTPState
    **/
   public HTTPState getState() {
      if (theState == null) {
         theState = new HTTPState();
      }
      return theState;
   }

   /**
    * Sets the object for the association that plays role: theState.
    *
    * <BR><BR><B>Supported API: </B>false
    *
    * @deprecated
    *
    * @param     a_State
    **/
   public void setState( HTTPState a_State ) {
      theState = a_State;
   }

   /**
    * Gets the value of the attribute: contextListIn.
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @deprecated
    *
    * @return    String[]
    **/
   public String[] getContextListIn() {
      return contextListIn;
   }

   /**
    * Sets the value of the attribute: contextListIn.
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @deprecated
    *
    * @param     a_ContextListIn
    **/
   public void setContextListIn( String[] a_ContextListIn ) {
      contextListIn = a_ContextListIn;
   }

   /**
    * Gets the value of the attribute: contextListOut.
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @deprecated
    *
    * @return    String[]
    **/
   public String[] getContextListOut() {
      return contextListOut;
   }

   /**
    * Sets the value of the attribute: contextListOut.
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @deprecated
    *
    * @param     a_ContextListOut
    **/
   public void setContextListOut( String[] a_ContextListOut ) {
      contextListOut = a_ContextListOut;
   }

   /**
    * Gets the value of the attribute: contextAction; Context Action is
    * the name of the action of the current page
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @deprecated
    *
    * @return    String
    **/
   public String getContextAction() {
      try { return getState().getContextAction(); }
      catch (NullPointerException npe) { return null; }
   }

   /**
    * Sets the value of the attribute: contextAction; Context Action is
    * the name of the action of the current page
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @deprecated
    *
    * @param     a_ContextAction
    **/
   public void setContextAction( String a_ContextAction ) {
      getState().setContextAction( a_ContextAction );
   }

   /**
    * Gets the value of the attribute: contextObj; Context Object is the
    * reference object of the current page
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @deprecated
    *
    * @return    Object
    **/
   public Object getContextObj() {
      try { return getState().getContextObj(); }
      catch (NullPointerException npe) { return null; }
   }

   /**
    * Sets the value of the attribute: contextObj; Context Object is the
    * reference object of the current page
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @deprecated
    *
    * @param     a_ContextObj
    **/
   public void setContextObj( Object a_ContextObj ) {
      getState().setContextObj( a_ContextObj );
   }

   /**
    * Gets the value of the attribute: contextClassName; Context Object
    * Name is the name of the class of the current page
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @deprecated
    *
    * @return    String
    **/
   public String getContextClassName() {
      try { return getState().getContextClassName(); }
      catch (NullPointerException npe) { return null; }
   }

   /**
    * Sets the value of the attribute: contextClassName; Context Object
    * Name is the name of the class of the current page
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @deprecated
    *
    * @param     a_ContextClassName
    **/
   public void setContextClassName( String a_ContextClassName ) {
      getState().setContextClassName( a_ContextClassName );
   }

   /**
    * Gets the value of the attribute: contextProperties.
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @deprecated
    *
    * @return    Properties
    **/
   public Properties getContextProperties() {
      try { return getState().getContextProperties(); }
      catch (NullPointerException npe) { return null; }
   }

   /**
    * Sets the value of the attribute: contextProperties.
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @deprecated
    *
    * @param     a_ContextProperties
    **/
   public void setContextProperties( Properties a_ContextProperties ) {
      getState().setContextProperties( a_ContextProperties );
   }

   /**
    * Gets the value of the attribute: responseExceptions.
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @deprecated
    *
    * @return    Vector
    **/
   public Vector getResponseExceptions() {
      try { return getState().getResponseExceptions(); }
      catch (NullPointerException npe) { return null; }
   }

   /**
    * Sets the value of the attribute: responseExceptions.
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @deprecated
    *
    * @param     a_ResponseExceptions
    **/
   public void setResponseExceptions( Vector a_ResponseExceptions ) {
      getState().setResponseExceptions( a_ResponseExceptions );
   }

   /**
    * Gets the value of the attribute: responseFooters.
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @deprecated
    *
    * @return    Vector
    **/
   public Vector getResponseFooters() {
      try { return getState().getResponseFooters(); }
      catch (NullPointerException npe) { return null; }
   }

   /**
    * Sets the value of the attribute: responseFooters.
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @deprecated
    *
    * @param     a_ResponseFooters
    **/
   public void setResponseFooters( Vector a_ResponseFooters ) {
      getState().setResponseFooters( a_ResponseFooters );
   }

   /**
    * Gets the value of the attribute: responseHeaders.
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @deprecated
    *
    * @return    Vector
    **/
   public Vector getResponseHeaders() {
      try { return getState().getResponseHeaders(); }
      catch (NullPointerException npe) { return null; }
   }

   /**
    * Sets the value of the attribute: responseHeaders.
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @deprecated
    *
    * @param     a_ResponseHeaders
    **/
   public void setResponseHeaders( Vector a_ResponseHeaders ) {
      getState().setResponseHeaders( a_ResponseHeaders );
   }

   /**
    * Gets the value of the attribute: responseMessages.
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @deprecated
    *
    * @return    Vector
    **/
   public Vector getResponseMessages() {
      try { return getState().getResponseMessages(); }
      catch (NullPointerException npe) { return null; }
   }

   /**
    * Sets the value of the attribute: responseMessages.
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @deprecated
    *
    * @param     a_ResponseMessages
    **/
   public void setResponseMessages( Vector a_ResponseMessages ) {
      getState().setResponseMessages( a_ResponseMessages );
   }

   /**
    * Gets the value of the attribute: responseString.
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @deprecated
    *
    * @return    String
    **/
   public String getResponseString() {
      try { return getState().getResponseString(); }
      catch (NullPointerException npe) { return null; }
   }

   /**
    * Sets the value of the attribute: responseString.
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @deprecated
    *
    * @param     a_ResponseString
    **/
   public void setResponseString( String a_ResponseString ) {
      getState().setResponseString( a_ResponseString );
   }

   /**
    * Gets the value of the attribute: status.
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @deprecated
    *
    * @return    int
    **/
   public int getStatus() {
      try { return getState().getStatus(); }
      catch (NullPointerException npe) { return 0; }
   }

   /**
    * Sets the value of the attribute: status.
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @deprecated
    *
    * @param     a_Status
    **/
   public void setStatus( int a_Status ) {
      getState().setStatus( a_Status );
   }

   /**
    * Gets the value of the attribute: formData; Form Data is the data present
    * in the form of the current HTML page.
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @deprecated
    *
    * @return    Properties
    **/
   public Properties getFormData() {
      try { return getState().getFormData(); }
      catch (NullPointerException npe) { return null; }
   }

   /**
    * Sets the value of the attribute: formData; Form Data is the data present
    * in the form of the current HTML page.
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @deprecated
    *
    * @param     a_FormData
    **/
   public void setFormData( Properties a_FormData ) {
      getState().setFormData( a_FormData );
   }

   /**
    * Gets the value of the attribute: queryData; Query Data is the information
    * present after the "?" mark on the URL of the current page.
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @deprecated
    *
    * @return    Properties
    **/
   public Properties getQueryData() {
      try { return getState().getQueryData(); }
      catch (NullPointerException npe) { return null; }
   }

   /**
    * Sets the value of the attribute: queryData; Query Data is the information
    * present after the "?" mark on the URL of the current page.
    *
    * <BR><BR><B>Supported API: </B>true
    *
    * @deprecated
    *
    * @param     a_QueryData
    **/
   public void setQueryData( Properties a_QueryData ) {
      getState().setQueryData( a_QueryData );
   }

   /**
    * Reads the non-transient fields of this class from an external source.
    *
    * @param     input
    * @param     readSerialVersionUID
    * @param     superDone
    * @return    boolean
    * @exception java.io.IOException
    * @exception java.lang.ClassNotFoundException
    **/
   private boolean readVersion957977401221134810L( ObjectInput input, long readSerialVersionUID, boolean superDone )
            throws IOException, ClassNotFoundException {
      if ( !( input instanceof wt.pds.PDSObjectInput ) ) {           // for non-persistent fields
         contextListIn = (String[])input.readObject();
         contextListOut = (String[])input.readObject();
         theState = (HTTPState)input.readObject();
      }

      return true;
   }

   /**
    *  Get a <code>PrintWriter</code> for the given output stream and locale.
    *  Template processing uses a special output stream that allows reuse of a
    *  shared print writer and writes to the output stream using a character
    *  encoding determined from the locale of the template file resource.
    *  Therefore, all template processor methods should call this method when they
    *  need a print writer for writing text to their output stream. <BR>
    *  <BR>
    *  <B>Supported API: </B> true
    *
    *@param  out     the output stream passed to the processing method
    *@param  locale  the locale passed to the processing method
    *@return         PrintWriter the print writer to use for writing text to the
    *      output stream
    */
   public static PrintWriter getLocalizedPrintWriter(OutputStream out, Locale locale) {
      return TemplateOutputStream.getPrintWriter(out, locale);
   }

   /**
    *  Get a <code>PrintWriter</code> for the given output stream and locale.
    *  Template processing uses a special output stream that allows reuse of a
    *  shared print writer and writes to the output stream using a character
    *  encoding determined from the locale of the template file resource.
    *  Therefore, all template processor methods should call this method when they
    *  need a print writer for writing text to their output stream. <BR>
    *  <BR>
    *  <B>Supported API: </B> true
    *
    *@param  out     the output stream passed to the processing method
    *@param  locale  the locale passed to the processing method
    *@return         PrintWriter the print writer to use for writing text to the
    *      output stream
    */
   public PrintWriter getPrintWriter(OutputStream out, Locale locale) {
      return TemplateOutputStream.getPrintWriter(out, locale);
   }

   /**
    *  Get character encoding that should be used with the template processing
    *  output stream. <BR>
    *  <BR>
    *  <B>Supported API: </B> true
    *
    *@param  out     the output stream passed to the processing method
    *@param  locale  the locale passed to the processing method
    *@return         the encoding name
    */
   public String getEncoding(OutputStream out, Locale locale) {
      return TemplateOutputStream.getEncoding(out, locale);
   }

   /**
    *  <A NAME=objectIdentification></A> Produces HTML which represents
    *  identification information for the contextual object of a
    *  BasicTemplateProcessor. <p>
    *
    *  It is expected that this method will be invoked as a result of an
    *  objectIdentification script call in an HTML template file. The format of
    *  objectIdentification script calls is: <p>
    *
    *
    *  <DL>
    *    <DD> <STRONG>objectIdentification</STRONG> [mode="Brief"|"Verbose"]
    *    [includeURL="url-property-name"]
    *    <DL>
    *      <DT> <STRONG>mode</STRONG> <BR>
    *
    *      <DD> The mode parameter determines the detail provided by the template
    *      file. It defaults to "Verbose".
    *      <DT> <STRONG>includeURL</STRONG> <BR>
    *
    *      <DD> The includeURL parameter is not currently defined. It defaults to
    *      "HtmlView".
    *    </DL>
    *
    *  </DL>
    *  <p>
    *
    *
    *  <DL>
    *    <DT> <STRONG>Example:</STRONG> <BR>
    *
    *    <DD> objectIdentification mode=Brief
    *  </DL>
    *  <p>
    *
    *  The format of the HTML that is output is determined by an HTML template
    *  file. The template that is used is chosen from the templates directory
    *  based on the class of the contextual object, the locale and the parameters
    *  passed to the objectIdentification script call within a higher level HTML
    *  template. Currently the template file is chosen from either the
    *  ObjectIdentificationVerbose or ObjectIdentificationBrief directories.
    *  Within those directories the template is chosen on the basis of: <PRE>
    *   1. the class name of the context object
    *   2. the wt.enterprise super class of the context object
    *   3. the default template
    * </PRE> <p>
    *
    *  <BR>
    *  <BR>
    *  <B>Supported API: </B> true
    *
    *@param  parameters       The arguments that were passed to the Windchill
    *      script call.
    *@param  locale           The Locale to send to the invoked methods for
    *      localization.
    *@param  os               The output stream.
    *@exception  WTException  Description of the Exception
    */
   public void objectIdentification(Properties parameters, Locale locale, OutputStream os)
          throws WTException {
      // xxx The default values shouldn't be hardcoded here.

      if (parameters != null) {
         // If a particular parameter is not set, then we will set the default.
         parameters.put("mode",
               parameters.getProperty("mode", "Verbose"));
         parameters.put("includeURL",
               parameters.getProperty("includeURL", "HtmlView"));
      } else {
         parameters = new Properties();
         // Default values for parameters.
         parameters.put("mode", "Verbose");
         parameters.put("includeURL", "HtmlView");
      }

      String mode = parameters.getProperty("mode");
      HTMLTemplate template = getOITemplate(mode, locale);

      template.process(os, this);
   }

   /**
    *  Return an initialized HTMLTemplate.
    *
    *@param  mode             Description of the Parameter
    *@param  locale           Description of the Parameter
    *@return                  The oITemplate value
    *@exception  WTException  Description of the Exception
    */
   private HTMLTemplate getOITemplate(String mode, Locale locale)
          throws WTException {
      HTMLTemplate result = null;
      String templateName = getOITemplateName(mode);

      if (VERBOSE) {
         System.out.println("TemplateName -> " + templateName);
      }

      result =
            new HTMLTemplate(templateName);
      if (VERBOSE) {
         System.out.println("template -> " + result);
         System.out.println("ORE");
         System.out.println("locale -> " + locale);
      }

      if (locale == null) {
         result.init();
      } else {
         result.init(locale, true);
      }
      return result;
   }

   /*
    *
    */

   /**
    *  Gets the oITemplateName attribute of the BasicTemplateProcessor object
    *
    *@param  mode  Description of the Parameter
    *@return       The oITemplateName value
    */
   private String getOITemplateName(String mode) {
      String result = null;
      File file;

      /*
       *  This is hokey, but...
       *  We have three different steps to find a template.
       *  1. class name
       *  2. instance of enterprise classes
       *  3. "default"
       *
       *  This should all be replaced by the resource service stuff
       *  eventually.
       */
      // 1. Try the class name.
      String fullClassName = getContextObj().getClass().getName();
      int dotIndex = fullClassName.lastIndexOf(".");
      String className = fullClassName.substring(dotIndex + 1);
      String fileName = className;
      if (mode.equals("Verbose")) {
         /*
          *  result =
          *  TemplateName.getObjectIdentificationVerbose(fileName);
          */
         result = "templates/ObjectIdentificationVerbose/" + className;
      } else {
         /*
          *  result =
          *  TemplateName.getObjectIdentificationBrief(fileName);
          */
         result = "templates/ObjectIdentificationBrief/" + className;
      }

      String tmpResult = "/" + result + ".html";
      InputStream templateStr =
            WTContext.getContext().getResourceAsStream(tmpResult);
      if (templateStr == null) {
         if (VERBOSE) {
            System.out.println("OID class name template does not exist -> " +
                  tmpResult);
         }
      } else {
         if (VERBOSE) {
            System.out.println("OID class name template EXISTS -> " +
                  result);
         }
         return result;
      }

      // 2. Try the enterprise classes.
      if (getContextObj() instanceof Simple) {
         fileName = "Simple";
      } else if (getContextObj() instanceof FolderResident) {
         fileName = "FolderResident";
      } else if (getContextObj() instanceof Managed) {
         fileName = "Managed";
      } else if (getContextObj() instanceof RevisionControlled) {
         fileName = "RevisionControlled";
      } else if (getContextObj() instanceof Mastered) {
         fileName = "Mastered";
      } else {
         // 3. default
         fileName = "default";
      }

      if (mode.equals("Verbose")) {
         result =
               TemplateName.getObjectIdentificationVerbose(fileName);
      } else {
         result =
               TemplateName.getObjectIdentificationBrief(fileName);
      }

      return result;
   }

   /**
    *  <A NAME=objectPropertyValue></A> Outputs HTML which represents the value of
    *  a property. The set of available properties are those that can be retrieved
    *  by the WTIntrospector class. <p>
    *
    *  It is expected that this method will be invoked as a result of an
    *  objectPropertyValue script call in an HTML template file. The format of
    *  objectPropertyValue script calls is: <p>
    *
    *
    *  <DL>
    *    <DD> <STRONG>objectPropertyValue</STRONG> propertyName="propertyName"
    *
    *    <DL>
    *      <DT> <STRONG>propertyName</STRONG> <BR>
    *
    *      <DD> The propertyName parameter is the name of a property.
    *    </DL>
    *
    *  </DL>
    *  <p>
    *
    *
    *  <DL>
    *    <DT> <STRONG>Example:</STRONG> <BR>
    *
    *    <DD> objectPropertyValue propertyName=requester
    *  </DL>
    *  <p>
    *
    *  The format of the HTML that is output is dependent on the type of the
    *  property value.
    *  <BR> Caution: This method will return "&amp;nbsp;" for null
    *  property values.  Also, if the MethodContext contains the property
    *  "preserveFormatting=true", characters with special meaning or treatment
    *  in HTML will be encoded to display properly in browsers.  For example,
    * '>' will be encoded as "&amp;gt;" and a carriage return will be encoded
    *  as "&lt;BR&gt;."  The preserveFormatting flag can be set using the
    *  Windchill script method DefaultTemplateProcessor.setMethodContextProperty().<p>
    *
    *  <BR>
    *  <BR>
    *  <B>Supported API: </B> true
    *
    *@param  parameters                                       The arguments that
    *      were passed to the Windchill script call.
    *@param  locale                                           The Locale to send
    *      to the invoked methods for localization.
    *@param  os                                               The output stream.
    *@exception  IllegalAccessException                       Description of the
    *      Exception
    *@exception  java.lang.reflect.InvocationTargetException  Description of the
    *      Exception
    *@exception  WTIntrospectionException                     Description of the
    *      Exception
    *@exception  WTException                                  Description of the
    *      Exception
    */
   public void objectPropertyValue(Properties parameters, Locale locale, OutputStream os)
          throws IllegalAccessException,
         java.lang.reflect.InvocationTargetException,
         WTIntrospectionException, WTException {
      if (parameters == null) {
         Object[] param = {"property name"};
         throw new WTException(null, RESOURCE,
               enterpriseResource.NOT_SPECIFIED, param);
      }
      objectPropertyValueAux(parameters, locale, getPrintWriter(os, locale));
   }

   /**
    *  A utility method that implements some of the basic functionality needed by
    *  objectPropertyValue(). <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  parameters                                       Description of the
    *      Parameter
    *@param  locale                                           Description of the
    *      Parameter
    *@param  out                                              Description of the
    *      Parameter
    *@exception  java.lang.IllegalAccessException             Description of the
    *      Exception
    *@exception  java.lang.reflect.InvocationTargetException  Description of the
    *      Exception
    *@exception  wt.introspection.WTIntrospectionException    Description of the
    *      Exception
    *@exception  wt.util.WTException                          Description of the
    *      Exception
    */
   public void objectPropertyValueAux(Properties parameters, Locale locale,
         PrintWriter out)
          throws java.lang.IllegalAccessException,
         java.lang.reflect.InvocationTargetException,
         wt.introspection.WTIntrospectionException,
         wt.util.WTException {
      String propertyName = parameters.getProperty("propertyName");
      objectPropertyValueAux2(parameters, locale, out, propertyName);
   }

   /**
    *  A utility method that implements some of the basic functionality needed by
    *  objectPropertyValue(). <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  parameters                                       Description of the
    *      Parameter
    *@param  locale                                           Description of the
    *      Parameter
    *@param  out                                              Description of the
    *      Parameter
    *@param  propertyName                                     Description of the
    *      Parameter
    *@exception  java.lang.IllegalAccessException             Description of the
    *      Exception
    *@exception  java.lang.reflect.InvocationTargetException  Description of the
    *      Exception
    *@exception  wt.introspection.WTIntrospectionException    Description of the
    *      Exception
    *@exception  wt.util.WTException                          Description of the
    *      Exception
    */
   public void objectPropertyValueAux2(Properties parameters, Locale locale,
         PrintWriter out, String propertyName)
          throws java.lang.IllegalAccessException,
         java.lang.reflect.InvocationTargetException,
         wt.introspection.WTIntrospectionException,
         wt.util.WTException {
      String value;

      /*
       *  AAR 18/12/2001 :
       *  Added Exception handling to suppress the Exception
       *  thrown  when  java.lang.Object  is  passed  as the
       *  Context Object.
       */
      try {
         if (null != getContextObj()) {
            ClassInfo contextInfo =
                  WTIntrospector.getClassInfo(getContextObj().getClass());
            value = objectPropertyValueString(propertyName, parameters, locale);
            if (value == null) {
               value = "";
            }
         } else {
            value = "[ " +
                  WTMessage.getLocalizedMessage(RESOURCE, enterpriseResource.NULL_CONTEXT_OBJECT,
                  null, locale)
                   + " ]";
         }
      } catch (Exception e) {
         if (VERBOSE) {
            e.printStackTrace();
         }
         return;
      }
      out.println(HTMLEncoder.encodeForHTMLContent(value));
      out.flush();
   }

   /**
    *  A utility method that implements some of the basic functionality needed by
    *  objectPropertyValue(). <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  propertyName                                   Description of the
    *      Parameter
    *@param  parameters                                     Description of the
    *      Parameter
    *@param  locale                                         Description of the
    *      Parameter
    *@return                                                Description of the
    *      Return Value
    *@exception  wt.introspection.WTIntrospectionException  Description of the
    *      Exception
    *@exception  wt.util.WTException                        Description of the
    *      Exception
    */
   protected String objectPropertyValueString(String propertyName,
         Properties parameters,
         Locale locale)
          throws wt.introspection.WTIntrospectionException,
         wt.util.WTException {
      boolean warn = false;
      if (parameters != null) {
         String warn_str = parameters.getProperty("warn");
         if ((warn_str != null) &&
               (warn_str.equals("true"))) {
            warn = true;
         }
      }

      return getObjectPropertyValueString(propertyName, parameters,
            locale, getContextObj(), warn);
   }

   /**
    *  A utility method that implements some of the basic functionality needed by
    *  objectPropertyValue(). This method can be called in the java code of
    *  TemplateProcessors to retrieve the value of an attribute associated with
    *  the context object of that TemplateProcessor. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  propertyName                  Description of the Parameter
    *@param  parameters                    Description of the Parameter
    *@param  locale                        Description of the Parameter
    *@param  obj                           Description of the Parameter
    *@return                               The objectPropertyValueString value
    *@exception  WTIntrospectionException  Description of the Exception
    *@exception  WTException               Description of the Exception
    */
   protected String getObjectPropertyValueString(String propertyName, Properties parameters,
         Locale locale, Object obj)
          throws WTIntrospectionException, WTException {
      return getObjectPropertyValueString(propertyName, parameters, locale, obj, false);
   }

   /**
    *  Returns the String value of the attribute specified by the given property
    *  name on the given object. Additional information on how to return the
    *  property may optionally be given in the <CODE>Properties</CODE> parameter.
    *  For example, if the given property has a type of VersionInfo, whether or
    *  not the returned version information contains the iteration can be
    *  specified in the Properties object. The following are key-value pairs
    *  currently checked: Property Type Key Value Result versionInfo version_info
    *  qualified contains the iteration WTPrincipalReference email true principal
    *  name is returned as link to e-mail <BR>
    *  <BR>
    *  Caution: This method will return "&amp;nbsp;" for null
    *  property values.  Also, if the MethodContext contains the property
    *  "preserveFormatting=true", characters with special meaning or treatment
    *  in HTML will be encoded to display properly in browsers.  For example,
    * '>' will be encoded as "&amp;gt;" and a carriage return will be encoded
    *  as "&lt;BR&gt;."  The preserveFormatting flag can be set using the
    *  Windchill script method DefaultTemplateProcessor.setMethodContextProperty().
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  propertyName                                   the name of the
    *      property whose value is returned
    *@param  parameters                                     a <CODE>Properties</CODE>
    *      object containing key-value pairs which provide more information in
    *      retrieving the property value.
    *@param  locale                                         the Locale in which
    *      the result should be formatted
    *@param  obj                                            the object from which
    *      the property value is retrieved
    *@param  warn                                           if true, when an
    *      exception occurs, the returned string will contain the text of the
    *      exception.
    *@return                                                The
    *      objectPropertyValueString value
    *@exception  wt.introspection.WTIntrospectionException  Description of the
    *      Exception
    *@exception  wt.util.WTException                        Description of the
    *      Exception
    */
   protected String getObjectPropertyValueString(String propertyName, Properties parameters,
         Locale locale, Object obj, boolean warn)
          throws wt.introspection.WTIntrospectionException,
         wt.util.WTException {
      /*
       *  ObjectPropertyValue objPropValue = new ObjectPropertyValue();
       *  return objPropValue.getObjectPropertyValueString( propertyName, parameters,
       *  locale, obj, warn );
       */
      if (parameters == null) {
         parameters = new Properties();
      }
      if (VERBOSE) {
         System.err.println("\n BasicTemplateProcessor - getObjectPropertyValueString : propertyName = " + propertyName);
      }
      String mode = parameters.getProperty("mode");
      if (mode == null || mode.length() == 0) {
         mode = HTMLComponent.VIEW;
      }

      String serviceSelector = parameters.getProperty("serviceSelector");
      if (VERBOSE) {
         System.err.println("\n BasicTemplateProcessor - addHTMLComponent :serviceSelector = " + serviceSelector);
      }

      HTMLComponent component = null;
      Object value = obj;

      if (VERBOSE) {
         System.err.println("\n BasicTemplateProcessor - addHTMLComponent : value = " + value);
      }
      Object selectObject = value;
      WTAttribute wtAttribute = null;

      wtAttribute = getWTAttribute();
      wtAttribute.setLocale(locale);
      wtAttribute.init(value, propertyName);
      value = wtAttribute;
      selectObject = value;
      //((WTAttribute)value).getDisplayClass();
      if (selectObject == null) {
         selectObject = value;
      }
      if (VERBOSE) {
                 System.err.println("\n BasicTemplateProcessor - addHTMLComponent : selectObject = " + selectObject.getClass ().getName ());
         System.err.println("\n BasicTemplateProcessor - addHTMLComponent : value instanceof WTObject");
         System.err.println("\n BasicTemplateProcessor - addHTMLComponent : selectObject = " + selectObject);
      }

      if (serviceSelector == null || serviceSelector.length() == 0 || serviceSelector.equals("")) {
         serviceSelector = null;
      }

      component = (HTMLComponent) getHTMLComponentFactory().getComponent(serviceSelector, selectObject);
      if (VERBOSE) {
         System.err.println("\n BasicTemplateProcessor - addHTMLComponent : component = " + component);
      }

      component.init(serviceSelector, value, getHTMLComponentFactory(), mode, parameters);

      if (getFormData() == null) {
         setFormData(new Properties());
      }
      for (Enumeration enumeration = parameters.keys(); enumeration.hasMoreElements(); ) {
         String key = (String) enumeration.nextElement();
         if (getFormData() != null) {
            getFormData().put(key, parameters.getProperty(key));
         }
      }
      getFormData().put(JavaScriptManager.ID, getJavaScriptManager());
      return component.show(value, getFormData(), null, locale);
   }

   /**
    *  Utility method to remove any enclosing quotes from the given String.
    *
    *@param  value  the String from which to remove quotes
    *@return        the String with any enclosing quotes removed.
    */
   private String removeEnclosingQuotes(String value) {
      // Remove any double quotes from around the date.
      if (value.startsWith("\"") == true) {
         value = value.substring(1);
         value = value.substring(0, value.length() - 1);
      }
      return value;
   }

   /**
    *  <A NAME=objectPropertyName></A> Outputs HTML which represents the display
    *  name of a property. The set of available properties are those that can be
    *  retrieved by the WTIntrospector class. <p>
    *
    *  It is expected that this method will be invoked as a result of an
    *  objectPropertyName script call in an HTML template file. The format of
    *  objectPropertyName script calls is: <p>
    *
    *
    *  <DL>
    *    <DD> <STRONG>objectPropertyName</STRONG> propertyName="propertyName"
    *    <DL>
    *      <DT> <STRONG>propertyName</STRONG> <BR>
    *
    *      <DD> The propertyName parameter is the name of a property.
    *    </DL>
    *
    *  </DL>
    *  <p>
    *
    *
    *  <DL>
    *    <DT> <STRONG>Example:</STRONG> <BR>
    *
    *    <DD> objectPropertyName propertyName=requester
    *  </DL>
    *  <p>
    *
    *  <BR>
    *  <BR>
    *  <B>Supported API: </B> true
    *
    *@param  parameters                                       The arguments that
    *      were passed to the Windchill script call.
    *@param  locale                                           The Locale to send
    *      to the invoked methods for localization.
    *@param  os                                               The output stream.
    *@exception  java.lang.IllegalAccessException             Description of the
    *      Exception
    *@exception  java.lang.reflect.InvocationTargetException  Description of the
    *      Exception
    *@exception  wt.introspection.WTIntrospectionException    Description of the
    *      Exception
    *@exception  wt.util.WTException                          Description of the
    *      Exception
    */
   public void objectPropertyName(Properties parameters, Locale locale,
         OutputStream os)
          throws java.lang.IllegalAccessException,
         java.lang.reflect.InvocationTargetException,
         wt.introspection.WTIntrospectionException,
         wt.util.WTException {
      if (parameters == null) {
         Object[] param = {"property name"};
         throw new WTException(null, RESOURCE,
               enterpriseResource.NOT_SPECIFIED, param);
      }

      objectPropertyNameAux(parameters, locale, getPrintWriter(os, locale));
   }

   /**
    *  A utility method that implements some of the basic functionality needed by
    *  objectPropertyName(). <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  parameters                                     Description of the
    *      Parameter
    *@param  locale                                         Description of the
    *      Parameter
    *@param  out                                            Description of the
    *      Parameter
    *@exception  wt.introspection.WTIntrospectionException  Description of the
    *      Exception
    */
   public void objectPropertyNameAux(Properties parameters, Locale locale,
         PrintWriter out)
          throws wt.introspection.WTIntrospectionException {
      /*
       *  throws java.lang.IllegalAccessException,
       *  java.lang.reflect.InvocationTargetException,
       *  wt.introspection.WTIntrospectionException,
       *  wt.util.WTException
       */
      String propertyName = parameters.getProperty("propertyName");
      objectPropertyNameAux2(parameters, locale, out, propertyName);
   }

   /**
    *  A utility method that implements some of the basic functionality needed by
    *  objectPropertyName(). <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  parameters    Description of the Parameter
    *@param  locale        Description of the Parameter
    *@param  out           Description of the Parameter
    *@param  propertyName  Description of the Parameter
    */
   public void objectPropertyNameAux2(Properties parameters, Locale locale,
         PrintWriter out, String propertyName) {
      /*
       *  throws java.lang.IllegalAccessException,
       *  java.lang.reflect.InvocationTargetException,
       *  wt.introspection.WTIntrospectionException,
       *  wt.util.WTException
       */
      if (parameters == null) {
         if (VERBOSE) {
            System.err.println("\n BasicTemplateProcessor - addHTMLComponentName : parameters == null");
         }
         return;
      }
      String attribute = parameters.getProperty("propertyName");
      if (attribute == null || attribute.length() == 0) {
         if (VERBOSE) {
            System.err.println("\n BasicTemplateProcessor - addHTMLComponentName : attribute == null || attribute.length() == 0 ");
         }
         return;
      }
      String mode = parameters.getProperty("mode");
      if (mode == null || mode.length() == 0) {
         mode = HTMLComponent.VIEW;
      }
      String serviceSelector = parameters.getProperty("serviceSelector");
      if (VERBOSE) {
         System.err.println("\n BasicTemplateProcessor - addHTMLComponentName :serviceSelector = " + serviceSelector);
      }
      HTMLComponent component = null;
      Object value = null;
      if (getContextObj() != null) {
         value = getContextObj();
      } else if (getContextClassName() != null) {
         value = getContextClassName();
      } else if ( parameters.getProperty("class") != null ) {
         value = parameters.getProperty("class");
      } else {
         value = new Object();
      }
      if (VERBOSE) {
         System.err.println("\n BasicTemplateProcessor - addHTMLComponentName : value = " + value);
      }
      WTAttribute wtAttribute = null;

      wtAttribute = getWTAttribute();
      wtAttribute.setLocale(locale);
      wtAttribute.init(value, attribute);
      value = wtAttribute;
      if (VERBOSE) {
         System.err.println("\n BasicTemplateProcessor - addHTMLComponentName : value instanceof WTObject");
         System.err.println("\n BasicTemplateProcessor - addHTMLComponentName : value = " + value);
      }

      if (serviceSelector == null || serviceSelector.length() == 0 || serviceSelector.equals("")) {
         serviceSelector = "NAME";
      }

      component = (HTMLComponent) getHTMLComponentFactory().getComponent(serviceSelector, value);

      if (VERBOSE) {
         System.err.println("\n BasicTemplateProcessor - addHTMLComponentName : component = " + component);
      }

      component.init(serviceSelector, value, getHTMLComponentFactory(), mode, parameters);
      out.print(component.show(value, getFormData(), null, locale));
      out.flush();
   }

   /**
    *  A utility method for use during development and testing. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  parameters                                       Description of the
    *      Parameter
    *@param  locale                                           Description of the
    *      Parameter
    *@param  os                                               Description of the
    *      Parameter
    *@exception  java.lang.IllegalAccessException             Description of the
    *      Exception
    *@exception  java.lang.reflect.InvocationTargetException  Description of the
    *      Exception
    *@exception  wt.introspection.WTIntrospectionException    Description of the
    *      Exception
    *@exception  wt.util.WTException                          Description of the
    *      Exception
    */
   public void objectProperties(Properties parameters, Locale locale,
         OutputStream os)
          throws java.lang.IllegalAccessException,
         java.lang.reflect.InvocationTargetException,
         wt.introspection.WTIntrospectionException,
         wt.util.WTException {
      String display = null;
      try {
         display = parameters.getProperty("display");
      } catch (java.lang.NullPointerException e) {
         display = "false";
      }

      /*
       *  AAR 18/12/2001 :
       *  If display is NULL, return.
       */
      if (display == null) {
         return;
      }

      if (VERBOSE || (display.equals("true"))) {
         if (VERBOSE) {
            System.out.println("Entering objectProperties()...");
            System.out.println("  parameters -> " + parameters);
         }
         PrintWriter out = getPrintWriter(os, locale);

         out.println("<HR WIDTH=\"100%\">");
         out.println("<p>");

         Object contextObject = getContextObj();
         ClassInfo contextInfo =
               WTIntrospector.getClassInfo(contextObject.getClass());
         PropertyDescriptor[] properties = contextInfo.getPropertyDescriptors();
         for (int i = 0; i < properties.length; i++) {
            if (VERBOSE) {
               System.out.println("  prop: " + properties[i].getName());
            }
            out.println("<B>");
            objectPropertyNameAux2(parameters, locale, out, properties[i].getName());
            out.println("</B> (");
            out.println(HTMLEncoder.encodeForHTMLContent((String)properties[i].getValue(WTIntrospector.DEFINED_AS)));
            out.println("): ");

            objectPropertyValueAux2(parameters, locale, out,
                  properties[i].getName());
            out.println("<P>");
            out.flush();
         }
      }
   }

   /**
    *  <A NAME=objectActionLink></A> Outputs an HTML HREF with a URL to the
    *  wt.enterprise.URLProcessor class and its URLTemplateAction method. The URL
    *  will be created with two or three parameters, an action, oid (object ID)
    *  and label. The action and label are supplied as input parameters. The oid
    *  is that of the context object of the BasicTemplateProcessor instance. <p>
    *
    *  It is expected that this method will be invoked as a result of an
    *  objectActionLink script call in an HTML template file. The format of
    *  objectActionLink script calls is: <p>
    *
    *
    *  <DL>
    *    <DD> <STRONG>objectActionLink</STRONG> action="action" [label="label"]
    *    [labelPropertyName="labelPropertyName"
    *    <DL>
    *      <DT> <STRONG>action</STRONG> <BR>
    *
    *      <DD> The action parameter is the action to take on the contextual
    *      object. The action is defined in the resource service used by the
    *      URLProcessor class.
    *    </DL>
    *
    *    <DL>
    *      <DT> <STRONG>label</STRONG> <BR>
    *
    *      <DD> By default, the label generated for the URL is the concatenation
    *      of the businessType and identity properties of the contextual object.
    *      This optional label will override the default.
    *    </DL>
    *
    *    <DL>
    *      <DT> <STRONG>labelPropertyName</STRONG> <BR>
    *
    *      <DD> By specifying a labelPropertyName, the value of the property will
    *      be used to override the default label.
    *    </DL>
    *
    *  </DL>
    *  <p>
    *
    *
    *  <DL>
    *    <DT> <STRONG>Example:</STRONG> <BR>
    *
    *    <DD> objectActionLink action=ObjProps label=Context1 <p>
    *
    *    objectActionLink action=ObjProps labelPropertyName=requester <p>
    *
    *    objectActionLink action=ObjProps
    *  </DL>
    *  <p>
    *
    *  <BR>
    *  <BR>
    *  <B>Supported API: </B> true
    *
    *@param  parameters               The arguments that were passed to the
    *      Windchill script call.
    *@param  locale                   The Locale to send to the invoked methods
    *      for localization.
    *@param  os                       The output stream.
    *@exception  wt.util.WTException  Description of the Exception
    */
   public void objectActionLink(Properties parameters, Locale locale, OutputStream os)
          throws wt.util.WTException {
      if (VERBOSE) {
         System.out.println("Entering BasicTemplateProcessor.objectActionLink()...");
      }

      if (parameters == null) {
         Object[] param = {"objectActionLink action"};
         throw new WTException(null, RESOURCE,
               enterpriseResource.NOT_SPECIFIED, param);
      }

      PrintWriter out = getPrintWriter(os, locale);

      //  Check if an installed.properties key has been given.  If so
      //  the link should only be displayed if the component corresponding
      //  to the given key is installed.
      String installed_key = parameters.getProperty("installed_key", null);
      boolean installed = true;

      if (installed_key != null) {
         // Check if the component is installed.
         installed = InstalledProperties.isInstalled(installed_key);
      }

      if (installed) {
         String action = parameters.getProperty("action");
         if (VERBOSE) {
            System.out.println("action -> " + action);
         }

         String label = parameters.getProperty("label");
         if (label != null) {
            label = splitStringIntoTokens(label);
            parameters.remove("label");
         }

         if (VERBOSE) {
            System.out.println("label -> " + label);
         }

         String labelPropertyName = parameters.getProperty("labelPropertyName");
         if (VERBOSE) {
            System.out.println("labelPropertyName -> " + labelPropertyName);
         }

         if (labelPropertyName != null) {
            label = objectPropertyValueString(labelPropertyName, parameters, locale);
            parameters.remove("labelPropertyName");
         } else if (label == null) {
            if (getContextObj() instanceof WTObject) {
               LocalizableMessage message =
                     ((WTObject) getContextObj()).getDisplayIdentity();
               label = message.getLocalizedMessage(locale);
            }
         }

         if (VERBOSE) {
            System.out.println("label -> " + label);
            System.out.println("  about to call objectActionLinkAux()...");
         }

         out.println(objectActionLinkAux(getContextObj(), action, label, parameters, locale));
         out.flush();
         if (VERBOSE) {
            System.out.println("Exiting objectActionLink().");
         }
      }
   }

   /**
    *  A utility method that implements some of the basic functionality needed by
    *  objectActionLink(). Returns a String representation of a URL to the
    *  specified action on the specified object. This method creates a URL to the
    *  given action on the given object with the given label as the link text via
    *  the URLProcessor. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  obj                      the Object to be the context object of the
    *      link
    *@param  action                   the action to link to (i.e. ObjProps)
    *@param  label                    the label to use as the text of the HREF
    *      link
    *@param  locale                   Description of the Parameter
    *@return                          a String representation of the URL formed
    *@exception  wt.util.WTException  Description of the Exception
    */
   public static String objectActionLinkAux(Object obj,
         String action,
         String label,
         Locale locale)
          throws wt.util.WTException {
      return objectActionLinkAux(obj, action, label, (String) null, locale);
   }

   /**
    *  A utility method that implements some of the basic functionality needed by
    *  objectActionLink(). Returns a String representation of a URL to the
    *  specified action on the specified object. This method creates a URL to the
    *  given action on the given object with the given label as the link text via
    *  the URLProcessor. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  obj                      the Object to be the context object of the
    *      link
    *@param  action                   the action to link to (i.e. ObjProps)
    *@param  label                    the label to use as the text of the HREF
    *      link
    *@param  locale                   Description of the Parameter
    *@param  inline                   Description of the Parameter
    *@return                          a String representation of the URL formed
    *@exception  wt.util.WTException  Description of the Exception
    */
   public static String objectActionLinkAux(Object obj,
         String action,
         String label,
         Locale locale,
         boolean inline)
          throws wt.util.WTException {
      return objectActionLinkAux(obj, action, label, null, null, locale, inline);
   }

   /**
    *  A utility method that implements some of the basic functionality needed by
    *  objectActionLink(). Returns a String representation of a URL to the
    *  specified action on the specified object. This method creates a URL to the
    *  given action on the given object with the given label as the link text and
    *  the given String HTML to include in the HREF tag. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  obj              the Object to be the context object of the link
    *@param  action           the action to link to (i.e. ObjProps)
    *@param  label            the label to use as the text of the HREF link
    *@param  href_props       a String containing HTML code to be used in the HREF
    *      tags.
    *@param  locale           Description of the Parameter
    *@return                  a String representation of the URL formed
    *@exception  WTException  Description of the Exception
    */
   public static String objectActionLinkAux(Object obj,
         String action,
         String label,
         String href_props,
         Locale locale) throws WTException {
      return (objectActionLinkAux(obj, action, label, null, href_props, locale));
   }

   /**
    *  A utility method that implements some of the basic functionality needed by
    *  objectActionLink(). <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  obj                      Description of the Parameter
    *@param  action                   Description of the Parameter
    *@param  label                    Description of the Parameter
    *@param  params                   Description of the Parameter
    *@param  locale                   Description of the Parameter
    *@return                          Description of the Return Value
    *@exception  wt.util.WTException  Description of the Exception
    */
   public static String objectActionLinkAux(Object obj, String action, String label,
         Properties params, Locale locale)
          throws wt.util.WTException {
      return objectActionLinkAux(obj, action, label, params, null, locale);
   }

   /**
    *  A utility method that implements some of the basic functionality needed by
    *  objectActionLink(). <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  obj                      Description of the Parameter
    *@param  action                   Description of the Parameter
    *@param  label                    Description of the Parameter
    *@param  params                   Description of the Parameter
    *@param  href_props               Description of the Parameter
    *@param  locale                   Description of the Parameter
    *@return                          Description of the Return Value
    *@exception  wt.util.WTException  Description of the Exception
    */
   public static String objectActionLinkAux(Object obj, String action, String label,
         Properties params, String href_props, Locale locale)
          throws wt.util.WTException {
      return objectActionLinkAux(obj, action, label, params, href_props, locale, false);
   }

   /**
    *  This method should be used ONLY for static methods for legacy processor
    *  support. Instance methods should obtain URLFactory by a call to
    *  getState().getURLFactory().
    *
    *@return    The uRLFactory value
    */
   protected static URLFactory getURLFactory() {
      try {
         URLFactory factory = (URLFactory) MethodContext.getContext().get("URLFactory");
         if (factory == null) {
            factory = new URLFactory();
         }
         return factory;
      } catch (WTException e) {
         e.printStackTrace();
      }
      return null;
   }

   /**
    *  A utility method that implements some of the basic functionality needed by
    *  objectActionLink(). <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  obj                      Description of the Parameter
    *@param  action                   Description of the Parameter
    *@param  label                    Description of the Parameter
    *@param  params                   Description of the Parameter
    *@param  href_props               Description of the Parameter
    *@param  locale                   Description of the Parameter
    *@param  inline                   Description of the Parameter
    *@return                          Description of the Return Value
    *@exception  wt.util.WTException  Description of the Exception
    */
   public static String objectActionLinkAux(Object obj, String action, String label,
         Properties params, String href_props, Locale locale,
         boolean inline)
          throws wt.util.WTException {
      if ((label == null) &&
            (obj instanceof WTObject)) {
         LocalizableMessage message = ((WTObject) obj).getDisplayIdentity();
         label = message.getLocalizedMessage(locale);
      }

      String url = "";
      if (action.equals("LCHistory")) {
         // URL history_url =
         //   new URL( HistoryProcessor.buildHistoryUrl(
         //              (LifeCycleManaged)folder_entry ) );
         url = HistoryProcessor.buildHistoryUrl((LifeCycleManaged) obj);
         label = label +
               WTMessage.getLocalizedMessage(RESOURCE,
               enterpriseResource.LIFECYCLE_HISTORY_LABEL,
               null, locale);
      } else {
         HashMap map = new HashMap();
         if (params == null) {
            params = new Properties();
         }
         Enumeration enumeration = params.propertyNames();
         while (enumeration.hasMoreElements()) {
            String key = (String) enumeration.nextElement();
            map.put(key, params.getProperty(key));
         }
         if (params.get("action") == null) {
            map.put("action", action);
         }

         /*
          *  If the context object isn't null, create an "oid" parameter
          *  with the oid of the object.
          */
         if (obj != null) {
            if (!action.equals("ObjProps")) {
               // xxx This case needs to be removed after we have versioning branch,
               //     and before R2.0 ships...
               // darryn
               ReferenceFactory rf = new ReferenceFactory();
               WTReference ref = rf.getReference((Persistable) obj);
               map.put("oid", rf.getReferenceString(ref));
               //params.put("oid", PersistenceHelper.getObjectIdentifier(
               //               (Persistable) obj).toString());
            } else {
               if (obj instanceof Shortcut) {
                  obj = ((Shortcut) obj).getTarget();
               }

               // xxx This is darryn's code to be finished for R2.0
               ReferenceFactory rf = new ReferenceFactory();
               WTReference ref = rf.getReference((Persistable) obj);
               if (VERBOSE) {
                  System.out.println("  WTReference: " + ref);
               }
               String queryString = rf.getReferenceString(ref);
               map.put("oid", queryString);
            }
         }

         url = GatewayServletHelper.buildAuthenticatedHREF(getURLFactory(),
               HTML_GATEWAY,
               HTML_TEMPLATE_ACTION,
               null, map);
      }

      String url_string;
      if (inline) {
         url_string = HtmlUtil.createInlineLink(url, href_props, label);
      } else {
         url_string = HtmlUtil.createLink(url, href_props, label);
      }

      return url_string;
   }

   /**
    *  A utility method that implements some of the basic functionality needed by
    *  objectActionLink(). Returns a String containing the HTML code to create a
    *  link (HREF) to the given action with the given label on the object
    *  associated with the given WTReference. Additional parameters to be added to
    *  the query string of the link are specified in the given Properties object.
    *  Additional HTML code to be used in creating the link is specified in the
    *  given href_props String. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  obj_ref                  the WTReference to the object which is to be
    *      associated as the context object for the link
    *@param  action                   the action to link to
    *@param  label                    the label to use as the text of the link
    *@param  params                   Properties to be included on the query
    *      string of the link
    *@param  href_props               Additional HTML to be included in the link
    *@param  locale                   the Locale of the client in which this link
    *      will be displayed
    *@return                          Description of the Return Value
    *@exception  wt.util.WTException  Description of the Exception
    */
   public static String objectActionLinkAux(WTReference obj_ref, String action, String label,
         Properties params, String href_props, Locale locale)
          throws wt.util.WTException {
      if ((label == null) &&
            (obj_ref != null)) {
         try {
            Persistable obj = obj_ref.getObject();
            if (obj instanceof WTObject) {
               LocalizableMessage message = ((WTObject) obj).getDisplayIdentity();
               label = message.getLocalizedMessage(locale);
            }
         } catch (Exception e) {}
      }

      String url = "";

      if (params == null) {
         params = new Properties();
      }
      HashMap map = new HashMap();
      Enumeration enumeration = params.propertyNames();
      while (enumeration.hasMoreElements()) {
         String key = (String) enumeration.nextElement();
         map.put(key, params.getProperty(key));
      }

      if (params.get("action") == null) {
         map.put("action", action);
      }

      /*
       *  If the object reference isn't null, create an "oid" parameter
       *  with the querystring of the object reference
       */
      if (obj_ref != null) {
         ReferenceFactory rf = new ReferenceFactory();
         String queryString = rf.getReferenceString(obj_ref);
         map.put("oid", queryString);
      }

      url = GatewayServletHelper.buildAuthenticatedHREF(getURLFactory(),
            HTML_GATEWAY,
            HTML_TEMPLATE_ACTION,
            null, map);
      return HtmlUtil.createLink(url.toString(), href_props, label);
   }

   /**
    *  <A NAME=objectBackground></A> Outputs HTML to set the background of the
    *  page to a supplied image. The image file used must be supplied as a
    *  parameter and is expected to be in $(wt.server.codebase)/wt/clients/images/.
    *  <p>
    *
    *  It is expected that this method will be invoked as a result of an
    *  objectBackground script call in an HTML template file. The format of
    *  objectBackground script calls is: <p>
    *
    *
    *  <DL>
    *    <DD> <STRONG>objectBackground</STRONG> image="image"
    *    <DL>
    *      <DT> <STRONG>image</STRONG> <BR>
    *
    *      <DD> The image parameter is the image file name to use as the
    *      background.
    *    </DL>
    *
    *  </DL>
    *  <p>
    *
    *
    *  <DL>
    *    <DT> <STRONG>Example:</STRONG> <BR>
    *
    *    <DD> objectBackground image=background_gray.gif
    *  </DL>
    *  <BR>
    *  <BR>
    *  <B>Supported API: </B> true
    *
    *@param  parameters       The arguments that were passed to the Windchill
    *      script call.
    *@param  locale           The Locale to send to the invoked methods for
    *      localization.
    *@param  os               The output stream.
    *@exception  WTException  Description of the Exception
    */
   public void objectBackground(Properties parameters, Locale locale,
         OutputStream os)
          throws WTException {
      PrintWriter out = getPrintWriter(os, locale);
      String image = parameters.getProperty("image");
      String href = this.getState().getURLFactory().getHREF("wt/clients/images/" + image);
      out.println(HtmlUtil.createBody(href));
      out.flush();
   }

   /**
    *  <A NAME=currentTimestamp></A> Outputs HTML that describes the current date
    *  and time. <p>
    *
    *  It is expected that this method will be invoked as a result of an
    *  currentTimestamp script call in an HTML template file. The format of
    *  currentTimestamp script calls is: <p>
    *
    *
    *  <DL>
    *    <DD> <STRONG>currentTimestamp</STRONG>
    *  </DL>
    *  <p>
    *
    *
    *  <DL>
    *    <DT> <STRONG>Example:</STRONG> <BR>
    *
    *    <DD> currentTimestamp
    *  </DL>
    *  <BR>
    *  <BR>
    *  <B>Supported API: </B> true
    *
    *@param  parameters       The arguments that were passed to the Windchill
    *      script call.
    *@param  locale           The Locale to send to the invoked methods for
    *      localization.
    *@param  os               The output stream.
    *@exception  WTException  Description of the Exception
    */
   public void currentTimestamp(Properties parameters, Locale locale,
         OutputStream os)
          throws WTException {
      String result = null;

      if (VERBOSE) {
         System.out.println("Entering currentTimestamp()");
      }

      PrintWriter out = getPrintWriter(os, locale);
      String format = null;
      if (parameters != null) {
         format = parameters.getProperty("format");
         if (VERBOSE) {
            System.out.println("format -> " + format);
         }
      }

      Date date = new Date();
      if (format == null) {
         result = WTStandardDateFormat.format(date, locale);
      } else {
         result = WTStandardDateFormat.format(date, format);
      }

      result = removeEnclosingQuotes(result);

      out.println(result);
      out.flush();
   }

   /**
    *  <A NAME=objectIdentifier></A> Outputs an object identifier of the form
    *  "wt.doc.General:2472". <p>
    *
    *  It is expected that this method will be invoked as a result of an
    *  objectIdentifier script call in an HTML template file. The format of
    *  objectIdentifier script calls is: <p>
    *
    *
    *  <DL>
    *    <DD> <STRONG>objectIdentifier</STRONG>
    *  </DL>
    *  <p>
    *
    *
    *  <DL>
    *    <DT> <STRONG>Example:</STRONG> <BR>
    *
    *    <DD> objectIdentifier <p>
    *
    *    <BR>
    *    <BR>
    *    <B>Supported API: </B> false
    *
    *@param  parameters               The arguments that were passed to the
    *      Windchill script call.
    *@param  locale                   The Locale to send to the invoked methods
    *      for localization.
    *@param  os                       The output stream.
    *@exception  wt.util.WTException  Description of the Exception
    */
   public void objectIdentifier(Properties parameters, Locale locale,
         OutputStream os)
          throws wt.util.WTException {
      if (VERBOSE) {
         System.out.println("Entering objectIdentifier()...");
      }

      PrintWriter out = getPrintWriter(os, locale);

      String oid = "";
      Object obj = getContextObj();
      if (obj != null && obj instanceof Persistable){
         try{
           oid = PersistenceHelper.getObjectIdentifier((Persistable)obj).toString();
       }
       catch (Exception e){
         if (VERBOSE)
            e.printStackTrace();
       }
     }
     out.println(oid);
      out.flush();
   }

   /**
    *  <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  parameters       Description of the Parameter
    *@param  locale           Description of the Parameter
    *@param  os               Description of the Parameter
    *@exception  WTException  Description of the Exception
    */
   public void getBaselineItems(Properties parameters, Locale locale, OutputStream os)
          throws WTException {
      PrintWriter out = getPrintWriter(os, locale);

      QueryResult items = BaselineHelper.service.getBaselineItems(
            (Baseline) getContextObj());
      if (items.size() > 0) {
         ResourceBundle messagesResource =
               ResourceBundle.getBundle(RESOURCE, locale);
         String headerFontColor = getWCColor("t1-f-col-head");
         String headerBgColor = getWCColor("t1-bg-col-head");
         String tableBgColor = getWCColor("t1-bg-evenrow");

         // Add table header
         out.print("<TABLE BORDER=0 CELLPADDING=3 CELLSPACING=2 ALIGN=CENTER WIDTH=\"93%\">");
         out.print("<TR BGCOLOR=\"" + headerBgColor + "\">");
         out.print(HtmlUtil.addTableEntry("COLSPAN=2",
               "<FONT COLOR=\"" + headerFontColor + "\">" + HtmlUtil.formatText("B",
               getDisplayName(wt.doc.WTDocument.class, "number", locale))
                + "</FONT>"));
         out.print(HtmlUtil.addTableEntry("",
               "<FONT COLOR=\"" + headerFontColor + "\">" +
               HtmlUtil.formatText("B",
               getDisplayName(wt.doc.WTDocument.class, "name", locale))
                + "</FONT>"));
         out.print(HtmlUtil.addTableEntry("WIDTH=\"7%\"",
               "<FONT COLOR=\"" + headerFontColor + "\">" +
               HtmlUtil.formatText("B", messagesResource.getString(
               enterpriseResource.ITERATION)) + "</FONT>"));
         out.print(HtmlUtil.addTableEntry("WIDTH=\"10%\"",
               "<FONT COLOR=\"" + headerFontColor + "\">" +
               HtmlUtil.formatText("B", messagesResource.getString(
               enterpriseResource.STATE)) + "</FONT>"));
         out.print(HtmlUtil.addTableEntry("WIDTH=\"15%\"",
               "<FONT COLOR=\"" + headerFontColor + "\">" +
               HtmlUtil.formatText("B",
               getDisplayName(wt.fc.WTObject.class, "modifyTimestamp", locale))
                + "</FONT>"));
         out.println("</TR>");
         out.flush();

         // Create objects used in generating rows
         ObjectReferenceQueryStringDelegate refStringDelegate =
               new ObjectReferenceQueryStringDelegate();
         HashMap params = new HashMap();
         params.put("action", "ObjProps");

         // Sort the list
         QueryCollationKeyFactory key = new QueryCollationKeyFactory(locale);
         SortedEnumeration sortedItems = new SortedEnumeration(
               items.getEnumeration(), key);

         // Add each table row
         while (sortedItems.hasMoreElements()) {
            Baselineable item = (Baselineable) sortedItems.nextElement();

            // Create item link
            params.put("oid", refStringDelegate.getReferenceString(
                  ObjectReference.newObjectReference(item)));
            String href = GatewayServletHelper.buildAuthenticatedHREF(getURLFactory(),
                  HTML_GATEWAY,
                  HTML_TEMPLATE_ACTION,
                  null, params);
            String itemLabel = "";
            if (item instanceof RevisionControlled) {
               Properties props = new Properties();
               props.put("version_info", "qualified");
               itemLabel = getObjectPropertyValueString("versionInfo", props, locale, item);
            } else {
               itemLabel = VersionControlHelper.getIterationIdentifier(item).getValue();
            }

            String view = getAttrValue(item, "viewName");
            if ((view != null) &&
                  (view.length() > 0) &&
                  (!view.equals(BLANK_SPACE))) {
               Object[] msg_parameters = {itemLabel, view};
               itemLabel = WTMessage.getLocalizedMessage(RESOURCE, enterpriseResource.PART_VERSION_DISPLAY,
                     msg_parameters, locale);
            }

            String image = "";
            try {
               image = getObjectIconImgTag((WTObject) item, tableBgColor);
            } catch (Exception e) {
               // Ignore Exception
               image = "";
            }
            out.print("<TR BGCOLOR=\"" + tableBgColor + "\">");
            out.print(HtmlUtil.addTableEntry("WIDTH=\"3%\"", image));
            out.print(HtmlUtil.addTableEntry("NOWRAP",
                  objectActionLinkAux(item, "ObjProps", getAttrValue(item, "number"), locale)));
            out.print(HtmlUtil.addTableEntry("NOWRAP",
                  getAttrValue(item, "name")));
            out.print(HtmlUtil.addTableEntry("NOWRAP",
                  (href == null) ? itemLabel :
                  HtmlUtil.createLink(href, "", itemLabel)));
            out.print(HtmlUtil.addTableEntry("NOWRAP",
                  (item instanceof LifeCycleManaged) ?
                  ((LifeCycleManaged) item).getState().getState().getDisplay(locale) :
                  BLANK_SPACE));
            out.print(HtmlUtil.addTableEntry("NOWRAP",
                  WTStandardDateFormat.format(
                  item.getPersistInfo().getModifyStamp(), locale)));
            out.println("</TR>");
            out.flush();
         }

         out.print("</TABLE>");
         out.flush();
      }
   }

   /**
    *  <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  parameters               Description of the Parameter
    *@param  locale                   Description of the Parameter
    *@param  os                       Description of the Parameter
    *@exception  wt.util.WTException  Description of the Exception
    */
   public void partUsedBy(Properties parameters, Locale locale,
         OutputStream os)
          throws wt.util.WTException {
      if (VERBOSE) {
         System.out.println("Entering partUsedBy()...");
      }

      String otherPartView = null;

      WTPartUsageLink[] usedBy = getUsedByLinks();
      if (VERBOSE) {
         System.out.println("  usedBy -> " + usedBy);
      }

      PrintWriter out = getPrintWriter(os, locale);

      if (usedBy.length > 0) {
         out.println("<HR WIDTH=\"100%\">");
         out.println("<p>");
         out.println("<B>Used By:</B><p>");
      }

      for (int i = 0; i < usedBy.length; i++) {
         if (VERBOSE) {
            System.out.println("  " + i + " usedBy[i].getUsedBy() -> " +
                  usedBy[i].getUsedBy());
         }
         out.println(objectActionLinkAux(usedBy[i].getUsedBy(), "ObjProps", null, locale));
         out.println("<BR>");
         out.flush();
      }
   }

   /**
    *  <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@return                  The usedByLinks value
    *@exception  WTException  Description of the Exception
    */
   public WTPartUsageLink[] getUsedByLinks()
          throws WTException {
      WTPartMaster part;
      Object refPart = getContextObj();

      if (refPart instanceof WTPartMaster) {
         part = (WTPartMaster) refPart;
      } else {
         part = (WTPartMaster) ((WTPart) refPart).getMaster();
      }

      Vector results = new Vector();
      WTPartUsageLink[] usedBy = null;

      try {
         QueryResult qr = null;

         if (VERBOSE) {
            System.out.println("  about to navigate WTPartUsageLink");
         }
         qr = StructHelper.service.navigateUsedBy(part, false);
         if (VERBOSE) {
            System.out.println("  navigated WTPartUsageLink");
         }
         while (qr.hasMoreElements()) {
            results.addElement((WTPartUsageLink) qr.nextElement());
         }
         // change from vector to WTPartUsageLink array

         usedBy = new WTPartUsageLink[results.size()];
         results.copyInto(usedBy);
      } catch (WTException wte) {
         System.out.println("Error in BasicTemplateProcessor.getUsedBy() ");
         wte.printStackTrace();
         System.out.println(wte);
         throw (wte);
      }
      return usedBy;
   }

   /**
    *  <A NAME=listContent></A> Outputs a table listing the content items (files
    *  and URLs) associated with the ContentHolder context object. If the current
    *  context object is not a ContentHolder, no output is generated. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  parameters                     The arguments that were passed to the
    *      Windchill script call.
    *@param  locale                         The Locale to send to the invoked
    *      methods for localization.
    *@param  os                             The output stream.
    *@exception  WTException                Description of the Exception
    *@exception  PropertyVetoException      Description of the Exception
    *@exception  InvocationTargetException  Description of the Exception
    *@exception  IllegalAccessException     Description of the Exception
    */
   public void listContent(Properties parameters, Locale locale, OutputStream os)
          throws WTException, PropertyVetoException, InvocationTargetException, IllegalAccessException {
      Object obj = getContextObj();

      //  If the context object is not a ContentHolder, return
      if (!(obj instanceof ContentHolder)) {
         return;
      }

      //  Get the list of content items in this content holder
      ContentHolder holder = ContentHelper.service.getContents((ContentHolder) obj);
      Vector contents = ContentHelper.getContentListExcludeRoles(holder, SECONDARY_EXCLUDE);

      PrintWriter out = getPrintWriter(os, locale);

      //  If the ContentHolder does not have any content items currently
      //  associated with it, do not output any tables.
      if ((contents == null) || (contents.size() == 0)) {
         return;
      }
      boolean apps = false;
      boolean urls = false;

      StringBuffer appTable = new StringBuffer(contents.size() * 1024)
            .append(appDataTableHeading(locale));

      StringBuffer urlTable = new StringBuffer(contents.size() * 1024)
            .append(urlTableHeading(locale));

      String bgColorHexValue = getWCColor("t1-bg-evenrow");
      String BGCOLOR = "BGCOLOR=\"" + bgColorHexValue + "\"";
      String LIST_AGGREGATE_ACTION = "ListAggregateContents";
      for (Enumeration e = contents.elements(); e.hasMoreElements(); ) {
         ContentItem next = (ContentItem) e.nextElement();
         StringBuffer row;

         String img = getObjectIconImgTag(next, bgColorHexValue, holder, getState());
         if (img == null) {
            img = BLANK_SPACE;
         }
         String image = HtmlUtil.addTableEntry(BGCOLOR + " WIDTH=3% VALIGN=TOP", img);

         Aggregate agg;
         ApplicationData app;
         URLData url;

         if (next instanceof Aggregate) {
            agg = (Aggregate) next;
            apps = true;

            String name = HtmlUtil.addTableEntry(BGCOLOR, agg.getDisplayIdentity().getLocalizedMessage(locale));
            String format = HtmlUtil.addTableEntry(BGCOLOR,
                  WTMessage.getLocalizedMessage(CONTENT_RESOURCE,
                  wt.content.contentResource.AGGREGATE_DISPLAY_NAME,
                  null, locale));
            String empty = HtmlUtil.addTableEntry(BGCOLOR + " COLSPAN=3", "");

            row = new StringBuffer(image.length() + name.length() + format.length() + empty.length())
                  .append(image).append(name).append(format).append(empty);

            appTable.append(HtmlUtil.addTableRow("", String.valueOf(row)));
         } else if (next instanceof ApplicationData) {
            app = (ApplicationData) next;
            apps = true;

            String fileNameDisplayString = app.getFileName();
            if (fileNameDisplayString.length() > MAXLENGTH) {
               fileNameDisplayString = fileNameDisplayString.substring(0, MAXLENGTH) + WTMessage.getLocalizedMessage(CONTENT_RESOURCE, wt.content.contentResource.ELLIPSES, null, locale);
            }

            String link = HtmlUtil.addTableEntry(BGCOLOR,
                  HtmlUtil.createLink(app.getViewContentURL(holder).toString(),
                  null, fileNameDisplayString));

            String format = HtmlUtil.addTableEntry(BGCOLOR,
                  DataFormat.getLocalizedFormatName( app.getFormat().getFormatName(), locale));

            String fileSize = HtmlUtil.addTableEntry(BGCOLOR + " ALIGN=RIGHT", getFileSize(app));
            String date = HtmlUtil.addTableEntry(BGCOLOR,
                  formatDate(WTStandardDateFormat.LONG_STANDARD_DATE_FORMAT,
                  PersistenceHelper.getModifyStamp(app), locale));
            WTPrincipalReference createdBy = app.getCreatedBy();
            String createdByEmail = null;
            String createdByDisplayName = null;

            if (createdBy != null) {
               createdByEmail = createdBy.getEMail();
               createdByDisplayName = createdBy.getDisplayName();
            }
            String email = "";
            if (createdByDisplayName != null) {
               if (createdByEmail != null) {
                  email = HtmlUtil.addTableEntry(BGCOLOR,
                        HtmlUtil.createLink("mailto:" + createdByEmail, null,
                        createdByDisplayName));
               } else {
                  email = HtmlUtil.addTableEntry(BGCOLOR, createdByDisplayName);
               }
            } else {
               email = HtmlUtil.addTableEntry(BGCOLOR, "&nbsp;");
            }

            row = new StringBuffer(image.length() + link.length() + format.length() + fileSize.length() + date.length() + email.length())
                  .append(image).append(link).append(format).append(fileSize).append(date).append(email);

            appTable.append(HtmlUtil.addTableRow("", String.valueOf(row)));
         } else {
            url = (URLData) next;
            urls = true;

            String loc = url.getUrlLocation();
            if (loc == null) {
               loc = BLANK_SPACE;
            }
            String location = HtmlUtil.addTableEntry(BGCOLOR + " VALIGN=TOP",
                  HtmlUtil.createPreformattedLink(loc, null, loc, MAXLENGTH));

            String desc = url.getDescription();
            if (desc == null) {
               desc = BLANK_SPACE;
               if (VERBOSE) {
                  System.out.println("No description for content " + loc);
               }
            }
            String description = HtmlUtil.addTableEntry(BGCOLOR + " VALIGN=TOP", desc);

            row = new StringBuffer(image.length() + location.length() + description.length())
                  .append(image).append(location).append(description);

            urlTable.append(HtmlUtil.addTableRow("", String.valueOf(row)));
         }
      }

      if (apps) {
         out.print("<P><BR>");
         out.println(HtmlUtil.createTable("", String.valueOf(appTable)));
      }
      if (urls) {
         out.print("<P><BR>");
         out.println(HtmlUtil.createTable("", String.valueOf(urlTable)));
      }
      out.flush();
   }

   /**
    *  Outputs the table headings for the Application Data (file content) table.
    *  This method is used by <A HREF="#listContent">listContent</A> to print the
    *  table of content items. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  locale  Description of the Parameter
    *@return         a String containing the HTML code to display the table
    *      headers for file content items.
    */
   private String appDataTableHeading(Locale locale) {
      String bgColor = getWCColor("t1-bg-col-head");
      String fColor = getWCColor("t1-f-col-head");
      String fileName = HtmlUtil.addTableHeading("bgcolor=\"" + bgColor + "\" COLSPAN=2 ALIGN=LEFT",
            "<FONT COLOR=\"" + fColor + "\"><B>" +
            getDisplayName(ApplicationData.class, ApplicationData.FILE_NAME, locale));

      String format = HtmlUtil.addTableHeading("bgcolor=\"" + bgColor + "\" ALIGN=LEFT",
            "<FONT COLOR=\"" + fColor + "\"><B>" +
            getDisplayName(ApplicationData.class, ApplicationData.FORMAT, locale));

      String fileSize = HtmlUtil.addTableHeading("bgcolor=\"" + bgColor + "\" ALIGN=RIGHT",
            "<FONT COLOR=\"" + fColor + "\"><B>" +
            getDisplayName(ApplicationData.class, ApplicationData.FILE_SIZE, locale));

      String lastUpdated = HtmlUtil.addTableHeading("bgcolor=\"" + bgColor + "\" ALIGN=LEFT",
            "<FONT COLOR=\"" + fColor + "\"><B>" +
            getDisplayName(WTObject.class, "modifyTimestamp", locale));

      String updatedBy = HtmlUtil.addTableHeading("bgcolor=\"" + bgColor + "\" ALIGN=LEFT",
            "<FONT COLOR=\"" + fColor + "\"><B>" +
            getDisplayName(RevisionControlled.class, "modifier", locale));

      StringBuffer row = new StringBuffer(fileName.length() + format.length() + fileSize.length() +
            lastUpdated.length() + updatedBy.length())
            .append(fileName).append(format).append(fileSize)
            .append(lastUpdated).append(updatedBy);

      return HtmlUtil.addTableRow("", String.valueOf(row));
   }

   /**
    *  Outputs the table headings for the URL table. This method is used by <A
    *  HREF="#listContent">listContent</A> to print the table of content items.
    *  <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  locale  Description of the Parameter
    *@return         a String containing the HTML code to display the table
    *      headers for URL content items.
    */
   private String urlTableHeading(Locale locale) {
      String bkColor = getWCColor("t1-bg-col-head");
      String fColor = getWCColor("t1-f-col-head");
      String location = HtmlUtil.addTableHeading("bgcolor=\"" + bkColor + "\" COLSPAN=2 ALIGN=LEFT",
            "<FONT COLOR=\"" + fColor + "\"><B>" +
            getDisplayName(URLData.class, URLData.URL_LOCATION, locale));

      String description = HtmlUtil.addTableHeading("bgcolor=\"" + bkColor + "\" ALIGN=LEFT",
            "<FONT COLOR=\"" + fColor + "\"><B>" +
            getDisplayName(URLData.class, URLData.DESCRIPTION, locale));

      return HtmlUtil.addTableRow("", location + description);
   }

   /**
    *  <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  params           Description of the Parameter
    *@param  paramName        Description of the Parameter
    *@return                  Description of the Return Value
    *@exception  WTException  Description of the Exception
    */
   public String parameterValue(Properties params, String paramName)
          throws WTException {
      String result = params.getProperty(paramName);
      if (VERBOSE) {
         System.out.println("parameterValue() " + paramName + " is " + result);
      }
      boolean paramsOK = true;
      String badParamsMsg = null;

      if (result == null) {
         paramsOK = false;

         Object[] param1 = {paramName, "null"};
         badParamsMsg = WTMessage.getLocalizedMessage(
               RESOURCE,
               enterpriseResource.PARAMETER_IS,
               param1);
         if (VERBOSE) {
            System.out.println("badParamsMsg -> " + badParamsMsg);
         }

         //If we received bad parameters we can't continue (BAIL OUT!)
         Object[] param2 = {badParamsMsg};

         String message = WTMessage.getLocalizedMessage(
               RESOURCE,
               enterpriseResource.BAD_PARAMETERS,
               param2);
         if (VERBOSE) {
            System.out.println(message);
         }

         throw new WTException(message);
      }
      return result;
   }

   /**
    *  Outputs a table displaying all of the cabinets for which the current user
    *  has read permissions.
    *
    *@param  parameters                     Description of the Parameter
    *@param  locale                         Description of the Parameter
    *@param  os                             Description of the Parameter
    *@exception  PropertyVetoException      Description of the Exception
    *@exception  InvocationTargetException  Description of the Exception
    *@exception  IllegalAccessException     Description of the Exception
    *@exception  WTException                Description of the Exception
    */
   public void displayAllCabinets(Properties parameters, Locale locale, OutputStream os)
          throws PropertyVetoException, InvocationTargetException,
         IllegalAccessException, WTException {
      PrintWriter out = getPrintWriter(os, locale);

      String cabinet_table = "";
      String row = "";
      String permission = null;

      if (parameters != null) {
         permission = parameters.getProperty("permission");
      }

      SortedEnumeration cabinets = getAllCabinets(permission);

      if ((cabinets == null) ||
            (cabinets.size() == 0)) {
         ResourceBundle messagesResource = ResourceBundle.getBundle(RESOURCE, locale);
         String no_cabinets_message = "";
         try {
            no_cabinets_message = messagesResource.getString(enterpriseResource.NO_CABINETS_MESSAGE);
         } catch (MissingResourceException mre) {
            if (VERBOSE) {
               System.out.println("displayAllCabinets: MissingResourceException caught:");
               mre.printStackTrace();
            }
         }

         out.println("<TABLE ALIGN=CENTER><TR><TD>" + no_cabinets_message + "</TD></TR></TABLE>");
         out.flush();
         return;
      }

      int MAX_ROWS = 8;
      int nbr_rows = (int) Math.ceil((float) cabinets.size() / MAX_ROWS);
      int width = 100 / nbr_rows;
      String bgColor = getWCColor("t1-bg-evenrow");

      while (cabinets.hasMoreElements()) {
         int i = 0;
         String inner_row = "";
         String inner_table = "";

         while ((i++ < MAX_ROWS) && cabinets.hasMoreElements()) {
            String entry = "";
            Cabinet current = (Cabinet) cabinets.nextElement();
            String image = getObjectIconImgTag(current, bgColor);
            entry += HtmlUtil.addTableEntry("BGCOLOR=\"" + bgColor + "\" WIDTH=\"3%\"", image);

            String value = objectActionLinkAux(current, "ObjProps", getAttrValue(current, "name"), locale);
            entry += HtmlUtil.addTableEntry("BGCOLOR=\"" + bgColor + "\"", value);
            inner_row += HtmlUtil.addTableRow("", entry);
         }

         inner_table = HtmlUtil.createTable("BORDER=0 CELLPADDING=2", inner_row);
         cabinet_table = cabinet_table.concat(
               HtmlUtil.addTableEntry("BGCOLOR=\"" + bgColor + "\" VALIGN=TOP WIDTH=\"" + width + "%\"", inner_table));
      }

      out.println(HtmlUtil.createTable("BORDER=0 WIDTH=\"95%\" ALIGN=CENTER CELLPADDING=3",
            cabinet_table));
      out.flush();
   }

   /**
    *  Returns all cabinets for which the current user has the given permission.
    *  If the given permission is null, all cabinets for which the current user
    *  has read permissions will be returned. The cabinets are returned in a
    *  SortedEnumeration which is sorted according to identity of the cabinet.
    *
    *@param  permission          optional String permission which determines what
    *      cabinets are retrieved; if not given, all cabinets for which the user
    *      has read permission is returned
    *@return                     a SortedEnumeration containing all cabinets for
    *      which the current user has the given permission.
    *@exception  QueryException  Description of the Exception
    *@exception  WTException     Description of the Exception
    */
   private SortedEnumeration getAllCabinets(String permission)
          throws QueryException, WTException {
      SortedEnumeration cabinets = null;
      QueryResult results = null;

      if ((permission == null) || (permission.length() == 0)) {
      //SPR 1262430-01- replace all uses of WTPermission with AccessPermission
      // default value
      results = FolderServerHelper.service.findCabinetsReadOnly(AccessPermission.READ, false);
      }

      results = FolderServerHelper.service.findCabinetsReadOnly(permission, false);

      if (results != null) {
         cabinets = new SortedEnumeration(results.getEnumeration(), new IdentityCollationKeyFactory());
      }

      return cabinets;
   }

   /**
    *  <A NAME=displayFolderContents> Displays the contents of the folder context
    *  object. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  parameters                     Description of the Parameter
    *@param  locale                         Description of the Parameter
    *@param  os                             Description of the Parameter
    *@exception  WTException                Description of the Exception
    *@exception  PropertyVetoException      Description of the Exception
    *@exception  InvocationTargetException  Description of the Exception
    *@exception  IllegalAccessException     Description of the Exception
    */
   public void displayFolderContents(Properties parameters, Locale locale,
         OutputStream os) throws WTException,
         PropertyVetoException,
         InvocationTargetException,
         IllegalAccessException {
      //  If the context object is not a folder, return
      if (!(getContextObj() instanceof Folder)) {
         if (VERBOSE) {
            System.out.println("displayFolderContents() - context object is not a folder.");
         }

         return;
      }

      PrintWriter out = getPrintWriter(os, locale);
      String row = "";
      SortedEnumeration query_result = null;
      try {
         //  Get the contents of the Folder
         query_result = retrieveFolderContents();
      } catch (PartialResultException pre) {
         // Only a partial amount of the contents of the folder could be retrieved
         // Display a message indicating this and output the partial results
         // retrieved.
         QueryResult partial_results = pre.getQueryResult();
         String items_returned = String.valueOf((partial_results == null) ? 0 : partial_results.size());

         // Get a displayable type for the context object
         String type = "";
         if (getContextObj() instanceof WTObject) {
            LocalizableMessage type_message = ((WTObject) getContextObj()).getDisplayType();

            if (type_message != null) {
               type = type_message.getLocalizedMessage(locale);
            } else {
               type = ((WTObject) getContextObj()).getType();
            }
         } else {
            type = getContextObj().getClass().getName();
         }

         // Build and output the message indicating only partial results were
         // returned.
         Object[] params = {type, items_returned};

         out.println("<HR SIZE=1>");
         out.println(HtmlUtil.beginStructure("TABLE", "BORDER=0 WIDTH=\"90%\" ALIGN=CENTER CELLPADDING=3"));
         out.println(HtmlUtil.addTableRow("",
               HtmlUtil.addTableEntry("", WTMessage.getLocalizedMessage(RESOURCE, enterpriseResource.PARTIAL_RESULTS_RETURNED,
               params, locale))));
         out.println(HtmlUtil.endStructure("TABLE"));
         out.println("<HR SIZE=1>");
         out.println("<BR>");
         out.println("<P>");

         if (partial_results != null) {
            query_result = new SortedEnumeration(partial_results, new FolderCollationKeyFactory(locale));
         }
      }

      if ((query_result == null) ||
            (query_result.size() == 0)) {
         String type = "";
         if (getContextObj() instanceof WTObject) {
            LocalizableMessage type_message = ((WTObject) getContextObj()).getDisplayType();

            if (type_message != null) {
               type = type_message.getLocalizedMessage(locale);
            } else {
               type = ((WTObject) getContextObj()).getType();
            }
         } else {
            type = getContextObj().getClass().getName();
         }

         Object[] params = {type};
         String message = WTMessage.getLocalizedMessage(RESOURCE,
               enterpriseResource.NO_FOLDER_CONTENTS,
               params, locale);
         out.println("<TABLE ALIGN=CENTER><TR>" +
               "<TD>" + message + "</TD></TR></TABLE>");
         out.flush();
         return;
      }

      // Output the initial <TABLE> tag and the row containing the table headers
      out.println(HtmlUtil.beginStructure("TABLE", "BORDER=0 WIDTH=\"90%\" ALIGN=CENTER CELLPADDING=3"));
      out.println(HtmlUtil.addTableRow("", getFolderTableHeading(locale)));

      //  Loop through and print out the table contents

      String bgColorData = getWCColor("t1-bg-evenrow");
      String bgColorActions = getWCColor("t1-bg-linksrow");

      while (query_result.hasMoreElements()) {
         //CabinetBased current = (CabinetBased)query_result.nextElement();
         WTObject current = (WTObject) query_result.nextElement();

         if (current instanceof WTContained) {
                if( !displayFolderObject(((WTContained)current).getContainer(), current ))
                {
                    continue;
                }
         }

         else if ( current instanceof EPMWorkspace ) {
            WTContainer owningContainer = ((EPMWorkspace)current).getContainer();
            if (!WTContainerHelper.isClassicContainer(owningContainer)) {
               continue;
            }
         }
         //  Add Icon Cell
         String image = getObjectIconImgTag((WTObject) current, bgColorData, getState());
         row = HtmlUtil.addTableEntry("BGCOLOR=\"" + bgColorData + "\" ROWSPAN=2 WIDTH=3%", image);

         //  Add Name Cell with link to properties page
         String value = objectActionLinkAux(current, "ObjProps", getAttrValue(current, "name"), locale);
         row = row.concat(HtmlUtil.addTableEntry("BGCOLOR=\"" + bgColorData + "\"", value));

         //  Add Number Cell
         row = row.concat(HtmlUtil.addTableEntry("BGCOLOR=\"" + bgColorData + "\"",
               getAttrValue(current, "number")));

         //  Add the Version Cell
         value = getObjectPropertyValueString("versionDisplayIdentifier",
               null, locale, current);
         if (value == null) {
            value = BLANK_SPACE;
         }

         row = row.concat(HtmlUtil.addTableEntry("BGCOLOR=\"" + bgColorData + "\"",
               value));

         //  Add the Life Cycle State Cell
         value = getObjectPropertyValueString(wt.lifecycle.LifeCycleManaged.STATE,
               null, locale, current);
         if (value == null) {
            value = BLANK_SPACE;
         }
         row = row.concat(HtmlUtil.addTableEntry("BGCOLOR=\"" + bgColorData + "\"",
               value));
                 if (current instanceof WTPart) {
                         value = getObjectPropertyValueString("partType",
               null, locale, current);
                 }
                 else {
                         value = getObjectPropertyValueString("displayType",
               null, locale, current);
                 }
         row = row.concat(HtmlUtil.addTableEntry("BGCOLOR=\"" + bgColorData + "\"",
               value));

         //  Add the Updated Date Cell
         Properties properties = new Properties();
         properties.put("formatType", "LONG_STANDARD_DATE_FORMAT");
         row = row.concat(HtmlUtil.addTableEntry("BGCOLOR=\"" + bgColorData + "\"",
               getObjectPropertyValueString("modifyTimestamp",
               properties, locale, current)));

         //  Add the TeamTemplate ID Cell
         value = getObjectPropertyValueString(wt.team.TeamManaged.TEAM_TEMPLATE_ID,
               null, locale, current);
         if (value == null) {
            value = BLANK_SPACE;
         }

         row = row.concat(HtmlUtil.addTableEntry("BGCOLOR=\"" + bgColorData + "\"",
               value));
         out.println(HtmlUtil.addTableRow("", row));
         String actions_string = "";

         if (current instanceof Shortcut) {
            //current = ((Shortcut)current).getTarget();
            current = (WTObject) ((Shortcut) current).getTarget();
         }

         Vector action_delegates = getURLActions(current, locale, true);
         Vector exception_vector = new Vector();
         if (action_delegates != null) {
            for (int i = 0; i < action_delegates.size(); i++) {
               try {
                  URLActionDelegate current_delegate = (URLActionDelegate) action_delegates.elementAt(i);
                  String label = current_delegate.getURLLabel(locale);
                  String url = getURLFromDelegate(current_delegate, current, getState());
                  if (VERBOSE) {
                     System.out.println("URL:   " + url);
                     System.out.println("Label: " + label);
                  }

                  if (url != null) {
                     actions_string += HtmlUtil.createLink(url, null, label);
                     if (i < action_delegates.size() - 1) {
                        actions_string += BLANK_SPACE + BLANK_SPACE;
                     }
                  }
               } catch (WTException wte) {
                  if (VERBOSE) {
                     wte.printStackTrace();
                  }
                  exception_vector.addElement(wte);
               } catch (Exception e) {
                  if (VERBOSE) {
                     System.err.println("displayFolderContents() - exception caught");
                     e.printStackTrace();
                  }
                  exception_vector.addElement(e);
               }
            }
         }

         row = HtmlUtil.addTableEntry("BGCOLOR=\"" + bgColorActions + "\" COLSPAN=7 VALIGN=MIDDLE ALIGN=LEFT",
               BLANK_SPACE + BLANK_SPACE + "<FONT SIZE=-1>" +
               actions_string + "</FONT>");
         out.println(HtmlUtil.addTableRow("BGCOLOR=\"" + bgColorData + "\"", row));
      }
      out.println(HtmlUtil.endStructure("TABLE"));
      out.flush();
   }

   /**
    *  Retrieve the folder contents of the folder context object. If the current
    *  context object is not a folder, this method returns a null
    *  SortedEnumeration <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@return    a SortedEnumeration of the contents associated with the folder
    *      context object, or null if the context object is not a folder
    */
   private SortedEnumeration getFolderContents() {
      SortedEnumeration sorted_results = null;

      if (getContextObj() instanceof Folder) {
         try {
            if (VERBOSE) {
               System.out.println("BasicTemplateProcessor.getFolderContents()");
            }
            QueryResult result = FolderHelper.service.findFolderContents((Folder) getContextObj());
            if (result != null) {
               sorted_results = new SortedEnumeration(result.getEnumeration(),
                     new FolderCollationKeyFactory());
            }
         } catch (WTException wte) {
            wte.printStackTrace();
            sorted_results = null;
         }
      }
      return sorted_results;
   }

   /**
    *  Retrieve the folder contents of the folder context object. If the current
    *  context object is not a folder, this method returns a null
    *  SortedEnumeration <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@return                  a SortedEnumeration of the contents associated with
    *      the folder context object, or null if the context object is not a
    *      folder
    *@exception  WTException  Description of the Exception
    */
   private SortedEnumeration retrieveFolderContents() throws WTException {
      SortedEnumeration sorted_results = null;

      if (getContextObj() instanceof Folder) {
         if (VERBOSE) {
            System.out.println("BasicTemplateProcessor.retrieveFolderContents()");
         }
         QueryResult result = FolderHelper.service.findFolderContents((Folder) getContextObj());
         if (result != null) {
            //dbhate : 13-May-2002 :
            //Disable Wildfire in stand-alone mode if the key 'com.ptc.windchill.cadx.standalone.enabled' is not set.
            //i.e. show the workspaces only if user agent is wildfire or above mentioned key is set
            boolean showWorkspaces = false;
            try {
               showWorkspaces = isWildfireEnabled();
            } catch (Exception e) {}
            ;
            if (showWorkspaces) {
               // if Folder has WORKSPACE role then replace folder object with workspace object
               Vector content = result.getObjectVectorIfc().getVector();
               for (int i = 0; i < content.size(); i++) {
                  if (content.elementAt(i) instanceof SubFolder) {
                     EPMWorkspace ws = null;
                     try {
                        QueryResult qr = PersistenceHelper.manager.navigate((SubFolder) content.elementAt(i),
                              WorkspaceFolder.WORKSPACE_ROLE,
                              WorkspaceFolder.class);
                        if (qr.hasMoreElements()) {
                           ws = (EPMWorkspace) qr.nextElement();
                        }
                     } catch (Exception e) {}
                     if (ws != null) {
                        content.setElementAt(ws, i);
                     }
                  }
               }
            }
            sorted_results = new SortedEnumeration(result.getEnumeration(),
                  new FolderCollationKeyFactory());
         }
      }
      return sorted_results;
   }

   /**
    *  Returns a String containing the URL to the icon for the context object. The
    *  returned URL is generated using the URLFactory associated with the key
    *  "URLFactory" in the Method Context, if any.
    *  If none is present, a new URLFactory is used and an absolute URL is returned.
    *  This method does not take into consideration any glyphs that may
    *  be associated with the context object. If the context object is not a
    *  WTObject, an empty string is returned. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@return                                the String URL to the icon file of the
    *      context object
    *@exception  WTException                if an error occurs retrieving the icon
    *      file
    *@exception  PropertyVetoException      if an error occurs retrieving the icon
    *      file
    *@exception  InvocationTargetException  if an error occurs retrieving the icon
    *      file
    *@exception  IllegalAccessException     if an error occurs retrieving the icon
    *      file
    */
   public String getIconResource() throws WTException, PropertyVetoException,
         InvocationTargetException,
         IllegalAccessException {
      String image_str = "";
      if (getContextObj() instanceof WTObject) {
         image_str = getIconResource((WTObject) getContextObj(), getState());
      }
      return image_str;
   }

   /**
    *  Returns a String containing the URL to the icon for the given WTObject. The
         *  returned URL is generated using the URLFactory associated with the key
         *  "URLFactory" in the Method Context, if any.
         *  If none is present, a new URLFactory is used and an absolute URL is returned.
         *  This method does not take into consideration any glyphs that may
    *  be associated with the given object. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  obj                            the WTObject whose icon is desired (input)
    *@param  a_State                        HTTPState object associated with the current page (input)
    *@return                                the String URL to the icon file of the
    *                                       given WTObject
    *@exception  WTException                if an error occurs retrieving the icon
    *      file
    *@exception  PropertyVetoException      if an error occurs retrieving the icon
    *      file
    *@exception  InvocationTargetException  if an error occurs retrieving the icon
    *      file
    *@exception  IllegalAccessException     if an error occurs retrieving the icon
    *      file
    */
   private static String getIconResource(WTObject obj, HTTPState a_State) throws WTException,
         PropertyVetoException,
         InvocationTargetException,
         IllegalAccessException {
      return getIconResource(obj,a_State,false);
   }

   /**
    *  Returns a String containing the URL to the icon for the given WTObject or class.
    *  The returned URL is generated using the URLFactory associated with the key
         *  "URLFactory" in the Method Context, if any.
         *  If none is present, a new URLFactory is used and an absolute URL is returned.
         *  This method does not take into consideration any glyphs
         *  that may be associated with the given object. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  obj                            The object whose icon image is desired (input)
          *@param  a_State                        The HTTPState object associated with the current page (input)
    *@return                                The URL for the icon file
    *@exception  WTException                if an error occurs retrieving the icon file
    *@exception  PropertyVetoException      if an error occurs retrieving the icon file
    *@exception  InvocationTargetException  if an error occurs retrieving the icon file
    *@exception  IllegalAccessException     if an error occurs retrieving the icon file
    */
   public static String getIconResource(Object obj, HTTPState a_State) throws WTException,
         PropertyVetoException,
         InvocationTargetException,
         IllegalAccessException {
           if (obj instanceof WTObject) {
               return getIconResource((WTObject)obj,a_State,false);
           }
           else {
               return getIconResource(obj.getClass());
           }
        }

   /**
    *  Returns a String containing the URL to the icon for the given WTObject. The
         *  returned URL is generated using the URLFactory associated with the key
         *  "URLFactory" in the Method Context, if any.
         *  If none is present, a new URLFactory is used and an absolute URL is returned.
         *  This method does not take into consideration any glyphs that may
    *  be associated with the given object. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  obj                            the WTObject whose icon is desired (input)
    *@param  a_State                        The HTTPState object associated with the current page (input)
    *@param  fullyQualified                 True if an absolute URL must be returned,
         *                                       regardless of the state of the existing URLFactory, if any (input)
    *@return                                the String URL to the icon file of the
    *                                       given WTObject
    *@exception  WTException                if an error occurs retrieving the icon
    *      file
    *@exception  PropertyVetoException      if an error occurs retrieving the icon
    *      file
    *@exception  InvocationTargetException  if an error occurs retrieving the icon
    *      file
    *@exception  IllegalAccessException     if an error occurs retrieving the icon
    *      file
    */
 private static String getIconResource(WTObject obj, HTTPState a_State, boolean forceFullyQualified) throws WTException,
         PropertyVetoException,
         InvocationTargetException,
         IllegalAccessException {
      String resource = null;
      if (VERBOSE) {
         System.out.println("Attempting to get IconDelegateFactory...");
      }
      IconDelegateFactory factory = new IconDelegateFactory();
      IconDelegate delegate = factory.getIconDelegate(obj);
      IconSelector selector = null;
      //dbhate : 6-May-2002 : Special case for icon delegates that needs 'state' do decide the icon
      if (isWildfireIconDelegate(delegate)) {
         selector = getWildfireIconSelector(delegate, a_State);
      }

      if (selector == null) {
         selector = delegate.getStandardIconSelector();
      }

      while (!selector.isResourceKey()) {
         delegate = delegate.resolveSelector(selector);
         selector = delegate.getStandardIconSelector();
      }
      if(forceFullyQualified) {
         resource = getURLFactory().getHREF(selector.getIconKey(),forceFullyQualified);
      } else {
         resource = getURLFactory().getHREF(selector.getIconKey());
      }
      if (VERBOSE) {
         System.out.println("Resource: " + resource);
      }

      return resource;
   }

   /**
    *  Returns a String containing the URL to the icon for the given class. The
    *  returned URL is generated using the URLFactory in the Method Context, if any.
         *  If none is present, a new URLFactory is used and an absolute URL is returned.
    *  This method does not take into consideration any glyphs that may
    *  be associated with the given object. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  obj_class                      the Class whose icon is desired (input)
    *@return                                the String URL to the icon file of the
    *                                       given Class
    *@exception  WTException                if an error occurs retrieving the icon
    *      file
    *@exception  PropertyVetoException      if an error occurs retrieving the icon
    *      file
    *@exception  InvocationTargetException  if an error occurs retrieving the icon
    *      file
    *@exception  IllegalAccessException     if an error occurs retrieving the icon
    *      file
    */
   private static String getIconResource(Class obj_class) throws WTException,
         PropertyVetoException,
         InvocationTargetException,
         IllegalAccessException {
      String resource = null;
      if (VERBOSE) {
         System.out.println("Attempting to get IconDelegateFactory...");
      }
      IconDelegateFactory factory = new IconDelegateFactory();
      IconDelegate delegate = factory.getIconDelegate(obj_class);
      IconSelector selector = delegate.getStandardIconSelector();
      while (!selector.isResourceKey()) {
         delegate = delegate.resolveSelector(selector);
         selector = delegate.getStandardIconSelector();
      }

      if ((selector != null) &&
            (selector.getIconKey() != null) &&
            (selector.getIconKey().length() > 0)) {
         resource = getURLFactory().getHREF(selector.getIconKey());
      }

      if (VERBOSE) {
         System.out.println("Resource: " + resource);
      }

      return resource;
   }

   /**
    *  Returns a String containing the URL to the icon for the given class. The
    *  returned URL is generated using the URLFactory in the Method Context, if any.
    *  If none is present, a new URLFactory is used and an absolute URL is returned.
    *  This method does not take into consideration any glyphs that may
    *  be associated with the given object. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  iconSelector                   IconSelector containing the icon image file name (input)
    *@param  forceFullyQualified            true if a fully qualified URL is needed, false if not (input)
    *@return                                the String URL to the icon file of the
    *                                       given Class
    *@exception  WTException                if an error occurs retrieving the icon
    *      file
    *@exception  PropertyVetoException      if an error occurs retrieving the icon
    *      file
    *@exception  InvocationTargetException  if an error occurs retrieving the icon
    *      file
    *@exception  IllegalAccessException     if an error occurs retrieving the icon
    *      file
    */
   public static String getIconResource(IconSelector iconSelector, boolean forceFullyQualified){
      String resource = null;
      if (forceFullyQualified) {
         resource = getURLFactory().getHREF(iconSelector.getIconKey(),forceFullyQualified);
      }
      else {
         resource = getURLFactory().getHREF(iconSelector.getIconKey());
      }
      return resource;
   }
    /**
    *  Returns an IconSelector for the given object.  The IconSelector
    *  contains the icon image file name and the names of the appropriate
    *  glyphs.
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  obj                            the WTObject whose icon is desired (input)
    *@param  a_State                        The HTTPState object associated with the current page (input)
    *@param  sharedFrom                     The following values are recognized:
    *
    *      <ul>
    *        <li> IconDelegate.DISPLAY_SHARED_FROM_GLYPH - display a glyph on the icon
    *        indicating the object has been shared into the current container from
    *        another container </li>
    *        <li> IconDelegate.NO_SHARED_FROM_GLYPH - don't display a glyph indicating the
    *        object has been shared into the container from another container
    *        </li>
    *        <li> IconDelegate.COMPUTE_SHARED_FROM_GLYPH - using the value of the
    *        currentContainerContext, determine if the object has been shared into
    *        the current container from another container and display the
    *        shared-from glyph if it has. </li>
    *      </ul>
    *
    *@param  sharedTo                       The following values are recognized:
    *
    *      <ul>
    *        <li> IconDelegate.DISPLAY_SHARED_TO_GLYPH - display a glyph on the icon indicating
    *        the object has been shared to another container by its owning
    *        container</li>
    *        <li> IconDelegate.NO_SHARED_TO_GLYPH - don't display a glyph indicating the object
    *        has been shared to another container</li>
    *        <li> IconDelegate.COMPUTE_SHARED_TO_GLYPH - if obj is a SubFolder and the
    *        currentContainerContext is the objects's owning container determine
    *        if the object has been shared to another container and display the
    *        shared-to glyph if it has. This computation requires a database
    *        query.</li>
    *      </ul>
    *
    *@param  contextContainer        The container forming the context from
    *      which the object is being viewed. Required if sharedFrom =
    *      COMPUTE_SHARED_FROM_GLYPH or sharedTo = COMPUTE_SHARED_TO_GLYPH.
    *      Otherwise may be null.
    *
    *@return                                IconSelector
    *@exception  WTException                if an error occurs retrieving the icon
    *      file
    *@exception  PropertyVetoException      if an error occurs retrieving the icon
    *      file
    *@exception  InvocationTargetException  if an error occurs retrieving the icon
    *      file
    *@exception  IllegalAccessException     if an error occurs retrieving the icon
    *      file
    */
   public static IconSelector getIconSelector(WTObject obj, HTTPState a_State,
                                         String sharedFrom,String sharedTo,
                                         WTContainer contextContainer)
              throws WTException,PropertyVetoException,
                     InvocationTargetException,IllegalAccessException {
      if (VERBOSE) {
          System.out.println("\nEnter BasicTemplateProcessor.getIconSelector(WTObject,HTTPState,String,String,WTContainer)");
          System.out.println("--- obj: " + obj.getDisplayIdentity());
          System.out.println("--- sharedFrom = " + sharedFrom);
          System.out.println("--- sharedTo  = " + sharedTo);
          if (contextContainer != null) {
             System.out.println("--- contextContainer = " + contextContainer.getDisplayIdentity());
          }
          else {
              System.out.println("--- contextContainer = null");
          }
      }
      IconDelegateFactory factory = new IconDelegateFactory();
      IconDelegate delegate = factory.getIconDelegate(obj);
      delegate.setShareGlyphDisplay(sharedFrom,sharedTo,contextContainer);
      if (VERBOSE) System.out.println("--- IconDelegate is " + delegate.getClass().getName());
      IconSelector iconSelector = null, newIconSelector = null;
      //dbhate : 6-May-2002 : Special case for icon delegates that needs 'state' do decide the icon
      if (isWildfireIconDelegate(delegate)) {
         iconSelector = getWildfireIconSelector(delegate, a_State);
      }

      if (iconSelector == null) {
         iconSelector = delegate.getStandardIconSelector();
      }

      while (!iconSelector.isResourceKey()) {
         delegate = delegate.resolveSelector(iconSelector);
         newIconSelector = delegate.getStandardIconSelector();
         newIconSelector.setAdornments(iconSelector.getAdornments());
         iconSelector = newIconSelector;
      }
      if (VERBOSE) {
          System.out.println("Exit BasicTemplateProcessor.getIconSelector(WTObject,HTTPState,String,String,WTContainer)");
          System.out.println("--- returning IconSelector with image = " + iconSelector.getIconKey());
          System.out.println("--- glyphs:");
          Vector glyphs = iconSelector.getAdornments();
          for (int i = 0; i < glyphs.size(); i++) {
             System.out.println("      " + glyphs.elementAt(i));
          }
      }
      return iconSelector;
   }

   /**
    *  Returns a String containing the HTML code to display the heading for the
    *  folder contents table. This method is used by <A
    *  HREF="#displayFolderContents">displayFolderContents</A> to display the
    *  contents of a folder. <BR>
    *  <BR>
    *  <B>Supported API:</B> false
    *
    *@param  locale  Description of the Parameter
    *@return         the string containing the HTML code to display the heading
    *      for the table of folder contents.
    */
   private static String getFolderTableHeading(Locale locale) {
      String row = "";
      String bgColor = getWCColor("t1-bg-col-head");
      String fColor = getWCColor("t1-f-col-head");
      String fFace = getWCFontFamily();

      String label = getDisplayName(wt.folder.CabinetBased.class,
            wt.folder.CabinetBased.NAME, locale);
      row = row.concat(HtmlUtil.addTableHeading("bgcolor=" + bgColor + " COLSPAN=2 ALIGN=LEFT",
            "<FONT FACE=\"" + fFace + "\" COLOR=" + fColor + "><B>" + label));

      label = getDisplayName(wt.doc.WTDocumentMaster.class, "number", locale);
      row = row.concat(HtmlUtil.addTableHeading("bgcolor=" + bgColor + " ALIGN=LEFT",
            "<FONT FACE=" + fFace + "\" COLOR=" + fColor + "><B>" + label));

      label = getDisplayName(wt.enterprise.RevisionControlled.class, "versionDisplayIdentifier", locale);
      row = row.concat(HtmlUtil.addTableHeading("bgcolor=" + bgColor + " ALIGN=LEFT",
            "<FONT FACE=" + fFace + "\" COLOR=" + fColor + "><B>" + label));

      label = getDisplayName(wt.lifecycle.LifeCycleManaged.class,
            "lifeCycleState", locale);
      row = row.concat(HtmlUtil.addTableHeading("bgcolor=" + bgColor + " ALIGN=LEFT",
            "<FONT FACE=" + fFace + "\" COLOR=" + fColor + "><B>" + label));

      label = getDisplayName(wt.fc.WTObject.class, "displayType", locale);
      row = row.concat(HtmlUtil.addTableHeading("bgcolor=" + bgColor + " ALIGN=LEFT",
            "<FONT FACE=" + fFace + "\" COLOR=" + fColor + "><B>" + label));

      label = getDisplayName(wt.fc.WTObject.class, "modifyTimestamp", locale);
      row = row.concat(HtmlUtil.addTableHeading("bgcolor=" + bgColor + " ALIGN=LEFT",
            "<FONT FACE=" + fFace + "\" COLOR=" + fColor + "><B>" + label));
      label = getDisplayName(wt.team.TeamManaged.class, "teamId", locale);
      row = row.concat(HtmlUtil.addTableHeading("bgcolor=" + bgColor + " ALIGN=LEFT",
            "<FONT FACE=" + fFace + "\" COLOR=" + fColor + "><B>" + label));

      return row;
   }

   /**
    *  Returns the String value of the given attribute on the given object. This
    *  method uses introspection to find the value of the given attribute on the
    *  given object. <BR>
    *  <BR>
    *  <B>Supported API: </B> false<BR>
    *  <BR>
    *
    *
    *@param  obj   the object for which the attribute value is returned
    *@param  attr  the attribute whose value is to be returned
    *@return       the String value of the given attribute on the given object
    */
   private static String getAttrValue(Object obj, String attr) {
      String value = "";
      if (classInfos != null) {
         ClassInfo class_info = getClassInfo(obj.getClass());
         if (class_info != null) {
            try {
               PropertyDescriptor descriptor = class_info.getPropertyDescriptor(attr);
               if (descriptor != null) {
                  Method read_method = descriptor.getReadMethod();
                  if (read_method != null) {
                     try {
                        Object result = read_method.invoke(obj, null);
                        value = result.toString();
                        if (VERBOSE) {
                           System.out.println("Attribute: " + attr +
                                 "  Value:  " + value);
                        }
                     } catch (Exception e) {
                        value = BLANK_SPACE;
                     }
                  }
               }
            } catch (WTIntrospectionException wtie) {
               value = BLANK_SPACE;
            }
         }
      }
      return value;
   }

   /**
    *  Returns the display name corresponding to the given attribute for the given
    *  class. This method uses introspection to retrieve the display name of the
    *  given attribute for the given class. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  class_name  the Class which contains the attribute whose display name
    *      is returned
    *@param  attr        the attribute whose display name is returned
    *@param  locale      Description of the Parameter
    *@return             the display name of the given attribute
    */
   protected static String getDisplayName(Class class_name, String attr, Locale locale) {
      String display_name = "";
      if (classInfos != null) {
         ClassInfo class_info = getClassInfo(class_name);
         if (class_info != null) {
            try {
               PropertyDescriptor descriptor = class_info.getReadPropertyDescriptor(attr);
               if (descriptor != null) {
                  display_name = ClassInfo.getPropertyDisplayName(descriptor, locale);

                  if (VERBOSE) {
                     System.out.println("\nAttribute: " + attr +
                           "Display Name: " + display_name);
                  }
               }
            } catch (WTIntrospectionException wtie) {
               display_name = "";

               if (VERBOSE) {
                  System.out.println("getDisplayName( " + class_name + ", " + attr + ")");
                  wtie.printStackTrace();
               }
            }
         }
      }
      return display_name;
   }

   /**
    *  Returns the ClassInfo object corresponding to the given Class. If an error
    *  occurs retrieving the ClassInfo, null is returned. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  class_name  the class for which the ClassInfo is returned
    *@return             the ClassInfo corresponding to the given class, or null
    *      if an error occurs retrieving the ClassInfo
    */
   protected static ClassInfo getClassInfo(Class class_name) {
      ClassInfo class_info = (ClassInfo) classInfos.get(class_name);
      if (class_info == null) {
         try {
            class_info = WTIntrospector.getClassInfo(class_name);
            classInfos.put(class_name, class_info);
         } catch (WTIntrospectionException wtie) {
            class_info = null;
         }
      }

      return class_info;
   }

   /**
    *  Prints the folder location of the FolderEntry context object to the given
    *  OutputStream. If the context object is not a FolderEntry object, nothing is
    *  output <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  parameters  the Properties passed in when this method is invoked.
    *      This parameter is not used
    *@param  locale      the Locale of the client
    *@param  os          the OutputStream which the folder location is written to
    */
   public void getFolderLocation(Properties parameters, Locale locale, OutputStream os) {
      Object obj = getContextObj();
      if (!(obj instanceof CabinetBased)) {
         return;
      }

      PrintWriter out = getPrintWriter(os, locale);
      String location = ((CabinetBased) obj).getLocation();
      out.println(HTMLEncoder.encodeForHTMLContent(location));
      out.flush();
   }

   /**
    *  Prints the folder path of the FolderEntry context object to the given
    *  OutputStream. If the context object is not a FolderEntry object, nothing is
    *  output <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  parameters  the Properties passed in when this method is invoked.
    *      This parameter is not used
    *@param  locale      the Locale of the client
    *@param  os          the OutputStream which the folder path is written to
    */
   public void getFolderPath(Properties parameters, Locale locale, OutputStream os) {
      Object obj = getContextObj();
      if (!(obj instanceof CabinetBased)) {
         return;
      }

      PrintWriter out = getPrintWriter(os, locale);
      String location = ((CabinetBased) obj).getFolderPath();
      out.println(HTMLEncoder.encodeForHTMLContent(location));
      out.flush();
   }

   /**
    *  Gets the domainPath attribute of the BasicTemplateProcessor object
    *
    *@param  parameters       Description of the Parameter
    *@param  locale           Description of the Parameter
    *@param  os               Description of the Parameter
    *@exception  WTException  Description of the Exception
    */
   public void getDomainPath(Properties parameters, Locale locale, OutputStream os) throws WTException {
      WTPreferences root = (WTPreferences) WTPreferences.root();
      WTPreferences myNode = (WTPreferences) root.node("/wt/admin");
      String[] context = PreferenceHelper.createContextMask();
      myNode.setContextMask(context);
      if (!(myNode.getBoolean("displayDomains", false))) {
         return;
      }
      Object obj = getContextObj();
      if (!(obj instanceof DomainAdministered)) {
         return;
      }
      ResourceBundle resource = ResourceBundle.getBundle(RESOURCE, locale);
      String message = "";
      String label = HtmlUtil.addTableEntry("WIDTH=\"15%\" ALIGN=RIGHT",
            HtmlUtil.formatText("B", resource.getString(enterpriseResource.DOMAIN_LABEL)));
      String domain_path =
            AdministrativeDomainHelper.manager.getDisplayDomainPath(((DomainAdministered) obj).getDomainRef());
      if ((obj instanceof Foldered) && (((DomainAdministered) obj).isInheritedDomain())) {
         Folder parent = FolderHelper.service.getFolder((Foldered) obj);
         Object[] params = {domain_path};
         if (parent instanceof SubFolder) {
            domain_path = WTMessage.getLocalizedMessage(RESOURCE, enterpriseResource.INHERITED_FROM_FOLDER, params, locale);
         } else if (parent instanceof Cabinet) {
            domain_path = WTMessage.getLocalizedMessage(RESOURCE, enterpriseResource.INHERITED_FROM_CABINET, params, locale);
         }
      }
      domain_path = HtmlUtil.addTableEntry("ALIGN=LEFT", domain_path);
      message = HtmlUtil.addTableRow("", label + domain_path);

      PrintWriter out = getPrintWriter(os, locale);
      out.println(message);
      out.flush();
   }

   /**
    *  Gets the HTML tag(s) to display the icon and glyphs associated with a given object.
    *  The urls to the icon image file(s) are generated using the URLFactory
    *  associated with the key "URLFactory" in the Method Context, if any.
    *  If none is present, a new URLFactory is used and absolute URLs are returned.
    *
    *@param  obj                            The object whose icon is desired
    *@return                                One or more image tags for the icon and glyphs of
         *                                       the given object
    *@exception  WTException                Description of the Exception
    *@exception  PropertyVetoException      Description of the Exception
    *@exception  InvocationTargetException  Description of the Exception
    *@exception  IllegalAccessException     Description of the Exception
    */
   public static String getObjectIconImgTag(WTObject obj)
          throws WTException, PropertyVetoException,
         InvocationTargetException, IllegalAccessException {
      return getObjectIconImgTag(obj, null, null, null, IconDelegate.NO_SHARED_FROM_GLYPH, IconDelegate.NO_SHARED_TO_GLYPH, null);
   }

   /**
    *  Gets the HTML tag(s) to display the icon and glyphs associated with a given object.
    *  The urls to the icon image file(s) are generated using the URLFactory
    *  associated with the key "URLFactory" in the Method Context, if any.
    *  If none is present, a new URLFactory is used and absolute URLs are returned.
    *
    *@param  obj                            Object whose icon is desired
    *@param  forceFullyQualified            True if image urls must be fully qualified
         *                                       regardless of the state of the existing URLFactory, if any
    *@return                                One or more image tags for the icon and glyphs
         *                                       associated with the given object
    *@exception  WTException                Description of the Exception
    *@exception  PropertyVetoException      Description of the Exception
    *@exception  InvocationTargetException  Description of the Exception
    *@exception  IllegalAccessException     Description of the Exception
    */
   public static String getObjectIconImgTag(WTObject obj,boolean forceFullyQualified)
          throws WTException, PropertyVetoException,
         InvocationTargetException, IllegalAccessException {
            if(forceFullyQualified) {
               return getObjectIconImgTag(obj, null, null, null, IconDelegate.NO_SHARED_FROM_GLYPH, IconDelegate.NO_SHARED_TO_GLYPH, null,forceFullyQualified);
            } else {
               return getObjectIconImgTag(obj, null, null, null, IconDelegate.NO_SHARED_FROM_GLYPH, IconDelegate.NO_SHARED_TO_GLYPH, null);
            }
   }

   /**
    *  Gets the HTML tag(s) to display the icon and glyphs associated with a given object.
    *  The urls to the icon image file(s) are generated using the URLFactory
    *  associated with the key "URLFactory" in the Method Context, if any.
    *  If none is present, a new URLFactory is used and absolute URLs are returned.
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  obj                            The WTObject whose icon is desired
    *@param  backgroundColor                Background color.  Optional.  Used for drag and drop.
    *@return                                a String containing the HTML code to
    *                                       display the icon and glyphs associated with the given WTObject.
    *@exception  WTException                if an error occurs retrieving the icon
    *@exception  PropertyVetoException      if an error occurs retrieving the icon
    *@exception  InvocationTargetException  if an error occurs retrieving the icon
    *@exception  IllegalAccessException     if an error occurs retrieving the icon
    */
   public static String getObjectIconImgTag(WTObject obj, String backgroundColor)
          throws WTException, PropertyVetoException,
         InvocationTargetException, IllegalAccessException {
      return (getObjectIconImgTag(obj, backgroundColor, null, null,
            IconDelegate.NO_SHARED_FROM_GLYPH, IconDelegate.NO_SHARED_TO_GLYPH, null));
   }

   /**
    *  Gets the HTML tag(s) to display the icon and glyphs associated with a given object.
    *  The urls to the icon image file(s) are generated using the URLFactory
    *  associated with the key "URLFactory" in the Method Context, if any.
    *  If none is present, a new URLFactory is used and absolute URLs are returned.
    * <BR>
    *  If object type is draggable and the
    *  drag and drop preference is on, the HTML will be an applet tag with the
    *  object icon. Otherwise, the HTML will be an IMG tag with the object icon.
    *  <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  obj                            Object whose icon is desired.
    *      Required.
    *@param  sharedFrom                     The following values are recognized:
    *
    *      <ul>
    *        <li> IconDelegate.DISPLAY_SHARED_FROM_GLYPH - display a glyph on the icon
    *        indicating the object has been shared into the current container from
    *        another container </li>
    *        <li> IconDelegate.NO_SHARED_FROM_GLYPH - don't display a glyph indicating the
    *        object has been shared into the container from another container
    *        </li>
    *        <li> IconDelegate.COMPUTE_SHARED_FROM_GLYPH - using the value of the
    *        currentContainerContext, determine if the object has been shared into
    *        the current container from another container and display the
    *        shared-from glyph if it has. </li>
    *      </ul>
    *
    *@param  sharedTo                       The following values are recognized:
    *
    *      <ul>
    *        <li> IconDelegate.DISPLAY_SHARED_TO_GLYPH - display a glyph on the icon indicating
    *        the object has been shared to another container by its owning
    *        container</li>
    *        <li> IconDelegate.NO_SHARED_TO_GLYPH - don't display a glyph indicating the object
    *        has been shared to another container</li>
    *        <li> IconDelegate.COMPUTE_SHARED_TO_GLYPH - if obj is a SubFolder and the
    *        currentContainerContext is the objects's owning container determine
    *        if the object has been shared to another container and display the
    *        shared-to glyph if it has. This computation requires a database
    *        query.</li>
    *      </ul>
    *
    *@param  currentContainerContext        The container forming the context from
    *      which the object is being viewed. Required if sharedFrom =
    *      COMPUTE_SHARED_FROM_GLYPH or sharedTo = COMPUTE_SHARED_TO_GLYPH.
    *      Otherwise may be null.
    *@return                                applet or img tag
    *@exception  WTException                Description of the Exception
    *@exception  PropertyVetoException      Description of the Exception
    *@exception  InvocationTargetException  Description of the Exception
    *@exception  IllegalAccessException     Description of the Exception
    */
   public static String getObjectIconImgTag(WTObject obj, String sharedFrom, String sharedTo, WTContainer currentContainerContext)
          throws WTException, PropertyVetoException,
         InvocationTargetException, IllegalAccessException {
      return getObjectIconImgTag(obj, sharedFrom, sharedTo, currentContainerContext, false);
   }

   /**
    *  Gets the HTML tag(s) to display the icon and glyphs associated with a given object.
    *  The urls to the icon image file(s) are generated using the URLFactory
    *  associated with the key "URLFactory" in the Method Context, if any.
    *  If none is present, a new URLFactory is used and absolute URLs are returned.
    *  <BR>
    *  If object type is draggable and the
    *  drag and drop preference is on, the HTML will be an applet tag with the
    *  object icon. Otherwise, the HTML will be one or more IMG tags for the
         *  object icon and glyphs.
    *  <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  obj                            Object whose icon is desired.
    *      Required.
    *@param  sharedFrom                     The following values are recognized:
    *
    *      <ul>
    *        <li> IconDelegate.DISPLAY_SHARED_FROM_GLYPH - display a glyph on the icon
    *        indicating the object has been shared into the current container from
    *        another container </li>
    *        <li> IconDelegate.NO_SHARED_FROM_GLYPH - don't display a glyph indicating the
    *        object has been shared into the container from another container
    *        </li>
    *        <li> IconDelegate.COMPUTE_SHARED_FROM_GLYPH - using the value of the
    *        currentContainerContext, determine if the object has been shared into
    *        the current container from another container and display the
    *        shared-from glyph if it has. </li>
    *      </ul>
    *
    *@param  sharedTo                       The following values are recognized:
    *
    *      <ul>
    *        <li> IconDelegate.DISPLAY_SHARED_TO_GLYPH - display a glyph on the icon indicating
    *        the object has been shared to another container by its owning
    *        container</li>
    *        <li> IconDelegate.NO_SHARED_TO_GLYPH - don't display a glyph indicating the object
    *        has been shared to another container</li>
    *        <li> IconDelegate.COMPUTE_SHARED_TO_GLYPH - if obj is a SubFolder and the
    *        currentContainerContext is the objects's owning container determine
    *        if the object has been shared to another container and display the
    *        shared-to glyph if it has. This computation requires a database
    *        query.</li>
    *      </ul>
    *
    *@param  currentContainerContext        The container forming the context from
    *      which the object is being viewed. Required if sharedFrom =
    *      COMPUTE_SHARED_FROM_GLYPH or sharedTo = COMPUTE_SHARED_TO_GLYPH.
    *      Otherwise may be null.
    *@param  forceFullyQualified            if true, returns the fully qualified
    *                                       url for the icon
    *@return                                applet or img tag
    *@exception  WTException                Description of the Exception
    *@exception  PropertyVetoException      Description of the Exception
    *@exception  InvocationTargetException  Description of the Exception
    *@exception  IllegalAccessException     Description of the Exception
    */
   public static String getObjectIconImgTag(WTObject obj, String sharedFrom, String sharedTo, WTContainer currentContainerContext,boolean forceFullyQualified)
          throws WTException, PropertyVetoException,
         InvocationTargetException, IllegalAccessException {
            if(forceFullyQualified) {
               return getObjectIconImgTag(obj, null, null, null, sharedFrom, sharedTo, currentContainerContext,forceFullyQualified);
            } else {
               return getObjectIconImgTag(obj, null, null, null, sharedFrom, sharedTo, currentContainerContext);
            }
   }

   /**
    *  Gets the HTML tag(s) to display the icon and glyphs associated with a given object.
    *  The urls to the icon image file(s) are generated using the URLFactory
    *  associated with the key "URLFactory" in the Method Context, if any.
    *  If none is present, a new URLFactory is used and absolute URLs are returned.
    *
    *@param  obj                            Object whose icon is desired
    *@param  backgroundColor                Background color. Optional. Used for drag and drop.
    *@param  a_State                        HTTPState object associated with the current page
    *@return                                The objectIconImgTag value
    *@exception  WTException                Description of the Exception
    *@exception  PropertyVetoException      Description of the Exception
    *@exception  InvocationTargetException  Description of the Exception
    *@exception  IllegalAccessException     Description of the Exception
    */
   private static String getObjectIconImgTag(WTObject obj, String backgroundColor, HTTPState a_State)
          throws WTException, PropertyVetoException, InvocationTargetException, IllegalAccessException {
      return (getObjectIconImgTag(obj, backgroundColor, null, a_State,
                                  IconDelegate.NO_SHARED_FROM_GLYPH, IconDelegate.NO_SHARED_TO_GLYPH, null));
   }

   /**
    *  Gets the HTML tag(s) to display the icon and glyphs associated with a given object.
    *  The urls to the icon image file(s) are generated using the URLFactory
    *  associated with the key "URLFactory" in the Method Context, if any.
    *  If none is present, a new URLFactory is used and absolute URLs are returned.
    *
    *@param  obj                            Object whose icon is desired
    *@param  backgroundColor                Background color.  Optional.  Used for drag and drop.
    *@param  holder                         Optional. Used for drag and drop.
    *@param  a_State                        HTTPState object associated with the current page
    *@return                                one or more image tags
    *@exception  WTException                Description of the Exception
    *@exception  PropertyVetoException      Description of the Exception
    *@exception  InvocationTargetException  Description of the Exception
    *@exception  IllegalAccessException     Description of the Exception
    */
   private static String getObjectIconImgTag(WTObject obj, String backgroundColor,
         ContentHolder holder, HTTPState a_State)
          throws WTException, PropertyVetoException,
         InvocationTargetException, IllegalAccessException {
      return getObjectIconImgTag(obj, backgroundColor, holder, a_State,
                                 IconDelegate.NO_SHARED_FROM_GLYPH, IconDelegate.NO_SHARED_TO_GLYPH, null);
   }

   /**
    *  Gets the HTML tag(s) to display the icon and glyphs associated with a given object.
    *  The urls to the icon image file(s) are generated using the URLFactory
    *  associated with the key "URLFactory" in the Method Context, if any.
    *  If none is present, a new URLFactory is used and absolute URLs are returned.
    *
    *@param  obj                            Object whose icon is desired
    *@param  backgroundColor                Background color.  Optional.  Used for drag and drop.
    *@param  holder                         Optional.  Used for drag and drop.
    *@param  a_State                        HTTPState object associated with the current page
    *@param  sharedFrom                     The following values are recognized:
    *
    *      <ul>
    *        <li> IconDelegate.DISPLAY_SHARED_FROM_GLYPH - display a glyph on the icon
    *        indicating the object has been shared into the current container from
    *        another container </li>
    *        <li> IconDelegate.NO_SHARED_FROM_GLYPH - don't display a glyph indicating the
    *        object has been shared into the container from another container
    *        </li>
    *        <li> IconDelegate.COMPUTE_SHARED_FROM_GLYPH - using the value of the
    *        currentContainerContext, determine if the object has been shared into
    *        the current container from another container and display the
    *        shared-from glyph if it has.</li>
    *      </ul>
    *
    *@param  sharedTo                       The following values are recognized:
    *
    *      <ul>
    *        <li> IconDelegate.DISPLAY_SHARED_TO_GLYPH - display a glyph on the icon indicating
    *        the object has been shared to another container by its owning
    *        container</li>
    *        <li> IconDelegate.NO_SHARED_TO_GLYPH - don't display a glyph indicating the object
    *        has been shared to another container</li>
    *        <li> IconDelegate.COMPUTE_SHARED_TO_GLYPH - if obj is a SubFolder and the
    *        currentContainerContext is the objects's owning container determine
    *        if the object has been shared to another container and display the
    *        shared-to glyph if it has. This computation requires a database
    *        query.</li>
    *      </ul>
    *
    *@param  currentContainerContext        The container forming the context from
    *      which the object is being viewed. Required if sharedFrom =
    *      COMPUTE_SHARED_FROM_GLYPH or sharedTo = COMPUTE_SHARED_TO_GLYPH.
    *      Otherwise may be null.
    *@return                                one or more image tags
    *@exception  WTException                Description of the Exception
    *@exception  PropertyVetoException      Description of the Exception
    *@exception  InvocationTargetException  Description of the Exception
    *@exception  IllegalAccessException     Description of the Exception
    */
   private static String getObjectIconImgTag(WTObject obj, String backgroundColor,
         ContentHolder holder, HTTPState a_State, String sharedFrom,
         String sharedTo, WTContainer currentContainerContext)
          throws WTException, PropertyVetoException,
         InvocationTargetException, IllegalAccessException {

         return getObjectIconImgTag(obj, backgroundColor, holder, a_State,
                                    IconDelegate.NO_SHARED_FROM_GLYPH, IconDelegate.NO_SHARED_TO_GLYPH, null,false);
   }

   /**
    *  Gets the HTML tag(s) to display the icon and glyphs associated with a given object.
    *  The urls to the icon image file(s) are generated using the URLFactory
    *  associated with the key "URLFactory" in the Method Context, if any.
    *  If none is present, a new URLFactory is used and absolute URLs are returned.
    *  <BR>
    *  If object type is draggable and the
    *  drag and drop preference is on, the HTML will be an applet tag with the
    *  object icon. Otherwise, the HTML will be an IMG tag with the object icon.
    *  <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  obj                            Object whose icon is desired.
    *                                       Required.
    *@param  backgroundColor                Background color. Optional. Used for
    *                                       drag and drop.
    *@param  holder                         Optional. Used for drag and drop.
c    *@param  a_State                        State of the object
    *@param  forceFullyQualified            get the fully qualified path of the icon
    *@return                                applet or img tag
    *@exception  WTException                Description of the Exception
    *@exception  PropertyVetoException      Description of the Exception
    *@exception  InvocationTargetException  Description of the Exception
    *@exception  IllegalAccessException     Description of the Exception
    */
   private static String getObjectIconImgTag(WTObject obj, String backgroundColor,
         ContentHolder holder, HTTPState a_State, String sharedFrom,
         String sharedTo, WTContainer currentContainerContext,boolean forceFullyQualified)
          throws WTException, PropertyVetoException,
         InvocationTargetException, IllegalAccessException {
      if (VERBOSE) {
         System.out.println("\nEntered BasicTemplateProcessor.getObjectIconImgTag(WTObject,String,ContentHolder,HTTPState,String,String,WTContainer,boolean)");
         System.out.println("--- object = " + IdentityFactory.getDisplayIdentifier(obj).getLocalizedMessage(Locale.ENGLISH));
         System.out.println("--- sharedTo = " + sharedTo + "; sharedFrom = " + sharedFrom);
         if (currentContainerContext != null) {
            System.out.println("--- currentContextContainer = " + currentContainerContext.getDisplayIdentity());
         }
         else {
            System.out.println("--- currentContextContainer = null");
         }
         System.out.println("--- forceFullyQualified = " + forceFullyQualified);
      }
      // DND applet tag code tries to get HTTPRequest object from Method Context.
      // If it is not available, don't turn DND on because an exception will be thrown.
      boolean shouldDoApplet = DnDMicroAppletUtil.isDndObject(obj) && isDndMicroAppletEnabled()
                              && wt.httpgw.HTTPServer.getHTTPRequest() != null;
      if (backgroundColor == null) {
         backgroundColor = getWCColor("dndAppletDefaultBGColor");
      }

      String image_resource =null;
      IconSelector iconSelector = getIconSelector(obj, a_State,sharedFrom,
                                                sharedTo,currentContainerContext);
      image_resource = getIconResource(iconSelector,forceFullyQualified);

      Vector glyphResources = getObjectIconGlyphs(iconSelector, shouldDoApplet);
      String image = "";

      boolean appletSetupFailed = false;
      if (shouldDoApplet) {
         try {
            String glyphUrlString = ((glyphResources.size() > 0) ? (CODEBASE + '/' + (String) glyphResources.elementAt(0)) : null);
            image = DnDMicroAppletUtil.makeAppletTag(obj, CODEBASE,
                                                image_resource,
                  glyphUrlString,
                  backgroundColor,
                  holder);
         } catch (Exception e) {
            appletSetupFailed = true;
            glyphResources = null;
            if (VERBOSE) {
               System.err.println("BasicTemplateProcessor.getObjectIconImg(): could not construct drag-and-drop applet tag.  Using non-drag-and-drop icon.");
            }
         }
                        // Append the GenericInstance to ProE EPMDocument
                        // if necessary. Make sure to do append image html
                        // after other glyphs.
                        if ( image.length() > 0 ) {
                 image = image + getGenericInstanceIconImgTag(obj);
              }
      }
      if (!shouldDoApplet || appletSetupFailed) {
            String objType = getObjectType(obj);
         StringBuffer buffer = new StringBuffer();
         if (image_resource != null) {
            buffer.append("<IMG BORDER=0 SRC=\"");
            buffer.append(image_resource);
            buffer.append("\" alt=\"" + objType + "\" title=\"" + objType +"\">");
            buffer.append(getGlyphTagsNoDnD(obj,glyphResources,forceFullyQualified));
            image = buffer.toString();
         }
      }
      if (VERBOSE) {
         System.out.println("Exit BasicTemplateProcessor.getObjectIconImgTag(WTObject,String,ContentHolder,HTTPState,String,String,WTContainer,boolean)");
      }
      return (image);
   }

   /**
     *  Returns a string for the type of the object (e.g. Part, Document), localized if possible
     **/
   private static String getObjectType(WTObject obj) {
      String type = "";
      Locale loc = null;
      if (obj != null) {
                    try {
                       loc = SessionHelper.getLocale();
                    }
                    catch (WTException wte) {}
                    if (loc != null) {
                       type = obj.getDisplayType().getLocalizedMessage(loc);
                    }
                    else {
                       if (VERBOSE) {
                          System.err.println("Cannot localize tool tip for object icon: cannot retrieve locale from session");
                       }
                       type = obj.getType();
                    }
      }
      return type;
   }

   /**
    *  Gets the tags for an object's icon glyphs (no drag and drop).
         *  A family table glyph is included for CAD documents, if appropriate.
         *  The urls to the icon image file(s) are generated using the URLFactory
         *  associated with the key "URLFactory" in the Method Context, if any.
         *  If none is present, a new URLFactory is used and absolute URLs are returned.
    *
         *@param   obj                     Object whose glyphs are desired (input)
    *@param  glyphResources           Glyph image file paths relative to codebase.
         *                                 If null and no family table glyph, an empty
         *                                 string will be returned. (input)
         *@param  forceFullyQualified      True if urls to images must be fully qualifed regardless
         *                                 of the state of an existing URLFactory (input)
    *@return                          Image tags referencing the input glyph image files
         *                                 (may be up to 4 tags in the string: 3 glyphs and family table icon
         *
    *@exception  WTException          Description of the Exception
    */
   public static String getGlyphTagsNoDnD(WTObject obj,Vector glyphResources,boolean forceFullyQualified)
            throws WTException {
          StringBuffer buffer = new StringBuffer();
          if (glyphResources != null) {
              String objType = getObjectType(obj);

               // upper right
               if (glyphResources.elementAt(0)!= null) {
                        buffer.append("<IMG BORDER=0 class=\"g1\" style=\"z-index:4; visibility: visible; position:relative; margin-left:-16px\" src=\"");
                        if (forceFullyQualified) {
                           buffer.append(getURLFactory().getHREF((String) glyphResources.elementAt(0),forceFullyQualified));
                        }
                        else {
                           buffer.append(getURLFactory().getHREF((String) glyphResources.elementAt(0)));
                        }
                        buffer.append("\" alt=\"" + objType + "\" title=\"" + objType +"\">");
                }
                // lower right
                if (glyphResources.elementAt(1) != null) {
                        buffer.append("<IMG BORDER=0  class=\"g2\" style=\"z-index:3; visibility: visible; position:relative; margin-left:-16px\" src=\"");
                        if (forceFullyQualified) {
                           buffer.append(getURLFactory().getHREF((String) glyphResources.elementAt(1),forceFullyQualified));
                        }
                        else {
                           buffer.append(getURLFactory().getHREF((String) glyphResources.elementAt(1)));
                        }
                        buffer.append("\" alt=\"" + objType + "\" title=\"" + objType +"\">");
                }
                // lower left
                if (glyphResources.elementAt(2)!= null) {
                        buffer.append("<IMG BORDER=0  class=\"g3\" style=\"z-index:2; visibility: visible; position:relative; margin-left:-18px\" src=\"");
                        if (forceFullyQualified) {
                           buffer.append(getURLFactory().getHREF((String) glyphResources.elementAt(2),forceFullyQualified));
                        }
                        else {
                           buffer.append(getURLFactory().getHREF((String) glyphResources.elementAt(2)));
                        }
                        buffer.append("\" alt=\"" + objType + "\" title=\"" + objType +"\">");
                }
           }
           else {
               buffer.append("");
           }
          // Append the GenericInstance to ProE EPMDocument
          // if necessary. Make sure to do append image html
          // after other glyphs.
          buffer.append(getGenericInstanceIconImgTag(obj));
          return buffer.toString();
        }

   /**
    *  Returns the image files for the glyphs for an object
    *
    *@param  obj                      Object whose glyphs are desired
    *@param  sharedFrom               The following values are recognized:
    *
    *      <ul>
    *        <li> IconDelegate.DISPLAY_SHARED_FROM_GLYPH - display a glyph on the icon
    *        indicating the object has been shared into the current container from
    *        another container </li>
    *        <li> IconDelegate.NO_SHARED_FROM_GLYPH - don't display a glyph indicating the
    *        object has been shared into the container from another container
    *        </li>
    *        <li> IconDelegate.COMPUTE_SHARED_FROM_GLYPH - using the value of the
    *        currentContainerContext, determine if the object has been shared into
    *        the current container from another container and display the
    *        shared-from glyph if it has. </li>
    *      </ul>
    *
    *@param  sharedTo                  The following values are recognized:
    *
    *      <ul>
    *        <li> IconDelegate.DISPLAY_SHARED_TO_GLYPH - display a glyph on the icon indicating
    *        the object has been shared to another container by its owning
    *        container</li>
    *        <li> IconDelegate.NO_SHARED_TO_GLYPH - don't display a glyph indicating the object
    *        has been shared to another container</li>
    *        <li> IconDelegate.COMPUTE_SHARED_TO_GLYPH - if obj is a SubFolder and the
    *        currentContainerContext is the objects's owning container determine
    *        if the object has been shared to another container and display the
    *        shared-to glyph if it has. This computation requires a database
    *        query.</li>
    *      </ul>
    *
    *@param  selector                 IconSelector containing the names of the glyphs to be used
    *@param  currentContainerContext  Current container.  May be null unless sharedFrom is
    *                                 COMPUTE_SHARED_FROM_GLYPH or sharedTo is COMPUTE_SHARED_TO_GLYPH.
    *
    *@param  doDnD                    True if drag and drop glyphs should be returned
    *@return                          Vector of strings containing the file paths of the
         *                                 glyphs relative to codebase
    *@exception  WTException          Description of the Exception
    */
     public static Vector getObjectIconGlyphs(IconSelector selector, boolean doDnD)
          throws WTException {
      if (VERBOSE) {
          System.out.println("\nEntered BasicTempalteProcessor.getObjectIconGlyphs()");
          if (selector != null) {
              System.out.println("--- selector = " + selector.toString());
          }
          else {
              System.out.println("--- selector is null");
          }
          System.out.println("--- doDnD = " + doDnD);
      }
      int SHORTCUT = 0;
      int WORKING_COPY = 1;
      int WIP_CHECKED_OUT = 2;
      int CHECKED_OUT_FROM_PDM = 3;
      int SHARED_TO = 4;
      int SHARED_FROM = 5;

      // The seletor has info on which glyphs are needed.  There will be most three glyphs. Except for share
      // glyphs (16x10), each glyph is drawn 16x16 but the actual picture occupies only a portion
      // of that area (rest is transparent) according to where glyph should appear relative to
      // the icon.
      // We store glyphs according to where they are on the grid:
      // glyph_ids[0] upper right - sandbox working copy (CHECKED_OUT_FROM_PDM)
      // glyph_ids[1] lower right - checked out (to person or project) (WIP_CHECKED_OUT) or
      //                            WIP working copy (WORKING_COPY)
      // glyph_ids[2] lower left - shared in (SHARED_FROM) or
      //                           shared out (SHARED_TO) or
      //                           shortcut (SHORTCUT)
      int[] glyph_ids = new int[3];
      glyph_ids[0] = glyph_ids[1] = glyph_ids[2] = -1;
      if (selector.hasAdornment(RevisionControlledIconDelegate.CHECKED_OUT_FROM_PDM)) {
          glyph_ids[0] = CHECKED_OUT_FROM_PDM;
      }
      if (selector.hasAdornment(RevisionControlledIconDelegate.CHECKED_OUT)) {
          glyph_ids[1] = WIP_CHECKED_OUT;
      }
      else if (selector.hasAdornment(RevisionControlledIconDelegate.WORKABLE)) {
           glyph_ids[1] = WORKING_COPY;
      }
      if (selector.hasAdornment(IconDelegate.SHARED_TO)) {
          glyph_ids[2] = SHARED_TO;
      }
      else if (selector.hasAdornment(IconDelegate.SHARED_FROM)) {
          glyph_ids[2] = SHARED_FROM;
      }
      else if (selector.hasAdornment(ShortcutIconDelegate.SHORTCUT)) {
          glyph_ids[2] = SHORTCUT;
      }
      // Determine which image files to use
      Vector glyph_resources = new Vector();

      // No drag and drop
      if (!doDnD) {
            for (int i = 0; i < glyph_ids.length; i++) {
                 if (glyph_ids[i] >= 0)
                glyph_resources.add(glyphImages[0][glyph_ids[i]]);
                 else
                   glyph_resources.add(null);
         }
      }
      // Drag and drop with no glyphs
      else if (glyph_ids[0] < 0 && glyph_ids[1] < 0 && glyph_ids[2] < 0) {
         glyph_resources.add(DND_GLYPH_IMG);
      }
      // Drag and drop with only one glyph
      else if ((glyph_ids[0] > 0 && glyph_ids[1] < 0 && glyph_ids[2] < 0) ||
               (glyph_ids[0] < 0 && glyph_ids[1] > 0 && glyph_ids[2] < 0) ||
               (glyph_ids[0] < 0 && glyph_ids[1] < 0 && glyph_ids[2] > 0)) {
          if (glyph_ids[0] >= 0)
             glyph_resources.add(glyphImages[1][glyph_ids[0]]);
          else if (glyph_ids[1] >= 0)
             glyph_resources.add(glyphImages[1][glyph_ids[1]]);
          else if (glyph_ids[2] >= 0)
             glyph_resources.add(glyphImages[1][glyph_ids[2]]);
      }
      // Drag and drop and more than one glyph.  Since we can only have one drag and drop
      // glyph we have to find the right composite.  We only handle the combinations known
      // to be possible.  These are the following:
      // Quadrant 1               Quadrant 2                  Quadrant 3
      // ----------               ----------                  ----------
      // CHECKED_OUT_FROM_PDM     WORKING_COPY                 --
      // CHECKED_OUT_FROM_PDM     WIP_CHECKED_OUT              --
      // CHECKED_OUT_FROM_PDM     --                           SHARED_FROM
      // CHECKED_OUT_FROM_PDM     WIP_CHECKED_OUT              SHARED_FROM
      // --                       WIP_CHECKED_OUT              SHARED_FROM
      // --                       WIP_CHECKED_OUT              SHARED_TO*
      // * don't think this is possible but since we already have a gif I'm handling it

      else {
         if (glyph_ids[0] == CHECKED_OUT_FROM_PDM) {
            if (glyph_ids[2] < 0) {  // glyphs in quadrants 1 and 2
               if (glyph_ids[1] == WORKING_COPY) {
                  glyph_resources.add(DND_WIP_WORKCOPY_AND_CHKOUT_FROM_PDM_GLYPH);
               }
               else {
                  glyph_resources.add(DND_WIP_CHECKED_OUT_AND_CHECKED_FROM_PDM_GLYPH);
               }
            }
            else if (glyph_ids[1] < 0) { // glyphs in quadrants 1 and 3
               glyph_resources.add(DND_CHECKED_FROM_PDM_AND_SHARED_FROM_GLYPH);
            }
            else { // glyphs in all three quadrants
               glyph_resources.add(DND_WIP_CHECKED_OUT_AND_CHECKED_OUT_FROM_PDM_AND_SHARED_FROM_GLYPH);
            }
         }
         else if (glyph_ids[1] == WIP_CHECKED_OUT) {   // glyphs in quadrants 2 and 3
            if (glyph_ids[2] == SHARED_FROM) {
               glyph_resources.add(DND_WIP_CHECKED_OUT_AND_SHARED_FROM_GLYPH);
            }
            else {
               glyph_resources.add(DND_WIP_CHECKED_OUT_AND_SHARED_TO_GLYPH);
            }
         }
         else // shouldn't get here
         {
            glyph_resources.add(DND_GLYPH_IMG);
         }
      }

      if (VERBOSE) {
         System.out.println("Exit BasicTemplateProcessor.getObjectIconGlyphs()");
         System.out.println("--- Returning glyphs:");
         for (int i = 0; i < glyph_resources.size(); i++) {
            System.err.println("---   glyph " + i + " = " + glyph_resources.elementAt(i));
         }
      }
      return glyph_resources;
   }

   /**
    *  Gets the sandboxWorkingCopy attribute of the BasicTemplateProcessor class
    *
    *@param  obj              Description of the Parameter
    *@return                  The sandboxWorkingCopy value
    *@exception  WTException  Description of the Exception
    */
   private static boolean isSandboxWorkingCopy(Versioned obj)
          throws WTException {
      return SandboxHelper.isSandboxWorkingCopyNoDBHack(obj);
   }

   /**
    *  Gets the HTML applet tag(s) to display the drag icon and glyphs associated with a given object.
    *
    *@param  obj                            Description of the Parameter
    *@param  backgroundColor                Description of the Parameter
    *@param  holder                         Description of the Parameter
    *@return                                The objectIconDnDTag value
    *@exception  WTException                Description of the Exception
    *@exception  PropertyVetoException      Description of the Exception
    *@exception  InvocationTargetException  Description of the Exception
    *@exception  IllegalAccessException     Description of the Exception
    */
   public static String getObjectIconDnDTag(WTObject obj, String backgroundColor,
         ContentHolder holder)
          throws WTException, PropertyVetoException,
         InvocationTargetException, IllegalAccessException {
      return getObjectIconImgTag(obj, backgroundColor, holder, null,
                                 IconDelegate.NO_SHARED_FROM_GLYPH, IconDelegate.NO_SHARED_TO_GLYPH, null, false);
   }

   /**
    *  Returns the HTML code to display the image corresponding to the given
    *  object class
    *
    *@param  obj_class                      the object class for which the icon is
    *                                       returned
    *@return                                the HTML code to display the image
    *      corresponding to the given object class
    *@exception  WTException                if an error occurs retrieving the icon
    *@exception  PropertyVetoException      if an error occurs retrieving the icon
    *@exception  InvocationTargetException  if an error occurs retrieving the icon
    *@exception  IllegalAccessException     if an error occurs retrieving the icon
    */
   public static String getObjectIconImgTag(Class obj_class)
          throws WTException, PropertyVetoException,
         InvocationTargetException, IllegalAccessException {
      String image_resource = getIconResource(obj_class);
      String image = "";

      if (image_resource != null) {
         image += "<IMG BORDER=0 SRC=\"";
         image += image_resource;
         image += "\">";
      }

      return image;
   }

   /**
    *  Returns a String containing the HTML code to display the glyph associated
    *  with the object. This method returns only the HTML to display the glyph,
    *  and not to display the object icon with the glyph. This method only returns
    *  a single glyph, so, for example, if a shortcut to a checked out object is
    *  given, the shortcut glyph will be returned. This method does not
         *  compute sandbox glyphs. <BR>
    *  <BR>
    *  <B>Supported API: </B> false<BR>
    *  <BR>
         * @deprecated
    *
    *
    *@param  object  the WTObject for which the glyph is returned
    *@return         a String containing the HTML code to display the glyph; if no
    *      glyph is associated with the given object, an empty string is returned.
    */
   public static String getGlyphImageTag(WTObject object) {
      boolean is_shortcut = false;
      boolean is_working_copy = false;
      boolean is_checked_out = false;

      String WORKING_GLYPH_LOC = "wt/clients/images/workingcopy_glyph.gif";
      String CHECK_OUT_GLYPH_LOC = "wt/clients/images/checkout_glyph.gif";
      String SHORTCUT_GLYPH_LOC = "wt/clients/images/shortcut_glyph.gif";
      String image = "";

      try {
         WTProperties properties = WTProperties.getLocalProperties();
         WORKING_GLYPH_LOC = properties.getProperty("wt.clients.workingGlyph", WORKING_GLYPH_LOC);
         CHECK_OUT_GLYPH_LOC = properties.getProperty("wt.clients.checkOutGlyph", CHECK_OUT_GLYPH_LOC);
         SHORTCUT_GLYPH_LOC = properties.getProperty("wt.clients.shortcutGlyph", SHORTCUT_GLYPH_LOC);
      } catch (Exception e) {
         if (VERBOSE) {
            e.printStackTrace();
         }
      }

      try {
         if (object instanceof wt.folder.Shortcut) {
            image += "<IMG SRC=\"";
            image += getURLFactory().getHREF(SHORTCUT_GLYPH_LOC);
            image += "\">";
         } else if (object instanceof Workable) {
            if (WorkInProgressHelper.isWorkingCopy((Workable) object)) {
               image += "<IMG SRC=\"";
               image += getURLFactory().getHREF(WORKING_GLYPH_LOC);
               image += "\">";
            } else if (WorkInProgressHelper.isCheckedOut((Workable) object)) {
               image += "<IMG SRC=\"";
               image += getURLFactory().getHREF(CHECK_OUT_GLYPH_LOC);
               image += "\">";
            }
         }
      } catch (Exception e) {
         if (VERBOSE) {
            e.printStackTrace();
         }
      }

      return image;
   }

   /**
    *  Outputs the icon associated with the context object to the given
    *  OutputStream. This method will output the image including any glyphs that
    *  should be associated with it. DnD may be turned on/off. <BR>
    *  <BR>
    *  <B>Supported API: </B> true
    *
    *@param  parameters  the Properties passed in when this method is invoked;
    *      this parameter is not used by this method
    *@param  locale      the Locale in which the image will be displayed
    *@param  os          the OutputStream to write the image to.
    */
   public void getObjectIcon(Properties parameters, Locale locale, OutputStream os) {
      try {
         PrintWriter out = getPrintWriter(os, locale);
         String image = "";
         String bgColor = getWCColor("dndAppletDefaultBGColor");

         if ((parameters != null) &&
               ((parameters.getProperty("styleClass")) != null)) {
            String styleClass = parameters.getProperty("styleClass");
            bgColor = getWCColor(styleClass);
         }

         if ((parameters != null) &&
               ((parameters.getProperty("showShare")) != null)) {
            String shareImage = getShareImage();
            if (shareImage != null) {
               image += shareImage;
            }
         }
         if (getContextObj() instanceof WTObject) {
            image += getObjectIconImgTag((WTObject) getContextObj(), bgColor);
         } else if (getContextClassName() != null) {
            image += getObjectIconImgTag(Class.forName(getContextClassName()));
         }
         out.println(image);
         out.flush();
      } catch (Exception e) {
         if (VERBOSE) {
            System.err.println("getObjectIcon() - exception caught");
            e.printStackTrace();
         }
      }
   }

   /**
    *  Returns the Share Image <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  obj              Description of the Parameter
    *@return                  a String containing the Share Image
    *@exception  WTException  Description of the Exception
    */
   public static String getShareImage(Object obj)
          throws WTException {
      String shareResource = null;
      String shareGif = null;
      String image = "";
      StringBuffer buffer = new StringBuffer();
      if (DataSharingHelper.service.isSharedTo((Persistable) obj, null)) {
//      if (true) {
         shareResource = WTCORE_IMAGE_DIR + "/" + SHARE_ICON;
         URLFactory url_factory = null;
         try {
            url_factory = new URLFactory();
         } catch (WTException wte) {
            wte.printStackTrace();
         }
         shareGif = url_factory.getHREF(shareResource);
      }

      if (shareGif != null) {
         buffer.append("<IMG BORDER=0 SRC=\"");
         buffer.append(shareGif);
         buffer.append("\">");
         image = buffer.toString();
      }
      return buffer.toString();
   }

   /**
    *  Gets the shareImage attribute of the BasicTemplateProcessor object
    *
    *@return                  The shareImage value
    *@exception  WTException  Description of the Exception
    */
   public String getShareImage()
          throws WTException {
      return getShareImage(getContextObj());
   }

   /**
    *  Outputs the icon associated with the given class to the given OutputStream.
    *  The object class whose icon is output is given by value of the
    *  "object_class" parameter passed in the given Properties object. This method
    *  is typically invoked as a Windchill script call in an HTML template. The
    *  only script parameter currently supported is "object_class", which
    *  specifies the object class whose icon is displayed. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  parameters  the Properties passed in when this method is invoked; the
    *      Properties object should contain the object class
    *@param  locale      the Locale in which the image will be displayed
    *@param  os          the OutputStream to write the image to.
    */
   public void getClassIcon(Properties parameters, Locale locale, OutputStream os) {
      if ((parameters != null) &&
            (parameters.getProperty("object_class") != null)) {
         String obj_class = parameters.getProperty("object_class");
         String image = "";
         try {
            PrintWriter out = getLocalizedPrintWriter(os, locale);
            if (parameters.getProperty("showShare") != null) {
               String shareImage = getShareImage();
               if (shareImage != null) {
                  image += shareImage;
               }
            }
            image += getObjectIconImgTag(Class.forName(obj_class));
            out.println(image);
            out.flush();
         } catch (ClassNotFoundException cnfe) {
            if (VERBOSE) {
               System.err.println("getClassIcon() - class " + obj_class +
                     " is not a valid class: ");
               cnfe.printStackTrace();
            }
         } catch (Exception e) {
            if (VERBOSE) {
               System.err.println("getClassIcon() - exception caught");
               e.printStackTrace();
            }
         }
      }
   }

   /**
    *  Returns the String containing the HTML code to display a Hyperlink to the
    *  object properties page of the parent folder of the given FolderEntry
    *  object. If the label used as the text of the hyperlink is not given, this
    *  method will use the display identity of the parent folder. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  cabinet_based  the CabinetBased object for which the link to the
    *      properties page of the parent folder is returned
    *@param  label          the String label to use as the link text. If no label
    *      is given, the display identity of the parent folder is used.
    *@param  locale         the Locale in which the link will be displayed
    *@return                a String containing the HTML code to display a
    *      hyperlink to the object properties page of the parent folder.
    */
   protected String getParentFolderLink(CabinetBased cabinet_based, String label, Locale locale) {
      String parent_link = "";
      try {
         if (cabinet_based instanceof Cabinet) {
            if (VERBOSE) {
               System.out.println("getParentFolderLink - getting link to all cabinets");
            }

            parent_link = objectActionLinkAux(null, "Cabinets", label, locale);
         } else if (cabinet_based instanceof FolderEntry) {
            Folder parent = wt.folder.FolderHelper.getFolder((FolderEntry) cabinet_based);
            parent_link = objectActionLinkAux(parent, "ObjProps", label, locale);
         }
      } catch (WTException wte) {
         parent_link = "";
      }

      return parent_link;
   }

   /**
    *  Outputs a hyperlink to the object properties page of the parent folder of
    *  the context object. The hyperlink is output to the given OutputStream. If
    *  the current context object is not a FolderEntry object, no output is
    *  written to the stream. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  parameters  the Properties passed to the method when it is invoked
    *      via a Script call. This method does not use this parameter.
    *@param  locale      the locale in which the hyperlink is displayed
    *@param  os          the OutputStream to which the hyperlink is written
    */
   public void createParentFolderLink(Properties parameters, Locale locale, OutputStream os) {
      if (VERBOSE) {
         System.out.println("createParentFolderLink: " + getContextObj());
      }

      PrintWriter print_writer = getPrintWriter(os, locale);
      if (getContextObj() instanceof CabinetBased) {
         try {
            ResourceBundle messagesResource = ResourceBundle.getBundle(RESOURCE, locale);

            String parent_link = getParentFolderLink((CabinetBased) getContextObj(),
                  messagesResource.getString(enterpriseResource.VIEW_PARENT_FOLDER),
                  locale);
            print_writer.println(parent_link);
            print_writer.flush();
         } catch (MissingResourceException mre) {
            if (VERBOSE) {
               System.out.println("createParentFolderLink: MissingResourceException caught:");
               mre.printStackTrace();
            }
         }
      }
   }

   /**
    *  Formats the given with the given format type for the given locale. This
    *  method uses the WTStandardDateFormat to format the given date. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  format  the format type to use in formatting the given date
    *@param  time    the Date to be formatted
    *@param  locale  the Locale in which to format the date
    *@return         Description of the Return Value
    *@see            wt.util.WTStandardDateFormat
    */
   public static String formatDate(int format, Date time, Locale locale) {
      return (WTStandardDateFormat.format(time, format, locale));
   }

   /**
    *  Returns a String containing the size of the given file in Kilobytes. The
    *  string returned also contains the units (e.g "100 KB"). <BR>
    *  <BR>
    *  <B>Supported API:</B> false<BR>
    *  <BR>
    *
    *
    *@param  file  the ApplicationData item whose size is returned
    *@return       a String containing the size of the file and the units.
    */
   private String getFileSize(ApplicationData file) {
      float file_size = file.getFileSize() / (float) 1024.00;

      file_size = roundToTwoDecimals(file_size);
      String string_size = Float.toString(file_size);

      try {
         ResourceBundle messagesResource = ResourceBundle.getBundle(RESOURCE, WTContext.getContext().getLocale());
         if (messagesResource != null) {
            string_size += (" " + messagesResource.getString(enterpriseResource.KILOBYTES));
         }
      } catch (MissingResourceException mre) {
      }

      return string_size;
   }

   /**
    *  Returns the given float rounded to two decimal places. <BR>
    *  <BR>
    *  <B>Supported API:</B> false
    *
    *@param  number  the float to be rounded
    *@return         the given number rounded to two decimal places
    */
   private float roundToTwoDecimals(float number) {
      BigDecimal big = new BigDecimal((double) number)
            .setScale(2, BigDecimal.ROUND_UP);
      return big.floatValue();
   }

   /**
    *  Returns a String containing the HTML code to display a mail-to hyperlink to
    *  the e-mail of the principal referenced by the given WTPrincipalReference.
    *  The display name of the principal is used as the text of the link. Any
    *  additional code to be used in formatting the HREF can be optionally given
    *  in the given args string. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  principal  the WTPrincipalReference referencing the principal for
    *      which a hyperlink to his/her e-mail is returned.
    *@param  args       an optional string containing HTML code to be used in
    *      building the HREF link.
    *@return            the String containing the HTML code to display a hyperlink
    *      to the e-mail of the given principal.
    */
   protected String getPrincipalEmailLink(WTPrincipalReference principal, String args) {
      String mailto = "";
      if (principal != null) {
         try {
                                if (principal.isAccessible()) {
                                        String e_mail = principal.getEMail();
                                        if ((e_mail != null) &&
                                        (e_mail.length() > 0)) {
                                                mailto = HtmlUtil.createLink("mailto:" + e_mail, args,
                                                principal.getDisplayName());
                                        } else {
                                                //mailto = principal.getDisplayName();
                                                mailto = principal.getDisplayName()+"\r\n";
                                        }
                                }
         } catch (WTRuntimeException wte) {
            Throwable t = wte.getNestedThrowable();
            if (t instanceof wt.access.NotAuthorizedException) {
               mailto = "";
            } else {
               throw wte;
            }
         } catch (WTException wte) {
            mailto = "";
         }
      }
      return mailto;
   }

   /**
    *  Returns a String containing the HTML code to display a mail-to hyperlink to
    *  the e-mail of the given principal. The display name of the principal is
    *  used as the text of the link. Any additional code to be used in formatting
    *  the HREF can be optionally given in the given args string. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  principal  the WTPrincipal for which a hyperlink to his/her e-mail is
    *      returned.
    *@param  args       an optional string containing HTML code to be used in
    *      building the HREF link.
    *@return            the String containing the HTML code to display a hyperlink
    *      to the e-mail of the given principal.
    */
   protected String getPrincipalEmailLink(WTPrincipal principal, String args) {
      String mailto = "";
      if (principal != null) {
         try {
            ReferenceFactory rf = new ReferenceFactory();
            WTReference ref = rf.getReference(principal);

            if (VERBOSE) {
               System.out.println("getPrincipalEmailLink: reference is " + ref);
            }

            if (ref instanceof WTPrincipalReference) {
               mailto = getPrincipalEmailLink((WTPrincipalReference) ref, args);
            }
         } catch (WTException wte) {
            mailto = "";
         }
      }
      return mailto;
   }

   /**
    *  Outputs HTML Navigation Bar specific for object. Objects must be declared
    *  in enterpriseResource. <BR>
    *  <BR>
    *  <B>Supported API: </B> true
    *
    *@param  parameters       The arguments that were passed to the Windchill
    *      script call.
    *@param  locale           The Locale to send to the invoked methods for
    *      localization.
    *@param  os               The output stream.
    *@exception  WTException  Description of the Exception
    */
   public void createNavigationBar(Properties parameters, Locale locale,
         OutputStream os) throws WTException {
      String name_separator = WTMessage.getLocalizedMessage(URL_RESOURCE,
            UrlLinkResource.NAME_SEPARATOR,
            null, locale);

      String table_row = "";
      String table = "";
      String keyRep = "";

      keyRep = getKeyRep(locale);

      String bgcolor = getWCColor("bg-navbar");
      String linkcolor = getWCColor("f-navbar");
      String sel_linkcolor = getWCColor("f-navbar-active");
      String fontface = getWCFontFamily();
      if (parameters != null) {
         if (parameters.get("bgcolor") != null) {
            bgcolor = (String) parameters.get("bgcolor");
         } else if (parameters.get("BGCOLOR") != null) {
            bgcolor = (String) parameters.get("BGCOLOR");
         }

         if (parameters.get("linkcolor") != null) {
            linkcolor = (String) parameters.get("linkcolor");
         } else if (parameters.get("LINKCOLOR") != null) {
            linkcolor = (String) parameters.get("LINKCOLOR");
         }

         if (parameters.get("selected_linkcolor") != null) {
            sel_linkcolor = (String) parameters.get("selected_linkcolor");
         } else if (parameters.get("SELECTED_LINKCOLOR") != null) {
            sel_linkcolor = (String) parameters.get("SELECTED_LINKCOLOR");
         }
      }

      if (linkPrefix == null) {
         linkPrefix = WTMessage.getLocalizedMessage(RESOURCE, enterpriseResource.LINK_PREFIX,
               null, locale);
      }

      String current_page = "";
      if (parameters != null) {
         current_page = parameters.getProperty("currentPage");
      }
      try {
         Vector pairs = getActionPairs(keyRep, locale);

         for (int i = 0; i < pairs.size(); i++) {
            StringTokenizer tokens = new StringTokenizer((String) pairs.elementAt(i),
                  name_separator);
            String action = tokens.nextToken();
            String label = tokens.nextToken();

            if (action.equals(current_page)) {
               String style = "STYLE=\"color:" + sel_linkcolor + ";text-decoration:none; font-family:" +
                     fontface + "\"";
               String bold_style = "STYLE=\"color:" + sel_linkcolor + "; font-size:10pt; font-family:" +
                     fontface + "\"";
               table_row = HtmlUtil.addTableEntry("ALIGN=RIGHT",
                     "<B " + bold_style + ">" + linkPrefix +
                     objectActionLinkAux(getContextObj(), action, label, style, locale) + "</FONT></B>");
            } else {
               String style = "STYLE=\"color:" + linkcolor + "; font-family:" +
                     fontface + "\"";
               String bold_style = "STYLE=\"color:" + linkcolor + "; font-size:10pt; font-family:" +
                     fontface + "\"";
               table_row = HtmlUtil.addTableEntry("ALIGN=RIGHT",
                     "<B " + bold_style + ">" + linkPrefix +
                     objectActionLinkAux(getContextObj(), action, label, style, locale) + "</B>");
            }

            table += HtmlUtil.addTableRow("ALIGN=RIGHT BORDER=0 BGCOLOR=" + bgcolor, table_row);
         }

         PrintWriter out = getPrintWriter(os, locale);
         out.println(HtmlUtil.createTable("BORDER=0 CELLPADDING=2 CELLSPACING=2 ALIGN=RIGHT BGCOLOR=" + bgcolor, table));
         out.flush();
      } catch (Exception e) {
         if (VERBOSE) {
            System.out.println("createNavigationBar() - exception caught");
            e.printStackTrace();
         }
      }
   }

   /*
    *  Returns the "actions" keyRep for the context object.
    *
    *  First it trys to find the actions for the class of the object.  If
    *  it can't be found, then it checks if the object is an instance of one
    *  of the "big five" classes, since we know there are actions for those.
    *
    *  If the object is not an instance of one of the big five, then it returns
    *  null.
    *
    *  The second try (against the big five) is done to find actions for
    *  objects that are customized subclasses of the big five.
    *
    *  <BR><BR><B>Supported API:</B> false
    */

   /**
    *  Gets the keyRep attribute of the BasicTemplateProcessor object
    *
    *@param  locale           Description of the Parameter
    *@return                  The keyRep value
    *@exception  WTException  Description of the Exception
    */
   private String getKeyRep(Locale locale)
          throws WTException {
      String keyName = "";
      String keyRep = "";

      Field keyField = null;
      Class target = null;

      boolean noSuchField = false;
      boolean done = false;

      ResourceBundle resource = ResourceBundle.getBundle(URL_RESOURCE, locale);

      try {
         keyName = (((WTObject) getContextObj()).getClassInfo()).getClassname();
      } catch (WTIntrospectionException e) {
         throw new WTException(e);
      }

      while (false == done) {
         keyName = keyName.substring((keyName.lastIndexOf(".")) + 1);
         keyName = keyName.toUpperCase();
         keyName = keyName.concat("_ACTIONS");

         if (VERBOSE) {
            System.out.println("Actions: " + keyName);
         }

         try {
            target = resource.getClass();
            keyField = target.getField(keyName);
            keyRep = (keyField.get(resource)).toString();
         } catch (IllegalAccessException iae) {
            if (VERBOSE) {
               System.out.println("AAA");
            }
         } catch (NoSuchFieldException nsfe) {
            if (VERBOSE) {
               System.out.println("BBB");
            }
            noSuchField = true;
         } catch (MissingResourceException mre) {
            if (VERBOSE) {
               System.out.println("CCC");
               System.out.println("Caught missing resource exception: ");
               System.out.println("\tClass: " + mre.getClassName());
               System.out.println("\tKey:   " + mre.getKey());

               mre.printStackTrace();
            }
         } catch (Exception e) {
            if (VERBOSE) {
               System.out.println("DDD");
               System.out.println("createNavigationBar() - exception caught");
            }
         }

         if (true == noSuchField) {
            noSuchField = false;
            // We tried to get actions for a class for which there are no actions.
            // See if the class is an instance of one of the classes for which
            // we know there are actions.
            if (getContextObj() instanceof WTPart) {
               keyName = (WTPart.class).toString();
            } else if (getContextObj() instanceof wt.doc.WTDocument) {
               keyName = (wt.doc.WTDocument.class).toString();
            } else if (getContextObj() instanceof wt.part.WTPartMaster) {
               keyName = (wt.part.WTPartMaster.class).toString();
            } else if (getContextObj() instanceof wt.doc.WTDocumentMaster) {
               keyName = (wt.doc.WTDocumentMaster.class).toString();
            } else if (getContextObj() instanceof wt.csm.businessentity.BusinessEntity) {
               keyName = (wt.csm.businessentity.BusinessEntity.class).toString();
            } else {
               done = true;
            }
         } else {
            done = true;
         }
      }
      return keyRep;
   }

   /**
    *  Write a list of links to actions corresponding to the context object to the
    *  given OutputStream. This method retrieves the actions corresponding to the
    *  current context object from the UrlLinkResource ResourceBundle, and creates
    *  a table of hyperlinks to those actions. This method is typically to be
    *  invoked as a script call in an HTML template file. The Windchill script
    *  call that invokes this method can contain optional parameters to specify
    *  the background color for the table cells and the color of the links:
    *  createActionsBar bgcolor=(Background Color) linkcolor=(Link Color)
    *  selected_linkcolor=(Selected Link Color) <BR>
    *  <BR>
    *  <B>Supported API:</B> true
    *
    *@param  parameters       the Properties which contains parameter values
    *      specified in the script call. Can contain values for the link color and
    *      background color of the table cells.
    *@param  locale           the locale in which the links will be displayed
    *@param  os               the OutputStream to write the table of links to.
    *@exception  WTException  if an error occurs building the links to the actions
    */
   public void createActionsBar(Properties parameters, Locale locale, OutputStream os)
          throws WTException {
      String bgcolor = getWCColor("bg-navbar");
      String linkcolor = getWCColor("f-navbar");
      String sel_linkcolor = getWCColor("f-navbar-active");
      String fontface = getWCFontFamily();
      String current_page = "";

      if (parameters != null) {
         if (parameters.get("bgcolor") != null) {
            bgcolor = (String) parameters.get("bgcolor");
         } else if (parameters.get("BGCOLOR") != null) {
            bgcolor = (String) parameters.get("BGCOLOR");
         }

         if (parameters.get("linkcolor") != null) {
            linkcolor = (String) parameters.get("linkcolor");
         } else if (parameters.get("LINKCOLOR") != null) {
            linkcolor = (String) parameters.get("LINKCOLOR");
         }

         if (parameters.get("selected_linkcolor") != null) {
            sel_linkcolor = (String) parameters.get("selected_linkcolor");
         } else if (parameters.get("SELECTED_LINKCOLOR") != null) {
            sel_linkcolor = (String) parameters.get("SELECTED_LINKCOLOR");
         }

         current_page = parameters.getProperty("currentPage");
         if (current_page == null) {
            String delegate_class = (String) parameters.getProperty("delegate");
            if (delegate_class != null) {
               current_page = getContextServiceName(delegate_class);
            }
         }

         if (VERBOSE) {
            System.out.println("*****************************************");
            System.out.println("createActionsBar() - currentPage = " + current_page);
         }
      }

      if (linkPrefix == null) {
         linkPrefix = WTMessage.getLocalizedMessage(RESOURCE, enterpriseResource.LINK_PREFIX,
               null, locale);
      }

      String table = "";
      String table_row = "";
      String style = "STYLE=\"color:" + linkcolor + ";\"";
      String bold_style = "STYLE=\"color:" + linkcolor + "; font-size:10pt\"";

      Vector action_delegates = getURLActions(getContextObj(), locale);
      if (action_delegates != null) {
         for (int i = 0; i < action_delegates.size(); i++) {
            try {
               URLActionDelegate current_delegate = (URLActionDelegate) action_delegates.elementAt(i);
               String label = current_delegate.getURLLabel(locale);
               String url = getURLFromDelegate(current_delegate, getContextObj(), getState());
               String service = null;

               if (url != null) {
                  if (current_delegate instanceof ActionDelegate) {
                     service = ((ActionDelegate) current_delegate).getContextServiceName();
                  }

                  if (VERBOSE) {
                     System.out.println("***************************************");
                     System.out.println("---> Service name for delegate: " + service);
                  }

                  if ((service != null) &&
                        (service.length() > 0) &&
                        (service.equals(current_page))) {
                     style = "STYLE=\"color:" + sel_linkcolor + "; font-size:10pt; font-size:10pt; font-family:" + fontface + "; text-decoration:none\"";
                     bold_style = "STYLE=\"color:" + sel_linkcolor + "; font-size:10pt; font-family:" + fontface + "; text-decoration:none\"";
                  } else {
                     style = "STYLE=\"color:" + linkcolor + "; font-family:" + fontface + "\"";
                     bold_style = "STYLE=\"color:" + linkcolor + "; font-size:10pt; font-family:" + fontface + "\"";
                  }

                  table_row = HtmlUtil.addTableEntry("ALIGN=RIGHT",
                        "<B " + bold_style + ">" + linkPrefix + HtmlUtil.createLink(url, style, label) + "</B>");
                  table += HtmlUtil.addTableRow("ALIGN=RIGHT BORDER=0 BGCOLOR=" + bgcolor, table_row);
               }
            } catch (WTException wte) {
               if (VERBOSE) {
                  System.out.println("createActionsBar() - exception caught");
                  wte.printStackTrace();
               }
            } catch (Exception e) {
               if (VERBOSE) {
                  System.out.println("createActionsBar() - exception caught");
                  e.printStackTrace();
               }
            }
         }
      }
      try {
         table += HtmlUtil.createMainFuncs();
      } catch (Exception e) {
         if (VERBOSE) {
            System.out.println("createActionsBar() - exception caught");
            e.printStackTrace();
         }
      }
      PrintWriter out = getPrintWriter(os, locale);
      out.println(HtmlUtil.createTable("BORDER=0 CELLPADDING=2 CELLSPACING=2 ALIGN=RIGHT BGCOLOR=" + bgcolor, table));
      out.flush();
   }

   /**
    *  Returns a Vector containing strings
    *
    *@param  keyRep  Description of the Parameter
    *@param  locale  Description of the Parameter
    *@return         The actionPairs value
    */
   protected static Vector getActionPairs(String keyRep, Locale locale) {
      Vector pairs = new Vector();
      String pair_separator = WTMessage.getLocalizedMessage(URL_RESOURCE,
            UrlLinkResource.PAIR_SEPARATOR,
            null, locale);

      String pair_string = WTMessage.getLocalizedMessage(URL_RESOURCE,
            keyRep,
            null, locale);
      StringTokenizer tokens = new StringTokenizer(pair_string, pair_separator);
      while (tokens.hasMoreTokens()) {
         pairs.addElement(tokens.nextToken());
      }
      return pairs;
   }

   /**
    *  Outputs HTML to display all versions of a Mastered object. Also has
    *  mechanism to compare versions if more than one version is found. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  parameters       The arguments that were passed to the Windchill
    *      script call.
    *@param  locale           The Locale to send to the invoked methods for
    *      localization.
    *@param  os               The output stream.
    *@exception  WTException  Description of the Exception
    */
   public void allVersions(Properties parameters, Locale locale,
         OutputStream os) throws WTException {
      QueryResult versions =
            VersionControlHelper.service.allVersionsOf((Mastered) getContextObj());

      showVersions(versions, true, parameters, locale, os);
   }

   /**
    *  Outputs HTML to display all versions of a Mastered object. Also has
    *  mechanism to compare versions if more than one version is found. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  parameters       The arguments that were passed to the Windchill
    *      script call.
    *@param  locale           The Locale to send to the invoked methods for
    *      localization.
    *@param  os               The output stream.
    *@param  versions         Description of the Parameter
    *@param  versionTable     Description of the Parameter
    *@exception  WTException  Description of the Exception
    */
   protected void showVersions(QueryResult versions, boolean versionTable,
         Properties parameters, Locale locale, OutputStream os) throws WTException {
      PrintWriter out = getPrintWriter(os, locale);

      HashMap map = new HashMap();
      String url_form = GatewayServletHelper.buildAuthenticatedHREF(getState().getURLFactory(),
            HTML_GATEWAY,
            HTML_TEMPLATE_ACTION,
            null, map);
      out.println("<FORM ACTION=\"" + HTMLEncoder.encodeForHTMLContent(url_form) + "\" METHOD=\"GET\">");

      boolean showCheckCol = true;
      if (versions.size() == 1) {
         showCheckCol = false;
      }

      String bgcolor = getWCColor("t1-bg-evenrow");
      String headercolor = getWCColor("t1-bg-col-head");
      String headerFontColor = getWCColor("t1-f-col-head");

      if (parameters != null) {
         if (parameters.get("bgcolor") != null) {
            bgcolor = (String) parameters.get("bgcolor");
         } else if (parameters.get("BGCOLOR") != null) {
            bgcolor = (String) parameters.get("BGCOLOR");
         }

         if (parameters.get("headercolor") != null) {
            headercolor = (String) parameters.get("headercolor");
         } else if (parameters.get("HEADERCOLOR") != null) {
            headercolor = (String) parameters.get("HEADERCOLOR");
         }
      }

      ResourceBundle resource = ResourceBundle.getBundle(RESOURCE, locale);

      out.println("<TABLE ALIGN=CENTER BORDER=0 CELLSPACING=3 CELLPADDING=3 WIDTH=90%>");
      out.println("<THEAD ALIGN=\"left\">");
      out.println("<TR>");
      int col_count = 0;
      if (showCheckCol == true) {
         out.println("<TH BGCOLOR=" + headercolor + " WIDTH=3%></TH>");
         col_count++;
      }

      out.println("<TH BGCOLOR=" + headercolor+ " ALIGN=LEFT COLSPAN=2><FONT COLOR=\"" +
            headerFontColor + "\"><B>");
      if (versionTable) {
         out.println(getLocalizedString(resource, enterpriseResource.VERSION, "Version"));
      } else {
         out.println(getLocalizedString(resource, enterpriseResource.ITERATION, "Iteration"));
      }
      col_count += 2;
      out.println("</B></FONT></TH>");

      if (versionTable) {
         out.println("<TH BGCOLOR=" + headercolor + " ALIGN=LEFT><FONT COLOR=\"" +
               headerFontColor + "\"><B>");
         out.println(getLocalizedString(resource, enterpriseResource.STATE, "State"));
         out.println("</B></FONT></TH>");

         out.println("<TH BGCOLOR=" + headercolor + " ALIGN=LEFT><FONT COLOR=\"" +
               headerFontColor + "\"><B>");
         out.println(getLocalizedString(resource, enterpriseResource.TEAM, "Team"));
         out.println("</B></FONT></TH>");
         col_count += 2;
      }

      out.println("<TH BGCOLOR=" + headercolor + " ALIGN=LEFT><FONT COLOR=\"" +
            headerFontColor + "\"><B>");
      out.println(getLocalizedString(resource, enterpriseResource.LAST_MOD_DATE, "Last Modified Date"));
      out.println("</B></FONT></TH>");

      out.println("<TH BGCOLOR=" + headercolor + " ALIGN=LEFT><FONT COLOR=\"" +
            headerFontColor + "\"><B>");
      if (versionTable) {
          out.println(getLocalizedString(resource, enterpriseResource.CREATED_BY, "Created By"));
      } else {
          out.println(getLocalizedString(resource, enterpriseResource.UPDATED_BY, "Updated By"));
      }
      out.println("</B></FONT></TH>");

      out.println("<TH BGCOLOR=" + headercolor + " ALIGN=LEFT><FONT COLOR=\"" +
            headerFontColor + "\"><B>");
      out.println(getLocalizedString(resource, enterpriseResource.CREATION_DATE, "Creation Date"));
      out.println("</B></FONT></TH>");

      col_count += 3;
      out.println("</TR>");
      out.println("</THEAD>");
      out.println("<TBODY>");

      int i = 0;
      while (versions.hasMoreElements()) {
         if (VERBOSE) {
            System.out.println("Version " + i + 1);
         }
         i++;
         String count = Integer.toString(i);

         Versioned rc = (Versioned) versions.nextElement();
         //String oid = rc.getPersistInfo().getObjectIdentifier().getStringValue();
         ReferenceFactory rf = new ReferenceFactory();
         WTReference ref = rf.getReference((Persistable) rc);
         String oid = rf.getReferenceString(ref);

         String versionIconStr = "";
         try {
            versionIconStr = getObjectIconImgTag((WTObject) rc, bgcolor);
         } catch (Exception e) {
            versionIconStr = "";
         }
         String versionPropLink;
         String projectVersionIcon = "";
         //if it isn't a version in a project this will remain an empty string

         if (versionTable) {
            String value = "";
               value += VersionControlHelper.getVersionIdentifier((Versioned) rc).getValue();
            if (rc instanceof OneOffVersioned && VersionControlHelper.isAOneOff((OneOffVersioned) rc)) {
               value += "-" + ((OneOffVersioned) rc).getOneOffVersionIdentifier().getValue();
            }

            //If the object is checked out to a project, get the extra icon and the link to the project details page
            if ((rc instanceof WTContained) && (((WTContained) rc).getContainer() instanceof Project2)) {
                                    /*
               String iconurl = getState().getURLFactory().getHREF("wtcore/images/com/ptc/core/foundation/project_avail.gif");
               projectVersionIcon = "<IMG BORDER=0 SRC=\"" + iconurl + "\">";
                                     **/
               URLActionDelegateFactory factory = new URLActionDelegateFactory();
               URLActionDelegate delegate = factory.getDelegate("PROJECTLINKOBJECTDETAILS");
               try {
                  String url = getURLFromDelegate(delegate, rc, getState());
                  versionPropLink = "<A HREF=\"" + url + "\"> " + value;
                  //if a problem occurs getting the projectlink url
                  //recover by using just the name, and then add t problem to the responseExceptions
               } catch (ClassNotFoundException e) {
                  versionPropLink = value;

                  if (VERBOSE) {
                     e.printStackTrace();
                  }
               } catch (NoSuchMethodException nsme) {
                  versionPropLink = value;

                  if (VERBOSE) {
                     nsme.printStackTrace();
                  }
               } catch (InvocationTargetException ite) {
                  versionPropLink = value;

                  if (VERBOSE) {
                     ite.printStackTrace();
                  }
               } catch (IllegalAccessException iac) {
                  versionPropLink = value;

                  if (VERBOSE) {
                     iac.printStackTrace();
                  }
               }
            } else {
               //otherwise just get the windchill pdm details page link
               ObjectReference rc_ref = ObjectReference.newObjectReference(rc);
               versionPropLink =
                     objectActionLinkAux(rc_ref, "ObjProps", value, null, null, locale);
            }
         } else {
            // Note that the value string is equivalent to the one in ObjectPropertyValue...
            String value = "";
            if (rc instanceof Versioned) {
               value += VersionControlHelper.getVersionIdentifier((Versioned) rc).getValue();
            }
            if (rc instanceof OneOffVersioned && VersionControlHelper.isAOneOff((OneOffVersioned) rc)) {
               value += "-" + ((OneOffVersioned) rc).getOneOffVersionIdentifier().getValue();
            }
            if (value != null) {
               value += HarvardSeries.getDelimiter();
            }
            value += VersionControlHelper.getIterationIdentifier((Iterated) rc).getValue();
            if (rc instanceof ViewManageable && ViewHelper.getView((ViewManageable) rc) != null) {
               value += " (" + ViewHelper.getView((ViewManageable) rc).getName() + ")";
            }
            // Note that the links in the IterationHistory table have object reference rather than version reference.
            ObjectReference rc_ref = ObjectReference.newObjectReference(rc);
            versionPropLink =
                  objectActionLinkAux(rc_ref, "ObjProps", value, null, null, locale);
         }

         String state = null;
         String teamTemplate = null;
         State stateObj = null;
         if (versionTable) {
            if (rc instanceof LifeCycleManaged) {
               stateObj = ((LifeCycleManaged) rc).getLifeCycleState();
               teamTemplate = ((LifeCycleManaged) rc).getTeamTemplateName();
            }
            if (rc instanceof RevisionControlled) {
               stateObj = ((RevisionControlled) rc).getLifeCycleState();
               teamTemplate = ((RevisionControlled) rc).getTeamTemplateName();
            }

            state = "---";
            if (stateObj != null) {
               state = stateObj.getDisplay(locale);
            }

            if ((teamTemplate == null) || (teamTemplate.length() == 0)) {
               teamTemplate = "&nbsp";
            }
         }

         String modifyDate = formatDate(WTStandardDateFormat.LONG_STANDARD_DATE_FORMAT,
               PersistenceHelper.getModifyStamp(rc), locale);

         String createDate = formatDate(WTStandardDateFormat.LONG_STANDARD_DATE_FORMAT,
               VersionTaskLogic.getCreationDate(rc), locale);

         WTPrincipal principal = null;
         WTPrincipalReference principal_ref = null;
         if (!versionTable) {
             principal_ref = VersionControlHelper.getIterationModifier(rc);
         }
         else if ((rc instanceof Workable) && WorkInProgressHelper.isWorkingCopy((Workable) rc)) {
             principal_ref = VersionControlHelper.getIterationModifier(rc);
         }
         else {
             principal_ref = VersionControlHelper.getVersionCreator(rc);
         }

         String creator = "";
         if (principal_ref != null) {
            //principal = principal_ref.getPrincipal();
            creator = getPrincipalEmailLink(principal_ref, null);
            //principal.getName();
         }

         int rowspan = versionTable ? 2 : 3;

         out.println("<TR BGCOLOR=" + bgcolor + ">");
         if (showCheckCol == true) {
            out.println("<TD WIDTH=3% VALIGN=TOP ROWSPAN=" + rowspan + ">" +
                  "<INPUT TYPE=\"checkbox\" NAME = \"oid" + count +
                  "\" VALUE=\"" + oid + "\"> </INPUT> </TD>");
         }
//       out.println("<TD WIDTH=3% NOWRAP>" + projectVersionIcon + " " + versionIconStr + "</TD>");
         out.println("<TD WIDTH=3% NOWRAP>"  + " " + versionIconStr + "</TD>");
         out.println("<TD class=\"text\">" + versionPropLink + "</A></TD>");
         if (versionTable) {
            out.println("<TD>" + state + "</TD>");
            out.println("<TD>" + teamTemplate + "</TD>");
         }
         out.println("<TD NOWRAP>" + modifyDate + "</TD>");
         out.println("<TD NOWRAP>" + creator + "</TD>");
         out.println("<TD NOWRAP>" + createDate + "</TD>");
         out.println("</TR>");

         int colspan = showCheckCol ? (col_count - 1) : col_count;

         // Print comments line
         if (!versionTable) {
            try {
               String note = VersionControlHelper.getNote(rc);
               note = (note == null) ? BLANK_SPACE : note;

               out.println("<TR>");
               out.println("<TD ALIGN=LEFT VALIGN=MIDDLE BGCOLOR=" + bgcolor + " COLSPAN=" + colspan + ">");
               out.println("<FONT SIZE=-1><B>" +
                     getLocalizedString(resource, enterpriseResource.COMMENT_LABEL, "Comment:") +
                     "</B>&nbsp;" +
                     note
                      + "</FONT><BR><BR>");

               out.println("</TD>");
               out.println("</TR>");
            } catch (VersionControlException vce) {
               if (VERBOSE) {
                  System.err.println("Exception getting Iteration Note: ");
                  vce.printStackTrace(System.err);
               }
            }
         }
      }

      out.println("</TBODY>");
      out.println("</TABLE>");
      out.println("<P>");

      if (i > 1) {
         String compare_lbl = resource.getString(enterpriseResource.COMPARE_BUTTON_LABEL);
         out.println("<INPUT TYPE=\"submit\" NAME=\"submit\" VALUE=\"" + compare_lbl + "\"" +
               "id=button>");
         out.println("</INPUT>");
         out.println("<INPUT TYPE=\"hidden\" NAME=\"action\" VALUE=\"CompareObjs\"></INPUT>");
         out.println("</P>");
      }
      out.println("</FORM>");

      out.flush();
   }

   /**
    *  Method to localize generic strings. There must be a key in the Resource
    *  Bundle otherwise the backup parameter will be returned. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  resource  The resource bundle for localization.
    *@param  key       The key in the resource bundle.
    *@param  backup    The string that will be used if key is not found.
    *@return           The localizedString value
    */
   public String getLocalizedString(ResourceBundle resource, String key, String backup) {
      String label;
      try {
         label = resource.getString(key);
         return label;
      } catch (MissingResourceException mre) {
         mre.printStackTrace();
         return backup;
      } finally {
      }
   }

   /**
    *  <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  attribute    Description of the Parameter
    *@param  value        Description of the Parameter
    *@param  label        Description of the Parameter
    *@param  search_objs  Description of the Parameter
    *@param  locale       Description of the Parameter
    *@return              The searchResultsURL value
    */
   public String getSearchResultsURL(String attribute, String value, String label,
         String search_objs, Locale locale) {
      String link = "";
      Properties params = new Properties();

      if (value == null) {
         value = BLANK_SPACE;
      }

      params.put(attribute, value);
      params.put("pick", search_objs);
      try {
         link = objectActionLinkAux(null, "DoSearch", label, params, locale);
      } catch (WTException wte) {
         link = label;
      }
      return link;
   }

   /**
    *  This method has been extended at Release 5.0. The original functionality
    *  remains and is unchanged. There are now more options available to use the
    *  <I>include</I> for a file into an HTML template. <P>
    *
    *  The additional functionality allows the inclusion of a file with a file
    *  extension of *.htm. *.html, or *.js. The included file is <B><I>NOT</I>
    *  </B> allowed to have Windchill script calls. If you would like to include a
    *  sub-Template with script calls, you should use the processSubTemplate
    *  script call. <P>
    *
    *  There are three ways to use this script call
    *  <UL>
    *    <LI> Insert an ObjectPropery template into the page (Original
    *    functionality, script calls ARE processed).
    *    <LI> Insert plain text HTML or JavaScript into a template by specifying a
    *    relative path from codebase(Script calls ARE <B><I>NOT</I> </B>
    *    processed)
    *    <LI> Insert plain text HTML into a template by specifying an entry in a
    *    properties file(Script calls ARE <B><I>NOT</I> </B> processed)
    *  </UL>
    *  <P>
    *
    *  <H2>Insert an ObjectPropery template into the page</H2> To have a
    *  ObjectProperty page or ObjectPropertyVerbose page inserted into your
    *  current page, use the following syntax <P>
    *
    *  include action=ObjProps <BR>
    *  or <BR>
    *  include action=ObjProps template=Simple <P>
    *
    *  If the template is not specified, the template will be determined using the
    *  current context object. <BR>
    *  The template can be any of the current ObjectProperties templates. <BR>
    *  This method is currently unsupported for customization. It is subject to
    *  change or removal without notice. <H2>Insert plain text HTML or JavaScript
    *  into a template by specifying a relative path from codebase</H2> You are
    *  able to insert a file into your current template by specifying the relative
    *  path from codebase. The file you reference <B><I>must have</I> </B> one of
    *  the following extensions : <B><I>htm, html, or js</I> </B> . The file you
    *  include <B><I>cannot have</I> </B> any Windchill Script calls. If you would
    *  like include an HTML template with script calls, you should call the
    *  processSubTemplate Windchill script call. <P>
    *
    *  The syntax for this usage is <P>
    *
    *  <I>include fileName=&lt;Relative file path from codebase, no file
    *  extension&gt;</I> <P>
    *
    *  An example of this would be if you would like to include copyright
    *  information and that file was located in
    *  /codebase/templates/util/copyright.html. You would the call <P>
    *
    *  <I>include fileName=templates/util/copyright</I> <H2>Insert plain text HTML
    *  into a template by specifying an entry in a properties file</H2> You are
    *  able to insert a file into your current template by specifying the context
    *  with which to find the file using the ApplicationContextServices. The file
    *  you reference must have one of the following extensions : htm, html, or js.
    *  The file you include cannot have any Windchill Script calls. If you would
    *  like include an HTML template with script calls, you should call the
    *  processSubTemplate Windchill script call. <P>
    *
    *  <H3>There are two difference calls this usage.</H3> <P>
    *
    *  <H4>The first version of the syntax is</H4> <P>
    *
    *  <I>include selectorString=&lt;String key to use in a properties file
    *  entry&gt;</I> <P>
    *
    *  This usage will find the following entry in a properties file <P>
    *
    *  <I><NOBR>wt.services/rsc/default/wt.templateutil.DefaultHTMLTemplate/&lt;String
    *  key to use in a properties file entry&gt;/&lt;Current Context
    *  Class&gt;/0=&lt;Relative path to file from codebase&gt;</NOBR> </I> <P>
    *
    *  An example of a call on a page displaying a <I>wt.part.WTPart</I> and the
    *  the corresponding entry in a property file is <P>
    *
    *  <I>include selectorString=<B>TestInclude</B> </I> <P>
    *
    *  <I><NOBR>wt.services/rsc/default/wt.templateutil.DefaultHTMLTemplate/<B>
    *  TestInclude</B> /wt.part.WTPart/0=templates.ObjectProperties.IncludeUsesTable
    *  </NOBR></I> <P>
    *
    *  <H4>The second version of the syntax is</H4> <P>
    *
    *  <I><NOBR>include selectorString=&lt;String key to use in a properties file
    *  entry&gt; requestorClass=&lt;Context Class to reference in properties
    *  file&gt;</NOBR> </I> <P>
    *
    *  This usage will find the following entry in a properties file <P>
    *
    *  <I><NOBR>wt.services/rsc/default/wt.templateutil.DefaultHTMLTemplate/&lt;String
    *  key to use in a properties file entry&gt;/&lt;Context Class to reference in
    *  properties file&gt;/0=&lt;Relative path to file from codebase&gt;</NOBR>
    *  </I> <P>
    *
    *  An example of a call and the the corresponding entry in a property file is
    *  <P>
    *
    *  <I>include selectorString=TestInclude requestorClass=java.lang.Object</I>
    *  <P>
    *
    *  <I><NOBR>wt.services/rsc/default/wt.templateutil.DefaultHTMLTemplate/TestInclude/java.lang.Object/0=templates.ObjectProperties.IncludeUsesTable
    *  </NOBR></I> <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  parameters  Description of the Parameter
    *@param  locale      Description of the Parameter
    *@param  os          Description of the Parameter
    */
   public void include(Properties parameters, Locale locale,
         OutputStream os) {
      if (VERBOSE) {
         System.out.println("Entering include()...");
         for (Enumeration enumeration = parameters.keys(); enumeration.hasMoreElements(); ) {
            Object key = enumeration.nextElement();
            System.out.println("include : key = " + key + " - value = " + parameters.get(key));
         }
      }
      String action = parameters.getProperty("action");

      // Check to see if the call is a call to insert an ObjectPropertiesVerbose
      // in an Object Properties page
      if ("ObjProps".equalsIgnoreCase(action)) {
         try {
            URLProcessor.handleIncludeRequest(parameters, locale,
                  os, getContextObj());
         } catch (wt.util.WTException e) {
            // This string should not be localized, it is the name of the method.
            handleExceptionTP("include", e, true,
                  parameters, locale,
                  os);
         }
         return;
      }
      // Otherwise, we will see if the call is for the typical "include"
      else {
         // Check and see if a relative file path is passed in
         String fileName = parameters.getProperty("fileName");
         if (fileName != null && fileName.length() > 0) {
            // Set the action key for processSubTemplate. It is required.
            parameters.put("action", "UseEntries");
            // Copy the filename into the correct key for processSubTemplate.
            parameters.put(SubTemplateService.TEMPLATE, fileName);
            // Set the Processor to be used by processSubTemplate.
            parameters.put(SubTemplateService.PROCESSOR, "wt.templateutil.processor.DefaultTemplateProcessor");

            // Call ProcessSubTemplate to process the template using DefaultTemplateProcessor
            processSubTemplate(parameters, locale, os);

            return;
         }

         // Check and see if the ApplicationContextServices registry option is being used.
         String selectorString = parameters.getProperty("selectorString");
         String requestorClass = parameters.getProperty("requestorClass");
         if (selectorString != null && selectorString.length() > 0) {
            // Copy the selectorString into the action key for processSubTemplate.
            parameters.put("action", selectorString);

            String currentContextClassName = getContextClassName();
            ;
            Object currentContextObj = getContextObj();
            if (requestorClass != null && !requestorClass.equals("")) {
               setContextClassName(requestorClass);
               setContextObj(null);
            }

            // Set the Processor to be used by processSubTemplate.
            parameters.put(SubTemplateService.PROCESSOR, "wt.templateutil.processor.DefaultTemplateProcessor");

            // Call ProcessSubTemplate to process the template using DefaultTemplateProcessor
            processSubTemplate(parameters, locale, os);

            setContextClassName(currentContextClassName);
            setContextObj(currentContextObj);

            return;
         }

         // If none of the above supported options are called, throw an exception
         // Here, a runtime exception is thrown as this is an existing call from
         // from previous releases and I do not want to affect any existing references.
         throw new wt.util.WTRuntimeException("include call failed...");
      }
   }

   /**
    *  HandleProcessorException() method can be called in the following scenario:
    *  <p>
    *
    *  Use case: An exception is thrown back to a method that corresponds to a
    *  "Windchill script language" call (a call that actually appears in an HTML
    *  template file, for example: objectPropertyValue or objectActionLink), and
    *  processing of the page should continue even though this call has failed.
    *  <p>
    *
    *  This method will handle the exception by embedding a message in the
    *  generated web page that states: <p>
    *
    *  "ERROR: During the dynamic generation of this web page, while processing
    *  the Windchill script language method "methodName". <BR>
    *  The error message is: "e.getLocalizedMessage()" <BR>
    *  A stack trace has been printed to the log. <BR>
    *  Generation of this web page will now continue." <p>
    *
    *  If printStackTrace is false, then the line about the "stack trace" will not
    *  be generated in the web page. <p>
    *
    *  <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  methodName       The name of the Windchill script language method
    *      that has failed.
    *@param  e                The exception that was generated by the method.
    *@param  printStackTrace  A boolean that specifies if a stack trace should be
    *      printed to the log.
    *@param  parameters       The arguments that were passed to the Windchill
    *      script call.
    *@param  locale           The Locale to send to the invoked methods for
    *      localization.
    *@param  os               The output stream.
    */
   public static void handleProcessorException(String methodName, Exception e,
         boolean printStackTrace,
         Properties parameters, Locale locale,
         OutputStream os) {
      PrintWriter out = getLocalizedPrintWriter(os, locale);

      out.println("<BR>");

      out.println("<B>");
      out.println(WTMessage.getLocalizedMessage(RESOURCE,
            enterpriseResource.ERROR,
            null, locale) +
            ": ");

      out.println("</B>");
      Object[] param = {methodName};
      out.println(WTMessage.getLocalizedMessage(RESOURCE,
            enterpriseResource.DURING_PAGE_GENERATION,
            param, locale));

      out.println("<BR>");
      out.println(WTMessage.getLocalizedMessage(RESOURCE,
            enterpriseResource.ERROR_MESSAGE_IS,
            null, locale));

      out.println("<BR>");
      if (e instanceof WTException) {
         out.println(BLANK_SPACE + BLANK_SPACE + BLANK_SPACE + BLANK_SPACE +
               ((WTException) e).getLocalizedMessage(locale));
      } else {
         out.println(BLANK_SPACE + BLANK_SPACE + BLANK_SPACE + BLANK_SPACE +
               e.getLocalizedMessage());
      }

      out.println("<BR>");

      if (printStackTrace && !ignoreException(e)) {
         e.printStackTrace();
         out.println(WTMessage.getLocalizedMessage(RESOURCE,
               enterpriseResource.STACK_TRACE_PRINTED,
               null, locale));
         out.println("<BR>");
      }

      out.println(WTMessage.getLocalizedMessage(RESOURCE,
            enterpriseResource.PAGE_GENERATION_CONTINUES,
            null, locale));
      out.println("<BR>");
      out.println("<BR>");
      out.flush();
   }

   /**
    *  Description of the Method
    *
    *@param  t  Description of the Parameter
    *@return    Description of the Return Value
    */
   private static boolean ignoreException(Throwable t) {
      // Ignore exceptions produced by HTTP client disconnects
      while (t instanceof WTRuntimeException) {
         Throwable nested = ((WTRuntimeException) t).getNestedThrowable();
         if (nested != null) {
            t = nested;
         } else {
            break;
         }
      }
      while (t instanceof WTException) {
         Throwable nested = ((WTException) t).getNestedThrowable();
         if (nested != null) {
            t = nested;
         } else {
            break;
         }
      }
      boolean ignore =
            t instanceof wt.util.WrappedSocketException ||
            t instanceof wt.pds.DBOperationInterruptedException ||
            t instanceof InterruptedException;

      return ignore;
   }

   /**
    *  This flavor of the handleExceptionTP() method can be called in the
    *  following scenario: <p>
    *
    *  Use case: An exception is thrown back to a method that corresponds to a
    *  "Windchill script language" call (a call that actually appears in an HTML
    *  template file, for example: objectPropertyValue or objectActionLink), and
    *  processing of the page should continue even though this call has failed.
    *  <p>
    *
    *  This method will handle the exception by embedding a message in the
    *  generated web page that states: <p>
    *
    *  "ERROR: During the dynamic generation of this web page, while processing
    *  the Windchill script language method "methodName". <BR>
    *  The error message is: "e.getLocalizedMessage()" <BR>
    *  A stack trace has been printed to the log. <BR>
    *  Generation of this web page will now continue." <p>
    *
    *  If printStackTrace is false, then the line about the "stack trace" will not
    *  be generated in the web page. <p>
    *
    *  <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  methodName       The name of the Windchill script language method
    *      that has failed.
    *@param  e                The exception that was generated by the method.
    *@param  printStackTrace  A boolean that specifies if a stack trace should be
    *      printed to the log.
    *@param  parameters       The arguments that were passed to the Windchill
    *      script call.
    *@param  locale           The Locale to send to the invoked methods for
    *      localization.
    *@param  os               The output stream.
    */
   public void handleExceptionTP(String methodName, Exception e,
         boolean printStackTrace,
         Properties parameters, Locale locale,
         OutputStream os) {
      handleProcessorException(methodName, e, printStackTrace, parameters, locale, os);
   }

   /**
    *  This flavor of the handleExceptionTP() method can be called in the
    *  following scenario: <p>
    *
    *  Use case: Many template processors have a handleRequest() method that
    *  instantiates an HTMLTemplate, and then calls the process() method on that
    *  instance. If the process() method throws and exception, then the processing
    *  by this template processor has stopped. After catching the exception thrown
    *  by the process() method, call this flavor of handleExceptionTP(). <p>
    *
    *  This method will handle the exception by appending a message to the
    *  generated web page that states: <p>
    *
    *  ERROR: During the dynamic generation of this web page. <text> <BR>
    *  The error message is: "e.getLocalizedMessage()" <BR>
    *  A stack trace has been printed to the log. <BR>
    *  Generation of this web page has halted. <p>
    *
    *  If printStackTrace is false, then the line about the "stack trace" will not
    *  be generated in the web page. <p>
    *
    *  <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  title            A title for the web page. If the page already has a
    *      title, this one will be ignored.
    *@param  text             An informative message to be presented to the user.
    *@param  e                The exception that was generated.
    *@param  printStackTrace  A boolean that specifies if a stack trace should be
    *      printed to the log.
    *@param  resp             The HTTPResponse object, from which the output
    *      stream is retrieved.
    *@param  locale           The Locale for which to localize the messages.
    *@exception  WTException  Description of the Exception
    */
   public void handleExceptionTP(String title, String text, Exception e,
         boolean printStackTrace,
         HTTPResponse resp, Locale locale)
          throws WTException {
      if (VERBOSE) {
         System.out.println("Entering handleExceptionTP()...");
      }

      OutputStream os = null;
      try {
         os = resp.getOutputStream();
      } catch (java.io.IOException ioe) {
         throw new WTException(ioe);
      }
      PrintWriter out = getPrintWriter(os, locale);

      if (VERBOSE) {
         System.out.println("  about to create a web page...");
      }

      out.flush();

      out.println(HtmlUtil.beginHtml(title, "wt/clients/images/bktile.gif"));
      out.println(HtmlUtil.createBase());
      out.println("<BR>");

      out.println("<B>");
      out.println(WTMessage.getLocalizedMessage(RESOURCE,
            enterpriseResource.ERROR,
            null, locale) +
            ": ");
      out.println("</B>");

      out.println(WTMessage.getLocalizedMessage(RESOURCE,
            enterpriseResource.DURING_PAGE_GENERATION_2,
            null, locale));
      out.println("<BR>");
      out.println(text);

      out.println("<BR>");
      out.println("<BR>");
      out.println(WTMessage.getLocalizedMessage(RESOURCE,
            enterpriseResource.ERROR_MESSAGE_IS,
            null, locale));

      out.println("<BR>");
      if (e instanceof WTException) {
         out.println(BLANK_SPACE + BLANK_SPACE + BLANK_SPACE + BLANK_SPACE +
               ((WTException) e).getLocalizedMessage(locale));
      } else {
         out.println(BLANK_SPACE + BLANK_SPACE + BLANK_SPACE + BLANK_SPACE +
               e.getLocalizedMessage());
      }

      out.println("<BR>");

      if (printStackTrace && !ignoreException(e)) {
         e.printStackTrace();

         out.println("<BR>");
         out.println(WTMessage.getLocalizedMessage(RESOURCE,
               enterpriseResource.STACK_TRACE_PRINTED,
               null, locale));
         out.println("<BR>");
      }

      out.println("<BR>");
      out.println(WTMessage.getLocalizedMessage(RESOURCE,
            enterpriseResource.PAGE_GENERATION_HALTED,
            null, locale));
      out.println("<BR>");
      out.println("<BR>");

      out.flush();
      if (VERBOSE) {
         System.out.println("Exiting handleHTMLError()");
      }
   }

   /**
    *  Gets the query specified by the value of the parameter whose key is
    *  'QueryName'. As a short-term solution, this method is forwarded to the
    *  StdTemplateProcessor which is maintained as an instance variable. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  parameters  Description of the Parameter
    *@param  locale      Description of the Parameter
    *@param  os          Description of the Parameter
    */
   public void getQuery(Properties parameters, Locale locale, OutputStream os) {
      if (queryService == null) {
         queryService = new StdTemplateProcessor();
         queryService.setContextObj(getContextObj());
      }

      try {
         queryService.getQuery(parameters, locale, os);
      } catch (WTException wte) {
         wte.printStackTrace();
      }
   }

   /**
    *  <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  parameters       The new columnProperties value
    *@param  locale           The new columnProperties value
    *@param  os               The new columnProperties value
    *@exception  WTException  Description of the Exception
    */
   public void setColumnProperties(Properties parameters, Locale locale, OutputStream os)
          throws WTException {
      if (queryService == null) {
         queryService = new StdTemplateProcessor();
         queryService.setContextObj(getContextObj());
      }

      try {
         queryService.setColumnProperties(parameters, locale, os);
      } catch (WTException wte) {
         wte.printStackTrace();
      }
   }

   /**
    *  <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  parameters       Description of the Parameter
    *@param  locale           Description of the Parameter
    *@param  os               Description of the Parameter
    *@exception  WTException  Description of the Exception
    */
   public void printTable(Properties parameters, Locale locale, OutputStream os)
          throws WTException {
      parameters.remove("ACTION");
      parameters.remove("action");
      parameters.put("action", "show");
      if (VERBOSE) {
         System.err.println("\n BasicTemplateProcessor - printTable : Before call to show");
      }
      tableService(parameters, locale, os);
      if (VERBOSE) {
         System.err.println("\n BasicTemplateProcessor - printTable : Before call to show");
      }
   }

   /**
    *  Return a String representation of a URL to the user's personal cabinet. If
    *  a value is not passed in for the label, use the principal's name. If a
    *  principal is not passed in, use the current principal. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  principal  the Principal whose personal cabinet will be the target of
    *      the link. If null, the current principal is used
    *@param  label      the String label to be used as the target of the link
    *@param  params     Description of the Parameter
    *@param  locale     Description of the Parameter
    *@return            a String representation of the URL to the personal cabinet
    */
   public static String getPersonalCabinetURL(WTPrincipal principal, String label,
         String params, Locale locale) {
      String url = label;

      try {
         if (principal == null) {
            principal = SessionHelper.manager.getPrincipal();
         }

         if (principal != null) {
            Cabinet cabinet = FolderHelper.service.getPersonalCabinet(principal);
            if ((label == null) ||
                  (label.length() == 0)) {
               label =
                     WTMessage.getLocalizedMessage(RESOURCE, enterpriseResource.PERSONAL_CABINET_LABEL,
                     null, locale);
            }

            url = objectActionLinkAux(cabinet, "ObjProps", label, params, locale);
         }
      } catch (WTException wte) {
         url = label;
      }

      return url;
   }

   /**
    *  Returns a String representation of a URL to the current principal's
    *  worklist. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  label            the label to use as the link text
    *@param  href_properties  any additional HTML code to be placed in the A HREF
    *      tags
    *@param  locale           Description of the Parameter
    *@return                  a string representation of the link to the user's
    *      worklist
    */
   public static String getWorklistURL(String label, String href_properties, Locale locale) {
      String worklist_url = label;

      if ((label == null) ||
            (label.length() == 0)) {
         label = WTMessage.getLocalizedMessage(RESOURCE, enterpriseResource.WORKLIST_LABEL, null, locale);
      }

      try {
         Properties parameters = new Properties();
         parameters.put("template", "workflow/WfWorkList");
         worklist_url = objectActionLinkAux(null, "WfWorkList", label, parameters, href_properties, locale);
      } catch (WTException wte) {
         worklist_url = label;
      }

      return worklist_url;
   }

   /**
    *  Returns a String representation of a URL to the Windchill homepage <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  label            the label to use as the link text
    *@param  href_properties  any additional HTML code to be placed in the A HREF
    *      tags
    *@param  locale           Description of the Parameter
    *@return                  a string representation of the link to the Windchill
    *      homepage
    */
   public static String getHomepageURL(String label, String href_properties, Locale locale) {
      String url = label;
      if ((label == null) ||
            (label.length() == 0)) {
         label = WTMessage.getLocalizedMessage(RESOURCE, enterpriseResource.HOMEPAGE_URL_LABEL, null, locale);
      }

      try {
         String windchillHome = GatewayServletHelper.buildAuthenticatedHREF(getURLFactory());
         url = HtmlUtil.createLink(windchillHome, href_properties, label);
      } catch (WTException wte) {
         url = label;
      }

      return url;
   }

   /**
    *  Returns a string representation of a URL to the FormProcessor. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  label       the label to use as the text of the link
    *@param  parameters  any additional HTML to include in the HREF tag
    *@param  method      the method to invoke in FormProcessor
    *@param  properties  Properties passed on the URL of the FormProcessor
    *@return             a String representation of the FormProcessor URL
    */
   public static String buildFormProcessorURL(String label, String parameters,
         String method, Properties properties) {
      return buildFormProcessorURL("wt.enterprise.FormProcessor", label, parameters, method, properties);
   }

   /**
    *  Description of the Method
    *
    *@param  class_string  Description of the Parameter
    *@param  label         Description of the Parameter
    *@param  parameters    Description of the Parameter
    *@param  method        Description of the Parameter
    *@param  properties    Description of the Parameter
    *@return               Description of the Return Value
    */
   public static String buildFormProcessorURL(String class_string, String label, String parameters,
         String method, Properties properties) {
      String url = label;
      if (properties == null) {
         properties = new Properties();
      }
      HashMap map = new HashMap(properties);
      try {
         url = GatewayServletHelper.buildAuthenticatedHREF(getURLFactory(),
               HTML_GATEWAY,
               HTML_TEMPLATE_ACTION,
               null, map);
      } catch (WTException e) {
         e.printStackTrace();
      }
      url = HtmlUtil.createLink(url, parameters, label);
      return url;
   }

   /**
    *  <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  label        Description of the Parameter
    *@param  search_type  Description of the Parameter
    *@param  parameters   Description of the Parameter
    *@param  href_props   Description of the Parameter
    *@param  locale       Description of the Parameter
    *@return              The searchCriteriaURL value
    */
   public static String getSearchCriteriaURL(String label, String search_type,
         Properties parameters, String href_props, Locale locale) {
      String url = label;
      if ((label == null) ||
            (label.length() == 0)) {
         label = WTMessage.getLocalizedMessage(RESOURCE,
               enterpriseResource.LOCAL_SEARCH_LABEL, null, locale);
      }

      if (parameters == null) {
         parameters = new Properties();
      }

      if (search_type != null && search_type.equals("WindchillPDM")) {
         return ClientSearchHelper.getFoundationSearchURL(getURLFactory());
      } else {
         return ClientSearchHelper.getSolutionsSearchURL(getURLFactory());
      }
   }

   /**
    *  <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  parameters  Description of the Parameter
    *@param  locale      Description of the Parameter
    *@param  os          Description of the Parameter
    */
   public void getSearchCriteriaURL(Properties parameters, Locale locale, OutputStream os) {
      PrintWriter out = getPrintWriter(os, locale);

      String label = null;
      String application = null;

      if (parameters != null) {
         if (parameters.get("label") != null) {
            label = (String) parameters.get("label");
         }
         if (parameters.get("application") != null) {
            application = (String) parameters.get("application");
         }
      }

      out.println(getSearchCriteriaURL(label, application, parameters, null, locale));
      out.flush();
   }

   /**
    *  Output a table of links to global actions. This method does not currently
    *  support customization, but needs to be updated so that it will support
    *  customization. <BR>
    *  <BR>
    *  <B>Supported API: </B> true
    *
    *@param  parameters  Description of the Parameter
    *@param  locale      Description of the Parameter
    *@param  os          Description of the Parameter
    */
   public void createGlobalNavigationBar(Properties parameters, Locale locale,
         OutputStream os) {
    if (WindchillPDM)
    {
      PrintWriter out = getPrintWriter(os, locale);

      String bgcolor = getWCColor("bg-navbar");
      String linkcolor = getWCColor("f-navbar");
      String sel_linkcolor = getWCColor("f-navbar-active");
      String fontface = getWCFontFamily();
      String current_page = "";

      if (parameters != null) {
         if (parameters.get("bgcolor") != null) {
            bgcolor = (String) parameters.get("bgcolor");
         } else if (parameters.get("BGCOLOR") != null) {
            bgcolor = (String) parameters.get("BGCOLOR");
         }

         if (parameters.get("linkcolor") != null) {
            linkcolor = (String) parameters.get("linkcolor");
         } else if (parameters.get("LINKCOLOR") != null) {
            linkcolor = (String) parameters.get("LINKCOLOR");
         }

         if (parameters.get("selected_linkcolor") != null) {
            sel_linkcolor = (String) parameters.get("selected_linkcolor");
         } else if (parameters.get("SELECTED_LINKCOLOR") != null) {
            sel_linkcolor = (String) parameters.get("SELECTED_LINKCOLOR");
         }

         current_page = parameters.getProperty("currentPage", current_page);
      }

      String table_entry = "";
      String table = "";
      String style = "STYLE=\"color:" + linkcolor + "; font-family:" + fontface + "\"";
      String bold_style = "STYLE=\"color:" + linkcolor + "; font-size:10pt; font-family:" + fontface + "\"";

      if (linkPrefix == null) {
         linkPrefix = WTMessage.getLocalizedMessage(RESOURCE, enterpriseResource.LINK_PREFIX,
               null, locale);
      }

      // Add Link to Homepage
      if (VERBOSE) {
         System.out.println("Creating Link to Homepage");
      }

      try {
         if (current_page.equals(HOMEPAGE)) {
            style = "STYLE=\"color:" + sel_linkcolor + ";text-decoration:none; font-family:" + fontface + "\"";
            bold_style = "STYLE=\"color:" + sel_linkcolor + "; font-size:10pt; font-family:" + fontface + "\"";
         } else {
            style = "STYLE=\"color:" + linkcolor + "; font-family:" + fontface + "\"";
            bold_style = "STYLE=\"color:" + linkcolor + "; font-size:10pt; font-family:" + fontface + "\"";
         }

         table_entry = HtmlUtil.addTableEntry("ALIGN=RIGHT",
               "<B " + bold_style + ">" +
               linkPrefix + getHomepageURL(null, style, locale) + "</B>");
         table += HtmlUtil.addTableRow("ALIGN=RIGHT", table_entry);
      } catch (Exception e) {
         // Ignore exception so the rest of the links can be built
         if (VERBOSE) {
            System.err.println("createGlobalNavigationBar() - exception creating link to " +
                  "homepage");
            e.printStackTrace();
         }
      }

      // Add Link to Personal Cabinet
      if (VERBOSE) {
         System.out.println("Creating link to Personal Cabinet");
      }

      try {
         try {
            if ((getContextObj() instanceof Cabinet) &&
                  (isEqual((Cabinet) getContextObj(), getPersonalCabinet(null)))) {
               style = "STYLE=\"color:" + sel_linkcolor + ";text-decoration:none; font-family:" + fontface + "\"";
               bold_style = "STYLE=\"color:" + sel_linkcolor + "; font-size:10pt; font-family:" + fontface + "\"";
            } else {
               style = "STYLE=\"color:" + linkcolor + "; font-family:" + fontface + "\"";
               bold_style = "STYLE=\"color:" + linkcolor + "; font-size:10pt; font-family:" + fontface + "\"";
            }
         } catch (WTException wte) {
            if (VERBOSE) {
               System.out.println("createGlobalNavigationBar - exception caught ");
               System.out.println("building link to personal cabinet: ");
               wte.printStackTrace();
            }
            // Ignore exception
         }

         table_entry = HtmlUtil.addTableEntry("ALIGN=RIGHT",
               "<B " + bold_style + ">" + linkPrefix +
               getPersonalCabinetURL(null, null, style, locale) + "</B>");
         table += HtmlUtil.addTableRow("ALIGN=RIGHT", table_entry);
      } catch (Exception e) {
         // Ignore exception so the rest of the links can be built
         if (VERBOSE) {
            System.err.println("createGlobalNavigationBar() - exception creating link to " +
                  "Personal Cabinet");
            e.printStackTrace();
         }
      }

      // Add Link to User's Checked out Folder
      if (VERBOSE) {
         System.out.println("Creating link to Checked Out Folder");
      }

      try {
         if ((getContextObj() instanceof Folder) &&
               (isEqual((WTObject) getContextObj(), (WTObject) WorkInProgressHelper.service.getCheckoutFolder()))) {
            style = "STYLE=\"color:" + sel_linkcolor + ";text-decoration:none; font-family:" + fontface + "\"";
            bold_style = "STYLE=\"color:" + sel_linkcolor + "; font-size:10pt; font-family:" + fontface + "\"";
         } else {
            style = "STYLE=\"color:" + linkcolor + "; font-family:" + fontface + "\"";
            bold_style = "STYLE=\"color:" + linkcolor + "; font-size:10pt; font-family:" + fontface + "\"";
         }

         table_entry = HtmlUtil.addTableEntry("ALIGN=RIGHT",
               "<B " + bold_style + ">" + linkPrefix +
               getCheckedOutFolderURL(null, style, locale) + "</B>");
         table += HtmlUtil.addTableRow("ALIGN=RIGHT", table_entry);
      } catch (Exception e) {
         // Ignore exception so the rest of the links can be built
         if (VERBOSE) {
            System.err.println("createGlobalNavigationBar() - exception creating link to " +
                  "Checked Out Folder");
            e.printStackTrace();
         }
      }

      // Add link to Search
      if (VERBOSE) {
         System.out.println("Creating link to Search");
      }

      try {
         style = "STYLE=\"color:" + linkcolor + "; font-family:" + fontface + "\"";
         bold_style = "STYLE=\"color:" + linkcolor + "; font-size:10pt; font-family:" + fontface + "\"";

         String label = WTMessage.getLocalizedMessage(RESOURCE, enterpriseResource.SEARCH_LABEL, null, locale);
         URLFactory factory = getState().getURLFactory();
         String searchHREF = factory.getHREF("/wtcore/jsp/wt/portal/index.jsp");

         table_entry = HtmlUtil.addTableEntry("ALIGN=RIGHT",
               "<B " + bold_style + ">" + linkPrefix +
               HtmlUtil.createLink(searchHREF, style, label) +
               "</B>");
         table += HtmlUtil.addTableRow("ALIGN=RIGHT", table_entry);
      } catch (Exception e) {
         // Ignore exception so the rest of the links can be built
         if (VERBOSE) {
            System.err.println("createGlobalNavigationBar() - exception creating link to " +
                  "Import Document");
            e.printStackTrace();
         }
      }

      // Add link to User's Worklist
      if (VERBOSE) {
         System.out.println("Creating link to User's Worklist");
      }

      try {
         if (current_page.equals(WORKLIST)) {
            style = "STYLE=\"color:" + sel_linkcolor + ";text-decoration:none; font-family:" + fontface + "\"";
            bold_style = "STYLE=\"color:" + sel_linkcolor + "; font-size:10pt; font-family:" + fontface + "\"";
         } else {
            style = "STYLE=\"color:" + linkcolor + "; font-family:" + fontface + "\"";
            bold_style = "STYLE=\"color:" + linkcolor + "; font-size:10pt; font-family:" + fontface + "\"";
         }
         table_entry = HtmlUtil.addTableEntry("ALIGN=RIGHT",
               "<B " + bold_style + ">" + linkPrefix +
               getWorklistURL(null, style, locale) + "</B>");
         table += HtmlUtil.addTableRow("ALIGN=RIGHT", table_entry);
      } catch (Exception e) {
         // Ignore exception so the rest of the links can be built
         if (VERBOSE) {
            System.err.println("createGlobalNavigationBar() - exception creating link to " +
                  "User's Worklist");
            e.printStackTrace();
         }
      }

      // Add link to Import
      if (VERBOSE) {
         System.out.println("Creating link to Import");
      }

      try {
         if (current_page.equals(IMPORT)) {
            style = "STYLE=\"color:" + sel_linkcolor + ";text-decoration:none; font-family:" + fontface + "\"";
            bold_style = "STYLE=\"color:" + sel_linkcolor + "; font-size:10pt; font-family:" + fontface + "\"";
         } else {
            style = "STYLE=\"color:" + linkcolor + "; font-family:" + fontface + "\"";
            bold_style = "STYLE=\"color:" + linkcolor + "; font-size:10pt; font-family:" + fontface + "\"";
         }

         String label = WTMessage.getLocalizedMessage(RESOURCE, enterpriseResource.IMPORT_LABEL, null, locale);
         Properties properties = new Properties();
         properties.put("Class", "wt.doc.WTDocument");
         properties.put("Action", "Create");

         if (getContextObj() instanceof Folder) {
            properties.put("Location", ((CabinetBased) getContextObj()).getFolderPath());
         } else if (getContextObj() instanceof CabinetBased) {
            properties.put("Location", ((CabinetBased) getContextObj()).getLocation());
         }

         table_entry = HtmlUtil.addTableEntry("ALIGN=RIGHT",
               "<B " + bold_style + ">" + linkPrefix +
               getURLProcessorLinkWithLabel(label, style, "generateForm", properties) +
               "</B>");
         table += HtmlUtil.addTableRow("ALIGN=RIGHT", table_entry);
      } catch (Exception e) {
         // Ignore exception so the rest of the links can be built
         if (VERBOSE) {
            System.err.println("createGlobalNavigationBar() - exception creating link to " +
                  "Import Document");
            e.printStackTrace();
         }
      }

      //Add Link to Create Document from Template
      try {
       boolean templatesExist = EnterpriseHelper.service.templatesExist(wt.inf.container.WTContainerHelper.service.getClassicContainer(), WTDocument.class, true, true);
       if (templatesExist) {
            if (current_page.equals(DOCFROMTEMPLATE)) {
               style = "STYLE=\"color:" + sel_linkcolor + ";text-decoration:none; font-family:" + fontface + "\"";
               bold_style = "STYLE=\"color:" + sel_linkcolor + "; font-size:10pt; font-family:" + fontface + "\"";
            } else {
               style = "STYLE=\"color:" + linkcolor + "; font-family:" + fontface + "\"";
               bold_style = "STYLE=\"color:" + linkcolor + "; font-size:10pt; font-family:" + fontface + "\"";
            }

            String label = WTMessage.getLocalizedMessage(RESOURCE, enterpriseResource.DOCFROMTEMPLATE_LABEL, null, locale);
            Properties properties = new Properties();
            properties.put("Class", "wt.doc.WTDocument");
            properties.put("Action", "CreateDocFromTemplateGeneral");

            if (getContextObj() instanceof Folder) {
               properties.put("Location", ((CabinetBased) getContextObj()).getFolderPath());
            } else if (getContextObj() instanceof CabinetBased) {
               properties.put("Location", ((CabinetBased) getContextObj()).getLocation());
            }

            table_entry = HtmlUtil.addTableEntry("ALIGN=RIGHT",
                  "<B " + bold_style + ">" + linkPrefix +
                  getURLProcessorLinkWithLabel(label, style, "generateForm", properties) +
                  "</B>");
            table += HtmlUtil.addTableRow("ALIGN=RIGHT", table_entry);
        }
      } catch (Exception e) {
         // Ignore exception so the rest of the links can be built
         if (VERBOSE) {
            System.err.println("createGlobalNavigationBar() - exception creating link to " +
                  "Create Document from Template");
            e.printStackTrace();
         }
      }

      // Add link to Preferences
      if (VERBOSE) {
         System.out.println("Creating link to Preferences");
      }

      try {
         style = "STYLE=\"color:" + linkcolor + "; font-family:" + fontface + "\"";
         bold_style = "STYLE=\"color:" + linkcolor + "; font-size:10pt; font-family:" + fontface + "\"";

         String label = WTMessage.getLocalizedMessage(RESOURCE, enterpriseResource.PREFERENCES_LABEL, null, locale);
         String prefAction = null;

         if (parameters != null) {
            prefAction = parameters.getProperty(PREFERENCE_CONTEXT, "");
         }
         if (prefAction == null || prefAction.length() < 1) {
            prefAction = DEFAULT_PREFERENCE_CONTEXT;
         }

         HashMap map = new HashMap();
         map.put("Action", prefAction);
         map.put("Class", "java.lang.Object");

         String urlString = GatewayServletHelper.buildAuthenticatedHREF(getState().getURLFactory(),
               HTML_GATEWAY,
               HTML_TEMPLATE_ACTION,
               null, map);

         urlString = "javascript:openNewWindow('" + urlString + "','prefWindow'," +
               "'resizable=yes,scrollbars=yes,menubar=no,toolbar=no," +
               "location=no,status=yes,width=' + .6*(screen.availWidth)" +
               "+ ',height=' + .7*(screen.availHeight))";

         urlString = HtmlUtil.createLink(urlString, style, label);

         String methodString = "\n<SCRIPT LANGUAGE=Javascript1.1>\n" +
               "function openNewWindow(linktext,win_name,props) {\n" +
               "   query_window = open(linktext,win_name,props);\n" +
               "}</SCRIPT>";

         table_entry = HtmlUtil.addTableEntry("ALIGN=RIGHT",
               "<B " + bold_style + ">" + linkPrefix +
               urlString + "</B>" + methodString);
         table += HtmlUtil.addTableRow("ALIGN=RIGHT", table_entry);
      } catch (Exception e) {
         // Ignore exception so the rest of the links can be built
         if (VERBOSE) {
            System.err.println("createGlobalNavigationBar() - exception creating link to " +
                  "User Preferences");
            e.printStackTrace();
         }
      }
      //Adding Link for MyWorkspaces if user agent is wildfire.
      try {
         if (isWildfireEnabled()) {
            style = "STYLE=\"color:" + linkcolor + "; font-family:" + fontface + "\"";
            bold_style = "STYLE=\"color:" + linkcolor + "; font-size:10pt; font-family:" + fontface + "\"";

            Properties props = new Properties();
            props.put("action", "MyWorkspace");

            // Show only the Windchill PDM workspaces when user clicks on this link. For that to happen,
            // pass the Windchill PDM container oid as query string to the action URL.
            // It's assumed that control comes to this method for only Windchill PDM container
            // context.
                WTContainer windchillPDMContainer = (WTContainer) WTContainerHelper.service.getClassicContainer();
                if (windchillPDMContainer != null) {
               ReferenceFactory rf = new ReferenceFactory();
               String oidStr = rf.getReferenceString((Persistable) windchillPDMContainer);
               if (oidStr != null) {
                        props.put("oid", oidStr);
                    }
                } else {
                    if (VERBOSE) {
                    System.err.println("createGlobalNavigationBar() - it is not a Windchill PDM context.");
                }
                }
            String label = WTMessage.getLocalizedMessage(RESOURCE, enterpriseResource.MY_WORKSPACE, null, locale);
            table_entry = HtmlUtil.addTableEntry("ALIGN=RIGHT",
                  "<B " + bold_style + ">" +
                  linkPrefix + getURLProcessorLinkWithLabel(label, style, "URLTemplateAction", props) + "</B>");
            table += HtmlUtil.addTableRow("ALIGN=RIGHT", table_entry);
         }
      } catch (Exception e) {
         // Ignore exception so the rest of the links can be built

         if (VERBOSE) {
            System.err.println("Creating GlobalNavigationBar - exception creating link to " +
                  "MyWorkspaces");
            e.printStackTrace();
         }
      }

      // This section adds the HTML Help link, if applicable. The link will be generated is there
      // is a valid entry in a properties file based on the context of the HTML page. If there
      // is not a valid entry, then no link or text is generated.
      if (VERBOSE) {
         System.out.println("Creating link to HTML Help");
      }

      try {
         if (parameters != null && parameters.getProperty("LINK_TEXT_COLOR") == null) {
            parameters.put("LINK_TEXT_COLOR", linkcolor);
         } else {
            parameters = new Properties();
            parameters.put("LINK_TEXT_COLOR", linkcolor);
         }

         if (parameters.getProperty("ADD_HELP_ICON") == null) {
            parameters.put("ADD_HELP_ICON", String.valueOf(globalNavBarHelpIconEnabledDefault));
         }

         bold_style = "STYLE=\"color:" + linkcolor + "; font-size:10pt; font-family:" + fontface + "\"";
         table_entry = HtmlUtil.addTableEntry("ALIGN=RIGHT",
               "<B " + bold_style + ">" + getHTMLHelpLink(parameters, locale, os) + "</B>");
         table += HtmlUtil.addTableRow("ALIGN=RIGHT", table_entry);
      } catch (Exception e) {
         // Ignore exception so the rest of the links can be built
         if (VERBOSE) {
            System.err.println("Creating GlobalNavigationBar - exception creating link to " +
                  "HTML Help");
            e.printStackTrace();
         }
      }

      // Print out table
      out.println(HtmlUtil.createTable("BORDER=0 CELLPADDING=2 CELLSPACING=2",
            table));
      out.flush();
   }
   }

   /**
    *  Gets the checkedOutFolderURL attribute of the BasicTemplateProcessor class
    *
    *@param  label       Description of the Parameter
    *@param  parameters  Description of the Parameter
    *@param  locale      Description of the Parameter
    *@return             The checkedOutFolderURL value
    */
   public static String getCheckedOutFolderURL(String label, String parameters, Locale locale) {
      String url = label;

      if ((label == null) ||
            (label.length() == 0)) {
         label = WTMessage.getLocalizedMessage(RESOURCE, enterpriseResource.CHECKOUT_FOLDER_LABEL, null, locale);
      }

      try {
         Folder checkout_folder = wt.vc.wip.WorkInProgressHelper.service.getCheckoutFolder();
         url = objectActionLinkAux(checkout_folder, "ObjProps", label, parameters, locale);
      } catch (WTException wte) {
         url = label;
      }

      return url;
   }

   /**
    *  <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  label        Description of the Parameter
    *@param  search_type  Description of the Parameter
    *@param  parameters   Description of the Parameter
    *@param  href_props   Description of the Parameter
    *@param  locale       Description of the Parameter
    *@return              The federationSearchURL value
    */
   public static String getFederationSearchURL(String label,
         String search_type,
         Properties parameters,
         String href_props,
         Locale locale) {
      String url = label;
      if ((label == null) ||
            (label.length() == 0)) {
         label = WTMessage.getLocalizedMessage(FEDERATION_RESOURCE,
               wt.federation.federationResource.FEDERATION_SEARCH_LABEL, null, locale);
      }

      if (parameters == null) {
         parameters = new Properties();
      }

      if (search_type != null) {
         parameters.put("query", search_type);
      }

      try {
         url = objectActionLinkAux(null, "FederationSearch", label,
               parameters, href_props, locale);
      } catch (WTException wte) {
         url = label;
      }

      return url;
   }

   /**
    *  Output a table of links to federation actions. This method does not
    *  currently support customization, but needs to be updated so that it will
    *  support customization. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  parameters  Description of the Parameter
    *@param  locale      Description of the Parameter
    *@param  os          Description of the Parameter
    */
   public void createFederationNavigationBar(Properties parameters,
         Locale locale,
         OutputStream os) {
      PrintWriter out = getPrintWriter(os, locale);

      String bgcolor = "#9B599B";
      String linkcolor = "#D9B2D9";
      String sel_linkcolor = "#FFFFCC";
      String current_page = "";

      if (parameters != null) {
         if (parameters.get("bgcolor") != null) {
            bgcolor = (String) parameters.get("bgcolor");
         } else if (parameters.get("BGCOLOR") != null) {
            bgcolor = (String) parameters.get("BGCOLOR");
         }

         if (parameters.get("linkcolor") != null) {
            linkcolor = (String) parameters.get("linkcolor");
         } else if (parameters.get("LINKCOLOR") != null) {
            linkcolor = (String) parameters.get("LINKCOLOR");
         }

         if (parameters.get("selected_linkcolor") != null) {
            sel_linkcolor = (String) parameters.get("selected_linkcolor");
         } else if (parameters.get("SELECTED_LINKCOLOR") != null) {
            sel_linkcolor = (String) parameters.get("SELECTED_LINKCOLOR");
         }

         current_page = parameters.getProperty("currentPage");
      }

      if (linkPrefix == null) {
         linkPrefix = WTMessage.getLocalizedMessage(RESOURCE, enterpriseResource.LINK_PREFIX,
               null, locale);
      }

      String table_entry = "";
      String table = "";
      String style = "STYLE=\"color:" + linkcolor + ";\"";
      String bold_style = "STYLE=\"color:" + linkcolor + "; font-size:10pt;\"";

      // Add link to Federation Search
      if (VERBOSE) {
         System.out.println("Creating link to Federation Search");
      }

      try {
         if (current_page.equals(FEDERATION_SEARCH)) {
            style = "STYLE=\"color:" + sel_linkcolor + ";text-decoration:none\"";
            bold_style = "STYLE=\"color:" + sel_linkcolor + "; font-size:10pt;\"";
         } else {
            style = "STYLE=\"color:" + linkcolor + ";\"";
            bold_style = "STYLE=\"color:" + linkcolor + "; font-size:10pt;\"";
         }

         table_entry = HtmlUtil.addTableEntry
               ("ALIGN=RIGHT",
               "<B " + bold_style + ">"
                + linkPrefix
                + getFederationSearchURL(null, null, null, style, locale)
                + "</B>");

         table += HtmlUtil.addTableRow("ALIGN=RIGHT", table_entry);
      } catch (Exception e) {
         // Ignore exception so the rest of the links can be built
         if (VERBOSE) {
            System.err.println("createFederationNavigationBar() - exception creating link to " +
                  "Federation Search");
            e.printStackTrace();
         }
      }

      // Print out table
      out.println(HtmlUtil.createTable("BORDER=0 CELLPADDING=2 CELLSPACING=2",
            table));
      out.flush();
   }

   /**
    *  Returns a String containing the HTML code to create a link to display all
    *  cabinets. The option String label is used as the text of the link. The
    *  optional String parameters is used to add additional HTML to the forming of
    *  the link. The given Locale parameter determines the locale in which the
    *  link is displayed.
    *
    *@param  label       Description of the Parameter
    *@param  parameters  Description of the Parameter
    *@param  locale      Description of the Parameter
    *@return             The allCabinetsURL value
    */
   public static String getAllCabinetsURL(String label, String parameters, Locale locale) {
      String url = label;

      if ((label == null) ||
            (label.length() == 0)) {
         label = WTMessage.getLocalizedMessage(RESOURCE, enterpriseResource.ALL_CABINETS_LABEL, null, locale);
      }

      try {
         url = objectActionLinkAux(null, "Cabinets", label, parameters, locale);
      } catch (WTException wte) {
         if (VERBOSE) {
            System.out.println("getAllCabinetsURL - exception caught building link:");
            wte.printStackTrace();
         }
      }

      return url;
   }

   /**
    *  Gets the stringFromResourceBundle attribute of the BasicTemplateProcessor
    *  class
    *
    *@param  resource_key     Description of the Parameter
    *@param  resource_class   Description of the Parameter
    *@param  params           Description of the Parameter
    *@param  locale           Description of the Parameter
    *@return                  The stringFromResourceBundle value
    *@exception  WTException  Description of the Exception
    */
   public static String getStringFromResourceBundle(String resource_key, String resource_class,
         Object[] params, Locale locale) throws WTException {
      if (VERBOSE) {
         System.out.println("Entering getStringFromResourceBundle()...");
      }

      Class resourceKeyClass = null;

      try {
         resourceKeyClass = Class.forName(resource_class);
      } catch (java.lang.ClassNotFoundException e) {
         if (resource_key == null || resource_key.length() == 0) {
            resource_key = "<resource key null or blank>";
         }
         if (resource_class == null || resource_key.length() == 0) {
            resource_class = "<resource class null or blank>";
         }
         Object[] param = {resource_key, resource_class};
         throw (new WTException(e,
               WTMessage.getLocalizedMessage(RESOURCE,
               enterpriseResource.NO_RESOURCEKEY_CLASS, param, locale)));
      }

      Field attributeField = null;
      try {
         attributeField =
               resourceKeyClass.getField(resource_key);
      } catch (java.lang.NoSuchFieldException e) {
         if (resource_key == null || resource_key.length() == 0) {
            resource_key = "<resource key null or blank>";
         }
         if (resource_class == null || resource_key.length() == 0) {
            resource_class = "<resource class null or blank>";
         }
         Object[] param = {resource_key, resource_class};
         throw (new WTException(e,
               WTMessage.getLocalizedMessage(RESOURCE,
               enterpriseResource.NO_RESOURCEKEY_FIELD, param, locale)));
      }
      if (VERBOSE) {
         System.out.println("  attributeField -> " + attributeField);
      }

      String attrName = null;
      try {
         attrName = (String) attributeField.get(null);
      } catch (java.lang.IllegalAccessException e) {
         throw (new WTException(e));
      }

      if (VERBOSE) {
         System.out.println("  attrName -> " + attrName);
      }

      return (WTMessage.getLocalizedMessage(resource_class, attrName,
            params, locale));
   }

   /**
    *  Gets the stringFromResourceBundle attribute of the BasicTemplateProcessor
    *  class
    *
    *@param  resource_key     Description of the Parameter
    *@param  resource_class   Description of the Parameter
    *@param  locale           Description of the Parameter
    *@return                  The stringFromResourceBundle value
    *@exception  WTException  Description of the Exception
    */
   public static String getStringFromResourceBundle(String resource_key, String resource_class, Locale locale)
          throws WTException {
      return getStringFromResourceBundle(resource_key, resource_class, null, locale);
   }

   /**
    *  <A NAME=getLocalizedMessage></A> Produces HTML that represents a localized
    *  message from a resouce bundle. <p>
    *
    *  Here is an example call from within and HTML template file:
    *  getLocalizedMessage resourceKey=NULL_CONTEXT_OBJECT
    *  resourceClass=wt.enterprise.enterpriseResource <p>
    *
    *  Optional parameter are:<BR>
    *  <BR>
    *
    *  <UL><code>unicodeEscaped=true</code> - Will unicode escape any non-Ascii
    *    characters in the form of \\uXXXX<BR>
    *    <code>byteEncoded=true</code> - Will encode the unicode bytes into Ascii
    *    using UTF-8 encoding. <code>newLine=false</code> - Will not append a line
    *    termination to the output. <code>paramX=value</code> - Includes the value
    *    of the parameter in an appropriate localized message. X must be a
    *    integer, distinct with respect to other 'param' values included in the
    *    same call. The referenced resource bundle key must be appropriately
    *    parameterized.
    *  </UL>
    *  <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  parameters  The arguments that were passed to the Windchill script
    *      call.
    *@param  locale      The Locale to send to the invoked methods for
    *      localization.
    *@param  os          The output stream.
    */
   public void getLocalizedMessage(Properties parameters, Locale locale,
         OutputStream os) {
      // throws wt.util.WTException

      if (VERBOSE) {
         System.out.println("Entering getLocalizedMessage()...");
      }

      String resourceKeyString = parameters.getProperty("resourceKey");
      String resourceClassString = parameters.getProperty("resourceClass");
      boolean newLine = ("false".equals(parameters.getProperty("newLine"))) ? false : true;
      boolean escape = ("true".equals(parameters.getProperty("unicodeEscaped"))) ? true : false;
      boolean byteEncoded = ("true".equals(parameters.getProperty("byteEncoded"))) ? true : false;
      TreeMap paramMap = new TreeMap();
      Enumeration keys = parameters.keys();
      while (keys.hasMoreElements()) {
         String key = (String) keys.nextElement();
         if (key.startsWith("param")) {
            paramMap.put(new Integer(key.substring("param".length())), parameters.getProperty(key));
         }
      }
      Object params[] = paramMap.size() > 0 ? new Object[paramMap.size()] : null;
      if (params != null) {
         Iterator values = paramMap.values().iterator();
         for (int i = 0; i < params.length; i++) {
            params[i] = values.next();
         }
      }

      try {
         String result = getStringFromResourceBundle(resourceKeyString, resourceClassString, params, locale);
         if (escape) {
            result = wt.httpgw.EncodingConverter.unicodeToAscii(result);
         } else if (byteEncoded) {
            EncodingConverter encoder = new EncodingConverter();
            result = encoder.encode(result, EncodingConverter.UTF8);
         }
         if (VERBOSE) {
            System.out.println("byteEncoded=" + byteEncoded + "\nescape=" + escape + "\nString Result=" + result);
         }

         PrintWriter out = getPrintWriter(os, locale);
         if (newLine) {
            out.println(result);
         } else {
            out.print(result);
         }
         out.flush();
      } catch (WTException wte) {
         // This string should not be localized, it is the name of the method.
         handleExceptionTP("getLocalizedMessage", wte, true,
               parameters, locale,
               os);
         return;
      }
   }

   /**
    *  Given an object, returns a <CODE>Vector</CODE> of <CODE>URLActionDelegates</CODE>
    *  available for that class of object. If no delegates for the particular
    *  class of the given object have been found, an empty vector is returned.
    *  <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  obj                                the <CODE>Object</CODE> for which
    *      a vector of URLActionDelegates is returned
    *@param  locale                             Description of the Parameter
    *@return                                    a Vector containing the
    *      URLActionDelegates which correspond to the class of the given object.
    *@exception  UnableToLoadServiceProperties  if the service.properties used to
    *      instantiate URLActionDelegates cannot be loaded.
    *@author                                    sah
    */
   public static Vector getURLActions(Object obj, Locale locale) throws UnableToLoadServiceProperties {
      return (getURLActions(obj.getClass(), locale));
   }

   /**
    *  Gets the uRLActions attribute of the BasicTemplateProcessor class
    *
    *@param  obj                                Description of the Parameter
    *@param  locale                             Description of the Parameter
    *@param  minimal                            Description of the Parameter
    *@return                                    The uRLActions value
    *@exception  UnableToLoadServiceProperties  Description of the Exception
    */
   public static Vector getURLActions(Object obj, Locale locale, boolean minimal) throws UnableToLoadServiceProperties {
      return (getURLActions(obj.getClass(), locale, minimal));
   }

   /**
    *  Given a class, returns a <CODE>Vector</CODE> of <CODE>URLActionDelegates</CODE>
    *  available for that class. If no delegates for the particular class have
    *  been found, an empty vector is returned. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  obj_class                          the <CODE>Class</CODE> for which a
    *      vector of URLActionDelegates is returned
    *@param  locale                             Description of the Parameter
    *@return                                    a Vector containing the
    *      URLActionDelegates which correspond to the given class.
    *@exception  UnableToLoadServiceProperties  if the service.properties used to
    *      instantiate URLActionDelegates cannot be loaded.
    *@author                                    sah
    */
   public static Vector getURLActions(Class obj_class, Locale locale) throws UnableToLoadServiceProperties {
      return getURLActions(obj_class, locale, false);
   }

   /**
    *  Gets the uRLActions attribute of the BasicTemplateProcessor class
    *
    *@param  obj_class                          Description of the Parameter
    *@param  locale                             Description of the Parameter
    *@param  minimal_list                       Description of the Parameter
    *@return                                    The uRLActions value
    *@exception  UnableToLoadServiceProperties  Description of the Exception
    */
   public static Vector getURLActions(Class obj_class, Locale locale, boolean minimal_list)
          throws UnableToLoadServiceProperties {
      Vector url_actions = new Vector();

      //  Get the entry from the UrlLinkResource resource bundle for the given class of object
      //  If 'minimal_list' is true, retrieve the minimal list from the resource bundle.
      if (obj_class != null) {
         String resource_key = obj_class.getName();

         if (minimal_list) {
            resource_key = resource_key.substring((resource_key.lastIndexOf(".")) + 1) + "_ACTION_LINKS_MIN";
         } else {
            resource_key = resource_key.substring((resource_key.lastIndexOf(".")) + 1) + "_ACTION_LINKS";
         }

         resource_key = resource_key.toUpperCase();
         if (VERBOSE) {
            System.out.println("getURLActions: key is " + resource_key);
         }

         Vector action_strings = null;
         try {
            action_strings = getActionPairs(resource_key, locale);
            if ((action_strings == null) ||
                  (action_strings.size() == 0)) {
               resource_key = resource_key.substring((resource_key.lastIndexOf(".")) + 1) + "_ACTION_LINKS";
               action_strings = getActionPairs(resource_key, locale);
            }
         } catch (MissingResourceException mre) {
            if (VERBOSE) {
               System.out.println("No action string resource for: " +
                     resource_key);
            }
         }

         if ((action_strings == null) ||
               (action_strings.size() == 0)) {
            Class super_class = obj_class.getSuperclass();
            while ((super_class != null) &&
                  ((action_strings == null) ||
                  (action_strings.size() == 0))) {
               resource_key = super_class.getName();

               String append_text = "_ACTION_LINKS";
               if (minimal_list) {
                  append_text = "_ACTION_LINKS_MIN";
               }

               resource_key =
                     resource_key.substring((resource_key.lastIndexOf(".")) + 1) +
                     append_text;
               resource_key = resource_key.toUpperCase();
               try {
                  action_strings = getActionPairs(resource_key, locale);
               } catch (MissingResourceException mre) {
                  if (VERBOSE) {
                     System.out.println("No action string resource for: " +
                           resource_key);
                  }
               }
               super_class = super_class.getSuperclass();
            }
         }

         if ((action_strings != null) &&
               (action_strings.size() > 0)) {
            URLActionDelegateFactory delegate_factory = new URLActionDelegateFactory();
            for (int i = 0; i < action_strings.size(); i++) {
               if (VERBOSE) {
                  System.out.println("Getting delegate for action " + action_strings.elementAt(i));
               }

               //This fix will enable customers to not show any actions under the TP search pages
               if (!action_strings.elementAt(i).equals(UrlLinkResource.NO_ACTIONS))
               {
                  URLActionDelegate delegate =
                     (URLActionDelegate)delegate_factory.getDelegate( (String)action_strings.elementAt(i) );
                  url_actions.addElement( delegate );
                  if( VERBOSE ) {
                     System.out.println( "Adding delegate " + delegate );
                  }
               }
            }
         } else if (minimal_list) {
            return (getURLActions(obj_class, locale, false));
         }
      }

      return url_actions;
   }

   /**
    *  Returns a String of links to actions that can be performed on the given
    *  Object. The labels displayed as the text of the links are formatted using
    *  the given locale. Additional HTML to be used in building the A HREF link
    *  tag is specified in the optionally given String parameter. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  obj                                the Object to which the URLs to
    *      actions apply
    *@param  locale                             the locale used in determining
    *      what text to display
    *@param  link_params                        optional HTML code to be used in
    *      the A HREF tags
    *@return                                    a String of HTML code containing
    *      links
    *@exception  UnableToLoadServiceProperties  Description of the Exception
    */
   public String getURLActionsString(Object obj, Locale locale, String link_params)
          throws UnableToLoadServiceProperties {
      Vector actionDelegates = getURLActions(obj, locale);
      return getURLActionsString(obj, actionDelegates, locale, link_params);
   }

   /**
    *  Gets the uRLActionsString attribute of the BasicTemplateProcessor object
    *
    *@param  obj                                Description of the Parameter
    *@param  actionDelegates                    Description of the Parameter
    *@param  locale                             Description of the Parameter
    *@param  link_params                        Description of the Parameter
    *@return                                    The uRLActionsString value
    *@exception  UnableToLoadServiceProperties  Description of the Exception
    */
   public String getURLActionsString(Object obj, Vector actionDelegates, Locale locale, String link_params)
          throws UnableToLoadServiceProperties {
      StringBuffer actionString = new StringBuffer(1024);
      URLActionDelegate currentDelegate;
      String label;
      String url;

      if ((actionDelegates != null) &&
            (actionDelegates.size() > 0)) {
         int i = 0;
         while (i < (actionDelegates.size() - 1)) {
            try {
               currentDelegate = (URLActionDelegate) actionDelegates.elementAt(i++);
               label = currentDelegate.getURLLabel(locale);
               url = getURLFromDelegate(currentDelegate, obj, getState());
               if (VERBOSE) {
                  System.out.println("URL:   " + url);
                  System.out.println("Label: " + label);
               }

               if (url != null) {
                  actionString.append(HtmlUtil.createLink(url, link_params, label))
                        .append(BLANK_SPACE).append(BLANK_SPACE);
               }
            } catch (Exception e) {
               if (VERBOSE) {
                  System.out.print("getURLActionsString: exception caught: ");
                  e.printStackTrace();
               }
            }
         }
         try {
            currentDelegate = (URLActionDelegate) actionDelegates.elementAt(i);
            label = currentDelegate.getURLLabel(locale);
            url = getURLFromDelegate(currentDelegate, obj, getState());
            if (VERBOSE) {
               System.out.println("URL:   " + url);
               System.out.println("Label: " + label);
            }

            if (url != null) {
               actionString.append(HtmlUtil.createLink(url, link_params, label));
            }
         } catch (Exception e) {
            if (VERBOSE) {
               System.err.println("getURLActionsString() - exception caught");
               e.printStackTrace();
            }
         }
      }
      return String.valueOf(actionString);
   }

   /**
    *  translateToHtml replaces null values or empty strings with the HTML code
    *  for a non-breakable space. It also replaces newLine characters with the
    *  html line break code.
    *
    *@param  value  The value to be translated to html viewable form
    *@return        the HTML code to display value
    */
   public String translateToHtml(String value) {
      if (value == null) {
         return BLANK_SPACE;
      }
      if (value.equals("")) {
         return BLANK_SPACE;
      }

      //Now look for carriage returns & replace them with the html equivilent
      if (value.indexOf("\n") == -1) {
         try {
            return (String) (new UrlAwareTextFormatter(value)).parse();
         } catch (java.io.IOException ioe) {
            ioe.printStackTrace();
            return value;
         }
      } else {
         //Replace newLines with <BR>

         StringBuffer temp = new StringBuffer();
         int i = 0;
         int j = 0;
         while (j != -1) {
            j = value.indexOf("\n", i);
            if (j != -1) {
               temp.append(value.substring(i, j) + "<BR>");
               i = j + 1;
            }
         }
         temp.append(value.substring(i));
         try {
            return (String) (new UrlAwareTextFormatter(temp.toString())).parse();
         } catch (java.io.IOException ioe) {
            ioe.printStackTrace();
            return temp.toString();
         }
      }
   }

   /**
    *  A call to printQueryTable from an HTML template will generate an HTML table
    *  with the results of a query presented in the HTML table. "printQueryTable"
    *  assumes several things
    *  <OL>
    *    <LI> You will be using a subclass of
    *    wt.enterprise.tabularresults.BasicQueryService to perform the query
    *    <LI> The table is being presented within another HTML table
    *    <LI> Your table's "Title" or "Caption" can be passed in and found either
    *    through introspection or a resource bundle
    *  </OL>
    *  Here is an example of how to use a call to "printQueryTable" in a WindChill
    *  Script in an HTML template without printing a title for the table <Code>
    *  <PRE>
    *
    *   &lt;SCRIPT LANGUAGE=Windchill>
    *   &lt;!--
    *   printQueryTable QueryName=UsedByQuery colspan=4
    *   -->
    *   &lt;/SCRIPT>
    *  </PRE> </Code> Here is an example of how to use a call to "printQueryTable"
    *  in a WindChill Script in an HTML template that does print a title for the
    *  table <Code>
    *  <PRE>
    *
    *   &lt;SCRIPT LANGUAGE=Windchill>
    *   &lt;!--
    *   printQueryTable QueryName=UsedByQuery colspan=4 tableType=<Resource Bundle Key> resourceBundle=<Resource Bundle>
    *   -->
    *   &lt;/SCRIPT>
    *  </PRE> </Code> Both of these calls assume that the call to printQueryTable
    *  is going to fill an entire row in the HTML table that it is embedded. The
    *  colspan argument is used to specify the width of the parent table. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  parameters       The parameters must to pass in 1. the parameter
    *      QueryName and 2.the parameter tableType
    *@param  locale           the locale used in determining what text to display
    *@param  os               The output stream for the generated table
    *@exception  WTException  Description of the Exception
    */
   public void printQueryTable(Properties parameters, Locale locale,
         OutputStream os)
          throws WTException {
      parameters.put("action", "INITQUERYTABLE");
      tableService(parameters, locale, os);
      String colspan = parameters.getProperty("colspan");
      int colspanValue = 1;
      try {
         colspanValue = Integer.parseInt(colspan);
      } catch (NumberFormatException nfe) {
         if (VERBOSE) {
            nfe.printStackTrace();
         }
      }
      PrintWriter out = getPrintWriter(os, locale);

      String caption = "";
      String tableType = null;
      String resourceBundle = null;
      if (parameters.containsKey("tableType")) {
         tableType = parameters.getProperty("tableType");
         if (tableType != null && !tableType.equals("")) {
            if (parameters.containsKey("tableType")) {
               resourceBundle = parameters.getProperty("resourceBundle");
            }
            if (resourceBundle == null) {
               throw new WTException("You must pass in both a tableType and a resourceBundle");
            }
            caption = getResourceString(tableType, locale, resourceBundle, tableType);
         }
      } else {
         caption = tableType;
      }
      if (caption != null && caption.trim().endsWith(":")) {
         caption = caption.substring(caption.length() - 2);
         if (VERBOSE) {
            System.out.println("printCenteredQueryTable : caption = " + caption);
         }
      }
//       parameters.put("caption",caption);
      if (caption != null && !caption.equals("")) {
         out.println("<TR>");
         out.println("<TD ALIGN=\"CENTER\" COLSPAN=" + String.valueOf(colspanValue) + ">");
         out.println("<TABLE border=0 WIDTH=\"90%\"><TR><TD ALIGN=\"LEFT\" COLSPAN=" + String.valueOf(colspanValue) + ">");
         out.println("<H3>" + caption + "</H3>");
         out.println("</TD></TR></TABLE>");
         out.println("</TD>");
         out.println("</TR>");
      }

      out.println("<TR >");
      out.println("<TD ALIGN=CENTER VALIGN=TOP COLSPAN=" + String.valueOf(colspanValue) + ">");
      out.flush();
      printTable(parameters, locale, os);
      out.println("</TD>");
      out.println("</TR>");
      out.flush();
   }

   /**
    *  A call to printTextArea will output an HTML textarea with a title under the
    *  assumption that the title and textarea are to appear as a row in the an
    *  HTML table. The only required entry in the "parameter" to be passed into
    *  printTextArea is "colspan" property that should pass in the number of
    *  columns in the parent HTML table. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  label       Used as the title to the textarea and is presented to the
    *      left of the textarea
    *@param  text        The actual text to be displayed in the textarea
    *@param  parameters  Needs to pass in several parameters to format textarea's
    *      output
    *@param  locale      the locale used in determining what text to display
    *@param  os          The output stream for the generated table
    */
   public void printTextArea(String label, String text, Properties parameters, Locale locale,
         OutputStream os) {
      PrintWriter out = getPrintWriter(os, locale);
      String colspan = parameters.getProperty("colspan");
      int colspanValue = 1;
      try {
         colspanValue = Integer.parseInt(colspan);
      } catch (NumberFormatException nfe) {
         if (VERBOSE) {
            nfe.printStackTrace();
         }
      }
      out.println("<TR rowspan=3 >");
      out.println("<TD ALIGN=RIGHT VALIGN=TOP>");
      out.println("<B>");
      if (!label.trim().endsWith(":")) {
         label = label.concat(":");
         if (VERBOSE) {
            System.out.println("printTextArea : label = " + label);
         }
      }
      out.println(label);
      out.println("</B>");
      out.println("</TD>");
      out.println("<TD COLSPAN=" + String.valueOf(colspanValue - 1) + ">");
      out.println("<FORM>");
      out.println("<TEXTAREA rows=3 cols=70 readonly=\"true\">");
      if (text == null) {
         text = "";
      }
      out.println(HTMLEncoder.encodeForHTMLContent(text));
      out.println("</TEXTAREA>");
      out.println("</FORM>");
      out.println("</TD>");
      out.println("</TR>");
      out.flush();
   }

   /**
    *  This is a service to get a localized string from a resource bundle. The
    *  resource bundle to be used is passed in as an argument, as is the locale.
    *  Additional arguments are a resource token and default text. This method
    *  uses reflection to see if the resource token is a constant name in either
    *  the locale specific or root resource bundle, and if so, uses the constant
    *  value as the resource bundle key. Otherwise, the resource token is expected
    *  to be the resource bundle key. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  resourceToken   the name of a constant whose value is a resource
    *      bundle key, or a string that is the actual resource bundle key
    *@param  locale          the locale used in determining what text to display
    *@param  resourceBundle  the resourceBundle to use to find the desired,
    *      localized string
    *@param  defaultText     the default text to display
    *@return                 string with text to display
    */
   public static synchronized String getResourceString(String resourceToken, Locale locale, String resourceBundle, String defaultText) {
      String text = defaultText;
      ResourceBundle htmlMessagesResource = null;
      ResourceBundle non_local_htmlMessagesResource = null;
      try {
         htmlMessagesResource = ResourceBundle.getBundle(resourceBundle, locale);
         /*
          *  NOTE: The following code is a workaround !
          *
          *  The Windchill root resource bundles contain English/US locale definitions
          *  and typically have constant definitions for the resource bundle keys.
          *  ResourceBundle.getBundle does not provide a way to request the root resource
          *  bundle.  The root resource bundle is only returned if there is no resource
          *  bundle for the desired or default locale.  This code sets the default locale
          *  to English/US to force the root resource bundle to be returned, and then
          *  restores the default locale to its original value.
          *
          *  This code should be reevaluated when Windchill supplies locale specific
          *  resource bundles for English/US.
          */
         non_local_htmlMessagesResource = ResourceBundle.getBundle(resourceBundle, new Locale("", ""));
      } catch (java.util.MissingResourceException mre) {
         if (VERBOSE) {
            mre.printStackTrace();
         }
         htmlMessagesResource =
               ResourceBundle.getBundle(resourceBundle);
      }
      if (htmlMessagesResource == null) {
         if (VERBOSE) {
            System.err.println("\n\nGetting of resource failed!!!\n\n");
         }

         return defaultText;
      }
      try {
         try {
            // See if resourceToken is a constant defined in the locale specific resource bundle

            Class resourceClass = htmlMessagesResource.getClass();
            Field nameField = resourceClass.getField(resourceToken);
            text = (String) nameField.get(htmlMessagesResource);
            text = htmlMessagesResource.getString(text);
         } catch (java.lang.NoSuchFieldException nsfe) {
            try {
               // See if resourceToken is a constant defined in the root resource bundle

               Class resourceClass = non_local_htmlMessagesResource.getClass();
               Field nameField = resourceClass.getField(resourceToken);
               text = (String) nameField.get(htmlMessagesResource);
               text = htmlMessagesResource.getString(text);
               if (VERBOSE) {
                  nsfe.printStackTrace();
               }
            } catch (java.lang.NoSuchFieldException nsfe2) {
               // Assume resourceToken is a key in the locale specific resource bundle

               text = htmlMessagesResource.getString(resourceToken);
            } catch (java.lang.IllegalAccessException iae2) {
               // Assume resourceToken is a key in the locale specific resource bundle

               text = htmlMessagesResource.getString(resourceToken);
            }
         } catch (java.lang.SecurityException se) {
            text = htmlMessagesResource.getString(resourceToken);
            if (VERBOSE) {
               se.printStackTrace();
            }
         } catch (java.lang.IllegalArgumentException iae) {
            text = htmlMessagesResource.getString(resourceToken);
            if (VERBOSE) {
               iae.printStackTrace();
            }
         } catch (java.lang.IllegalAccessException iae) {
            text = htmlMessagesResource.getString(resourceToken);
             {
               iae.printStackTrace();
            }
         } finally {
            return text;
         }
      } catch (java.util.MissingResourceException mre) {
         if (VERBOSE) {
            mre.printStackTrace();
         }
         text = defaultText;

         return text;
      }
   }

   /**
    *  Returns the CONTEXT_SERVICE_NAME for the ActionDelegate specified by the
    *  given delegate parameter. The given parameter is a String representation of
    *  the delegate class whose CONTEXT_SERVICE_NAME is to be returned.
    *
    *@param  action_delegate  a String representation of the ActionDelegate class
    *      whose CONTEXT_SERVICE_NAME is to be returned
    *@return                  the CONTEXT_SERVICE_NAME of the given delegate. If
    *      no delegate is found corresponding to the given String class, null is
    *      returned.
    */
   public static String getContextServiceName(String action_delegate) {
      String context_service_name = null;
      if (action_delegate != null) {
         Class delegate = null;
         try {
            delegate = Class.forName(action_delegate);

            if (delegate != null) {
               ActionDelegate delegate_instance = (ActionDelegate) delegate.newInstance();

               if (delegate_instance != null) {
                  context_service_name = delegate_instance.getContextServiceName();
               }
            }
         } catch (ClassNotFoundException cnfe) {
            if (VERBOSE) {
               System.out.println("getContextServiceName() - class " + action_delegate +
                     " not found: ");
               cnfe.printStackTrace();
            }
         } catch (InstantiationException ie) {
            if (VERBOSE) {
               System.out.println("getContextServiceName() - an exception occured " +
                     "instantiating: " + delegate.getName());
               ie.printStackTrace();
            }
         } catch (Exception e) {
            if (VERBOSE) {
               System.out.println("getContextServiceName() - an exception occured " +
                     "instantiating: " + delegate.getName());
               e.printStackTrace();
            }
         }
      }

      return context_service_name;
   }

   /**
    *  Returns a boolean value indicating whether or not the two given WTObjects
    *  are equal. If the given WTObjects are both persistent, this method will
    *  compare for equality using the WTReferences of the object.
    *
    *@param  object_a         one object to be compared
    *@param  object_b         one object to be compared
    *@return                  true, if the two given objects are equal
    *@exception  WTException  Description of the Exception
    */
   public static boolean isEqual(WTObject object_a, WTObject object_b) throws WTException {
      boolean equals = false;
      if ((PersistenceHelper.isPersistent(object_a)) &&
            (PersistenceHelper.isPersistent(object_b))) {
         ReferenceFactory delegate_factory = new ReferenceFactory();
         WTReference reference_a = delegate_factory.getReference(object_a);
         WTReference reference_b = delegate_factory.getReference(object_b);
         equals = reference_a.equals(reference_b);
      } else {
         equals = object_a == object_b;
      }
      return equals;
   }

   /**
    *  Description of the Method
    *
    *@param  parameters  Description of the Parameter
    *@param  locale      Description of the Parameter
    *@param  os          Description of the Parameter
    */
   public void displayFolderTrail(Properties parameters, Locale locale, OutputStream os) {
      if (VERBOSE) {
         System.out.println("\n=-=-=-=-=-=-=-=-= displayFolderTrail -=-=-=-=-=-=-=-=-=-=");
      }

      PrintWriter out = getPrintWriter(os, locale);

      // We only do this if the context object is a Folder object
      if (getContextObj() instanceof Folder) {
         String row = "";
         String table = "";

         Vector hierarchy = new Vector();
         Folder current = (Folder) getContextObj();

         try {
            while (current != null) {
               hierarchy.addElement(current);
               if (current instanceof Cabinet) {
                  current = null;
               } else {
                  current = FolderHelper.service.getFolder((Foldered) current);
               }
            }
         } catch (WTException wte) {
//          out.close();
//          let the caller close
            return;
         }

         String you_are_here = WTMessage.getLocalizedMessage(RESOURCE, enterpriseResource.YOU_ARE_HERE,
               null, locale);
         String separator = WTMessage.getLocalizedMessage(RESOURCE, enterpriseResource.CONTEXT_PATH_SEPARATOR,
               null, locale);

         int size = hierarchy.size();
         String root = WTMessage.getLocalizedMessage(RESOURCE, enterpriseResource.ROOT_CABINET, null, locale);
         String path = you_are_here + BLANK_SPACE + BLANK_SPACE +
               getAllCabinetsURL(root, null, locale);
         for (int i = size; i > 0; i--) {
            current = (Folder) hierarchy.elementAt(i - 1);
            String link = "<FONT COLOR=NAVY>" + current.getName() + "</FONT>";
            try {
               if (!isEqual((WTObject) current, (WTObject) getContextObj())) {
                  link = objectActionLinkAux(current, "ObjProps", current.getName(), locale);
               }
            } catch (Exception e) {
            }

            path += BLANK_SPACE + BLANK_SPACE + separator + BLANK_SPACE + BLANK_SPACE + link;
         }

         out.println(path);
         out.flush();
      }
   }

   /**
    *  Returns the personal cabinet corresponding to the given WTPrincipal. If the
    *  given WTPrincipal is null, the current session principal is used.
    *
    *@param  principal                    the WTPrincipal whose personal cabinet
    *      is returned
    *@return                              the personal cabinet of the given
    *      WTPrincipal.
    *@exception  FolderNotFoundException  Description of the Exception
    *@exception  WTException              Description of the Exception
    */
   protected static Cabinet getPersonalCabinet(WTPrincipal principal)
          throws FolderNotFoundException, WTException {
      Cabinet cabinet = null;

      if (principal == null) {
         principal = SessionHelper.manager.getPrincipal();
      }

      if (principal != null) {
         cabinet = FolderHelper.service.getPersonalCabinet(principal);
      }

      return cabinet;
   }

   /**
    *  Gets the referenceString attribute of the BasicTemplateProcessor class
    *
    *@param  obj              Description of the Parameter
    *@return                  The referenceString value
    *@exception  WTException  Description of the Exception
    */
   public static String getReferenceString(WTObject obj) throws WTException {
      ReferenceFactory rf = new ReferenceFactory();
      WTReference ref = rf.getReference(obj);
      if (VERBOSE) {
         System.out.println("getReferenceString: WTReference is " + ref);
      }
      return (rf.getReferenceString(ref));
   }

   /**
    *  Gets the referenceString attribute of the BasicTemplateProcessor object
    *
    *@param  parameters       Description of the Parameter
    *@param  locale           Description of the Parameter
    *@param  os               Description of the Parameter
    *@exception  WTException  Description of the Exception
    */
   public void getReferenceString(Properties parameters, Locale locale,
         OutputStream os) throws WTException {
      Object obj = getContextObj();
      String obj_reference = "";
      if (obj instanceof WTObject) {
         obj_reference = getReferenceString((WTObject) obj);
      }

      PrintWriter print_writer = getPrintWriter(os, locale);
      print_writer.print(obj_reference);
      print_writer.flush();
   }

   /**
    *  Sorts the given QueryResult using the given CollationKeyFactory. If the
    *  given CollationKeyFactory is null, the IdentityCollationKeyFactory is used.
    *
    *@param  results  the QueryResult to be sorted
    *@param  key      the CollationKeyFactory used for sorting
    *@return          the sorted SortedEnumeration containing the QueryResult
    */
   public static SortedEnumeration sortQueryResult(QueryResult results, CollationKeyFactory key) {
      SortedEnumeration sorted_enum = null;
      if (results != null) {
         if (key == null) {
            key = new IdentityCollationKeyFactory();
         }
         sorted_enum = new SortedEnumeration(results.getEnumeration(), key);
      }

      return sorted_enum;
   }

   /**
    *  This call will generate the URL of a Help page based on the current Context
    *  Object or Context Class Name and the HelpContext String that is passed in.
    *  The Locale of the HTML Help page will be determined by the the locale that
    *  is passed in as the second argument.
    *
    *@param  helpContext      The String to be used as the Context String in
    *      properties file entry
    *@param  locale           The current Locale of the browser
    *@return                  The URL of the correct Help Page
    *@exception  WTException  Description of the Exception
    */
   public String getHTMLHelpURL(String helpContext, Locale locale) throws WTException {
      wt.templateutil.processor.HelpHTMLTemplateFactory templateFactory = new wt.templateutil.processor.HelpHTMLTemplateFactory();
      templateFactory.setLocale(locale);
      templateFactory.setContextAction(helpContext);

      if (getContextObj() != null) {
         templateFactory.setContextObj(getContextObj());
      } else if (getContextClassName() != null) {
         templateFactory.setContextClassName(getContextClassName());
      }
      String templateName = templateFactory.getHTMLHelpPagePath();

      String urlString = null;
      try {
         urlString = HelpLinkHelper.createHelpHREF(templateName);
         if (VERBOSE) {
            System.out.println("getHTMLHelpURL : urlString = " + urlString);
         }
      } catch (Exception e) {
         throw new WTException(e);
      }

      return urlString;
   }

   /**
    *  Prints out a String with the HTML for a link to a context dependent Help
    *  page to the browser. <P>
    *
    *  The correct Windchill script call is <P>
    *
    *  <code>
    * getHTMLHelpURL  HelpContext=&lt;Context to locate Help Page&gt;
    * </code> <P>
    *
    *  The HelpContext parameter is optional. See comments below. <P>
    *
    *  The help page is based on the context of the current HTML page and is found
    *  via an entry in a *.properties file. The correct entry in a properties file
    *  will look like <P>
    *
    *  <code>
    * wt.services/rsc/default/wt.templateutil.processor.HelpHTMLTemplate/&lt;Context Action&gt;/&lt;Context Class&gt;/0=&lt;Path to HTML Help Page&gt;&lt;Anchor&gt;
    * </code> <P>
    *
    *  Where
    *  <UL>
    *    <LI> Context Action - The current value of getContextAction, unless the
    *    HelpContext is passed in the Script call
    *    <LI> Context Class - If getContextObj != null, the class of the
    *    contextObj is used, otherwise the value of getContextClassName is used.
    *    If both return null, the java.lang.Object is used.
    *    <LI> Path to HTML Help Page - The non-localized path to the HTML Help
    *    page relative to codebase. For example, if the HTML Help page was at
    *    /wt/helpfiles/federation/help<B>_en</B> /IEServices.html, then the entry
    *    in the properties file would be, <P>
    *
    *    <code>
    * wt.helpfiles.federation.help.IEServices
    * </code>
    *    <LI> Anchor - Optionally you can specify an anchor in the page, so if the
    *    wanted to get the anchor,VIEWIESERVICE, in the Help page above, then you
    *    would have the following entry in your properties file <code>
    * wt.helpfiles.federation.help.IEServices<B>#VIEWIESERVICE</B> </code>
    *  </UL>
    *  The current list of options for configuring the Help link, via name/value
    *  pairs in the parameters argument is
    *  <UL>
    *    <LI> HelpContext - Replaces the Context Action that is used to locate the
    *    Help Page
    *
     *  <BR><BR><B>Supported API: </B>true
     *
     * @deprecated
    *
    *@param  parameters       The name/value pairs passed in from the Windchill
    *      Script Call
    *@param  locale           The current Locale of the browser
    *@param  os               The OutputStream to the Browser
    *@exception  WTException  Description of the Exception
    */
   public void getHTMLHelpURL(Properties parameters, Locale locale, OutputStream os) throws WTException {
      PrintWriter out = getPrintWriter(os, locale);
      String helpContext = parameters.getProperty(HELP_CONTEXT);

      if (helpContext == null) {
         helpContext = getContextAction();
      }

      try {
         out.print(HTMLEncoder.encodeURIForHTMLAttribute(getHTMLHelpURL(helpContext, locale)));
      } catch (WTException wte) {
         if (VERBOSE) {
            wte.printStackTrace();
            wte.printStackTrace(out);
         } else {
            out.print("--- Link Did Not Generate Correctly ---");
         }
      }
      out.flush();
   }

   /**
    *  Prints the HTML with a link to a context dependent Help page. The help page
    *  is based on the context of the current HTML page, namely the contextAction
    *  and the contextObj or contextClassName(if the contextObj == null) <P>
    *
    *  The correct Windchill script call is <P>
    *
    *  <code>
    * addHTMLHelpLink  &lt;name/value pairs indicating customized options&gt;
    * </code> <P>
    *
    *  Please the javadoc for the method, getHTMLHelpLink, for listing and
    *  description of the name/value pairs that specify the options for presenting
    *  an HTML help link. <P>
    *
    *  Please the javadoc for the method, getHTMLHelpLink, for description of how
    *  register an HTML Help page for the page that the help link is to appear.
    *
     *  <BR><BR><B>Supported API: </B>true
     *
     * @deprecated
     *
    *@param  parameters
    *@param  locale
    *@param  os
    *@exception  WTException
    */
   public void addHTMLHelpLink(Properties parameters, Locale locale, OutputStream os) throws WTException {
      PrintWriter out = getPrintWriter(os, locale);
      try {
         out.print(getHTMLHelpLink(parameters, locale, os));
      } catch (WTException wte)
      {
         if (VERBOSE) {
            wte.printStackTrace();
         }
         String helpContext = null;
         if ( parameters != null ) helpContext = parameters.getProperty(HELP_CONTEXT);
         if ( helpContext == null && getContextAction( ) != null ) helpContext = getContextAction() ;
         if ( helpContext != null )
         {
            System.out.println(WTMessage.getLocalizedMessage(RESOURCE,enterpriseResource.UNABLE_TO_CREATE_HELPLINK,new Object[] { helpContext }, locale ) );
         }
      }
      out.flush();
   }

   /**
    *  Returns a String with the HTML for a link to a context dependent Help page.
    *  The help page is based on the context of the current HTML page and is found
    *  via an entry in a *.properties file. The correct entry in a properties file
    *  will look like <P>
    *
    *  <code>
    * wt.services/rsc/default/wt.templateutil.processor.HelpHTMLTemplate/&lt;Context Action&gt;/&lt;Context Class&gt;/0=&lt;Path to HTML Help Page&gt;&lt;Anchor&gt;
    * </code> <P>
    *
    *  Where
    *  <UL>
    *    <LI> Context Action - The current value of getContextAction
    *    <LI> Context Class - If getContextObj != null, the class of the
    *    contextObj is used, otherwise the value of getContextClassName is used.
    *    If both return null, the java.lang.Object is used.
    *    <LI> Path to HTML Help Page - The non-localized path to the HTML Help
    *    page relative to codebase. For example, if the HTML Help page was at
    *    /wt/helpfiles/federation/help<B>_en</B> /IEServices.html, then the entry
    *    in the properties file would be, <P>
    *
    *    <code>
    * wt.helpfiles.federation.help.IEServices
    * </code>
    *    <LI> Anchor - Optionally you can specify an anchor in the page, so if the
    *    wanted to get the anchor,VIEWIESERVICE, in the Help page above, then you
    *    would have the following entry in your properties file <code>
    * wt.helpfiles.federation.help.IEServices<B>#VIEWIESERVICE</B> </code>
    *  </UL>
    *  <P>
    *
    *  <P>
    *
    *  The current list of options for configuring the Help link, via name/value
    *  pairs in the parameters argument is
    *  <UL>
    *    <LI> HelpContext - Replaces the Context Action that is used to locate the
    *    Help Page
    *    <LI> PresentHelpLabel - true/false option that allows turning on/off the
    *    presentation of a text label. Default is on
    *    <LI> HelpLabelResource - The resource bundle that is used to locate the
    *    Localized text label
    *    <LI> HelpLabel - The resource key that is used to locate the Localized
    *    text label
    *    <LI> ADD_HELP_ICON - true/false option that allows turning on/off the
    *    presentation of an Icon with theHelp Link
    *    <LI> HELP_ICON_SELECTOR - Replaces the Context Action that is used to
    *    locate the Icon from service.properties
    *    <LI> HELP_ICON_POSITION - LEFT/RIGHT option allows have the Icon appear
    *    to Right of the text label. The default positioning of the Help Icon is
    *    to the left of the text label
    *  </UL>
    *  <P>
    *
    *
    *
    *@param  parameters
    *@param  locale
    *@param  os
    *@return                  The hTMLHelpLink value
    *@exception  WTException
    */
   public String getHTMLHelpLink(Properties parameters, Locale locale, OutputStream os) throws WTException {
      if (parameters == null) {
         parameters = new Properties();
      }
      String helpContext = parameters.getProperty(HELP_CONTEXT);

      if (helpContext == null) {
         helpContext = getContextAction();
      }

      if (VERBOSE) {
         System.out.println("addHTMLHelpLink : helpContext = " + helpContext);
      }

      String helpLabelEnabledStr = parameters.getProperty(PRESENT_HELP_LABEL, String.valueOf(true));

      if (VERBOSE) {
         System.err.println("getHTMLHelpLink : helpLabelEnabledStr = " + helpLabelEnabledStr);
      }

      boolean helpLabelEnabled = helpLabelEnabledStr.equalsIgnoreCase("TRUE");

      if (VERBOSE) {
         System.err.println("getHTMLHelpLink : helpLabelEnabled = " + helpLabelEnabled);
      }

      String label = null;

      if (helpLabelEnabled) {
         String helpLabel = parameters.getProperty(HELP_LABEL);
         if (helpLabel == null) {
            helpLabel = DEFAULT_HELP_LABEL;
         }

         if (VERBOSE) {
            System.out.println("addHTMLHelpLink : helpLabel = " + helpLabel);
         }

         String resourceBundle = parameters.getProperty(HELP_LABEL_RESOURCE);
         if (resourceBundle == null) {
            resourceBundle = "wt.enterprise.enterpriseResource";
         }

         label = getResourceString(helpLabel, locale, resourceBundle, "-- Exception getting Label --");
         //getResourceString(helpLabel, resourceBundle, locale, "-- Exception getting Label --");
      }

      if (VERBOSE) {
         System.out.println("addHTMLHelpLink : label = " + label);
      }

      String helpIconEnabledStr = parameters.getProperty(ADD_HELP_ICON, String.valueOf(helpIconEnabledDefault));

      if (VERBOSE) {
         System.err.println("getHTMLHelpLink : helpIconEnabledStr = " + helpIconEnabledStr);
      }

      helpIconEnabled = helpIconEnabledStr.equalsIgnoreCase("TRUE");

      if (VERBOSE) {
         System.err.println("getHTMLHelpLink : helpIconEnabled = " + helpIconEnabled);
      }

      if (helpIconEnabled) {
         String helpIconURL = "";

         String helpIconSelector = parameters.getProperty(HELP_ICON_SELECTOR, DEFAULT_HELP_ICON_SELECTOR);

         ContextBasedLocalizedResourceSrv contextLocalizeResource = new ContextBasedLocalizedResourceSrv();
         contextLocalizeResource.setLocale(locale);

         if (getContextObj() != null) {
            helpIconURL = contextLocalizeResource.getResourceURL(helpIconSelector, getContextObj());
         } else {
            helpIconURL = contextLocalizeResource.getResourceURL(helpIconSelector, getContextClassName());
         }
         helpIconURL = "<IMG BORDER=0 SRC=\"" + helpIconURL + "\"  ALIGN=ABSMIDDLE  ></IMG>";
         if (label == null) {
            label = helpIconURL;
         } else {
            String labelAlignment = parameters.getProperty(HELP_ICON_POSITION);
            if (labelAlignment != null && labelAlignment.toUpperCase().equals("RIGHT")) {
               label = label + helpIconURL;
            } else {
               label = helpIconURL + label;
            }
         }
      }

      if (VERBOSE) {
         System.out.println("addHTMLHelpLink : label = " + label);
      }

      String urlString = getHTMLHelpURL(helpContext, locale);

      if (VERBOSE) {
         System.out.println("addHTMLHelpLink : urlString = " + urlString);
      }

      String linkParameters = "TARGET=\"" + HELP_WINDOW_NAME + "\"";
      if (parameters.getProperty("LINK_TEXT_COLOR") != null) {
         String linkTextColor = parameters.getProperty("LINK_TEXT_COLOR");
         if (linkTextColor.startsWith("\"")) {
            linkTextColor = linkTextColor.substring(1);
         }
         if (linkTextColor.endsWith("\"")) {
            linkTextColor = linkTextColor.substring(0, linkTextColor.length() - 1);
         }
         linkParameters += " STYLE=\"color:" + linkTextColor + "; font-size:10pt;\"";
      }
      urlString = HtmlUtil.createLink(urlString, linkParameters, label);
      return urlString;
   }

   /**
    *  Gets the resourceString attribute of the BasicTemplateProcessor object
    *
     *  <BR><BR><B>Supported API: </B>true
     *
     * @deprecated
    *
    *@param  parameters       Description of the Parameter
    *@param  locale           Description of the Parameter
    *@param  os               Description of the Parameter
    *@exception  WTException  Description of the Exception
    */
   public void getResourceString(Properties parameters, Locale locale, OutputStream os) throws WTException {
      String TEXT_TOKEN = "textToken";
      String ESCAPED = "escaped";
      String textToken = parameters.getProperty(TEXT_TOKEN);

      if (textToken == null) {
         throw new WTException("getResourceString : textToken not specified in HTML Template");
      }

      String resourceBundle = parameters.getProperty(TEXT_RESOURCE_BUNDLE);

      if (resourceBundle == null) {
         resourceBundle = RESOURCE;
      }

      ESCAPED = parameters.getProperty(ESCAPED);
      String text = getResourceString(textToken, locale, resourceBundle, "Exception getting label");
      if (ESCAPED != null && ESCAPED.equalsIgnoreCase("true") && text.length() > 0) {
         StringBuffer messageStrings = new StringBuffer(text);
         if (messageStrings.length() > 0) {
            // Replace line feeds and carriage returns with a magic string so we can reconstruct
            // them on client. Allow only single spacing of lines.
            int i = 0;
            int length = messageStrings.length();
            while (i < length) {
               if (messageStrings.charAt(i) == '\n' || messageStrings.charAt(i) == '\r') {
                  messageStrings = messageStrings.replace(i, i + 1, LINE_FEED_DELIMITER);
                  i += 3;
                  while (messageStrings.charAt(i) == '\n' || messageStrings.charAt(i) == '\r') {
                     messageStrings.delete(i, i + 1);
                  }
               } else if (messageStrings.charAt(i) == '\"') {
                  messageStrings = messageStrings.replace(i, i + 1, "\\\"");
                  i += 2;
               } else if (messageStrings.charAt(i) == '\'') {
                  messageStrings = messageStrings.replace(i, i + 1, "\\\'");
                  i += 2;
               } else {
                  i++;
               }
               length = messageStrings.length();
            }
         }

         text = messageStrings.toString();
      }

      PrintWriter out = getPrintWriter(os, locale);
      out.print(text);
      out.flush();
   }

   /**
    *  Sets the helpContext attribute of the BasicTemplateProcessor object
    *
    *@param  a_HelpContext  The new helpContext value
    */
   public void setHelpContext(String a_HelpContext) {
      helpContext = a_HelpContext;
   }

   /**
    *  Gets the helpContext attribute of the BasicTemplateProcessor object
    *
    *@return    The helpContext value
    */
   public String getHelpContext() {
      return helpContext;
   }

   /**
    *  Sets the contextClass attribute of the BasicTemplateProcessor object
    *
    *@param  a_contextClass  The new contextClass value
    */
   public void setContextClass(String a_contextClass) {
      contextClass = a_contextClass;
   }

   /**
    *  Gets the contextClass attribute of the BasicTemplateProcessor object
    *
    *@return    The contextClass value
    */
   public String getContextClass() {
      return contextClass;
   }

   // implement methods for the ContextTranslator interface

   /**
    *  Description of the Method
    *
    *@param  request          Description of the Parameter
    *@exception  WTException  Description of the Exception
    */
    public void readContext(wt.httpgw.HTTPRequest request)
          throws WTException {
      /*
       *  boolean paramsOK = true;
       *  String badParamsMsg = null;
       *  String pOID = null;
       *  java.util.Enumeration enumeration = props.keys();
       *  String keyName = null;
       *  String keyNameUpperCase = null;
       *  while ( enumeration.hasMoreElements() )
       *  {
       *  keyName = (String)enumeration.nextElement();
       *  keyNameUpperCase = keyName.toUpperCase();
       *  props.put( keyNameUpperCase , props.getProperty(keyName) );
       *  }
       *  pOID = props.getProperty( OID );
       *  contextHolder.setContextAction( props.getProperty( ACTION ) );
       *  if (VERBOSE) {
       *  System.out.println(" OID is: " + pOID);
       *  }
       *  if ( pOID == null || pOID.equals( "" ) )
       *  {
       *  if ( props.getProperty( CLASS ) == null || props.getProperty( CLASS ).equals( "" ))
       *  {
       *  contextHolder.setContextObj( new Object() );
       *  }
       *  else
       *  {
       *  try
       *  {
       *  Class testClass = Class.forName( props.getProperty( CLASS ) );
       *  }
       *  catch ( java.lang.ClassNotFoundException cnfe )
       *  {
       *  if (VERBOSE) cnfe.printStackTrace();
       *  throw new WTException( cnfe );
       *  }
       *  contextHolder.setContextClassName( props.getProperty( CLASS ) );
       *  }
       *  }
       *  else
       *  {
       *  ReferenceFactory rf = new ReferenceFactory();
       *  WTReference ref = rf.getReference(pOID);
       *  Object contextObject = ref.getObject();
       *  setContextObj(contextObject);
       *  contextHolder.setContextObj(contextObject);
       *  }
       *  return ;
       */
   }

   /**
    *  Description of the Method
    *
    *@param  parameters  Description of the Parameter
    *@param  locale      Description of the Parameter
    *@param  os          Description of the Parameter
    */
   public void writeHiddenContext(Properties parameters, Locale locale, OutputStream os) {
      java.io.PrintWriter out = getPrintWriter(os, locale);

      if (VERBOSE) {
         System.err.println("DefaultTemplateProcessor : Entering writeHiddenContext");
      }

      String[] outList = getContextListOut();
      if (outList == null) {
         if (VERBOSE) {
            System.err.println("DefaultTemplateProcessor : outList == null");
         }
         return;
      }
      for (int i = 0; i < outList.length; i++) {
         if (VERBOSE) {
            System.err.println("DefaultTemplateProcessor : outList[" + i + "] = " + outList[i]);
         }
         if (outList[i].equals(OID)) {
            if (getContextObj() instanceof wt.fc.Persistable) {
               try {
                  wt.fc.ReferenceFactory referenceFactory = new wt.fc.ReferenceFactory();
                  String oid = referenceFactory.getReferenceString((wt.fc.Persistable) getContextObj());
                  out.println(hiddenContextString(outList[i].toLowerCase(), oid));
               } catch (wt.util.WTException wte) {
                  if (VERBOSE) {
                     wte.printStackTrace();
                  }
               }
            } else if (getContextObj() instanceof Object) {
               out.println(hiddenContextString(outList[i].toLowerCase(), ""));
            }
         } else if (outList[i].equals(ACTION)) {
            out.println(hiddenContextString(outList[i].toLowerCase(), getContextAction()));
         } else if (outList[i].equals(CLASS)) {
            out.println(hiddenContextString(outList[i].toLowerCase(), getContextClassName()));
         } else {
            out.println(hiddenContextString(outList[i].toLowerCase(), getContextProperties().getProperty(outList[i])));
         }
      }
      out.flush();
   }

   /**
    *  Description of the Method
    *
    *@return    Description of the Return Value
    */
   public String writeQueryStringContext() {
      String nameValuePairs = "";
      String namevalueDivider = "&";
      String[] outList = getContextListOut();
      for (int i = 0; i < outList.length; i++) {
         if (outList[i].equals(OID)) {
            if (getContextObj() == null) {
               nameValuePairs += outList[i].toLowerCase() + namevalueDivider + "";
            } else if (getContextObj() instanceof wt.fc.Persistable) {
               try {
                  wt.fc.ReferenceFactory referenceFactory = new wt.fc.ReferenceFactory();
                  String oid = referenceFactory.getReferenceString((wt.fc.Persistable) getContextObj());
                  nameValuePairs += outList[i].toLowerCase() + namevalueDivider + oid;
               } catch (wt.util.WTException wte) {
                  if (VERBOSE) {
                     wte.printStackTrace();
                  }
               }
            } else {
               nameValuePairs += outList[i].toLowerCase() + namevalueDivider + "";
            }
         } else if (outList[i].equals(ACTION)) {
            nameValuePairs += outList[i].toLowerCase() + namevalueDivider + getContextAction();
         } else if (outList[i].equals(CLASS)) {
            nameValuePairs += outList[i].toLowerCase() + namevalueDivider + getContextClassName();
         } else {
            nameValuePairs += outList[i].toLowerCase() + namevalueDivider + getContextProperties().getProperty(outList[i]);
         }
      }
      return nameValuePairs;
   }

   /**
    *  Description of the Method
    *
    *@param  name   Description of the Parameter
    *@param  value  Description of the Parameter
    *@return        Description of the Return Value
    */
   public String hiddenContextString(String name, String value) {
      String hiddenStart = "Type=hidden ";
      return wt.htmlutil.HtmlUtil.addFormInput(
            hiddenStart + "NAME=\"" + name + "\" VALUE=\"" + value + "\"", "");
   }

   /**
    *  Adds a feature to the HiddenProperty attribute of the
    *  BasicTemplateProcessor object
    *
    *@param  parameters       The feature to be added to the HiddenProperty
    *      attribute
    *@param  locale           The feature to be added to the HiddenProperty
    *      attribute
    *@param  os               The feature to be added to the HiddenProperty
    *      attribute
    *@exception  WTException  Description of the Exception
    */
   public void addHiddenProperty(Properties parameters, java.util.Locale locale, OutputStream os) throws WTException {
      java.io.PrintWriter out = getPrintWriter(os, locale);
      String name = parameters.getProperty("name");
      out.println(hiddenContextString(name, getContextProperties().getProperty(name.toUpperCase())));
   }

   /**
    *  Description of the Method
    *
    *@param  parameters  Description of the Parameter
    *@param  locale      Description of the Parameter
    *@param  os          Description of the Parameter
    */
   public void displayAttributesTable(Properties parameters, Locale locale, OutputStream os) {
      boolean dbg = false;
      PrintWriter out = getPrintWriter(os, locale);
      Object theContextObject = getContextObj();
      IBAHolder ibah = null;
      String mode = null;
      DefaultAttributeContainer dac = null;
      AbstractValueView[] avv = null;
      Vector avvVec = null;
      Vector refVals = null;
      Vector attVals = null;
      Vector indVals = new Vector();
      String displayFormTag = "true";
      String tableAlignment = "CENTER";
      String tableWidth = "90%";
      String tableHeaderBackgroundColor;
      String tableHeaderFontColor;
      String tableRowBackgroundColor;
      String tableRowFontColor;
      String tableFontFace;
      final String tableAlignmentKey = "attributes_table_alignment";
      final String tableWidthKey = "attributes_table_width";
      long rowspan = 0;

      uniqueAttributeKeyValues = new Hashtable();

      if (dbg == true) {
         System.out.println("^$^ DEBUG ^$^");
      }

      ResourceBundle messagesResource = ResourceBundle.getBundle(RESOURCE, locale);

      if (parameters != null) {
         mode = parameters.getProperty("mode");
         tableAlignment = parameters.getProperty(tableAlignmentKey, tableAlignment);
         tableWidth = parameters.getProperty(tableWidthKey, tableWidth);
      } else {
         parameters = new Properties();

         parameters.put("mode", "Simple");
         mode = parameters.getProperty("mode");

         parameters.put(tableAlignmentKey, tableAlignment);
         tableAlignment = parameters.getProperty(tableAlignmentKey);

         parameters.put(tableWidthKey, tableWidth);
         tableWidth = parameters.getProperty(tableWidthKey);
      }

      try {
         if (!(theContextObject instanceof IBAHolder)) {
            System.out.println("theContextObject = " + theContextObject + " is not an IBAHolder.");
            return;
         }

         ibah = (IBAHolder) theContextObject;
         ibah = (IBAHolder) IBAValueHelper.service.refreshAttributeContainer(ibah, null, locale, null);
         dac = (DefaultAttributeContainer) ibah.getAttributeContainer();

         if (dac != null) {
            int j = 0;
            avv = dac.getAttributeValues();
            avvVec = new Vector();

            for (int i = 0; i < avv.length; i++) {
               avvVec.addElement(avv[i]);
            }

            Enumeration e = avvVec.elements();
            AbstractValueViewCollationKeyFactory avvckf = new AbstractValueViewCollationKeyFactory(locale);
            SortedEnumeration se = new SortedEnumeration(e, avvckf);
            avv = new AbstractValueView[se.size()];

            while (se.hasMoreElements()) {
               avv[j] = (AbstractValueView) se.nextElement();
               j++;
            }
         }
      } catch (wt.access.NotAuthorizedException nae) {
         nae.printStackTrace();
      } catch (RemoteException re) {
         re.printStackTrace();
      } catch (WTException wte) {
         wte.printStackTrace();
      }

      tableHeaderBackgroundColor = " BGCOLOR=\"" + getWCColor("t1-bg-col-head") + "\"";
      tableHeaderFontColor = " COLOR=\"" + getWCColor("t1-f-col-head") + "\"";
      tableRowBackgroundColor = " BGCOLOR=\"" + getWCColor("t1-bg-evenrow") + "\"";
      tableRowFontColor = " COLOR=\"" + getWCColor("t1-f-cell") + "\"";
      tableFontFace = " FACE=\"" + getWCFontFamily() + "\"";

      if (mode.equals("Simple")) {
         String currentMeasurementSystem = DefaultUnitRenderer.getDisplayUnitsPref();
         // Display dropdown for desired measurement system
         try {
            if (avv != null && avv.length != 0) {
               displayMeasurementSystemsDropDown(currentMeasurementSystem,
                     parameters.getProperty("displayMeasurementSysFormTag", "true"), tableAlignment, tableWidth, out, locale);
            }
         } catch (Exception e) {
            if (VERBOSE) {
               System.err.println("Error in displaying measurement system dropdown menu");
            }
            e.printStackTrace();
         }
         out.println("<BR><TABLE BORDER=\"0\" CELLPADDING=\"2\" ALIGN=\"" + tableAlignment + "\" WIDTH=\"" + tableWidth + "\"> \n");

         if (avv != null && avv.length != 0) {
            out.println("<THEAD>\n");
            out.println("<TR " + tableHeaderBackgroundColor + ">\n");
            out.println("<TH ALIGN=\"LEFT\">\n");
            out.println("<FONT " + tableHeaderFontColor + tableFontFace + " SIZE=\"3\">\n");
            out.println(messagesResource.getString(enterpriseResource.ATTRIBUTE_NAME_LABEL) + "\n");
            out.println("</FONT>\n");
            out.println("</TH>\n");
            out.println("<TH ALIGN=\"LEFT\">\n");
            out.println("<FONT " + tableHeaderFontColor + tableFontFace + " SIZE=\"3\">\n");
            out.println((messagesResource.getString(enterpriseResource.ATTRIBUTE_VALUE_LABEL)) + "\n");
            out.println("</FONT>\n");
            out.println("</TH>\n");
            out.println("<TH ALIGN=\"LEFT\">\n");
            out.println("<FONT " + tableHeaderFontColor + tableFontFace + " SIZE=\"3\">\n");
            out.println(messagesResource.getString(enterpriseResource.ATTRIBUTE_DEPENDENCY_LABEL) + "\n");
            out.println("</FONT>\n");
            out.println("</TH>\n");
            out.println("</TR>\n");
            out.println("</THEAD>\n");

            for (int i = 0; i < avv.length; i++) {
                // To ensure only unique key-value pairs are displayed.
                if(isValueExist(avv[i]))
                    continue;

               out.println("<TR " + tableRowBackgroundColor + ">\n");

               //  Display the attribute definition name
               out.println("<TD VALIGN=\"CENTER\">");
               out.println(avv[i].getDefinition().getDisplayName());
               out.println("</TD>\n");

               //  Diplay the attribute value
               out.println("<TD VALIGN=\"CENTER\">");
               DefaultUnitRenderer unitRenderer = new DefaultUnitRenderer();
               if (!(avv[i] instanceof ReferenceValueDefaultView)) {
                  try {
                     if (avv[i] instanceof URLValueDefaultView) {
                        URLValueDefaultView urlv = (URLValueDefaultView) avv[i];
                        out.println("<A HREF=\"" + HTMLEncoder.encodeURIForHTMLAttribute(urlv.getValue()) + "\">" + HTMLEncoder.encodeForHTMLContent(urlv.getDescription()) + "</A>");
                     } else if (avv[i] instanceof UnitValueDefaultView) {
                        UnitValueDefaultView unitValue = (UnitValueDefaultView) avv[i];
                  try {
                     unitRenderer.setDisplayDigits(unitValue.getPrecision());
                  }
                  catch( Exception e ) { }
                        out.println(unitRenderer.render(unitValue.toUnit(), unitValue.getUnitDisplayInfo(currentMeasurementSystem)));
                     } else {
                                                String val = IBAValueUtility.getLocalizedIBAValueDisplayString(avv[i], locale);
                                                if (avv[i] instanceof StringValueDefaultView)
                                                   out.println(HTMLEncoder.encodeAndFormatForHTMLContent(val, false));
                                                else
                                 out.println(val);
                     }
                  } catch (WTException e) {
                     e.printStackTrace();
                  }
               } else {
                  //  This is a ReferenceValueDefaultView
                  out.println(HTMLEncoder.encodeForHTMLContent(((ReferenceValueDefaultView) avv[i]).getLiteIBAReferenceable().getIBAReferenceableDisplayString()));
                  out.println("</TD>\n");
                  out.println("<TD VALIGN=\"CENTER\">");
                  out.println("&nbsp;");
               }

               out.println("</TD>\n");

               //  Display the attribute's dependency value if any
               if (!(avv[i] instanceof ReferenceValueDefaultView)) {
                  if (((AbstractContextualValueDefaultView) avv[i]).getReferenceValueDefaultView() != null) {
                     out.println("<TD VALIGN=\"CENTER\">");
                     out.println(((AbstractContextualValueDefaultView) avv[i]).getReferenceValueDefaultView().getLiteIBAReferenceable().getIBAReferenceableDisplayString());
                     out.println("</TD>\n");
                  } else {
                     out.println("<TD VALIGN=\"CENTER\">");
                     out.println("&nbsp;");
                     out.println("</TD>\n");
                  }
               }

               out.println("</TR>\n");
            }
         } else {
            //  Attribute values array is empty
            //out.println("<TR><TD COLSPAN=\"3\"><CENTER>" + messagesResource.getString(enterpriseResource.NO_ATTRIBUTES_MESSAGE) + "</CENTER></TD></TR>\n");

            if (dbg) {
               System.out.println("Attribute value array is empty.");
            }
         }
      } else if (mode.equals("Nested")) {
         StringBuffer sb = new StringBuffer();

         if (dbg == true) {
            System.out.println("displayAttributesTable mode=Nested");
            System.out.println("Context object - " + theContextObject);
         }

         out.println("<BR><TABLE BORDER=\"0\" CELLPADDING=\"2\" ALIGN=\"" + tableAlignment + "\" WIDTH=\"" + tableWidth + "\">\n");

         if (avv != null && avv.length != 0) {
            refVals = new Vector();
            attVals = new Vector();

            out.println("<THEAD>\n");
            out.println("<TR " + tableHeaderBackgroundColor + ">\n");
            out.println("<TH ALIGN=\"LEFT\" WIDTH=\"30%\">\n");
            out.println("<FONT " + tableHeaderFontColor + tableFontFace + " SIZE=\"3\">\n");
            out.println(messagesResource.getString(enterpriseResource.ATTRIBUTE_NAME_LABEL) + "\n");
            out.println("</FONT>\n");
            out.println("</TH>\n");
            out.println("<TH ALIGN=\"LEFT\" COLSPAN=\"2\" WIDTH=\"30%\">\n");
            out.println("<FONT " + tableHeaderFontColor + tableFontFace + " SIZE=\"3\">\n");
            out.println(messagesResource.getString(enterpriseResource.ATTRIBUTE_VALUE_LABEL) + "\n");
            out.println("</FONT>\n");
            out.println("</TH>\n");
            out.println("</TR>\n");
            out.println("</THEAD>\n");

            for (int i = 0; i < avv.length; i++) {
               if (avv[i] instanceof ReferenceValueDefaultView) {
                  refVals.addElement(avv[i]);
               } else {
                  attVals.addElement(avv[i]);
               }
            }

            if (dbg == true) {
               System.out.println("***** refVals Vector *****");
               for (int i = 0; i < refVals.size(); i++) {
                  System.out.println(((ReferenceValueDefaultView) refVals.elementAt(i)).getDefinition().getDisplayName() + " - " + ((ReferenceValueDefaultView) refVals.elementAt(i)).getLiteIBAReferenceable().getIBAReferenceableDisplayString());
               }
               System.out.println("***** End refVals Vector *****");
               System.out.println("***** attVals Vector *****");
               for (int i = 0; i < attVals.size(); i++) {
                  try {
                     System.out.println(((AbstractValueView) attVals.elementAt(i)).getDefinition().getDisplayName() + " - " + IBAValueUtility.getLocalizedIBAValueDisplayString((AbstractValueView) attVals.elementAt(i), locale));
                  } catch (WTException e) {
                     e.printStackTrace();
                  }
               }
               System.out.println("***** End attVals Vector *****");
            }

            //  Now we have a vector of reference values and a vector of attribute values
            //  loop through the reference values one by one and then check the elements
            //  in the attributes vector to see if they are a dependent value of the current
            //  reference value.

            Vector tempRefVals = new Vector();
            Vector tempAttVals = new Vector();

            do {
               sb = new StringBuffer();
               rowspan = 0;
               tempRefVals.addElement(refVals.firstElement());
               refVals.removeElement(refVals.firstElement());

               Vector temp = (Vector) refVals.clone();

               for (int i = 0; i < temp.size(); i++) {
                  if (((AbstractValueView) temp.elementAt(i)).getDefinition().isPersistedObjectEqual(((AbstractValueView) tempRefVals.firstElement()).getDefinition())) {
                     tempRefVals.addElement(temp.elementAt(i));
                     refVals.removeElement(temp.elementAt(i));
                  }
               }

               //  Done with temp, empty it for reuse.
               temp.removeAllElements();

               //  Now we have a vector of referenceValues with the same definition. For each
               //  element in this vector, we need to loop through the attribute value vector
               //  and see if the value is dependent on the current reference value.

               if (dbg == true) {
                  System.out.println("***** tempRefVals Vector dependencies *****");
               }

               for (int i = 0; i < tempRefVals.size(); i++) {
                  //  Display the reference value
                  if (dbg == true) {
                     System.out.println(((AbstractValueView) tempRefVals.elementAt(i)).getDefinition().getDisplayName() + " - " + ((ReferenceValueDefaultView) tempRefVals.elementAt(i)).getLiteIBAReferenceable().getIBAReferenceableDisplayString());
                  }

                  if (i > 0) {
                     //  Special case where the first element cannot have the <TR> element before
                     //  it because we need to have the attribute definition and rowspan inserted
                     //  before it...all subsequet rows can have this line.
                     sb.append("<TR " + tableRowBackgroundColor + ">\n");
                  }

                  sb.append("<TD " + tableRowBackgroundColor +
                        " COLSPAN=2><FONT" + tableRowFontColor +
                        ">" + ((ReferenceValueDefaultView) tempRefVals.elementAt(i)).getLiteIBAReferenceable().getIBAReferenceableDisplayString() + "</FONT></TD></TR>\n");

                  //  Need to find if this element in tempRefVals has any dependent values in
                  //  attVals.
                  temp = (Vector) attVals.clone();

                  for (int j = 0; j < temp.size(); j++) {
                     if (((AbstractContextualValueDefaultView) temp.elementAt(j)).getReferenceValueDefaultView() != null) {
                        //  Attribute has a reference value, check to see if it is the current tempRefVals value
                        if (((ReferenceValueDefaultView) tempRefVals.elementAt(i)).isPersistedObjectEqual(((AbstractContextualValueDefaultView) temp.elementAt(j)).getReferenceValueDefaultView())) {
                           try {
                              System.out.println("\t" + IBAValueUtility.getLocalizedIBAValueDisplayString((AbstractValueView) temp.elementAt(j), locale) + " - (" + ((AbstractContextualValueDefaultView) temp.elementAt(j)).getReferenceValueDefaultView().getLiteIBAReferenceable().getIBAReferenceableDisplayString() + ")");
                           } catch (WTException e) {
                              e.printStackTrace();
                           }

                           tempAttVals.addElement(temp.elementAt(j));
                           rowspan++;
                        }
                     } else {
                        //  No reference value for this attribute, put it into the indVals vector,
                        //  and remove it from the attVals vector.
                        indVals.addElement(temp.elementAt(j));
                        attVals.removeElement(temp.elementAt(j));
                     }
                  }

                  //  If the current reference value has dependencies, display them
                  if (!tempAttVals.isEmpty()) {
                     sb.append("<TR " + tableRowBackgroundColor + ">\n");
                     sb.append("<TD WIDTH=\"30%\"><FONT " + tableRowFontColor + tableFontFace + " SIZE=\"2\"><B>Dependency</B></FONT></TD>\n");
                     sb.append("<TD WIDTH=\"70%\"><FONT " + tableRowFontColor + tableFontFace + " SIZE=\"2\"><B>Value</B></FONT></TD>\n");
                     sb.append("</TR>\n");

                     for (int k = 0; k < tempAttVals.size(); k++) {
                        sb.append("<TR " + tableRowBackgroundColor + ">\n");
                        sb.append("<TD>" + ((AbstractValueView) tempAttVals.elementAt(k)).getDefinition().getDisplayName() + "</TD>\n");

                        try {
                           sb.append("<TD BGCOLOR=\"#9B599B\"><FONT " + tableRowFontColor + ">" + IBAValueUtility.getLocalizedIBAValueDisplayString((AbstractValueView) tempAttVals.elementAt(k), locale) + "</FONT></TD>\n");
                        } catch (WTException e) {
                           e.printStackTrace();
                        }

                        sb.append("</TR>\n");
                     }
                  }

                  tempAttVals.removeAllElements();
               }

               if (dbg == true) {
                  System.out.println("***** End tempRefVals Vector dependencies *****");
               }

               rowspan += (tempRefVals.size() * 2);
               out.println("<TR " + tableRowBackgroundColor + ">\n");
               out.println("<TD VALIGN=\"CENTER\" ROWSPAN=" + rowspan + ">" + HTMLEncoder.encodeForHTMLContent(((AbstractValueView) tempRefVals.firstElement()).getDefinition().getDisplayName()) + "</TD>\n");
               out.print(sb.toString());

               //  Empty tempRefVals vector and start the process all over again...
               tempRefVals.removeAllElements();
            } while (!refVals.isEmpty());

            //  Display the independent attribute values.

            if (dbg == true) {
               System.out.println("***** indVals Vector  *****");
               for (int i = 0; i < indVals.size(); i++) {
                  try {
                     System.out.println(((AbstractValueView) indVals.elementAt(i)).getDefinition().getDisplayName() + " - " + IBAValueUtility.getLocalizedIBAValueDisplayString((AbstractValueView) indVals.elementAt(i), locale));
                  } catch (WTException e) {
                     e.printStackTrace();
                  }
               }

               System.out.println("***** End indVals Vector *****");
            }

            for (int i = 0; i < indVals.size(); i++) {
               out.println("<TR " + tableRowBackgroundColor + ">\n");
               out.println("<TD VALIGN=\"CENTER\">" + HTMLEncoder.encodeForHTMLContent(((AbstractValueView) indVals.elementAt(i)).getDefinition().getDisplayName()) + "</TD>\n");

               try {
                  if (avv[i] instanceof URLValueDefaultView) {
                     URLValueDefaultView urlv = (URLValueDefaultView) avv[i];
                     out.println("<TD VALIGN=\"CENTER\" COLSPAN=\"2\" " + tableRowBackgroundColor + "><FONT " + tableRowFontColor + ">" + "<A HREF=\"" + urlv.getValue() + "\">" + urlv.getDescription() + "</A>" + "</FONT></TD>\n");
                  } else {
                     out.println("<TD VALIGN=\"CENTER\" COLSPAN=\"2\" " + tableRowBackgroundColor + "><FONT " + tableRowFontColor + ">" + IBAValueUtility.getLocalizedIBAValueDisplayString((AbstractValueView) indVals.elementAt(i), locale) + "</FONT></TD>\n");
                  }
               } catch (WTException e) {
                  e.printStackTrace();
               }

               out.println("</TR>\n");
            }
         } else {
            //  Attribute values array is empty
            //out.println("<TR><TD COLSPAN=\"3\"><CENTER>" + messagesResource.getString(enterpriseResource.NO_ATTRIBUTES_MESSAGE) + "</CENTER></TD></TR>\n");

            if (dbg) {
               System.out.println("Attribute value array is empty.");
            }
         }
      } else {
         //  Error, unknown mode.
         out.println("<H1>Error: Unknown Mode <I>" + HTMLEncoder.encodeForHTMLContent(mode) + "</I>.<H1>");
      }

      if (dbg == true) {
         System.out.println("^$^ END DEBUG ^$^");
      }

      out.println("</TABLE>\n");
      out.flush();
   }

   /*
    *  Outputs a dropdown menu of available measurement systems for displaying units
    *  for IBA values
    */

   /**
    *  Description of the Method
    *
    *@param  currentMeasurementSystem  Description of the Parameter
    *@param  displayFormTag            Description of the Parameter
    *@param  tableAlignment            Description of the Parameter
    *@param  tableWidth                Description of the Parameter
    *@param  out                       Description of the Parameter
    *@param  locale                    Description of the Parameter
    *@exception  RemoteException       Description of the Exception
    *@exception  WTException           Description of the Exception
    */
   private void displayMeasurementSystemsDropDown(String currentMeasurementSystem, String displayFormTag, String tableAlignment, String tableWidth, PrintWriter out, Locale locale)
          throws RemoteException, WTException {
      String[] measurementSystemsNameList = wt.units.service.UnitsHelper.service.getMeasurementSystemNames();
      if (measurementSystemsNameList.length > 0) {
         out.println("<BR><TABLE BORDER=\"0\" CELLPADDING=\"0\" ALIGN=\"" + tableAlignment + "\" WIDTH=\"" + tableWidth + "\"> \n");
         if (displayFormTag.equalsIgnoreCase("true")) {
            Properties props = new Properties();
            props.put("action", "ModifyIBADisplayUnits");
            props.put("nextAction", getContextAction());
            Object contextObj = getContextObj();
            if (contextObj != null) {
               ReferenceFactory rf = new ReferenceFactory();
               String objRef = rf.getReferenceString(ObjectReference.newObjectReference(((Persistable) contextObj).getPersistInfo().getObjectIdentifier()));
               props.put("nextContextObj", objRef);
            } else if (getContextClass() != null) {
               props.put("nextContextClass", getContextClass());
            }
            out.println("<FORM NAME=\"MeasurementSystemsList\" METHOD=\"post\" ACTION=\"" +
                  HTMLEncoder.encodeURIForHTMLAttribute(getURLProcessorLink("processForm", props)) + "\" style=\"margin-bottom:0;margin-top:0\">");
         }
         out.println("<TR><TD>" + getResourceString(enterpriseResource.IBA_DISPLAY_UNITS_DROPDOWN_LABEL, locale, RESOURCE, "Units:"));
         out.println("<SELECT NAME=\"DisplayUnitsDropDown\">");
         for (int i = 0; i < measurementSystemsNameList.length; i++) {
            String selectedAttr = measurementSystemsNameList[i].equals(currentMeasurementSystem) ? " selected" : "";
            out.println("<OPTION" + selectedAttr + ">" + HTMLEncoder.encodeForHTMLContent(measurementSystemsNameList[i]));
         }
         out.println("</SELECT>");
         out.println("<INPUT TYPE=\"submit\" VALUE=\"" + getResourceString(enterpriseResource.IBA_DISPLAY_UNITS_BUTTON_LABEL, locale, RESOURCE, "Change") + "\">");
         if (displayFormTag.equalsIgnoreCase("true")) {
            out.println("</FORM>");
         }
         out.println("</TABLE>");
      }
   }

   /**
    *  This method when is the same as displayAttributesTable() with hard-coded
    *  fonts and colors removed and references to stylesheet settings added. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  parameters       the properties passed to the processing method
    *@param  locale           the locale passed to the processing method
    *@param  os               the output stream passed to the processing method
    *@param  USE_STYLE_SHEET  informs the processing method to use solutions.css
    *      style classes
    */
   public void displayStyleSheetAttributesTable(Properties parameters, Locale locale, OutputStream os, boolean USE_STYLE_SHEET) {
      boolean dbg = false;
      PrintWriter out = getPrintWriter(os, locale);
      Object theContextObject = getContextObj();
      IBAHolder ibah = null;
      String mode = null;
      DefaultAttributeContainer dac = null;
      AbstractValueView[] avv = null;
      Vector avvVec = null;
      Vector refVals = null;
      Vector attVals = null;
      Vector indVals = new Vector();
      String tableAlignment = "CENTER";
      String tableWidth = "90%";

      // Create strings that can derive style from the stylesheet.
      // Reference stylesheet for declarations is Windchill_src_com_ptc_core_ui_solution.css.
      // Change these values in your stylesheet to implement new styles.
      String tableTextBold = "class=tableTextBold";
      String tableText = "class=tableText";
      String tableBkgd = "class=tableBkgd";
      String tableSdw1 = "class=tableSdw1";
      String tableSdw2 = "class=tableSdw2";
      String tableHdr = "class=tableHdr";
      String tableRowSel = "class=tableRowSel";
      String tableRowNSel = "class=tableRowNSel";
      String tableBtm = "class=tableBtm";
      String tableTabSel = "class=tableTabSel";
      String tableTabNSel = "class=tableTabNSel";
      String tableColSdw1 = "class=tableColSdw1";
      String tableColSdw2 = "class=tableColSdw2";
      String tableColSdwSel1 = "class=tableColSdwSel1";
      String tableColSdwSel2 = "class=tableColSdwSel2";
      String tableColSdwSel3 = "class=tableColSdwSel3";
      String tableColSdwSel4 = "class=tableColSdwSel4";
      String tableColSdwSel5 = "class=tableColSdwSel5";
      String tableColSdwSel6 = "class=tableColSdwSel6";
      String tableAnnotation = "class=tableAnnotation";

      final String tableAlignmentKey = "attributes_table_alignment";
      final String tableWidthKey = "attributes_table_width";
      long rowspan = 0;

      if (dbg == true) {
         System.out.println("^$^ DEBUG ^$^");
      }

      ResourceBundle messagesResource = ResourceBundle.getBundle(RESOURCE, locale);

      if (parameters != null) {
         mode = parameters.getProperty("mode");
         tableAlignment = parameters.getProperty(tableAlignmentKey, tableAlignment);
         tableWidth = parameters.getProperty(tableWidthKey, tableWidth);
      } else {
         parameters = new Properties();
         parameters.put("mode", "Simple");
         mode = parameters.getProperty("mode");
         parameters.put(tableAlignmentKey, tableAlignment);
         tableAlignment = parameters.getProperty(tableAlignmentKey);
         parameters.put(tableWidthKey, tableWidth);
         tableWidth = parameters.getProperty(tableWidthKey);
      }

      try {
         if (!(theContextObject instanceof IBAHolder)) {
            System.out.println("theContextObject = " + theContextObject + " is not an IBAHolder.");
            return;
         }

         ibah = (IBAHolder) theContextObject;
         ibah = (IBAHolder) IBAValueHelper.service.refreshAttributeContainer(ibah, null, locale, null);
         dac = (DefaultAttributeContainer) ibah.getAttributeContainer();

         if (dac != null) {
            int j = 0;
            avv = dac.getAttributeValues();
            avvVec = new Vector();

            for (int i = 0; i < avv.length; i++) {
               avvVec.addElement(avv[i]);
            }

            Enumeration e = avvVec.elements();
            AbstractValueViewCollationKeyFactory avvckf = new AbstractValueViewCollationKeyFactory(locale);
            SortedEnumeration se = new SortedEnumeration(e, avvckf);
            avv = new AbstractValueView[se.size()];

            while (se.hasMoreElements()) {
               avv[j] = (AbstractValueView) se.nextElement();
               j++;
            }
         }
      } catch (wt.access.NotAuthorizedException nae) {
         nae.printStackTrace();
      } catch (RemoteException re) {
         re.printStackTrace();
      } catch (WTException wte) {
         wte.printStackTrace();
      }

      if (mode.equals("Simple")) {
         out.println("<br><table border=\"0\" cellpadding=\"2\" align=\"" + tableAlignment + "\" width=\"" + tableWidth + "\" \n");
         if (avv != null && avv.length != 0) {
            out.println("<thead>\n");
            out.println("<tr " + tableSdw1 + ">\n");
            out.println("<th align=\"left\">\n");
            out.println(messagesResource.getString(enterpriseResource.ATTRIBUTE_NAME_LABEL) + "\n");
            out.println("</th>\n");
            out.println("<th align=\"left\">\n");
            out.println(messagesResource.getString(enterpriseResource.ATTRIBUTE_VALUE_LABEL) + "\n");
            out.println("</th>\n");
            out.println("<th align=\"left\">\n");
            out.println(messagesResource.getString(enterpriseResource.ATTRIBUTE_DEPENDENCY_LABEL) + "\n");
            out.println("</th>\n");
            out.println("</tr>\n");
            out.println("</thead>\n");

            for (int i = 0; i < avv.length; i++) {
               // Toggle the row background color for readability
               if ((i % 2) == 0) {
                  out.println("<tr " + tableRowSel + ">\n");
               } else {
                  out.println("<tr " + tableRowNSel + ">\n");
               }

               //  Display the attribute definition name
               out.println("<td valign=\"center\">");
               out.println(HTMLEncoder.encodeForHTMLContent(avv[i].getDefinition().getDisplayName()));
               out.println("</td>\n");

               //  Diplay the attribute value
               out.println("<td align=\"center\">");

               if (!(avv[i] instanceof ReferenceValueDefaultView)) {
                  try {
                     if (avv[i] instanceof URLValueDefaultView) {
                        URLValueDefaultView urlv = (URLValueDefaultView) avv[i];
                        out.println("<a href=\"" + HTMLEncoder.encodeURIForHTMLAttribute(urlv.getValue()) + "\">" + HTMLEncoder.encodeForHTMLContent(urlv.getDescription()) + "</a>");
                     } else {
                        out.println(IBAValueUtility.getLocalizedIBAValueDisplayString(avv[i], locale));
                     }
                  } catch (WTException e) {
                     e.printStackTrace();
                  }
               } else {
                  //  This is a ReferenceValueDefaultView
                  out.println(((ReferenceValueDefaultView) avv[i]).getLiteIBAReferenceable().getIBAReferenceableDisplayString());
                  out.println("</td>\n");
                  out.println("<td valign=\"center\">");
                  out.println("&nbsp;");
               }

               out.println("</td>\n");

               //  Display the attribute's dependency value if any
               if (!(avv[i] instanceof ReferenceValueDefaultView)) {
                  if (((AbstractContextualValueDefaultView) avv[i]).getReferenceValueDefaultView() != null) {
                     out.println("<td valign=\"center\">");
                     out.println(((AbstractContextualValueDefaultView) avv[i]).getReferenceValueDefaultView().getLiteIBAReferenceable().getIBAReferenceableDisplayString());
                     out.println("</td>\n");
                  } else {
                     out.println("<td valign=\"center\">");
                     out.println("&nbsp;");
                     out.println("</td>\n");
                  }
               }
               out.println("</tr>\n");
            }
         } else {
            //  Attribute values array is empty
            //out.println("<TR><TD COLSPAN=\"3\"><CENTER>" + messagesResource.getString(enterpriseResource.NO_ATTRIBUTES_MESSAGE) + "</CENTER></TD></TR>\n");
            if (dbg) {
               System.out.println("Attribute value array is empty.");
            }
         }
      } else if (mode.equals("Nested")) {
         StringBuffer sb = new StringBuffer();

         if (dbg == true) {
            System.out.println("displayAttributesTable mode=Nested");
            System.out.println("Context object - " + theContextObject);
         }

         out.println("<br><table border=\"0\" cellpadding=\"2\" align=\"" + tableAlignment + "\" width=\"" + tableWidth + "\">\n");

         if (avv != null && avv.length != 0) {
            refVals = new Vector();
            attVals = new Vector();

            out.println("<thead>\n");
            out.println("<tr " + tableSdw1 + ">\n");
            out.println("<th align=\"left\" width=\"30%\">\n");
            out.println(messagesResource.getString(enterpriseResource.ATTRIBUTE_NAME_LABEL) + "\n");
            out.println("</th>\n");
            out.println("<th align=\"left\" colspan=\"2\" width=\"30%\">\n");
            out.println(messagesResource.getString(enterpriseResource.ATTRIBUTE_VALUE_LABEL) + "\n");
            out.println("</th>\n");
            out.println("</tr>\n");
            out.println("</thead>\n");

            for (int i = 0; i < avv.length; i++) {
               if (avv[i] instanceof ReferenceValueDefaultView) {
                  refVals.addElement(avv[i]);
               } else {
                  attVals.addElement(avv[i]);
               }
            }
            if (dbg == true) {
               System.out.println("***** refVals Vector *****");
               for (int i = 0; i < refVals.size(); i++) {
                  System.out.println(((ReferenceValueDefaultView) refVals.elementAt(i)).getDefinition().getDisplayName() + " - " + ((ReferenceValueDefaultView) refVals.elementAt(i)).getLiteIBAReferenceable().getIBAReferenceableDisplayString());
               }
               System.out.println("***** End refVals Vector *****");
               System.out.println("***** attVals Vector *****");
               for (int i = 0; i < attVals.size(); i++) {
                  try {
                     System.out.println(((AbstractValueView) attVals.elementAt(i)).getDefinition().getDisplayName() + " - " + IBAValueUtility.getLocalizedIBAValueDisplayString((AbstractValueView) attVals.elementAt(i), locale));
                  } catch (WTException e) {
                     e.printStackTrace();
                  }
               }
               System.out.println("***** End attVals Vector *****");
            }

            //  Now we have a vector of reference values and a vector of attribute values
            //  loop through the reference values one by one and then check the elements
            //  in the attributes vector to see if they are a dependent value of the current
            //  reference value.
            Vector tempRefVals = new Vector();
            Vector tempAttVals = new Vector();

            do {
               sb = new StringBuffer();
               rowspan = 0;
               tempRefVals.addElement(refVals.firstElement());
               refVals.removeElement(refVals.firstElement());

               Vector temp = (Vector) refVals.clone();

               for (int i = 0; i < temp.size(); i++) {
                  if (((AbstractValueView) temp.elementAt(i)).getDefinition().isPersistedObjectEqual(((AbstractValueView) tempRefVals.firstElement()).getDefinition())) {
                     tempRefVals.addElement(temp.elementAt(i));
                     refVals.removeElement(temp.elementAt(i));
                  }
               }

               //  Done with temp, empty it for reuse.
               temp.removeAllElements();

               //  Now we have a vector of referenceValues with the same definition. For each
               //  element in this vector, we need to loop through the attribute value vector
               //  and see if the value is dependent on the current reference value.

               if (dbg == true) {
                  System.out.println("***** tempRefVals Vector dependencies *****");
               }

               for (int i = 0; i < tempRefVals.size(); i++) {
                  //  Display the reference value
                  if (dbg == true) {
                     System.out.println(((AbstractValueView) tempRefVals.elementAt(i)).getDefinition().getDisplayName() + " - " + ((ReferenceValueDefaultView) tempRefVals.elementAt(i)).getLiteIBAReferenceable().getIBAReferenceableDisplayString());
                  }

                  if (i > 0) {
                     //  Special case where the first element cannot have the <TR> element before
                     //  it because we need to have the attribute definition and rowspan inserted
                     //  before it...all subsequet rows can have this line.
                     sb.append("<tr " + tableRowSel + ">\n");
                  }
                  sb.append("<td " + tableRowSel + " colspan=\"2\">" + ((ReferenceValueDefaultView) tempRefVals.elementAt(i)).getLiteIBAReferenceable().getIBAReferenceableDisplayString() + "</td></tr>\n");

                  //  Need to find if this element in tempRefVals has any dependent values in
                  //  attVals.
                  temp = (Vector) attVals.clone();

                  for (int j = 0; j < temp.size(); j++) {
                     if (((AbstractContextualValueDefaultView) temp.elementAt(j)).getReferenceValueDefaultView() != null) {
                        //  Attribute has a reference value, check to see if it is the current tempRefVals value
                        if (((ReferenceValueDefaultView) tempRefVals.elementAt(i)).isPersistedObjectEqual(((AbstractContextualValueDefaultView) temp.elementAt(j)).getReferenceValueDefaultView())) {
                           try {
                              System.out.println("\t" + IBAValueUtility.getLocalizedIBAValueDisplayString((AbstractValueView) temp.elementAt(j), locale) + " - (" + ((AbstractContextualValueDefaultView) temp.elementAt(j)).getReferenceValueDefaultView().getLiteIBAReferenceable().getIBAReferenceableDisplayString() + ")");
                           } catch (WTException e) {
                              e.printStackTrace();
                           }
                           tempAttVals.addElement(temp.elementAt(j));
                           rowspan++;
                        }
                     } else {
                        //  No reference value for this attribute, put it into the indVals vector,
                        //  and remove it from the attVals vector.
                        indVals.addElement(temp.elementAt(j));
                        attVals.removeElement(temp.elementAt(j));
                     }
                  }

                  //  If the current reference value has dependencies, display them
                  if (!tempAttVals.isEmpty()) {
                     sb.append("<tr " + tableRowSel + ">\n");
                     sb.append("<td width=\"30%\"><b>Dependency</b></td>\n");
                     sb.append("<td width=\"70%\"><b>Value</b></td>\n");
                     sb.append("</tr>\n");

                     for (int k = 0; k < tempAttVals.size(); k++) {
                        sb.append("<tr " + tableRowSel + ">\n");
                        sb.append("<td>" + ((AbstractValueView) tempAttVals.elementAt(k)).getDefinition().getDisplayName() + "</td>\n");

                        try {
                           sb.append("<td>" + IBAValueUtility.getLocalizedIBAValueDisplayString((AbstractValueView) tempAttVals.elementAt(k), locale) + "</td>\n");
                        } catch (WTException e) {
                           e.printStackTrace();
                        }
                        sb.append("</tr>\n");
                     }
                  }
                  tempAttVals.removeAllElements();
               }
               if (dbg == true) {
                  System.out.println("***** End tempRefVals Vector dependencies *****");
               }
               rowspan += (tempRefVals.size() * 2);
               out.println("<tr " + tableRowSel + ">\n");
               out.println("<td valign=\"center\" rowspan=" + rowspan + ">" + HTMLEncoder.encodeForHTMLContent(((AbstractValueView) tempRefVals.firstElement()).getDefinition().getDisplayName()) + "</td>\n");
               out.print(sb.toString());

               //  Empty tempRefVals vector and start the process all over again...
               tempRefVals.removeAllElements();
            } while (!refVals.isEmpty());
            //  Display the independent attribute values.
            if (dbg == true) {
               System.out.println("***** indVals Vector  *****");
               for (int i = 0; i < indVals.size(); i++) {
                  try {
                     System.out.println(((AbstractValueView) indVals.elementAt(i)).getDefinition().getDisplayName() + " - " + IBAValueUtility.getLocalizedIBAValueDisplayString((AbstractValueView) indVals.elementAt(i), locale));
                  } catch (WTException e) {
                     e.printStackTrace();
                  }
               }
               System.out.println("***** End indVals Vector *****");
            }

            for (int i = 0; i < indVals.size(); i++) {
               out.println("<tr " + tableRowSel + ">\n");
               out.println("<td valign=\"center\">" + HTMLEncoder.encodeForHTMLContent(((AbstractValueView) indVals.elementAt(i)).getDefinition().getDisplayName()) + "</td>\n");

               try {
                  if (avv[i] instanceof URLValueDefaultView) {
                     URLValueDefaultView urlv = (URLValueDefaultView) avv[i];
                     out.println("<td valign=\"center\" colspan=\"2\"><a href=\"" + HTMLEncoder.encodeURIForHTMLAttribute(urlv.getValue()) + "\">" + HTMLEncoder.encodeForHTMLContent(urlv.getDescription()) + "</a>" + "</td>\n");
                  } else {
                     out.println("<td valign=\"center\" colspan=\"2\">" + IBAValueUtility.getLocalizedIBAValueDisplayString((AbstractValueView) indVals.elementAt(i), locale) + "</td>\n");
                  }
               } catch (WTException e) {
                  e.printStackTrace();
               }
               out.println("</tr>\n");
            }
         } else {
            //  Attribute values array is empty
            //out.println("<TR><TD COLSPAN=\"3\"><CENTER>" + messagesResource.getString(enterpriseResource.NO_ATTRIBUTES_MESSAGE) + "</CENTER></TD></TR>\n");

            if (dbg) {
               System.out.println("Attribute value array is empty.");
            }
         }
      } else {
         //  Error, unknown mode.
         out.println("<h1>Error: Unknown Mode <i>" + HTMLEncoder.encodeForHTMLContent(mode) + "</i>.<h1>");
      }

      if (dbg == true) {
         System.out.println("^$^ END DEBUG ^$^");
      }

      out.println("</table>\n");
      out.flush();
   }

   /**
    *  Gets the uRLProcessorLink attribute of the BasicTemplateProcessor object
    *
     *  <BR><BR><B>Supported API: </B>true
     *
     * @deprecated
    *
    *@param  parameters       Description of the Parameter
    *@param  locale           Description of the Parameter
    *@param  os               Description of the Parameter
    *@exception  WTException  Description of the Exception
    */
   public void getURLProcessorLink(Properties parameters, Locale locale, OutputStream os) throws WTException {
      PrintWriter out = getPrintWriter(os, locale);
      String methodName = "URLTemplateAction";
      boolean installed = true;
      boolean fullyQualified = false;

      if (parameters != null) {
         String installed_key = parameters.getProperty("installed_key", null);
         if (installed_key != null) {
            // Check if the component is installed.
            installed = InstalledProperties.isInstalled(installed_key);
         }

         if (installed) {
            if (VERBOSE) {
               System.err.println("getURLProcessorLink : parameters != null");
            }
            String currentKey = null;
            String value = null;
            for (Enumeration enumeration = parameters.keys(); enumeration.hasMoreElements(); ) {
               currentKey = (String) enumeration.nextElement();
               value = parameters.getProperty(currentKey);
               if (VERBOSE) {
                  System.err.println("getURLProcessorLink : currentKey = " + currentKey + "   value = " + value);
               }
               if (value == null) {
                  parameters.remove(currentKey);
               } else if (currentKey.equalsIgnoreCase("method")) {
                  if (VERBOSE) {
                     System.err.println("getURLProcessorLink : methodName = " + methodName);
                  }
                  methodName = value;
                  parameters.remove(currentKey);
               } else if (currentKey.equalsIgnoreCase("fullyQualified")) {
                  if (value.equalsIgnoreCase("true")) {
                     fullyQualified = true;
                  }
                  parameters.remove(currentKey);
                  if (VERBOSE) {
                     System.err.println("getURLProcessorLink : fullyQualified = " + fullyQualified);
                  }
               } else if (value.equals("")) {
                  parameters.remove(currentKey);
               }
            }
         }
      }

      if (installed) {
         out.print(getURLProcessorLink(methodName, parameters, fullyQualified));
         out.flush();
      }
   }

   /**
    *  Gets the uRLProcessorLink attribute of the BasicTemplateProcessor object
    *
    *@param  methodName       Description of the Parameter
    *@param  parameters       Description of the Parameter
    *@return                  The uRLProcessorLink value
    *@exception  WTException  Description of the Exception
    */
   public String getURLProcessorLink(String methodName, Properties parameters) throws WTException {
      return getURLProcessorLink(methodName, parameters, false);
   }

   /**
    *  Gets the uRLProcessorLink attribute of the BasicTemplateProcessor object
    *
    *@param  methodName       Description of the Parameter
    *@param  parameters       Description of the Parameter
    *@param  fullyQualified   Description of the Parameter
    *@return                  The uRLProcessorLink value
    *@exception  WTException  Description of the Exception
    */
   public String getURLProcessorLink(String methodName, Properties parameters, boolean fullyQualified)
          throws WTException {
      if (parameters == null) {
         parameters = new Properties();
      }
      HashMap map = new HashMap(parameters);
      try {
         if (fullyQualified == true) {
            return GatewayServletHelper.buildAuthenticatedURL(getState().getURLFactory(),
                  HTML_GATEWAY,
                  methodName, null, map).toExternalForm();
         } else {
            return GatewayServletHelper.buildAuthenticatedHREF(getState().getURLFactory(),
                  HTML_GATEWAY,
                  methodName, null, map);
         }
      } catch (WTException e) {
         e.printStackTrace();
         return "";
      }
   }

   /**
    *  Gets the uRLProcessorLinkWithLabel attribute of the BasicTemplateProcessor
    *  class
    *
    *@param  label       Description of the Parameter
    *@param  parameters  Description of the Parameter
    *@param  method      Description of the Parameter
    *@param  properties  Description of the Parameter
    *@return             The uRLProcessorLinkWithLabel value
    */
   public static String getURLProcessorLinkWithLabel(String label, String parameters,
         String method, Properties properties) {
      String url = label;
      try {
         if (properties == null) {
            properties = new Properties();
         }
         HashMap map = new HashMap(properties);
         url = GatewayServletHelper.buildAuthenticatedHREF(getURLFactory(),
               HTML_GATEWAY,
               method, null,
               map);
         url = HtmlUtil.createLink(url, parameters, label);
      } catch (WTException wte) {
         url = label;
      }

      return url;
   }

   /**
    *  This method should be used to add to a running list of exceptions kept
    *  track of in the HTTPState object. <P>
    *
    *  The stackTraces for the list of exceptions can be presented using the
    *  following Windchill Script call <P>
    *
    *  <code>
    *  <!-- showResponseExceptions -->
    *  </code></p>
    *
     *  <BR><BR><B>Supported API: </B>true
     *
     * @deprecated
     *
    *@param  newException
    */
   public void addToResponseExceptions(Exception newException) {
      getState().addToResponseExceptions(newException);
   }

   /**
    *  This method should be used to add to a running list of LocalizableMessages
    *  kept track of in the HTTPState object. This list of LocalizableMessage can
    *  be presented using the following Windchill Script call <P>
    *
    *  <Script language=Windchill> <!-- showResponseMessages --> </Script> <P>
    *
    *  The messages will be presented in the Locale of the HTML template
    *
    *
     *  <BR><BR><B>Supported API: </B>true
     *
     * @deprecated
     *
    *@param  newMessage
    */
   public void addToResponseMessages(LocalizableMessage newMessage) {
      getState().addToResponseMessages(newMessage);
   }

   /**
    *  This method should be used to add to a running list of LocalizableMessages
    *  kept track of in the HTTPState object to present at the top of an HTML
    *  page. This list of messages can be presented using the following Windchill
    *  Script call <P>
    *
    *  <Script language=Windchill> <!-- showResponseHeaders --> </Script> <P>
    *
    *  The goal of this option is to provide a way to present any information that
    *  occured preparing the processing of the page at the top of the page. <P>
    *
    *  The LocalizableMessages will be presented in the locale of the HTML page
    *
     * <BR><BR><B>Supported API: </B>true
     *
     * @deprecated
    *
    *@param  newHeader
    */
   public void addToResponseHeaders(LocalizableMessage newHeader) {
      getState().addToResponseHeaders(newHeader);
   }

   /**
    *  This method should be used to add to a running list of Messages kept track
    *  of in the HTTPState object. This list of messages can be presented using
    *  the following Windchill Script call <Script language=Windchill> <!--
    *  showResponseFooters --> </Script> The goal of this option is to provide a
    *  way to present any information that occured during the processing of the
    *  page at the bottom of the page.
    *
     *  <BR><BR><B>Supported API: </B>true
     *
     * @deprecated
    *
    *@param  newFooter  The feature to be added to the ToResponseFooters attribute
    */
   public void addToResponseFooters(LocalizableMessage newFooter) {
      getState().addToResponseFooters(newFooter);
   }

   /**
    *  Gets a TemplateProcessor and HTML Template and processes the HTML template
    *  in place. That is, the Windchill script call is replaced with the output of
    *  the processed HTML template. This allows a more componentized approach to
    *  your HTML templates and also allows your template processors to be
    *  componentized also. <P>
    *
    *  The correct Windchill script call is <P>
    *
    *  <code>
    * <NOBR>processSubTemplate action=&lt;Context Action for sub-TemplateProcessor&gt; &lt;More name/value pairs&gt;</NOBR>
    *  </code> <P>
    *
    *  This entry will look for the following in entry in a properties file <P>
    *
    *  <CODE>
    * <NOBR>wt.services/rsc/default/wt.templateutil.DefaultHTMLTemplate/<B>&lt;Context Action for sub-TemplateProcessor&gt;</B>
    *  /&lt;Current Context Object&gt;/0=&lt;relative path to HTML Template&gt;
    *  </NOBR> <P>
    *
    *  The action specified in the Windchill script call and the current context
    *  object are used to locate the correct TemplateProcessor and the HTML
    *  template via entries in the properties files, as usual. <P>
    *
    *  After getting the TemplateProcessor, the current state is passed off the
    *  the sub-TemplateProcessor via the HTTPState object. The action on the
    *  HTTPState object is temporalily set to the action in the Windchill script
    *  call. The contextAction is restored to its previous value after the
    *  processing of the sub-HTML template is finished. <P>
    *
    *  The Locale and OutputStream are passed off to the sub-TemplateProcessor so
    *  it knows the current Locale and has access to output stream to the browser.
    *  The name/values pairs passed in via the Windchill script are made available
    *  to the Sub-TemplateProcessor via contextProperties object of the
    *  TemplateProcessor.
    *
     *  <BR><BR><B>Supported API: </B>true
     *
     * @deprecated
    *
    *@param  parameters
    *@param  locale
    *@param  os
    */
   public void processSubTemplate(Properties parameters, Locale locale, OutputStream os) {
      PrintWriter out = getPrintWriter(os, locale);
      if (parameters == null) {
         System.err.println("the action parameter is required to use this Script Call");
         out.println("the action parameter is required to use this Script Call");
         return;
      }

      parameters.put(PageContext.KEY, getPageContext().getID());

      String tempTemplate = parameters.getProperty(SubTemplateService.TEMPLATE);
      String tempAction = parameters.getProperty("action");

      // If an action is specified, all is OK
      if (tempTemplate == null || tempTemplate.equals("")) {
         if (tempAction == null || tempAction.equals("")) {
            System.err.println("Specify the action parameter or the " + SubTemplateService.TEMPLATE + " parameter.");
            out.println("Specify the action parameter or the " + SubTemplateService.TEMPLATE + " parameter.");
            return;
         } else if (tempAction.equals(getContextAction())) {
            System.err.println("Specify the action parameter or the " + SubTemplateService.TEMPLATE + " parameter.");
            out.println("Specify the action parameter or the " + SubTemplateService.TEMPLATE + " parameter.");
            return;
         }
      }
      if (tempAction == null || tempAction.equals("")) {
         tempAction = getContextAction();
      }
      String cacheAction = getContextAction();
      setContextAction(tempAction);
      SubTemplateService templateService = new SubTemplateService(parameters, locale, os);
      templateService.processTemplate(getState());
      setContextAction(cacheAction);

      try {
         os.flush();
      } catch (java.io.IOException ioe) {
      }

      return;
   }

   /**
    *  Presents the stackTraces from each Exception in maintained in the HTTPState
    *  object. The exceptions are added to the HTTPState object with the call to
    *  the addToResponseExceptions method. <P>
    *
    *  The correct Windchill script call is <P>
    *
    *  showResponseExceptions <P>
    *
    *  The goal of this option is to provide a way to present a list of non-fatal
    *  stackTraces that occured in the processing of an HTML template or a HTTP
    *  Get/Post request.
    *
     *  <BR><BR><B>Supported API: </B>true
     *
     * @deprecated
    *
    *@param  parameters
    *@param  locale
    *@param  os
    */
   public void showResponseExceptions(Properties parameters, Locale locale, OutputStream os) {
      Vector responseExceptions = getResponseExceptions();
      String message = "";
      responseExceptions.trimToSize();
      int size = responseExceptions.size();
      if (VERBOSE) {
         System.err.println("BasicTemplateProcessor - showResponseExceptions...");
         System.err.println("BasicTemplateProcessor - showResponseExceptions : There are " + size + " messages");
      }

      for (int i = 0; i < size; i++) {
         Object baseMessage = responseExceptions.elementAt(i);
         if (baseMessage instanceof Exception) {
            message = ((Exception) baseMessage).getLocalizedMessage();
         } else if (baseMessage instanceof WTException) {
            message = ((WTException) baseMessage).getLocalizedMessage(locale, true);
         }

         showResponseMessage(message, parameters, locale, os);
      }

      getResponseExceptions().removeAllElements();
   }

   /**
    *  Presents the LocalizableMessages in maintained in the HTTPState object that
    *  were added to the HTTPState object with the call to the
    *  addToResponseHeaders method. <P>
    *
    *  The correct Windchill script call is <P>
    *
    *  showResponseHeaders <P>
    *
    *  The goal of this option is to provide a way to present any information that
    *  occured during the processing of the page at the top of the page. <P>
    *
    *  This ideal to present information in the response page that was occurred in
    *  the processing of a request in a FormTaskDelegate.
    *
    *@param  parameters
    *@param  locale
    *@param  os
    */
   public void showResponseHeaders(Properties parameters, Locale locale, OutputStream os) {
      if (VERBOSE) {
         System.err.println("BasicTemplateProcessor - showResponseHeaders...");
      }
      Vector responseHeaders = getResponseHeaders();
      showResponseMessages(responseHeaders, parameters, locale, os);
      getResponseHeaders().removeAllElements();
   }

   /**
    *  Presents the LocalizableMessages in maintained in the HTTPState object that
    *  were added to the HTTPState object with the call to the
    *  addToResponseFooters method. <P>
    *
    *  The correct Windchill script call is <P>
    *
    *  showResponseFooters <P>
    *
    *  The goal of this option is to provide a way to present any information that
    *  occured during the processing of the page at the bottom of the page. <P>
    *
    *  This ideal to present information in the bottom of the HTML page regarding
    *  any occurances while processing the HTML template for the page.
    *
    *@param  parameters
    *@param  locale
    *@param  os
    */
   public void showResponseFooters(Properties parameters, Locale locale, OutputStream os) {
      Vector responseFooters = getResponseFooters();
      showResponseMessages(responseFooters, parameters, locale, os);
      getResponseFooters().removeAllElements();
   }

   /**
    *  This method will receive a Vector containing LocalizableMessages and prints
    *  them out in order. A call to the method showResponseMessage is used to
    *  format each message to be presented. A subclass of BasicTemplateProcessor
    *  can override the showResponseMessage to provide its own formatting for the
    *  LocalizableMessage
    *
    *@param  messages
    *@param  parameters
    *@param  locale
    *@param  os
    */
   public void showResponseMessages(Vector messages, Properties parameters, Locale locale, OutputStream os) {
      LocalizableMessage response = null;
      messages.trimToSize();
      int size = messages.size();
      if (VERBOSE) {
         System.err.println("BasicTemplateProcessor - showResponseMessages...");
         System.err.println("BasicTemplateProcessor - showResponseMessages : There are " + size + " messages");
      }

      for (int i = 0; i < size; i++) {
         response = (LocalizableMessage) messages.elementAt(i);
         String message = response.getLocalizedMessage(locale);
         showResponseMessage(message, parameters, locale, os);
      }
   }

   /**
    *  This method will receive a String and print the String out to the HMTL page
    *  in a centered HTML table with a border=3. The cell's background color is
    *  set to the wt.property wt.html.color.bg-msg and font color is set to
    *  wt.html.color.f-msg. <P>
    *
    *  This method is used to format the output for the showResponseMessages
    *  method. The showResponseMessages method in turn is used to print out the
    *  html for the show*Messages method calls, such as showResponseHeaders. A
    *  subclass of BasicTemplateProcessor can override the this method to provide
    *  its own formatting for the presentation of the LocalizedMessages.
    *
    *@param  message
    *@param  parameters
    *@param  locale
    *@param  os
    */
   public void showResponseMessage(String message, Properties parameters, Locale locale, OutputStream os) {
      PrintWriter out = getPrintWriter(os, locale);
      out.println("<center><Table border=3><TR><TD bgcolor=\"" +
            getWCColor("bg-msg") + "\"><FONT color=\"" + getWCColor("f-msg") + "\">");
      out.println(message);
      out.println("</FONT></TD></TR></Table></center>");
      out.flush();
   }

   /**
    *  Sets the TemplateProcessorTableService instance to be used for the
    *  Windchill script calls <P>
    *
    *  tableService action=... <P>
    *
    *
    *
    *@param  newTableService
    */
   public void setHTMLTableService(TemplateProcessorTableService newTableService) {
      htmlTableService = newTableService;
   }

   /**
    *  Gets the TemplateProcessorTableService instance to be used for the
    *  Windchill script calls <P>
    *
    *  tableService action=... <P>
    *
    *  If there is not an instance of TemplateProcessorTableService available,
    *  lazy initialization is done to present one. <P>
    *
    *  The current state is passed to the TemplateProcessorTableService instance
    *  via the HTTPState object.
    *
     *  <BR><BR><B>Supported API: </B>true
     *
     * @deprecated
    *
    *@return                  The hTMLTableService value
    */
   public TemplateProcessorTableService getHTMLTableService() {
      if (htmlTableService == null) {
         htmlTableService = new TemplateProcessorTableService();
      }
      htmlTableService.setState(this.getState());
      return htmlTableService;
   }

   /**
    *  Calls the current TemplateProcessorTableService instance and fires off an
    *  HTMLTableServiceEvent to all of the registered
    *  HTMLTableServiceEventListeners. <P>
    *
    *  The correct Windchill script call is <P>
    *
    *  <code>
    * tableService action=&lt;A valid action for a HTMLTableServiceEventListener&gt; &lt;More name/value pairs&gt;
    * </code> <P>
    *
    *  The HTMLTableServiceEventListeners are registered in wt.properties in the
    *  entry <P>
    *
    *  wt.templateutil.table.HTMLTableServiceEventListeners=... <P>
    *
    *  Please see the Javadoc for the registered HTMLTableServiceEventListeners as
    *  to how to use the individual method calls and what parameters they take
    *
     *  <BR><BR><B>Supported API: </B>true
     *
     * @deprecated
     *
    *@param  parameters
    *@param  locale
    *@param  os
    *@exception  WTException  Description of the Exception
    */
   public void tableService(Properties parameters, Locale locale, OutputStream os) throws WTException {
      getHTMLTableService().performAction(parameters, locale, os);
   }

   /**
    *  Calls a method on a ProcessorService (or TemplateProcessor ) that is
    *  context aware. <P>
    *
    *  The correct Windchill script call is <P>
    *
    *  <code>
    * useProcessorService service=&lt;Fully qualified classpath&gt; method=&lt;Method name&gt; &lt;More name/value pairs&gt;
    * </code> <P>
    *
    *  The parameters have the following meaning
    *  <UL>
    *    <LI> service - The fully qualified classpath of a subclass of
    *    ProcessorService or of BasicTemplateProcessor
    *    <LI> method - a method ( non-static or static ) that has the following
    *    signature
    *  </UL>
    *  <code>
    * public void &lt;Method name&gt;( Properties parameters, Locale locale, OutputStream os )
    * </code>
    *  <UL>
    *    <LI> &lt;More name/value pairs&gt; - method dependent name/values pairs
    *    to pass in
    *  </UL>
    *  The call to the method will have access to the <I>context</> of the
    *  template processor currently processing the HTML template, for example the
    *  context object, the context action, Form data, via the HTTPState object
    *  that is passed in. <P>
    *
    *  Consequently, any changes to the context, for example calls to
    *  setContextObj or setContextAction, will be reflected in the
    *  templateprocessor that is calling the method.
    *
    *@param  parameters
    *@param  locale
    *@param  os
    */
   public void useProcessorService(Properties parameters, Locale locale, OutputStream os) {
      try {
         String serviceName = parameters.getProperty("service");
         String methodName = parameters.getProperty("method");
         Class serviceClass = Class.forName(serviceName);

         Class argTypes[] = {Properties.class, Locale.class, OutputStream.class};
         Object argObjects[] = {parameters, locale, os};
         Object newService = serviceClass.newInstance();

         if (newService instanceof ProcessorService) {
            ((ProcessorService) newService).setState(getState());
         } else if (newService instanceof BasicTemplateProcessor) {
            ((BasicTemplateProcessor) newService).setState(getState());
         }

         Method method = serviceClass.getMethod(methodName, argTypes);
         method.invoke(newService, argObjects);
      } catch (java.lang.ClassNotFoundException cnfe) {
         if (VERBOSE) {
            cnfe.printStackTrace();
         }
      } catch (java.lang.InstantiationException ie) {
         if (VERBOSE) {
            ie.printStackTrace();
         }
      } catch (java.lang.IllegalAccessException iae) {
         if (VERBOSE) {
            iae.printStackTrace();
         }
      } catch (java.lang.reflect.InvocationTargetException ite) {
         if (VERBOSE) {
            ite.printStackTrace();
         }
      } catch (java.lang.NoSuchMethodException nsme) {
         if (VERBOSE) {
            nsme.printStackTrace();
         }
      }
   }

   /**
    *  Gets the hTMLComponentFactory attribute of the BasicTemplateProcessor
    *  object
    *
    *@return    The hTMLComponentFactory value
    */
   public HTMLComponentFactory getHTMLComponentFactory() {
      if (componentFactory == null) {
         try {
            componentFactory = new HTMLComponentFactory();
         } catch (wt.services.applicationcontext.implementation.UnableToLoadServiceProperties utlsp) {
            utlsp.printStackTrace();
         }
      }
      return componentFactory;
   }

   /**
    *  Gets the wTAttribute attribute of the BasicTemplateProcessor object
    *
    *@return    The wTAttribute value
    */
   public WTAttribute getWTAttribute() {
      if (wtAttribute == null) {
         wtAttribute = new WTAttribute();
      }
      return wtAttribute;
   }

   /**
    *  Sets the pageContext attribute of the BasicTemplateProcessor object
    *
    *@param  pageContext  The new pageContext value
    */
   public void setPageContext(PageContext pageContext) {
      this.pageContext = pageContext;
   }

   /**
    *  Gets the pageContext attribute of the BasicTemplateProcessor object
    *
    *@return    The pageContext value
    */
   public PageContext getPageContext() {
      if (pageContext == null) {
         pageContext = new PageContext();
      }
      return pageContext;
   }

   /**
    *  Sets the javaScriptManager attribute of the BasicTemplateProcessor object
    *
    *@param  javaScriptManager  The new javaScriptManager value
    */
   public void setJavaScriptManager(JavaScriptManager javaScriptManager) {
      getPageContext().put(JavaScriptManager.ID, javaScriptManager);
   }

   /**
    *  Description of the Method
    *
    *@param  props   Description of the Parameter
    *@param  locale  Description of the Parameter
    *@param  os      Description of the Parameter
    */
   public void initJavaScriptManager(Properties props, Locale locale, OutputStream os) {
      getPageContext().put(JavaScriptManager.ID, new JavaScriptManager());
   }

   /**
    *  Gets the javaScriptManager attribute of the BasicTemplateProcessor object
    *
    *@return    The javaScriptManager value
    */
   public JavaScriptManager getJavaScriptManager() {
      JavaScriptManager jsm = (JavaScriptManager) (getPageContext().get(JavaScriptManager.ID));
      if (jsm == null) {
         jsm = new JavaScriptManager();
         getPageContext().put(JavaScriptManager.ID, jsm);
      }
      return jsm;
   }

   /*
    *  Use this script call to display example of date format next to an html form entry for date.
    */

   /**
    *  Gets the localizedDateFormatMessage attribute of the BasicTemplateProcessor
    *  object
    *
    *@param  parameters  Description of the Parameter
    *@param  locale      Description of the Parameter
    *@param  os          Description of the Parameter
    */
   public void getLocalizedDateFormatMessage(Properties parameters, Locale locale, OutputStream os) {
      PrintWriter out = getPrintWriter(os, locale);
      String message = WTMessage.getLocalizedMessage("wt.query.dateHelperResource",
            wt.query.dateHelperResource.DATE_INPUT_FORMAT,
            null, locale);
      out.print("(" + message + ")");
      out.flush();
   }

   /**
    *  Removes the LABEL_SEPARATOR characters in the given string and replaces
    *  them with spaces. This method is useful for allowing arguments with spaces
    *  to be passed in to Windchill scripts.
    *
    *@param  string_to_split  the string to split
    *@return                  the given string with spaces replacing the
    *      separators
    */
   public static String splitStringIntoTokens(String string_to_split) {
      if (string_to_split != null) {
         string_to_split = string_to_split.replace((LABEL_SEPARATOR.toCharArray())[0], ' ');
      }
      return string_to_split;
   }

   /**
    *  Method to output the appropriate charset encoding for the current locale.
    *  If the specific locale encoding value has been defined in wt.properties, it
    *  will be returned, otherwise the default value (also from wt.properties)
    *  will be returned. The property value is "wt.template.charset"
    *
    *@param  parameters  Description of the Parameter
    *@param  locale      Description of the Parameter
    *@param  os          Description of the Parameter
    */
   public static void getCharsetEncoding(Properties parameters, Locale locale, OutputStream os) {
      String base_encoding_String = "wt.template.charset";
      String locale_encoding_String = base_encoding_String + "." + locale;
      String encoding = DEFAULT_CHARSET;

      try {
         WTProperties properties = WTProperties.getLocalProperties();
         encoding = properties.getProperty(base_encoding_String, DEFAULT_CHARSET);
         encoding = properties.getProperty(locale_encoding_String, encoding);
      } catch (Throwable t) {}

      PrintWriter out = getLocalizedPrintWriter(os, locale);
      out.print(encoding);
      out.flush();
   }

   /**
    *  Method to output the value of a property from a property file. The
    *  particular subclass of Properties and the particular method to invoke on
    *  that subclass are given in the "class" and "method" parameters. This method
    *  assumes that the method specified by the method parameter is static. The
    *  property to be output is specified by "propertyName".
    *
    *@param  parameters  Description of the Parameter
    *@param  locale      Description of the Parameter
    *@param  os          Description of the Parameter
    */
   public static void outputPropertyFromPropertyFile(Properties parameters, Locale locale, OutputStream os) {
      String class_name = "wt.util.WTProperties";
      String method_name = "getLocalProperties";
      Properties properties = null;

      if (parameters != null) {
         // Test if outputting the property is dependent on whether or not a
         // particular component is installed
         String installed_key = parameters.getProperty("installed_key", null);
         if ((installed_key == null) ||
               (InstalledProperties.isInstalled(installed_key))) {
            try {
               String property_value = getPropertyFromPropertyFile(parameters);

               if (property_value != null) {
                  PrintWriter out = getLocalizedPrintWriter(os, locale);
                  out.print(HTMLEncoder.encodeForHTMLContent(property_value));
                  out.flush();
               }
            } catch (Exception e) {
               if (VERBOSE) {
                  System.err.println("outputPropertyFromPropertyFile() - exception caught attempting to " +
                        "invoke method '" + method_name + "' on class '" + class_name + "':");
                  e.printStackTrace(System.err);
               }
            }
         }
      }
   }

   /**
    *  Method to return the String value of a property from a property file. The
    *  particular subclass of Properties and the particular method to invoke on
    *  that subclass are given in the "class" and "method" parameters. This method
    *  assumes that the method specified by the method parameter is static. The
    *  property to be returned is specified by "propertyName".
    *
    *@param  parameters                     the Properties object containing the
    *      Class name, method name and property to return
    *@return                                The propertyFromPropertyFile value
    *@exception  ClassNotFoundException     Description of the Exception
    *@exception  IllegalAccessException     Description of the Exception
    *@exception  NoSuchMethodException      Description of the Exception
    *@exception  InvocationTargetException  Description of the Exception
    */
   public static String getPropertyFromPropertyFile(Properties parameters) throws ClassNotFoundException,
         IllegalAccessException, NoSuchMethodException, InvocationTargetException {
      String class_name = "wt.util.WTProperties";
      String method_name = "getLocalProperties";
      String property_value = null;

      if (parameters != null) {
         class_name = parameters.getProperty("class", class_name);
         method_name = parameters.getProperty("method", method_name);

         String property_name = parameters.getProperty("propertyName", null);
         if (property_name != null) {
            // Attempt to retrieve the method specified by the given
            // class and method parameters
            Class props_class = Class.forName(class_name);
            Method method = props_class.getMethod(method_name, null);
            Object result = method.invoke(null, null);
            if (result instanceof Properties) {
               property_value = ((Properties) result).getProperty(property_name, null);
            }
         }
      }

      return property_value;
   }

   /**
    *  Description of the Method
    *
    *@param  parameters  Description of the Parameter
    *@param  locale      Description of the Parameter
    *@param  os          Description of the Parameter
    */
   public static void outputLink(Properties parameters, Locale locale, OutputStream os) {
      if (parameters != null) {
         //  In a key in the installed.properties has been specified, determine
         //  if the specified component has been installed
         String installed_key = parameters.getProperty("installed_key", null);
         boolean can_print_row = (installed_key == null) ||
               InstalledProperties.isInstalled(installed_key);

         //  If the component has been installed or is part
         //  of Windchill core, print the table row
         if (can_print_row) {
            String label = splitStringIntoTokens(parameters.getProperty("label"));

            //Parse the label for special characters that indicate a break.
            int labelIndex;
            String display_label = label;

            if (display_label != null) {
               while ((labelIndex = display_label.indexOf(BREAK_SEPARATOR)) != -1) {
                  if (display_label.length() == 1) {
                     display_label = BREAK_TAG;
                  } else if (labelIndex == display_label.length() - 1) {
                     display_label = display_label.substring(0, labelIndex) + BREAK_TAG;
                  } else if (labelIndex == 0) {
                     display_label = BREAK_TAG + display_label.substring(labelIndex + 1);
                  } else {
                     display_label = display_label.substring(0, labelIndex) + BREAK_TAG + display_label.substring(labelIndex + 1);
                  }
               }
            }

            String icon = parameters.getProperty("image");
            String url = splitStringIntoTokens(parameters.getProperty("url"));
            String alt = splitStringIntoTokens(parameters.getProperty("alt", null));
            String img_border = parameters.getProperty("border", null);
            String onmouseover = splitStringIntoTokens(parameters.getProperty("onmouseover", null));
            String id = parameters.getProperty("id", null);
            String css_class = parameters.getProperty("css_class", null);
            String window_status = splitStringIntoTokens(parameters.getProperty("window_status", null));

            //  Build string containing attributes for
            //  the <A HREF> link tags
            String link_attrs = "";
            if (onmouseover != null) {
               link_attrs = "ONMOUSEOVER=\"" + onmouseover + "\"";
            }

            if (window_status != null) {
               link_attrs += " ONMOUSEOVER=\"window.status='" + window_status + "';return;\"";
            }

            if (id != null) {
               link_attrs += " ID=\"" + id + "\"";
            }

            if (css_class != null) {
               link_attrs += " CLASS=\"" + css_class + "\"";
            }

            String link = "";
            String image = null;
            if (icon != null) {
               //  Build string containing attributes for
               //  the <IMG> image tags
               String img_attrs = "";
               if (alt != null) {
                  img_attrs += "ALT=\"" + alt + "\"";
               }

               if (img_border != null) {
                  img_attrs += " BORDER=\"" + img_border + "\"";
               }
               if (VERBOSE) {
                  System.out.println("img tag src: " + icon);
               }
               image = "<IMG SRC=\"" + icon + "\" " + img_attrs + " ALIGN=BOTTOM>";
            }

            // If no URL has been given and a page location has been
            // given, use the page location to build a URL
            if (url == null) {
               String page_location = parameters.getProperty("page", null);
               if (page_location != null) {
                  try {
                     url = WindchillHome.getPageAsURL(page_location);
                  } catch (WTException wte) {
                     url = null;
                  }
               }
            }

            // If no URL has been given and a key has been given,
            // use the key to build a URL from an entry in
            // wt.properties
            if (url == null) {
               String key = parameters.getProperty("key", null);
               if (key != null) {
                  // Determine if URL needs to be localized
                  boolean localize =
                        Boolean.valueOf(parameters.getProperty("localize", "false")).booleanValue();

                  url = WindchillHome.getKeyAsURL(key, localize, locale);
               }
            }

            // If no URL has been given, check if a URL should
            // be built using URLProcessor.
            if (url == null) {
               boolean use_urlprocessor =
                     Boolean.valueOf(parameters.getProperty("use_urlprocessor", "false")).booleanValue();
               if (use_urlprocessor) {
                  try {
                     Properties props = parameters;
                     props.remove("label");
                     props.remove("image");
                     props.remove("url");
                     props.remove("alt");
                     props.remove("border");
                     props.remove("onmouseover");
                     props.remove("id");
                     props.remove("window_status");
                     props.remove("use_urlprocessor");

                     url = WindchillHome.getURLString(props);
                  } catch (WTException wte) {
                     url = null;
                  }
               }
            }

            //  If no URL has been given, check if a URL should
            //  be built by invoking a method on some class.  This
            //  code assumes that such a method would take two parameters -
            //  a properties object and a locale
            if (url == null) {
               if (VERBOSE) {
                  System.out.println("printTableEntry() - attempting to build " +
                        "URL by invoking a given method...");
               }
               try {
                  String method = parameters.getProperty("method");
                  if (method != null) {
                     if (VERBOSE) {
                        System.out.println("printTableEntry() - method to invoke is "
                               + method);
                     }

                     String class_name = parameters.getProperty("class");
                     Class[] param_types = {Properties.class, Locale.class};
                     Object[] params = {parameters, locale};
                     url = WindchillHome.getUrlFromMethod(class_name, method, param_types, null, params);
                  }
               } catch (WTException wte) {
                  if (VERBOSE) {
                     System.err.println("printTableEntry() - exception trying to " +
                           "retrieve URL by invoking a given method: ");
                     wte.printStackTrace(System.err);
                  }

                  url = null;
               }
            }

            //  Create the link using either the given image or
            //  the given label if the image is null
            boolean display_without_url =
                  Boolean.valueOf(parameters.getProperty("display_without_url", "true")).booleanValue();
            if (icon != null) {
               if (url != null) {
                  link = HtmlUtil.createLink(url, link_attrs, image);
               } else if (display_without_url) {
                  link = image;
               }
            } else {
               if (url != null) {
                  link = HtmlUtil.createLink(url, link_attrs, display_label);
               } else if (display_without_url) {
                  link = display_label;
               }
            }

            PrintWriter out = getLocalizedPrintWriter(os, locale);
            out.print(link);
            out.flush();
         }
      }
   }

   /**
    *  Returns true if Windhcill visualization services are enabled or false
    *  otherwise. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@return    Description of the Return Value
    */
   public static boolean visualizationIsEnabled() {
      return visualizationEnabled;
   }

   /**
    *  Displays a thumbnail and/or other appropriate visualization action links on
    *  the ObjectIdentificationVerbose html page of a Representable or Viewable
    *  object (WTPart, EPMDocument, WTDocument) <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  parameters  Description of the Parameter
    *@param  locale      Description of the Parameter
    *@param  os          Description of the Parameter
    */
   public void displayVisualData(Properties parameters, Locale locale, OutputStream os) {
      PrintWriter out = getPrintWriter(os, locale);
      try {
         Object obj = getContextObj();
         VisualizationRendererFactory visRendererFactory = new VisualizationRendererFactory();
         VisualizationRendererIfc visRenderer = visRendererFactory.getRenderer();
         visRenderer.displayCommonThumbnailButtonCmp(obj, getState(), locale, os);
         out.flush();
      } catch (WTException wte) {
         wte.printStackTrace();
         addToResponseFooters((LocalizableMessage) wte);
      } catch (java.lang.Exception e) {
         addToResponseFooters(new WTMessage(RESOURCE,
               enterpriseResource.ERROR_DISPLAYING_VISUAL_DATA, null));
         System.err.println("Unable to display thumbnail and/or other visualization data.");
         System.err.println("Possible causes: (1) Could not instantiate an instance of ");
         System.err.println("VisualizationHelperIfc because the value for the property ");
         System.err.println("wt.wvs.VisualizationHelper in the wt.properties file is invalid ");
         System.err.println("or the implementation class is constructed incorrectly, ");
         System.err.println("(2) visualization preferences are not set up correctly");
         if (VERBOSE) {
            e.printStackTrace();
         }
      }
   }

   /**
    *  Retrieves the value of a HTML color property from wt.properties
    *
    *@param  styleClass  Refers to the name of a wt.html.color property in
    *      wt.properties (for example, "bg-body")
    *@return             Unquoted string in the form: #NNNNNN * <BR>
    *      <BR>
    *      <B>Supported API: </B> true
    */
   public static String getWCColor(String styleClass) {
      String color = null;
      try {
         WTProperties properties = WTProperties.getLocalProperties();
         color = properties.getProperty("wt.html.color." + styleClass);
         if (color == null) {
            // Ignore exception
            if (VERBOSE) {
               System.err.println("BasicTemplateProcessor.getWCColor - could not retrieve " +
                     "color value for " + styleClass + " from wt.properties");
            }
                                return "#000000";
         }
                        else
                            return color;
      } catch (IOException ioe) {
         // Ignore exception
         if (VERBOSE) {
            System.err.println("BasicTemplateProcessor.getWCColor - exception retrieving " +
                  "color value for " + styleClass + " from wt.properties");
            ioe.printStackTrace();
         }
                        return "#000000";
      }
   }

   /**
    *  Retrieves the value of a HTML color property from wt.properties and writes
    *  it to the HTML page. This is for use as a Windchill script call. <p>
    *
    *  The script call must pass the required parameter "styleClass" which refers
    *  to the name of a wt.html.color property in wt.properties. <p>
    *
    *  The optional parameter "quotes" determines what kind of quotes will be
    *  included in the output. <p>
    *
    *  The output written to the html page will be a color number string. <BR>
    *  If quotes=none, the output will not have quotes: #NNNNNN <BR>
    *  If quotes=single, the output will be in single quotes: '#NNNNNN' <BR>
    *  If quotes=double or if there is no quotes parameter, the output will be in
    *  double quotes: "#NNNNNN" <p>
    *
    *  Example usage in an html template:
    *  <TDALIGN=RIGHT BGCOLOR=<SCRIPT LANGUAGE=Windchill>
    *    getWCColor styleClass=bg-navbar</SCRIPT> > <BR>
    *    <BR>
    *    <B>Supported API: </B> true
    *
    *@param  parameters       Description of the Parameter
    *@param  locale           Description of the Parameter
    *@param  os               Description of the Parameter
    *@exception  WTException  Description of the Exception
    */
   public void getWCColor(Properties parameters, Locale locale,
         OutputStream os) throws WTException {
      if ((parameters != null) &&
            ((parameters.getProperty("styleClass")) != null)) {
         PrintWriter out = getPrintWriter(os, locale);
         String styleClass = parameters.getProperty("styleClass");
         String quote = parameters.getProperty("quotes");
         if (quote != null && quote.length() > 0) {
            if (quote.equalsIgnoreCase("single")) {
               quote = "'";
            } else if (quote.equalsIgnoreCase("none")) {
               quote = "";
            }
         } else {
            quote = "\"";
         }
         out.print(quote + getWCColor(styleClass) + quote);
         out.flush();
      } else {
         Object[] param = {"styleClass parameter"};
         throw new WTException(null, RESOURCE,
               enterpriseResource.NOT_SPECIFIED, param);
      }
   }

   /**
    *  Retrieves the value of a HTML color property from wt.properties and writes
    *  it to the HTML page as a decimal integer. This is for use as a Windchill
    *  script call. <p>
    *
    *  The script call must pass the required parameter "styleClass" which refers
    *  to the name of a wt.html.color property in wt.properties. <p>
    *
    *  The output written to the html page will be an unquoted string in the form:
    *  nnnn... <p>
    *
    *  Example usage in an html template: <BODY BGCOLOR=<SCRIPT
    *  LANGUAGE=Windchill>getWCColor styleClass=bg-body</SCRIPT> > <BR>
    *  <BR>
    *  <B>Supported API: </B> true
    *
    *@param  parameters       Description of the Parameter
    *@param  locale           Description of the Parameter
    *@param  os               Description of the Parameter
    *@exception  WTException  Description of the Exception
    */
   public void getWCColorDec(Properties parameters, Locale locale,
         OutputStream os) throws WTException {
      if ((parameters != null) &&
            ((parameters.getProperty("styleClass")) != null)) {
         PrintWriter out = getPrintWriter(os, locale);
         String styleClass = parameters.getProperty("styleClass");
         String hexColorValue = getWCColor(styleClass);
         Integer decNum = Integer.valueOf(hexColorValue.substring(1), 16);
         String decColorValue = decNum.toString();
         out.print(decColorValue);
         out.flush();
      } else {
         Object[] param = {"styleClass parameter"};
         throw new WTException(null, RESOURCE,
               enterpriseResource.NOT_SPECIFIED, param);
      }
   }

   /**
    *  Gets the wCColorDec attribute of the BasicTemplateProcessor class
    *
    *@param  styleClass       Description of the Parameter
    *@return                  The wCColorDec value
    *@exception  WTException  Description of the Exception
    */
   public static String getWCColorDec(String styleClass) throws WTException {
      if (styleClass != null) {
         String hexColorValue = getWCColor(styleClass);
         Integer decNum = Integer.valueOf(hexColorValue.substring(1), 16);
         return decNum.toString();
      } else {
         Object[] param = {"styleClass parameter"};
         throw new WTException(null, RESOURCE, enterpriseResource.NOT_SPECIFIED, param);
      }
   }

   /**
    *  Retrieves the list of preferred HTML font faces from wt.properties
    *
    *@return    Unquoted string in the form: <first font face preference>[,
    *      <second font face preference>,
    *      <thirdfont face preference>
    *        , ...] <BR>
    *        <BR>
    *        <B>Supported API: </B> true
    */
   public static String getWCFontFamily() {
      String fontFamily = null;
      try {
         WTProperties properties = WTProperties.getLocalProperties();
         fontFamily = properties.getProperty("wt.html.font-family");
         if (fontFamily == null) {
            // Ignore exception
            if (VERBOSE) {
               System.err.println("BasicTemplateProcessor.getWCFontFamily - exception  retrieving " +
                     "font-family value from wt.properties");
            }
                                return "Arial, Helvetica, Geneva, Swiss, SunSans-Regular";
         }
                        else
                            return fontFamily;
      } catch (IOException ioe) {
         // Ignore exception
         if (VERBOSE) {
            System.err.println("BasicTemplateProcessor.getWCFontFamily - exception retrieving " +
                  "font-family value from wt.properties");
            ioe.printStackTrace();
         }
                        return "Arial, Helvetica, Geneva, Swiss, SunSans-Regular";
      }
   }

   /**
    *  Retrieves the list of preferred HTML font faces from wt.properties. For use
    *  as a Windchill script call. <p>
    *
    *  The output written to the HTML page will be a string in the form: "<first
    *  font face preference>[, <second font face preference>,
    *  <thirdfont face preference>
    *    , ...]" <p>
    *
    *    The optional parameter "quotes" determines what kind of quotes will be
    *    included in the output. <p>
    *
    *    If quotes=none, the output will not have quotes: <first font face
    *    preference>[, <second font face preference>,...] If quotes=single, the
    *    output will be in single quotes: '<first font face preference>[, <second
    *    font face preference>,...]' If quotes=double or if there is no quotes
    *    parameter, the output will be in double quotes: "<first font face
    *    preference>[, <second font face preference>,...]" <BR>
    *    <BR>
    *    <B>Supported API: </B> true
    *
    *@param  parameters  Description of the Parameter
    *@param  locale      Description of the Parameter
    *@param  os          Description of the Parameter
    */
   public void getWCFontFamily(Properties parameters, Locale locale,
         OutputStream os) {
      PrintWriter out = getPrintWriter(os, locale);
      String quote = "\"";
      if ((parameters != null)) {
         quote = parameters.getProperty("quotes");
         if (quote != null && quote.length() > 0) {
            if (quote.equalsIgnoreCase("single")) {
               quote = "'";
            } else if (quote.equalsIgnoreCase("none")) {
               quote = "";
            }
         } else {
            quote = "\"";
         }
      }
      out.print(quote + getWCFontFamily() + quote);
      out.flush();
   }

   /**
    *  Given an object, a URLActionDelegate and a HTTPState object, returns a URL
    *  string for an action. This method should be used instead of calling
    *  URLActionDelegate.URL() when the concrete URLActionDelegate class is
    *  unknown. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  delegate                       Description of the Parameter
    *@param  obj                            Description of the Parameter
    *@param  state                          Description of the Parameter
    *@return                                The uRLFromDelegate value
    *@exception  WTException                Description of the Exception
    *@exception  ClassNotFoundException     Description of the Exception
    *@exception  NoSuchMethodException      Description of the Exception
    *@exception  IllegalAccessException     Description of the Exception
    *@exception  InvocationTargetException  Description of the Exception
    */
   public static String getURLFromDelegate(URLActionDelegate delegate, Object obj, HTTPState state)
          throws WTException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
      // All this nonsense to instantiate the WildfireActionDelegateHelper class at
      // runtime is to avoid a compile-time mutual dependency between the wt/enterprise
      // and wt/wildfire packages
      String url = null;
      Class wildfireHelperClass = Class.forName("wt.wildfire.WildfireActionDelegateHelper");
      Class args[] = {wt.enterprise.URLActionDelegate.class};
      Method isWildfireURLActionDelegateMethod = wildfireHelperClass.getMethod("isWildfireURLActionDelegate", args);
      Object params[] = {delegate};
      if (((Boolean) isWildfireURLActionDelegateMethod.invoke(null, params)).booleanValue()) {
         Class args2[] = {wt.enterprise.URLActionDelegate.class, java.lang.Object.class,
               wt.templateutil.processor.HTTPState.class};
         Method getWildfireURLMethod = wildfireHelperClass.getMethod("getWildfireURL", args2);
         Object params2[] = {delegate, obj, state};
         url = (String) getWildfireURLMethod.invoke(null, params2);
      } else {
         if( delegate instanceof NavBarURLActionDelegate ) {
             ((NavBarURLActionDelegate)delegate).setState( state );
         }
         url = delegate.URL(obj);
      }
      return url;
   }

   /**
    *  Checks whether a given action is currently available for a given object.
    *  This method should be used instead of calling ActionDelegate.enableable()
    *  when the concrete ActionDelegate class is unknown. <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  action                         Description of the Parameter
    *@param  obj                            Description of the Parameter
    *@param  request                        Description of the Parameter
    *@return                                Description of the Return Value
    *@exception  WTException                Description of the Exception
    *@exception  ClassNotFoundException     Description of the Exception
    *@exception  NoSuchMethodException      Description of the Exception
    *@exception  IllegalAccessException     Description of the Exception
    *@exception  InvocationTargetException  Description of the Exception
    */
   public static boolean AccessOK(String action, Object obj, HTTPRequest request)
          throws WTException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
      boolean accessOK = false;
      if (action != null) {
         action = action.toUpperCase();
      }
      ActionDelegate checkAccess = null;
      try {
         ActionDelegateFactory checkAccessFactory = new ActionDelegateFactory();
         checkAccess = checkAccessFactory.getDelegate(action);
      } catch (UnableToCreateServiceException e) {
         if (VERBOSE) {
            System.err.println("URLProcessor.AccessOK()  : Unable to create the ActionDelegate");
            e.printStackTrace();
         }
      }

      if (checkAccess == null) {
         accessOK = true;
      } else {
         // All this nonsense to instantiate the WildfireActionDelegateHelper class at
         // runtime is to avoid a compile-time mutual dependency between the wt/enterprise
         // and wt/wildfire packages
         if (obj != null) {
            Class wildfireHelperClass = Class.forName("wt.wildfire.WildfireActionDelegateHelper");
            Class args[] = {wt.enterprise.ActionDelegate.class};
            Method isWildfireActionDelegateMethod = wildfireHelperClass.getMethod("isWildfireActionDelegate", args);
            Object params[] = {checkAccess};
            if (((Boolean) isWildfireActionDelegateMethod.invoke(null, params)).booleanValue()) {
               Class args2[] = {wt.enterprise.ActionDelegate.class, java.lang.Object.class,
                     wt.httpgw.HTTPRequest.class};
               Method isEnableableMethod = wildfireHelperClass.getMethod("isEnableable", args2);
               Object params2[] = {checkAccess, obj, request};
               accessOK = ((Boolean) isEnableableMethod.invoke(null, params2)).booleanValue();
            } else {
               accessOK = checkAccess.enableable(obj).booleanValue();
            }
         }
      }
      return accessOK;
   }

   /**
    *  Gets the preferenceValue attribute of the BasicTemplateProcessor class
    *
    *@param  node  Description of the Parameter
    *@param  key   Description of the Parameter
    *@return       The preferenceValue value
    */
   protected static String getPreferenceValue(String node, String key) {
      Preferences root = WTPreferences.root();
      WTPreferences myPrefs = (WTPreferences) root.node(node);
      myPrefs.setContextMask(PreferenceHelper.createContextMask());
      return myPrefs.get(key, "");
   }

   /**
    *
    * @deprecated
    *<BR>
    * As of R 8.0, MOR10 use the method DefaultTemplateProcessor.displayAttributeLabel() instead.
    */
   public void displayAttributeName(Properties parameters, Locale locale, OutputStream os) {
      boolean dbg = true;
      PrintWriter out = getPrintWriter(os, locale);
      Object theContextObject = getContextObj();
      IBAHolder ibah = null;
      String mode = null;
      DefaultAttributeContainer dac = null;
      AbstractValueView[] avv = null;
      Vector avvVec = null;
      Vector refVals = null;
      Vector attVals = null;
      Vector indVals = new Vector();
      String currentName = null;
      String attributeName = null;

      if (dbg == true) {
         System.out.println("^$^ DEBUG ^$^");
      }

      ResourceBundle messagesResource = ResourceBundle.getBundle(RESOURCE, locale);

      if (parameters != null) {
         attributeName = parameters.getProperty("attributeName");
         if (attributeName == null) {
            attributeName = "none";
         }
      } else {
         parameters = new Properties();

         parameters.put("attributeName", "none");
         attributeName = parameters.getProperty("attributeName");
      }

      try {
         if (!(theContextObject instanceof IBAHolder)) {
            System.out.println("theContextObject = " + theContextObject + " is not an IBAHolder.");
            return;
         }

         ibah = (IBAHolder) theContextObject;
         ibah = (IBAHolder) IBAValueHelper.service.refreshAttributeContainer(ibah, null, locale, null);
         dac = (DefaultAttributeContainer) ibah.getAttributeContainer();

         if (dac != null) {
            int j = 0;
            avv = dac.getAttributeValues();
            avvVec = new Vector();

            for (int i = 0; i < avv.length; i++) {
               avvVec.addElement(avv[i]);
            }

            Enumeration e = avvVec.elements();
            AbstractValueViewCollationKeyFactory avvckf = new AbstractValueViewCollationKeyFactory(locale);
            SortedEnumeration se = new SortedEnumeration(e, avvckf);
            avv = new AbstractValueView[se.size()];

            while (se.hasMoreElements()) {
               avv[j] = (AbstractValueView) se.nextElement();
               j++;
            }
         }
      } catch (wt.access.NotAuthorizedException nae) {
         nae.printStackTrace();
      } catch (RemoteException re) {
         re.printStackTrace();
      } catch (WTException wte) {
         wte.printStackTrace();
      }
      if (!attributeName.equals("none")) {
         if (avv != null && avv.length != 0) {
            for (int i = 0; i < avv.length; i++) {
               currentName = avv[i].getDefinition().getName();

               //  Display the attribute definition name
               if (currentName.equals(attributeName)) {
                  out.println("<b>" + HTMLEncoder.encodeForHTMLContent(avv[i].getDefinition().getDisplayName()) + ": </b>");

                  //  Diplay the attribute value
               }
            }
         } else {
            //  Attribute values array is empty
            if (dbg) {
               System.out.println("Attribute value array is empty.");
            }
         }
      }

      if (dbg == true) {
         System.out.println("^$^ END DEBUG ^$^");
      }
      out.flush();
   }

   /**
    * @deprecated
    *<BR>
    * As of R 8.0, MOR10 use the method DefaultTemplateProcessor.displayAttributeVal() instead.
    */
   public void displayAttributeValue(Properties parameters, Locale locale, OutputStream os) {
      boolean dbg = true;
      PrintWriter out = getPrintWriter(os, locale);
      Object theContextObject = getContextObj();
      IBAHolder ibah = null;
      String mode = null;
      DefaultAttributeContainer dac = null;
      AbstractValueView[] avv = null;
      Vector avvVec = null;
      Vector refVals = null;
      Vector attVals = null;
      Vector indVals = new Vector();
      String currentName = null;
      String attributeName = null;

      if (dbg == true) {
         System.out.println("^$^ DEBUG ^$^");
      }

      ResourceBundle messagesResource = ResourceBundle.getBundle(RESOURCE, locale);

      if (parameters != null) {
         attributeName = parameters.getProperty("attributeName");
         if (attributeName == null) {
            attributeName = "none";
         }
      } else {
         parameters = new Properties();

         parameters.put("attributeName", "none");
         attributeName = parameters.getProperty("attributeName");
      }

      try {
         if (!(theContextObject instanceof IBAHolder)) {
            System.out.println("theContextObject = " + theContextObject + " is not an IBAHolder.");
            return;
         }

         ibah = (IBAHolder) theContextObject;
         ibah = (IBAHolder) IBAValueHelper.service.refreshAttributeContainer(ibah, null, locale, null);
         dac = (DefaultAttributeContainer) ibah.getAttributeContainer();

         if (dac != null) {
            int j = 0;
            avv = dac.getAttributeValues();
            avvVec = new Vector();

            for (int i = 0; i < avv.length; i++) {
               avvVec.addElement(avv[i]);
            }

            Enumeration e = avvVec.elements();
            AbstractValueViewCollationKeyFactory avvckf = new AbstractValueViewCollationKeyFactory(locale);
            SortedEnumeration se = new SortedEnumeration(e, avvckf);
            avv = new AbstractValueView[se.size()];

            while (se.hasMoreElements()) {
               avv[j] = (AbstractValueView) se.nextElement();
               j++;
            }
         }
      } catch (wt.access.NotAuthorizedException nae) {
         nae.printStackTrace();
      } catch (RemoteException re) {
         re.printStackTrace();
      } catch (WTException wte) {
         wte.printStackTrace();
      }
      if (!attributeName.equals("none")) {
         if (avv != null && avv.length != 0) {
            for (int i = 0; i < avv.length; i++) {
               currentName = avv[i].getDefinition().getName();

               //  Get the attribute definition name
               if (currentName.equals(attributeName)) {
                  //  Diplay the attribute value
                  if (!(avv[i] instanceof ReferenceValueDefaultView)) {
                     try {
                        if (avv[i] instanceof URLValueDefaultView) {
                           URLValueDefaultView urlv = (URLValueDefaultView) avv[i];
                           out.println("<A HREF=\"" + HTMLEncoder.encodeURIForHTMLAttribute(urlv.getValue()) + "\">" + HTMLEncoder.encodeForHTMLContent(urlv.getDescription()) + "</A>");
                        } else {
                           out.println(IBAValueUtility.getLocalizedIBAValueDisplayString(avv[i], locale));
                        }
                     } catch (WTException e) {
                        e.printStackTrace();
                     }
                  } else {
                     //  This is a ReferenceValueDefaultView
                     out.println(((ReferenceValueDefaultView) avv[i]).getLiteIBAReferenceable().getIBAReferenceableDisplayString());
                     out.println("&nbsp;");
                  }
               }
            }
         } else {
            //  Attribute values array is empty
            if (dbg) {
               System.out.println("Attribute value array is empty.");
            }
         }
      }

      if (dbg == true) {
         System.out.println("^$^ END DEBUG ^$^");
      }
      out.flush();
   }

   /**
    *  Returns true if PDMLink is installed <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@return    Description of the Return Value
    */
   public static boolean PDMLinkIsInstalled() {
      return PDMLinkInstalled;
   }

   /**
    *  Outputs the icon associated with the context object to the given
    *  OutputStream. This method will output the image including any glyphs that
    *  should be associated with it.
    *  <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  parameters  the Properties passed in when this method is invoked;
    *      this parameter is not used by this method
    *@param  locale      the Locale in which the image will be displayed
    *@param  os          the OutputStream to write the image to.
    */
   public void getObjectDnDIcon(Properties parameters, Locale locale, OutputStream os) {
      getObjectIcon(parameters, locale, os);
   }

   /**
    *  No-op method to filter out copyright statements on templates so they aren't
    *  sent to browser. <p>
    *
    *  The following can be included in a source subtemplate to satisfy copyright
    *  requirements but will not be sent to the browser: <p>
    *
    *  &lt;script language=windchill&gt; <p>
    *
    *  bcwti Copyright(c) 2002 Parametric Technology Corporation, all rights
    *  reserved. ecwti <p>
    *
    *  &lt;/script&gt; <p>
    *
    *  This reduces the amount of data sent over the network and improves
    *  performance.
    *
    *@param  parameters  Description of the Parameter
    *@param  locale      Description of the Parameter
    *@param  os          Description of the Parameter
    */
   public void bcwti(Properties parameters, Locale locale, OutputStream os) {
   }

   /**
    *  No-op method to filter out comments on templates so they aren't sent to
    *  browser unless the wt.property "wt.template.commentsEnabled" is true. <p>
    *
    *  Comments on templates should be formatted as follows: <p>
    *
    *  &lt;SCRIPT language=Windchill&gt; <p>
    *
    *  comment text="&lt;my comment text&gt;" <p>
    *
    *  &lt;/SCRIPT&gt;
    *
    *@param  parameters  Description of the Parameter
    *@param  locale      Description of the Parameter
    *@param  os          Description of the Parameter
    */
   public void comment(Properties parameters, Locale locale, OutputStream os) {
      if (templateCommentsEnabled) {
         PrintWriter out = getPrintWriter(os, locale);
         out.println("<!--" + HTMLEncoder.encodeForHTMLContent(parameters.getProperty("text")) + "-->");
         out.flush();
      }
   }

   // This method checks if user agent is wildfire by instantiating WildfireActionDelegateHelper class at
   // runtime is to avoid a compile-time mutual dependency between the wt/enterprise
   // and wt/wildfire packages. Reference to property "com.ptc.windchill.cadx.standalone.enabled"
   // remove since determination to display Wildfire/Cadx pages will depend on
   // user preference. Code has been modified to solely depend on method call
   // to WildfireActionDelegateHelper class on whether it should or should not
   // display information in a CADX related style. New method called is
   // "isDisplayCadxEnabled". CADX is always displayed in ProE embedded browser.

   /**
    *  Gets the wildfireEnabled attribute of the BasicTemplateProcessor object
    *
    *@return                                The wildfireEnabled value
    *@exception  WTException                Description of the Exception
    *@exception  ClassNotFoundException     Description of the Exception
    *@exception  NoSuchMethodException      Description of the Exception
    *@exception  IllegalAccessException     Description of the Exception
    *@exception  InvocationTargetException  Description of the Exception
    */
   private boolean isWildfireEnabled() throws
         WTException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
      Class wildfireHelperClass = Class.forName("wt.wildfire.WildfireActionDelegateHelper");
      Class args2[] = {wt.templateutil.processor.HTTPState.class};
      Method CheckUserAgent = wildfireHelperClass.getMethod("isDisplayCadxEnabled", args2);
      Object params[] = {getState()};
      boolean isWildfire = ((Boolean) CheckUserAgent.invoke(null, params)).booleanValue();

      return isWildfire;
   }

   //dbhate : New methods to find out if a particular IconDelegate is WildfireIconDelegate

   /**
    *  Gets the wildfireIconDelegate attribute of the BasicTemplateProcessor class
    *
    *@param  a_IconDelegate  Description of the Parameter
    *@return                 The wildfireIconDelegate value
    */
   private static boolean isWildfireIconDelegate(IconDelegate a_IconDelegate) {
      if (isWildfireIconDelegateMethod == null) {
         return false;
      }

      try {
         Object params[] = {a_IconDelegate};
         return ((Boolean) isWildfireIconDelegateMethod.invoke(null, params)).booleanValue();
      } catch (Throwable t) {
         if (VERBOSE) {
            System.out.println(t.getLocalizedMessage());
         }

         return false;
      }
   }

   /**
    *  Gets the wildfireIconSelector attribute of the BasicTemplateProcessor class
    *
    *@param  a_IconDelegate  Description of the Parameter
    *@param  a_State         Description of the Parameter
    *@return                 The wildfireIconSelector value
    */
   private static IconSelector getWildfireIconSelector(IconDelegate a_IconDelegate, HTTPState a_State) {
      try {
         //Use of introspection API to avoid dependency on wildfire classes
         Class args[] = {HTTPState.class};
         Method getStandardIconSelectorMethod = a_IconDelegate.getClass().getMethod("getStandardIconSelector", args);
         Object params[] = {a_State};
         return (IconSelector) getStandardIconSelectorMethod.invoke(a_IconDelegate, params);
      } catch (Throwable t) {
         if (VERBOSE) {
            System.out.println(t.getLocalizedMessage());
         }

         return null;
      }
   }

   /**
    *  This template processor method generates an ID value. The
    *  BasicTemplateProcessor.IDPREFIX ("idprefix") property is supported. If
    *  "idprefix" is configured then the id format is <id prefix>_<context action>
    *  . <p>
    *
    *  If "idprefix" is not configured then the id format is <context action>. <p>
    *
    *  <id prefix> - If "idprefix" is specified is the HTTPState's context
    *  properties then it's value is used; otherwise if "idprefix" is specified as
    *  a method property then it's value is used. <p>
    *
    *  <context action> - the value returned by getContextAction() is used. <p>
    *
    *  NOTE: "id=" is not generated. The correct way to call this method from an
    *  html template file is: <p>
    *
    *  id="<Script language=Windchill><!-- getId idprefix="yourPrefix" -->
    *  </Script>"
    *
    *@param  parameters  Description of the Parameter
    *@param  locale      Description of the Parameter
    *@param  os          Description of the Parameter
    */
   public void getId(Properties parameters, Locale locale, OutputStream os) {
      try {
         //This is for ProE/Wildfire Trail File support.

         // Get default idprefix specified a a mehod paramater.
         String prefixDefault = parameters.getProperty(BasicTemplateProcessor.IDPREFIX);
         if (prefixDefault == null) {
            // Set to empty string.
            prefixDefault = "";
         }

         // Now get prefix override from context properties.
         String idprefix = this.getState().getContextProperties().getProperty(IDPREFIX, prefixDefault);

         // Set the id value.
         String idstr = HtmlUtil.createTrailId("", idprefix + "_" + getContextAction(), "", "");

         this.getPrintWriter(os, locale).print(idstr);
         this.getPrintWriter(os, locale).flush();
      } catch (Exception e) {
         e.printStackTrace();
      }
   }

   /**
    *  Description of the Method
    *
    *@param  object    Description of the Parameter
    *@param  gifImage  Description of the Parameter
    *@param  showURL   Description of the Parameter
    *@return           Description of the Return Value
    */
   public static String displayPendingChangeIndicator(Object object, String gifImage, boolean showURL) {
      String output = "";
      if( object instanceof Changeable2 && ((Changeable2)object).isHasPendingChange() ) {
         URLFactory url_factory = null;
         StringBuffer image_buf = new StringBuffer(1024);
         StringBuffer url_buf = new StringBuffer(1024);

         try {
            url_factory = new URLFactory();
         } catch (WTException wte) {
            wte.printStackTrace();
         }
         image_buf.append("<img src=\"").append(url_factory.getHREF(gifImage)).append("\" border=\"0\" alt=\""
                + WTMessage.getLocalizedMessage(RESOURCE, enterpriseResource.PENDING_CHANGE, null) + "\"").append("\">");
         if (showURL) {
            //Append the url buf and image buf together
            try {
               ReferenceFactory rf = new ReferenceFactory();
               String oidStr = rf.getReferenceString((Persistable) object);
               String enterprise_class = "wt.enterprise.URLProcessor";
               String method = "URLTemplateAction";
               HashMap map = new HashMap();
               map.put("action", "AssociatedChanges_plm");
               map.put("oid", oidStr);
               URL url = GatewayServletHelper.buildAuthenticatedURL(url_factory, enterprise_class, method, map);
               String href = url.toExternalForm();
               url_buf.append("<a href=").append(href).append(">").append(image_buf.toString()).append("</a>");
               ;
            } catch (Exception e) {
               e.printStackTrace();
            }
            output = url_buf.toString();
         } else {
            //display the image only
            output = image_buf.toString();
         }
      }
      return (output);
   }

   /**
    *  Constructs an HTML tag ID suitable for Pro/E trail file instrumentation.
    *  All HTML tags with which the user can interact should have such an ID
    *  attribute. The ids returned will have the form: <p>
    *
    *  &lt;product prefix&gt;:&lt;feature name&gt;:&lt;element type&gt;:&lt;unique
    *  id&gt; <BR>
    *  <p>
    *
    *  where:
    *  <ul>
    *    <li> Product prefix identifies the product with which this element is
    *    associated. </li>
    *    <li> Feature name identifies the feature with which this tag is
    *    associated.</li>
    *    <li> Element type is the type of HTML tag to which this ID belongs. </li>
    *
    *    <li> Unique id is a string designed to insure the tag is unique within
    *    the page. For the purposes of this method this is usually an arbitrary
    *    magic number or string. When an id is constructed programmatically and
    *    the tag is associated with a particular object, this can be a string
    *    identifying that object. </li>
    *  </ul>
    *  <p>
    *
    *  Any of the above four components may be missing from an id, although ids
    *  with all four are preferred wherever possible. Each of the four elements in
    *  the ID is input as a parameter to this script call. Any that are missing
    *  will be excluded from the ID. If all are missing, an empty string will be
    *  returned. <p>
    *
    *  We recommend that the total number of characters in all four components not
    *  exceed 65 characters (one trail file line) for trail file readability. <p>
    *
    *  NOTE: The trail id components should include only letter characters
    *  [A..Z,a..z] and digit characters [0..9]. <p>
    *
    *  Sample ids: <p>
    *
    *  &nbsp;&nbsp;&nbsp;PDML:MoreSearchOptions:TextLink:782241247 <p>
    *
    *  &nbsp;&nbsp;&nbsp;PDML:Tools:DropDown:98735 <p>
    *
    *  &nbsp;&nbsp;&nbsp;Tools:DropDown <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  parameters  Name/value pairs passed to the Windchill script call. The
    *      following parameters are recognized:
    *      <ul>
    *        <li> product - the product prefix. See wt.htmltuil.TrailIdConstants
    *        for possible values</li>
    *        <li> feature - string to identify the page feature with which the
    *        HTML element is associated</li>
    *        <li> elementType - string to identify the type of HTML element.
    *        Recommend using one of the values in wt.htmltuil.TrailIdConstants
    *        </li>
    *        <li> uniqueId - string to ensure id uniqueness
    *      </ul>
    *
    *@param  locale      The Locale to send to the invoked methods for
    *      localization.
    *@param  os          The output stream.
    */
   public void getTrailId(Properties parameters, Locale locale, OutputStream os) {
      PrintWriter out = getPrintWriter(os, locale);
      String productPrefix = parameters.getProperty("product");
      String featureName = parameters.getProperty("feature");
      String elementType = parameters.getProperty("elementType");
      String uniqueId = parameters.getProperty("uniqueId");
      out.print(HtmlUtil.createTrailId(productPrefix, featureName, elementType, uniqueId));
      out.flush();
   }

   /**
    *  Constructs a trlId tag suitable for Pro/E trail file instrumentation.
    *  All HTML tags with which the user can interact should have an ID
    *  attribute or trlId attribute. The trlIds returned will have the form: <p>
    *
    *  trlId=&lt;product prefix&gt;:&lt;feature name&gt;:&lt;element type&gt;:&lt;unique
    *  id&gt; <BR>
    *  <p>
    *
    *  where:
    *  <ul>
    *    <li> Product prefix identifies the product with which this element is
    *    associated. </li>
    *    <li> Feature name identifies the feature with which this tag is
    *    associated.</li>
    *    <li> Element type is the type of HTML tag to which this ID belongs. </li>
    *
    *    <li> Unique id is a string designed to insure the tag is unique within
    *    the page. For the purposes of this method this is usually an arbitrary
    *    magic number or string. When an id is constructed programmatically and
    *    the tag is associated with a particular object, this can be a string
    *    identifying that object. </li>
    *  </ul>
    *  <p>
    *
    *  Any of the above four components may be missing from a trlId, although trlIds
    *  with all four are preferred wherever possible. Each of the four elements in
    *  the trlId is input as a parameter to this script call. Any that are missing
    *  will be excluded from the trlId. If all are missing, an empty string will be
    *  returned. <p>
    *
    *  We recommend that the total number of characters in all four components not
    *  exceed 65 characters (one trail file line) for trail file readability. <p>
    *
    *  NOTE: The trail id components should include only letter characters
    *  [A..Z,a..z] and digit characters [0..9]. <p>
    *
    *  Sample trlIds: <p>
    *
    *  &nbsp;&nbsp;&nbsp;trlId=PDML:MoreSearchOptions:TextLink:782241247 <p>
    *
    *  &nbsp;&nbsp;&nbsp;trlId=PDML:Tools:DropDown:98735 <p>
    *
    *  &nbsp;&nbsp;&nbsp;trlId=Tools:DropDown <BR>
    *  <BR>
    *  <B>Supported API: </B> false
    *
    *@param  parameters  Name/value pairs passed to the Windchill script call. The
    *      following parameters are recognized:
    *      <ul>
    *        <li> product - the product prefix. See wt.htmltuil.TrailIdConstants
    *        for possible values</li>
    *        <li> feature - string to identify the page feature with which the
    *        HTML element is associated</li>
    *        <li> elementType - string to identify the type of HTML element.
    *        Recommend using one of the values in wt.htmltuil.TrailIdConstants
    *        </li>
    *        <li> uniqueId - string to ensure id uniqueness
    *      </ul>
    *
    *@param  locale      The Locale to send to the invoked methods for
    *      localization.
    *@param  os          The output stream.
    */
   public void getTrlIdAttribute(Properties parameters, Locale locale, OutputStream os) {
      PrintWriter out = getPrintWriter(os, locale);
      String productPrefix = parameters.getProperty("product");
      String featureName = parameters.getProperty("feature");
      String elementType = parameters.getProperty("elementType");
      String uniqueId = parameters.getProperty("uniqueId");
      out.print(HtmlUtil.createTrlIdAttribute(productPrefix, featureName, elementType, uniqueId));
      out.flush();
   }

   /**
    *  Gets the dndMicroAppletEnabled attribute of the BasicTemplateProcessor
    *  class.
    *  @deprecated
    *@return    The dndMicroAppletEnabled value
    */
   protected static boolean isDndMicroAppletEnabled() {
      return false;
   }

   /**
    *  Description of the Method
    *
    *@param  parameters       Description of the Parameter
    *@param  locale           Description of the Parameter
    *@param  os               Description of the Parameter
    *@exception  WTException  Description of the Exception
    */
   public void displayOrganizationIdentifier(Properties parameters, Locale locale, OutputStream os)
          throws WTException {
       Object context_object = getContextObj();
       if (!(context_object instanceof OrganizationOwned)) {
          return;
       }

       OrganizationOwned org_owned = (OrganizationOwned)context_object;
       WTContainerRef container_ref = null;
       if (org_owned instanceof WTContained) {
          container_ref = ((WTContained)org_owned).getContainerReference();
       }

       if (!OrganizationServicesHelper.isOrganizationDisplayEnabled(org_owned.getClass())) {
          return;
       }

       String display_value = null;
       WTOrganization organization;
       boolean enforce = SessionServerHelper.manager.setAccessEnforced(false);
       try {
          organization = org_owned.getOrganization();
          if (organization != null) {
             display_value = organization.getUniqueIdentifier();
             if (display_value == null) {
                display_value = organization.getName();
             }
          }
       }
       finally {
          SessionServerHelper.manager.setAccessEnforced(enforce);
       }

       String label = "";
       if (SumaFacade.getInstance().isSupplierPart(org_owned.getClass())) {
          label = SumaFacade.getInstance().getSupplierIdLabel(org_owned.getClass(), /*colon*/true).getLocalizedMessage(locale);
       }
       else {
          LocalizableMessage localizedLabel = OrganizationServicesHelper.OrganizationLabel(locale);
          label = localizedLabel.getLocalizedMessage(locale).concat(":");
       }
       String link = null;
       if (AccessControlHelper.manager.hasAccess(organization, AccessPermission.READ)) {
          boolean popup = true;
          String oid_str = null;
          if (SumaFacade.getInstance().isSupplierPart(org_owned.getClass())) {
             // TBD: need to figure out how to get org container
             ObjectReference supplier_ref = SumaFacade.getInstance().getSupplierRef(WTContainerHelper.service.getOrgContainerRef((WTPart) org_owned), (WTPart)org_owned);
             if (supplier_ref != null) {
               oid_str = new ReferenceFactory().getReferenceString(supplier_ref);
               popup = false;
             }
          }
          if (oid_str == null) {
             oid_str = new ReferenceFactory().getReferenceString(organization);
          }
          URLFactory urlFactory = getState().getURLFactory();
          String enterprise_class = "wt.enterprise.URLProcessor";
          HashMap map = new HashMap( );
          map.put("action", "ObjProps");
          map.put("oid", oid_str);
          URL url = GatewayServletHelper.buildAuthenticatedURL(urlFactory,enterprise_class,"URLTemplateAction",map);
          if (popup) {
            link = wt.htmlutil.PopupHTMLUtils.getLaunchWindowURLWrapper(url.toExternalForm(),"",600,430,"directories=no,location=no,menubar=no,scrollbars=yes,status=yes,toolbar=no,resizable=yes");
          }
          else {
            link = url.toExternalForm();
          }
       }

       PrintWriter out = TemplateOutputStream.getPrintWriter(os, locale);

       out.println("      <TD align=right nowrap class=\"propTitle\"> <B>");
       out.print("                   ");
       out.print(label);
       if (PDMLinkIsInstalled()) {
          out.println("&nbsp;&nbsp;&nbsp;");
       }
       out.println("</B>");

       out.println("      </TD>");
       out.print("      <TD align=left class=\"propValue\">");
       if (link == null) {
          out.print(HTMLEncoder.encodeForHTMLContent(display_value));
       }
       else {
          out.print("<a href=\"");
          out.print(link);
          out.print("\" ");
          out.print(HtmlUtil.createTrlIdAttribute("WNC","DETAIL", "ORG", organization));
          out.print(">");
          out.print(HTMLEncoder.encodeForHTMLContent(display_value));
          out.print("</a>");
       }
       out.println("      </TD>");
       out.flush();
   }

   /**
    *  Method to display the template property of an object along with its
    *  availability for use. This method is common to all Templateable objects
    *  that is objects that implement interface Templateable like Parts,Documents
    *  etc. All Templateable objects should have their own service or a separate
    *  TemplateProcessor, when more methods that act on Templateables need to be
    *  added.
    *
    *@param  locale
    *@param  os
    *@param  properties       Description of the Parameter
    *@exception  WTException  Description of the Exception
    */
   public void getTemplateDisplayGeneral(Properties properties, Locale locale, OutputStream os)
          throws WTException {
      String label = null;
      String value = null;
      PrintWriter out = TemplateOutputStream.getPrintWriter(os, locale);
      Templateable object = (Templateable) getContextObj();

      if (object != null) {
         TemplateInfo template = object.getTemplate();
         if (template != null) {
            if (template.isTemplated()) {
               label = WTMessage.getLocalizedMessage(
                     RESOURCE,
                     enterpriseResource.TEMPLATE,
                     null, locale);
               if (template.isEnabled()) {
                  value = WTMessage.getLocalizedMessage(
                        RESOURCE,
                        enterpriseResource.TEMPLATE_AVAILABLE,
                        null, locale);
               } else {
                  value = WTMessage.getLocalizedMessage(
                        RESOURCE,
                        enterpriseResource.TEMPLATE_UNAVAILABLE,
                        null, locale);
               }
               out.println("      <TD ALIGN=RIGHT WIDTH=10% NOWRAP class=\"propTitle\"> <B>");
               out.println("      " + HTMLEncoder.encodeForHTMLContent(label) + "</B>");
               out.println("      </TD>");
               out.println("      <TD ALIGN=LEFT WIDTH=40% class=\"propValue\">");
               out.println("      " + HTMLEncoder.encodeForHTMLContent(value));
               out.println("      </TD>");
               out.flush();
            }
         }
      }
   }

   /**
    *  Get container value as contextual value. If not found then, if the context
    *  object is WTContained object, return back it's container.
    *
    *@param  parameters       Description of the Parameter
    *@param  locale           Description of the Parameter
    *@return                  The wTContainerRef value
    *@exception  WTException  Description of the Exception
    */
   public WTContainerRef getWTContainerRef(Properties parameters, Locale locale) throws WTException {
      WTContainerRef containerRef = null;
      WTContainer container = null;
      Object contextObject = getContextObj();

      // First try to get container based upon the contextual value of container.
      if (contextObject instanceof WTContainer) {
         // If context object is a WTContainer object, it's container name is name of it's parent container. So
         // exclude WTContainer object from such look-up. Handling for WTContainer is addressed with
         // alternate logic below.
      } else {
         if (parameters != null && parameters.containsKey(CONTAINER_NAME)) {
            String containerName = parameters.getProperty(CONTAINER_NAME);
            if (VERBOSE) {
               System.out.println("VERBOSE: getWTContainerRef " + " containerName = " + containerName);
            }
            container = getWTContainerByName(containerName);
         }
      }

      // If no container could be found from above logic, see whether we could get it directly from the
      // WTContained object.
      if (container == null) {
         if (contextObject instanceof WTContained) {
            if (contextObject instanceof WTContainer) {
               // If it's a container object itself, then return that directly. (BTW, the WTContainer object
               // also has getContainer() method which returns it's parent container. We don't want to call
               // this method here.)
               container = (WTContainer) contextObject;
            } else {
               container = ((WTContained) contextObject).getContainer();
            }
         }
      }

      // If container could not be found from above logic and if the context object is EPMWorkspace,
      // get it's associated container.
      if (container == null) {
         if (contextObject instanceof EPMWorkspace) {
             container = ((EPMWorkspace)contextObject).getContainer();
            if (VERBOSE) {
                String containerName = "";
                if (container != null) {
                    containerName = container.getName();
                }
               System.out.println("VERBOSE: getWTContainerRef Workspace container is " + containerName);
            }
         } else {
             // The objectPropertyValueString method below throws null pointer exception if context object is null,which
             // is possible is some cases. Check for it.
             if (getContextObj() != null) {
                // If object's associated container is a WTLibrary, the value of WTContained.CONTAINER attribute
                // as returned by objectPropertyValueString() call is in the form of
                // "Library library_name" ? Shouldn't it be in the form of an oid as
                // "wt.inf.library.WTLibrary:12345". That's what is returned for PDMLink product.
                // This logic will not work for the WTLibrary scenario.
                // Needs to be further investigated. Perhaps it's not safe to do introspection
                // based upon this attribute for a object to get the referenced container.
                // The reason is that the 'container' property name is not fool-proof. It is
                // tightly coupled to what name is given to the association name in Rose and so
                // is not safe to use.
                //
                // @todo This code should be removed in future if no API is using this.
                String associatedContainerPropertyName = WTContained.CONTAINER; // value is "container"
                String associatedContainerOid = objectPropertyValueString(associatedContainerPropertyName, null, locale);
                if (VERBOSE) {
                   System.out.println("VERBOSE: getWTContainerRef " + " associatedContainerOid = " + associatedContainerOid);
                }
                if (associatedContainerOid != null && associatedContainerOid.length() > 0) {
                   container = getContainerByOid(associatedContainerOid);
                }
             }
         }
      }

      // Get the contaier reference from this container
      if (container != null) {
         try {
            containerRef = WTContainerRef.newWTContainerRef(container);
         } catch (WTException wte) {}
      }
      return containerRef;
   }

   /**
    *  For the specified container name, return back the WTContainer object.
    *
    *@param  containerName    Description of the Parameter
    *@return                  The wTContainerByName value
    *@exception  WTException  Description of the Exception
    */
   public WTContainer getWTContainerByName(String containerName) throws WTException {
      if (containerName == null || containerName.equals("")) {
         return null;
      }
      QuerySpec querySpec = new QuerySpec(WTContainer.class);
      SearchCondition sc = new SearchCondition(WTContainer.class, WTContainer.NAME,
            SearchCondition.EQUAL, containerName);
      querySpec.appendWhere(sc, 0, -1);
      QueryResult queryResult = PersistenceHelper.manager.find(querySpec);

      if (queryResult == null) {
         return null;
      }

      if (queryResult.size() > 1) {
         if (VERBOSE) {
            System.out.println("VERBOSE: getWTContainerRef " + "More than one containers found for specified container name " + containerName);
            System.out.println("VERBOSE: getWTContainerRef " + "The first one found from the query is used.");
         }
      }
      if (queryResult.hasMoreElements()) {
         return (WTContainer) queryResult.nextElement();
      } else {
         return null;
      }
   }

   /**
    *  For the specified container oid, return the WTContainer object.
    *
    *@param  containerOid  Description of the Parameter
    *@return               The containerByOid value
    */
   private WTContainer getContainerByOid(String containerOid) {
      WTContainer container = null;

      if (containerOid != null && containerOid.length() > 0) {
         ReferenceFactory rf = new ReferenceFactory();
         try {
            WTReference ref = rf.getReference(containerOid);
            Object refObject = ref.getObject();
            if (refObject instanceof WTContainer) {
               container = (WTContainer) refObject;
            }
         } catch (Exception e) {
            if (VERBOSE) {
               e.printStackTrace();
            }
         }
      }
      return container;
   }

   /**
    *  Method to display the enabled version property of a templateable object
    *  that is a template
    *
    *@param  locale
    *@param  os
    *@param  properties       Description of the Parameter
    *@exception  WTException  Description of the Exception
    */
   public void getEnabledVersion(Properties properties, Locale locale, OutputStream os)
          throws WTException {
      String enabledVerStr = "";
      String label = null;
      QueryResult result = null;
      PrintWriter out = TemplateOutputStream.getPrintWriter(os, locale);
      Templateable object = (Templateable) getContextObj();
      if (object != null) {
         TemplateInfo template = ((Templateable) object).getTemplate();
         if (!(template != null && template.isTemplated())) {
            return;
         }

         result = VersionControlHelper.service.allIterationsOf(((Iterated) object).getMaster());
         while (result.hasMoreElements()) {
            Object currObj = (Object) result.nextElement();
            template = ((Templateable) currObj).getTemplate();

            if (template.isEnabled()) {
               enabledVerStr = VersionControlHelper.getVersionIdentifier((Versioned) currObj).getSeries().getValue() + "." +
                     VersionControlHelper.getIterationIdentifier((Versioned) currObj).getSeries().getValue();
               break;
            } else {
               enabledVerStr = WTMessage.getLocalizedMessage(
                     RESOURCE,
                     enterpriseResource.NONE,
                     null, locale);
            }
         }

         label = WTMessage.getLocalizedMessage(
               RESOURCE,
               enterpriseResource.ENABLED_VER,
               null, locale);
         out.println("      <TD ALIGN=RIGHT WIDTH=10% NOWRAP class=\"propTitle\"> <B>");
         out.println("      " + HTMLEncoder.encodeForHTMLContent(label) + "</B>");
         out.println("      </TD>");
         out.println("      <TD ALIGN=LEFT WIDTH=40% class=\"propValue\">");
         out.println("      " + HTMLEncoder.encodeForHTMLContent(enabledVerStr));
         out.println("      </TD>");
         out.flush();
      }
   }

   /*  THis method returns the glyphs array that is created during static initialization.  THis is
    *  currently being used by the image icon SCA in the DCA Search
    *
    */
   public static String [][] getGlyphsArray()
   {
      return glyphImages;
   }
   // EPMDocument have a special case in identify the object type
   // icon for family EPMDocuments. This method will generate the
   // addition image text if the EPMDocument has family relationships.
   // This is not just for ProE authored EPMDocument but any CAD engine
   // that builds Contains/ContainedIn relationships.
   public static String getGenericInstanceIconImgTag(Object obj) throws WTException {
      String familyGlyph = "";
      if ( obj instanceof EPMDocument ) {
         // See if document is a generic or an instances
         EPMDocument doc = (EPMDocument)obj;
         if ((doc.isGeneric()) && (doc.isInstance())) {
            familyGlyph = getFamilyGlyph("com/ptc/core/htmlcomp/images/intermediate_generic9x9.gif");
         }
         else if (doc.isGeneric()) {
            familyGlyph = getFamilyGlyph("com/ptc/core/htmlcomp/images/generic9x9.gif");
         }
         else if (doc.isInstance()) {
            familyGlyph = getFamilyGlyph("com/ptc/core/htmlcomp/images/instance9x9.gif");
         }
      }
      return familyGlyph;
   }
   private static String getFamilyGlyph(String glyphLocation) throws WTException {
      StringBuffer glyph = new StringBuffer();
      glyph.append("<IMG BORDER=0 src=\"");
      glyph.append(getURLFactory().getHREF(glyphLocation));
      glyph.append("\" style=\"z-index:2 visibility:visible; position:relative; left:2px; top:2px;\">");
      return glyph.toString();
   }

   public static boolean displayFolderObject( WTContainer container, WTObject obj)
        throws WTException
   {
       boolean retval = false;
       if (obj instanceof SubFolder ) {
           SubFolder folder = (SubFolder) obj;
           if ( WTContainerHelper.isContainedBy((WTContained)folder, WTContainerHelper.service.getClassicRef()) )
                   retval = true;
           if ( WTContainerHelper.isContainedBy((WTContained)folder, WTContainerHelper.service.getExchangeRef()) ) {
                EPMWorkspace ws = null;
                try {
                    QueryResult qr = PersistenceHelper.manager.navigate(folder,
                            WorkspaceFolder.WORKSPACE_ROLE,
                            WorkspaceFolder.class);
                    if (!qr.hasMoreElements()) {
                        retval = true;
                    }
                    else
                        retval = false;
                } catch (Exception e) {
                    retval = false;
                }
           }
       }
       else if ( WTContainerHelper.isContainedBy((WTContained)obj, WTContainerHelper.service.getClassicRef()) )
           retval = true;

       return retval;
   }
      /**
    *  dysplay Go To Latest iteration link on the attribute block
    *
    *@param  parameters       Description of the Parameter
    *@param  locale           Description of the Parameter
    *@param  os               Description of the Parameter
    *@exception  WTException  Description of the Exception
    */
   public void displayGoToLatestLink(Properties parameters, Locale locale, OutputStream os)
         throws WTException
   {
      PrintWriter out = TemplateOutputStream.getPrintWriter(os, locale);
      Iterated object = (Iterated) getContextObj();

      ConfigSpec cs = ConfigHelper.service.getConfigSpecFor(object);
      QueryResult qr = ConfigHelper.service.filteredIterationsOf(object.getMaster(), cs);
      if(qr.size() != 1) {
         return;
      }
      Iterated latest = (Iterated) qr.nextElement();

      if(PersistenceHelper.equals(object, latest)) {
         return;
      }

      URLFactory urlFactory = getState().getURLFactory();

      HashMap map = new HashMap();
      String action = getState().getQueryData().getProperty("action");
      map.put("action", action);
      String oid = PersistenceHelper.getObjectIdentifier(latest).getStringValue();
      map.put("oid", oid);

      String label = WTMessage.getLocalizedMessage(RESOURCE, enterpriseResource.GO_TO_LATEST, null, locale);
      String url = GatewayServletHelper.buildAuthenticatedURL(urlFactory, "wt.enterprise.URLProcessor", "URLTemplateAction", map).toExternalForm();
      String link = HtmlUtil.createLink(url, null, label);
                out.println(link);
      out.flush();
   }

   /**
    *  A helper method for displaying attributes.
    *  This method ensures only unique key-value pairs are displayed under the Attributes table.
    *
    *@param  avv                        AbstractValueView object
    *@return boolean                    valueExist
    */
   private boolean isValueExist(AbstractValueView avv) {
        if(!uniqueAttributeKeyValues.containsKey(avv.getDefinition().getDisplayName())) {
            uniqueAttributeKeyValues.put(avv.getDefinition().getDisplayName(), avv.getLocalizedDisplayString());
            return false;
        }
        else {
            String value = (String)uniqueAttributeKeyValues.get(avv.getDefinition().getDisplayName());
            if(!value.equals(avv.getLocalizedDisplayString())) {
                uniqueAttributeKeyValues.put(avv.getDefinition().getDisplayName(), avv.getLocalizedDisplayString());
                return false;
            }
            else {
                return true;
            }
        }
    }
}
