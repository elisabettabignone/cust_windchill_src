/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.enterprise;

import wt.util.resource.*;

@RBUUID("wt.enterprise.enterpriseResource")
public final class enterpriseResource_it extends WTListResourceBundle {
   @RBEntry("Errore durante l'inizializzazione di \"{0}\".")
   public static final String ERROR_INITIALIZING = "0";

   @RBEntry("Nessuna specifica di \"{0}\" nel file HTML.")
   public static final String NOT_SPECIFIED = "1";

   @RBEntry("Link")
   public static final String LINKS = "10";

   @RBEntry("Generazione della pagina Web in corso.")
   public static final String PAGE_GENERATION_CONTINUES = "100";

   @RBEntry("Generazione della pagina Web interrotta.")
   public static final String PAGE_GENERATION_HALTED = "101";

   @RBEntry("Nessun elemento influenzato dall'ordine di modifica")
   public static final String NO_AFFECTS_RETURNED = "102";

   @RBEntry("Nessuna operazione di modifica restituita")
   public static final String NO_CHANGEACTIVITIES_RETURNED = "103";

   @RBEntry("L'ordine di modifica correlato non è stato trovato")
   public static final String NO_CHANGEORDER_RETURNED = "104";

   @RBEntry("Nessun ordine di modifica restituito")
   public static final String NO_CHANGEORDERS_RETURNED = "105";

   @RBEntry("Nessuna richiesta di modifica restituita")
   public static final String NO_CHANGEREQUESTS_RETURNED = "106";

   @RBEntry("Nessun elemento identificato dalla richiesta di modifica")
   public static final String NO_IDENTIFIES_RETURNED = "107";

   @RBEntry("Nessun elemento revisionato dall'operazione di modifica")
   public static final String NO_REVISEDOBJS_RETURNED = "108";

   @RBEntry("Si è verificato un errore nel caricamento della classe {0} in {1}")
   public static final String ERROR_LOADING_CLASS = "109";

   @RBEntry("Il modello di elaborazione dell'oggetto è nullo.")
   public static final String NULL_CONTEXT_OBJECT = "11";

   @RBEntry("ERRORE: mediante il processore URL è possibile elaborare solo richieste GET HTTP")
   public static final String INVALID_HTTP_REQUEST = "110";

   @RBEntry("** La parte non è descritta da alcuna origine **")
   public static final String NO_BUILD_SOURCE = "111";

   @RBEntry("Sfoglia schedari")
   public static final String ALL_CABINETS_LABEL = "112";

   @RBEntry("Posizione attuale:")
   public static final String YOU_ARE_HERE = "113";

   @RBEntry(">")
   public static final String CONTEXT_PATH_SEPARATOR = "114";

   @RBEntry("/")
   public static final String ROOT_CABINET = "115";

   @RBEntry("** Non è stato trovato alcuno schedario **")
   public static final String NO_CABINETS_MESSAGE = "116";

   @RBEntry("<b>Posizione operazione:</b>")
   public static final String WORKING_LOCATION_NAME = "117";

   @RBEntry("Copia in modifica di {0}")
   public static final String WORKING_COPY_TITLE = "118";

   @RBEntry("Check-Out effettuato da {0}")
   public static final String CHECKED_OUT_BY_TITLE = "119";

   @RBEntry("nullo")
   public static final String NULL = "12";

   @RBEntry("Attributo")
   public static final String ATTRIBUTE_NAME_LABEL = "120";

   @RBEntry("Valore")
   public static final String ATTRIBUTE_VALUE_LABEL = "121";

   @RBEntry("Contesto")
   public static final String ATTRIBUTE_DEPENDENCY_LABEL = "122";

   @RBEntry("** Non sono stati trovati attributi associati **")
   public static final String NO_ATTRIBUTES_MESSAGE = "123";

   @RBEntry("Errore durante la copia non persistente dell'oggetto {0}.")
   public static final String COPY_NON_PERSISTENT = "124";

   @RBEntry("La copia dell'oggetto {0} non è stata resa persistente a causa di un errore verificatosi durante il relativo processo.")
   public static final String COPY_PERSISTENCE = "125";

   @RBEntry("Errore durante la copia degli attributi dall'originale ad una nuova copia dell'oggetto.")
   public static final String COPY_ATTRIBUTES = "126";

   @RBEntry("L'oggetto da copiare è nullo.")
   public static final String COPY_NULL_OBJECT = "127";

   @RBEntry("Usando i parametri predefiniti le regole di copiatura non sono state trovate né in wt.properties, né in dynamic.")
   public static final String COPY_RULES_WARNING = "128";

   @RBEntry("Regola di copiatura non valida.")
   public static final String COPY_INVALID_RULE = "129";

   @RBEntry("Impossibile visualizzare il valore \"{0}\".")
   public static final String UNDISPLAYABLE_VALUE = "13";

   @RBEntry("Impossibile copiare un oggetto della classe {0}, non supportato.")
   public static final String COPY_BAD_CLASS = "130";

   @RBEntry("L'oggetto {0} fornito è nullo, impossibile continuare.")
   public static final String COPY_NULL_PARAMETER = "131";

   @RBEntry("Errore, risultati inattesi della interrogazione (di {0}) trovati per {1}.")
   public static final String COPY_BAD_QUERY = "132";

   @RBEntry("Salvato da")
   public static final String MADEFROM_SUBTITLE = "133";

   @RBEntry("Salvato con nome")
   public static final String MADEINTO_SUBTITLE = "134";

   @RBEntry("** Nessuno **")
   public static final String MADEFROM_EMPTY = "135";

   @RBEntry("Cronologia salvataggi")
   public static final String MADE_FROM_URL_LABEL = "136";

   @RBEntry("Il salvataggio con nome di {0} è fallito. Controllare il log per ulteriori informazioni.")
   public static final String SAVEAS_FAILURE = "137";

   @RBEntry("Salva con nome")
   public static final String SAVE_AS_URL_LABEL = "138";

   @RBEntry("Il salvataggio con nome di {0} è stato completato.")
   public static final String SAVEAS_SUCCESS = "139";

   @RBEntry("Aggiorna")
   public static final String UPDATE = "14";

   @RBEntry("{0} ({1})")
   @RBArgComment0("The qualified version, i.e. \"A.1\"")
   @RBArgComment1("The view the part is in, i.e. \"Engineering\"")
   public static final String PART_VERSION_DISPLAY = "140";

   @RBEntry("Elementi descritti")
   public static final String DESCRIBES_HEADER = "141";

   @RBEntry("Elementi descritti da {0}")
   public static final String DESCRIBES_TITLE = "142";

   @RBEntry("Guida")
   public static final String DEFAULT_HELP_LABEL = "143";

   @RBEntry("Nello schedario personale")
   public static final String IN_PERSONAL_CABINET_LABEL = "144";

   @RBEntry("<BR><H3>\nTask: Recupero di TemplateProcessor da Factory\n</H3><BR><B>\nFactory: {0}\n<BR>\nSelettore stringa: {1}\n<BR>\nSelettore classe: {2}\n<BR>\nTipo di errore: la voce corrispondente nel file delle proprietà non esiste\n<B><BR>\n")
   public static final String SNFE_TEMPLATEPROCESSOR = "145";

   @RBEntry("<BR><H3>\nTask: recupero di TemplateProcessor da Factory\n</H3> <BR> <B>\nFactory: {0}\n<BR>\nSelettore stringa: {1}\n <BR>\nSelettore classe: {2}\n <BR>\nTipo di errore: impossibile trovare dati nel file delle proprietà oppure istanziare/trovare la classe indicata nel file delle proprietà\n\n <B><BR><BR>\n")
   public static final String UTCSE_TEMPLATEPROCESSOR = "146";

   @RBEntry("Nome")
   public static final String EPM_NAME = "147";

   @RBEntry("Numero")
   public static final String EPM_NUMBER = "148";

   @RBEntry("Collocato")
   public static final String EPM_PLACED = "149";

   @RBEntry("Visualizza")
   public static final String VIEW = "15";

   @RBEntry("Obbligatorio")
   public static final String EPM_REQUIRED = "150";

   @RBEntry("Eliminato")
   public static final String EPM_SUPPRESSED = "151";

   @RBEntry("Quantità")
   public static final String EPM_QUANTITY = "152";

   @RBEntry("Applicazione proprietaria")
   public static final String EPM_OWNER_APP = "153";

   @RBEntry("Descrizione")
   public static final String EPM_DESCRIPTION = "154";

   @RBEntry("Commento:")
   public static final String COMMENT_LABEL = "155";

   @RBEntry("*")
   public static final String LINK_PREFIX = "156";

   @RBEntry("**Questa parte non ha alternative oppure l'utente non dispone del permesso di visualizzarle.**")
   public static final String NO_PART_ALTERNATES = "157";

   @RBEntry("**Questa parte non è un'alternativa oppure l'utente non dispone del permesso di visualizzare le alternative.**")
   public static final String NO_PARTS_ALTERNATE_FOR = "158";

   @RBEntry("**Questa parte non ha sostituzioni oppure l'utente non dispone del permesso di visualizzarle.**")
   public static final String NO_PART_SUBSTITUTES = "159";

   @RBEntry("Autore modifiche")
   public static final String UPDATED_BY = "16";

   @RBEntry("**Questa parte non è una sostituzione oppure l'utente non dispone del permesso di visualizzare le sostituzioni.**")
   public static final String NO_PARTS_SUBSTITUTE_FOR = "160";

   @RBEntry("SOSTITUZIONI")
   public static final String REPLACEMENTS_TITLE = "161";

   @RBEntry("Alternative")
   public static final String ALTERNATES = "162";

   @RBEntry("Sostituzioni")
   public static final String SUBSTITUTES = "163";

   @RBEntry("Alternativa per")
   public static final String ALTERNATE_FOR = "164";

   @RBEntry("Sostituzione per")
   public static final String SUBSTITUTE_FOR = "165";

   @RBEntry("Alternative della parte {0}")
   public static final String ALTERNATES_HEADER = "166";

   @RBEntry("Sostituzioni della parte {0}")
   public static final String SUBSTITUTES_HEADER = "167";

   @RBEntry("Parti per cui la parte {0} è un'alternativa")
   public static final String ALTERNATE_FOR_HEADER = "168";

   @RBEntry("Parti per cui la parte {0} è una sostituzione")
   public static final String SUBSTITUTE_FOR_HEADER = "169";

   @RBEntry("KB")
   public static final String KILOBYTES = "17";

   @RBEntry("Numero di parte")
   public static final String SUB_PART_NUMBER_HEADER = "170";

   @RBEntry("Nome parte")
   public static final String SUB_PART_NAME_HEADER = "171";

   @RBEntry("Numero dell'assieme")
   public static final String ASSEMBLY_NUMBER_HEADER = "172";

   @RBEntry("Nome dell'assieme")
   public static final String ASSEMBLY_NAME_HEADER = "173";

   @RBEntry("Cerca")
   public static final String SEARCH_LABEL = "174";

   @RBEntry("<BR><H3>\nTask: recupero di ActionDelegate da Factory\n</H3><BR><B>\nFactory: {0}\n<BR>\nSelettore stringa: {1}\n<BR>\nSelettore classe: {2}\n<BR>\nTipo di errore: impossibile trovare dati nel file delle proprietà oppure istanziare/trovare la classe indicata nel file delle proprietà\n\n<BR><P>\nIgnorare l'eccezione se non è necessario il controllo di Stato del ciclo di vita/Accesso\n<BR></B>\n\n")
   public static final String UTCSE_ACTIONDELEGATE = "176";

   @RBEntry("<B>Errore:</B> È in corso il tentativo di visualizzazione di {0}, contenente troppi oggetti da visualizzare e solo i primi <B>{1}</B> oggetti recuperati vengono visualizzati al momento. Se si cerca un particolare oggetto, si consiglia di effettuare una ricerca mirata all'oggetto in questione.")
   public static final String PARTIAL_RESULTS_RETURNED = "177";

   @RBEntry("Appunti Windchill")
   public static final String VISUALIZATION_CLIPBOARD_LABEL = "178";

   @RBEntry("Impossibile visualizzare la miniatura e/o altri dati di visualizzazione")
   public static final String ERROR_DISPLAYING_VISUAL_DATA = "179";

   @RBEntry("Scaricamento del contenuto principale in corso...")
   public static final String DOWNLOAD_PRIMARY = "180";

   @RBEntry("Preferenze")
   public static final String PREFERENCES_LABEL = "181";

   @RBEntry("Dominio: ")
   public static final String DOMAIN_LABEL = "182";

   @RBEntry("{0} (Ereditato dallo schedario)")
   public static final String INHERITED_FROM_CABINET = "183";

   @RBEntry("{0} (Ereditato dalla cartella padre)")
   public static final String INHERITED_FROM_FOLDER = "184";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String FOLDER_BROWSER_TITLE = "185";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String FOLDER_LOCATION_LABEL = "186";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String FOLDER_SELECT_ITEM = "187";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String FOLDER_SELECT_TITLE = "188";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String FOLDER_OPTIONS_LABEL = "189";

   @RBEntry("URL")
   public static final String URL = "18";

   @RBEntry("Nome descrittivo")
   public static final String URL_NAME = "19";

   @RBEntry(" [ La proprietà \"{0}\" non è disponibile.] ")
   public static final String PROPERTY_NOT_AVAILABLE = "2";

   @RBEntry("Visualizza cartella padre")
   public static final String VIEW_PARENT_FOLDER = "20";

   @RBEntry("** L'oggetto {0} è vuoto **")
   public static final String NO_FOLDER_CONTENTS = "21";

   @RBEntry("Link attivi")
   public static final String ACTIVE_LINKS = "22";

   @RBEntry("Link passivi")
   public static final String PASSIVE_LINKS = "23";

   @RBEntry("Componenti")
   public static final String USES = "24";

   @RBEntry("** La parte non utilizza altre parti **")
   public static final String NO_PART_USES = "25";

   @RBEntry("** Non esistono altre parti che utilizzano la parte **")
   public static final String NO_PART_WHERE_USED = "26";

   @RBEntry("** {0} non ha contenuto **")
   public static final String NO_CONTENTS = "27";

   @RBEntry("** {0} non è referenziato da altre parti **")
   public static final String NO_REFERENCED_BY = "28";

   @RBEntry("Numero")
   public static final String NUMBER = "29";

   @RBEntry("Parametri applet non validi. \"{0}\"")
   public static final String INVALID_PARAMETERS = "3";

   @RBEntry("Versione più recente")
   public static final String LATEST_VERSION = "30";

   @RBEntry("Tipo")
   public static final String TYPE = "31";

   @RBEntry("** La parte non contiene riferimenti a documenti **")
   public static final String NO_DOCUMENTS_REFERENCED = "32";

   @RBEntry("Versione")
   public static final String VERSION = "33";

   @RBEntry("Stato del ciclo di vita")
   public static final String STATE = "34";

   @RBEntry("Team")
   public static final String TEAMTEMPLATE = "35";

   @RBEntry("Data ultima modifica")
   public static final String LAST_MOD_DATE = "36";

   @RBEntry("Autore")
   public static final String CREATED_BY = "37";

   @RBEntry("Data creazione")
   @RBComment("The date on which the object was created (Per Terminology CFT 05 13 2005  SPR 1321929 01)")
   public static final String CREATION_DATE = "38";

   @RBEntry("** Non sono state trovate modifiche associate **")
   public static final String NO_CHANGES = "39";

   @RBEntry("Si è verificata un'eccezione durante il recupero dell'oggetto: \"{0}\" ")
   public static final String RETRIEVING_OBJECT = "4";

   @RBEntry("L'URL deve identificare due o più elementi")
   public static final String NOT_ENOUGH_OIDS = "41";

   @RBEntry("Elementi correlati")
   public static final String RELATED_ITEMS = "42";

   @RBEntry("Non sono state trovate differenze")
   public static final String NO_DIFFERENCE_FOUND = "43";

   @RBEntry("sì")
   public static final String HAS_RELATION = "44";

   @RBEntry("no")
   public static final String HAS_RELATION_NOT = "45";

   @RBEntry("Chiamata del processore modelli non riuscita")
   public static final String TP_FAILED_TITLE = "46";

   @RBEntry("Tentativo di visualizzazione della pagina Web tramite un processore modelli non riuscito. Per assistenza, contattare l'amministratore. Di seguito sono riportate le informazioni di riferimento.")
   public static final String TP_FAILED_MESSAGE = "47";

   @RBEntry("Confronta")
   public static final String COMPARE_BUTTON_LABEL = "49";

   @RBEntry(" L'azione \"{0}\" non è disponibile per \"{1}\". ")
   public static final String ACTION_NOT_AVAILABLE = "5";

   @RBEntry("Nessun oggetto nel contesto")
   public static final String NO_OBJECT = "50";

   @RBEntry("Check-Out di {0} non riuscito. L'oggetto è già sottoposto a Check-Out.")
   public static final String CHECKOUT_FAILED = "51";

   @RBEntry("Check-Out di {0} completato")
   public static final String CHECKOUT_SUCCESS = "52";

   @RBEntry("Il parametro \"{0}\" è \"{1}\".")
   public static final String PARAMETER_IS = "53";

   @RBEntry("Parametri non validi. \"{0}\" ")
   public static final String BAD_PARAMETERS = "54";

   @RBEntry("Descritto con")
   public static final String DESCRIBED_BY = "55";

   @RBEntry("Riferimenti")
   public static final String REFERENCES = "56";

   @RBEntry("Schedario personale")
   public static final String PERSONAL_CABINET_LABEL = "57";

   @RBEntry("Elenco incarichi")
   public static final String WORKLIST_LABEL = "58";

   @RBEntry("Ricerca locale")
   public static final String LOCAL_SEARCH_LABEL = "59";

   @RBEntry("Nome")
   public static final String NAME = "6";

   @RBEntry("Ricerca globale")
   public static final String ENTERPRISE_SEARCH_LABEL = "60";

   @RBEntry("Crea documento")
   public static final String IMPORT_LABEL = "61";

   @RBEntry("Cartella Checked-Out")
   public static final String CHECKOUT_FOLDER_LABEL = "62";

   @RBEntry("Iterazione")
   public static final String ITERATION = "63";

   @RBEntry("Tutte le versioni")
   public static final String ALL_VERSIONS_URL_LABEL = "64";

   @RBEntry("Check-In")
   public static final String CHECK_IN_URL_LABEL = "65";

   @RBEntry("Check-Out")
   public static final String CHECK_OUT_URL_LABEL = "66";

   @RBEntry("Cronologia versioni")
   public static final String VERSION_HISTORY_URL_LABEL = "67";

   @RBEntry("Cronologia iterazioni")
   public static final String ITERATION_HISTORY_URL_LABEL = "68";

   @RBEntry("Stato condivisione")
   public static final String SHARESTATUS_URL_LABEL = "69";

   @RBEntry("Formato")
   public static final String FORMAT = "7";

   @RBEntry("Impossibile accedere alla stringa localizzata: impossibile trovare la classe {1}. Chiave risorsa: {0}.")
   public static final String NO_RESOURCEKEY_CLASS = "74";

   @RBEntry("Impossibile accedere alla stringa localizzata: impossibile trovare la classe {0}. Classe risorsa: {1}.")
   public static final String NO_RESOURCEKEY_FIELD = "75";

   @RBEntry("L'utente non è autorizzato a {0}.")
   public static final String NOT_PERMITTED = "76";

   @RBEntry("lettura")
   public static final String READ = "77";

   @RBEntry("modifica")
   public static final String MODIFY = "78";

   @RBEntry("L'oggetto del contesto non è {0}.")
   public static final String OBJECT_NOT = "79";

   @RBEntry("Dimensione")
   public static final String SIZE = "8";

   @RBEntry("Interrogazione sull'oggetto {1} non riuscita durante la chiamata a {0}.")
   public static final String QUERY_FAILED_MESSAGE = "80";

   @RBEntry("Non è stato creato alcun nome di colonna. Impossibile compilare la tabella.")
   public static final String NO_COLUMN_NAMES = "81";

   @RBEntry("Nessun elemento per i nomi di colonna in wt.properties.")
   public static final String NO_COLUMN_NAME_PROPERTIES = "82";

   @RBEntry("Si è verificato un errore durante la creazione della specifica di configurazione. Impossibile generare l'interrogazione.")
   public static final String CONFIG_SPEC_ERROR = "83";

   @RBEntry("Creazione di informazioni generali per l'oggetto non riuscita.")
   public static final String OBJ_ID_FAILED = "84";

   @RBEntry("Il servizio di interrogazione richiesto non è disponibile o non esiste.")
   public static final String NO_QUERYSERVICE_NAME = "85";

   @RBEntry("Si è verificata un'eccezione nell'introspezione: È possibile che sia stato utilizzato un attributo derivato non valido")
   public static final String INTROSPECTION_EXCEPTION = "86";

   @RBEntry("Personalizzazione del modello HTML non valida.")
   public static final String INVALID_CUSTOMIZATION = "87";

   @RBEntry("Inizializzazione della tabella non riuscita.")
   public static final String INITIALIZATION_OF_TABLE_FAILED = "88";

   @RBEntry("<CENTER>** {0} **</CENTER>")
   public static final String NO_TABLE_ENTRIES = "89";

   @RBEntry("Data ultima modifica")
   public static final String MODIFIED = "9";

   @RBEntry("Check-In di {0} non riuscito. L'oggetto non è sottoposto a Check-Out.")
   public static final String CHECKIN_FAILED = "90";

   @RBEntry("Check-In di {0} completato")
   public static final String CHECKIN_SUCCESS = "91";

   @RBEntry("Home")
   public static final String HOMEPAGE_URL_LABEL = "93";

   @RBEntry("Cronologia del ciclo di vita")
   public static final String LIFECYCLE_HISTORY_LABEL = "94";

   @RBEntry("ERRORE")
   public static final String ERROR = "95";

   @RBEntry("nella generazione dinamica della pagina Web, durante l'elaborazione del metodo \"{0}\" scritto con il linguaggio script di Windchill.")
   public static final String DURING_PAGE_GENERATION = "96";

   @RBEntry("durante la generazione dinamica della pagina Web.")
   public static final String DURING_PAGE_GENERATION_2 = "97";

   @RBEntry("Messaggio di errore: ")
   public static final String ERROR_MESSAGE_IS = "98";

   @RBEntry("Nel file di log è stata memorizzata una traccia stack.")
   public static final String STACK_TRACE_PRINTED = "99";

   @RBEntry("Data")
   public static final String DATE = "190";

   @RBEntry("Team")
   public static final String TEAM = "191";

   @RBEntry("Dettagli originale")
   public static final String DETAILS_OF_ORIG = "192";

   @RBEntry("Operazione in avanzamento ({0})")
   public static final String WORK_IN_PROGRESS = "193";

   @RBEntry("Nome team")
   public static final String TEAM_NAME = "194";

   @RBEntry("In attesa di promozione")
   public static final String AWAITING_PROMOTION = "195";

   @RBEntry("Stato")
   public static final String STATUS = "196";

   @RBEntry("Posizione")
   public static final String LOCATION = "197";

   @RBEntry("Ciclo di vita")
   public static final String LIFE_CYCLE = "198";

   @RBEntry("Nome modello")
   public static final String EPM_CAD_NAME = "199";

   @RBEntry("Tipo istanza")
   public static final String EPM_INSTANCE_TYPE = "200";

   @RBEntry("I miei workspace")
   public static final String MY_WORKSPACE = "201";

   @RBEntry("Errore trasmissione URL: richiesta HTTP non valida")
   public static final String URL_POST_ERROR = "202";

   @RBEntry("Errore InputStream: impossibile trovare InputStream")
   public static final String MPINPUTSTREAM_NULL_ERROR = "203";

   @RBEntry("Eccezione processore moduli: metodo trasmissione URL non valido")
   public static final String PROCESSFORM_POST_ERROR = "204";

   @RBEntry("Eccezione processore moduli: valore input nullo")
   public static final String PROCESSFORM_NULL_ERROR = "205";

   @RBEntry("Errore: la variabile rappresenta una classe non valida")
   public static final String INVALID_CLASS_ERROR = "206";

   @RBEntry("URL non valido: non è stato trasmesso alcun parametro.")
   public static final String INVALID_URL_ERROR = "207";

   @RBEntry("Accesso negato: permessi non sufficienti o stato del ciclo di vita non valido dell'oggetto")
   public static final String ACCESS_DENIED_ERROR = "208";

   @RBEntry("Azioni")
   public static final String ACTION_COLUMN_LABEL = "209";

   @RBEntry("Visualizza unità in:")
   @RBComment("label for a dropdown menu giving alternative measurement unit systems for displaying IBA values (e.g., SI, USCS)")
   public static final String IBA_DISPLAY_UNITS_DROPDOWN_LABEL = "210";

   @RBEntry("Modifica")
   @RBComment("label on button next to measurement systems dropdown menu; causes IBA values to be redisplayed using selected measurment units")
   public static final String IBA_DISPLAY_UNITS_BUTTON_LABEL = "211";

   @RBEntry("ID organizzazione:")
   @RBComment("label for an organization identifier attribute")
   public static final String ORGANIZATION_IDENTIFIER = "212";

   @RBEntry("Copia multipla non consentita: non sono permesse più revisioni dello stesso master.")
   public static final String INVALID_MULTI_COPY_CONDITION = "213";

   @RBEntry("Proprietario:")
   @RBComment("A label for a field displaying the owner of an object, e.g., the owner of a personal cabinet")
   public static final String OWNER_LABEL = "214";

   @RBEntry("Copiato da")
   public static final String COPYFROM_SUBTITLE = "215";

   @RBEntry("Copiato in")
   public static final String COPYTO_SUBTITLE = "216";

   @RBEntry("Modifica in sospeso")
   @RBComment("tool tip that is displayed when user mouses over the image that indicates a pending change exists on the object")
   public static final String PENDING_CHANGE = "217";

   @RBEntry("Applicazione di creazione")
   public static final String EPM_AUTH_APP = "218";

   @RBEntry("Modello:")
   @RBComment("Used to indicate that the object is a template.")
   public static final String TEMPLATE = "219";

   @RBEntry("Sì - Attivato")
   @RBComment("\"Yes\" indicates that the object is a template; \"Enabled\" indictates that the object is available for use.")
   public static final String TEMPLATE_AVAILABLE = "220";

   @RBEntry("Sì - Disattivato")
   @RBComment("\"Yes\" - indicates that the object is a template; \"Disabled\" indictates that the object is not available for use.")
   public static final String TEMPLATE_UNAVAILABLE = "221";

   @RBEntry("Cronologia target di distribuzione per")
   public static final String DISTRIBUTION_LIST_TITLE = "222";

   @RBEntry("Crea documento da modello")
   public static final String DOCFROMTEMPLATE_LABEL = "223";

   @RBEntry("Incolla")
   @RBComment("Paste an ojbect to the clipboard")
   public static final String PASTE = "224";

   @RBEntry("Documento CAD")
   @RBComment("a general purpose label for displaying the generic label for CAD Document in the messages")
   public static final String EPMDOCUMENT_LABEL = "225";

   @RBEntry("Parte")
   @RBComment("a general purpose label for displaying the generic label for Part in the messages")
   public static final String WTPART_LABEL = "226";

   @RBEntry("Log transazioni ESI")
   @RBComment("Link to the ESI transaction log page")
   public static final String ESI_TRANS_LOG_LABEL = "227";

   @RBEntry("Stato condivisione")
   @RBComment("Share status link label")
   public static final String SHARE_STATUS_URL_LABEL = "228";

   @RBEntry("Sottoposto a Check-Out nel progetto {0} da {1}")
   @RBComment("possible value of the attribute Status on Details pages of parts and documents")
   public static final String CHECKED_OUT_TO_PROJECT_W_NAME_BY = "229";

   @RBEntry("Sottoposto a Check-Out nel progetto da {0}")
   @RBComment("possible value of the attribute Status on Details pages of parts and documents")
   public static final String CHECKED_OUT_TO_PROJECT_BY = "230";

   @RBEntry("Sottoposto a Check-Out nel  progetto {0}")
   @RBComment("possible value of the attribute Status on Details pages of parts and documents")
   public static final String CHECKED_OUT_TO_PROJECT_W_NAME = "231";

   @RBEntry("Sottoposto a Check-Out in un progetto")
   @RBComment("possible value of the attribute Status on Details pages of parts and documents")
   public static final String CHECKED_OUT_TO_PROJECT = "232";

   @RBEntry("Contesto")
   @RBComment("Label for the Container Name")
   public static final String CONTEXT = "233";

   @RBEntry("Violazione dell'univocità nella nuova copia di {0}. Impossibile salvare la copia.")
   @RBComment("uniqueness exception encountered during Save As.")
   public static final String UNIQUE_ERROR = "234";

   @RBEntry("Amministrazione delle regole di inizializzazione degli oggetti")
   @RBComment("Link to the Rule Administrator page")
   public static final String RULE_ADMIN_LABEL = "235";

   @RBEntry("Nessuno")
   @RBComment("None of the versions are enabled")
   public static final String NONE = "236";

   @RBEntry("Versione attivata:")
   @RBComment("Version of the object that is enabled")
   public static final String ENABLED_VER = "237";

   @RBEntry("Gestione report")
   @RBComment("Link to the Report Manager page")
   public static final String REPORT_MANAGER = "238";

   @RBEntry("Riga")
   @RBComment("Label for the line number column in the Product Structure page table")
   public static final String LINE_NUMBER_COLUMN_LABEL = "239";

   @RBEntry("Confronto di oggetti fallito : numero di oggetti insufficiente")
   @RBComment("Title for Insufficient Number of Objects error page")
   public static final String OBJ_COMPARE_FAIL_TITLE = "240";

   @RBEntry("Non è stato effettuato alcun confronto.")
   @RBComment("First line for Insufficient Number of Objects error page")
   public static final String OBJ_COMPARE_FAIL_LINE_1 = "241";

   @RBEntry("Selezionare due o più oggetti da confrontare.")
   @RBComment("Second line for Insufficient Number of Objects error page")
   public static final String OBJ_COMPARE_FAIL_LINE_2 = "242";

   @RBEntry("Fare clic sul pulsante Indietro ed effettuare le selezioni.")
   @RBComment("Third line for Insufficient Number of Objects error page (Non PDMLink only)")
   public static final String OBJ_COMPARE_FAIL_LINE_3 = "243";

   @RBEntry("L'oggetto non esiste o non si dispone dei diritti di accesso richiesti.")
   public static final String OBJECT_GONE = "244";

   @RBEntry("AVVERTENZA: Impossibile creare HelpLink per l'azione di contesto {0}")
   @RBComment("This is a debug message which is only displayed in the logs if there is a configuration error.  The context action is the help action such as VIEW_OBJECT etc.")
   public static final String UNABLE_TO_CREATE_HELPLINK = "245";

   @RBEntry("Target di distribuzione")
   @RBComment("This string is used in the HTML")
   public static final String DISTRIBUTION_LIST = "246";

   @RBEntry("Cronologia target di distribuzione")
   @RBComment("This string is used in the HTML")
   public static final String DISTRIBUTION_LIST_HISTORY = "247";

   @RBEntry("Vai a più recente")
   @RBComment("This string is used in the HTML")
   public static final String GO_TO_LATEST = "248";

   @RBEntry("Gestione pulizia")
   @RBComment("Label for the link to the Purge Administrator ui")
   public static final String PURGE_ADMINISTRATOR = "249";

   @RBEntry("Crea e gestisce operazioni di pulizia.")
   @RBComment("Purge administrator description")
   public static final String PURGE_ADMINISTRATOR_DESCRIPTION = "250";

   @RBEntry("Generatore file di configurazione")
   @RBComment("Label for the link to the Configuration File Generator utility")
   public static final String CONFIGURATION_FILE_GENERATOR = "251";

   @RBEntry("Genera o aggiorna le configurazioni DCA per le definizioni dei tipi del sistema, sia di tipo soft che di tipo modellato, da utilizzare per personalizzare le ricerche.")
   @RBComment("Configuration File Generator utility description")
   public static final String CONFIGURATION_FILE_GENERATOR_DESCRIPTION = "252";

   @RBEntry("La pagina richiesta è disponibile solo in Windchill PDMLink. Questa installazione di Pro/INTRALINK non include le funzionalità di Windchill PDMLink richieste.")
   @RBComment("Message when someone tries to get access to a feature that is not available for PRO/I")
   public static final String PROI_INSTALLED_FEATURE_NOT_AVAILABLE = "253";

   @RBEntry("L'azione selezionata non può essere eseguita a causa di un altro processo in corso. Aggiornare la pagina per aggiornare le azioni.")
   @RBComment("Message when someone tries to perform an action that is no longer valid due to a system state change that has occured since the action was displayed.")
   public static final String ACTION_NO_LONGER_VALID = "254";

   /**
    * The strings below are already translated in \Windchill\src\wt\enterprise\htmltmpl\forms\SetWTPartEffectivityConfigSpec_*.html
    * take the translations from those files before remofing the translated template file.
    **/
   @RBEntry("Specificare un contesto di effettività da associare al valore indicato")
   public static final String SET_WTPART_EFFECTIVITY_CONFIG_SPEC_ALERT = "255";

   @RBEntry("Più recente")
   public static final String SET_WTPART_EFFECTIVITY_CONFIG_SPEC_LATEST_LABEL = "256";

   @RBEntry("Baseline")
   public static final String SET_WTPART_EFFECTIVITY_CONFIG_SPEC_BASELINE_LABEL = "257";

   @RBEntry("Effettività")
   public static final String SET_WTPART_EFFECTIVITY_CONFIG_SPEC_EFFECTIVITY_LABEL = "258";

   @RBEntry("Imposta la specifica di configurazione dell'effettività")
   public static final String SET_WTPART_EFFECTIVITY_CONFIG_SPEC = "259";

   @RBEntry("Vista:")
   public static final String SET_WTPART_EFFECTIVITY_CONFIG_SPEC_VIEW_LABEL = "260";

   @RBEntry("Contesto di effettività:")
   public static final String SET_WTPART_EFFECTIVITY_CONFIG_SPEC_CONTEXT_LABEL = "261";

   @RBEntry("Cerca...")
   public static final String SET_WTPART_EFFECTIVITY_CONFIG_SPEC_SEARCH_BUTTON = "262";

   @RBEntry("Data di effettività:")
   public static final String SET_WTPART_EFFECTIVITY_CONFIG_SPEC_EFFECTIVE_DATE = "263";

   @RBEntry("Valore:")
   public static final String SET_WTPART_EFFECTIVITY_CONFIG_SPEC_VALUE_LABEL = "264";

   @RBEntry("Usa solo per questa sessione")
   public static final String SET_WTPART_EFFECTIVITY_CONFIG_SPEC_SESSION_ONLY = "265";

   @RBEntry("OK")
   public static final String SET_WTPART_EFFECTIVITY_CONFIG_SPEC_OK = "266";

   /**
    * End of strings from SetWTPartEffectivityConfigSpec_*.html
    **/
   @RBEntry("Gestione pulizia, archiviazioni e ripristini")
   @RBComment("Label for the link to the Purge, Archive, and Restore Administrator ui when an archive system is installed")
   public static final String PURGE_ARCHIVE_RESTORE_ADMINISTRATOR = "267";

   @RBEntry("Crea operazioni di pulizia per eliminare dati dal sistema Windchill in modo permanente o, in alternativa, crea un archivio dei dati da ripristinare in un momento successivo.")
   @RBComment("Purge, Archive, and Restore Administrator description when an archive system is installed")
   public static final String PURGE_ARCHIVE_RESTORE_ADMINISTRATOR_DESCRIPTION = "268";

   @RBEntry("MASTER DOCUMENTO PROXY")
   @RBComment("This string is used in the HTML")
   public static final String PROXY_DOCUMENT_MASTER = "279";

   @RBEntry("MASTER PARTE PROXY")
   @RBComment("This string is used in the HTML")
   public static final String PROXY_PART_MASTER = "280";

   @RBEntry("Revisione")
   public static final String REVISION = "281";

   @RBEntry("Parti confrontate")
   @RBComment("This string is used in the HTML ")
   public static final String PARTS_BEING_COMPARED = "282";

   @RBEntry("Il sistema non è configurato per consentire la copia attraverso i limiti di organizzazione.")
   public static final String ONDEMAND_INVALID_COPY = "290";

   @RBEntry("Check-In di {0}")
   public static final String CHECK_IN_LABEL = "300";

   @RBEntry("Check-In del documento")
   public static final String CHECK_IN_DOCUMENT_LABEL = "301";

   @RBEntry("Commenti:")
   public static final String COMMENTS_COLON = "302";

   @RBEntry("IMPORTA DOCUMENTO")
   public static final String IMPORT_DOCUMENT = "303";

   @RBEntry("*Numero:")
   public static final String REQUIRED_NUMBER_COLON = "304";

   @RBEntry("Reparto:")
   public static final String DEPARTMENT_COLON = "305";

   @RBEntry("Titolo:")
   public static final String TITLE_COLON = "306";

   @RBEntry("*Posizione:")
   public static final String REQUIRED_LOCATION_COLON = "307";

   @RBEntry("Descrizione:")
   public static final String DESCRIPTION_COLON = "308";

   @RBEntry("Ciclo di vita:")
   public static final String LIFE_CYCLE_COLON = "309";

   @RBEntry("Team:")
   public static final String TEAM_COLON = "310";

   @RBEntry("*File:")
   public static final String REQUIRED_FILE_COLON = "311";

   @RBEntry("Posizione:")
   public static final String LOCATION_COLON = "312";

   @RBEntry("Salvataggio da")
   public static final String SAVING_FROM = "313";

   @RBEntry("*Ciclo di vita:")
   public static final String REQUIRED_LIFE_CYCLE_COLON = "314";

   @RBEntry("Imposta la specifica di configurazione della baseline")
   public static final String SET_BASELINE_CONFIG_SPEC_LABEL = "315";

   @RBEntry("Baseline")
   public static final String BASELINE = "316";

   @RBEntry("Imposta la specifica di configurazione più recente")
   public static final String SET_LATEST_CONFIG_SPEC_LABEL = "317";

   @RBEntry("Includi le parti nel mio schedario personale")
   public static final String INCLUDE_PARTS_IN_PERSONAL_CABINET = "318";

   @RBEntry("Cronologia iterazioni di {0}")
   public static final String ITERATION_HISTORY_LABEL = "319";

   @RBEntry("Confronto baseline")
   public static final String BASELINE_COMPARISON_LABEL = "320";

   @RBEntry("Confronto baseline in corso")
   public static final String COMPARING_BASELINE = "321";

   @RBEntry("Confronta elementi")
   public static final String COMPARE_ITEMS_LABEL = "322";

   @RBEntry("Confronto elementi in corso")
   public static final String COMPARING_ITEMS_LABEL = "323";

   @RBEntry("Confronta documenti")
   public static final String COMPARE_DOCUMENTS_LABEL = "324";

   @RBEntry("Confronto documenti in corso")
   public static final String COMPARING_DOCUMENTS = "325";

   @RBEntry("Contenuto:")
   public static final String CONTENTS_COLON = "326";

   @RBEntry("Componenti:")
   public static final String USES_COLON = "327";

   @RBEntry("Documenti Riferimenti:")
   public static final String REFERENCES_DOCUMENTS_COLON = "328";

   @RBEntry("Parti Descrive:")
   public static final String DESCRIBES_PARTS_COLON = "329";

   @RBEntry("Confronta elementi revisionati")
   public static final String COMPARE_REVISED_ITEMS_LABEL = "330";

   @RBEntry("Confronto elementi revisionati in corso")
   public static final String COMPARING_REVISED_ITEMS_LABEL = "331";

   @RBEntry("Confronto oggetti non supportato per questi elementi")
   public static final String OBJECT_SUPPORT_BOT_FOR_THESE_ITEMS = "332";

   @RBEntry("Confronta parti")
   public static final String COMPARE_PARTS_LABEL = "333";

   @RBEntry("Parti correlate:")
   public static final String RELATED_PARTS_COLON = "334";

   @RBEntry("Documenti correlati:")
   public static final String RELATED_DOCUMENTS_COLON = "335";

   @RBEntry("Documento CAD correlato:")
   public static final String RELATED_CAD_DOCUMENTS_COLON = "336";

   @RBEntry("Contenuto di {0}")
   public static final String CONTENT_OF_LABEL = "337";

   @RBEntry("CONTENUTO")
   public static final String CAP_CONTENTS = "338";

   @RBEntry("Contenuto in {0}")
   public static final String CONTENT_IN_LABEL = "339";

   @RBEntry("posizione")
   public static final String S_LOCATION = "340";

   @RBEntry("versione")
   public static final String S_VERSION = "341";

   @RBEntry("stato del ciclo di vita")
   public static final String S_STATE = "342";

   @RBEntry("Sfoglia schedari")
   public static final String BROWSE_CABINETS_LABEL = "343";

   @RBEntry("Risultati query Descritto con")
   public static final String DESCRIBED_BY_RESULTS_LABEL = "344";

   @RBEntry("Contenuto di {0}")
   public static final String CONTENTS_OF_LABEL = "345";

   @RBEntry("Risultati interrogazione Attività di modifica")
   public static final String CHANGE_ACTIVITY_QUERY_RESULTS_LABEL = "346";

   @RBEntry("Risultati interrogazione ordine di modifica")
   public static final String CHANGE_ORDER_QUERY_RESULTS_LABEL = "347";

   @RBEntry("Risultati interrogazione Ordini di modifica")
   public static final String CHANGE_ORDERS_QUERY_RESULTS_LABEL = "348";

   @RBEntry("Risultati interrogazione Richiesta di modifica")
   public static final String CHANGE_REQUESTS_QUERY_RESULTS_LABEL = "349";

   @RBEntry("Modifiche per {0}")
   public static final String CHANGES_FOR_LABEL = "350";

   @RBEntry("Elementi descritti da {0}")
   public static final String ITEMS_DESCRIBED_BY_LABEL = "351";

   @RBEntry("Parti descritte da {0}")
   public static final String PARTS_DESCRIBED_BY_LABEL = "352";

   @RBEntry("Documenti che referenziano {0}")
   public static final String DOCUMENTS_THAT_REFERENCE_LABEL = "353";

   @RBEntry("Documenti referenziati da {0}")
   public static final String DOCUMENTS_REFERENCED_BY_LABEL = "354";

   @RBEntry("Documenti che impiegano {0}")
   public static final String DOCUMENTS_THAT_USE_LABEL = "355";

   @RBEntry("Documenti impiegati da {0}")
   public static final String DOCUMENTS_USED_BY_LABEL = "356";

   @RBEntry("Tutte le versioni di {0}")
   public static final String ALL_VERSIONS_OF_LABEL = "357";

   @RBEntry("Riferimenti per {0}")
   public static final String REFERENCES_FOR_LABEL = "358";

   @RBEntry("Componenti per {0}")
   public static final String USED_BY_FOR_LABEL = "359";

   @RBEntry("Componenti/impieghi per {0}")
   public static final String USED_BY_USES_FOR_LABEL = "360";

   @RBEntry("Impiegato da")
   public static final String USED_BY_LABEL = "361";

   @RBEntry("Componenti")
   public static final String USES_LABEL = "362";

   @RBEntry("Impiegato da:")
   public static final String USED_BY_COLON = "363";

   @RBEntry("Tabella Referenziato da")
   public static final String REFERENCED_BY_TABLE_LABEL = "365";

   @RBEntry("{0} è referenziato da")
   public static final String IS_REFERENCED_BY = "366";

   @RBEntry("Risultati interrogazione Oggetti revisionati")
   public static final String REVISED_OBJS_QUERY_RESULTS = "367";

   @RBEntry("Struttura prodotto per {0}")
   public static final String PRODUCT_STRUCTURE_FOR_LABEL = "368";

   @RBEntry("Risultati interrogazione Impiegato da")
   public static final String USED_BY_QUERY_RESULTS = "369";

   @RBEntry("Risultati interrogazione Componenti")
   public static final String USES_QUERY_RESULTS = "370";

   @RBEntry("Cronologia salvataggi {0}")
   public static final String SAVED_AS_HISTORY_LABEL = "371";

   @RBEntry("Cronologia salvataggi per {0}")
   public static final String SAVED_AS_HISTORY_FOR_LABEL = "372";

   @RBEntry("Rilevato errore")
   public static final String ERROR_FOUND_LABEL = "373";

   @RBEntry("Errore rilevato...")
   public static final String ERROR_FOUND_EXTN = "374";

   @RBEntry("Cronologia versioni di {0}")
   public static final String VERSION_HISTORY_OF_LABEL = "375";

   @RBEntry("Invia")
   public static final String SUBMIT = "376";

   @RBEntry("Importa documento")
   public static final String IMPORT_DOCUMENT_LABEL = "377";

   @RBEntry("Tipo:")
   public static final String TYPE_COLON = "378";

   @RBEntry("Sfoglia cartelle")
   public static final String BROWSE_FOLDERS_LABEL = "379";

   @RBEntry("Apri")
   public static final String OPEN = "380";

   @RBEntry("Sfoglia...")
   public static final String BROWSE = "381";

   @RBEntry("&nbsp;&nbsp; OK &nbsp;&nbsp;")
   public static final String OK_BUTTON = "382";

   @RBEntry("Cerca...")
   public static final String SEARCH_BUTTON = "383";

   @RBEntry("Stato del ciclo di vita:")
   public static final String STATE_COLON = "384";

   @RBEntry("Il confronto di questi elementi non è supportato")
   public static final String COMPARISON_NOT_SUPPORTED = "385";

   @RBEntry("Origini che descrivono {0}")
   public static final String SOURCES_THAT_DESCRIBE = "386";

   @RBEntry("{0} è <B>usato da:</B>")
   public static final String IS_USED_BY = "387";

   @RBEntry("Non è stato fornito alcun valore per la baseline da utilizzare nella specifica di configurazione. Immettere il nome della baseline o cercarne una.")
   @RBComment("Do not remove the newline characters. Place newline characters in appropriate places for foreign text.")
   public static final String VALIDATE_SETBASECONFIGSPEC_ALERT = "388";

   @RBEntry("Errore: immettere valori per numero, file e posizione")
   public static final String VALIDATE_CREATEWTDOCUMENT_ALERT = "389";

   @RBEntry("Nome:")
   public static final String NAME_COLON = "390";

   @RBEntry("Specifica di configurazione della baseline")
   public static final String BASELINE_CONFIG_SPEC_LABEL = "391";

   @RBEntry("Specifica di configurazione")
   public static final String CONFIG_SPEC_LABEL = "392";

   @RBEntry("Specifica di configurazione più recente")
   public static final String LATEST_CONFIG_SPEC_LABEL = "393";

   @RBEntry("Specifica di configurazione di effettività")
   public static final String EFFECTIVE_CONFIG_SPEC_LABEL = "394";

   @RBEntry("Documenti Riferimenti:")
   public static final String REFERENCES_DOCUMENTS = "395";

   @RBEntry("Tutte le revisioni")
   public static final String ALL_REVISIONS_URL_LABEL = "396";

   @RBEntry("Errore durante il recupero della stringa della versione attiva.")
   public static final String GET_ENABLED_VERSION = "401";

   @RBEntry("Errore durante la disattivazione di versioni o iterazioni precedenti.")
   public static final String DISABLE_ITER = "402";

   @RBEntry("Errore durante il recupero dell'ultima iterazione della versione più recente.")
   public static final String LATESTVER_ITER = "410";

   @RBEntry("Impossibile modificare il nome del file")
   public static final String CHNG_FILENNAME_FAILED = "411";

   @RBEntry("Impossibile ottenere il contenuto principale.")
   public static final String GET_PRIM_CONTENT_FAILED = "412";

   @RBEntry("L'attributo {0} non è disponibile per il tipo.")
   @RBComment("The first argument should be an attribute name")
   public static final String ATTR_NOT_FOUND = "413";

   @RBEntry("Impossibile copiare in un oggetto esistente.")
   public static final String COPY_MERGE_NOT_ALLOWED = "414";

   @RBEntry("Il delegato di copia ha restituito un oggetto di copia NULLO: {0}")
   @RBComment("The class name of the copy delegate which returned a null copy.")
   public static final String COPY_DELEGATE_RETURNED_NULL_COPY = "415";

   @RBEntry("Messaggio di errore")
   public static final String ERROR_MESSAGE = "416";

   @RBEntry("ID richiesta")
   public static final String REQUEST_ID = "417";

   @RBEntry("ID contesto metodo")
   public static final String METHOD_CONTEXT_ID = "418";

   @RBEntry("Data e ora correnti")
   public static final String CURRENT_TIME = "419";

   @RBEntry("Identificatore oggetto")
   public static final String OBJOID = "OBJOID";
}
