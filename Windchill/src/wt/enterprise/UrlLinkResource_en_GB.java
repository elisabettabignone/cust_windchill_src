/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */

package wt.enterprise;

import wt.util.InstalledProperties;

/**
 * enterpriseResource message resource bundle [English/US]
 *
 * Usage notes:
 * Last USED ID: 5
 * DO NOT REUSE IDS.  If an message becomes obsolete, comment it out
 * but do not create a new message with that id.
 *
 * To Remove all actions from the search page, use the NO_ACTIONS constant in
 *	the <OBJECTTYPE>_ACTION_LINKS_MIN paragraph, see example below.
 *
 * example:<br>
 * { "WTPART_ACTION_LINKS_MIN", NO_ACTIONS },
 *
 * <BR><BR><B>Supported API: </B>true
 * <BR><BR><B>Extendable: </B>false
 *
 *
 **/
public class UrlLinkResource_en_GB extends UrlLinkResource {


  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */

   public Object[][] getContents() {
      return contents;
   }

  /**
   *
   * <BR><BR><B>Supported API: </B>false
   * <BR><BR><B>Extendable: </B>false
   *
   */
   static final Object[][] contents = {
      {UrlLinkResource. NAME_SEPARATOR, ":" },
      {UrlLinkResource. PAIR_SEPARATOR, "," },
      {UrlLinkResource.NO_ACTIONS, "NOACTIONS" },


      /**
       *  <TRANSLATE>
       *
       *  Entries with _ACTIONS need localizing.  These values contain lists of pairs.  Each pair
       *  contains an action tag and the corresponding display name.  Pairs are separated by
       *  a comma (,).  The action tag and display name are separated by a colon (:).  The
       *  value following the colon is displayed and needs to be localized.  For example, in
       *  the value   "ObjProps:Properties,PartUses:Uses", "Properties" and "Uses" are displayed
       *  and will need localizing.
       **/


      { UrlLinkResource.WTPART_ACTIONS, getValuesForWTPART_ACTIONS() },
      { UrlLinkResource.WTSERIALNUMBEREDPART_ACTIONS, getValuesForWTSERIALNUMBEREDPART_ACTIONS() },
      { UrlLinkResource.WTPRODUCTCONFIGURATION_ACTIONS,
                    "ObjProps:Properties,part_structure:Product Structure,ProductInstances:Product Instances" },
      { UrlLinkResource.WTPRODUCTINSTANCE2_ACTIONS,
                    "ObjProps:Properties,part_structure:Product Structure,Allocate:Allocated By/ Allocates,AssociatedProcess:Related Process" },
      { UrlLinkResource.WTDOCUMENT_ACTIONS, getValuesForWTDOCUMENT_ACTIONS() },
//                    "ObjProps:Properties,document_structure:Structure Hierarchy,DependencyView:References,DocUsesView:Structure Usage,Changes:Changes,Describes:Describes,AssociatedProcess:Related Process"  },
      {UrlLinkResource. WTCHANGEACTIVITY_ACTIONS,
                    "ObjProps:Properties,Revises:Revises,Content:Contents,AssociatedProcess:Related Process"  },
      {UrlLinkResource. WTCHANGEORDER_ACTIONS,
                     "ObjProps:Properties,Affects:Affects,BusinessCase:Business Case,Disposition:Disposition,Content:Contents,AssociatedProcess:Related Process" },
      {UrlLinkResource. WTCHANGEREQUEST_ACTIONS,
                     "ObjProps:Properties,Identifies:Identifies,Disposition:Disposition,Content:Contents,AssociatedProcess:Related Process" },
      {UrlLinkResource. WTPARTMASTER_ACTIONS,
                    "ObjProps:Properties" },
      {UrlLinkResource. WTDOCUMENTMASTER_ACTIONS,
                    "ObjProps:Properties,ReferencedBy:Referenced By" },
      {UrlLinkResource.GENERICPART_ACTIONS,
                    "ObjProps:Properties,part_structure:Product Structure,PartUses:Used By / Uses,PartReferences:References,GPVariants:Variants,GPVariantSpecs:VariantSpecs,GeneratorLauncher:Order Generator,AssociatedProcess:Related Process" },
      { UrlLinkResource.VARIANTSPEC_ACTIONS,
                    "ObjProps:Properties,Changes:Changes,ReferencedBy:Referenced By,AssociatedProcess:Related Process" },
      {UrlLinkResource. EPMDOCUMENT_ACTIONS, getValuesForEPMDOCUMENT_ACTIONS() },
//                    "ObjProps:Properties,EPMUses:Uses,EPMUsedBy:Used By,EPMDescribes:Describes,EPMReferences:References,EPMReferencedBy:Referenced By" },
      { UrlLinkResource.REPORTTEMPLATE_ACTIONS,
                    "ObjProps:Properties" },
      { UrlLinkResource.TEAM_ACTIONS,
                    "ObjProps:Properties" },
      { UrlLinkResource.TEAMTEMPLATE_ACTIONS,
                    "ObjProps:Properties" },
      { UrlLinkResource.TEAMDISTRIBUTIONLIST_ACTIONS,
                    "ObjProps:Properties" },
      { UrlLinkResource.LIFECYCLETEMPLATE_ACTIONS,
                    "ObjProps:Properties" },
      { UrlLinkResource.CONFIGURATIONITEM_ACTIONS,
                    "ObjProps:Properties" },
      { UrlLinkResource.WTPRODUCTINSTANCE_ACTIONS,
                    "ObjProps:Properties,part_structure:Product Structure" },
      { UrlLinkResource.PMOPERATION_ACTIONS,
                    "ObjProps:Properties,OperationUsedBy:Used By,OperationAssembly:Manufacturing  Assembly,OperationResource:Resources,OperationMfg:Manufacturing Features,studies:Studies,pert:PERT View,references:References,ewiheader:EWI Header"},
      { UrlLinkResource.PMCOMPOUNDOPERATION_ACTIONS,
                    "ObjProps:Properties,operation_structure:Process Structure,OperationUsedBy:Used By,OperationUses:Uses,OperationAssembly:Manufacturing  Assembly,OperationResource:Resources,OperationMfg:Manufacturing Features,studies:Studies,pert:PERT View,references:References,ewiheader:EWI Header"},
      { UrlLinkResource.PMMFGFEATURE_ACTIONS,
                    "ObjProps:Properties,MfgParts:Parts,MfgAssembly:Manufacturing  Assembly,MfgOperations:Operations,MfgResources:Resources,studies:Studies,references:References"},
      { UrlLinkResource.PMCOMPOUNDRESOURCE_ACTIONS,
                    "ObjProps:Properties,resource_structure:Resource Structure,ResourceUsedBy:Used By,ResourceUses:Uses,ResourceAssembly:Manufacturing  Assembly,ResourceOperations:Operations,studies:Studies,references:References"},
      { UrlLinkResource.PMTOOLINSTANCE_ACTIONS,
                    "ObjProps:Properties,resource_structure:Resource Structure,ResourceUsedBy:Used By,ResourceUses:Uses,prototype:Tool Prototype,ResourceAssembly:Manufacturing  Assembly,ResourceOperations:Operations,ResourceMfg:Manufacturing Features,studies:Studies,references:References"},
      { UrlLinkResource.PMRESOURCEPLACEHOLDER_ACTIONS,
                    "ObjProps:Properties,ResourceUsedBy:Used By,ResourceAssembly:Manufacturing  Assembly,ResourceOperations:Operations,studies:Studies,references:References"},
      { UrlLinkResource.PMTOOLPROTOTYPE_ACTIONS,
                    "ObjProps:Properties,instances:Tool Instances,studies:Studies,references:References"},
      { UrlLinkResource.PMCOMPOUNDASSEMBLY_ACTIONS,
                    "ObjProps:Properties,assembly_structure:Assembly Structure,usedby:UsedBy,uses:Uses,parts:Parts,assemblyOperations:Operations,assemblyResources:Resources,assemblyMfg:Manufacturing Features,studies:Studies,references:References"},
      { UrlLinkResource.PMASSEMBLY_ACTIONS,
                    "ObjProps:Properties,usedby:UsedBy,parts:Parts,assemblyOperations:Operations,assemblyResources:Resources,assemblyMfg:Manufacturing Features,studies:Studies,references:References"},
      { UrlLinkResource.PMSTUDY_ACTIONS,
                    "ObjProps:Properties,parts:Parts in study,operations:Operations in study,resources:Resources in study,mfgfeatures:Manufacturing Features in study,references:References"},
      { UrlLinkResource.PMINTERMEDIATEASMBLY_ACTIONS,
                    "ObjProps:Properties,assembly_structure:Assembly Structure,parts:Parts,assemblyOperations:Operations,assemblyResources:Resources,assemblyMfg:Manufacturing Features,studies:Studies,references:References"},
      { UrlLinkResource.PMPROCESS_ACTIONS,
                    "ObjProps:Properties,operation_structure:Process Structure,OperationUsedBy:Used By,OperationUses:Uses,OperationAssembly:Manufacturing  Assembly,OperationResource:Resources,OperationMfg:Manufacturing Features,studies:Studies,pert:PERT View,references:References,ewiheader:EWI Header"},
      { UrlLinkResource.PMPROCESSRESOURCE_ACTIONS,
                    "ObjProps:Properties,resource_structure:Resource Structure,ResourceUsedBy:Used By,ResourceUses:Uses,ResourceAssembly:Manufacturing  Assembly,ResourceOperations:Operations,studies:Studies,references:References"},

      { "WTCHANGEACTIVITY_ACTION_LINKS",
                                       wt.lifecycle.LifeCycleHistoryActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.org.electronicIdentity.ElectronicSignaturesActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.lifecycle.SubmitActionDelegate.CONTEXT_SERVICE_NAME },

      { "WTCHANGEREQUEST_ACTION_LINKS",
                                       wt.lifecycle.LifeCycleHistoryActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.org.electronicIdentity.ElectronicSignaturesActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.lifecycle.SubmitActionDelegate.CONTEXT_SERVICE_NAME },

      { "WTCHANGEORDER_ACTION_LINKS",
                                       wt.lifecycle.LifeCycleHistoryActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.org.electronicIdentity.ElectronicSignaturesActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.lifecycle.SubmitActionDelegate.CONTEXT_SERVICE_NAME },
      { "REPORTTEMPLATE_ACTION_LINKS",
                                       wt.query.template.ReportTemplateExecuteActionDelegate.CONTEXT_SERVICE_NAME },
      { "CONFIGURATIONITEM_ACTION_LINKS",
                                       wt.lifecycle.LifeCycleHistoryActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.lifecycle.SubmitActionDelegate.CONTEXT_SERVICE_NAME },
      { "WTCHANGEACTIVITY_ACTION_LINKS_MIN",
                                       wt.lifecycle.LifeCycleHistoryActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.lifecycle.SubmitActionDelegate.CONTEXT_SERVICE_NAME },

      { "WTCHANGEACTIVITY2_ACTION_LINKS_MIN",
                                       wt.lifecycle.LifeCycleHistoryActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.lifecycle.SubmitActionDelegate.CONTEXT_SERVICE_NAME },

      { "WTCHANGEREQUEST_ACTION_LINKS_MIN",
                                       wt.lifecycle.LifeCycleHistoryActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.lifecycle.SubmitActionDelegate.CONTEXT_SERVICE_NAME },

      { "WTCHANGEREQUEST2_ACTION_LINKS_MIN",
                                       wt.lifecycle.LifeCycleHistoryActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.lifecycle.SubmitActionDelegate.CONTEXT_SERVICE_NAME },

      { "WTCHANGEORDER_ACTION_LINKS_MIN",
                                       wt.lifecycle.LifeCycleHistoryActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.lifecycle.SubmitActionDelegate.CONTEXT_SERVICE_NAME },

      { "WTCHANGEORDER2_ACTION_LINKS_MIN",
                                       wt.lifecycle.LifeCycleHistoryActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.lifecycle.SubmitActionDelegate.CONTEXT_SERVICE_NAME },

      { "MANAGEDBASELINE_ACTION_LINKS_MIN",
                                       wt.lifecycle.LifeCycleHistoryActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.lifecycle.SubmitActionDelegate.CONTEXT_SERVICE_NAME },

      { "REPORTTEMPLATE_ACTION_LINKS_MIN",
                                       wt.query.template.ReportTemplateExecuteActionDelegate.CONTEXT_SERVICE_NAME },
      { "CONFIGURATIONITEM_ACTION_LINKS_MIN",
                                       wt.lifecycle.LifeCycleHistoryActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.lifecycle.SubmitActionDelegate.CONTEXT_SERVICE_NAME },
      { "WTPRODUCTCONFIGURATION_ACTION_LINKS_MIN",
                                       wt.lifecycle.LifeCycleHistoryActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.org.electronicIdentity.ElectronicSignaturesActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.lifecycle.SubmitActionDelegate.CONTEXT_SERVICE_NAME },

/*<NEW>*/  { UrlLinkResource.WTUNIT_ACTIONS, "ObjProps:Properties" },

   };

   private static String getValuesForWTPART_ACTIONS() {
      if (InstalledProperties.isInstalled("WMF.WindchillProcessPlanner") && InstalledProperties.isInstalled("Windchill.EnterpriseSystemsIntegration"))
          return "ObjProps:Properties,part_structure:Product Structure,PartUses:Used By / Uses,PartReferences:References,BuildRuleBuildSource:Described By,PartReplacements:Replacements,PartAssembly:Manufacturing Assembly,AssociatedProcess:Related Process,Configurations:Configurations,ProductInstances:Product Instances,DistributionList:Distribution Targets";
      else if (InstalledProperties.isInstalled("WMF.WindchillProcessPlanner"))
          return "ObjProps:Properties,part_structure:Product Structure,PartUses:Used By / Uses,PartReferences:References,BuildRuleBuildSource:Described By,PartReplacements:Replacements,PartAssembly:Manufacturing Assembly,AssociatedProcess:Related Process,Configurations:Configurations,ProductInstances:Product Instances";
      else if (InstalledProperties.isInstalled("Windchill.EnterpriseSystemsIntegration"))
          return "ObjProps:Properties,part_structure:Product Structure,PartUses:Used By / Uses,PartReferences:References,BuildRuleBuildSource:Described By,PartReplacements:Replacements,AssociatedProcess:Related Process,Configurations:Configurations,ProductInstances:Product Instances,DistributionList:Distribution Targets";
      else
          return "ObjProps:Properties,part_structure:Product Structure,PartUses:Used By / Uses,PartReferences:References,BuildRuleBuildSource:Described By,PartReplacements:Replacements,AssociatedProcess:Related Process,Configurations:Configurations,ProductInstances:Product Instances";
   }
   private static String getValuesForWTSERIALNUMBEREDPART_ACTIONS() {
     if (InstalledProperties.isInstalled("Windchill.EnterpriseSystemsIntegration"))
            return "ObjProps:Properties,part_structure:Product Structure,PartUses:Used By / Uses,PartReferences:References,BuildRuleBuildSource:Described By,PartReplacements:Replacements,AssociatedProcess:Related Process,DistributionList:Distribution Targets";
     else
            return "ObjProps:Properties,part_structure:Product Structure,PartUses:Used By / Uses,PartReferences:References,BuildRuleBuildSource:Described By,PartReplacements:Replacements,AssociatedProcess:Related Process";
   }

   private static String getValuesForWTDOCUMENT_ACTIONS() {
      return "ObjProps:Properties,document_structure:Structure Hierarchy,DependencyView:References,DocUsesView:Structure Usage,Changes:Changes,Describes:Describes,AssociatedProcess:Related Process";
   }

   private static String getValuesForEPMDOCUMENT_ACTIONS() {
      return "ObjProps:Properties,EPMUses:Uses,EPMUsedBy:Used By,EPMDescribes:Describes,EPMReferences:References,EPMReferencedBy:Referenced By";
   }
}
