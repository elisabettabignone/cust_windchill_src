/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.enterprise;

import wt.util.resource.*;

@RBUUID("wt.enterprise.enterpriseResource")
public final class enterpriseResource extends WTListResourceBundle {
   @RBEntry("Error initializing \"{0}\".")
   public static final String ERROR_INITIALIZING = "0";

   @RBEntry("No \"{0}\" was specified in the HTML file.")
   public static final String NOT_SPECIFIED = "1";

   @RBEntry("Links")
   public static final String LINKS = "10";

   @RBEntry("Generation of this web page will now continue.")
   public static final String PAGE_GENERATION_CONTINUES = "100";

   @RBEntry("Generation of this web page has halted.")
   public static final String PAGE_GENERATION_HALTED = "101";

   @RBEntry("No items affected by this Change Order")
   public static final String NO_AFFECTS_RETURNED = "102";

   @RBEntry("No Change Activities returned")
   public static final String NO_CHANGEACTIVITIES_RETURNED = "103";

   @RBEntry("The related Change Order was not found")
   public static final String NO_CHANGEORDER_RETURNED = "104";

   @RBEntry("No Change Orders returned")
   public static final String NO_CHANGEORDERS_RETURNED = "105";

   @RBEntry("No Change Requests returned")
   public static final String NO_CHANGEREQUESTS_RETURNED = "106";

   @RBEntry("No items identified by this Change Request")
   public static final String NO_IDENTIFIES_RETURNED = "107";

   @RBEntry("No items were revised by this Change Activity")
   public static final String NO_REVISEDOBJS_RETURNED = "108";

   @RBEntry("Exception loading class {0} in {1}")
   public static final String ERROR_LOADING_CLASS = "109";

   @RBEntry("Template processing context object is null.")
   public static final String NULL_CONTEXT_OBJECT = "11";

   @RBEntry("ERROR : URLProcessor is only able to process HTTP GET Requests")
   public static final String INVALID_HTTP_REQUEST = "110";

   @RBEntry("** This part is not described by any sources **")
   public static final String NO_BUILD_SOURCE = "111";

   @RBEntry("Browse Cabinets")
   public static final String ALL_CABINETS_LABEL = "112";

   @RBEntry("You Are Here:")
   public static final String YOU_ARE_HERE = "113";

   @RBEntry(">")
   public static final String CONTEXT_PATH_SEPARATOR = "114";

   @RBEntry("/")
   public static final String ROOT_CABINET = "115";

   @RBEntry("** No Cabinets Found **")
   public static final String NO_CABINETS_MESSAGE = "116";

   @RBEntry("<b>Working Location:</b>")
   public static final String WORKING_LOCATION_NAME = "117";

   @RBEntry("Working Copy of {0}")
   public static final String WORKING_COPY_TITLE = "118";

   @RBEntry("Checked Out by {0}")
   public static final String CHECKED_OUT_BY_TITLE = "119";

   @RBEntry("null")
   public static final String NULL = "12";

   @RBEntry("Attribute")
   public static final String ATTRIBUTE_NAME_LABEL = "120";

   @RBEntry("Value")
   public static final String ATTRIBUTE_VALUE_LABEL = "121";

   @RBEntry("Context")
   public static final String ATTRIBUTE_DEPENDENCY_LABEL = "122";

   @RBEntry("** No associated attributes found **")
   public static final String NO_ATTRIBUTES_MESSAGE = "123";

   @RBEntry("Error making a non-persistent copy of object {0}.")
   public static final String COPY_NON_PERSISTENT = "124";

   @RBEntry("Error persisting copy of object {0}.")
   public static final String COPY_PERSISTENCE = "125";

   @RBEntry("Error copying attributes from original to new copy of object.")
   public static final String COPY_ATTRIBUTES = "126";

   @RBEntry("Object to copy is null.")
   public static final String COPY_NULL_OBJECT = "127";

   @RBEntry("Copy Rules either in wt.properties or dynamic were not found, using defaults.")
   public static final String COPY_RULES_WARNING = "128";

   @RBEntry("Not a valid Copy Rule.")
   public static final String COPY_INVALID_RULE = "129";

   @RBEntry("Unable to display \"{0}\" value.")
   public static final String UNDISPLAYABLE_VALUE = "13";

   @RBEntry("Can't copy object of class {0}, not supported.")
   public static final String COPY_BAD_CLASS = "130";

   @RBEntry("Supplied object {0} is null, cannot continue.")
   public static final String COPY_NULL_PARAMETER = "131";

   @RBEntry("Error, unexpected query results (of {0}) found for {1}.")
   public static final String COPY_BAD_QUERY = "132";

   @RBEntry("Saved From")
   public static final String MADEFROM_SUBTITLE = "133";

   @RBEntry("Saved As")
   public static final String MADEINTO_SUBTITLE = "134";

   @RBEntry("** None **")
   public static final String MADEFROM_EMPTY = "135";

   @RBEntry("Saved As History")
   public static final String MADE_FROM_URL_LABEL = "136";

   @RBEntry("Save As of {0} failed.  Check log for more information.")
   public static final String SAVEAS_FAILURE = "137";

   @RBEntry("Save As")
   public static final String SAVE_AS_URL_LABEL = "138";

   @RBEntry("Save As of {0} was successful")
   public static final String SAVEAS_SUCCESS = "139";

   @RBEntry("Update")
   public static final String UPDATE = "14";

   @RBEntry("{0} ({1})")
   @RBArgComment0("The qualified version, i.e. \"A.1\"")
   @RBArgComment1("The view the part is in, i.e. \"Engineering\"")
   public static final String PART_VERSION_DISPLAY = "140";

   @RBEntry("Items Described")
   public static final String DESCRIBES_HEADER = "141";

   @RBEntry("Items described by {0}")
   public static final String DESCRIBES_TITLE = "142";

   @RBEntry("Help")
   public static final String DEFAULT_HELP_LABEL = "143";

   @RBEntry("In Personal Cabinet")
   public static final String IN_PERSONAL_CABINET_LABEL = "144";

   @RBEntry("<BR><H3>\nTask : Retrieving a TemplateProcessor from Factory\n</H3><BR><B>\nFactory : {0}\n<BR>\nString Selector : {1}\n<BR>\nClass Selector : {2}\n<BR>\nType of Failure : Corresponding entry in property file does not exist\n<B><BR>\n")
   public static final String SNFE_TEMPLATEPROCESSOR = "145";

   @RBEntry("<BR><H3>\nTask : Retrieving a TemplateProcessor from Factory\n</H3><BR><B>\nFactory : {0}\n<BR>\nString Selector : {1}\n<BR>\nClass Selector : {2}\n<BR>\nType of Failure : Either unable to find entry in properties file or unable to instantiate/find class listed in properties file\n\n<B><BR><BR>\n")
   public static final String UTCSE_TEMPLATEPROCESSOR = "146";

   @RBEntry("Name")
   public static final String EPM_NAME = "147";

   @RBEntry("Number")
   public static final String EPM_NUMBER = "148";

   @RBEntry("Placed")
   public static final String EPM_PLACED = "149";

   @RBEntry("View")
   public static final String VIEW = "15";

   @RBEntry("Required")
   public static final String EPM_REQUIRED = "150";

   @RBEntry("Suppressed")
   public static final String EPM_SUPPRESSED = "151";

   @RBEntry("Quantity")
   public static final String EPM_QUANTITY = "152";

   @RBEntry("Owner Application")
   public static final String EPM_OWNER_APP = "153";

   @RBEntry("Description")
   public static final String EPM_DESCRIPTION = "154";

   @RBEntry("Comment:")
   public static final String COMMENT_LABEL = "155";

   @RBEntry("*")
   public static final String LINK_PREFIX = "156";

   @RBEntry("**This part has no alternates or you are not permitted to view alternates.**")
   public static final String NO_PART_ALTERNATES = "157";

   @RBEntry("**This part is not an alternate or you are not permitted to view alternates.**")
   public static final String NO_PARTS_ALTERNATE_FOR = "158";

   @RBEntry("**This part has no substitutes or you are not permitted to view substitutes.**")
   public static final String NO_PART_SUBSTITUTES = "159";

   @RBEntry("Modified By")
   public static final String UPDATED_BY = "16";

   @RBEntry("**This part is not a substitute or you are not permitted to view substitutes.**")
   public static final String NO_PARTS_SUBSTITUTE_FOR = "160";

   @RBEntry("REPLACEMENTS")
   public static final String REPLACEMENTS_TITLE = "161";

   @RBEntry("Alternates")
   public static final String ALTERNATES = "162";

   @RBEntry("Substitutes")
   public static final String SUBSTITUTES = "163";

   @RBEntry("Alternate for")
   public static final String ALTERNATE_FOR = "164";

   @RBEntry("Substitute for")
   public static final String SUBSTITUTE_FOR = "165";

   @RBEntry("Alternates of Part {0}")
   public static final String ALTERNATES_HEADER = "166";

   @RBEntry("Substitutes of Part {0}")
   public static final String SUBSTITUTES_HEADER = "167";

   @RBEntry("Parts for Which Part {0} is an Alternate")
   public static final String ALTERNATE_FOR_HEADER = "168";

   @RBEntry("Parts for Which Part {0} is a Substitute")
   public static final String SUBSTITUTE_FOR_HEADER = "169";

   @RBEntry("KB")
   public static final String KILOBYTES = "17";

   @RBEntry("Part Number")
   public static final String SUB_PART_NUMBER_HEADER = "170";

   @RBEntry("Part Name")
   public static final String SUB_PART_NAME_HEADER = "171";

   @RBEntry("Assembly Number")
   public static final String ASSEMBLY_NUMBER_HEADER = "172";

   @RBEntry("Assembly Name")
   public static final String ASSEMBLY_NAME_HEADER = "173";

   @RBEntry("Search")
   public static final String SEARCH_LABEL = "174";

   @RBEntry("<BR><H3>\nTask : Retrieving a ActionDelegate from Factory\n</H3><BR><B>\nFactory : {0}\n<BR>\nString Selector : {1}\n<BR>\nClass Selector : {2}\n<BR>\nType of Failure : Either unable to find entry in properties file or unable to instantiate/find class listed in properties file\n\n<BR> <P>\nIf you do not require State/Access checking you can ignore this exception\n<BR></B>\n\n")
   public static final String UTCSE_ACTIONDELEGATE = "176";

   @RBEntry("<B>Error:</B> The {0} you are trying to view contains too many objects to display, and only the first <B>{1}</B> objects retrieved are being displayed.  If you are looking for a particular object, you might want to try searching for it instead.")
   public static final String PARTIAL_RESULTS_RETURNED = "177";

   @RBEntry("Windchill Clipboard")
   public static final String VISUALIZATION_CLIPBOARD_LABEL = "178";

   @RBEntry("Unable to display thumbnail and/or other visualization data")
   public static final String ERROR_DISPLAYING_VISUAL_DATA = "179";

   @RBEntry("Downloading primary content...")
   public static final String DOWNLOAD_PRIMARY = "180";

   @RBEntry("Preferences")
   public static final String PREFERENCES_LABEL = "181";

   @RBEntry("Domain:")
   public static final String DOMAIN_LABEL = "182";

   @RBEntry("{0} (Inherited from cabinet)")
   public static final String INHERITED_FROM_CABINET = "183";

   @RBEntry("{0} (Inherited from parent folder)")
   public static final String INHERITED_FROM_FOLDER = "184";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String FOLDER_BROWSER_TITLE = "185";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String FOLDER_LOCATION_LABEL = "186";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String FOLDER_SELECT_ITEM = "187";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String FOLDER_SELECT_TITLE = "188";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String FOLDER_OPTIONS_LABEL = "189";

   @RBEntry("URL")
   public static final String URL = "18";

   @RBEntry("Descriptive Name")
   public static final String URL_NAME = "19";

   @RBEntry(" [ \"{0}\" property is not available.]")
   public static final String PROPERTY_NOT_AVAILABLE = "2";

   @RBEntry("View parent folder")
   public static final String VIEW_PARENT_FOLDER = "20";

   @RBEntry("** This {0} is empty **")
   public static final String NO_FOLDER_CONTENTS = "21";

   @RBEntry("Active Links")
   public static final String ACTIVE_LINKS = "22";

   @RBEntry("Passive Links")
   public static final String PASSIVE_LINKS = "23";

   @RBEntry("Uses")
   public static final String USES = "24";

   @RBEntry("** This part uses no other parts **")
   public static final String NO_PART_USES = "25";

   @RBEntry("** No other parts use this part **")
   public static final String NO_PART_WHERE_USED = "26";

   @RBEntry("** This {0} has no contents **")
   public static final String NO_CONTENTS = "27";

   @RBEntry("** This {0} is not referenced by any parts **")
   public static final String NO_REFERENCED_BY = "28";

   @RBEntry("Number")
   public static final String NUMBER = "29";

   @RBEntry("Invalid applet parameters. \"{0}\"")
   public static final String INVALID_PARAMETERS = "3";

   @RBEntry("Latest Version")
   public static final String LATEST_VERSION = "30";

   @RBEntry("Type")
   public static final String TYPE = "31";

   @RBEntry("** No document references for this part **")
   public static final String NO_DOCUMENTS_REFERENCED = "32";

   @RBEntry("Version")
   public static final String VERSION = "33";

   @RBEntry("State")
   public static final String STATE = "34";

   @RBEntry("Team")
   public static final String TEAMTEMPLATE = "35";

   @RBEntry("Last Modified")
   public static final String LAST_MOD_DATE = "36";

   @RBEntry("Created By")
   public static final String CREATED_BY = "37";

   @RBEntry("Created On")
   @RBComment("The date on which the object was created (Per Terminology CFT 05 13 2005  SPR 1321929 01)")
   public static final String CREATION_DATE = "38";

   @RBEntry("** No associated changes found **")
   public static final String NO_CHANGES = "39";

   @RBEntry("Exception retrieving the object: \"{0}\"")
   public static final String RETRIEVING_OBJECT = "4";

   @RBEntry("The URL must identify two or more items")
   public static final String NOT_ENOUGH_OIDS = "41";

   @RBEntry("Related Items")
   public static final String RELATED_ITEMS = "42";

   @RBEntry("No differences were found")
   public static final String NO_DIFFERENCE_FOUND = "43";

   @RBEntry("yes")
   public static final String HAS_RELATION = "44";

   @RBEntry("no")
   public static final String HAS_RELATION_NOT = "45";

   @RBEntry("Template Processor Invocation Failed")
   public static final String TP_FAILED_TITLE = "46";

   @RBEntry("An attempt to display a web page via a template processor has failed.  Contact your administrator for assistance.  Information for their reference follows:")
   public static final String TP_FAILED_MESSAGE = "47";

   @RBEntry("Compare")
   public static final String COMPARE_BUTTON_LABEL = "49";

   @RBEntry(" \"{0}\" action is not available for \"{1}\".")
   public static final String ACTION_NOT_AVAILABLE = "5";

   @RBEntry("No object in context")
   public static final String NO_OBJECT = "50";

   @RBEntry("Check out of {0} failed.  This object is already checked out.")
   public static final String CHECKOUT_FAILED = "51";

   @RBEntry("{0} has been successfully checked out")
   public static final String CHECKOUT_SUCCESS = "52";

   @RBEntry("The \"{0}\" parameter is \"{1}\".")
   public static final String PARAMETER_IS = "53";

   @RBEntry("Invalid parameters. \"{0}\"")
   public static final String BAD_PARAMETERS = "54";

   @RBEntry("Described By")
   public static final String DESCRIBED_BY = "55";

   @RBEntry("References")
   public static final String REFERENCES = "56";

   @RBEntry("Personal Cabinet")
   public static final String PERSONAL_CABINET_LABEL = "57";

   @RBEntry("Worklist")
   public static final String WORKLIST_LABEL = "58";

   @RBEntry("Local Search")
   public static final String LOCAL_SEARCH_LABEL = "59";

   @RBEntry("Name")
   public static final String NAME = "6";

   @RBEntry("Enterprise Search")
   public static final String ENTERPRISE_SEARCH_LABEL = "60";

   @RBEntry("Create Document")
   public static final String IMPORT_LABEL = "61";

   @RBEntry("Checked Out Folder")
   public static final String CHECKOUT_FOLDER_LABEL = "62";

   @RBEntry("Iteration")
   public static final String ITERATION = "63";

   @RBEntry("All Versions")
   public static final String ALL_VERSIONS_URL_LABEL = "64";

   @RBEntry("Check In")
   public static final String CHECK_IN_URL_LABEL = "65";

   @RBEntry("Check Out")
   public static final String CHECK_OUT_URL_LABEL = "66";

   @RBEntry("Version History")
   public static final String VERSION_HISTORY_URL_LABEL = "67";

   @RBEntry("Iteration History")
   public static final String ITERATION_HISTORY_URL_LABEL = "68";

   @RBEntry("Share Status")
   public static final String SHARESTATUS_URL_LABEL = "69";

   @RBEntry("Format")
   public static final String FORMAT = "7";

   @RBEntry("Unable to obtain a localized resource string because the resource class: {1} can not be found.  Resource key is: {0}")
   public static final String NO_RESOURCEKEY_CLASS = "74";

   @RBEntry("Unable to obtain a localized resource string because the resource key: {0} can not be found.  Resource class is: {1}")
   public static final String NO_RESOURCEKEY_FIELD = "75";

   @RBEntry("User is not permitted to {0}.")
   public static final String NOT_PERMITTED = "76";

   @RBEntry("read")
   public static final String READ = "77";

   @RBEntry("modify")
   public static final String MODIFY = "78";

   @RBEntry("The context object is not {0}.")
   public static final String OBJECT_NOT = "79";

   @RBEntry("Size")
   public static final String SIZE = "8";

   @RBEntry("Query on object {1} failed in call to {0}.")
   public static final String QUERY_FAILED_MESSAGE = "80";

   @RBEntry("No column names have been created. Cannot build table.")
   public static final String NO_COLUMN_NAMES = "81";

   @RBEntry("No entry for the column names in wt.properties.")
   public static final String NO_COLUMN_NAME_PROPERTIES = "82";

   @RBEntry("Error creating configSpec. Cannot generate query.")
   public static final String CONFIG_SPEC_ERROR = "83";

   @RBEntry("Creation of general information for object failed.")
   public static final String OBJ_ID_FAILED = "84";

   @RBEntry("Requested query service is not available or does not exist.")
   public static final String NO_QUERYSERVICE_NAME = "85";

   @RBEntry("Introspection Exception : Possibly used an invalid derived attribute")
   public static final String INTROSPECTION_EXCEPTION = "86";

   @RBEntry("Invalid customization of HTML Template attempted.")
   public static final String INVALID_CUSTOMIZATION = "87";

   @RBEntry("Initialization of the Table failed.")
   public static final String INITIALIZATION_OF_TABLE_FAILED = "88";

   @RBEntry("<CENTER>** {0} **</CENTER>")
   public static final String NO_TABLE_ENTRIES = "89";

   @RBEntry("Last Modified")
   public static final String MODIFIED = "9";

   @RBEntry("Check in of {0} failed.  This object is not checked out.")
   public static final String CHECKIN_FAILED = "90";

   @RBEntry("{0} has been successfully checked in")
   public static final String CHECKIN_SUCCESS = "91";

   @RBEntry("Home")
   public static final String HOMEPAGE_URL_LABEL = "93";

   @RBEntry("Life Cycle History")
   public static final String LIFECYCLE_HISTORY_LABEL = "94";

   @RBEntry("ERROR")
   public static final String ERROR = "95";

   @RBEntry("During the dynamic generation of this web page, while processing the Windchill script language method \"{0}\" .")
   public static final String DURING_PAGE_GENERATION = "96";

   @RBEntry("During the dynamic generation of this web page.")
   public static final String DURING_PAGE_GENERATION_2 = "97";

   @RBEntry("The error message is:")
   public static final String ERROR_MESSAGE_IS = "98";

   @RBEntry("A stack trace has been printed to the log.")
   public static final String STACK_TRACE_PRINTED = "99";

   @RBEntry("Date")
   public static final String DATE = "190";

   @RBEntry("Team")
   public static final String TEAM = "191";

   @RBEntry(" Details of original")
   public static final String DETAILS_OF_ORIG = "192";

   @RBEntry("Work in progress ({0})")
   public static final String WORK_IN_PROGRESS = "193";

   @RBEntry("Team Name")
   public static final String TEAM_NAME = "194";

   @RBEntry("Awaiting Promotion")
   public static final String AWAITING_PROMOTION = "195";

   @RBEntry("Status")
   public static final String STATUS = "196";

   @RBEntry("Location")
   public static final String LOCATION = "197";

   @RBEntry("Life Cycle")
   public static final String LIFE_CYCLE = "198";

   @RBEntry("Model Name")
   public static final String EPM_CAD_NAME = "199";

   @RBEntry("Instance Type")
   public static final String EPM_INSTANCE_TYPE = "200";

   @RBEntry("My Workspaces")
   public static final String MY_WORKSPACE = "201";

   @RBEntry("URL Post Error: Invalid HTTP Request")
   public static final String URL_POST_ERROR = "202";

   @RBEntry("InputStream Error: InputStream was not found")
   public static final String MPINPUTSTREAM_NULL_ERROR = "203";

   @RBEntry("Form Processor Exception: Invalid URL Posting Method")
   public static final String PROCESSFORM_POST_ERROR = "204";

   @RBEntry("Form Processor Exception: Input Value was Null")
   public static final String PROCESSFORM_NULL_ERROR = "205";

   @RBEntry("Invalid Class Error:  String variable for Class represents an invalid Class")
   public static final String INVALID_CLASS_ERROR = "206";

   @RBEntry("Invalid URL Error: No action parameter passed")
   public static final String INVALID_URL_ERROR = "207";

   @RBEntry("Access to action denied : Either insufficient permissions or state of object not valid for action")
   public static final String ACCESS_DENIED_ERROR = "208";

   @RBEntry("Actions")
   public static final String ACTION_COLUMN_LABEL = "209";

   @RBEntry("View Units in:")
   @RBComment("label for a dropdown menu giving alternative measurement unit systems for displaying IBA values (e.g., SI, USCS)")
   public static final String IBA_DISPLAY_UNITS_DROPDOWN_LABEL = "210";

   @RBEntry("Change")
   @RBComment("label on button next to measurement systems dropdown menu; causes IBA values to be redisplayed using selected measurment units")
   public static final String IBA_DISPLAY_UNITS_BUTTON_LABEL = "211";

   @RBEntry("Organization ID:")
   @RBComment("label for an organization identifier attribute")
   public static final String ORGANIZATION_IDENTIFIER = "212";

   @RBEntry("Invalid multi-copy condition: Different revisions of the same master not allowed.")
   public static final String INVALID_MULTI_COPY_CONDITION = "213";

   @RBEntry("Owner:")
   @RBComment("A label for a field displaying the owner of an object, e.g., the owner of a personal cabinet")
   public static final String OWNER_LABEL = "214";

   @RBEntry("Copied From")
   public static final String COPYFROM_SUBTITLE = "215";

   @RBEntry("Copied To")
   public static final String COPYTO_SUBTITLE = "216";

   @RBEntry("Pending Change")
   @RBComment("tool tip that is displayed when user mouses over the image that indicates a pending change exists on the object")
   public static final String PENDING_CHANGE = "217";

   @RBEntry("Authoring Application")
   public static final String EPM_AUTH_APP = "218";

   @RBEntry("Template:")
   @RBComment("Used to indicate that the object is a template.")
   public static final String TEMPLATE = "219";

   @RBEntry("Yes - Enabled")
   @RBComment("\"Yes\" indicates that the object is a template; \"Enabled\" indictates that the object is available for use.")
   public static final String TEMPLATE_AVAILABLE = "220";

   @RBEntry("Yes - Disabled")
   @RBComment("\"Yes\" - indicates that the object is a template; \"Disabled\" indictates that the object is not available for use.")
   public static final String TEMPLATE_UNAVAILABLE = "221";

   @RBEntry("Distribution Target History for")
   public static final String DISTRIBUTION_LIST_TITLE = "222";

   @RBEntry("Create Document from Template")
   public static final String DOCFROMTEMPLATE_LABEL = "223";

   @RBEntry("Paste")
   @RBComment("Paste an ojbect to the clipboard")
   public static final String PASTE = "224";

   @RBEntry("CAD Document")
   @RBComment("a general purpose label for displaying the generic label for CAD Document in the messages")
   public static final String EPMDOCUMENT_LABEL = "225";

   @RBEntry("Part")
   @RBComment("a general purpose label for displaying the generic label for Part in the messages")
   public static final String WTPART_LABEL = "226";

   @RBEntry("Enterprise Systems Transaction Log")
   @RBComment("Link to the ESI transaction log page")
   public static final String ESI_TRANS_LOG_LABEL = "227";

   @RBEntry("Share Status")
   @RBComment("Share status link label")
   public static final String SHARE_STATUS_URL_LABEL = "228";

   @RBEntry("Checked Out to Project {0} by {1}")
   @RBComment("possible value of the attribute Status on Details pages of parts and documents")
   public static final String CHECKED_OUT_TO_PROJECT_W_NAME_BY = "229";

   @RBEntry("Checked Out to Project by {0}")
   @RBComment("possible value of the attribute Status on Details pages of parts and documents")
   public static final String CHECKED_OUT_TO_PROJECT_BY = "230";

   @RBEntry("Checked Out to Project {0}")
   @RBComment("possible value of the attribute Status on Details pages of parts and documents")
   public static final String CHECKED_OUT_TO_PROJECT_W_NAME = "231";

   @RBEntry("Checked Out to Project")
   @RBComment("possible value of the attribute Status on Details pages of parts and documents")
   public static final String CHECKED_OUT_TO_PROJECT = "232";

   @RBEntry("Context")
   @RBComment("Label for the Container Name")
   public static final String CONTEXT = "233";

   @RBEntry("Uniqueness violation found on new copy of {0}. Unable to save the copy.")
   @RBComment("uniqueness exception encountered during Save As.")
   public static final String UNIQUE_ERROR = "234";

   @RBEntry("Object Initialization Rules Administrator")
   @RBComment("Link to the Rule Administrator page")
   public static final String RULE_ADMIN_LABEL = "235";

   @RBEntry("None")
   @RBComment("None of the versions are enabled")
   public static final String NONE = "236";

   @RBEntry("Enabled Version:")
   @RBComment("Version of the object that is enabled")
   public static final String ENABLED_VER = "237";

   @RBEntry("Report Manager")
   @RBComment("Link to the Report Manager page")
   public static final String REPORT_MANAGER = "238";

   @RBEntry("Line")
   @RBComment("Label for the line number column in the Product Structure page table")
   public static final String LINE_NUMBER_COLUMN_LABEL = "239";

   @RBEntry("Object Comparison Failed : Insufficient Number of Objects")
   @RBComment("Title for Insufficient Number of Objects error page")
   public static final String OBJ_COMPARE_FAIL_TITLE = "240";

   @RBEntry("No comparison done.")
   @RBComment("First line for Insufficient Number of Objects error page")
   public static final String OBJ_COMPARE_FAIL_LINE_1 = "241";

   @RBEntry("You must select two or more objects to compare.")
   @RBComment("Second line for Insufficient Number of Objects error page")
   public static final String OBJ_COMPARE_FAIL_LINE_2 = "242";

   @RBEntry("Please click on the back button and make your selections.")
   @RBComment("Third line for Insufficient Number of Objects error page (Non PDMLink only)")
   public static final String OBJ_COMPARE_FAIL_LINE_3 = "243";

   @RBEntry("You do not have access to this object or it does not exist.")
   public static final String OBJECT_GONE = "244";

   @RBEntry("WARNING: Unable to create HelpLink for the Context Action: {0}")
   @RBComment("This is a debug message which is only displayed in the logs if there is a configuration error.  The context action is the help action such as VIEW_OBJECT etc.")
   public static final String UNABLE_TO_CREATE_HELPLINK = "245";

   @RBEntry("Distribution Targets")
   @RBComment("This string is used in the HTML")
   public static final String DISTRIBUTION_LIST = "246";

   @RBEntry("Distribution Target History")
   @RBComment("This string is used in the HTML")
   public static final String DISTRIBUTION_LIST_HISTORY = "247";

   @RBEntry("Go To Latest")
   @RBComment("This string is used in the HTML")
   public static final String GO_TO_LATEST = "248";

   @RBEntry("Purge Management")
   @RBComment("Label for the link to the Purge Administrator ui")
   public static final String PURGE_ADMINISTRATOR = "249";

   @RBEntry("Create and manage purge jobs.")
   @RBComment("Purge administrator description")
   public static final String PURGE_ADMINISTRATOR_DESCRIPTION = "250";

   @RBEntry("Configuration File Generator")
   @RBComment("Label for the link to the Configuration File Generator utility")
   public static final String CONFIGURATION_FILE_GENERATOR = "251";

   @RBEntry("Generate or Update the DCA configurations for the system's type definitions (modeled and soft types) to be used for customizing search.")
   @RBComment("Configuration File Generator utility description")
   public static final String CONFIGURATION_FILE_GENERATOR_DESCRIPTION = "252";

   @RBEntry("The page you are requesting is only available with Windchill PDMLink. This Pro/INTRALINK installation does not include these Windchill PDMLink capabilities.")
   @RBComment("Message when someone tries to get access to a feature that is not available for PRO/I")
   public static final String PROI_INSTALLED_FEATURE_NOT_AVAILABLE = "253";

   @RBEntry("Another process has made the action you selected unavailable. Refresh the page to update the actions.")
   @RBComment("Message when someone tries to perform an action that is no longer valid due to a system state change that has occured since the action was displayed.")
   public static final String ACTION_NO_LONGER_VALID = "254";

   /**
    * The strings below are already translated in \Windchill\src\wt\enterprise\htmltmpl\forms\SetWTPartEffectivityConfigSpec_*.html
    * take the translations from those files before remofing the translated template file.
    **/
   @RBEntry("Please provide an Effectivity Context to be associated with the given value")
   public static final String SET_WTPART_EFFECTIVITY_CONFIG_SPEC_ALERT = "255";

   @RBEntry("Latest")
   public static final String SET_WTPART_EFFECTIVITY_CONFIG_SPEC_LATEST_LABEL = "256";

   @RBEntry("Baseline")
   public static final String SET_WTPART_EFFECTIVITY_CONFIG_SPEC_BASELINE_LABEL = "257";

   @RBEntry("Effectivity")
   public static final String SET_WTPART_EFFECTIVITY_CONFIG_SPEC_EFFECTIVITY_LABEL = "258";

   @RBEntry("Set Effectivity Configuration Specification")
   public static final String SET_WTPART_EFFECTIVITY_CONFIG_SPEC = "259";

   @RBEntry("View:")
   public static final String SET_WTPART_EFFECTIVITY_CONFIG_SPEC_VIEW_LABEL = "260";

   @RBEntry("Effectivity Context:")
   public static final String SET_WTPART_EFFECTIVITY_CONFIG_SPEC_CONTEXT_LABEL = "261";

   @RBEntry("Search...")
   public static final String SET_WTPART_EFFECTIVITY_CONFIG_SPEC_SEARCH_BUTTON = "262";

   @RBEntry("Effective Date:")
   public static final String SET_WTPART_EFFECTIVITY_CONFIG_SPEC_EFFECTIVE_DATE = "263";

   @RBEntry("Value:")
   public static final String SET_WTPART_EFFECTIVITY_CONFIG_SPEC_VALUE_LABEL = "264";

   @RBEntry("Use for this session only")
   public static final String SET_WTPART_EFFECTIVITY_CONFIG_SPEC_SESSION_ONLY = "265";

   @RBEntry("Ok")
   public static final String SET_WTPART_EFFECTIVITY_CONFIG_SPEC_OK = "266";

   /**
    * End of strings from SetWTPartEffectivityConfigSpec_*.html
    **/
   @RBEntry("Purge, Archive, and Restore Management")
   @RBComment("Label for the link to the Purge, Archive, and Restore Administrator ui when an archive system is installed")
   public static final String PURGE_ARCHIVE_RESTORE_ADMINISTRATOR = "267";

   @RBEntry("Create purge jobs to permanently remove data from the Windchill system, or, optionally, create an archive of the data to restore at a later time.")
   @RBComment("Purge, Archive, and Restore Administrator description when an archive system is installed")
   public static final String PURGE_ARCHIVE_RESTORE_ADMINISTRATOR_DESCRIPTION = "268";

   @RBEntry("PROXY DOCUMENT MASTER")
   @RBComment("This string is used in the HTML")
   public static final String PROXY_DOCUMENT_MASTER = "279";

   @RBEntry("PROXY PART MASTER")
   @RBComment("This string is used in the HTML")
   public static final String PROXY_PART_MASTER = "280";

   @RBEntry("Revision")
   public static final String REVISION = "281";

   @RBEntry("Parts Being Compared")
   @RBComment("This string is used in the HTML ")
   public static final String PARTS_BEING_COMPARED = "282";

   @RBEntry("This system is not configured to allow copying across organizational boundaries.")
   public static final String ONDEMAND_INVALID_COPY = "290";

   @RBEntry("Check In {0}")
   public static final String CHECK_IN_LABEL = "300";

   @RBEntry("CheckIn Document")
   public static final String CHECK_IN_DOCUMENT_LABEL = "301";

   @RBEntry("Comments:")
   public static final String COMMENTS_COLON = "302";

   @RBEntry("IMPORT DOCUMENT")
   public static final String IMPORT_DOCUMENT = "303";

   @RBEntry("*Number:")
   public static final String REQUIRED_NUMBER_COLON = "304";

   @RBEntry("Department:")
   public static final String DEPARTMENT_COLON = "305";

   @RBEntry("Title:")
   public static final String TITLE_COLON = "306";

   @RBEntry("*Location:")
   public static final String REQUIRED_LOCATION_COLON = "307";

   @RBEntry("Description:")
   public static final String DESCRIPTION_COLON = "308";

   @RBEntry("LifeCycle:")
   public static final String LIFE_CYCLE_COLON = "309";

   @RBEntry("Team:")
   public static final String TEAM_COLON = "310";

   @RBEntry("*File:")
   public static final String REQUIRED_FILE_COLON = "311";

   @RBEntry("Location:")
   public static final String LOCATION_COLON = "312";

   @RBEntry("Saving from")
   public static final String SAVING_FROM = "313";

   @RBEntry("*LifeCycle:")
   public static final String REQUIRED_LIFE_CYCLE_COLON = "314";

   @RBEntry("Set Baseline Configuration Specification")
   public static final String SET_BASELINE_CONFIG_SPEC_LABEL = "315";

   @RBEntry("Baseline")
   public static final String BASELINE = "316";

   @RBEntry("Set Latest Configuration Specification")
   public static final String SET_LATEST_CONFIG_SPEC_LABEL = "317";

   @RBEntry("Include parts in my personal cabinet")
   public static final String INCLUDE_PARTS_IN_PERSONAL_CABINET = "318";

   @RBEntry("Iteration History of {0}")
   public static final String ITERATION_HISTORY_LABEL = "319";

   @RBEntry("Baseline Comparison")
   public static final String BASELINE_COMPARISON_LABEL = "320";

   @RBEntry("Comparing Baselines")
   public static final String COMPARING_BASELINE = "321";

   @RBEntry("Compare Items")
   public static final String COMPARE_ITEMS_LABEL = "322";

   @RBEntry("Comparing Items")
   public static final String COMPARING_ITEMS_LABEL = "323";

   @RBEntry("Compare Documents")
   public static final String COMPARE_DOCUMENTS_LABEL = "324";

   @RBEntry("Comparing Documents")
   public static final String COMPARING_DOCUMENTS = "325";

   @RBEntry("Contents:")
   public static final String CONTENTS_COLON = "326";

   @RBEntry("Uses:")
   public static final String USES_COLON = "327";

   @RBEntry("References Documents:")
   public static final String REFERENCES_DOCUMENTS_COLON = "328";

   @RBEntry("Describes Parts:")
   public static final String DESCRIBES_PARTS_COLON = "329";

   @RBEntry("Compare Revised Items")
   public static final String COMPARE_REVISED_ITEMS_LABEL = "330";

   @RBEntry("Comparing Revised Items")
   public static final String COMPARING_REVISED_ITEMS_LABEL = "331";

   @RBEntry("Object compare not support for these items")
   public static final String OBJECT_SUPPORT_BOT_FOR_THESE_ITEMS = "332";

   @RBEntry("Compare Parts")
   public static final String COMPARE_PARTS_LABEL = "333";

   @RBEntry("Related Parts:")
   public static final String RELATED_PARTS_COLON = "334";

   @RBEntry("Related  Documents:")
   public static final String RELATED_DOCUMENTS_COLON = "335";

   @RBEntry("Related CAD Document:")
   public static final String RELATED_CAD_DOCUMENTS_COLON = "336";

   @RBEntry("Content of {0}")
   public static final String CONTENT_OF_LABEL = "337";

   @RBEntry("CONTENTS")
   public static final String CAP_CONTENTS = "338";

   @RBEntry("Content in {0}")
   public static final String CONTENT_IN_LABEL = "339";

   @RBEntry("location")
   public static final String S_LOCATION = "340";

   @RBEntry("version")
   public static final String S_VERSION = "341";

   @RBEntry("state")
   public static final String S_STATE = "342";

   @RBEntry("Browse Cabinets")
   public static final String BROWSE_CABINETS_LABEL = "343";

   @RBEntry("Described By Results")
   public static final String DESCRIBED_BY_RESULTS_LABEL = "344";

   @RBEntry("Contents of {0}")
   public static final String CONTENTS_OF_LABEL = "345";

   @RBEntry("ChangeActivity Query Results")
   public static final String CHANGE_ACTIVITY_QUERY_RESULTS_LABEL = "346";

   @RBEntry("ChangeOrder Query Results")
   public static final String CHANGE_ORDER_QUERY_RESULTS_LABEL = "347";

   @RBEntry("ChangeOrders Query Results")
   public static final String CHANGE_ORDERS_QUERY_RESULTS_LABEL = "348";

   @RBEntry("ChangeRequest Query Results")
   public static final String CHANGE_REQUESTS_QUERY_RESULTS_LABEL = "349";

   @RBEntry("Changes for {0}")
   public static final String CHANGES_FOR_LABEL = "350";

   @RBEntry("Items described by {0}")
   public static final String ITEMS_DESCRIBED_BY_LABEL = "351";

   @RBEntry("Parts Described by {0}")
   public static final String PARTS_DESCRIBED_BY_LABEL = "352";

   @RBEntry("Documents that reference {0}")
   public static final String DOCUMENTS_THAT_REFERENCE_LABEL = "353";

   @RBEntry("Documents referenced by {0}")
   public static final String DOCUMENTS_REFERENCED_BY_LABEL = "354";

   @RBEntry("Documents that Use {0}")
   public static final String DOCUMENTS_THAT_USE_LABEL = "355";

   @RBEntry("Documents used by {0}")
   public static final String DOCUMENTS_USED_BY_LABEL = "356";

   @RBEntry("All Versions of {0}")
   public static final String ALL_VERSIONS_OF_LABEL = "357";

   @RBEntry("References for {0}")
   public static final String REFERENCES_FOR_LABEL = "358";

   @RBEntry("Used by for {0}")
   public static final String USED_BY_FOR_LABEL = "359";

   @RBEntry("Used by / Uses for {0}")
   public static final String USED_BY_USES_FOR_LABEL = "360";

   @RBEntry("Used by")
   public static final String USED_BY_LABEL = "361";

   @RBEntry("Uses")
   public static final String USES_LABEL = "362";

   @RBEntry("Used by:")
   public static final String USED_BY_COLON = "363";

   @RBEntry("Referenced By Table")
   public static final String REFERENCED_BY_TABLE_LABEL = "365";

   @RBEntry("{0} is referenced by")
   public static final String IS_REFERENCED_BY = "366";

   @RBEntry("RevisedObjs Query Results")
   public static final String REVISED_OBJS_QUERY_RESULTS = "367";

   @RBEntry("Product Structure for {0}")
   public static final String PRODUCT_STRUCTURE_FOR_LABEL = "368";

   @RBEntry("UsedBy Query Results")
   public static final String USED_BY_QUERY_RESULTS = "369";

   @RBEntry("Uses Query Results")
   public static final String USES_QUERY_RESULTS = "370";

   @RBEntry("Saved As History {0}")
   public static final String SAVED_AS_HISTORY_LABEL = "371";

   @RBEntry("Saved As History for {0}")
   public static final String SAVED_AS_HISTORY_FOR_LABEL = "372";

   @RBEntry("Error found")
   public static final String ERROR_FOUND_LABEL = "373";

   @RBEntry("Error Found ...")
   public static final String ERROR_FOUND_EXTN = "374";

   @RBEntry("Version History of {0}")
   public static final String VERSION_HISTORY_OF_LABEL = "375";

   @RBEntry("Submit")
   public static final String SUBMIT = "376";

   @RBEntry("Import Document")
   public static final String IMPORT_DOCUMENT_LABEL = "377";

   @RBEntry("Type:")
   public static final String TYPE_COLON = "378";

   @RBEntry("Browse Folders")
   public static final String BROWSE_FOLDERS_LABEL = "379";

   @RBEntry("Open")
   public static final String OPEN = "380";

   @RBEntry("Browse...")
   public static final String BROWSE = "381";

   @RBEntry("&nbsp;&nbsp; OK &nbsp;&nbsp;")
   public static final String OK_BUTTON = "382";

   @RBEntry("Search...")
   public static final String SEARCH_BUTTON = "383";

   @RBEntry("State:")
   public static final String STATE_COLON = "384";

   @RBEntry("Comparison of these items is not supported")
   public static final String COMPARISON_NOT_SUPPORTED = "385";

   @RBEntry("Sources that describe {0}")
   public static final String SOURCES_THAT_DESCRIBE = "386";

   @RBEntry("{0} is <B>Used By:</B>")
   public static final String IS_USED_BY = "387";

   @RBEntry("No value has been given for the baseline to use in the configuration specification.  Please enter the name of the baseline or search for a baseline.")
   @RBComment("Do not remove the newline characters. Place newline characters in appropriate places for foreign text.")
   public static final String VALIDATE_SETBASECONFIGSPEC_ALERT = "388";

   @RBEntry("Error: you need to enter values for Number, File, and Location")
   public static final String VALIDATE_CREATEWTDOCUMENT_ALERT = "389";

   @RBEntry("Name:")
   public static final String NAME_COLON = "390";

   @RBEntry("Baseline Configuration Specification")
   public static final String BASELINE_CONFIG_SPEC_LABEL = "391";

   @RBEntry("Configuration Specification")
   public static final String CONFIG_SPEC_LABEL = "392";

   @RBEntry("Latest Configuration Specification")
   public static final String LATEST_CONFIG_SPEC_LABEL = "393";

   @RBEntry("Effectivity Configuration Specification")
   public static final String EFFECTIVE_CONFIG_SPEC_LABEL = "394";

   @RBEntry("References Documents:")
   public static final String REFERENCES_DOCUMENTS = "395";

   @RBEntry("All Revisions")
   public static final String ALL_REVISIONS_URL_LABEL = "396";

   @RBEntry("Error getting enabled version string.")
   public static final String GET_ENABLED_VERSION = "401";

   @RBEntry("Error disabling prior versions or iterations.")
   public static final String DISABLE_ITER = "402";

   @RBEntry("Error getting the latest iteration of the latest version.")
   public static final String LATESTVER_ITER = "410";

   @RBEntry("Unable to change file name.")
   public static final String CHNG_FILENNAME_FAILED = "411";

   @RBEntry("Not able to get primary content.")
   public static final String GET_PRIM_CONTENT_FAILED = "412";

   @RBEntry("Attribute {0} is not available for this type.")
   @RBComment("The first argument should be an attribute name")
   public static final String ATTR_NOT_FOUND = "413";

   @RBEntry("Cannot copy to an existing object.")
   public static final String COPY_MERGE_NOT_ALLOWED = "414";

   @RBEntry("Copy delegate returned a NULL copy object: {0}")
   @RBComment("The class name of the copy delegate which returned a null copy.")
   public static final String COPY_DELEGATE_RETURNED_NULL_COPY = "415";

   @RBEntry("Error Message")
   public static final String ERROR_MESSAGE = "416";

   @RBEntry("Request Id")
   public static final String REQUEST_ID = "417";

   @RBEntry("Method Context Id")
   public static final String METHOD_CONTEXT_ID = "418";

   @RBEntry("Current Time")
   public static final String CURRENT_TIME = "419";

   @RBEntry("Object Identifier")
   public static final String OBJOID = "OBJOID";
}
