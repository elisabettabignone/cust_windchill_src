/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */

package wt.enterprise;

import java.util.ListResourceBundle;
import wt.util.InstalledProperties;

/**
 * enterpriseResource message resource bundle [English/US]
 *
 * Usage notes:
 * Last USED ID: 5
 * DO NOT REUSE IDS.  If an message becomes obsolete, comment it out
 * but do not create a new message with that id.
 *
 * To Remove all actions from the search page, use the NO_ACTIONS constant in
 *	the <OBJECTTYPE>_ACTION_LINKS_MIN paragraph, see example below.
 *
 * example:<br>
 * { "WTPART_ACTION_LINKS_MIN", NO_ACTIONS },

 *
 * <BR><BR><B>Supported API: </B>true
 * <BR><BR><B>Extendable: </B>false
 *
 **/
public class UrlLinkResource extends ListResourceBundle {

  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String WTPART_ACTIONS            = "0";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String NAME_SEPARATOR            = "1";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String PAIR_SEPARATOR            = "2";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String WTDOCUMENT_ACTIONS        = "3";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String WTCHANGEACTIVITY_ACTIONS  = "4";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String WTCHANGEORDER_ACTIONS     = "5";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String WTCHANGEREQUEST_ACTIONS   = "6";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String WTPARTMASTER_ACTIONS      = "7";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String WTDOCUMENTMASTER_ACTIONS  = "8";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String WTCHANGEISSUE_ACTIONS     = "9";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String EPMDOCUMENT_ACTIONS       = "10";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String REPORTTEMPLATE_ACTIONS    = "11";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String TEAM_ACTIONS              = "12";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String LIFECYCLETEMPLATE_ACTIONS = "13";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String GENERICPART_ACTIONS       = "14";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String VARIANTSPEC_ACTIONS       = "15";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String CONFIGURATIONITEM_ACTIONS = "16";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String WTPRODUCTINSTANCE_ACTIONS = "17";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String PMOPERATION_ACTIONS             = "18";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String PMCOMPOUNDOPERATION_ACTIONS     = "19";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String PMCOMPOUNDRESOURCE_ACTIONS      = "20";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String PMRESOURCEPLACEHOLDER_ACTIONS   = "21";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String PMTOOLINSTANCE_ACTIONS          = "22";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String PMTOOLPROTOTYPE_ACTIONS         = "23";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String PMMFGFEATURE_ACTIONS            = "24";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String PMCOMPOUNDASSEMBLY_ACTIONS      = "25";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String PMASSEMBLY_ACTIONS              = "26";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String PMSTUDY_ACTIONS                 = "27";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String PMINTERMEDIATEASMBLY_ACTIONS    = "28";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String PMPROCESS_ACTIONS               = "29";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String PMPROCESSRESOURCE_ACTIONS       = "30";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String WTPRODUCTCONFIGURATION_ACTIONS  = "31";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String WTPRODUCTINSTANCE2_ACTIONS      = "32";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String WTUNIT_ACTIONS                  = "33";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String TEAMTEMPLATE_ACTIONS            = "34";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String TEAMDISTRIBUTIONLIST_ACTIONS    = "35";
  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String WTSERIALNUMBEREDPART_ACTIONS    = "36";

   /**
   *
   * <BR><BR><B>Supported API: </B>false
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public final static String NO_ACTIONS 							  = "37";

  /**
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   */
   public Object[][] getContents() {
      return contents;
   }

  /**
   *
   * <BR><BR><B>Supported API: </B>false
   * <BR><BR><B>Extendable: </B>false
   *
   */
   static final Object[][] contents = {
      { NAME_SEPARATOR, ":" },
      { PAIR_SEPARATOR, "," },
      { NO_ACTIONS, "NOACTIONS" },


      /**
       *  <TRANSLATE>
       *
       *  Entries with _ACTIONS need localizing.  These values contain lists of pairs.  Each pair
       *  contains an action tag and the corresponding display name.  Pairs are separated by
       *  a comma (,).  The action tag and display name are separated by a colon (:).  The
       *  value following the colon is displayed and needs to be localized.  For example, in
       *  the value   "ObjProps:Properties,PartUses:Uses", "Properties" and "Uses" are displayed
       *  and will need localizing.
       **/
      { WTPART_ACTIONS, getValuesForWTPART_ACTIONS() },
      { WTSERIALNUMBEREDPART_ACTIONS, getValuesForWTSERIALNUMBEREDPART_ACTIONS() },
      { WTPRODUCTCONFIGURATION_ACTIONS,
                    "ObjProps:Properties,part_structure:Product Structure,ProductInstances:Product Instances" },
      { WTPRODUCTINSTANCE2_ACTIONS,
                    "ObjProps:Properties,part_structure:Product Structure,Allocate:Allocated By/ Allocates,AssociatedProcess:Related Process" },
      { WTDOCUMENT_ACTIONS, getValuesForWTDOCUMENT_ACTIONS() },
//                    "ObjProps:Properties,document_structure:Structure Hierarchy,DependencyView:References,DocUsesView:Structure Usage,Changes:Changes,Describes:Describes,AssociatedProcess:Related Process"  },
      { WTCHANGEACTIVITY_ACTIONS,
                    "ObjProps:Properties,Revises:Revises,Content:Contents,AssociatedProcess:Related Process"  },
      { WTCHANGEORDER_ACTIONS,
                     "ObjProps:Properties,Affects:Affects,BusinessCase:Business Case,Disposition:Disposition,Content:Contents,AssociatedProcess:Related Process" },
      { WTCHANGEREQUEST_ACTIONS,
                     "ObjProps:Properties,Identifies:Identifies,Disposition:Disposition,Content:Contents,AssociatedProcess:Related Process" },
      { WTPARTMASTER_ACTIONS,
                    "ObjProps:Properties" },
      { WTDOCUMENTMASTER_ACTIONS,
                    "ObjProps:Properties,ReferencedBy:Referenced By" },
      { GENERICPART_ACTIONS,
                    "ObjProps:Properties,part_structure:Product Structure,PartUses:Used By / Uses,PartReferences:References,GPVariants:Variants,GPVariantSpecs:VariantSpecs,GeneratorLauncher:Order Generator,AssociatedProcess:Related Process" },
      { VARIANTSPEC_ACTIONS,
                    "ObjProps:Properties,Changes:Changes,ReferencedBy:Referenced By,AssociatedProcess:Related Process" },
      { EPMDOCUMENT_ACTIONS, getValuesForEPMDOCUMENT_ACTIONS() },
//                    "ObjProps:Properties,EPMUses:Uses,EPMUsedBy:Used By,EPMDescribes:Describes,EPMReferences:References,EPMReferencedBy:Referenced By,EPMContains:Family" },
      { REPORTTEMPLATE_ACTIONS,
                    "ObjProps:Properties" },
      { TEAM_ACTIONS,
                    "ObjProps:Properties" },
      { TEAMTEMPLATE_ACTIONS,
                    "ObjProps:Properties" },
      { TEAMDISTRIBUTIONLIST_ACTIONS,
                    "ObjProps:Properties" },
      { LIFECYCLETEMPLATE_ACTIONS,
                    "ObjProps:Properties" },
      { CONFIGURATIONITEM_ACTIONS,
                    "ObjProps:Properties" },
      { WTPRODUCTINSTANCE_ACTIONS,
                    "ObjProps:Properties,part_structure:Product Structure" },
      { PMOPERATION_ACTIONS,
                    "ObjProps:Properties,OperationUsedBy:Used By,OperationAssembly:Manufacturing  Assembly,OperationResource:Resources,OperationMfg:Manufacturing Features,studies:Studies,pert:PERT View,references:References,ewiheader:EWI Header"},
      { PMCOMPOUNDOPERATION_ACTIONS,
                    "ObjProps:Properties,operation_structure:Process Structure,OperationUsedBy:Used By,OperationUses:Uses,OperationAssembly:Manufacturing  Assembly,OperationResource:Resources,OperationMfg:Manufacturing Features,studies:Studies,pert:PERT View,references:References,ewiheader:EWI Header"},
      { PMMFGFEATURE_ACTIONS,
                    "ObjProps:Properties,MfgParts:Parts,MfgAssembly:Manufacturing  Assembly,MfgOperations:Operations,MfgResources:Resources,studies:Studies,references:References"},
      { PMCOMPOUNDRESOURCE_ACTIONS,
                    "ObjProps:Properties,resource_structure:Resource Structure,ResourceUsedBy:Used By,ResourceUses:Uses,ResourceAssembly:Manufacturing  Assembly,ResourceOperations:Operations,studies:Studies,references:References"},
      { PMTOOLINSTANCE_ACTIONS,
                    "ObjProps:Properties,resource_structure:Resource Structure,ResourceUsedBy:Used By,ResourceUses:Uses,prototype:Tool Prototype,ResourceAssembly:Manufacturing  Assembly,ResourceOperations:Operations,ResourceMfg:Manufacturing Features,studies:Studies,references:References"},
      { PMRESOURCEPLACEHOLDER_ACTIONS,
                    "ObjProps:Properties,ResourceUsedBy:Used By,ResourceAssembly:Manufacturing  Assembly,ResourceOperations:Operations,studies:Studies,references:References"},
      { PMTOOLPROTOTYPE_ACTIONS,
                    "ObjProps:Properties,instances:Tool Instances,studies:Studies,references:References"},
      { PMCOMPOUNDASSEMBLY_ACTIONS,
                    "ObjProps:Properties,assembly_structure:Assembly Structure,usedby:UsedBy,uses:Uses,parts:Parts,assemblyOperations:Operations,assemblyResources:Resources,assemblyMfg:Manufacturing Features,studies:Studies,references:References"},
      { PMASSEMBLY_ACTIONS,
                    "ObjProps:Properties,usedby:UsedBy,parts:Parts,assemblyOperations:Operations,assemblyResources:Resources,assemblyMfg:Manufacturing Features,studies:Studies,references:References"},
      { PMSTUDY_ACTIONS,
                    "ObjProps:Properties,parts:Parts in study,operations:Operations in study,resources:Resources in study,mfgfeatures:Manufacturing Features in study,references:References"},
      { PMINTERMEDIATEASMBLY_ACTIONS,
                    "ObjProps:Properties,assembly_structure:Assembly Structure,parts:Parts,assemblyOperations:Operations,assemblyResources:Resources,assemblyMfg:Manufacturing Features,studies:Studies,references:References"},
      { PMPROCESS_ACTIONS,
                    "ObjProps:Properties,operation_structure:Process Structure,OperationUsedBy:Used By,OperationUses:Uses,OperationAssembly:Manufacturing  Assembly,OperationResource:Resources,OperationMfg:Manufacturing Features,studies:Studies,pert:PERT View,references:References,ewiheader:EWI Header"},
      { PMPROCESSRESOURCE_ACTIONS,
                    "ObjProps:Properties,resource_structure:Resource Structure,ResourceUsedBy:Used By,ResourceUses:Uses,ResourceAssembly:Manufacturing  Assembly,ResourceOperations:Operations,studies:Studies,references:References"},


      { "WTCHANGEACTIVITY_ACTION_LINKS",
                                       wt.lifecycle.LifeCycleHistoryActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.org.electronicIdentity.ElectronicSignaturesActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.lifecycle.SubmitActionDelegate.CONTEXT_SERVICE_NAME },

      { "WTCHANGEREQUEST_ACTION_LINKS",
                                       wt.lifecycle.LifeCycleHistoryActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.org.electronicIdentity.ElectronicSignaturesActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.lifecycle.SubmitActionDelegate.CONTEXT_SERVICE_NAME },

      { "WTCHANGEORDER_ACTION_LINKS",
                                       wt.lifecycle.LifeCycleHistoryActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.org.electronicIdentity.ElectronicSignaturesActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.lifecycle.SubmitActionDelegate.CONTEXT_SERVICE_NAME },
      { "REPORTTEMPLATE_ACTION_LINKS",
                                       wt.query.template.ReportTemplateExecuteActionDelegate.CONTEXT_SERVICE_NAME },
      { "CONFIGURATIONITEM_ACTION_LINKS",
                                       wt.lifecycle.LifeCycleHistoryActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.lifecycle.SubmitActionDelegate.CONTEXT_SERVICE_NAME },

      { "WTCHANGEACTIVITY_ACTION_LINKS_MIN",
                                       wt.lifecycle.LifeCycleHistoryActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.lifecycle.SubmitActionDelegate.CONTEXT_SERVICE_NAME },

      { "WTCHANGEACTIVITY2_ACTION_LINKS_MIN",
                                       wt.lifecycle.LifeCycleHistoryActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.lifecycle.SubmitActionDelegate.CONTEXT_SERVICE_NAME },

      { "WTCHANGEREQUEST_ACTION_LINKS_MIN",
                                       wt.lifecycle.LifeCycleHistoryActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.lifecycle.SubmitActionDelegate.CONTEXT_SERVICE_NAME },

      { "WTCHANGEREQUEST2_ACTION_LINKS_MIN",
                                       wt.lifecycle.LifeCycleHistoryActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.lifecycle.SubmitActionDelegate.CONTEXT_SERVICE_NAME },

      { "WTCHANGEORDER_ACTION_LINKS_MIN",
                                       wt.lifecycle.LifeCycleHistoryActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.lifecycle.SubmitActionDelegate.CONTEXT_SERVICE_NAME },

      { "WTCHANGEORDER2_ACTION_LINKS_MIN",
                                       wt.lifecycle.LifeCycleHistoryActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.lifecycle.SubmitActionDelegate.CONTEXT_SERVICE_NAME },

      { "MANAGEDBASELINE_ACTION_LINKS_MIN",
                                       wt.lifecycle.LifeCycleHistoryActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.lifecycle.SubmitActionDelegate.CONTEXT_SERVICE_NAME },

      { "REPORTTEMPLATE_ACTION_LINKS_MIN",
                                       wt.query.template.ReportTemplateExecuteActionDelegate.CONTEXT_SERVICE_NAME },

      { "CONFIGURATIONITEM_ACTION_LINKS_MIN",
                                       wt.lifecycle.LifeCycleHistoryActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.lifecycle.SubmitActionDelegate.CONTEXT_SERVICE_NAME },

      { "WTPRODUCTCONFIGURATION_ACTION_LINKS_MIN",
                                       wt.lifecycle.LifeCycleHistoryActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.org.electronicIdentity.ElectronicSignaturesActionDelegate.CONTEXT_SERVICE_NAME + "," +
                                       wt.lifecycle.SubmitActionDelegate.CONTEXT_SERVICE_NAME },

 /*<NEW>*/{ WTUNIT_ACTIONS, "ObjProps:Properties" },
   };

   private static String getValuesForWTPART_ACTIONS() {
      if (InstalledProperties.isInstalled("WMF.WindchillProcessPlanner") && InstalledProperties.isInstalled("Windchill.EnterpriseSystemsIntegration"))
          return "ObjProps:Properties,part_structure:Product Structure,PartUses:Used By / Uses,PartReferences:References,BuildRuleBuildSource:Described By,PartReplacements:Replacements,PartAssembly:Manufacturing Assembly,AssociatedProcess:Related Process,Configurations:Configurations,ProductInstances:Product Instances,DistributionList:Distribution Targets";
      else if (InstalledProperties.isInstalled("WMF.WindchillProcessPlanner"))
          return "ObjProps:Properties,part_structure:Product Structure,PartUses:Used By / Uses,PartReferences:References,BuildRuleBuildSource:Described By,PartReplacements:Replacements,PartAssembly:Manufacturing Assembly,AssociatedProcess:Related Process,Configurations:Configurations,ProductInstances:Product Instances";
      else if (InstalledProperties.isInstalled("Windchill.EnterpriseSystemsIntegration"))
          return "ObjProps:Properties,part_structure:Product Structure,PartUses:Used By / Uses,PartReferences:References,BuildRuleBuildSource:Described By,PartReplacements:Replacements,AssociatedProcess:Related Process,Configurations:Configurations,ProductInstances:Product Instances,DistributionList:Distribution Targets";
      else
          return "ObjProps:Properties,part_structure:Product Structure,PartUses:Used By / Uses,PartReferences:References,BuildRuleBuildSource:Described By,PartReplacements:Replacements,AssociatedProcess:Related Process,Configurations:Configurations,ProductInstances:Product Instances";
   }

   private static String getValuesForWTSERIALNUMBEREDPART_ACTIONS() {
     if (InstalledProperties.isInstalled("Windchill.EnterpriseSystemsIntegration"))
            return "ObjProps:Properties,part_structure:Product Structure,PartUses:Used By / Uses,PartReferences:References,BuildRuleBuildSource:Described By,PartReplacements:Replacements,AssociatedProcess:Related Process,DistributionList:Distribution Targets";
     else
            return "ObjProps:Properties,part_structure:Product Structure,PartUses:Used By / Uses,PartReferences:References,BuildRuleBuildSource:Described By,PartReplacements:Replacements,AssociatedProcess:Related Process";
   }

   private static String getValuesForWTDOCUMENT_ACTIONS() {
     return "ObjProps:Properties,document_structure:Structure Hierarchy,DependencyView:References,DocUsesView:Structure Usage,Changes:Changes,Describes:Describes,AssociatedProcess:Related Process";
   }

   private static String getValuesForEPMDOCUMENT_ACTIONS() {
     return "ObjProps:Properties,EPMUses:Uses,EPMUsedBy:Used By,EPMDescribes:Describes,EPMReferences:References,EPMReferencedBy:Referenced By,EPMContains:Family";
   }
}
