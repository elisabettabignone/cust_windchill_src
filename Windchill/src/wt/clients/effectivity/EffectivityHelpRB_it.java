/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.effectivity;

import wt.util.resource.*;

@RBUUID("wt.clients.effectivity.EffectivityHelpRB")
public final class EffectivityHelpRB_it extends WTListResourceBundle {
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_0 = "Contents/effectivity";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_1 = "Help/effectivity";

   /**
    * ------Associations Panel-----
    **/
   @RBEntry("CICreate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/effectivity/ConfigItemFrame";

   @RBEntry("CIUpdate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_3 = "Help/effectivity/UpdateConfigItemFrame";

   @RBEntry("CIView")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_4 = "Help/effectivity/ViewConfigItemFrame";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_5 = "Desc/effectivity/ConfigItemFrame";

   @RBEntry("Fare clic per salvare il configuration item e chiudere la finestra")
   public static final String PRIVATE_CONSTANT_6 = "Desc/effectivity/ConfigItemFrame/Ok";

   @RBEntry("Fare clic per chiudere la finestra senza salvare le modifiche")
   public static final String PRIVATE_CONSTANT_7 = "Desc/effectivity/ConfigItemFrame/Close";

   @RBEntry("Fare clic per visualizzare le informazioni della Guida relative ai configuration item")
   public static final String PRIVATE_CONSTANT_8 = "Desc/effectivity/ConfigItemFrame/Help";
}
