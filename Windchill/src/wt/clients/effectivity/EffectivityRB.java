/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.effectivity;

import wt.util.resource.*;

@RBUUID("wt.clients.effectivity.EffectivityRB")
public final class EffectivityRB extends WTListResourceBundle {
   @RBEntry("OK")
   public static final String OK = "0";

   @RBEntry("Close")
   public static final String CLOSE = "1";

   @RBEntry("Help")
   public static final String HELP = "2";

   @RBEntry("Create Configuration Item")
   public static final String CREATE_CONFIG_ITEM = "3";

   @RBEntry("Update Configuration Item")
   public static final String UPDATE_CONFIG_ITEM = "4";

   @RBEntry("View Configuration Item")
   public static final String VIEW_CONFIG_ITEM = "5";

   @RBEntry("The selected object is not a Configuration Item")
   public static final String OBJECT_NOT_CONFIG_ITEM = "6";

   @RBEntry("Product Solution:")
   public static final String PRODUCT_SOLUTION_LABEL = "7";

   @RBEntry("Search...")
   public static final String SEARCH_BUTTON = "8";

   @RBEntry("Search for a Product Solution")
   public static final String SEARCH_DIALOG_LABEL = "9";
}
