/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.windchill;

import wt.util.resource.*;

@RBUUID("wt.clients.windchill.WindchillRB")
public final class WindchillRB extends WTListResourceBundle {
   @RBEntry("About Windchill")
   public static final String PRIVATE_CONSTANT_0 = "aboutWindchill";

   @RBEntry("Parametric Technology Corporation, 140 Kendrick Street, Needham, MA 02494 USA")
   public static final String PRIVATE_CONSTANT_1 = "address";

   @RBEntry("010505")
   public static final String PRIVATE_CONSTANT_2 = "endPageDate";

   @RBEntry("This product includes software developed by the Apache Software Foundation (http://www.apache.org/).")
   public static final String PRIVATE_CONSTANT_3 = "apache";

   @RBEntry("Close")
   public static final String PRIVATE_CONSTANT_4 = "close";

   @RBEntry("Copyright &copy 2001 Parametric Technology Corporation. All rights reserved.")
   public static final String PRIVATE_CONSTANT_5 = "copyright";

   @RBEntry("Datecode")
   public static final String PRIVATE_CONSTANT_6 = "dateCode";

   @RBEntry("DSU history")
   public static final String PRIVATE_CONSTANT_7 = "dsuhistory";

   @RBEntry("ClusterProven, Advanced ClusterProven and the ClusterProven design are trademarks or registered trademarks of International Business Machines Corporation in the United States and other countries and are used under license.  IBM Corporation does not warrant and is not responsible for the operation of this software product.")
   public static final String PRIVATE_CONSTANT_8 = "ibm";

   @RBEntry("UNITED STATES GOVERNMENT RESTRICTED RIGHTS LEGEND")
   public static final String PRIVATE_CONSTANT_9 = "legend";

   @RBEntry("This software is provided under written license agreement, contains valuable trade secrets and proprietary information, and is protected by the copyright laws of the United States and other countries. It may not be copied or distributed in any form or medium, disclosed to third parties, or used in any manner not provided for in the software licenses agreement except with written prior approval from Parametric Technology Corporation (PTC).")
   public static final String PRIVATE_CONSTANT_10 = "license";

   @RBEntry("DSU history unavailable")
   public static final String PRIVATE_CONSTANT_11 = "nodsuhistory";

   @RBEntry("Oracle 8i run-time, Copyright (c) 2000 Oracle Corporation")
   public static final String PRIVATE_CONSTANT_12 = "oracle";

   @RBEntry("UNAUTHORIZED USE OF SOFTWARE OR ITS DOCUMENTATION CAN RESULT IN CIVIL DAMAGES AND CRIMINAL PROSECUTION.")
   public static final String PRIVATE_CONSTANT_13 = "prosecution";

   @RBEntry("Contains Rational Rose 2000 TM Copyright &copy 1999 Rational Software Corporation ")
   public static final String PRIVATE_CONSTANT_14 = "rational";

   @RBEntry("Release")
   public static final String PRIVATE_CONSTANT_15 = "release";

   @RBEntry("This document and the software described herein are Commercial Computer Documentation and Software, pursuant to FAR 12.212(a)-(b) (OCT'95) or DFARS 227.7202-1(a) and 227.7202-3(a) (JUN'95), and are provided to the US Government under a limited commercial license only. For procurements predating the above clauses, the use, duplication, or disclosure by the Government is subject to the restrictions set forth in subparagraph (c)(1)(ii) of the Rights in Technical Data and Computer Software Clause at DFARS 252.227-7013 (OCT'88) or Commercial Computer Software-Restricted Rights at FAR 52.227-19(c)(1)-(2) (JUN'87), as applicable.")
   public static final String PRIVATE_CONSTANT_16 = "rights";

   @RBEntry("Contains Convera RetrievalWare, Copyright &copy Convera Corporation")
   public static final String PRIVATE_CONSTANT_17 = "searchEngine";

   @RBEntry("Contains WebGain Visual Cafe TM Copyright &copy 1998 WebGain, Inc.")
   public static final String PRIVATE_CONSTANT_18 = "symantec";

   @RBEntry("Contains the Java (TM) Telnet Applet (StatusPeer.java, TelnetIO.java, TelnetWrapper.java, TimedOutException.java), copyright 1996, 97 Mattias L. Jugel, Marcus Mei&#223ner, which is redistributed under the GNU General Public License http://www.gnu.org/copyleft/gpl.html.  This license is from the original copyright holder and the Applet is provided WITHOUT WARRANTY of any kind.   You may obtain a copy of the source code for the Applet at http://www.mud.de/se/jta/ [for a charge of no more than the cost of physically performing the source distribution, by sending an e-mail to ________].  The source code is likewise provided under the GNU General Public License.")
   public static final String PRIVATE_CONSTANT_19 = "telnet";

   @RBEntry("The Java Getopt.jar, copyright 1987-1997 Free Software Foundation, Inc.;  Java Port copyright 1998 by Aaron M. Renn (arenn@urbanophile.com),  is redistributed under the GNU Lesser Public License.  You may obtain a copy of the source code at   http://www.urbanophile.com/arenn/hacking/download.html.   The source code is likewise provided under the GNU Lesser Public License.")
   public static final String PRIVATE_CONSTANT_20 = "gnugetopt";

   @RBEntry("PTC, The Product Development Company, Pro/ENGINEER, Windchill, and all PTC product names and logos are trademarks or registered trademarks of Parametric Technology Corporation or its subsidiaries in the United States and in other countries. This notice is intended as a precaution against inadvertent publication and does not imply publication or any waiver of confidentiality.  The year included in this notice is the year of creation of the work.  Java and all Java based marks are trademarks or registered trademarks of Sun Microsystems, Inc., Oracle is a registered trademark of Oracle Corporation,  and Windows and Windows NT are registered trademarks of Microsoft Corporation.  Other company and product names are trademarks or registered trademarks of their respective holders.")
   public static final String PRIVATE_CONSTANT_21 = "trademark";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_22 = "xml";

   @RBEntry("Copyright &copy 1998 Netscape Communications Corporation. All Rights Reserved.")
   public static final String PRIVATE_CONSTANT_23 = "netscape";

   @RBEntry("User and training documentation from Parametric Technology Corporation (PTC) is subject to the copyright laws of the United States and other countries and is provided under a license agreement that restricts copying, disclosure, and use of such documentation. PTC hereby grants to the licensed user the right to make copies in printed form of this documentation if provided on software media, but only for internal/personal use and in accordance with the license agreement under which the applicable software is licensed. Any copy made shall include the PTC copyright notice and any other proprietary notice provided by PTC. This documentation may not be disclosed, transferred, modified, or reduced to any form, including electronic media, or transmitted or made publicly available by any means without the prior written consent of PTC and no authorization is granted to make copies for such purposes.")
   public static final String PRIVATE_CONSTANT_24 = "docs";

   @RBEntry("Information described herein is furnished for general information only, is subject to change without notice, and should not be construed as a warranty or commitment by PTC. PTC assumes no responsibility or liability for any errors or inaccuracies that may appear in this document.")
   public static final String PRIVATE_CONSTANT_25 = "docs2";

   @RBEntry("Registered Trademarks of Parametric Technology Corporation or a Subsidiary")
   public static final String PRIVATE_CONSTANT_26 = "rtmTitle";

   @RBEntry("Advanced Surface Design, Behavioral Modeling, CADDS, Computervision, CounterPart, Create &middot Collaborate &middot Control, EPD, EPD.Connect, Expert Machinist, Flexible Engineering, GRANITE, HARNESSDESIGN, Info*Engine, InPart, MECHANICA, Optegra, Parametric Technology, Parametric Technology Corporation, PartSpeak, PHOTORENDER, Pro/DESKTOP, Pro/E, Pro/ENGINEER, Pro/HELP, Pro/INTRALINK, Pro/MECHANICA, Pro/TOOLKIT, Product First, Product Development Means Business, Product Makes the Company, PTC, the PTC logo, PT/Products, Shaping Innovation, The Way to Product First, and Windchill.")
   public static final String PRIVATE_CONSTANT_27 = "rtm";

   @RBEntry("Trademarks of Parametric Technology Corporation or a Subsidiary")
   public static final String PRIVATE_CONSTANT_28 = "tmTitle";

   @RBEntry("3DPAINT, Associative Topology Bus, AutobuildZ, CDRS, CV, CVact, CVaec, CVdesign, CV-DORS, CVMAC, CVNC, CVToolmaker, EDAcompare, EDAconduit, DataDoctor, DesignSuite, DIMENSION III, Distributed Services Manager, DIVISION, e/ENGINEER, eNC Explorer, Expert Framework, Expert MoldBase, Expert Toolmaker, Harmony, InterComm, InterComm Expert, InterComm EDAcompare, InterComm EDAconduit, ISSM, KDiP, Knowledge Discipline in Practice, Knowledge System Driver, ModelCHECK, MoldShop, NC Builder, Pro/ANIMATE, Pro/ASSEMBLY, Pro/CABLING, Pro/CASTING, Pro/CDT, Pro/CMM, Pro/COLLABORATE, Pro/COMPOSITE, Pro/CONCEPT, Pro/CONVERT, Pro/DATA for PDGS, Pro/DESIGNER, Pro/DETAIL, Pro/DIAGRAM, Pro/DIEFACE, Pro/DRAW, Pro/ECAD, Pro/ENGINE, Pro/FEATURE, Pro/FEM-POST, Pro/FICIENCY, Pro/FLY-THROUGH, Pro/HARNESS, Pro/INTERFACE, Pro/LANGUAGE, Pro/LEGACY, Pro/LIBRARYACCESS, Pro/MESH, Pro/Model.View, Pro/MOLDESIGN, Pro/NC-ADVANCED, Pro/NC-CHECK, Pro/NCMILL, Pro/NCPOST, Pro/NC-SHEETMETAL, Pro/NC-TURN, Pro/NC-WEDM, Pro/NC-Wire EDM, Pro/NETWORK ANIMATOR, Pro/NOTEBOOK, Pro/PDM, Pro/PHOTORENDER, Pro/PIPING, Pro/PLASTIC ADVISOR, Pro/PLOT, Pro/POWER DESIGN, Pro/PROCESS, Pro/REPORT, Pro/REVIEW, Pro/SCAN-TOOLS, Pro/SHEETMETAL, Pro/SURFACE, Pro/VERIFY, Pro/Web.Link, Pro/Web.Publish, Pro/WELDING, ProductView, PTC Precision, Routed Systems Designer Shrinkwrap, Simple &middot Powerful &middot Connected, The Product Development Company, Wildfire, Windchill DynamicDesignLink, Windchill PartsLink, Windchill PDMLink, Windchill ProjectLink, and Windchill SupplyLink.")
   public static final String PRIVATE_CONSTANT_29 = "tm";

   @RBEntry("Patents of Parametric Technology Corporation or a Subsidiary")
   public static final String PRIVATE_CONSTANT_30 = "patentTitle";

   @RBEntry("Additionally, equivalent patents may be issued or pending outside of the United States. Contact PTC for further information.")
   public static final String PRIVATE_CONSTANT_31 = "patentDesc";

   @RBEntry("Registration No.")
   public static final String PRIVATE_CONSTANT_32 = "patentRegNoTitle";

   @RBEntry("Issue Date")
   public static final String PRIVATE_CONSTANT_33 = "patentIssueDateTitle";

   @RBEntry("Third-Party Trademarks")
   public static final String PRIVATE_CONSTANT_34 = "3rdPartyTmTitle";

   @RBEntry("Adobe, Acrobat, Distiller and the Acrobat Logo are trademarks of Adobe Systems Incorporated. Advanced ClusterProven, ClusterProven, and the ClusterProven design are trademarks or registered trademarks of International Business Machines Corporation in the United States and other countries and are used under license. IBM Corporation does not warrant and is not responsible for the operation of this software product. AIX is a registered trademark of IBM Corporation. Allegro, Cadence, and Concept are registered trademarks of Cadence Design Systems, Inc. Apple, Mac, Mac OS, and Panther are trademarks or registered trademarks of Apple Computer, Inc. AutoCAD and Autodesk Inventor are registered trademarks of Autodesk, Inc. Baan is a registered trademark of Baan Company. CADAM and CATIA are registered trademarks of Dassault Systemes. COACH is a trademark of CADTRAIN, Inc. DOORS is a registered trademark of Telelogic AB. FLEX<i>lm</i> is a trademark of Macrovision Corporation. Geomagic is a registered trademark of Raindrop Geomagic, Inc. EVERSYNC, GROOVE, GROOVEFEST, GROOVE.NET, GROOVE NETWORKS, iGROOVE, PEERWARE, and the interlocking circles logo are trademarks of Groove Networks, Inc. Helix is a trademark of Microcadam, Inc. HOOPS is a trademark of Tech Soft America, Inc. HP-UX is a registered trademark Hewlett-Packard Company. I-DEAS, Metaphase, Parasolid, SHERPA, Solid Edge, and Unigraphics are trademarks or registered trademarks of UGS Corp. InstallShield is a registered trademark and service mark of InstallShield Software Corporation in the United States and/or other countries. Intel is a registered trademark of Intel Corporation. IRIX is a registered trademark of Silicon Graphics, Inc. LINUX is a registered trademark of Linus Torvalds, MainWin and Mainsoft are trademarks of Mainsoft Corporation. MatrixOne is a trademark of MatrixOne, Inc. Mentor Graphics and Board Station are registered trademarks and 3D Design, AMPLE, and Design Manager are trademarks of Mentor Graphics Corporation. MEDUSA and STHENO are trademarks of CAD Schroer GmbH. Microsoft, Microsoft Project, Windows, the Windows logo, Windows NT, Visual Basic, and the Visual Basic logo are registered trademarks of Microsoft Corporation in the United States and/or other countries. Netscape and the Netscape N and Ship's Wheel logos are registered trademarks of Netscape Communications Corporation in the U.S. and other countries. Oracle is a registered trademark of Oracle Corporation. OrbixWeb is a registered trademark of IONA Technologies PLC. PDGS is a registered trademark of Ford Motor Company. RAND is a trademark of RAND Worldwide. Rational Rose is a registered trademark of Rational Software Corporation. RetrievalWare is a registered trademark of Convera Corporation. RosettaNet is a trademark and Partner Interface Process and PIP are registered trademarks of RosettaNet, a nonprofit organization. SAP and R/3 are registered trademarks of SAP AG Germany. SolidWorks is a registered trademark of SolidWorks Corporation. All SPARC trademarks are used under license and are trademarks or registered trademarks of SPARC International, Inc. in the United States and in other countries. Products bearing SPARC trademarks are based upon an architecture developed by Sun Microsystems, Inc. Sun, Sun Microsystems, the Sun logo, Solaris, UltraSPARC, Java and all Java based marks, and \"The Network is the Computer\" are trademarks or registered trademarks of Sun Microsystems, Inc. in the United States and in other countries. TIBCO, TIBCO Software, TIBCO ActiveEnterprise, TIBCO Designer, TIBCO Enterprise for JMS, TIBCO Rendezvous, TIBCO Turbo XML, TIBCO BusinessWorks are the trademarks or registered trademarks of TIBCO Software Inc. in the United States and other countries. WebEx is a trademark of WebEx Communications, Inc. ")
   public static final String PRIVATE_CONSTANT_35 = "3rdPartyTm";

   @RBEntry("Third-Party Technology Information")
   public static final String PRIVATE_CONSTANT_36 = "3rdPartyInfoTitle";

   @RBEntry("Certain PTC software products contain licensed third-party technology: Rational Rose 2000E is copyrighted software of Rational Software Corporation. RetrievalWare is copyrighted software of Convera Corporation. VisTools library is copyrighted software of Visual Kinematics, Inc. (VKI) containing confidential trade secret information belonging to VKI. HOOPS graphics system is a proprietary software product of, and is copyrighted by, Tech Soft America, Inc. G-POST is copyrighted software and a registered trademark of Intercim. VERICUT is copyrighted software and a registered trademark of CGTech. Pro/PLASTIC ADVISOR is powered by Moldflow technology. Moldflow is a registered trademark of Moldflow Corporation. <br>MainWin Dedicated Libraries are copyrighted software of Mainsoft Corporation. Certain software provided by TIBCO Software Inc. The JPEG image output in the Pro/Web.Publish module is based in part on the work of the independent JPEG Group. DFORMD.DLL is copyrighted software from Compaq Computer Corporation and may not be distributed. METIS, developed by George Karypis and Vipin Kumar at the University of Minnesota, can be researched at <A HREF='javascript:var a=window.open(\"http://www.cs.umn.edu/~karypis/metis\")'>http://www.cs.umn.edu/~karypis/metis</A> METIS is &copy 1997 Regents of the University of Minnesota. LightWork Libraries are copyrighted by LightWork Design 1990-2001. Visual Basic for Applications and Internet Explorer is copyrighted software of Microsoft Corporation. <br>Parasolid &copy UGS Corp. Windchill Info*Engine Server contains IBM XML Parser for Java Edition and the IBM Lotus XSL Edition. Pop-up calendar components Copyright &copy 1998 Netscape Communications Corporation. All Rights Reserved. TECHNOMATIX is copyrighted software and contains proprietary information of Technomatix Technologies Ltd. TIBCO ActiveEnterprise, TIBCO Designer, TIBCO Enterprise for JMS, TIBCO Rendezvous, TIBCO Turbo XML, <br>TIBCO BusinessWorks are provided by TIBCO Software Inc. Technology \"Powered by Groove\" is provided by Groove Networks, Inc. Technology \"Powered by WebEx\" is provided by WebEx Communications, Inc. Oracle 8i run-time and Oracle 9i run-time, Copyright 2002-2003 Oracle Corporation. Oracle programs provided herein are subject to a restricted use license and can only be used in conjunction with the PTC software they are provided with. Apache Server, Tomcat, Xalan, Xerces and Jakarta are technologies developed by, and are copyrighted software of, the Apache Software Foundation <A HREF='javascript:var a=window.open(\"http://www.apache.org/\")'>http://www.apache.org</A> - their use is subject to the terms and limitations of the Apache License at: <A HREF='javascript:var a=window.open(\"http://www.apache.org\")'>http://www.apache.org</A> Adobe Acrobat Reader and Adobe Distiller are copyrighted software of Adobe Systems Inc. and is subject to the Adobe End-User License Agreement as provided by Adobe with those products.UnZip (&copy 1990-2001 Info-ZIP, All Rights Reserved) is provided \"AS IS\" and WITHOUT WARRANTY OF ANY KIND. For the complete Info ZIP license see <A HREF='javascript:var a=window.open(\"ftp://ftp.info-zip.org/pub/infozip/license.html\")'>ftp://ftp.info-zip.org/pub/infozip/license.html</A> The Java (TM) Telnet Applet (StatusPeer.java, TelnetIO.java, TelnetWrapper.java, timedOutException.java), Copyright &copy 1996, 97 Mattias L. Jugel, Marcus Mei&#223ner, is redistributed under the GNU General Public License. This license is from the original copyright holder and the Applet is provided WITHOUT WARRANTY OF ANY KIND. You may obtain a copy of the source code for the Applet at <A HREF='javascript:var a=window.open(\"http://www.mud.de/se/jta\")'>http://www.mud.de/se/jta</A> (for a charge of no more than the cost of physically performing the source distribution), by sending e-mail to leo@mud.de or marcus@mud.de-you are allowed to choose either distribution method. The source code is likewise provided under the GNU General Public License. GTK+ - The GIMP Toolkit are licensed under the GNU Library General Public License (LGPL). You may obtain a copy of the source code at <A HREF='javascript:var a=window.open(\"http://www.gtk.org\")'>http://www.gtk.org</A> which is likewise provided under the GNU LGPL. zlib software Copyright &copy 1995-2002 Jean-loup Gailly and Mark Adler. May include cryptographic software written by Eric Young (eay@cryptsoft.com). OmniORB is distributed under the terms and conditions of the GNU. The Java Getopt.jar, copyright 1987-1997 Free Software Foundation, Inc.; Java Port copyright 1998 by Aaron M. Renn (arenn@urbanophile.com), is redistributed under the GNU LGPL. You may obtain a copy of the source code at: <A HREF='javascript:var a=window.open(\"http://www.urbanophile.com/arenn/hacking/download.html\")'>http://www.urbanophile.com/arenn/hacking/download.html</A> The source code is likewise provided under the GNU LGPL. This product may include software developed by the OpenSSL Project for use in the OpenSSL Toolkit. <A HREF='javascript:var a=window.open(\"http://www.openssl.org/\")'>http://www.openssl.org</A>: Copyright (c) <br>1998-2003 The OpenSSL Project. All rights reserved. This product may include cryptographic software written by Eric Young (eay@cryptsoft.com). Gecko and Mozilla components are subject to the Mozilla Public License Version 1.1 at <A HREF='javascript:var a=window.open(\"http://www.mozilla.org/MPL/\")'>http://www.mozilla.org/MPL</A> Software distributed under the Mozilla Public License (MPL) is distributed on an \"AS IS\" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the MPL for the specific language governing rights and limitations. Mozilla Japanese localization components are subject to the Netscape Public License Version 1.1 <A HREF='javascript:var a=window.open(\"http://www.mozilla.org/NPL/\")'>http://www.mozilla.org/NPL</A>. Software distributed under Netscape Public License (NPL) is distributed on an \"AS IS\" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied (see the NPL for rights and limitations that are governing different languages). The Original Code is Mozilla Communicator client code, released March 31, 1998 and the Initial Developer of the Original Code is Netscape Communications Corporation. Portions created by Netscape are Copyright (c) 1998 Netscape Communications Corporation. All Rights Reserved. Contributor(s): Kazu Yamamoto (kazu@mozilla.gr.jp); Ryoichi Furukawa (furu@mozilla.gr.jp); Tsukasa Maruyama (mal@mozilla.gr.jp); Teiji Matsuba (matsuba@dream.com). iCal4j Copyright &copy; 2005, Ben Fortuna.")
   public static final String PRIVATE_CONSTANT_37 = "3rdPartyInfo";

   @RBEntry("Installed Assemblies")
   public static final String PRIVATE_CONSTANT_38 = "installedAssemblies";

   @RBEntry("SOFTWARE ABOUT")
   public static final String PRIVATE_CONSTANT_39 = "softwareAbout";
}
