/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.windchill;

import wt.util.resource.*;

@RBUUID("wt.clients.windchill.WindchillRB")
public final class WindchillRB_it extends WTListResourceBundle {
   @RBEntry("Informazioni su Windchill")
   public static final String PRIVATE_CONSTANT_0 = "aboutWindchill";

   @RBEntry("Parametric Technology Corporation, 140 Kendrick Street, Needham, MA 02494 USA")
   public static final String PRIVATE_CONSTANT_1 = "address";

   @RBEntry("010505")
   public static final String PRIVATE_CONSTANT_2 = "endPageDate";

   @RBEntry("Questo prodotto comprende software sviluppato da  Apache Software Foundation (http://www.apache.org/).")
   public static final String PRIVATE_CONSTANT_3 = "apache";

   @RBEntry("Chiudi")
   public static final String PRIVATE_CONSTANT_4 = "close";

   @RBEntry("Copyright &copy 2001 Parametric Technology Corporation. Tutti i diritti riservati.")
   public static final String PRIVATE_CONSTANT_5 = "copyright";

   @RBEntry("Codice data")
   public static final String PRIVATE_CONSTANT_6 = "dateCode";

   @RBEntry("Cronologia DSU")
   public static final String PRIVATE_CONSTANT_7 = "dsuhistory";

   @RBEntry("ClusterProven, Advanced ClusterProven e il design ClusterProven sono marchi o marchi registrati della International Business Machines Corporation negli Stati Uniti e in altri paesi e sono usati dietro licenza. IBM Corporation non garantisce e non è responsabile del funzionamento di questo software.")
   public static final String PRIVATE_CONSTANT_8 = "ibm";

   @RBEntry("CLAUSOLA DI LIMITAZIONE DEI DIRITTI PER IL GOVERNO STATUNITENSE")
   public static final String PRIVATE_CONSTANT_9 = "legend";

   @RBEntry("Questo software è stato fornito dietro contratto di licenza software che ne disciplina l'uso. Il software contiene segreti commerciali e informazioni esclusive della Parametric Technology Corporation (PTC) ed è protetto dalle leggi statunitensi e internazionali sul copyright. Non può essere copiato, distribuito in alcuna forma o con alcun mezzo, divulgato a terze parti o utilizzato in alcun modo non contemplato nel contratto di licenza senza il previo consenso scritto di Parametric Technology Corporation PTC.")
   public static final String PRIVATE_CONSTANT_10 = "license";

   @RBEntry("Cronologia DSU non disponibile")
   public static final String PRIVATE_CONSTANT_11 = "nodsuhistory";

   @RBEntry("Oracle 8i run-time, Copyright (c) 2000 Oracle Corporation")
   public static final String PRIVATE_CONSTANT_12 = "oracle";

   @RBEntry("L'USO NON AUTORIZZATO DEL SOFTWARE O DELLA RELATIVA DOCUMENTAZIONE È PERSEGUIBILE A NORMA DI LEGGE.")
   public static final String PRIVATE_CONSTANT_13 = "prosecution";

   @RBEntry("Contiene Rational Rose 2000 TM Copyright &copy 1999 Rational Software Corporation")
   public static final String PRIVATE_CONSTANT_14 = "rational";

   @RBEntry("Release")
   public static final String PRIVATE_CONSTANT_15 = "release";

   @RBEntry("Questo software commerciale per computer e la relativa documentazione sono forniti al governo statunitense solamente come licenza commerciale limitata, come prescritto dalle normative FAR 12.212(a)-(b) (OTT. '95) o DFARS 227.7202-1(a) e 227.7202-3(a) (GIU. '95). Nel caso di acquisti avvenuti precedentemente alle clausole sopra menzionate, l'uso, la duplicazione o la divulgazione da parte del Governo sono soggetti alle limitazioni descritte nei sottoparagrafi (c)(1)(ii) della Clausola \"Rights in Technical Data and Computer Software\" DFARS 252.227-7013 (OTT. '88) o \"Commercial Computer Software-Restricted Rights\" FAR 52.227-19(c)(1)-(2) (GIU. '87), secondo quanto pertinente.")
   public static final String PRIVATE_CONSTANT_16 = "rights";

   @RBEntry("Contiene Convera RetrievalWare, Copyright &copy Convera Corporation")
   public static final String PRIVATE_CONSTANT_17 = "searchEngine";

   @RBEntry("Contiene WebGain Visual Cafe TM Copyright &copy 1998 WebGain, Inc.")
   public static final String PRIVATE_CONSTANT_18 = "symantec";

   @RBEntry("Contiene Telnet Applet Java (TM) (StatusPeer.java, TelnetIO.java, TelnetWrapper.java, TimedOutException.java), copyright 1996, 97 Mattias L. Jugel, Marcus Mei&#223ner, ridistribuito su licenza GNU General Public License (http://www.gnu.org/copyleft/gpl.html). Tale licenza viene rilasciata dal titolare originario del copyright e l'Applet viene fornito SENZA GARANZIA di alcun tipo. È possibile ottenere una copia del codice sorgente dell'Applet all'indirizzo http://www.mud.de/se/jta/ [ad un prezzo non superiore al costo dell'esecuzione fisica della sua distribuzione, inviando un'e-mail a  ________].  Anche il codice sorgente è fornito su licenza GNU General Public License.")
   public static final String PRIVATE_CONSTANT_19 = "telnet";

   @RBEntry("Java Getopt.jar, Copyright 1987-1997 Free Software Foundation, Inc.; Java Port Copyright 1998 Aaron M. Renn (arenn@urbanophile.com) &egrave; ridistribuito ai sensi di GNU LGPL. Per richiedere una copia del codice sorgente, visitare: http://www.urbanophile.com/arenn/hacking/download.html. Anche il codice sorgente viene fornito su licenza GNU LGPL.")
   public static final String PRIVATE_CONSTANT_20 = "gnugetopt";

   @RBEntry("PTC, The Product Development Company, Pro/ENGINEER, Windchill, e tutti i nomi e i logo dei prodotti PTC sono marchi o marchi registrati della Parametric Technology Corporation o delle società controllate negli Stati Uniti e in altri paesi. Questo avviso costituisce una precauzione in caso di pubblicazioni accidentali. L'anno indicato è quello della creazione del prodotto. Java e tutti i marchi collegati a Java sono marchi o marchi commerciali della Sun MicroSystems, Inc., Oracle è un marchio registrato della Oracle Corporation e Windows e Windows NT sono marchi registrati della Microsoft Corporation. I nomi di prodotti e di altre società sono marchi o marchi registrati dei rispettivi titolari.")
   public static final String PRIVATE_CONSTANT_21 = "trademark";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_22 = "xml";

   @RBEntry("Copyright &copy 1998 Netscape Communications Corporation. Tutti i diritti riservati.")
   public static final String PRIVATE_CONSTANT_23 = "netscape";

   @RBEntry("La documentazione per l'utente e di formazione fornita da Parametric Technology Corporation (PTC) è soggetta alle leggi sul copyright degli Stati Uniti e di altri paesi e viene fornita in base a un contratto di licenza che ne limita la copia, la divulgazione e l'uso. PTC conferisce all'utente con licenza il diritto di effettuare copie stampate della documentazione, se fornita su supporti software, ma solo per uso interno e personale ed in conformità al contratto di licenza che governa il software in questione. Eventuali copie dovranno riportare la nota sul copyright di PTC e qualsiasi altro avviso di natura proprietaria fornito da PTC. La presente documentazione non può essere divulgata, trasferita, modificata o convertita con alcun mezzo, compreso i supporti elettronici, trasmessa o resa pubblica in alcun modo, senza il previo consenso scritto di PTC e non viene concessa alcuna autorizzazione per l'esecuzione di copie per tali scopi.")
   public static final String PRIVATE_CONSTANT_24 = "docs";

   @RBEntry("Quanto riportato in questa sede ha scopo puramente informativo ed è soggetto a modifica senza preavviso; in nessun caso dovrà essere considerato come una garanzia o un impegno da parte di PTC.")
   public static final String PRIVATE_CONSTANT_25 = "docs2";

   @RBEntry("Marchi registrati di Parametric Technology Corporation o di una sua consociata")
   public static final String PRIVATE_CONSTANT_26 = "rtmTitle";

   @RBEntry("Advanced Surface Design, Behavioral Modeling, CADDS, Computervision, CounterPart, Create &middot Collaborate &middot Control, EPD, EPD.Connect, Expert Machinist, Flexible Engineering, GRANITE, HARNESSDESIGN, Info*Engine, InPart, MECHANICA, Optegra, Parametric Technology, Parametric Technology Corporation, PartSpeak, PHOTORENDER, Pro/DESKTOP, Pro/E, Pro/ENGINEER, Pro/HELP, Pro/INTRALINK, Pro/MECHANICA, Pro/TOOLKIT, Product First, Product Development Means Business, Product Makes the Company, PTC, il logo PTC, PT/Products, Shaping Innovation, The Way to Product First e Windchill.")
   public static final String PRIVATE_CONSTANT_27 = "rtm";

   @RBEntry("Marchi di Parametric Technology Corporation o di una sua consociata")
   public static final String PRIVATE_CONSTANT_28 = "tmTitle";

   @RBEntry("3DPAINT, Associative Topology Bus, AutobuildZ, CDRS, CV, CVact, CVaec, CVdesign, CV-DORS, CVMAC, CVNC, CVToolmaker, EDAcompare, EDAconduit, DataDoctor, DesignSuite, DIMENSION III, Distributed Services Manager, DIVISION, e/ENGINEER, eNC Explorer, Expert Framework, Expert MoldBase, Expert Toolmaker, Harmony, InterComm, InterComm Expert, InterComm EDAcompare, InterComm EDAconduit, ISSM, KDiP, Knowledge Discipline in Practice, Knowledge System Driver, ModelCHECK, MoldShop, NC Builder, Pro/ANIMATE, Pro/ASSEMBLY, Pro/CABLING, Pro/CASTING, Pro/CDT, Pro/CMM, Pro/COLLABORATE, Pro/COMPOSITE, Pro/CONCEPT, Pro/CONVERT, Pro/DATA for PDGS, Pro/DESIGNER, Pro/DETAIL, Pro/DIAGRAM, Pro/DIEFACE, Pro/DRAW, Pro/ECAD, Pro/ENGINE, Pro/FEATURE, Pro/FEM-POST, Pro/FICIENCY, Pro/FLY-THROUGH, Pro/HARNESS, Pro/INTERFACE, Pro/LANGUAGE, Pro/LEGACY, Pro/LIBRARYACCESS, Pro/MESH, Pro/Model.View, Pro/MOLDESIGN, Pro/NC-ADVANCED, Pro/NC-CHECK, Pro/NCMILL, Pro/NCPOST, Pro/NC-SHEETMETAL, Pro/NC-TURN, Pro/NC-WEDM, Pro/NC-Wire EDM, Pro/NETWORK ANIMATOR, Pro/NOTEBOOK, Pro/PDM, Pro/PHOTORENDER, Pro/PIPING, Pro/PLASTIC ADVISOR, Pro/PLOT, Pro/POWER DESIGN, Pro/PROCESS, Pro/REPORT, Pro/REVIEW, Pro/SCAN-TOOLS, Pro/SHEETMETAL, Pro/SURFACE, Pro/VERIFY, Pro/Web.Link, Pro/Web.Publish, Pro/WELDING, ProductView, PTC Precision, Routed Systems Designer Shrinkwrap, Simple &middot Powerful &middot Connected, The Product Development Company, Wildfire, Windchill DynamicDesignLink, Windchill PartsLink, Windchill PDMLink, Windchill ProjectLink e Windchill SupplyLink.")
   public static final String PRIVATE_CONSTANT_29 = "tm";

   @RBEntry("Brevetti della Parametric Technology Corporation o di una sua consociata")
   public static final String PRIVATE_CONSTANT_30 = "patentTitle";

   @RBEntry("Ulteriori brevetti equivalenti possono esistere o essere in corso di concessione al di fuori degli Stati Uniti. Contattare PTC per ulteriori informazioni.")
   public static final String PRIVATE_CONSTANT_31 = "patentDesc";

   @RBEntry("N. registrazione")
   public static final String PRIVATE_CONSTANT_32 = "patentRegNoTitle";

   @RBEntry("Data concessione")
   public static final String PRIVATE_CONSTANT_33 = "patentIssueDateTitle";

   @RBEntry("Marchi di terze parti")
   public static final String PRIVATE_CONSTANT_34 = "3rdPartyTmTitle";

   @RBEntry("Adobe, Acrobat, Distiller e il logo Acrobat sono marchi registrati di Adobe Systems Incorporated. Advanced ClusterProven, ClusterProven e il logo ClusterProven sono marchi o marchi registrati di International Business Machines Corporation negli Stati Uniti e in altri paesi e sono utilizzati su licenza. IBM Corporation non garantisce e non &egrave; responsabile del funzionamento di questo prodotto software. AIX &egrave; un marchio registrato di IBM Corporation. Allegro, Cadence e Concept sono marchi registrati di Cadence Design Systems, Inc. Apple, Mac, Mac OS e Panther sono marchi o marchi registrati di Apple Computer, Inc. AutoCAD e Autodesk Inventor sono marchi registrati di Autodesk, Inc. Baan &egrave; un marchio registrato di Baan Company. CADAM e CATIA sono marchi registrati di Dassault Syst&egrave;mes. COACH &egrave; un marchio di CADTRAIN, Inc. DOORS &egrave; un marchio registrato di Telelogic AB. FLEX<i>lm </i> &egrave; un marchio di Macrovision Corporation. Geomagic &egrave; un marchio registrato di Raindrop Geomagic, Inc. EVERSYNC, GROOVE, GROOVEFEST, GROOVE.NET, GROOVE NETWORKS, iGROOVE, PEERWARE e il logo con i cerchi intrecciati sono marchi di Groove Networks, Inc. Helix &egrave; un marchio di Microcadam, Inc. HOOPS &egrave; un marchio di Tech Soft America, Inc. HP-UX &egrave; un marchio registrato di Hewlett-Packard Company. I-DEAS, Metaphase, Parasolid, SHERPA, Solid Edge e Unigraphics sono marchi o marchi registrati di UGS Corp. InstallShield &egrave; un marchio registrato e un marchio di servizio di InstallShield Software Corporation negli Stati Uniti e/o in altri paesi. Intel &egrave; un marchio registrato di Intel Corporation. IRIX &egrave; un marchio registrato di Silicon Graphics, Inc. LINUX &egrave; un marchio registrato di Linus Torvalds. MainWin e Mainsoft sono marchi di Mainsoft Corporation. MatrixOne &egrave; un marchio di MatrixOne, Inc. Mentor Graphics e Board Station sono marchi registrati, 3D Design, AMPLE e Design Manager sono marchi commerciali di Mentor Graphics Corporation. MEDUSA e STHENO sono marchi di CAD Schroer GmbH. Microsoft, Microsoft Project, Windows, il logo Windows, Windows NT, Visual Basic e il logo Visual Basic sono marchi registrati di Microsoft Corporation negli Stati Uniti d'America e/o in altri paesi. Netscape, la N di Netscape e il logo del timone sono marchi registrati di Netscape Communications Corporation negli Stati Uniti e in altri paesi. Oracle &egrave; un marchio registrato di Oracle Corporation. OrbixWeb &egrave; un marchio registrato di IONA Technologies PLC. PDGS &egrave; un marchio registrato di Ford Motor Company. RAND &egrave; un marchio di RAND Worldwide. Rational Rose &egrave; un marchio registrato di Rational Software Corporation. RetrievalWare &egrave; un marchio registrato di Convera Corporation. RosettaNet &egrave; un marchio, Partner Interface Process e PIP sono marchi registrati di RosettaNet, un'organizzazione senza scopo di lucro. SAP e R/3 sono marchi registrati di SAP AG Germany. SolidWorks &egrave; un marchio registrato di SolidWorks Corporation. Tutti i marchi SPARC sono usati su licenza e sono marchi o marchi registrati di SPARC International, Inc. negli Stati Uniti e in altri paesi. I prodotti con marchio SPARC sono stati realizzati su un'architettura sviluppata da Sun Microsystems, Inc. Sun, Sun Microsystems, il logo Sun, Solaris, UltraSPARC, Java, tutti i marchi basati su Java e &quot;The Network is the Computer&quot; sono marchi o marchi registrati di Sun Microsystems, Inc. negli Stati Uniti e in altri Paesi. TIBCO, TIBCO Software, TIBCO ActiveEnterprise, TIBCO Designer, TIBCO Enterprise for JMS, TIBCO Rendezvous, TIBCO Turbo XML, TIBCO Business Works sono marchi commerciali o marchi depositati di TIBCO Software Inc. negli Stati Uniti e in altri paesi. WebEx &egrave; un marchio di WebEx Communications, Inc.")
   public static final String PRIVATE_CONSTANT_35 = "3rdPartyTm";

   @RBEntry("Informazioni tecnologiche di terzi")
   public static final String PRIVATE_CONSTANT_36 = "3rdPartyInfoTitle";

   @RBEntry("Alcuni prodotti software PTC contengono tecnologie ottenute in licenza da terze parti: Rational Rose 2000E è un software protetto da copyright di Rational Software Corporation. RetrievalWare è un software protetto da copyright di Convera Corporation. La libreria VisTools è un software protetto da copyright di Visual Kinematics, Inc. (VKI) contenente segreti commerciali di natura riservata di proprietà di VKI. HOOPS Graphics System è un prodotto software proprietario ed è protetto da copyright di Tech Soft America, Inc. G-POST è un software protetto da copyright e un marchio registrato di Intercim. VERICUT è un software protetto da copyright e un marchio registrato di CGTech. Pro/PLASTIC ADVISOR è basato su tecnologia fornita da Moldflow. Moldflow è un marchio registrato di Moldflow Corporation. <br>MainWin Dedicated Libraries sono software protetto da copyright di Mainsoft Corporation. Alcuni software sono forniti da TIBCO Software Inc. L'output di immagini JPEG nel modulo Pro/Web.Publish è basato in parte sul lavoro dell'associazione indipendente JPEG Group. DFORMD.DLL è software protetto da copyright di Compaq Computer Corporation e non può essere distribuito. METIS è stato sviluppato da George Karypis e Vipin Kumar presso la University of Minnesota; per informazioni relative al progetto, consultare il sito Web <A HREF='javascript:var a=window.open(\"http://www.cs.umn.edu/~karypis/metis\")'>http://www.cs.umn.edu/~karypis/metis</A>. METIS è &copy 1997 Regents of the University of Minnesota. LightWork Libraries sono protette da copyright di LightWork Design 1990-2001. Visual Basic for Applications e Internet Explorer sono software protetto da copyright di Microsoft Corporation. <br>Parasolid &copy UGS Corp. Windchill Info*Engine Server contiene IBM XML Parser for Java Edition e IBM Lotus XSL Edition. Componenti del calendario popup Copyright &copy 1998 Netscape Communications Corporation. Tutti i diritti riservati. TECHNOMATIX è un software protetto da copyright e contiene informazioni proprietarie di Technomatix Technologies Ltd. TIBCO ActiveEnterprise, TIBCO Designer, TIBCO Enterprise for JMS, TIBCO Rendezvous, TIBCO Turbo XML, <br>TIBCO Business Works sono forniti da TIBCO Software Inc. La tecnologia \"Powered by Groove\" è fornita da Groove Networks, Inc. La tecnologia \"Powered by WebEx\" è fornita da WebEx Communications, Inc. Oracle 8i run-time e Oracle 9i run-time, copyright 2002-2003 Oracle Corporation. I programmi Oracle forniti sono soggetti a licenza d'uso limitata e possono essere utilizzati esclusivamente con il software PTC a cui sono acclusi. Apache Server, TomCat, Xalan, Xerces e Jakarta sono tecnologie sviluppate da Apache Software Foundation <A HREF='javascript:var a=window.open(\"http://www.apache.org/\")'>http://www.apache.org</A>, che ne detiene il copyright. Il loro utilizzo è soggetto alle condizioni e limiti specificati al seguente indirizzo: <A HREF='javascript:var a=window.open(\"http://www.apache.org\")'>http://www.apache.org</A>. Adobe Acrobat Reader e Adobe Distiller sono programmi protetti da copyright di Adobe Systems Inc. e soggetti al contratto di licenza con l'utente finale di Adobe fornito dalla società con il prodotto. UnZip (&copy 1990-2001 Info-ZIP, Tutti i diritti riservati) viene fornito \"COME TALE\" e SENZA GARANZIE DI ALCUN TIPO. Per la licenza completa Info-ZIP, vedere <A HREF='javascript:var a=window.open(\"ftp://ftp.info-zip.org/pub/infozip/license.html\")'>ftp://ftp.info-zip.org/pub/infozip/license.html</A>. Java (TM) Telnet Applet (StatusPeer.java, TelnetIO.java, TelnetWrapper.java, TimedOutException.java), copyright &copy 1996, 97 Mattias L. Jugel, Marcus Mei&#223ner, è ridistribuita con GNU General Public License. La licenza proviene dal titolare originale del copyright e l'Applet viene fornito SENZA ALCUNA GARANZIA. Per ricevere una copia del codice sorgente per l'Applet (al costo dell'esecuzione materiale della distribuzione del codice), è possibile scegliere se visitare <A HREF='javascript:var a=window.open(\"http://www.mud.de/se/jta\")'>http://www.mud.de/se/jta</A> oppure inviare un messaggio di posta elettronica a leo@mud.de o marcus@mud.de. Anche il codice sorgente viene fornito su licenza GNU General Public License. GTK+ - The GIMP Toolkit sono forniti in licenza in base alla GNU Library General Public License (LGPL). Per richiedere una copia del codice sorgente, visitare <A HREF='javascript:var a=window.open(\"http://www.gtk.org\")'>http://www.gtk.org</A>. Anch'esso verrà fornito ai sensi di GNU LGPL. zlib software Copyright &copy 1995-2002 Jean-loup Gailly e Mark Adler. Può includere software crittografato, scritto da Eric Young (eay@cryptsoft.com). OmniORB è distribuito ai sensi della GNU. Java Getopt.jar, copyright 1987-1997 Free Software Foundation, Inc.; Java Port Copyright 1998 Aaron M. Renn (arenn@urbanophile.com) è ridistribuito ai sensi di GNU LGPL. Per richiedere una copia del codice sorgente, visitare: <A HREF='javascript:var a=window.open(\"http://www.urbanophile.com/arenn/hacking/download.html\")'>http://www.urbanophile.com/arenn/hacking/download.html</A>. Anche il codice sorgente viene fornito su licenza GNU LGPL. Questo prodotto può includere software sviluppato da OpenSSL Project per l'uso in OpenSSL Toolkit <A HREF='javascript:var a=window.open(\"http://www.openssl.org/\")'>http://www.openssl.org</A>: <br>Copyright (c) 1998-2003 The OpenSSL Project. Tutti i diritti riservati. Questo prodotto può includere software crittografato scritto da Eric Young (eay@cryptsoft.com). I componenti Gecko e Mozilla sono soggetti alla licenza Mozilla Public License Version 1.1 all'indirizzo <A HREF='javascript:var a=window.open(\"http://www.mozilla.org/MPL/\")'>http://www.mozilla.org/MPL</A>. Il software distribuito dietro MPL viene fornito \"COME TALE\" e SENZA GARANZIE DI ALCUN TIPO (implicite o esplicite). Per i diritti e le limitazioni relativi a una versione in una lingua specifica, vedere la licenza MPL. I componenti Mozilla della versione localizzata in giapponese sono soggetti alla licenza Netscape Public License Version 1.1 (<A HREF='javascript:var a=window.open(\"http://www.mozilla.org/NPL/\")'>http://www.mozilla.org/NPL</A>. Il software distribuito ai sensi della Netscape Public License (NPL) viene fornito \"COME TALE\" e SENZA GARANZIE DI ALCUN TIPO (implicite o esplicite). Vedere NPL per la dichiarazione specifica che disciplina i diritti e le limitazioni per le edizioni nelle varie lingue. Il codice originale è il client Mozilla Communicator, release del 31 marzo 1998 e il codice originale del programmatore iniziale è di proprietà di Netscape Communications Corporation. Le parti create da Netscape sono Copyright (c) 1998 Netscape Communications Corporation. Tutti i diritti riservati. Hanno contribuito: Kazu Yamamoto (kazu@mozilla.gr.jp); Ryoichi Furukawa (furu@mozilla.gr.jp); Tsukasa Maruyama (mal@mozilla.gr.jp); Teiji Matsuba (matsuba@dream.com).  iCal4j è Copyright &copy 2005, Ben Fortuna.")
   public static final String PRIVATE_CONSTANT_37 = "3rdPartyInfo";

   @RBEntry("Assiemi installati")
   public static final String PRIVATE_CONSTANT_38 = "installedAssemblies";

   @RBEntry("INFORMAZIONI SUL SOFTWARE")
   public static final String PRIVATE_CONSTANT_39 = "softwareAbout";
}
