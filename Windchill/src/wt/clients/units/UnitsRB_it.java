/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.units;

import wt.util.resource.*;

@RBUUID("wt.clients.units.UnitsRB")
public final class UnitsRB_it extends wt.util.resource.NestableListResourceBundle {
   @RBEntry("Recupero sistemi di misurazione fallito:\n\n {0}")
   public static final String GET_ALL_MEASUREMENT_SYSTEM_FAILED = "ex00";

   @RBEntry("Eliminazione di \"{0}\" non riuscita:\n\n{1}")
   public static final String DELETE_MEASUREMENT_SYSTEM_FAILED = "ex01";

   @RBEntry("Creazione nuovo \"{0}\" non riuscita:\n\n {1}")
   public static final String CREATE_MEASUREMENT_SYSTEM_FAILED = "ex02";

   @RBEntry("Modifica di \"{0}\" non riuscita:\n\n{1}")
   public static final String UPDATE_MEASUREMENT_SYSTEM_FAILED = "ex03";

   @RBEntry("Recupero quantità di misura fallito:\n\n {0}")
   public static final String GET_ALL_QUANTITY_OF_MEASURE_FAILED = "ex04";

   @RBEntry("Eliminazione di \"{0}\" non riuscita:\n\n{1}")
   public static final String DELETE_QUANTITY_OF_MEASURE_FAILED = "ex05";

   @RBEntry("Creazione nuovo \"{0}\" non riuscita:\n\n {1}")
   public static final String CREATE_QUANTITY_OF_MEASURE_FAILED = "ex06";

   @RBEntry("Modifica di \"{0}\" non riuscita:\n\n{1}")
   public static final String UPDATE_QUANTITY_OF_MEASURE_FAILED = "ex07";

   @RBEntry("Il sistema di misurazione \"{0}\" esiste già")
   public static final String MEASUREMENT_SYSTEM_NOT_UNIQUE = "ex08";

   @RBEntry("La quantità di misura \"{0}\"  esiste già")
   public static final String QUANTITY_OF_MEASURE_NOT_UNIQUE = "ex09";

   @RBEntry("Sistema di misurazione")
   public static final String MS_HEADER = "h00";

   @RBEntry("Default")
   public static final String DEFAULT_HEADER = "h01";

   @RBEntry("Ignora")
   public static final String OVERRIDE_HEADER = "h02";

   @RBEntry("Nome:")
   public static final String NAME_LABEL = "la0";

   @RBEntry("Descrizione: ")
   public static final String DESCRIPTION_LABEL = "la1";

   @RBEntry("Unità:")
   public static final String BASE_UNIT_LABEL = "la2";

   @RBEntry("Unità visualizzate:")
   public static final String DISPLAY_UNITS_LABEL = "la3";

   @RBEntry("Unità rappresentative:")
   public static final String REPERSENTATIVE_UNITS_LABEL = "la4";

   @RBEntry("'Nome' è un campo obbligatorio.")
   public static final String MS_BLANK_NAME = "m00";

   @RBEntry("Il sistema di misurazione esiste già.")
   public static final String MS_ALREADY_EXISTS = "m01";

   @RBEntry("'Nome' è un campo obbligatorio.")
   public static final String QOM_BLANK_NAME = "m02";

   @RBEntry("'Unità' non specificata. Continuare?")
   public static final String QOM_BLANK_UNIT = "m03";

   @RBEntry("La quantità  di misura esiste già.")
   public static final String QOM_ALREADY_EXISTS = "m04";

   @RBEntry("Nuovo sistema di misurazione")
   public static final String MS_CREATOR_TITLE = "ti0";

   @RBEntry("Seleziona sistema di misurazione")
   public static final String MS_SELECTOR_TITLE = "ti1";

   @RBEntry("Modifica sistema di misurazione")
   public static final String MS_EDITOR_TITLE = "ti2";

   @RBEntry("Visualizza sistema di misurazione")
   public static final String MS_VIEWER_TITLE = "ti3";

   @RBEntry("Gestisci sistemi di misurazione")
   public static final String MS_MANAGER_TITLE = "ti4";

   @RBEntry("Nuova quantità di misura")
   public static final String QOM_CREATOR_TITLE = "ti5";

   @RBEntry("Seleziona quantità di misura")
   public static final String QOM_SELECTOR_TITLE = "ti6";

   @RBEntry("Modifica quantità di misura")
   public static final String QOM_EDITOR_TITLE = "ti7";

   @RBEntry("Visualizza quantità di misura")
   public static final String QOM_VIEWER_TITLE = "ti8";

   @RBEntry("Gestisci quantità di misura")
   public static final String QOM_MANAGER_TITLE = "ti9";

   @RBEntry("Gestione schema")
   public static final String SCH_MANAGER_TITLE = "ti10";

   @RBEntry("TypeMgrMeasSysMgrAbout")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String MSHELPPAGE = "url0";

   @RBEntry("TypeMgrQuantOfMeasAbout")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String QOMHELPPAGE = "url1";

   @RBEntry("wt/clients/iba/IBA_it.html")
   @RBPseudo(false)
   public static final String MSHOMEPAGE = "url2";

   @RBEntry("wt/clients/iba/IBA_it.html")
   @RBPseudo(false)
   public static final String QOMHOMEPAGE = "url3";

   @RBEntry("L'unità non può contenere spazi")
   @RBComment("Error message displaed when a Unit contains spaces.")
   public static final String UNIT_CANT_CONTAIN_SPACES = "UNIT_CANT_CONTAIN_SPACES";
}
