/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.units;

import wt.util.resource.*;

@RBUUID("wt.clients.units.UnitsRB")
public final class UnitsRB extends wt.util.resource.NestableListResourceBundle {
   @RBEntry("Retrieve Measurement Systems failed:\n\n {0}")
   public static final String GET_ALL_MEASUREMENT_SYSTEM_FAILED = "ex00";

   @RBEntry("Delete \"{0}\" failed:\n\n {1}")
   public static final String DELETE_MEASUREMENT_SYSTEM_FAILED = "ex01";

   @RBEntry("New \"{0}\" failed:\n\n {1}")
   public static final String CREATE_MEASUREMENT_SYSTEM_FAILED = "ex02";

   @RBEntry("Edit \"{0}\" failed:\n\n {1}")
   public static final String UPDATE_MEASUREMENT_SYSTEM_FAILED = "ex03";

   @RBEntry("Retrieve Quantities of Measure failed:\n\n {0}")
   public static final String GET_ALL_QUANTITY_OF_MEASURE_FAILED = "ex04";

   @RBEntry("Delete \"{0}\" failed:\n\n {1}")
   public static final String DELETE_QUANTITY_OF_MEASURE_FAILED = "ex05";

   @RBEntry("New \"{0}\" failed:\n\n {1}")
   public static final String CREATE_QUANTITY_OF_MEASURE_FAILED = "ex06";

   @RBEntry("Edit \"{0}\" failed:\n\n {1}")
   public static final String UPDATE_QUANTITY_OF_MEASURE_FAILED = "ex07";

   @RBEntry("The measurement system \"{0}\" already exists")
   public static final String MEASUREMENT_SYSTEM_NOT_UNIQUE = "ex08";

   @RBEntry("The Quantity of Measure \"{0}\"  already exists")
   public static final String QUANTITY_OF_MEASURE_NOT_UNIQUE = "ex09";

   @RBEntry("Measurement System")
   public static final String MS_HEADER = "h00";

   @RBEntry("Default")
   public static final String DEFAULT_HEADER = "h01";

   @RBEntry("Override")
   public static final String OVERRIDE_HEADER = "h02";

   @RBEntry("Name:")
   public static final String NAME_LABEL = "la0";

   @RBEntry("Description:")
   public static final String DESCRIPTION_LABEL = "la1";

   @RBEntry("Unit:")
   public static final String BASE_UNIT_LABEL = "la2";

   @RBEntry("Display Units:")
   public static final String DISPLAY_UNITS_LABEL = "la3";

   @RBEntry("Representative Units:")
   public static final String REPERSENTATIVE_UNITS_LABEL = "la4";

   @RBEntry("'Name' is a required field.")
   public static final String MS_BLANK_NAME = "m00";

   @RBEntry("Measurement System already exists.")
   public static final String MS_ALREADY_EXISTS = "m01";

   @RBEntry("'Name' is a required field.")
   public static final String QOM_BLANK_NAME = "m02";

   @RBEntry("'Unit' not specified.  Do you want to continue?")
   public static final String QOM_BLANK_UNIT = "m03";

   @RBEntry("Quantity of Measure already exists.")
   public static final String QOM_ALREADY_EXISTS = "m04";

   @RBEntry("New Measurement System")
   public static final String MS_CREATOR_TITLE = "ti0";

   @RBEntry("Select Measurement System")
   public static final String MS_SELECTOR_TITLE = "ti1";

   @RBEntry("Edit Measurement System")
   public static final String MS_EDITOR_TITLE = "ti2";

   @RBEntry("View Measurement System")
   public static final String MS_VIEWER_TITLE = "ti3";

   @RBEntry("Manage Measurement Systems")
   public static final String MS_MANAGER_TITLE = "ti4";

   @RBEntry("New Quantity of Measure")
   public static final String QOM_CREATOR_TITLE = "ti5";

   @RBEntry("Select Quantity of Measure")
   public static final String QOM_SELECTOR_TITLE = "ti6";

   @RBEntry("Edit Quantity of Measure")
   public static final String QOM_EDITOR_TITLE = "ti7";

   @RBEntry("View Quantity of Measure")
   public static final String QOM_VIEWER_TITLE = "ti8";

   @RBEntry("Manage Quantity of Measures")
   public static final String QOM_MANAGER_TITLE = "ti9";

   @RBEntry("Schema Manager")
   public static final String SCH_MANAGER_TITLE = "ti10";

   @RBEntry("TypeMgrMeasSysMgrAbout")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String MSHELPPAGE = "url0";

   @RBEntry("TypeMgrQuantOfMeasAbout")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String QOMHELPPAGE = "url1";

   @RBEntry("wt/clients/iba/IBA.html")
   @RBPseudo(false)
   public static final String MSHOMEPAGE = "url2";

   @RBEntry("wt/clients/iba/IBA.html")
   @RBPseudo(false)
   public static final String QOMHOMEPAGE = "url3";

   @RBEntry("Unit cannot contain spaces")
   @RBComment("Error message displaed when a Unit contains spaces.")
   public static final String UNIT_CANT_CONTAIN_SPACES = "UNIT_CANT_CONTAIN_SPACES";
}
