/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.widgets;

import wt.util.resource.*;

@RBUUID("wt.clients.widgets.BaseRB")
public final class BaseRB_it extends WTListResourceBundle {
   @RBEntry("Seleziona")
   public static final String SELECT_BUTTON_LABEL = "bb0";

   @RBEntry("Salva")
   public static final String APPLY_BUTTON_LABEL = "bb1";

   @RBEntry("Annulla ")
   public static final String CANCEL_BUTTON_LABEL = "bb2";

   @RBEntry("Guida")
   public static final String HELP_BUTTON_LABEL = "bb3";

   @RBEntry("OK")
   public static final String OK_BUTTON_LABEL = "bb4";

   @RBEntry("Sì")
   public static final String YES_BUTTON_LABEL = "bb5";

   @RBEntry("No")
   public static final String NO_BUTTON_LABEL = "bb6";

   @RBEntry("Unità:")
   public static final String MEASUREMENT_SYSTEM_LABEL = "bb7";

   @RBEntry("netmarkets/images/create_tbar.gif")
   @RBPseudo(false)
   public static final String CREATE_ICON = "bi0";

   @RBEntry("netmarkets/images/delete.gif")
   @RBPseudo(false)
   public static final String DELETE_ICON = "bi1";

   @RBEntry("netmarkets/images/edit.gif")
   @RBPseudo(false)
   public static final String EDIT_ICON = "bi2";

   @RBEntry("wt/clients/images/cut.gif")
   @RBPseudo(false)
   public static final String CUT_ICON = "bi3";

   @RBEntry("wt/clients/images/copy.gif")
   @RBPseudo(false)
   public static final String COPY_ICON = "bi4";

   @RBEntry("wt/clients/images/paste.gif")
   @RBPseudo(false)
   public static final String PASTE_ICON = "bi5";

   @RBEntry("wt/clients/images/home.gif")
   @RBPseudo(false)
   public static final String HOME_ICON = "bi6";

   @RBEntry("netmarkets/images/help_tablebutton.gif")
   @RBPseudo(false)
   public static final String HELP_ICON = "bi7";

   @RBEntry("Creazione fallita.")
   public static final String CREATE_FAILED = "bm00";

   @RBEntry("Recupero fallito.")
   public static final String RETRIEVE_FAILED = "bm01";

   @RBEntry("Modifica non riuscita")
   public static final String UPDATE_FAILED = "bm02";

   @RBEntry("Eliminazione fallita.")
   public static final String DELETE_FAILED = "bm03";

   @RBEntry("Connessione server fallita.")
   public static final String RMI_FAILED = "bm04";

   @RBEntry("Salvare le modifiche apportate?")
   public static final String CONFIRM_CLOSE = "bm05";

   @RBEntry("Eliminare \"{0}\"?")
   public static final String CONFIRM_DELETE = "bm06";

   @RBEntry("Uscire?")
   public static final String CONFIRM_EXIT = "bm07";

   @RBEntry("Eliminare?")
   public static final String CONFIRM_CANCEL = "bm08";

   @RBEntry("Creazione in corso...")
   public static final String STATUS_CREATE = "bst00";

   @RBEntry("Modalità visualizzazione")
   public static final String STATUS_VIEW = "bst01";

   @RBEntry("Modalità di modifica")
   public static final String STATUS_UPDATE = "bst02";

   @RBEntry("Eliminazione in corso...")
   public static final String STATUS_DELETE = "bst03";

   @RBEntry("Aggiornamento in corso...")
   public static final String STATUS_SAVE = "bst04";

   @RBEntry("Modifica completata")
   public static final String STATUS_SAVE_SUCCESS = "bst05";

   @RBEntry("Modifica non riuscita")
   public static final String STATUS_SAVE_FAILURE = "bst06";

   @RBEntry("\"{0}\" tagliato")
   public static final String STATUS_CUT = "bst07";

   @RBEntry("\"{0}\" copiato")
   public static final String STATUS_COPY = "bst08";

   @RBEntry("\"{0}\" spostato")
   public static final String STATUS_MOVED = "bst09";

   @RBEntry("\"{0}\" copiato")
   public static final String STATUS_COPIED = "bst10";

   @RBEntry("Conferma chiusura")
   public static final String CONFIRM_CLOSE_DIALOG_TITLE = "bti0";

   @RBEntry("Conferma eliminazione")
   public static final String CONFIRM_DELETE_DIALOG_TITLE = "bti1";

   @RBEntry("Errore")
   public static final String ERROR_DIALOG_TITLE = "bti2";

   @RBEntry("Avvertenza")
   public static final String WARNING_DIALOG_TITLE = "bti3";

   @RBEntry("Autore base")
   public static final String BASE_CREATOR_TITLE = "bti4";

   @RBEntry("Editor base")
   public static final String BASE_EDITOR_TITLE = "bti5";

   @RBEntry("Selettore base")
   public static final String BASE_SELECTOR_TITLE = "bti6";

   @RBEntry("Gestore base")
   public static final String BASE_MANAGER_TITLE = "bti7";

   @RBEntry("Nuovo")
   public static final String CREATE_TOOLTIP = "btt0";

   @RBEntry("Elimina")
   public static final String DELETE_TOOLTIP = "btt1";

   @RBEntry("Modifica")
   public static final String EDIT_TOOLTIP = "btt2";

   @RBEntry("Taglia ")
   public static final String CUT_TOOLTIP = "btt3";

   @RBEntry("Copia ")
   public static final String COPY_TOOLTIP = "btt4";

   @RBEntry("Incolla")
   public static final String PASTE_TOOLTIP = "btt5";

   @RBEntry("Home ")
   public static final String HOME_TOOLTIP = "btt6";

   @RBEntry("Guida")
   public static final String HELP_TOOLTIP = "btt7";
}
