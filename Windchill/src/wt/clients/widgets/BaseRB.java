/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.widgets;

import wt.util.resource.*;

@RBUUID("wt.clients.widgets.BaseRB")
public final class BaseRB extends WTListResourceBundle {
   @RBEntry("Select ")
   public static final String SELECT_BUTTON_LABEL = "bb0";

   @RBEntry("Save ")
   public static final String APPLY_BUTTON_LABEL = "bb1";

   @RBEntry("Cancel ")
   public static final String CANCEL_BUTTON_LABEL = "bb2";

   @RBEntry("Help ")
   public static final String HELP_BUTTON_LABEL = "bb3";

   @RBEntry("OK ")
   public static final String OK_BUTTON_LABEL = "bb4";

   @RBEntry("Yes ")
   public static final String YES_BUTTON_LABEL = "bb5";

   @RBEntry("No ")
   public static final String NO_BUTTON_LABEL = "bb6";

   @RBEntry("Units: ")
   public static final String MEASUREMENT_SYSTEM_LABEL = "bb7";

   @RBEntry("netmarkets/images/create_tbar.gif")
   @RBPseudo(false)
   public static final String CREATE_ICON = "bi0";

   @RBEntry("netmarkets/images/delete.gif")
   @RBPseudo(false)
   public static final String DELETE_ICON = "bi1";

   @RBEntry("netmarkets/images/edit.gif")
   @RBPseudo(false)
   public static final String EDIT_ICON = "bi2";

   @RBEntry("wt/clients/images/cut.gif")
   @RBPseudo(false)
   public static final String CUT_ICON = "bi3";

   @RBEntry("wt/clients/images/copy.gif")
   @RBPseudo(false)
   public static final String COPY_ICON = "bi4";

   @RBEntry("wt/clients/images/paste.gif")
   @RBPseudo(false)
   public static final String PASTE_ICON = "bi5";

   @RBEntry("wt/clients/images/home.gif")
   @RBPseudo(false)
   public static final String HOME_ICON = "bi6";

   @RBEntry("netmarkets/images/help_tablebutton.gif")
   @RBPseudo(false)
   public static final String HELP_ICON = "bi7";

   @RBEntry("New failed.")
   public static final String CREATE_FAILED = "bm00";

   @RBEntry("Retrieve failed.")
   public static final String RETRIEVE_FAILED = "bm01";

   @RBEntry("Edit failed.")
   public static final String UPDATE_FAILED = "bm02";

   @RBEntry("Delete failed.")
   public static final String DELETE_FAILED = "bm03";

   @RBEntry("Server connection failed.")
   public static final String RMI_FAILED = "bm04";

   @RBEntry("Do you want to save the changes you have made?")
   public static final String CONFIRM_CLOSE = "bm05";

   @RBEntry("Are you sure you want to delete \"{0}\"?")
   public static final String CONFIRM_DELETE = "bm06";

   @RBEntry("Do you want to exit?")
   public static final String CONFIRM_EXIT = "bm07";

   @RBEntry("Are you sure you want to cancel?")
   public static final String CONFIRM_CANCEL = "bm08";

   @RBEntry("Creating...")
   public static final String STATUS_CREATE = "bst00";

   @RBEntry("View Mode")
   public static final String STATUS_VIEW = "bst01";

   @RBEntry("Edit Mode")
   public static final String STATUS_UPDATE = "bst02";

   @RBEntry("Deleting...")
   public static final String STATUS_DELETE = "bst03";

   @RBEntry("Updating...")
   public static final String STATUS_SAVE = "bst04";

   @RBEntry("Edit successful")
   public static final String STATUS_SAVE_SUCCESS = "bst05";

   @RBEntry("Edit failed")
   public static final String STATUS_SAVE_FAILURE = "bst06";

   @RBEntry("\"{0}\" cut")
   public static final String STATUS_CUT = "bst07";

   @RBEntry("\"{0}\" copied")
   public static final String STATUS_COPY = "bst08";

   @RBEntry("\"{0}\" moved successfully")
   public static final String STATUS_MOVED = "bst09";

   @RBEntry("\"{0}\" copied successfully")
   public static final String STATUS_COPIED = "bst10";

   @RBEntry("Confirm Close")
   public static final String CONFIRM_CLOSE_DIALOG_TITLE = "bti0";

   @RBEntry("Confirm Delete")
   public static final String CONFIRM_DELETE_DIALOG_TITLE = "bti1";

   @RBEntry("Error")
   public static final String ERROR_DIALOG_TITLE = "bti2";

   @RBEntry("Warning")
   public static final String WARNING_DIALOG_TITLE = "bti3";

   @RBEntry("Base Creator")
   public static final String BASE_CREATOR_TITLE = "bti4";

   @RBEntry("Base Editor")
   public static final String BASE_EDITOR_TITLE = "bti5";

   @RBEntry("Base Selector")
   public static final String BASE_SELECTOR_TITLE = "bti6";

   @RBEntry("Base Manager")
   public static final String BASE_MANAGER_TITLE = "bti7";

   @RBEntry("New ")
   public static final String CREATE_TOOLTIP = "btt0";

   @RBEntry("Delete ")
   public static final String DELETE_TOOLTIP = "btt1";

   @RBEntry("Edit ")
   public static final String EDIT_TOOLTIP = "btt2";

   @RBEntry("Cut ")
   public static final String CUT_TOOLTIP = "btt3";

   @RBEntry("Copy ")
   public static final String COPY_TOOLTIP = "btt4";

   @RBEntry("Paste ")
   public static final String PASTE_TOOLTIP = "btt5";

   @RBEntry("Home ")
   public static final String HOME_TOOLTIP = "btt6";

   @RBEntry("Help ")
   public static final String HELP_TOOLTIP = "btt7";
}
