/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.util;

import wt.util.resource.*;

@RBUUID("wt.clients.util.UtilRB")
public final class UtilRB_it extends WTListResourceBundle {
   @RBEntry("<Tutto>")
   public static final String ALL = "0";

   @RBEntry(" ")
   public static final String UNSPECIFIED = "1";

   @RBEntry("Partecipanti")
   public static final String SELECTED_PANEL = "10";

   @RBEntry("Aggiungi >>")
   public static final String ADD_BUTTON = "11";

   @RBEntry("Aggiungi tutto >>")
   public static final String ADD_ALL_BUTTON = "12";

   @RBEntry("<< Rimuovi")
   public static final String REMOVE_BUTTON = "13";

   @RBEntry("<< Rimuovi tutto")
   public static final String REMOVE_ALL_BUTTON = "14";

   @RBEntry(":")
   public static final String COLON = "15";

   @RBEntry("Attori")
   public static final String ACTORS_TAB = "16";

   @RBEntry("Esporta modelli")
   public static final String EXPORT_DIALOG = "17";

   @RBEntry("Importa modelli")
   public static final String IMPORT_DIALOG = "18";

   @RBEntry("File esistenti")
   public static final String EXISTING_FILES = "19";

   @RBEntry("Eliminare {0}?")
   public static final String CONFIRM_DELETE_OBJECT = "2";

   @RBEntry("Nome file")
   public static final String FILE_NAME = "20";

   @RBEntry("Sovrascrivi file")
   public static final String OVERWRITE_FILE = "21";

   @RBEntry("Esporta")
   public static final String EXPORT_BTN = "22";

   @RBEntry("Importa")
   public static final String IMPORT_BTN = "23";

   @RBEntry("Annulla")
   public static final String CANCEL = "24";

   @RBEntry("Nota:  gli oggetti di riferimento del modello sono esclusi.")
   public static final String WARNING = "25";

   @RBEntry("Windchill - Home")
   public static final String WINDCHILL_HOME = "26";

   @RBEntry("senza titolo")
   public static final String UNTITLED = "27";

   @RBEntry("Aggiungi file")
   public static final String APPEND_FILE = "28";

   @RBEntry("Nome")
   public static final String NAME = "29";

   @RBEntry("Nome:")
   public static final String NAME_COLON = "42";

   @RBEntry("* Nome:")
   public static final String REQUIRED_NAME_COLON = "43";

   @RBEntry("Nessun riquadro padre disponibile. Impossibile visualizzare la finestra di dialogo per confermare l'operazione. Per visualizzare una finestra di dialogo è necessario un riquadro padre.")
   public static final String CANNOT_CONFIRM_ACTION = "3";

   @RBEntry("OK")
   public static final String OK_BUTTON = "30";

   @RBEntry("Annulla")
   public static final String CANCEL_BUTTON = "31";

   @RBEntry("Rinomina ")
   public static final String RENAME_LC_AS_TITLE = "32";

   @RBEntry("Salva con nome ")
   public static final String SAVE_LC_AS_TITLE = "33";

   @RBEntry("Copia di ")
   public static final String COPY_OF = "34";

   @RBEntry("Rinomina...")
   public static final String RENAME_BUTTON = "35";

   @RBEntry("Salva con nome...")
   public static final String SAVEAS_BUTTON = "36";

   @RBEntry("Gruppi")
   public static final String ALL_GROUPS_TAB = "4";

   @RBEntry("Utenti")
   public static final String USERS_TAB = "5";

   @RBEntry("Ruoli")
   public static final String ALL_ROLES_TAB = "6";

   @RBEntry("Utente")
   public static final String USER_LABEL = "7";

   @RBEntry("Gruppo")
   public static final String GROUP_LABEL = "8";

   @RBEntry("Trova")
   public static final String SEARCH_BUTTON = "9";

   @RBEntry("Home")
   public static final String HOME = "37";

   @RBEntry("Sfoglia...")
   public static final String BROWSE_FOR_FILE = "38";

   @RBEntry("Seleziona file")
   public static final String FILE_DIALOG_LOAD_TITLE = "39";

   @RBEntry("Salva con nome")
   public static final String FILE_DIALOG_SAVE_TITLE = "40";

   @RBEntry("Apri")
   @RBComment("Label for file dialog title/button (Open, as opposed to Save)")
   public static final String OPEN_BUTTON = "41";
}
