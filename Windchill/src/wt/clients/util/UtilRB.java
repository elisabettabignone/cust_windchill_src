/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.util;

import wt.util.resource.*;

@RBUUID("wt.clients.util.UtilRB")
public final class UtilRB extends WTListResourceBundle {
   @RBEntry("<All>")
   public static final String ALL = "0";

   @RBEntry(" ")
   public static final String UNSPECIFIED = "1";

   @RBEntry("Participants")
   public static final String SELECTED_PANEL = "10";

   @RBEntry("Add >>")
   public static final String ADD_BUTTON = "11";

   @RBEntry("Add All >>")
   public static final String ADD_ALL_BUTTON = "12";

   @RBEntry("<< Remove")
   public static final String REMOVE_BUTTON = "13";

   @RBEntry("<< Remove All")
   public static final String REMOVE_ALL_BUTTON = "14";

   @RBEntry(":")
   public static final String COLON = "15";

   @RBEntry("Actors")
   public static final String ACTORS_TAB = "16";

   @RBEntry("Export Templates")
   public static final String EXPORT_DIALOG = "17";

   @RBEntry("Import Templates")
   public static final String IMPORT_DIALOG = "18";

   @RBEntry("Existing Files")
   public static final String EXISTING_FILES = "19";

   @RBEntry("Are you sure you would like to delete {0}?")
   public static final String CONFIRM_DELETE_OBJECT = "2";

   @RBEntry("File name")
   public static final String FILE_NAME = "20";

   @RBEntry("Overwrite File")
   public static final String OVERWRITE_FILE = "21";

   @RBEntry("Export")
   public static final String EXPORT_BTN = "22";

   @RBEntry("Import")
   public static final String IMPORT_BTN = "23";

   @RBEntry("Cancel")
   public static final String CANCEL = "24";

   @RBEntry("Note:  Referenced objects within the template are excluded.")
   public static final String WARNING = "25";

   @RBEntry("Windchill Home")
   public static final String WINDCHILL_HOME = "26";

   @RBEntry("untitled")
   public static final String UNTITLED = "27";

   @RBEntry("Append File")
   public static final String APPEND_FILE = "28";

   @RBEntry("Name")
   public static final String NAME = "29";

   @RBEntry("Name:")
   public static final String NAME_COLON = "42";

   @RBEntry("* Name:")
   public static final String REQUIRED_NAME_COLON = "43";

   @RBEntry("The dialog to confirm the action could not be launched because no parenting frame was found. A parent frame is necessary to display a dialog.")
   public static final String CANNOT_CONFIRM_ACTION = "3";

   @RBEntry("OK")
   public static final String OK_BUTTON = "30";

   @RBEntry("Cancel")
   public static final String CANCEL_BUTTON = "31";

   @RBEntry("Rename ")
   public static final String RENAME_LC_AS_TITLE = "32";

   @RBEntry("Save As ")
   public static final String SAVE_LC_AS_TITLE = "33";

   @RBEntry("Copy of ")
   public static final String COPY_OF = "34";

   @RBEntry("Rename...")
   public static final String RENAME_BUTTON = "35";

   @RBEntry("Save As...")
   public static final String SAVEAS_BUTTON = "36";

   @RBEntry("Groups")
   public static final String ALL_GROUPS_TAB = "4";

   @RBEntry("Users")
   public static final String USERS_TAB = "5";

   @RBEntry("Roles")
   public static final String ALL_ROLES_TAB = "6";

   @RBEntry("User")
   public static final String USER_LABEL = "7";

   @RBEntry("Group")
   public static final String GROUP_LABEL = "8";

   @RBEntry("Find")
   public static final String SEARCH_BUTTON = "9";

   @RBEntry("Home")
   public static final String HOME = "37";

   @RBEntry("Browse...")
   public static final String BROWSE_FOR_FILE = "38";

   @RBEntry("Choose File")
   public static final String FILE_DIALOG_LOAD_TITLE = "39";

   @RBEntry("Save As")
   public static final String FILE_DIALOG_SAVE_TITLE = "40";

   @RBEntry("Open")
   @RBComment("Label for file dialog title/button (Open, as opposed to Save)")
   public static final String OPEN_BUTTON = "41";
}
