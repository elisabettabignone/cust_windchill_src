/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.util.http;

import wt.util.resource.*;

@RBUUID("wt.clients.util.http.HTTPUtilRB")
public final class HTTPUtilRB_it extends WTListResourceBundle {
   /**
    * BUTTON LABELS ---------------------------------------------------------
    * Labels used on command buttons
    * 
    **/
   @RBEntry("OK")
   @RBComment("Labels used on command buttons")
   public static final String PRIVATE_CONSTANT_0 = "okButton";

   @RBEntry("Annulla")
   @RBComment("Labels used on command buttons")
   public static final String PRIVATE_CONSTANT_1 = "cancelButton";

   @RBEntry("Apri file")
   @RBComment("Labels used on command buttons")
   public static final String PRIVATE_CONSTANT_2 = "openFileRadioButton";

   @RBEntry("Seleziona posizione di salvataggio file")
   @RBComment("Labels used on command buttons")
   public static final String PRIVATE_CONSTANT_3 = "saveFileRadioButton";

   @RBEntry("Sì")
   @RBComment("Labels used on command buttons")
   public static final String PRIVATE_CONSTANT_4 = "yesButton";

   @RBEntry("No")
   @RBComment("Labels used on command buttons")
   public static final String PRIVATE_CONSTANT_5 = "noButton";

   /**
    * DIALOG MESSAGES ---------------------------------------------------------
    * Labels for explanation.
    * 
    **/
   @RBEntry("È in corso lo scaricamento del seguente file: {0}")
   @RBArgComment0("The file path and filename of the file to be downloaded.")
   public static final String PRIVATE_CONSTANT_6 = "chooseFileOperationMessage";

   @RBEntry("L'utente ha selezionato {0} file da scaricare. Tutti i file saranno scaricati nella stessa posizione del primo file. Se si seleziona Apri file, viene aperto solo il primo file e tutti i file vengono salvati nella directory locale di default (che può essere impostata nelle preferenze utente), o nella directory temporanea Windows o nella home directory UNIX. Se si seleziona Salva file su disco, all'utente verrà richiesto di selezionare una directory locale in cui verranno salvati tutti i file.")
   @RBComment("Clarifying expected behavior when downloading multiple files.  ")
   @RBArgComment0("Number of files to be downloaded.")
   public static final String PRIVATE_CONSTANT_7 = "multipleFileOperationMessage";

   @RBEntry("Il file {0} esiste già. Sostituirlo?")
   @RBArgComment0("The file path and filename of the file to be downloaded.")
   public static final String PRIVATE_CONSTANT_8 = "confirmFileReplaceMessage";

   /**
    * DIALOG TITLES ------------------------------------------------------------
    * 
    **/
   @RBEntry("Scegli operazione su file")
   @RBComment("Titles displayed on dialogs.")
   public static final String PRIVATE_CONSTANT_9 = "chooseFileOperationTitle";

   @RBEntry("Conferma sostituzione file")
   @RBComment("Titles displayed on dialogs.")
   public static final String PRIVATE_CONSTANT_10 = "confirmFileReplaceTitle";

   /**
    * ------------------------------------------------
    **/
   @RBEntry("Scaricamento in corso")
   public static final String DOWNLOAD_PROGRESS_LABEL = "0";

   @RBEntry("Dimensione totale:")
   public static final String DOWNLOAD_PROGRESS_TOTAL_LABEL = "1";

   @RBEntry("Ricevuto:")
   public static final String DOWNLOAD_PROGRESS_RECEIVED_LABEL = "2";

   @RBEntry("Completo:")
   public static final String DOWNLOAD_PROGRESS_PERCENT_LABEL = "3";

   @RBEntry("Annulla")
   public static final String DOWNLOAD_PROGRESS_CANCEL_BUTTON = "4";

   @RBEntry("Avanzamento")
   public static final String DOWNLOAD_PROGRESS_DIALOG_TITLE = "5";

   /**
    * ------------------------------------------------
    **/
   @RBEntry("File principale")
   public static final String UPDN_FILE_CHOICE = "6";

   @RBEntry("URL principale")
   public static final String UPDN_URL_CHOICE = "7";

   /**
    * ------------------------------------------------
    **/
   @RBEntry("Sfoglia...")
   public static final String UPDN_BROWSE_LABEL = "8";

   @RBEntry("Carica")
   public static final String UPDN_UPLOAD_OPERATION_LABEL = "9";

   @RBEntry("Preleva")
   public static final String UPDN_DOWNLOAD_OPERATION_LABEL = "10";

   @RBEntry("Nome file predefinito:")
   public static final String UPDN_DEFAULT_FILE_NAME_LABEL = "11";

   @RBEntry("Formato:")
   public static final String UPDN_FILE_FORMAT_LABEL = "12";

   @RBEntry("Dimensione:")
   public static final String UPDN_FILE_SIZE_LABEL = "13";

   @RBEntry("Avvertimento di sovrascrittura file")
   public static final String UPDN_FILE_OVERWRITE_WARNING_LABEL = "14";

   @RBEntry("Richiesta di modifica file")
   public static final String UPDN_FILE_CHANGE_PROMPT_LABEL = "15";

   @RBEntry("Data server:")
   public static final String UPDN_SERVER_DATE_LABEL = "16";

   @RBEntry("Dimensione server:")
   public static final String UPDN_SERVER_SIZE_LABEL = "17";

   @RBEntry("URL server:")
   public static final String UPDN_SERVER_URL_LABEL = "18";

   @RBEntry("Commuta modalità")
   public static final String UPDN_TOGGLE_MODE_LABEL = "19";

   @RBEntry("Stato operazione:")
   public static final String UPDN_OPERATION_STATUS_LABEL = "20";

   @RBEntry("Messaggio operazione:")
   public static final String UPDN_OPERATION_STATUS_MESSAGE_LABEL = "21";

   @RBEntry("Codice errore:")
   public static final String UPDN_FAILURE_CODE_LABEL = "22";

   @RBEntry("Codice risposta HTTP:")
   public static final String UPDN_HTTP_RESPONSE_CODE_LABEL = "23";

   @RBEntry("Messaggio HTTP:")
   public static final String UPDN_HTTP_RESPONSE_MESSAGE_LABEL = "24";

   /**
    * -----------------------------------------------------------
    **/
   @RBEntry("Non è stato ritrovato un file o un URL principale per questo oggetto.")
   public static final String NO_FILE_MESSAGE = "25";

   @RBEntry("Impossibile recuperare l'elemento (contenuto o file).  ")
   public static final String BAD_URL_MESSAGE = "26";

   @RBEntry("Lo scaricamento del tipo di contenuto non è supportato.")
   public static final String UNSUPPORTED_CONTENT_MESSAGE = "27";

   @RBEntry("Impossibile avviare l'applicazione a {0}.  ")
   @RBArgComment0("path of application")
   public static final String LAUNCH_APPLICATION = "28";

   @RBEntry("Non è stato ritrovato alcun file o URL associati all'oggetto da scaricare.")
   public static final String NO_CONTENT_MESSAGE = "29";

   /**
    * -------------------------------------------------
    **/
   @RBEntry("Carica")
   public static final String UPLOAD_CHECKBOX_LABEL = "30";

   @RBEntry("Rimuovi")
   public static final String DELETE_PRIMARY_CONTENT_LABEL = "31";

   /**
    * -------------------------------------------------
    **/
   @RBEntry("Errore durante lo scaricamento di {0} in {1}.")
   @RBArgComment0("filename")
   @RBArgComment1("full filepath")
   public static final String BAD_DOWNLOAD = "32";

   @RBEntry("Il file {0} è stato scaricato in {1}.")
   @RBArgComment0("filename")
   @RBArgComment1("full filepath")
   public static final String DOWNLOADED_TO = "33";

   @RBEntry("Nessun file scaricabile ritrovato.")
   public static final String NO_CONTENT = "34";

   @RBEntry("Problema nella ricerca del contenuto di {0}")
   @RBArgComment0("object identity")
   public static final String FINDING_CONTENT = "35";

   @RBEntry("Nessuna modifica da salvare nel file {0}. Caricamento non effettuato.")
   @RBArgComment0("name of the file")
   public static final String NO_CHANGES = "36";

   @RBEntry("Impossibile leggere il file {0} (oppure la sua dimensione è pari a 0). Scegliere un file leggibile e riprovare.")
   @RBArgComment0("name of the file")
   public static final String CANT_READ_FILE = "38";

   @RBEntry("Nessun file principale")
   public static final String NO_PRIMARY = "37";

   @RBEntry("Rimuovere il file principale")
   public static final String REMOVE_FILE_LABEL = "39";

   @RBEntry("Caricamento")
   public static final String UPLOAD_PROGRESS_LABEL = "40";

   @RBEntry("Inviato:")
   public static final String UPLOAD_PROGRESS_RECEIVED_LABEL = "41";

   @RBEntry("Impossibile scaricare {0} perché la posizione {1} corrisponde ad una directory di sola lettura o contiene un file di sola lettura con lo stesso nome. Selezionare una posizione modificabile per lo scaricamento.")
   @RBArgComment0("filename")
   @RBArgComment1("full filepath")
   public static final String CANT_WRITE = "42";

   @RBEntry("Impossibile scaricare {0} nella posizione {1}. Questo file potrebbe essere già aperto in una finestra diversa.")
   @RBArgComment0("filename")
   @RBArgComment1("full filepath")
   public static final String BAD_DOWNLOAD_FILE_OPEN = "43";

   @RBEntry("Impossibile seguire il collegamento selezionato.  Spostarsi direttamente nella cartella base per aprire e selezionare il file da caricare.")
   @RBComment("Message shown when user selects file or folder shortcut from file browse dialog, but JFileChooser can't navigate the shortcut.")
   public static final String CANT_FOLLOW_SHORTCUT = "44";

   @RBEntry("Impossibile caricare la directory selezionata come contenuto.  Selezionare un file da caricare.")
   @RBComment("Message shown when user multi-selects many files and one or more directories from file browse dialog.  Files are valid, directories are not.")
   public static final String CANT_SELECT_DIRECTORIES = "45";

   @RBEntry("Caricamento file {0} di {1}")
   @RBArgComment0("number of files uploaded, including this one")
   @RBArgComment1("total number of files to be uploaded")
   public static final String UPLOAD_PROGRESS_MESSAGE = "46";
}
