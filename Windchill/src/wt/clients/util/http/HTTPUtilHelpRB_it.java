/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.util.http;

import wt.util.resource.*;

@RBUUID("wt.clients.util.http.HTTPUtilHelpRB")
public final class HTTPUtilHelpRB_it extends WTListResourceBundle {
   @RBEntry("wt/clients/util/http/help_it/ConfirmFileReplaceHelp.html")
   public static final String PRIVATE_CONSTANT_0 = "Help/ConfirmFileReplaceDialog/ConfirmFileReplaceHelp";

   @RBEntry("wt/clients/util/http/help_it/ChooseFileOperationTypeHelp.html")
   public static final String PRIVATE_CONSTANT_1 = "Help/chooseFile";
}
