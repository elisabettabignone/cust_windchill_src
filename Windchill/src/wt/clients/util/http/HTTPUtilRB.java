/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.util.http;

import wt.util.resource.*;

@RBUUID("wt.clients.util.http.HTTPUtilRB")
public final class HTTPUtilRB extends WTListResourceBundle {
   /**
    * BUTTON LABELS ---------------------------------------------------------
    * Labels used on command buttons
    * 
    **/
   @RBEntry("OK")
   @RBComment("Labels used on command buttons")
   public static final String PRIVATE_CONSTANT_0 = "okButton";

   @RBEntry("Cancel")
   @RBComment("Labels used on command buttons")
   public static final String PRIVATE_CONSTANT_1 = "cancelButton";

   @RBEntry("Open File")
   @RBComment("Labels used on command buttons")
   public static final String PRIVATE_CONSTANT_2 = "openFileRadioButton";

   @RBEntry("Select a location to save the file")
   @RBComment("Labels used on command buttons")
   public static final String PRIVATE_CONSTANT_3 = "saveFileRadioButton";

   @RBEntry("Yes")
   @RBComment("Labels used on command buttons")
   public static final String PRIVATE_CONSTANT_4 = "yesButton";

   @RBEntry("No")
   @RBComment("Labels used on command buttons")
   public static final String PRIVATE_CONSTANT_5 = "noButton";

   /**
    * DIALOG MESSAGES ---------------------------------------------------------
    * Labels for explanation.
    * 
    **/
   @RBEntry("The following file is being downloaded:  {0}")
   @RBArgComment0("The file path and filename of the file to be downloaded.")
   public static final String PRIVATE_CONSTANT_6 = "chooseFileOperationMessage";

   @RBEntry("You have selected {0} files for download.  All files will be downloaded to the same location as the first file.  If you select Open File, only the first file will be opened, and all files will either be saved to your Default Local Directory (which can be set in your user preferences), or your Windows Temp or Unix Home directory.  If you select Save File to Disk, you will be prompted to select a local directory location where all files will be saved.")
   @RBComment("Clarifying expected behavior when downloading multiple files.  ")
   @RBArgComment0("Number of files to be downloaded.")
   public static final String PRIVATE_CONSTANT_7 = "multipleFileOperationMessage";

   @RBEntry("File {0} already exists.  Do you want to replace it?")
   @RBArgComment0("The file path and filename of the file to be downloaded.")
   public static final String PRIVATE_CONSTANT_8 = "confirmFileReplaceMessage";

   /**
    * DIALOG TITLES ------------------------------------------------------------
    * 
    **/
   @RBEntry("Choose File Operation")
   @RBComment("Titles displayed on dialogs.")
   public static final String PRIVATE_CONSTANT_9 = "chooseFileOperationTitle";

   @RBEntry("Confirm File Replace")
   @RBComment("Titles displayed on dialogs.")
   public static final String PRIVATE_CONSTANT_10 = "confirmFileReplaceTitle";

   /**
    * ------------------------------------------------
    **/
   @RBEntry("Downloading")
   public static final String DOWNLOAD_PROGRESS_LABEL = "0";

   @RBEntry("Total Size:")
   public static final String DOWNLOAD_PROGRESS_TOTAL_LABEL = "1";

   @RBEntry("Received:")
   public static final String DOWNLOAD_PROGRESS_RECEIVED_LABEL = "2";

   @RBEntry("Complete:")
   public static final String DOWNLOAD_PROGRESS_PERCENT_LABEL = "3";

   @RBEntry("Cancel")
   public static final String DOWNLOAD_PROGRESS_CANCEL_BUTTON = "4";

   @RBEntry("Progress")
   public static final String DOWNLOAD_PROGRESS_DIALOG_TITLE = "5";

   /**
    * ------------------------------------------------
    **/
   @RBEntry("Primary File")
   public static final String UPDN_FILE_CHOICE = "6";

   @RBEntry("Primary URL")
   public static final String UPDN_URL_CHOICE = "7";

   /**
    * ------------------------------------------------
    **/
   @RBEntry("Browse...")
   public static final String UPDN_BROWSE_LABEL = "8";

   @RBEntry("Upload")
   public static final String UPDN_UPLOAD_OPERATION_LABEL = "9";

   @RBEntry("Get")
   public static final String UPDN_DOWNLOAD_OPERATION_LABEL = "10";

   @RBEntry("Default File Name:")
   public static final String UPDN_DEFAULT_FILE_NAME_LABEL = "11";

   @RBEntry("Format:")
   public static final String UPDN_FILE_FORMAT_LABEL = "12";

   @RBEntry("Size:")
   public static final String UPDN_FILE_SIZE_LABEL = "13";

   @RBEntry("File Overwrite Warning")
   public static final String UPDN_FILE_OVERWRITE_WARNING_LABEL = "14";

   @RBEntry("File Change Prompt")
   public static final String UPDN_FILE_CHANGE_PROMPT_LABEL = "15";

   @RBEntry("Server Date:")
   public static final String UPDN_SERVER_DATE_LABEL = "16";

   @RBEntry("Server Size:")
   public static final String UPDN_SERVER_SIZE_LABEL = "17";

   @RBEntry("Server URL:")
   public static final String UPDN_SERVER_URL_LABEL = "18";

   @RBEntry("Toggle Mode")
   public static final String UPDN_TOGGLE_MODE_LABEL = "19";

   @RBEntry("Operation Status:")
   public static final String UPDN_OPERATION_STATUS_LABEL = "20";

   @RBEntry("Operation Message:")
   public static final String UPDN_OPERATION_STATUS_MESSAGE_LABEL = "21";

   @RBEntry("Failure Code:")
   public static final String UPDN_FAILURE_CODE_LABEL = "22";

   @RBEntry("HTTP Response Code:")
   public static final String UPDN_HTTP_RESPONSE_CODE_LABEL = "23";

   @RBEntry("HTTP Message:")
   public static final String UPDN_HTTP_RESPONSE_MESSAGE_LABEL = "24";

   /**
    * -----------------------------------------------------------
    **/
   @RBEntry("No primary file or URL found for this object.")
   public static final String NO_FILE_MESSAGE = "25";

   @RBEntry("Unable to retrieve content item (content or file).  ")
   public static final String BAD_URL_MESSAGE = "26";

   @RBEntry("Content type is not supported for download.")
   public static final String UNSUPPORTED_CONTENT_MESSAGE = "27";

   @RBEntry("Unable to launch application at {0}.  ")
   @RBArgComment0("path of application")
   public static final String LAUNCH_APPLICATION = "28";

   @RBEntry("No file or URL content found to download for this object.")
   public static final String NO_CONTENT_MESSAGE = "29";

   /**
    * -------------------------------------------------
    **/
   @RBEntry("Upload")
   public static final String UPLOAD_CHECKBOX_LABEL = "30";

   @RBEntry("Remove")
   public static final String DELETE_PRIMARY_CONTENT_LABEL = "31";

   /**
    * -------------------------------------------------
    **/
   @RBEntry("Error downloading {0} to {1}.")
   @RBArgComment0("filename")
   @RBArgComment1("full filepath")
   public static final String BAD_DOWNLOAD = "32";

   @RBEntry("File {0} downloaded to {1}.")
   @RBArgComment0("filename")
   @RBArgComment1("full filepath")
   public static final String DOWNLOADED_TO = "33";

   @RBEntry("No downloadable file found to download.")
   public static final String NO_CONTENT = "34";

   @RBEntry("Problem finding content for {0}")
   @RBArgComment0("object identity")
   public static final String FINDING_CONTENT = "35";

   @RBEntry("No changes to save for file {0}. Upload not performed.")
   @RBArgComment0("name of the file")
   public static final String NO_CHANGES = "36";

   @RBEntry("The file {0} cannot be read or is of 0 length. Please choose a readable file and try again.")
   @RBArgComment0("name of the file")
   public static final String CANT_READ_FILE = "38";

   @RBEntry("No Primary")
   public static final String NO_PRIMARY = "37";

   @RBEntry("Remove Primary")
   public static final String REMOVE_FILE_LABEL = "39";

   @RBEntry("Uploading")
   public static final String UPLOAD_PROGRESS_LABEL = "40";

   @RBEntry("Sent:")
   public static final String UPLOAD_PROGRESS_RECEIVED_LABEL = "41";

   @RBEntry("Unable to download {0} because the location {1} is a read-only directory or contains a read-only file of the same filename.  Please choose a writeable location for download.")
   @RBArgComment0("filename")
   @RBArgComment1("full filepath")
   public static final String CANT_WRITE = "42";

   @RBEntry("Unable to download {0} to location {1}.  This file may already be open in a different window.")
   @RBArgComment0("filename")
   @RBArgComment1("full filepath")
   public static final String BAD_DOWNLOAD_FILE_OPEN = "43";

   @RBEntry("The shortcut you selected cannot be followed.  Please navigate directly to the base folder to open and select the file to upload.")
   @RBComment("Message shown when user selects file or folder shortcut from file browse dialog, but JFileChooser can't navigate the shortcut.")
   public static final String CANT_FOLLOW_SHORTCUT = "44";

   @RBEntry("The directory you selected cannot be uploaded as content.  Please select a file for upload.")
   @RBComment("Message shown when user multi-selects many files and one or more directories from file browse dialog.  Files are valid, directories are not.")
   public static final String CANT_SELECT_DIRECTORIES = "45";

   @RBEntry("Uploading {0} of {1}")
   @RBArgComment0("number of files uploaded, including this one")
   @RBArgComment1("total number of files to be uploaded")
   public static final String UPLOAD_PROGRESS_MESSAGE = "46";
}
