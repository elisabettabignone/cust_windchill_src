/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.util;

import wt.util.resource.*;

@RBUUID("wt.clients.util.ParticipantSelectorHelpResources")
@RBNameException //Grandfathered by conversion
public final class ParticipantSelectorHelpResources extends WTListResourceBundle {
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_0 = "Desc/ParticipantSelector/ParticipantSelectorHelp";

   @RBEntry("Add the participant to the list of selected participants")
   public static final String PRIVATE_CONSTANT_1 = "Desc/ParticipantSelector/main/Add";

   @RBEntry("Add all the participant to the list of selected participants")
   public static final String PRIVATE_CONSTANT_2 = "Desc/ParticipantSelector/main/AddAll";

   @RBEntry("Cancel all changes and close this window")
   public static final String PRIVATE_CONSTANT_3 = "Desc/ParticipantSelector/main/Cancel";

   @RBEntry("Choose a group to show the users of")
   public static final String PRIVATE_CONSTANT_4 = "Desc/ParticipantSelector/main/Group";

   @RBEntry("The list of groups to select")
   public static final String PRIVATE_CONSTANT_5 = "Desc/ParticipantSelector/main/GroupList";

   @RBEntry("Help for this window")
   public static final String PRIVATE_CONSTANT_6 = "Desc/ParticipantSelector/main/Help";

   @RBEntry("Save all changes and close this window")
   public static final String PRIVATE_CONSTANT_7 = "Desc/ParticipantSelector/main/OK";

   @RBEntry("Remove the participant from the list of selected participants")
   public static final String PRIVATE_CONSTANT_8 = "Desc/ParticipantSelector/main/Remove";

   @RBEntry("Remove all the participant from the list of selected participants")
   public static final String PRIVATE_CONSTANT_9 = "Desc/ParticipantSelector/main/RemoveAll";

   @RBEntry("The list of roles to select")
   public static final String PRIVATE_CONSTANT_10 = "Desc/ParticipantSelector/main/RoleList";

   @RBEntry("Search for the specified user")
   public static final String PRIVATE_CONSTANT_11 = "Desc/ParticipantSelector/main/Search";

   @RBEntry("The list of selected participants")
   public static final String PRIVATE_CONSTANT_12 = "Desc/ParticipantSelector/main/SelectedList";

   @RBEntry("The list of users to select")
   public static final String PRIVATE_CONSTANT_13 = "Desc/ParticipantSelector/main/UserList";

   @RBEntry("User name to search for")
   public static final String PRIVATE_CONSTANT_14 = "Desc/ParticipantSelector/main/UserName";

   @RBEntry("TeamTemplateAdminAssignPtcpnt")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_15 = "Help/ParticipantSelector/ParticipantSelectorHelp";
}
