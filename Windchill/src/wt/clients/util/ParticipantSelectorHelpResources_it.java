/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.util;

import wt.util.resource.*;

@RBUUID("wt.clients.util.ParticipantSelectorHelpResources")
@RBNameException //Grandfathered by conversion
public final class ParticipantSelectorHelpResources_it extends WTListResourceBundle {
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_0 = "Desc/ParticipantSelector/ParticipantSelectorHelp";

   @RBEntry("Aggiunge il partecipante all'elenco dei partecipanti selezionati")
   public static final String PRIVATE_CONSTANT_1 = "Desc/ParticipantSelector/main/Add";

   @RBEntry("Aggiunge tutti i partecipanti all'elenco dei partecipanti selezionati")
   public static final String PRIVATE_CONSTANT_2 = "Desc/ParticipantSelector/main/AddAll";

   @RBEntry("Annulla tutte le modifiche e chiude la finestra")
   public static final String PRIVATE_CONSTANT_3 = "Desc/ParticipantSelector/main/Cancel";

   @RBEntry("Sceglie un gruppo del quale visualizzare gli utenti")
   public static final String PRIVATE_CONSTANT_4 = "Desc/ParticipantSelector/main/Group";

   @RBEntry("Elenco dei gruppi da selezionare")
   public static final String PRIVATE_CONSTANT_5 = "Desc/ParticipantSelector/main/GroupList";

   @RBEntry("Guida relativa alla finestra")
   public static final String PRIVATE_CONSTANT_6 = "Desc/ParticipantSelector/main/Help";

   @RBEntry("Salva tutte le modifiche e chiude la finestra")
   public static final String PRIVATE_CONSTANT_7 = "Desc/ParticipantSelector/main/OK";

   @RBEntry("Rimuove il partecipante dall'elenco dei partecipanti selezionati")
   public static final String PRIVATE_CONSTANT_8 = "Desc/ParticipantSelector/main/Remove";

   @RBEntry("Rimuove tutti i partecipanti dall'elenco dei partecipanti selezionati")
   public static final String PRIVATE_CONSTANT_9 = "Desc/ParticipantSelector/main/RemoveAll";

   @RBEntry("Elenco dei ruoli da selezionare")
   public static final String PRIVATE_CONSTANT_10 = "Desc/ParticipantSelector/main/RoleList";

   @RBEntry("Cerca l'utente specificato")
   public static final String PRIVATE_CONSTANT_11 = "Desc/ParticipantSelector/main/Search";

   @RBEntry("Elenco dei partecipanti selezionati")
   public static final String PRIVATE_CONSTANT_12 = "Desc/ParticipantSelector/main/SelectedList";

   @RBEntry("Elenco degli utenti da selezionare")
   public static final String PRIVATE_CONSTANT_13 = "Desc/ParticipantSelector/main/UserList";

   @RBEntry("Nome utente da ricercare")
   public static final String PRIVATE_CONSTANT_14 = "Desc/ParticipantSelector/main/UserName";

   @RBEntry("TeamTemplateAdminAssignPtcpnt")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_15 = "Help/ParticipantSelector/ParticipantSelectorHelp";
}
