/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.administrator;

import wt.util.resource.*;

@RBUUID("wt.clients.administrator.AdministratorHelpRB")
public final class AdministratorHelpRB_it extends WTListResourceBundle {
   @RBEntry("Chiude la finestra")
   public static final String PRIVATE_CONSTANT_0 = "Desc/Administrator//Close";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_1 = "Desc/Administrator/Domain";

   @RBEntry("Consente di definire una nuova regola di accesso. Premere F1 per visualizzare la Guida")
   public static final String PRIVATE_CONSTANT_2 = "Desc/Administrator/Domain/ACCreate";

   @RBEntry("Elimina la regola di accesso selezionata")
   public static final String PRIVATE_CONSTANT_3 = "Desc/Administrator/Domain/ACDelete";

   @RBEntry("Visualizza il browser delle regole di accesso generate (ACL)")
   public static final String PRIVATE_CONSTANT_4 = "Desc/Administrator/Domain/ACReport";

   @RBEntry("Recupera dal database tutte le regole di accesso per il dominio")
   public static final String PRIVATE_CONSTANT_5 = "Desc/Administrator/Domain/ACRetrieve";

   @RBEntry("Regole di accesso per il dominio. Premere F1 per visualizzare la Guida")
   public static final String PRIVATE_CONSTANT_6 = "Desc/Administrator/Domain/ACRules";

   @RBEntry("Modifica la regola di accesso selezionata")
   public static final String PRIVATE_CONSTANT_7 = "Desc/Administrator/Domain/ACUpdate";

   @RBEntry("Applica tutte le modifiche e lascia la finestra aperta")
   public static final String PRIVATE_CONSTANT_8 = "Desc/Administrator/Domain/Apply";

   @RBEntry("Annulla tutte le modifiche e chiude la finestra")
   public static final String PRIVATE_CONSTANT_9 = "Desc/Administrator/Domain/Cancel";

   @RBEntry("Consente di immettere una descrizione per il nuovo dominio (facoltativo)")
   public static final String PRIVATE_CONSTANT_10 = "Desc/Administrator/Domain/Description";

   @RBEntry("Consente di definire una nuova regola d'indicizzazione. Premere F1 per visualizzare la Guida")
   public static final String PRIVATE_CONSTANT_11 = "Desc/Administrator/Domain/IndexCreate";

   @RBEntry("Elimina la regola d'indicizzazione selezionata")
   public static final String PRIVATE_CONSTANT_12 = "Desc/Administrator/Domain/IndexDelete";

   @RBEntry("Visualizza il browser delle regole di indicizzazione")
   public static final String PRIVATE_CONSTANT_13 = "Desc/Administrator/Domain/IndexReport";

   @RBEntry("Recupera dal database tutte le regole d'indicizzazione per il dominio")
   public static final String PRIVATE_CONSTANT_14 = "Desc/Administrator/Domain/IndexRetrieve";

   @RBEntry("Regole d'indicizzazione per il dominio. Premere F1 per visualizzare la Guida")
   public static final String PRIVATE_CONSTANT_15 = "Desc/Administrator/Domain/IndexRules";

   @RBEntry("Modifica la regola d'indicizzazione selezionata")
   public static final String PRIVATE_CONSTANT_16 = "Desc/Administrator/Domain/IndexUpdate";

   @RBEntry("Consente di specificare un nome univoco per il nuovo dominio (obbligatorio)")
   public static final String PRIVATE_CONSTANT_17 = "Desc/Administrator/Domain/Name";

   @RBEntry("Consente di definire una nuova regola di notifica. Premere F1 per visualizzare la Guida")
   public static final String PRIVATE_CONSTANT_18 = "Desc/Administrator/Domain/NotifyCreate";

   @RBEntry("Elimina la regola di notifica selezionata")
   public static final String PRIVATE_CONSTANT_19 = "Desc/Administrator/Domain/NotifyDelete";

   @RBEntry("Visualizza il browser delle regole di notifica")
   public static final String PRIVATE_CONSTANT_20 = "Desc/Administrator/Domain/NotifyReport";

   @RBEntry("Recupera dal database tutte le regole di notifica per il dominio")
   public static final String PRIVATE_CONSTANT_21 = "Desc/Administrator/Domain/NotifyRetrieve";

   @RBEntry("Regole di notifica per il dominio. Premere F1 per visualizzare la Guida")
   public static final String PRIVATE_CONSTANT_22 = "Desc/Administrator/Domain/NotifyRules";

   @RBEntry("Modifica la regola di notifica selezionata")
   public static final String PRIVATE_CONSTANT_23 = "Desc/Administrator/Domain/NotifyUpdate";

   @RBEntry("Salva tutte le modifiche e chiude la finestra")
   public static final String PRIVATE_CONSTANT_24 = "Desc/Administrator/Domain/OK";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_25 = "Desc/Administrator/Group";

   @RBEntry("Aggiunge al gruppo l'utente selezionato")
   public static final String PRIVATE_CONSTANT_26 = "Desc/Administrator/Group/Add";

   @RBEntry("Aggiunge al gruppo tutti gli utenti elencati nel pannello Trovati")
   public static final String PRIVATE_CONSTANT_27 = "Desc/Administrator/Group/AddAll";

   @RBEntry("Applica tutte le modifiche e lascia la finestra aperta")
   public static final String PRIVATE_CONSTANT_28 = "Desc/Administrator/Group/Apply";

   @RBEntry("Annulla tutte le modifiche e chiude la finestra")
   public static final String PRIVATE_CONSTANT_29 = "Desc/Administrator/Group/Cancel";

   @RBEntry("Azzera tutti i campi tranne l'ID")
   public static final String PRIVATE_CONSTANT_30 = "Desc/Administrator/Group/Clear";

   @RBEntry("Consente di immettere una descrizione per il nuovo gruppo di utenti (facoltativo)")
   public static final String PRIVATE_CONSTANT_31 = "Desc/Administrator/Group/Description";

   @RBEntry("Utilizzare Cerca per completare l'elenco degli utenti. Fare clic su un elemento per selezionarlo")
   public static final String PRIVATE_CONSTANT_32 = "Desc/Administrator/Group/Found";

   @RBEntry("Consente di selezionare un gruppo dall'elenco a discesa per completare il riquadro Trovato")
   public static final String PRIVATE_CONSTANT_33 = "Desc/Administrator/Group/GroupList";

   @RBEntry("Consente di specificare un nome univoco per il nuovo gruppo di utenti (obbligatorio)")
   public static final String PRIVATE_CONSTANT_34 = "Desc/Administrator/Group/GroupName";

   @RBEntry("Membri attuali del gruppo")
   public static final String PRIVATE_CONSTANT_35 = "Desc/Administrator/Group/Members";

   @RBEntry("Salva tutte le modifiche e chiude la finestra")
   public static final String PRIVATE_CONSTANT_36 = "Desc/Administrator/Group/OK";

   @RBEntry("Rimuove dal gruppo l'utente selezionato")
   public static final String PRIVATE_CONSTANT_37 = "Desc/Administrator/Group/Remove";

   @RBEntry("Avvia la ricerca")
   public static final String PRIVATE_CONSTANT_38 = "Desc/Administrator/Group/Search";

   @RBEntry("Annulla l'ultima operazione")
   public static final String PRIVATE_CONSTANT_39 = "Desc/Administrator/Group/Undo";

   @RBEntry("Consente di specificare criteri di ricerca. Premere F1 per visualizzare la Guida")
   public static final String PRIVATE_CONSTANT_40 = "Desc/Administrator/Group/UserName";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_41 = "Desc/Administrator/GroupAdmin";

   @RBEntry("Definisci gli utenti che appartengono al gruppo selezionato")
   public static final String PRIVATE_CONSTANT_42 = "Desc/Administrator/GroupAdmin/AddRemove";

   @RBEntry("Chiude la finestra")
   public static final String PRIVATE_CONSTANT_43 = "Desc/Administrator/GroupAdmin/Close";

   @RBEntry("Crea un nuovo gruppo")
   public static final String PRIVATE_CONSTANT_44 = "Desc/Administrator/GroupAdmin/Create";

   @RBEntry("Consente di specificare criteri di ricerca. Premere F1 per visualizzare la Guida")
   public static final String PRIVATE_CONSTANT_45 = "Desc/Administrator/GroupAdmin/Criteria";

   @RBEntry("Elimina il gruppo selezionato")
   public static final String PRIVATE_CONSTANT_46 = "Desc/Administrator/GroupAdmin/Delete";

   @RBEntry("Elenco dei gruppi. Fare clic sul nome di un gruppo per selezionarlo")
   public static final String PRIVATE_CONSTANT_47 = "Desc/Administrator/GroupAdmin/GroupList";

   @RBEntry("Avvia la ricerca")
   public static final String PRIVATE_CONSTANT_48 = "Desc/Administrator/GroupAdmin/Search";

   @RBEntry("Modifica gli attributi del gruppo selezionato")
   public static final String PRIVATE_CONSTANT_49 = "Desc/Administrator/GroupAdmin/Update";

   @RBEntry("Esamina gli attributi per il gruppo selezionato")
   public static final String PRIVATE_CONSTANT_50 = "Desc/Administrator/GroupAdmin/View";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_51 = "Desc/Administrator/Indexing";

   @RBEntry("Aggiunge le raccolte di documenti selezionate all'elenco di indicizzazioni")
   public static final String PRIVATE_CONSTANT_52 = "Desc/Administrator/Indexing/Add";

   @RBEntry("Aggiunge tutte le raccolte di documenti disponibili all'elenco di indicizzazioni")
   public static final String PRIVATE_CONSTANT_53 = "Desc/Administrator/Indexing/AddAll";

   @RBEntry("Applica tutte le modifiche, cancella le selezioni e lascia aperta la finestra")
   public static final String PRIVATE_CONSTANT_54 = "Desc/Administrator/Indexing/Apply";

   @RBEntry("Elenco di raccolte di documenti fornito dalla Gestione indici. Fare clic sui nomi di raccolta da selezionare")
   public static final String PRIVATE_CONSTANT_55 = "Desc/Administrator/Indexing/Available";

   @RBEntry("Annulla tutte le modifiche e chiude la finestra")
   public static final String PRIVATE_CONSTANT_56 = "Desc/Administrator/Indexing/Cancel";

   @RBEntry("Classi di oggetti appartenenti al dominio. Fare clic su un nome di una classe per selezionarla")
   public static final String PRIVATE_CONSTANT_57 = "Desc/Administrator/Indexing/Classes";

   @RBEntry("Reimposta tutti i campi")
   public static final String PRIVATE_CONSTANT_58 = "Desc/Administrator/Indexing/Clear";

   @RBEntry("Elenco di eventi. Fare clic su uno o più eventi per selezionarli")
   public static final String PRIVATE_CONSTANT_59 = "Desc/Administrator/Indexing/Events";

   @RBEntry("Salva tutte le modifiche e chiude la finestra")
   public static final String PRIVATE_CONSTANT_60 = "Desc/Administrator/Indexing/OK";

   @RBEntry("Rimuove le raccolte di documenti selezionate dall'elenco di indicizzazioni")
   public static final String PRIVATE_CONSTANT_61 = "Desc/Administrator/Indexing/Remove";

   @RBEntry("Raccolte di documenti da aggiornare quando si verifica l'evento")
   public static final String PRIVATE_CONSTANT_62 = "Desc/Administrator/Indexing/Selected";

   @RBEntry("Annulla l'ultima operazione")
   public static final String PRIVATE_CONSTANT_63 = "Desc/Administrator/Indexing/Undo";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_64 = "Desc/Administrator/Notification";

   @RBEntry("Aggiunge all'elenco notifiche gli utenti e i gruppi selezionati")
   public static final String PRIVATE_CONSTANT_65 = "Desc/Administrator/Notification/Add";

   @RBEntry("Aggiunge all'elenco notifiche tutti gli utenti e i gruppi riportati nel pannello Trovato")
   public static final String PRIVATE_CONSTANT_66 = "Desc/Administrator/Notification/AddAll";

   @RBEntry("Applica tutte le modifiche, cancella le selezioni e lascia aperta la finestra")
   public static final String PRIVATE_CONSTANT_67 = "Desc/Administrator/Notification/Apply";

   @RBEntry("Annulla tutte le modifiche e chiude la finestra")
   public static final String PRIVATE_CONSTANT_68 = "Desc/Administrator/Notification/Cancel";

   @RBEntry("Classi di oggetti appartenenti al dominio. Fare clic su un nome di una classe per selezionarla")
   public static final String PRIVATE_CONSTANT_69 = "Desc/Administrator/Notification/Classes";

   @RBEntry("Reimposta tutti i campi")
   public static final String PRIVATE_CONSTANT_70 = "Desc/Administrator/Notification/Clear";

   @RBEntry("Elenco di eventi. Fare clic su uno o più eventi per selezionarli")
   public static final String PRIVATE_CONSTANT_71 = "Desc/Administrator/Notification/Events";

   @RBEntry("Utilizzare Cerca per completare l'elenco degli utenti e dei gruppi. Fare clic su un elemento per selezionarlo")
   public static final String PRIVATE_CONSTANT_72 = "Desc/Administrator/Notification/Found";

   @RBEntry("Consente di selezionare un gruppo dall'elenco a discesa per completare il riquadro Trovato")
   public static final String PRIVATE_CONSTANT_73 = "Desc/Administrator/Notification/GroupName";

   @RBEntry("Tutti gli utenti e i gruppi da notificare")
   public static final String PRIVATE_CONSTANT_74 = "Desc/Administrator/Notification/Notify";

   @RBEntry("Salva tutte le modifiche e chiude la finestra")
   public static final String PRIVATE_CONSTANT_75 = "Desc/Administrator/Notification/OK";

   @RBEntry("Rimuove dall'elenco notifiche gli utenti e i gruppi selezionati")
   public static final String PRIVATE_CONSTANT_76 = "Desc/Administrator/Notification/Remove";

   @RBEntry("Avvia la ricerca")
   public static final String PRIVATE_CONSTANT_77 = "Desc/Administrator/Notification/Search";

   @RBEntry("Stato del ciclo di vita")
   public static final String PRIVATE_CONSTANT_78 = "Desc/Administrator/Notification/State";

   @RBEntry("Annulla l'ultima operazione")
   public static final String PRIVATE_CONSTANT_79 = "Desc/Administrator/Notification/Undo";

   @RBEntry("Consente di specificare una stringa di ricerca. Gli utenti il cui nome o ID coincide con la stringa vengono elencati nel riquadro Trovato")
   public static final String PRIVATE_CONSTANT_80 = "Desc/Administrator/Notification/UserName";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_81 = "Desc/Administrator/Permission";

   @RBEntry("Premere F1 per ulteriori informazioni")
   public static final String PRIVATE_CONSTANT_82 = "Desc/Administrator/Permission/All";

   @RBEntry("Salva la regola corrente")
   public static final String PRIVATE_CONSTANT_83 = "Desc/Administrator/Permission/Apply";

   @RBEntry("Permesso di associazione. Premere F1 per ulteriori informazioni")
   public static final String PRIVATE_CONSTANT_84 = "Desc/Administrator/Permission/Attach";

   @RBEntry("Annulla tutte le modifiche e chiude la finestra")
   public static final String PRIVATE_CONSTANT_85 = "Desc/Administrator/Permission/Cancel";

   @RBEntry("Classi di oggetti appartenenti al dominio. Fare clic su un nome di una classe per selezionarla")
   public static final String PRIVATE_CONSTANT_86 = "Desc/Administrator/Permission/Class";

   @RBEntry("Reimposta tutti i campi")
   public static final String PRIVATE_CONSTANT_87 = "Desc/Administrator/Permission/Clear";

   @RBEntry("Chiude la finestra")
   public static final String PRIVATE_CONSTANT_88 = "Desc/Administrator/Permission/Close";

   @RBEntry("Permesso di creazione. Premere F1 per ulteriori informazioni")
   public static final String PRIVATE_CONSTANT_89 = "Desc/Administrator/Permission/Create";

   @RBEntry("Permesso di cancellazione. Premere F1 per ulteriori informazioni")
   public static final String PRIVATE_CONSTANT_90 = "Desc/Administrator/Permission/Delete";

   @RBEntry("Rifiuta esplicitamente il permesso selezionato")
   public static final String PRIVATE_CONSTANT_91 = "Desc/Administrator/Permission/Deny";

   @RBEntry("Tutti i gruppi utente. Fare clic sul nome di un gruppo per selezionarlo")
   public static final String PRIVATE_CONSTANT_92 = "Desc/Administrator/Permission/Groups";

   @RBEntry("Permesso di modifica. Premere F1 per ulteriori informazioni")
   public static final String PRIVATE_CONSTANT_93 = "Desc/Administrator/Permission/Modify";

   @RBEntry("Premere F1 per ulteriori informazioni")
   public static final String PRIVATE_CONSTANT_94 = "Desc/Administrator/Permission/Other";

   @RBEntry("Accorda il permesso selezionato (opzione predefinita)")
   public static final String PRIVATE_CONSTANT_95 = "Desc/Administrator/Permission/Permit";

   @RBEntry("Utente/gruppo per cui sono impostati i permessi. Premere F1 per visualizzare la Guida")
   public static final String PRIVATE_CONSTANT_96 = "Desc/Administrator/Permission/Principal";

   @RBEntry("Permesso di lettura. Premere F1 per ulteriori informazioni")
   public static final String PRIVATE_CONSTANT_97 = "Desc/Administrator/Permission/Read";

   @RBEntry("Permesso speciale. Premere F1 per ulteriori informazioni")
   public static final String PRIVATE_CONSTANT_98 = "Desc/Administrator/Permission/Special";

   @RBEntry("Stato del ciclo di vita")
   public static final String PRIVATE_CONSTANT_99 = "Desc/Administrator/Permission/State";

   @RBEntry("Permesso d'uso. Premere F1 per ulteriori informazioni")
   public static final String PRIVATE_CONSTANT_100 = "Desc/Administrator/Permission/Use";

   @RBEntry("Tutti gli utenti. Fare clic su un nome utente per selezionarlo")
   public static final String PRIVATE_CONSTANT_101 = "Desc/Administrator/Permission/Users";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_102 = "Desc/Administrator/User";

   @RBEntry("Aggiunge l'utente al gruppo selezionato")
   public static final String PRIVATE_CONSTANT_103 = "Desc/Administrator/User/Add";

   @RBEntry("Aggiunge il file selezionato per l'identificazione elettronica dell'utente")
   public static final String PRIVATE_CONSTANT_104 = "Desc/Administrator/User/AddFile";

   @RBEntry("Applica tutte le modifiche, cancella le selezioni e lascia aperta la finestra")
   public static final String PRIVATE_CONSTANT_105 = "Desc/Administrator/User/Apply";

   @RBEntry("ID utente per autenticazione server (obbligatorio)")
   public static final String PRIVATE_CONSTANT_106 = "Desc/Administrator/User/AuthorizationID";

   @RBEntry("Gruppi a cui appartiene l'utente. Fare clic sul nome di un gruppo per selezionarlo")
   public static final String PRIVATE_CONSTANT_107 = "Desc/Administrator/User/Belongs";

   @RBEntry("Annulla tutte le modifiche e chiude la finestra")
   public static final String PRIVATE_CONSTANT_108 = "Desc/Administrator/User/Cancel";

   @RBEntry("Reimposta tutti i campi")
   public static final String PRIVATE_CONSTANT_109 = "Desc/Administrator/User/Clear";

   @RBEntry("Indirizzo e-mail dell'utente")
   public static final String PRIVATE_CONSTANT_110 = "Desc/Administrator/User/Email";

   @RBEntry("Combinazione di cognome, nome e iniziale secondo nome dell'utente (obbligatorio)")
   public static final String PRIVATE_CONSTANT_111 = "Desc/Administrator/User/FullName";

   @RBEntry("Tutti i gruppi utente esistenti. Fare clic sul nome di un gruppo per selezionarlo")
   public static final String PRIVATE_CONSTANT_112 = "Desc/Administrator/User/Groups";

   @RBEntry("ID Windchill dell'utente (obbligatorio)")
   public static final String PRIVATE_CONSTANT_113 = "Desc/Administrator/User/ID";

   @RBEntry("Attributi dell'utente forniti dal server LDAP")
   public static final String PRIVATE_CONSTANT_114 = "Desc/Administrator/User/LDAP";

   @RBEntry("Codice lingua ISO specifico per la regione geografica o politica")
   public static final String PRIVATE_CONSTANT_115 = "Desc/Administrator/User/Locale";

   @RBEntry("Indirizzo postale dell'utente")
   public static final String PRIVATE_CONSTANT_116 = "Desc/Administrator/User/MailAddress";

   @RBEntry("Crea un nuovo gruppo")
   public static final String PRIVATE_CONSTANT_117 = "Desc/Administrator/User/NewGroup";

   @RBEntry("Salva tutte le modifiche e chiude la finestra")
   public static final String PRIVATE_CONSTANT_118 = "Desc/Administrator/User/OK";

   @RBEntry("Aggiorna l'elenco di tutti i gruppi")
   public static final String PRIVATE_CONSTANT_119 = "Desc/Administrator/User/Refresh";

   @RBEntry("Rimuove l'utente dal gruppo selezionato")
   public static final String PRIVATE_CONSTANT_120 = "Desc/Administrator/User/Remove";

   @RBEntry("Annulla l'ultima operazione")
   public static final String PRIVATE_CONSTANT_121 = "Desc/Administrator/User/Undo";

   @RBEntry("Visualizza l'identificazione elettronica")
   public static final String PRIVATE_CONSTANT_122 = "Desc/Administrator/User/View";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_123 = "Desc/Administrator/UserAdmin";

   @RBEntry("Aggiorna i gruppi a cui appartiene l'utente")
   public static final String PRIVATE_CONSTANT_124 = "Desc/Administrator/UserAdmin/AddToGroup";

   @RBEntry("Chiude la finestra")
   public static final String PRIVATE_CONSTANT_125 = "Desc/Administrator/UserAdmin/Close";

   @RBEntry("Crea un nuovo account utente")
   public static final String PRIVATE_CONSTANT_126 = "Desc/Administrator/UserAdmin/Create";

   @RBEntry("Consente di specificare criteri di ricerca. Premere F1 per ulteriori informazioni")
   public static final String PRIVATE_CONSTANT_127 = "Desc/Administrator/UserAdmin/Criteria";

   @RBEntry("Elimina l'elemento utente selezionato")
   public static final String PRIVATE_CONSTANT_128 = "Desc/Administrator/UserAdmin/Delete";

   @RBEntry("Salva con un nome diverso l'utente selezionato.")
   public static final String PRIVATE_CONSTANT_129 = "Desc/Administrator/UserAdmin/SaveAs";

   @RBEntry("Avvia la ricerca")
   public static final String PRIVATE_CONSTANT_130 = "Desc/Administrator/UserAdmin/Search";

   @RBEntry("Modifica gli attributi per l'utente selezionato")
   public static final String PRIVATE_CONSTANT_131 = "Desc/Administrator/UserAdmin/Update";

   @RBEntry("Utilizzare Cerca per completare l'elenco degli account utente. Fare clic su un elemento per selezionarlo")
   public static final String PRIVATE_CONSTANT_132 = "Desc/Administrator/UserAdmin/UserList";

   @RBEntry("Esamina gli attributi per l'utente selezionato")
   public static final String PRIVATE_CONSTANT_133 = "Desc/Administrator/UserAdmin/View";

   @RBEntry("WCAdminDomainPol")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_134 = "Help/Administrator/Domain";

   @RBEntry("WCAdminGroupCreate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_135 = "Help/Administrator/Group";

   @RBEntry("WCAdminGroupOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_136 = "Help/Administrator/GroupAdmin";

   @RBEntry("WCAdminIndexRuleCreate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_137 = "Help/Administrator/Indexing";

   @RBEntry("WCAdminNotifCreate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_138 = "Help/Administrator/Notification";

   @RBEntry("WCAdminACLCreate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_139 = "Help/Administrator/Permission";

   @RBEntry("WCAdminCreateUser")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_140 = "Help/Administrator/User";

   @RBEntry("WCAdminUserOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_141 = "Help/Administrator/UserAdmin";

   @RBEntry("ParticipantAdminUserElectSig")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_142 = "Help/Administrator/UserEID";

   @RBEntry("Definisci regole di controllo di accesso")
   public static final String PRIVATE_CONSTANT_143 = "Tip/Administrator//AccessAdmin";

   @RBEntry("Gestisci domini")
   public static final String PRIVATE_CONSTANT_144 = "Tip/Administrator//DomainAdmin";

   @RBEntry("Gestisci gruppi di utenti")
   public static final String PRIVATE_CONSTANT_145 = "Tip/Administrator//GroupAdmin";

   @RBEntry("Definisci regole d'indicizzazione")
   public static final String PRIVATE_CONSTANT_146 = "Tip/Administrator//IndexAdmin";

   @RBEntry("Definisci regole di notifica")
   public static final String PRIVATE_CONSTANT_147 = "Tip/Administrator//NotifyAdmin";

   @RBEntry("Gestisci account utenti")
   public static final String PRIVATE_CONSTANT_148 = "Tip/Administrator//UserAdmin";
}
