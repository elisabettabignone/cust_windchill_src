/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.administrator;

import wt.util.resource.*;

@RBUUID("wt.clients.administrator.AdministratorHelpRB")
public final class AdministratorHelpRB extends WTListResourceBundle {
   @RBEntry("Close this window")
   public static final String PRIVATE_CONSTANT_0 = "Desc/Administrator//Close";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_1 = "Desc/Administrator/Domain";

   @RBEntry("Define a new access rule; press F1 for Help")
   public static final String PRIVATE_CONSTANT_2 = "Desc/Administrator/Domain/ACCreate";

   @RBEntry("Delete the selected access rule")
   public static final String PRIVATE_CONSTANT_3 = "Desc/Administrator/Domain/ACDelete";

   @RBEntry("Display the generated ACLs")
   public static final String PRIVATE_CONSTANT_4 = "Desc/Administrator/Domain/ACReport";

   @RBEntry("Retrieve from database all access rules for this domain")
   public static final String PRIVATE_CONSTANT_5 = "Desc/Administrator/Domain/ACRetrieve";

   @RBEntry("Access rules for this domain; press F1 for Help")
   public static final String PRIVATE_CONSTANT_6 = "Desc/Administrator/Domain/ACRules";

   @RBEntry("Change the selected access rule")
   public static final String PRIVATE_CONSTANT_7 = "Desc/Administrator/Domain/ACUpdate";

   @RBEntry("Apply all changes and leave this window open")
   public static final String PRIVATE_CONSTANT_8 = "Desc/Administrator/Domain/Apply";

   @RBEntry("Cancel all changes and close this window")
   public static final String PRIVATE_CONSTANT_9 = "Desc/Administrator/Domain/Cancel";

   @RBEntry("Provide text description for new domain (Optional)")
   public static final String PRIVATE_CONSTANT_10 = "Desc/Administrator/Domain/Description";

   @RBEntry("Define a new index rule; press F1 for Help")
   public static final String PRIVATE_CONSTANT_11 = "Desc/Administrator/Domain/IndexCreate";

   @RBEntry("Delete the selected index rule")
   public static final String PRIVATE_CONSTANT_12 = "Desc/Administrator/Domain/IndexDelete";

   @RBEntry("Display the generated index lists")
   public static final String PRIVATE_CONSTANT_13 = "Desc/Administrator/Domain/IndexReport";

   @RBEntry("Retrieve from database all index rules for this domain")
   public static final String PRIVATE_CONSTANT_14 = "Desc/Administrator/Domain/IndexRetrieve";

   @RBEntry("Index rules for this domain; press F1 for Help")
   public static final String PRIVATE_CONSTANT_15 = "Desc/Administrator/Domain/IndexRules";

   @RBEntry("Change the selected index rule")
   public static final String PRIVATE_CONSTANT_16 = "Desc/Administrator/Domain/IndexUpdate";

   @RBEntry("Specify unique name for new domain (Required)")
   public static final String PRIVATE_CONSTANT_17 = "Desc/Administrator/Domain/Name";

   @RBEntry("Define a new notification rule; press F1 for Help")
   public static final String PRIVATE_CONSTANT_18 = "Desc/Administrator/Domain/NotifyCreate";

   @RBEntry("Delete the selected notification rule")
   public static final String PRIVATE_CONSTANT_19 = "Desc/Administrator/Domain/NotifyDelete";

   @RBEntry("Display the generated notification lists")
   public static final String PRIVATE_CONSTANT_20 = "Desc/Administrator/Domain/NotifyReport";

   @RBEntry("Retrieve from database all notification rules for this domain")
   public static final String PRIVATE_CONSTANT_21 = "Desc/Administrator/Domain/NotifyRetrieve";

   @RBEntry("Notification rules for this domain; press F1 for Help")
   public static final String PRIVATE_CONSTANT_22 = "Desc/Administrator/Domain/NotifyRules";

   @RBEntry("Change the selected notification rule")
   public static final String PRIVATE_CONSTANT_23 = "Desc/Administrator/Domain/NotifyUpdate";

   @RBEntry("Save all changes and close this window")
   public static final String PRIVATE_CONSTANT_24 = "Desc/Administrator/Domain/OK";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_25 = "Desc/Administrator/Group";

   @RBEntry("Enroll selected user in the group")
   public static final String PRIVATE_CONSTANT_26 = "Desc/Administrator/Group/Add";

   @RBEntry("Enroll all users in Found panel in the group")
   public static final String PRIVATE_CONSTANT_27 = "Desc/Administrator/Group/AddAll";

   @RBEntry("Apply all changes and leave this window open")
   public static final String PRIVATE_CONSTANT_28 = "Desc/Administrator/Group/Apply";

   @RBEntry("Cancel all changes and close this window")
   public static final String PRIVATE_CONSTANT_29 = "Desc/Administrator/Group/Cancel";

   @RBEntry("Clear all fields except ID")
   public static final String PRIVATE_CONSTANT_30 = "Desc/Administrator/Group/Clear";

   @RBEntry("Provide text description for new user group (Optional)")
   public static final String PRIVATE_CONSTANT_31 = "Desc/Administrator/Group/Description";

   @RBEntry("Use Search to populate list of users; click on an entry to select it")
   public static final String PRIVATE_CONSTANT_32 = "Desc/Administrator/Group/Found";

   @RBEntry("Choose a group from the drop-down list to populate Found panel")
   public static final String PRIVATE_CONSTANT_33 = "Desc/Administrator/Group/GroupList";

   @RBEntry("Specify unique name for new user group (Required)")
   public static final String PRIVATE_CONSTANT_34 = "Desc/Administrator/Group/GroupName";

   @RBEntry("Current members of the group")
   public static final String PRIVATE_CONSTANT_35 = "Desc/Administrator/Group/Members";

   @RBEntry("Save all changes and close this window")
   public static final String PRIVATE_CONSTANT_36 = "Desc/Administrator/Group/OK";

   @RBEntry("Remove selected user from the group")
   public static final String PRIVATE_CONSTANT_37 = "Desc/Administrator/Group/Remove";

   @RBEntry("Initiate search")
   public static final String PRIVATE_CONSTANT_38 = "Desc/Administrator/Group/Search";

   @RBEntry("Undo the last operation")
   public static final String PRIVATE_CONSTANT_39 = "Desc/Administrator/Group/Undo";

   @RBEntry("Specify search criteria; press F1 for Help")
   public static final String PRIVATE_CONSTANT_40 = "Desc/Administrator/Group/UserName";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_41 = "Desc/Administrator/GroupAdmin";

   @RBEntry("Change membership of selected group")
   public static final String PRIVATE_CONSTANT_42 = "Desc/Administrator/GroupAdmin/AddRemove";

   @RBEntry("Close this window")
   public static final String PRIVATE_CONSTANT_43 = "Desc/Administrator/GroupAdmin/Close";

   @RBEntry("Create a new user group")
   public static final String PRIVATE_CONSTANT_44 = "Desc/Administrator/GroupAdmin/Create";

   @RBEntry("Specify search criteria; press F1 for Help")
   public static final String PRIVATE_CONSTANT_45 = "Desc/Administrator/GroupAdmin/Criteria";

   @RBEntry("Delete the selected group")
   public static final String PRIVATE_CONSTANT_46 = "Desc/Administrator/GroupAdmin/Delete";

   @RBEntry("List of groups; click on a group name to select it.")
   public static final String PRIVATE_CONSTANT_47 = "Desc/Administrator/GroupAdmin/GroupList";

   @RBEntry("Initiate search")
   public static final String PRIVATE_CONSTANT_48 = "Desc/Administrator/GroupAdmin/Search";

   @RBEntry("Change attributes and membership for selected group")
   public static final String PRIVATE_CONSTANT_49 = "Desc/Administrator/GroupAdmin/Update";

   @RBEntry("Examine attributes for selected group")
   public static final String PRIVATE_CONSTANT_50 = "Desc/Administrator/GroupAdmin/View";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_51 = "Desc/Administrator/Indexing";

   @RBEntry("Add selected collections to the indexing list")
   public static final String PRIVATE_CONSTANT_52 = "Desc/Administrator/Indexing/Add";

   @RBEntry("Add all available collections to the indexing list")
   public static final String PRIVATE_CONSTANT_53 = "Desc/Administrator/Indexing/AddAll";

   @RBEntry("Save all changes, clear selections, and keep window open")
   public static final String PRIVATE_CONSTANT_54 = "Desc/Administrator/Indexing/Apply";

   @RBEntry("List of collections provided by Index Manager; click on collection names to select")
   public static final String PRIVATE_CONSTANT_55 = "Desc/Administrator/Indexing/Available";

   @RBEntry("Cancel all changes and close this window")
   public static final String PRIVATE_CONSTANT_56 = "Desc/Administrator/Indexing/Cancel";

   @RBEntry("Object classes within domain; click on a class name to select it")
   public static final String PRIVATE_CONSTANT_57 = "Desc/Administrator/Indexing/Classes";

   @RBEntry("Clear all selections")
   public static final String PRIVATE_CONSTANT_58 = "Desc/Administrator/Indexing/Clear";

   @RBEntry("List of events; click on one or more events to select them")
   public static final String PRIVATE_CONSTANT_59 = "Desc/Administrator/Indexing/Events";

   @RBEntry("Save all changes and close this window")
   public static final String PRIVATE_CONSTANT_60 = "Desc/Administrator/Indexing/OK";

   @RBEntry("Remove selected collections from the indexing list")
   public static final String PRIVATE_CONSTANT_61 = "Desc/Administrator/Indexing/Remove";

   @RBEntry("Collections to be updated when event occurs")
   public static final String PRIVATE_CONSTANT_62 = "Desc/Administrator/Indexing/Selected";

   @RBEntry("Undo the last operation")
   public static final String PRIVATE_CONSTANT_63 = "Desc/Administrator/Indexing/Undo";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_64 = "Desc/Administrator/Notification";

   @RBEntry("Add selected users and groups to notification list")
   public static final String PRIVATE_CONSTANT_65 = "Desc/Administrator/Notification/Add";

   @RBEntry("Add all users and groups in Found panel to notification list")
   public static final String PRIVATE_CONSTANT_66 = "Desc/Administrator/Notification/AddAll";

   @RBEntry("Save all changes, clear selections, and keep window open")
   public static final String PRIVATE_CONSTANT_67 = "Desc/Administrator/Notification/Apply";

   @RBEntry("Cancel all changes and close this window")
   public static final String PRIVATE_CONSTANT_68 = "Desc/Administrator/Notification/Cancel";

   @RBEntry("Object classes within domain; click on a class name to select it")
   public static final String PRIVATE_CONSTANT_69 = "Desc/Administrator/Notification/Classes";

   @RBEntry("Clear all selections")
   public static final String PRIVATE_CONSTANT_70 = "Desc/Administrator/Notification/Clear";

   @RBEntry("List of events; click on one or more events to select them")
   public static final String PRIVATE_CONSTANT_71 = "Desc/Administrator/Notification/Events";

   @RBEntry("Use Search to populate list of users and groups; click on an entry select it")
   public static final String PRIVATE_CONSTANT_72 = "Desc/Administrator/Notification/Found";

   @RBEntry("Choose a group from the drop-down list to populate Found pane")
   public static final String PRIVATE_CONSTANT_73 = "Desc/Administrator/Notification/GroupName";

   @RBEntry("All users and groups to be notified")
   public static final String PRIVATE_CONSTANT_74 = "Desc/Administrator/Notification/Notify";

   @RBEntry("Save all changes and close this window")
   public static final String PRIVATE_CONSTANT_75 = "Desc/Administrator/Notification/OK";

   @RBEntry("Remove selected users and groups from notification list")
   public static final String PRIVATE_CONSTANT_76 = "Desc/Administrator/Notification/Remove";

   @RBEntry("Initiate search")
   public static final String PRIVATE_CONSTANT_77 = "Desc/Administrator/Notification/Search";

   @RBEntry("Lifecycle state")
   public static final String PRIVATE_CONSTANT_78 = "Desc/Administrator/Notification/State";

   @RBEntry("Undo the last operation")
   public static final String PRIVATE_CONSTANT_79 = "Desc/Administrator/Notification/Undo";

   @RBEntry("Specify a search string; all users with matching name or ID will be returned in Found pane")
   public static final String PRIVATE_CONSTANT_80 = "Desc/Administrator/Notification/UserName";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_81 = "Desc/Administrator/Permission";

   @RBEntry("Press F1 for more information")
   public static final String PRIVATE_CONSTANT_82 = "Desc/Administrator/Permission/All";

   @RBEntry("Save the current rule")
   public static final String PRIVATE_CONSTANT_83 = "Desc/Administrator/Permission/Apply";

   @RBEntry("Attach permission; press F1 for more information")
   public static final String PRIVATE_CONSTANT_84 = "Desc/Administrator/Permission/Attach";

   @RBEntry("Cancel all changes and close this window")
   public static final String PRIVATE_CONSTANT_85 = "Desc/Administrator/Permission/Cancel";

   @RBEntry("Object classes within domain; click on a class name to select it")
   public static final String PRIVATE_CONSTANT_86 = "Desc/Administrator/Permission/Class";

   @RBEntry("Clear all selections")
   public static final String PRIVATE_CONSTANT_87 = "Desc/Administrator/Permission/Clear";

   @RBEntry("Close this window")
   public static final String PRIVATE_CONSTANT_88 = "Desc/Administrator/Permission/Close";

   @RBEntry("Create permission; press F1 for more information")
   public static final String PRIVATE_CONSTANT_89 = "Desc/Administrator/Permission/Create";

   @RBEntry("Delete permission; press F1 for more information")
   public static final String PRIVATE_CONSTANT_90 = "Desc/Administrator/Permission/Delete";

   @RBEntry("Explicitly deny the selected permission")
   public static final String PRIVATE_CONSTANT_91 = "Desc/Administrator/Permission/Deny";

   @RBEntry("All user groups; click on group name to select it.")
   public static final String PRIVATE_CONSTANT_92 = "Desc/Administrator/Permission/Groups";

   @RBEntry("Modify permission; press F1 for more information")
   public static final String PRIVATE_CONSTANT_93 = "Desc/Administrator/Permission/Modify";

   @RBEntry("Press F1 for more information")
   public static final String PRIVATE_CONSTANT_94 = "Desc/Administrator/Permission/Other";

   @RBEntry("Grant the selected permission (default)")
   public static final String PRIVATE_CONSTANT_95 = "Desc/Administrator/Permission/Permit";

   @RBEntry("User or group for whom permissions are set; press F1 for Help")
   public static final String PRIVATE_CONSTANT_96 = "Desc/Administrator/Permission/Principal";

   @RBEntry("Read permission; press F1 for more information")
   public static final String PRIVATE_CONSTANT_97 = "Desc/Administrator/Permission/Read";

   @RBEntry("Special permission; press F1 for more information")
   public static final String PRIVATE_CONSTANT_98 = "Desc/Administrator/Permission/Special";

   @RBEntry("Lifecycle state")
   public static final String PRIVATE_CONSTANT_99 = "Desc/Administrator/Permission/State";

   @RBEntry("Use permission; press F1 for more information")
   public static final String PRIVATE_CONSTANT_100 = "Desc/Administrator/Permission/Use";

   @RBEntry("All users; click on user name to select it.")
   public static final String PRIVATE_CONSTANT_101 = "Desc/Administrator/Permission/Users";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_102 = "Desc/Administrator/User";

   @RBEntry("Enroll user in selected group")
   public static final String PRIVATE_CONSTANT_103 = "Desc/Administrator/User/Add";

   @RBEntry("Add selected file for user's Electronic Identification")
   public static final String PRIVATE_CONSTANT_104 = "Desc/Administrator/User/AddFile";

   @RBEntry("Apply all changes, clear selections, and leave this window open")
   public static final String PRIVATE_CONSTANT_105 = "Desc/Administrator/User/Apply";

   @RBEntry("User's ID for server authentication (Required)")
   public static final String PRIVATE_CONSTANT_106 = "Desc/Administrator/User/AuthorizationID";

   @RBEntry("Groups to which user belongs; click on group name to select it")
   public static final String PRIVATE_CONSTANT_107 = "Desc/Administrator/User/Belongs";

   @RBEntry("Cancel all changes and close this window")
   public static final String PRIVATE_CONSTANT_108 = "Desc/Administrator/User/Cancel";

   @RBEntry("Clear all selections")
   public static final String PRIVATE_CONSTANT_109 = "Desc/Administrator/User/Clear";

   @RBEntry("User's email address")
   public static final String PRIVATE_CONSTANT_110 = "Desc/Administrator/User/Email";

   @RBEntry("Some combination of user's last name, first name, and middle initial (Required)")
   public static final String PRIVATE_CONSTANT_111 = "Desc/Administrator/User/FullName";

   @RBEntry("All existing user groups; click on group name to select it")
   public static final String PRIVATE_CONSTANT_112 = "Desc/Administrator/User/Groups";

   @RBEntry("User's Windchill ID (Required)")
   public static final String PRIVATE_CONSTANT_113 = "Desc/Administrator/User/ID";

   @RBEntry("User's attributes to be supplied from LDAP server")
   public static final String PRIVATE_CONSTANT_114 = "Desc/Administrator/User/LDAP";

   @RBEntry("ISO language code for this geographic or political region")
   public static final String PRIVATE_CONSTANT_115 = "Desc/Administrator/User/Locale";

   @RBEntry("User's mailing address")
   public static final String PRIVATE_CONSTANT_116 = "Desc/Administrator/User/MailAddress";

   @RBEntry("Create a new user group")
   public static final String PRIVATE_CONSTANT_117 = "Desc/Administrator/User/NewGroup";

   @RBEntry("Save all changes and close this window")
   public static final String PRIVATE_CONSTANT_118 = "Desc/Administrator/User/OK";

   @RBEntry("Update the list of all groups")
   public static final String PRIVATE_CONSTANT_119 = "Desc/Administrator/User/Refresh";

   @RBEntry("Remove user from selected group")
   public static final String PRIVATE_CONSTANT_120 = "Desc/Administrator/User/Remove";

   @RBEntry("Undo the last operation")
   public static final String PRIVATE_CONSTANT_121 = "Desc/Administrator/User/Undo";

   @RBEntry("View the Electronic Identification")
   public static final String PRIVATE_CONSTANT_122 = "Desc/Administrator/User/View";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_123 = "Desc/Administrator/UserAdmin";

   @RBEntry("Update group membership for selected user")
   public static final String PRIVATE_CONSTANT_124 = "Desc/Administrator/UserAdmin/AddToGroup";

   @RBEntry("Close this window")
   public static final String PRIVATE_CONSTANT_125 = "Desc/Administrator/UserAdmin/Close";

   @RBEntry("Create a new user account")
   public static final String PRIVATE_CONSTANT_126 = "Desc/Administrator/UserAdmin/Create";

   @RBEntry("Specify search criteria; press F1 for more information")
   public static final String PRIVATE_CONSTANT_127 = "Desc/Administrator/UserAdmin/Criteria";

   @RBEntry("Delete selected user entry")
   public static final String PRIVATE_CONSTANT_128 = "Desc/Administrator/UserAdmin/Delete";

   @RBEntry("Save the selected user entry as a new user with a different name")
   public static final String PRIVATE_CONSTANT_129 = "Desc/Administrator/UserAdmin/SaveAs";

   @RBEntry("Initiate search")
   public static final String PRIVATE_CONSTANT_130 = "Desc/Administrator/UserAdmin/Search";

   @RBEntry("Change attributes for selected user")
   public static final String PRIVATE_CONSTANT_131 = "Desc/Administrator/UserAdmin/Update";

   @RBEntry("Use Search to populate list of user accounts; click on entry to select it")
   public static final String PRIVATE_CONSTANT_132 = "Desc/Administrator/UserAdmin/UserList";

   @RBEntry("Examine attributes for selected user")
   public static final String PRIVATE_CONSTANT_133 = "Desc/Administrator/UserAdmin/View";

   @RBEntry("WCAdminDomainPol")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_134 = "Help/Administrator/Domain";

   @RBEntry("WCAdminGroupCreate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_135 = "Help/Administrator/Group";

   @RBEntry("WCAdminGroupOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_136 = "Help/Administrator/GroupAdmin";

   @RBEntry("WCAdminIndexRuleCreate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_137 = "Help/Administrator/Indexing";

   @RBEntry("WCAdminNotifCreate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_138 = "Help/Administrator/Notification";

   @RBEntry("WCAdminACLCreate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_139 = "Help/Administrator/Permission";

   @RBEntry("WCAdminCreateUser")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_140 = "Help/Administrator/User";

   @RBEntry("WCAdminUserOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_141 = "Help/Administrator/UserAdmin";

   @RBEntry("ParticipantAdminUserElectSig")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_142 = "Help/Administrator/UserEID";

   @RBEntry("Define access control policy")
   public static final String PRIVATE_CONSTANT_143 = "Tip/Administrator//AccessAdmin";

   @RBEntry("Administer domains")
   public static final String PRIVATE_CONSTANT_144 = "Tip/Administrator//DomainAdmin";

   @RBEntry("Administer user groups")
   public static final String PRIVATE_CONSTANT_145 = "Tip/Administrator//GroupAdmin";

   @RBEntry("Define indexing policy")
   public static final String PRIVATE_CONSTANT_146 = "Tip/Administrator//IndexAdmin";

   @RBEntry("Define notification policy")
   public static final String PRIVATE_CONSTANT_147 = "Tip/Administrator//NotifyAdmin";

   @RBEntry("Administer user accounts")
   public static final String PRIVATE_CONSTANT_148 = "Tip/Administrator//UserAdmin";
}
