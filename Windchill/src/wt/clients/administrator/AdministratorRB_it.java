/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.administrator;

import wt.util.resource.*;

@RBUUID("wt.clients.administrator.AdministratorRB")
public final class AdministratorRB_it extends WTListResourceBundle {
   /**
    * BUTTON LABELS
    **/
   @RBEntry("Applica")
   public static final String PRIVATE_CONSTANT_0 = "btnApply";

   @RBEntry("Annulla")
   public static final String PRIVATE_CONSTANT_1 = "btnCancel";

   @RBEntry("Chiudi")
   public static final String PRIVATE_CONSTANT_2 = "btnClose";

   @RBEntry("OK")
   public static final String PRIVATE_CONSTANT_3 = "btnOK";

   @RBEntry("Visualizza")
   public static final String PRIVATE_CONSTANT_4 = "btnView";

   @RBEntry("Rimuovi")
   public static final String PRIVATE_CONSTANT_5 = "btnRemove";

   /**
    * ERROR MESSAGES AND STATUS
    **/
   @RBEntry("Si è verificata un'eccezione durante l'inizializzazione dei dati:")
   @RBComment("Exception: there was a problem that occurred during initialization")
   public static final String INITIALIZATION_FAILED = "2";

   @RBEntry("Impossibile inizializzare la Guida in linea:")
   @RBComment("Exception: Help system initialization failed.")
   public static final String HELP_INITIALIZATION_FAILED = "4";

   @RBEntry("Impossibile rimuovere gli applet listeners:")
   @RBComment("Exception: there was a problem removing one or more of the applet listeners")
   public static final String REMOVING_LISTENER_FAILED = "7";

   @RBEntry("Nome file")
   @RBComment("MultiList Column Heading used in the Electronic Signature Applet: File Name")
   public static final String FILE_NAME = "43";

   @RBEntry("Data di aggiunta")
   @RBComment("MultiList Column Heading used in the Electronic Signature Applet: Date Added")
   public static final String DATE_ADDED = "44";

   @RBEntry("Si è verificato un errore durante il salvataggio del contenuto di {0}.")
   @RBComment("Exception: there was an error while saving the specified Electronic Signature file")
   @RBArgComment0("Display identity of the object for which there was an error saving its contents")
   public static final String CONTENT_NOT_UPLOADED = "46";

   @RBEntry("Seleziona dominio amministrativo")
   @RBComment("Title of the Domain Browser dialog for selecting domains")
   public static final String SELECT_DOMAIN_TITLE = "53";

   @RBEntry("Selezionare un file per l'applicazione della firma elettronica")
   @RBComment("Prompt message when no file is selected in the Electronic Signature Applet")
   public static final String NO_FILE_SELECTED = "99";
}
