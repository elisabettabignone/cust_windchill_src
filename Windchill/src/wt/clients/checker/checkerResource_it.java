/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.checker;

import wt.util.resource.*;

@RBUUID("wt.clients.checker.checkerResource")
public final class checkerResource_it extends WTListResourceBundle {
   @RBEntry("Errore durante la lettura delle proprietà del server: impossibile trovare wt/clients/checker/checker.properties")
   @RBComment("Error message displayed when the checker.properties file could not be found on the server.")
   public static final String SRVR_PROPS_ERR_MSG = "0";

   @RBEntry("Dimensione cache corrente: {0}. Per migliori prestazioni, si consiglia di aumentare la dimensione a {1}. Digitare '0' per una cache di dimensioni illimitate.")
   @RBComment("The message displayed when the plugin's cache size is set to small.  It warns that the cache is to small and recommends a new cache size.")
   @RBArgComment0("The current cache size, eg \"50m\"")
   @RBArgComment1("The recommended cache size, eg \"100m\"")
   public static final String CACHE_TOO_SMALL_MSG = "1";

   @RBEntry("\"{0}\" è un valore non valido.")
   @RBComment("Message displayed when a user inputs an invalid entry.")
   @RBArgComment0("The invalid entry string")
   public static final String INVALID_ENTRY = "2";

   @RBEntry("Installare pacchetto per connessione RMI PTC per il supporto di connessioni tramite firewall?")
   @RBComment("message requestion install permission of boot.jar")
   public static final String RMI_CONNECT_INSTALL_MSG = "3";

   @RBEntry("Pacchetto per connessione RMI PTC")
   @RBComment("header message")
   public static final String RMI_CONNECT_INSTALL_HDR = "4";

   @RBEntry("Installare il pacchetto PTC più recente per connessione RMI per il supporto di connessioni tramite firewall?")
   @RBComment("message requestion install permission of new boot.jar")
   public static final String NEWER_RMI_CONNECT_INSTALL_MSG = "10";

   @RBEntry("Parametro {0} mancante")
   @RBComment("message given when applet parameter {0} was not specified")
   public static final String MISSING_PARAMETER_EXCEPTION = "11";

   @RBEntry("Installazione o aggiornamento del pacchetto di connessione RMI PTC")
   public static final String INSTALL_OR_UPDATE_REASON = "14";

   @RBEntry("Installazione del pacchetto di connessione RMI PTC")
   public static final String INSTALL_REASON = "15";

   @RBEntry("Aggiornamento del pacchetto di connessione RMI PTC")
   public static final String UPDATE_REASON = "16";

   @RBEntry("Configurazione socket factory RMI")
   public static final String SET_RMI_SOCKET_FACTORY_REASON = "17";
}
