/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.prefs;

import wt.util.resource.*;

@RBUUID("wt.clients.prefs.PrefsRB")
public final class PrefsRB extends WTListResourceBundle {
   @RBEntry("Apply")
   public static final String PRIVATE_CONSTANT_0 = "applyButton";

   @RBEntry("Ascending")
   public static final String PRIVATE_CONSTANT_1 = "ascendingChoice";

   @RBEntry("Browse...")
   @RBComment("should be whatever the HTML file input button label is for this locale.")
   public static final String PRIVATE_CONSTANT_2 = "browseButton";

   @RBEntry("Cancel")
   public static final String PRIVATE_CONSTANT_3 = "cancelButton";

   @RBEntry("Close")
   @RBComment("closes the current window")
   public static final String PRIVATE_CONSTANT_4 = "closeButton";

   @RBEntry("Current Filepath:")
   public static final String PRIVATE_CONSTANT_5 = "currentFilepathLabel";

   @RBEntry("Descending")
   public static final String PRIVATE_CONSTANT_6 = "descendingChoice";

   @RBEntry("Select the checkbox to turn this preference on.  Deselect the checkbox to turn it off.")
   public static final String PRIVATE_CONSTANT_7 = "embeddedHelpBoolean";

   @RBEntry("Choose an option by selecting a radio button to the left of the appropriate choice.")
   public static final String PRIVATE_CONSTANT_8 = "embeddedHelpChoice";

   @RBEntry("Click the Browse button to navigate to the appropriate directory.  Set the preference by selecting any file within your chosen directory.")
   @RBComment("\"Browse\" should be whatever the HTML file input button label is for this locale.")
   public static final String PRIVATE_CONSTANT_9 = "embeddedHelpDirectory";

   @RBEntry("Click the Browse button to navigate to the appropriate directory.  Set the preference by selecting your chosen file.")
   @RBComment("\"Browse\" should be whatever the HTML file input button label is for this locale.")
   public static final String PRIVATE_CONSTANT_10 = "embeddedHelpFile";

   @RBEntry("Use the drop-down lists to specify the sort order for your data.  Select either Ascending or Descending for each grouping.")
   @RBComment("\"Ascending\" and \"Descending\" need to match ascendingChoice and descendingChoice values.")
   public static final String PRIVATE_CONSTANT_11 = "embeddedHelpSortOrder";

   @RBEntry("Type a value into the text field.")
   public static final String PRIVATE_CONSTANT_12 = "embeddedHelpString";

   @RBEntry("Invalid value.  Must enter a numeric integer value greater than zero.")
   public static final String PRIVATE_CONSTANT_13 = "errorIntegerRequired";

   @RBEntry("New Filepath:")
   public static final String PRIVATE_CONSTANT_14 = "newFilepathLabel";

   @RBEntry("OK")
   public static final String PRIVATE_CONSTANT_15 = "okButton";

   @RBEntry("Edit User Preferences")
   public static final String PRIVATE_CONSTANT_16 = "prefsEditorTitle";

   @RBEntry("Related Preferences:")
   public static final String PRIVATE_CONSTANT_17 = "relatedPreferencesLabel";

   @RBEntry("Sort By:")
   @RBComment("this is the first level of sorting when setting sort order (see thenByLabel)")
   public static final String PRIVATE_CONSTANT_18 = "sortByLabel";

   @RBEntry("Then By:")
   @RBComment("this is a subsequent level of sorting when setting sort order - the first level says \"Sort by:\" and all the other levels say \"Then by:\" (see sortByLabel)")
   public static final String PRIVATE_CONSTANT_19 = "thenByLabel";

   @RBEntry("Use System Default")
   @RBComment("telling the system to clear the user-specific preference value and just use the system's default value instead")
   public static final String PRIVATE_CONSTANT_20 = "useSystemDefaultButton";
}
