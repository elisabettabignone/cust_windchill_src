/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.prefs;

import wt.util.resource.*;

@RBUUID("wt.clients.prefs.PrefsRB")
public final class PrefsRB_it extends WTListResourceBundle {
   @RBEntry("Applica")
   public static final String PRIVATE_CONSTANT_0 = "applyButton";

   @RBEntry("Crescente")
   public static final String PRIVATE_CONSTANT_1 = "ascendingChoice";

   @RBEntry("Sfoglia...")
   @RBComment("should be whatever the HTML file input button label is for this locale.")
   public static final String PRIVATE_CONSTANT_2 = "browseButton";

   @RBEntry("Annulla")
   public static final String PRIVATE_CONSTANT_3 = "cancelButton";

   @RBEntry("Chiudi")
   @RBComment("closes the current window")
   public static final String PRIVATE_CONSTANT_4 = "closeButton";

   @RBEntry("Percorso file attuale:")
   public static final String PRIVATE_CONSTANT_5 = "currentFilepathLabel";

   @RBEntry("Decrescente")
   public static final String PRIVATE_CONSTANT_6 = "descendingChoice";

   @RBEntry("Selezionare la casella di controllo per attivare questa preferenza. Deselezionarla per disattivare la preferenza.")
   public static final String PRIVATE_CONSTANT_7 = "embeddedHelpBoolean";

   @RBEntry("Scegliere un'opzione selezionando il pulsante a sinistra della scelta appropriata. ")
   public static final String PRIVATE_CONSTANT_8 = "embeddedHelpChoice";

   @RBEntry("Fare clic sul pulsante Sfoglia per raggiungere la directory desiderata. Impostare la preferenza selezionando il file nella directory selezionata.")
   @RBComment("\"Browse\" should be whatever the HTML file input button label is for this locale.")
   public static final String PRIVATE_CONSTANT_9 = "embeddedHelpDirectory";

   @RBEntry("Fare clic sul pulsante Sfoglia per raggiungere la directory desiderata. Impostare la preferenza selezionando il file.")
   @RBComment("\"Browse\" should be whatever the HTML file input button label is for this locale.")
   public static final String PRIVATE_CONSTANT_10 = "embeddedHelpFile";

   @RBEntry("Utilizzare gli elenchi a discesa per specificare il tipo di organizzazione dei dati. Selezionare Crescente o Decrescente per ogni gruppo di dati.")
   @RBComment("\"Ascending\" and \"Descending\" need to match ascendingChoice and descendingChoice values.")
   public static final String PRIVATE_CONSTANT_11 = "embeddedHelpSortOrder";

   @RBEntry("Immettere un valore nel campo di testo.")
   public static final String PRIVATE_CONSTANT_12 = "embeddedHelpString";

   @RBEntry("Valore non valido. Immettere un valore numerico intero maggiore di zero.")
   public static final String PRIVATE_CONSTANT_13 = "errorIntegerRequired";

   @RBEntry("Nuovo percorso file:")
   public static final String PRIVATE_CONSTANT_14 = "newFilepathLabel";

   @RBEntry("OK")
   public static final String PRIVATE_CONSTANT_15 = "okButton";

   @RBEntry("Modifica preferenze utente")
   public static final String PRIVATE_CONSTANT_16 = "prefsEditorTitle";

   @RBEntry("Preferenze collegate")
   public static final String PRIVATE_CONSTANT_17 = "relatedPreferencesLabel";

   @RBEntry("Ordina per:")
   @RBComment("this is the first level of sorting when setting sort order (see thenByLabel)")
   public static final String PRIVATE_CONSTANT_18 = "sortByLabel";

   @RBEntry("E poi per:")
   @RBComment("this is a subsequent level of sorting when setting sort order - the first level says \"Sort by:\" and all the other levels say \"Then by:\" (see sortByLabel)")
   public static final String PRIVATE_CONSTANT_19 = "thenByLabel";

   @RBEntry("Usa il default di sistema")
   @RBComment("telling the system to clear the user-specific preference value and just use the system's default value instead")
   public static final String PRIVATE_CONSTANT_20 = "useSystemDefaultButton";
}
