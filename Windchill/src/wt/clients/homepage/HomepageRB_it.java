/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.homepage;

import wt.util.resource.*;

@RBUUID("wt.clients.homepage.HomepageRB")
public final class HomepageRB_it extends WTListResourceBundle {
   /**
    * 
    * Link Display Text.
    * Below are the entries for the links displayed on the Setup page
    * Each entry has a corresponding "ALT" entry which is used to
    * set the "ALT" tag for the image associated with the entry
    * Please DO NOT modify the constant name for the alt tags - the
    * code assumes <link key>_ALT_TAG
    * 
    **/
   @RBEntry("Impostazioni di protezione")
   public static final String SECURITY_SETTINGS = "SECURITY_SETTINGS";

   @RBEntry("Vai all'Editor delle impostazioni di protezione")
   public static final String SECURITY_SETTINGS_ALT_TAG = "SECURITY_SETTINGS_ALT_TAG";

   @RBEntry("Installazione caricatore bootstrap")
   public static final String BOOTSTRAP_INSTALL = "BOOTSTRAP_INSTALL";

   @RBEntry("Vai all'Installazione del caricatore bootstrap")
   public static final String BOOTSTRAP_INSTALL_ALT_TAG = "BOOTSTRAP_INSTALL_ALT_TAG";

   @RBEntry("InterComm")
   public static final String INTERCOMM_INSTALL = "INTERCOMM_INSTALL";

   @RBEntry("Vai all'installazione di InterComm")
   public static final String INTERCOMM_INSTALL_ALT_TAG = "INTERCOMM_INSTALL_ALT_TAG";

   @RBEntry("Desktop Integration")
   public static final String DESKTOP_INTEGRATION = "DESKTOP_INTEGRATION";

   @RBEntry("Vai a Desktop Integration")
   public static final String DESKTOP_INTEGRATION_ALT_TAG = "DESKTOP_INTEGRATION_ALT_TAG";

   @RBEntry("Installazione browser Internet Explorer")
   public static final String IE_BROWSER_SETUP = "IE_BROWSER_SETUP";

   @RBEntry("Vai all'Installazione del browser Internet Explorer")
   public static final String IE_BROWSER_SETUP_ALT_TAG = "IE_BROWSER_SETUP_ALT_TAG";

   @RBEntry("Visualizzatore browser processi")
   public static final String PROCESS_BROWSER_VIEWER = "PROCESS_BROWSER_VIEWER";

   @RBEntry("Vai al Visualizzatore del browser processi")
   public static final String PROCESS_BROWSER_VIEWER_ALT_TAG = "PROCESS_BROWSER_VIEWER_ALT_TAG";

   @RBEntry("Workgroup Manager per AutoCAD")
   public static final String WGM_AUTOCAD = "WGM_AUTOCAD";

   @RBEntry("Vai al Workgroup Manager per AutoCAD")
   public static final String WGM_AUTOCAD_ALT_TAG = "WGM_AUTOCAD_ALT_TAG";

   @RBEntry("Workgroup Manager per CADDS")
   public static final String WGM_CADDS = "WGM_CADDS";

   @RBEntry("Vai al Workgroup Manager per CADDS")
   public static final String WGM_CADDS_ALT_TAG = "WGM_CADDS_ALT_TAG";

   @RBEntry("Workgroup Manager per Cadence")
   public static final String WGM_CADENCE = "WGM_CADENCE";

   @RBEntry("Vai al Workgroup Manager per Cadence")
   public static final String WGM_CADENCE_ALT_TAG = "WGM_CADENCE_ALT_TAG";

   @RBEntry("Workgroup Manager per CATIA V4")
   public static final String WGM_CATIA = "WGM_CATIA";

   @RBEntry("Vai al Workgroup Manager per CATIA")
   public static final String WGM_CATIA_ALT_TAG = "WGM_CATIA_ALT_TAG";

   @RBEntry("Workgroup Manager per CATIA V5")
   public static final String WGM_CATIA5 = "WGM_CATIA5";

   @RBEntry("Workgroup Manager per CATIA V5")
   public static final String WGM_CATIA5_ALT_TAG = "WGM_CATIA5_ALT_TAG";

   @RBEntry("Workgroup Manager per ECAD")
   public static final String WGM_ECAD = "WGM_ECAD";

   @RBEntry("Vai al Workgroup Manager per ECAD")
   public static final String WGM_ECAD_ALT_TAG = "WGM_ECAD_ALT_TAG";

   @RBEntry("Workgroup Manager per I-DEAS")
   public static final String WGM_IDEAS = "WGM_IDEAS";

   @RBEntry("Vai al Workgroup Manager per I-DEAS")
   public static final String WGM_IDEAS_ALT_TAG = "WGM_IDEAS_ALT_TAG";

   @RBEntry("Workgroup Manager per Autodesk Inventor")
   public static final String WGM_INVENTOR = "WGM_INVENTOR";

   @RBEntry("Workgroup Manager per Autodesk Inventor")
   public static final String WGM_INVENTOR_ALT_TAG = "WGM_INVENTOR_ALT_TAG";

   @RBEntry("Workgroup Manager per Mentor Graphics")
   public static final String WGM_MENTOR_GRAPHICS = "WGM_MENTOR_GRAPHICS";

   @RBEntry("Workgroup Manager per Mentor Graphics")
   public static final String WGM_MENTOR_GRAPHICS_ALT_TAG = "WGM_MENTOR_GRAPHICS_ALT_TAG";

   @RBEntry("Workgroup Manager per Pro/ENGINEER 2001")
   public static final String WGM_PRO_E = "WGM_PRO_E";

   @RBEntry("Vai al Workgroup Manager per Pro/ENGINEER 2001")
   public static final String WGM_PRO_E_ALT_TAG = "WGM_PRO_E_ALT_TAG";

   @RBEntry("Workgroup Manager per SolidWorks")
   public static final String WGM_SOLIDWORKS = "WGM_SOLIDWORKS";

   @RBEntry("Vai al Workgroup Manager per SolidWorks")
   public static final String WGM_SOLIDWORKS_ALT_TAG = "WGM_SOLIDWORKS_ALT_TAG";

   @RBEntry("Workgroup Manager per Unigraphics")
   public static final String WGM_UNIGRAPHICS = "WGM_UNIGRAPHICS";

   @RBEntry("Vai al Workgroup Manager per Unigraphics")
   public static final String WGM_UNIGRAPHICS_ALT_TAG = "WGM_UNIGRAPHICS_ALT_TAG";

   @RBEntry("Pagina principale Windchill")
   public static final String WINDCHILL_HOME = "WINDCHILL_HOME";

   @RBEntry("Vai alla Pagina principale Windchill")
   public static final String WINDCHILL_HOME_ALT_TAG = "WINDCHILL_HOME_ALT_TAG";

   /**
    * 
    * Title Entry
    * 
    **/
   @RBEntry("Installazione")
   @RBComment("Title displayed on the Setup page ")
   public static final String SETUP_TITLE_TEXT = "SETUP_TITLE_TEXT";
}
