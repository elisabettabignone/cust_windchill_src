/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.login;

import wt.util.resource.*;

@RBUUID("wt.clients.login.LoginRB")
public final class LoginRB extends WTListResourceBundle {
   @RBEntry("Login Failed")
   public static final String LOGIN_FAILED = "3";

   @RBEntry("Login Failed -- Unknown Web Id")
   public static final String WEB_AUTH_FAILED = "4";

   @RBEntry("Login Failed -- Unknown Windchill User")
   public static final String WINDCHILL_AUTH_FAILED = "5";

   @RBEntry("Authenticating...")
   public static final String AUTHENTICATING = "6";

   @RBEntry("Sucessfully authenticated")
   public static final String COMPLETED = "8";

   @RBEntry("Re Authenticate")
   public static final String RE_AUTHENTICATE = "9";

   @RBEntry("Status: ")
   public static final String STATUS = "10";

   @RBEntry("Web Id:  ")
   public static final String AUTH_USER = "11";

   @RBEntry("User Id: ")
   public static final String USER = "12";

   @RBEntry("Login ")
   public static final String TITLE = "13";
}
