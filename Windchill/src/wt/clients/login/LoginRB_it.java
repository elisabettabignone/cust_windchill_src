/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.login;

import wt.util.resource.*;

@RBUUID("wt.clients.login.LoginRB")
public final class LoginRB_it extends WTListResourceBundle {
   @RBEntry("Accesso non riuscito")
   public static final String LOGIN_FAILED = "3";

   @RBEntry("Accesso non riuscito. ID Web sconosciuto")
   public static final String WEB_AUTH_FAILED = "4";

   @RBEntry("Accesso non riuscito. Utente Windchill sconosciuto")
   public static final String WINDCHILL_AUTH_FAILED = "5";

   @RBEntry("Autenticazione in corso...")
   public static final String AUTHENTICATING = "6";

   @RBEntry("Autenticazione completata")
   public static final String COMPLETED = "8";

   @RBEntry("Nuova autenticazione")
   public static final String RE_AUTHENTICATE = "9";

   @RBEntry("Stato: ")
   public static final String STATUS = "10";

   @RBEntry("ID Web:  ")
   public static final String AUTH_USER = "11";

   @RBEntry("ID utente: ")
   public static final String USER = "12";

   @RBEntry("Accesso")
   public static final String TITLE = "13";
}
