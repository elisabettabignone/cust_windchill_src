/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.prodmgmt;

import wt.util.resource.*;

@RBUUID("wt.clients.prodmgmt.ProdMgmtHelpRB")
public final class ProdMgmtHelpRB_it extends WTListResourceBundle {
   @RBEntry("ExportImportExpSpecifyConfigSpec")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_0 = "Help/prodmgmt/ConfigSpecTask";

   @RBEntry("PIMPartCreate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_1 = "Help/prodmgmt/CreatePartTask";

   @RBEntry("PIMProdCreate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/prodmgmt/CreateProductTask";

   @RBEntry("PIMSNPCreate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_3 = "Help/prodmgmt/CreateSerialNumberTask";

   @RBEntry("PIMPICreate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_4 = "Help/prodmgmt/CreateProductInstanceTask";

   @RBEntry("PIMPIDuplicate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_5 = "Help/prodmgmt/DuplicateProductInstanceTask";

   @RBEntry("PIMPCCreate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_6 = "Help/prodmgmt/CreateConfigurationTask";

   @RBEntry("PIMExpOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_7 = "Help/prodmgmt/PartExplorerTask";

   @RBEntry("PIMOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_8 = "Help/prodmgmt/PartTask";

   @RBEntry("PIMPartUpdate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_9 = "Help/prodmgmt/UpdatePartTask";

   @RBEntry("PIMProdUpdate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_10 = "Help/prodmgmt/UpdateProductTask";

   @RBEntry("PIMSNPUpdate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_11 = "Help/prodmgmt/UpdateSerialNumberTask";

   @RBEntry("PIMPartView")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_12 = "Help/prodmgmt/ViewPartTask";

   @RBEntry("PIMPartVersionAssign")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_13 = "Help/prodmgmt/AssignPartTask";

   @RBEntry("PIMProdStrucPopFrom")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_14 = "Help/prodmgmt/PopulateFromTask";

   @RBEntry("PIMSAOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_15 = "Help/prodmgmt/OpenAnnotationTask";

   @RBEntry("PIMSAAssemblyAnnote")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_16 = "Help/prodmgmt/AnnotateAssemblyTask";

   @RBEntry("PIMPCAssociate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_17 = "Help/prodmgmt/AssociateConfigurationTask";

   @RBEntry("PIMPIVersionAssign")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_18 = "Help/prodmgmt/AssignProductInstanceTask";

   @RBEntry("PIMSNPAllocate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_19 = "Help/prodmgmt/AllocateSNPartTask";

   @RBEntry("PIMViewSetPref")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_20 = "Help/prodmgmt/SetViewPreferenceTask";

   @RBEntry("PIMOccurrenceDetails")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_21 = "Help/prodmgmt/OccurrencesTask";

   @RBEntry("PIMOccurrenceAdd")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_22 = "Help/prodmgmt/AddOccurrence";

   @RBEntry("PIMOccurrenceEdit")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_23 = "Help/prodmgmt/EditOccurrence";

   @RBEntry("PIMOccurrenceRemove")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_24 = "Help/prodmgmt/ChooseOccurrences";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_25 = "Desc/prodmgmt/PartTask";

   @RBEntry("Numero della parte")
   public static final String PRIVATE_CONSTANT_26 = "Desc/prodmgmt/PartTask/Number";

   @RBEntry("Nome parte")
   public static final String PRIVATE_CONSTANT_27 = "Desc/prodmgmt/PartTask/Name";

   @RBEntry("Tipo della parte")
   public static final String PRIVATE_CONSTANT_28 = "Desc/prodmgmt/PartTask/Type";

   @RBEntry("Posizione della parte")
   public static final String PRIVATE_CONSTANT_29 = "Desc/prodmgmt/PartTask/Location";

   @RBEntry("Revisione della parte")
   public static final String PRIVATE_CONSTANT_30 = "Desc/prodmgmt/PartTask/Revision";

   @RBEntry("Iterazione della parte")
   public static final String PRIVATE_CONSTANT_31 = "Desc/prodmgmt/PartTask/IterationIdentifier";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Stato del ciclo di vita della parte")
   public static final String PRIVATE_CONSTANT_32 = "Desc/prodmgmt/PartTask/State";

   @RBEntry("Stato della parte")
   public static final String PRIVATE_CONSTANT_33 = "Desc/prodmgmt/PartTask/StatusText";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Data di creazione della parte")
   public static final String PRIVATE_CONSTANT_34 = "Desc/prodmgmt/PartTask/CreationDate";

   @RBEntry("Data di modifica della parte")
   public static final String PRIVATE_CONSTANT_35 = "Desc/prodmgmt/PartTask/LastUpdated";

   @RBEntry("Persona che ha creato la parte")
   public static final String PRIVATE_CONSTANT_36 = "Desc/prodmgmt/PartTask/CreatedByPersonName";

   @RBEntry("Persona che ha apportato l'ultima modifica")
   public static final String PRIVATE_CONSTANT_37 = "Desc/prodmgmt/PartTask/ModifiedByPersonName";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Origine della parte")
   public static final String PRIVATE_CONSTANT_38 = "Desc/prodmgmt/PartTask/Source";

   @RBEntry("Nome della vista della parte")
   public static final String PRIVATE_CONSTANT_39 = "Desc/prodmgmt/PartTask/View";

   @RBEntry("Nome della vista della parte")
   public static final String PRIVATE_CONSTANT_40 = "Desc/prodmgmt/PartTask/ViewName";

   @RBEntry("Fare clic per visualizzare il contenuto della parte")
   public static final String PRIVATE_CONSTANT_41 = "Desc/prodmgmt/PartTask/Contents";

   @RBEntry("Fare clic per vedere i componenti della parte")
   public static final String PRIVATE_CONSTANT_42 = "Desc/prodmgmt/PartTask/Uses";

   @RBEntry("Fare clic per vedere l'effettività associata alla parte")
   public static final String PRIVATE_CONSTANT_43 = "Desc/prodmgmt/PartTask/Effectivity";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Consente di eseguire la ricerca di una cartella")
   public static final String PRIVATE_CONSTANT_44 = "Desc/prodmgmt/PartTask/Browse";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Fare clic qui per visualizzare i riferimenti per la parte")
   public static final String PRIVATE_CONSTANT_45 = "Desc/prodmgmt/PartTask/References";

   @RBEntry("Salva le modifiche apportate alla parte e chiude la finestra")
   public static final String PRIVATE_CONSTANT_46 = "Desc/prodmgmt/PartTask/OK";

   @RBEntry("Salva le modifiche apportate alla parte")
   public static final String PRIVATE_CONSTANT_47 = "Desc/prodmgmt/PartTask/Save";

   @RBEntry("Non salva le modifiche apportate alla parte e chiude la finestra")
   public static final String PRIVATE_CONSTANT_48 = "Desc/prodmgmt/PartTask/Cancel";

   @RBEntry("Visualizza le informazioni della guida relative al task")
   public static final String PRIVATE_CONSTANT_49 = "Desc/prodmgmt/PartTask/Help";

   @RBEntry("Elenco dei componenti utilizzati dalla parte")
   public static final String PRIVATE_CONSTANT_50 = "Desc/prodmgmt/PartTask/UsesList";

   @RBEntry("Elenco delle parti che utilizzano la parte")
   public static final String PRIVATE_CONSTANT_51 = "Desc/prodmgmt/PartTask/UsedByList";

   @RBEntry("La quantità usata per la parte selezionata")
   public static final String PRIVATE_CONSTANT_52 = "Desc/prodmgmt/PartTask/UsesQuantity";

   @RBEntry("Visualizza la parte selezionata")
   public static final String PRIVATE_CONSTANT_53 = "Desc/prodmgmt/PartTask/UsesView";

   @RBEntry("Aggiunge una parte da utilizzare")
   public static final String PRIVATE_CONSTANT_54 = "Desc/prodmgmt/PartTask/UsesAdd";

   @RBEntry("Rimuove la parte selezionata dall'elenco dei componenti utilizzati dalla parte")
   public static final String PRIVATE_CONSTANT_55 = "Desc/prodmgmt/PartTask/UsesRemove";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Elenco dei documenti a cui la parte fa riferimento")
   public static final String PRIVATE_CONSTANT_56 = "Desc/prodmgmt/PartTask/ReferencesList";

   @RBEntry("Mostra le versioni del documento selezionato")
   public static final String PRIVATE_CONSTANT_57 = "Desc/prodmgmt/PartTask/ReferencesShowVersions";

   @RBEntry("Aggiunge un riferimento al documento")
   public static final String PRIVATE_CONSTANT_58 = "Desc/prodmgmt/PartTask/ReferencesAdd";

   @RBEntry("Rimuove il riferimento al documento selezionato")
   public static final String PRIVATE_CONSTANT_59 = "Desc/prodmgmt/PartTask/ReferencesRemove";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Elenco delle operazioni di modifica associate")
   public static final String PRIVATE_CONSTANT_60 = "Desc/prodmgmt/PartTask/ChangesList";

   @RBEntry("Visualizza l'operazione di modifica selezionata")
   public static final String PRIVATE_CONSTANT_61 = "Desc/prodmgmt/PartTask/ChangesView";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Elenco delle versioni associate")
   public static final String PRIVATE_CONSTANT_62 = "Desc/prodmgmt/PartTask/VersionsList";

   @RBEntry("Visualizza la versione selezionata")
   public static final String PRIVATE_CONSTANT_63 = "Desc/prodmgmt/PartTask/VersionsView";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Chiude la finestra e utilizza la parte selezionata")
   public static final String PRIVATE_CONSTANT_64 = "Desc/prodmgmt/FindPartTask/OK";

   @RBEntry("Chiude la finestra senza selezionare una parte")
   public static final String PRIVATE_CONSTANT_65 = "Desc/prodmgmt/FindPartTask/Cancel";

   @RBEntry("Ricerca di parti")
   public static final String PRIVATE_CONSTANT_66 = "Desc/prodmgmt/FindPartTask/Find";

   @RBEntry("Immettere il nome della parte da ricercare")
   public static final String PRIVATE_CONSTANT_67 = "Desc/prodmgmt/FindPartTask/PartName";

   @RBEntry("Immettere il numero della parte da ricercare")
   public static final String PRIVATE_CONSTANT_68 = "Desc/prodmgmt/FindPartTask/PartNumber";

   @RBEntry("Visualizza la Guida in linea")
   public static final String PRIVATE_CONSTANT_69 = "Desc/prodmgmt/FindPartTask/Help";

   @RBEntry("Fare clic sulla parte da utilizzare")
   public static final String PRIVATE_CONSTANT_70 = "Desc/prodmgmt/FindPartTask/SelectionList";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Fare clic qui per attivare la specifica di configurazione standard.")
   public static final String PRIVATE_CONSTANT_71 = "Desc/prodmgmt/PartTask/StandardRadioButton";

   @RBEntry("Fare clic per attivare la qualificazione della specifica di configurazione della baseline.")
   public static final String PRIVATE_CONSTANT_72 = "Desc/prodmgmt/PartTask/BaselineRadioButton";

   @RBEntry("Fare clic per attivare la qualificazione della specifica di configurazione dell'effettività.")
   public static final String PRIVATE_CONSTANT_73 = "Desc/prodmgmt/PartTask/EffectivityRadioButton";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Seleziona una vista")
   public static final String PRIVATE_CONSTANT_74 = "Desc/prodmgmt/PartTask/StandardView";

   @RBEntry("Seleziona uno stato del ciclo di vita")
   public static final String PRIVATE_CONSTANT_75 = "Desc/prodmgmt/PartTask/StandardLifeCycleState";

   @RBEntry("Fare clic per ottenere le copie d'uso.")
   public static final String PRIVATE_CONSTANT_76 = "Desc/prodmgmt/PartTask/StandardCheckOut";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Baseline correntemente specificata")
   public static final String PRIVATE_CONSTANT_77 = "Desc/prodmgmt/PartTask/BaselineName";

   @RBEntry("Sceglie una baseline.")
   public static final String PRIVATE_CONSTANT_78 = "Desc/prodmgmt/PartTask/BaselineSearch";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Seleziona una vista collegata all'effettività")
   public static final String PRIVATE_CONSTANT_79 = "Desc/prodmgmt/PartTask/EffectivityView";

   @RBEntry("Immettere la data di effettività.")
   public static final String PRIVATE_CONSTANT_80 = "Desc/prodmgmt/PartTask/EffectiveDate";

   @RBEntry("Immettere il valore del configuration item.")
   public static final String PRIVATE_CONSTANT_81 = "Desc/prodmgmt/PartTask/Value";

   @RBEntry("Configuration item correntemente specificato.")
   public static final String PRIVATE_CONSTANT_82 = "Desc/prodmgmt/PartTask/ConfigurationItem";

   @RBEntry("Scegliere un configuration item.")
   public static final String PRIVATE_CONSTANT_83 = "Desc/prodmgmt/PartTask/ConfigurationItemSearch";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Nuova parte")
   public static final String PRIVATE_CONSTANT_84 = "Tip/prodmgmt/PartExplorerTask/newpart_tbar";

   @RBEntry("Check-In")
   public static final String PRIVATE_CONSTANT_85 = "Tip/prodmgmt/PartExplorerTask/checkin_tbar";

   @RBEntry("Annulla Check-Out")
   public static final String PRIVATE_CONSTANT_86 = "Tip/prodmgmt/PartExplorerTask/undocheckout_tbar";

   @RBEntry("Check-Out")
   public static final String PRIVATE_CONSTANT_87 = "Tip/prodmgmt/PartExplorerTask/checkout_tbar";

   @RBEntry("Aggiorna")
   public static final String PRIVATE_CONSTANT_88 = "Tip/prodmgmt/PartExplorerTask/update_tbar";

   @RBEntry("Visualizza la pagina delle proprietà")
   public static final String PRIVATE_CONSTANT_89 = "Tip/prodmgmt/PartExplorerTask/viewpropertiespage_tbar";

   @RBEntry("Ricerca locale")
   public static final String PRIVATE_CONSTANT_90 = "Tip/prodmgmt/PartExplorerTask/localsearch";

   @RBEntry("Elimina l'oggetto selezionato dal navigatore")
   public static final String PRIVATE_CONSTANT_91 = "Tip/prodmgmt/PartExplorerTask/clear_tbar";

   @RBEntry("Apri il Navigatore Windchill")
   public static final String PRIVATE_CONSTANT_92 = "Tip/prodmgmt/PartExplorerTask/wexplr_tbar";

   @RBEntry("Taglia")
   public static final String PRIVATE_CONSTANT_93 = "Tip/prodmgmt/PartExplorerTask/cut";

   @RBEntry("Copia")
   public static final String PRIVATE_CONSTANT_94 = "Tip/prodmgmt/PartExplorerTask/copy";

   @RBEntry("Incolla")
   public static final String PRIVATE_CONSTANT_95 = "Tip/prodmgmt/PartExplorerTask/Paste";

   @RBEntry("Imposta specifica di configurazione")
   public static final String PRIVATE_CONSTANT_96 = "Tip/prodmgmt/PartExplorerTask/configspec_tbar";

   @RBEntry("Nuova revisione")
   public static final String PRIVATE_CONSTANT_97 = "Tip/prodmgmt/PartExplorerTask/revise_tbar";

   @RBEntry("Cerca")
   public static final String PRIVATE_CONSTANT_98 = "Tip/prodmgmt/PartExplorerTask/search_tbar";

   @RBEntry("Aggiorna l'oggetto selezionato")
   public static final String PRIVATE_CONSTANT_99 = "Tip/prodmgmt/PartExplorerTask/refresh_tbar";

   @RBEntry("Elimina")
   public static final String PRIVATE_CONSTANT_100 = "Tip/prodmgmt/PartExplorerTask/delete";

   @RBEntry("Guida")
   public static final String PRIVATE_CONSTANT_101 = "Tip/prodmgmt/PartExplorerTask/help_tbar";

   @RBEntry("Avvia un nuovo Navigatore dati di prodotto")
   public static final String PRIVATE_CONSTANT_102 = "Tip/prodmgmt/PartExplorerTask/pie_tbar";

   @RBEntry("Apri")
   public static final String PRIVATE_CONSTANT_103 = "Tip/prodmgmt/PartExplorerTask/open_tbar";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Contenuto del nodo selezionato")
   public static final String PRIVATE_CONSTANT_104 = "Desc/prodmgmt/PartExplorerTask/List";

   @RBEntry("Espandi e comprimi albero")
   public static final String PRIVATE_CONSTANT_105 = "Desc/prodmgmt/PartExplorerTask/Tree";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Fare clic per vedere gli oggetti che descrivono la parte")
   public static final String PRIVATE_CONSTANT_106 = "Desc/prodmgmt/PartTask/BuildRule";

   @RBEntry("Elenco degli oggetti che descrivono la parte")
   public static final String PRIVATE_CONSTANT_107 = "Desc/prodmgmt/PartTask/BuildRuleList";

   @RBEntry("Visualizza l'oggetto selezionato")
   public static final String PRIVATE_CONSTANT_108 = "Desc/prodmgmt/PartTask/BuildRuleView";
}
