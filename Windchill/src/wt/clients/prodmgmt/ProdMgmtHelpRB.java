/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.prodmgmt;

import wt.util.resource.*;

@RBUUID("wt.clients.prodmgmt.ProdMgmtHelpRB")
public final class ProdMgmtHelpRB extends WTListResourceBundle {
   @RBEntry("ExportImportExpSpecifyConfigSpec")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_0 = "Help/prodmgmt/ConfigSpecTask";

   @RBEntry("PIMPartCreate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_1 = "Help/prodmgmt/CreatePartTask";

   @RBEntry("PIMProdCreate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/prodmgmt/CreateProductTask";

   @RBEntry("PIMSNPCreate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_3 = "Help/prodmgmt/CreateSerialNumberTask";

   @RBEntry("PIMPICreate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_4 = "Help/prodmgmt/CreateProductInstanceTask";

   @RBEntry("PIMPIDuplicate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_5 = "Help/prodmgmt/DuplicateProductInstanceTask";

   @RBEntry("PIMPCCreate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_6 = "Help/prodmgmt/CreateConfigurationTask";

   @RBEntry("PIMExpOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_7 = "Help/prodmgmt/PartExplorerTask";

   @RBEntry("PIMOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_8 = "Help/prodmgmt/PartTask";

   @RBEntry("PIMPartUpdate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_9 = "Help/prodmgmt/UpdatePartTask";

   @RBEntry("PIMProdUpdate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_10 = "Help/prodmgmt/UpdateProductTask";

   @RBEntry("PIMSNPUpdate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_11 = "Help/prodmgmt/UpdateSerialNumberTask";

   @RBEntry("PIMPartView")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_12 = "Help/prodmgmt/ViewPartTask";

   @RBEntry("PIMPartVersionAssign")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_13 = "Help/prodmgmt/AssignPartTask";

   @RBEntry("PIMProdStrucPopFrom")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_14 = "Help/prodmgmt/PopulateFromTask";

   @RBEntry("PIMSAOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_15 = "Help/prodmgmt/OpenAnnotationTask";

   @RBEntry("PIMSAAssemblyAnnote")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_16 = "Help/prodmgmt/AnnotateAssemblyTask";

   @RBEntry("PIMPCAssociate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_17 = "Help/prodmgmt/AssociateConfigurationTask";

   @RBEntry("PIMPIVersionAssign")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_18 = "Help/prodmgmt/AssignProductInstanceTask";

   @RBEntry("PIMSNPAllocate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_19 = "Help/prodmgmt/AllocateSNPartTask";

   @RBEntry("PIMViewSetPref")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_20 = "Help/prodmgmt/SetViewPreferenceTask";

   @RBEntry("PIMOccurrenceDetails")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_21 = "Help/prodmgmt/OccurrencesTask";

   @RBEntry("PIMOccurrenceAdd")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_22 = "Help/prodmgmt/AddOccurrence";

   @RBEntry("PIMOccurrenceEdit")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_23 = "Help/prodmgmt/EditOccurrence";

   @RBEntry("PIMOccurrenceRemove")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_24 = "Help/prodmgmt/ChooseOccurrences";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_25 = "Desc/prodmgmt/PartTask";

   @RBEntry("The number of the part")
   public static final String PRIVATE_CONSTANT_26 = "Desc/prodmgmt/PartTask/Number";

   @RBEntry("The name of the part")
   public static final String PRIVATE_CONSTANT_27 = "Desc/prodmgmt/PartTask/Name";

   @RBEntry("The type of the part")
   public static final String PRIVATE_CONSTANT_28 = "Desc/prodmgmt/PartTask/Type";

   @RBEntry("The location of the part")
   public static final String PRIVATE_CONSTANT_29 = "Desc/prodmgmt/PartTask/Location";

   @RBEntry("The revision of the part")
   public static final String PRIVATE_CONSTANT_30 = "Desc/prodmgmt/PartTask/Revision";

   @RBEntry("The iteration of the part")
   public static final String PRIVATE_CONSTANT_31 = "Desc/prodmgmt/PartTask/IterationIdentifier";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("The state of the part")
   public static final String PRIVATE_CONSTANT_32 = "Desc/prodmgmt/PartTask/State";

   @RBEntry("The status of the part")
   public static final String PRIVATE_CONSTANT_33 = "Desc/prodmgmt/PartTask/StatusText";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("The creation date of the part")
   public static final String PRIVATE_CONSTANT_34 = "Desc/prodmgmt/PartTask/CreationDate";

   @RBEntry("The modification date of the part")
   public static final String PRIVATE_CONSTANT_35 = "Desc/prodmgmt/PartTask/LastUpdated";

   @RBEntry("The person who created the part")
   public static final String PRIVATE_CONSTANT_36 = "Desc/prodmgmt/PartTask/CreatedByPersonName";

   @RBEntry("The person who last modified the part")
   public static final String PRIVATE_CONSTANT_37 = "Desc/prodmgmt/PartTask/ModifiedByPersonName";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("The source of the part")
   public static final String PRIVATE_CONSTANT_38 = "Desc/prodmgmt/PartTask/Source";

   @RBEntry("The view of the part")
   public static final String PRIVATE_CONSTANT_39 = "Desc/prodmgmt/PartTask/View";

   @RBEntry("The view of the part")
   public static final String PRIVATE_CONSTANT_40 = "Desc/prodmgmt/PartTask/ViewName";

   @RBEntry("Click here to see the contents of this part")
   public static final String PRIVATE_CONSTANT_41 = "Desc/prodmgmt/PartTask/Contents";

   @RBEntry("Click here to see parts this part uses")
   public static final String PRIVATE_CONSTANT_42 = "Desc/prodmgmt/PartTask/Uses";

   @RBEntry("Click here to see the effectivities associated with this part")
   public static final String PRIVATE_CONSTANT_43 = "Desc/prodmgmt/PartTask/Effectivity";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Browse for a folder location")
   public static final String PRIVATE_CONSTANT_44 = "Desc/prodmgmt/PartTask/Browse";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Click here to see references for this part")
   public static final String PRIVATE_CONSTANT_45 = "Desc/prodmgmt/PartTask/References";

   @RBEntry("Save the changes to this part and close the window")
   public static final String PRIVATE_CONSTANT_46 = "Desc/prodmgmt/PartTask/OK";

   @RBEntry("Save the changes to this part")
   public static final String PRIVATE_CONSTANT_47 = "Desc/prodmgmt/PartTask/Save";

   @RBEntry("Do not save the changes to this part, just close the window")
   public static final String PRIVATE_CONSTANT_48 = "Desc/prodmgmt/PartTask/Cancel";

   @RBEntry("Display help for this task")
   public static final String PRIVATE_CONSTANT_49 = "Desc/prodmgmt/PartTask/Help";

   @RBEntry("The list of parts this part uses")
   public static final String PRIVATE_CONSTANT_50 = "Desc/prodmgmt/PartTask/UsesList";

   @RBEntry("The list of parts this part is used by")
   public static final String PRIVATE_CONSTANT_51 = "Desc/prodmgmt/PartTask/UsedByList";

   @RBEntry("The quantity used of the selected part")
   public static final String PRIVATE_CONSTANT_52 = "Desc/prodmgmt/PartTask/UsesQuantity";

   @RBEntry("View the selected part")
   public static final String PRIVATE_CONSTANT_53 = "Desc/prodmgmt/PartTask/UsesView";

   @RBEntry("Add a part to be used")
   public static final String PRIVATE_CONSTANT_54 = "Desc/prodmgmt/PartTask/UsesAdd";

   @RBEntry("Remove the selected part from use")
   public static final String PRIVATE_CONSTANT_55 = "Desc/prodmgmt/PartTask/UsesRemove";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("The list of documents this part references")
   public static final String PRIVATE_CONSTANT_56 = "Desc/prodmgmt/PartTask/ReferencesList";

   @RBEntry("Show versions of the selected document")
   public static final String PRIVATE_CONSTANT_57 = "Desc/prodmgmt/PartTask/ReferencesShowVersions";

   @RBEntry("Add a reference to a document")
   public static final String PRIVATE_CONSTANT_58 = "Desc/prodmgmt/PartTask/ReferencesAdd";

   @RBEntry("Remove the reference to the selected document")
   public static final String PRIVATE_CONSTANT_59 = "Desc/prodmgmt/PartTask/ReferencesRemove";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("The list of associated change activities")
   public static final String PRIVATE_CONSTANT_60 = "Desc/prodmgmt/PartTask/ChangesList";

   @RBEntry("View the selected change activity")
   public static final String PRIVATE_CONSTANT_61 = "Desc/prodmgmt/PartTask/ChangesView";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("The list of associated versions")
   public static final String PRIVATE_CONSTANT_62 = "Desc/prodmgmt/PartTask/VersionsList";

   @RBEntry("View the selected version")
   public static final String PRIVATE_CONSTANT_63 = "Desc/prodmgmt/PartTask/VersionsView";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Close the window and use the selected part")
   public static final String PRIVATE_CONSTANT_64 = "Desc/prodmgmt/FindPartTask/OK";

   @RBEntry("Close the window without selecting a part")
   public static final String PRIVATE_CONSTANT_65 = "Desc/prodmgmt/FindPartTask/Cancel";

   @RBEntry("Search for parts")
   public static final String PRIVATE_CONSTANT_66 = "Desc/prodmgmt/FindPartTask/Find";

   @RBEntry("Enter the name of the part to search for")
   public static final String PRIVATE_CONSTANT_67 = "Desc/prodmgmt/FindPartTask/PartName";

   @RBEntry("Enter the number of the part to search for")
   public static final String PRIVATE_CONSTANT_68 = "Desc/prodmgmt/FindPartTask/PartNumber";

   @RBEntry("Display Help")
   public static final String PRIVATE_CONSTANT_69 = "Desc/prodmgmt/FindPartTask/Help";

   @RBEntry("Click on the part to use")
   public static final String PRIVATE_CONSTANT_70 = "Desc/prodmgmt/FindPartTask/SelectionList";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Click here to enable the standard configuration specification qualification.")
   public static final String PRIVATE_CONSTANT_71 = "Desc/prodmgmt/PartTask/StandardRadioButton";

   @RBEntry("Click here to enable the baseline configuration specification qualification.")
   public static final String PRIVATE_CONSTANT_72 = "Desc/prodmgmt/PartTask/BaselineRadioButton";

   @RBEntry("Click here to enable the effectivity configuration specification qualification.")
   public static final String PRIVATE_CONSTANT_73 = "Desc/prodmgmt/PartTask/EffectivityRadioButton";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Select a view")
   public static final String PRIVATE_CONSTANT_74 = "Desc/prodmgmt/PartTask/StandardView";

   @RBEntry("Select a Life Cycle State")
   public static final String PRIVATE_CONSTANT_75 = "Desc/prodmgmt/PartTask/StandardLifeCycleState";

   @RBEntry("Click to obtain working copies.")
   public static final String PRIVATE_CONSTANT_76 = "Desc/prodmgmt/PartTask/StandardCheckOut";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("The currently specified baseline")
   public static final String PRIVATE_CONSTANT_77 = "Desc/prodmgmt/PartTask/BaselineName";

   @RBEntry("Choose a Baseline.")
   public static final String PRIVATE_CONSTANT_78 = "Desc/prodmgmt/PartTask/BaselineSearch";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Select an Effectivity-related view")
   public static final String PRIVATE_CONSTANT_79 = "Desc/prodmgmt/PartTask/EffectivityView";

   @RBEntry("Enter the Effective Date.")
   public static final String PRIVATE_CONSTANT_80 = "Desc/prodmgmt/PartTask/EffectiveDate";

   @RBEntry("Enter the value for the Configuration Item.")
   public static final String PRIVATE_CONSTANT_81 = "Desc/prodmgmt/PartTask/Value";

   @RBEntry("The currently specified Configuration Item.")
   public static final String PRIVATE_CONSTANT_82 = "Desc/prodmgmt/PartTask/ConfigurationItem";

   @RBEntry("Choose a Configuration Item.")
   public static final String PRIVATE_CONSTANT_83 = "Desc/prodmgmt/PartTask/ConfigurationItemSearch";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("New part")
   public static final String PRIVATE_CONSTANT_84 = "Tip/prodmgmt/PartExplorerTask/newpart_tbar";

   @RBEntry("Check in")
   public static final String PRIVATE_CONSTANT_85 = "Tip/prodmgmt/PartExplorerTask/checkin_tbar";

   @RBEntry("Undo check out")
   public static final String PRIVATE_CONSTANT_86 = "Tip/prodmgmt/PartExplorerTask/undocheckout_tbar";

   @RBEntry("Check out")
   public static final String PRIVATE_CONSTANT_87 = "Tip/prodmgmt/PartExplorerTask/checkout_tbar";

   @RBEntry("Update")
   public static final String PRIVATE_CONSTANT_88 = "Tip/prodmgmt/PartExplorerTask/update_tbar";

   @RBEntry("View the properties page")
   public static final String PRIVATE_CONSTANT_89 = "Tip/prodmgmt/PartExplorerTask/viewpropertiespage_tbar";

   @RBEntry("Search Local")
   public static final String PRIVATE_CONSTANT_90 = "Tip/prodmgmt/PartExplorerTask/localsearch";

   @RBEntry("Clear the selected object from the explorer")
   public static final String PRIVATE_CONSTANT_91 = "Tip/prodmgmt/PartExplorerTask/clear_tbar";

   @RBEntry("Open the Windchill Explorer")
   public static final String PRIVATE_CONSTANT_92 = "Tip/prodmgmt/PartExplorerTask/wexplr_tbar";

   @RBEntry("Cut")
   public static final String PRIVATE_CONSTANT_93 = "Tip/prodmgmt/PartExplorerTask/cut";

   @RBEntry("Copy")
   public static final String PRIVATE_CONSTANT_94 = "Tip/prodmgmt/PartExplorerTask/copy";

   @RBEntry("Paste")
   public static final String PRIVATE_CONSTANT_95 = "Tip/prodmgmt/PartExplorerTask/Paste";

   @RBEntry("Set the Configuration Specification")
   public static final String PRIVATE_CONSTANT_96 = "Tip/prodmgmt/PartExplorerTask/configspec_tbar";

   @RBEntry("Revise")
   public static final String PRIVATE_CONSTANT_97 = "Tip/prodmgmt/PartExplorerTask/revise_tbar";

   @RBEntry("Search")
   public static final String PRIVATE_CONSTANT_98 = "Tip/prodmgmt/PartExplorerTask/search_tbar";

   @RBEntry("Refresh the selected object")
   public static final String PRIVATE_CONSTANT_99 = "Tip/prodmgmt/PartExplorerTask/refresh_tbar";

   @RBEntry("Delete")
   public static final String PRIVATE_CONSTANT_100 = "Tip/prodmgmt/PartExplorerTask/delete";

   @RBEntry("Help")
   public static final String PRIVATE_CONSTANT_101 = "Tip/prodmgmt/PartExplorerTask/help_tbar";

   @RBEntry("Open a new Product Information Explorer window")
   public static final String PRIVATE_CONSTANT_102 = "Tip/prodmgmt/PartExplorerTask/pie_tbar";

   @RBEntry("Open")
   public static final String PRIVATE_CONSTANT_103 = "Tip/prodmgmt/PartExplorerTask/open_tbar";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Contents of the selected node")
   public static final String PRIVATE_CONSTANT_104 = "Desc/prodmgmt/PartExplorerTask/List";

   @RBEntry("Expand and Collapse Tree")
   public static final String PRIVATE_CONSTANT_105 = "Desc/prodmgmt/PartExplorerTask/Tree";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Click here to see the objects that describe this part")
   public static final String PRIVATE_CONSTANT_106 = "Desc/prodmgmt/PartTask/BuildRule";

   @RBEntry("The list of objects describing this part")
   public static final String PRIVATE_CONSTANT_107 = "Desc/prodmgmt/PartTask/BuildRuleList";

   @RBEntry("View the selected object")
   public static final String PRIVATE_CONSTANT_108 = "Desc/prodmgmt/PartTask/BuildRuleView";
}