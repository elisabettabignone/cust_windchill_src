/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.prodmgmt;

import wt.util.resource.*;

@RBUUID("wt.clients.prodmgmt.ProdMgmtRB")
public final class ProdMgmtRB extends WTListResourceBundle {
   /**
    * Labels ------------------------------------------------------------------
    **/
   @RBEntry("Created:")
   public static final String PRIVATE_CONSTANT_0 = "createdLbl";

   @RBEntry("Created By:")
   public static final String PRIVATE_CONSTANT_1 = "createdByLbl";

   @RBEntry("Created On:")
   public static final String PRIVATE_CONSTANT_2 = "createdOnLbl";

   @RBEntry("Source:")
   public static final String PRIVATE_CONSTANT_3 = "sourceLbl";

   @RBEntry("Location:")
   public static final String PRIVATE_CONSTANT_4 = "locationLbl";

   @RBEntry("Organization ID:")
   public static final String orgIdLbl = "orgIdLbl";

   @RBEntry("Name:")
   public static final String nameLbl = "nameLbl";

   @RBEntry("Number:")
   public static final String numberLbl = "numberLbl";

   @RBEntry("Status:")
   public static final String PRIVATE_CONSTANT_5 = "statusLbl";

   @RBEntry("Type:")
   public static final String PRIVATE_CONSTANT_6 = "typeLbl";

   @RBEntry("Folder Name:")
   public static final String PRIVATE_CONSTANT_7 = "folderNameLbl";

   @RBEntry("Modified By:")
   public static final String PRIVATE_CONSTANT_8 = "updatedByLbl";

   @RBEntry("Last Modified:")
   public static final String PRIVATE_CONSTANT_9 = "updatedOnLbl";

   @RBEntry("Version:")
   public static final String PRIVATE_CONSTANT_10 = "revisionLbl";

   @RBEntry("Iteration:")
   public static final String PRIVATE_CONSTANT_11 = "iterationLbl";

   @RBEntry("Project:")
   public static final String PRIVATE_CONSTANT_12 = "projectLbl";

   @RBEntry("Life Cycle:")
   public static final String PRIVATE_CONSTANT_13 = "lifecycleLbl";

   @RBEntry("State:")
   public static final String PRIVATE_CONSTANT_14 = "stateLbl";

   @RBEntry("View:")
   public static final String PRIVATE_CONSTANT_15 = "viewLbl";

   @RBEntry("Include parts in my personal cabinet")
   public static final String PRIVATE_CONSTANT_16 = "checkedOutLbl";

   @RBEntry("Qty:")
   public static final String PRIVATE_CONSTANT_17 = "quantityLbl";

   @RBEntry("Unit:")
   public static final String PRIVATE_CONSTANT_18 = "unitLbl";

   @RBEntry("Line:")
   public static final String PRIVATE_CONSTANT_19 = "lineNumberLbl";

   @RBEntry("Line:")
   public static final String PRIVATE_CONSTANT_20 = "editLineNumberLbl";

   @RBEntry("Working...")
   public static final String PRIVATE_CONSTANT_21 = "workingLbl";

   @RBEntry("Finding uses...")
   public static final String PRIVATE_CONSTANT_22 = "findingUsesLbl";

   @RBEntry("Finding references...")
   public static final String PRIVATE_CONSTANT_23 = "findingReferencesLbl";

   @RBEntry("Description:")
   public static final String PRIVATE_CONSTANT_24 = "descriptionLbl";

   @RBEntry("Populate with part versions")
   public static final String PRIVATE_CONSTANT_25 = "populateLbl";

   @RBEntry("Configuration:")
   public static final String PRIVATE_CONSTANT_26 = "configurationLbl";

   @RBEntry("Part Name:")
   public static final String PRIVATE_CONSTANT_27 = "partNameLbl";

   @RBEntry("Part Number:")
   public static final String PRIVATE_CONSTANT_28 = "partNumberLbl";

   @RBEntry("Allow Reassignment")
   public static final String PRIVATE_CONSTANT_29 = "allowReassignmentLbl";

   @RBEntry("Part Versions:")
   public static final String PRIVATE_CONSTANT_30 = "partVersionsLbl";

   @RBEntry("Serial Number:")
   public static final String PRIVATE_CONSTANT_31 = "serialNumberLbl";

   @RBEntry("Annotation Sets:")
   public static final String PRIVATE_CONSTANT_32 = "annotationsLbl";

   @RBEntry("Part:")
   public static final String PRIVATE_CONSTANT_33 = "openAnnotationForPartLbl";

   @RBEntry("Part:")
   public static final String PRIVATE_CONSTANT_34 = "annotateAssemblyForPartLbl";

   @RBEntry("Product Instance:")
   @RBComment("Associate Product Configuration Dialog, serial number field from Product Instance")
   public static final String PRIVATE_CONSTANT_35 = "productInstanceLbl";

   @RBEntry("Select a Product Instance:")
   @RBComment("Allocate ProductInstance Dialog, label for list of product instances")
   public static final String PRIVATE_CONSTANT_36 = "productInstancesLbl";

   @RBEntry("Sub-Product:")
   @RBComment("Allocate ProductInstance Dialog, label for sub product")
   public static final String PRIVATE_CONSTANT_37 = "subProductLbl";

   @RBEntry("Valid Product Configurations:")
   @RBComment("Associate Product Configuration Dialog, label for list of configurations")
   public static final String PRIVATE_CONSTANT_38 = "validProductConfigurationLbl";

   @RBEntry("*S/N:")
   @RBComment("Serial Number Label")
   public static final String PRIVATE_CONSTANT_39 = "snLbl";

   /**
    * Button Labels -------------------------------------------------------------
    **/
   @RBEntry("Add...")
   public static final String addButton = "addButton";

   @RBEntry("Browse...")
   public static final String PRIVATE_CONSTANT_40 = "browseButton";

   @RBEntry("Cancel")
   public static final String cancelButton = "cancelButton";

   @RBEntry("Check In")
   public static final String PRIVATE_CONSTANT_41 = "checkinButton";

   @RBEntry("Check Out")
   public static final String PRIVATE_CONSTANT_42 = "checkoutButton";

   @RBEntry("Clear")
   public static final String PRIVATE_CONSTANT_43 = "clearButton";

   @RBEntry("Close")
   public static final String PRIVATE_CONSTANT_44 = "closeButton";

   @RBEntry("Continue")
   public static final String PRIVATE_CONSTANT_45 = "continueButton";

   @RBEntry("Delete")
   public static final String PRIVATE_CONSTANT_46 = "deleteButton";

   @RBEntry("Get")
   public static final String PRIVATE_CONSTANT_47 = "getButton";

   @RBEntry("Help")
   public static final String helpButton = "helpButton";

   @RBEntry("OK")
   public static final String okButton = "okButton";

   @RBEntry("References")
   public static final String PRIVATE_CONSTANT_48 = "referencesButton";

   @RBEntry("Contents")
   public static final String PRIVATE_CONSTANT_49 = "contentsButton";

   @RBEntry("Uses")
   public static final String PRIVATE_CONSTANT_50 = "usesButton";

   @RBEntry("Effectivity")
   public static final String PRIVATE_CONSTANT_51 = "effectivityButton";

   @RBEntry("Update")
   public static final String PRIVATE_CONSTANT_52 = "updateButton";

   @RBEntry("Save")
   public static final String PRIVATE_CONSTANT_53 = "saveButton";

   @RBEntry("Remove")
   public static final String removeButton = "removeButton";

   @RBEntry("View")
   public static final String PRIVATE_CONSTANT_54 = "viewButton";

   @RBEntry("Edit Line Number")
   public static final String PRIVATE_CONSTANT_55 = "editLineNumberButton";

   @RBEntry("Next Line Number")
   public static final String PRIVATE_CONSTANT_56 = "nextLineButton";

   @RBEntry("Show Versions")
   public static final String PRIVATE_CONSTANT_57 = "showVersionsButton";

   @RBEntry("Edit Attributes")
   public static final String PRIVATE_CONSTANT_58 = "editAttributesButton";

   /**
    * MultiList column headings -------------------------------------------------
    **/
   @RBEntry("Number")
   public static final String PRIVATE_CONSTANT_59 = "numberHeading";

   @RBEntry("Line")
   public static final String PRIVATE_CONSTANT_60 = "lineNumberHeading";

   @RBEntry("Name")
   public static final String PRIVATE_CONSTANT_61 = "nameHeading";

   @RBEntry("Qty")
   public static final String PRIVATE_CONSTANT_62 = "quantityHeading";

   @RBEntry("Unit")
   public static final String PRIVATE_CONSTANT_63 = "unitHeading";

   @RBEntry("Described By")
   public static final String PRIVATE_CONSTANT_64 = "managedByHeading";

   @RBEntry("Described By")
   public static final String PRIVATE_CONSTANT_65 = "buildRuleButton";

   @RBEntry("Finding describing documents...")
   public static final String PRIVATE_CONSTANT_66 = "findingBuildRuleLbl";

   @RBEntry("Described By:")
   public static final String PRIVATE_CONSTANT_67 = "buildRuleTitle";

   @RBEntry("Owned by {0}")
   public static final String PRIVATE_CONSTANT_68 = "ownedBy";

   @RBEntry("Size")
   public static final String PRIVATE_CONSTANT_69 = "sizeHeading";

   @RBEntry("Status")
   public static final String PRIVATE_CONSTANT_70 = "statusHeading";

   @RBEntry("Version")
   public static final String PRIVATE_CONSTANT_71 = "versionHeading";

   @RBEntry("Type")
   public static final String PRIVATE_CONSTANT_72 = "typeHeading";

   @RBEntry("ID")
   public static final String PRIVATE_CONSTANT_73 = "idHeading";

   @RBEntry("Identity")
   public static final String PRIVATE_CONSTANT_74 = "identityHeading";

   @RBEntry("Revision")
   public static final String PRIVATE_CONSTANT_75 = "revisionHeading";

   @RBEntry("State")
   public static final String PRIVATE_CONSTANT_76 = "stateHeading";

   @RBEntry("View")
   public static final String PRIVATE_CONSTANT_77 = "viewHeading";

   /**
    * Checkbox labels ------------------------------------------------------------
    **/
   @RBEntry("Do not download now")
   public static final String PRIVATE_CONSTANT_78 = "doNotDownloadCheckBox";

   @RBEntry("Keep checked out")
   public static final String PRIVATE_CONSTANT_79 = "keepCheckedOut";

   /**
    * Messages ------------------------------------------------------------------
    * Panels & Tabs -------------------------------------------------------------
    * 
    **/
   @RBEntry("Available Collections")
   public static final String PRIVATE_CONSTANT_80 = "AvailableCollections";

   /**
    * Symbols -------------------------------------------------------------------
    * 
    **/
   @RBEntry("...")
   public static final String PRIVATE_CONSTANT_81 = "ellipses";

   @RBEntry("*")
   public static final String PRIVATE_CONSTANT_82 = "required";

   /**
    * Titles --------------------------------------------------------------------
    **/
   @RBEntry("Uses:")
   public static final String PRIVATE_CONSTANT_83 = "usesTitle";

   @RBEntry("Used By:")
   public static final String PRIVATE_CONSTANT_84 = "usedByTitle";

   @RBEntry("Contents:")
   public static final String PRIVATE_CONSTANT_85 = "contentsTitle";

   @RBEntry("References:")
   public static final String PRIVATE_CONSTANT_86 = "referencesTitle";

   @RBEntry("Change Activities:")
   public static final String PRIVATE_CONSTANT_87 = "changesTitle";

   @RBEntry("Set Configuration Specification")
   public static final String PRIVATE_CONSTANT_88 = "configSpecTitle";

   @RBEntry("Product Structure Explorer")
   public static final String PRIVATE_CONSTANT_89 = "pieTitle";

   @RBEntry("Create Part {0}")
   public static final String PRIVATE_CONSTANT_90 = "createPartTitle";

   @RBEntry("Create Product Configuration")
   public static final String PRIVATE_CONSTANT_91 = "createConfigurationTitle";

   @RBEntry("Create Product Instance")
   public static final String PRIVATE_CONSTANT_92 = "createInstanceTitle";

   @RBEntry("Save As <{0}>")
   @RBArgComment0("object being duplicated")
   public static final String PRIVATE_CONSTANT_93 = "duplicateInstanceTitle";

   @RBEntry("Save As <{0}>")
   @RBArgComment0("object being duplicated")
   public static final String PRIVATE_CONSTANT_94 = "duplicateInstanceHeader";

   @RBEntry("Create Product Configuration for {0}")
   @RBArgComment0("product label")
   public static final String PRIVATE_CONSTANT_95 = "createConfigurationHeader";

   @RBEntry("Create Product Instance for {0}")
   @RBArgComment0("product configuration label")
   public static final String PRIVATE_CONSTANT_96 = "createInstanceHeader";

   @RBEntry("Update Part <{0}>")
   public static final String PRIVATE_CONSTANT_97 = "updatePartTitle";

   @RBEntry("View Part <{0}>")
   public static final String PRIVATE_CONSTANT_98 = "viewPartTitle";

   @RBEntry("Working Copy of {0}")
   public static final String PRIVATE_CONSTANT_99 = "workingCopyTitle";

   @RBEntry("Find Part")
   public static final String PRIVATE_CONSTANT_100 = "findPartTitle";

   @RBEntry("Find Document")
   public static final String PRIVATE_CONSTANT_101 = "findDocTitle";

   @RBEntry("Checked In")
   public static final String PRIVATE_CONSTANT_102 = "checkedIn";

   @RBEntry("Checked Out")
   public static final String PRIVATE_CONSTANT_103 = "checkedOut";

   @RBEntry("Checked Out by {0}")
   public static final String PRIVATE_CONSTANT_104 = "checkedOutBy";

   @RBEntry("Not Checked Out")
   public static final String PRIVATE_CONSTANT_105 = "notCheckedOut";

   @RBEntry("Working Copy")
   public static final String PRIVATE_CONSTANT_106 = "workingCopy";

   @RBEntry("Assign Part to Version")
   public static final String PRIVATE_CONSTANT_107 = "assignPartTitle";

   @RBEntry("Populate from Product Structure")
   public static final String PRIVATE_CONSTANT_108 = "populateFromTitle";

   @RBEntry("Set View Preference")
   public static final String PRIVATE_CONSTANT_109 = "setViewPreferenceTitle";

   @RBEntry("Open Annotation Set")
   public static final String PRIVATE_CONSTANT_110 = "openAnnotationTitle";

   @RBEntry("Delete Annotation Set")
   public static final String PRIVATE_CONSTANT_111 = "deleteAnnotationTitle";

   @RBEntry("Annotate Assembly")
   public static final String PRIVATE_CONSTANT_112 = "annotateAssemblyTitle";

   @RBEntry("Associate Product Configuration")
   public static final String PRIVATE_CONSTANT_113 = "associateConfigurationTitle";

   @RBEntry("Allocate")
   public static final String PRIVATE_CONSTANT_114 = "allocateTitle";

   @RBEntry("Edit Line Number")
   public static final String PRIVATE_CONSTANT_115 = "editLineDialogTitle";

   /**
    * Menus ---------------------------------------------------------------------
    **/
   @RBEntry("File")
   public static final String PRIVATE_CONSTANT_116 = "file.label";

   @RBEntry("F")
   public static final String PRIVATE_CONSTANT_117 = "file.shortcut";

   @RBEntry("New")
   public static final String PRIVATE_CONSTANT_118 = "file.new.label";

   @RBEntry("N")
   public static final String PRIVATE_CONSTANT_119 = "file.new.shortcut";

   @RBEntry("Part")
   public static final String PRIVATE_CONSTANT_120 = "file.new.part.label";

   @RBEntry("P")
   public static final String PRIVATE_CONSTANT_121 = "file.new.part.shortcut";

   @RBEntry("Exit")
   public static final String PRIVATE_CONSTANT_122 = "file.exit.label";

   @RBEntry("X")
   public static final String PRIVATE_CONSTANT_123 = "file.exit.shortcut";

   @RBEntry("Close")
   public static final String PRIVATE_CONSTANT_124 = "file.close.label";

   @RBEntry("Windchill")
   public static final String PRIVATE_CONSTANT_125 = "windchill.label";

   @RBEntry("Search Local")
   public static final String PRIVATE_CONSTANT_126 = "tools.searchlocal.label";

   @RBEntry("S")
   public static final String PRIVATE_CONSTANT_127 = "tools.searchlocal.shortcut";

   @RBEntry("Search")
   public static final String PRIVATE_CONSTANT_128 = "tools.searchenterprise.label";

   @RBEntry("A")
   public static final String PRIVATE_CONSTANT_129 = "tools.searchenterprise.shortcut";

   @RBEntry("Windchill Explorer")
   public static final String PRIVATE_CONSTANT_130 = "tools.windchillexplorer.label";

   @RBEntry("W")
   public static final String PRIVATE_CONSTANT_131 = "tools.windchillexplorer.shortcut";

   @RBEntry("Product Explorer")
   public static final String PRIVATE_CONSTANT_132 = "tools.productexplorer.label";

   @RBEntry("E")
   public static final String PRIVATE_CONSTANT_133 = "tools.productexplorer.shortcut";

   @RBEntry("Help")
   public static final String PRIVATE_CONSTANT_134 = "help.label";

   @RBEntry("H")
   public static final String PRIVATE_CONSTANT_135 = "help.shortcut";

   @RBEntry("Help Topics")
   public static final String PRIVATE_CONSTANT_136 = "help.helptopics.label";

   @RBEntry("L")
   public static final String PRIVATE_CONSTANT_137 = "help.helptopics.shortcut";

   @RBEntry("About Windchill")
   public static final String PRIVATE_CONSTANT_138 = "help.aboutwindchill.label";

   /**
    * PartExplorer info ---------------------------------------------------------
    **/
   @RBEntry("All Parts")
   public static final String PRIVATE_CONSTANT_139 = "partexplorer.explorer.tree.statusBarText";

   @RBEntry("Product Structure")
   public static final String PRIVATE_CONSTANT_140 = "partexplorer.explorer.tree.rootNodeText";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Latest")
   public static final String PRIVATE_CONSTANT_141 = "standardSectionLbl";

   @RBEntry("Baseline")
   public static final String PRIVATE_CONSTANT_142 = "baselineSectionLbl";

   @RBEntry("Effectivity")
   public static final String PRIVATE_CONSTANT_143 = "effectivitySectionLbl";

   @RBEntry("Effective Date:")
   public static final String PRIVATE_CONSTANT_144 = "effectiveDateLbl";

   @RBEntry("Configuration Item:")
   public static final String PRIVATE_CONSTANT_145 = "configurationItemLbl";

   @RBEntry("Value:")
   public static final String PRIVATE_CONSTANT_146 = "valueLbl";

   @RBEntry("Search...")
   public static final String PRIVATE_CONSTANT_147 = "searchButton";

   @RBEntry("Find...")
   public static final String PRIVATE_CONSTANT_148 = "findButton";

   /**
    * Messages -------------------------------------------------------------------
    **/
   @RBEntry("Please save before adding contents...")
   public static final String PRIVATE_CONSTANT_149 = "contentsPreSaveLbl";

   @RBEntry("The following error occurred while retrieving cabinets:  {0}")
   public static final String RETRIEVE_CABINET_ERROR = "0";

   @RBEntry("Please provide a value for \"{0}\".")
   public static final String NULL_ATTRIBUTE_VALUE = "1";

   @RBEntry("The attempt to rename the folder resulted in the following error: {0}")
   public static final String RENAME_FOLDER_ERROR = "2";

   @RBEntry("The following error occurred while trying to initiate the update task for {0}:  {1}")
   public static final String INITIATE_UPDATE_FAILED = "3";

   @RBEntry("The following error occurred while trying to initiate the create task for {0}:  {1}")
   public static final String INITIATE_CREATE_FAILED = "4";

   @RBEntry("The following error occurred while trying to initiate the view task for {0}:  {1}")
   public static final String INITIATE_VIEW_FAILED = "5";

   @RBEntry("The following error occurred while trying to initiate the delete task for {0}:  {1}")
   @RBArgComment0("Display identity of object being deleted")
   @RBArgComment1("Text from the exception caught while attempting to initiate the delete")
   public static final String INITIATE_DELETE_FAILED = "36";

   @RBEntry("The following error occurred while attempting to intialize the Enterprise Search URL: {0}")
   @RBArgComment0("Text from the exception caught trying to build the URL to the enterprise search")
   public static final String INIT_SEARCH_URL_FAILED = "6";

   @RBEntry("The property, \"{0}\", was not found in your wt.properties file.  This property is needed for creating the URL to the Enterprise Search.")
   @RBArgComment0("The name of the property which was not found in the wt.properties file - e.g. wt.clients.folderexplorer.debug")
   public static final String NO_SEARCH_URL = "7";

   @RBEntry("Deleting folder \"{0}\" will delete all of its contents as well. Continue deleting folder \"{0}\"?")
   @RBArgComment0("The name of the folder which is being deleted")
   public static final String CONFIRM_DELETE_FOLDER = "8";

   @RBEntry("All iterations in this version of \"{0}\" will be deleted.  Continue deleting \"{0}\"?")
   @RBArgComment0("The display identity of the part version which is being deleted")
   public static final String CONFIRM_DELETE_VERSIONS = "9";

   @RBEntry("Are you sure you want to delete the shortcut to {0} \"{1}\"?")
   public static final String CONFIRM_DELETE_SHORTCUT = "10";

   @RBEntry("Are you sure you want to delete \"{0}\"?")
   public static final String CONFIRM_DELETE_OBJECT = "11";

   @RBEntry("The following error occurred while attempting to delete \"{0}\": {1}")
   @RBArgComment0("The display identity of the object for which a delete was attempted")
   public static final String DELETE_FAILED = "12";

   @RBEntry("The following error occurred while attempting to get the URL for online help: {0}")
   public static final String INIT_HELP_URL_FAILED = "13";

   @RBEntry("You currently have \"{0}\" checked out.  Please update the working copy in your \"{1}\" folder.")
   public static final String UPDATE_WORKING_COPY = "14";

   @RBEntry("The following error occurred while attempting to submit \"{0}\" for approval:  {1}.")
   public static final String LIFECYCLE_SUBMIT_FAILED = "16";

   @RBEntry("{0} was submitted for approval successfully.")
   public static final String LIFECYCLE_SUBMIT_SUCCEEDED = "17";

   @RBEntry("The following error occurred while attempting to change folders: {0}.")
   public static final String CHANGE_FOLDERS_FAILED = "18";

   @RBEntry("The following error occurred while attempting to show the life cycle history for {0}: {1}.")
   public static final String LIFECYCLE_HISTORY_FAILED = "19";

   @RBEntry("The following error occurred while attempting to launch the task to update the role participants of {0}: {1}.")
   @RBArgComment0("The display identity of the object for which the update role participants task was initiated")
   @RBArgComment1("The text of the exception caught trying to initiate this task")
   public static final String AUGMENT_LIFECYCLE_FAILED = "20";

   @RBEntry("The following error occurred while attempting to reassign the life cycle for {0}: {1}.")
   public static final String REASSIGN_LIFECYCLE_FAILED = "21";

   @RBEntry("The following error occurred while attempting to reassign the team for {0}: {1}.")
   public static final String REASSIGN_TEAMTEMPLATE_FAILED = "22";

   @RBEntry("The Windchill Explorer could not be expanded to {0} - {1} was not found.")
   public static final String EXPAND_TO_OBJECT_FAILED = "23";

   @RBEntry("Currently, the Windchill Explorer does not support viewing of {0} objects.")
   public static final String VIEW_NOT_AVAILABLE = "26";

   @RBEntry("Currently, the Windchill Explorer does not support deleting of {0} objects.")
   public static final String DELETE_NOT_AVAILABLE = "37";

   @RBEntry("Currently, the Windchill Explorer does not support creating {0} objects.")
   public static final String CREATE_NOT_AVAILABLE = "24";

   @RBEntry("Currently, the Windchill Explorer does not support updating {0} objects.")
   public static final String UPDATE_NOT_AVAILABLE = "25";

   @RBEntry("You are not authorized to update {0}.")
   public static final String UPDATE_NOT_AUTHORIZED = "27";

   @RBEntry("Would you like to view {0}?")
   public static final String PROMPT_VIEW_OBJECT = "28";

   @RBEntry("An error occurred while attempting to launch the update task for {0}.  The object you are attempting to update no longer exists.")
   public static final String OBJECT_DOES_NOT_EXIST = "29";

   @RBEntry("Cabinets cannot be moved using the Windchill Explorer.")
   public static final String CANNOT_MOVE_CABINET = "30";

   @RBEntry("Cabinets cannot be deleted using the Windchill Explorer.")
   public static final String CANNOT_DELETE_CABINET = "31";

   @RBEntry("An error occurred while attempting to launch the task to change the identity for {0}: {1}.")
   public static final String INITIATE_CHANGE_IDENTITY_FAILED = "32";

   @RBEntry("You must be a submitter for the current life cycle phase in order to be able to reassign the life cycle for {0}.")
   public static final String LC_TEMPLATE_NOT_SUBMITTER = "33";

   @RBEntry("You must be a submitter for the current life cycle phase in order to be able to reassign the team for {0}.")
   public static final String PROJECT_NOT_SUBMITTER = "34";

   @RBEntry("You must be a submitter in order to be able to update the role participants in the life cycle of {0}.")
   public static final String AUGMENT_NOT_SUBMITTER = "35";

   @RBEntry("An error occurred trying to initialize the cache of icons: {0}")
   public static final String ICON_CACHE_ERROR = "38";

   @RBEntry("An error occurred while attempting to check out {0}: {1}")
   public static final String CHECK_OUT_FAILED = "39";

   @RBEntry("You already have {0} checked out.  The working copy of {0} is located in {1}.")
   @RBArgComment0("The display identity of the object which the user is trying to check out")
   @RBArgComment1("The folder location in which the working copy of the checked out object is stored. e.g. sahChecked Out")
   public static final String ALREADY_CHECKOUT_OWNER = "40";

   @RBEntry("An error occurred trying to retrieve your check-out folder: {1}.")
   public static final String RETRIEVE_CHECKOUT_FOLDER_FAILED = "41";

   @RBEntry("You need to have {0} checked out in order to update it.  Would you like to check out {0} now?")
   public static final String UPDATE_CHECK_OUT_PROMPT = "42";

   @RBEntry("{0} is currently checked out and cannot be updated.  Would you like to view {0}?")
   public static final String ALREADY_CHECKED_OUT_VIEW_PROMPT = "43";

   @RBEntry("\"{0}\" is not a valid value for property \"{1}\": {2}.")
   @RBArgComment0("The value which the user provided for a particular property")
   @RBArgComment1("The property for which the user provided an invalid value")
   @RBArgComment2("The exception text indicating why the value given is not valid")
   public static final String PROPERTY_VETO_EXCEPTION = "44";

   @RBEntry("An error occurred while attempting to initialize the online help system: {0}.")
   public static final String INITIALIZE_HELP_FAILED = "45";

   @RBEntry("An error occurred while localizing the Windchill Explorer.  The key, \"{0}\", is not a valid key in the resource bundle, \"{1}\".  Please contact your support staff.")
   public static final String RESOURCE_BUNDLE_ERROR = "46";

   /**
    * ProdMgmt specific messages -------------------------------------------------
    **/
   @RBEntry("No qualifying version was found for {0}.")
   @RBArgComment0("The display identity of the part master for which no versions could be selected")
   public static final String NO_QUALIFIED_VERSION = "100";

   @RBEntry("You cannot delete {0} because it is a working copy.")
   @RBArgComment0("The display identity of the part which the user is trying to delete")
   public static final String CANNOT_DELETE_WORKING_COPY = "101";

   @RBEntry("You cannot delete {0} because it is checked out.")
   @RBArgComment0("The name of the part for which the user is attempting to delete")
   public static final String CANNOT_DELETE_CHECKED_OUT = "102";

   @RBEntry("Part {0} is already displayed in the Product Information Explorer.")
   @RBArgComment0("The display identity of the part which is already opened in the Product Information Explorer")
   public static final String PART_ALREADY_DISPLAYED = "103";

   @RBEntry("Please enter a name for the part.")
   public static final String NO_NAME_ENTERED = "104";

   @RBEntry("Please enter a number for the part.")
   public static final String NO_NUMBER_ENTERED = "105";

   @RBEntry("Please enter a folder location for the part.")
   public static final String NO_LOCATION_ENTERED = "106";

   @RBEntry("The location given is not in your personal cabinet.")
   public static final String LOCATION_NOT_PERSONAL_CABINET = "107";

   @RBEntry("Location not valid")
   public static final String LOCATION_NOT_VALID = "108";

   @RBEntry("Required fields missing")
   public static final String REQUIRED_FIELDS_MISSING = "109";

   @RBEntry("Saving...")
   public static final String SAVING = "110";

   @RBEntry("Part {0} was successfully created.")
   public static final String CREATE_PART_SUCCESSFUL = "111";

   @RBEntry("Create part failed.")
   public static final String CREATE_PART_FAILED = "112";

   @RBEntry("Part {0} was successfully updated.")
   public static final String UPDATE_PART_SUCCESSFUL = "113";

   @RBEntry("Update part {0} failed.")
   public static final String UPDATE_PART_FAILURE = "114";

   @RBEntry("Save of part {0} failed.")
   public static final String SAVE_PART_FAILURE = "116";

   @RBEntry("Save of configuration {0} failed.")
   @RBArgComment0("product configuration identity")
   public static final String SAVE_CONFIGURATION_FAILURE = "131";

   @RBEntry("Please enter serial number for the Product Instance")
   public static final String NO_SERIAL_NUMBER_ENTERED = "132";

   @RBEntry("Save of product instance {0} failed.")
   @RBArgComment0("product instance identity")
   @RBArgComment1("The text from the exception caught while trying to delete the object")
   public static final String SAVE_INSTANCE_FAILURE = "133";

   @RBEntry("Please enter serial number for the Part.")
   public static final String NO_PART_SERIAL_NUMBER_ENTERED = "134";

   @RBEntry("Please select a Product Instance.")
   public static final String NO_PRODUCT_INSTANCE_CHOOSEN = "135";

   @RBEntry("Line Number {0} has been used more than once.  Please find the duplicate and resolve the problem.")
   @RBArgComment0("The line number that is in duplicate")
   public static final String DUPLICATE_LINE_NUMBER = "136";

   /**
    * ------------------------------------------------------------------------
    **/
   @RBEntry("invalid mode")
   public static final String INVALID_MODE = "115";

   @RBEntry(" {0} now references {1}")
   public static final String NOW_REFERENCES = "117";

   @RBEntry(" {0} now uses {1} of {2}")
   public static final String NOW_USES = "118";

   @RBEntry(" {0} references {1}")
   public static final String REFERENCES = "120";

   @RBEntry(" {0} does not reference {1} anymore")
   public static final String DOESNT_REFERENCE = "123";

   @RBEntry(" {0} uses {1}")
   public static final String USES = "119";

   @RBEntry(" Working ")
   public static final String WORKING = "121";

   @RBEntry(" {0} does not use {1} anymore")
   @RBArgComment0("The display identity of the parent part which used to use the child part")
   @RBArgComment1("The display identity of the child part which is no longer used by the parent part")
   public static final String DOESNT_USE = "122";

   @RBEntry("No part has been specified to update.")
   public static final String NO_PART_TO_UPDATE = "124";

   @RBEntry("The given object must be a WTPart in order to initiate this task. ")
   public static final String OBJECT_NOT_WTPART = "125";

   @RBEntry("Could not get PropertyDescriptor")
   public static final String NO_PROPERTY_DESCRIPTOR = "126";

   @RBEntry("Could not retrieve ClassInfo")
   public static final String NO_CLASSINFO = "127";

   @RBEntry(" {0} is not currently in a baseline.")
   @RBArgComment0("The display identity of the object (e.g. part) which the user is trying to remove from a baseline")
   public static final String NOT_IN_BASELINE = "128";

   @RBEntry("Please enter a name for the product configuration.")
   public static final String NO_CONFIGURATION_NAME_ENTERED = "130";

   @RBEntry("The Baseline name \"{0}\" is not unique.")
   public static final String BASELINE_NAME_NOT_UNIQUE = "155";

   @RBEntry("The Baseline name \"{0}\" is not valid.")
   public static final String BASELINE_NAME_NOT_VALID = "156";

   @RBEntry("The Configuration Item name \"{0}\" is not unique.")
   public static final String CONFIG_ITEM_NAME_NOT_UNIQUE = "157";

   @RBEntry("The Configuration Item \"{0}\" is not valid.")
   public static final String CONFIG_ITEM_NAME_NOT_VALID = "158";

   @RBEntry(" The top level part version has been changed from {0} to {1} to reflect the configuration specification used.")
   public static final String PART_VERSION_HAS_CHANGED = "129";

   @RBEntry("Search for Configuration Item")
   public static final String FIND_CONFIG_ITEM_TITLE = "159";

   @RBEntry("Search for Baseline")
   public static final String FIND_BASELINE_TITLE = "160";

   /**
    * ------------------------------------------------------------------------
    **/
   @RBEntry("Use for this session only")
   public static final String SAVE_CONFIG_SPEC_LABEL = "161";

   @RBEntry("Progress")
   public static final String PROGRESS_TITLE = "162";

   @RBEntry("The usage of {0} cannot be removed because it is described by {1}.")
   public static final String BUILT_LINK_CAN_NOT_BE_DELETED = "163";

   @RBEntry("The {0} cannot be changed because the usage of {1} is described by {2}.")
   public static final String BUILT_LINK_CAN_NOT_BE_UPDATED = "164";

   @RBEntry(" {0} builds {1}.")
   public static final String BUILD_RULE = "165";

   /**
    * -------------------------------------------------------------------------
    **/
   @RBEntry("The following error occurred while attempting to publish {0}: {1}")
   public static final String PUBLISH_PART_FAILED = "166";

   @RBEntry("Finding the objects describing {0}...")
   public static final String FINDING_DESCRIBES_LABEL = "167";

   @RBEntry("Finding attributes...")
   public static final String FINDING_ATTRIBUTES_LABEL = "168";

   /**
    * -------------------------------------------------------------------------
    **/
   @RBEntry("No qualifying version found for {0} using configuration specification {1}.  Would you like to set a different configuration specification?")
   public static final String NO_QUALIFIED_VERSION_PROMPT = "169";

   @RBEntry("Currently, the Product Information Explorer does not support part alternates.")
   public static final String ALTERNATES_NOT_AVAILABLE = "170";

   @RBEntry("The following error occurred while trying to initiate the define alternates task for {0}:  {1}")
   public static final String INITIATE_DEFINE_ALTERNATES_FAILED = "171";

   @RBEntry("The given object must be a WTPartMaster to initiate this task.")
   public static final String OBJECT_NOT_WTPARTMASTER = "172";

   @RBEntry("Currently, the Product Information Explorer does not support part substitutes.")
   public static final String SUBSTITUTES_NOT_AVAILABLE = "173";

   @RBEntry("The following error occurred while trying to initiate the define substitutes task for {0}:  {1}")
   public static final String INITIATE_DEFINE_SUBSTITUTES_FAILED = "174";

   @RBEntry("Substitutes cannot be defined for this top-level part because no assembly is specified.")
   public static final String ASSEMBLY_NEEDED_TO_DEFINE_SUBSTITUTES = "175";

   @RBEntry("The given object must be a WTPart or WTPartMaster initiate this task.")
   public static final String OBJ_NOT_PART_OR_PARTMASTER = "176";

   @RBEntry("No usage relationship could be found between the selected object and its parent.")
   public static final String NO_USAGE_RELATIONSHIP_FOUND = "177";

   @RBEntry("Search for Effectivity Context")
   public static final String FIND_EFF_CONTEXT_TITLE = "178";

   @RBEntry("Effectivity Context:")
   public static final String PRIVATE_CONSTANT_150 = "effectivityContextLbl";

   @RBEntry("The Replacements->Define Alternates menu item cannot be enabled due to the following error: {0}")
   public static final String CANNOT_ENABLE_DEFINE_ALTERNATES = "179";

   @RBEntry("The Replacements->Define Substitutes menu item cannot be enabled due to the following error: {0}")
   public static final String CANNOT_ENABLE_DEFINE_SUBSTITUTES = "180";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Product Manager")
   public static final String PRODUCT_MANAGER = "181";

   @RBEntry("Go to Windchill Home Page")
   public static final String GO_TO_HOME = "182";

   @RBEntry("Windchill")
   public static final String WINDCHILL = "183";

   @RBEntry("Product Structure Explorer")
   public static final String PRODUCT_INFORMATION_EXPLORER = "184";

   @RBEntry("The given object must be a WTProduct in order to initiate this task. ")
   public static final String OBJECT_NOT_WTPRODUCT = "185";

   @RBEntry("The given object must be a WTSerialNumberedPart in order to initiate this task. ")
   public static final String OBJECT_NOT_WTSERIALNUMBEREDPART = "186";

   @RBEntry("The given object must be a WTSerialNumberedPartMaster to initiate this task.")
   public static final String OBJECT_NOT_WTSERIALNUMBEREDPARTMASTER = "187";

   @RBEntry("The given object must be a WTProductMaster to initiate this task.")
   public static final String OBJECT_NOT_WTPRODUCTMASTER = "188";

   @RBEntry("Open")
   public static final String OPEN_OBJECT_TITLE = "189";

   @RBEntry("The following error occurred while trying to display the HTML page for updating role participants: {0}")
   public static final String UPDATE_ROLE_PARTICIPANTS_URL_ERROR = "190";

   @RBEntry("he following error occurred while trying to display the HTML page for showing role participants: {0}")
   public static final String SHOW_ROLE_PARTICIPANTS_URL_ERROR = "191";

   @RBEntry("The given object must be a WTProductConfiguration in order to initiate this task. ")
   public static final String OBJECT_NOT_WTPRODUCTCONFIGURATION = "192";

   @RBEntry("The following error occurred while trying to create a new product instance from {0}:  {1}")
   @RBArgComment0("identity of product configuration")
   @RBArgComment1("error message from server")
   public static final String LAUNCH_PRODUCT_INSTANCE_FAILED = "193";

   @RBEntry("The given object must be a WTProductInstance in order to initiate this task. ")
   public static final String OBJECT_NOT_WTPRODUCTINSTANCE = "194";

   @RBEntry("The location, {0}, is not in your personal cabinet.  Please provide a location in your personal cabinet.")
   @RBArgComment0("The value of the location entered by the user.  e.g. Design")
   public static final String LOCATION_NOT_PERSONAL_CABINET_ARG = "195";

   @RBEntry("Please enter a name for the annotation.")
   public static final String ANNOTATION_NAME_NEEDED = "196";

   @RBEntry("You have changed your preferred view in the default configuration specification setting.  Would you like to redisplay this product structure according to the new preferred view?")
   public static final String VIEW_PREFERENCE_UPDATED_SINGULAR = "197";

   @RBEntry("You have changed your preferred view in the default configuration specification setting.  Would you like to redisplay these product structures according to the new preferred view?")
   public static final String VIEW_PREFERENCE_UPDATED_PLURAL = "198";

   @RBEntry("Redisplay")
   public static final String UPDATE_CONFIG_SPEC_OK_BUTTON = "199";

   @RBEntry("Do Not Redisplay")
   public static final String UPDATE_CONFIG_SPEC_CANCEL_BUTTON = "200";

   @RBEntry("Checking out {0} ({1})...")
   @RBArgComment0("The number of the part being checked out")
   @RBArgComment1("The name of the part being checked out")
   public static final String CHECKOUT_PART_PROGRESS_TEXT = "201";

   @RBEntry("Checking out...")
   public static final String CHECKOUT_PROGRESS_TEXT = "202";

   @RBEntry("The annotation name must be 60 characters or less.")
   public static final String ANNOTATION_NAME_TOO_BIG = "203";

   @RBEntry("Please choose a life cycle for the part.")
   @RBComment("Message displayed to the user who attempts to save a new part without first choosing a life cycle (required).")
   public static final String NO_LIFE_CYCLE_CHOSEN = "204";

   @RBEntry("{0} is currently checked out to a project.  Would you like to view {0}?")
   @RBComment("Message received when user attempts to update an workable object that is currently checked out to a project.")
   public static final String CHECKED_OUT_TO_PROJECT_VIEW_PROMPT = "205";

   @RBEntry("Annotation")
   public static final String PRIVATE_CONSTANT_151 = "annotation.label";

   @RBEntry("A")
   public static final String PRIVATE_CONSTANT_152 = "annotation.shortcut";

   @RBEntry("Annotate Assembly...")
   public static final String PRIVATE_CONSTANT_153 = "annotation.annotateassembly.label";

   @RBEntry("Open Annotation Set...")
   public static final String PRIVATE_CONSTANT_154 = "annotation.openannotation.label";

   @RBEntry("Delete Annotation Set...")
   public static final String PRIVATE_CONSTANT_155 = "annotation.deleteannotation.label";

   @RBEntry("Create Product {0}")
   public static final String PRIVATE_CONSTANT_156 = "createProductTitle";

   @RBEntry("Create Serial Numbered Part {0}")
   public static final String PRIVATE_CONSTANT_157 = "createSerialNumberedPartTitle";

   @RBEntry("Open...")
   public static final String PRIVATE_CONSTANT_158 = "file.open.label";

   @RBEntry("O")
   public static final String PRIVATE_CONSTANT_159 = "file.open.shortcut";

   @RBEntry("Product")
   public static final String PRIVATE_CONSTANT_160 = "file.new.product.label";

   @RBEntry("T")
   public static final String PRIVATE_CONSTANT_161 = "file.new.product.shortcut";

   @RBEntry("Serial Numbered Part")
   public static final String PRIVATE_CONSTANT_162 = "file.new.serialNumberedPart.label";

   @RBEntry("S")
   public static final String PRIVATE_CONSTANT_163 = "file.new.serialNumberedPart.shortcut";

   @RBEntry("Life Cycle")
   public static final String PRIVATE_CONSTANT_164 = "lifecycle.label";

   @RBEntry("Reassign Life Cycle")
   public static final String PRIVATE_CONSTANT_165 = "lifecycle.reassignlifecycle.label";

   @RBEntry("Reassign Team")
   public static final String PRIVATE_CONSTANT_166 = "lifecycle.reassignteam.label";

   @RBEntry("Set State")
   public static final String PRIVATE_CONSTANT_167 = "lifecycle.setstate.label";

   @RBEntry("L")
   public static final String PRIVATE_CONSTANT_168 = "lifecycle.shortcut";

   @RBEntry("Submit")
   public static final String PRIVATE_CONSTANT_169 = "lifecycle.submit.label";

   @RBEntry("Update Team")
   public static final String PRIVATE_CONSTANT_170 = "lifecycle.updateroleparticipants.label";

   @RBEntry("Show Team")
   public static final String PRIVATE_CONSTANT_171 = "lifecycle.showroleparticipants.label";

   @RBEntry("Product Configuration Explorer")
   public static final String PRIVATE_CONSTANT_172 = "configurationTitle";

   @RBEntry("Product Instance Explorer")
   public static final String PRIVATE_CONSTANT_173 = "instanceTitle";

   @RBEntry("Add...")
   public static final String PRIVATE_CONSTANT_174 = "selected.baseline.add.label";

   @RBEntry("Baseline")
   public static final String PRIVATE_CONSTANT_175 = "selected.baseline.label";

   @RBEntry("Populate...")
   public static final String PRIVATE_CONSTANT_176 = "selected.baseline.populate.label";

   @RBEntry("Remove...")
   public static final String PRIVATE_CONSTANT_177 = "selected.baseline.remove.label";

   @RBEntry("Cancel Check Out")
   public static final String PRIVATE_CONSTANT_178 = "selected.cancelCheckOut.label";

   @RBEntry("C")
   public static final String PRIVATE_CONSTANT_179 = "selected.cancelCheckOut.shortcut";

   @RBEntry("Check In...")
   public static final String PRIVATE_CONSTANT_180 = "selected.checkIn.label";

   @RBEntry("I")
   public static final String PRIVATE_CONSTANT_181 = "selected.checkIn.shortcut";

   @RBEntry("Check Out")
   public static final String PRIVATE_CONSTANT_182 = "selected.checkOut.label";

   @RBEntry("K")
   public static final String PRIVATE_CONSTANT_183 = "selected.checkOut.shortcut";

   @RBEntry("Delete")
   public static final String PRIVATE_CONSTANT_184 = "selected.delete.label";

   @RBEntry("D")
   public static final String PRIVATE_CONSTANT_185 = "selected.delete.shortcut";

   @RBEntry("Save As...")
   public static final String PRIVATE_CONSTANT_186 = "selected.duplicate.label";

   @RBEntry("Selected")
   public static final String PRIVATE_CONSTANT_187 = "selected.label";

   @RBEntry("New View Version...")
   public static final String PRIVATE_CONSTANT_188 = "selected.newviewversion.label";

   @RBEntry("M")
   public static final String PRIVATE_CONSTANT_189 = "selected.newviewversion.shortcut";

   @RBEntry("Publish")
   public static final String PRIVATE_CONSTANT_190 = "selected.publish.label";

   @RBEntry("Rename...")
   public static final String PRIVATE_CONSTANT_191 = "selected.rename.label";

   @RBEntry("Define Alternates")
   public static final String PRIVATE_CONSTANT_192 = "selected.replacements.definealternates.label";

   @RBEntry("Define Substitutes")
   public static final String PRIVATE_CONSTANT_193 = "selected.replacements.definesubstitutes.label";

   @RBEntry("Replacements")
   public static final String PRIVATE_CONSTANT_194 = "selected.replacements.label";

   @RBEntry("Revise...")
   public static final String PRIVATE_CONSTANT_195 = "selected.revise.label";

   @RBEntry("R")
   public static final String PRIVATE_CONSTANT_196 = "selected.revise.shortcut";

   @RBEntry("New One-off Version...")
   public static final String PRIVATE_CONSTANT_197 = "selected.oneoffversion.label";

   @RBEntry("Set ERP Organization")
   public static final String PRIVATE_CONSTANT_198 = "selected.rtp.label";

   @RBEntry("Save As...")
   public static final String PRIVATE_CONSTANT_199 = "selected.saveas.label";

   @RBEntry("S")
   public static final String PRIVATE_CONSTANT_200 = "selected.shortcut";

   @RBEntry("Show Iteration History")
   public static final String PRIVATE_CONSTANT_201 = "selected.showiterationhistory.label";

   @RBEntry("Show Version History")
   public static final String PRIVATE_CONSTANT_202 = "selected.showversionhistory.label";

   @RBEntry("Undo Check Out")
   public static final String PRIVATE_CONSTANT_203 = "selected.undoCheckOut.label";

   @RBEntry("C")
   public static final String PRIVATE_CONSTANT_204 = "selected.undoCheckOut.shortcut";

   @RBEntry("Update")
   public static final String PRIVATE_CONSTANT_205 = "selected.update.label";

   @RBEntry("U")
   public static final String PRIVATE_CONSTANT_206 = "selected.update.shortcut";

   @RBEntry("View Properties Page")
   public static final String PRIVATE_CONSTANT_207 = "selected.view.label";

   @RBEntry("V")
   public static final String PRIVATE_CONSTANT_208 = "selected.view.shortcut";

   @RBEntry("Visualize Geometry")
   public static final String PRIVATE_CONSTANT_209 = "selected.visualize.label";

   @RBEntry("Define Distribution Targets")
   public static final String PRIVATE_CONSTANT_210 = "selected.esitargets.label";

   @RBEntry("History")
   public static final String PRIVATE_CONSTANT_211 = "tools.history.label";

   @RBEntry("Iteration History")
   public static final String PRIVATE_CONSTANT_212 = "tools.history.iterationhistory.label";

   @RBEntry("Life Cycle History")
   public static final String PRIVATE_CONSTANT_213 = "tools.history.lifecyclehistory.label";

   @RBEntry("Version History")
   public static final String PRIVATE_CONSTANT_214 = "tools.history.versionhistory.label";

   @RBEntry("Tools")
   public static final String PRIVATE_CONSTANT_215 = "tools.label";

   @RBEntry("T")
   public static final String PRIVATE_CONSTANT_216 = "tools.label.shortcut";

   @RBEntry("Multi-level BOM Comparison")
   public static final String PRIVATE_CONSTANT_217 = "tools.reports.bomcompare.label";

   @RBEntry("Indented BOM")
   public static final String PRIVATE_CONSTANT_218 = "tools.reports.indentedbom.label";

   @RBEntry("Reports")
   public static final String PRIVATE_CONSTANT_219 = "tools.reports.label";

   @RBEntry("Parts List")
   public static final String PRIVATE_CONSTANT_220 = "tools.reports.partslist.label";

   @RBEntry("Multi-level Where Used")
   public static final String PRIVATE_CONSTANT_221 = "tools.reports.whereused.label";

   @RBEntry("Update Product <{0}>")
   public static final String PRIVATE_CONSTANT_222 = "updateProductTitle";

   @RBEntry("Update Serial Numbered Part <{0}>")
   public static final String PRIVATE_CONSTANT_223 = "updateSerialNumberedPartTitle";

   @RBEntry("View")
   public static final String PRIVATE_CONSTANT_224 = "view.label";

   @RBEntry("V")
   public static final String PRIVATE_CONSTANT_225 = "view.shortcut";

   @RBEntry("Set Configuration Specification")
   public static final String PRIVATE_CONSTANT_226 = "view.configspec.label";

   @RBEntry("Show Occurrences")
   public static final String PRIVATE_CONSTANT_227 = "view.showoccurrences.label";

   @RBEntry("Set View Preference")
   public static final String PRIVATE_CONSTANT_228 = "view.setviewpreference.label";

   @RBEntry("Clear")
   public static final String PRIVATE_CONSTANT_229 = "view.clear.label";

   @RBEntry("Clear All")
   public static final String PRIVATE_CONSTANT_230 = "view.clearall.label";

   @RBEntry("Refresh")
   public static final String PRIVATE_CONSTANT_231 = "view.refresh.label";

   @RBEntry("h")
   public static final String PRIVATE_CONSTANT_232 = "view.refresh.shortcut";

   @RBEntry("Configuration")
   public static final String PRIVATE_CONSTANT_233 = "configuration.label";

   @RBEntry("C")
   public static final String PRIVATE_CONSTANT_234 = "configuration.shortcut";

   @RBEntry("New Product Configuration")
   public static final String PRIVATE_CONSTANT_235 = "configuration.newconfiguration.label";

   @RBEntry("C")
   public static final String PRIVATE_CONSTANT_236 = "configuration.newconfiguration.shortcut";

   @RBEntry("New Product Instance")
   public static final String PRIVATE_CONSTANT_237 = "configuration.newproductinstance.label";

   @RBEntry("I")
   public static final String PRIVATE_CONSTANT_238 = "configuration.newproductinstance.shortcut";

   @RBEntry("Assign Part Version")
   public static final String PRIVATE_CONSTANT_239 = "configuration.assignpartversion.label";

   @RBEntry("A")
   public static final String PRIVATE_CONSTANT_240 = "configuration.assignpartversion.shortcut";

   @RBEntry("Populate from Product Structure")
   public static final String PRIVATE_CONSTANT_241 = "configuration.populatefrompartstructure.label";

   @RBEntry("P")
   public static final String PRIVATE_CONSTANT_242 = "configuration.populatefrompartstructure.shortcut";

   @RBEntry("Assign Product Configuration")
   public static final String PRIVATE_CONSTANT_243 = "configuration.associateconfiguration.label";

   @RBEntry("Allocate")
   public static final String PRIVATE_CONSTANT_244 = "configuration.allocate.label";

   @RBEntry("l")
   public static final String PRIVATE_CONSTANT_245 = "configuration.allocate.shortcut";

   @RBEntry("Deallocate")
   public static final String PRIVATE_CONSTANT_246 = "configuration.deallocate.label";

   @RBEntry("D")
   public static final String PRIVATE_CONSTANT_247 = "configuration.deallocate.shortcut";

   @RBEntry("Product Configuration")
   public static final String PRIVATE_CONSTANT_248 = "configurationexplorer.explorer.tree.rootNodeText";

   @RBEntry("Product Configuration")
   public static final String PRIVATE_CONSTANT_249 = "configurationexplorer.explorer.tree.statusBarText";

   @RBEntry("Product Instance")
   public static final String PRIVATE_CONSTANT_250 = "instanceexplorer.explorer.tree.rootNodeText";

   @RBEntry("Product Instance")
   public static final String PRIVATE_CONSTANT_251 = "instanceexplorer.explorer.tree.statusBarText";

   /**
    * -----------------------------------------------------------------------------
    * Labels and Messages used to support reference designators and occurrences
    * -----------------------------------------------------------------------------
    **/
   @RBEntry("Edit Occurrences...")
   public static final String OCCURRENCES_BUTTON_LABEL = "OCCURRENCES_BUTTON_LABEL";

   @RBEntry("<Not named>")
   public static final String UNNAMED_OCCURRENCE_LABEL = "UNNAMED_OCCURRENCE_LABEL";

   @RBEntry("Reference Designator")
   public static final String REFERENCE_DESIGNATOR_LABEL = "REFERENCE_DESIGNATOR_LABEL";

   @RBEntry("Reference Designator:")
   public static final String REFERENCE_DESIGNATOR_FIELD_LABEL = "REFERENCE_DESIGNATOR_FIELD_LABEL";

   @RBEntry("<New Assembly>")
   public static final String NEW_ASSEMBLY_LABEL = "NEW_ASSEMBLY_LABEL";

   @RBEntry("Quantity:")
   public static final String QUANTITY_LABEL = "QUANTITY_LABEL";

   @RBEntry("Used in assembly:")
   public static final String USED_IN_ASSEMBLY_LABEL = "USED_IN_ASSEMBLY_LABEL";

   @RBEntry("Part Details")
   public static final String PART_DETAILS_LABEL = "PART_DETAILS_LABEL";

   @RBEntry("Occurrence Details")
   public static final String OCCURRENCES_DETAILS_LABEL = "OCCURRENCES_DETAILS_LABEL";

   @RBEntry("Edit...")
   public static final String EDIT_BUTTON_LABEL = "EDIT_BUTTON_LABEL";

   @RBEntry("An occurrence with reference designator \"{0}\" already exists.  Please provide a different reference designator for this occurrence.")
   @RBArgComment0("The reference designator (name of the occurrence)")
   public static final String REFERENCE_DESIGNATOR_NOT_UNIQUE = "REFERENCE_DESIGNATOR_NOT_UNIQUE";

   @RBEntry("Another usage in this assembly already contains an occurrence with reference designator \"{0}\".  Reference designators must be unique across all occurrences of all usages in this assembly. Please provide a different reference designator for this occurrence.")
   @RBArgComment0("The reference designator (name of the occurrence)")
   public static final String REFERENCE_DESIGNATOR_NOT_UNIQUE_IN_ASM = "REFERENCE_DESIGNATOR_NOT_UNIQUE_IN_ASM";

   @RBEntry("Add Occurrence")
   public static final String ADD_OCCURRENCE_TITLE = "ADD_OCCURRENCE_TITLE";

   @RBEntry("{0} - Edit Occurrence")
   public static final String EDIT_OCCURRENCE_TITLE = "EDIT_OCCURRENCE_TITLE";

   @RBEntry("{0} {1}")
   @RBComment("Displays the quantity and units of a part used.  For example, 4 each")
   @RBArgComment0("The quantity of the part uses, e.g. 4")
   @RBArgComment1("The units describing the quantity, e.g. each")
   public static final String QUANTITY_UNITS_DISPLAY = "QUANTITY_UNITS_DISPLAY";

   @RBEntry("{0} ({1}) Version {2}, {3}")
   @RBComment("Example: Engine Body (189320) Version A.1, Engineering View")
   @RBArgComment0("Name of the assembly")
   @RBArgComment1("Number of the assembly")
   @RBArgComment2("Version letter")
   @RBArgComment3("View")
   public static final String ASSEMBLY_DISPLAY_IDENTITY = "ASSEMBLY_DISPLAY_IDENTITY";

   @RBEntry("{0} View")
   @RBComment("Display for view, example: Engineering View")
   @RBArgComment0("View name")
   public static final String VIEW_DISPLAY = "VIEW_DISPLAY";

   @RBEntry("When the units for the part used are \"{0}\", the quantity used must be an integer.  Please enter an integer for the quantity.")
   @RBComment("Error message displayed when user attempts to enter a decimal for quantity.")
   @RBArgComment0("The units - e.g. Each (For example, how much quantity of a material would you need: 2 meters, 2 kilos, 2 each.)")
   public static final String QUANTITY_NOT_INTEGER = "QUANTITY_NOT_INTEGER";

   @RBEntry("* {0}")
   @RBComment("Creates a required field by appending an asterisk and space to the given label")
   @RBArgComment0("The label to which an asterisk is prepended")
   public static final String REQUIRED_LABEL = "REQUIRED_LABEL";

   @RBEntry("All of the occurrences of {0} ({1}) have reference designators.  To decrement the quantity of {0} used, please select the occurrences to be removed.")
   @RBArgComment0("The name of the part whose usage quantity is being decremented")
   @RBArgComment1("The number of the part whose usage quantity is being decremented")
   public static final String CHOOSE_OCCURRENCES_PROMPT = "CHOOSE_OCCURRENCES_PROMPT";

   @RBEntry("Choose Occurrences to Remove")
   public static final String CHOOSE_OCCURRENCES_DIALOG_TITLE = "CHOOSE_OCCURRENCES_DIALOG_TITLE";

   @RBEntry("Some of the occurrences of {0} ({1}) have reference designators.  To decrement the quantity of {0} used to {2} {3}, at least {4} of the occurrences with reference designators must be removed.  Please select the occurrences to be removed.")
   public static final String CHOOSE_SOME_OCCURRENCES_PROMPT = "CHOOSE_SOME_OCCURRENCES_PROMPT";

   @RBEntry("Occurrences")
   public static final String OCCURRENCES_TITLE = "OCCURRENCES_TITLE";
}
