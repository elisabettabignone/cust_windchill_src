/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.prodmgmt;

import wt.util.resource.*;

@RBUUID("wt.clients.prodmgmt.ProdMgmtRB")
public final class ProdMgmtRB_it extends WTListResourceBundle {
   /**
    * Labels ------------------------------------------------------------------
    **/
   @RBEntry("Data creazione:")
   public static final String PRIVATE_CONSTANT_0 = "createdLbl";

   @RBEntry("Autore:")
   public static final String PRIVATE_CONSTANT_1 = "createdByLbl";

   @RBEntry("Data creazione:")
   public static final String PRIVATE_CONSTANT_2 = "createdOnLbl";

   @RBEntry("Origine:")
   public static final String PRIVATE_CONSTANT_3 = "sourceLbl";

   @RBEntry("Posizione:")
   public static final String PRIVATE_CONSTANT_4 = "locationLbl";

   @RBEntry("ID organizzazione:")
   public static final String orgIdLbl = "orgIdLbl";

   @RBEntry("Nome:")
   public static final String nameLbl = "nameLbl";

   @RBEntry("Numero:")
   public static final String numberLbl = "numberLbl";

   @RBEntry("Stato:")
   public static final String PRIVATE_CONSTANT_5 = "statusLbl";

   @RBEntry("Tipo:")
   public static final String PRIVATE_CONSTANT_6 = "typeLbl";

   @RBEntry("Nome cartella:")
   public static final String PRIVATE_CONSTANT_7 = "folderNameLbl";

   @RBEntry("Autore modifiche:")
   public static final String PRIVATE_CONSTANT_8 = "updatedByLbl";

   @RBEntry("Data ultima modifica:")
   public static final String PRIVATE_CONSTANT_9 = "updatedOnLbl";

   @RBEntry("Versione:")
   public static final String PRIVATE_CONSTANT_10 = "revisionLbl";

   @RBEntry("Iterazione:")
   public static final String PRIVATE_CONSTANT_11 = "iterationLbl";

   @RBEntry("Progetto:")
   public static final String PRIVATE_CONSTANT_12 = "projectLbl";

   @RBEntry("Ciclo di vita:")
   public static final String PRIVATE_CONSTANT_13 = "lifecycleLbl";

   @RBEntry("Stato del ciclo di vita:")
   public static final String PRIVATE_CONSTANT_14 = "stateLbl";

   @RBEntry("Vista:")
   public static final String PRIVATE_CONSTANT_15 = "viewLbl";

   @RBEntry("Includi le parti nel mio schedario personale")
   public static final String PRIVATE_CONSTANT_16 = "checkedOutLbl";

   @RBEntry("Qtà:")
   public static final String PRIVATE_CONSTANT_17 = "quantityLbl";

   @RBEntry("Unità:")
   public static final String PRIVATE_CONSTANT_18 = "unitLbl";

   @RBEntry("Riga:")
   public static final String PRIVATE_CONSTANT_19 = "lineNumberLbl";

   @RBEntry("Riga:")
   public static final String PRIVATE_CONSTANT_20 = "editLineNumberLbl";

   @RBEntry("Elaborazione in corso...")
   public static final String PRIVATE_CONSTANT_21 = "workingLbl";

   @RBEntry("Ricerca componenti in corso...")
   public static final String PRIVATE_CONSTANT_22 = "findingUsesLbl";

   @RBEntry("Ricerca dei riferimenti...")
   public static final String PRIVATE_CONSTANT_23 = "findingReferencesLbl";

   @RBEntry("Descrizione:")
   public static final String PRIVATE_CONSTANT_24 = "descriptionLbl";

   @RBEntry("Completa con le versioni della parte")
   public static final String PRIVATE_CONSTANT_25 = "populateLbl";

   @RBEntry("Configurazione:")
   public static final String PRIVATE_CONSTANT_26 = "configurationLbl";

   @RBEntry("Nome parte:")
   public static final String PRIVATE_CONSTANT_27 = "partNameLbl";

   @RBEntry("Numero di parte:")
   public static final String PRIVATE_CONSTANT_28 = "partNumberLbl";

   @RBEntry("Consenti riassegnazione")
   public static final String PRIVATE_CONSTANT_29 = "allowReassignmentLbl";

   @RBEntry("Versione parte:")
   public static final String PRIVATE_CONSTANT_30 = "partVersionsLbl";

   @RBEntry("Numero di serie:")
   public static final String PRIVATE_CONSTANT_31 = "serialNumberLbl";

   @RBEntry("Gruppi di annotazioni:")
   public static final String PRIVATE_CONSTANT_32 = "annotationsLbl";

   @RBEntry("Parte:")
   public static final String PRIVATE_CONSTANT_33 = "openAnnotationForPartLbl";

   @RBEntry("Parte:")
   public static final String PRIVATE_CONSTANT_34 = "annotateAssemblyForPartLbl";

   @RBEntry("Istanza di prodotto:")
   @RBComment("Associate Product Configuration Dialog, serial number field from Product Instance")
   public static final String PRIVATE_CONSTANT_35 = "productInstanceLbl";

   @RBEntry("Seleziona l'istanza di prodotto")
   @RBComment("Allocate ProductInstance Dialog, label for list of product instances")
   public static final String PRIVATE_CONSTANT_36 = "productInstancesLbl";

   @RBEntry("Sottoprodotto")
   @RBComment("Allocate ProductInstance Dialog, label for sub product")
   public static final String PRIVATE_CONSTANT_37 = "subProductLbl";

   @RBEntry("Configurazioni valide di prodotto:")
   @RBComment("Associate Product Configuration Dialog, label for list of configurations")
   public static final String PRIVATE_CONSTANT_38 = "validProductConfigurationLbl";

   @RBEntry("*N/S:")
   @RBComment("Serial Number Label")
   public static final String PRIVATE_CONSTANT_39 = "snLbl";

   /**
    * Button Labels -------------------------------------------------------------
    **/
   @RBEntry("Aggiungi...")
   public static final String addButton = "addButton";

   @RBEntry("Sfoglia...")
   public static final String PRIVATE_CONSTANT_40 = "browseButton";

   @RBEntry("Annulla")
   public static final String cancelButton = "cancelButton";

   @RBEntry("Check-In")
   public static final String PRIVATE_CONSTANT_41 = "checkinButton";

   @RBEntry("Check-Out")
   public static final String PRIVATE_CONSTANT_42 = "checkoutButton";

   @RBEntry("Reimposta")
   public static final String PRIVATE_CONSTANT_43 = "clearButton";

   @RBEntry("Chiudi")
   public static final String PRIVATE_CONSTANT_44 = "closeButton";

   @RBEntry("Continua")
   public static final String PRIVATE_CONSTANT_45 = "continueButton";

   @RBEntry("Elimina")
   public static final String PRIVATE_CONSTANT_46 = "deleteButton";

   @RBEntry("Visualizza")
   public static final String PRIVATE_CONSTANT_47 = "getButton";

   @RBEntry("Guida")
   public static final String helpButton = "helpButton";

   @RBEntry("OK")
   public static final String okButton = "okButton";

   @RBEntry("Riferimenti")
   public static final String PRIVATE_CONSTANT_48 = "referencesButton";

   @RBEntry("Contenuto")
   public static final String PRIVATE_CONSTANT_49 = "contentsButton";

   @RBEntry("Componenti")
   public static final String PRIVATE_CONSTANT_50 = "usesButton";

   @RBEntry("Effettività")
   public static final String PRIVATE_CONSTANT_51 = "effectivityButton";

   @RBEntry("Aggiorna")
   public static final String PRIVATE_CONSTANT_52 = "updateButton";

   @RBEntry("Salva")
   public static final String PRIVATE_CONSTANT_53 = "saveButton";

   @RBEntry("Rimuovi")
   public static final String removeButton = "removeButton";

   @RBEntry("Visualizza")
   public static final String PRIVATE_CONSTANT_54 = "viewButton";

   @RBEntry("Modifica numero di riga")
   public static final String PRIVATE_CONSTANT_55 = "editLineNumberButton";

   @RBEntry("Numero di riga successivo")
   public static final String PRIVATE_CONSTANT_56 = "nextLineButton";

   @RBEntry("Mostra versioni")
   public static final String PRIVATE_CONSTANT_57 = "showVersionsButton";

   @RBEntry("Modifica attributi")
   public static final String PRIVATE_CONSTANT_58 = "editAttributesButton";

   /**
    * MultiList column headings -------------------------------------------------
    **/
   @RBEntry("Numero")
   public static final String PRIVATE_CONSTANT_59 = "numberHeading";

   @RBEntry("Riga")
   public static final String PRIVATE_CONSTANT_60 = "lineNumberHeading";

   @RBEntry("Nome")
   public static final String PRIVATE_CONSTANT_61 = "nameHeading";

   @RBEntry("Qtà")
   public static final String PRIVATE_CONSTANT_62 = "quantityHeading";

   @RBEntry("Unità")
   public static final String PRIVATE_CONSTANT_63 = "unitHeading";

   @RBEntry("Descritto con")
   public static final String PRIVATE_CONSTANT_64 = "managedByHeading";

   @RBEntry("Descritto con")
   public static final String PRIVATE_CONSTANT_65 = "buildRuleButton";

   @RBEntry("Ricerca dei documenti descrittivi in corso...")
   public static final String PRIVATE_CONSTANT_66 = "findingBuildRuleLbl";

   @RBEntry("Descritto con:")
   public static final String PRIVATE_CONSTANT_67 = "buildRuleTitle";

   @RBEntry("Proprietario: {0}")
   public static final String PRIVATE_CONSTANT_68 = "ownedBy";

   @RBEntry("Dimensione")
   public static final String PRIVATE_CONSTANT_69 = "sizeHeading";

   @RBEntry("Stato")
   public static final String PRIVATE_CONSTANT_70 = "statusHeading";

   @RBEntry("Versione")
   public static final String PRIVATE_CONSTANT_71 = "versionHeading";

   @RBEntry("Tipo")
   public static final String PRIVATE_CONSTANT_72 = "typeHeading";

   @RBEntry("ID")
   public static final String PRIVATE_CONSTANT_73 = "idHeading";

   @RBEntry("Identificativo")
   public static final String PRIVATE_CONSTANT_74 = "identityHeading";

   @RBEntry("Revisione")
   public static final String PRIVATE_CONSTANT_75 = "revisionHeading";

   @RBEntry("Stato del ciclo di vita")
   public static final String PRIVATE_CONSTANT_76 = "stateHeading";

   @RBEntry("Visualizza")
   public static final String PRIVATE_CONSTANT_77 = "viewHeading";

   /**
    * Checkbox labels ------------------------------------------------------------
    **/
   @RBEntry("Non scaricare ora")
   public static final String PRIVATE_CONSTANT_78 = "doNotDownloadCheckBox";

   @RBEntry("Mantieni in stato di Check-Out")
   public static final String PRIVATE_CONSTANT_79 = "keepCheckedOut";

   /**
    * Messages ------------------------------------------------------------------
    * Panels & Tabs -------------------------------------------------------------
    * 
    **/
   @RBEntry("Raccolte di documenti disponibili")
   public static final String PRIVATE_CONSTANT_80 = "AvailableCollections";

   /**
    * Symbols -------------------------------------------------------------------
    * 
    **/
   @RBEntry("...")
   public static final String PRIVATE_CONSTANT_81 = "ellipses";

   @RBEntry("*")
   public static final String PRIVATE_CONSTANT_82 = "required";

   /**
    * Titles --------------------------------------------------------------------
    **/
   @RBEntry("Componenti:")
   public static final String PRIVATE_CONSTANT_83 = "usesTitle";

   @RBEntry("Impiegato da:")
   public static final String PRIVATE_CONSTANT_84 = "usedByTitle";

   @RBEntry("Contenuto:")
   public static final String PRIVATE_CONSTANT_85 = "contentsTitle";

   @RBEntry("Riferimenti:")
   public static final String PRIVATE_CONSTANT_86 = "referencesTitle";

   @RBEntry("Operazioni di modifica:")
   public static final String PRIVATE_CONSTANT_87 = "changesTitle";

   @RBEntry("Imposta specifica di configurazione")
   public static final String PRIVATE_CONSTANT_88 = "configSpecTitle";

   @RBEntry("Navigatore struttura di prodotto")
   public static final String PRIVATE_CONSTANT_89 = "pieTitle";

   @RBEntry("Crea parte {0}")
   public static final String PRIVATE_CONSTANT_90 = "createPartTitle";

   @RBEntry("Crea configurazione di prodotto")
   public static final String PRIVATE_CONSTANT_91 = "createConfigurationTitle";

   @RBEntry("Crea istanza di prodotto")
   public static final String PRIVATE_CONSTANT_92 = "createInstanceTitle";

   @RBEntry("Salva con nome: <{0}>")
   @RBArgComment0("object being duplicated")
   public static final String PRIVATE_CONSTANT_93 = "duplicateInstanceTitle";

   @RBEntry("Salva con nome: <{0}>")
   @RBArgComment0("object being duplicated")
   public static final String PRIVATE_CONSTANT_94 = "duplicateInstanceHeader";

   @RBEntry("Crea istanza di prodotto per {0}")
   @RBArgComment0("product label")
   public static final String PRIVATE_CONSTANT_95 = "createConfigurationHeader";

   @RBEntry("Crea istanza di prodotto per {0}")
   @RBArgComment0("product configuration label")
   public static final String PRIVATE_CONSTANT_96 = "createInstanceHeader";

   @RBEntry("Aggiorna parte <{0}>")
   public static final String PRIVATE_CONSTANT_97 = "updatePartTitle";

   @RBEntry("Visualizza parte <{0}>")
   public static final String PRIVATE_CONSTANT_98 = "viewPartTitle";

   @RBEntry("Copia in modifica di {0}")
   public static final String PRIVATE_CONSTANT_99 = "workingCopyTitle";

   @RBEntry("Trova parte")
   public static final String PRIVATE_CONSTANT_100 = "findPartTitle";

   @RBEntry("Trova documento")
   public static final String PRIVATE_CONSTANT_101 = "findDocTitle";

   @RBEntry("In stato di Check-In")
   public static final String PRIVATE_CONSTANT_102 = "checkedIn";

   @RBEntry("Sottoposto a Check-Out")
   public static final String PRIVATE_CONSTANT_103 = "checkedOut";

   @RBEntry("Check-Out effettuato da {0}")
   public static final String PRIVATE_CONSTANT_104 = "checkedOutBy";

   @RBEntry("Non sottoposto a Check-Out")
   public static final String PRIVATE_CONSTANT_105 = "notCheckedOut";

   @RBEntry("Copia in modifica")
   public static final String PRIVATE_CONSTANT_106 = "workingCopy";

   @RBEntry("Assegna versione alla parte")
   public static final String PRIVATE_CONSTANT_107 = "assignPartTitle";

   @RBEntry("Completa in base alla struttura di prodotto")
   public static final String PRIVATE_CONSTANT_108 = "populateFromTitle";

   @RBEntry("Imposta preferenze vista")
   public static final String PRIVATE_CONSTANT_109 = "setViewPreferenceTitle";

   @RBEntry("Apri gruppo di annotazioni")
   public static final String PRIVATE_CONSTANT_110 = "openAnnotationTitle";

   @RBEntry("Elimina gruppo di annotazioni")
   public static final String PRIVATE_CONSTANT_111 = "deleteAnnotationTitle";

   @RBEntry("Annota assieme")
   public static final String PRIVATE_CONSTANT_112 = "annotateAssemblyTitle";

   @RBEntry("Associa configurazione di prodotto")
   public static final String PRIVATE_CONSTANT_113 = "associateConfigurationTitle";

   @RBEntry("Alloca")
   public static final String PRIVATE_CONSTANT_114 = "allocateTitle";

   @RBEntry("Modifica numero di riga")
   public static final String PRIVATE_CONSTANT_115 = "editLineDialogTitle";

   /**
    * Menus ---------------------------------------------------------------------
    **/
   @RBEntry("File")
   public static final String PRIVATE_CONSTANT_116 = "file.label";

   @RBEntry("F")
   public static final String PRIVATE_CONSTANT_117 = "file.shortcut";

   @RBEntry("Nuovo")
   public static final String PRIVATE_CONSTANT_118 = "file.new.label";

   @RBEntry("N")
   public static final String PRIVATE_CONSTANT_119 = "file.new.shortcut";

   @RBEntry("Parte")
   public static final String PRIVATE_CONSTANT_120 = "file.new.part.label";

   @RBEntry("P")
   public static final String PRIVATE_CONSTANT_121 = "file.new.part.shortcut";

   @RBEntry("Esci")
   public static final String PRIVATE_CONSTANT_122 = "file.exit.label";

   @RBEntry("X")
   public static final String PRIVATE_CONSTANT_123 = "file.exit.shortcut";

   @RBEntry("Chiudi")
   public static final String PRIVATE_CONSTANT_124 = "file.close.label";

   @RBEntry("Windchill")
   public static final String PRIVATE_CONSTANT_125 = "windchill.label";

   @RBEntry("Ricerca locale")
   public static final String PRIVATE_CONSTANT_126 = "tools.searchlocal.label";

   @RBEntry("S")
   public static final String PRIVATE_CONSTANT_127 = "tools.searchlocal.shortcut";

   @RBEntry("Ricerca")
   public static final String PRIVATE_CONSTANT_128 = "tools.searchenterprise.label";

   @RBEntry("A")
   public static final String PRIVATE_CONSTANT_129 = "tools.searchenterprise.shortcut";

   @RBEntry("Navigatore Windchill")
   public static final String PRIVATE_CONSTANT_130 = "tools.windchillexplorer.label";

   @RBEntry("W")
   public static final String PRIVATE_CONSTANT_131 = "tools.windchillexplorer.shortcut";

   @RBEntry("Navigatore prodotto")
   public static final String PRIVATE_CONSTANT_132 = "tools.productexplorer.label";

   @RBEntry("E")
   public static final String PRIVATE_CONSTANT_133 = "tools.productexplorer.shortcut";

   @RBEntry("Guida")
   public static final String PRIVATE_CONSTANT_134 = "help.label";

   @RBEntry("G")
   public static final String PRIVATE_CONSTANT_135 = "help.shortcut";

   @RBEntry("Argomenti della Guida")
   public static final String PRIVATE_CONSTANT_136 = "help.helptopics.label";

   @RBEntry("L")
   public static final String PRIVATE_CONSTANT_137 = "help.helptopics.shortcut";

   @RBEntry("Informazioni su Windchill")
   public static final String PRIVATE_CONSTANT_138 = "help.aboutwindchill.label";

   /**
    * PartExplorer info ---------------------------------------------------------
    **/
   @RBEntry("Tutte le parti")
   public static final String PRIVATE_CONSTANT_139 = "partexplorer.explorer.tree.statusBarText";

   @RBEntry("Struttura di prodotto")
   public static final String PRIVATE_CONSTANT_140 = "partexplorer.explorer.tree.rootNodeText";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Più recente")
   public static final String PRIVATE_CONSTANT_141 = "standardSectionLbl";

   @RBEntry("Baseline")
   public static final String PRIVATE_CONSTANT_142 = "baselineSectionLbl";

   @RBEntry("Effettività")
   public static final String PRIVATE_CONSTANT_143 = "effectivitySectionLbl";

   @RBEntry("Effective Date:")
   public static final String PRIVATE_CONSTANT_144 = "effectiveDateLbl";

   @RBEntry("Configuration item:")
   public static final String PRIVATE_CONSTANT_145 = "configurationItemLbl";

   @RBEntry("Valore:")
   public static final String PRIVATE_CONSTANT_146 = "valueLbl";

   @RBEntry("Cerca...")
   public static final String PRIVATE_CONSTANT_147 = "searchButton";

   @RBEntry("Trova...")
   public static final String PRIVATE_CONSTANT_148 = "findButton";

   /**
    * Messages -------------------------------------------------------------------
    **/
   @RBEntry("Salvare prima di aggiungere il contenuto...")
   public static final String PRIVATE_CONSTANT_149 = "contentsPreSaveLbl";

   @RBEntry("Errore durante il recupero degli schedari: {0}")
   public static final String RETRIEVE_CABINET_ERROR = "0";

   @RBEntry("Fornire un valore per \"{0}\".")
   public static final String NULL_ATTRIBUTE_VALUE = "1";

   @RBEntry("Errore durante la ridenominazione della cartella:  {0}")
   public static final String RENAME_FOLDER_ERROR = "2";

   @RBEntry("Errore durante l'avvio del task di aggiornamento per {0}:  {1}")
   public static final String INITIATE_UPDATE_FAILED = "3";

   @RBEntry("Errore durante l'avvio del task di creazione per {0}:  {1}")
   public static final String INITIATE_CREATE_FAILED = "4";

   @RBEntry("Errore durante l'avvio del task di visualizzazione per {0}:  {1}")
   public static final String INITIATE_VIEW_FAILED = "5";

   @RBEntry("Errore durante l'avvio del task di eliminazione per {0}:  {1}")
   @RBArgComment0("Display identity of object being deleted")
   @RBArgComment1("Text from the exception caught while attempting to initiate the delete")
   public static final String INITIATE_DELETE_FAILED = "36";

   @RBEntry("Errore durante l'inizializzazione dell'URL per la Ricerca globale: {0}")
   @RBArgComment0("Text from the exception caught trying to build the URL to the enterprise search")
   public static final String INIT_SEARCH_URL_FAILED = "6";

   @RBEntry("Impossibile trovare la proprietà \"{0}\" in nel file wt.properties. Questa proprietà è necessaria per la creazione dell'URL per la Ricerca globale.")
   @RBArgComment0("The name of the property which was not found in the wt.properties file - e.g. wt.clients.folderexplorer.debug")
   public static final String NO_SEARCH_URL = "7";

   @RBEntry("Eliminando la cartella \"{0}\" verrà eliminato anche tutto il relativo contenuto. Eliminare la cartella \"{0}\"?")
   @RBArgComment0("The name of the folder which is being deleted")
   public static final String CONFIRM_DELETE_FOLDER = "8";

   @RBEntry("Tutte le iterazioni di questa versione di \"{0}\" verranno eliminate. Eliminare \"{0}\"?")
   @RBArgComment0("The display identity of the part version which is being deleted")
   public static final String CONFIRM_DELETE_VERSIONS = "9";

   @RBEntry("Eliminare il collegamento a {0} \"{1}\"?")
   public static final String CONFIRM_DELETE_SHORTCUT = "10";

   @RBEntry("Eliminare \"{0}\"?")
   public static final String CONFIRM_DELETE_OBJECT = "11";

   @RBEntry("Errore durante l'eliminazione di \"{0}\": {1}")
   @RBArgComment0("The display identity of the object for which a delete was attempted")
   public static final String DELETE_FAILED = "12";

   @RBEntry("Errore durante la ricerca dell'URL per la Guida in linea: {0}")
   public static final String INIT_HELP_URL_FAILED = "13";

   @RBEntry("Il documento \"{0}\" è attualmente sottoposto a Check-Out dall'utente. Aggiornare la copia in modifica nella cartella \"{1}\".")
   public static final String UPDATE_WORKING_COPY = "14";

   @RBEntry("Errore durante l'invio di \"{0}\" per approvazione:  {1}.")
   public static final String LIFECYCLE_SUBMIT_FAILED = "16";

   @RBEntry("{0} è stato sottoposto all'approvazione.")
   public static final String LIFECYCLE_SUBMIT_SUCCEEDED = "17";

   @RBEntry("Errore durante la modifica delle cartelle: {0}.")
   public static final String CHANGE_FOLDERS_FAILED = "18";

   @RBEntry("Errore durante la visualizzazione della cronologia del ciclo di vita per {0}: {1}.")
   public static final String LIFECYCLE_HISTORY_FAILED = "19";

   @RBEntry("Errore durante l'avvio del task di aggiornamento dei partecipanti dei ruoli di {0}: {1}.")
   @RBArgComment0("The display identity of the object for which the update role participants task was initiated")
   @RBArgComment1("The text of the exception caught trying to initiate this task")
   public static final String AUGMENT_LIFECYCLE_FAILED = "20";

   @RBEntry("Errore durante il tentativo di riassegnazione al ciclo di vita per {0}: {1}.")
   public static final String REASSIGN_LIFECYCLE_FAILED = "21";

   @RBEntry("Errore durante il tentativo di riassegnare il team per {0}: {1}.")
   public static final String REASSIGN_TEAMTEMPLATE_FAILED = "22";

   @RBEntry("Impossibile espandere il Navigatore Windchill a {0} - Impossibile trovare {1}.")
   public static final String EXPAND_TO_OBJECT_FAILED = "23";

   @RBEntry("Al momento, il Navigatore Windchill non supporta la visualizzazione degli oggetti {0}.")
   public static final String VIEW_NOT_AVAILABLE = "26";

   @RBEntry("Al momento, il Navigatore Windchill non supporta l'eliminazione degli oggetti {0}.")
   public static final String DELETE_NOT_AVAILABLE = "37";

   @RBEntry("Al momento, il Navigatore Windchill non supporta la creazione degli oggetti {0}.")
   public static final String CREATE_NOT_AVAILABLE = "24";

   @RBEntry("Al momento, il Navigatore Windchill non supporta l'aggiornamento degli oggetti {0}.")
   public static final String UPDATE_NOT_AVAILABLE = "25";

   @RBEntry("Per aggiornare {0} è necessario disporre di autorizzazione.")
   public static final String UPDATE_NOT_AUTHORIZED = "27";

   @RBEntry("Visualizzare {0}?")
   public static final String PROMPT_VIEW_OBJECT = "28";

   @RBEntry("Errore durante l'avvio del task di aggiornamento di {0}. L'oggetto che si sta cercando di aggiornare non è più presente.")
   public static final String OBJECT_DOES_NOT_EXIST = "29";

   @RBEntry("Impossibile spostare gli schedari utilizzando il Navigatore Windchill.")
   public static final String CANNOT_MOVE_CABINET = "30";

   @RBEntry("Impossibile eliminare gli schedari utilizzando il Navigatore Windchill.")
   public static final String CANNOT_DELETE_CABINET = "31";

   @RBEntry("Errore durante l'avvio del task di modifica dell'identificativo per {0}: {1}.")
   public static final String INITIATE_CHANGE_IDENTITY_FAILED = "32";

   @RBEntry("È necessario essere un autore invio della fase del ciclo di vita corrente per riassegnare il ciclo di vita per {0}.")
   public static final String LC_TEMPLATE_NOT_SUBMITTER = "33";

   @RBEntry("È necessario essere un autore invio della fase del ciclo di vita corrente  per riassegnare {0} ad un altro team.")
   public static final String PROJECT_NOT_SUBMITTER = "34";

   @RBEntry("È necessario essere un autore invio per aggiornare i partecipanti al ruolo nel ciclo di vita di {0}.")
   public static final String AUGMENT_NOT_SUBMITTER = "35";

   @RBEntry("Si è verificato un errore durante l'inizializzazione della cache delle icone: {0}")
   public static final String ICON_CACHE_ERROR = "38";

   @RBEntry("Si è verificato un errore durante il Check-Out di {0}: {1}")
   public static final String CHECK_OUT_FAILED = "39";

   @RBEntry("{0} è già sottoposto a Check-Out. La copia in modifica di {0} si trova in {1}.")
   @RBArgComment0("The display identity of the object which the user is trying to check out")
   @RBArgComment1("The folder location in which the working copy of the checked out object is stored. e.g. sahChecked Out")
   public static final String ALREADY_CHECKOUT_OWNER = "40";

   @RBEntry("Si è verificato un errore durante il recupero della cartella Checked-Out: {1}.")
   public static final String RETRIEVE_CHECKOUT_FOLDER_FAILED = "41";

   @RBEntry("Per aggiornare {0} è necessario sottoporlo a Check-Out. Effettuare il Check-Out {0} ora?")
   public static final String UPDATE_CHECK_OUT_PROMPT = "42";

   @RBEntry("{0} è attualmente sottoposto a Check-Out e non può essere aggiornato. Visualizzare {0}?")
   public static final String ALREADY_CHECKED_OUT_VIEW_PROMPT = "43";

   @RBEntry("\"{0}\" non è un valore valido per la proprietà \"{1}\": {2}.")
   @RBArgComment0("The value which the user provided for a particular property")
   @RBArgComment1("The property for which the user provided an invalid value")
   @RBArgComment2("The exception text indicating why the value given is not valid")
   public static final String PROPERTY_VETO_EXCEPTION = "44";

   @RBEntry("Si è verificato un errore durante l'inizializzazione della Guida in linea: {0}.")
   public static final String INITIALIZE_HELP_FAILED = "45";

   @RBEntry("Si è verificato un errore durante la localizzazione del Navigatore Windchill. La chiave \"{0}\" non è valida nel resource bundle \"{1}\". Contattare il personale di assistenza.")
   public static final String RESOURCE_BUNDLE_ERROR = "46";

   /**
    * ProdMgmt specific messages -------------------------------------------------
    **/
   @RBEntry("Non è stata trovata alcuna versione qualificante di {0}.")
   @RBArgComment0("The display identity of the part master for which no versions could be selected")
   public static final String NO_QUALIFIED_VERSION = "100";

   @RBEntry("{0} è una copia in modifica. Impossibile eliminarla.")
   @RBArgComment0("The display identity of the part which the user is trying to delete")
   public static final String CANNOT_DELETE_WORKING_COPY = "101";

   @RBEntry("{0} è sottoposto a Check-Out. Impossibile eliminarlo.")
   @RBArgComment0("The name of the part for which the user is attempting to delete")
   public static final String CANNOT_DELETE_CHECKED_OUT = "102";

   @RBEntry("La parte {0} è già visualizzata nel Navigatore dati di prodotto.")
   @RBArgComment0("The display identity of the part which is already opened in the Product Information Explorer")
   public static final String PART_ALREADY_DISPLAYED = "103";

   @RBEntry("Immettere un nome da assegnare alla parte.")
   public static final String NO_NAME_ENTERED = "104";

   @RBEntry("Immettere un numero da assegnare alla parte.")
   public static final String NO_NUMBER_ENTERED = "105";

   @RBEntry("Specificare la posizione della cartella contenente la parte.")
   public static final String NO_LOCATION_ENTERED = "106";

   @RBEntry("La posizione non si trova nello schedario personale.")
   public static final String LOCATION_NOT_PERSONAL_CABINET = "107";

   @RBEntry("Posizione non valida")
   public static final String LOCATION_NOT_VALID = "108";

   @RBEntry("Campi obbligatori mancanti")
   public static final String REQUIRED_FIELDS_MISSING = "109";

   @RBEntry("Salvataggio in corso...")
   public static final String SAVING = "110";

   @RBEntry("Creazione della parte {0} completata.")
   public static final String CREATE_PART_SUCCESSFUL = "111";

   @RBEntry("Creazione della parte non riuscita.")
   public static final String CREATE_PART_FAILED = "112";

   @RBEntry("Aggiornamento della parte {0} completato.")
   public static final String UPDATE_PART_SUCCESSFUL = "113";

   @RBEntry("Aggiornamento della parte {0} non riuscito.")
   public static final String UPDATE_PART_FAILURE = "114";

   @RBEntry("Salvataggio della parte {0} non riuscito.")
   public static final String SAVE_PART_FAILURE = "116";

   @RBEntry("Salvataggio della configurazione {0} non riuscito.")
   @RBArgComment0("product configuration identity")
   public static final String SAVE_CONFIGURATION_FAILURE = "131";

   @RBEntry("Immettere il numero di serie per l'istanza di prodotto")
   public static final String NO_SERIAL_NUMBER_ENTERED = "132";

   @RBEntry("Salvataggio dell'istanza di prodotto {0} non riuscito.")
   @RBArgComment0("product instance identity")
   @RBArgComment1("The text from the exception caught while trying to delete the object")
   public static final String SAVE_INSTANCE_FAILURE = "133";

   @RBEntry("Immettere il numero di serie per la parte.")
   public static final String NO_PART_SERIAL_NUMBER_ENTERED = "134";

   @RBEntry("Selezionare un'istanza di prodotto.")
   public static final String NO_PRODUCT_INSTANCE_CHOOSEN = "135";

   @RBEntry("Il numero di riga {0} è stato usato più volte. Trovare il duplicato e risolvere il problema.")
   @RBArgComment0("The line number that is in duplicate")
   public static final String DUPLICATE_LINE_NUMBER = "136";

   /**
    * ------------------------------------------------------------------------
    **/
   @RBEntry("modo non valido")
   public static final String INVALID_MODE = "115";

   @RBEntry("{0} fa ora riferimento a {1}")
   public static final String NOW_REFERENCES = "117";

   @RBEntry("{0} utilizza {1} di {2}")
   public static final String NOW_USES = "118";

   @RBEntry("{0} fa riferimento a {1}")
   public static final String REFERENCES = "120";

   @RBEntry("{0} non fa più riferimento a {1}")
   public static final String DOESNT_REFERENCE = "123";

   @RBEntry("{0} utilizza {1}")
   public static final String USES = "119";

   @RBEntry(" Operazione in corso ")
   public static final String WORKING = "121";

   @RBEntry("{0} non utilizza più {1}")
   @RBArgComment0("The display identity of the parent part which used to use the child part")
   @RBArgComment1("The display identity of the child part which is no longer used by the parent part")
   public static final String DOESNT_USE = "122";

   @RBEntry("Nessuna parte specificata da aggiornare.")
   public static final String NO_PART_TO_UPDATE = "124";

   @RBEntry("È necessario che l'oggetto specificato sia una WTPart perché sia possibile l'avvio del task.")
   public static final String OBJECT_NOT_WTPART = "125";

   @RBEntry("Impossibile ottenere PropertyDescriptor")
   public static final String NO_PROPERTY_DESCRIPTOR = "126";

   @RBEntry("Impossibile recuperare ClassInfo")
   public static final String NO_CLASSINFO = "127";

   @RBEntry("{0} non è contenuto in una baseline.")
   @RBArgComment0("The display identity of the object (e.g. part) which the user is trying to remove from a baseline")
   public static final String NOT_IN_BASELINE = "128";

   @RBEntry("Immettere un nome da assegnare alla configurazione di prodotto.")
   public static final String NO_CONFIGURATION_NAME_ENTERED = "130";

   @RBEntry("Il nome della baseline \"{0}\" non è univoco.")
   public static final String BASELINE_NAME_NOT_UNIQUE = "155";

   @RBEntry("Il nome della baseline \"{0}\" non è valido.")
   public static final String BASELINE_NAME_NOT_VALID = "156";

   @RBEntry("Il nome del configuration item \"{0}\" non è univoco.")
   public static final String CONFIG_ITEM_NAME_NOT_UNIQUE = "157";

   @RBEntry("Il configuration item \"{0}\" non è valido.")
   public static final String CONFIG_ITEM_NAME_NOT_VALID = "158";

   @RBEntry("La versione di parte di livello superiore è stata modificata da {0} a {1} per riflettere la specifica di configurazione usata.")
   public static final String PART_VERSION_HAS_CHANGED = "129";

   @RBEntry("Cerca configuration item")
   public static final String FIND_CONFIG_ITEM_TITLE = "159";

   @RBEntry("Cerca baseline")
   public static final String FIND_BASELINE_TITLE = "160";

   /**
    * ------------------------------------------------------------------------
    **/
   @RBEntry("Usa solo per questa sessione")
   public static final String SAVE_CONFIG_SPEC_LABEL = "161";

   @RBEntry("Avanzamento")
   public static final String PROGRESS_TITLE = "162";

   @RBEntry("L'utilizzo di {0} non può essere rimosso perché è descritto da {1}.")
   public static final String BUILT_LINK_CAN_NOT_BE_DELETED = "163";

   @RBEntry("{0} non può essere modificato perché l'utilizzo di {1} è descritto da {2}.")
   public static final String BUILT_LINK_CAN_NOT_BE_UPDATED = "164";

   @RBEntry("{0} costruisce {1}.")
   public static final String BUILD_RULE = "165";

   /**
    * -------------------------------------------------------------------------
    **/
   @RBEntry("Errore durante la pubblicazione di {0}: {1}")
   public static final String PUBLISH_PART_FAILED = "166";

   @RBEntry("Ricerca degli oggetti che descrivono {0} in corso...")
   public static final String FINDING_DESCRIBES_LABEL = "167";

   @RBEntry("Ricerca di attributi in corso...")
   public static final String FINDING_ATTRIBUTES_LABEL = "168";

   /**
    * -------------------------------------------------------------------------
    **/
   @RBEntry("Impossibile trovare una versione qualificante di {0} usando la specifica di configurazione {1}. Impostare una specifica di configurazione diversa?")
   public static final String NO_QUALIFIED_VERSION_PROMPT = "169";

   @RBEntry("Il Navigatore dati di prodotto non supporta correntemente parti alternative.")
   public static final String ALTERNATES_NOT_AVAILABLE = "170";

   @RBEntry("Errore durante l'avvio del task di definizione delle alternative di {0}:  {1}")
   public static final String INITIATE_DEFINE_ALTERNATES_FAILED = "171";

   @RBEntry("È necessario che l'oggetto specificato sia un WTPartMaster perché sia possibile l'avvio del task.")
   public static final String OBJECT_NOT_WTPARTMASTER = "172";

   @RBEntry("Il Navigatore dati di prodotto non supporta correntemente parti sostitutive.")
   public static final String SUBSTITUTES_NOT_AVAILABLE = "173";

   @RBEntry("Errore durante l'avvio del task di definizione delle sostituzioni di {0}:  {1}")
   public static final String INITIATE_DEFINE_SUBSTITUTES_FAILED = "174";

   @RBEntry("Le sostituzioni non possono essere definite per questa parte di livello superiore  perché non è stato specificato un assieme.")
   public static final String ASSEMBLY_NEEDED_TO_DEFINE_SUBSTITUTES = "175";

   @RBEntry("È necessario che l'oggetto specificato sia una WTPart o un WTPartMaster perché sia possibile l'avvio del task.")
   public static final String OBJ_NOT_PART_OR_PARTMASTER = "176";

   @RBEntry("Impossibile trovare una relazione di utilizzo tra l'oggetto selezionato e quello padre.")
   public static final String NO_USAGE_RELATIONSHIP_FOUND = "177";

   @RBEntry("Cerca il contesto di effettività")
   public static final String FIND_EFF_CONTEXT_TITLE = "178";

   @RBEntry("Contesto di effettività:")
   public static final String PRIVATE_CONSTANT_150 = "effectivityContextLbl";

   @RBEntry("L'opzione di menu Sostituzioni->Definisci alternative non può essere attivata a causa del seguente errore: {0}")
   public static final String CANNOT_ENABLE_DEFINE_ALTERNATES = "179";

   @RBEntry("L'opzione di menu Sostituzioni->Definisci sostituzioni non può essere attivata a causa del seguente errore: {0}")
   public static final String CANNOT_ENABLE_DEFINE_SUBSTITUTES = "180";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Responsabile di prodotto")
   public static final String PRODUCT_MANAGER = "181";

   @RBEntry("Vai alla home page Windchill")
   public static final String GO_TO_HOME = "182";

   @RBEntry("Windchill")
   public static final String WINDCHILL = "183";

   @RBEntry("Navigatore struttura di prodotto")
   public static final String PRODUCT_INFORMATION_EXPLORER = "184";

   @RBEntry("È necessario che l'oggetto specificato sia un WTproduct perché sia possibile l'avvio del task.")
   public static final String OBJECT_NOT_WTPRODUCT = "185";

   @RBEntry("È necessario che l'oggetto specificato sia una WTSerialNumberedPart perché sia possibile l'avvio del task.")
   public static final String OBJECT_NOT_WTSERIALNUMBEREDPART = "186";

   @RBEntry("È necessario che l'oggetto specificato sia un WTSerialNumberedPartMaster perché sia possibile l'avvio del task.")
   public static final String OBJECT_NOT_WTSERIALNUMBEREDPARTMASTER = "187";

   @RBEntry("È necessario che l'oggetto specificato sia un WTProductMaster perché sia possibile l'avvio del task.")
   public static final String OBJECT_NOT_WTPRODUCTMASTER = "188";

   @RBEntry("Apri")
   public static final String OPEN_OBJECT_TITLE = "189";

   @RBEntry("Errore durante il tentativo di visualizzazione della pagina HTML per l'aggiornamento dei partecipanti ai ruoli: {0}")
   public static final String UPDATE_ROLE_PARTICIPANTS_URL_ERROR = "190";

   @RBEntry("Errore durante il tentativo di apertura della pagina HTML per la visualizzazione dei partecipanti ai ruoli: {0}")
   public static final String SHOW_ROLE_PARTICIPANTS_URL_ERROR = "191";

   @RBEntry("È necessario che l'oggetto specificato sia una WTProductConfiguration perché sia possibile l'avvio del task.")
   public static final String OBJECT_NOT_WTPRODUCTCONFIGURATION = "192";

   @RBEntry("Errore durante la creazione di una nuova istanza di prodotto per {0}: {1}")
   @RBArgComment0("identity of product configuration")
   @RBArgComment1("error message from server")
   public static final String LAUNCH_PRODUCT_INSTANCE_FAILED = "193";

   @RBEntry("È necessario che l'oggetto specificato sia un WTProductInstance perché sia possibile l'avvio del task.")
   public static final String OBJECT_NOT_WTPRODUCTINSTANCE = "194";

   @RBEntry("La posizione {0} non è inclusa nello schedario personale. Selezionare una posizione inclusa nello schedario personale.")
   @RBArgComment0("The value of the location entered by the user.  e.g. Design")
   public static final String LOCATION_NOT_PERSONAL_CABINET_ARG = "195";

   @RBEntry("Immettere un nome per l'annotazione.")
   public static final String ANNOTATION_NAME_NEEDED = "196";

   @RBEntry("L'utente ha modificato le impostazioni di visualizzazione nella specifica di configurazione di default. Visualizzare nuovamente la struttura del prodotto secondo le nuove impostazioni?")
   public static final String VIEW_PREFERENCE_UPDATED_SINGULAR = "197";

   @RBEntry("L'utente ha modificato le impostazioni di visualizzazione nella specifica di configurazione di default. Visualizzare nuovamente le strutture del prodotto secondo le nuove impostazioni?")
   public static final String VIEW_PREFERENCE_UPDATED_PLURAL = "198";

   @RBEntry("Rivisualizza")
   public static final String UPDATE_CONFIG_SPEC_OK_BUTTON = "199";

   @RBEntry("Non rivisualizzare")
   public static final String UPDATE_CONFIG_SPEC_CANCEL_BUTTON = "200";

   @RBEntry("Check-Out di {0} ({1}) in corso...")
   @RBArgComment0("The number of the part being checked out")
   @RBArgComment1("The name of the part being checked out")
   public static final String CHECKOUT_PART_PROGRESS_TEXT = "201";

   @RBEntry("Check-Out in corso...")
   public static final String CHECKOUT_PROGRESS_TEXT = "202";

   @RBEntry("Il nome dell'annotazione non può contenere più di 60 caratteri.")
   public static final String ANNOTATION_NAME_TOO_BIG = "203";

   @RBEntry("Scegliere un ciclo di vita per la parte.")
   @RBComment("Message displayed to the user who attempts to save a new part without first choosing a life cycle (required).")
   public static final String NO_LIFE_CYCLE_CHOSEN = "204";

   @RBEntry("{0} è attualmente sottoposto a Check-Out in un progetto. Visualizzare {0}?")
   @RBComment("Message received when user attempts to update an workable object that is currently checked out to a project.")
   public static final String CHECKED_OUT_TO_PROJECT_VIEW_PROMPT = "205";

   @RBEntry("Annotazione")
   public static final String PRIVATE_CONSTANT_151 = "annotation.label";

   @RBEntry("A")
   public static final String PRIVATE_CONSTANT_152 = "annotation.shortcut";

   @RBEntry("Annota assieme...")
   public static final String PRIVATE_CONSTANT_153 = "annotation.annotateassembly.label";

   @RBEntry("Apri gruppo di annotazioni")
   public static final String PRIVATE_CONSTANT_154 = "annotation.openannotation.label";

   @RBEntry("Elimina gruppo di annotazioni...")
   public static final String PRIVATE_CONSTANT_155 = "annotation.deleteannotation.label";

   @RBEntry("Crea il prodotto{0}")
   public static final String PRIVATE_CONSTANT_156 = "createProductTitle";

   @RBEntry("Crea parte con numero di serie {0}")
   public static final String PRIVATE_CONSTANT_157 = "createSerialNumberedPartTitle";

   @RBEntry("Apri...")
   public static final String PRIVATE_CONSTANT_158 = "file.open.label";

   @RBEntry("O")
   public static final String PRIVATE_CONSTANT_159 = "file.open.shortcut";

   @RBEntry("Prodotto")
   public static final String PRIVATE_CONSTANT_160 = "file.new.product.label";

   @RBEntry("T")
   public static final String PRIVATE_CONSTANT_161 = "file.new.product.shortcut";

   @RBEntry("Parte con numero di serie")
   public static final String PRIVATE_CONSTANT_162 = "file.new.serialNumberedPart.label";

   @RBEntry("S")
   public static final String PRIVATE_CONSTANT_163 = "file.new.serialNumberedPart.shortcut";

   @RBEntry("Ciclo di vita")
   public static final String PRIVATE_CONSTANT_164 = "lifecycle.label";

   @RBEntry("Assegna a un altro ciclo di vita")
   public static final String PRIVATE_CONSTANT_165 = "lifecycle.reassignlifecycle.label";

   @RBEntry("Assegna a un altro team")
   public static final String PRIVATE_CONSTANT_166 = "lifecycle.reassignteam.label";

   @RBEntry("Imposta stato del ciclo di vita")
   public static final String PRIVATE_CONSTANT_167 = "lifecycle.setstate.label";

   @RBEntry("L")
   public static final String PRIVATE_CONSTANT_168 = "lifecycle.shortcut";

   @RBEntry("Invia")
   public static final String PRIVATE_CONSTANT_169 = "lifecycle.submit.label";

   @RBEntry("Aggiorna team")
   public static final String PRIVATE_CONSTANT_170 = "lifecycle.updateroleparticipants.label";

   @RBEntry("Mostra team")
   public static final String PRIVATE_CONSTANT_171 = "lifecycle.showroleparticipants.label";

   @RBEntry("Navigatore configurazioni di prodotto")
   public static final String PRIVATE_CONSTANT_172 = "configurationTitle";

   @RBEntry("Navigatore istanze di prodotto")
   public static final String PRIVATE_CONSTANT_173 = "instanceTitle";

   @RBEntry("Aggiungi...")
   public static final String PRIVATE_CONSTANT_174 = "selected.baseline.add.label";

   @RBEntry("Baseline")
   public static final String PRIVATE_CONSTANT_175 = "selected.baseline.label";

   @RBEntry("Completa...")
   public static final String PRIVATE_CONSTANT_176 = "selected.baseline.populate.label";

   @RBEntry("Rimuovi...")
   public static final String PRIVATE_CONSTANT_177 = "selected.baseline.remove.label";

   @RBEntry("Annulla Check-Out")
   public static final String PRIVATE_CONSTANT_178 = "selected.cancelCheckOut.label";

   @RBEntry("C")
   public static final String PRIVATE_CONSTANT_179 = "selected.cancelCheckOut.shortcut";

   @RBEntry("Check-In...")
   public static final String PRIVATE_CONSTANT_180 = "selected.checkIn.label";

   @RBEntry("I")
   public static final String PRIVATE_CONSTANT_181 = "selected.checkIn.shortcut";

   @RBEntry("Check-Out")
   public static final String PRIVATE_CONSTANT_182 = "selected.checkOut.label";

   @RBEntry("K")
   public static final String PRIVATE_CONSTANT_183 = "selected.checkOut.shortcut";

   @RBEntry("Elimina")
   public static final String PRIVATE_CONSTANT_184 = "selected.delete.label";

   @RBEntry("D")
   public static final String PRIVATE_CONSTANT_185 = "selected.delete.shortcut";

   @RBEntry("Salva con nome...")
   public static final String PRIVATE_CONSTANT_186 = "selected.duplicate.label";

   @RBEntry("Selezione")
   public static final String PRIVATE_CONSTANT_187 = "selected.label";

   @RBEntry("Nuova versione vista...")
   public static final String PRIVATE_CONSTANT_188 = "selected.newviewversion.label";

   @RBEntry("M")
   public static final String PRIVATE_CONSTANT_189 = "selected.newviewversion.shortcut";

   @RBEntry("Pubblica")
   public static final String PRIVATE_CONSTANT_190 = "selected.publish.label";

   @RBEntry("Rinomina...")
   public static final String PRIVATE_CONSTANT_191 = "selected.rename.label";

   @RBEntry("Defisci alternative")
   public static final String PRIVATE_CONSTANT_192 = "selected.replacements.definealternates.label";

   @RBEntry("Definisci sostituzioni")
   public static final String PRIVATE_CONSTANT_193 = "selected.replacements.definesubstitutes.label";

   @RBEntry("Sostituzioni")
   public static final String PRIVATE_CONSTANT_194 = "selected.replacements.label";

   @RBEntry("Nuova revisione...")
   public static final String PRIVATE_CONSTANT_195 = "selected.revise.label";

   @RBEntry("R")
   public static final String PRIVATE_CONSTANT_196 = "selected.revise.shortcut";

   @RBEntry("Nuova versione variante...")
   public static final String PRIVATE_CONSTANT_197 = "selected.oneoffversion.label";

   @RBEntry("Imposta organizzazione ERP")
   public static final String PRIVATE_CONSTANT_198 = "selected.rtp.label";

   @RBEntry("Salva con nome...")
   public static final String PRIVATE_CONSTANT_199 = "selected.saveas.label";

   @RBEntry("S")
   public static final String PRIVATE_CONSTANT_200 = "selected.shortcut";

   @RBEntry("Mostra cronologia iterazioni")
   public static final String PRIVATE_CONSTANT_201 = "selected.showiterationhistory.label";

   @RBEntry("Mostra cronologia versioni")
   public static final String PRIVATE_CONSTANT_202 = "selected.showversionhistory.label";

   @RBEntry("Annulla Check-Out")
   public static final String PRIVATE_CONSTANT_203 = "selected.undoCheckOut.label";

   @RBEntry("C")
   public static final String PRIVATE_CONSTANT_204 = "selected.undoCheckOut.shortcut";

   @RBEntry("Aggiorna")
   public static final String PRIVATE_CONSTANT_205 = "selected.update.label";

   @RBEntry("U")
   public static final String PRIVATE_CONSTANT_206 = "selected.update.shortcut";

   @RBEntry("Visualizza la pagina delle proprietà")
   public static final String PRIVATE_CONSTANT_207 = "selected.view.label";

   @RBEntry("V")
   public static final String PRIVATE_CONSTANT_208 = "selected.view.shortcut";

   @RBEntry("Visualizza la geometria")
   public static final String PRIVATE_CONSTANT_209 = "selected.visualize.label";

   @RBEntry("Definisci target di distribuzione")
   public static final String PRIVATE_CONSTANT_210 = "selected.esitargets.label";

   @RBEntry("Cronologia")
   public static final String PRIVATE_CONSTANT_211 = "tools.history.label";

   @RBEntry("Cronologia iterazioni")
   public static final String PRIVATE_CONSTANT_212 = "tools.history.iterationhistory.label";

   @RBEntry("Cronologia del ciclo di vita")
   public static final String PRIVATE_CONSTANT_213 = "tools.history.lifecyclehistory.label";

   @RBEntry("Cronologia versioni")
   public static final String PRIVATE_CONSTANT_214 = "tools.history.versionhistory.label";

   @RBEntry("Strumenti")
   public static final String PRIVATE_CONSTANT_215 = "tools.label";

   @RBEntry("T")
   public static final String PRIVATE_CONSTANT_216 = "tools.label.shortcut";

   @RBEntry("Confronto distinte base multilivello")
   public static final String PRIVATE_CONSTANT_217 = "tools.reports.bomcompare.label";

   @RBEntry("Distinta base multilivello")
   public static final String PRIVATE_CONSTANT_218 = "tools.reports.indentedbom.label";

   @RBEntry("Report")
   public static final String PRIVATE_CONSTANT_219 = "tools.reports.label";

   @RBEntry("Elenco parti")
   public static final String PRIVATE_CONSTANT_220 = "tools.reports.partslist.label";

   @RBEntry("Dove usato - multilivello")
   public static final String PRIVATE_CONSTANT_221 = "tools.reports.whereused.label";

   @RBEntry("Aggiorna prodotto .<{0}>")
   public static final String PRIVATE_CONSTANT_222 = "updateProductTitle";

   @RBEntry("Aggiorna la parte con numero di serie <{0}>")
   public static final String PRIVATE_CONSTANT_223 = "updateSerialNumberedPartTitle";

   @RBEntry("Visualizza")
   public static final String PRIVATE_CONSTANT_224 = "view.label";

   @RBEntry("V")
   public static final String PRIVATE_CONSTANT_225 = "view.shortcut";

   @RBEntry("Imposta specifica di configurazione")
   public static final String PRIVATE_CONSTANT_226 = "view.configspec.label";

   @RBEntry("Mostra casi d'impiego")
   public static final String PRIVATE_CONSTANT_227 = "view.showoccurrences.label";

   @RBEntry("Imposta preferenze vista")
   public static final String PRIVATE_CONSTANT_228 = "view.setviewpreference.label";

   @RBEntry("Reimposta")
   public static final String PRIVATE_CONSTANT_229 = "view.clear.label";

   @RBEntry("Reimposta tutto")
   public static final String PRIVATE_CONSTANT_230 = "view.clearall.label";

   @RBEntry("Aggiorna")
   public static final String PRIVATE_CONSTANT_231 = "view.refresh.label";

   @RBEntry("h")
   public static final String PRIVATE_CONSTANT_232 = "view.refresh.shortcut";

   @RBEntry("Configurazione")
   public static final String PRIVATE_CONSTANT_233 = "configuration.label";

   @RBEntry("C")
   public static final String PRIVATE_CONSTANT_234 = "configuration.shortcut";

   @RBEntry("Nuova configurazione di prodotto")
   public static final String PRIVATE_CONSTANT_235 = "configuration.newconfiguration.label";

   @RBEntry("C")
   public static final String PRIVATE_CONSTANT_236 = "configuration.newconfiguration.shortcut";

   @RBEntry("Nuova istanza di prodotto")
   public static final String PRIVATE_CONSTANT_237 = "configuration.newproductinstance.label";

   @RBEntry("I")
   public static final String PRIVATE_CONSTANT_238 = "configuration.newproductinstance.shortcut";

   @RBEntry("Assegna versione parte")
   public static final String PRIVATE_CONSTANT_239 = "configuration.assignpartversion.label";

   @RBEntry("A")
   public static final String PRIVATE_CONSTANT_240 = "configuration.assignpartversion.shortcut";

   @RBEntry("Completa in base alla struttura di prodotto")
   public static final String PRIVATE_CONSTANT_241 = "configuration.populatefrompartstructure.label";

   @RBEntry("P")
   public static final String PRIVATE_CONSTANT_242 = "configuration.populatefrompartstructure.shortcut";

   @RBEntry("Assegna configurazione di prodotto")
   public static final String PRIVATE_CONSTANT_243 = "configuration.associateconfiguration.label";

   @RBEntry("Alloca")
   public static final String PRIVATE_CONSTANT_244 = "configuration.allocate.label";

   @RBEntry("l")
   public static final String PRIVATE_CONSTANT_245 = "configuration.allocate.shortcut";

   @RBEntry("Annulla allocazione")
   public static final String PRIVATE_CONSTANT_246 = "configuration.deallocate.label";

   @RBEntry("D")
   public static final String PRIVATE_CONSTANT_247 = "configuration.deallocate.shortcut";

   @RBEntry("Configurazione di prodotto")
   public static final String PRIVATE_CONSTANT_248 = "configurationexplorer.explorer.tree.rootNodeText";

   @RBEntry("Configurazione di prodotto")
   public static final String PRIVATE_CONSTANT_249 = "configurationexplorer.explorer.tree.statusBarText";

   @RBEntry("Istanza di prodotto")
   public static final String PRIVATE_CONSTANT_250 = "instanceexplorer.explorer.tree.rootNodeText";

   @RBEntry("Istanza di prodotto")
   public static final String PRIVATE_CONSTANT_251 = "instanceexplorer.explorer.tree.statusBarText";

   /**
    * -----------------------------------------------------------------------------
    * Labels and Messages used to support reference designators and occurrences
    * -----------------------------------------------------------------------------
    **/
   @RBEntry("Modifica casi d'impiego...")
   public static final String OCCURRENCES_BUTTON_LABEL = "OCCURRENCES_BUTTON_LABEL";

   @RBEntry("<senza nome>")
   public static final String UNNAMED_OCCURRENCE_LABEL = "UNNAMED_OCCURRENCE_LABEL";

   @RBEntry("Indicatore di riferimento")
   public static final String REFERENCE_DESIGNATOR_LABEL = "REFERENCE_DESIGNATOR_LABEL";

   @RBEntry("Indicatore di riferimento:")
   public static final String REFERENCE_DESIGNATOR_FIELD_LABEL = "REFERENCE_DESIGNATOR_FIELD_LABEL";

   @RBEntry("<Nuovo assieme>")
   public static final String NEW_ASSEMBLY_LABEL = "NEW_ASSEMBLY_LABEL";

   @RBEntry("Quantità:")
   public static final String QUANTITY_LABEL = "QUANTITY_LABEL";

   @RBEntry("Usato nell'assieme:")
   public static final String USED_IN_ASSEMBLY_LABEL = "USED_IN_ASSEMBLY_LABEL";

   @RBEntry("Dettagli parte")
   public static final String PART_DETAILS_LABEL = "PART_DETAILS_LABEL";

   @RBEntry("Dettagli casi d'impiego")
   public static final String OCCURRENCES_DETAILS_LABEL = "OCCURRENCES_DETAILS_LABEL";

   @RBEntry("Modifica...")
   public static final String EDIT_BUTTON_LABEL = "EDIT_BUTTON_LABEL";

   @RBEntry("Un caso d'impiego con indicatore \"{0}\" esiste già. Fornire un indicatore di riferimento diverso per il presente caso d'impiego.")
   @RBArgComment0("The reference designator (name of the occurrence)")
   public static final String REFERENCE_DESIGNATOR_NOT_UNIQUE = "REFERENCE_DESIGNATOR_NOT_UNIQUE";

   @RBEntry("Un altro utilizzo in questo assieme contiene un caso d'impiego con indicatore di riferimento \"{0}\". Gli indicatori di riferimento devono essere univoci per i casi d'impiego di tutti gli utilizzi in questo assieme. Fornire un indicatore di riferimento diverso.")
   @RBArgComment0("The reference designator (name of the occurrence)")
   public static final String REFERENCE_DESIGNATOR_NOT_UNIQUE_IN_ASM = "REFERENCE_DESIGNATOR_NOT_UNIQUE_IN_ASM";

   @RBEntry("Aggiungi caso d'impiego")
   public static final String ADD_OCCURRENCE_TITLE = "ADD_OCCURRENCE_TITLE";

   @RBEntry("{0} - Modifica caso d'impiego")
   public static final String EDIT_OCCURRENCE_TITLE = "EDIT_OCCURRENCE_TITLE";

   @RBEntry("{0} {1}")
   @RBComment("Displays the quantity and units of a part used.  For example, 4 each")
   @RBArgComment0("The quantity of the part uses, e.g. 4")
   @RBArgComment1("The units describing the quantity, e.g. each")
   public static final String QUANTITY_UNITS_DISPLAY = "QUANTITY_UNITS_DISPLAY";

   @RBEntry("{0} ({1}) Versione {2}, {3}")
   @RBComment("Example: Engine Body (189320) Version A.1, Engineering View")
   @RBArgComment0("Name of the assembly")
   @RBArgComment1("Number of the assembly")
   @RBArgComment2("Version letter")
   @RBArgComment3("View")
   public static final String ASSEMBLY_DISPLAY_IDENTITY = "ASSEMBLY_DISPLAY_IDENTITY";

   @RBEntry("Vista: {0}")
   @RBComment("Display for view, example: Engineering View")
   @RBArgComment0("View name")
   public static final String VIEW_DISPLAY = "VIEW_DISPLAY";

   @RBEntry("Quando le unità per la parte usata sono \"{0}\", la quantità deve essere un numero intero. Inserire un numero intero per la quantità.")
   @RBComment("Error message displayed when user attempts to enter a decimal for quantity.")
   @RBArgComment0("The units - e.g. Each (For example, how much quantity of a material would you need: 2 meters, 2 kilos, 2 each.)")
   public static final String QUANTITY_NOT_INTEGER = "QUANTITY_NOT_INTEGER";

   @RBEntry("* {0}")
   @RBComment("Creates a required field by appending an asterisk and space to the given label")
   @RBArgComment0("The label to which an asterisk is prepended")
   public static final String REQUIRED_LABEL = "REQUIRED_LABEL";

   @RBEntry("Tutti i casi d'impiego di {0} ({1}) hanno un indicatore di riferimento. Per ridurre la quantità di {0}, selezionare i casi d'impiego da rimuovere.")
   @RBArgComment0("The name of the part whose usage quantity is being decremented")
   @RBArgComment1("The number of the part whose usage quantity is being decremented")
   public static final String CHOOSE_OCCURRENCES_PROMPT = "CHOOSE_OCCURRENCES_PROMPT";

   @RBEntry("Scegli casi d'impiego da rimuovere")
   public static final String CHOOSE_OCCURRENCES_DIALOG_TITLE = "CHOOSE_OCCURRENCES_DIALOG_TITLE";

   @RBEntry("Alcuni casi d'impiego di {0} ({1}) hanno un indicatore di riferimento. Per ridurre la quantità di {0} a {2} {3}, rimuovere almeno {4} casi d'impiego con indicatore di riferimento. Selezionare i casi da rimuovere.")
   public static final String CHOOSE_SOME_OCCURRENCES_PROMPT = "CHOOSE_SOME_OCCURRENCES_PROMPT";

   @RBEntry("Casi d'impiego")
   public static final String OCCURRENCES_TITLE = "OCCURRENCES_TITLE";
}
