package wt.clients.csm.search;

import wt.util.resource.*;

public final class SearchRB_it extends wt.util.resource.NestableListResourceBundle {
   /**
    * Images
    **/
   @RBEntry("wt/clients/images/wsfsrch.gif")
   @RBPseudo(false)
   public static final String LAUNCH_ICON = "bi0";

   @RBEntry("wt/clients/images/prevtl.gif")
   @RBPseudo(false)
   public static final String PREVIOUS_ICON = "bi1";

   @RBEntry("wt/clients/images/checkin.gif")
   @RBPseudo(false)
   public static final String CHECKIN_ICON = "bi10";

   @RBEntry("wt/clients/images/undocheckout.gif")
   @RBPseudo(false)
   public static final String UNDO_CHECKOUT_ICON = "bi11";

   @RBEntry("wt/clients/images/view.gif")
   @RBPseudo(false)
   public static final String VIEW_PART_ICON = "bi12";

   @RBEntry("wt/clients/images/update.gif")
   @RBPseudo(false)
   public static final String UPDATE_ICON = "bi13";

   @RBEntry("wt/clients/images/wexplr.gif")
   @RBPseudo(false)
   public static final String LAUNCH_WINDCHILL_EXPLORER_ICON = "bi14";

   @RBEntry("wt/clients/images/pexplr.gif")
   @RBPseudo(false)
   public static final String LAUNCH_PRODUCT_INFO_EXPLORER_ICON = "bi15";

   @RBEntry("wt/clients/images/findnavnode.gif")
   @RBPseudo(false)
   public static final String SEARCH_NODE_ICON = "bi16";

   @RBEntry("wt/clients/images/nodenoimage.jpg")
   @RBPseudo(false)
   public static final String NODE_NO_IMAGE_ICON = "bi17";

   @RBEntry("wt/clients/images/search_up.gif")
   @RBPseudo(false)
   public static final String SEARCH_UP_IMAGE_ICON = "bi18";

   @RBEntry("wt/clients/images/create.gif")
   @RBPseudo(false)
   public static final String CREATE_PART_DIALOG_ICON = "bi19";

   @RBEntry("wt/clients/images/nexttl.gif")
   @RBPseudo(false)
   public static final String NEXT_ICON = "bi2";

   @RBEntry("wt/clients/images/sortascend.gif")
   @RBPseudo(false)
   public static final String SORT_ASCENDING_ICON = "bi3";

   @RBEntry("wt/clients/images/sortdesc.gif")
   @RBPseudo(false)
   public static final String SORT_DESCENDING_ICON = "bi4";

   @RBEntry("wt/clients/images/funcequivsrch.gif")
   @RBPseudo(false)
   public static final String FUNCTIONAL_EQUIVALENTS_ICON = "bi5";

   @RBEntry("wt/clients/images/view.gif")
   @RBPseudo(false)
   public static final String VIEW_ICON = "bi6";

   @RBEntry("wt/clients/images/exportsrchresults_tl.gif")
   @RBPseudo(false)
   public static final String EXPORT_RESULTS_ICON = "bi7";

   @RBEntry("wt/clients/images/stoptl.gif")
   @RBPseudo(false)
   public static final String STOP_ICON = "bi8";

   @RBEntry("wt/clients/images/checkout.gif")
   @RBPseudo(false)
   public static final String CHECKOUT_ICON = "bi9";

   /**
    * Combo box values
    **/
   @RBEntry("20, 50, 100, 200")
   @RBPseudo(false)
   public static final String COMBOBOX_VALUES = "cbx0";

   @RBEntry("Contiene, Inizia con, Termina con, Corrispondenza esatta")
   public static final String SEARCH_PATTERN_COMBOBOX_VALUES = "cbx1";

   /**
    * Search Button, Combo box labels, text field label
    **/
   @RBEntry("Reimposta")
   public static final String CLEARTAB_BUTTON_LABEL = "l0";

   @RBEntry("Sovrapponi")
   public static final String CASCADE_BUTTON_LABEL = "l1";

   @RBEntry("Cerca")
   public static final String SEARCH_BUTTON_LABEL = "l2";

   @RBEntry("Numero di risultati:")
   public static final String DISPLAY_LABEL = "l3";

   @RBEntry("Unità:")
   public static final String MEASUREMENT_SYSTEM_LABEL = "l4";

   @RBEntry("OK")
   public static final String SELECT_BUTTON_LABEL = "l5";

   @RBEntry("Annulla")
   public static final String CANCEL_BUTTON_LABEL = "l6";

   @RBEntry("Contesto di classificazione:")
   public static final String RANKING_CONTEXT_LABEL = "l7";

   @RBEntry("Nome nodo")
   public static final String SEARCH_RESULT_NODE_NAME_COL_LABEL = "l8";

   @RBEntry("Percorso")
   public static final String SEARCH_RESULT_PATH_NAME_COL_LABEL = "l9";

   @RBEntry("Seleziona")
   public static final String SEARCH_RESULT_SELECT_BUTTON_LABEL = "l10";

   @RBEntry("Trova il nodo di navigazione")
   public static final String SEARCH_NODE_LABEL = "l11";

   @RBEntry("Tutto")
   public static final String RANKING_CONTEXT_ALL = "112";

   /**
    * Error messages
    **/
   @RBEntry("Il file esiste già. Sostituirlo?")
   public static final String FILE_EXISTS = "m0";

   @RBEntry("Nessuna corrispondenza")
   public static final String NO_RESULTS_MESSAGE = "m1";

   @RBEntry("Errore durante il recupero dell'oggetto selezionato. Vedere\n la console Java per informazioni dettagliate.")
   public static final String ERROR_GET_HEAVY_OBJ = "m2";

   @RBEntry("L'aggiornamento è applicabile solo a WTPart e BusinessEntity.")
   public static final String ERROR_UPDATE_PART_BUSI_ONLY = "m3";

   @RBEntry("Il Check-out è applicabile solo agli oggetti iterati.")
   public static final String ERROR_CHECKOUT_ITERATED_ONLY = "m4";

   @RBEntry("Il Check-in è applicabile solo agli oggetti iterati.")
   public static final String ERROR_CHECKIN_ITERATED_ONLY = "m5";

   @RBEntry("L'annullamento del Check-out è applicabile solo agli oggetti iterati.")
   public static final String ERROR_UNDO_CHECKOUT_ITERATED_ONLY = "m6";

   @RBEntry("Nessuna corrispondenza")
   public static final String SEARCH_RESULT_EMPTY_MESSAGE = "m7";

   @RBEntry("Errore durante la ricerca dei nodi di navigazione:\n\n {0}")
   public static final String SEARCH_NAVIGATION_NODE_FAILURE = "m8";

   @RBEntry("Selezione attuale")
   public static final String CURRENT_SELECTION = "popup00";

   @RBEntry("Modifica")
   public static final String POPUP_EDIT = "popup01";

   @RBEntry("Visualizza")
   public static final String POPUP_VIEW = "popup02";

   /**
    * ToolTips
    **/
   @RBEntry("Gestione riutilizzo")
   public static final String LAUNCH_TOOLTIP = "stt0";

   @RBEntry("Gestione riutilizzo:")
   public static final String SEARCH_FRAME_TITLE = "stt1";

   @RBEntry("Trova nodo")
   public static final String FIND_BUTTON_TOOLTIP = "stt10";

   @RBEntry("Check-Out")
   public static final String CHECKOUT_TOOLTIP = "stt11";

   @RBEntry("Check-In")
   public static final String CHECKIN_TOOLTIP = "stt12";

   @RBEntry("Annulla Check-Out")
   public static final String UNDO_CHECKOUT_TOOLTIP = "stt13";

   @RBEntry("Visualizza")
   public static final String VIEW_PART_TOOLTIP = "stt14";

   @RBEntry("Aggiorna")
   public static final String UPDATE_TOOLTIP = "stt15";

   @RBEntry("Avvia il Navigatore Windchill")
   public static final String LAUNCH_WINDCHILL_EXPLORER_TOOLTIP = "stt16";

   @RBEntry("Avvia un nuovo Navigatore struttura di prodotto")
   public static final String LAUNCH_PRODUCT_INFO_EXPLORER_TOOLTIP = "stt17";

   @RBEntry("Crea una nuova parte")
   public static final String CREATE_PART_DIALOG_TOOLTIP = "stt18";

   @RBEntry("Pagina dei risultati precedente")
   public static final String PREVIOUS_TOOLTIP = "stt2";

   @RBEntry("Pagina dei risultati successiva")
   public static final String NEXT_TOOLTIP = "stt3";

   @RBEntry("Ordine crescente ")
   public static final String SORT_ASCENDING_TOOLTIP = "stt4";

   @RBEntry("Ordine decrescente ")
   public static final String SORT_DESCENDING_TOOLTIP = "stt5";

   @RBEntry("Cerca gli equivalenti funzionali")
   public static final String FUNCTIONAL_EQUIVALENTS_TOOLTIP = "stt6";

   @RBEntry("Avvia Visualizzatore")
   public static final String VIEW_TOOLTIP = "stt7";

   @RBEntry("Esporta risultati")
   public static final String EXPORT_RESULTS_TOOLTIP = "stt8";

   @RBEntry("Arresta interrogazione")
   public static final String STOP_TOOLTIP = "stt9";

   /**
    * Titles
    **/
   @RBEntry("Cerca")
   public static final String SEARCH_TAB_TITLE = "t0";

   @RBEntry("Risultati")
   public static final String RESULTS_TAB_TITLE = "t1";

   @RBEntry("Testuale")
   public static final String TEXTUAL_TAB_TITLE = "t2";

   @RBEntry("Grafico")
   public static final String GRAPHICAL_TAB_TITLE = "t3";

   @RBEntry("Ricerca equivalenti funzionali")
   public static final String FUNC_EQUIV_SEARCH_TAB_TITLE = "t4";

   @RBEntry("Avvertenza")
   public static final String WARNING_TITLE = "t5";

   @RBEntry("Risultati della ricerca ")
   public static final String SEARCH_RESULT_DIALOG_TITLE_P1 = "t6";

   @RBEntry("Risultati della ricerca - Primi {0} nodi trovati")
   public static final String SEARCH_RESULTS_DISPLAY = "t9";

   /**
    * URLs
    **/
   @RBEntry("wt/helpfiles/help_it/online/wsf/Admin_Guide/Search.html")
   @RBPseudo(false)
   public static final String SHELPPAGE = "url0";

   @RBEntry("wt/helpfiles/help_it/online/wsf/Admin_Guide/Results.html")
   @RBPseudo(false)
   public static final String RHELPPAGE = "url1";

   @RBEntry("wt/helpfiles/help_it/online/wsf/Admin_Guide/Results.html")
   @RBPseudo(false)
   public static final String FHELPPAGE = "url2";
}
