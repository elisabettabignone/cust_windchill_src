package wt.clients.csm.classification;

import wt.util.resource.*;

public final class ClassificationRB_it extends wt.util.resource.NestableListResourceBundle {
   /**
    * classification node constraint tab column headers
    **/
   @RBEntry("Vincoli:")
   public static final String CNCTH_CONSTRAINTS = "CNCTH0";

   @RBEntry("Descrizione:")
   public static final String CNCTH_DESCRIPTIONS = "CNCTH1";

   @RBEntry("wt/helpfiles/help_it/online/wsf/Admin_Guide/classstruc.html")
   @RBPseudo(false)
   public static final String CSHELPPAGE = "URL0";

   @RBEntry("wt/helpfiles/help_it/online/wsf/Admin_Guide/classnode.html")
   @RBPseudo(false)
   public static final String CNHELPPAGE = "URL2";

   @RBEntry("wt/clients/images/cabopn_blu.gif")
   @RBPseudo(false)
   public static final String ROOT_OPEN_IMAGE = "adimg00";

   @RBEntry("wt/clients/images/cabcls_blu.gif")
   @RBPseudo(false)
   public static final String ROOT_CLOSE_IMAGE = "adimg01";

   @RBEntry("wt/clients/images/classnode.gif")
   @RBPseudo(false)
   public static final String NODE_IMAGE = "adimg02";

   /**
    * classifier button labels
    **/
   @RBEntry("Classifica")
   public static final String CLASSIFIER_CLASSIFY = "cla0";

   @RBEntry("Riapplica modello")
   public static final String CLASSIFIER_REAPPLY_TEMPLATE = "cla1";

   @RBEntry("Rimuovi classificazione")
   public static final String CLASSIFIER_REMOVE_CLASSIFICATION = "cla2";

   /**
    * classification node creator details
    **/
   @RBEntry("Copia dal padre")
   public static final String CNCREATOR_COPY_FROM_PARENT = "cnv0";

   @RBEntry("Vuoto")
   public static final String CNCREATOR_BLANK = "cnv1";

   @RBEntry("Nome:")
   public static final String CNCREATOR_NAME_LABEL = "cnv2";

   @RBEntry("Modello iniziale \n e vincoli:")
   public static final String CNCREATOR_TEMPLATE_LABEL = "cnv3";

   /**
    * Create Constraint wizard labels
    **/
   @RBEntry("Vincolo:")
   public static final String CTMP_CONSTRAIN = "conla0";

   @RBEntry("Nodo di classificazione intero")
   public static final String CTMP_ENTIRE_NODE = "conla1";

   @RBEntry("Attributo del modello di classificazione")
   public static final String CTMP_ATTR_FROM_CLASS_TEMPLATE = "conla2";

   @RBEntry("Altro attributo")
   public static final String CTMP_OTHER_ATTR = "conla3";

   @RBEntry("Seleziona l'attributo da sottoporre a vincolo:")
   public static final String CTAP_SELECT = "conla4";

   @RBEntry("Applica ai discendenti dell'attributo")
   public static final String CTAP_APPLY_TO_DESC = "conla5";

   @RBEntry("Seleziona tipo di vincolo:")
   public static final String CTSP_SELECT = "conla6";

   @RBEntry("Tipo:")
   public static final String CEP_TYPE = "conla7";

   @RBEntry("Nome:")
   public static final String CEP_NAME = "conla8";

   @RBEntry("Riepilogo")
   public static final String CSP_SUMMARY = "conla11";

   @RBEntry("Indietro")
   public static final String CONS_BACK = "conla12";

   @RBEntry("Avanti")
   public static final String CONS_NEXT = "conla13";

   @RBEntry("Annulla")
   public static final String CONS_CANCEL = "conla14";

   @RBEntry("Fine")
   public static final String CONS_FINISH = "conla15";

   /**
    * classification structure creator labels
    **/
   @RBEntry("Classe:")
   public static final String CSCREATOR_CLASS_LABEL = "csl0";

   @RBEntry("Attributo di classificazione:")
   public static final String CSCREATOR_CLASSIFICATION_ATTRIBUTE_LABEL = "csl1";

   @RBEntry("Seleziona")
   public static final String CSCREATOR_BUTTON = "csl2";

   /**
    * Constraint tab button labels
    **/
   @RBEntry("Aggiungi vincolo, Elimina vincolo, Modifica vincolo")
   public static final String CONSTRAINT_BUTTON_LABELS = "ctla00";

   /**
    * Error messages
    **/
   @RBEntry("Errore durante il prelievo delle strutture della classificazione:\n\n {0}")
   public static final String GET_ALL_CLASSIFICATIONSTRUCT_FAILED = "ex00";

   @RBEntry("Errore durante il prelievo delle radici dei nodi di classificazione:\n\n {0}")
   public static final String GET_ALL_CLASSIFICATIONNODE_ROOTS_FAILED = "ex01";

   @RBEntry("Errore durante il prelievo di tutti i figli di \"{0}\":\n\n {1}")
   public static final String GET_ALL_CLASSIFICATIONNODE_CHILDREN_FAILED = "ex02";

   @RBEntry("Nessun nodo è selezionato al momento. Impossibile incollare.")
   public static final String PASTE_NO_NODE_SELECT = "ex03";

   @RBEntry("Selezione invalida. Impossibile incollare un nodo sotto il nodo stesso.")
   public static final String PASTE_SAME_NODE = "ex04";

   @RBEntry("Errore durante le operazioni di taglia e incolla di \"{0}\":\n\n {1}")
   public static final String PASTE_CUT_CLASSIFICATIONNODE_FAILED = "ex05";

   @RBEntry("Errore durante le operazioni di taglia e incolla di \"{0}\":\n\n {1}")
   public static final String PASTE_COPY_CLASSIFICATIONNODE_FAILED = "ex06";

   @RBEntry("Errore durante l'eliminazione della struttura di classificazione \"{0}\":\n\n {1}")
   public static final String DELETE_CLASSIFICATION_STRUCTURE_FAILED = "ex07";

   @RBEntry("Errore durante l'eliminazione del nodo di classificazione \"{0}\":\n\n {1}")
   public static final String DELETE_CLASSIFICATION_NODE_FAILED = "ex08";

   @RBEntry("L'attributo selezionato \"{0}\" non è un attributo di riferimento")
   public static final String CSCREATOR_SELECT_REFERENCE_TYPE_FAILED = "ex09";

   @RBEntry("L'attributo selezionato \"{0}\" non si riferisce al nodo di classificazione")
   public static final String CSCREATOR_SELECT_REFERENCE_CLASSIFICATION_TYPE_FAILED = "ex10";

   @RBEntry("L'organizer non può essere selezionato")
   public static final String CSCREATOR_SELECT_ORGANIZER_FAILED = "ex11";

   @RBEntry("Gli attributi selezionati esistono già")
   public static final String SELECTED_MESSAGES_ALREADY_EXIST = "ex12";

   @RBEntry("Impossibile imporre un vincolo a \"{0}\"")
   public static final String NO_CONCRETE_CLASS = "ex13";

   @RBEntry("La struttura di classificazione non è stata trovata")
   public static final String STRUCTURE_NOT_FOUND = "ex14";

   @RBEntry("Non è stato trovato alcun riferimento al nodo di classificazione")
   public static final String NODE_NOT_FOUND = "ex15";

   @RBEntry("L'oggetto aziendale non è classificato")
   public static final String BO_NOT_CLASSIFIED = "ex16";

   @RBEntry("L'oggetto di riferimento non è associato ad una struttura di classificazione")
   public static final String NO_CLASSIFICATION_STRUCTURE_FOUND = "ex17";

   @RBEntry("Il campo nome non può essere vuoto.")
   public static final String ERR_NO_NAME = "ex18";

   @RBEntry("Classe:")
   public static final String CLASS_LABEL = "la0";

   @RBEntry("Attributo di classificazione:")
   public static final String ATTRIBUTE_LABEL = "la1";

   @RBEntry("Nome:")
   public static final String NODE_NAME_LABEL = "la2";

   @RBEntry("Immagine:")
   public static final String NODE_IMAGE_LABEL = "la3";

   @RBEntry("Descrizione:")
   public static final String NODE_DESCRIPTION_LABEL = "la4";

   @RBEntry("Unità:")
   public static final String MEASUREMENT_SYSTEM_LABEL = "la5";

   /**
    * Messages
    **/
   @RBEntry("Applicare le modifiche ai figli?")
   public static final String APPLY_TO_CHILDREN = "msg00";

   @RBEntry("Eliminare i(l) vincoli(o) selezionati(o)?")
   public static final String MSG_DELETE_CONSTRAINT = "msg01";

   @RBEntry("Applicare le modifiche ai figli?\n(L'eliminazione dei vincoli non sarà propagata ai figli automaticamente.\nDeve essere effettuata manualmente)")
   public static final String APPLY_TO_CHILDREN_CREATE_CONSTRAINT = "msg02";

   @RBEntry("L'eliminazione dei vincoli non sarà propagata ai figli automaticamente.\nDeve essere effettuata manualmente")
   public static final String APPLY_TO_CHILDREN_DELETE_CONSTRAINT = "msg03";

   /**
    * Titles
    **/
   @RBEntry("Seleziona struttura di classificazione")
   public static final String CLASSIFICATIONSTRUCTURE_SELECTOR_TITLE = "ti0";

   @RBEntry("Modifica struttura di classificazione")
   public static final String CLASSIFICATIONSTRUCTURE_EDITOR_TITLE = "ti1";

   @RBEntry("Visualizza struttura di classificazione")
   public static final String CLASSIFICATIONSTRUCTURE_VIEWER_TITLE = "ti2";

   @RBEntry("Seleziona nodo di classificazione")
   public static final String CLASSIFICATIONNODE_SELECTOR_TITLE = "ti3";

   @RBEntry("Modifica nodo di classificazione")
   public static final String CLASSIFICATIONNODE_EDITOR_TITLE = "ti4";

   @RBEntry("Visualizza nodo di classificazione")
   public static final String CLASSIFICATIONNODE_VIEWER_TITLE = "ti5";

   @RBEntry("Crea struttura di classificazione")
   public static final String CSCREATOR_TITLE = "ti6";

   @RBEntry("Crea nodo di classificazione")
   public static final String CNCREATOR_TITLE = "ti7";

   @RBEntry("Seleziona definizione di attributo")
   public static final String CASELECTOR_TITLE = "ti8";

   @RBEntry("Crea vincolo")
   public static final String NODE_CREATE_CONSTRAINT_TITLE = "ti9";

   @RBEntry("Applica ai discendenti")
   public static final String APPLY_TO_CHILDREN_TITLE = "ti10";

   @RBEntry("Conferma eliminazione")
   public static final String CONFIRM_DELETE_ATTRIBUTE_TITLE = "ti11";

   @RBEntry("Amministrazione classificazioni")
   public static final String CLASSIFICATION_ADMIN_TITLE = "ti12";

   @RBEntry("Modifica vincolo")
   public static final String NODE_EDIT_CONSTRAINT_TITLE = "ti13";

   /**
    * classification node tab labels
    **/
   @RBEntry("Generale")
   public static final String CLASSIFICATION_NODE_GENERAL_LABEL = "tla0";

   @RBEntry("Modello")
   public static final String CLASSIFICATION_NODE_TEMPLATE_LABEL = "tla1";

   @RBEntry("Vincoli")
   public static final String CLASSIFICATION_NODE_CONSTRAINT_LABEL = "tla2";

   /**
    * ToolTips
    **/
   @RBEntry("Avvia la Gestione dei nodi di classificazione")
   public static final String LAUNCHCNM_TOOLTIP = "tt0";

   @RBEntry("Avvia la Gestione delle strutture di classificazione")
   public static final String LAUNCHCSM_TOOLTIP = "tt1";

   /**
    * Icons - do not translate
    **/
   @RBEntry("wt/clients/images/tonodemgrtl.gif")
   @RBPseudo(false)
   public static final String LAUNCHCNM_ICON = "ic0";

   @RBEntry("wt/clients/images/tostructmgrtl.gif")
   @RBPseudo(false)
   public static final String LAUNCHCSM_ICON = "ic1";
}
