/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.csm.navigation;

import wt.util.resource.*;

@RBUUID("wt.clients.csm.navigation.NavigationRB")
public final class NavigationRB_it extends wt.util.resource.NestableListResourceBundle {
   @RBEntry("navstruc")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String NSHELPPAGE = "URL0";

   @RBEntry("navnode")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String NNHELPPAGE = "URL2";

   @RBEntry("wt/clients/images/cabopn_blu.gif")
   @RBPseudo(false)
   public static final String ROOT_OPEN_IMAGE = "adimg00";

   @RBEntry("wt/clients/images/cabcls_blu.gif")
   @RBPseudo(false)
   public static final String ROOT_CLOSE_IMAGE = "adimg01";

   @RBEntry("wt/clients/images/classnode.gif")
   @RBPseudo(false)
   public static final String NODE_IMAGE = "adimg02";

   @RBEntry("Indietro")
   public static final String PREV_LABEL = "discrete00";

   @RBEntry("Avanti")
   public static final String NEXT_LABEL = "discrete01";

   /**
    * Error messages
    **/
   @RBEntry("Errore durante il prelievo di tutte le strutture di navigazione:\n\n {0}")
   public static final String GET_ALL_NAVIGATIONSTRUCT_FAILED = "ex00";

   @RBEntry("Errore durante il prelievo di tutte le radici dei nodi di navigazione:\n\n {0}")
   public static final String GET_ALL_NAVIGATIONNODE_ROOTS_FAILED = "ex01";

   @RBEntry("Errore durante il prelievo di tutti i figli di \"{0}\":\n\n {1}")
   public static final String GET_ALL_NAVIGATIONNODE_CHILDREN_FAILED = "ex02";

   @RBEntry("Nessun nodo è selezionato al momento. Impossibile incollare.")
   public static final String PASTE_NO_NODE_SELECT = "ex03";

   @RBEntry("Selezione invalida. Impossibile incollare un nodo sotto il nodo stesso.")
   public static final String PASTE_SAME_NODE = "ex04";

   @RBEntry("Errore durante le operazioni di taglia e incolla di \"{0}\":\n\n {1}")
   public static final String PASTE_CUT_NAVIGATIONNODE_FAILED = "ex05";

   @RBEntry("Errore durante le operazioni di taglia e incolla di \"{0}\":\n\n {1}")
   public static final String PASTE_COPY_NAVIGATIONNODE_FAILED = "ex06";

   @RBEntry("Errore durante l'eliminazione della struttura di navigazione \"{0}\":\n\n {1}")
   public static final String DELETE_NAVIGATION_STRUCTURE_FAILED = "ex07";

   @RBEntry("Errore durante l'eliminazione del nodo di navigazione \"{0}\":\n\n {1}")
   public static final String DELETE_NAVIGATION_NODE_FAILED = "ex08";

   @RBEntry("Errore durante il prelievo del valore dalla definizione \"{0}\":\n\n {1}")
   public static final String GET_SHOWN_VALUE_FAILED = "ex09";

   @RBEntry("Errore durante il prelievo di nuovi valori dalla definizione \"{0}\":\n\n {1}")
   public static final String GET_NEW_VALUES_FAILED = "ex10";

   @RBEntry("Errore durante il confronto di oggetti:\n\n {0}")
   public static final String COMPARE_OBJECTS_FAILED = "ex11";

   @RBEntry("Errore durante il salvataggio dei valori per la definizione \"{0}\":\n\n {1}")
   public static final String SAVE_VALUE_FAILED = "ex12";

   @RBEntry("{0} non può essere vuoto")
   public static final String NSCREATOR_SEARCH_CLASS_FAILED = "ex13";

   @RBEntry("L'attributo selezionato \"{0}\" non è un attributo di riferimento")
   public static final String NSTCREATOR_SELECT_REFERENCE_TYPE_FAILED = "ex14";

   @RBEntry("L'organizer non può essere selezionato")
   public static final String NSTCREATOR_SELECT_ORGANIZER_FAILED = "ex15";

   @RBEntry("Il campo nome non può essere vuoto.")
   public static final String ERR_NO_NAME = "ex16";

   @RBEntry("Errore durante la ricerca in \"{0}\":\n\n {1}")
   public static final String NAVIGATION_NODE_SEARCH_FAILED = "ex17";

   /**
    * Icon images - do not translate
    **/
   @RBEntry("wt/clients/images/upArrow.gif")
   @RBPseudo(false)
   public static final String IMAGE_UP_ARROW = "im00";

   @RBEntry("wt/clients/images/downArrow.gif")
   @RBPseudo(false)
   public static final String IMAGE_DOWN_ARROW = "im01";

   @RBEntry("wt/clients/images/dotdotdot.gif")
   @RBPseudo(false)
   public static final String IMAGE_DETAILS = "im02";

   /**
    * Icons - do not translate
    **/
   @RBEntry("wt/clients/images/tonodemgrtl.gif")
   @RBPseudo(false)
   public static final String LAUNCHNNM_ICON = "in0";

   @RBEntry("wt/clients/images/tostructmgrtl.gif")
   @RBPseudo(false)
   public static final String LAUNCHNSM_ICON = "in1";

   /**
    * labels
    **/
   @RBEntry("Aggiungi scheda...")
   public static final String ADD_TAB_LABEL = "la0";

   @RBEntry("Elimina scheda")
   public static final String DELETE_TAB_LABEL = "la1";

   @RBEntry("Non applicabile")
   public static final String NOT_APPLICABLE = "la10";

   @RBEntry("Ignora")
   public static final String OVERRIDE_LABEL = "la11";

   @RBEntry("Usa default")
   public static final String USE_DEFAULT_LABEL = "la12";

   @RBEntry("Nome:")
   public static final String NODE_NAME_LABEL = "la13";

   @RBEntry("Immagine:")
   public static final String NODE_IMAGE_LABEL = "la14";

   @RBEntry("Descrizione:")
   public static final String NODE_DESCRIPTION_LABEL = "la15";

   @RBEntry("<Utilizzo del formulario d'interrogazione predefinito>")
   public static final String NODE_TAB_USING_DEFAULT_LABEL = "la16";

   @RBEntry("Generale")
   public static final String GENERAL = "la17";

   /**
    * Labels
    **/
   @RBEntry("Nome:")
   public static final String NAVIGATIONSTRUCTURE_GENERAL_NAME_LABEL = "la2";

   @RBEntry("Tipo:")
   public static final String NAVIGATIONSTRUCTURE_GENERAL_TYPE_LABEL = "la3";

   @RBEntry("Classe di ricerca:")
   public static final String NAVIGATIONSTRUCTURE_GENERAL_CLASS_LABEL = "la4";

   @RBEntry("Link a:")
   public static final String NAVIGATIONSTRUCTURE_GENERAL_LINK_TO_LABEL = "la4a";

   @RBEntry("Collegato alla classificazione")
   public static final String CLASSIFICATION_LINKED = "la5";

   @RBEntry("Indipendente")
   public static final String INDEPENDENT = "la6";

   @RBEntry("Etichetta scheda:")
   public static final String NAVIGATIONSTRUCTURE_TAB_TAB_LABEL = "la7";

   @RBEntry("Classe di ricerca:")
   public static final String NAVIGATIONSTRUCTURE_TAB_CLASS_LABEL = "la8";

   @RBEntry("Attributo di riferimento:")
   public static final String NAVIGATIONSTRUCTURE_TAB_ATTRIBUTE_LABEL = "la9";

   @RBEntry("Unità:")
   public static final String MEASUREMENT_SYSTEM_LABEL = "la23";

   @RBEntry("Scheda grafico:")
   public static final String NAVIGATIONSTRUCTURE_NAVIGATION_TAB_LABEL = "la24";

   @RBEntry("Mostra attributi:")
   public static final String NAVIGATIONSTRUCTURE_SHOW_ATTRIBUTES_LABEL = "la25";

   @RBEntry("Esatto ")
   public static final String LIKE_STRING_EXACT = "lk00";

   @RBEntry("Inizia con")
   public static final String LIKE_STRING_BEGINSWITH = "lk01";

   @RBEntry("Termina con")
   public static final String LIKE_STRING_ENDSWITH = "lk02";

   @RBEntry("Contiene ")
   public static final String LIKE_STRING_CONTAINS = "lk03";

   /**
    * navigation node creator labels
    **/
   @RBEntry("Nome:")
   public static final String NNCREATOR_NAME_LABEL = "nnl0";

   /**
    * Navigation structure creator labels
    **/
   @RBEntry("Nome:")
   public static final String NSCREATOR_NAME_LABEL = "nsl0";

   @RBEntry("Tipo:")
   public static final String NSCREATOR_TYPE_LABEL = "nsl1";

   @RBEntry("Classe di ricerca:")
   public static final String NSCREATOR_SEARCH_CLASS_LABEL = "nsl2";

   @RBEntry("Link a:")
   public static final String NSCREATOR_LINK_TO_LABEL = "nsl3";

   @RBEntry("Indipendente")
   public static final String NSCREATOR_INDEPENDENT = "nsd0";

   @RBEntry("Collegato alla classificazione")
   public static final String NSCREATOR_CLASSIFICATION_LINKED = "nsd1";

   @RBEntry("Etichetta:")
   public static final String NSTCREATOR_LABEL = "nstl0";

   @RBEntry("Attributo di riferimento:")
   public static final String NSTCREATOR_REFERENCE_ATTRIBUTE_LABEL = "nstl1";

   @RBEntry("Seleziona...")
   public static final String NSTCREATOR_BUTTON = "nstl2";

   /**
    * Misc
    **/
   @RBEntry("non")
   public static final String NOT_LABEL = "ranged00";

   @RBEntry("<< nullo >>")
   public static final String NULL_LABEL = "ranged01";

   @RBEntry("Da:")
   public static final String RANGE_FROM_LABEL = "ranged02";

   @RBEntry(" A:")
   public static final String RANGE_TO_LABEL = "ranged03";

   @RBEntry("Aggiungi")
   public static final String ADD_LABEL = "ranged04";

   @RBEntry("Visualizza")
   public static final String GET_LABEL = "ranged05";

   @RBEntry("Elimina")
   public static final String DELETE_LABEL = "ranged06";

   @RBEntry("Nuovo valore")
   public static final String NEW_VALUE_TITLE = "ranged07";

   @RBEntry("Valore esistente")
   public static final String EXIST_VALUE_TITLE = "ranged08";

   @RBEntry("Elenco di valori")
   public static final String LIST_VALUE_TITLE = "ranged09";

   /**
    * Navigation Tab and show attributes Radial Button
    **/
   @RBEntry("Il")
   public static final String NAVIGATION_TAB_ON_LABEL = "rb1";

   @RBEntry("Disattivato")
   public static final String NAVIGATION_TAB_OFF_LABEL = "rb2";

   @RBEntry("Mostra tutto")
   public static final String SHOW_ALL_ATTRIBUTES_LABEL = "rb3";

   @RBEntry("Mostra solo gli attributi del formulario d'interrogazione")
   public static final String SHOW_QUERY_FORM_ATTRIBUTES_LABEL = "rb4";

   /**
    * Titles
    **/
   @RBEntry("Seleziona struttura di navigazione")
   public static final String NAVIGATIONSTRUCTURE_SELECTOR_TITLE = "ti0";

   @RBEntry("Seleziona nodo di navigazione")
   public static final String NAVIGATIONNODE_SELECTOR_TITLE = "ti1";

   @RBEntry("Aggiorna struttura di navigazione")
   public static final String NAVIGATIONSTRUCTURE_EDITOR_TITLE = "ti2";

   @RBEntry("Aggiorna nodo di navigazione")
   public static final String NAVIGATIONNODE_EDITOR_TITLE = "ti3";

   @RBEntry("Visualizza struttura di navigazione")
   public static final String NAVIGATIONSTRUCTURE_VIEWER_TITLE = "ti4";

   @RBEntry("Visualizza nodo di navigazione")
   public static final String NAVIGATIONNODE_VIEWER_TITLE = "ti5";

   @RBEntry("Crea struttura di navigazione")
   public static final String NSCREATOR_TITLE = "ti6";

   @RBEntry("Crea nodo di navigazione")
   public static final String NNCREATOR_TITLE = "ti7";

   @RBEntry("Aggiungi scheda struttura di navigazione")
   public static final String NSTCREATOR_TITLE = "ti8";

   @RBEntry("Struttura di navigazione")
   public static final String NAVIGATION_STRUCTURE_PAGE_TITLE = "ti15";

   /**
    * Override QueryForm Defaults
    **/
   @RBEntry("Sovrascrivi i valori predefiniti del formulario d'interrogazione")
   public static final String OVERRIDE_QF_CHOOSER_TITLE = "ti9";

   @RBEntry("Crea un formulario d'interrogazione basato su:")
   public static final String CREATE_QF_LABEL = "la18";

   @RBEntry("Vuoto")
   public static final String BLANK = "la19";

   @RBEntry("Default")
   public static final String DEFAULT = "la20";

   @RBEntry("Padre")
   public static final String PARENT = "la21";

   @RBEntry("Modello di classificazione")
   public static final String CLASSIFICATION_TEMPLATE = "la22";

   @RBEntry("Dettagli di \"{0}\"")
   public static final String DETAILS = "ti10";

   @RBEntry("Avvertenza:")
   public static final String WARNING_TITLE = "ti11";

   @RBEntry("Conferma eliminazione")
   public static final String DELETE_TAB_TITLE = "ti12";

   @RBEntry("Usa default")
   public static final String CONFIRM_USE_DEFAULT_TITLE = "ti13";

   @RBEntry("Amministrazione navigazione")
   public static final String NAVIGATION_ADMIN_TITLE = "ti14";

   /**
    * ToolTips
    **/
   @RBEntry("Avvia Gestione nodi di navigazione")
   public static final String LAUNCHNNM_TOOLTIP = "tt0";

   @RBEntry("Avvia Gestione strutture di navigazione")
   public static final String LAUNCHNSM_TOOLTIP = "tt1";

   @RBEntry("L'operazione potrebbe richiedere diversi minuti. Continuare?")
   public static final String RANGE_DETAILS_DELAY_WARNING_MESSAGE = "wx00";

   @RBEntry("Usare il formulario d'interrogazione predefinito?")
   public static final String MSG_USE_DEFAULT = "wx01";

   @RBEntry("Eliminare la scheda \" {0} \"?")
   public static final String MSG_DELETE_TAB = "wx02";

   @RBEntry("Nessun risultato")
   public static final String NO_RESULTS_MESSAGE = "wx03";
}
