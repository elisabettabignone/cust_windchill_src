/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.csm.navigation;

import wt.util.resource.*;

@RBUUID("wt.clients.csm.navigation.NavigationRB")
public final class NavigationRB extends wt.util.resource.NestableListResourceBundle {
   @RBEntry("navstruc")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String NSHELPPAGE = "URL0";

   @RBEntry("navnode")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String NNHELPPAGE = "URL2";

   @RBEntry("wt/clients/images/cabopn_blu.gif")
   @RBPseudo(false)
   public static final String ROOT_OPEN_IMAGE = "adimg00";

   @RBEntry("wt/clients/images/cabcls_blu.gif")
   @RBPseudo(false)
   public static final String ROOT_CLOSE_IMAGE = "adimg01";

   @RBEntry("wt/clients/images/classnode.gif")
   @RBPseudo(false)
   public static final String NODE_IMAGE = "adimg02";

   @RBEntry("Previous")
   public static final String PREV_LABEL = "discrete00";

   @RBEntry("Next")
   public static final String NEXT_LABEL = "discrete01";

   /**
    * Error messages
    **/
   @RBEntry("The following error occurred while trying to get all navigation structures:\n\n {0}")
   public static final String GET_ALL_NAVIGATIONSTRUCT_FAILED = "ex00";

   @RBEntry("The following error occurred while trying to get all navigation node roots:\n\n {0}")
   public static final String GET_ALL_NAVIGATIONNODE_ROOTS_FAILED = "ex01";

   @RBEntry("The following error occurred while trying to get all children of \"{0}\":\n\n {1}")
   public static final String GET_ALL_NAVIGATIONNODE_CHILDREN_FAILED = "ex02";

   @RBEntry("No node currently selected; cannot paste.")
   public static final String PASTE_NO_NODE_SELECT = "ex03";

   @RBEntry("Invalid selection; cannot paste a node under itself.")
   public static final String PASTE_SAME_NODE = "ex04";

   @RBEntry("The following error occurred while trying to cut and paste \"{0}\":\n\n {1}")
   public static final String PASTE_CUT_NAVIGATIONNODE_FAILED = "ex05";

   @RBEntry("The following error occurred while trying to copy and paste \"{0}\":\n\n {1}")
   public static final String PASTE_COPY_NAVIGATIONNODE_FAILED = "ex06";

   @RBEntry("The following error occurred while trying to delete navigation structure \"{0}\":\n\n {1}")
   public static final String DELETE_NAVIGATION_STRUCTURE_FAILED = "ex07";

   @RBEntry("The following error occurred while trying to delete navigation node \"{0}\":\n\n {1}")
   public static final String DELETE_NAVIGATION_NODE_FAILED = "ex08";

   @RBEntry("The following error occurred when trying to get the value from definition \"{0}\":\n\n {1}")
   public static final String GET_SHOWN_VALUE_FAILED = "ex09";

   @RBEntry("The following error occurred when trying to get new values from definition \"{0}\":\n\n {1}")
   public static final String GET_NEW_VALUES_FAILED = "ex10";

   @RBEntry("The following error occurred when trying to compare objects:\n\n {0}")
   public static final String COMPARE_OBJECTS_FAILED = "ex11";

   @RBEntry("The following error occurred when trying to save values for definition \"{0}\":\n\n {1}")
   public static final String SAVE_VALUE_FAILED = "ex12";

   @RBEntry("{0} cannot be empty")
   public static final String NSCREATOR_SEARCH_CLASS_FAILED = "ex13";

   @RBEntry("Selected Attribute: \"{0}\" is not a Reference Attribute")
   public static final String NSTCREATOR_SELECT_REFERENCE_TYPE_FAILED = "ex14";

   @RBEntry("Organizer cannot be selected")
   public static final String NSTCREATOR_SELECT_ORGANIZER_FAILED = "ex15";

   @RBEntry("Name Field cannot be empty.")
   public static final String ERR_NO_NAME = "ex16";

   @RBEntry("The following error occurred while trying to search in \"{0}\":\n\n {1}")
   public static final String NAVIGATION_NODE_SEARCH_FAILED = "ex17";

   /**
    * Icon images - do not translate
    **/
   @RBEntry("wt/clients/images/upArrow.gif")
   @RBPseudo(false)
   public static final String IMAGE_UP_ARROW = "im00";

   @RBEntry("wt/clients/images/downArrow.gif")
   @RBPseudo(false)
   public static final String IMAGE_DOWN_ARROW = "im01";

   @RBEntry("wt/clients/images/dotdotdot.gif")
   @RBPseudo(false)
   public static final String IMAGE_DETAILS = "im02";

   /**
    * Icons - do not translate
    **/
   @RBEntry("wt/clients/images/tonodemgrtl.gif")
   @RBPseudo(false)
   public static final String LAUNCHNNM_ICON = "in0";

   @RBEntry("wt/clients/images/tostructmgrtl.gif")
   @RBPseudo(false)
   public static final String LAUNCHNSM_ICON = "in1";

   /**
    * labels
    **/
   @RBEntry("Add Tab...")
   public static final String ADD_TAB_LABEL = "la0";

   @RBEntry("Delete Tab")
   public static final String DELETE_TAB_LABEL = "la1";

   @RBEntry("Not Applicable")
   public static final String NOT_APPLICABLE = "la10";

   @RBEntry("Override")
   public static final String OVERRIDE_LABEL = "la11";

   @RBEntry("Use Default")
   public static final String USE_DEFAULT_LABEL = "la12";

   @RBEntry("Name:")
   public static final String NODE_NAME_LABEL = "la13";

   @RBEntry("Image:")
   public static final String NODE_IMAGE_LABEL = "la14";

   @RBEntry("Description:")
   public static final String NODE_DESCRIPTION_LABEL = "la15";

   @RBEntry("<Using Default Query Form>")
   public static final String NODE_TAB_USING_DEFAULT_LABEL = "la16";

   @RBEntry("General")
   public static final String GENERAL = "la17";

   /**
    * Labels
    **/
   @RBEntry("Name:")
   public static final String NAVIGATIONSTRUCTURE_GENERAL_NAME_LABEL = "la2";

   @RBEntry("Type:")
   public static final String NAVIGATIONSTRUCTURE_GENERAL_TYPE_LABEL = "la3";

   @RBEntry("Search Class:")
   public static final String NAVIGATIONSTRUCTURE_GENERAL_CLASS_LABEL = "la4";

   @RBEntry("Link To:")
   public static final String NAVIGATIONSTRUCTURE_GENERAL_LINK_TO_LABEL = "la4a";

   @RBEntry("Classification Linked")
   public static final String CLASSIFICATION_LINKED = "la5";

   @RBEntry("Independent")
   public static final String INDEPENDENT = "la6";

   @RBEntry("Tab Label:")
   public static final String NAVIGATIONSTRUCTURE_TAB_TAB_LABEL = "la7";

   @RBEntry("Search Class:")
   public static final String NAVIGATIONSTRUCTURE_TAB_CLASS_LABEL = "la8";

   @RBEntry("Reference Attribute:")
   public static final String NAVIGATIONSTRUCTURE_TAB_ATTRIBUTE_LABEL = "la9";

   @RBEntry("Units: ")
   public static final String MEASUREMENT_SYSTEM_LABEL = "la23";

   @RBEntry("Graphical Tab:")
   public static final String NAVIGATIONSTRUCTURE_NAVIGATION_TAB_LABEL = "la24";

   @RBEntry("Show Attributes:")
   public static final String NAVIGATIONSTRUCTURE_SHOW_ATTRIBUTES_LABEL = "la25";

   @RBEntry("Exact ")
   public static final String LIKE_STRING_EXACT = "lk00";

   @RBEntry("Begins with ")
   public static final String LIKE_STRING_BEGINSWITH = "lk01";

   @RBEntry("Ends with ")
   public static final String LIKE_STRING_ENDSWITH = "lk02";

   @RBEntry("Contains ")
   public static final String LIKE_STRING_CONTAINS = "lk03";

   /**
    * navigation node creator labels
    **/
   @RBEntry("Name:")
   public static final String NNCREATOR_NAME_LABEL = "nnl0";

   /**
    * Navigation structure creator labels
    **/
   @RBEntry("Name:")
   public static final String NSCREATOR_NAME_LABEL = "nsl0";

   @RBEntry("Type:")
   public static final String NSCREATOR_TYPE_LABEL = "nsl1";

   @RBEntry("Search Class:")
   public static final String NSCREATOR_SEARCH_CLASS_LABEL = "nsl2";

   @RBEntry("Linked To:")
   public static final String NSCREATOR_LINK_TO_LABEL = "nsl3";

   @RBEntry("Independent")
   public static final String NSCREATOR_INDEPENDENT = "nsd0";

   @RBEntry("Classification Linked")
   public static final String NSCREATOR_CLASSIFICATION_LINKED = "nsd1";

   @RBEntry("Label:")
   public static final String NSTCREATOR_LABEL = "nstl0";

   @RBEntry("Reference Attribute:")
   public static final String NSTCREATOR_REFERENCE_ATTRIBUTE_LABEL = "nstl1";

   @RBEntry("Select...")
   public static final String NSTCREATOR_BUTTON = "nstl2";

   /**
    * Misc
    **/
   @RBEntry("Not")
   public static final String NOT_LABEL = "ranged00";

   @RBEntry("<< null >>")
   public static final String NULL_LABEL = "ranged01";

   @RBEntry("From: ")
   public static final String RANGE_FROM_LABEL = "ranged02";

   @RBEntry(" To: ")
   public static final String RANGE_TO_LABEL = "ranged03";

   @RBEntry("Add")
   public static final String ADD_LABEL = "ranged04";

   @RBEntry("Get")
   public static final String GET_LABEL = "ranged05";

   @RBEntry("Delete")
   public static final String DELETE_LABEL = "ranged06";

   @RBEntry("New Value")
   public static final String NEW_VALUE_TITLE = "ranged07";

   @RBEntry("Existing Value")
   public static final String EXIST_VALUE_TITLE = "ranged08";

   @RBEntry("List of Values")
   public static final String LIST_VALUE_TITLE = "ranged09";

   /**
    * Navigation Tab and show attributes Radial Button
    **/
   @RBEntry("On")
   public static final String NAVIGATION_TAB_ON_LABEL = "rb1";

   @RBEntry("Off")
   public static final String NAVIGATION_TAB_OFF_LABEL = "rb2";

   @RBEntry("Show All")
   public static final String SHOW_ALL_ATTRIBUTES_LABEL = "rb3";

   @RBEntry("Show Query Form Attributes Only")
   public static final String SHOW_QUERY_FORM_ATTRIBUTES_LABEL = "rb4";

   /**
    * Titles
    **/
   @RBEntry("Select Navigation Structure")
   public static final String NAVIGATIONSTRUCTURE_SELECTOR_TITLE = "ti0";

   @RBEntry("Select Navigation Node")
   public static final String NAVIGATIONNODE_SELECTOR_TITLE = "ti1";

   @RBEntry("Update Navigation Structure")
   public static final String NAVIGATIONSTRUCTURE_EDITOR_TITLE = "ti2";

   @RBEntry("Update Navigation Node")
   public static final String NAVIGATIONNODE_EDITOR_TITLE = "ti3";

   @RBEntry("View Navigation Structure")
   public static final String NAVIGATIONSTRUCTURE_VIEWER_TITLE = "ti4";

   @RBEntry("View Navigation Node")
   public static final String NAVIGATIONNODE_VIEWER_TITLE = "ti5";

   @RBEntry("Create Navigation Structure")
   public static final String NSCREATOR_TITLE = "ti6";

   @RBEntry("Create Navigation Node")
   public static final String NNCREATOR_TITLE = "ti7";

   @RBEntry("Add Navigation Structure Tab")
   public static final String NSTCREATOR_TITLE = "ti8";

   @RBEntry("Navigation Structure")
   public static final String NAVIGATION_STRUCTURE_PAGE_TITLE = "ti15";

   /**
    * Override QueryForm Defaults
    **/
   @RBEntry("Override Query Form Defaults")
   public static final String OVERRIDE_QF_CHOOSER_TITLE = "ti9";

   @RBEntry("Create new query form based on:")
   public static final String CREATE_QF_LABEL = "la18";

   @RBEntry("Blank")
   public static final String BLANK = "la19";

   @RBEntry("Default")
   public static final String DEFAULT = "la20";

   @RBEntry("Parent")
   public static final String PARENT = "la21";

   @RBEntry("Classification Template")
   public static final String CLASSIFICATION_TEMPLATE = "la22";

   @RBEntry("Details of  \"{0}\"")
   public static final String DETAILS = "ti10";

   @RBEntry("Warning: ")
   public static final String WARNING_TITLE = "ti11";

   @RBEntry("Confirm Delete")
   public static final String DELETE_TAB_TITLE = "ti12";

   @RBEntry("Use Default")
   public static final String CONFIRM_USE_DEFAULT_TITLE = "ti13";

   @RBEntry("Navigation Administrator")
   public static final String NAVIGATION_ADMIN_TITLE = "ti14";

   /**
    * ToolTips
    **/
   @RBEntry("Launch Navigation Node Manager ")
   public static final String LAUNCHNNM_TOOLTIP = "tt0";

   @RBEntry("Launch Navigation Structure Manager ")
   public static final String LAUNCHNSM_TOOLTIP = "tt1";

   @RBEntry("This operation may take several minutes, do you want to continue?")
   public static final String RANGE_DETAILS_DELAY_WARNING_MESSAGE = "wx00";

   @RBEntry("Are you sure you want to use the default query form?")
   public static final String MSG_USE_DEFAULT = "wx01";

   @RBEntry("Are you sure you want to delete the \" {0} \" tab")
   public static final String MSG_DELETE_TAB = "wx02";

   @RBEntry("No Results Found")
   public static final String NO_RESULTS_MESSAGE = "wx03";
}
