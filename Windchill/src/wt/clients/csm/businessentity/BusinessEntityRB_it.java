package wt.clients.csm.businessentity;

import wt.util.resource.*;

public final class BusinessEntityRB_it extends wt.util.resource.NestableListResourceBundle {
   /**
    * gif path
    **/
   @RBEntry("wt/clients/images/createbusenticn.gif")
   @RBPseudo(false)
   public static final String BUSINESS_ENTITY_APPLET_GIF = "gf00";

   /**
    * businessEntity creator labels
    **/
   @RBEntry("Nome:")
   public static final String BUSINESSENTITY_CREATOR_NAME_LABEL = "la00";

   @RBEntry("Modifica")
   public static final String BUSINESSENTITY_EDITOR_EDIT_BUTTON = "la01";

   @RBEntry("Elimina")
   public static final String BUSINESSENTITY_EDITOR_DELETE_BUTTON = "la02";

   @RBEntry("Guida")
   public static final String BUSINESSENTITY_EDITOR_HELP_BUTTON = "la03";

   /**
    * messages
    **/
   @RBEntry("Specificare un nome")
   public static final String BLANK_NAME = "sm00";

   @RBEntry("Conferma eliminazione")
   public static final String CONFIRM_DELETE_DIALOG_TITLE = "bti0";

   @RBEntry("Eliminare {0}?")
   public static final String CONFIRM_DELETE = "bti1";

   @RBEntry("Errore durante il tentativo di eliminare l'entità aziendale \"{0}\":\n\n {1}")
   public static final String DELETE_BUSINESSENTITY_FAILED = "bti2";

   @RBEntry("L'oggetto dell'entità aziendale è stato eliminato.")
   public static final String SUCCESSFUL = "bti3";

   @RBEntry("Non è un oggetto BusinessEntity.")
   public static final String OBJECT_NOT_BUSINESSENTITY = "er1";

   @RBEntry("Nessun delegato di visualizzazione associato all'entità aziendale.")
   public static final String ERROR_NO_VIEW_DELEGATE = "er2";

   @RBEntry("Errore durante il recupero dell'oggetto selezionato. Vedere\n la console Java per informazioni dettagliate.")
   public static final String ERROR_GET_HEAVY_OBJ = "er3";

   /**
    * Titles
    **/
   @RBEntry("Aggiorna entità aziendale")
   public static final String BUSINESSENTITY_EDITOR_TITLE = "ti00";

   @RBEntry("Visualizza entità aziendale")
   public static final String BUSINESSENTITY_VIEWER_TITLE = "ti01";

   @RBEntry("Crea entità aziendale")
   public static final String BUSINESSENTITY_CREATOR_TITLE = "ti02";

   /**
    * help url
    **/
   @RBEntry("wt/helpfiles/help_it/online/wsf/Admin_Guide/BussEntityHelp.html")
   @RBPseudo(false)
   public static final String HELPURL = "url0";
}
