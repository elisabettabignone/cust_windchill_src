/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.csm.widgets;

import wt.util.resource.*;

@RBUUID("wt.clients.csm.widgets.WidgetsRB")
public final class WidgetsRB extends wt.util.resource.NestableListResourceBundle {
   @RBEntry("The following error occurred when trying to create the criteria:\n\n{0}")
   public static final String CREATE_CRITERIA_FAILED = "ce00";

   /**
    * Results table 1st column header
    **/
   @RBEntry("Item")
   public static final String COLUMN_1_HEADER = "cl0";

   @RBEntry("Exact")
   public static final String CRITERIA_EXACT = "cr00";

   @RBEntry("+/-")
   public static final String CRITERIA_PLUS_MINUS = "cr01";

   @RBEntry("Absolute")
   public static final String CRITERIA_ABSOLUTE = "cr02";

   @RBEntry("Percent")
   public static final String CRITERIA_PERCENT = "cr03";

   @RBEntry("Exact")
   public static final String CRITERIA_STRING_EXACT = "cr04";

   @RBEntry("Begins with")
   public static final String CRITERIA_STRING_BEGINSWITH = "cr05";

   @RBEntry("Ends with")
   public static final String CRITERIA_STRING_ENDSWITH = "cr06";

   @RBEntry("Contains")
   public static final String CRITERIA_STRING_CONTAINS = "cr07";

   @RBEntry("Seconds")
   public static final String CRITERIA_TIMESTAMP_SECONDS = "cr08";

   @RBEntry("Minutes")
   public static final String CRITERIA_TIMESTAMP_MINUTES = "cr09";

   @RBEntry("Hours")
   public static final String CRITERIA_TIMESTAMP_HOURS = "cr10";

   @RBEntry("Days")
   public static final String CRITERIA_TIMESTAMP_DAYS = "cr11";

   @RBEntry("Months")
   public static final String CRITERIA_TIMESTAMP_MONTHS = "cr12";

   @RBEntry("Years")
   public static final String CRITERIA_TIMESTAMP_YEARS = "cr13";

   @RBEntry("Invalid type passed: {0}.  The valid type is: {1}")
   public static final String INVALID_PARAM_PASSED = "e02";

   @RBEntry("\"{0}\" is an invalid number format")
   public static final String INVALID_NUMBER_FORMAT = "e03";

   @RBEntry("\"{0}\" already added to the query form.\n")
   public static final String ATTRIBUTE_EXIST = "e04";

   @RBEntry("\"{0}\" is already added to the set.\n")
   public static final String VALUE_EXIST = "e05";

   @RBEntry("The following error occurred when trying to get the value from definition \"{0}\":\n\n{1}")
   public static final String GET_SHOWN_VALUE_FAILED = "ex00";

   @RBEntry("The following error occurred when trying to get new values from definition \"{0}\":\n\n{1}")
   public static final String GET_NEW_VALUES_FAILED = "ex01";

   /**
    * Image FileChooser
    **/
   @RBEntry("Select Image...")
   public static final String FILECHOOSER_TITLE = "fch00";

   @RBEntry("Image Previewer")
   public static final String FILECHOOSER_IMAGEPREVIEW = "fch01";

   @RBEntry("Find...")
   public static final String FILECHOOSER_BUTTON = "fch02";

   @RBEntry("Attribute")
   public static final String HEADER_FUNCEQUIVSEARCH_ATTRIBUTE = "fqshdr00";

   @RBEntry("Include")
   public static final String HEADER_FUNCEQUIVSEARCH_INCLUDE = "fqshdr01";

   @RBEntry("Match Nulls")
   public static final String HEADER_FUNCEQUIVSEARCH_MATCH_NULLS = "fqshdr02";

   @RBEntry("Value")
   public static final String HEADER_FUNCEQUIVSEARCH_VALUE = "fqshdr03";

   @RBEntry("Criteria")
   public static final String HEADER_FUNCEQUIVSEARCH_RANGE = "fqshdr04";

   @RBEntry("The following error occurred while trying to get all the mesurement systems:\n\n {0}")
   public static final String GET_MEASUREMENT_SYSTEM_FAILED = "gms00";

   @RBEntry("Attribute")
   public static final String HEADER_QUERYFORM_ATTRIBUTE = "hdr00";

   @RBEntry("Value")
   public static final String HEADER_QUERYFORM_VALUE = "hdr01";

   @RBEntry("Visible")
   public static final String HEADER_QUERYFORM_VISIBLE = "hdr02";

   @RBEntry("Enforce Dependency")
   public static final String HEADER_QUERYFORM_DEPENDENCY = "hdr03";

   @RBEntry("Order")
   public static final String HEADER_QUERYFORM_ORDER = "hdr04";

   @RBEntry("Include")
   public static final String HEADER_QUERYFORM_INCLUDE = "hdr05";

   @RBEntry("wt/clients/images/match.gif")
   @RBPseudo(false)
   public static final String IMAGE_INRANGE_ARROW = "im00";

   @RBEntry("wt/clients/images/nomatch.gif")
   @RBPseudo(false)
   public static final String IMAGE_OUTRANGE_ARROW = "im01";

   @RBEntry("wt/clients/images/visible.gif")
   @RBPseudo(false)
   public static final String IMAGE_QUERYFORM_VISIBLE = "im03";

   @RBEntry("wt/clients/images/dependency.gif")
   @RBPseudo(false)
   public static final String IMAGE_QUERYFORM_DEPENDENCY = "im04";

   @RBEntry("wt/clients/images/ordercolhd.gif")
   @RBPseudo(false)
   public static final String IMAGE_QUERYFORM_ORDER = "im05";

   @RBEntry("wt/clients/images/include.gif")
   @RBPseudo(false)
   public static final String IMAGE_QUERYFORM_INCLUDE = "im06";

   @RBEntry("Search")
   public static final String SEARCH_BUTTON_LABEL = "l0";

   @RBEntry("Reset")
   public static final String CLEAR_BUTTON_LABEL = "l1";

   @RBEntry("Select a field type for each attribute:")
   public static final String SELECT_FIELD_TYPE = "la00";

   @RBEntry("Add Attributes, Remove Attributes")
   public static final String QUERYFORM_BUTTON_LABELS = "la01";

   @RBEntry("Add")
   public static final String CONSTRAINT_ADDBUTTON = "lb00";

   @RBEntry("Remove")
   public static final String CONSTRAINT_REMOVEBUTTON = "lb01";

   /**
    * Tooltip for check-out
    **/
   @RBEntry("Checked-out")
   public static final String TOOLTIP_CHECK_OUT = "tp0";

   @RBEntry("From:")
   public static final String FROM_LABEL = "lb02";

   @RBEntry("To:")
   public static final String TO_LABEL = "lb03";

   @RBEntry("Maximum:")
   public static final String CONSTRAINT_WIDGET_MAXIMUM = "lc00";

   @RBEntry("Minimum:")
   public static final String CONSTRAINT_WIDGET_MINIMUM = "lc01";

   @RBEntry("Minimum value cannot be greater than Maximum value")
   public static final String MIN_MAX_ERROR = "mm00";

   @RBEntry("Wild Card")
   public static final String QUERYFORM_WILDCARD = "qft102";

   @RBEntry("Include")
   public static final String INCLUDE = "qft103";

   @RBEntry("Attribute")
   public static final String ATTRIBUTE = "qft104";

   @RBEntry("Field Type")
   public static final String FIELD_TYPE = "qft105";

   @RBEntry("Range")
   public static final String QUERYFORM_RANGE = "qftl00";

   @RBEntry("Discrete Set")
   public static final String QUERYFORM_DISCRETE_SET = "qftl01";

   @RBEntry("Update Query Form")
   public static final String QUERYFORM_EDITOR_TITLE = "ti00";

   @RBEntry("View Query Form")
   public static final String QUERYFORM_VIEWER_TITLE = "ti01";

   @RBEntry("Do you want to remove the selected Attribute(s)?")
   public static final String CONFIRM_DELETE = "ti02";

   @RBEntry("Confirm Delete")
   public static final String CONFIRM_DELETE_DIALOG_TITLE = "ti03";

   @RBEntry("Multi-Attribute Field Selection")
   public static final String QF_FIELD_TYPE_SELECTOR_TITLE = "ti04";

   /**
    * Data type Tooltip
    **/
   @RBEntry("Data Type: Boolean\n")
   public static final String BOOLEAN_TOOLTIP = "tooltip_wt.iba.definition.BooleanDefinition";

   @RBEntry("Data Type: Float\n")
   public static final String FLOAT_TOOLTIP = "tooltip_wt.iba.definition.FloatDefinition";

   @RBEntry("Data Type: Integer\n")
   public static final String INTEGER_TOOLTIP = "tooltip_wt.iba.definition.IntegerDefinition";

   @RBEntry("Data Type: Ratio\n")
   public static final String RATIO_TOOLTIP = "tooltip_wt.iba.definition.RatioDefinition";

   @RBEntry("Data Type: Reference\n")
   public static final String REFERENCE_TOOLTIP = "tooltip_wt.iba.definition.ReferenceDefinition";

   @RBEntry("Data Type: String\n")
   public static final String STRING_TOOLTIP = "tooltip_wt.iba.definition.StringDefinition";

   @RBEntry("Data Type: Timestamp\n")
   public static final String TIMESTAMP_TOOLTIP = "tooltip_wt.iba.definition.TimestampDefinition";

   @RBEntry("Data Type: URL\n")
   public static final String URL_TOOLTIP = "tooltip_wt.iba.definition.URLDefinition";

   @RBEntry("Data Type: Unit\n")
   public static final String UNIT_TOOLTIP = "tooltip_wt.iba.definition.UnitDefinition";
}
