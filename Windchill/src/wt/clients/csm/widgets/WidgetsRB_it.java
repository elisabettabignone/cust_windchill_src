/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.csm.widgets;

import wt.util.resource.*;

@RBUUID("wt.clients.csm.widgets.WidgetsRB")
public final class WidgetsRB_it extends wt.util.resource.NestableListResourceBundle {
   @RBEntry("Errore durante la creazione dei criteri:\n\n{0}")
   public static final String CREATE_CRITERIA_FAILED = "ce00";

   /**
    * Results table 1st column header
    **/
   @RBEntry("Elemento")
   public static final String COLUMN_1_HEADER = "cl0";

   @RBEntry("Esatto")
   public static final String CRITERIA_EXACT = "cr00";

   @RBEntry("+/-")
   public static final String CRITERIA_PLUS_MINUS = "cr01";

   @RBEntry("Assoluto")
   public static final String CRITERIA_ABSOLUTE = "cr02";

   @RBEntry("Percentuale")
   public static final String CRITERIA_PERCENT = "cr03";

   @RBEntry("Esatto")
   public static final String CRITERIA_STRING_EXACT = "cr04";

   @RBEntry("Inizia con")
   public static final String CRITERIA_STRING_BEGINSWITH = "cr05";

   @RBEntry("Termina con")
   public static final String CRITERIA_STRING_ENDSWITH = "cr06";

   @RBEntry("Contiene")
   public static final String CRITERIA_STRING_CONTAINS = "cr07";

   @RBEntry("Secondi")
   public static final String CRITERIA_TIMESTAMP_SECONDS = "cr08";

   @RBEntry("Minuti")
   public static final String CRITERIA_TIMESTAMP_MINUTES = "cr09";

   @RBEntry("Ore")
   public static final String CRITERIA_TIMESTAMP_HOURS = "cr10";

   @RBEntry("Giorni")
   public static final String CRITERIA_TIMESTAMP_DAYS = "cr11";

   @RBEntry("Mesi")
   public static final String CRITERIA_TIMESTAMP_MONTHS = "cr12";

   @RBEntry("Anni")
   public static final String CRITERIA_TIMESTAMP_YEARS = "cr13";

   @RBEntry("È stato inviato un tipo non corretto: {0}. Il tipo valido è: {1}")
   public static final String INVALID_PARAM_PASSED = "e02";

   @RBEntry("\"{0}\" non è un formato numerico valido")
   public static final String INVALID_NUMBER_FORMAT = "e03";

   @RBEntry("\"{0}\" è stato già aggiunto al formulario d'interrogazione.\n")
   public static final String ATTRIBUTE_EXIST = "e04";

   @RBEntry("\"{0}\" è stato già aggiunto alla serie.\n")
   public static final String VALUE_EXIST = "e05";

   @RBEntry("Errore durante il prelievo del valore dalla definizione \"{0}\":\n\n {1}")
   public static final String GET_SHOWN_VALUE_FAILED = "ex00";

   @RBEntry("Errore durante il prelievo di nuovi valori dalla definizione \"{0}\":\n\n {1}")
   public static final String GET_NEW_VALUES_FAILED = "ex01";

   /**
    * Image FileChooser
    **/
   @RBEntry("Seleziona immagine...")
   public static final String FILECHOOSER_TITLE = "fch00";

   @RBEntry("Anteprima d'immagine")
   public static final String FILECHOOSER_IMAGEPREVIEW = "fch01";

   @RBEntry("Trova...")
   public static final String FILECHOOSER_BUTTON = "fch02";

   @RBEntry("Attributo")
   public static final String HEADER_FUNCEQUIVSEARCH_ATTRIBUTE = "fqshdr00";

   @RBEntry("Includi")
   public static final String HEADER_FUNCEQUIVSEARCH_INCLUDE = "fqshdr01";

   @RBEntry("Corrispondenze nulle")
   public static final String HEADER_FUNCEQUIVSEARCH_MATCH_NULLS = "fqshdr02";

   @RBEntry("Valore")
   public static final String HEADER_FUNCEQUIVSEARCH_VALUE = "fqshdr03";

   @RBEntry("Criteri")
   public static final String HEADER_FUNCEQUIVSEARCH_RANGE = "fqshdr04";

   @RBEntry("Errore durante il prelievo di tutti i sistemi di misurazione:\n\n {0}")
   public static final String GET_MEASUREMENT_SYSTEM_FAILED = "gms00";

   @RBEntry("Attributo")
   public static final String HEADER_QUERYFORM_ATTRIBUTE = "hdr00";

   @RBEntry("Valore")
   public static final String HEADER_QUERYFORM_VALUE = "hdr01";

   @RBEntry("Visibile")
   public static final String HEADER_QUERYFORM_VISIBLE = "hdr02";

   @RBEntry("Imponi dipendenza")
   public static final String HEADER_QUERYFORM_DEPENDENCY = "hdr03";

   @RBEntry("Ordine")
   public static final String HEADER_QUERYFORM_ORDER = "hdr04";

   @RBEntry("Includi")
   public static final String HEADER_QUERYFORM_INCLUDE = "hdr05";

   @RBEntry("wt/clients/images/match.gif")
   @RBPseudo(false)
   public static final String IMAGE_INRANGE_ARROW = "im00";

   @RBEntry("wt/clients/images/nomatch.gif")
   @RBPseudo(false)
   public static final String IMAGE_OUTRANGE_ARROW = "im01";

   @RBEntry("wt/clients/images/visible.gif")
   @RBPseudo(false)
   public static final String IMAGE_QUERYFORM_VISIBLE = "im03";

   @RBEntry("wt/clients/images/dependency.gif")
   @RBPseudo(false)
   public static final String IMAGE_QUERYFORM_DEPENDENCY = "im04";

   @RBEntry("wt/clients/images/ordercolhd.gif")
   @RBPseudo(false)
   public static final String IMAGE_QUERYFORM_ORDER = "im05";

   @RBEntry("wt/clients/images/include.gif")
   @RBPseudo(false)
   public static final String IMAGE_QUERYFORM_INCLUDE = "im06";

   @RBEntry("Cerca")
   public static final String SEARCH_BUTTON_LABEL = "l0";

   @RBEntry("Reimposta")
   public static final String CLEAR_BUTTON_LABEL = "l1";

   @RBEntry("Seleziona un tipo di campo per ogni attributo:")
   public static final String SELECT_FIELD_TYPE = "la00";

   @RBEntry("Aggiungi attributi, rimuovi attributi")
   public static final String QUERYFORM_BUTTON_LABELS = "la01";

   @RBEntry("Aggiungi")
   public static final String CONSTRAINT_ADDBUTTON = "lb00";

   @RBEntry("Rimuovi")
   public static final String CONSTRAINT_REMOVEBUTTON = "lb01";

   /**
    * Tooltip for check-out
    **/
   @RBEntry("Sottoposto a Check-Out")
   public static final String TOOLTIP_CHECK_OUT = "tp0";

   @RBEntry("Da:")
   public static final String FROM_LABEL = "lb02";

   @RBEntry("A:")
   public static final String TO_LABEL = "lb03";

   @RBEntry("Massimo:")
   public static final String CONSTRAINT_WIDGET_MAXIMUM = "lc00";

   @RBEntry("Minimo:")
   public static final String CONSTRAINT_WIDGET_MINIMUM = "lc01";

   @RBEntry("Il valore minimo non può essere superiore al valore massimo")
   public static final String MIN_MAX_ERROR = "mm00";

   @RBEntry("Carattere jolly")
   public static final String QUERYFORM_WILDCARD = "qft102";

   @RBEntry("Includi")
   public static final String INCLUDE = "qft103";

   @RBEntry("Attributo")
   public static final String ATTRIBUTE = "qft104";

   @RBEntry("Tipo di campo")
   public static final String FIELD_TYPE = "qft105";

   @RBEntry("Intervallo")
   public static final String QUERYFORM_RANGE = "qftl00";

   @RBEntry("Serie di valori")
   public static final String QUERYFORM_DISCRETE_SET = "qftl01";

   @RBEntry("Aggiorna formulario d'interrogazione")
   public static final String QUERYFORM_EDITOR_TITLE = "ti00";

   @RBEntry("Visualizza formulario d'interrogazione")
   public static final String QUERYFORM_VIEWER_TITLE = "ti01";

   @RBEntry("Rimuovere gli attributi selezionati?")
   public static final String CONFIRM_DELETE = "ti02";

   @RBEntry("Conferma eliminazione")
   public static final String CONFIRM_DELETE_DIALOG_TITLE = "ti03";

   @RBEntry("Selezione di campo con attributi multipli")
   public static final String QF_FIELD_TYPE_SELECTOR_TITLE = "ti04";

   /**
    * Data type Tooltip
    **/
   @RBEntry("Tipo di dati: booleano\n")
   public static final String BOOLEAN_TOOLTIP = "tooltip_wt.iba.definition.BooleanDefinition";

   @RBEntry("Tipo di dati: virgola mobile\n")
   public static final String FLOAT_TOOLTIP = "tooltip_wt.iba.definition.FloatDefinition";

   @RBEntry("Tipo di dati: intero\n")
   public static final String INTEGER_TOOLTIP = "tooltip_wt.iba.definition.IntegerDefinition";

   @RBEntry("Tipo di dati: rapporto\n")
   public static final String RATIO_TOOLTIP = "tooltip_wt.iba.definition.RatioDefinition";

   @RBEntry("Tipo di dati: riferimento\n")
   public static final String REFERENCE_TOOLTIP = "tooltip_wt.iba.definition.ReferenceDefinition";

   @RBEntry("Tipo di dati: stringa\n")
   public static final String STRING_TOOLTIP = "tooltip_wt.iba.definition.StringDefinition";

   @RBEntry("Tipo di dati: ora e data\n")
   public static final String TIMESTAMP_TOOLTIP = "tooltip_wt.iba.definition.TimestampDefinition";

   @RBEntry("Tipo di dati: URL\n")
   public static final String URL_TOOLTIP = "tooltip_wt.iba.definition.URLDefinition";

   @RBEntry("Tipo di dati: unità\n")
   public static final String UNIT_TOOLTIP = "tooltip_wt.iba.definition.UnitDefinition";
}
