package wt.clients.csm.ranking.rankingdata;

import wt.util.resource.*;

public final class RankingDataRB_it extends wt.util.resource.NestableListResourceBundle {
   @RBEntry("Normalizzazione")
   public static final String WEIGHTED_AVERAGE_CRITERIA_NORMALIZATION_LABEL = "113";

   @RBEntry("100")
   public static final String WEIGHTED_AVERAGE_CRITERIA_NORMALIZATION_VALUE = "114";

   @RBEntry("wt/clients/images/localsearch.gif")
   @RBPseudo(false)
   public static final String LOCALSEARCH_ICON = "i00";

   @RBEntry("wt/clients/images/reusemgricon.gif")
   @RBPseudo(false)
   public static final String REUSEMGR_ICON = "i01";

   /**
    * Rank Labels
    **/
   @RBEntry("Aggiungi")
   public static final String ADD_LABEL = "l00";

   @RBEntry("Elimina")
   public static final String DELETE_LABEL = "l01";

   @RBEntry("Reimposta")
   public static final String CLEAR_LABEL = "l02";

   @RBEntry("Pubblica")
   public static final String PUBLISH_LABEL = "l04";

   @RBEntry("Calcola")
   public static final String CALCULATE_LABEL = "l05";

   @RBEntry("Valore classificazione:")
   public static final String RANK_VALUE_LABEL = "l06";

   /**
    * Criteria Labels
    **/
   @RBEntry("Scegli")
   public static final String SIMPLE_CRITERIA_CHOOSE_LABEL = "l07";

   @RBEntry("Non usare, Non approvato, Approvato, Preferito")
   public static final String SIMPLE_CRITERIA_LABELS = "l08";

   @RBEntry("25, 50, 75, 100")
   @RBPseudo(false)
   public static final String SIMPLE_CRITERIA_VALUES = "l09";

   @RBEntry("Media ponderata")
   public static final String WEIGHTED_AVERAGE_CRITERIA_CHOOSE_LABEL = "l10";

   @RBEntry("Test1, Test2, Test3, Test4")
   @RBPseudo(false)
   public static final String WEIGHTED_AVERAGE_CRITERIA_LABELS = "l11";

   @RBEntry("10, 20, 30, 40")
   @RBPseudo(false)
   public static final String WEIGHTED_AVERAGE_CRITERIA_VALUES = "l12";

   /**
    * Messages
    **/
   @RBEntry("Tutti i contenitori di attributi d'istanza (IBAHolders) sono stati classificati. Reimpostare la selezione?")
   public static final String RANK_SUCCESSFUL_MESSAGE = "m00";

   @RBEntry("Seleziona la Ricerca locale o la Gestione riutilizzo per classificare le informazioni")
   public static final String RANK_BLANK_MESSAGE = "m01";

   /**
    * Titles
    **/
   @RBEntry("Contesto")
   public static final String RD_SELECTOR_TITLE = "t00";

   @RBEntry("Modifica i dati di classificazione")
   public static final String RD_EDITOR_TITLE = "t01";

   @RBEntry("Visualizza i dati di classificazione")
   public static final String RD_VIEWER_TITLE = "t02";

   @RBEntry("Cerca classe")
   public static final String RD_SEARCH_SELECTOR_TITLE = "t03";

   @RBEntry("Criteri")
   public static final String RD_CRITERIA_TITLE = "t04";

   @RBEntry("Amministrazione dati di classificazione")
   public static final String RD_ADMIN_TITLE = "t05";

   /**
    * ToolTips
    **/
   @RBEntry("Ricerca locale")
   public static final String LOCALSEARCH_TOOLTIP = "tt00";

   @RBEntry("Ricerca Gestione riutilizzo")
   public static final String REUSEMGR_TOOLTIP = "tt01";

   @RBEntry("wt/helpfiles/help_it/online/wsf/Admin_Guide/rankdata.html")
   @RBPseudo(false)
   public static final String RDHELPPAGE = "url1";
}
