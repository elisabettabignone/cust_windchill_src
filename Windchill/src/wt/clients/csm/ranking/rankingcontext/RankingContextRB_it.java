package wt.clients.csm.ranking.rankingcontext;

import wt.util.resource.*;

public final class RankingContextRB_it extends wt.util.resource.NestableListResourceBundle {
   /**
    * Exception messages
    **/
   @RBEntry("Errore durante il salvataggio del valore \"{0}\" della definizione \"{1}\":\n\n{2}")
   public static final String SET_VALUE_FAILED = "e00";

   @RBEntry("Errore durante il prelievo di nuovi valori per la definizione \"{0}\":\n\n{1}")
   public static final String GET_NEW_VALUES_FAILED = "e01";

   @RBEntry("È stato inviato un tipo non corretto: {0}. Il tipo valido è: {1}")
   public static final String INVALID_PARAM_PASSED = "e02";

   @RBEntry("\"{0}\" è un formato di numero non valido per l'attributo \"{1}\"")
   public static final String INVALID_NUMBER_FORMAT = "e03";

   @RBEntry("Errore durante il prelievo di nuovi valori per la definizione \"{0}\":\n\n{1}")
   public static final String GET_SHOWN_VALUE_FAILED = "e04";

   @RBEntry("wt/helpfiles/help_it/online/wsf/Admin_Guide/rankcontext.html")
   @RBPseudo(false)
   public static final String RCHELPPAGE = "e06";

   @RBEntry("Creazione di \"{0}\" fallita:\n\n {1}")
   public static final String CREATE_RANKING_CONTEXT_FAILED = "e07";

   @RBEntry("Eliminazione di \"{0}\" fallita:\n\n {1}")
   public static final String DELETE_RANKING_CONTEXT_FAILED = "e08";

   @RBEntry("Aggiornamento di \"{0}\" fallito:\n\n {1}")
   public static final String UPDATE_RANKING_CONTEXT_FAILED = "e09";

   @RBEntry("Il recupero del contesto di classificazione è fallito:\n\n {0}")
   public static final String GET_ALL_RANKING_CONTEXT_FAILED = "e10";

   @RBEntry("'Nome' è un campo obbligatorio.")
   public static final String RC_BLANK_NAME = "e11";

   @RBEntry("Il contesto di classificazione esiste già.")
   public static final String RC_ALREADY_EXISTS = "e12";

   @RBEntry("Classe classificabile, Criteri")
   public static final String SELECT_CRITERIA_TABLE_HEADERS = "hdr00";

   @RBEntry("wt/clients/images/find.gif")
   @RBPseudo(false)
   public static final String IMAGE_FIND = "im00";

   @RBEntry("Nome:")
   public static final String NAME_LABEL = "lb00";

   @RBEntry("Descrizione:")
   public static final String DESCRIPTION_LABEL = "lb01";

   @RBEntry("Seleziona criteri:")
   public static final String SELECT_CRITERIA_LABEL = "lb02";

   @RBEntry("Scegli...")
   public static final String CHOOSE_LABEL = "lb03";

   @RBEntry("Contesto di classificazione:")
   public static final String RC_CONTEXT_LABEL = "lb04";

   /**
    * Titles
    **/
   @RBEntry("Imposta contesto di classificazione")
   public static final String RC_SELECTOR_TITLE = "t00";

   @RBEntry("Modifica contesto di classificazione")
   public static final String RC_EDITOR_TITLE = "t01";

   @RBEntry("Visualizza contesto di classificazione")
   public static final String RC_VIEWER_TITLE = "t02";

   @RBEntry("Crea contesto di classificazione")
   public static final String RC_CREATOR_TITLE = "t03";

   @RBEntry("Amministrazione contesti classificazione")
   public static final String RC_ADMIN_TITLE = "t04";
}
