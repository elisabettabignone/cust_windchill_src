/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.saveas;

import wt.util.resource.*;

@RBUUID("wt.clients.saveas.SaveAsRB")
public final class SaveAsRB extends WTListResourceBundle {
   /**
    * DIALOG LABELS -------------------------------------------------------------
    **/
   @RBEntry("Name:")
   @RBComment("Labels used to identify fields on the dialogs")
   public static final String PRIVATE_CONSTANT_0 = "nameLabel";

   @RBEntry("Number:")
   @RBComment("Labels used to identify fields on the dialogs")
   public static final String PRIVATE_CONSTANT_1 = "numberLabel";

   @RBEntry("Location:")
   @RBComment("Labels used to identify fields on the dialogs")
   public static final String PRIVATE_CONSTANT_2 = "locationLabel";

   @RBEntry("View:")
   @RBComment("Labels used to identify fields on the dialogs")
   public static final String PRIVATE_CONSTANT_3 = "viewLabel";

   /**
    * BUTTON LABELS ---------------------------------------------------------
    **/
   @RBEntry("OK")
   @RBComment("Labels used on command buttons")
   public static final String PRIVATE_CONSTANT_4 = "okButton";

   @RBEntry("Cancel")
   @RBComment("Labels used on command buttons")
   public static final String PRIVATE_CONSTANT_5 = "cancelButton";

   @RBEntry("Help")
   @RBComment("Labels used on command buttons")
   public static final String PRIVATE_CONSTANT_6 = "helpButton";

   @RBEntry("Browse")
   @RBComment("Labels used on command buttons")
   public static final String PRIVATE_CONSTANT_7 = "browseButton";

   /**
    * SYMBOLS ----------------------------------------------------------------
    **/
   @RBEntry("...")
   @RBComment("used to indicate more information will be needed from the user (i.e. 'Browse...').")
   public static final String PRIVATE_CONSTANT_8 = "ellipses";

   @RBEntry("*")
   public static final String PRIVATE_CONSTANT_9 = "required";

   /**
    * DIALOG TITLES ----------------------------------------------------------
    * 
    **/
   @RBEntry("Save As")
   @RBComment("Titles displayed on dialogs.")
   public static final String PRIVATE_CONSTANT_10 = "saveAsTitle";

   @RBEntry("Save As")
   @RBComment("Titles displayed on dialogs.")
   public static final String PRIVATE_CONSTANT_11 = "saveAsViewTitle";

   @RBEntry("Saving From")
   @RBComment("Titles displayed on dialogs.")
   public static final String PRIVATE_CONSTANT_12 = "savingFrom";

   /**
    * ERROR MESSAGES -----------------------------------------------------------
    * 
    **/
   @RBEntry("An error occurred while initializing localization resources:  ")
   public static final String RESOURCE_BUNDLE_ERROR = "1";

   @RBEntry("\"{0}\" is not a valid value for property \"{1}\".")
   public static final String PROPERTY_VETO_EXCEPTION = "2";

   @RBEntry("You are not allowed to revise \"{0}\".")
   public static final String COPY_NOT_ALLOWED = "3";

   @RBEntry("\"{0}\" cannot be assigned to a view.")
   public static final String NOT_VIEW_COPYABLE = "4";

   @RBEntry("No eligible views for object.")
   public static final String NO_VIEWS = "5";

   @RBEntry("Unable to initialize class {0}")
   public static final String UNABLE_TO_INITIALIZE_CLASS = "6";

   @RBEntry("The location field contains the following error:  {0}")
   public static final String LOCATION_NOT_VALID = "7";

   @RBEntry("The name field contains the following error:  {0}")
   public static final String NAME_NOT_VALID = "8";

   @RBEntry("The number field contains the following error:  {0}")
   public static final String NUMBER_NOT_VALID = "9";

   @RBEntry("Save As <{0}>")
   public static final String SAVING_FROM_TITLE = "10";

   @RBEntry("Save As <{0}>")
   public static final String DUPLICATE_DIALOG_TITLE = "11";

   @RBEntry("Description:")
   public static final String DESCRIPTION_LABEL = "12";

   @RBEntry("Save As")
   public static final String DUPLICATE_DIALOG_EMPTY_TITLE = "13";

   @RBEntry("The name of the new configuration must be different from the name of the configuration being duplicated.  Please enter a name other than \"{0}\".")
   public static final String NAME_NOT_UNIQUE = "14";

   @RBEntry("Saving from {0}")
   @RBArgComment0("identity of object which is being duplicated")
   public static final String SAVING_FROM_OBJECT_TITLE = "15";
}
