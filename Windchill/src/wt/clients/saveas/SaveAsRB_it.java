/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.saveas;

import wt.util.resource.*;

@RBUUID("wt.clients.saveas.SaveAsRB")
public final class SaveAsRB_it extends WTListResourceBundle {
   /**
    * DIALOG LABELS -------------------------------------------------------------
    **/
   @RBEntry("Nome:")
   @RBComment("Labels used to identify fields on the dialogs")
   public static final String PRIVATE_CONSTANT_0 = "nameLabel";

   @RBEntry("Numero:")
   @RBComment("Labels used to identify fields on the dialogs")
   public static final String PRIVATE_CONSTANT_1 = "numberLabel";

   @RBEntry("Posizione:")
   @RBComment("Labels used to identify fields on the dialogs")
   public static final String PRIVATE_CONSTANT_2 = "locationLabel";

   @RBEntry("Vista:")
   @RBComment("Labels used to identify fields on the dialogs")
   public static final String PRIVATE_CONSTANT_3 = "viewLabel";

   /**
    * BUTTON LABELS ---------------------------------------------------------
    **/
   @RBEntry("OK")
   @RBComment("Labels used on command buttons")
   public static final String PRIVATE_CONSTANT_4 = "okButton";

   @RBEntry("Annulla")
   @RBComment("Labels used on command buttons")
   public static final String PRIVATE_CONSTANT_5 = "cancelButton";

   @RBEntry("Guida")
   @RBComment("Labels used on command buttons")
   public static final String PRIVATE_CONSTANT_6 = "helpButton";

   @RBEntry("Sfoglia")
   @RBComment("Labels used on command buttons")
   public static final String PRIVATE_CONSTANT_7 = "browseButton";

   /**
    * SYMBOLS ----------------------------------------------------------------
    **/
   @RBEntry("...")
   @RBComment("used to indicate more information will be needed from the user (i.e. 'Browse...').")
   public static final String PRIVATE_CONSTANT_8 = "ellipses";

   @RBEntry("*")
   public static final String PRIVATE_CONSTANT_9 = "required";

   /**
    * DIALOG TITLES ----------------------------------------------------------
    * 
    **/
   @RBEntry("Salva con nome")
   @RBComment("Titles displayed on dialogs.")
   public static final String PRIVATE_CONSTANT_10 = "saveAsTitle";

   @RBEntry("Salva con nome")
   @RBComment("Titles displayed on dialogs.")
   public static final String PRIVATE_CONSTANT_11 = "saveAsViewTitle";

   @RBEntry("Salva da")
   @RBComment("Titles displayed on dialogs.")
   public static final String PRIVATE_CONSTANT_12 = "savingFrom";

   /**
    * ERROR MESSAGES -----------------------------------------------------------
    * 
    **/
   @RBEntry("Errore durante l'inizializzazione delle risorse di localizzazione:")
   public static final String RESOURCE_BUNDLE_ERROR = "1";

   @RBEntry("\"{0}\" non è un valore valido per la proprietà \"{1}\".")
   public static final String PROPERTY_VETO_EXCEPTION = "2";

   @RBEntry("La revisione di \"{0}\" non è consentita.")
   public static final String COPY_NOT_ALLOWED = "3";

   @RBEntry("\"{0}\" non può essere assegnato ad una vista.")
   public static final String NOT_VIEW_COPYABLE = "4";

   @RBEntry("Nessuna vista per l'oggetto.")
   public static final String NO_VIEWS = "5";

   @RBEntry("Impossibile inizializzare la classe {0}")
   public static final String UNABLE_TO_INITIALIZE_CLASS = "6";

   @RBEntry("Il campo posizione contiene l'errore seguente:  {0}")
   public static final String LOCATION_NOT_VALID = "7";

   @RBEntry("Il campo nome contiene l'errore seguente:  {0}")
   public static final String NAME_NOT_VALID = "8";

   @RBEntry("Il campo numero contiene l'errore seguente:  {0}")
   public static final String NUMBER_NOT_VALID = "9";

   @RBEntry("Salva con nome: <{0}>")
   public static final String SAVING_FROM_TITLE = "10";

   @RBEntry("Salva con nome: <{0}>")
   public static final String DUPLICATE_DIALOG_TITLE = "11";

   @RBEntry("Descrizione:")
   public static final String DESCRIPTION_LABEL = "12";

   @RBEntry("Salva con nome")
   public static final String DUPLICATE_DIALOG_EMPTY_TITLE = "13";

   @RBEntry("Il nome della nuova configurazione deve essere diverso da quello della configurazione duplicata.  Immettere un nome diverso da \"{0}\".")
   public static final String NAME_NOT_UNIQUE = "14";

   @RBEntry("Salvataggio da {0} in corso")
   @RBArgComment0("identity of object which is being duplicated")
   public static final String SAVING_FROM_OBJECT_TITLE = "15";
}
