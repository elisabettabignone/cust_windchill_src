/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.saveas;

import wt.util.resource.*;

@RBUUID("wt.clients.saveas.SaveAsHelpRB")
public final class SaveAsHelpRB_it extends WTListResourceBundle {
   @RBEntry("DocMgrDocSaveAsNewOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_0 = "Help/saveas/SaveAs";

   @RBEntry("PIMPartSaveNew")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_1 = "Help/saveas/SaveAsView";

   @RBEntry("PIMPartSaveNew")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/duplicateconfiguration/DuplicateConfiguration";
}
