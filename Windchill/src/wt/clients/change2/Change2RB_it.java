/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.change2;

import wt.util.resource.*;

@RBUUID("wt.clients.change2.Change2RB")
public final class Change2RB_it extends WTListResourceBundle {
   @RBEntry("Parametri applet invalidi.  L'Id dell'oggetto è nullo.")
   @RBComment("Parameters passed to applet are invalid.")
   public static final String PRIVATE_CONSTANT_0 = "InvalidAppletParameters";

   @RBEntry("È necessario creare l'operazione di modifica partendo da un ordine di modifica.")
   @RBComment("The Change Activity must be created from within a Create or Update of a Change Order")
   public static final String PRIVATE_CONSTANT_1 = "CreateChangeActivityFromChangeOrder";

   @RBEntry("Crea suggerimento di modifica")
   public static final String CREATE_CHANGE_ISSUE = "0";

   @RBEntry("Trova suggerimento di modifica")
   public static final String FIND_CHANGE_ISSUE = "1";

   @RBEntry("Gestione modifiche")
   public static final String CHANGE_MANAGER = "2";

   @RBEntry("File allegato")
   public static final String FILE = "3";

   @RBEntry("GESTIONE MODIFICHE")
   public static final String CHANGE_MANAGER_CAPS = "4";

   @RBEntry("Richiesta")
   public static final String REQUEST = "5";

   @RBEntry("Esamina")
   public static final String INVESTIGATE = "6";

   @RBEntry("Proponi")
   public static final String PROPOSE = "7";

   @RBEntry("Implementa")
   public static final String IMPLEMENT = "8";

   @RBEntry("Modifica che ha determinato la revisione")
   public static final String CAUSED_THIS_REVISION = "9";

   @RBEntry("Modifica proposta per questa versione")
   public static final String PROPOSED_CHANGE = "10";

   @RBEntry("Analisi di riferimento")
   public static final String ANALYSIS_ACTIVITIES = "11";

   @RBEntry("Crea richiesta di modifica")
   public static final String CREATE_CHANGE_REQUEST = "12";

   @RBEntry("Trova oggetto di modifica")
   public static final String FIND_CHANGE_OBJECT = "13";

   @RBEntry("Introduzione")
   public static final String GETTING_STARTED = "14";

   @RBEntry("Windchill")
   public static final String WINDCHILL_HOME = "15";

   @RBEntry("Vecchia gestione modifiche")
   public static final String OLD_CHANGE_MANAGER = "16";

   @RBEntry("Pagina principale di Gestione modifiche")
   public static final String CHANGE_MANAGER_HOME = "17";

   @RBEntry("Aggiorna suggerimento modifiche")
   public static final String UPDATE_CHANGE_ISSUE = "18";

   @RBEntry("Visualizza suggerimento modifiche")
   public static final String VIEW_CHANGE_ISSUE = "19";

   @RBEntry("Errore: il riepilogo è obbligatorio")
   public static final String NAME_IS_REQUIRED = "20";

   @RBEntry("Errore: il richiedente è obbligatorio.")
   public static final String REQUESTER_IS_REQUIRED = "21";

   @RBEntry("Errore: la posizione è obbligatoria.")
   public static final String LOCATION_IS_REQUIRED = "22";

   @RBEntry("Errore: il ciclo di vita è obbligatorio.")
   public static final String LIFECYCLE_IS_REQUIRED = "23";

   @RBEntry("Trova dati interessati")
   public static final String FIND_PART_OR_DOCUMENT = "24";

   @RBEntry("Risultati della ricerca")
   public static final String SEARCH_RESULTS = "25";

   @RBEntry("Aggiungi")
   public static final String ADD_CHANGE_ISSUES = "26";

   @RBEntry("Aggiungi gli elementi selezionati")
   public static final String ADD_OBJECTS = "27";

   @RBEntry("Aggiunta avvenuta")
   public static final String SUCCESSFUL_ADD = "28";

   @RBEntry("invia")
   public static final String SUBMIT = "29";

   @RBEntry("Guida")
   public static final String VIEW_REQUEST = "30";

   @RBEntry("Guida")
   public static final String VIEW_INVESTIGATION = "31";

   @RBEntry("Guida")
   public static final String VIEW_PROPOSAL = "32";

   @RBEntry("Guida")
   public static final String VIEW_ORDER = "33";

   @RBEntry("Errore: la descrizione è obbligatoria")
   public static final String DESCRIPTION_IS_REQUIRED = "34";

   @RBEntry("Guida")
   public static final String VIEW_ISSUE = "35";

   @RBEntry("Questa azione cancellerà permanentemente l'oggetto di modifica. Continuare?")
   public static final String CONFIRM_DELETE = "36";

   @RBEntry("Aggiunta non riuscita")
   public static final String ADD_FAILED = "37";

   @RBEntry("Allegati della richiesta di modifica")
   public static final String REQUEST_ATTACHMENTS = "38";

   @RBEntry("Allegati dell'investigazione di modifica")
   public static final String INVESTIGATION_ATTACHMENTS = "39";

   @RBEntry("Allegati della proposta di modifica")
   public static final String PROPOSAL_ATTACHMENTS = "40";

   @RBEntry("Allegati dell'ordine di modifica")
   public static final String ORDER_ATTACHMENTS = "41";

   @RBEntry("Allegati dell'analisi")
   public static final String ANALYSIS_ACTIVITY_ATTACHMENTS = "42";

   @RBEntry("Allegati dell'operazione di modifica")
   public static final String CHANGE_ACTIVITY_ATTACHMENTS = "43";

   @RBEntry("Suggerimenti di modifica")
   public static final String CHANGE_ISSUE_HEADER = "44";

   @RBEntry("Analisi")
   public static final String ANALYSIS_ACTIVITY_HEADER = "45";

   @RBEntry("Operazioni di modifica")
   public static final String CHANGE_ACTIVITY_HEADER = "46";

   @RBEntry("Versioni nuove dei dati")
   public static final String NEW_REVISIONS = "47";

   @RBEntry("Versioni originali dei dati")
   public static final String ORIGINAL_REVISIONS = "48";

   @RBEntry("Dati referenziati")
   public static final String RELEVANT_PARTS_AND_DOCS = "49";

   @RBEntry("Rimuovi")
   public static final String REMOVE = "50";

   @RBEntry("Nome")
   public static final String NAME = "51";

   @RBEntry("Formato")
   public static final String FORMAT = "52";

   @RBEntry("Percorso")
   public static final String PATH = "53";

   @RBEntry("Aggiungi")
   public static final String ADD_CHANGEABLES = "54";

   @RBEntry("Allegati del suggerimento di modifica")
   public static final String CHANGE_ISSUE_ATTACHMENTS = "55";

   @RBEntry("Richiesta di modifica associata")
   public static final String ASSOCIATED_CHANGE_REQUEST = "56";

   @RBEntry("Proposta di modifica")
   public static final String NEW_CHANGE_PROPOSAL = "57";

   @RBEntry("Ordine di modifica")
   public static final String NEW_CHANGE_ORDER = "58";

   @RBEntry("Investigazione di modifica")
   public static final String NEW_CHANGE_INVESTIGATION = "59";

   @RBEntry("Analisi")
   public static final String NEW_ANALYSIS_ACTIVITY = "60";

   @RBEntry("Operazione di modifica")
   public static final String NEW_CHANGE_ACTIVITY = "61";

   @RBEntry("[Aggiorna]")
   public static final String UPDATE_MODE = "62";

   @RBEntry("[Crea]")
   public static final String CREATE_MODE = "63";

   @RBEntry("Sfoglia...")
   public static final String BROWSE = "64";

   @RBEntry("Crea suggerimento di modifica")
   public static final String CREATE_CHANGE_ISSUE_HOME = "65";

   @RBEntry("Crea richiesta di modifica")
   public static final String CREATE_CHANGE_REQUEST_HOME = "66";

   @RBEntry("Trova oggetto di modifica")
   public static final String FIND_CHANGE_OBJECT_HOME = "67";

   @RBEntry("Errore: immettere la data odierna o una successiva.")
   public static final String PAST_DATE_NOT_ALLOWED = "68";

   @RBEntry("Pagina principale Windchill")
   public static final String WINDCHILL_HOME_FULL = "69";

   @RBEntry("Richieste di modifica")
   public static final String CHANGE_REQUESTS = "70";

   @RBEntry("Aggiunge i suggerimenti selezionati")
   public static final String ATTACH_ISSUES = "71";

   @RBEntry("Firma proprietario modifiche")
   public static final String CHANGE_MANAGER_SIGNATURE = "72";

   @RBEntry("Prodotti referenziati")
   public static final String SUBJECT_PRODUCT = "73";

   @RBEntry("Aggiungi")
   public static final String ADD_SUBJECT_PRODUCT = "74";

   @RBEntry("Trova prodotto")
   public static final String FIND_SUBJECT_PRODUCT = "75";

   @RBEntry("Aggiungi i prodotti selezionati")
   public static final String ATTACH_PRODUCTS = "76";

   @RBEntry("Chiudi")
   @RBComment("Close button action label")
   public static final String CLOSE = "77";

   @RBEntry("Il suggerimento di modifica è stato salvato.")
   public static final String CHANGE_ISSUE_SUCCESSFULLY_SAVED = "78";
}
