package wt.clients.gwrevaulting;

import wt.util.resource.*;

public final class GWRevaultingHelpRB_it extends WTListResourceBundle {
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_0 = "Contents/gwrevaulting";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_1 = "Help/gwrevaulting";

   @RBEntry("wt/helpfiles/externalstorage/help_it/ExternalStorageRevaulting.html")
   @RBComment("Pointer to the help for this screen")
   public static final String PRIVATE_CONSTANT_2 = "Help/gwrevaulting/GWRevaulting";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_3 = "Desc/gwrevaulting/GWRevaulting";

   @RBEntry("Fare clic per chiudere la finestra")
   public static final String PRIVATE_CONSTANT_4 = "Desc/gwrevaulting/GWRevaulting/Close";

   @RBEntry("Fare clic per visualizzare la guida sulla Programmazione delle archiviazioni temporizzate")
   public static final String PRIVATE_CONSTANT_5 = "Desc/gwrevaulting/GWRevaulting/Help";
}
