package wt.clients.gwrevaulting;

import wt.util.resource.*;

public final class gwRevaultingResource_it extends WTListResourceBundle {
   @RBEntry("Archiviazioni temporizzate gateway")
   public static final String REVAULTING_TAB = "1";

   @RBEntry("Programmazione archiviazioni temporizzate gateway")
   public static final String REVAULTING_TITLE = "2";

   @RBEntry("Chiudi")
   public static final String CLOSE_BUTT = "3";

   @RBEntry("Guida")
   public static final String HELP_BUTT = "4";

   /**
    * Scheduling Framework properties
    * 
    **/
   @RBEntry("Cronologia archiviazioni temporizzate gateway")
   public static final String PRIVATE_CONSTANT_0 = "schedulelogtitle";

   @RBEntry("Cronologia operazioni di archiviazione temporizzata gateway:")
   public static final String PRIVATE_CONSTANT_1 = "schedulelogdescr";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_2 = "schedulelogheader";

   @RBEntry("Programmazione archiviazioni temporizzate gateway")
   public static final String PRIVATE_CONSTANT_3 = "scheduleTimeTitle";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_4 = "scheduleTimeObjectLab";
}
