/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.scheduler;

import wt.util.resource.*;

@RBUUID("wt.clients.scheduler.GenericSchedTimeFrameHelpRB")
public final class GenericSchedTimeFrameHelpRB extends WTListResourceBundle {
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_0 = "Contents/scheduler";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_1 = "Help/scheduler";

   @RBEntry("FileVaultRevaultScheduleRevault")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/scheduler/GenericSchedTimeFrame";

   @RBEntry("ExtStgRevaultSched")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_3 = "Help/scheduler/PDM_GenericSchedTimeFrame";

   @RBEntry("ExtStgRevaultSched")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_4 = "Help/scheduler/PJL_GenericSchedTimeFrame";

   @RBEntry("VaultReplScheduleAdministratorRef")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_5 = "Help/scheduler/ReplSchedTimeFrame";

   @RBEntry("VaultReplScheduleAdministratorRef")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_6 = "Help/scheduler/PDM_ReplSchedTimeFrame";

   @RBEntry("VaultReplScheduleAdministratorRef")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_7 = "Help/scheduler/PJL_ReplSchedTimeFrame";

   @RBEntry(" ")
   @RBComment("default description")
   public static final String PRIVATE_CONSTANT_8 = "Desc/scheduler/GenericSchedTimeFrame";

   @RBEntry("Execute immediately, start date/time ignored")
   public static final String PRIVATE_CONSTANT_9 = "Desc/scheduler/GenericSchedTimeFrame/Immediate";

   @RBEntry("Begin execution at the specified date/time")
   public static final String PRIVATE_CONSTANT_10 = "Desc/scheduler/GenericSchedTimeFrame/OnSchedule";

   @RBEntry("Enter the time to execute the method")
   public static final String PRIVATE_CONSTANT_11 = "Desc/scheduler/GenericSchedTimeFrame/TimeField";

   @RBEntry("Enter the date to execute the method")
   public static final String PRIVATE_CONSTANT_12 = "Desc/scheduler/GenericSchedTimeFrame/DateField";

   @RBEntry("Execute the scheduled method once")
   public static final String PRIVATE_CONSTANT_13 = "Desc/scheduler/GenericSchedTimeFrame/Once";

   @RBEntry("Execute for the given period")
   public static final String PRIVATE_CONSTANT_14 = "Desc/scheduler/GenericSchedTimeFrame/Periodic";

   @RBEntry("Specify the number of days between runs")
   public static final String PRIVATE_CONSTANT_15 = "Desc/scheduler/GenericSchedTimeFrame/DaySpin";

   @RBEntry("Specify the number of hourse between runs")
   public static final String PRIVATE_CONSTANT_16 = "Desc/scheduler/GenericSchedTimeFrame/HourSpin";

   @RBEntry("Specify the number of minutes between runs. If 0, default is 30 secs")
   public static final String PRIVATE_CONSTANT_17 = "Desc/scheduler/GenericSchedTimeFrame/MinSpin";

   @RBEntry("Uncheck to temporarily disable the schedule ")
   public static final String PRIVATE_CONSTANT_18 = "Desc/scheduler/GenericSchedTimeFrame/Enabled";

   @RBEntry("Click to save your changes and close the screen.")
   public static final String PRIVATE_CONSTANT_19 = "Desc/scheduler/GenericSchedTimeFrame/OK";

   @RBEntry("Click to apply your changes and schedule another run.")
   public static final String PRIVATE_CONSTANT_20 = "Desc/scheduler/GenericSchedTimeFrame/Apply";

   @RBEntry("Click to clear all the fields.")
   public static final String PRIVATE_CONSTANT_21 = "Desc/scheduler/GenericSchedTimeFrame/Clear";

   @RBEntry("Click to exit without saving changes")
   public static final String PRIVATE_CONSTANT_22 = "Desc/scheduler/GenericSchedTimeFrame/Cancel";

   @RBEntry("Click to view the help on specifying schedule details")
   public static final String PRIVATE_CONSTANT_23 = "Desc/scheduler/GenericSchedTimeFrame/Help";
}