/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.scheduler;

import wt.util.resource.*;

@RBUUID("wt.clients.scheduler.GenericSchedTimeFrameHelpRB")
public final class GenericSchedTimeFrameHelpRB_it extends WTListResourceBundle {
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_0 = "Contents/scheduler";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_1 = "Help/scheduler";

   @RBEntry("FileVaultRevaultScheduleRevault")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/scheduler/GenericSchedTimeFrame";

   @RBEntry("ExtStgRevaultSched")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_3 = "Help/scheduler/PDM_GenericSchedTimeFrame";

   @RBEntry("ExtStgRevaultSched")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_4 = "Help/scheduler/PJL_GenericSchedTimeFrame";

   @RBEntry("VaultReplScheduleAdministratorRef")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_5 = "Help/scheduler/ReplSchedTimeFrame";

   @RBEntry("VaultReplScheduleAdministratorRef")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_6 = "Help/scheduler/PDM_ReplSchedTimeFrame";

   @RBEntry("VaultReplScheduleAdministratorRef")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_7 = "Help/scheduler/PJL_ReplSchedTimeFrame";

   @RBEntry(" ")
   @RBComment("default description")
   public static final String PRIVATE_CONSTANT_8 = "Desc/scheduler/GenericSchedTimeFrame";

   @RBEntry("Esegui immediatamente, ignora data/ora d'inizio")
   public static final String PRIVATE_CONSTANT_9 = "Desc/scheduler/GenericSchedTimeFrame/Immediate";

   @RBEntry("Inizia l'esecuzione all'ora/nella data specificati")
   public static final String PRIVATE_CONSTANT_10 = "Desc/scheduler/GenericSchedTimeFrame/OnSchedule";

   @RBEntry("Immetti l'ora dell'esecuzione del metodo")
   public static final String PRIVATE_CONSTANT_11 = "Desc/scheduler/GenericSchedTimeFrame/TimeField";

   @RBEntry("Immetti la data dell'esecuzione del metodo")
   public static final String PRIVATE_CONSTANT_12 = "Desc/scheduler/GenericSchedTimeFrame/DateField";

   @RBEntry("Esegui il metodo programmato una volta")
   public static final String PRIVATE_CONSTANT_13 = "Desc/scheduler/GenericSchedTimeFrame/Once";

   @RBEntry("Esegui nel periodo indicato")
   public static final String PRIVATE_CONSTANT_14 = "Desc/scheduler/GenericSchedTimeFrame/Periodic";

   @RBEntry("Specifica il numero di giorni tra processi di archiviazione")
   public static final String PRIVATE_CONSTANT_15 = "Desc/scheduler/GenericSchedTimeFrame/DaySpin";

   @RBEntry("Specifica il numero di ore tra processi di archiviazione")
   public static final String PRIVATE_CONSTANT_16 = "Desc/scheduler/GenericSchedTimeFrame/HourSpin";

   @RBEntry("Specifica il numero di minuti tra processi di archiviazione. Se 0, il valore predefinito è 30 secondi")
   public static final String PRIVATE_CONSTANT_17 = "Desc/scheduler/GenericSchedTimeFrame/MinSpin";

   @RBEntry("Deselezionare per disattivare temporaneamente la programmazione ")
   public static final String PRIVATE_CONSTANT_18 = "Desc/scheduler/GenericSchedTimeFrame/Enabled";

   @RBEntry("Fare clic per salvare le modifiche e chiudere la finestra.")
   public static final String PRIVATE_CONSTANT_19 = "Desc/scheduler/GenericSchedTimeFrame/OK";

   @RBEntry("Fare clic per applicare le modifiche e programmare un altro processo di archiviazione.")
   public static final String PRIVATE_CONSTANT_20 = "Desc/scheduler/GenericSchedTimeFrame/Apply";

   @RBEntry("Fare clic per reimpostare tutti i campi.")
   public static final String PRIVATE_CONSTANT_21 = "Desc/scheduler/GenericSchedTimeFrame/Clear";

   @RBEntry("Fare clic per uscire senza salvare le modifiche.")
   public static final String PRIVATE_CONSTANT_22 = "Desc/scheduler/GenericSchedTimeFrame/Cancel";

   @RBEntry("Fare clic per visualizzare la guida sulla specificazione dei dettagli del programma")
   public static final String PRIVATE_CONSTANT_23 = "Desc/scheduler/GenericSchedTimeFrame/Help";
}
