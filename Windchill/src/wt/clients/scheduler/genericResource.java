/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.scheduler;

import wt.util.resource.*;

@RBUUID("wt.clients.scheduler.genericResource")
public final class genericResource extends WTListResourceBundle {
   /**
    * labels
    * 
    **/
   @RBEntry("External Storage")
   public static final String STORAGE_TITLE = "0";

   @RBEntry("Storage")
   public static final String STORAGE = "1";

   @RBEntry("Schedule Relocation")
   public static final String RELOC_PANEL = "2";

   /**
    * Buttons
    * 
    **/
   @RBEntry("Close")
   public static final String CLOSE_BUTT = "100";

   @RBEntry("Update")
   public static final String UPDATE_BUTT = "101";

   @RBEntry("View")
   public static final String VIEW_BUTT = "102";

   @RBEntry("Delete")
   public static final String DELETE_BUTT = "103";

   @RBEntry("Log")
   public static final String LOG_BUTT = "104";

   @RBEntry("New")
   public static final String CREATE_BUTT = "105";

   @RBEntry("Done")
   public static final String DONE_BUTT = "106";

   /**
    * ScheduleTimerWidgets
    * 
    **/
   @RBEntry("Time")
   public static final String TIME_LABEL = "107";

   @RBEntry("Immediate")
   public static final String IMMED_LAB = "108";

   @RBEntry("On Schedule")
   public static final String SCHED_LAB = "109";

   @RBEntry("Start Time")
   public static final String START_TIME = "110";

   @RBEntry("Start Date")
   public static final String START_DATE = "111";

   @RBEntry("Frequency")
   public static final String FREQ_LABEL = "112";

   @RBEntry("Once")
   public static final String ONCE_LAB = "113";

   @RBEntry("Periodic")
   public static final String PER_LAB = "114";

   @RBEntry("Every")
   public static final String EVERY_LAB = "115";

   @RBEntry("Days")
   public static final String DAY_LAB = "116";

   @RBEntry("Hours")
   public static final String HOUR_LAB = "117";

   @RBEntry("Mins")
   public static final String MIN_LAB = "118";

   @RBEntry("Enabled")
   public static final String ENABLED_LAB = "119";

   @RBEntry("OK")
   public static final String OK_BUTT = "120";

   @RBEntry("Apply")
   public static final String APPLY_BUTT = "121";

   @RBEntry("Clear")
   public static final String CLEAR_BUTT = "122";

   @RBEntry("Cancel")
   public static final String CANCEL_BUTT = "123";

   @RBEntry("Help")
   public static final String HELP_BUTT = "124";

   /**
    * Messages
    * 
    **/
   @RBEntry("Schedule object is null")
   public static final String NULL_SCHEDITEM = "200";

   @RBEntry("Illegal Screen mode.")
   public static final String ILLEGAL_MODE = "201";

   @RBEntry("Mode must be SchedInfo.IMMED_ONCE or SchedInfo.ONSTART_PERIODIC")
   public static final String ILLEGAL_TIME_MODE = "202";

   @RBEntry("Method was not defined")
   public static final String UNDEF_METHOD = "203";

   @RBEntry("Cannot update a null schedule")
   public static final String NULL_UPDATESCHED = "204";

   @RBEntry("Schedule Date should be a future date and should adhere to the format specified")
   public static final String ILLEGAL_SCHEDDATE = "205";

   @RBEntry(":")
   @RBPseudo(false)
   @RBComment("Symbols")
   public static final String COLON = "300";
}
