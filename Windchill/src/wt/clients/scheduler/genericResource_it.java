/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.scheduler;

import wt.util.resource.*;

@RBUUID("wt.clients.scheduler.genericResource")
public final class genericResource_it extends WTListResourceBundle {
   /**
    * labels
    * 
    **/
   @RBEntry("Memorizzazione esterna")
   public static final String STORAGE_TITLE = "0";

   @RBEntry("Memorizzazione")
   public static final String STORAGE = "1";

   @RBEntry("Programma riposizionamento")
   public static final String RELOC_PANEL = "2";

   /**
    * Buttons
    * 
    **/
   @RBEntry("Chiudi")
   public static final String CLOSE_BUTT = "100";

   @RBEntry("Aggiorna")
   public static final String UPDATE_BUTT = "101";

   @RBEntry("Visualizza")
   public static final String VIEW_BUTT = "102";

   @RBEntry("Elimina")
   public static final String DELETE_BUTT = "103";

   @RBEntry("Log")
   public static final String LOG_BUTT = "104";

   @RBEntry("Nuovo")
   public static final String CREATE_BUTT = "105";

   @RBEntry("Fine")
   public static final String DONE_BUTT = "106";

   /**
    * ScheduleTimerWidgets
    * 
    **/
   @RBEntry("Prossima archiviazione")
   public static final String TIME_LABEL = "107";

   @RBEntry("Immediata")
   public static final String IMMED_LAB = "108";

   @RBEntry("Momento programmato")
   public static final String SCHED_LAB = "109";

   @RBEntry("Ora d'inizio")
   public static final String START_TIME = "110";

   @RBEntry("Data d'inizio")
   public static final String START_DATE = "111";

   @RBEntry("Frequenza")
   public static final String FREQ_LABEL = "112";

   @RBEntry("Una volta")
   public static final String ONCE_LAB = "113";

   @RBEntry("Periodica")
   public static final String PER_LAB = "114";

   @RBEntry("Ogni")
   public static final String EVERY_LAB = "115";

   @RBEntry("Giorni")
   public static final String DAY_LAB = "116";

   @RBEntry("Ore")
   public static final String HOUR_LAB = "117";

   @RBEntry("Minuti")
   public static final String MIN_LAB = "118";

   @RBEntry("Attivato")
   public static final String ENABLED_LAB = "119";

   @RBEntry("OK")
   public static final String OK_BUTT = "120";

   @RBEntry("Applica")
   public static final String APPLY_BUTT = "121";

   @RBEntry("Reimposta")
   public static final String CLEAR_BUTT = "122";

   @RBEntry("Annulla")
   public static final String CANCEL_BUTT = "123";

   @RBEntry("Guida")
   public static final String HELP_BUTT = "124";

   /**
    * Messages
    * 
    **/
   @RBEntry("L'attività programmata è nulla")
   public static final String NULL_SCHEDITEM = "200";

   @RBEntry("Modalità schermo invalida.")
   public static final String ILLEGAL_MODE = "201";

   @RBEntry("La modalità deve essere SchedInfo.IMMED_ONCE o SchedInfo.ONSTART_PERIODIC")
   public static final String ILLEGAL_TIME_MODE = "202";

   @RBEntry("Non è stato definito il metodo.")
   public static final String UNDEF_METHOD = "203";

   @RBEntry("Impossibile aggiornare una programmazione nulla")
   public static final String NULL_UPDATESCHED = "204";

   @RBEntry("La data programmata deve essere una data futura e rispettare il formato specificato")
   public static final String ILLEGAL_SCHEDDATE = "205";

   @RBEntry(":")
   @RBPseudo(false)
   @RBComment("Symbols")
   public static final String COLON = "300";
}
