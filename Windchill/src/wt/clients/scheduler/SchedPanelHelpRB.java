/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.scheduler;

import wt.util.resource.*;

@RBUUID("wt.clients.scheduler.SchedPanelHelpRB")
public final class SchedPanelHelpRB extends WTListResourceBundle {
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_0 = "Contents/scheduler";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_1 = "Help/scheduler";

   @RBEntry("FileVaultRevaultScheduleExStor")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/scheduler/SchedPanel";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_3 = "Desc/scheduler/SchedPanel";

   @RBEntry("Create a new revaulting schedule")
   public static final String PRIVATE_CONSTANT_4 = "Desc/scheduler/SchedPanel/Create";

   @RBEntry("Refresh the current view")
   public static final String PRIVATE_CONSTANT_5 = "Desc/scheduler/SchedPanel/Refresh";

   @RBEntry("View details for the selected schedule")
   public static final String PRIVATE_CONSTANT_6 = "Desc/scheduler/SchedPanel/View";

   @RBEntry("Update the selected schedule")
   public static final String PRIVATE_CONSTANT_7 = "Desc/scheduler/SchedPanel/Update";

   @RBEntry("View history of runs for the selected schedule")
   public static final String PRIVATE_CONSTANT_8 = "Desc/scheduler/SchedPanel/Log";

   @RBEntry("Cancel the selected schedule without removing it")
   public static final String PRIVATE_CONSTANT_9 = "Desc/scheduler/SchedPanel/Cancel";

   @RBEntry("Delete the selected schedule")
   public static final String PRIVATE_CONSTANT_10 = "Desc/scheduler/SchedPanel/Delete";

   @RBEntry("Click a schedule to select it, double click a schedule to update it.")
   public static final String PRIVATE_CONSTANT_11 = "Desc/scheduler/SchedPanel/List";
   
   
   /**
    * Status bar messages
    *
    **/ 

   @RBEntry("Click to close the screen")
   public static final String PRIVATE_CONSTANT_14 = "Desc/scheduler/SchedPanel/Close";

   @RBEntry("Click to view the help on Scheduling Replication")
   public static final String PRIVATE_CONSTANT_15 = "Desc/scheduler/SchedPanel/Help";
}
