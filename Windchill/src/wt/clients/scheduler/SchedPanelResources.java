/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.scheduler;

import wt.util.resource.*;

@RBUUID("wt.clients.scheduler.SchedPanelResources")
@RBNameException //Grandfathered by conversion
public final class SchedPanelResources extends WTListResourceBundle {
   @RBEntry("Refresh")
   public static final String REFRESH_BUTT = "100";

   @RBEntry("Update")
   public static final String UPDATE_BUTT = "101";

   @RBEntry("View")
   public static final String VIEW_BUTT = "102";

   @RBEntry("Delete")
   public static final String DELETE_BUTT = "103";

   @RBEntry("Log")
   public static final String LOG_BUTT = "104";

   @RBEntry("New")
   public static final String CREATE_BUTT = "105";

   @RBEntry("Cancel")
   public static final String CANCEL_BUTT = "106";

   /**
    * Headers
    * 
    **/
   @RBEntry("Frequency")
   public static final String FREQ_HEADER = "200";

   @RBEntry("Next Time")
   public static final String NEXT_TIME = "201";

   /**
    * Messages
    * 
    **/
   @RBEntry("Could not find specified method.")
   public static final String METHOD_NOT_FOUND = "300";

   @RBEntry("All runs completed.")
   public static final String ALL_RUNS_DONE = "301";

   @RBEntry("Are you sure you want to delete the selected schedule?")
   public static final String SURE_DELETE = "302";

   @RBEntry("Are you sure you want to cancel the selected schedule?")
   public static final String SURE_CANCEL = "303";

   /**
    * Time Strings
    * 
    **/
   @RBEntry("weeks")
   public static final String WEEKS = "402";

   @RBEntry("week")
   public static final String WEEK = "403";

   @RBEntry("days")
   public static final String DAYS = "404";

   @RBEntry("day")
   public static final String DAY = "405";

   @RBEntry("hours")
   public static final String HOURS = "408";

   @RBEntry("hour")
   public static final String HOUR = "409";

   @RBEntry("mins")
   public static final String MINUTES = "406";

   @RBEntry("min")
   public static final String MINUTE = "407";

   @RBEntry("secs")
   public static final String SECONDS = "411";

   @RBEntry("sec")
   public static final String SECOND = "410";

   /**
    * Current status is the status of the currently executing
    * entry in the scheduling panel list.
    * 
    **/
   @RBEntry("Current run Status")
   public static final String CURR_STATUS = "500";

   @RBEntry("Last in progress")
   public static final String LAST_RUN = "501";

   @RBEntry("Last run failed")
   public static final String LAST_FAILED = "502";

   @RBEntry("One run scheduled")
   public static final String NO_FREQ = "503";

   @RBEntry("You cannot update a running scheduler process, opening details in read-only mode")
   public static final String CANT_UPDATE_RUNNING = "504";

   @RBEntry("You cannot delete a running scheduler process")
   public static final String CANT_DELETE_RUNNING = "505";

   @RBEntry("You cannot cancel a running scheduler process")
   public static final String CANT_CANCEL_RUNNING = "506";

   @RBEntry("Scheduler process already running.")
   public static final String SCHEDULER_PROCESS_RUNNING = "507";
}
