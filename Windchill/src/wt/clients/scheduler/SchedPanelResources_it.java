/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.scheduler;

import wt.util.resource.*;

@RBUUID("wt.clients.scheduler.SchedPanelResources")
@RBNameException //Grandfathered by conversion
public final class SchedPanelResources_it extends WTListResourceBundle {
   @RBEntry("Aggiorna")
   public static final String REFRESH_BUTT = "100";

   @RBEntry("Aggiorna")
   public static final String UPDATE_BUTT = "101";

   @RBEntry("Visualizza")
   public static final String VIEW_BUTT = "102";

   @RBEntry("Elimina")
   public static final String DELETE_BUTT = "103";

   @RBEntry("Log")
   public static final String LOG_BUTT = "104";

   @RBEntry("Nuovo")
   public static final String CREATE_BUTT = "105";

   @RBEntry("Annulla")
   public static final String CANCEL_BUTT = "106";

   /**
    * Headers
    * 
    **/
   @RBEntry("Frequenza")
   public static final String FREQ_HEADER = "200";

   @RBEntry("Prossima archiviazione")
   public static final String NEXT_TIME = "201";

   /**
    * Messages
    * 
    **/
   @RBEntry("Il metodo specificato non è stato trovato.")
   public static final String METHOD_NOT_FOUND = "300";

   @RBEntry("Tutti i processi di archiviazione sono stati completati.")
   public static final String ALL_RUNS_DONE = "301";

   @RBEntry("Eliminare la programmazione selezionata?")
   public static final String SURE_DELETE = "302";

   @RBEntry("Annullare la programmazione selezionata?")
   public static final String SURE_CANCEL = "303";

   /**
    * Time Strings
    * 
    **/
   @RBEntry("settimane")
   public static final String WEEKS = "402";

   @RBEntry("settimana")
   public static final String WEEK = "403";

   @RBEntry("giorni")
   public static final String DAYS = "404";

   @RBEntry("giorno")
   public static final String DAY = "405";

   @RBEntry("ore")
   public static final String HOURS = "408";

   @RBEntry("ora")
   public static final String HOUR = "409";

   @RBEntry("minuti")
   public static final String MINUTES = "406";

   @RBEntry("minuto")
   public static final String MINUTE = "407";

   @RBEntry("secondi")
   public static final String SECONDS = "411";

   @RBEntry("secondo")
   public static final String SECOND = "410";

   /**
    * Current status is the status of the currently executing
    * entry in the scheduling panel list.
    * 
    **/
   @RBEntry("Stato attuale del processo")
   public static final String CURR_STATUS = "500";

   @RBEntry("Ultimo in corso")
   public static final String LAST_RUN = "501";

   @RBEntry("Ultimo processo non riuscito")
   public static final String LAST_FAILED = "502";

   @RBEntry("Un processo programmato")
   public static final String NO_FREQ = "503";

   @RBEntry("Impossibile aggiornare un processo di programmazione in esecuzione. Apertura in modalità di sola lettura")
   public static final String CANT_UPDATE_RUNNING = "504";

   @RBEntry("Impossibile eliminare un processo di programmazione in esecuzione")
   public static final String CANT_DELETE_RUNNING = "505";

   @RBEntry("Impossibile annullare un processo di programmazione in esecuzione")
   public static final String CANT_CANCEL_RUNNING = "506";

   @RBEntry("Processo di programmazione già in esecuzione")
   public static final String SCHEDULER_PROCESS_RUNNING = "507";
}
