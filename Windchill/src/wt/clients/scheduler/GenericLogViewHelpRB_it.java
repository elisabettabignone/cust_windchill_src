/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.scheduler;

import wt.util.resource.*;

@RBUUID("wt.clients.scheduler.GenericLogViewHelpRB")
public final class GenericLogViewHelpRB_it extends WTListResourceBundle {
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_0 = "Contents/scheduler";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_1 = "Help/scheduler";

   @RBEntry("ExStoreRevaultHistRef")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/scheduler/GenericLogView";

   @RBEntry(" ")
   @RBComment("default description ")
   public static final String PRIVATE_CONSTANT_3 = "Desc/scheduler/GenericLogView";

   @RBEntry("Cronologia delle esecuzioni per questa programmazione")
   public static final String PRIVATE_CONSTANT_4 = "Desc/scheduler/GenericLogView/List";

   @RBEntry("Chiudi questa finestra")
   public static final String PRIVATE_CONSTANT_5 = "Desc/scheduler/GenericLogView/Close";

   @RBEntry("Fare clic per visualizzare la guida che descrive le informazioni su questa pagina")
   public static final String PRIVATE_CONSTANT_6 = "Desc/scheduler/GenericLogView/Help";
}
