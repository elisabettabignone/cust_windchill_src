/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.scheduler;

import wt.util.resource.*;

@RBUUID("wt.clients.scheduler.SchedPanelHelpRB")
public final class SchedPanelHelpRB_it extends WTListResourceBundle {
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_0 = "Contents/scheduler";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_1 = "Help/scheduler";

   @RBEntry("FileVaultRevaultScheduleExStor")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/scheduler/SchedPanel";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_3 = "Desc/scheduler/SchedPanel";

   @RBEntry("Crea una nuova programmazione di archiviazione temporizzata")
   public static final String PRIVATE_CONSTANT_4 = "Desc/scheduler/SchedPanel/Create";

   @RBEntry("Aggiorna la vista corrente")
   public static final String PRIVATE_CONSTANT_5 = "Desc/scheduler/SchedPanel/Refresh";

   @RBEntry("Visualizza dettagli programmazione selezionata")
   public static final String PRIVATE_CONSTANT_6 = "Desc/scheduler/SchedPanel/View";

   @RBEntry("Aggiorna la programmazione selezionata")
   public static final String PRIVATE_CONSTANT_7 = "Desc/scheduler/SchedPanel/Update";

   @RBEntry("Visualizza cronologia esecuzione per la programmazione selezionata")
   public static final String PRIVATE_CONSTANT_8 = "Desc/scheduler/SchedPanel/Log";

   @RBEntry("Annulla la programmazione selezionata senza rimuoverla")
   public static final String PRIVATE_CONSTANT_9 = "Desc/scheduler/SchedPanel/Cancel";

   @RBEntry("Elimina la programmazione selezionata")
   public static final String PRIVATE_CONSTANT_10 = "Desc/scheduler/SchedPanel/Delete";

   @RBEntry("Fare clic su una programmazione per selezionarla e doppio clic per aggiornarla.")
   public static final String PRIVATE_CONSTANT_11 = "Desc/scheduler/SchedPanel/List";
   
   
   /**
    * Status bar messages
    *
    **/ 

   @RBEntry("Fare clic per chiudere la finestra")
   public static final String PRIVATE_CONSTANT_14 = "Desc/scheduler/SchedPanel/Close";

   @RBEntry("Fare clic per visualizzare la guida sulla programmazione delle repliche")
   public static final String PRIVATE_CONSTANT_15 = "Desc/scheduler/SchedPanel/Help";
}
