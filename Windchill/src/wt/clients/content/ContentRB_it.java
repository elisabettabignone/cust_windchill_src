/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.content;

import wt.util.resource.*;

@RBUUID("wt.clients.content.ContentRB")
public final class ContentRB_it extends WTListResourceBundle {
   /**
    * The following was defined in the original resource bundle but there was
    * no string definition for it so it didn't get migrated:
    * RESOURCE_BUNDLE_ERROR      = "1";
    * PROMPTS
    **/
   @RBEntry("Selezionare i file desiderati:")
   @RBComment("Labels used as prompts on the Select, Download and Upload dialog boxes.")
   public static final String PRIVATE_CONSTANT_0 = "selectFilesLabel";

   @RBEntry("Selezionare tutti i file da scaricare adesso:")
   @RBComment("Labels used as prompts on the Select, Download and Upload dialog boxes.")
   public static final String PRIVATE_CONSTANT_1 = "downloadFilesLabel";

   @RBEntry("Selezionare tutti i file da caricare adesso:")
   @RBComment("Labels used as prompts on the Select, Download and Upload dialog boxes.")
   public static final String PRIVATE_CONSTANT_2 = "uploadFilesLabel";

   /**
    * MULTILIST LABELS
    **/
   @RBEntry("Nome file")
   @RBComment("Labels used for the column headings in the multilists which display the content files.")
   public static final String PRIVATE_CONSTANT_3 = "fileNameLabel";

   @RBEntry("Dimensione")
   @RBComment("Labels used for the column headings in the multilists which display the content files.")
   public static final String PRIVATE_CONSTANT_4 = "fileSizeLabel";

   @RBEntry("Formato")
   @RBComment("Labels used for the column headings in the multilists which display the content files.")
   public static final String PRIVATE_CONSTANT_5 = "fileFormatLabel";

   /**
    * BUTTON LABELS
    **/
   @RBEntry("OK")
   @RBComment("Label used on command button.")
   public static final String PRIVATE_CONSTANT_6 = "okButton";

   @RBEntry("Annulla")
   @RBComment("Label used on command button.")
   public static final String PRIVATE_CONSTANT_7 = "cancelButton";

   /**
    * DIALOG TITLES
    * Titles displayed on the dialogs.  The parameter {0} passed in is
    * the identity of the ContentHolder object.  For example, if the
    * dialog displayed is allowing the user to download files from the
    * document "UI Design Spec", the title would be
    * "Download Files from UI Design Spec"
    **/
   @RBEntry("Scarica file")
   @RBComment("Titles displayed on the dialogs.")
   public static final String PRIVATE_CONSTANT_8 = "downloadFilesTitle";

   @RBEntry("Scarica file da {0}")
   @RBArgComment0("identity of the ContentHolder object")
   public static final String PRIVATE_CONSTANT_9 = "downloadFileContents";

   @RBEntry("Seleziona file")
   @RBComment("Titles displayed on the dialogs.")
   public static final String PRIVATE_CONSTANT_10 = "selectFilesTitle";

   @RBEntry("Seleziona file da {0}")
   @RBArgComment0("identity of the ContentHolder object")
   public static final String PRIVATE_CONSTANT_11 = "selectFileContents";

   @RBEntry("Carica file")
   @RBComment("Titles displayed on the dialogs.")
   public static final String PRIVATE_CONSTANT_12 = "uploadFilesTitle";

   @RBEntry("Carica file in {0}")
   @RBArgComment0("identity of the ContentHolder object")
   public static final String PRIVATE_CONSTANT_13 = "uploadFileContents";

   /**
    * UNITS OF MEASURE
    **/
   @RBEntry("KB")
   @RBComment("Unit of measure.")
   public static final String KILOBYTES = "100";

   /**
    * ERROR MESSAGES
    **/
   @RBEntry("\"{0}\" non è un valore valido per la proprietà \"{1}\": {2}.")
   @RBComment("The message displayed when the attempt to set some property on an object fails.  Example \"value\" is not a valid value for property \"Name\":  PropertyVetoException.")
   @RBArgComment0("the value of the property which caused the exception to be thrown")
   @RBArgComment1("the name of the property to be set")
   @RBArgComment2("the message of the exception thrown")
   public static final String PROPERTY_VETO_EXCEPTION = "2";

   @RBEntry("Il seguente errore si è verificato durante il caricamento dei file in {0}: {1}")
   @RBComment("The message displayed when an exception is thrown while trying to upload a file associated with a ContentHolder object.")
   @RBArgComment0("the identity of the ContentHolder")
   @RBArgComment1("the message of the exception thrown")
   public static final String UPLOAD_FAILED = "3";

   @RBEntry("Il seguente errore si è verificato durante lo scaricamento dei file da {0}: {1}")
   @RBComment("The message displayed when an exception is thrown while trying to download a file associated with a ContentHolder object.")
   @RBArgComment0("the identity of the ContentHolder")
   @RBArgComment1("the message of the exception thrown")
   public static final String DOWNLOAD_FAILED = "4";

   @RBEntry("Impossibile caricare file in {0} con lo stato {1}.")
   @RBComment("The message used in an exception to indicate that the status of an upload was not COMPLETE nor CANCEL.")
   @RBArgComment0("the status of the upload")
   @RBArgComment1("the identity of the ContentHolder")
   public static final String FAILED_UPLOAD_STATUS = "5";

   @RBEntry("Chiedi sempre se aprire o salvare")
   public static final String PRIVATE_CONSTANT_14 = "alwaysAsk";

   @RBEntry("Apri sempre i file in un'applicazione")
   public static final String PRIVATE_CONSTANT_15 = "alwaysOpen";

   @RBEntry("Salva sempre il file nella directory")
   public static final String PRIVATE_CONSTANT_16 = "alwaysSave";

   @RBEntry("Impossibile trovare il file principale {0} da caricare. Usare il file principale esistente e continuare?")
   @RBArgComment0("primary file filepath as submitted from checkin/update form")
   public static final String PRIVATE_CONSTANT_17 = "askContinueIfNotFound";

   @RBEntry("Il file principale {0} non è stato modificato. Continuare il Check-In?")
   @RBArgComment0("primary file filepath as submitted from checkin/update form")
   public static final String PRIVATE_CONSTANT_18 = "askContinueIfUnchangedCheckin";

   @RBEntry("Il file principale {0} non è stato modificato. Continuare l'aggiornamento?")
   @RBArgComment0("primary file filepath as submitted from checkin/update form")
   public static final String PRIVATE_CONSTANT_19 = "askContinueIfUnchangedUpdate";

   @RBEntry("Continuare il Check-In senza caricare il file principale?")
   public static final String PRIVATE_CONSTANT_20 = "askContinueOrCancelCheckin";

   @RBEntry("Continuare l'aggiornamento senza il caricamento del file principale?")
   public static final String PRIVATE_CONSTANT_21 = "askContinueOrCancelUpdate";

   @RBEntry("Il file principale {0} è stato modificato. Caricare il file modificato con il Check-In?")
   @RBArgComment0("primary file filepath as submitted from checkin/update form")
   public static final String PRIVATE_CONSTANT_22 = "askUploadIfChangedCheckin";

   @RBEntry("Il file principale {0} è stato modificato. Caricare il file modificato con l'aggiornamento?")
   @RBArgComment0("primary file filepath as submitted from checkin/update form")
   public static final String PRIVATE_CONSTANT_23 = "askUploadIfChangedUpdate";

   @RBEntry("Impossibile trovare il file principale {0} da caricare. Usare il file {1} del workspace e continuare?")
   @RBArgComment0("primary file filepath as submitted from checkin/update form")
   @RBArgComment1("workspace directory path with primary file filename")
   public static final String PRIVATE_CONSTANT_24 = "askUseWorkspaceUpload";

   @RBEntry("Le preferenze controllano il caricamento e lo scaricamento dei file di contenuto principale del documento.")
   public static final String PRIVATE_CONSTANT_25 = "contentUploadDownloadDescription";

   @RBEntry("Contenuto")
   public static final String PRIVATE_CONSTANT_26 = "contentUploadDownloadDisplayName";

   @RBEntry("Continua senza caricare se il file non viene trovato")
   public static final String PRIVATE_CONSTANT_27 = "fileNotFoundBehaviorContinueLabel";

   @RBEntry("Conserva la possibilità di continuare o riprendere la ricerca se il file non viene trovato")
   public static final String PRIVATE_CONSTANT_28 = "fileNotFoundBehaviorPromptLabel";

   @RBEntry("Se il file di contenuto principale selezionato per operazioni di <B>Aggiorna</B> o <B>Check-In </B> di documenti non viene trovato nella directory specificata sul computer, è possibile mantenere l'eventuale file di contenuto principale esistente e continuare l'operazione senza caricare il file, oppure scegliere se continuare senza caricare o tornare indietro e ricercare un file valido. Il default di sistema è di offrire l'opzione.")
   public static final String PRIVATE_CONSTANT_29 = "fileNotFoundBehaviorDescription";

   @RBEntry("Comportamento in caso di file non trovato")
   public static final String PRIVATE_CONSTANT_30 = "fileNotFoundBehaviorDisplayName";

   @RBEntry("Scegliere il comportamento se il file locale non viene trovato")
   public static final String PRIVATE_CONSTANT_31 = "fileNotFoundBehaviorShortDescription";

   @RBEntry("Continua senza caricare il file che non è stato modificato")
   public static final String PRIVATE_CONSTANT_32 = "fileUnchangedBehaviorContinueLabel";

   @RBEntry("Conserva la possibilità di scegliere se continuare o riprendere la ricerca se il file non è stato modificato")
   public static final String PRIVATE_CONSTANT_33 = "fileUnchangedBehaviorPromptLabel";

   @RBEntry("Durante le operazioni <B>Aggiorna</B> o <B>Check-In</B> di documenti, il file locale sul computer viene confrontato con il file di contenuto principale memorizzato in Windchill. Se i file sono identici, è possibile continuare l'operazione senza caricare il file non modificato, oppure scegliere se continuare senza caricare o tornare indietro e ricercare un file valido. Il default di sistema è di continuare senza caricamento.")
   public static final String PRIVATE_CONSTANT_34 = "fileUnchangedBehaviorDescription";

   @RBEntry("Comportamento in caso di file non modificato")
   public static final String PRIVATE_CONSTANT_35 = "fileUnchangedBehaviorDisplayName";

   @RBEntry("Scegliere il comportamento se il file locale non è stato modificato.")
   public static final String PRIVATE_CONSTANT_36 = "fileUnchangedBehaviorShortDescription";

   @RBEntry("Scarica il file di contenuto principale al Check-Out del documento")
   public static final String PRIVATE_CONSTANT_37 = "downloadOnCheckoutAllowLabel";

   @RBEntry("Non scaricare il file principale al Check-Out del documento")
   public static final String PRIVATE_CONSTANT_38 = "downloadOnCheckoutSkipLabel";

   @RBEntry("Scegliere se scaricare il file di contenuto principale quando si effettua il Check-Out di un documento. Se il file non viene scaricato durante il Check-Out, può essere scaricato separatamente tramite il comando <B>Preleva contenuto</B>. Per default il sistema scarica il file di contenuto principale quando il documento è sottoposto a Check-Out.")
   public static final String PRIVATE_CONSTANT_39 = "downloadOnCheckoutDescription";

   @RBEntry("Scaricamento del contenuto al Check-Out del documento")
   public static final String PRIVATE_CONSTANT_40 = "downloadOnCheckoutDisplayName";

   @RBEntry("Scegli se scaricare il file di contenuto principale durante il Check-Out del documento.")
   public static final String PRIVATE_CONSTANT_41 = "downloadOnCheckoutShortDescription";

   @RBEntry("Quando si scarica il file di contenuto principale di un documento, scegliere se aprire il file automaticamente con un'applicazione o se salvarlo su una directory selezionata sul computer. Per default, il sistema richiede all'utente se aprire o salvare un file di contenuto principale dopo averlo scaricato. <BR><BR><B>Nota:<B> questa preferenza è disponibile solo quando si scaricano i file tramite Windchill Desktop Integration o gli applet Java (è necessario disporre dei plug-in Java o Windchill DTI).<BR>")
   @RBComment("Please preserve the HTML formatting tags to help emphasize the exclusions at the end.")
   public static final String PRIVATE_CONSTANT_42 = "downloadOpTypeDescription";

   @RBEntry("Tipo di scaricamento")
   public static final String PRIVATE_CONSTANT_43 = "downloadOpTypeDisplayName";

   @RBEntry("Scegliere se aprire il file principale o se salvarlo su disco")
   public static final String PRIVATE_CONSTANT_44 = "downloadOpTypeShortDescription";

   @RBEntry("Caricamento/scaricamento file")
   public static final String PRIVATE_CONSTANT_45 = "fileUploadDownload";

   @RBEntry("Carica automaticamente il file modificato")
   public static final String PRIVATE_CONSTANT_46 = "fileChangedBehaviorUploadLabel";

   @RBEntry("Mantieni l'opzione di caricare o non caricare")
   public static final String PRIVATE_CONSTANT_47 = "fileChangedBehaviorPromptLabel";

   @RBEntry("Quando si esegue un'operazione <B>Aggiorna </B> o <B>Check-In </B>, il file locale sul computer viene confrontato con il file di contenuto principale memorizzato in Windchill. Se i file sono diversi, è possibile caricare automaticamente il file modificato o scegliere di volta in volta se effettuare o meno il caricamento. Il default di sistema è il caricamento automatico.")
   public static final String PRIVATE_CONSTANT_48 = "fileChangedBehaviorDescription";

   @RBEntry("Comportamento in caso di file modificato")
   public static final String PRIVATE_CONSTANT_49 = "fileChangedBehaviorDisplayName";

   @RBEntry("Scegliere il comportamento se il file locale è stato modificato")
   public static final String PRIVATE_CONSTANT_50 = "fileChangedBehaviorShortDescription";

   @RBEntry("Directory di default attuale:")
   public static final String PRIVATE_CONSTANT_51 = "workspacePathCurrent";

   @RBEntry("Nuova directory di default:")
   public static final String PRIVATE_CONSTANT_52 = "workspacePathNew";

   @RBEntry("Scegliere una directory sul proprio computer da impostare come posizione di default per il caricamento e lo scaricamento dei file di contenuto principale dei documenti Windchill. Questa preferenza è disponibile solo quando si caricano e scaricano i file tramite gli applet Java o Desktop Integration. Se non si sceglie una directory di default o se si accede da un computer diverso dove la directory non esiste, il default verrà determinato dal sistema operativo.<BR><BR><B>Nota: </B>impostare la directory di default selezionando un file della directory che si intende usare. La directory deve esistere sul computer dell'utente e contenere almeno un file. Se si preferisce invece specificare manualmente il percorso della directory, inserire sempre un separatore dopo l'ultima directory del percorso, ad es. \"C:\\Cartella\\\" anziché \"C:\\Cartella\".<BR>")
   @RBComment("Please preserve the HTML formatting tags to help emphasize the instructions at the end, please preserve the double-backslashes which will display as a single backslash.  It would be fine to change \"MyFolder\" to some localized example folder name instead.")
   public static final String PRIVATE_CONSTANT_53 = "workspacePathDescription";

   @RBEntry("Directory locale di default")
   public static final String PRIVATE_CONSTANT_54 = "workspacePathDisplayName";

   @RBEntry("Scegliere una directory di default per il caricamento e lo scaricamento dei file principali")
   public static final String PRIVATE_CONSTANT_55 = "workspacePathShortDescription";

   @RBEntry("Impossibile leggere il file {0} per il caricamento. Operazione annullata.")
   public static final String PRIVATE_CONSTANT_56 = "fileNotFoundCancel";

   @RBEntry("Impossibile leggere il file {0} per il caricamento. È stato mantenuto il file principale esistente.")
   public static final String PRIVATE_CONSTANT_57 = "fileNotFoundNoUpload";

   @RBEntry("Impossibile leggere il file {0} per il caricamento. Continuare senza caricare?")
   public static final String PRIVATE_CONSTANT_58 = "fileNotFoundPrompt";

   @RBEntry("Impossibile trovare il file {0}, usare il file  {1} del workspace?")
   public static final String PRIVATE_CONSTANT_59 = "fileNotFoundWorkspacePrompt";

   @RBEntry("File {0} non modificato rispetto al file principale esistente.  Operazione annullata.")
   public static final String PRIVATE_CONSTANT_60 = "fileUnchangedCancel";

   @RBEntry("File {0} non modificato rispetto al file principale esistente. Il file principale esistente è stato mantenuto.")
   public static final String PRIVATE_CONSTANT_61 = "fileUnchangedNoUpload";

   @RBEntry("File {0} non modificato rispetto al file principale esistente. Continuare senza il caricamento?")
   public static final String PRIVATE_CONSTANT_62 = "fileUnchangedPrompt";

   @RBEntry("Il file {0} non è stato caricato. È stato mantenuto il file principale esistente.")
   public static final String PRIVATE_CONSTANT_63 = "fileChangedNoUpload";

   @RBEntry("Il file {0} è stato modificato. Caricare il file modificato?")
   public static final String PRIVATE_CONSTANT_64 = "fileChangedPrompt";

   @RBEntry("Trascinamento selezione su schermate di visualizzazione")
   public static final String PRIVATE_CONSTANT_65 = "dndMicroAppletEnabledDisplayName";

   @RBEntry("Scegliere se consentire il trascinamento delle selezioni sulle schermate di visualizzazione.")
   public static final String PRIVATE_CONSTANT_66 = "dndMicroAppletEnabledShortDescription";

   @RBEntry("Determina l'attivazione della funzionalità applet di trascinamento selezione nelle schermate di visualizzazione HTML come i risultati della ricerca, il contenuto di cartelle e schedari e le pagine delle proprietà. Se la funzionalità è attivata, gli applet leggeri sensibili al trascinamento delle selezioni sono usati su queste pagine per rappresentare graficamente gli oggetti Windchill. Se la funzionalità è disattivata vengono invece usati tag di immagini HTML. Per default, la funzionalità è disattivata.")
   public static final String PRIVATE_CONSTANT_67 = "dndMicroAppletEnabledDescription";

   @RBEntry("Attiva trascinamento selezione.")
   public static final String PRIVATE_CONSTANT_68 = "dndMicroAppletEnabledLabel";

   @RBEntry("Disattiva trascinamento selezione.")
   public static final String PRIVATE_CONSTANT_69 = "dndMicroAppletDisabledLabel";

   @RBEntry("Trascinamento selezione su pagine modulo")
   public static final String PRIVATE_CONSTANT_70 = "fileDndAppletEnabledDisplayName";

   @RBEntry("Scegliere se consentire il trascinamento selezioni su pagine modulo.")
   public static final String PRIVATE_CONSTANT_71 = "fileDndAppletEnabledShortDescription";

   @RBEntry("Scegliere se attivare la funzionalità di trascinamento selezione su pagine modulo come  <B>Crea, Aggiorna,</B> e <B>Check-In</B> documento. Per default, la funzionalità è disattivata.")
   public static final String PRIVATE_CONSTANT_72 = "fileDndAppletEnabledDescription";

   @RBEntry("Attiva trascinamento selezione.")
   public static final String PRIVATE_CONSTANT_73 = "fileDndAppletEnabledLabel";

   @RBEntry("Disattiva trascinamento selezione.")
   public static final String PRIVATE_CONSTANT_74 = "fileDndAppletDisabledLabel";

   @RBEntry("Avvio di aggiornamento documento con trascinamento")
   public static final String PRIVATE_CONSTANT_75 = "dndDocUpdateEnabledDisplayName";

   @RBEntry("Scegliere se permettere l'aggiornamento del documento tramite trascinamento di un file.")
   public static final String PRIVATE_CONSTANT_76 = "dndDocUpdateEnabledShortDescription";

   @RBEntry("Determina la possibilità di avviare l'aggiornamento dei documenti trascinando un file in un applet di documento sulle pagine di visualizzazione HTML. L'impostazione viene ignorata se la funzione Trascinamento selezione su schermate di visualizzazione non è attivata. Se attivata, la funzionalità si applica solo ai documenti aggiornabili e quindi non a quelli sottoposti a Check-In in uno schedario condiviso. Per default, la funzionalità è attivata quando la funzionalità Trascinamento selezione su schermate di visualizzazione è attivata.")
   public static final String PRIVATE_CONSTANT_77 = "dndDocUpdateEnabledDescription";

   @RBEntry("Attiva trascinamento selezione.")
   public static final String PRIVATE_CONSTANT_78 = "dndDocUpdateEnabledLabel";

   @RBEntry("Disattiva trascinamento selezione.")
   public static final String PRIVATE_CONSTANT_79 = "dndDocUpdateDisabledLabel";

   @RBEntry("Esplorazione file per le pagine modulo dei documenti")
   public static final String PRIVATE_CONSTANT_80 = "fileBrowseAppletEnabledDisplayName";

   @RBEntry("Scegliere l'applet di esplorazione file o l'input file HTML per tutti i documenti.")
   public static final String PRIVATE_CONSTANT_81 = "fileBrowseAppletEnabledShortDescription";

   @RBEntry("Scegliere l'applet di esplorazione file o l'input di file HTML per le pagine modulo dei documenti come <B>Crea, Aggiorna,</B> e <B>Check-In</B>. Le pagine vengono caricate più velocemente se si sceglie l'input di file HTML ma alcune funzionalità sono disponibili solo nell'applet di esplorazione file, come l'apertura automatica dei file di tipo \"*.* Tutti i file\" nella directory specificata nelle preferenze del workspace. Per default, il sistema utilizza l'applet di esplorazione file.")
   public static final String PRIVATE_CONSTANT_82 = "fileBrowseAppletEnabledDescription";

   @RBEntry("Applet esplorazione file")
   public static final String PRIVATE_CONSTANT_83 = "fileBrowseAppletEnabledLabel";

   @RBEntry("Input file HTML")
   public static final String PRIVATE_CONSTANT_84 = "fileBrowseAppletDisabledLabel";

   @RBEntry("Esplorazione di file principali sulle pagine modulo dei documenti")
   public static final String PRIVATE_CONSTANT_85 = "primaryFileBrowseAppletEnabledDisplayName";

   @RBEntry("Scegliere l'applet di esplorazione file o l'input file HTML per i documenti con dati principali.")
   public static final String PRIVATE_CONSTANT_86 = "primaryFileBrowseAppletEnabledShortDescription";

   @RBEntry("Scegliere l'applet di esplorazione file o l'input di file HTML per le pagine modulo dei documenti come <B>Crea, Aggiorna,</B> e <B>Check-In</B>. Le pagine vengono caricate più velocemente se si sceglie l'input di file HTML ma alcune funzionalità sono disponibili solo nell'applet di esplorazione file, come l'apertura automatica dei file di tipo \"*.* Tutti i file\" nella directory specificata nelle preferenze del workspace. Per default, il sistema utilizza l'applet di esplorazione file.")
   public static final String PRIVATE_CONSTANT_87 = "primaryFileBrowseAppletEnabledDescription";

   @RBEntry("Applet esplorazione file")
   public static final String PRIVATE_CONSTANT_88 = "primaryFileBrowseAppletEnabledLabel";

   @RBEntry("Input file HTML")
   public static final String PRIVATE_CONSTANT_89 = "primaryFileBrowseAppletDisabledLabel";

   @RBEntry("Esplorazione file allegati per le pagine dei documenti")
   public static final String PRIVATE_CONSTANT_90 = "secondaryFileBrowseAppletEnabledDisplayName";

   @RBEntry("Scegliere l'applet di esplorazione file o l'input file HTML per tutti i documenti con allegati.")
   public static final String PRIVATE_CONSTANT_91 = "secondaryFileBrowseAppletEnabledShortDescription";

   @RBEntry("Scegliere l'applet di esplorazione file o l'input di file HTML per le pagine modulo degli allegati di contenuto principale come <B>Crea</B> e <B>Aggiorna</B>. Le pagine vengono caricate più velocemente se si sceglie l'input di file HTML ma alcune funzionalità sono disponibili solo nell'applet di esplorazione file, come l'apertura automatica dei file di tipo \"*.* Tutti i file\" nella directory specificata nelle preferenze del workspace. Per default, il sistema utilizza l'applet di esplorazione file.")
   public static final String PRIVATE_CONSTANT_92 = "secondaryFileBrowseAppletEnabledDescription";

   @RBEntry("Applet esplorazione file")
   public static final String PRIVATE_CONSTANT_93 = "secondaryFileBrowseAppletEnabledLabel";

   @RBEntry("Input file HTML")
   public static final String PRIVATE_CONSTANT_94 = "secondaryFileBrowseAppletDisabledLabel";

   @RBEntry("File server preferito")
   public static final String PRIVATE_CONSTANT_95 = "contentCacheSiteBehaviorDisplayName";

   @RBEntry("Selezionare il file server preferito per il caricamento e lo scaricamento.")
   public static final String PRIVATE_CONSTANT_96 = "contentCacheSiteBehaviorShortDescription";

   @RBEntry("Quando il contenuto di un oggetto Windchill viene scaricato o caricato, la scelta del sito file server appropriato migliora le prestazioni. Il sito master Windchill è il sito di default.")
   public static final String PRIVATE_CONSTANT_97 = "contentCacheSiteBehaviorDescription";

   @RBEntry("Sito master Windchill:")
   public static final String PRIVATE_CONSTANT_98 = "contentCacheSiteMasterLabel";

   @RBEntry("Sito preferito per cache di dati:")
   public static final String PRIVATE_CONSTANT_99 = "contentCacheSiteReplicaLabel";

   @RBEntry("Opzione di scaricamento file con applet Java")
   public static final String PRIVATE_CONSTANT_100 = "JPIDetectionForDownloadDisplayName";

   @RBEntry("Specificare se usare l'applet Java per scaricare i file da sottoporre a editing.")
   public static final String PRIVATE_CONSTANT_101 = "JPIDetectionForDownloadShortDescription";

   @RBEntry("Specificare se utilizzare l'applet Java per scaricare i file principali utilizzando le azioni sui documenti Windchill come <B>Check-Out</B> o <B>Preleva contenuto</B>. (Il plug-in Java deve essere installato in locale). Lo scaricamento con l'applet Java offre le seguenti funzionalità avanzate: <UL><LI>Non è necessario selezionare il percorso del file per le azioni <B>Aggiorna</B> o <B>Check-In</B>.<LI>I file non modificati non verranno ricaricati.<LI>È possibile scaricare i file principali da tutti i documenti di una struttura con una sola azione.</UL>Per default, il sistema utilizza l'applet Java per lo scaricamento dei file principali durante l'esecuzione del <B>Check-Out</B> o di <B>Preleva contenuto</B> (se il plug-in Java viene rilevato sulla macchina locale).")
   public static final String PRIVATE_CONSTANT_102 = "JPIDetectionForDownloadDescription";

   @RBEntry("Specificare se utilizzare l'applet Java per scaricare i file principali utilizzando le azioni sui documenti Windchill come <B>Check-Out</B> o <B>Visualizza</B>. (Il plug-in Java deve essere installato in locale). Lo scaricamento con l'applet Java offre le seguenti funzionalità avanzate:<UL><LI>Non è necessario selezionare il percorso del file per le azioni <B>Aggiorna</B> o <B>Check-In</B>.<LI>I file non nidificati non verranno ricaricati.</UL>Per default, il sistema utilizza l'applet Java per lo scaricamento dei file principali durante l'esecuzione del <B>Check-Out</B> o di <B>Visualizza</B> (se il plug-in Java viene rilevato sulla macchina locale).")
   public static final String PRIVATE_CONSTANT_103 = "JPIDetectionForDownloadDescriptionPDMLink";

   @RBEntry("Usa sempre l'applet Java per scaricare il file principale dei documenti Windchill (se viene rilevato il plug-in Java).")
   public static final String PRIVATE_CONSTANT_104 = "JPIDetectionForDownloadTrueLabel";

   @RBEntry("Non utilizzare mai l'applet Java per lo scaricamento del file principale dei documenti Windchill, anche se viene rilevata la presenza del plug-in Java. Usa le funzioni di base del browser per scaricare il file principale dei documenti Windchill.")
   public static final String PRIVATE_CONSTANT_105 = "JPIDetectionForDownloadFalseLabel";

   @RBEntry("Comportamento di rilevamento automatico del plug-in dell'applet Java")
   public static final String PRIVATE_CONSTANT_106 = "JPIDetectionFailoverPromptDisplayName";

   @RBEntry("Specificare se il rilevamento automatico di un plug-in Java mancante deve determinare un messaggio di prompt per l'installazione del plug-in.")
   public static final String PRIVATE_CONSTANT_107 = "JPIDetectionFailoverPromptShortDescription";

   @RBEntry("Specificare il comportamento se è impostata la preferenza <B>Opzione di scaricamento file con applet Java</B> in modo da utilizzare sempre l'applet Java per lo scaricamento, ma il plug-in corretto non è stato rilevato sulla macchina dell'utente. Per default, il sistema richiede all'utente di installare il plug-in Java.")
   @RBComment("need to keep preference name in this description consistent with JPIDetectionForDownloadDisplayName value")
   public static final String PRIVATE_CONSTANT_108 = "JPIDetectionFailoverPromptDescription";

   @RBEntry("Sollecita l'installazione del plug-in Java (se non rilevato sulla macchina dell'utente).")
   public static final String PRIVATE_CONSTANT_109 = "JPIDetectionFailoverPromptTrueLabel";

   @RBEntry("Utilizza automaticamente la gestione di base del browser per scaricare i file.")
   public static final String PRIVATE_CONSTANT_110 = "JPIDetectionFailoverPromptFalseLabel";

   @RBEntry("Comprimi dati")
   public static final String PRIVATE_CONSTANT_111 = "compressContentsAllowLabel";

   @RBEntry("Non comprimere i dati")
   public static final String PRIVATE_CONSTANT_112 = "compressContentsSkipLabel";

   @RBEntry("Specificare se si intende ricevere dati compressi su HTTP. Se si seleziona 'Comprimi dati' ed il browser che l'utente usa per accedere a Windchill è in grado di gestire dati compressi, l'utente riceverà i dati in forma compressa. La decompressione viene gestita dal browser durante l'esecuzione. Questa opzione viene consigliata negli ambienti con larghezza di banda ridotta al fine di ridurre il traffico di rete e migliorare le prestazioni di scaricamento. <P>Se l'utente incontra dei problemi di visualizzazione dopo aver optato per la compressione dei dati, si consiglia di scegliere l'opzione 'Non comprimere i dati'. <P>Questa preferenza imposta un cookie persistente e l'eliminazione dei cookie del brower determinerà la perdita dell'impostazione. Per default, il sistema non comprime i dati.<P>Importante : nell'ambiente Pro/E Wildfire, se l'opzione di compressione di Pro/E è attivata (vedere la documentazione Pro/E Wildfire per informazioni dettagliate), l'utente riceverà dati compressi indipendentemente dall'impostazione di questa preferenza. <P><I>Nota: questa impostazione è efficace se l'amministratore del sito ha attivato il filtro di compressione (vedere la Guida dell'amministratore per informazioni dettagliate).</I>")
   public static final String PRIVATE_CONSTANT_113 = "compressContentsDescription";

   @RBEntry("Comprimi dati")
   public static final String PRIVATE_CONSTANT_114 = "compressContentsDisplayName";

   @RBEntry("Specificare se si intende ricevere dati compressi via HTTP.")
   public static final String PRIVATE_CONSTANT_115 = "compressContentsShortDescription";
   
   @RBEntry("Archivio master preferito")
   public static final String PRIVATE_CONSTANT_116 = "contentMasterVaultBehaviorDisplayName";

   @RBEntry("Selezionare l'archivio master preferito per il caricamento.")
   public static final String PRIVATE_CONSTANT_117 = "contentMasterVaultBehaviorShortDescription";

   @RBEntry("Durante la migrazione di dati Windchill, quando il contenuto di un oggetto <I>Windchill</I> viene <I>caricato</I>, la scelta dell'<B>Archivio master</B> appropriato è in genere utile per evitare la riarchiviazione e migliorare le prestazioni. L'archivio di default non è impostato.")
   public static final String PRIVATE_CONSTANT_118 = "contentMasterVaultBehaviorDescription";
}
