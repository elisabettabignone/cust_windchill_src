/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.content;

import wt.util.resource.*;

@RBUUID("wt.clients.content.ContentRB")
public final class ContentRB extends WTListResourceBundle {
   /**
    * The following was defined in the original resource bundle but there was
    * no string definition for it so it didn't get migrated:
    * RESOURCE_BUNDLE_ERROR      = "1";
    * PROMPTS
    **/
   @RBEntry("Please select the desired files:")
   @RBComment("Labels used as prompts on the Select, Download and Upload dialog boxes.")
   public static final String PRIVATE_CONSTANT_0 = "selectFilesLabel";

   @RBEntry("Please select any files to download now:")
   @RBComment("Labels used as prompts on the Select, Download and Upload dialog boxes.")
   public static final String PRIVATE_CONSTANT_1 = "downloadFilesLabel";

   @RBEntry("Please select any files to upload now:")
   @RBComment("Labels used as prompts on the Select, Download and Upload dialog boxes.")
   public static final String PRIVATE_CONSTANT_2 = "uploadFilesLabel";

   /**
    * MULTILIST LABELS
    **/
   @RBEntry("File Name")
   @RBComment("Labels used for the column headings in the multilists which display the content files.")
   public static final String PRIVATE_CONSTANT_3 = "fileNameLabel";

   @RBEntry("Size")
   @RBComment("Labels used for the column headings in the multilists which display the content files.")
   public static final String PRIVATE_CONSTANT_4 = "fileSizeLabel";

   @RBEntry("Format")
   @RBComment("Labels used for the column headings in the multilists which display the content files.")
   public static final String PRIVATE_CONSTANT_5 = "fileFormatLabel";

   /**
    * BUTTON LABELS
    **/
   @RBEntry("OK")
   @RBComment("Label used on command button.")
   public static final String PRIVATE_CONSTANT_6 = "okButton";

   @RBEntry("Cancel")
   @RBComment("Label used on command button.")
   public static final String PRIVATE_CONSTANT_7 = "cancelButton";

   /**
    * DIALOG TITLES
    * Titles displayed on the dialogs.  The parameter {0} passed in is
    * the identity of the ContentHolder object.  For example, if the
    * dialog displayed is allowing the user to download files from the
    * document "UI Design Spec", the title would be
    * "Download Files from UI Design Spec"
    **/
   @RBEntry("Download Files")
   @RBComment("Titles displayed on the dialogs.")
   public static final String PRIVATE_CONSTANT_8 = "downloadFilesTitle";

   @RBEntry("Download Files from {0}")
   @RBArgComment0("identity of the ContentHolder object")
   public static final String PRIVATE_CONSTANT_9 = "downloadFileContents";

   @RBEntry("Select Files")
   @RBComment("Titles displayed on the dialogs.")
   public static final String PRIVATE_CONSTANT_10 = "selectFilesTitle";

   @RBEntry("Select Files from {0}")
   @RBArgComment0("identity of the ContentHolder object")
   public static final String PRIVATE_CONSTANT_11 = "selectFileContents";

   @RBEntry("Upload Files")
   @RBComment("Titles displayed on the dialogs.")
   public static final String PRIVATE_CONSTANT_12 = "uploadFilesTitle";

   @RBEntry("Upload Files to {0}")
   @RBArgComment0("identity of the ContentHolder object")
   public static final String PRIVATE_CONSTANT_13 = "uploadFileContents";

   /**
    * UNITS OF MEASURE
    **/
   @RBEntry("KB")
   @RBComment("Unit of measure.")
   public static final String KILOBYTES = "100";

   /**
    * ERROR MESSAGES
    **/
   @RBEntry("\"{0}\" is not a valid value for property \"{1}\": {2}.")
   @RBComment("The message displayed when the attempt to set some property on an object fails.  Example \"value\" is not a valid value for property \"Name\":  PropertyVetoException.")
   @RBArgComment0("the value of the property which caused the exception to be thrown")
   @RBArgComment1("the name of the property to be set")
   @RBArgComment2("the message of the exception thrown")
   public static final String PROPERTY_VETO_EXCEPTION = "2";

   @RBEntry("The following error occurred while attempting to upload files to {0}: {1}")
   @RBComment("The message displayed when an exception is thrown while trying to upload a file associated with a ContentHolder object.")
   @RBArgComment0("the identity of the ContentHolder")
   @RBArgComment1("the message of the exception thrown")
   public static final String UPLOAD_FAILED = "3";

   @RBEntry("The following error occurred while attempting to download files from {0}: {1}")
   @RBComment("The message displayed when an exception is thrown while trying to download a file associated with a ContentHolder object.")
   @RBArgComment0("the identity of the ContentHolder")
   @RBArgComment1("the message of the exception thrown")
   public static final String DOWNLOAD_FAILED = "4";

   @RBEntry("Attempt to upload files to {0} failed with status {1}.")
   @RBComment("The message used in an exception to indicate that the status of an upload was not COMPLETE nor CANCEL.")
   @RBArgComment0("the status of the upload")
   @RBArgComment1("the identity of the ContentHolder")
   public static final String FAILED_UPLOAD_STATUS = "5";

   @RBEntry("Always ask whether to open or save")
   public static final String PRIVATE_CONSTANT_14 = "alwaysAsk";

   @RBEntry("Always open file in application")
   public static final String PRIVATE_CONSTANT_15 = "alwaysOpen";

   @RBEntry("Always save file to directory")
   public static final String PRIVATE_CONSTANT_16 = "alwaysSave";

   @RBEntry("Primary file {0} not found for uploading.  Use existing primary file and continue?")
   @RBArgComment0("primary file filepath as submitted from checkin/update form")
   public static final String PRIVATE_CONSTANT_17 = "askContinueIfNotFound";

   @RBEntry("Primary file {0} has not changed.  Continue the checkin?")
   @RBArgComment0("primary file filepath as submitted from checkin/update form")
   public static final String PRIVATE_CONSTANT_18 = "askContinueIfUnchangedCheckin";

   @RBEntry("Primary file {0} has not changed.  Continue the update?")
   @RBArgComment0("primary file filepath as submitted from checkin/update form")
   public static final String PRIVATE_CONSTANT_19 = "askContinueIfUnchangedUpdate";

   @RBEntry("Continue checkin without uploading primary file?")
   public static final String PRIVATE_CONSTANT_20 = "askContinueOrCancelCheckin";

   @RBEntry("Continue update without uploading primary file?")
   public static final String PRIVATE_CONSTANT_21 = "askContinueOrCancelUpdate";

   @RBEntry("Primary file {0} has changed.  Upload changed file with checkin?")
   @RBArgComment0("primary file filepath as submitted from checkin/update form")
   public static final String PRIVATE_CONSTANT_22 = "askUploadIfChangedCheckin";

   @RBEntry("Primary file {0} has changed.  Upload changed file with update?")
   @RBArgComment0("primary file filepath as submitted from checkin/update form")
   public static final String PRIVATE_CONSTANT_23 = "askUploadIfChangedUpdate";

   @RBEntry("Primary file {0} not found for uploading.  Use file {1} from workspace and continue?")
   @RBArgComment0("primary file filepath as submitted from checkin/update form")
   @RBArgComment1("workspace directory path with primary file filename")
   public static final String PRIVATE_CONSTANT_24 = "askUseWorkspaceUpload";

   @RBEntry("Preferences controlling the upload and download of document primary content files.")
   public static final String PRIVATE_CONSTANT_25 = "contentUploadDownloadDescription";

   @RBEntry("Content")
   public static final String PRIVATE_CONSTANT_26 = "contentUploadDownloadDisplayName";

   @RBEntry("Continue without uploading if file not found")
   public static final String PRIVATE_CONSTANT_27 = "fileNotFoundBehaviorContinueLabel";

   @RBEntry("Have the option to continue or browse again if file not found")
   public static final String PRIVATE_CONSTANT_28 = "fileNotFoundBehaviorPromptLabel";

   @RBEntry("If the primary content file selected for document <B>Update</B> or <B>Check In</B> cannot be found in the specified directory on your computer, choose to keep the existing primary content file (if one exists) and continue the operation without uploading the file, or choose to have the option of whether to continue without uploading or to go back and browse for a valid file.  The system default is to offer the option.")
   public static final String PRIVATE_CONSTANT_29 = "fileNotFoundBehaviorDescription";

   @RBEntry("File Not Found Behavior")
   public static final String PRIVATE_CONSTANT_30 = "fileNotFoundBehaviorDisplayName";

   @RBEntry("Choose behavior if local file is not found.")
   public static final String PRIVATE_CONSTANT_31 = "fileNotFoundBehaviorShortDescription";

   @RBEntry("Continue without uploading the unchanged file")
   public static final String PRIVATE_CONSTANT_32 = "fileUnchangedBehaviorContinueLabel";

   @RBEntry("Have the option to continue or browse again if file unchanged")
   public static final String PRIVATE_CONSTANT_33 = "fileUnchangedBehaviorPromptLabel";

   @RBEntry("When a document <B>Update</B> or <B>Check In</B> is performed, the local file on your computer is compared with the primary content file stored in Windchill.  If files are the same, choose to continue the operation without uploading the unchanged file, or choose to have the option of whether to continue without uploading or to go back and browse for a different file.  The system default is to continue without uploading.")
   public static final String PRIVATE_CONSTANT_34 = "fileUnchangedBehaviorDescription";

   @RBEntry("Unchanged File Behavior")
   public static final String PRIVATE_CONSTANT_35 = "fileUnchangedBehaviorDisplayName";

   @RBEntry("Choose behavior if local file has not changed.")
   public static final String PRIVATE_CONSTANT_36 = "fileUnchangedBehaviorShortDescription";

   @RBEntry("Download primary content file on document checkout")
   public static final String PRIVATE_CONSTANT_37 = "downloadOnCheckoutAllowLabel";

   @RBEntry("Do not download primary file on document checkout")
   public static final String PRIVATE_CONSTANT_38 = "downloadOnCheckoutSkipLabel";

   @RBEntry("Choose whether or not to have the primary content file downloaded when you check out a document.  If the primary content file is not downloaded upon checkout, it can be downloaded separately through <B>Get Content</B>.  The system default is to download the primary content file when a document is checked out.")
   public static final String PRIVATE_CONSTANT_39 = "downloadOnCheckoutDescription";

   @RBEntry("Content Download Upon Document Checkout")
   public static final String PRIVATE_CONSTANT_40 = "downloadOnCheckoutDisplayName";

   @RBEntry("Choose whether primary content file is downloaded when you check out a document.")
   public static final String PRIVATE_CONSTANT_41 = "downloadOnCheckoutShortDescription";

   @RBEntry("When downloading the primary content file of a document, choose to have the file automatically opened in an application or saved to a directory you select on your computer.  The system default is to ask you whether to open or save whenever a primary content file is downloaded.<BR><BR><B>Note:</B>  This preference only applies to downloads performed through Windchill Desktop Integration or through Java applets (Windchill DTI or Java plug-ins required).<BR>")
   @RBComment("Please preserve the HTML formatting tags to help emphasize the exclusions at the end.")
   public static final String PRIVATE_CONSTANT_42 = "downloadOpTypeDescription";

   @RBEntry("Download Operation Type")
   public static final String PRIVATE_CONSTANT_43 = "downloadOpTypeDisplayName";

   @RBEntry("Choose to have primary file opened or saved to disk.")
   public static final String PRIVATE_CONSTANT_44 = "downloadOpTypeShortDescription";

   @RBEntry("File Upload/Download")
   public static final String PRIVATE_CONSTANT_45 = "fileUploadDownload";

   @RBEntry("Automatically upload the changed file")
   public static final String PRIVATE_CONSTANT_46 = "fileChangedBehaviorUploadLabel";

   @RBEntry("Have the option to upload or not upload")
   public static final String PRIVATE_CONSTANT_47 = "fileChangedBehaviorPromptLabel";

   @RBEntry("When a document <B>Update</B> or <B>Check In</B> is performed, the local file on your computer is compared with the primary content file stored in Windchill.  If files are different, choose to automatically upload the changed file, or choose to have the option of uploading or not uploading.  The system default is to upload the file.")
   public static final String PRIVATE_CONSTANT_48 = "fileChangedBehaviorDescription";

   @RBEntry("Changed File Behavior")
   public static final String PRIVATE_CONSTANT_49 = "fileChangedBehaviorDisplayName";

   @RBEntry("Choose behavior if local file has changed.")
   public static final String PRIVATE_CONSTANT_50 = "fileChangedBehaviorShortDescription";

   @RBEntry("Current Default Directory:")
   public static final String PRIVATE_CONSTANT_51 = "workspacePathCurrent";

   @RBEntry("New Default Directory:")
   public static final String PRIVATE_CONSTANT_52 = "workspacePathNew";

   @RBEntry("Choose a directory on your local computer to be a default location for uploading and downloading the primary content files of Windchill documents. This preference applies only when uploading and downloading files through Java applets or Desktop Integration.  If you do not choose a default directory, or if you log in from a different computer where your specified default directory value does not exist, a default will be determined by your operating system.<BR><BR><B>Note:  </B>Set your default directory by browsing to and selecting a file in the directory you want to use. The directory must already exist on your computer and contain at least one file.  If, instead of browsing, you manually enter a directory path, always use a filepath separator after the final directory (ex. \"C:\\MyFolder\\\" rather than \"C:\\MyFolder\").<BR>")
   @RBComment("Please preserve the HTML formatting tags to help emphasize the instructions at the end, please preserve the double-backslashes which will display as a single backslash.  It would be fine to change \"MyFolder\" to some localized example folder name instead.")
   public static final String PRIVATE_CONSTANT_53 = "workspacePathDescription";

   @RBEntry("Default Local Directory")
   public static final String PRIVATE_CONSTANT_54 = "workspacePathDisplayName";

   @RBEntry("Choose a default directory for uploading and downloading primary files.")
   public static final String PRIVATE_CONSTANT_55 = "workspacePathShortDescription";

   @RBEntry("File {0} could not be read for upload.  Operation cancelled.")
   public static final String PRIVATE_CONSTANT_56 = "fileNotFoundCancel";

   @RBEntry("File {0} could not be read for upload.  Existing primary file was kept.")
   public static final String PRIVATE_CONSTANT_57 = "fileNotFoundNoUpload";

   @RBEntry("File {0} could not be read for upload.  Continue without uploading?")
   public static final String PRIVATE_CONSTANT_58 = "fileNotFoundPrompt";

   @RBEntry("File {0} not found, use file {1} from workspace?")
   public static final String PRIVATE_CONSTANT_59 = "fileNotFoundWorkspacePrompt";

   @RBEntry("File {0} unchanged from existing primary file.  Operation cancelled.")
   public static final String PRIVATE_CONSTANT_60 = "fileUnchangedCancel";

   @RBEntry("File {0} unchanged from existing primary file.  Existing primary file was kept.")
   public static final String PRIVATE_CONSTANT_61 = "fileUnchangedNoUpload";

   @RBEntry("File {0} unchanged from existing primary file.  Continue without uploading?")
   public static final String PRIVATE_CONSTANT_62 = "fileUnchangedPrompt";

   @RBEntry("File {0} was not uploaded.  Existing primary file was kept.")
   public static final String PRIVATE_CONSTANT_63 = "fileChangedNoUpload";

   @RBEntry("File {0} has changed.  Upload changed file?")
   public static final String PRIVATE_CONSTANT_64 = "fileChangedPrompt";

   @RBEntry("Drag-and-Drop on View Screens")
   public static final String PRIVATE_CONSTANT_65 = "dndMicroAppletEnabledDisplayName";

   @RBEntry("Choose whether drag-and-drop is available on view screens.")
   public static final String PRIVATE_CONSTANT_66 = "dndMicroAppletEnabledShortDescription";

   @RBEntry("Controls whether or not drag-and-drop applet functionality is enabled on HTML view screens such as search results, cabinet and folder contents, and properties pages.  When this functionality is enabled, lightweight drag-and-drop-aware applets are used in these pages to graphically represent Windchill objects, whereas HTML image tags are used in the same places when this functionality is disabled.  The system default is to disable drag-and-drop.")
   public static final String PRIVATE_CONSTANT_67 = "dndMicroAppletEnabledDescription";

   @RBEntry("Enable drag-and-drop.")
   public static final String PRIVATE_CONSTANT_68 = "dndMicroAppletEnabledLabel";

   @RBEntry("Disable drag-and-drop.")
   public static final String PRIVATE_CONSTANT_69 = "dndMicroAppletDisabledLabel";

   @RBEntry("Drag-and-Drop on Form Pages")
   public static final String PRIVATE_CONSTANT_70 = "fileDndAppletEnabledDisplayName";

   @RBEntry("Choose whether drag-and-drop is available on form pages.")
   public static final String PRIVATE_CONSTANT_71 = "fileDndAppletEnabledShortDescription";

   @RBEntry("Choose whether or not drag-and-drop functionality is enabled on form pages such as document <B>Create, Update,</B> and <B>Check In</B>.  The system default is to disable drag-and-drop.")
   public static final String PRIVATE_CONSTANT_72 = "fileDndAppletEnabledDescription";

   @RBEntry("Enable drag-and-drop.")
   public static final String PRIVATE_CONSTANT_73 = "fileDndAppletEnabledLabel";

   @RBEntry("Disable drag-and-drop.")
   public static final String PRIVATE_CONSTANT_74 = "fileDndAppletDisabledLabel";

   @RBEntry("Document Update Drag-and-Drop Trigger")
   public static final String PRIVATE_CONSTANT_75 = "dndDocUpdateEnabledDisplayName";

   @RBEntry("Choose whether dropping a file on a document triggers an update.")
   public static final String PRIVATE_CONSTANT_76 = "dndDocUpdateEnabledShortDescription";

   @RBEntry("Controls whether or not document update can be triggered by dropping a file onto a document applet in HTML view pages.  This setting is ignored when Drag-and-Drop On View Screens is not enabled.  When enabled this functionality only applies to updatable documents, i.e. not those which are checked-in to a shared cabinet.  The default value of this settings is enabled, i.e. by default this functionality is enabled whenever Drag-and-Drop On View Screens is enabled.")
   public static final String PRIVATE_CONSTANT_77 = "dndDocUpdateEnabledDescription";

   @RBEntry("Enable drag-and-drop.")
   public static final String PRIVATE_CONSTANT_78 = "dndDocUpdateEnabledLabel";

   @RBEntry("Disable drag-and-drop.")
   public static final String PRIVATE_CONSTANT_79 = "dndDocUpdateDisabledLabel";

   @RBEntry("File Browser on Document Form Pages")
   public static final String PRIVATE_CONSTANT_80 = "fileBrowseAppletEnabledDisplayName";

   @RBEntry("Choose between file browse applet or HTML file input on all document forms.")
   public static final String PRIVATE_CONSTANT_81 = "fileBrowseAppletEnabledShortDescription";

   @RBEntry("Choose between file browse applet or HTML file input on form pages such as document <B>Create, Update,</B> and <B>Check In</B>.  Note that pages load more quickly if you choose HTML file input, but other functionality is only available through the file browse applet, such as the file browser automatically opening to files of type \"*.* All Files\" in the directory specified in the Workspace preference.  The system default is the file browse applet.")
   public static final String PRIVATE_CONSTANT_82 = "fileBrowseAppletEnabledDescription";

   @RBEntry("File browse applet")
   public static final String PRIVATE_CONSTANT_83 = "fileBrowseAppletEnabledLabel";

   @RBEntry("HTML file input")
   public static final String PRIVATE_CONSTANT_84 = "fileBrowseAppletDisabledLabel";

   @RBEntry("Primary File Browser on Document Form Pages")
   public static final String PRIVATE_CONSTANT_85 = "primaryFileBrowseAppletEnabledDisplayName";

   @RBEntry("Choose between file browse applet or HTML file input on  document forms with primary content.")
   public static final String PRIVATE_CONSTANT_86 = "primaryFileBrowseAppletEnabledShortDescription";

   @RBEntry("Choose between file browse applet or HTML file input on form pages such as document <B>Create, Update,</B> and <B>Check In</B>.  Note that pages load more quickly if you choose HTML file input, but other functionality is only available through the file browse applet, such as the file browser automatically opening to files of type \"*.* All Files\" in the directory specified in the Workspace preference.  The system default is the file browse applet.")
   public static final String PRIVATE_CONSTANT_87 = "primaryFileBrowseAppletEnabledDescription";

   @RBEntry("File browse applet")
   public static final String PRIVATE_CONSTANT_88 = "primaryFileBrowseAppletEnabledLabel";

   @RBEntry("HTML file input")
   public static final String PRIVATE_CONSTANT_89 = "primaryFileBrowseAppletDisabledLabel";

   @RBEntry("Attachments File Browser on Document Form Pages")
   public static final String PRIVATE_CONSTANT_90 = "secondaryFileBrowseAppletEnabledDisplayName";

   @RBEntry("Choose between file browse applet or HTML file input on all document forms with attachments.")
   public static final String PRIVATE_CONSTANT_91 = "secondaryFileBrowseAppletEnabledShortDescription";

   @RBEntry("Choose between file browse applet or HTML file input on primary content attachments form pages such as document <B>Create</B> and <B>Update</B>.  Note that pages load more quickly if you choose HTML file input, but other functionality is only available through the file browse applet, such as the file browser automatically opening to files of type \"*.* All Files\" in the directory specified in the Workspace preference.  The system default is the file browse applet.")
   public static final String PRIVATE_CONSTANT_92 = "secondaryFileBrowseAppletEnabledDescription";

   @RBEntry("File browse applet")
   public static final String PRIVATE_CONSTANT_93 = "secondaryFileBrowseAppletEnabledLabel";

   @RBEntry("HTML file input")
   public static final String PRIVATE_CONSTANT_94 = "secondaryFileBrowseAppletDisabledLabel";

   @RBEntry("Preferred File Server")
   public static final String PRIVATE_CONSTANT_95 = "contentCacheSiteBehaviorDisplayName";

   @RBEntry("Choose preferred file server site for upload and download.")
   public static final String PRIVATE_CONSTANT_96 = "contentCacheSiteBehaviorShortDescription";

   @RBEntry("When a Windchill object content is downloaded or uploaded, choosing the appropriate File Server Site is generally helpful to improve the performance. The default site is Windchill Master Site.")
   public static final String PRIVATE_CONSTANT_97 = "contentCacheSiteBehaviorDescription";

   @RBEntry("Windchill Master Site:")
   public static final String PRIVATE_CONSTANT_98 = "contentCacheSiteMasterLabel";

   @RBEntry("Preferred Content Cache Site:")
   public static final String PRIVATE_CONSTANT_99 = "contentCacheSiteReplicaLabel";

   @RBEntry("Java Applet File Download Option")
   public static final String PRIVATE_CONSTANT_100 = "JPIDetectionForDownloadDisplayName";

   @RBEntry("Choose whether to use Java applet to download files for editing.")
   public static final String PRIVATE_CONSTANT_101 = "JPIDetectionForDownloadShortDescription";

   @RBEntry("Choose whether to use a Java applet to download primary files from Windchill document actions such as <B>Check Out</B> or <B>Get Content</B>. (This requires the Java plug-in be installed locally.) Downloading with the Java applet provides the following enhanced functionality:<UL><LI>You do not need to re-browse for the file location upon <B>Update</B> or <B>Check In</B>.<LI>Unchanged files will not be unnecessarily re-uploaded.<LI>You can download the primary files from all documents in a structure with a single action.</UL>The system default is to use the Java applet for the primary file download when performing a <B>Check Out</B> or <B>Get Content</B> (if the Java plug-in is detected on your local machine).")
   public static final String PRIVATE_CONSTANT_102 = "JPIDetectionForDownloadDescription";

   @RBEntry("Choose whether to use a Java applet to download primary files from Windchill document actions such as <B>Check Out</B> or <B>View</B>. (This requires the Java plug-in be installed locally.) Downloading with the Java applet provides the following enhanced functionality:<UL><LI>You do not need to re-browse for the file location upon <B>Update</B> or <B>Check In</B>.<LI>Unchanged files will not be unnecessarily re-uploaded.</UL>The system default is to use the Java applet for the primary file download when performing a <B>Check Out</B> or <B>View</B> (if the Java plug-in is detected on your local machine).")
   public static final String PRIVATE_CONSTANT_103 = "JPIDetectionForDownloadDescriptionPDMLink";

   @RBEntry("Always use Java applet to download the primary file of Windchill documents (if Java plug-in is detected).  ")
   public static final String PRIVATE_CONSTANT_104 = "JPIDetectionForDownloadTrueLabel";

   @RBEntry("Never use Java applet to download the primary file of Windchill documents, regardless of whether or not the Java plug-in is present. Use basic browser handling to download the primary file of Windchill documents.")
   public static final String PRIVATE_CONSTANT_105 = "JPIDetectionForDownloadFalseLabel";

   @RBEntry("Java Applet Plug-In Auto Detect Behavior")
   public static final String PRIVATE_CONSTANT_106 = "JPIDetectionFailoverPromptDisplayName";

   @RBEntry("Choose whether auto detection of a missing Java plug-in will cause a prompt for plug-in installation.")
   public static final String PRIVATE_CONSTANT_107 = "JPIDetectionFailoverPromptShortDescription";

   @RBEntry("Choose the behavior if the <B>Java Applet File Download Option</B> preference is set to \"Always use Java applet to download...\" but the proper Java plug-in is not detected on your machine. The system default is to prompt to install the Java plug-in.")
   @RBComment("need to keep preference name in this description consistent with JPIDetectionForDownloadDisplayName value")
   public static final String PRIVATE_CONSTANT_108 = "JPIDetectionFailoverPromptDescription";

   @RBEntry("Prompt to install the Java plug-in if not detected on your machine.")
   public static final String PRIVATE_CONSTANT_109 = "JPIDetectionFailoverPromptTrueLabel";

   @RBEntry("Automatically use the basic browser handling to download files.")
   public static final String PRIVATE_CONSTANT_110 = "JPIDetectionFailoverPromptFalseLabel";

   @RBEntry("Compress contents")
   public static final String PRIVATE_CONSTANT_111 = "compressContentsAllowLabel";

   @RBEntry("Do not compress contents")
   public static final String PRIVATE_CONSTANT_112 = "compressContentsSkipLabel";

   @RBEntry("Choose whether or not to receive compressed data over HTTP. If 'Compress contents' setting is opted and the browser from which user is accessing Windchill is capable of handling compressed data, user will receive the contents in a compressed form. Uncompression is handled by the browser on the fly. It is recommended to enable this setting in a low bandwidth environment to reduce the network traffic and enhance download performance. <P>If user faces any problem in viewing the content after opting for compressed contents, it is recommended to choose 'Do not compress contents' option. <P>This preference sets a persistant cookie and clearing browser cookies will result into losing this setting. System default is 'Do not compress contents'.<P>Important : In Pro/E Wildfire environment, if Pro/E compression option in enabled (see Pro/E Wildfire documentation for details), user will receive compressed data irrespective of this preference setting. <P><I>Note : This setting is effective if your site administrator has enabled compression filter (see Administrator Guide for details).</I>")
   public static final String PRIVATE_CONSTANT_113 = "compressContentsDescription";

   @RBEntry("Compress Contents")
   public static final String PRIVATE_CONSTANT_114 = "compressContentsDisplayName";

   @RBEntry("Choose to receive compressed data over HTTP.")
   public static final String PRIVATE_CONSTANT_115 = "compressContentsShortDescription";
   
   @RBEntry("Preferred Master Vault")
   public static final String PRIVATE_CONSTANT_116 = "contentMasterVaultBehaviorDisplayName";

   @RBEntry("Choose preferred master vault for upload.")
   public static final String PRIVATE_CONSTANT_117 = "contentMasterVaultBehaviorShortDescription";

   @RBEntry("During Windchill Bulk Migration, when a <I>Windchill</I> object content is <I>uploaded</I>, choosing the appropriate <B>Master Vault</B> is generally helpful to avoid re-vaulting and improve the performance. The default vault is not set.")
   public static final String PRIVATE_CONSTANT_118 = "contentMasterVaultBehaviorDescription";
}
