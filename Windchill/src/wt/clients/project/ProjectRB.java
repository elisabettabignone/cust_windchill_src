/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.project;

import wt.util.resource.*;

@RBUUID("wt.clients.project.ProjectRB")
public final class ProjectRB extends WTListResourceBundle {
   /**
    * Buttons ----------------------------------------------------------------
    **/
   @RBEntry("New...")
   public static final String NEW_BUTTON = "54";

   @RBEntry("View")
   public static final String VIEW_BUTTON = "26";

   @RBEntry("Help")
   public static final String HELP_BUTTON = "23";

   @RBEntry("Update")
   public static final String UPDATE_BUTTON = "55";

   @RBEntry("Delete")
   public static final String DELETE = "28";

   @RBEntry("Copy from...")
   public static final String COPY_FROM = "42";

   @RBEntry("OK")
   public static final String OK_BUTTON = "20";

   @RBEntry("Cancel")
   public static final String CANCEL_BUTTON = "21";

   @RBEntry("Save")
   public static final String SAVE_BUTTON = "22";

   @RBEntry("Browse...")
   public static final String BROWSE_BUTTON = "24";

   @RBEntry("Rename...")
   public static final String RENAME_BUTTON = "51";

   @RBEntry("Save As...")
   public static final String SAVEAS_BUTTON = "52";

   /**
    * Labels -----------------------------------------------------------------
    **/
   @RBEntry("*Team Name:")
   public static final String LABEL_PROJECT_NAME = "56";

   @RBEntry("*Location:")
   public static final String LABEL_LOCATION = "57";

   @RBEntry(" Description:")
   public static final String LABEL_DESCRIPTION_LEFT = "58";

   /**
    * Titles -----------------------------------------------------------------
    **/
   @RBEntry("View Team")
   public static final String TITLE_FRAME_VIEW = "59";

   @RBEntry("Update Team")
   public static final String TITLE_FRAME_UPDATE = "60";

   @RBEntry("Create Team")
   public static final String TITLE_FRAME_CREATE = "61";

   /**
    * Messages ---------------------------------------------------------------
    **/
   @RBEntry("The value of \"Name\" cannot be set to null, since it is a \"required\" attribute.")
   public static final String NULL_PROJECT_NAME = "62";

   @RBEntry("The value of \"Location\" cannot be set to null, since it is a \"required\" attribute.")
   public static final String NULL_LOCATION = "63";

   @RBEntry("Teams must be created in the System cabinet.")
   public static final String NOT_SYSTEM = "64";

   @RBEntry("You do not have permission to update this Team.")
   public static final String NO_UPDATE_PERMISSION = "65";

   @RBEntry("The value exceeds the upper limit of \"128\" for \"Name\".")
   public static final String NAME_EXCEEDS_LIMIT = "72";

   @RBEntry("The value exceeds the upper limit of \"1000\" for \"Description\".")
   public static final String DESCRIPTION_EXCEEDS_LIMIT = "73";

   @RBEntry("*Name:")
   public static final String REQUIRED_NAME_COLON = "74";

   @RBEntry("Location:")
   public static final String LOCATION_COLON = "75";

   @RBEntry("Available Roles:")
   public static final String AVAILABLE_ROLES_COLON = "76";

   @RBEntry("Selected Roles:")
   public static final String SELECTED_ROLES_COLON = "77";

   @RBEntry("New")
   public static final String NEW = "78";

   @RBEntry("Edit")
   public static final String EDIT = "79";

   @RBEntry("Yes")
   public static final String YES = "80";

   @RBEntry("No")
   public static final String NO = "81";

   /**
    * -------------------------------------------------------------------------
    **/
   @RBEntry("Create")
   public static final String CREATE = "25";

   /**
    * RolePanel ----------------------------------------------------------------
    **/
   @RBEntry("Add > >")
   public static final String ADD_BUTTON = "50";

   @RBEntry("Add All > >")
   public static final String ADD_ALL_BUTTON = "47";

   @RBEntry("< < Remove")
   public static final String REMOVE_BUTTON = "48";

   @RBEntry("< < Remove All")
   public static final String REMOVE_ALL_BUTTON = "49";

   @RBEntry("Participants")
   public static final String PARTICIPANTS_BUTTON = "46";

   @RBEntry("Available Roles")
   public static final String AVAILABLE_ROLES_LABEL = "44";

   @RBEntry("Selected Roles")
   public static final String SELECTED_ROLES_LABEL = "45";

   /**
    * Labels ------------------------------------------------------------------
    **/
   @RBEntry("Team")
   public static final String NAME = "29";

   @RBEntry("Location")
   public static final String LOCATION = "30";

   @RBEntry(" Description")
   public static final String DESCRIPTION = "31";

   @RBEntry("Enabled")
   public static final String ENABLED = "32";

   @RBEntry("Context")
   public static final String CONTEXT = "71";

   @RBEntry("*")
   public static final String REQUIRED = "39";

   @RBEntry(":")
   public static final String COLON = "40";

   /**
    * Titles -------------------------------------------------------------------
    **/
   @RBEntry("View Team")
   public static final String VIEW_FRAME = "33";

   @RBEntry("Edit Team")
   public static final String UPDATE_FRAME = "34";

   @RBEntry("New Team")
   public static final String CREATE_FRAME = "35";

   @RBEntry("Select Team")
   public static final String SELECT_PROJECT = "43";

   /**
    * Messages ------------------------------------------------------------------
    **/
   @RBEntry("Fill in the required Team Name field.")
   public static final String PROJECT_NAME_REQUIRED = "36";

   @RBEntry("Fill in the Location field, or click Browse... to select a Location.")
   public static final String LOCATION_REQUIRED = "37";

   @RBEntry("You do not have permission to update this Team.")
   public static final String UPDATE_PERMISSION_REQUIRED = "38";

   @RBEntry("Team name can not be changed after the Team has been saved to the database.")
   public static final String MESSAGE_PERSISTED_NAME_CHANGE = "18";

   @RBEntry("Internal Error: CProject object has neither a projectRef or roleHolder objects populated.  This message should never occur.")
   public static final String MESSAGE_CPROJECT_INVALID = "19";

   @RBEntry("Finding Teams...")
   public static final String FINDING_PROJECTS = "41";

   @RBEntry("Your are not authorized to modify this Team.")
   public static final String MODIFICATION_NOT_AUTHORIZED = "53";

   @RBEntry("You do not have permissions to create a Team.")
   public static final String NO_CREATE = "66";

   /**
    * -------------------------------------------------------------------------
    **/
   @RBEntry("Changes were made to template {0} which have not been saved. Save these changes?")
   public static final String UNSAVED = "67";

   @RBEntry("Save Changes")
   public static final String SAVE_CHANGES = "68";

   @RBEntry("Team Administration")
   public static final String TEAM_ADMINISTRATOR = "69";
   
   @RBEntry("Administration")
   public static final String ADMINISTRATION = "ADMINISTRATION";

   @RBEntry("Teams")
   public static final String TEAMS = "70";

   @RBEntry("Project Administrator")
   public static final String PROJECT_ADMINISTRATOR = "82";

   @RBEntry("Projects")
   public static final String PROJECTS = "83";
}
