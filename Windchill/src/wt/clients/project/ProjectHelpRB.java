/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.project;

import wt.util.resource.*;

@RBUUID("wt.clients.project.ProjectHelpRB")
public final class ProjectHelpRB extends WTListResourceBundle {
   /**
    * -----Project Admin Defaults----- ----------------------------------------
    **/
   @RBEntry("TeamTemplateAdminAbout")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_0 = "Help/ProjectAdmin/ProjectAdminHelp";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_1 = "Desc/ProjectAdmin/ProjectAdminHelp";

   /**
    * RolePanel components ----------------------------------------------------
    **/
   @RBEntry("The roles available to be selected")
   public static final String PRIVATE_CONSTANT_2 = "Desc/ProjectAdmin/role/AvailableList";

   @RBEntry("Adds the role selected in the available list to the selected list")
   public static final String PRIVATE_CONSTANT_3 = "Desc/ProjectAdmin/role/Add";

   @RBEntry("Adds all the remaining roles in the available list to the selected list")
   public static final String PRIVATE_CONSTANT_4 = "Desc/ProjectAdmin/role/AddAll";

   @RBEntry("Moves the role selected in the selected list to the available list")
   public static final String PRIVATE_CONSTANT_5 = "Desc/ProjectAdmin/role/Remove";

   @RBEntry("Moves all the remaining non-required roles in the selected list to the available list")
   public static final String PRIVATE_CONSTANT_6 = "Desc/ProjectAdmin/role/RemoveAll";

   @RBEntry("Sets whether or not the selected role is required")
   public static final String PRIVATE_CONSTANT_7 = "Desc/ProjectAdmin/role/Required";

   @RBEntry("Allows you to choose participants for the selected role")
   public static final String PRIVATE_CONSTANT_8 = "Desc/ProjectAdmin/role/Participants";

   /**
    * ProjectDefinitionFrame components ---------------------------------------
    **/
   @RBEntry("The name of the Team")
   public static final String PRIVATE_CONSTANT_9 = "Desc/ProjectAdmin/main/textProject";

   @RBEntry("The folder in which the Team is stored")
   public static final String PRIVATE_CONSTANT_10 = "Desc/ProjectAdmin/main/textLocation";

   @RBEntry("Browse for the folder of the Team")
   public static final String PRIVATE_CONSTANT_11 = "Desc/ProjectAdmin/main/buttonLocation";

   @RBEntry("The description of the Team")
   public static final String PRIVATE_CONSTANT_12 = "Desc/ProjectAdmin/main/textDescription";

   @RBEntry("Save all changes and close this window")
   public static final String PRIVATE_CONSTANT_13 = "Desc/ProjectAdmin/main/buttonOK";

   @RBEntry("Save all changes and leave this window open")
   public static final String PRIVATE_CONSTANT_14 = "Desc/ProjectAdmin/main/buttonSave";

   @RBEntry("Cancel without saving changes")
   public static final String PRIVATE_CONSTANT_15 = "Desc/ProjectAdmin/main/buttonCancel";

   @RBEntry("Team Definition help")
   public static final String PRIVATE_CONSTANT_16 = "Desc/ProjectAdmin/main/buttonHelp";

   @RBEntry("Enable or Disable this Team")
   public static final String PRIVATE_CONSTANT_17 = "Desc/ProjectAdmin/main/enabled";

   @RBEntry("Copy the initial details of this team from an existing Team")
   public static final String PRIVATE_CONSTANT_18 = "Desc/ProjectAdmin/main/copyFrom";
}
