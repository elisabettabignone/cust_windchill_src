/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.project;

import wt.util.resource.*;

@RBUUID("wt.clients.project.ProjectRB")
public final class ProjectRB_it extends WTListResourceBundle {
   /**
    * Buttons ----------------------------------------------------------------
    **/
   @RBEntry("Nuovo...")
   public static final String NEW_BUTTON = "54";

   @RBEntry("Visualizza")
   public static final String VIEW_BUTTON = "26";

   @RBEntry("Guida")
   public static final String HELP_BUTTON = "23";

   @RBEntry("Aggiorna")
   public static final String UPDATE_BUTTON = "55";

   @RBEntry("Elimina")
   public static final String DELETE = "28";

   @RBEntry("Copia da...")
   public static final String COPY_FROM = "42";

   @RBEntry("OK")
   public static final String OK_BUTTON = "20";

   @RBEntry("Annulla")
   public static final String CANCEL_BUTTON = "21";

   @RBEntry("Salva")
   public static final String SAVE_BUTTON = "22";

   @RBEntry("Sfoglia...")
   public static final String BROWSE_BUTTON = "24";

   @RBEntry("Rinomina...")
   public static final String RENAME_BUTTON = "51";

   @RBEntry("Salva con nome...")
   public static final String SAVEAS_BUTTON = "52";

   /**
    * Labels -----------------------------------------------------------------
    **/
   @RBEntry("*Nome team:")
   public static final String LABEL_PROJECT_NAME = "56";

   @RBEntry("*Posizione:")
   public static final String LABEL_LOCATION = "57";

   @RBEntry(" Descrizione:")
   public static final String LABEL_DESCRIPTION_LEFT = "58";

   /**
    * Titles -----------------------------------------------------------------
    **/
   @RBEntry("Visualizza team")
   public static final String TITLE_FRAME_VIEW = "59";

   @RBEntry("Aggiorna team")
   public static final String TITLE_FRAME_UPDATE = "60";

   @RBEntry("Crea team")
   public static final String TITLE_FRAME_CREATE = "61";

   /**
    * Messages ---------------------------------------------------------------
    **/
   @RBEntry("Assegnare un valore al campo Nome. È un attributo obbligatorio.")
   public static final String NULL_PROJECT_NAME = "62";

   @RBEntry("Assegnare un valore al campo Posizione. È un attributo obbligatorio.")
   public static final String NULL_LOCATION = "63";

   @RBEntry("I team devono essere creati nello schedario System.")
   public static final String NOT_SYSTEM = "64";

   @RBEntry("Non si dispone dei permessi per aggiornare questo team.")
   public static final String NO_UPDATE_PERMISSION = "65";

   @RBEntry("Il valore supera il limite superiore di \"128\" per \"Nome\".")
   public static final String NAME_EXCEEDS_LIMIT = "72";

   @RBEntry("Il valore supera il limite superiore di \"1000\" per \"Descrizione\".")
   public static final String DESCRIPTION_EXCEEDS_LIMIT = "73";

   @RBEntry("*Nome:")
   public static final String REQUIRED_NAME_COLON = "74";

   @RBEntry("Posizione:")
   public static final String LOCATION_COLON = "75";

   @RBEntry("Ruoli disponibili:")
   public static final String AVAILABLE_ROLES_COLON = "76";

   @RBEntry("Ruoli selezionati:")
   public static final String SELECTED_ROLES_COLON = "77";

   @RBEntry("Nuovo")
   public static final String NEW = "78";

   @RBEntry("Modifica")
   public static final String EDIT = "79";

   @RBEntry("Sì")
   public static final String YES = "80";

   @RBEntry("No")
   public static final String NO = "81";

   /**
    * -------------------------------------------------------------------------
    **/
   @RBEntry("Crea")
   public static final String CREATE = "25";

   /**
    * RolePanel ----------------------------------------------------------------
    **/
   @RBEntry("Aggiungi > >")
   public static final String ADD_BUTTON = "50";

   @RBEntry("Aggiungi tutto > >")
   public static final String ADD_ALL_BUTTON = "47";

   @RBEntry("<< Rimuovi")
   public static final String REMOVE_BUTTON = "48";

   @RBEntry("<< Rimuovi tutto")
   public static final String REMOVE_ALL_BUTTON = "49";

   @RBEntry("Partecipanti")
   public static final String PARTICIPANTS_BUTTON = "46";

   @RBEntry("Ruoli disponibili")
   public static final String AVAILABLE_ROLES_LABEL = "44";

   @RBEntry("Ruoli selezionati")
   public static final String SELECTED_ROLES_LABEL = "45";

   /**
    * Labels ------------------------------------------------------------------
    **/
   @RBEntry("Team")
   public static final String NAME = "29";

   @RBEntry("Posizione")
   public static final String LOCATION = "30";

   @RBEntry(" Descrizione")
   public static final String DESCRIPTION = "31";

   @RBEntry("Attivato")
   public static final String ENABLED = "32";

   @RBEntry("Contesto")
   public static final String CONTEXT = "71";

   @RBEntry("*")
   public static final String REQUIRED = "39";

   @RBEntry(":")
   public static final String COLON = "40";

   /**
    * Titles -------------------------------------------------------------------
    **/
   @RBEntry("Visualizza team")
   public static final String VIEW_FRAME = "33";

   @RBEntry("Modifica team")
   public static final String UPDATE_FRAME = "34";

   @RBEntry("Nuovo team")
   public static final String CREATE_FRAME = "35";

   @RBEntry("Seleziona team")
   public static final String SELECT_PROJECT = "43";

   /**
    * Messages ------------------------------------------------------------------
    **/
   @RBEntry("Completare il campo Nome team.")
   public static final String PROJECT_NAME_REQUIRED = "36";

   @RBEntry("Completare il campo Posizione o fare clic su Sfoglia... per selezionare una posizione.")
   public static final String LOCATION_REQUIRED = "37";

   @RBEntry("Non si dispone dei permessi per aggiornare questo team.")
   public static final String UPDATE_PERMISSION_REQUIRED = "38";

   @RBEntry("Impossibile modificare il nome del team dopo aver salvato il team nel database.")
   public static final String MESSAGE_PERSISTED_NAME_CHANGE = "18";

   @RBEntry("Errore interno: l'oggetto CProject non ha né un oggetto projectRef né un oggetto roleHolder completato. Questo messaggio non dovrebbe essere mai visualizzato.")
   public static final String MESSAGE_CPROJECT_INVALID = "19";

   @RBEntry("Ritrovamento team in corso...")
   public static final String FINDING_PROJECTS = "41";

   @RBEntry("Non si dispone dell'autorizzazione a modificare questo team.")
   public static final String MODIFICATION_NOT_AUTHORIZED = "53";

   @RBEntry("Non si dispone dei permessi per creare un team.")
   public static final String NO_CREATE = "66";

   /**
    * -------------------------------------------------------------------------
    **/
   @RBEntry("Le modifiche apportate al modello {0} non sono state salvate. Salvare le modifiche?")
   public static final String UNSAVED = "67";

   @RBEntry("Salva modifiche")
   public static final String SAVE_CHANGES = "68";

   @RBEntry("Amministrazione team")
   public static final String TEAM_ADMINISTRATOR = "69";
   
   @RBEntry("Amministrazione")
   public static final String ADMINISTRATION = "ADMINISTRATION";

   @RBEntry("Team")
   public static final String TEAMS = "70";

   @RBEntry("Amministratore progetto")
   public static final String PROJECT_ADMINISTRATOR = "82";

   @RBEntry("Progetti")
   public static final String PROJECTS = "83";
}
