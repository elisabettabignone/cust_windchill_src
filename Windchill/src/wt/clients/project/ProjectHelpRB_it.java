/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.project;

import wt.util.resource.*;

@RBUUID("wt.clients.project.ProjectHelpRB")
public final class ProjectHelpRB_it extends WTListResourceBundle {
   /**
    * -----Project Admin Defaults----- ----------------------------------------
    **/
   @RBEntry("TeamTemplateAdminAbout")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_0 = "Help/ProjectAdmin/ProjectAdminHelp";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_1 = "Desc/ProjectAdmin/ProjectAdminHelp";

   /**
    * RolePanel components ----------------------------------------------------
    **/
   @RBEntry("Ruoli disponibili da selezionare")
   public static final String PRIVATE_CONSTANT_2 = "Desc/ProjectAdmin/role/AvailableList";

   @RBEntry("Aggiunge il ruolo selezionato nell'elenco disponibile all'elenco selezionato")
   public static final String PRIVATE_CONSTANT_3 = "Desc/ProjectAdmin/role/Add";

   @RBEntry("Aggiunge tutti i ruoli rimanenti nell'elenco disponibile all'elenco selezionato")
   public static final String PRIVATE_CONSTANT_4 = "Desc/ProjectAdmin/role/AddAll";

   @RBEntry("Sposta il ruolo selezionato dall'elenco selezionato a quello disponibile")
   public static final String PRIVATE_CONSTANT_5 = "Desc/ProjectAdmin/role/Remove";

   @RBEntry("Sposta tutti i ruoli non obbligatori rimanenti dall'elenco selezionato a quello disponibile")
   public static final String PRIVATE_CONSTANT_6 = "Desc/ProjectAdmin/role/RemoveAll";

   @RBEntry("Consente di specificare se il ruolo selezionato è obbligatorio")
   public static final String PRIVATE_CONSTANT_7 = "Desc/ProjectAdmin/role/Required";

   @RBEntry("Consente di selezionare i partecipanti per il ruolo selezionato")
   public static final String PRIVATE_CONSTANT_8 = "Desc/ProjectAdmin/role/Participants";

   /**
    * ProjectDefinitionFrame components ---------------------------------------
    **/
   @RBEntry("Nome del team")
   public static final String PRIVATE_CONSTANT_9 = "Desc/ProjectAdmin/main/textProject";

   @RBEntry("Cartella in cui è memorizzato il team")
   public static final String PRIVATE_CONSTANT_10 = "Desc/ProjectAdmin/main/textLocation";

   @RBEntry("Effettua la ricerca della cartella del team")
   public static final String PRIVATE_CONSTANT_11 = "Desc/ProjectAdmin/main/buttonLocation";

   @RBEntry("Descrizione del team")
   public static final String PRIVATE_CONSTANT_12 = "Desc/ProjectAdmin/main/textDescription";

   @RBEntry("Salva tutte le modifiche e chiude la finestra")
   public static final String PRIVATE_CONSTANT_13 = "Desc/ProjectAdmin/main/buttonOK";

   @RBEntry("Salva tutte le modifiche senza chiudere la finestra")
   public static final String PRIVATE_CONSTANT_14 = "Desc/ProjectAdmin/main/buttonSave";

   @RBEntry("Annulla senza salvare le modifiche")
   public static final String PRIVATE_CONSTANT_15 = "Desc/ProjectAdmin/main/buttonCancel";

   @RBEntry("Guida sulla definizione del team")
   public static final String PRIVATE_CONSTANT_16 = "Desc/ProjectAdmin/main/buttonHelp";

   @RBEntry("Attiva o disattiva questo team")
   public static final String PRIVATE_CONSTANT_17 = "Desc/ProjectAdmin/main/enabled";

   @RBEntry("Copia i dettagli iniziali di questo team da un team esistente")
   public static final String PRIVATE_CONSTANT_18 = "Desc/ProjectAdmin/main/copyFrom";
}
