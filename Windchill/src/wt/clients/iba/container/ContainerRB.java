/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.iba.container;

import wt.util.resource.*;

@RBUUID("wt.clients.iba.container.ContainerRB")
public final class ContainerRB extends wt.util.resource.NestableListResourceBundle {
   @RBEntry("Constraints:")
   public static final String CONSTRAINT_LIST_LABEL = "CON_L00";

   @RBEntry("Description:")
   public static final String CONSTRAINT_DESC_LABEL = "CON_L01";

   @RBEntry("<<NONE>>")
   public static final String CONSTRAINT_NONE_LABEL = "CON_L02";

   @RBEntry("Attribute")
   public static final String CONTAINER_TABLE_HEADER_1 = "hdr00a";

   @RBEntry("Value")
   public static final String CONTAINER_TABLE_HEADER_2 = "hdr00b";

   @RBEntry("Value")
   public static final String VALUE_DEPENDENCY_TABLE_HEADER_1 = "hdr01a";

   @RBEntry("Reference Attribute")
   public static final String VALUE_DEPENDENCY_TABLE_HEADER_2 = "hdr01b";

   @RBEntry("Reference Value")
   public static final String VALUE_DEPENDENCY_TABLE_HEADER_3 = "hdr01c";

   @RBEntry("wt/clients/images/upArrow.gif")
   @RBPseudo(false)
   public static final String IMAGE_LEFT_ARROW = "im00";

   @RBEntry("wt/clients/images/downArrow.gif")
   @RBPseudo(false)
   public static final String IMAGE_RIGHT_ARROW = "im01";

   @RBEntry("wt/clients/images/dotdotdot.gif")
   @RBPseudo(false)
   public static final String IMAGE_DETAILS = "im02";

   @RBEntry("Add Attributes")
   public static final String CONTAINER_BUTTON_LABEL_1 = "la00a";

   @RBEntry("Remove Attributes")
   public static final String CONTAINER_BUTTON_LABEL_2 = "la00b";

   @RBEntry("Add")
   public static final String VALUE_DETAILS_MAINPANEL_ADD_BUTTON = "la01";

   @RBEntry("Remove")
   public static final String VALUE_DETAILS_MAINPANEL_REMOVE_BUTTON = "la02";

   @RBEntry("Are you sure you want to remove the selected attribute(s)?")
   public static final String CONFIRM_DELETE = "msg00";

   @RBEntry("Constraint Violation, \"{0}\" :\n{1}")
   public static final String CONSTRAINT_VIOLATED = "msg01";

   @RBEntry("One or more attributes does not have a value specified.\nIf you continue, these attributes will not be saved.\nDo you want to continue?")
   public static final String WARNING_UNINITIALIZED_VALUES = "msg02";

   @RBEntry("\"{0}\" already exists.")
   public static final String ATTRIBUTE_EXIST = "msg03";

   @RBEntry("Edit")
   public static final String POPUP_EDIT = "popup00";

   @RBEntry("View")
   public static final String POPUP_VIEW = "popup01";

   @RBEntry("Edit Attributes")
   public static final String CONTAINER_EDITOR_TITLE = "ti00";

   @RBEntry("View Attributes")
   public static final String CONTAINER_VIEWER_TITLE = "ti01";

   @RBEntry("Edit Attribute Value Details")
   public static final String VALUE_DETAILS_EDIT_TITLE = "ti02";

   @RBEntry("View Attribute Value Details")
   public static final String VALUE_DETAILS_VIEW_TITLE = "ti03";

   @RBEntry("Values")
   public static final String VALUE_DETAILS_MAINPANEL_TITLE = "ti04";

   @RBEntry("Current Values")
   public static final String VALUE_DETAILS_MAINPANEL_LIST_TITLE = "ti05";

   @RBEntry("Constraints")
   public static final String VALUE_DETAILS_CONSTRAINTS_TITLE = "ti06";

   @RBEntry("Current Constraints")
   public static final String VALUE_DETAILS_CONSTRAINTS_LIST_TITLE = "ti07";

   @RBEntry("Dependencies")
   public static final String VALUE_DETAILS_DEPENDENCY_TITLE = "ti08";

   @RBEntry("Warning")
   public static final String WARNING_DIALOG_TITLE = "ti09";

   @RBEntry("Details: ")
   public static final String DETAILS = "ti10";

   @RBEntry("Attribute must have at least one value to satisfy \"{0}\" constraint.\n")
   public static final String VALUE_DELETE_ERROR = "msg04";

   @RBEntry("none ")
   public static final String NONE_VALUE = "ti11";
}
