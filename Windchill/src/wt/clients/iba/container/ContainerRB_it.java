/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.iba.container;

import wt.util.resource.*;

@RBUUID("wt.clients.iba.container.ContainerRB")
public final class ContainerRB_it extends wt.util.resource.NestableListResourceBundle {
   @RBEntry("Vincoli:")
   public static final String CONSTRAINT_LIST_LABEL = "CON_L00";

   @RBEntry("Descrizione:")
   public static final String CONSTRAINT_DESC_LABEL = "CON_L01";

   @RBEntry("<<NESSUNO>>")
   public static final String CONSTRAINT_NONE_LABEL = "CON_L02";

   @RBEntry("Attributo")
   public static final String CONTAINER_TABLE_HEADER_1 = "hdr00a";

   @RBEntry("Valore")
   public static final String CONTAINER_TABLE_HEADER_2 = "hdr00b";

   @RBEntry("Valore")
   public static final String VALUE_DEPENDENCY_TABLE_HEADER_1 = "hdr01a";

   @RBEntry("Attributo di riferimento")
   public static final String VALUE_DEPENDENCY_TABLE_HEADER_2 = "hdr01b";

   @RBEntry("Valore di riferimento")
   public static final String VALUE_DEPENDENCY_TABLE_HEADER_3 = "hdr01c";

   @RBEntry("wt/clients/images/upArrow.gif")
   @RBPseudo(false)
   public static final String IMAGE_LEFT_ARROW = "im00";

   @RBEntry("wt/clients/images/downArrow.gif")
   @RBPseudo(false)
   public static final String IMAGE_RIGHT_ARROW = "im01";

   @RBEntry("wt/clients/images/dotdotdot.gif")
   @RBPseudo(false)
   public static final String IMAGE_DETAILS = "im02";

   @RBEntry("Aggiungi attributi")
   public static final String CONTAINER_BUTTON_LABEL_1 = "la00a";

   @RBEntry("Rimuovi attributi")
   public static final String CONTAINER_BUTTON_LABEL_2 = "la00b";

   @RBEntry("Aggiungi")
   public static final String VALUE_DETAILS_MAINPANEL_ADD_BUTTON = "la01";

   @RBEntry("Rimuovi")
   public static final String VALUE_DETAILS_MAINPANEL_REMOVE_BUTTON = "la02";

   @RBEntry("Rimuovere gli attributi selezionati?")
   public static final String CONFIRM_DELETE = "msg00";

   @RBEntry("Violazione dei vincoli, \"{0}\" :\n{1}")
   public static final String CONSTRAINT_VIOLATED = "msg01";

   @RBEntry("Uno o più attributi non hanno un valore specificato.\nSe si continua, questi attributi non verranno salvati.\nContinuare?")
   public static final String WARNING_UNINITIALIZED_VALUES = "msg02";

   @RBEntry("\"{0}\" esiste già.")
   public static final String ATTRIBUTE_EXIST = "msg03";

   @RBEntry("Modifica")
   public static final String POPUP_EDIT = "popup00";

   @RBEntry("Visualizza")
   public static final String POPUP_VIEW = "popup01";

   @RBEntry("Modifica attributi")
   public static final String CONTAINER_EDITOR_TITLE = "ti00";

   @RBEntry("Visualizza attributi")
   public static final String CONTAINER_VIEWER_TITLE = "ti01";

   @RBEntry("Modifica i dettagli del valore dell'attributo")
   public static final String VALUE_DETAILS_EDIT_TITLE = "ti02";

   @RBEntry("Visualizza i dettagli dei valori degli attributi")
   public static final String VALUE_DETAILS_VIEW_TITLE = "ti03";

   @RBEntry("Valori")
   public static final String VALUE_DETAILS_MAINPANEL_TITLE = "ti04";

   @RBEntry("Valori attuali")
   public static final String VALUE_DETAILS_MAINPANEL_LIST_TITLE = "ti05";

   @RBEntry("Vincoli")
   public static final String VALUE_DETAILS_CONSTRAINTS_TITLE = "ti06";

   @RBEntry("Vincoli attuali")
   public static final String VALUE_DETAILS_CONSTRAINTS_LIST_TITLE = "ti07";

   @RBEntry("Dipendenze")
   public static final String VALUE_DETAILS_DEPENDENCY_TITLE = "ti08";

   @RBEntry("Avvertenza")
   public static final String WARNING_DIALOG_TITLE = "ti09";

   @RBEntry("Dettagli:")
   public static final String DETAILS = "ti10";

   @RBEntry("L'attributo deve avere almeno un valore per rispettare il vincolo \"{0}\".\n")
   public static final String VALUE_DELETE_ERROR = "msg04";

   @RBEntry("nessuno")
   public static final String NONE_VALUE = "ti11";
}
