/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.iba;

import wt.util.resource.*;

@RBUUID("wt.clients.iba.IBARB")
public final class IBARB extends WTListResourceBundle {
   @RBEntry("No editor defined for the selected item.")
   public static final String NO_EDITOR_DEFINED = "er01";
}
