/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.iba.widgets;

import wt.util.resource.*;

@RBUUID("wt.clients.iba.widgets.WidgetsRB")
public final class WidgetsRB extends WTListResourceBundle {
   @RBEntry("Store value \"{0}\" for attribute \"{1}\" failed:\n\n{2}")
   public static final String SET_VALUE_FAILED = "e00";

   @RBEntry("Error evaluating shown value for attribute \"{0}\":\n\n{1}")
   public static final String GET_NEW_VALUES_FAILED = "e01";

   @RBEntry("Invalid {0} type has been passed.  The type should be {1}")
   public static final String INVALID_PARAM_PASSED = "e02";

   @RBEntry("Invalid number format, \"{0}\", for attribute \"{1}\".")
   public static final String INVALID_NUMBER_FORMAT = "e03";

   @RBEntry("Error evaluating shown value for attribute \"{0}\":\n\n{1}")
   public static final String GET_SHOWN_VALUE_FAILED = "e04";

   @RBEntry("wt/clients/images/find.gif")
   @RBPseudo(false)
   public static final String IMAGE_FIND = "im00";

   @RBEntry("wt.clients.iba.widgets.BooleanKeyListener")
   @RBPseudo(false)
   public static final String BOOLEAN_KEY_EVENT_LISTENER = "kef00";

   @RBEntry("wt.clients.iba.widgets.FloatKeyListener")
   @RBPseudo(false)
   public static final String FLOAT_KEY_EVENT_LISTENER = "kef01";

   @RBEntry("wt.clients.iba.widgets.IntegerKeyListener")
   @RBPseudo(false)
   public static final String INTEGER_KEY_EVENT_LISTENER = "kef02";

   @RBEntry("wt.clients.iba.widgets.StringKeyListener")
   @RBPseudo(false)
   public static final String STRING_KEY_EVENT_LISTENER = "kef03";

   @RBEntry("wt.clients.iba.widgets.TimestampKeyListener")
   @RBPseudo(false)
   public static final String TIMESTAMP_KEY_EVENT_LISTENER = "kef04";

   @RBEntry("wt.clients.iba.widgets.RatioKeyListener")
   @RBPseudo(false)
   public static final String RATIO_KEY_EVENT_LISTENER = "kef05";

   @RBEntry("wt.clients.iba.widgets.ReferenceKeyListener")
   @RBPseudo(false)
   public static final String REFERENCE_KEY_EVENT_LISTENER = "kef06";

   @RBEntry("wt.clients.iba.widgets.URLKeyListener")
   @RBPseudo(false)
   public static final String URL_KEY_EVENT_LISTENER = "kef07";

   @RBEntry("wt.clients.iba.widgets.UnitKeyListener")
   @RBPseudo(false)
   public static final String UNIT_KEY_EVENT_LISTENER = "kef08";

   @RBEntry("True")
   public static final String LABEL_TRUE = "lb00";

   @RBEntry("False")
   public static final String LABEL_FALSE = "lb01";

   @RBEntry("Choose Rank")
   public static final String CHOOSE_RANK = "lb02";

   @RBEntry("Select")
   public static final String IBAREFERENCEABLE_SELECTOR_TITLE = "t00";

   @RBEntry("Set Ranking")
   public static final String RANKING_SELECTOR_TITLE = "t01";

   @RBEntry("Boolean\n ")
   public static final String BOOLEAN_TOOLTIP = "tooltip_wt.iba.definition.BooleanDefinition";

   @RBEntry("Real Number\n ")
   public static final String FLOAT_TOOLTIP = "tooltip_wt.iba.definition.FloatDefinition";

   @RBEntry("Integer Number\n ")
   public static final String INTEGER_TOOLTIP = "tooltip_wt.iba.definition.IntegerDefinition";

   @RBEntry("Ratio\n ")
   public static final String RATIO_TOOLTIP = "tooltip_wt.iba.definition.RatioDefinition";

   @RBEntry("Reference\n ")
   public static final String REFERENCE_TOOLTIP = "tooltip_wt.iba.definition.ReferenceDefinition";

   @RBEntry("String\n ")
   public static final String STRING_TOOLTIP = "tooltip_wt.iba.definition.StringDefinition";

   @RBEntry("Date & Time\n ")
   public static final String TIMESTAMP_TOOLTIP = "tooltip_wt.iba.definition.TimestampDefinition";

   @RBEntry("URL\n ")
   public static final String URL_TOOLTIP = "tooltip_wt.iba.definition.URLDefinition";

   @RBEntry("Real Number, with Units\n ")
   public static final String UNIT_TOOLTIP = "tooltip_wt.iba.definition.UnitDefinition";

   @RBEntry("<<NONE>>")
   public static final String NONE_TOOLTIP = "tt00";

   @RBEntry("Constraints:")
   public static final String CONSTRAINTS = "tt01";

   @RBEntry("Link: ")
   public static final String URL_LINK_LABEL = "url00";

   @RBEntry("Label: ")
   public static final String URL_DESCRIPTION_LABEL = "url01";

   @RBEntry("wt.clients.iba.widgets.InlineBooleanValueEditorWidget")
   @RBPseudo(false)
   public static final String BOOLEAN_INLINE_VALUE_EDITOR = "wt.iba.definition.BooleanDefinition";

   @RBEntry("wt.clients.iba.widgets.InlineFloatValueEditorWidget")
   @RBPseudo(false)
   public static final String FLOAT_INLINE_VALUE_EDITOR = "wt.iba.definition.FloatDefinition";

   @RBEntry("wt.clients.iba.widgets.InlineIntegerValueEditorWidget")
   @RBPseudo(false)
   public static final String INTEGER_INLINE_VALUE_EDITOR = "wt.iba.definition.IntegerDefinition";

   @RBEntry("wt.clients.iba.widgets.InlineRatioValueEditorWidget")
   @RBPseudo(false)
   public static final String RATIO_INLINE_VALUE_EDITOR = "wt.iba.definition.RatioDefinition";

   @RBEntry("wt.clients.iba.widgets.InlineReferenceValueEditorWidget")
   @RBPseudo(false)
   public static final String REFERENCE_INLINE_VALUE_EDITOR = "wt.iba.definition.ReferenceDefinition";

   @RBEntry("wt.clients.csm.classification.InlineClassificationNodeValueEditorWidget")
   @RBPseudo(false)
   public static final String REFERENCE_CN_INLINE_VALUE_EDITOR = "wt.iba.definition.ReferenceDefinition-ClassificationNode";

   @RBEntry("wt.clients.iba.widgets.InlineStringValueEditorWidget")
   @RBPseudo(false)
   public static final String STRING_INLINE_VALUE_EDITOR = "wt.iba.definition.StringDefinition";

   @RBEntry("wt.clients.iba.widgets.InlineTimestampValueEditorWidget")
   @RBPseudo(false)
   public static final String TIMESTAMP_INLINE_VALUE_EDITOR = "wt.iba.definition.TimestampDefinition";

   @RBEntry("wt.clients.iba.widgets.InlineURLValueEditorWidget")
   @RBPseudo(false)
   public static final String URL_INLINE_VALUE_EDITOR = "wt.iba.definition.URLDefinition";

   @RBEntry("wt.clients.iba.widgets.InlineUnitValueEditorWidget")
   @RBPseudo(false)
   public static final String UNIT_INLINE_VALUE_EDITOR = "wt.iba.definition.UnitDefinition";

   /**
    * Buttons
    * aparkitny, June 2001, SPR848295
    **/
   @RBEntry("Find")
   public static final String BUTTON_FIND = "bt00";
}
