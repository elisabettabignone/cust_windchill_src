/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.iba.widgets;

import wt.util.resource.*;

@RBUUID("wt.clients.iba.widgets.WidgetsRB")
public final class WidgetsRB_it extends WTListResourceBundle {
   @RBEntry("L'operazione di memorizzazione del valore \"{0}\" dell'attributo \"{1}\" non è riuscita:\n\n{2}")
   public static final String SET_VALUE_FAILED = "e00";

   @RBEntry("Errore durante la valutazione del valore visualizzato dell'attributo \"{0}\":\n\n{1}")
   public static final String GET_NEW_VALUES_FAILED = "e01";

   @RBEntry("Il tipo invalido {0} è stato saltato.  Il tipo dovrebbe essere {1}")
   public static final String INVALID_PARAM_PASSED = "e02";

   @RBEntry("Formato numero non valido, \"{0}\", per l'attributo \"{1}\".")
   public static final String INVALID_NUMBER_FORMAT = "e03";

   @RBEntry("Errore durante la valutazione del valore visualizzato dell'attributo \"{0}\":\n\n{1}")
   public static final String GET_SHOWN_VALUE_FAILED = "e04";

   @RBEntry("wt/clients/images/find.gif")
   @RBPseudo(false)
   public static final String IMAGE_FIND = "im00";

   @RBEntry("wt.clients.iba.widgets.BooleanKeyListener")
   @RBPseudo(false)
   public static final String BOOLEAN_KEY_EVENT_LISTENER = "kef00";

   @RBEntry("wt.clients.iba.widgets.FloatKeyListener")
   @RBPseudo(false)
   public static final String FLOAT_KEY_EVENT_LISTENER = "kef01";

   @RBEntry("wt.clients.iba.widgets.IntegerKeyListener")
   @RBPseudo(false)
   public static final String INTEGER_KEY_EVENT_LISTENER = "kef02";

   @RBEntry("wt.clients.iba.widgets.StringKeyListener")
   @RBPseudo(false)
   public static final String STRING_KEY_EVENT_LISTENER = "kef03";

   @RBEntry("wt.clients.iba.widgets.TimestampKeyListener")
   @RBPseudo(false)
   public static final String TIMESTAMP_KEY_EVENT_LISTENER = "kef04";

   @RBEntry("wt.clients.iba.widgets.RatioKeyListener")
   @RBPseudo(false)
   public static final String RATIO_KEY_EVENT_LISTENER = "kef05";

   @RBEntry("wt.clients.iba.widgets.ReferenceKeyListener")
   @RBPseudo(false)
   public static final String REFERENCE_KEY_EVENT_LISTENER = "kef06";

   @RBEntry("wt.clients.iba.widgets.URLKeyListener")
   @RBPseudo(false)
   public static final String URL_KEY_EVENT_LISTENER = "kef07";

   @RBEntry("wt.clients.iba.widgets.UnitKeyListener")
   @RBPseudo(false)
   public static final String UNIT_KEY_EVENT_LISTENER = "kef08";

   @RBEntry("True")
   public static final String LABEL_TRUE = "lb00";

   @RBEntry("False")
   public static final String LABEL_FALSE = "lb01";

   @RBEntry("Scegli classificazione")
   public static final String CHOOSE_RANK = "lb02";

   @RBEntry("Seleziona")
   public static final String IBAREFERENCEABLE_SELECTOR_TITLE = "t00";

   @RBEntry("Imposta classificazione")
   public static final String RANKING_SELECTOR_TITLE = "t01";

   @RBEntry("Booleano\n")
   public static final String BOOLEAN_TOOLTIP = "tooltip_wt.iba.definition.BooleanDefinition";

   @RBEntry("Numero reale\n")
   public static final String FLOAT_TOOLTIP = "tooltip_wt.iba.definition.FloatDefinition";

   @RBEntry("Numero intero\n")
   public static final String INTEGER_TOOLTIP = "tooltip_wt.iba.definition.IntegerDefinition";

   @RBEntry("Rapporto\n")
   public static final String RATIO_TOOLTIP = "tooltip_wt.iba.definition.RatioDefinition";

   @RBEntry("Riferimento\n")
   public static final String REFERENCE_TOOLTIP = "tooltip_wt.iba.definition.ReferenceDefinition";

   @RBEntry("Stringa\n")
   public static final String STRING_TOOLTIP = "tooltip_wt.iba.definition.StringDefinition";

   @RBEntry("Data e ora\n")
   public static final String TIMESTAMP_TOOLTIP = "tooltip_wt.iba.definition.TimestampDefinition";

   @RBEntry("URL\n")
   public static final String URL_TOOLTIP = "tooltip_wt.iba.definition.URLDefinition";

   @RBEntry("Unità e numero reale\n")
   public static final String UNIT_TOOLTIP = "tooltip_wt.iba.definition.UnitDefinition";

   @RBEntry("<<NESSUNO>>")
   public static final String NONE_TOOLTIP = "tt00";

   @RBEntry("Vincoli:")
   public static final String CONSTRAINTS = "tt01";

   @RBEntry("Link:")
   public static final String URL_LINK_LABEL = "url00";

   @RBEntry("Etichetta:")
   public static final String URL_DESCRIPTION_LABEL = "url01";

   @RBEntry("wt.clients.iba.widgets.InlineBooleanValueEditorWidget")
   @RBPseudo(false)
   public static final String BOOLEAN_INLINE_VALUE_EDITOR = "wt.iba.definition.BooleanDefinition";

   @RBEntry("wt.clients.iba.widgets.InlineFloatValueEditorWidget")
   @RBPseudo(false)
   public static final String FLOAT_INLINE_VALUE_EDITOR = "wt.iba.definition.FloatDefinition";

   @RBEntry("wt.clients.iba.widgets.InlineIntegerValueEditorWidget")
   @RBPseudo(false)
   public static final String INTEGER_INLINE_VALUE_EDITOR = "wt.iba.definition.IntegerDefinition";

   @RBEntry("wt.clients.iba.widgets.InlineRatioValueEditorWidget")
   @RBPseudo(false)
   public static final String RATIO_INLINE_VALUE_EDITOR = "wt.iba.definition.RatioDefinition";

   @RBEntry("wt.clients.iba.widgets.InlineReferenceValueEditorWidget")
   @RBPseudo(false)
   public static final String REFERENCE_INLINE_VALUE_EDITOR = "wt.iba.definition.ReferenceDefinition";

   @RBEntry("wt.clients.csm.classification.InlineClassificationNodeValueEditorWidget")
   @RBPseudo(false)
   public static final String REFERENCE_CN_INLINE_VALUE_EDITOR = "wt.iba.definition.ReferenceDefinition-ClassificationNode";

   @RBEntry("wt.clients.iba.widgets.InlineStringValueEditorWidget")
   @RBPseudo(false)
   public static final String STRING_INLINE_VALUE_EDITOR = "wt.iba.definition.StringDefinition";

   @RBEntry("wt.clients.iba.widgets.InlineTimestampValueEditorWidget")
   @RBPseudo(false)
   public static final String TIMESTAMP_INLINE_VALUE_EDITOR = "wt.iba.definition.TimestampDefinition";

   @RBEntry("wt.clients.iba.widgets.InlineURLValueEditorWidget")
   @RBPseudo(false)
   public static final String URL_INLINE_VALUE_EDITOR = "wt.iba.definition.URLDefinition";

   @RBEntry("wt.clients.iba.widgets.InlineUnitValueEditorWidget")
   @RBPseudo(false)
   public static final String UNIT_INLINE_VALUE_EDITOR = "wt.iba.definition.UnitDefinition";

   /**
    * Buttons
    * aparkitny, June 2001, SPR848295
    **/
   @RBEntry("Trova")
   public static final String BUTTON_FIND = "bt00";
}
