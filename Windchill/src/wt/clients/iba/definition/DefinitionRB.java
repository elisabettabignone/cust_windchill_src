/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.iba.definition;

import wt.util.resource.*;

@RBUUID("wt.clients.iba.definition.DefinitionRB")
public final class DefinitionRB extends wt.util.resource.NestableListResourceBundle {
   @RBEntry("netmarkets/images/folder_open.gif")
   @RBPseudo(false)
   public static final String ROOT_OPEN_IMAGE = "adimg00";

   @RBEntry("netmarkets/images/folder.gif")
   @RBPseudo(false)
   public static final String ROOT_CLOSE_IMAGE = "adimg01";

   @RBEntry("netmarkets/images/folder.gif")
   @RBPseudo(false)
   public static final String ORG_IMAGE = "adimg02";

   @RBEntry("wt/clients/images/attdeficon.gif")
   @RBPseudo(false)
   public static final String DEF_IMAGE = "adimg03";

   @RBEntry("Attribute Root")
   public static final String ATTRIBUTE_ROOT = "ar00";

   @RBEntry("New \"{0}\" failed:\n\n{1}")
   public static final String CREATE_ATTRIB_DEF_FAILED = "ex00";

   @RBEntry("New \"{0}\" failed:\n\n{1}")
   public static final String CREATE_ATTRIB_ORG_FAILED = "ex01";

   @RBEntry("Illegal 'Paste' target.")
   public static final String ILLEGAL_PASTE_TARGET = "ex02";

   @RBEntry("Copy \"{0}\" failed:\n\n{1}")
   public static final String COPY_FAILED = "ex03";

   @RBEntry("Move \"{0}\" failed:\n\n{1}")
   public static final String MOVE_FAILED = "ex04";

   @RBEntry("Edit \"{0}\" failed:\n\n{1}")
   public static final String UPDATE_ATTRIBUTE_FAILED = "ex05";

   @RBEntry("Delete \"{0}\" failed:\n\n{1}")
   public static final String DELETE_ATTRIBUTE_FAILED = "ex06";

   @RBEntry("Attribute organizers cannot be created under attributes.")
   public static final String CREATE_INVALID_ORGANIZER = "ex07";

   @RBEntry("Server access error:\n\n{0}")
   public static final String GENERAL_SERVER_ERROR = "ex08";

   @RBEntry("Name field cannot be empty.")
   public static final String ERR_NO_NAME = "ex09";

   @RBEntry("Measurement System")
   public static final String AD_HEADER = "h00";

   @RBEntry("Default")
   public static final String DEFAULT_HEADER = "h01";

   @RBEntry("Override")
   public static final String OVERRIDE_HEADER = "h02";

   @RBEntry("netmarkets/images/newfoldertl.gif")
   @RBPseudo(false)
   public static final String CREATEORG_ICON = "i00";

   @RBEntry("wt/clients/images/createAtt.gif")
   @RBPseudo(false)
   public static final String CREATEATTR_ICON = "i01";

   @RBEntry("Name:")
   public static final String NAME_LABEL = "la00";

   @RBEntry("Display Name:")
   public static final String DISPLAY_NAME_LABEL = "la01";

   @RBEntry("Description:")
   public static final String DESCRIPTION_LABEL = "la02";

   @RBEntry("Data Type:")
   public static final String DATA_TYPE_LABEL = "la03";

   @RBEntry("Qty of Measure:")
   public static final String QOM_LABEL = "la04";

   @RBEntry("Display Units:")
   public static final String DISPLAY_UNITS_LABEL = "la05";

   @RBEntry("Class Name:")
   public static final String CLASS_LABEL = "la06";

   @RBEntry("Reference Class:")
   public static final String REFERENCE_CLASS_LABEL = "la07";

   @RBEntry("New Attribute")
   public static final String AD_ATTR_CREATOR_TITLE = "ti00";

   @RBEntry("New Attribute Organizer")
   public static final String AD_ORG_CREATOR_TITLE = "ti01";

   @RBEntry("Select Attribute")
   public static final String AD_SELECTOR_TITLE = "ti02";

   @RBEntry("Edit Attribute Organizer")
   public static final String AD_ORGANIZER_EDITOR_TITLE = "ti03";

   @RBEntry("Edit Attribute")
   public static final String AD_ATTRIBUTE_EDITOR_TITLE = "ti04";

   @RBEntry("View Attribute Organizer")
   public static final String AD_ORGANIZER_VIEWER_TITLE = "ti05";

   @RBEntry("View Attribute")
   public static final String AD_ATTRIBUTE_VIEWER_TITLE = "ti06";

   @RBEntry("Manage Global Attributes")
   public static final String AD_MANAGER_TITLE = "ti07";

   @RBEntry("Schema Manager")
   public static final String SCH_MANAGER_TITLE = "ti08";

   @RBEntry("New Organizer ")
   public static final String CREATEORG_TOOLTIP = "tt00";

   @RBEntry("New Attribute ")
   public static final String CREATEATTR_TOOLTIP = "tt01";

   @RBEntry("wt/clients/iba/IBA.html")
   @RBPseudo(false)
   public static final String DEFHOMEPAGE = "url0";

   @RBEntry("TypeMgrGlobalAttrMgrAbout")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String DEFHELPPAGE = "url1";

   @RBEntry("Hierarchy Display Name:")
   public static final String HIERARCHY_DISPLAY_NAME_LABEL = "la08";

   @RBEntry("Logical Identifier:")
   public static final String LOGICAL_IDENTIFIER_LABEL = "la09";

   @RBEntry("The logical identifier contains invalid characters.")
   public static final String INVALID_LOGICAL_IDENTIFIER = "ex10";

   @RBEntry("Unit cannot contain spaces")
   @RBComment("Error message displaed when a Unit contains spaces.")
   public static final String UNIT_CANT_CONTAIN_SPACES = "UNIT_CANT_CONTAIN_SPACES";
}
