/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.iba.definition;

import wt.util.resource.*;

@RBUUID("wt.clients.iba.definition.DefinitionRB")
public final class DefinitionRB_it extends wt.util.resource.NestableListResourceBundle {
   @RBEntry("netmarkets/images/folder_open.gif")
   @RBPseudo(false)
   public static final String ROOT_OPEN_IMAGE = "adimg00";

   @RBEntry("netmarkets/images/folder.gif")
   @RBPseudo(false)
   public static final String ROOT_CLOSE_IMAGE = "adimg01";

   @RBEntry("netmarkets/images/folder.gif")
   @RBPseudo(false)
   public static final String ORG_IMAGE = "adimg02";

   @RBEntry("wt/clients/images/attdeficon.gif")
   @RBPseudo(false)
   public static final String DEF_IMAGE = "adimg03";

   @RBEntry("Radice attributo")
   public static final String ATTRIBUTE_ROOT = "ar00";

   @RBEntry("Creazione nuovo \"{0}\" non riuscita:\n\n {1}")
   public static final String CREATE_ATTRIB_DEF_FAILED = "ex00";

   @RBEntry("Creazione nuovo \"{0}\" non riuscita:\n\n {1}")
   public static final String CREATE_ATTRIB_ORG_FAILED = "ex01";

   @RBEntry("Destinazione invalida di 'Incolla'.")
   public static final String ILLEGAL_PASTE_TARGET = "ex02";

   @RBEntry("Copia di \"{0}\" non riuscita:\n\n{1}")
   public static final String COPY_FAILED = "ex03";

   @RBEntry("Spostamento di \"{0}\" non riuscito:\n\n{1}")
   public static final String MOVE_FAILED = "ex04";

   @RBEntry("Modifica di \"{0}\" non riuscita:\n\n{1}")
   public static final String UPDATE_ATTRIBUTE_FAILED = "ex05";

   @RBEntry("Eliminazione di \"{0}\" non riuscita:\n\n{1}")
   public static final String DELETE_ATTRIBUTE_FAILED = "ex06";

   @RBEntry("Gli organizer di attributi non possono essere creati sotto attributi.")
   public static final String CREATE_INVALID_ORGANIZER = "ex07";

   @RBEntry("Errore di accesso al server:\n\n{0}")
   public static final String GENERAL_SERVER_ERROR = "ex08";

   @RBEntry("Il campo del nome non può essere lasciato vuoto.")
   public static final String ERR_NO_NAME = "ex09";

   @RBEntry("Sistema di misurazione")
   public static final String AD_HEADER = "h00";

   @RBEntry("Default")
   public static final String DEFAULT_HEADER = "h01";

   @RBEntry("Ignora")
   public static final String OVERRIDE_HEADER = "h02";

   @RBEntry("netmarkets/images/newfoldertl.gif")
   @RBPseudo(false)
   public static final String CREATEORG_ICON = "i00";

   @RBEntry("wt/clients/images/createAtt.gif")
   @RBPseudo(false)
   public static final String CREATEATTR_ICON = "i01";

   @RBEntry("Nome:")
   public static final String NAME_LABEL = "la00";

   @RBEntry("Nome visualizzato:")
   public static final String DISPLAY_NAME_LABEL = "la01";

   @RBEntry("Descrizione:")
   public static final String DESCRIPTION_LABEL = "la02";

   @RBEntry("Tipo di dati:")
   public static final String DATA_TYPE_LABEL = "la03";

   @RBEntry("Qtà di misura :")
   public static final String QOM_LABEL = "la04";

   @RBEntry("Unità visualizzate:")
   public static final String DISPLAY_UNITS_LABEL = "la05";

   @RBEntry("Nome classe:")
   public static final String CLASS_LABEL = "la06";

   @RBEntry("Classe di riferimento:")
   public static final String REFERENCE_CLASS_LABEL = "la07";

   @RBEntry("Nuovo attributo")
   public static final String AD_ATTR_CREATOR_TITLE = "ti00";

   @RBEntry("Nuovo organizer attributi")
   public static final String AD_ORG_CREATOR_TITLE = "ti01";

   @RBEntry("Seleziona attributo")
   public static final String AD_SELECTOR_TITLE = "ti02";

   @RBEntry("Modifica organizer attributi")
   public static final String AD_ORGANIZER_EDITOR_TITLE = "ti03";

   @RBEntry("Modifica attributo")
   public static final String AD_ATTRIBUTE_EDITOR_TITLE = "ti04";

   @RBEntry("Visualizza organizer attributi")
   public static final String AD_ORGANIZER_VIEWER_TITLE = "ti05";

   @RBEntry("Visualizza attributo")
   public static final String AD_ATTRIBUTE_VIEWER_TITLE = "ti06";

   @RBEntry("Gestisci attributi globali")
   public static final String AD_MANAGER_TITLE = "ti07";

   @RBEntry("Gestione schema")
   public static final String SCH_MANAGER_TITLE = "ti08";

   @RBEntry("Nuovo organizer attributi")
   public static final String CREATEORG_TOOLTIP = "tt00";

   @RBEntry("Nuovo attributo")
   public static final String CREATEATTR_TOOLTIP = "tt01";

   @RBEntry("wt/clients/iba/IBA_it.html")
   @RBPseudo(false)
   public static final String DEFHOMEPAGE = "url0";

   @RBEntry("TypeMgrGlobalAttrMgrAbout")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String DEFHELPPAGE = "url1";

   @RBEntry("Nome nella gerarchia:")
   public static final String HIERARCHY_DISPLAY_NAME_LABEL = "la08";

   @RBEntry("Identificatore logico:")
   public static final String LOGICAL_IDENTIFIER_LABEL = "la09";

   @RBEntry("L'identificatore logico contiene caratteri non validi.")
   public static final String INVALID_LOGICAL_IDENTIFIER = "ex10";

   @RBEntry("L'unità non può contenere spazi")
   @RBComment("Error message displaed when a Unit contains spaces.")
   public static final String UNIT_CANT_CONTAIN_SPACES = "UNIT_CANT_CONTAIN_SPACES";
}
