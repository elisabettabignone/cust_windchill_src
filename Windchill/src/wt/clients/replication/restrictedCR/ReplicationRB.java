/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.replication.restrictedCR;

import wt.util.resource.*;

@RBUUID("wt.clients.replication.restrictedCR.ReplicationRB")
public final class ReplicationRB extends WTListResourceBundle {
   @RBEntry("Replication Administrator")
   public static final String REPLICATION_ADMIN_TITLE = "1";
}
