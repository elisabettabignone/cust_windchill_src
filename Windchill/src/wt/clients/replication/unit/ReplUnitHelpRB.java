/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.replication.unit;

import wt.util.resource.*;

@RBUUID("wt.clients.replication.unit.ReplUnitHelpRB")
public final class ReplUnitHelpRB extends WTListResourceBundle {
   /**
    * -----UnitTask-----
    **/
   @RBEntry("The name of the replication unit")
   public static final String PRIVATE_CONSTANT_0 = "Desc/replunit/UnitTask/Name";

   @RBEntry("The number of the replication unit")
   public static final String PRIVATE_CONSTANT_1 = "Desc/replunit/UnitTask/Number";

   @RBEntry("The location of the replication unit")
   public static final String PRIVATE_CONSTANT_2 = "Desc/replunit/UnitTask/Location";

   @RBEntry("The iteration of the replication unit")
   public static final String PRIVATE_CONSTANT_3 = "Desc/replunit/UnitTask/IterationIdentifier";

   @RBEntry("The state of the replication unit")
   public static final String PRIVATE_CONSTANT_4 = "Desc/replunit/UnitTask/State";

   @RBEntry("The status of the replication unit")
   public static final String PRIVATE_CONSTANT_5 = "Desc/replunit/UnitTask/StatusText";

   @RBEntry("The creation date of the part")
   public static final String PRIVATE_CONSTANT_6 = "Desc/replunit/UnitTask/CreationDate";

   @RBEntry("The modification date of the part")
   public static final String PRIVATE_CONSTANT_7 = "Desc/replunit/UnitTask/LastUpdated";

   @RBEntry("The person who created the part")
   public static final String PRIVATE_CONSTANT_8 = "Desc/replunit/UnitTask/CreatedByPersonName";

   @RBEntry("The person who last modified the part")
   public static final String PRIVATE_CONSTANT_9 = "Desc/replunit/UnitTask/ModifiedByPersonName";

   @RBEntry("The source of the part")
   public static final String PRIVATE_CONSTANT_10 = "Desc/replunit/UnitTask/Source";

   @RBEntry("The view of the part")
   public static final String PRIVATE_CONSTANT_11 = "Desc/replunit/UnitTask/View";

   @RBEntry("The view of the part")
   public static final String PRIVATE_CONSTANT_12 = "Desc/replunit/UnitTask/ViewName";

   @RBEntry("Click here to see the contents of this part")
   public static final String PRIVATE_CONSTANT_13 = "Desc/replunit/UnitTask/Contents";

   @RBEntry("Click here to see parts in this replication unit")
   public static final String PRIVATE_CONSTANT_14 = "Desc/replunit/UnitTask/Uses";

   @RBEntry("Click here to see the effectivities associated with this part")
   public static final String PRIVATE_CONSTANT_15 = "Desc/replunit/UnitTask/Effectivity";

   @RBEntry("Browse for a folder location")
   public static final String PRIVATE_CONSTANT_16 = "Desc/replunit/UnitTask/Browse";

   @RBEntry("Click here to see documents in this replication unit")
   public static final String PRIVATE_CONSTANT_17 = "Desc/replunit/UnitTask/References";

   @RBEntry("Save the changes to this replication unit and close the window")
   public static final String PRIVATE_CONSTANT_18 = "Desc/replunit/UnitTask/OK";

   @RBEntry("Save the changes to this replication unit")
   public static final String PRIVATE_CONSTANT_19 = "Desc/replunit/UnitTask/Save";

   @RBEntry("Do not save the changes to this replication unit, just close the window")
   public static final String PRIVATE_CONSTANT_20 = "Desc/replunit/UnitTask/Cancel";

   @RBEntry("Display help for this task")
   public static final String PRIVATE_CONSTANT_21 = "Desc/replunit/UnitTask/Help";

   @RBEntry("The list of parts this replication unit contains")
   public static final String PRIVATE_CONSTANT_22 = "Desc/replunit/UnitTask/UsesList";

   @RBEntry("The quantity used of the selected part")
   public static final String PRIVATE_CONSTANT_23 = "Desc/replunit/UnitTask/UsesQuantity";

   @RBEntry("View the selected part")
   public static final String PRIVATE_CONSTANT_24 = "Desc/replunit/UnitTask/UsesView";

   @RBEntry("Add a part to the replication unit")
   public static final String PRIVATE_CONSTANT_25 = "Desc/replunit/UnitTask/UsesAdd";

   @RBEntry("Remove the selected part from the replication unit")
   public static final String PRIVATE_CONSTANT_26 = "Desc/replunit/UnitTask/UsesRemove";

   @RBEntry("The list of documents this replication unit contains")
   public static final String PRIVATE_CONSTANT_27 = "Desc/replunit/UnitTask/ReferencesList";

   @RBEntry("Show versions of the selected document")
   public static final String PRIVATE_CONSTANT_28 = "Desc/replunit/UnitTask/ReferencesShowVersions";

   @RBEntry("Add a reference to a document")
   public static final String PRIVATE_CONSTANT_29 = "Desc/replunit/UnitTask/ReferencesAdd";

   @RBEntry("Remove the selected document from the replication unit")
   public static final String PRIVATE_CONSTANT_30 = "Desc/replunit/UnitTask/ReferencesRemove";

   @RBEntry("Click here to enable the baseline configuration specification qualification.")
   public static final String PRIVATE_CONSTANT_31 = "Desc/replunit/UnitTask/BaselineRadioButton";

   @RBEntry("Click here to enable the effectivity configuration specification qualification.")
   public static final String PRIVATE_CONSTANT_32 = "Desc/replunit/UnitTask/EffectivityRadioButton";

   @RBEntry("Select a view")
   public static final String PRIVATE_CONSTANT_33 = "Desc/replunit/UnitTask/StandardView";

   @RBEntry("Select a Life Cycle State")
   public static final String PRIVATE_CONSTANT_34 = "Desc/replunit/UnitTask/StandardLifeCycleState";

   @RBEntry("The currently specified baseline")
   public static final String PRIVATE_CONSTANT_35 = "Desc/replunit/UnitTask/BaselineName";

   @RBEntry("Choose a Baseline.")
   public static final String PRIVATE_CONSTANT_36 = "Desc/replunit/UnitTask/BaselineSearch";

   @RBEntry("Select an Effectivity-related view")
   public static final String PRIVATE_CONSTANT_37 = "Desc/replunit/UnitTask/EffectivityView";

   @RBEntry("Enter the Effective Date.")
   public static final String PRIVATE_CONSTANT_38 = "Desc/replunit/UnitTask/EffectiveDate";

   @RBEntry("Enter the value for the Configuration Item.")
   public static final String PRIVATE_CONSTANT_39 = "Desc/replunit/UnitTask/Value";

   @RBEntry("The currently specified Configuration Item.")
   public static final String PRIVATE_CONSTANT_40 = "Desc/replunit/UnitTask/ConfigurationItem";

   @RBEntry("Choose a Configuration Item.")
   public static final String PRIVATE_CONSTANT_41 = "Desc/replunit/UnitTask/ConfigurationItemSearch";

   @RBEntry("Click here to see the objects that describe this part")
   public static final String PRIVATE_CONSTANT_42 = "Desc/replunit/UnitTask/BuildRule";

   @RBEntry("The list of objects describing this part")
   public static final String PRIVATE_CONSTANT_43 = "Desc/replunit/UnitTask/BuildRuleList";

   @RBEntry("View the selected object")
   public static final String PRIVATE_CONSTANT_44 = "Desc/replunit/UnitTask/BuildRuleView";

   @RBEntry("ProductReplicationCreatUnit")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_45 = "Help/replunit/CreateUnitTask";

   @RBEntry("ProductReplicationUpdatUnit")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_46 = "Help/replunit/UpdateUnitTask";

   @RBEntry("Launch a new Product Information Explorer")
   public static final String PRIVATE_CONSTANT_47 = "Tip/replunit/PartExplorerTask/pexplr";
}
