/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.replication.unit;

import wt.util.resource.*;

@RBUUID("wt.clients.replication.unit.ReplUnitRB")
public final class ReplUnitRB extends WTListResourceBundle {
   /**
    * Labels
    * 
    **/
   @RBEntry("Created:")
   public static final String PRIVATE_CONSTANT_0 = "createdLbl";

   @RBEntry("Created By:")
   public static final String PRIVATE_CONSTANT_1 = "createdByLbl";

   @RBEntry("Source:")
   public static final String PRIVATE_CONSTANT_2 = "sourceLbl";

   @RBEntry("Location:")
   public static final String PRIVATE_CONSTANT_3 = "locationLbl";

   @RBEntry("Name:")
   public static final String PRIVATE_CONSTANT_4 = "nameLbl";

   @RBEntry("Number:")
   public static final String PRIVATE_CONSTANT_5 = "numberLbl";

   @RBEntry("Status:")
   public static final String PRIVATE_CONSTANT_6 = "statusLbl";

   @RBEntry("Type:")
   public static final String PRIVATE_CONSTANT_7 = "typeLbl";

   @RBEntry("Last Modified:")
   public static final String PRIVATE_CONSTANT_8 = "updatedOnLbl";

   @RBEntry("Modified By:")
   public static final String PRIVATE_CONSTANT_9 = "updatedByLbl";

   @RBEntry("Rev:")
   public static final String PRIVATE_CONSTANT_10 = "revisionLbl";

   @RBEntry("Iteration:")
   public static final String PRIVATE_CONSTANT_11 = "iterationLbl";

   @RBEntry("State:")
   public static final String PRIVATE_CONSTANT_12 = "stateLbl";

   @RBEntry("View:")
   public static final String PRIVATE_CONSTANT_13 = "viewLbl";

   @RBEntry("Include parts in my personal cabinet")
   public static final String PRIVATE_CONSTANT_14 = "checkedOutLbl";

   @RBEntry("Qty:")
   public static final String PRIVATE_CONSTANT_15 = "quantityLbl";

   @RBEntry("Unit:")
   public static final String PRIVATE_CONSTANT_16 = "unitLbl";

   @RBEntry("Finding uses...")
   public static final String PRIVATE_CONSTANT_17 = "findingUsesLbl";

   @RBEntry("Finding Parts...")
   public static final String PRIVATE_CONSTANT_18 = "findingPartsLbl";

   @RBEntry("Finding Documents...")
   public static final String PRIVATE_CONSTANT_19 = "findingDocumentsLbl";

   /**
    * Button Labels
    * 
    **/
   @RBEntry("Add")
   public static final String PRIVATE_CONSTANT_20 = "addButton";

   @RBEntry("Browse...")
   public static final String PRIVATE_CONSTANT_21 = "browseButton";

   @RBEntry("Cancel")
   public static final String PRIVATE_CONSTANT_22 = "cancelButton";

   @RBEntry("Check In")
   public static final String PRIVATE_CONSTANT_23 = "checkinButton";

   @RBEntry("Check Out")
   public static final String PRIVATE_CONSTANT_24 = "checkoutButton";

   @RBEntry("Clear")
   public static final String PRIVATE_CONSTANT_25 = "clearButton";

   @RBEntry("Close")
   public static final String PRIVATE_CONSTANT_26 = "closeButton";

   @RBEntry("Delete")
   public static final String PRIVATE_CONSTANT_27 = "deleteButton";

   @RBEntry("Get")
   public static final String PRIVATE_CONSTANT_28 = "getButton";

   @RBEntry("Help")
   public static final String PRIVATE_CONSTANT_29 = "helpButton";

   @RBEntry("OK")
   public static final String PRIVATE_CONSTANT_30 = "okButton";

   @RBEntry("References")
   public static final String PRIVATE_CONSTANT_31 = "referencesButton";

   @RBEntry("Documents")
   public static final String PRIVATE_CONSTANT_32 = "documentsButton";

   @RBEntry("Contents")
   public static final String PRIVATE_CONSTANT_33 = "contentsButton";

   @RBEntry("Uses")
   public static final String PRIVATE_CONSTANT_34 = "usesButton";

   @RBEntry("Parts")
   public static final String PRIVATE_CONSTANT_35 = "partsButton";

   @RBEntry("Update")
   public static final String PRIVATE_CONSTANT_36 = "updateButton";

   @RBEntry("Save")
   public static final String PRIVATE_CONSTANT_37 = "saveButton";

   @RBEntry("Remove")
   public static final String PRIVATE_CONSTANT_38 = "removeButton";

   @RBEntry("View")
   public static final String PRIVATE_CONSTANT_39 = "viewButton";

   @RBEntry("Show Versions")
   public static final String PRIVATE_CONSTANT_40 = "showVersionsButton";

   /**
    * MultiList column headings
    * 
    **/
   @RBEntry("Number")
   public static final String PRIVATE_CONSTANT_41 = "numberHeading";

   @RBEntry("Name")
   public static final String PRIVATE_CONSTANT_42 = "nameHeading";

   @RBEntry("Qty")
   public static final String PRIVATE_CONSTANT_43 = "quantityHeading";

   @RBEntry("Unit")
   public static final String PRIVATE_CONSTANT_44 = "unitHeading";

   @RBEntry("Described By")
   public static final String PRIVATE_CONSTANT_45 = "managedByHeading";

   @RBEntry("Described By")
   public static final String PRIVATE_CONSTANT_46 = "buildRuleButton";

   @RBEntry("Finding described by...")
   public static final String PRIVATE_CONSTANT_47 = "findingBuildRuleLbl";

   @RBEntry("Described By:")
   public static final String PRIVATE_CONSTANT_48 = "buildRuleTitle";

   @RBEntry("Owned by {0}")
   public static final String PRIVATE_CONSTANT_49 = "ownedBy";

   @RBEntry("Size")
   public static final String PRIVATE_CONSTANT_50 = "sizeHeading";

   @RBEntry("Status")
   public static final String PRIVATE_CONSTANT_51 = "statusHeading";

   @RBEntry("Version")
   public static final String PRIVATE_CONSTANT_52 = "versionHeading";

   @RBEntry("Type")
   public static final String PRIVATE_CONSTANT_53 = "typeHeading";

   @RBEntry("ID")
   public static final String PRIVATE_CONSTANT_54 = "idHeading";

   @RBEntry("Identity")
   public static final String PRIVATE_CONSTANT_55 = "identityHeading";

   @RBEntry("Revision")
   public static final String PRIVATE_CONSTANT_56 = "revisionHeading";

   @RBEntry("State")
   public static final String PRIVATE_CONSTANT_57 = "stateHeading";

   @RBEntry("View")
   public static final String PRIVATE_CONSTANT_58 = "viewHeading";

   @RBEntry("*")
   @RBPseudo(false)
   @RBComment("Symbols")
   public static final String PRIVATE_CONSTANT_59 = "required";

   /**
    * Titles
    * 
    **/
   @RBEntry("Uses:")
   public static final String PRIVATE_CONSTANT_60 = "usesTitle";

   @RBEntry("Parts:")
   public static final String PRIVATE_CONSTANT_61 = "partsTitle";

   @RBEntry("Used By:")
   public static final String PRIVATE_CONSTANT_62 = "usedByTitle";

   @RBEntry("Contents:")
   public static final String PRIVATE_CONSTANT_63 = "contentsTitle";

   @RBEntry("References:")
   public static final String PRIVATE_CONSTANT_64 = "referencesTitle";

   @RBEntry("Documents:")
   public static final String PRIVATE_CONSTANT_65 = "documentsTitle";

   @RBEntry("Set Configuration Specification")
   public static final String PRIVATE_CONSTANT_66 = "configSpecTitle";

   @RBEntry("Create Replication Unit {0}")
   public static final String PRIVATE_CONSTANT_67 = "createWTUnitTitle";

   @RBEntry("Update Replication Unit <{0}>")
   public static final String PRIVATE_CONSTANT_68 = "updateWTUnitTitle";

   @RBEntry("Working Copy of {0}")
   public static final String PRIVATE_CONSTANT_69 = "workingCopyTitle";

   @RBEntry("Find Part")
   public static final String PRIVATE_CONSTANT_70 = "findPartTitle";

   @RBEntry("Find Document")
   public static final String PRIVATE_CONSTANT_71 = "findDocTitle";

   @RBEntry("Checked In")
   public static final String PRIVATE_CONSTANT_72 = "checkedIn";

   @RBEntry("Checked Out")
   public static final String PRIVATE_CONSTANT_73 = "checkedOut";

   @RBEntry("Checked Out by {0}")
   public static final String PRIVATE_CONSTANT_74 = "checkedOutBy";

   @RBEntry("Working Copy")
   public static final String PRIVATE_CONSTANT_75 = "workingCopy";

   @RBEntry("Standard")
   public static final String PRIVATE_CONSTANT_76 = "standardSectionLbl";

   @RBEntry("Baseline")
   public static final String PRIVATE_CONSTANT_77 = "baselineSectionLbl";

   @RBEntry("Effectivity")
   public static final String PRIVATE_CONSTANT_78 = "effectivitySectionLbl";

   @RBEntry("Effective Date:")
   public static final String PRIVATE_CONSTANT_79 = "effectiveDateLbl";

   @RBEntry("Configuration Item:")
   public static final String PRIVATE_CONSTANT_80 = "configurationItemLbl";

   @RBEntry("Value:")
   public static final String PRIVATE_CONSTANT_81 = "valueLbl";

   @RBEntry("Search...")
   public static final String PRIVATE_CONSTANT_82 = "searchButton";

   @RBEntry("Find...")
   public static final String PRIVATE_CONSTANT_83 = "findButton";

   /**
    * Messages
    * 
    **/
   @RBEntry("The following error occurred while trying to initiate the view task for {0}:  {1}")
   public static final String INITIATE_VIEW_FAILED = "5";

   @RBEntry("All iterations in this version of \"{0}\" will be deleted.  Continue deleting \"{0}\"?")
   public static final String CONFIRM_DELETE_VERSIONS = "9";

   @RBEntry("Currently, the Windchill Explorer does not support viewing of {0} objects.")
   public static final String VIEW_NOT_AVAILABLE = "26";

   /**
    * ProdMgmt specific messages
    * 
    **/
   @RBEntry("No qualifying version found for {0}.")
   public static final String NO_QUALIFIED_VERSION = "100";

   @RBEntry("Please enter a name for the replicaton unit.")
   public static final String NO_NAME_ENTERED = "104";

   @RBEntry("Please enter a number for the replication unit.")
   public static final String NO_NUMBER_ENTERED = "105";

   @RBEntry("Please enter a folder location for the replication unit.")
   public static final String NO_LOCATION_ENTERED = "106";

   @RBEntry("The folder location is not valid.")
   public static final String LOCATION_NOT_VALID = "108";

   @RBEntry("Required fields missing")
   public static final String REQUIRED_FIELDS_MISSING = "109";

   @RBEntry("Saving...")
   public static final String SAVING = "110";

   @RBEntry("Save of replication unit {0} failed.")
   public static final String SAVE_UNIT_FAILURE = "116";

   @RBEntry("invalid mode")
   public static final String INVALID_MODE = "115";

   @RBEntry(" {0} now references {1}")
   public static final String NOW_REFERENCES = "117";

   @RBEntry(" {0} now uses {1} of {2}")
   public static final String NOW_USES = "118";

   @RBEntry(" {0} references {1}")
   public static final String REFERENCES = "120";

   @RBEntry(" {0} uses {1}")
   public static final String USES = "119";

   @RBEntry(" Working ")
   public static final String WORKING = "121";

   @RBEntry(" {0} does not use {1} anymore")
   public static final String DOESNT_USE = "122";

   @RBEntry("The given object must be a WTUnit in order to initiate this task. ")
   public static final String OBJECT_NOT_WTUNIT = "125";

   @RBEntry("Could not get PropertyDescriptor")
   public static final String NO_PROPERTY_DESCRIPTOR = "126";

   @RBEntry("Could not retrieve ClassInfo")
   public static final String NO_CLASSINFO = "127";

   @RBEntry("wt.part.Source")
   @RBPseudo(false)
   public static final String WT_PART_SOURCE_CLASSNAME = "150";

   @RBEntry("wt.part.PartType")
   @RBPseudo(false)
   public static final String WT_PART_TYPE_CLASSNAME = "151";

   @RBEntry("wt.doc.WTDocumentMaster")
   @RBPseudo(false)
   public static final String WT_DOC_MASTER_CLASSNAME = "152";

   @RBEntry("The Baseline name \"{0}\" is not unique.")
   public static final String BASELINE_NAME_NOT_UNIQUE = "155";

   @RBEntry("The Baseline name \"{0}\" is not valid.")
   public static final String BASELINE_NAME_NOT_VALID = "156";

   @RBEntry("The Configuration Item name \"{0}\" is not unique.")
   public static final String CONFIG_ITEM_NAME_NOT_UNIQUE = "157";

   @RBEntry("The Configuration Item \"{0}\" is not valid.")
   public static final String CONFIG_ITEM_NAME_NOT_VALID = "158";

   @RBEntry("Search for Configuration Item")
   public static final String FIND_CONFIG_ITEM_TITLE = "159";

   @RBEntry("Search for Baseline")
   public static final String FIND_BASELINE_TITLE = "160";

   @RBEntry("Use for this session only")
   public static final String SAVE_CONFIG_SPEC_LABEL = "161";

   @RBEntry("Progress")
   public static final String PROGRESS_TITLE = "162";

   @RBEntry("The usage of {0} cannot be removed because it is described by {1}.")
   public static final String BUILT_LINK_CAN_NOT_BE_DELETED = "163";

   @RBEntry("The {0} cannot be changed because the usage of {1} is described by {2}.")
   public static final String BUILT_LINK_CAN_NOT_BE_UPDATED = "164";

   @RBEntry(" {0} builds {1}.")
   public static final String BUILD_RULE = "165";

   @RBEntry("Finding attributes...")
   public static final String FINDING_ATTRIBUTES_LABEL = "168";

   @RBEntry("A config spec must be created in Product Information Explorer before a replication unit can be created.")
   public static final String MISSING_CONFIG_SPEC = "169";

   @RBEntry("The replication unit has already been exported and cannot be updated.")
   public static final String WTUNIT_ALREADY_EXPORTED = "170";

   @RBEntry("The replication unit has already been received and can not be updated.")
   public static final String WTUNIT_ALREADY_RECEIVED = "171";
}
