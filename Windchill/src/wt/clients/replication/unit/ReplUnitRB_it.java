/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.replication.unit;

import wt.util.resource.*;

@RBUUID("wt.clients.replication.unit.ReplUnitRB")
public final class ReplUnitRB_it extends WTListResourceBundle {
   /**
    * Labels
    * 
    **/
   @RBEntry("Data creazione:")
   public static final String PRIVATE_CONSTANT_0 = "createdLbl";

   @RBEntry("Autore:")
   public static final String PRIVATE_CONSTANT_1 = "createdByLbl";

   @RBEntry("Origine:")
   public static final String PRIVATE_CONSTANT_2 = "sourceLbl";

   @RBEntry("Posizione:")
   public static final String PRIVATE_CONSTANT_3 = "locationLbl";

   @RBEntry("Nome:")
   public static final String PRIVATE_CONSTANT_4 = "nameLbl";

   @RBEntry("Numero:")
   public static final String PRIVATE_CONSTANT_5 = "numberLbl";

   @RBEntry("Stato:")
   public static final String PRIVATE_CONSTANT_6 = "statusLbl";

   @RBEntry("Tipo:")
   public static final String PRIVATE_CONSTANT_7 = "typeLbl";

   @RBEntry("Data ultima modifica:")
   public static final String PRIVATE_CONSTANT_8 = "updatedOnLbl";

   @RBEntry("Autore modifiche:")
   public static final String PRIVATE_CONSTANT_9 = "updatedByLbl";

   @RBEntry("Nuova versione:")
   public static final String PRIVATE_CONSTANT_10 = "revisionLbl";

   @RBEntry("Iterazione:")
   public static final String PRIVATE_CONSTANT_11 = "iterationLbl";

   @RBEntry("Stato del ciclo di vita:")
   public static final String PRIVATE_CONSTANT_12 = "stateLbl";

   @RBEntry("Vista:")
   public static final String PRIVATE_CONSTANT_13 = "viewLbl";

   @RBEntry("Includi le parti nel mio schedario personale")
   public static final String PRIVATE_CONSTANT_14 = "checkedOutLbl";

   @RBEntry("Qtà:")
   public static final String PRIVATE_CONSTANT_15 = "quantityLbl";

   @RBEntry("Unità:")
   public static final String PRIVATE_CONSTANT_16 = "unitLbl";

   @RBEntry("Ricerca componenti in corso...")
   public static final String PRIVATE_CONSTANT_17 = "findingUsesLbl";

   @RBEntry("Ricerca parti in corso...")
   public static final String PRIVATE_CONSTANT_18 = "findingPartsLbl";

   @RBEntry("Ricerca documenti in corso...")
   public static final String PRIVATE_CONSTANT_19 = "findingDocumentsLbl";

   /**
    * Button Labels
    * 
    **/
   @RBEntry("Aggiungi")
   public static final String PRIVATE_CONSTANT_20 = "addButton";

   @RBEntry("Sfoglia...")
   public static final String PRIVATE_CONSTANT_21 = "browseButton";

   @RBEntry("Annulla")
   public static final String PRIVATE_CONSTANT_22 = "cancelButton";

   @RBEntry("Check-In")
   public static final String PRIVATE_CONSTANT_23 = "checkinButton";

   @RBEntry("Check-Out")
   public static final String PRIVATE_CONSTANT_24 = "checkoutButton";

   @RBEntry("Reimposta")
   public static final String PRIVATE_CONSTANT_25 = "clearButton";

   @RBEntry("Chiudi")
   public static final String PRIVATE_CONSTANT_26 = "closeButton";

   @RBEntry("Elimina")
   public static final String PRIVATE_CONSTANT_27 = "deleteButton";

   @RBEntry("Visualizza")
   public static final String PRIVATE_CONSTANT_28 = "getButton";

   @RBEntry("Guida")
   public static final String PRIVATE_CONSTANT_29 = "helpButton";

   @RBEntry("OK")
   public static final String PRIVATE_CONSTANT_30 = "okButton";

   @RBEntry("Riferimenti")
   public static final String PRIVATE_CONSTANT_31 = "referencesButton";

   @RBEntry("Documenti")
   public static final String PRIVATE_CONSTANT_32 = "documentsButton";

   @RBEntry("Contenuto")
   public static final String PRIVATE_CONSTANT_33 = "contentsButton";

   @RBEntry("Componenti")
   public static final String PRIVATE_CONSTANT_34 = "usesButton";

   @RBEntry("Parti")
   public static final String PRIVATE_CONSTANT_35 = "partsButton";

   @RBEntry("Aggiorna")
   public static final String PRIVATE_CONSTANT_36 = "updateButton";

   @RBEntry("Salva")
   public static final String PRIVATE_CONSTANT_37 = "saveButton";

   @RBEntry("Rimuovi")
   public static final String PRIVATE_CONSTANT_38 = "removeButton";

   @RBEntry("Visualizza")
   public static final String PRIVATE_CONSTANT_39 = "viewButton";

   @RBEntry("Mostra versioni")
   public static final String PRIVATE_CONSTANT_40 = "showVersionsButton";

   /**
    * MultiList column headings
    * 
    **/
   @RBEntry("Numero")
   public static final String PRIVATE_CONSTANT_41 = "numberHeading";

   @RBEntry("Nome")
   public static final String PRIVATE_CONSTANT_42 = "nameHeading";

   @RBEntry("Qtà")
   public static final String PRIVATE_CONSTANT_43 = "quantityHeading";

   @RBEntry("Unità")
   public static final String PRIVATE_CONSTANT_44 = "unitHeading";

   @RBEntry("Descritto con")
   public static final String PRIVATE_CONSTANT_45 = "managedByHeading";

   @RBEntry("Descritto con")
   public static final String PRIVATE_CONSTANT_46 = "buildRuleButton";

   @RBEntry("Ricerca di Descritto con in corso...")
   public static final String PRIVATE_CONSTANT_47 = "findingBuildRuleLbl";

   @RBEntry("Descritto con:")
   public static final String PRIVATE_CONSTANT_48 = "buildRuleTitle";

   @RBEntry("Proprietario: {0}")
   public static final String PRIVATE_CONSTANT_49 = "ownedBy";

   @RBEntry("Dimensione")
   public static final String PRIVATE_CONSTANT_50 = "sizeHeading";

   @RBEntry("Stato")
   public static final String PRIVATE_CONSTANT_51 = "statusHeading";

   @RBEntry("Versione")
   public static final String PRIVATE_CONSTANT_52 = "versionHeading";

   @RBEntry("Tipo")
   public static final String PRIVATE_CONSTANT_53 = "typeHeading";

   @RBEntry("ID")
   public static final String PRIVATE_CONSTANT_54 = "idHeading";

   @RBEntry("Identificativo")
   public static final String PRIVATE_CONSTANT_55 = "identityHeading";

   @RBEntry("Revisione")
   public static final String PRIVATE_CONSTANT_56 = "revisionHeading";

   @RBEntry("Stato del ciclo di vita")
   public static final String PRIVATE_CONSTANT_57 = "stateHeading";

   @RBEntry("Vista")
   public static final String PRIVATE_CONSTANT_58 = "viewHeading";

   @RBEntry("*")
   @RBPseudo(false)
   @RBComment("Symbols")
   public static final String PRIVATE_CONSTANT_59 = "required";

   /**
    * Titles
    * 
    **/
   @RBEntry("Componenti:")
   public static final String PRIVATE_CONSTANT_60 = "usesTitle";

   @RBEntry("Parti:")
   public static final String PRIVATE_CONSTANT_61 = "partsTitle";

   @RBEntry("Impiegato da:")
   public static final String PRIVATE_CONSTANT_62 = "usedByTitle";

   @RBEntry("Contenuto:")
   public static final String PRIVATE_CONSTANT_63 = "contentsTitle";

   @RBEntry("Riferimenti:")
   public static final String PRIVATE_CONSTANT_64 = "referencesTitle";

   @RBEntry("Documenti:")
   public static final String PRIVATE_CONSTANT_65 = "documentsTitle";

   @RBEntry("Imposta specifica di configurazione")
   public static final String PRIVATE_CONSTANT_66 = "configSpecTitle";

   @RBEntry("Crea unità di replica {0}")
   public static final String PRIVATE_CONSTANT_67 = "createWTUnitTitle";

   @RBEntry("Aggiorna unità di replica <{0}>")
   public static final String PRIVATE_CONSTANT_68 = "updateWTUnitTitle";

   @RBEntry("Copia in modifica di {0}")
   public static final String PRIVATE_CONSTANT_69 = "workingCopyTitle";

   @RBEntry("Trova parte")
   public static final String PRIVATE_CONSTANT_70 = "findPartTitle";

   @RBEntry("Trova documento")
   public static final String PRIVATE_CONSTANT_71 = "findDocTitle";

   @RBEntry("In stato di Check-In")
   public static final String PRIVATE_CONSTANT_72 = "checkedIn";

   @RBEntry("Sottoposto a Check-Out")
   public static final String PRIVATE_CONSTANT_73 = "checkedOut";

   @RBEntry("Check-Out effettuato da {0}")
   public static final String PRIVATE_CONSTANT_74 = "checkedOutBy";

   @RBEntry("Copia in modifica")
   public static final String PRIVATE_CONSTANT_75 = "workingCopy";

   @RBEntry("Standard")
   public static final String PRIVATE_CONSTANT_76 = "standardSectionLbl";

   @RBEntry("Baseline")
   public static final String PRIVATE_CONSTANT_77 = "baselineSectionLbl";

   @RBEntry("Effettività")
   public static final String PRIVATE_CONSTANT_78 = "effectivitySectionLbl";

   @RBEntry("Data di effettività:")
   public static final String PRIVATE_CONSTANT_79 = "effectiveDateLbl";

   @RBEntry("Configuration item:")
   public static final String PRIVATE_CONSTANT_80 = "configurationItemLbl";

   @RBEntry("Valore:")
   public static final String PRIVATE_CONSTANT_81 = "valueLbl";

   @RBEntry("Cerca...")
   public static final String PRIVATE_CONSTANT_82 = "searchButton";

   @RBEntry("Trova...")
   public static final String PRIVATE_CONSTANT_83 = "findButton";

   /**
    * Messages
    * 
    **/
   @RBEntry("Errore durante l'avvio del task di visualizzazione per {0}:  {1}")
   public static final String INITIATE_VIEW_FAILED = "5";

   @RBEntry("Tutte le iterazioni di questa versione di \"{0}\" verranno eliminate. Eliminare \"{0}\"?")
   public static final String CONFIRM_DELETE_VERSIONS = "9";

   @RBEntry("Al momento, il Navigatore Windchill non supporta la visualizzazione degli oggetti {0}.")
   public static final String VIEW_NOT_AVAILABLE = "26";

   /**
    * ProdMgmt specific messages
    * 
    **/
   @RBEntry("Non è stata trovata alcuna versione qualificante di {0}.")
   public static final String NO_QUALIFIED_VERSION = "100";

   @RBEntry("Immettere un nome per l'unità di replica.")
   public static final String NO_NAME_ENTERED = "104";

   @RBEntry("Immettere un numero per l'unità di replica.")
   public static final String NO_NUMBER_ENTERED = "105";

   @RBEntry("Immettere una posizione di cartella per l'unità di replica.")
   public static final String NO_LOCATION_ENTERED = "106";

   @RBEntry("La posizione della cartella non è valida.")
   public static final String LOCATION_NOT_VALID = "108";

   @RBEntry("Campi obbligatori mancanti")
   public static final String REQUIRED_FIELDS_MISSING = "109";

   @RBEntry("Salvataggio in corso...")
   public static final String SAVING = "110";

   @RBEntry("Il salvataggio dell'unità di replica {0} è fallita.")
   public static final String SAVE_UNIT_FAILURE = "116";

   @RBEntry("modo non valido")
   public static final String INVALID_MODE = "115";

   @RBEntry("{0} fa ora riferimento a {1}")
   public static final String NOW_REFERENCES = "117";

   @RBEntry("{0} utilizza {1} di {2}")
   public static final String NOW_USES = "118";

   @RBEntry("{0} fa riferimento a {1}")
   public static final String REFERENCES = "120";

   @RBEntry("{0} utilizza {1}")
   public static final String USES = "119";

   @RBEntry(" Operazione in corso ")
   public static final String WORKING = "121";

   @RBEntry("{0} non utilizza più {1}")
   public static final String DOESNT_USE = "122";

   @RBEntry("È necessario che l'oggetto sia una WTUnit perché sia possibile l'avvio del task.")
   public static final String OBJECT_NOT_WTUNIT = "125";

   @RBEntry("Impossibile ottenere PropertyDescriptor")
   public static final String NO_PROPERTY_DESCRIPTOR = "126";

   @RBEntry("Impossibile recuperare ClassInfo")
   public static final String NO_CLASSINFO = "127";

   @RBEntry("wt.part.Source")
   @RBPseudo(false)
   public static final String WT_PART_SOURCE_CLASSNAME = "150";

   @RBEntry("wt.part.PartType")
   @RBPseudo(false)
   public static final String WT_PART_TYPE_CLASSNAME = "151";

   @RBEntry("wt.doc.WTDocumentMaster")
   @RBPseudo(false)
   public static final String WT_DOC_MASTER_CLASSNAME = "152";

   @RBEntry("Il nome della baseline \"{0}\" non è univoco.")
   public static final String BASELINE_NAME_NOT_UNIQUE = "155";

   @RBEntry("Il nome della baseline \"{0}\" non è valido.")
   public static final String BASELINE_NAME_NOT_VALID = "156";

   @RBEntry("Il nome del configuration item \"{0}\" non è univoco.")
   public static final String CONFIG_ITEM_NAME_NOT_UNIQUE = "157";

   @RBEntry("Il configuration item \"{0}\" non è valido.")
   public static final String CONFIG_ITEM_NAME_NOT_VALID = "158";

   @RBEntry("Cerca configuration item")
   public static final String FIND_CONFIG_ITEM_TITLE = "159";

   @RBEntry("Cerca baseline")
   public static final String FIND_BASELINE_TITLE = "160";

   @RBEntry("Usa solo per questa sessione")
   public static final String SAVE_CONFIG_SPEC_LABEL = "161";

   @RBEntry("Avanzamento")
   public static final String PROGRESS_TITLE = "162";

   @RBEntry("L'utilizzo di {0} non può essere rimosso perché è descritto da {1}.")
   public static final String BUILT_LINK_CAN_NOT_BE_DELETED = "163";

   @RBEntry("{0} non può essere modificato perché l'utilizzo di {1} è descritto da {2}.")
   public static final String BUILT_LINK_CAN_NOT_BE_UPDATED = "164";

   @RBEntry("{0} costruisce {1}.")
   public static final String BUILD_RULE = "165";

   @RBEntry("Ricerca di attributi in corso...")
   public static final String FINDING_ATTRIBUTES_LABEL = "168";

   @RBEntry("La specifica di configurazione deve essere creata nel Navigatore dati di prodotto prima della creazione dell'unità di replica.")
   public static final String MISSING_CONFIG_SPEC = "169";

   @RBEntry("L'unità di replica è già stata esportata e non può essere aggiornata.")
   public static final String WTUNIT_ALREADY_EXPORTED = "170";

   @RBEntry("L'unità di replica è già stata ricevuta e non può essere aggiornata.")
   public static final String WTUNIT_ALREADY_RECEIVED = "171";
}
