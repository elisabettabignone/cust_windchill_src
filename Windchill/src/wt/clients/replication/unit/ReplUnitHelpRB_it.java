/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.replication.unit;

import wt.util.resource.*;

@RBUUID("wt.clients.replication.unit.ReplUnitHelpRB")
public final class ReplUnitHelpRB_it extends WTListResourceBundle {
   /**
    * -----UnitTask-----
    **/
   @RBEntry("Nome dell'unità di replica")
   public static final String PRIVATE_CONSTANT_0 = "Desc/replunit/UnitTask/Name";

   @RBEntry("Numero dell'unità di replica")
   public static final String PRIVATE_CONSTANT_1 = "Desc/replunit/UnitTask/Number";

   @RBEntry("Posizione dell'unità di replica")
   public static final String PRIVATE_CONSTANT_2 = "Desc/replunit/UnitTask/Location";

   @RBEntry("Iterazione dell'unità di replica")
   public static final String PRIVATE_CONSTANT_3 = "Desc/replunit/UnitTask/IterationIdentifier";

   @RBEntry("Stato dell'unità di replica")
   public static final String PRIVATE_CONSTANT_4 = "Desc/replunit/UnitTask/State";

   @RBEntry("Stato dell'unità di replica")
   public static final String PRIVATE_CONSTANT_5 = "Desc/replunit/UnitTask/StatusText";

   @RBEntry("Data di creazione della parte")
   public static final String PRIVATE_CONSTANT_6 = "Desc/replunit/UnitTask/CreationDate";

   @RBEntry("Data di modifica della parte")
   public static final String PRIVATE_CONSTANT_7 = "Desc/replunit/UnitTask/LastUpdated";

   @RBEntry("Persona che ha creato la parte")
   public static final String PRIVATE_CONSTANT_8 = "Desc/replunit/UnitTask/CreatedByPersonName";

   @RBEntry("Persona che ha apportato l'ultima modifica")
   public static final String PRIVATE_CONSTANT_9 = "Desc/replunit/UnitTask/ModifiedByPersonName";

   @RBEntry("Origine della parte")
   public static final String PRIVATE_CONSTANT_10 = "Desc/replunit/UnitTask/Source";

   @RBEntry("Nome della vista della parte")
   public static final String PRIVATE_CONSTANT_11 = "Desc/replunit/UnitTask/View";

   @RBEntry("Nome della vista della parte")
   public static final String PRIVATE_CONSTANT_12 = "Desc/replunit/UnitTask/ViewName";

   @RBEntry("Fare clic per visualizzare il contenuto della parte")
   public static final String PRIVATE_CONSTANT_13 = "Desc/replunit/UnitTask/Contents";

   @RBEntry("Fare clic per vedere le parti dell'unità di replica")
   public static final String PRIVATE_CONSTANT_14 = "Desc/replunit/UnitTask/Uses";

   @RBEntry("Fare clic per vedere l'effettività associata alla parte")
   public static final String PRIVATE_CONSTANT_15 = "Desc/replunit/UnitTask/Effectivity";

   @RBEntry("Consente di eseguire la ricerca di una cartella")
   public static final String PRIVATE_CONSTANT_16 = "Desc/replunit/UnitTask/Browse";

   @RBEntry("Fare clic per vedere documenti dell'unità di replica")
   public static final String PRIVATE_CONSTANT_17 = "Desc/replunit/UnitTask/References";

   @RBEntry("Salva le modifiche di questa unità di replica e chiude la finestra")
   public static final String PRIVATE_CONSTANT_18 = "Desc/replunit/UnitTask/OK";

   @RBEntry("Salva le modifiche di questa unità di replica")
   public static final String PRIVATE_CONSTANT_19 = "Desc/replunit/UnitTask/Save";

   @RBEntry("Chiude la finestra senza salvare le modifiche di questa unità di replica")
   public static final String PRIVATE_CONSTANT_20 = "Desc/replunit/UnitTask/Cancel";

   @RBEntry("Visualizza le informazioni della guida relative al task")
   public static final String PRIVATE_CONSTANT_21 = "Desc/replunit/UnitTask/Help";

   @RBEntry("L'elenco di parti contenute da questa unità di replica")
   public static final String PRIVATE_CONSTANT_22 = "Desc/replunit/UnitTask/UsesList";

   @RBEntry("La quantità usata per la parte selezionata")
   public static final String PRIVATE_CONSTANT_23 = "Desc/replunit/UnitTask/UsesQuantity";

   @RBEntry("Visualizza la parte selezionata")
   public static final String PRIVATE_CONSTANT_24 = "Desc/replunit/UnitTask/UsesView";

   @RBEntry("Aggiunge una parte all'unità di replica")
   public static final String PRIVATE_CONSTANT_25 = "Desc/replunit/UnitTask/UsesAdd";

   @RBEntry("Rimuove la parte selezionata dall'unità di replica")
   public static final String PRIVATE_CONSTANT_26 = "Desc/replunit/UnitTask/UsesRemove";

   @RBEntry("L'elenco di documenti contenuti da questa unità di replica")
   public static final String PRIVATE_CONSTANT_27 = "Desc/replunit/UnitTask/ReferencesList";

   @RBEntry("Mostra le versioni del documento selezionato")
   public static final String PRIVATE_CONSTANT_28 = "Desc/replunit/UnitTask/ReferencesShowVersions";

   @RBEntry("Aggiunge un riferimento al documento")
   public static final String PRIVATE_CONSTANT_29 = "Desc/replunit/UnitTask/ReferencesAdd";

   @RBEntry("Rimuove il documento selezionato dall'unità di replica")
   public static final String PRIVATE_CONSTANT_30 = "Desc/replunit/UnitTask/ReferencesRemove";

   @RBEntry("Fare clic per attivare la qualificazione della specifica di configurazione della baseline.")
   public static final String PRIVATE_CONSTANT_31 = "Desc/replunit/UnitTask/BaselineRadioButton";

   @RBEntry("Fare clic per attivare la qualificazione della specifica di configurazione dell'effettività.")
   public static final String PRIVATE_CONSTANT_32 = "Desc/replunit/UnitTask/EffectivityRadioButton";

   @RBEntry("Seleziona una vista")
   public static final String PRIVATE_CONSTANT_33 = "Desc/replunit/UnitTask/StandardView";

   @RBEntry("Seleziona uno stato del ciclo di vita")
   public static final String PRIVATE_CONSTANT_34 = "Desc/replunit/UnitTask/StandardLifeCycleState";

   @RBEntry("Baseline correntemente specificata")
   public static final String PRIVATE_CONSTANT_35 = "Desc/replunit/UnitTask/BaselineName";

   @RBEntry("Sceglie una baseline.")
   public static final String PRIVATE_CONSTANT_36 = "Desc/replunit/UnitTask/BaselineSearch";

   @RBEntry("Seleziona una vista collegata all'effettività")
   public static final String PRIVATE_CONSTANT_37 = "Desc/replunit/UnitTask/EffectivityView";

   @RBEntry("Immettere la data di effettività.")
   public static final String PRIVATE_CONSTANT_38 = "Desc/replunit/UnitTask/EffectiveDate";

   @RBEntry("Immettere il valore del configuration item.")
   public static final String PRIVATE_CONSTANT_39 = "Desc/replunit/UnitTask/Value";

   @RBEntry("Configuration item correntemente specificato.")
   public static final String PRIVATE_CONSTANT_40 = "Desc/replunit/UnitTask/ConfigurationItem";

   @RBEntry("Scegliere un configuration item.")
   public static final String PRIVATE_CONSTANT_41 = "Desc/replunit/UnitTask/ConfigurationItemSearch";

   @RBEntry("Fare clic per vedere gli oggetti che descrivono la parte")
   public static final String PRIVATE_CONSTANT_42 = "Desc/replunit/UnitTask/BuildRule";

   @RBEntry("Elenco degli oggetti che descrivono la parte")
   public static final String PRIVATE_CONSTANT_43 = "Desc/replunit/UnitTask/BuildRuleList";

   @RBEntry("Visualizza l'oggetto selezionato")
   public static final String PRIVATE_CONSTANT_44 = "Desc/replunit/UnitTask/BuildRuleView";

   @RBEntry("ProductReplicationCreatUnit")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_45 = "Help/replunit/CreateUnitTask";

   @RBEntry("ProductReplicationUpdatUnit")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_46 = "Help/replunit/UpdateUnitTask";

   @RBEntry("Avvia un nuovo Navigatore dati di prodotto")
   public static final String PRIVATE_CONSTANT_47 = "Tip/replunit/PartExplorerTask/pexplr";
}
