/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.gui;

import wt.util.resource.*;

@RBUUID("wt.clients.gui.GuiRB")
public final class GuiRB_it extends WTListResourceBundle {
   /**
    * FIELD LABELS -----------------------------------------------------------
    **/
   @RBEntry("Schedario di consultazione:")
   public static final String PRIVATE_CONSTANT_0 = "browseLbl";

   @RBEntry("Etichetta:")
   public static final String PRIVATE_CONSTANT_1 = "genericLbl";

   @RBEntry("Posizione:")
   public static final String PRIVATE_CONSTANT_2 = "locationLbl";

   @RBEntry("Nome:")
   public static final String PRIVATE_CONSTANT_3 = "nameLbl";

   @RBEntry("Numero:")
   public static final String PRIVATE_CONSTANT_4 = "numberLbl";

   /**
    * Labels for Container Chooser ----------------------------------
    **/
   @RBEntry("Mostra:")
   public static final String PRIVATE_CONSTANT_5 = "showLabel";

   @RBEntry("Repository")
   public static final String PRIVATE_CONSTANT_6 = "repositoriesLabel";

   @RBEntry("Libreria")
   public static final String PRIVATE_CONSTANT_7 = "repositoryLabel";

   @RBEntry("Prodotti")
   public static final String PRIVATE_CONSTANT_8 = "productsLabel";

   @RBEntry("Prodotto")
   public static final String PRIVATE_CONSTANT_9 = "productLabel";

   @RBEntry("Guida")
   public static final String PRIVATE_CONSTANT_10 = "helpButtonLabel";

   @RBEntry("Scegliere prodotto o repository")
   public static final String PRIVATE_CONSTANT_11 = "chooseProductRepositoryDialogTitle";

   /**
    * Help for Container Chooser --------------------------------------
    **/
   @RBEntry("PIMProductRepositoryChoose")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_12 = "Help/gui/ContainerChooser";

   /**
    * BUTTON LABELS ----------------------------------------------------------
    **/
   @RBEntry("OK")
   public static final String PRIVATE_CONSTANT_13 = "okButtonLbl";

   @RBEntry("Annulla")
   public static final String PRIVATE_CONSTANT_14 = "cancelButtonLbl";

   @RBEntry("Sì")
   public static final String PRIVATE_CONSTANT_15 = "yesButtonLbl";

   @RBEntry("No")
   public static final String PRIVATE_CONSTANT_16 = "noButtonLbl";

   @RBEntry("Mostra dettagli")
   public static final String DETAILS_BUTTON_LABEL = "16";

   /**
    * TITLES -------------------------------------------------------------------
    **/
   @RBEntry("Sfoglia cartelle")
   public static final String PRIVATE_CONSTANT_17 = "browseFoldersTitle";

   @RBEntry("Rinomina <{0}>")
   public static final String PRIVATE_CONSTANT_18 = "changeIdentityObjTitle";

   @RBEntry("Rinomina")
   public static final String PRIVATE_CONSTANT_19 = "changeIdentityTitle";

   @RBEntry("Avviso")
   public static final String PRIVATE_CONSTANT_20 = "noticeTitle";

   @RBEntry("Traccia stack per eccezione {0}")
   public static final String STACK_TRACE_TITLE = "17";

   @RBEntry("Seleziona oggetti")
   public static final String SELECT_OBJECTS_TITLE = "18";

   @RBEntry("Seleziona utente")
   public static final String SELECT_USER_TITLE = "19";

   /**
    * STATUS MESSAGES ----------------------------------------------------------
    **/
   @RBEntry("Inizializzazione schedari in corso...")
   public static final String PRIVATE_CONSTANT_21 = "initializingCabinets";

   @RBEntry("Check-Out del contenuto di {0} in corso...")
   public static final String PRIVATE_CONSTANT_22 = "retrievingContents";

   /**
    * ERROR MESSAGES -----------------------------------------------------------
    **/
   @RBEntry("Fornire un valore per \"{0}\".")
   public static final String NULL_ATTRIBUTE_VALUE = "0";

   @RBEntry("{0} non possiede alcun attributo '{1}'.")
   public static final String ATTRIBUTE_NOT_FOUND = "1";

   @RBEntry("Si è verificato un errore durante l'impostazione del valore di '{0}': {1}.")
   public static final String SET_ATTRIBUTE_FAILED = "2";

   @RBEntry("Impossibile trovare il metodo per impostare il valore di '{0}'.")
   public static final String SETTER_METHOD_NOT_FOUND = "3";

   @RBEntry("Si è verificato un errore durante l'impostazione del valore di '{0}' su '{1}'.")
   public static final String SETTER_METHOD_FAILED = "4";

   @RBEntry("Impossibile trovare il metodo per ottenere il valore di '{0}'.")
   public static final String GETTER_METHOD_NOT_FOUND = "5";

   @RBEntry("Si è verificato un errore durante la lettura del valore di '{0}' per '{1}'. ")
   public static final String GETTER_METHOD_FAILED = "6";

   @RBEntry("Nessun attributo identificativo modificabile per l'oggetto {0}")
   public static final String NO_IDENTITY_ATTRIBUTES = "7";

   @RBEntry("Nessuna modifica apportata all'identificativo di {0}.  Utilizzare 'Annulla' per chiudere la finestra di dialogo senza apportare modifiche.")
   public static final String NO_CHANGES_MADE = "8";

   @RBEntry("\"{0}\" non è un valore valido per la proprietà \"{1}\": {2}.")
   public static final String PROPERTY_VETO_EXCEPTION = "9";

   @RBEntry("Si è verificato un errore durante la localizzazione della finestra di dialogo \"{0}\".  \"{1}\" non è una chiave valida nell'insieme di risorse, \"{2}\".  Contattare il personale di assistenza.")
   public static final String RESOURCE_BUNDLE_ERROR = "10";

   @RBEntry("Errore durante l'inizializzazione della cache di icone: {0}.")
   public static final String INIT_ICONCACHE_FAILED = "11";

   @RBEntry("Errore durante il recupero dell'elenco schedari: {0}.")
   public static final String INIT_CABINETS_FAILED = "12";

   @RBEntry("Errore durante il caricamento dell'immagine '{0}'.  Caricamento dell'immagine non riuscito.")
   public static final String LOAD_IMAGE_FAILED = "13";

   @RBEntry("Errore durante il recupero del contenuto dello schedario {0}: {1}")
   public static final String GET_CABINET_CONTENTS_FAILED = "14";

   @RBEntry("Errore durante la ricerca della cartella padre {0}: {1}")
   public static final String FIND_PARENT_FOLDER_FAILED = "15";

   @RBEntry("Installazione")
   public static final String SETUP_TITLE = "20";

   @RBEntry("L'oggetto \"{0}\" esiste già. Reinserire un valore univoco per la proprietà \"{1}\".")
   public static final String UNIQUE_IDENTITY_EXCEPTION = "21";

   @RBEntry("Cerca")
   public static final String SEARCH_LABEL = "22";

   @RBEntry("Nome")
   public static final String NAME_LABEL = "23";

   @RBEntry("Numero")
   public static final String NUMBER_LABEL = "24";

   @RBEntry("Nome organizzazione")
   public static final String ORGANIZATION_LABEL = "25";

   @RBEntry("Criteri di ricerca")
   public static final String SEARCH_CRITERIA_LABEL = "26";

   @RBEntry("Reimposta")
   public static final String CLEAR_LABEL = "27";

   @RBEntry("Risultati")
   public static final String RESULTS_LABEL = "29";

   @RBEntry("Trova contesto")
   public static final String FIND_CONTEXT_LABEL = "30";

   @RBEntry("Trova")
   public static final String FIND_LABEL = "31";

   @RBEntry("Contesto")
   public static final String CONTEXT_LABEL = "32";

   @RBEntry("Tipo contesto")
   public static final String CONTEXT_TYPE_LABEL = "33";

   @RBEntry("ID organizzazione")
   public static final String ORGANIZATION_ID_LABEL = "34";

   @RBEntry("Tutti")
   public static final String ALL_LABEL = "35";

   @RBEntry("Chiave di ricerca")
   public static final String SEARCH_ON_LABEL = "36";

   @RBEntry("OK")
   public static final String OK_BUTTON_LABEL = "37";
}
