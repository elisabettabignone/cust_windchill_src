/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.gui;

import wt.util.resource.*;

@RBUUID("wt.clients.gui.GuiRB")
public final class GuiRB extends WTListResourceBundle {
   /**
    * FIELD LABELS -----------------------------------------------------------
    **/
   @RBEntry("Look In Cabinet:")
   public static final String PRIVATE_CONSTANT_0 = "browseLbl";

   @RBEntry("Label:")
   public static final String PRIVATE_CONSTANT_1 = "genericLbl";

   @RBEntry("Location:")
   public static final String PRIVATE_CONSTANT_2 = "locationLbl";

   @RBEntry("Name:")
   public static final String PRIVATE_CONSTANT_3 = "nameLbl";

   @RBEntry("Number:")
   public static final String PRIVATE_CONSTANT_4 = "numberLbl";

   /**
    * Labels for Container Chooser ----------------------------------
    **/
   @RBEntry("Show:")
   public static final String PRIVATE_CONSTANT_5 = "showLabel";

   @RBEntry("Repositories")
   public static final String PRIVATE_CONSTANT_6 = "repositoriesLabel";

   @RBEntry("Library")
   public static final String PRIVATE_CONSTANT_7 = "repositoryLabel";

   @RBEntry("Products")
   public static final String PRIVATE_CONSTANT_8 = "productsLabel";

   @RBEntry("Product")
   public static final String PRIVATE_CONSTANT_9 = "productLabel";

   @RBEntry("Help")
   public static final String PRIVATE_CONSTANT_10 = "helpButtonLabel";

   @RBEntry("Choose a Product or Repository")
   public static final String PRIVATE_CONSTANT_11 = "chooseProductRepositoryDialogTitle";

   /**
    * Help for Container Chooser --------------------------------------
    **/
   @RBEntry("PIMProductRepositoryChoose")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_12 = "Help/gui/ContainerChooser";

   /**
    * BUTTON LABELS ----------------------------------------------------------
    **/
   @RBEntry("OK")
   public static final String PRIVATE_CONSTANT_13 = "okButtonLbl";

   @RBEntry("Cancel")
   public static final String PRIVATE_CONSTANT_14 = "cancelButtonLbl";

   @RBEntry("Yes")
   public static final String PRIVATE_CONSTANT_15 = "yesButtonLbl";

   @RBEntry("No")
   public static final String PRIVATE_CONSTANT_16 = "noButtonLbl";

   @RBEntry("Show Details")
   public static final String DETAILS_BUTTON_LABEL = "16";

   /**
    * TITLES -------------------------------------------------------------------
    **/
   @RBEntry("Browse Folders")
   public static final String PRIVATE_CONSTANT_17 = "browseFoldersTitle";

   @RBEntry("Rename <{0}>")
   public static final String PRIVATE_CONSTANT_18 = "changeIdentityObjTitle";

   @RBEntry("Rename")
   public static final String PRIVATE_CONSTANT_19 = "changeIdentityTitle";

   @RBEntry("Notice")
   public static final String PRIVATE_CONSTANT_20 = "noticeTitle";

   @RBEntry("Stack Trace for Exception {0}")
   public static final String STACK_TRACE_TITLE = "17";

   @RBEntry("Select Objects")
   public static final String SELECT_OBJECTS_TITLE = "18";

   @RBEntry("Select a User")
   public static final String SELECT_USER_TITLE = "19";

   /**
    * STATUS MESSAGES ----------------------------------------------------------
    **/
   @RBEntry("Initializing Cabinets...")
   public static final String PRIVATE_CONSTANT_21 = "initializingCabinets";

   @RBEntry("Getting contents of {0}...")
   public static final String PRIVATE_CONSTANT_22 = "retrievingContents";

   /**
    * ERROR MESSAGES -----------------------------------------------------------
    **/
   @RBEntry("Please provide a value for \"{0}\".")
   public static final String NULL_ATTRIBUTE_VALUE = "0";

   @RBEntry("{0} does not have a '{1}' attribute.")
   public static final String ATTRIBUTE_NOT_FOUND = "1";

   @RBEntry("An error occurred while trying to set the value of '{0}': {1}.")
   public static final String SET_ATTRIBUTE_FAILED = "2";

   @RBEntry("The method to set the value of '{0}' was not found.")
   public static final String SETTER_METHOD_NOT_FOUND = "3";

   @RBEntry("An error occurred while trying to set the value of '{0}' on '{1}' ")
   public static final String SETTER_METHOD_FAILED = "4";

   @RBEntry("The method to get the value of '{0}' was not found.")
   public static final String GETTER_METHOD_NOT_FOUND = "5";

   @RBEntry("An error occurred while trying to get the value of '{0}' for '{1}' ")
   public static final String GETTER_METHOD_FAILED = "6";

   @RBEntry("No changeable identity attributes were found for object {0}")
   public static final String NO_IDENTITY_ATTRIBUTES = "7";

   @RBEntry("No changes have been made to the identity of {0}.  Please use 'Cancel' to close this dialog without changes.")
   public static final String NO_CHANGES_MADE = "8";

   @RBEntry("\"{0}\" is not a valid value for property \"{1}\": {2}.")
   public static final String PROPERTY_VETO_EXCEPTION = "9";

   @RBEntry("An error occurred while localizing the \"{0}\" dialog.  The key, \"{1}\", is not a valid key in the resource bundle, \"{2}\".  Please contact your support staff.")
   public static final String RESOURCE_BUNDLE_ERROR = "10";

   @RBEntry("The following error occurred while trying to initialize the cache of icons: {0}.")
   public static final String INIT_ICONCACHE_FAILED = "11";

   @RBEntry("The following error occurred while trying to retrieve the list of cabinets: {0}.")
   public static final String INIT_CABINETS_FAILED = "12";

   @RBEntry("An error occurred while trying to load the image '{0}'.  The image was not loaded successfully.")
   public static final String LOAD_IMAGE_FAILED = "13";

   @RBEntry("The following error occurred while trying to retrieve the contents of cabinet {0}: {1}")
   public static final String GET_CABINET_CONTENTS_FAILED = "14";

   @RBEntry("The following error occurred while trying to find the parent of folder {0}: {1}")
   public static final String FIND_PARENT_FOLDER_FAILED = "15";

   @RBEntry("Setup")
   public static final String SETUP_TITLE = "20";

   @RBEntry("Object \"{0}\" already exists. Please re-enter a unique value for property \"{1}\".")
   public static final String UNIQUE_IDENTITY_EXCEPTION = "21";

   @RBEntry("Search")
   public static final String SEARCH_LABEL = "22";

   @RBEntry("Name")
   public static final String NAME_LABEL = "23";

   @RBEntry("Number")
   public static final String NUMBER_LABEL = "24";

   @RBEntry("Organization Name")
   public static final String ORGANIZATION_LABEL = "25";

   @RBEntry("Search Criteria")
   public static final String SEARCH_CRITERIA_LABEL = "26";

   @RBEntry("Clear")
   public static final String CLEAR_LABEL = "27";

   @RBEntry("Results")
   public static final String RESULTS_LABEL = "29";

   @RBEntry("Find Context")
   public static final String FIND_CONTEXT_LABEL = "30";

   @RBEntry("Find")
   public static final String FIND_LABEL = "31";

   @RBEntry("Context")
   public static final String CONTEXT_LABEL = "32";

   @RBEntry("Context Type")
   public static final String CONTEXT_TYPE_LABEL = "33";

   @RBEntry("Organization ID")
   public static final String ORGANIZATION_ID_LABEL = "34";

   @RBEntry("All")
   public static final String ALL_LABEL = "35";

   @RBEntry("Search On")
   public static final String SEARCH_ON_LABEL = "36";

   @RBEntry("OK")
   public static final String OK_BUTTON_LABEL = "37";
}
