/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.filevault;

import wt.util.resource.*;

@RBUUID("wt.clients.filevault.FvHelpRB")
public final class FvHelpRB_it extends WTListResourceBundle {
   @RBEntry("WCAdminDomainPol")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_0 = "Help/Administrator/Fv";

   @RBEntry("Recupera le regole di archiviazione file per questo dominio")
   public static final String PRIVATE_CONSTANT_1 = "Desc/Administrator/Fv/FvRetrieve";

   @RBEntry("Definisce una nuova regola di archiviazione file")
   public static final String PRIVATE_CONSTANT_2 = "Desc/Administrator/Fv/FvCreate";

   @RBEntry("Modifica la regola di archiviazione file selezionata")
   public static final String PRIVATE_CONSTANT_3 = "Desc/Administrator/Fv/FvUpdate";

   @RBEntry("Elimina la regola di archiviazione file selezionata")
   public static final String PRIVATE_CONSTANT_4 = "Desc/Administrator/Fv/FvDelete";

   @RBEntry("Visualizza il browser delle regole di archiviazione")
   public static final String PRIVATE_CONSTANT_5 = "Desc/Administrator/Fv/FvReport";

   @RBEntry("Visualizza le informazioni della guida relative alla finestra")
   public static final String PRIVATE_CONSTANT_6 = "Desc/Administrator/Fv/FvHelp";

   @RBEntry("FileVaultConfigAddVault")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_7 = "Help/Administrator/Vaulting";

   @RBEntry("Classi di oggetti del dominio")
   public static final String PRIVATE_CONSTANT_8 = "Desc/Administrator/Vaulting/Classes";

   @RBEntry("Stato del ciclo di vita")
   public static final String PRIVATE_CONSTANT_9 = "Desc/Administrator/Vaulting/State";

   @RBEntry("Selezionare il sito per filtrare la lista degli archivi")
   public static final String PRIVATE_CONSTANT_10 = "Desc/Administrator/Vaulting/Site";

   @RBEntry("Archivi file")
   public static final String PRIVATE_CONSTANT_11 = "Desc/Administrator/Vaulting/FileVault";

   @RBEntry("Annulla tutte le modifiche e chiude la finestra")
   public static final String PRIVATE_CONSTANT_12 = "Desc/Administrator/Vaulting/Cancel";

   @RBEntry("Salva tutte le modifiche e chiude la finestra")
   public static final String PRIVATE_CONSTANT_13 = "Desc/Administrator/Vaulting/Close";

   @RBEntry("Visualizza le informazioni della guida relative alla creazione delle regole di archiviazione")
   public static final String PRIVATE_CONSTANT_14 = "Desc/Administrator/Vaulting/Help";

   @RBEntry("Chiude la finestra")
   public static final String PRIVATE_CONSTANT_15 = "Desc/Administrator/Domain/Close";

   @RBEntry("vaultreplAbout")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_16 = "Help/Administrator/Repl";

   @RBEntry("Recupera le regole di replica contenuto per questo dominio.")
   public static final String PRIVATE_CONSTANT_17 = "Desc/Administrator/Repl/Retrieve";

   @RBEntry("Definisce una nuova regola di replica di dati.")
   public static final String PRIVATE_CONSTANT_18 = "Desc/Administrator/Repl/Create";

   @RBEntry("Modifica la regola di replica di dati selezionata.")
   public static final String PRIVATE_CONSTANT_19 = "Desc/Administrator/Repl/Update";

   @RBEntry("Elimina la regola di replica di dati selezionata.")
   public static final String PRIVATE_CONSTANT_20 = "Desc/Administrator/Repl/Delete";

   @RBEntry("Visualizza gli elenchi di archivi replica")
   public static final String PRIVATE_CONSTANT_21 = "Desc/Administrator/Repl/Report";

   @RBEntry("Classi di oggetti del dominio")
   public static final String PRIVATE_CONSTANT_22 = "Desc/Administrator/Repl/Classes";

   @RBEntry("Stato del ciclo di vita")
   public static final String PRIVATE_CONSTANT_23 = "Desc/Administrator/Repl/State";

   @RBEntry("Annulla tutte le modifiche e chiude la finestra")
   public static final String PRIVATE_CONSTANT_24 = "Desc/Administrator/Repl/Cancel";

   @RBEntry("Salva tutte le modifiche e chiude la finestra")
   public static final String PRIVATE_CONSTANT_25 = "Desc/Administrator/Repl/Close";

   @RBEntry("Visualizza le informazioni della guida relative alla finestra")
   public static final String PRIVATE_CONSTANT_26 = "Desc/Administrator/Repl/Help";

   @RBEntry("Archivi replica")
   public static final String PRIVATE_CONSTANT_27 = "Desc/Administrator/Repl/RemoteVault";

   @RBEntry("FileVaultWorkAdministrativeDomains")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_28 = "Help/Administrator/WorkAdministrativeDomains";

   @RBEntry("FileVaultCreateVaultingRule")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_29 = "Help/Administrator/CreateVaultingRule";

   @RBEntry("FileVaultViewDomainVaultingRules")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_30 = "Help/Administrator/ViewDomainVaultingRules";

   @RBEntry("FileVaultUpdateDomainVaultingRules")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_31 = "Help/Administrator/UpdateDomainVaultingRules";
   
   @RBEntry("FileVaultUpdateVaultingRule")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_32 = "Help/Administrator/UpdateVaultingRule";
}
