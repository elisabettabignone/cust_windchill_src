/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.filevault.replication;

import wt.util.resource.*;

@RBUUID("wt.clients.filevault.replication.replicaResources")
@RBNameException //Grandfathered by conversion
public final class replicaResources extends WTListResourceBundle {
   @RBEntry("Content Replication")
   public static final String CONTENT_REPL_TAB = "1";

   @RBEntry("Schedule Content Replication")
   public static final String CONTENT_REPL_TITLE = "2";

   @RBEntry("Close")
   public static final String CLOSE_BUTT = "3";

   @RBEntry("Help")
   public static final String HELP_BUTT = "4";

   @RBEntry("Replication Schedule Items")
   public static final String REPLICATION_SCHED_ITEMS = "5";

   /**
    * Scheduling Framework properties
    * 
    **/
   @RBEntry("Content Replication History")
   public static final String PRIVATE_CONSTANT_0 = "schedulelogtitle";

   @RBEntry("History of Content Replication:")
   public static final String PRIVATE_CONSTANT_1 = "schedulelogdescr";

   @RBEntry("Replica Vault:")
   public static final String PRIVATE_CONSTANT_2 = "scheduleTimeObjectLab";

   @RBEntry("Content Replication Scheduler")
   public static final String PRIVATE_CONSTANT_3 = "scheduleTimeTitle";

   @RBEntry("Replica Vault")
   public static final String PRIVATE_CONSTANT_4 = "schedulelogheader";
}
