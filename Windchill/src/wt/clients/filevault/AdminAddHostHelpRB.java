/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.filevault;

import wt.util.resource.*;

@RBUUID("wt.clients.filevault.AdminAddHostHelpRB")
public final class AdminAddHostHelpRB extends WTListResourceBundle {
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_0 = "Contents/filevault";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_1 = "Help/filevault";

   /**
    * Pointer the help for this screen ------------------------------------------
    *
    **/
   @RBEntry("FileVaultConfigAddHost")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/filevault/AdminAddHost";

   @RBEntry("FileVaultConfigAddHost")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_3 = "Help/filevault/PDM_AdminAddHost";

   @RBEntry("FileVaultConfigAddHost")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_4 = "Help/filevault/PJL_AdminAddHost";

   @RBEntry("FileVaultConfigUpdateHost")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_5 = "Help/filevault/AdminUpdateHost";

   @RBEntry("FileVaultConfigAddVault")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_6 = "Help/filevault/PDM_AdminUpdateHost";

   @RBEntry("FileVaultConfigAddVault")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_7 = "Help/filevault/PJL_AdminUpdateHost";

   /**
    * Status bar messages for Add mode ------------------------------------------
    *
    **/
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_8 = "Desc/filevault/AdminAddHost";

   @RBEntry("Click to close the window without saving changes")
   public static final String PRIVATE_CONSTANT_9 = "Desc/filevault/AdminAddHost/Cancel";

   @RBEntry("Click to view the help on adding hosts.")
   public static final String PRIVATE_CONSTANT_10 = "Desc/filevault/AdminAddHost/Help";

   @RBEntry("Enter a unique name for the host")
   public static final String PRIVATE_CONSTANT_11 = "Desc/filevault/AdminAddHost/HostName";

   @RBEntry("Click to save the Host Name and close the window")
   public static final String PRIVATE_CONSTANT_12 = "Desc/filevault/AdminAddHost/OK";

   /**
    * Status bar messages for Update mode ---------------------------------------
    *
    **/
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_13 = "Desc/filevault/AdminUpdateHost";

   @RBEntry("Click to close the window without saving changes")
   public static final String PRIVATE_CONSTANT_14 = "Desc/filevault/AdminUpdateHost/Cancel";

   @RBEntry("Click to view the help on updating hosts")
   public static final String PRIVATE_CONSTANT_15 = "Desc/filevault/AdminUpdateHost/Help";

   @RBEntry("Enter a unique name for the host")
   public static final String PRIVATE_CONSTANT_16 = "Desc/filevault/AdminUpdateHost/HostName";

   @RBEntry("Click to save the Host Name and close the window")
   public static final String PRIVATE_CONSTANT_17 = "Desc/filevault/AdminUpdateHost/OK";
}