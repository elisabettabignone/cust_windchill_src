/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.filevault;

import wt.util.resource.*;

@RBUUID("wt.clients.filevault.filevaultResource")
public final class filevaultResource extends WTListResourceBundle {
   /**
    * Button Labels
    *
    **/
   @RBEntry("OK")
   public static final String OK = "1";

   @RBEntry("Cancel")
   public static final String CANCEL = "2";

   @RBEntry("Help")
   public static final String HELP = "3";

   @RBEntry("Move Top")
   public static final String TOP = "4";

   @RBEntry("Move Up")
   public static final String UP = "5";

   @RBEntry("Move Down")
   public static final String DOWN = "6";

   @RBEntry("Move Bottom")
   public static final String BOTTOM = "7";

   /**
    * Screen Titles
    *
    **/
   @RBEntry("New Host")
   public static final String NEWHOST_TITLE = "8";

   @RBEntry("Update Host")
   public static final String UPDATEHOST_TITLE = "9";

   @RBEntry("New Vault")
   public static final String NEWVAULT_TITLE = "10";

   @RBEntry("Update Vault")
   public static final String UPDATEVAULT_TITLE = "11";

   @RBEntry("New Folder")
   public static final String NEWFOLDER_TITLE = "12";

   @RBEntry("Update Folder")
   public static final String UPDATEFOLDER_TITLE = "13";

   @RBEntry("New Root Folder")
   public static final String NEWROOTFOLDER_TITLE = "428";

   /**
    * Symbols
    *
    **/
   @RBEntry(":")
   @RBPseudo(false)
   public static final String COLON = "14";

   @RBEntry("...")
   @RBPseudo(false)
   public static final String ELLIPSES = "15";

   /**
    * Screen labels
    *
    **/
   @RBEntry("Folder Operations")
   public static final String FOLDER_OPS = "22";

   @RBEntry("Show Only Writable")
   public static final String SHOW_WRITABLE = "28";

   @RBEntry("Show All")
   public static final String SHOW_ALL = "29";

   @RBEntry("Show only existing mounts")
   public static final String SHOW_EXISTING_MOUNTS = "30";

   @RBEntry("%")
   @RBPseudo(false)
   public static final String PERCENT = "31";

   @RBEntry("Folder Type")
   public static final String FOLDER_TYPE = "431";

   /**
    * combo boxes
    *
    **/
   @RBEntry("TB")
   public static final String SIZE_TB = "32";

   @RBEntry("GB")
   public static final String SIZE_GB = "33";

   @RBEntry("MB")
   public static final String SIZE_MB = "34";

   @RBEntry("KB")
   public static final String SIZE_KB = "35";

   /**
    * list headings
    *
    **/
   @RBEntry("Master Folders")
   public static final String FOLDERS = "48";

   @RBEntry("Hosts")
   public static final String HOSTS = "49";

   @RBEntry("Master Vaults")
   public static final String VAULTS = "50";

   @RBEntry("Mounts")
   public static final String MOUNTS = "51";

   @RBEntry("Sites")
   public static final String SITES = "320";

   @RBEntry("Replica Vaults")
   public static final String REMOTE_VAULTS = "321";

   @RBEntry("Root Folders")
   public static final String ROOT_FOLDERS = "425";

   /**
    * Menus
    *
    **/
   @RBEntry("File")
   public static final String FILEPANE = "52";

   @RBEntry("New")
   public static final String FILENEW = "53";

   @RBEntry("Host")
   public static final String NEWHOST = "54";

   @RBEntry("Vault")
   public static final String NEWVAULT = "55";

   @RBEntry("Folder")
   public static final String NEWFOLDER = "56";

   @RBEntry("Root Folder")
   public static final String NEWROOTFOLDER = "429";

   @RBEntry("Refresh")
   public static final String REFRESH = "57";

   @RBEntry("Delete ")
   public static final String DELETE = "58";

   @RBEntry("Close")
   public static final String CLOSE = "59";

   @RBEntry("Object")
   public static final String OBJECTPANE = "60";

   @RBEntry("Update")
   public static final String UPDATE = "61";

   @RBEntry("Remove unreferenced files")
   public static final String CLEAN = "62";

   @RBEntry("Mounts")
   public static final String TOOLSPANE = "63";

   @RBEntry("Mount")
   public static final String MOUNT = "64";

   @RBEntry("Update Mount")
   public static final String UPDATE_MOUNT = "65";

   @RBEntry("Unmount")
   public static final String UNMOUNT = "66";

   @RBEntry("Help")
   public static final String HELP_PANE = "67";

   @RBEntry("File Vault Topics")
   public static final String HELPVAULT = "68";

   @RBEntry("About Windchill")
   public static final String HELPABOUT = "69";

   @RBEntry("Validate")
   public static final String VALIDATE_MOUNT = "300";

   /**
    * Mount/Unmount titles
    *
    **/
   @RBEntry("New Mount")
   public static final String NEWMOUNT_TITLE = "70";

   @RBEntry("Update Mount")
   public static final String UPDATEMOUNT_TITLE = "71";

   /**
    * String contants
    *
    **/
   @RBEntry("No Mounts")
   public static final String NO_MOUNTS = "75";

   @RBEntry("Not mounted.")
   public static final String NOT_MOUNTED = "76";

   /**
    * MountInfo messages
    *
    **/
   @RBEntry("The host has been updated by another user.")
   public static final String HOST_UPDATED_BY_OTHER = "77";

   @RBEntry("Cannot delete a host that has mounts.")
   public static final String HOST_HAS_MOUNTS = "78";

   @RBEntry("Vault not found.")
   public static final String VAULT_NOT_FOUND = "79";

   @RBEntry("Host not found.")
   public static final String HOST_NOT_FOUND = "80";

   @RBEntry("Folder not found.")
   public static final String FOLDER_NOT_FOUND = "81";

   @RBEntry("Bad vault reference.")
   public static final String BAD_VAULT = "82";

   @RBEntry("There are no hosts in the system.")
   public static final String NO_SYSTEM_HOSTS = "83";

   @RBEntry("There are no folders in the system.")
   public static final String NO_SYSTEM_FOLDERS = "84";

   @RBEntry("There are no vaults in the system.")
   public static final String NO_SYSTEM_VAULTS = "85";

   /**
    * FileVaultFrame messages
    *
    **/
   @RBEntry("No mount selected")
   public static final String NO_MOUNT_SELECTED = "86";

   @RBEntry("Are you sure you want to delete the selected folder and all of its mounts?")
   public static final String SURE_ABOUT_FOLDERS_AND_MOUNTS = "87";

   @RBEntry("Sorry, you cannot remove a vault that has folders")
   public static final String CANT_REMOVE_VAULT = "88";

   @RBEntry("Sorry, you cannot remove a host which has mounts")
   public static final String CANT_REMOVE_HOST_WITH_MOUNTS = "89";

   @RBEntry("Sorry, you cannot mount a vault")
   public static final String CANT_MOUNT_VAULT = "90";

   @RBEntry("The selected vault has no mounts")
   public static final String VAULT_HAS_NO_MOUNTS = "91";

   @RBEntry("The selected folder has no mounts")
   public static final String FOLDER_HAS_NO_MOUNTS = "92";

   @RBEntry("No Folders")
   public static final String NO_FOLDERS = "93";

   @RBEntry("No Vaults")
   public static final String NO_VAULTS = "94";

   @RBEntry("No Hosts")
   public static final String NO_HOSTS = "95";

   @RBEntry("Sorry, you cannot clean a host")
   public static final String CANT_CLEAN_HOSTS = "96";

   @RBEntry("Are you sure you want to delete the selected root folder and all of its mounts?")
   public static final String SURE_ABOUT_ROOT_FOLDERS_AND_MOUNTS = "442";

   @RBEntry("Are you sure you want to delete the selected root folder?")
   public static final String SURE_DELETE_ROOT_FOLDER = "443";

   @RBEntry("Update Policy Items")
   public static final String UPDATE_POLICY_ITEMS = "454";

   @RBEntry("Policy items have been updated successfully.")
   public static final String POLICY_ITEMS_UPDATED = "455";

   /**
    * AdminAddVault Messages
    *
    **/
   @RBEntry("No vault name specified")
   public static final String NO_VAULT_NAME_SPECIFIED = "97";

   @RBEntry("Please enter a Vault Name")
   public static final String PLEASE_ENTER_VAULT = "98";

   @RBEntry("Sorry, the vault name is not unique")
   public static final String VAULT_NAME_INUSE = "99";

   /**
    * AdminAddHost messages
    *
    **/
   @RBEntry("Please specify a Host Name.")
   public static final String SPECIFY_HOST = "100";

   @RBEntry("Please specify a site name.")
   public static final String SPECIFY_SITE = "330";

   /**
    * AdminEditFolder messages
    *
    **/
   @RBEntry("Please select a vault.")
   public static final String NO_VAULT_SELECTED = "101";

   @RBEntry("Please supply the folder name.")
   public static final String NO_FOLDER_NAME = "102";

   @RBEntry("please specify the capacity.")
   public static final String NO_CAPACITY = "103";

   @RBEntry("Please specify the threshold.")
   public static final String NO_THRESHOLD = "104";

   @RBEntry("Threshold percent must be less than or equal to 100.0")
   public static final String BAD_PERCENT = "105";

   @RBEntry("Threshold must be positive")
   public static final String NEG_THRESH = "106";

   @RBEntry("Percentage thresholds must be a floating point or an integer less than or equal to 100.0")
   public static final String BAD_THRESH_FORMAT = "107";

   @RBEntry("Please enter a numeric value.")
   public static final String NEED_NUMBERS = "108";

   @RBEntry("Threshold must be less than or equal to the capacity")
   public static final String THRESH_TOO_BIG = "109";

   @RBEntry("Capacity must be positive")
   public static final String NEG_CAPACITY = "110";

   @RBEntry("Capacity must be specified as a numeric value")
   public static final String CAPACITY_NOT_NUMBER = "111";

   /**
    * AdminAdd/Edit mount messages.
    *
    **/
   @RBEntry("Local mounts must have a path specified")
   public static final String LOCAL_MOUNT_NO_PATH = "112";

   @RBEntry("Path not specified")
   public static final String NO_PATH = "113";

   @RBEntry("You cannot remove a folder that has mounts")
   public static final String CANNOT_REMOVE_FOLDER = "114";

   @RBEntry("The selected hosts has mounts, are you sure you want to remove the host and all of its mounts?")
   public static final String REMOVE_ALL_HOST_MOUNTS = "115";

   @RBEntry("The folder has been updated by another user")
   public static final String FOLDER_STALE = "116";

   @RBEntry("Show all possible mounts")
   public static final String ALL_POSSIBLE_MOUNTS = "117";

   @RBEntry("Vault Configuration")
   public static final String FVADMIN_TITLE = "118";

   @RBEntry("Are you sure you want to delete the selected host?")
   public static final String SURE_DELETE_HOST = "119";

   @RBEntry("Are you sure you want to delete the selected vault?")
   public static final String SURE_DELETE_VAULT = "120";

   @RBEntry("Are you sure you want to delete the selected folder?")
   public static final String SURE_DELETE_FOLDER = "121";

   @RBEntry("This action will permanently remove files from your system. Parametric Technology Corporation strongly recommends that you back up all the files in the vault before continuing with this operation. Do you wish to continue?")
   public static final String SURE_CLEAN = "122";

   @RBEntry("***")
   public static final String NO_DATA = "123";

   @RBEntry("Generate Backup Info")
   public static final String BACKUP_DATA = "124";

   @RBEntry("Backup Info")
   public static final String BACKUPS = "125";

   @RBEntry("Download")
   public static final String BACKUP_DOWNLOAD = "126";

   @RBEntry("You are not authorized to access this screen")
   public static final String NOT_AUTHORIZED = "127";

   @RBEntry("Backup Info generated")
   public static final String BACKUP_GENERATED = "128";

   @RBEntry("Unreferenced files removed from vault")
   public static final String VAULT_CLEANED = "129";

   @RBEntry("Unreferenced files removed from folder")
   public static final String FOLDER_CLEANED = "130";

   @RBEntry("Mount not found")
   public static final String MOUNT_NOT_FOUND = "131";

   @RBEntry("Saving...")
   public static final String SAVING = "210";

   @RBEntry("Saved")
   public static final String SAVED = "211";

   @RBEntry("Deleted")
   public static final String DELETED = "212";

   /**
    * Exceptions for creating or modifying file vault rules
    *
    **/
   @RBEntry("The Help system could not be initialized: ")
   public static final String HELP_INITIALIZATION_FAILED = "201";

   @RBEntry("An exception occurred initializing the data: ")
   public static final String INITIALIZATION_FAILED = "200";

   @RBEntry("Select a class, a state and a file vault.")
   public static final String MISSING_FV_INPUT = "202";

   @RBEntry("Cannot get file vault policy rules.")
   public static final String RETRIEVE_FAILED = "203";

   @RBEntry("Cannot delete file vault policy rules.")
   public static final String DELETE_FAILED = "204";

   @RBEntry("The applet listeners could not be initialized: ")
   public static final String APPLET_LISTENERS_NOT_INITIALIZED = "205";

   @RBEntry("?")
   public static final String QUESTION_MARK = "414";

   @RBEntry("Domain:")
   public static final String DOMAIN_LABEL = "215";

   @RBEntry("The applet listeners could not be removed: ")
   public static final String REMOVING_LISTENER_FAILED = "216";

   @RBEntry("Domain Vaulting Rules")
   public static final String DOMAIN_TITLE = "217";

   @RBEntry("Are you sure you want to remove the selected mount?")
   public static final String REM_MOUNT = "213";

   @RBEntry("Revaulting")
   public static final String REVAULT = "214";

   @RBEntry("Domain")
   @RBComment("message for creating or modifying file vault rules")
   public static final String DOMAIN = "360";

   @RBEntry("All")
   public static final String ALL = "361";

   @RBEntry("Full Class")
   public static final String FULL_CLASS = "362";

   @RBEntry("Class")
   public static final String CLASS = "363";

   @RBEntry("State")
   public static final String STATE = "364";

   @RBEntry("Master Vault")
   public static final String FILE_VAULT = "365";

   @RBEntry("Vaulting Rule")
   public static final String VAULTING_RULE = "366";

   @RBEntry("Vaulting")
   public static final String VAULTING = "367";

   @RBEntry("Retrieve")
   public static final String RETRIEVE = "371";

   @RBEntry("New")
   public static final String CREATE = "372";

   @RBEntry("Report")
   public static final String REPORT = "373";

   @RBEntry("Classes")
   public static final String CLASSES = "380";

   @RBEntry("States")
   public static final String STATES = "381";

   @RBEntry("Master Vaults")
   public static final String FILE_VAULTS = "382";

   @RBEntry("Replica Folders")
   public static final String REMOTE_FOLDERS = "383";

   @RBEntry("An exception occurred while refreshing: ")
   public static final String REFRESH_FAILED = "350";

   @RBEntry("Site")
   public static final String SITE = "351";

   @RBEntry("Replica Vault")
   public static final String REMOTE_VAULT = "352";

   @RBEntry("Replica Vaulting")
   public static final String REMOTE_VAULTING = "353";

   @RBEntry("Administrative Domains")
   public static final String ADMINISTRATIVE_DOMAINS = "354";

   @RBEntry("View")
   public static final String VIEW = "355";

   @RBEntry("Clear")
   public static final String CLEAR = "384";

   @RBEntry("Display")
   public static final String DISPLAY = "385";

   @RBEntry("List Values")
   public static final String LIST_VALUES = "386";

   @RBEntry("Event")
   public static final String EVENT = "387";

   @RBEntry("List")
   public static final String LIST = "388";

   @RBEntry("File Vault Report")
   public static final String FILE_VAULT_REPORT = "389";

   @RBEntry("Replica Vault Report")
   public static final String REMOTE_VAULT_REPORT = "390";

   @RBEntry("Name")
   public static final String NAME = "391";

   @RBEntry("Master")
   public static final String LOCAL = "392";

   @RBEntry("Synchronized")
   public static final String SYNCHED = "393";

   @RBEntry("Last Time")
   public static final String LAST_TIME = "394";

   @RBEntry("Last Status")
   public static final String LAST_STATUS = "395";

   @RBEntry("Next Time")
   public static final String NEXT_TIME = "396";

   @RBEntry("Cannot reset this object type")
   public static final String CANT_RESET = "397";

   @RBEntry("Replication rules reset")
   public static final String REPL_RESET = "398";

   @RBEntry("Cannot remove Replica Vault which has folders.")
   public static final String CANT_REMOVE_REMVAULT = "399";

   @RBEntry("Replica Folder")
   public static final String REMOTE_FOLDER = "400";

   @RBEntry("Content Replication Rules")
   public static final String REMOTE_VAULT_RULE = "401";

   @RBEntry("Broadcast Configuration")
   public static final String BROADCAST = "402";

   @RBEntry("No host is selected")
   public static final String NO_HOST_SELECTED = "403";

   @RBEntry("No folder is selected")
   public static final String NO_FOLDER_SELECTED = "404";

   @RBEntry("Can't reset local site")
   public static final String CANT_RESET_LOCAL = "405";

   @RBEntry("Resetting the replication rules means that during the next replication run all content will be replicated. Are you sure you want to do this")
   public static final String SURE_ABOUT_RESET = "406";

   @RBEntry("Are you sure you want to delete the selected site?")
   public static final String SURE_DELETE_SITE = "407";

   @RBEntry("All Sites")
   public static final String ALL_SITES = "408";

   @RBEntry("Toggle Enabled")
   public static final String TOGGLE_ENABLED = "409";

   @RBEntry("Schedule Content Replication")
   public static final String SCHED_CONT_REPL = "410";

   @RBEntry("Reset Replication")
   public static final String RESET_CONT_REPL = "411";

   @RBEntry("Reset Undelivered Replication Items")
   public static final String RESET_UNDELIVERED = "412";

   @RBEntry("External Storage Administrator")
   public static final String ES_ADMIN_TITLE = "413";

   @RBEntry("Cache vault")
   public static final String DESIGNATED_FOR_CACHE = "132";

   @RBEntry("Cache")
   public static final String CACHE = "133";

   @RBEntry("Cannot create designated vault for content cache on main site.")
   public static final String CANT_CREATE_MASTERED_VAULT_ON_MASTER = "134";

   @RBEntry("Cannot create more than one vault designated for content cache.")
   public static final String CANT_CREATE_TWO_MASTERED_VAULTS = "135";

   @RBEntry("Vault Error")
   public static final String VAULT_ERROR = "136";

   @RBEntry("Site:")
   public static final String NEW_VAULT_SITE = "137";

   @RBEntry("Vault Warning")
   public static final String VAULT_WARNING = "138";

   @RBEntry("There is no vault on the local master designated for content cache. Ensure that another vault on local master is designated for content cache immediately.")
   public static final String NO_MASTERED_VAULT_ON_LOCAL_MASTER = "139";

   @RBEntry("Existing content cache designated vault on local master is removed. Ensure that another vault on local master is designated for content cache immediately.")
   public static final String MASTERED_VAULT_ON_LOCAL_MASTER_REMOVED = "140";

   @RBEntry("Existing content cache designated vault on local master is marked readonly. Ensure that another vault on local master is designated for content cache immediately.")
   public static final String MASTERED_VAULT_ON_LOCAL_MASTER_READONLY = "141";

   @RBEntry("Vault can not be removed as there is schedule on it")
   public static final String HAS_SCHEDULE_ON_VAULT = "415";

   @RBEntry("Folder cannot be enabled, mount(s) missing.")
   public static final String ENABLE_FOLDER_CHECKBOX = "416";

   @RBEntry("No mount has been validated. Please check the logs for more details.")
   public static final String NO_MOUNT_TO_VALIDATE = "417";

   @RBEntry("Some of the mounts are not valid")
   public static final String INVALID_MOUNT_STATUS = "418";

   @RBEntry("Mount validation is done partially. Please check logs for more details.")
   public static final String PARTIAL_MOUNT_VALIDATION = "419";

   @RBEntry("Some of the mounts are not valid and some of the folders corresponding to the mounts to be validated are not enabled")
   public static final String PARTIAL_VALIDATATION_AND_INVALID_MOUNT_STATUS = "420";

   @RBEntry("Mount validation completes successfully")
   public static final String VALIDATION_SUCCEED = "421";

   @RBEntry("A vault selected for upload cannot be selected as a default target for a site unless it has been selected for download")
   public static final String DEFAULT_TARGET_CANT_BE_FOR_UPLOAD = "422";

   @RBEntry("Vault {0} was earlier selected as the default target for the site. If you set this vault as the default target for the site and save it, the earlier selection will be unset.")
   public static final String EARLIER_DEFAULT_TARGET_SWITCHED = "423";

   @RBEntry("One vault has to be mandatorily selected as the default target for the site")
   public static final String SITE_NEEDS_A_DEFAULT_TARGET = "424";

   @RBEntry("A replica vault should be either selected as 'For cached content' or 'For replicated content'")
   public static final String REPLICA_VAULT_SHLD_BE_FOR_UPLOAD_OR_DWNLD = "426";

   @RBEntry("Root Folder")
   public static final String ROOT_FOLDER = "427";

   @RBEntry("A replica vault should be either selected as 'For cached content' or 'For replicated content'")
   public static final String AT_LEAST_ONE_SELECTED_FOR_REPLICA_VLT = "430";

   @RBEntry("Master Folder")
   public static final String FOLDER = "432";

   @RBEntry("Replica vault")
   public static final String FOR_REPLICATED_CONTENT = "433";

   @RBEntry("Vault type:")
   public static final String TARGET_FOR = "434";

   @RBEntry("Default target for site")
   public static final String DEFAULT_TARGET_FOR_SITE = "435";

   @RBEntry("Automatic folder creation")
   public static final String AUTO_FOLDER_CREATE = "436";

   @RBEntry("Root Path")
   public static final String ROOT_PATH = "437";

   @RBEntry("This folder cannot be mounted onto a vault that has the 'Automatic Folder Creation' property facility enabled ")
   public static final String CANNOT_SELECT_AUTO_FOLDER_CREATE_VAULT = "438";

   @RBEntry("Vault")
   public static final String VAULT = "439";

   @RBEntry("This path is already mounted to another folder or root folder mount. Please choose another path.")
   public static final String PATH_ALREADY_USED = "440";

   @RBEntry("Are you sure you want to delete this rule? Rule parameters:")
   public static final String DELETE_RULE = "441";

   @RBEntry("Move files instead of deleting")
   public static final String MOVE = "444";

   @RBEntry("This action will permanently remove files from your system. You may want to move the files so that you can take a backup and then manually delete these files. Do you wish to continue?")
   public static final String SURE_MOVE = "445";

   @RBEntry("Cannot delete vault selected as the default target for the site")
   public static final String CANNOT_DELETE_DEFAULT_TARGET_VAULT = "446";

   @RBEntry("Master Vault")
   public static final String MASTER_VAULT = "447";

   @RBEntry("Default system target")
   public static final String DEFAULT_SYSTEM_TARGET = "448";

   @RBEntry("One master vault has to be mandatorily selected as the default target for the system.")
   public static final String SITE_NEEDS_A_DEFAULT_SYS_TARGET = "449";

   @RBEntry("Vault {0} was earlier selected as the default system target. If you set this vault as the default system target and save it, the earlier selection will be unset.")
   public static final String EARLIER_DEFAULT_SYS_TARGET_SWITCHED = "450";

   @RBEntry("Cannot delete vault selected as the default system target.")
   public static final String CANNOT_DELETE_DEFAULT_SYS_TARGET = "451";

   @RBEntry("All vaults")
   public static final String ALL_VAULTS = "452";

   @RBEntry("A job to remove unreferenced files is now initiated in background. See Event Manager for the status of the job.")
   public static final String REMOVE_UNREFERENCED_FILES_JOB_INITIATED = "453";

   @RBEntry("Define Content Cleanup Rules")
   public static final String AUTO_VAULT_CLEANUP_MENU_LABEL = "456";

   @RBEntry("Automated cleanup of replica vaults")
   public static final String AUTO_CLEANUP_TITLE = "457";

   @RBEntry("Site:")
   public static final String AUTO_CLEANUP_SITENAME = "458";

   @RBEntry("Cleanup Rules:")
   public static final String AUTO_CLEANUP_RULES = "459";

   @RBEntry("Restrict vault size to")
   public static final String AUTO_CLEANUP_SIZE_RULE = "460";

   @RBEntry("Age-based rule,")
   public static final String AUTO_CLEANUP_AGE_RULE = "461";

   @RBEntry("Dereference content not accessed in the last")
   public static final String AUTO_CLEAN_AGE_RULE_DEREF = "476";

   @RBEntry("% of total available space on disk")
   public static final String AUTO_CLEANUP_PCT_SIZE = "462";

   @RBEntry("MB")
   public static final String AUTO_CLEANUP_SIZE_OPTION_MB = "463";

   @RBEntry("GB")
   public static final String AUTO_CLEANUP_SIZE_OPTION_GB = "464";

   @RBEntry("day(s)")
   public static final String AUTO_CLEANUP_AGE_OPTION_DAYS = "465";

   @RBEntry("week(s)")
   public static final String AUTO_CLEANUP_AGE_OPTION_WEEKS = "466";

   @RBEntry("month(s)")
   public static final String AUTO_CLEANUP_AGE_OPTION_MONTHS = "467";

   @RBEntry("Cleanup Schedule:")
   public static final String AUTO_CLEANUP_SCHEDULE = "468";

   @RBEntry("Run every")
   public static final String AUTO_CLEANUP_SCHEDULE_RUN = "469";

   @RBEntry("day(s)")
   public static final String AUTO_CLEANUP_DAYS_AT = "470";

   @RBEntry("AM")
   public static final String AUTO_CLEANUP_SCHEDULE_AM = "471";

   @RBEntry("PM")
   public static final String AUTO_CLEANUP_SCHEDULE_PM = "472";

   @RBEntry("Next Scheduled Run:")
   public static final String AUTO_CLEANUP_NEXT_SCHEDULE = "473";

   @RBEntry("Run cleanup now")
   public static final String AUTO_CLEANUP_RUN_IMM = "474";

   @RBEntry("Automatic cleanup of older content")
   public static final String INCLUDE_FOR_AUTO_CLEANUP_TITLE = "475";

   @RBEntry("ATTENTION: A value that was entered is not valid. \nCleanup Schedule: You must enter a value between 0 and 23 hours and between 0 and 59 minutes.")
   public static final String ERR_MSG_SCHED_HOURS = "477";

   @RBEntry("ATTENTION: A value that was entered is not valid. \nCleanup Schedule: You must enter a value between 0 and 23 hours and between 0 and 59 minutes.")
   public static final String ERR_MSG_SCHED_MINS = "478";

   @RBEntry("Error getting local time zone")
   public static final String ERR_TIME_ZONE_GETTER = "479";

   @RBEntry("ATTENTION: Required information is missing. \nCleanup Rules: You must specify at least one rule for the cleanup schedule.")
   public static final String ERR_MSG_AUTO_CLEANUP_CRITERIA = "480";

   @RBEntry("ATTENTION: Required information is missing. \nCleanup Rules: To restrict vault size, you must enter the maximum vault size.")
   public static final String ERR_MSG_AUTO_CLEANUP_SIZE_ABS_VAL = "481";

   @RBEntry("Please enter a finite percentage value for size - based rule")
   public static final String ERR_MSG_AUTO_CLEANUP_SIZE_PERC_VAL = "482";

   @RBEntry("ATTENTION: Required information is missing. \nCleanup Rules: To dereference replicated content files based on when it was last accessed, you must specify a time period.")
   public static final String ERR_MSG_AUTO_CLEANUP_AGE_VAL = "483";

   @RBEntry("ATTENTION: Required information is missing. \nCleanup Schedule: You must specify the time for the cleanup schedule.")
   public static final String ERR_MSG_AUTO_CLEANUP_SCHED_TIME_HRS = "484";

   @RBEntry("ATTENTION: Required information is missing. \nCleanup Schedule: You must specify the time for the cleanup schedule.")
   public static final String ERR_MSG_AUTO_CLEANUP_SCHED_TIME_MINS = "485";

   @RBEntry("Absolute value for size - based rule is out of range")
   public static final String ERR_MSG_AUTO_CLEANUP_SIZE_ABS_VALUE_OVFL = "486";

   @RBEntry("ATTENTION: Maximum limit reached for the last accessed days value in the cleanup rule.")
   public static final String ERR_MSG_AUTO_CLEANUP_AGE_DAYS_OVFL = "487";

   @RBEntry("ATTENTION: The value for the frequency for the cleanup schedule is out of range")
   public static final String ERR_MSG_AUTO_CLEANUP_SCHED_PERIODICITY_OVFL = "488";

   @RBEntry("Please enter only non - zero integer")
   public static final String ERR_MSG_ONLY_NON_ZERO_INTS = "489";

   @RBEntry("Please enter only finite decimal number values for the size - based rule absolute value field")
   public static final String ERR_MSG_DECIMALS_ONLY_ABS_SIZE = "490";

   @RBEntry("Please enter only decimal numbers in the range of 0 to 100.0 for the size - based rule percentage value field")
   public static final String ERR_MSG_DECIMALS_ONLY_PERC_SIZE = "491";

   @RBEntry("Please specify a valid schedule time to perform automatic vault cleanup")
   public static final String ERR_MSG_AUTO_CLEANUP_TIME = "492";

   @RBEntry("yyyy-MM-dd HH:mm")
   public static final String AUTO_CLEANUP_NEXT_SCHEDULE_DATE_FORMAT="493";

   @RBEntry("ATTENTION: If you clear the Cleanup Schedule checkbox, cleanup operations will not be run for the replica vaults in this site. \nDo you want to proceed?")
   public static final String AUTO_CLEANUP_SCHEDULE_DISABLE_WARNING="494";

   @RBEntry("Alert!")
   public static final String WARNING_MESSAGE_DIALOG_TITLE="495";

   @RBEntry("Warning!The value will be rounded to two digits after decimal point")
   public static final String ONLY_TWO_DIGITS_AFTER_DECIMAL="496";

   @RBEntry("Master Folder")
   public static final String MASTER_FOLDER="497";

   @RBEntry("Replica Vault")
   public static final String VAULT_SIZE_COLUMN_HEADING_REP_VAULT = "500";

   @RBEntry("Size Occupied (MB)")
   public static final String VAULT_SIZE_COLUMN_HEADING_VAULT_SIZE = "501";

   @RBEntry("Vault Sizes:")
   public static final String TITLE_VAULT_SIZES = "502";

   @RBEntry("Status of Latest Run:")
   public static final String LATEST_RUN_STATUS_LBL = "503";

   @RBEntry("* Running this utility cleans up least recently used replicated content files until the \nspecified cleanup criteria are met. For a vault having folders mounted across multiple \ndrives, this need not result in any space freed up in the active folder")
   public static final String CLEANUP_INFO_MSG = "508";

   @RBEntry("Starting cleanup of replica vaults on site {0}")
   public static final String NOW_RUNNING_IMM = "510";

   @RBEntry("Logs for Latest Run: ")
   public static final String LOGS_TITLE = "514";

   @RBEntry("Download")
   public static final String VIEW_LOGS = "515";

   @RBEntry("ATTENTION: The value for the size - based rule absolute value field for the cleanup schedule is out of range")
   public static final String ERR_MSG_AUTO_CLEANUP_SCHED_ABS_SIZE_OVFL = "516";

   @RBEntry("Run once")
   public static final String RUN_ONCE = "517";

   @RBEntry(" Run at")
   public static final String RUN_AT = "518";

   @RBEntry("ATTENTION: Required information is missing. \nCleanup Rules: Please specify a valid frequency for the cleanup schedule")
   public static final String ERR_MSG_AUTO_CLEANUP_PERIODICITY = "519";

   @RBEntry("ATTENTION: Please select schedule for automatic cleanup of vault")
   public static final String ERR_MSG_AUTO_CLEANUP_SCHEDULE = "520";

   @RBEntry("ATTENTION: Required information is missing. \nCleanup Rules: You must specify at least one rule before running a cleanup operation.")
   public static final String ERR_MSG_AUTO_CLEANUP_CRITERIA_IMMEDIATE = "521";

   @RBEntry("ATTENTION: Required information is missing.")
   public static final String ERR_MSG_COMMON = "522";

   @RBEntry("'Ready'")
   public static final String READY_STATE = "523";

   @RBEntry("'Executing'")
   public static final String IN_PROGRESS = "524";

   @RBEntry("ATTENTION: Create mount failed. \nThe value for the mount path should not exceed 180 characters")
   public static final String NEW_MOUNT_PATH_EXCEEDS_LIMIT = "525";

   @RBEntry("ATTENTION: Update mount failed. \nThe value for the mount path should not exceed 180 characters")
   public static final String UPDATE_MOUNT_PATH_EXCEEDS_LIMIT = "526";

   @RBEntry("ATTENTION: Create mount failed. \nDuplicate mount paths are not allowed.")
   public static final String NEW_MOUNT_PATH_DUPLICATE_PATH_ERROR = "527";
   
   @RBEntry("Host Type:")
   public static final String HOST_TYPE = "528";
   
   @RBEntry("New host of type Cluster will update the host's mount to that of the cluster.")
   public static final String NEW_CLUSTER_HOST_WARNING = "529";
   
   @RBEntry("Updating this host will convert host type from Standalone to Cluster.")
   public static final String UPDATE_HOST_TO_CLUSTER = "530";
   
   @RBEntry("Updating this host will convert host type from Cluster to Standalone.")
   public static final String UPDATE_HOST_TO_STANDALONE = "531";
 
   @RBEntry("Mounting on this host will affect all cluster nodes.")
   public static final String NEW_MOUNT_ON_CLUSTER_WARNING = "532";
   
   @RBEntry("Updating mount on this host will affect all cluster nodes.")
   public static final String UPDATE_MOUNT_ON_CLUSTER_WARNING = "533";
   
   @RBEntry("Are you sure you want to unmount the selected folder from all cluster nodes?")
   public static final String REMOVE_MOUNT_ON_CLUSTER_WARNING = "534";

}



