/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.filevault;

import wt.util.resource.*;

@RBUUID("wt.clients.filevault.containerPickResource")
public final class containerPickResource_it extends WTListResourceBundle {
   /**
    * Labels for containerPickPanel
    **/
   @RBEntry("Contesto")
   public static final String CONTAINER_LABEL = "0";

   @RBEntry("Descrizione")
   public static final String CONTAINER_DESC = "1";

   /**
    * Button labels
    **/
   @RBEntry("OK")
   public static final String OK = "2";

   @RBEntry("Annulla")
   public static final String CANCEL = "3";

   @RBEntry("     Seleziona")
   public static final String CONTAINER_SEL_BTN_LABEL = "4";

   @RBEntry("Scegli contesto")
   public static final String CONTAINER_PICK_DLG_TITLE = "5";
}
