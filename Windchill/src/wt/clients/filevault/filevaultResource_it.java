/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.filevault;

import wt.util.resource.*;

@RBUUID("wt.clients.filevault.filevaultResource")
public final class filevaultResource_it extends WTListResourceBundle {
   /**
    * Button Labels
    *
    **/
   @RBEntry("OK")
   public static final String OK = "1";

   @RBEntry("Annulla")
   public static final String CANCEL = "2";

   @RBEntry("Guida")
   public static final String HELP = "3";

   @RBEntry("Sposta all'inizio")
   public static final String TOP = "4";

   @RBEntry("Sposta su")
   public static final String UP = "5";

   @RBEntry("Sposta giù")
   public static final String DOWN = "6";

   @RBEntry("Sposta in fondo")
   public static final String BOTTOM = "7";

   /**
    * Screen Titles
    *
    **/
   @RBEntry("Nuovo host")
   public static final String NEWHOST_TITLE = "8";

   @RBEntry("Aggiorna host")
   public static final String UPDATEHOST_TITLE = "9";

   @RBEntry("Nuovo archivio")
   public static final String NEWVAULT_TITLE = "10";

   @RBEntry("Aggiorna archivio")
   public static final String UPDATEVAULT_TITLE = "11";

   @RBEntry("Nuova cartella")
   public static final String NEWFOLDER_TITLE = "12";

   @RBEntry("Aggiorna cartella")
   public static final String UPDATEFOLDER_TITLE = "13";

   @RBEntry("Nuova cartella radice")
   public static final String NEWROOTFOLDER_TITLE = "428";

   /**
    * Symbols
    *
    **/
   @RBEntry(":")
   @RBPseudo(false)
   public static final String COLON = "14";

   @RBEntry("...")
   @RBPseudo(false)
   public static final String ELLIPSES = "15";

   /**
    * Screen labels
    *
    **/
   @RBEntry("Operazioni sulla cartella")
   public static final String FOLDER_OPS = "22";

   @RBEntry("Mostra solo le cartelle accessibili in scrittura")
   public static final String SHOW_WRITABLE = "28";

   @RBEntry("Mostra tutto")
   public static final String SHOW_ALL = "29";

   @RBEntry("Visualizza soltanto i mount esistenti")
   public static final String SHOW_EXISTING_MOUNTS = "30";

   @RBEntry("%")
   @RBPseudo(false)
   public static final String PERCENT = "31";

   @RBEntry("Tipo di cartella")
   public static final String FOLDER_TYPE = "431";

   /**
    * combo boxes
    *
    **/
   @RBEntry("TB")
   public static final String SIZE_TB = "32";

   @RBEntry("GB")
   public static final String SIZE_GB = "33";

   @RBEntry("MB")
   public static final String SIZE_MB = "34";

   @RBEntry("KB")
   public static final String SIZE_KB = "35";

   /**
    * list headings
    *
    **/
   @RBEntry("Cartelle master")
   public static final String FOLDERS = "48";

   @RBEntry("Host")
   public static final String HOSTS = "49";

   @RBEntry("Archivi master")
   public static final String VAULTS = "50";

   @RBEntry("Mount")
   public static final String MOUNTS = "51";

   @RBEntry("Siti")
   public static final String SITES = "320";

   @RBEntry("Archivi replica")
   public static final String REMOTE_VAULTS = "321";

   @RBEntry("Cartelle radice")
   public static final String ROOT_FOLDERS = "425";

   /**
    * Menus
    *
    **/
   @RBEntry("File")
   public static final String FILEPANE = "52";

   @RBEntry("Nuovo")
   public static final String FILENEW = "53";

   @RBEntry("Host")
   public static final String NEWHOST = "54";

   @RBEntry("Archivio")
   public static final String NEWVAULT = "55";

   @RBEntry("Cartella")
   public static final String NEWFOLDER = "56";

   @RBEntry("Cartella radice")
   public static final String NEWROOTFOLDER = "429";

   @RBEntry("Aggiorna")
   public static final String REFRESH = "57";

   @RBEntry("Elimina ")
   public static final String DELETE = "58";

   @RBEntry("Chiudi")
   public static final String CLOSE = "59";

   @RBEntry("Oggetto")
   public static final String OBJECTPANE = "60";

   @RBEntry("Aggiorna")
   public static final String UPDATE = "61";

   @RBEntry("Rimuovi file senza riferimenti")
   public static final String CLEAN = "62";

   @RBEntry("Mount")
   public static final String TOOLSPANE = "63";

   @RBEntry("Mount")
   public static final String MOUNT = "64";

   @RBEntry("Aggiorna mount")
   public static final String UPDATE_MOUNT = "65";

   @RBEntry("Disinstalla")
   public static final String UNMOUNT = "66";

   @RBEntry("Guida")
   public static final String HELP_PANE = "67";

   @RBEntry("Argomenti della Guida")
   public static final String HELPVAULT = "68";

   @RBEntry("Informazioni su Windchill")
   public static final String HELPABOUT = "69";

   @RBEntry("Convalida")
   public static final String VALIDATE_MOUNT = "300";

   /**
    * Mount/Unmount titles
    *
    **/
   @RBEntry("Nuovo mount")
   public static final String NEWMOUNT_TITLE = "70";

   @RBEntry("Aggiorna mount")
   public static final String UPDATEMOUNT_TITLE = "71";

   /**
    * String contants
    *
    **/
   @RBEntry("Nessun mount")
   public static final String NO_MOUNTS = "75";

   @RBEntry("Non montato")
   public static final String NOT_MOUNTED = "76";

   /**
    * MountInfo messages
    *
    **/
   @RBEntry("L'host è stato aggiornato da un altro utente.")
   public static final String HOST_UPDATED_BY_OTHER = "77";

   @RBEntry("Impossibile eliminare un host contenente mount.")
   public static final String HOST_HAS_MOUNTS = "78";

   @RBEntry("Impossibile trovare l'archivio.")
   public static final String VAULT_NOT_FOUND = "79";

   @RBEntry("Impossibile trovare l'host.")
   public static final String HOST_NOT_FOUND = "80";

   @RBEntry("Impossibile trovare la cartella.")
   public static final String FOLDER_NOT_FOUND = "81";

   @RBEntry("Riferimento archivio non valido.")
   public static final String BAD_VAULT = "82";

   @RBEntry("Nessun host nel sistema.")
   public static final String NO_SYSTEM_HOSTS = "83";

   @RBEntry("Nessuna cartella nel sistema.")
   public static final String NO_SYSTEM_FOLDERS = "84";

   @RBEntry("Nessun archivio nel sistema.")
   public static final String NO_SYSTEM_VAULTS = "85";

   /**
    * FileVaultFrame messages
    *
    **/
   @RBEntry("Nessun mount selezionato.")
   public static final String NO_MOUNT_SELECTED = "86";

   @RBEntry("Eliminare la cartella selezionata e tutti i relativi mount? ")
   public static final String SURE_ABOUT_FOLDERS_AND_MOUNTS = "87";

   @RBEntry("Impossibile rimuovere un archivio contenente cartelle. ")
   public static final String CANT_REMOVE_VAULT = "88";

   @RBEntry("Impossibile rimuovere un host contenente mount. ")
   public static final String CANT_REMOVE_HOST_WITH_MOUNTS = "89";

   @RBEntry("Impossibile montare un archivio.")
   public static final String CANT_MOUNT_VAULT = "90";

   @RBEntry("L'archivio selezionato non contiene mount.")
   public static final String VAULT_HAS_NO_MOUNTS = "91";

   @RBEntry("La cartella selezionata non contiene mount.")
   public static final String FOLDER_HAS_NO_MOUNTS = "92";

   @RBEntry("Nessuna cartella")
   public static final String NO_FOLDERS = "93";

   @RBEntry("Nessun archivio")
   public static final String NO_VAULTS = "94";

   @RBEntry("Nessun host")
   public static final String NO_HOSTS = "95";

   @RBEntry("Impossibile eseguire la pulizia di un host.")
   public static final String CANT_CLEAN_HOSTS = "96";

   @RBEntry("Eliminare la cartella radice selezionata e tutti i relativi mount?")
   public static final String SURE_ABOUT_ROOT_FOLDERS_AND_MOUNTS = "442";

   @RBEntry("Eliminare la cartella radice selezionata?")
   public static final String SURE_DELETE_ROOT_FOLDER = "443";

   @RBEntry("Aggiorna regole")
   public static final String UPDATE_POLICY_ITEMS = "454";

   @RBEntry("Le regole sono state aggiornate.")
   public static final String POLICY_ITEMS_UPDATED = "455";

   /**
    * AdminAddVault Messages
    *
    **/
   @RBEntry("Nessun nome di archivio specificato.")
   public static final String NO_VAULT_NAME_SPECIFIED = "97";

   @RBEntry("Immettere un nome di archivio.")
   public static final String PLEASE_ENTER_VAULT = "98";

   @RBEntry("Il nome di archivio non è univoco.")
   public static final String VAULT_NAME_INUSE = "99";

   /**
    * AdminAddHost messages
    *
    **/
   @RBEntry("Specificare un nome host.")
   public static final String SPECIFY_HOST = "100";

   @RBEntry("Specificare il nome del sito.")
   public static final String SPECIFY_SITE = "330";

   /**
    * AdminEditFolder messages
    *
    **/
   @RBEntry("Selezionare un archivio.")
   public static final String NO_VAULT_SELECTED = "101";

   @RBEntry("Specificare il nome della cartella.")
   public static final String NO_FOLDER_NAME = "102";

   @RBEntry("Specificare la capacità.")
   public static final String NO_CAPACITY = "103";

   @RBEntry("Specificare la soglia.")
   public static final String NO_THRESHOLD = "104";

   @RBEntry("La percentuale di soglia deve essere inferiore o uguale a 100.0 ")
   public static final String BAD_PERCENT = "105";

   @RBEntry("Il valore della soglia deve essere positivo.")
   public static final String NEG_THRESH = "106";

   @RBEntry("Le percentuali di soglia devono essere un numero a virgola mobile oppure un intero inferiore o uguale a 100.0 ")
   public static final String BAD_THRESH_FORMAT = "107";

   @RBEntry("Immettere un valore numerico.")
   public static final String NEED_NUMBERS = "108";

   @RBEntry("La soglia deve essere inferiore o uguale alla capacità. ")
   public static final String THRESH_TOO_BIG = "109";

   @RBEntry("Il valore della capacità deve essere positivo.")
   public static final String NEG_CAPACITY = "110";

   @RBEntry("La capacità deve essere specificata come valore numerico. ")
   public static final String CAPACITY_NOT_NUMBER = "111";

   /**
    * AdminAdd/Edit mount messages.
    *
    **/
   @RBEntry("Per i mount locali è necessario specificare un percorso")
   public static final String LOCAL_MOUNT_NO_PATH = "112";

   @RBEntry("Nessun percorso specificato")
   public static final String NO_PATH = "113";

   @RBEntry("Impossibile rimuovere una cartella contenente mount. ")
   public static final String CANNOT_REMOVE_FOLDER = "114";

   @RBEntry("Gli host selezionati contengono mount. Eliminare gli host e tutti i relativi mount. ")
   public static final String REMOVE_ALL_HOST_MOUNTS = "115";

   @RBEntry("La cartella è stata aggiornata da un altro utente.")
   public static final String FOLDER_STALE = "116";

   @RBEntry("Visualizza tutti i mount possibili")
   public static final String ALL_POSSIBLE_MOUNTS = "117";

   @RBEntry("Configurazione archivi")
   public static final String FVADMIN_TITLE = "118";

   @RBEntry("Eliminare l'host selezionato?")
   public static final String SURE_DELETE_HOST = "119";

   @RBEntry("Eliminare l'archivio selezionato?")
   public static final String SURE_DELETE_VAULT = "120";

   @RBEntry("Eliminare la cartella selezionata?")
   public static final String SURE_DELETE_FOLDER = "121";

   @RBEntry("L'operazione rimuoverà in modo permanente i file dal sistema.  Si consiglia di eseguire il backup di tutti i file dell'archivio prima di continuare l'operazione. Continuare? ")
   public static final String SURE_CLEAN = "122";

   @RBEntry("***")
   public static final String NO_DATA = "123";

   @RBEntry("Genera informazioni di backup")
   public static final String BACKUP_DATA = "124";

   @RBEntry("Informazioni di backup")
   public static final String BACKUPS = "125";

   @RBEntry("Scarica")
   public static final String BACKUP_DOWNLOAD = "126";

   @RBEntry("Nessuna autorizzazione per accedere a questa schermata ")
   public static final String NOT_AUTHORIZED = "127";

   @RBEntry("Informazioni di backup generate")
   public static final String BACKUP_GENERATED = "128";

   @RBEntry("File senza riferimenti rimossi dall'archivio")
   public static final String VAULT_CLEANED = "129";

   @RBEntry("File senza riferimenti rimossi dalla cartella")
   public static final String FOLDER_CLEANED = "130";

   @RBEntry("Impossibile trovare il mount.")
   public static final String MOUNT_NOT_FOUND = "131";

   @RBEntry("Salvataggio in corso...")
   public static final String SAVING = "210";

   @RBEntry("Salvataggio eseguito")
   public static final String SAVED = "211";

   @RBEntry("Eliminato")
   public static final String DELETED = "212";

   /**
    * Exceptions for creating or modifying file vault rules
    *
    **/
   @RBEntry("Impossibile inizializzare la Guida in linea:")
   public static final String HELP_INITIALIZATION_FAILED = "201";

   @RBEntry("Si è verificata un'eccezione durante l'inizializzazione dei dati:")
   public static final String INITIALIZATION_FAILED = "200";

   @RBEntry("Selezionare una classe, uno stato del ciclo di vita e un archivio file.")
   public static final String MISSING_FV_INPUT = "202";

   @RBEntry("Impossibile accedere alle regole di archiviazione file.")
   public static final String RETRIEVE_FAILED = "203";

   @RBEntry("Impossibile eliminare le regole di archiviazione file.")
   public static final String DELETE_FAILED = "204";

   @RBEntry("Impossibile inizializzare gli applet listener:")
   public static final String APPLET_LISTENERS_NOT_INITIALIZED = "205";

   @RBEntry("?")
   public static final String QUESTION_MARK = "414";

   @RBEntry("Dominio:")
   public static final String DOMAIN_LABEL = "215";

   @RBEntry("Impossibile rimuovere gli applet listeners: ")
   public static final String REMOVING_LISTENER_FAILED = "216";

   @RBEntry("Regole di archiviazione del dominio")
   public static final String DOMAIN_TITLE = "217";

   @RBEntry("Eliminare il mount selezionato?")
   public static final String REM_MOUNT = "213";

   @RBEntry("Archiviazione temporizzata")
   public static final String REVAULT = "214";

   @RBEntry("Dominio")
   @RBComment("message for creating or modifying file vault rules")
   public static final String DOMAIN = "360";

   @RBEntry("Tutto")
   public static final String ALL = "361";

   @RBEntry("Classe completa")
   public static final String FULL_CLASS = "362";

   @RBEntry("Classe")
   public static final String CLASS = "363";

   @RBEntry("Stato del ciclo di vita")
   public static final String STATE = "364";

   @RBEntry("Archivio master")
   public static final String FILE_VAULT = "365";

   @RBEntry("Regola di archiviazione")
   public static final String VAULTING_RULE = "366";

   @RBEntry("Archiviazione")
   public static final String VAULTING = "367";

   @RBEntry("Recupera")
   public static final String RETRIEVE = "371";

   @RBEntry("Nuovo")
   public static final String CREATE = "372";

   @RBEntry("Report")
   public static final String REPORT = "373";

   @RBEntry("Classi")
   public static final String CLASSES = "380";

   @RBEntry("Stati del ciclo di vita")
   public static final String STATES = "381";

   @RBEntry("Archivi master")
   public static final String FILE_VAULTS = "382";

   @RBEntry("Cartelle replica")
   public static final String REMOTE_FOLDERS = "383";

   @RBEntry("Errore durante l'aggiornamento: ")
   public static final String REFRESH_FAILED = "350";

   @RBEntry("Sito")
   public static final String SITE = "351";

   @RBEntry("Archivio replica")
   public static final String REMOTE_VAULT = "352";

   @RBEntry("Archiviazione replica")
   public static final String REMOTE_VAULTING = "353";

   @RBEntry("Domini amministrativi")
   public static final String ADMINISTRATIVE_DOMAINS = "354";

   @RBEntry("Visualizza")
   public static final String VIEW = "355";

   @RBEntry("Reimposta")
   public static final String CLEAR = "384";

   @RBEntry("Visualizzazione")
   public static final String DISPLAY = "385";

   @RBEntry("Elenca valori")
   public static final String LIST_VALUES = "386";

   @RBEntry("Evento")
   public static final String EVENT = "387";

   @RBEntry("Elenco")
   public static final String LIST = "388";

   @RBEntry("Report archivio file")
   public static final String FILE_VAULT_REPORT = "389";

   @RBEntry("Report archivio replica")
   public static final String REMOTE_VAULT_REPORT = "390";

   @RBEntry("Nome")
   public static final String NAME = "391";

   @RBEntry("Master")
   public static final String LOCAL = "392";

   @RBEntry("Sincronizzato")
   public static final String SYNCHED = "393";

   @RBEntry("L'ultima volta")
   public static final String LAST_TIME = "394";

   @RBEntry("L'ultimo stato")
   public static final String LAST_STATUS = "395";

   @RBEntry("La prossima volta")
   public static final String NEXT_TIME = "396";

   @RBEntry("Impossibile resettare questo tipo di oggetto")
   public static final String CANT_RESET = "397";

   @RBEntry("Reset delle regole di replica")
   public static final String REPL_RESET = "398";

   @RBEntry("Impossibile spostare l'archivio replica con le cartelle.")
   public static final String CANT_REMOVE_REMVAULT = "399";

   @RBEntry("Cartella Replica")
   public static final String REMOTE_FOLDER = "400";

   @RBEntry("Regole di replica di dati")
   public static final String REMOTE_VAULT_RULE = "401";

   @RBEntry("Trasmetti configurazione")
   public static final String BROADCAST = "402";

   @RBEntry("L'host non è stato selezionato")
   public static final String NO_HOST_SELECTED = "403";

   @RBEntry("La cartella non è stata selezionata")
   public static final String NO_FOLDER_SELECTED = "404";

   @RBEntry("Impossibile resettare il sito locale")
   public static final String CANT_RESET_LOCAL = "405";

   @RBEntry("Se si resettano le regole di replica, alla replica successiva, tutto il contenuto verrà replicato. Procedere?")
   public static final String SURE_ABOUT_RESET = "406";

   @RBEntry("Eliminare il sito selezionato?")
   public static final String SURE_DELETE_SITE = "407";

   @RBEntry("Tutti i siti")
   public static final String ALL_SITES = "408";

   @RBEntry("Abilita/disabilita")
   public static final String TOGGLE_ENABLED = "409";

   @RBEntry("Programma la replica di dati")
   public static final String SCHED_CONT_REPL = "410";

   @RBEntry("Reimposta replica")
   public static final String RESET_CONT_REPL = "411";

   @RBEntry("Reimposta le repliche non inviate")
   public static final String RESET_UNDELIVERED = "412";

   @RBEntry("Amministrazione memorizzazione esterna")
   public static final String ES_ADMIN_TITLE = "413";

   @RBEntry("Archivio cache")
   public static final String DESIGNATED_FOR_CACHE = "132";

   @RBEntry("Cache")
   public static final String CACHE = "133";

   @RBEntry("Impossibile creare l'archivio designato per la cache di contenuto sul sito principale.")
   public static final String CANT_CREATE_MASTERED_VAULT_ON_MASTER = "134";

   @RBEntry("Impossibile designare più di un archivio per la cache di contenuto.")
   public static final String CANT_CREATE_TWO_MASTERED_VAULTS = "135";

   @RBEntry("Errore archivio")
   public static final String VAULT_ERROR = "136";

   @RBEntry("Sito:")
   public static final String NEW_VAULT_SITE = "137";

   @RBEntry("Avvertenza archivio")
   public static final String VAULT_WARNING = "138";

   @RBEntry("Non esiste un archivio designato per la cache dei dati nel master locale. Verificare immediatamente che un altro archivio sul master locale sia stato designato.")
   public static final String NO_MASTERED_VAULT_ON_LOCAL_MASTER = "139";

   @RBEntry("L'archivio designato per la cache dei dati sul master locale è stato rimosso. Verificare immediatamente la designazione di un altro archivio sul master locale.")
   public static final String MASTERED_VAULT_ON_LOCAL_MASTER_REMOVED = "140";

   @RBEntry("L'archivio designato per la cache dei dati sul master locale è contrassegnato come sola lettura. Verificare immediatamente la designazione di un altro archivio sul master locale.")
   public static final String MASTERED_VAULT_ON_LOCAL_MASTER_READONLY = "141";

   @RBEntry("L'archivio ha una programmazione e non può essere rimosso")
   public static final String HAS_SCHEDULE_ON_VAULT = "415";

   @RBEntry("Impossibile attivare la cartella, mount mancanti.")
   public static final String ENABLE_FOLDER_CHECKBOX = "416";

   @RBEntry("Nessun mount convalidato. Consultare i log per i dettagli.")
   public static final String NO_MOUNT_TO_VALIDATE = "417";

   @RBEntry("Alcuni mount non sono validi")
   public static final String INVALID_MOUNT_STATUS = "418";

   @RBEntry("Convalida mount parzialmente completata. Consultare il log per dettagli.")
   public static final String PARTIAL_MOUNT_VALIDATION = "419";

   @RBEntry("Alcuni mount non sono validi e alcune delle cartelle corrispondenti ai mount da convalidare non sono attivate.")
   public static final String PARTIAL_VALIDATATION_AND_INVALID_MOUNT_STATUS = "420";

   @RBEntry("Convalida mount completata")
   public static final String VALIDATION_SUCCEED = "421";

   @RBEntry("Un archivio selezionato per il caricamento non può essere selezionato come destinazione di default a meno che non sia stato selezionato per lo scaricamento")
   public static final String DEFAULT_TARGET_CANT_BE_FOR_UPLOAD = "422";

   @RBEntry("L'archivio {0} è stato selezionato in precedenza come destinazione di defautl per il sito. Se si imposta l'archivio come destinazione di default per il sito e lo si salva, tale selezione verrà annullata.")
   public static final String EARLIER_DEFAULT_TARGET_SWITCHED = "423";

   @RBEntry("È obbligatorio che un archivio sia selezionato come destinazione di default per il sito")
   public static final String SITE_NEEDS_A_DEFAULT_TARGET = "424";

   @RBEntry("È necessario scegliere un archivio di replica 'Per il contenuto in cache' o 'Per il contenuto replicato'")
   public static final String REPLICA_VAULT_SHLD_BE_FOR_UPLOAD_OR_DWNLD = "426";

   @RBEntry("Cartella radice")
   public static final String ROOT_FOLDER = "427";

   @RBEntry("È necessario scegliere un archivio di replica 'Per il contenuto in cache' o 'Per il contenuto replicato'")
   public static final String AT_LEAST_ONE_SELECTED_FOR_REPLICA_VLT = "430";

   @RBEntry("Cartella master")
   public static final String FOLDER = "432";

   @RBEntry("Archivio replica")
   public static final String FOR_REPLICATED_CONTENT = "433";

   @RBEntry("Tipo di archivio:")
   public static final String TARGET_FOR = "434";

   @RBEntry("Destinazione di default per il sito")
   public static final String DEFAULT_TARGET_FOR_SITE = "435";

   @RBEntry("Creazione automatica cartelle")
   public static final String AUTO_FOLDER_CREATE = "436";

   @RBEntry("Percorso radice")
   public static final String ROOT_PATH = "437";

   @RBEntry("Impossibile effettuare il mount della cartella in un archivio per cui la proprietà 'Creazione automatica cartelle' sia attivata")
   public static final String CANNOT_SELECT_AUTO_FOLDER_CREATE_VAULT = "438";

   @RBEntry("Archivio")
   public static final String VAULT = "439";

   @RBEntry("Mount già effettuato per il percorso a un altra cartella o al mount di una cartella radice. Scegliere un percorso differente.")
   public static final String PATH_ALREADY_USED = "440";

   @RBEntry("Eliminare questa regola? Parametri della regola:")
   public static final String DELETE_RULE = "441";

   @RBEntry("Sposta i file invece di eliminarli")
   public static final String MOVE = "444";

   @RBEntry("L'azione elimina permanentemente i file dal sistema. È possibile scegliere di spostare i file in modo che sia possibile eseguirne il backup, quindi eliminarli manualmente. Continuare?")
   public static final String SURE_MOVE = "445";

   @RBEntry("Impossibile eliminare un archivio selezionato come destinazione di default per il sito")
   public static final String CANNOT_DELETE_DEFAULT_TARGET_VAULT = "446";

   @RBEntry("Archivio master")
   public static final String MASTER_VAULT = "447";

   @RBEntry("Destinazione sistema di default")
   public static final String DEFAULT_SYSTEM_TARGET = "448";

   @RBEntry("È necessario selezionare un archivio master come destinazione di default per il sistema.")
   public static final String SITE_NEEDS_A_DEFAULT_SYS_TARGET = "449";

   @RBEntry("L'archivio {0} è stato selezionato in precedenza come destinazione di defautl per il sistema. Se si imposta l'archivio come destinazione di default per il sistema e lo si salva, tale selezione verrà annullata.")
   public static final String EARLIER_DEFAULT_SYS_TARGET_SWITCHED = "450";

   @RBEntry("Impossibile eliminare un archivio selezionato come destinazione di default per il sistema.")
   public static final String CANNOT_DELETE_DEFAULT_SYS_TARGET = "451";

   @RBEntry("Tutti gli archivi")
   public static final String ALL_VAULTS = "452";

   @RBEntry("L'eliminazione in background dei file senza riferimenti è stata avviata. Per conoscere lo stato dell'operazione, vedere il Gestore eventi.")
   public static final String REMOVE_UNREFERENCED_FILES_JOB_INITIATED = "453";

   @RBEntry("Definisci regole di pulizia del contenuto")
   public static final String AUTO_VAULT_CLEANUP_MENU_LABEL = "456";

   @RBEntry("Pulizia automatica degli archivi di replica")
   public static final String AUTO_CLEANUP_TITLE = "457";

   @RBEntry("Sito:")
   public static final String AUTO_CLEANUP_SITENAME = "458";

   @RBEntry("Regole di pulizia")
   public static final String AUTO_CLEANUP_RULES = "459";

   @RBEntry("Limita la dimensione dell'archivio a")
   public static final String AUTO_CLEANUP_SIZE_RULE = "460";

   @RBEntry("Regola temporale")
   public static final String AUTO_CLEANUP_AGE_RULE = "461";

   @RBEntry("Annulla i riferimenti al contenuto non utilizzato da")
   public static final String AUTO_CLEAN_AGE_RULE_DEREF = "476";

   @RBEntry("% di spazio totale disponibile su disco")
   public static final String AUTO_CLEANUP_PCT_SIZE = "462";

   @RBEntry("MB")
   public static final String AUTO_CLEANUP_SIZE_OPTION_MB = "463";

   @RBEntry("GB")
   public static final String AUTO_CLEANUP_SIZE_OPTION_GB = "464";

   @RBEntry("giorni")
   public static final String AUTO_CLEANUP_AGE_OPTION_DAYS = "465";

   @RBEntry("settimane")
   public static final String AUTO_CLEANUP_AGE_OPTION_WEEKS = "466";

   @RBEntry("mesi")
   public static final String AUTO_CLEANUP_AGE_OPTION_MONTHS = "467";

   @RBEntry("Programmazione pulizia:")
   public static final String AUTO_CLEANUP_SCHEDULE = "468";

   @RBEntry("Esegui ogni")
   public static final String AUTO_CLEANUP_SCHEDULE_RUN = "469";

   @RBEntry("giorni")
   public static final String AUTO_CLEANUP_DAYS_AT = "470";

   @RBEntry("AM")
   public static final String AUTO_CLEANUP_SCHEDULE_AM = "471";

   @RBEntry("PM")
   public static final String AUTO_CLEANUP_SCHEDULE_PM = "472";

   @RBEntry("Prossima esecuzione programmata:")
   public static final String AUTO_CLEANUP_NEXT_SCHEDULE = "473";

   @RBEntry("Esegui pulizia ora")
   public static final String AUTO_CLEANUP_RUN_IMM = "474";

   @RBEntry("Pulizia automatica di contenuto meno recente")
   public static final String INCLUDE_FOR_AUTO_CLEANUP_TITLE = "475";

   @RBEntry("ATTENZIONE: il valore immesso non è valido. \nProgrammazione della pulizia: immettere un valore compreso tra 0 e 23 per le ore e tra 0 e 59 per i minuti.")
   public static final String ERR_MSG_SCHED_HOURS = "477";

   @RBEntry("ATTENZIONE: il valore immesso non è valido. \nProgrammazione della pulizia: immettere un valore compreso tra 0 e 23 per le ore e tra 0 e 59 per i minuti.")
   public static final String ERR_MSG_SCHED_MINS = "478";

   @RBEntry("Errore di recupero del fuso orario locale")
   public static final String ERR_TIME_ZONE_GETTER = "479";

   @RBEntry("ATTENZIONE: informazioni necessarie mancanti.\nRegole di pulizia: selezionare almeno una regola per la programmazione della pulizia.")
   public static final String ERR_MSG_AUTO_CLEANUP_CRITERIA = "480";

   @RBEntry("ATTENZIONE: informazioni necessarie mancanti.\nRegole di pulizia: per limitare la dimensione dell'archivio, immetterne la dimensione massima.")
   public static final String ERR_MSG_AUTO_CLEANUP_SIZE_ABS_VAL = "481";

   @RBEntry("Immettere un valore percentuale finito per la regola dimensionale")
   public static final String ERR_MSG_AUTO_CLEANUP_SIZE_PERC_VAL = "482";

   @RBEntry("ATTENZIONE: informazioni necessarie mancanti.\nRegole di pulizia: per annullare i riferimenti ai file di contenuto replicato in base all'ultimo accesso, specificare un periodo di tempo.")
   public static final String ERR_MSG_AUTO_CLEANUP_AGE_VAL = "483";

   @RBEntry("ATTENZIONE: informazioni necessarie mancanti.\nRegole di pulizia: indicare l'ora per la programmazione della pulizia.")
   public static final String ERR_MSG_AUTO_CLEANUP_SCHED_TIME_HRS = "484";

   @RBEntry("ATTENZIONE: informazioni necessarie mancanti.\nRegole di pulizia: indicare l'ora per la programmazione della pulizia.")
   public static final String ERR_MSG_AUTO_CLEANUP_SCHED_TIME_MINS = "485";

   @RBEntry("Il valore assoluto specificato per la regola dimensionale non rientra nell'intervallo consentito")
   public static final String ERR_MSG_AUTO_CLEANUP_SIZE_ABS_VALUE_OVFL = "486";

   @RBEntry("ATTENZIONE: è stato raggiunto il limite massimo per il valore dei giorni dell'ultimo accesso nella regola di pulizia.")
   public static final String ERR_MSG_AUTO_CLEANUP_AGE_DAYS_OVFL = "487";

   @RBEntry("ATTENZIONE: il valore di frequenza della programmazione della pulizia non rientra nell'intervallo consentito")
   public static final String ERR_MSG_AUTO_CLEANUP_SCHED_PERIODICITY_OVFL = "488";

   @RBEntry("Immettere solo numeri interi diversi da zero")
   public static final String ERR_MSG_ONLY_NON_ZERO_INTS = "489";

   @RBEntry("Immettere solo numeri decimali finiti per il campo di valore assoluto della regola dimensionale")
   public static final String ERR_MSG_DECIMALS_ONLY_ABS_SIZE = "490";

   @RBEntry("Immettere solo numeri decimali compresi tra 0 e 100,0 per il campo di valore percentuale della regola dimensionale")
   public static final String ERR_MSG_DECIMALS_ONLY_PERC_SIZE = "491";

   @RBEntry("Specificare un orario valido per l'esecuzione della pulizia automatica dell'archivio")
   public static final String ERR_MSG_AUTO_CLEANUP_TIME = "492";

   @RBEntry("dd/MM/yyyy, HH.mm")
   public static final String AUTO_CLEANUP_NEXT_SCHEDULE_DATE_FORMAT="493";

   @RBEntry("ATTENZIONE: se si deseleziona la casella di controllo Programmazione pulizia, le operazioni di pulizia non verranno eseguite per gli archivi di replica di questo sito.\nContinuare?")
   public static final String AUTO_CLEANUP_SCHEDULE_DISABLE_WARNING="494";

   @RBEntry("Avviso")
   public static final String WARNING_MESSAGE_DIALOG_TITLE="495";

   @RBEntry("Avvertenza: il valore verrà arrotondato a due cifre dopo il separatore decimale")
   public static final String ONLY_TWO_DIGITS_AFTER_DECIMAL="496";

   @RBEntry("Cartella master")
   public static final String MASTER_FOLDER="497";

   @RBEntry("Archivio replica")
   public static final String VAULT_SIZE_COLUMN_HEADING_REP_VAULT = "500";

   @RBEntry("Dimensione (MB)")
   public static final String VAULT_SIZE_COLUMN_HEADING_VAULT_SIZE = "501";

   @RBEntry("Dimensioni archivio:")
   public static final String TITLE_VAULT_SIZES = "502";

   @RBEntry("Stato ultima esecuzione:")
   public static final String LATEST_RUN_STATUS_LBL = "503";

   @RBEntry("* Questa utilità consente di pulire i file di contenuto replicato usati meno di recente \nfino a quando non vengono soddisfatti i criteri di pulizia specificati. Per un archivio con \ncartelle montate su più unità, ciò non comporta la liberazione di spazio nella cartella attiva")
   public static final String CLEANUP_INFO_MSG = "508";

   @RBEntry("Avvio della pulizia degli archivi di replica sul sito {0}")
   public static final String NOW_RUNNING_IMM = "510";

   @RBEntry("Log ultima esecuzione: ")
   public static final String LOGS_TITLE = "514";

   @RBEntry("Scarica")
   public static final String VIEW_LOGS = "515";

   @RBEntry("ATTENZIONE: il valore del campo del valore assoluto della regola basata sulle dimensioni per la programmazione della pulizia è fuori dall'intervallo")
   public static final String ERR_MSG_AUTO_CLEANUP_SCHED_ABS_SIZE_OVFL = "516";

   @RBEntry("Esegui una sola volta")
   public static final String RUN_ONCE = "517";

   @RBEntry("Esegui alle")
   public static final String RUN_AT = "518";

   @RBEntry("ATTENZIONE: informazioni necessarie mancanti.\nRegole di pulizia: specificare una frequenza valida per la programmazione della pulizia")
   public static final String ERR_MSG_AUTO_CLEANUP_PERIODICITY = "519";

   @RBEntry("ATTENZIONE: selezionare la programmazione per la pulizia automatica dell'archivio")
   public static final String ERR_MSG_AUTO_CLEANUP_SCHEDULE = "520";

   @RBEntry("ATTENZIONE: informazioni necessarie mancanti.\nRegole di pulizia: selezionare almeno una regola prima di eseguire un'operazione di pulizia.")
   public static final String ERR_MSG_AUTO_CLEANUP_CRITERIA_IMMEDIATE = "521";

   @RBEntry("ATTENZIONE: informazioni necessarie mancanti.")
   public static final String ERR_MSG_COMMON = "522";

   @RBEntry("'Pronto'")
   public static final String READY_STATE = "523";

   @RBEntry("'In esecuzione'")
   public static final String IN_PROGRESS = "524";

   @RBEntry("ATTENZIONE: creazione mount non riuscita. \nIl valore per il percorso di mount non deve superare i 180 caratteri")
   public static final String NEW_MOUNT_PATH_EXCEEDS_LIMIT = "525";

   @RBEntry("ATTENZIONE: aggiornamento mount non riuscito. \nIl valore per il percorso di mount non deve superare i 180 caratteri")
   public static final String UPDATE_MOUNT_PATH_EXCEEDS_LIMIT = "526";

   @RBEntry("ATTENZIONE: creazione mount non riuscita. \nImpossibile utilizzare percorsi di mount duplicati.")
   public static final String NEW_MOUNT_PATH_DUPLICATE_PATH_ERROR = "527";
   
   @RBEntry("Tipo di host:")
   public static final String HOST_TYPE = "528";
   
   @RBEntry("Il nuovo host di tipo cluster aggiornerà il mount dell'host con quello del cluster.")
   public static final String NEW_CLUSTER_HOST_WARNING = "529";
   
   @RBEntry("L'aggiornamento dell'host comporta la conversione del tipo di host da indipendente a cluster.")
   public static final String UPDATE_HOST_TO_CLUSTER = "530";
   
   @RBEntry("L'aggiornamento dell'host comporta la conversione del tipo di host da cluster a indipendente.")
   public static final String UPDATE_HOST_TO_STANDALONE = "531";
 
   @RBEntry("Il montaggio nell'host ha effetto su tutti i nodi del cluster.")
   public static final String NEW_MOUNT_ON_CLUSTER_WARNING = "532";
   
   @RBEntry("L'aggiornamento del mount nell'host ha effetto su tutti i nodi del cluster.")
   public static final String UPDATE_MOUNT_ON_CLUSTER_WARNING = "533";
   
   @RBEntry("Smontare la cartella selezionata da tutti i nodi del cluster?")
   public static final String REMOVE_MOUNT_ON_CLUSTER_WARNING = "534";

}



