/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.filevault;

import wt.util.resource.*;

@RBUUID("wt.clients.filevault.AdminEditVaultHelpRB")
public final class AdminEditVaultHelpRB_it extends WTListResourceBundle {
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_0 = "Contents/filevault";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_1 = "Help/filevault";

   @RBEntry("FileVaultConfigUpdateVault")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/filevault/AdminEditVault";

   @RBEntry("FileVaultConfigUpdateVault")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_3 = "Help/filevault/PDM_AdminEditVault";

   @RBEntry("FileVaultConfigUpdateVault")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_4 = "Help/filevault/PJL_AdminEditVault";

   /**
    * Status bar messages
    *
    **/
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_5 = "Desc/filevault/AdminEditVault";

   @RBEntry("Fare clic per salvare le modifiche e chiudere la finestra")
   public static final String PRIVATE_CONSTANT_6 = "Desc/filevault/AdminEditVault/OK";

   @RBEntry("Fare clic per chiudere la finestra senza salvare le modifiche")
   public static final String PRIVATE_CONSTANT_7 = "Desc/filevault/AdminEditVault/Cancel";

   @RBEntry("Fare clic per visualizzare le informazioni della Guida relative all'aggiornamento degli attributi dell'archivio file")
   public static final String PRIVATE_CONSTANT_8 = "Desc/filevault/AdminEditVault/Help";

   @RBEntry("Sposta la cartella selezionata all'inizio dell'elenco")
   public static final String PRIVATE_CONSTANT_9 = "Desc/filevault/AdminEditVault/MoveTop";

   @RBEntry("Sposta la cartella selezionata verso l'alto nell'elenco")
   public static final String PRIVATE_CONSTANT_10 = "Desc/filevault/AdminEditVault/MoveUp";

   @RBEntry("Sposta la cartella selezionata verso il basso nell'elenco")
   public static final String PRIVATE_CONSTANT_11 = "Desc/filevault/AdminEditVault/MoveDown";

   @RBEntry("Sposta la cartella selezionata alla fine dell'elenco")
   public static final String PRIVATE_CONSTANT_12 = "Desc/filevault/AdminEditVault/MoveBottom";

   @RBEntry("Attiva lo stato di sola lettura dell'archivio")
   public static final String PRIVATE_CONSTANT_13 = "Desc/filevault/AdminEditVault/ReadOnly";

   @RBEntry("Consente di modificare il nome dell'archivio")
   public static final String PRIVATE_CONSTANT_14 = "Desc/filevault/AdminEditVault/VaultName";

   @RBEntry("Consente di modificare il tipo dell'archivio")
   public static final String PRIVATE_CONSTANT_15 = "Desc/filevault/AdminEditVault/VaultTypeChoice";

   @RBEntry("Se selezionata, nell'archivio verranno create automaticamente cartelle")
   public static final String PRIVATE_CONSTANT_16 = "Desc/filevault/AdminEditVault/AutomaticFolder";

   @RBEntry("Imposta come archivio di destinazione replica di default per il sito")
   public static final String PRIVATE_CONSTANT_17 = "Desc/filevault/AdminEditVault/DefaultTargetForSite";

   @RBEntry("Imposta come archivio di destinazione per gli oggetti senza una regola corrispondente")
   public static final String PRIVATE_CONSTANT_18 = "Desc/filevault/AdminEditVault/DefaultSystemTarget";

   @RBEntry("Elenco delle cartelle presenti nell'archivio")
   public static final String PRIVATE_CONSTANT_19 = "Desc/filevault/AdminEditVault/FolderList";

   @RBEntry("Visualizza tutte le cartelle dell'archivio accessibili in scrittura")
   public static final String PRIVATE_CONSTANT_20 = "Desc/filevault/AdminEditVault/ShowWritable";

   @RBEntry("Visualizza tutte le cartelle dell'archivio")
   public static final String PRIVATE_CONSTANT_21 = "Desc/filevault/AdminEditVault/ShowAll";

   @RBEntry("Include l'archivio nelle operazioni di pulizia automatica in base alle regole definite")
   public static final String PRIVATE_CONSTANT_22 = "Desc/filevault/AdminEditVault/AutoVaultCleanup";

}
