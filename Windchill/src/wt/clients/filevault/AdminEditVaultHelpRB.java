/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.filevault;

import wt.util.resource.*;

@RBUUID("wt.clients.filevault.AdminEditVaultHelpRB")
public final class AdminEditVaultHelpRB extends WTListResourceBundle {
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_0 = "Contents/filevault";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_1 = "Help/filevault";

   @RBEntry("FileVaultConfigUpdateVault")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/filevault/AdminEditVault";

   @RBEntry("FileVaultConfigUpdateVault")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_3 = "Help/filevault/PDM_AdminEditVault";

   @RBEntry("FileVaultConfigUpdateVault")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_4 = "Help/filevault/PJL_AdminEditVault";

   /**
    * Status bar messages
    *
    **/
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_5 = "Desc/filevault/AdminEditVault";

   @RBEntry("Click to save your changes and close the window")
   public static final String PRIVATE_CONSTANT_6 = "Desc/filevault/AdminEditVault/OK";

   @RBEntry("Click to close the window without saving changes")
   public static final String PRIVATE_CONSTANT_7 = "Desc/filevault/AdminEditVault/Cancel";

   @RBEntry("Click to view the help on updating file vault attributes")
   public static final String PRIVATE_CONSTANT_8 = "Desc/filevault/AdminEditVault/Help";

   @RBEntry("Move the selected folder to the top of the folder list")
   public static final String PRIVATE_CONSTANT_9 = "Desc/filevault/AdminEditVault/MoveTop";

   @RBEntry("Move the selected folder up in the folder list")
   public static final String PRIVATE_CONSTANT_10 = "Desc/filevault/AdminEditVault/MoveUp";

   @RBEntry("Move the selected folder down in the folder list")
   public static final String PRIVATE_CONSTANT_11 = "Desc/filevault/AdminEditVault/MoveDown";

   @RBEntry("Move the selected folder to the bottom of the folder list")
   public static final String PRIVATE_CONSTANT_12 = "Desc/filevault/AdminEditVault/MoveBottom";

   @RBEntry("Toggle read only status of vault")
   public static final String PRIVATE_CONSTANT_13 = "Desc/filevault/AdminEditVault/ReadOnly";

   @RBEntry("Modify the name of the vault")
   public static final String PRIVATE_CONSTANT_14 = "Desc/filevault/AdminEditVault/VaultName";

   @RBEntry("Modify the type of the vault")
   public static final String PRIVATE_CONSTANT_15 = "Desc/filevault/AdminEditVault/VaultTypeChoice";

   @RBEntry("If checked, the vault automatically creates folders")
   public static final String PRIVATE_CONSTANT_16 = "Desc/filevault/AdminEditVault/AutomaticFolder";

   @RBEntry("Set as the default replication target vault for the site")
   public static final String PRIVATE_CONSTANT_17 = "Desc/filevault/AdminEditVault/DefaultTargetForSite";

   @RBEntry("Set as target vault for objects without a matching rule")
   public static final String PRIVATE_CONSTANT_18 = "Desc/filevault/AdminEditVault/DefaultSystemTarget";

   @RBEntry("List of folders in the vault")
   public static final String PRIVATE_CONSTANT_19 = "Desc/filevault/AdminEditVault/FolderList";

   @RBEntry("Show all writable folders in vault")
   public static final String PRIVATE_CONSTANT_20 = "Desc/filevault/AdminEditVault/ShowWritable";

   @RBEntry("Show all folders in vault")
   public static final String PRIVATE_CONSTANT_21 = "Desc/filevault/AdminEditVault/ShowAll";

   @RBEntry("Include vault in auto-cleanup operations as per defined rules")
   public static final String PRIVATE_CONSTANT_22 = "Desc/filevault/AdminEditVault/AutoVaultCleanup";

}
