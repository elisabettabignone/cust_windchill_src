/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.filevault;

import wt.util.resource.*;

@RBUUID("wt.clients.filevault.AdminAddMountHelpRB")
public final class AdminAddMountHelpRB_it extends WTListResourceBundle {
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_0 = "Contents/filevault";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_1 = "Help/filevault";

   @RBEntry("FileVaultConfigUpdateMount")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/filevault/AdminAddMount";

   @RBEntry("VaultCreateMount")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_3 = "Help/filevault/PDM_AdminAddMount";

   @RBEntry("VaultCreateMount")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_4 = "Help/filevault/PJL_AdminAddMount";

   /**
    * Status bar messages
    * 
    **/
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_5 = "Desc/filevault/AdminAddMount";

   @RBEntry("Fare clic per salvare il mount e chiudere la finestra")
   public static final String PRIVATE_CONSTANT_6 = "Desc/filevault/AdminAddMount/OK";

   @RBEntry("Fare clic per chiudere la finestra senza salvare le modifiche")
   public static final String PRIVATE_CONSTANT_7 = "Desc/filevault/AdminAddMount/Cancel";

   @RBEntry("Fare clic per visualizzare le informazioni della Guida relative all'aggiornamento dei mount")
   public static final String PRIVATE_CONSTANT_8 = "Desc/filevault/AdminAddMount/Help";

   @RBEntry("Specificare il percorso per il mount")
   public static final String PRIVATE_CONSTANT_9 = "Desc/filevault/AdminAddMount/Path";

   @RBEntry("Selezionare una cartella")
   public static final String PRIVATE_CONSTANT_10 = "Desc/filevault/AdminAddMount/FolderChoice";

   @RBEntry("Selezionare un host")
   public static final String PRIVATE_CONSTANT_11 = "Desc/filevault/AdminAddMount/HostChoice";
}
