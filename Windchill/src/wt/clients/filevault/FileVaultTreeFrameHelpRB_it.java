/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.filevault;

import wt.util.resource.*;

@RBUUID("wt.clients.filevault.FileVaultTreeFrameHelpRB")
public final class FileVaultTreeFrameHelpRB_it extends WTListResourceBundle {
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_0 = "Contents/filevault";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_1 = "Help/filevault";

   @RBEntry("FileVaultConfigRef")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/filevault/FileVaultTreeFrame";

   @RBEntry("pdm_filevault")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_3 = "Help/filevault/PDM_FileVaultTreeFrame";

   @RBEntry("pjl_filevault")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_4 = "Help/filevault/PJL_FileVaultTreeFrame";

   /**
    * Status bar messages
    *
    **/
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_5 = "Desc/filevault/FileVaultTreeFrame";

   /**
    * button descriptions
    *
    **/
   @RBEntry("Aggiunge un nuovo host al sistema")
   public static final String PRIVATE_CONSTANT_6 = "Desc/filevault/FileVaultTreeFrame/newHost";

   @RBEntry("Aggiunge un nuovo archivio al sistema")
   public static final String PRIVATE_CONSTANT_7 = "Desc/filevault/FileVaultTreeFrame/newVault";

   @RBEntry("Aggiunge una nuova cartella al sistema")
   public static final String PRIVATE_CONSTANT_8 = "Desc/filevault/FileVaultTreeFrame/newFolder";

   @RBEntry("Elimina in modo permanente dal sistema l'oggetto selezionato")
   public static final String PRIVATE_CONSTANT_9 = "Desc/filevault/FileVaultTreeFrame/delete";

   @RBEntry("Aggiorna tutte le viste")
   public static final String PRIVATE_CONSTANT_10 = "Desc/filevault/FileVaultTreeFrame/refresh";

   @RBEntry("Aggiorna l'oggetto selezionato")
   public static final String PRIVATE_CONSTANT_11 = "Desc/filevault/FileVaultTreeFrame/update";

   @RBEntry("Convalida i mount dell'oggetto selezionato")
   public static final String PRIVATE_CONSTANT_12 = "Desc/filevault/FileVaultTreeFrame/validateObject";

   @RBEntry("Convalida il mount selezionato")
   public static final String PRIVATE_CONSTANT_13 = "Desc/filevault/FileVaultTreeFrame/validateMount";

   @RBEntry("Aggiorna il mount selezionato")
   public static final String PRIVATE_CONSTANT_14 = "Desc/filevault/FileVaultTreeFrame/updateMount";

   @RBEntry("Aggiunge un mount all'oggetto selezionato")
   public static final String PRIVATE_CONSTANT_15 = "Desc/filevault/FileVaultTreeFrame/mount";

   @RBEntry("Rimuove il mount selezionato")
   public static final String PRIVATE_CONSTANT_16 = "Desc/filevault/FileVaultTreeFrame/unmount";

   @RBEntry("Esegue la pulizia dell'oggetto")
   public static final String PRIVATE_CONSTANT_17 = "Desc/filevault/FileVaultTreeFrame/clean";

   /**
    * panel descriptions
    *
    **/
   @RBEntry("Fare doppio clic su un mount per aggiornarne gli attributi")
   public static final String PRIVATE_CONSTANT_18 = "Desc/filevault/FileVaultTreeFrame/mountList";

   @RBEntry("Fare doppio clic su un oggetto per aggiornarlo. Fare clic una sola volta per visualizzare i relativi mount")
   public static final String PRIVATE_CONSTANT_19 = "Desc/filevault/FileVaultTreeFrame/objectTree";

   /**
    * Check boxes
    *
    **/
   @RBEntry("Visualizza soltanto i mount esistenti")
   public static final String PRIVATE_CONSTANT_20 = "Desc/filevault/FileVaultTreeFrame/Existing";

   @RBEntry("Visualizza tutti i mount possibili")
   public static final String PRIVATE_CONSTANT_21 = "Desc/filevault/FileVaultTreeFrame/AllPossible";

   /**
    * Will need to add the tooltip descriptions
    *
    **/
   @RBEntry("Nuovo host")
   public static final String PRIVATE_CONSTANT_22 = "Tip/filevault/FileVaultTreeFrame/newHost";

   @RBEntry("Nuovo archivio")
   public static final String PRIVATE_CONSTANT_23 = "Tip/filevault/FileVaultTreeFrame/newVault";

   @RBEntry("Nuova cartella")
   public static final String PRIVATE_CONSTANT_24 = "Tip/filevault/FileVaultTreeFrame/newFolder";

   @RBEntry("Elimina l'oggetto selezionato")
   public static final String PRIVATE_CONSTANT_25 = "Tip/filevault/FileVaultTreeFrame/delete";

   @RBEntry("Aggiorna i dati")
   public static final String PRIVATE_CONSTANT_26 = "Tip/filevault/FileVaultTreeFrame/refresh";

   @RBEntry("Aggiorna l'oggetto selezionato")
   public static final String PRIVATE_CONSTANT_27 = "Tip/filevault/FileVaultTreeFrame/update";

   @RBEntry("Convalida i mount dell'oggetto selezionato")
   public static final String PRIVATE_CONSTANT_28 = "Tip/filevault/FileVaultTreeFrame/validateObject";

   @RBEntry("Convalida il mount selezionato")
   public static final String PRIVATE_CONSTANT_29 = "Tip/filevault/FileVaultTreeFrame/validateMount";

   @RBEntry("Aggiorna il mount selezionato")
   public static final String PRIVATE_CONSTANT_30 = "Tip/filevault/FileVaultTreeFrame/updateMount";

   @RBEntry("Aggiunge il mount all'oggetto selezionato")
   public static final String PRIVATE_CONSTANT_31 = "Tip/filevault/FileVaultTreeFrame/mount";

   @RBEntry("Rimuove il mount selezionato")
   public static final String PRIVATE_CONSTANT_32 = "Tip/filevault/FileVaultTreeFrame/unmount";

   @RBEntry("Esegue la pulizia dell'oggetto")
   public static final String PRIVATE_CONSTANT_33 = "Tip/filevault/FileVaultTreeFrame/clean";

   @RBEntry("Programmazione archiviazione temporizzata")
   public static final String PRIVATE_CONSTANT_34 = "Tip/filevault/FileVaultTreeFrame/revault";

   @RBEntry("Programmazione archiviazione temporizzata")
   public static final String PRIVATE_CONSTANT_35 = "Desc/filevault/FileVaultTreeFrame/revault";

   @RBEntry("Programma la rimozione dei file non referenziati")
   public static final String PRIVATE_CONSTANT_36 = "Tip/filevault/FileVaultTreeFrame/cleanup";

   @RBEntry("Programma la rimozione dei file non referenziati")
   public static final String PRIVATE_CONSTANT_37 = "Desc/filevault/FileVaultTreeFrame/cleanup";

   @RBEntry("Programma la replica di dati")
   public static final String PRIVATE_CONSTANT_38 = "Tip/filevault/FileVaultTreeFrame/contentrepl";

   @RBEntry("Programma la replica di dati")
   public static final String PRIVATE_CONSTANT_39 = "Desc/filevault/FileVaultTreeFrame/contentrepl";

   @RBEntry("Resetta la replica di dati")
   public static final String PRIVATE_CONSTANT_40 = "Tip/filevault/FileVaultTreeFrame/resetrepl";

   @RBEntry("Resetta la replica di dati")
   public static final String PRIVATE_CONSTANT_41 = "Desc/filevault/FileVaultTreeFrame/resetrepl";

   @RBEntry("Commuta lo stato di abilitazione di un oggetto")
   public static final String PRIVATE_CONSTANT_42 = "Tip/filevault/FileVaultTreeFrame/toggle";

   @RBEntry("Commuta lo stato di abilitazione di un oggetto")
   public static final String PRIVATE_CONSTANT_43 = "Desc/filevault/FileVaultTreeFrame/toggle";

   @RBEntry("FileVaultConfigAutoCleanup")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_44 = "Help/filevault/FileVaultConfigAutoCleanup";
}
