/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.filevault;

import wt.util.resource.*;

@RBUUID("wt.clients.filevault.FvHelpRB")
public final class FvHelpRB extends WTListResourceBundle {
   @RBEntry("WCAdminDomainPol")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_0 = "Help/Administrator/Fv";

   @RBEntry("Retrieve file vault policy rules for this domain")
   public static final String PRIVATE_CONSTANT_1 = "Desc/Administrator/Fv/FvRetrieve";

   @RBEntry("Define a new file vault policy rule")
   public static final String PRIVATE_CONSTANT_2 = "Desc/Administrator/Fv/FvCreate";

   @RBEntry("Change the selected file vault policy rule")
   public static final String PRIVATE_CONSTANT_3 = "Desc/Administrator/Fv/FvUpdate";

   @RBEntry("Delete the selected file vault rule")
   public static final String PRIVATE_CONSTANT_4 = "Desc/Administrator/Fv/FvDelete";

   @RBEntry("Display the file vault lists")
   public static final String PRIVATE_CONSTANT_5 = "Desc/Administrator/Fv/FvReport";

   @RBEntry("Display help for this window")
   public static final String PRIVATE_CONSTANT_6 = "Desc/Administrator/Fv/FvHelp";

   @RBEntry("FileVaultConfigAddVault")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_7 = "Help/Administrator/Vaulting";

   @RBEntry("Object classes within domain")
   public static final String PRIVATE_CONSTANT_8 = "Desc/Administrator/Vaulting/Classes";

   @RBEntry("Lifecycle state")
   public static final String PRIVATE_CONSTANT_9 = "Desc/Administrator/Vaulting/State";

   @RBEntry("Select site to filter vaults list")
   public static final String PRIVATE_CONSTANT_10 = "Desc/Administrator/Vaulting/Site";

   @RBEntry("File vaults")
   public static final String PRIVATE_CONSTANT_11 = "Desc/Administrator/Vaulting/FileVault";

   @RBEntry("Cancel all changes and close this window")
   public static final String PRIVATE_CONSTANT_12 = "Desc/Administrator/Vaulting/Cancel";

   @RBEntry("Save all changes and close this window")
   public static final String PRIVATE_CONSTANT_13 = "Desc/Administrator/Vaulting/Close";

   @RBEntry("Display help for creating vaulting rules")
   public static final String PRIVATE_CONSTANT_14 = "Desc/Administrator/Vaulting/Help";

   @RBEntry("Close this window")
   public static final String PRIVATE_CONSTANT_15 = "Desc/Administrator/Domain/Close";

   @RBEntry("vaultreplAbout")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_16 = "Help/Administrator/Repl";

   @RBEntry("Retrieve content replication rules for this domain.")
   public static final String PRIVATE_CONSTANT_17 = "Desc/Administrator/Repl/Retrieve";

   @RBEntry("Define new content replication rule.")
   public static final String PRIVATE_CONSTANT_18 = "Desc/Administrator/Repl/Create";

   @RBEntry("Change the selected content replication rule.")
   public static final String PRIVATE_CONSTANT_19 = "Desc/Administrator/Repl/Update";

   @RBEntry("Delete the selected content replication rule.")
   public static final String PRIVATE_CONSTANT_20 = "Desc/Administrator/Repl/Delete";

   @RBEntry("Display the replica vault lists")
   public static final String PRIVATE_CONSTANT_21 = "Desc/Administrator/Repl/Report";

   @RBEntry("Object classes within domain")
   public static final String PRIVATE_CONSTANT_22 = "Desc/Administrator/Repl/Classes";

   @RBEntry("Lifecycle state")
   public static final String PRIVATE_CONSTANT_23 = "Desc/Administrator/Repl/State";

   @RBEntry("Cancel all changes and close this window")
   public static final String PRIVATE_CONSTANT_24 = "Desc/Administrator/Repl/Cancel";

   @RBEntry("Save all changes and close this window")
   public static final String PRIVATE_CONSTANT_25 = "Desc/Administrator/Repl/Close";

   @RBEntry("Display help for this window")
   public static final String PRIVATE_CONSTANT_26 = "Desc/Administrator/Repl/Help";

   @RBEntry("Replica Vaults")
   public static final String PRIVATE_CONSTANT_27 = "Desc/Administrator/Repl/RemoteVault";

   @RBEntry("FileVaultWorkAdministrativeDomains")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_28 = "Help/Administrator/WorkAdministrativeDomains";

   @RBEntry("FileVaultCreateVaultingRule")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_29 = "Help/Administrator/CreateVaultingRule";

   @RBEntry("FileVaultViewDomainVaultingRules")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_30 = "Help/Administrator/ViewDomainVaultingRules";

   @RBEntry("FileVaultUpdateDomainVaultingRules")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_31 = "Help/Administrator/UpdateDomainVaultingRules";
   
   @RBEntry("FileVaultUpdateVaultingRule")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_32 = "Help/Administrator/UpdateVaultingRule";
}
