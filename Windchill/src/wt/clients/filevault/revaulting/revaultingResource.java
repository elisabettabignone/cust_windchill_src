/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.filevault.revaulting;

import wt.util.resource.*;

@RBUUID("wt.clients.filevault.revaulting.revaultingResource")
public final class revaultingResource extends WTListResourceBundle {
   @RBEntry("Revaulting")
   public static final String REVAULTING_TAB = "1";

   @RBEntry("External Storage Scheduling")
   public static final String REVAULTING_TITLE = "2";

   @RBEntry("Close")
   public static final String CLOSE_BUTT = "3";

   @RBEntry("Help")
   public static final String HELP_BUTT = "4";

   /**
    * Scheduling Framwork properties
    * 
    **/
   @RBEntry("Revaulting History")
   public static final String PRIVATE_CONSTANT_0 = "schedulelogtitle";

   @RBEntry("History of Revaulting operations:")
   public static final String PRIVATE_CONSTANT_1 = "schedulelogdescr";

   @RBEntry("Master Vault")
   public static final String PRIVATE_CONSTANT_2 = "schedulelogheader";

   @RBEntry("Revaulting Scheduler")
   public static final String PRIVATE_CONSTANT_3 = "scheduleTimeTitle";

   @RBEntry("Master Vault:")
   public static final String PRIVATE_CONSTANT_4 = "scheduleTimeObjectLab";
}