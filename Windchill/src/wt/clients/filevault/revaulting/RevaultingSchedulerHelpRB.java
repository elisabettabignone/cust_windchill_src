/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.filevault.revaulting;

import wt.util.resource.*;

@RBUUID("wt.clients.filevault.revaulting.RevaultingSchedulerHelpRB")
public final class RevaultingSchedulerHelpRB extends WTListResourceBundle {
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_0 = "Contents/revaulting";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_1 = "Help/revaulting";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_2 = "Contents/replication";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_3 = "Help/replication";

   @RBEntry("FileVaultRevaultScheduleExStor")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_4 = "Help/revaulting/RevaultingScheduler";

   @RBEntry("ExtStgRevaultSched")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_5 = "Help/revaulting/PDM_RevaultingScheduler";

   @RBEntry("ExtStgRevaultSched")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_6 = "Help/revaulting/PJL_RevaultingScheduler";

   /**
    * Status bar messages
    *
    **/
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_7 = "Desc/revaulting/RevaultingScheduler";

   @RBEntry("Click to close the screen")
   public static final String PRIVATE_CONSTANT_8 = "Desc/revaulting/RevaultingScheduler/Close";

   @RBEntry("Click to view the help on Scheduling Revaulting")
   public static final String PRIVATE_CONSTANT_9 = "Desc/revaulting/RevaultingScheduler/Help";

   @RBEntry("ScheduleAdministratorRef")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_10 = "Help/replication/ReplicationScheduler";

   @RBEntry("ContentReplicationSchedule")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_11 = "Help/replication/PDM_ReplicationScheduler";

   @RBEntry("ContentReplicationSchedule")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_12 = "Help/replication/PJL_ReplicationScheduler";

   /**
    * Status bar messages
    *
    **/
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_13 = "Desc/replication/ReplicationScheduler";

   @RBEntry("Click to close the screen")
   public static final String PRIVATE_CONSTANT_14 = "Desc/replication/ReplicationScheduler/Close";

   @RBEntry("Click to view the help on Scheduling Replication")
   public static final String PRIVATE_CONSTANT_15 = "Desc/replication/ReplicationScheduler/Help";
}