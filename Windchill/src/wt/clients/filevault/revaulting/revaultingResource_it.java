/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.filevault.revaulting;

import wt.util.resource.*;

@RBUUID("wt.clients.filevault.revaulting.revaultingResource")
public final class revaultingResource_it extends WTListResourceBundle {
   @RBEntry("Archiviazione temporizzata")
   public static final String REVAULTING_TAB = "1";

   @RBEntry("Programmazione memorizzazione esterna")
   public static final String REVAULTING_TITLE = "2";

   @RBEntry("Chiudi")
   public static final String CLOSE_BUTT = "3";

   @RBEntry("Guida")
   public static final String HELP_BUTT = "4";

   /**
    * Scheduling Framwork properties
    * 
    **/
   @RBEntry("Cronologia delle archiviazioni temporizzate")
   public static final String PRIVATE_CONSTANT_0 = "schedulelogtitle";

   @RBEntry("Cronologia delle operazioni di archiviazione temporizzata:")
   public static final String PRIVATE_CONSTANT_1 = "schedulelogdescr";

   @RBEntry("Archivio master")
   public static final String PRIVATE_CONSTANT_2 = "schedulelogheader";

   @RBEntry("Programmazione archiviazioni temporizzate")
   public static final String PRIVATE_CONSTANT_3 = "scheduleTimeTitle";

   @RBEntry("Archivio master:")
   public static final String PRIVATE_CONSTANT_4 = "scheduleTimeObjectLab";
}
