/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.filevault;

import wt.util.resource.*;

@RBUUID("wt.clients.filevault.AdminEditFolderHelpRB")
public final class AdminEditFolderHelpRB extends WTListResourceBundle {
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_0 = "Contents/filevault";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_1 = "Help/filevault";

   /**
    * Pointer the help for this screen
    *
    **/
   @RBEntry("FileVaultConfigUpdateFolder")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/filevault/AdminEditFolder";

   @RBEntry("FileVaultConfigUpdateFolder")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_3 = "Help/filevault/PDM_AdminEditFolder";

   @RBEntry("FileVaultConfigUpdateFolder")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_4 = "Help/filevault/PJL_AdminEditFolder";

   @RBEntry("FileVaultConfigAddFolder")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_5 = "Help/filevault/AdminAddFolder";

   @RBEntry("FileVaultConfigAddFolder")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_6 = "Help/filevault/PDM_AdminAddFolder";

   @RBEntry("FileVaultConfigAddFolder")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_7 = "Help/filevault/PJL_AdminAddFolder";

   /**
    * Status bar messages for when we are in Edit mode
    *
    **/
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_8 = "Desc/filevault/AdminEditFolder";

   @RBEntry("Click to save your changes and close the window")
   public static final String PRIVATE_CONSTANT_9 = "Desc/filevault/AdminEditFolder/OK";

   @RBEntry("Click to close the window without saving changes")
   public static final String PRIVATE_CONSTANT_10 = "Desc/filevault/AdminEditFolder/Cancel";

   @RBEntry("Enter a unique folder name")
   public static final String PRIVATE_CONSTANT_11 = "Desc/filevault/AdminEditFolder/FolderName";

   @RBEntry("Select a vault")
   public static final String PRIVATE_CONSTANT_12 = "Desc/filevault/AdminEditFolder/VaultChoice";

   @RBEntry("Toggle folder read only status")
   public static final String PRIVATE_CONSTANT_13 = "Desc/filevault/AdminEditFolder/ReadOnly";

   @RBEntry("Toggle folder enabled status")
   public static final String PRIVATE_CONSTANT_14 = "Desc/filevault/AdminEditFolder/Enabled";

   @RBEntry("Click to view the help on updating file vault Folders")
   public static final String PRIVATE_CONSTANT_15 = "Desc/filevault/AdminEditFolder/Help";

   /**
    * Status bar messages for when we are in Add mode.
    *
    **/
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_16 = "Desc/filevault/AdminAddFolder";

   @RBEntry("Click to save your changes and close the window")
   public static final String PRIVATE_CONSTANT_17 = "Desc/filevault/AdminAddFolder/OK";

   @RBEntry("Click to close the window without saving changes")
   public static final String PRIVATE_CONSTANT_18 = "Desc/filevault/AdminAddFolder/Cancel";

   @RBEntry("Enter a unique folder name")
   public static final String PRIVATE_CONSTANT_19 = "Desc/filevault/AdminAddFolder/FolderName";

   @RBEntry("Select a vault")
   public static final String PRIVATE_CONSTANT_20 = "Desc/filevault/AdminAddFolder/VaultChoice";

   @RBEntry("Toggle folder read only status")
   public static final String PRIVATE_CONSTANT_21 = "Desc/filevault/AdminAddFolder/ReadOnly";

   @RBEntry("Toggle folder enabled status")
   public static final String PRIVATE_CONSTANT_22 = "Desc/filevault/AdminAddFolder/Enabled";

   @RBEntry("Click to view the help on creating file vault Folders")
   public static final String PRIVATE_CONSTANT_23 = "Desc/filevault/AdminAddFolder/Help";
}
