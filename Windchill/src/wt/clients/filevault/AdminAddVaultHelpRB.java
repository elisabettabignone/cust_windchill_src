/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.filevault;

import wt.util.resource.*;

@RBUUID("wt.clients.filevault.AdminAddVaultHelpRB")
public final class AdminAddVaultHelpRB extends WTListResourceBundle {
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_0 = "Contents/filevault";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_1 = "Help/filevault";

   @RBEntry("FileVaultConfigAddVault")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/filevault/AdminAddVault";

   @RBEntry("FileVaultConfigAddVault")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_3 = "Help/filevault/PDM_AdminAddVault";

   @RBEntry("FileVaultConfigAddVault")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_4 = "Help/filevault/PJL_AdminAddVault";

   /**
    * Status bar messages
    *
    **/
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_5 = "Desc/filevault/AdminAddVault";

   @RBEntry("Click to create the vault and close the window")
   public static final String PRIVATE_CONSTANT_6 = "Desc/filevault/AdminAddVault/OK";

   @RBEntry("Click to close the window without saving changes")
   public static final String PRIVATE_CONSTANT_7 = "Desc/filevault/AdminAddVault/Cancel";

   @RBEntry("Click to view the help on creating new file vaults")
   public static final String PRIVATE_CONSTANT_8 = "Desc/filevault/AdminAddVault/Help";

   @RBEntry("Select a site under which to create the vault")
   public static final String PRIVATE_CONSTANT_9 = "Desc/filevault/AdminAddVault/SiteChoice";

   @RBEntry("Enter a unique vault name")
   public static final String PRIVATE_CONSTANT_10 = "Desc/filevault/AdminAddVault/VaultField";

   @RBEntry("Select a vault type")
   public static final String PRIVATE_CONSTANT_11 = "Desc/filevault/AdminAddVault/VaultTypeChoice";

   @RBEntry("Set as the default replication target vault for the site")
   public static final String PRIVATE_CONSTANT_12 = "Desc/filevault/AdminAddVault/DefaultTargetForSite";

   @RBEntry("Set as target vault for objects without a matching rule")
   public static final String PRIVATE_CONSTANT_13 = "Desc/filevault/AdminAddVault/DefaultSystemTarget";

   @RBEntry("Toggle the read only status")
   public static final String PRIVATE_CONSTANT_14 = "Desc/filevault/AdminAddVault/ReadOnlyCheck";

   @RBEntry("Set the vault to automatically create folders under it")
   public static final String PRIVATE_CONSTANT_15 = "Desc/filevault/AdminAddVault/AutomaticFolder";
   
   @RBEntry("Include vault in auto-cleanup operations as per defined rules")
   public static final String PRIVATE_CONSTANT_16 = "Desc/filevault/AdminAddVault/AutoCleanup";
}