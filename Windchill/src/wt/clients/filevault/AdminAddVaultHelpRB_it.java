/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.filevault;

import wt.util.resource.*;

@RBUUID("wt.clients.filevault.AdminAddVaultHelpRB")
public final class AdminAddVaultHelpRB_it extends WTListResourceBundle {
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_0 = "Contents/filevault";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_1 = "Help/filevault";

   @RBEntry("FileVaultConfigAddVault")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/filevault/AdminAddVault";

   @RBEntry("FileVaultConfigAddVault")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_3 = "Help/filevault/PDM_AdminAddVault";

   @RBEntry("FileVaultConfigAddVault")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_4 = "Help/filevault/PJL_AdminAddVault";

   /**
    * Status bar messages
    *
    **/
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_5 = "Desc/filevault/AdminAddVault";

   @RBEntry("Fare clic per creare l'archivio e chiudere la finestra")
   public static final String PRIVATE_CONSTANT_6 = "Desc/filevault/AdminAddVault/OK";

   @RBEntry("Fare clic per chiudere la finestra senza salvare le modifiche")
   public static final String PRIVATE_CONSTANT_7 = "Desc/filevault/AdminAddVault/Cancel";

   @RBEntry("Fare clic per visualizzare le informazioni della Guida relative alla creazione di nuovi archivi file")
   public static final String PRIVATE_CONSTANT_8 = "Desc/filevault/AdminAddVault/Help";

   @RBEntry("Selezionare un sito nel quale creare l'archivio")
   public static final String PRIVATE_CONSTANT_9 = "Desc/filevault/AdminAddVault/SiteChoice";

   @RBEntry("Immettere un nome di archivio univoco")
   public static final String PRIVATE_CONSTANT_10 = "Desc/filevault/AdminAddVault/VaultField";

   @RBEntry("Selezionare un tipo di archivio")
   public static final String PRIVATE_CONSTANT_11 = "Desc/filevault/AdminAddVault/VaultTypeChoice";

   @RBEntry("Imposta come archivio di destinazione replica di default per il sito")
   public static final String PRIVATE_CONSTANT_12 = "Desc/filevault/AdminAddVault/DefaultTargetForSite";

   @RBEntry("Imposta come archivio di destinazione per gli oggetti senza una regola corrispondente")
   public static final String PRIVATE_CONSTANT_13 = "Desc/filevault/AdminAddVault/DefaultSystemTarget";

   @RBEntry("Attiva lo stato di sola lettura")
   public static final String PRIVATE_CONSTANT_14 = "Desc/filevault/AdminAddVault/ReadOnlyCheck";

   @RBEntry("Imposta l'archivio per la creazione automatica di cartelle al suo interno")
   public static final String PRIVATE_CONSTANT_15 = "Desc/filevault/AdminAddVault/AutomaticFolder";
   
   @RBEntry("Include l'archivio nelle operazioni di pulizia automatica in base alle regole definite")
   public static final String PRIVATE_CONSTANT_16 = "Desc/filevault/AdminAddVault/AutoCleanup";
}
