/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.filevault;

import wt.util.resource.*;

@RBUUID("wt.clients.filevault.FileVaultTreeFrameHelpRB")
public final class FileVaultTreeFrameHelpRB extends WTListResourceBundle {
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_0 = "Contents/filevault";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_1 = "Help/filevault";

   @RBEntry("FileVaultConfigRef")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/filevault/FileVaultTreeFrame";

   @RBEntry("pdm_filevault")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_3 = "Help/filevault/PDM_FileVaultTreeFrame";

   @RBEntry("pjl_filevault")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_4 = "Help/filevault/PJL_FileVaultTreeFrame";

   /**
    * Status bar messages
    *
    **/
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_5 = "Desc/filevault/FileVaultTreeFrame";

   /**
    * button descriptions
    *
    **/
   @RBEntry("Add a new Host to the system")
   public static final String PRIVATE_CONSTANT_6 = "Desc/filevault/FileVaultTreeFrame/newHost";

   @RBEntry("Add a new Vault to the system")
   public static final String PRIVATE_CONSTANT_7 = "Desc/filevault/FileVaultTreeFrame/newVault";

   @RBEntry("Add a new Folder to the system")
   public static final String PRIVATE_CONSTANT_8 = "Desc/filevault/FileVaultTreeFrame/newFolder";

   @RBEntry("Permanently delete selected object from the system")
   public static final String PRIVATE_CONSTANT_9 = "Desc/filevault/FileVaultTreeFrame/delete";

   @RBEntry("Refresh all views")
   public static final String PRIVATE_CONSTANT_10 = "Desc/filevault/FileVaultTreeFrame/refresh";

   @RBEntry("Update selected object")
   public static final String PRIVATE_CONSTANT_11 = "Desc/filevault/FileVaultTreeFrame/update";

   @RBEntry("Validate the selected object's mounts")
   public static final String PRIVATE_CONSTANT_12 = "Desc/filevault/FileVaultTreeFrame/validateObject";

   @RBEntry("Validate the selected mount")
   public static final String PRIVATE_CONSTANT_13 = "Desc/filevault/FileVaultTreeFrame/validateMount";

   @RBEntry("Update selected mount")
   public static final String PRIVATE_CONSTANT_14 = "Desc/filevault/FileVaultTreeFrame/updateMount";

   @RBEntry("Add a mount to the selected object")
   public static final String PRIVATE_CONSTANT_15 = "Desc/filevault/FileVaultTreeFrame/mount";

   @RBEntry("Remove the selected mount")
   public static final String PRIVATE_CONSTANT_16 = "Desc/filevault/FileVaultTreeFrame/unmount";

   @RBEntry("Clean Object")
   public static final String PRIVATE_CONSTANT_17 = "Desc/filevault/FileVaultTreeFrame/clean";

   /**
    * panel descriptions
    *
    **/
   @RBEntry("Double click on a mount to update its attributes")
   public static final String PRIVATE_CONSTANT_18 = "Desc/filevault/FileVaultTreeFrame/mountList";

   @RBEntry("Double click on an object to update it, single click to view its mounts")
   public static final String PRIVATE_CONSTANT_19 = "Desc/filevault/FileVaultTreeFrame/objectTree";

   /**
    * Check boxes
    *
    **/
   @RBEntry("Show only existing mounts")
   public static final String PRIVATE_CONSTANT_20 = "Desc/filevault/FileVaultTreeFrame/Existing";

   @RBEntry("Show all possible mounts")
   public static final String PRIVATE_CONSTANT_21 = "Desc/filevault/FileVaultTreeFrame/AllPossible";

   /**
    * Will need to add the tooltip descriptions
    *
    **/
   @RBEntry("New Host")
   public static final String PRIVATE_CONSTANT_22 = "Tip/filevault/FileVaultTreeFrame/newHost";

   @RBEntry("New Vault")
   public static final String PRIVATE_CONSTANT_23 = "Tip/filevault/FileVaultTreeFrame/newVault";

   @RBEntry("New Folder")
   public static final String PRIVATE_CONSTANT_24 = "Tip/filevault/FileVaultTreeFrame/newFolder";

   @RBEntry("Delete selected object")
   public static final String PRIVATE_CONSTANT_25 = "Tip/filevault/FileVaultTreeFrame/delete";

   @RBEntry("Refresh data")
   public static final String PRIVATE_CONSTANT_26 = "Tip/filevault/FileVaultTreeFrame/refresh";

   @RBEntry("Update selected object")
   public static final String PRIVATE_CONSTANT_27 = "Tip/filevault/FileVaultTreeFrame/update";

   @RBEntry("Validate the selected object's mounts")
   public static final String PRIVATE_CONSTANT_28 = "Tip/filevault/FileVaultTreeFrame/validateObject";

   @RBEntry("Validate the selected mount")
   public static final String PRIVATE_CONSTANT_29 = "Tip/filevault/FileVaultTreeFrame/validateMount";

   @RBEntry("Update selected mount")
   public static final String PRIVATE_CONSTANT_30 = "Tip/filevault/FileVaultTreeFrame/updateMount";

   @RBEntry("Add mount to selected object")
   public static final String PRIVATE_CONSTANT_31 = "Tip/filevault/FileVaultTreeFrame/mount";

   @RBEntry("Remove selected mount")
   public static final String PRIVATE_CONSTANT_32 = "Tip/filevault/FileVaultTreeFrame/unmount";

   @RBEntry("Clean Object")
   public static final String PRIVATE_CONSTANT_33 = "Tip/filevault/FileVaultTreeFrame/clean";

   @RBEntry("Schedule Revaulting")
   public static final String PRIVATE_CONSTANT_34 = "Tip/filevault/FileVaultTreeFrame/revault";

   @RBEntry("Schedule revaulting")
   public static final String PRIVATE_CONSTANT_35 = "Desc/filevault/FileVaultTreeFrame/revault";

   @RBEntry("Schedule Remove Unreferenced Files")
   public static final String PRIVATE_CONSTANT_36 = "Tip/filevault/FileVaultTreeFrame/cleanup";

   @RBEntry("Schedule Remove Unreferenced Files")
   public static final String PRIVATE_CONSTANT_37 = "Desc/filevault/FileVaultTreeFrame/cleanup";

   @RBEntry("Schedule Content Replication")
   public static final String PRIVATE_CONSTANT_38 = "Tip/filevault/FileVaultTreeFrame/contentrepl";

   @RBEntry("Schedule Content Replication")
   public static final String PRIVATE_CONSTANT_39 = "Desc/filevault/FileVaultTreeFrame/contentrepl";

   @RBEntry("Reset Content Replication")
   public static final String PRIVATE_CONSTANT_40 = "Tip/filevault/FileVaultTreeFrame/resetrepl";

   @RBEntry("Reset Content Replication")
   public static final String PRIVATE_CONSTANT_41 = "Desc/filevault/FileVaultTreeFrame/resetrepl";

   @RBEntry("Toggle the enabled status of an object")
   public static final String PRIVATE_CONSTANT_42 = "Tip/filevault/FileVaultTreeFrame/toggle";

   @RBEntry("Toggle the enabled status of an object")
   public static final String PRIVATE_CONSTANT_43 = "Desc/filevault/FileVaultTreeFrame/toggle";

   @RBEntry("FileVaultConfigAutoCleanup")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_44 = "Help/filevault/FileVaultConfigAutoCleanup";
}
