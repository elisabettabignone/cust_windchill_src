/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.workflow.manager;

import wt.util.resource.*;

@RBUUID("wt.clients.workflow.manager.managerHelpRB")
public final class managerHelpRB extends WTListResourceBundle {
   /**
    * -------------------------------------------------
    * Test string
    **/
   @RBEntry("..Localized..")
   public static final String TEST_STRING = "TEST_STRING";

   /**
    * -------------------------------------------------
    * Other
    **/
   @RBEntry("MainHelp")
   @RBPseudo(false)
   public static final String MAIN_HELP = "MAIN_HELP";

   /**
    * -------------------------------------------------
    * Help files
    **/
   @RBEntry("This will refresh the diagram.")
   public static final String PRIVATE_CONSTANT_0 = "Desc/ProcessManager/ProcessManagerFrame/refresh";

   @RBEntry("This will refresh the diagram.")
   public static final String PRIVATE_CONSTANT_1 = "Desc/ProcessManager/ProcessManagerPanel/refresh";

   @RBEntry("WFProcessMgrViewManage")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/ProcessManager/MainHelp";

   /**
    * -------------------------------------------------
    * component pop-up help
    * toolbar= refresh - activityrestart activitysuspend activityresume activityterminate activitycomplete - processsuspend processresume processterminate processcomplete - helpwindow
    **/
   @RBEntry("Complete Activity")
   public static final String PRIVATE_CONSTANT_3 = "Tip/ProcessManager/ProcessManagerFrame/activitycomplete";

   @RBEntry("Restart Aborted Activity")
   public static final String PRIVATE_CONSTANT_4 = "Tip/ProcessManager/ProcessManagerFrame/activityrestart";

   @RBEntry("Resume Activity")
   public static final String PRIVATE_CONSTANT_5 = "Tip/ProcessManager/ProcessManagerFrame/activityresume";

   @RBEntry("Suspend Activity")
   public static final String PRIVATE_CONSTANT_6 = "Tip/ProcessManager/ProcessManagerFrame/activitysuspend";

   @RBEntry("Terminate Activity")
   public static final String PRIVATE_CONSTANT_7 = "Tip/ProcessManager/ProcessManagerFrame/activityterminate";

   @RBEntry("Help Window")
   public static final String PRIVATE_CONSTANT_8 = "Tip/ProcessManager/ProcessManagerFrame/helpwindow";

   @RBEntry("Open Local Search")
   public static final String PRIVATE_CONSTANT_9 = "Tip/ProcessManager/ProcessManagerFrame/openlocalsearch";

   @RBEntry("Open Report Generator")
   public static final String PRIVATE_CONSTANT_10 = "Tip/ProcessManager/ProcessManagerFrame/openreportgenerator";

   @RBEntry("Complete Process")
   public static final String PRIVATE_CONSTANT_11 = "Tip/ProcessManager/ProcessManagerFrame/processcomplete";

   @RBEntry("Resume Process")
   public static final String PRIVATE_CONSTANT_12 = "Tip/ProcessManager/ProcessManagerFrame/processresume";

   @RBEntry("Suspend Process")
   public static final String PRIVATE_CONSTANT_13 = "Tip/ProcessManager/ProcessManagerFrame/processsuspend";

   @RBEntry("Terminate Process")
   public static final String PRIVATE_CONSTANT_14 = "Tip/ProcessManager/ProcessManagerFrame/processterminate";

   @RBEntry("Refresh")
   public static final String PRIVATE_CONSTANT_15 = "Tip/ProcessManager/ProcessManagerFrame/refresh";

   @RBEntry("Complete Activity")
   public static final String PRIVATE_CONSTANT_16 = "Tip/ProcessManager/ProcessManagerPanel/activitycomplete";

   @RBEntry("Restart Aborted Activity")
   public static final String PRIVATE_CONSTANT_17 = "Tip/ProcessManager/ProcessManagerPanel/activityrestart";

   @RBEntry("Resume Activity")
   public static final String PRIVATE_CONSTANT_18 = "Tip/ProcessManager/ProcessManagerPanel/activityresume";

   @RBEntry("Suspend Activity")
   public static final String PRIVATE_CONSTANT_19 = "Tip/ProcessManager/ProcessManagerPanel/activitysuspend";

   @RBEntry("Terminate Activity")
   public static final String PRIVATE_CONSTANT_20 = "Tip/ProcessManager/ProcessManagerPanel/activityterminate";

   @RBEntry("Help Window")
   public static final String PRIVATE_CONSTANT_21 = "Tip/ProcessManager/ProcessManagerPanel/helpwindow";

   @RBEntry("Open Local Search")
   public static final String PRIVATE_CONSTANT_22 = "Tip/ProcessManager/ProcessManagerPanel/openlocalsearch";

   @RBEntry("Open Report Generator")
   public static final String PRIVATE_CONSTANT_23 = "Tip/ProcessManager/ProcessManagerPanel/openreportgenerator";

   @RBEntry("Complete Process")
   public static final String PRIVATE_CONSTANT_24 = "Tip/ProcessManager/ProcessManagerPanel/processcomplete";

   @RBEntry("Resume Process")
   public static final String PRIVATE_CONSTANT_25 = "Tip/ProcessManager/ProcessManagerPanel/processresume";

   @RBEntry("Suspend Process")
   public static final String PRIVATE_CONSTANT_26 = "Tip/ProcessManager/ProcessManagerPanel/processsuspend";

   @RBEntry("Terminate Process")
   public static final String PRIVATE_CONSTANT_27 = "Tip/ProcessManager/ProcessManagerPanel/processterminate";

   @RBEntry("Refresh")
   public static final String PRIVATE_CONSTANT_28 = "Tip/ProcessManager/ProcessManagerPanel/refresh";
}
