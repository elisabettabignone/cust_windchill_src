/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.workflow.manager;

import wt.util.resource.*;

@RBUUID("wt.clients.workflow.manager.managerHelpRB")
public final class managerHelpRB_it extends WTListResourceBundle {
   /**
    * -------------------------------------------------
    * Test string
    **/
   @RBEntry("..Localizzato..")
   public static final String TEST_STRING = "TEST_STRING";

   /**
    * -------------------------------------------------
    * Other
    **/
   @RBEntry("MainHelp")
   @RBPseudo(false)
   public static final String MAIN_HELP = "MAIN_HELP";

   /**
    * -------------------------------------------------
    * Help files
    **/
   @RBEntry("Aggiorna il diagramma.")
   public static final String PRIVATE_CONSTANT_0 = "Desc/ProcessManager/ProcessManagerFrame/refresh";

   @RBEntry("Aggiorna il diagramma.")
   public static final String PRIVATE_CONSTANT_1 = "Desc/ProcessManager/ProcessManagerPanel/refresh";

   @RBEntry("WFProcessMgrViewManage")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/ProcessManager/MainHelp";

   /**
    * -------------------------------------------------
    * component pop-up help
    * toolbar= refresh - activityrestart activitysuspend activityresume activityterminate activitycomplete - processsuspend processresume processterminate processcomplete - helpwindow
    **/
   @RBEntry("Completa l'attività")
   public static final String PRIVATE_CONSTANT_3 = "Tip/ProcessManager/ProcessManagerFrame/activitycomplete";

   @RBEntry("Riavvia l'attività interrotta")
   public static final String PRIVATE_CONSTANT_4 = "Tip/ProcessManager/ProcessManagerFrame/activityrestart";

   @RBEntry("Riprendi l'attività")
   public static final String PRIVATE_CONSTANT_5 = "Tip/ProcessManager/ProcessManagerFrame/activityresume";

   @RBEntry("Sospendi l'attività")
   public static final String PRIVATE_CONSTANT_6 = "Tip/ProcessManager/ProcessManagerFrame/activitysuspend";

   @RBEntry("Termina l'attività")
   public static final String PRIVATE_CONSTANT_7 = "Tip/ProcessManager/ProcessManagerFrame/activityterminate";

   @RBEntry("Finestra della guida")
   public static final String PRIVATE_CONSTANT_8 = "Tip/ProcessManager/ProcessManagerFrame/helpwindow";

   @RBEntry("Apri la ricerca locale")
   public static final String PRIVATE_CONSTANT_9 = "Tip/ProcessManager/ProcessManagerFrame/openlocalsearch";

   @RBEntry("Apri la generazione rapporti")
   public static final String PRIVATE_CONSTANT_10 = "Tip/ProcessManager/ProcessManagerFrame/openreportgenerator";

   @RBEntry("Completa il processo")
   public static final String PRIVATE_CONSTANT_11 = "Tip/ProcessManager/ProcessManagerFrame/processcomplete";

   @RBEntry("Riprendi il processo")
   public static final String PRIVATE_CONSTANT_12 = "Tip/ProcessManager/ProcessManagerFrame/processresume";

   @RBEntry("Sospendi il processo")
   public static final String PRIVATE_CONSTANT_13 = "Tip/ProcessManager/ProcessManagerFrame/processsuspend";

   @RBEntry("Termina processo")
   public static final String PRIVATE_CONSTANT_14 = "Tip/ProcessManager/ProcessManagerFrame/processterminate";

   @RBEntry("Aggiorna")
   public static final String PRIVATE_CONSTANT_15 = "Tip/ProcessManager/ProcessManagerFrame/refresh";

   @RBEntry("Completa l'attività")
   public static final String PRIVATE_CONSTANT_16 = "Tip/ProcessManager/ProcessManagerPanel/activitycomplete";

   @RBEntry("Riavvia l'attività interrotta")
   public static final String PRIVATE_CONSTANT_17 = "Tip/ProcessManager/ProcessManagerPanel/activityrestart";

   @RBEntry("Riprendi l'attività")
   public static final String PRIVATE_CONSTANT_18 = "Tip/ProcessManager/ProcessManagerPanel/activityresume";

   @RBEntry("Sospendi l'attività")
   public static final String PRIVATE_CONSTANT_19 = "Tip/ProcessManager/ProcessManagerPanel/activitysuspend";

   @RBEntry("Termina l'attività")
   public static final String PRIVATE_CONSTANT_20 = "Tip/ProcessManager/ProcessManagerPanel/activityterminate";

   @RBEntry("Finestra della guida")
   public static final String PRIVATE_CONSTANT_21 = "Tip/ProcessManager/ProcessManagerPanel/helpwindow";

   @RBEntry("Apri la ricerca locale")
   public static final String PRIVATE_CONSTANT_22 = "Tip/ProcessManager/ProcessManagerPanel/openlocalsearch";

   @RBEntry("Apri la generazione rapporti")
   public static final String PRIVATE_CONSTANT_23 = "Tip/ProcessManager/ProcessManagerPanel/openreportgenerator";

   @RBEntry("Completa il processo")
   public static final String PRIVATE_CONSTANT_24 = "Tip/ProcessManager/ProcessManagerPanel/processcomplete";

   @RBEntry("Riprendi il processo")
   public static final String PRIVATE_CONSTANT_25 = "Tip/ProcessManager/ProcessManagerPanel/processresume";

   @RBEntry("Sospendi il processo")
   public static final String PRIVATE_CONSTANT_26 = "Tip/ProcessManager/ProcessManagerPanel/processsuspend";

   @RBEntry("Termina processo")
   public static final String PRIVATE_CONSTANT_27 = "Tip/ProcessManager/ProcessManagerPanel/processterminate";

   @RBEntry("Aggiorna")
   public static final String PRIVATE_CONSTANT_28 = "Tip/ProcessManager/ProcessManagerPanel/refresh";
}
