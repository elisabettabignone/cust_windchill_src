/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.workflow.manager;

import wt.util.resource.*;

@RBUUID("wt.clients.workflow.manager.managerRB")
public final class managerRB extends WTListResourceBundle {
   /**
    * -------------------------------------------------
    * Test string
    **/
   @RBEntry("..Localized..")
   public static final String TEST_STRING = "TEST_STRING";

   /**
    * -------------------------------------------------
    * Buttons
    **/
   @RBEntry("Detach")
   public static final String DETACH_BUTTON = "DETACH_BUTTON";

   /**
    * -------------------------------------------------
    * Labels
    **/
   @RBEntry("Content")
   public static final String CONTENT_TAB = "CONTENT_TAB";

   @RBEntry("General")
   public static final String GENERAL_TAB = "GENERAL_TAB";

   @RBEntry("Participant")
   public static final String PARTICIPANT_TAB = "PARTICIPANT_TAB";

   @RBEntry("Role")
   public static final String ROLE_TAB = "ROLE_TAB";

   @RBEntry("Routing Events")
   public static final String ROUTING_TAB = "ROUTNG_TAB";

   @RBEntry("Time")
   public static final String TIME_TAB = "TIME_TAB";

   @RBEntry("Variable")
   public static final String VARIABLE_TAB = "VARIABLE_TAB";

   @RBEntry("Action")
   public static final String ACTION_LABEL = "ACTION_LABEL";

   @RBEntry("Error")
   public static final String ACTIVITY_EXCEPTION_LABEL = "ACTIVITY_EXCEPTION_LABEL";

   @RBEntry("Actor")
   public static final String ACTOR_LABEL = "ACTOR_LABEL";

   @RBEntry("Alert time")
   public static final String ALERT_TIME_LABEL = "ALERT_TIME_LABEL";

   @RBEntry("Cancel")
   public static final String CANCEL_LABEL = "CANCEL_LABEL";

   @RBEntry("Category")
   public static final String CATEGORY_LABEL = "CATEGORY_LABEL";

   @RBEntry("Creator")
   public static final String CREATOR_LABEL = "CREATOR_LABEL";

   @RBEntry("Deadline")
   public static final String DEADLINE_LABEL = "DEADLINE_LABEL";

   @RBEntry("Default Value")
   public static final String DEFAULT_VALUE_LABEL = "DEFAULT_VALUE_LABEL";

   @RBEntry("Defined")
   public static final String DEFINED_LABEL = "DEFINED_LABEL";

   @RBEntry("Defined Threshold")
   public static final String DEFINED_THRESHOLD_LABEL = "DEFINED_THRESHOLD_LABEL";

   @RBEntry("Description")
   public static final String DESCRIPTION_LABEL = "DESCRIPTION_LABEL";

   @RBEntry("End time")
   public static final String END_TIME_LABEL = "END_TIME_LABEL";

   @RBEntry("Event")
   public static final String EVENT_LABEL = "EVENT_LABEL";

   @RBEntry("Information not available")
   public static final String INFO_NA_LABEL = "INFO_NA_LABEL";

   @RBEntry("Input")
   public static final String INPUT_VAR_LABEL = "INPUT_VAR_LABEL";

   @RBEntry("Location")
   public static final String LOCATION_LABEL = "LOCATION_LABEL";

   @RBEntry("Mut")
   public static final String MUTABLE_ABR_LABEL = "MUTABLE_ABR_LABEL";

   @RBEntry("Name")
   public static final String NAME_LABEL = "NAME_LABEL";

   @RBEntry("Not applicable")
   public static final String NAPPLICABLE_LABEL = "NAPPLICABLE_LABEL";

   @RBEntry("Notification")
   public static final String NOTIFICATION_LABEL = "NOTIFICATION_LABEL";

   @RBEntry("Not ended")
   public static final String NOT_ENDED_LABEL = "NOT_ENDED_LABEL";

   @RBEntry("Not started")
   public static final String NOT_STARTED_LABEL = "NOT_STARTED_LABEL";

   @RBEntry("No deadline set.")
   public static final String NO_DEADLINE_LABEL = "NO_DEADLINE_LABEL";

   @RBEntry("No")
   public static final String NO_LABEL = "NO_LABEL";

   @RBEntry("No project associated")
   public static final String NO_PROJECT_ASSOC_LABEL = "NO_PROJECT_ASSOC_LABEL";

   @RBEntry("No team template associated")
   public static final String NO_TEAM_TEMPLATE_ASSOC_LABEL = "NO_TEAM_TEMPLATE_ASSOC_LABEL";

   @RBEntry("Ok")
   public static final String OK_LABEL = "OK_LABEL";

   @RBEntry("Output")
   public static final String OUTPUT_VAR_LABEL = "OUTPUT_VAR_LABEL";

   @RBEntry("Owner")
   public static final String OWNER_LABEL = "OWNER_LABEL";

   @RBEntry("Principal")
   public static final String PRINCIPAL_LABEL = "PRINCIPAL_LABEL";

   @RBEntry("Priority")
   public static final String PRIORITY_LABEL = "PRIORITY_LABEL";

   @RBEntry("Project")
   public static final String PROJECT_LABEL = "PROJECT_LABEL";

   @RBEntry("Real")
   public static final String REAL_LABEL = "REAL_LABEL";

   @RBEntry("Real Threshold")
   public static final String REAL_THRESHOLD_LABEL = "REAL_THRESHOLD_LABEL";

   @RBEntry("Req")
   public static final String REQUIRED_ABR_LABEL = "REQUIRED_ABR_LABEL";

   @RBEntry("Res")
   public static final String RESETABLE_ABR_LABEL = "RESETABLE_ABR_LABEL";

   @RBEntry("Role")
   public static final String ROLE_LABEL = "ROLE_LABEL";

   @RBEntry("Start time")
   public static final String START_TIME_LABEL = "START_TIME_LABEL";

   @RBEntry("State")
   public static final String STATE_LABEL = "STATE_LABEL";

   @RBEntry("Suspend time")
   public static final String SUSPEND_TIME_LABEL = "SUSPEND_TIME_LABEL";

   @RBEntry("Task Name")
   public static final String TASK_NAME_LABEL = "TASK_NAME_LABEL";

   @RBEntry("Template")
   public static final String TEMPLATE_LABEL = "TEMPLATE_LABEL";

   @RBEntry("Type")
   public static final String TYPE_LABEL = "TYPE_LABEL";

   @RBEntry("Value")
   public static final String VALUE_LABEL = "VALUE_LABEL";

   @RBEntry("Vis")
   public static final String VISIBLE_ABR_LABEL = "VISIBLE_ABR_LABEL";

   @RBEntry("Yes")
   public static final String YES_LABEL = "YES_LABEL";

   /**
    * -------------------------------------------------
    * Titles
    **/
   @RBEntry("Property")
   public static final String PROPERTY_TITLE = "PROPERTY_TITLE";

   /**
    * -------------------------------------------------
    * Text
    **/
   @RBEntry("Do you really want to complete the activity ?")
   public static final String CONFIRM_ACTIVITY_COMPLETE_MESSAGE = "CONFIRM_ACTIVITY_COMPLETE_MESSAGE";

   @RBEntry("Do you really want to resume the activity ?")
   public static final String CONFIRM_ACTIVITY_RESUME_MESSAGE = "CONFIRM_ACTIVITY_RESUME_MESSAGE";

   @RBEntry("Do you really want to skip the activity ?")
   public static final String CONFIRM_ACTIVITY_SKIP_MESSAGE = "CONFIRM_ACTIVITY_SKIP_MESSAGE";

   @RBEntry("Do you really want to suspend the activity ?")
   public static final String CONFIRM_ACTIVITY_SUSPEND_MESSAGE = "CONFIRM_ACTIVITY_SUSPEND_MESSAGE";

   @RBEntry("Do you really want to terminate the activity ?")
   public static final String CONFIRM_ACTIVITY_TERMINATE_MESSAGE = "CONFIRM_ACTIVITY_TERMINATE_MESSAGE";

   @RBEntry("Do you really want to complete the process ?")
   public static final String CONFIRM_PROCESS_COMPLETE_MESSAGE = "CONFIRM_PROCESS_COMPLETE_MESSAGE";

   @RBEntry("Do you really want to resume the process ?")
   public static final String CONFIRM_PROCESS_RESUME_MESSAGE = "CONFIRM_PROCESS_RESUME_MESSAGE";

   @RBEntry("Do you really want to skip the process ?")
   public static final String CONFIRM_PROCESS_SKIP_MESSAGE = "CONFIRM_PROCESS_SKIP_MESSAGE";

   @RBEntry("Do you really want to suspend the process ?")
   public static final String CONFIRM_PROCESS_SUSPEND_MESSAGE = "CONFIRM_PROCESS_SUSPEND_MESSAGE";

   @RBEntry("Do you really want to terminate the process ?")
   public static final String CONFIRM_PROCESS_TERMINATE_MESSAGE = "CONFIRM_PROCESS_TERMINATE_MESSAGE";

   /**
    * -------------------------------------------------
    * Menu
    **/
   @RBEntry("Complete...")
   public static final String ACTIVITY_COMPLETE_MENU = "ACTIVITY_COMPLETE_MENU";

   @RBEntry("Activity")
   public static final String ACTIVITY_MENU = "ACTIVITY_MENU";

   @RBEntry("Restart")
   public static final String ACTIVITY_RESTART_MENU = "ACTIVITY_RESTART_MENU";

   @RBEntry("Resume...")
   public static final String ACTIVITY_RESUME_MENU = "ACTIVITY_RESUME_MENU";

   @RBEntry("Skip...")
   public static final String ACTIVITY_SKIP_MENU = "ACTIVITY_SKIP_MENU";

   @RBEntry("Suspend...")
   public static final String ACTIVITY_SUSPEND_MENU = "ACTIVITY_SUSPEND_MENU";

   @RBEntry("Terminate...")
   public static final String ACTIVITY_TERMINATE_MENU = "ACTIVITY_TERMINATE_MENU";

   @RBEntry("Align Vertically")
   public static final String ALIGN_CENTER_X_MENU = "ALIGN_CENTER_X_MENU";

   @RBEntry("Align Horizontally")
   public static final String ALIGN_CENTER_Y_MENU = "ALIGN_CENTER_Y_MENU";

   @RBEntry("Distribute Horizontally")
   public static final String DISTRIBUTE_X_MENU = "DISTRIBUTE_X_MENU";

   @RBEntry("Distribute Vertically")
   public static final String DISTRIBUTE_Y_MENU = "DISTRIBUTE_Y_MENU";

   @RBEntry("Edit")
   public static final String EDIT_MENU = "EDIT_MENU";

   @RBEntry("Exit")
   public static final String EXIT_MENU = "EXIT_MENU";

   @RBEntry("File")
   public static final String FILE_MENU = "FILE_MENU";

   @RBEntry("Help")
   public static final String HELP_MENU = "HELP_MENU";

   @RBEntry("Help...")
   public static final String HELP_WINDOW_MENU = "HELP_WINDOW_MENU";

   @RBEntry("Icon/Square line view")
   public static final String ICON_SQUARE_MENU = "ICON_SQUARE_MENU";

   @RBEntry("Icon/Straight line view")
   public static final String ICON_STRAIGHT_MENU = "ICON_STRAIGHT_MENU";

   @RBEntry("Invert selection")
   public static final String INVERT_SELECTION_MENU = "INVERT_SELECTION_MENU";

   @RBEntry("Complete...")
   public static final String PROCESS_COMPLETE_MENU = "PROCESS_COMPLETE_MENU";

   @RBEntry("Process")
   public static final String PROCESS_MENU = "PROCESS_MENU";

   @RBEntry("Resume...")
   public static final String PROCESS_RESUME_MENU = "PROCESS_RESUME_MENU";

   @RBEntry("Skip...")
   public static final String PROCESS_SKIP_MENU = "PROCESS_SKIP_MENU";

   @RBEntry("Suspend...")
   public static final String PROCESS_SUSPEND_MENU = "PROCESS_SUSPEND_MENU";

   @RBEntry("Terminate...")
   public static final String PROCESS_TERMINATE_MENU = "PROCESS_TERMINATE_MENU";

   @RBEntry("Refresh")
   public static final String REFRESH_MENU = "REFRESH_MENU";

   @RBEntry("Select all links")
   public static final String SELECT_ALL_LINKS_MENU = "SELECT_ALL_LINKS_MENU";

   @RBEntry("Select all")
   public static final String SELECT_ALL_MENU = "SELECT_ALL_MENU";

   @RBEntry("Select all nodes")
   public static final String SELECT_ALL_NODES_MENU = "SELECT_ALL_NODES_MENU";

   @RBEntry("Time View")
   public static final String TIME_MENU = "TIME_MENU";

   @RBEntry("Toggle multiselection")
   public static final String TOGGLE_MULTISELECTION_MENU = "TOGGLE_MULTISELECTION_MENU";

   @RBEntry("Unselect all")
   public static final String UNSELECT_ALL_MENU = "UNSELECT_ALL_MENU";

   @RBEntry("View")
   public static final String VIEW_MENU = "VIEW_MENU";

   @RBEntry("Scale 1:1")
   public static final String ZOOM_1_MENU = "ZOOM_1_MENU";

   @RBEntry("Zoom in")
   public static final String ZOOM_IN_MENU = "ZOOM_IN_MENU";

   @RBEntry("Zoom out")
   public static final String ZOOM_OUT_MENU = "ZOOM_OUT_MENU";

   /**
    * -------------------------------------------------
    * Tool Tips
    * -------------------------------------------------
    * Exceptions
    **/
   @RBEntry("The resource file could not be found!")
   public static final String RESOURCE_FILE_MISSING_EXCEPTION = "RESOURCE_FILE_MISSING_EXCEPTION";

   /**
    * -------------------------------------------------
    * Other
    **/
   @RBEntry("Type not supported by the Process Manager applet.")
   public static final String TYPE_NOT_SUPPORTED = "TYPE_NOT_SUPPORTED";

   @RBEntry("Suspended by ")
   public static final String COMMENT_SUSPEND = "COMMENT_SUSPEND";

   @RBEntry("Resumed by")
   public static final String COMMENT_RESUME = "COMMENT_RESUME";

   @RBEntry("Completed by")
   public static final String COMMENT_COMPLETE = "COMMENT_COMPLETE";

   @RBEntry("Terminated by")
   public static final String COMMENT_TERMINATE = "COMMENT_TERMINATE";
   
   @RBEntry("The activity could not be completed because the object is currently awaiting for promotion.")
   public static final String ACTIVITY_COMPLETION_FAILED = "ACTIVITY_COMPLETION_FAILED";

   @RBEntry("Event name should not start with number.")
   public static final String EVENT_NAME_CHECK = "EVENT_NAME_CHECK";
   
   @RBEntry("Event name should not have special characters.")
   public static final String EVENT_NAME_CHAR_CHECK = "EVENT_NAME_CHAR_CHECK";
}
