/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.workflow.manager;

import wt.util.resource.*;

@RBUUID("wt.clients.workflow.manager.managerRB")
public final class managerRB_it extends WTListResourceBundle {
   /**
    * -------------------------------------------------
    * Test string
    **/
   @RBEntry("..Localizzato..")
   public static final String TEST_STRING = "TEST_STRING";

   /**
    * -------------------------------------------------
    * Buttons
    **/
   @RBEntry("Scollega")
   public static final String DETACH_BUTTON = "DETACH_BUTTON";

   /**
    * -------------------------------------------------
    * Labels
    **/
   @RBEntry("Contenuto")
   public static final String CONTENT_TAB = "CONTENT_TAB";

   @RBEntry("Generale")
   public static final String GENERAL_TAB = "GENERAL_TAB";

   @RBEntry("Partecipante")
   public static final String PARTICIPANT_TAB = "PARTICIPANT_TAB";

   @RBEntry("Ruolo")
   public static final String ROLE_TAB = "ROLE_TAB";

   @RBEntry("Eventi di instradamento")
   public static final String ROUTING_TAB = "ROUTNG_TAB";

   @RBEntry("Tempo")
   public static final String TIME_TAB = "TIME_TAB";

   @RBEntry("Variabile")
   public static final String VARIABLE_TAB = "VARIABLE_TAB";

   @RBEntry("Azione")
   public static final String ACTION_LABEL = "ACTION_LABEL";

   @RBEntry("Errore")
   public static final String ACTIVITY_EXCEPTION_LABEL = "ACTIVITY_EXCEPTION_LABEL";

   @RBEntry("Attore")
   public static final String ACTOR_LABEL = "ACTOR_LABEL";

   @RBEntry("Periodo d'avviso")
   public static final String ALERT_TIME_LABEL = "ALERT_TIME_LABEL";

   @RBEntry("Annulla")
   public static final String CANCEL_LABEL = "CANCEL_LABEL";

   @RBEntry("Categoria")
   public static final String CATEGORY_LABEL = "CATEGORY_LABEL";

   @RBEntry("Autore")
   public static final String CREATOR_LABEL = "CREATOR_LABEL";

   @RBEntry("Scadenza")
   public static final String DEADLINE_LABEL = "DEADLINE_LABEL";

   @RBEntry("Valore di default")
   public static final String DEFAULT_VALUE_LABEL = "DEFAULT_VALUE_LABEL";

   @RBEntry("Definito")
   public static final String DEFINED_LABEL = "DEFINED_LABEL";

   @RBEntry("Soglia definita")
   public static final String DEFINED_THRESHOLD_LABEL = "DEFINED_THRESHOLD_LABEL";

   @RBEntry("Descrizione")
   public static final String DESCRIPTION_LABEL = "DESCRIPTION_LABEL";

   @RBEntry("Ora di fine")
   public static final String END_TIME_LABEL = "END_TIME_LABEL";

   @RBEntry("Evento")
   public static final String EVENT_LABEL = "EVENT_LABEL";

   @RBEntry("Informazioni non disponibili")
   public static final String INFO_NA_LABEL = "INFO_NA_LABEL";

   @RBEntry("Input")
   public static final String INPUT_VAR_LABEL = "INPUT_VAR_LABEL";

   @RBEntry("Posizione")
   public static final String LOCATION_LABEL = "LOCATION_LABEL";

   @RBEntry("Mut")
   public static final String MUTABLE_ABR_LABEL = "MUTABLE_ABR_LABEL";

   @RBEntry("Nome")
   public static final String NAME_LABEL = "NAME_LABEL";

   @RBEntry("Non applicabile")
   public static final String NAPPLICABLE_LABEL = "NAPPLICABLE_LABEL";

   @RBEntry("Notifica")
   public static final String NOTIFICATION_LABEL = "NOTIFICATION_LABEL";

   @RBEntry("Non terminato")
   public static final String NOT_ENDED_LABEL = "NOT_ENDED_LABEL";

   @RBEntry("Non avviato")
   public static final String NOT_STARTED_LABEL = "NOT_STARTED_LABEL";

   @RBEntry("Nessuna scadenza.")
   public static final String NO_DEADLINE_LABEL = "NO_DEADLINE_LABEL";

   @RBEntry("No")
   public static final String NO_LABEL = "NO_LABEL";

   @RBEntry("Nessun progetto associato")
   public static final String NO_PROJECT_ASSOC_LABEL = "NO_PROJECT_ASSOC_LABEL";

   @RBEntry("Nessun modello di team associato")
   public static final String NO_TEAM_TEMPLATE_ASSOC_LABEL = "NO_TEAM_TEMPLATE_ASSOC_LABEL";

   @RBEntry("OK")
   public static final String OK_LABEL = "OK_LABEL";

   @RBEntry("Output")
   public static final String OUTPUT_VAR_LABEL = "OUTPUT_VAR_LABEL";

   @RBEntry("Proprietario")
   public static final String OWNER_LABEL = "OWNER_LABEL";

   @RBEntry("Utente/gruppo/ruolo")
   public static final String PRINCIPAL_LABEL = "PRINCIPAL_LABEL";

   @RBEntry("Priorità")
   public static final String PRIORITY_LABEL = "PRIORITY_LABEL";

   @RBEntry("Progetto")
   public static final String PROJECT_LABEL = "PROJECT_LABEL";

   @RBEntry("Reale")
   public static final String REAL_LABEL = "REAL_LABEL";

   @RBEntry("Soglia reale")
   public static final String REAL_THRESHOLD_LABEL = "REAL_THRESHOLD_LABEL";

   @RBEntry("Obbligatorio")
   public static final String REQUIRED_ABR_LABEL = "REQUIRED_ABR_LABEL";

   @RBEntry("Reimp")
   public static final String RESETABLE_ABR_LABEL = "RESETABLE_ABR_LABEL";

   @RBEntry("Ruolo")
   public static final String ROLE_LABEL = "ROLE_LABEL";

   @RBEntry("Ora d'inizio")
   public static final String START_TIME_LABEL = "START_TIME_LABEL";

   @RBEntry("Stato del ciclo di vita")
   public static final String STATE_LABEL = "STATE_LABEL";

   @RBEntry("Tempo di sospensione")
   public static final String SUSPEND_TIME_LABEL = "SUSPEND_TIME_LABEL";

   @RBEntry("Nome task")
   public static final String TASK_NAME_LABEL = "TASK_NAME_LABEL";

   @RBEntry("Modello")
   public static final String TEMPLATE_LABEL = "TEMPLATE_LABEL";

   @RBEntry("Tipo")
   public static final String TYPE_LABEL = "TYPE_LABEL";

   @RBEntry("Valore")
   public static final String VALUE_LABEL = "VALUE_LABEL";

   @RBEntry("Vis")
   public static final String VISIBLE_ABR_LABEL = "VISIBLE_ABR_LABEL";

   @RBEntry("Sì")
   public static final String YES_LABEL = "YES_LABEL";

   /**
    * -------------------------------------------------
    * Titles
    **/
   @RBEntry("Proprietà")
   public static final String PROPERTY_TITLE = "PROPERTY_TITLE";

   /**
    * -------------------------------------------------
    * Text
    **/
   @RBEntry("Completare l'attività?")
   public static final String CONFIRM_ACTIVITY_COMPLETE_MESSAGE = "CONFIRM_ACTIVITY_COMPLETE_MESSAGE";

   @RBEntry("Riprendere l'attività?")
   public static final String CONFIRM_ACTIVITY_RESUME_MESSAGE = "CONFIRM_ACTIVITY_RESUME_MESSAGE";

   @RBEntry("Ignorare l'attività?")
   public static final String CONFIRM_ACTIVITY_SKIP_MESSAGE = "CONFIRM_ACTIVITY_SKIP_MESSAGE";

   @RBEntry("Sospendere l'attività?")
   public static final String CONFIRM_ACTIVITY_SUSPEND_MESSAGE = "CONFIRM_ACTIVITY_SUSPEND_MESSAGE";

   @RBEntry("Terminare l'attività?")
   public static final String CONFIRM_ACTIVITY_TERMINATE_MESSAGE = "CONFIRM_ACTIVITY_TERMINATE_MESSAGE";

   @RBEntry("Completare il processo?")
   public static final String CONFIRM_PROCESS_COMPLETE_MESSAGE = "CONFIRM_PROCESS_COMPLETE_MESSAGE";

   @RBEntry("Riprendere il processo?")
   public static final String CONFIRM_PROCESS_RESUME_MESSAGE = "CONFIRM_PROCESS_RESUME_MESSAGE";

   @RBEntry("Ignorare il processo?")
   public static final String CONFIRM_PROCESS_SKIP_MESSAGE = "CONFIRM_PROCESS_SKIP_MESSAGE";

   @RBEntry("Sospendere il processo?")
   public static final String CONFIRM_PROCESS_SUSPEND_MESSAGE = "CONFIRM_PROCESS_SUSPEND_MESSAGE";

   @RBEntry("Terminare il processo?")
   public static final String CONFIRM_PROCESS_TERMINATE_MESSAGE = "CONFIRM_PROCESS_TERMINATE_MESSAGE";

   /**
    * -------------------------------------------------
    * Menu
    **/
   @RBEntry("Completa...")
   public static final String ACTIVITY_COMPLETE_MENU = "ACTIVITY_COMPLETE_MENU";

   @RBEntry("Attività")
   public static final String ACTIVITY_MENU = "ACTIVITY_MENU";

   @RBEntry("Riavvia")
   public static final String ACTIVITY_RESTART_MENU = "ACTIVITY_RESTART_MENU";

   @RBEntry("Riprendi...")
   public static final String ACTIVITY_RESUME_MENU = "ACTIVITY_RESUME_MENU";

   @RBEntry("Ignora...")
   public static final String ACTIVITY_SKIP_MENU = "ACTIVITY_SKIP_MENU";

   @RBEntry("Sospendi...")
   public static final String ACTIVITY_SUSPEND_MENU = "ACTIVITY_SUSPEND_MENU";

   @RBEntry("Termina...")
   public static final String ACTIVITY_TERMINATE_MENU = "ACTIVITY_TERMINATE_MENU";

   @RBEntry("Allinea verticalmente")
   public static final String ALIGN_CENTER_X_MENU = "ALIGN_CENTER_X_MENU";

   @RBEntry("Allinea orizzontalmente")
   public static final String ALIGN_CENTER_Y_MENU = "ALIGN_CENTER_Y_MENU";

   @RBEntry("Distribuisci orizzontalmente")
   public static final String DISTRIBUTE_X_MENU = "DISTRIBUTE_X_MENU";

   @RBEntry("Distribuisci verticalmente")
   public static final String DISTRIBUTE_Y_MENU = "DISTRIBUTE_Y_MENU";

   @RBEntry("Modifica")
   public static final String EDIT_MENU = "EDIT_MENU";

   @RBEntry("Esci")
   public static final String EXIT_MENU = "EXIT_MENU";

   @RBEntry("File")
   public static final String FILE_MENU = "FILE_MENU";

   @RBEntry("Guida")
   public static final String HELP_MENU = "HELP_MENU";

   @RBEntry("Guida...")
   public static final String HELP_WINDOW_MENU = "HELP_WINDOW_MENU";

   @RBEntry("Icona/linea quadrata")
   public static final String ICON_SQUARE_MENU = "ICON_SQUARE_MENU";

   @RBEntry("Icona/linea diritta")
   public static final String ICON_STRAIGHT_MENU = "ICON_STRAIGHT_MENU";

   @RBEntry("Inverti selezione")
   public static final String INVERT_SELECTION_MENU = "INVERT_SELECTION_MENU";

   @RBEntry("Completa...")
   public static final String PROCESS_COMPLETE_MENU = "PROCESS_COMPLETE_MENU";

   @RBEntry("Processo")
   public static final String PROCESS_MENU = "PROCESS_MENU";

   @RBEntry("Riprendi...")
   public static final String PROCESS_RESUME_MENU = "PROCESS_RESUME_MENU";

   @RBEntry("Ignora...")
   public static final String PROCESS_SKIP_MENU = "PROCESS_SKIP_MENU";

   @RBEntry("Sospendi...")
   public static final String PROCESS_SUSPEND_MENU = "PROCESS_SUSPEND_MENU";

   @RBEntry("Termina...")
   public static final String PROCESS_TERMINATE_MENU = "PROCESS_TERMINATE_MENU";

   @RBEntry("Aggiorna")
   public static final String REFRESH_MENU = "REFRESH_MENU";

   @RBEntry("Seleziona tutti i link")
   public static final String SELECT_ALL_LINKS_MENU = "SELECT_ALL_LINKS_MENU";

   @RBEntry("Seleziona tutto")
   public static final String SELECT_ALL_MENU = "SELECT_ALL_MENU";

   @RBEntry("Seleziona tutti i nodi")
   public static final String SELECT_ALL_NODES_MENU = "SELECT_ALL_NODES_MENU";

   @RBEntry("Vista temporale")
   public static final String TIME_MENU = "TIME_MENU";

   @RBEntry("Commuta selezioni")
   public static final String TOGGLE_MULTISELECTION_MENU = "TOGGLE_MULTISELECTION_MENU";

   @RBEntry("Annulla tutte le selezioni")
   public static final String UNSELECT_ALL_MENU = "UNSELECT_ALL_MENU";

   @RBEntry("Visualizza")
   public static final String VIEW_MENU = "VIEW_MENU";

   @RBEntry("Scala 1:1")
   public static final String ZOOM_1_MENU = "ZOOM_1_MENU";

   @RBEntry("Zoom avanti")
   public static final String ZOOM_IN_MENU = "ZOOM_IN_MENU";

   @RBEntry("Zoom indietro")
   public static final String ZOOM_OUT_MENU = "ZOOM_OUT_MENU";

   /**
    * -------------------------------------------------
    * Tool Tips
    * -------------------------------------------------
    * Exceptions
    **/
   @RBEntry("Impossibile trovare il file di risorsa.")
   public static final String RESOURCE_FILE_MISSING_EXCEPTION = "RESOURCE_FILE_MISSING_EXCEPTION";

   /**
    * -------------------------------------------------
    * Other
    **/
   @RBEntry("Tipo non supportato dall'applet Gestione processi.")
   public static final String TYPE_NOT_SUPPORTED = "TYPE_NOT_SUPPORTED";

   @RBEntry("Sospeso da")
   public static final String COMMENT_SUSPEND = "COMMENT_SUSPEND";

   @RBEntry("Ripreso da")
   public static final String COMMENT_RESUME = "COMMENT_RESUME";

   @RBEntry("Completato da")
   public static final String COMMENT_COMPLETE = "COMMENT_COMPLETE";

   @RBEntry("Terminato da")
   public static final String COMMENT_TERMINATE = "COMMENT_TERMINATE";
   
   @RBEntry("Impossibile completare l'attività perché l'oggetto è in attesa di promozione.")
   public static final String ACTIVITY_COMPLETION_FAILED = "ACTIVITY_COMPLETION_FAILED";

   @RBEntry("Il nome dell'evento non deve iniziare con un numero.")
   public static final String EVENT_NAME_CHECK = "EVENT_NAME_CHECK";
   
   @RBEntry("Il nome dell'evento non deve contenere caratteri speciali.")
   public static final String EVENT_NAME_CHAR_CHECK = "EVENT_NAME_CHAR_CHECK";
}
