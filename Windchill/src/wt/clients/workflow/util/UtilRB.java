/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.workflow.util;

import wt.util.resource.*;

@RBUUID("wt.clients.workflow.util.UtilRB")
public final class UtilRB extends WTListResourceBundle {
   @RBEntry("OK")
   public static final String OK = "0";

   @RBEntry("Cancel")
   public static final String CANCEL = "1";

   @RBEntry("Select Process Template")
   public static final String SELECT_PROCESS_TEMPLATE = "2";

   @RBEntry("Process Templates:")
   public static final String PROCESS_TEMPLATES = "3";
}
