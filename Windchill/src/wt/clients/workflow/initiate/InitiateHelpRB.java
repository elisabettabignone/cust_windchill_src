/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.workflow.initiate;

import wt.util.resource.*;

@RBUUID("wt.clients.workflow.initiate.InitiateHelpRB")
public final class InitiateHelpRB extends WTListResourceBundle {
   @RBEntry("Test")
   public static final String TEST_STRING = "0";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_0 = "Desc/Initiate/WfInitiate";

   @RBEntry("WFProcessInitiate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_1 = "Help/Initiate/WfInitiate";
}
