/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.workflow.initiate;

import wt.util.resource.*;

@RBUUID("wt.clients.workflow.initiate.InitiateRB")
public final class InitiateRB extends WTListResourceBundle {
   /**
    * -------------------------------------------------
    * Misc
    **/
   @RBEntry("Workflow")
   public static final String WORKFLOW_TITLE = "2";

   @RBEntry(":")
   public static final String COLON = "5";

   @RBEntry("*")
   public static final String REQUIRED_FIELD = "30";

   /**
    * -------------------------------------------------
    * Labels
    **/
   @RBEntry("Due Date")
   public static final String DUE_DATE_LABEL = "10";

   @RBEntry("Priority")
   public static final String PRIORITY_LABEL = "11";

   @RBEntry("Process Variables")
   public static final String PROCESS_VARIABLES_LABEL = "12";

   @RBEntry("Project")
   public static final String PROJECT_LABEL = "20";

   @RBEntry("Due On")
   public static final String DUE_ON_LABEL = "31";

   @RBEntry("Due In")
   public static final String DUE_IN_LABEL = "32";

   @RBEntry("Delay Start")
   public static final String DELAY_START_LABEL = "33";

   @RBEntry("Team Template")
   public static final String TEAM_TEMPLATE_LABEL = "42";

   @RBEntry("Team")
   public static final String TEAM_LABEL = "43";

   @RBEntry("Process Name")
   public static final String PROCESS_NAME_LABEL = "6";

   @RBEntry("Target Object")
   public static final String TARGET_OBJECT_LABEL = "7";

   @RBEntry("Instructions")
   public static final String INSTRUCTIONS_LABEL = "8";

   @RBEntry("Description")
   public static final String DESCRIPTION_LABEL = "9";

   @RBEntry("minutes")
   public static final String MINUTES = "36";

   @RBEntry("hours")
   public static final String HOURS = "37";

   @RBEntry("days")
   public static final String DAYS = "38";

   @RBEntry("weeks")
   public static final String WEEKS = "39";

   @RBEntry("months")
   public static final String MONTHS = "40";

   /**
    * -------------------------------------------------
    * Buttons
    **/
   @RBEntry("Start Workflow Process")
   public static final String START_WORKFLOW_BUTTON = "13";

   @RBEntry("Help")
   public static final String HELP_BUTTON = "14";

   /**
    * -------------------------------------------------
    * Priorities
    **/
   @RBEntry("Highest")
   public static final String PRIORITY_ONE = "25";

   @RBEntry("High")
   public static final String PRIORITY_TWO = "26";

   @RBEntry("Normal")
   public static final String PRIORITY_THREE = "27";

   @RBEntry("Low")
   public static final String PRIORITY_FOUR = "28";

   @RBEntry("Lowest")
   public static final String PRIORITY_FIVE = "29";

   /**
    * -------------------------------------------------
    * Messages
    **/
   @RBEntry("The following problem(s) were found updating the process variables: ")
   public static final String VARIABLES_WRONG = "0";

   @RBEntry("The value for  \"{0}\"  must be a number")
   @RBArgComment0(" refers to the name of the field with the invalid attribute")
   public static final String MUST_BE_NUMBER = "1";

   @RBEntry("Workflow started.")
   public static final String WORKFLOW_STARTED = "15";

   @RBEntry("The workflow was not started: ")
   public static final String WORKFLOW_NOT_STARTED = "16";

   @RBEntry("The value supplied for  \"{0}\"  is not the correct type.")
   @RBArgComment0(" refers to the name of the field with the invalid data")
   public static final String INVALID_DATA = "21";

   @RBEntry("The value supplied for  \"{0}\"  is not a recognized date.  Date values may be entered in this format: {1}")
   @RBArgComment0(" refers to the name of the field with the invalid date")
   @RBArgComment1(" is today's date displayed in the desired format")
   public static final String INVALID_DATE = "22";

   @RBEntry("The workflow name cannot exceed {0} characters.")
   @RBArgComment0(" is the maximum character length of the workflow name field")
   public static final String WORKFLOW_NAME_TOO_BIG = "23";

   @RBEntry("The description cannot exceed {0} characters.")
   @RBArgComment0(" is the maximum character length of the description field")
   public static final String DESCRIPTION_TOO_BIG = "24";

   @RBEntry("A value must be supplied for  \"{0}\" ")
   @RBArgComment0(" refers to the name of the required field which has no value supplied")
   public static final String MISSING_REQUIRED_VALUE = "3";

   @RBEntry("Specifying the \"{0}\" offset requires a number and a unit type.")
   @RBArgComment0(" is the label of the field where the error is occurring")
   public static final String MISSING_DELAY_FIELDS = "34";

   @RBEntry("The \"{0}\" offset value must be a number.")
   @RBArgComment0(" is the label of the field where the error is occurring")
   public static final String NUMBER_REQUIRED = "35";

   @RBEntry("The value supplied for  \"{0}\" must be in the future.")
   @RBArgComment0(" refers to the name of the field with the invalid date")
   public static final String PRIOR_DATE = "41";
   
   @RBEntry("Initiate Process :")
   @RBArgComment0(" Initiate Process Label")
   public static final String INITIATE_PROCESS_LABEL = "44";
   
   @RBEntry("Initiate ")
   @RBArgComment0(" Initiate Process Label")
   public static final String INITIATE_LABEL = "45";
   

   /**
    * -------------------------------------------------
    * Test string
    **/
   @RBEntry("..Localized..")
   public static final String TEST_STRING = "999";
   
   
}
