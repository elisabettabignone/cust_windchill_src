/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.workflow.initiate;

import wt.util.resource.*;

@RBUUID("wt.clients.workflow.initiate.InitiateRB")
public final class InitiateRB_it extends WTListResourceBundle {
   /**
    * -------------------------------------------------
    * Misc
    **/
   @RBEntry("Workflow")
   public static final String WORKFLOW_TITLE = "2";

   @RBEntry(":")
   public static final String COLON = "5";

   @RBEntry("*")
   public static final String REQUIRED_FIELD = "30";

   /**
    * -------------------------------------------------
    * Labels
    **/
   @RBEntry("Data di scadenza")
   public static final String DUE_DATE_LABEL = "10";

   @RBEntry("Priorità")
   public static final String PRIORITY_LABEL = "11";

   @RBEntry("Variabili processo")
   public static final String PROCESS_VARIABLES_LABEL = "12";

   @RBEntry("Progetto")
   public static final String PROJECT_LABEL = "20";

   @RBEntry("Scadenza")
   public static final String DUE_ON_LABEL = "31";

   @RBEntry("Tempo per il completamento")
   public static final String DUE_IN_LABEL = "32";

   @RBEntry("Differimento avvio")
   public static final String DELAY_START_LABEL = "33";

   @RBEntry("Modello team")
   public static final String TEAM_TEMPLATE_LABEL = "42";

   @RBEntry("Team")
   public static final String TEAM_LABEL = "43";

   @RBEntry("Nome processo")
   public static final String PROCESS_NAME_LABEL = "6";

   @RBEntry("Oggetto di destinazione")
   public static final String TARGET_OBJECT_LABEL = "7";

   @RBEntry("Istruzioni")
   public static final String INSTRUCTIONS_LABEL = "8";

   @RBEntry("Descrizione")
   public static final String DESCRIPTION_LABEL = "9";

   @RBEntry("minuti")
   public static final String MINUTES = "36";

   @RBEntry("ore")
   public static final String HOURS = "37";

   @RBEntry("giorni")
   public static final String DAYS = "38";

   @RBEntry("settimane")
   public static final String WEEKS = "39";

   @RBEntry("mesi")
   public static final String MONTHS = "40";

   /**
    * -------------------------------------------------
    * Buttons
    **/
   @RBEntry("Avvia workflow")
   public static final String START_WORKFLOW_BUTTON = "13";

   @RBEntry("Guida")
   public static final String HELP_BUTTON = "14";

   /**
    * -------------------------------------------------
    * Priorities
    **/
   @RBEntry("Massima")
   public static final String PRIORITY_ONE = "25";

   @RBEntry("Alta")
   public static final String PRIORITY_TWO = "26";

   @RBEntry("Normale")
   public static final String PRIORITY_THREE = "27";

   @RBEntry("Bassa")
   public static final String PRIORITY_FOUR = "28";

   @RBEntry("Minima")
   public static final String PRIORITY_FIVE = "29";

   /**
    * -------------------------------------------------
    * Messages
    **/
   @RBEntry("Durante l'aggiornamento delle variabili del processo sono stati rilevati i seguenti problemi: ")
   public static final String VARIABLES_WRONG = "0";

   @RBEntry("Specificare un numero come valore per \"{0}\"")
   @RBArgComment0(" refers to the name of the field with the invalid attribute")
   public static final String MUST_BE_NUMBER = "1";

   @RBEntry("Workflow avviato.")
   public static final String WORKFLOW_STARTED = "15";

   @RBEntry("Workflow non avviato: ")
   public static final String WORKFLOW_NOT_STARTED = "16";

   @RBEntry("Il valore fornito per \"{0}\" non è di tipo corretto.")
   @RBArgComment0(" refers to the name of the field with the invalid data")
   public static final String INVALID_DATA = "21";

   @RBEntry("Il valore fornito per \"{0}\" non è una data riconosciuta. I valori della data possono essere immessi in questo formato: {1}")
   @RBArgComment0(" refers to the name of the field with the invalid date")
   @RBArgComment1(" is today's date displayed in the desired format")
   public static final String INVALID_DATE = "22";

   @RBEntry("Il nome del workflow non può superare i {0} caratteri.")
   @RBArgComment0(" is the maximum character length of the workflow name field")
   public static final String WORKFLOW_NAME_TOO_BIG = "23";

   @RBEntry("La descrizione non può superare i {0} caratteri.")
   @RBArgComment0(" is the maximum character length of the description field")
   public static final String DESCRIPTION_TOO_BIG = "24";

   @RBEntry("Fornire un valore per \"{0}\" ")
   @RBArgComment0(" refers to the name of the required field which has no value supplied")
   public static final String MISSING_REQUIRED_VALUE = "3";

   @RBEntry("La specificazione dell'offset \"{0}\" richiede un numero e un tipo di unità.")
   @RBArgComment0(" is the label of the field where the error is occurring")
   public static final String MISSING_DELAY_FIELDS = "34";

   @RBEntry("Il valore di offset \"{0}\" deve essere un numero.")
   @RBArgComment0(" is the label of the field where the error is occurring")
   public static final String NUMBER_REQUIRED = "35";

   @RBEntry("Il valore fornito per \"{0}\" deve essere una data futura.")
   @RBArgComment0(" refers to the name of the field with the invalid date")
   public static final String PRIOR_DATE = "41";
   
   @RBEntry("Avvia processo:")
   @RBArgComment0(" Initiate Process Label")
   public static final String INITIATE_PROCESS_LABEL = "44";
   
   @RBEntry("Avvia ")
   @RBArgComment0(" Initiate Process Label")
   public static final String INITIATE_LABEL = "45";
   

   /**
    * -------------------------------------------------
    * Test string
    **/
   @RBEntry("..Localizzato..")
   public static final String TEST_STRING = "999";
   
   
}
