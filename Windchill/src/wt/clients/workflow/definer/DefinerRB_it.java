/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.workflow.definer;

import wt.util.resource.*;

@RBUUID("wt.clients.workflow.definer.DefinerRB")
public final class DefinerRB_it extends WTListResourceBundle {
   /**
    * -------------------------------------------------
    * Buttons
    **/
   @RBEntry("OK")
   public static final String OK = "0";

   @RBEntry("Salva")
   public static final String SAVE = "1";

   @RBEntry("Guida")
   public static final String HELP = "2";

   @RBEntry("Annulla")
   public static final String CANCEL = "16";

   @RBEntry("Chiudi")
   public static final String CLOSE = "148";

   @RBEntry("Crea")
   public static final String CREATE = "92";

   @RBEntry("Aggiorna")
   public static final String UPDATE = "93";

   @RBEntry("Sì")
   public static final String YES = "120";

   @RBEntry("No")
   public static final String NO = "121";

   @RBEntry("Aggiorna mappatura")
   public static final String MAPPINGS = "133";

   /**
    * -------------------------------------------------
    * Radio Buttons
    **/
   @RBEntry("Sincronizza all'evento classe")
   public static final String SYNCHRONIZE_ON_EVENT = "226";

   @RBEntry("Sincronizza all'espressione")
   public static final String SYNCHRONIZE_ON_EXPRESSION = "227";

   /**
    * -------------------------------------------------
    * Labels
    **/
   @RBEntry("Ruolo")
   public static final String ROLE = "3";

   @RBEntry("Nome:")
   public static final String NAME_COLON = "310";

   @RBEntry("*Nome:")
   public static final String REQUIRED_NAME_COLON = "311";

   @RBEntry("Categoria:")
   public static final String CATEGORY_COLON = "312";

   @RBEntry("Descrizione:")
   public static final String DESCRIPTION_COLON = "313";

   @RBEntry(":")
   public static final String COLON = "4";

   @RBEntry("Errori di compilazione")
   public static final String COMPILE_ERRORS = "10";

   @RBEntry("Evento")
   public static final String EVENT = "11";

   @RBEntry("Azione")
   public static final String ACTION = "12";

   @RBEntry("...")
   public static final String MORE = "13";

   @RBEntry("?")
   public static final String UNKNOWN = "14";

   @RBEntry("Evento in {0}")
   public static final String EVENT_IN = "17";

   @RBEntry("Azione in {0}")
   public static final String ACTION_IN = "18";

   @RBEntry("Copia in {0}")
   public static final String COPYING_TO = "19";

   @RBEntry("Posizione")
   public static final String LOCATION = "20";

   @RBEntry("Attivato")
   public static final String ENABLED = "21";

   @RBEntry("Processo di convalida in corso...")
   public static final String VALIDATING_PROCESS = "110";

   @RBEntry("Convalida completata.")
   public static final String VALIDATION_COMPLETE = "111";

   @RBEntry("Errori di convalida")
   public static final String VALIDATE_ERRORS = "113";

   @RBEntry("Convalida")
   public static final String VALIDATE = "114";

   @RBEntry(" ")
   public static final String FIRE = "115";

   @RBEntry("Check-Out come")
   public static final String CHECKOUT_AS = "122";

   @RBEntry("Mappatura variabili")
   public static final String VARIABLE_MAPPING = "134";

   @RBEntry("Espressione WHILE")
   public static final String WHILE_EXPRESSION = "143";

   @RBEntry("Questa espressione viene valutata ogni volta che il blocco viene avviato. Il blocco viene rieseguito finché l'espressione risulta true.")
   public static final String WHILE_HELP = "144";

   @RBEntry("Espressione WHILE facoltativa:")
   public static final String OPTIONAL_WHILE_EXPRESSION = "145";

   @RBEntry("Converti in blocco")
   public static final String TO_BLOCK = "146";

   @RBEntry("Espandi blocco")
   public static final String EXPAND_BLOCK = "147";

   @RBEntry("Robot")
   public static final String ROBOT = "149";

   @RBEntry("Tipo di robot")
   public static final String ROBOT_TYPE = "150";

   @RBEntry("Stato specifico")
   public static final String SPECIFIC_STATE = "151";

   @RBEntry("Posizione ordinale")
   public static final String ORDINAL_LOCATION = "152";

   @RBEntry("Imposta stato del ciclo di vita")
   public static final String SETSTATE = "153";

   @RBEntry("Parametro")
   public static final String PARAMETER = "154";

   @RBEntry("Loop link")
   public static final String LOOP = "155";

   @RBEntry("Visualizza")
   public static final String VIEW = "156";

   @RBEntry("Annulla Check-Out")
   public static final String UNDO_CHECKOUT = "157";

   @RBEntry("Avvia")
   public static final String INITIATE = "158";

   @RBEntry("Ricerca dei modelli di processo in corso...")
   public static final String FINDING_TEMPLATES = "159";

   @RBEntry("Nome")
   public static final String NAME = "160";

   @RBEntry("Categoria")
   public static final String CATEGORY = "161";

   @RBEntry("Cronologia iterazioni")
   public static final String HISTORY = "162";

   @RBEntry("Schedario")
   public static final String CABINET = "163";

   @RBEntry("Scadenza")
   public static final String TIMING = "164";

   @RBEntry("Durante l'esecuzione definisci i ruoli del progetto")
   public static final String PROJECT_RESOLVE = "165";

   @RBEntry("o variabile progetto")
   public static final String PROJECT_VAR_RESOLVE = "166";

   @RBEntry("Consente all'utente di scegliere più eventi")
   public static final String MANUAL_MANY = "167";

   @RBEntry("Imposta scadenza a")
   public static final String DUE_LABEL = "168";

   @RBEntry("dall'inizio dell'attività, o")
   public static final String FROM_ACTIVITY_LABEL = "169";

   @RBEntry("dall'inizio del processo padre.")
   public static final String FROM_PROCESS_LABEL = "170";

   @RBEntry("giorni,")
   public static final String DAYS = "171";

   @RBEntry("ore e")
   public static final String HOURS = "172";

   @RBEntry("minuti")
   public static final String MINUTES = "173";

   @RBEntry("Descrizione")
   public static final String DESCRIPTION = "174";

   @RBEntry("Non è stato trovato alcun modello di processo.")
   public static final String NO_PROCESSES_FOUND = "175";

   @RBEntry("I componenti di {0} sono:")
   public static final String PROCESS_USES = "176";

   @RBEntry("{0} è impiegato da")
   public static final String PROCESS_USED_BY = "177";

   @RBEntry("Ruolo responsabile:")
   public static final String RESPONSIBLE_ROLE = "178";

   @RBEntry("Regole di gestione errori")
   public static final String ERROR_POLICIES = "179";

   @RBEntry("Notifica il ruolo responsabile in caso di errore")
   public static final String ERROR_NOTIFY = "180";

   @RBEntry("Notifica il ruolo responsabile in caso di interruzione")
   public static final String ABORT_NOTIFY = "181";

   @RBEntry("Interrompi in caso di errore")
   public static final String ERROR_ABORT = "182";

   @RBEntry("Interrompi il processo padre (se esistente) in caso di interruzione")
   public static final String ABORT_PROCESS = "183";

   @RBEntry("Conseguenze in caso di ritardo")
   public static final String OVERDUE_POLICIES = "184";

   @RBEntry("Contrassegna come terminato")
   public static final String OVERDUE_COMPLETE = "185";

   @RBEntry("Ignora")
   public static final String OVERDUE_SKIP = "186";

   @RBEntry("Riassegna al ruolo responsabile")
   public static final String OVERDUE_REASSIGN = "187";

   @RBEntry("Notifica il ruolo responsabile:")
   public static final String OVERDUE_NOTIFY1 = "188";

   @RBEntry("prima della scadenza")
   public static final String OVERDUE_NOTIFY2 = "189";

   @RBEntry("Notifica i ruoli selezionati:")
   public static final String OVERDUE_NOTIFY_LIST = "190";

   @RBEntry("Errori")
   public static final String ESCALATION_POLICIES = "191";

   @RBEntry("Variabile")
   public static final String VARIABLE = "193";

   @RBEntry("Valore")
   public static final String VALUE = "194";

   @RBEntry("Comando")
   public static final String COMMAND = "195";

   @RBEntry("Il processo è")
   public static final String PROCESS_IS = "196";

   @RBEntry("Sincrono")
   public static final String RADIO_ONE = "197";

   @RBEntry("Asincrono")
   public static final String RADIO_TWO = "198";

   @RBEntry("Argomenti")
   public static final String ARGUMENTS = "199";

   @RBEntry("Ambiente")
   public static final String ENVIRONMENT = "200";

   @RBEntry("Disponibile")
   public static final String AVAILABLE = "201";

   @RBEntry("Selezionato")
   public static final String SELECTED = "202";

   @RBEntry("Aggiungi->>")
   public static final String ADD = "203";

   @RBEntry("<<-Rimuovi")
   public static final String REMOVE = "204";

   @RBEntry("Applicazione")
   public static final String APPLICATION = "206";

   @RBEntry("Espressione")
   public static final String EXPRESSION = "207";

   @RBEntry("Timer")
   public static final String TIMER_ROBOT = "208";

   @RBEntry("Attendi:")
   public static final String WAIT_LABEL = "209";

   @RBEntry("Attendi finché il processo padre è stato eseguito per:")
   public static final String PROCESS_WAIT_LABEL = "210";

   @RBEntry("Iniziatore processo")
   public static final String PROCESS_INITIATOR = "211";

   @RBEntry("Notifiche di ritardo")
   public static final String OVERDUE_NOTIFY = "212";

   @RBEntry("Sincronizza")
   public static final String SYNCHRONIZE = "213";

   @RBEntry("È necessario fornire eventi di instradamento per un router condizionale o manuale.")
   public static final String NEED_EVENTS = "214";

   @RBEntry("Utente")
   public static final String USER = "215";

   @RBEntry("Gruppo")
   public static final String GROUP = "216";

   @RBEntry("Attore")
   public static final String ACTOR = "217";

   @RBEntry("Progetto")
   public static final String PROJECT = "218";

   @RBEntry("Assegnatario")
   public static final String ASSIGNEE_NAME = "219";

   @RBEntry("Tipo")
   public static final String ASSIGNEE_TYPE = "220";

   @RBEntry("Obbligatorio")
   public static final String ASSIGNEE_REQUIRED = "221";

   @RBEntry("Progetti")
   public static final String PROJECTS = "222";

   @RBEntry("È possibile specificare solo {0} partecipanti.")
   public static final String MAX_ASSIGNEES = "223";

   @RBEntry("Importa...")
   public static final String IMPORT = "224";

   @RBEntry("Cerca file")
   public static final String FILE_DIALOG_TITLE = "224a";

   @RBEntry("Importazione file in corso...")
   public static final String IMPORT_PROGRESS = "224b";

   @RBEntry("Importazione del modello non riuscita. Per ulteriori informazioni, vedere i log dei dettagli.")
   public static final String IMPORT_ERROR = "224c";

   @RBEntry("Esporta...")
   public static final String EXPORT = "225";

   @RBEntry("Esportazione del modello non riuscita. Vedere i log dei dettagli per informazioni supplementari.")
   public static final String EXPORT_ERROR = "225a";

   @RBEntry("Specificare o selezionare solo i file .zip o .jar.")
   public static final String WRONG_FILE_ERROR = "225b";

   @RBEntry("dopo la scadenza")
   public static final String OVERDUE_NOTIFY3 = "228";

   @RBEntry("Tutte le iterazioni di {0} saranno eliminate. Continuare l'eliminazione di {0}?")
   public static final String CONFIRM_DELETE_ITERATIONS = "229";

   @RBEntry("Rinomina...")
   public static final String RENAME = "230";

   @RBEntry("Rinomina <{0}>")
   public static final String RENAME_TITLE = "231";

   @RBEntry("Convalida tutto")
   public static final String VALIDATE_ALL = "233";

   @RBEntry("Esistono delle istanze aperte che usano il modello di processo <{0}>. Impossibile apportare modifiche al modello se queste istanze non vengono prima completate o terminate. È possibile terminare tutte le istanze in esecuzione e sospese di questo modello o salvare il modello con un nuovo nome e modificarlo.")
   public static final String IN_USE = "234";

   @RBEntry("Sfoglia...")
   public static final String BROWSE = "235";

   @RBEntry("*Processo di riferimento:")
   public static final String REFERENCED_TEMPLATE = "236";

   @RBEntry("Non specificato")
   public static final String UNSPECIFIED = "237";

   @RBEntry("Usa l'ultima iterazione")
   public static final String USE_LATEST = "238";

   @RBEntry("La variabile {0} esiste già. Usare un altro nome.")
   public static final String SAME_NAME = "239";

   @RBEntry("Tipo di instradamento:")
   public static final String ROUTING_TYPE = "240";

   @RBEntry("Tipo di connettore:")
   public static final String CONNECTOR_TYPE = "241";

   @RBEntry("Robot URL")
   public static final String URL_ROBOT = "260";

   @RBEntry("URL")
   public static final String URL = "261";

   @RBEntry("Variabile di output")
   public static final String OUTPUT_VARIABLE = "262";

   @RBEntry("Interrompi in caso di messaggio d'errore")
   public static final String ABORT_ON_ERROR = "263";

   @RBEntry("Emetti evento se compare li messaggio d'errore selezionato:")
   public static final String EMIT_ON_ERROR = "264";

   @RBEntry("Amministrazione workflow")
   public static final String WORKFLOW_ADMINISTRATOR = "265";

   @RBEntry("Modelli di processo")
   public static final String PROCESS_TEMPLATES = "266";

   @RBEntry("Durante l'esecuzione definisci i ruoli del team")
   public static final String TEAM_RESOLVE = "267";

   @RBEntry("o variabile squadra")
   public static final String TEAM_VAR_RESOLVE = "268";

   @RBEntry("Team")
   public static final String TEAM = "269";

   @RBEntry("Team")
   public static final String TEAMS = "270";

   @RBEntry("Registra riassegnazione task")
   @RBComment("Property that determines whether task assignments and reassignments are recorded in the database.")
   public static final String RECORD_REASSIGNMENT = "274";

   @RBEntry("Registra votazioni")
   @RBComment("Property that determines whether task completion information is recorded in the database.")
   public static final String RECORD_VOTING = "275";

   @RBEntry("Ignora ruoli non risolti")
   @RBComment("Property that determines whether roles that can't be mapped to a specific user are ignored.")
   public static final String IGNORE_UNRESOLVED_ROLE = "276";

   @RBEntry("Imposta coda dedicata")
   @RBComment("Property that determines whether dedicated queues are set for the process template in question.")
   public static final String DEDICATED_QUEUE = "277";

   @RBEntry("Opzioni di esecuzione")
   @RBComment("Label for tab that allows the setting of execution properties.")
   public static final String EXECUTION_OPTIONS = "278";

   @RBEntry("Proprietà di esecuzione")
   @RBComment("Title of the panel that allows the setting of execution properties.")
   public static final String CONFIG_PROPERTIES = "279";

   @RBEntry("Registra modifiche alle variabili")
   @RBComment("Property that determines whether variable changes are recorded in the database.")
   public static final String RECORD_DATA = "280";

   @RBEntry("Verranno eliminate soltanto le iterazioni di {0}. Continuare con l'eliminazione di {0}?")
   public static final String CONFIRM_DELETE_ONLYITERATIONS = "307";

   @RBEntry("Richiesta non valida")
   public static final String HTTP_BAD_REQUEST = "400";

   @RBEntry("Non autorizzato")
   public static final String HTTP_UNAUTHORIZED = "401";

   @RBEntry("Proibito")
   public static final String HTTP_FORBIDDEN = "403";

   @RBEntry("Non trovato")
   public static final String HTTP_NOT_FOUND = "404";

   @RBEntry("Le modifiche apportate al processo {0} non sono state salvate. Salvare le modifiche?")
   public static final String UNSAVED = "50";

   @RBEntry("Errore del server")
   public static final String HTTP_SERVER_ERROR = "500";

   @RBEntry("Errore interno")
   public static final String HTTP_INTERAL_ERROR = "501";

   @RBEntry("Non disponibile")
   public static final String HTTP_UNAVAILABLE = "503";

   @RBEntry("Timeout gateway")
   public static final String HTTP_GATEWAY_TIMEOUT = "504";

   @RBEntry("Le modifiche apportate al processo {0} non sono state salvate. È necessario salvare le modifiche prima di creare una copia del processo.  Salvare le modifiche prima di creare la copia?")
   public static final String UNSAVED_SAVE_AS = "51";

   @RBEntry("Nome nuovo processo")
   public static final String NEW_PROCESS_NAME = "52";

   @RBEntry("Salva con nome: <{0}>")
   public static final String SAVE_PROCESS_AS = "53";

   @RBEntry("Avanzamento della memorizzazione")
   public static final String SAVE_PROGRESS = "54";

   @RBEntry("{0} Proprietà")
   public static final String PROPERTIES_TITLE = "55";

   @RBEntry("Nome di classe Windchill")
   public static final String WINDCHILL_CLASSNAME = "59";

   @RBEntry("Altro nome di classe")
   public static final String OTHER_CLASSNAME = "60";

   @RBEntry("Altro errore")
   public static final String HTTP_OTHER_ERROR = "600";

   @RBEntry("Inizializza da")
   public static final String INITIALIZE_FROM = "61";

   @RBEntry("Copia in")
   public static final String COPY_INTO = "62";

   @RBEntry("Generale")
   public static final String GENERAL = "64";

   @RBEntry("Istruzioni")
   public static final String INSTRUCTIONS = "67";

   @RBEntry("Salvataggio in corso di {0}")
   public static final String SAVING = "74";

   @RBEntry("Aggiornamento in corso di {0}")
   public static final String REFRESHING = "75";

   @RBEntry("Oggetto")
   public static final String SUBJECT = "76";

   @RBEntry("Eventi di instradamento:")
   public static final String CUSTOM_EVENTS = "78";

   @RBEntry("Verifica sintassi")
   public static final String CHECK_SYNTAX = "79";

   @RBEntry("Espressione di instradamento:")
   public static final String ROUTING_EXPRESSION = "80";

   @RBEntry("Verifica sintassi in corso...")
   public static final String CHECKING_SYNTAX = "81";

   @RBEntry("Verifica sintassi completa.")
   public static final String SYNTAX_CHECK_COMPLETE = "82";

   @RBEntry("Durata in giorni")
   public static final String DURATION_IN_DAYS = "83";

   @RBEntry("Generale")
   public static final String IDENTIFY = "84";

   @RBEntry("Variabili")
   public static final String VARIABLES = "85";

   @RBEntry("Transizioni")
   public static final String TRANSITION_CONDITIONS = "86";

   @RBEntry("Instradamento")
   public static final String ROUTING_EVENTS = "87";

   @RBEntry("Transizione")
   public static final String TRANSITION = "88";

   @RBEntry("Condizione facoltativa per transizione: {0}")
   public static final String TRANSITION_CONDITION = "89";

   @RBEntry("Avanzamento della compilazione")
   public static final String COMPILE_PROGRESS = "9";

   @RBEntry("x")
   public static final String CHECK = "90";

   @RBEntry("Sconosciuto")
   public static final String UNKNOWN_MAPPING = "91";

   @RBEntry("Crea variabile")
   public static final String CREATE_VARIABLE = "94";

   @RBEntry("Aggiorna variabile")
   public static final String UPDATE_VARIABLE = "95";

   @RBEntry("*")
   public static final String REQUIRED = "96";

   @RBEntry("Espressione di instradamento/conteggio:")
   public static final String ROUTING_TALLYING_EXPRESSION = "97";

   /**
    * -------------------------------------------------
    * Menus
    **/
   @RBEntry("File")
   public static final String FILE = "22";

   @RBEntry("Nuovo")
   public static final String NEW = "108";

   @RBEntry("Apri...")
   public static final String OPEN = "109";

   @RBEntry("Salva con nome...")
   public static final String SAVE_AS = "23";

   @RBEntry("Esci")
   public static final String EXIT = "24";

   @RBEntry("Modifica")
   public static final String EDIT = "25";

   @RBEntry("Taglia")
   public static final String CUT = "26";

   @RBEntry("Copia")
   public static final String COPY = "27";

   @RBEntry("Incolla")
   public static final String PASTE = "28";

   @RBEntry("Elimina")
   public static final String DELETE = "29";

   @RBEntry("Seleziona tutto")
   public static final String SELECT_ALL = "30";

   @RBEntry("Processo")
   public static final String PROCESS = "31";

   @RBEntry("Componenti...")
   public static final String USES = "135";

   @RBEntry("Impiegato da...")
   public static final String USED_BY = "136";

   @RBEntry("Proprietà...")
   public static final String PROPERTIES = "32";

   @RBEntry("Oggetto")
   public static final String OBJECT = "242";

   @RBEntry("Blocco")
   public static final String BLOCK = "137";

   @RBEntry("Formato")
   public static final String LAYOUT = "33";

   @RBEntry("Allinea orizzontalmente")
   public static final String ALIGN_HORZ = "34";

   @RBEntry("Allinea verticalmente")
   public static final String ALIGN_VERT = "35";

   @RBEntry("Distribuisci orizzontalmente")
   public static final String DIST_HORZ = "36";

   @RBEntry("Distribuisci verticalmente")
   public static final String DIST_VERT = "37";

   @RBEntry("Guida")
   public static final String HELP_MENU = "129";

   @RBEntry("Guida del Workflow")
   public static final String WORKFLOW_HELP = "38";

   @RBEntry("Informazioni sul workflow")
   public static final String ABOUT_WORKFLOW = "49";

   @RBEntry("Classi Windchill...")
   public static final String WINDCHILL_CLASSES = "57";

   @RBEntry("Altra classe...")
   public static final String OTHER_CLASS = "58";

   /**
    * -------------------------------------------------
    * Titles
    **/
   @RBEntry("Workflow")
   public static final String WORKFLOW = "5";

   @RBEntry("Attività")
   public static final String ACTIVITY = "63";

   @RBEntry("Attività:")
   public static final String ACTIVITY_COLON = "314";

   @RBEntry("Invia dati come:")
   public static final String RENDER_AS = "315";

   @RBEntry("HTML")
   public static final String RENDER_AS_HTML = "316";

   @RBEntry("PDF")
   public static final String RENDER_AS_PDF = "317";

   @RBEntry("Nome modello")
   public static final String TEMPLATE_NAME = "318";

   @RBEntry("Istruzioni:")
   public static final String INSTRUCTIONS_COLON = "319";

   @RBEntry("Genera XML")
   public static final String GENERATE_XML = "320";

   @RBEntry("Variabili:")
   public static final String VARIABLES_COLON = "321";

   @RBEntry("Transizione:")
   public static final String TRANSITION_COLON = "322";

   @RBEntry("Variabile:")
   public static final String VARIABLE_COLON = "323";

   @RBEntry("Posizione:")
   public static final String LOCATION_COLON = "324";

   @RBEntry("Soglia di attivazione:")
   public static final String FIRING_THRESHOLD_COLON = "325";

   @RBEntry("Proprietà AND")
   public static final String AND_PROPERTIES = "326";

   @RBEntry("Proprietà OR")
   public static final String OR_PROPERTIES = "327";

   @RBEntry("Proprietà condizionali")
   public static final String CONDITIONAL_PROPERTIES = "328";

   @RBEntry("Proprietà soglia")
   public static final String THRESHOLD_PROPERTIES = "329";

   @RBEntry("Proprietà avvio")
   public static final String START_PROPERTIES = "330";

   @RBEntry("Oggetto:")
   public static final String SUBJECT_COLON = "331";

   @RBEntry("Modello:")
   public static final String TEMPLATE_COLON = "332";

   @RBEntry("Tipo di robot:")
   public static final String ROBOT_TYPE_COLON = "333";

   @RBEntry("Parametro:")
   public static final String PARAMETER_COLON = "334";

   @RBEntry("Stato specifico:")
   public static final String SPECIFIC_STATE_COLON = "335";

   @RBEntry("Posizione ordinale:")
   public static final String ORDINAL_LOCATION_COLON = "336";

   @RBEntry("Check-Out come:")
   public static final String CHECKOUT_AS_COLON = "337";

   @RBEntry("Proprietà Check-In")
   public static final String CHECKIN_PROPERTIES = "338";

   @RBEntry("Proprietà Check-Out")
   public static final String CHECKOUT_PROPERTIES = "339";

   @RBEntry("Proprietà declassamento")
   public static final String DEMOTE_PROPERTIES = "340";

   @RBEntry("Proprietà negazione")
   public static final String DENY_PROPERTIES = "341";

   @RBEntry("Proprietà rilascio")
   public static final String DROP_PROPERTIES = "342";

   @RBEntry("Proprietà promozione")
   public static final String PROMOTE_PROPERTIES = "343";

   @RBEntry("Proprietà impostazione stato del ciclo di vita")
   public static final String SETSTATE_PROPERTIES = "344";

   @RBEntry("Proprietà invio")
   public static final String SUBMIT_PROPERTIES = "345";

   @RBEntry("Comando:")
   public static final String COMMAND_COLON = "346";

   @RBEntry("Il processo è:")
   public static final String PROCESS_IS_COLON = "347";

   @RBEntry("Argomenti:")
   public static final String ARGUMENTS_COLON = "348";

   @RBEntry("Ambiente:")
   public static final String ENVIRONMENT_COLON = "349";

   @RBEntry("Valore:")
   public static final String VALUE_COLON = "350";

   @RBEntry("Classe Windchill:")
   public static final String WINDCHILL_CLASS_COLON = "351";

   @RBEntry("Evento:")
   public static final String EVENT_COLON = "352";

   @RBEntry("Oggetto Windchill:")
   public static final String WINDCHILL_OBJECT_COLON = "353";

   @RBEntry("Nome tipo:")
   public static final String TYPE_NAME_COLON = "354";

   @RBEntry("Valore di default:")
   public static final String DEFAULT_VALUE_COLON = "355";

   @RBEntry("Nome di classe Windchill:")
   public static final String WINDCHILL_CLASSNAME_COLON = "356";

   @RBEntry("Altro nome di classe:")
   public static final String OTHER_CLASSNAME_COLON = "357";

   @RBEntry("Inizializza da:")
   public static final String INITIALIZE_FROM_COLON = "358";

   @RBEntry("Copia in:")
   public static final String COPY_INTO_COLON = "359";

   @RBEntry("Ruoli definiti")
   public static final String DEFINED_ROLES = "360";

   @RBEntry("Rimuovi ")
   public static final String REMOVE_LABEL = "361";

   @RBEntry("Impostazione ruolo")
   public static final String ROLE_SETUP = "362";

   @RBEntry("Ruolo:")
   public static final String ROLE_COLON = "363";

   @RBEntry("Pool:")
   public static final String POOLS_COLON = "364";

   @RBEntry("Pool di risorse")
   public static final String RESOURCE_POOL = "365";

   @RBEntry("Team contenitore")
   public static final String CONTAINER_TEAM = "366";

   @RBEntry("Inglese")
   public static final String LANGUAGE_ENGLISH = "367";

   @RBEntry("Francese")
   public static final String LANGUAGE_FRENCH = "368";

   @RBEntry("Tedesco")
   public static final String LANGUAGE_GERMAN = "369";

   @RBEntry("Italiano")
   public static final String LANGUAGE_ITALIAN = "370";

   @RBEntry("Giapponese")
   public static final String LANGUAGE_JAPANESE = "371";

   @RBEntry("Coreano")
   public static final String LANGUAGE_KOREAN = "372";

   @RBEntry("Panoramica del processo:")
   public static final String PROCESS_OVERVIEW_COLON = "373";

   @RBEntry("Seleziona lingua")
   public static final String SELECT_LOCALE = "374";

   @RBEntry("Continua")
   public static final String CONTINUE = "375";

   @RBEntry("Lingua:")
   public static final String LOCALE_COLON = "376";

   @RBEntry("Team contesto")
   public static final String CONTEXT_TEAM = "377";

   @RBEntry("Ad hoc")
   public static final String ADHOC = "232";

   @RBEntry("Metodo interno")
   public static final String INTERNAL_METHOD = "68";

   @RBEntry("Notifica")
   public static final String NOTIFY = "69";

   @RBEntry("Invia")
   public static final String SUBMIT = "98";

   @RBEntry("Promuovi")
   public static final String PROMOTE = "99";

   @RBEntry("Declassa")
   public static final String DEMOTE = "100";

   @RBEntry("Rifiuta")
   public static final String DENY = "101";

   @RBEntry("Abbandona")
   public static final String DROP = "102";

   @RBEntry("Stampa")
   public static final String PRINT = "103";

   @RBEntry("Check-Out")
   public static final String CHECKOUT = "104";

   @RBEntry("Check-In")
   public static final String CHECKIN = "105";

   @RBEntry("Applicazione chiamata")
   public static final String INVOKED_APPL = "72";

   @RBEntry("Proprietà link")
   public static final String LINK_TITLE = "15";

   @RBEntry("Attività")
   public static final String TASK = "65";

   @RBEntry("Partecipanti")
   public static final String PARTICIPANTS = "66";

   @RBEntry("Definizione del metodo")
   public static final String METHOD_SPECIFICATION = "70";

   @RBEntry("Definizione dell'applicazione")
   public static final String APPLICATION_SPECIFICATION = "73";

   @RBEntry("Messaggio")
   public static final String MESSAGE = "77";

   @RBEntry("Destinatari")
   public static final String RECIPIENTS = "106";

   @RBEntry("Condizionale")
   public static final String CONDITIONAL = "107";

   @RBEntry("Salva modifiche")
   public static final String SAVE_CHANGES = "119";

   @RBEntry("Eliminare l'iterazione {0}?")
   public static final String ITERATION_QUESTION = "257";

   @RBEntry("Elimina ultima iterazione")
   public static final String ITERATION_BUTTON = "258";

   /**
    * -------------------------------------------------
    * Exceptions
    **/
   @RBEntry("Impossibile inizializzare la Guida in linea:")
   public static final String HELP_INITIALIZATION_FAILED = "6";

   @RBEntry("Si è verificata un'eccezione durante la localizzazione: ")
   public static final String LOCALIZING_FAILED = "7";

   @RBEntry("Si è verificata un'eccezione durante l'inizializzazione dei dati:")
   public static final String INITIALIZATION_FAILED = "8";

   @RBEntry("La stringa di input non è valida come soglia di attivazione.")
   public static final String INVALID_INPUT_THRESHOLD = "56";

   @RBEntry("Fornire un tipo di ritorno e tutti i parametri prima di salvare un metodo.")
   public static final String NEED_ALL_PARAMETERS = "71";

   @RBEntry("È stata specificata una durata non valida.  Fornire un valore numerico.")
   public static final String INVALID_DURATION = "116";

   @RBEntry("Fornire un'espressione di instradamento per attivare gli eventi di instradamento.")
   public static final String NEED_EXPRESSION = "117";

   @RBEntry("Il nome {0} è troppo lungo. La lunghezza deve essere uguale o inferiore a {1} caratteri.")
   public static final String NAME_TOO_LARGE = "118";

   @RBEntry("Descrizione troppo lunga. La lunghezza deve essere uguale o inferiore a {0} caratteri.")
   public static final String DESCRIPTION_TOO_LARGE = "123";

   @RBEntry("Espressione troppo lunga. La lunghezza deve essere uguale o inferiore a {0} caratteri.")
   public static final String EXPRESSION_TOO_LARGE = "192";

   @RBEntry("Specificare un nome prima di salvare.")
   public static final String REQUIRED_NAME = "124";

   @RBEntry("Nessuna proprietà associata all'attività da modificare.")
   public static final String NO_PROPERTIES = "125";

   @RBEntry("Eliminazione di un modello di processo non consentita.")
   public static final String NO_DELETE = "126";

   @RBEntry("Creazione di un modello di processo non consentita.")
   public static final String CANT_CREATE = "127";

   @RBEntry("Modifica del modello di processo non consentita. È consentita la visualizzazione.")
   public static final String CANT_MODIFY = "128";

   @RBEntry("Il nome dell'oggetto {0} è troppo lungo. La lunghezza deve essere uguale o inferiore a {1} caratteri.")
   public static final String SUBJECT_TOO_LARGE = "130";

   @RBEntry("Specificare un oggetto prima di salvare.")
   public static final String REQUIRED_SUBJECT = "131";

   @RBEntry("Valore di default")
   public static final String DEFAULT = "132";

   @RBEntry("Impossibile collegare un indicatore Messa a terra ad un indicatore Fine.")
   public static final String NO_LINK_TO = "138";

   @RBEntry("Impossibile collegare ad un connettore Inizio.")
   public static final String NO_LINK_FROM = "139";

   @RBEntry("È necessario creare i riferimenti del modello di processo in proxy prima di poter tracciare delle linee di collegamento verso di esso.")
   public static final String NO_LINK_TO_REF = "140";

   @RBEntry("È necessario creare i riferimenti del modello di processo in proxy prima di poter tracciare delle linee di collegamento da esso.")
   public static final String NO_LINK_FROM_REF = "141";

   @RBEntry("È necessario creare i riferimenti del modello di processo prima di continuare. Per indicare un processo, fare clic sul pulsante Sfoglia, selezionare un processo e premere OK.")
   public static final String NO_REFERENCE = "142";

   @RBEntry("Immettere un comando da eseguire.")
   public static final String REQUIRED_COMMAND = "205";

   @RBEntry("Termina le attività ancora in esecuzione dopo l'attivazione")
   public static final String TERMINATE_PREDECESSORS = "243";

   @RBEntry("Un modello di processo con questo nome esiste già. Modificare il nome e riprovare.")
   public static final String ALREADY_EXISTS = "259";

   @RBEntry("Inserisci:")
   public static final String INSERT = "244";

   @RBEntry("Evento classe")
   public static final String CLASS_EVENT = "245";

   @RBEntry("Evento oggetto")
   public static final String OBJECT_EVENT = "246";

   @RBEntry("Oggetto Windchill")
   public static final String WINDCHILL_OBJECT = "247";

   @RBEntry("Classe Windchill")
   public static final String WINDCHILL_CLASS = "248";

   @RBEntry("Sincronizza a:")
   public static final String SYNCHRONIZE_ON = "249";

   @RBEntry("Sola lettura")
   public static final String READ_ONLY = "250";

   @RBEntry("Permessi:")
   public static final String PERMISSIONS = "251";

   @RBEntry("Controllo completo (tutti)")
   public static final String FULL_CONTROL = "252";

   @RBEntry("Altro")
   public static final String OTHER = "253";

   @RBEntry("Lettura")
   public static final String READ = "254";

   @RBEntry("Modifica")
   public static final String MODIFY = "255";

   @RBEntry("Amministrazione")
   public static final String ADMINISTRATIVE = "256";

   @RBEntry("Organizzazione")
   public static final String Organization = "271";

   @RBEntry("Contesto")
   @RBComment("Represents the container that a given template is a part of. Eg. ClassicContainer or OrgContainer or ExchangeContainer")
   public static final String CONTEXT = "273";

   /**
    * Please translate into localize versions
    * of this files.
    **/
   @RBEntry("Espressione iniziale")
   public static final String INITIAL_EXPRESSION = "272";

   /**
    * Display name dialog
    **/
   @RBEntry("Lingua:")
   public static final String LANGUAGE_LABEL = "281";

   @RBEntry("Nome visualizzato:")
   public static final String DISPLAYNAME_LABEL = "282";

   @RBEntry("Cinese (semplificato)")
   public static final String SIMPLIFIED_CHINESE_LABEL = "283";

   @RBEntry("Cinese (tradizionale)")
   public static final String TRADITIONAL_CHINESE_LABEL = "284";

   @RBEntry("Default")
   public static final String DEFAULT_LABEL = "285";

   @RBEntry("Definisci nome visualizzato")
   public static final String DEFINE_DISPLAY_NAME = "286";

   @RBEntry("Nome visualizzato")
   public static final String DISPLAY_NAME = "287";

   /**
    * 
    **/
   @RBEntry("Formato numero non valido")
   public static final String INVALID_NUMBER_FORMAT = "288";

   @RBEntry("Panoramica")
   @RBComment("Label for tab that allows the management of Process Overview documents.")
   public static final String OVERVIEW = "289";

   @RBEntry("Aggiungi")
   public static final String ADD_OVERVIEW = "290";

   @RBEntry("Sostituisci")
   public static final String REPLACE = "291";

   @RBEntry("Scarica")
   public static final String DOWNLOAD = "292";

   @RBEntry("Panoramica del processo")
   public static final String PROCESS_OVERVIEW = "293";

   @RBEntry("Lingua")
   public static final String LOCALE_LANGUAGE = "294";

   @RBEntry("Nome file")
   public static final String FILENAME = "295";

   @RBEntry("Allegati")
   public static final String ATTACHMENTS = "296";

   @RBEntry("Proprietà allegato")
   public static final String ATTACHMENT_PROPERTIES = "297";

   @RBEntry("Business object principale del processo")
   public static final String PBO_LABEL = "298";

   @RBEntry("Contenuto principale")
   public static final String PRIMARY_CONTENT = "299";

   @RBEntry("Contenuto secondario")
   public static final String SECONDARY_CONTENT = "300";

   @RBEntry("Informazioni attributo")
   public static final String ATTRIBUTE_INFO = "301";

   @RBEntry("Seleziona variabili di processo")
   public static final String SELECT_VARIABLES = "302";

   @RBEntry("Modello")
   public static final String TEMPLATE = "303";

   @RBEntry("E-mail ")
   public static final String EMAIL = "304";

   @RBEntry("Indirizzo e-mail")
   public static final String EMAIL_ADDR = "305";

   @RBEntry("Nome utente")
   public static final String EMAIL_NAME = "306";

   @RBEntry("Una variabile richiede un nome e un valore")
   public static final String VARAIBLE_DETAILS = "308";

   @RBEntry("Le iterazioni di {0} verranno eliminate. Proseguire?")
   public static final String WORKFLOW_DELETE_CONFIRMATION = "309";

   @RBEntry("Sposta")
   public static final String MOVE = "378";

   @RBEntry("Spostare?")
   public static final String WORKFLOW_MOVE_CONFIRMATION = "379";

   @RBEntry("Impossibile spostare il modello di processo: un modello con lo stesso nome esiste già.")
   public static final String WORKFLOW_ALREADY_EXIST = "380";

   @RBEntry("Il nome modello processo non può essere più lungo di 200 caratteri. Modificarlo e riprovare.")
   public static final String CANT_EXCEED_NAME_CHARACTERS = "381";

   @RBEntry("È stata specificata una durata non valida. Specificare un valore numerico positivo.")
   public static final String NEGATIVE_DURATION = "382";

   @RBEntry("Esternalizza espressioni")
   public static final String EXTERNALIZE_EXPRESSIONS = "385";

   @RBEntry("Nome package:")
   public static final String EXT_PACKAGE_NAME = "386";

   @RBEntry("Esternalizza ultima iterazione")
   public static final String EXT_LATEST_ITERATION = "387";

   @RBEntry("Esternalizza tutte le iterazioni")
   public static final String EXT_ALL_ITERATIONS = "388";

   @RBEntry("Effettua Check-Out prima di esternalizzare")
   public static final String CHECKOUT_BEFORE_EXTRENALIZE = "389";

   @RBEntry("Scegliere un'opzione")
   public static final String CHOOSE_AN_OPTION = "390";

   @RBEntry("Avviso")
   public static final String NOTICE = "391";

   @RBEntry("Modello di workflow già esternalizzato.")
   public static final String ALREADY_EXTERNALIZED = "392";

   @RBEntry("ATTENZIONE: il modello di workflow selezionato non contiene espressioni da esternalizzare.")
   public static final String EXTLATEST_NO_EXPRESSIONS = "393";

   @RBEntry("Tutte le espressioni del modello sono già state esternalizzate.")
   public static final String NO_MODIFICATION = "394";

   @RBEntry("Alcune espressioni del modello sono già state esternalizzate. Le espressioni non esternalizzate in precedenza sono state esternalizzate per questa iterazione.")
   public static final String SOME_MODIFICATION = "395";

   @RBEntry("Le espressioni del modello di workflow sono state esternalizzate e compilate. Per maggiori dettagli, vedere il file di log WfExternalization.log nella directory dei log di Windchill.")
   public static final String ALL_EXTERNALIZED = "396";

   @RBEntry("Punto d'arresto")
   public static final String CHECK_POINT = "397";

   @RBEntry("Proprietà punto d'arresto")
   public static final String CHECK_POINT_PROPERTIES = "398";

   @RBEntry("Usa oggetto da modello")
   public static final String NOTIFICATION_SUBJECT_TEMPLATE = "399";

   @RBEntry("ATTENZIONE: nessuna iterazione del modello di workflow selezionato contiene espressioni da esternalizzare.")
   public static final String EXTALL_NO_EXPRESSIONS = "405";

   @RBEntry("ATTENZIONE: errore di compilazione. Le espressioni del modello di workflow sono state esternalizzate ma non è stato possibile compilare uno o più file java. Per maggiori dettagli, vedere il file di log WfExternalization.log nella directory dei log di Windchill.")
   public static final String EXT_SUCCESS_NOT_COMPILED = "406";

   @RBEntry("ATTENZIONE: operazione di esternalizzazione non riuscita. Per maggiori dettagli, vedere il file di log WfExternalization.log.")
   public static final String EXT_FAILED = "407";
   
   
   @RBEntry("ATTENZIONE: è possibile impostare solo le cassi Evolvable e Persistable come variabili di workflow.")
   public static final String OBJECT_NOT_EVOLVABLE_PERSISTABLE = "408";
   
   @RBEntry("Un {0} con questo nome esiste già. Cambiare il nome e riprovare")
   public static final String NAME_ALREADY_EXIST = "410";
   
   @RBEntry("AVVERTENZA: è possibile impostare solo le cassi Evolvable e Persistable come variabili di workflow.")
   public static final String OBJECT_NOT_EVOLVABLE_PERSISTABLE_WARNING = "411";
   
   @RBEntry("Il file jar o zip non contiene alcun file XML dell'oggetto.")
   public static final String JAR_FILE_DOES_NOT_CONTAINS_XML = "412";
   
   @RBEntry("ATTENZIONE: errore durante l'apertura del file jar o zip.")
   public static final String JAR_FILE_IO_ERROR = "413";
   
   @RBEntry("ATTENZIONE: errore di compilazione. Espressione non compilata a causa del seguente errore di compilazione: {0}")
   public static final String COMPILILATION_FAILED_AT_SAVING = "414";
   
   @RBEntry("URL non valido: {0}")
   public static final String INVALID_URL = "415";
      
}
