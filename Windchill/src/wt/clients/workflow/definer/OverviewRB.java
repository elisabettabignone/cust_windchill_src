/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.workflow.definer;

import wt.util.resource.*;

@RBUUID("wt.clients.workflow.definer.OverviewRB")
public final class OverviewRB extends WTListResourceBundle {
   /**
    * -------------------------------------------------
    * List of Locale selection in Overview Locales dropdown button
    **/
   @RBEntry("Default")
   public static final String DEFAULT = "default";

   @RBEntry("English")
   public static final String en = "en";

   @RBEntry("English (United States)")
   public static final String en_US = "en_US";

   @RBEntry("English (United Kingdom)")
   public static final String en_GB = "en_GB";

   @RBEntry("French")
   public static final String fr = "fr";

   @RBEntry("German")
   public static final String de = "de";

   @RBEntry("Italian")
   public static final String it = "it";

   @RBEntry("Spanish")
   public static final String es = "es";

   @RBEntry("Korean")
   public static final String ko = "ko";

   @RBEntry("Chinese (Simplified)")
   public static final String zh_CN = "zh_CN";

   @RBEntry("Chinese (Traditional)")
   public static final String zh_TW = "zh_TW";

   @RBEntry("Japanese")
   public static final String ja = "ja";

   @RBEntry("Russian")
   public static final String ru = "ru";
}
