/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.workflow.definer;

import wt.util.resource.*;

@RBUUID("wt.clients.workflow.definer.DefinerRB")
public final class DefinerRB extends WTListResourceBundle {
   /**
    * -------------------------------------------------
    * Buttons
    **/
   @RBEntry("OK")
   public static final String OK = "0";

   @RBEntry("Save")
   public static final String SAVE = "1";

   @RBEntry("Help")
   public static final String HELP = "2";

   @RBEntry("Cancel")
   public static final String CANCEL = "16";

   @RBEntry("Close")
   public static final String CLOSE = "148";

   @RBEntry("Create")
   public static final String CREATE = "92";

   @RBEntry("Update")
   public static final String UPDATE = "93";

   @RBEntry("Yes")
   public static final String YES = "120";

   @RBEntry("No")
   public static final String NO = "121";

   @RBEntry("Update Mapping")
   public static final String MAPPINGS = "133";

   /**
    * -------------------------------------------------
    * Radio Buttons
    **/
   @RBEntry("Synchronize on Class Event")
   public static final String SYNCHRONIZE_ON_EVENT = "226";

   @RBEntry("Synchronize on Expression")
   public static final String SYNCHRONIZE_ON_EXPRESSION = "227";

   /**
    * -------------------------------------------------
    * Labels
    **/
   @RBEntry("Role")
   public static final String ROLE = "3";

   @RBEntry("Name:")
   public static final String NAME_COLON = "310";

   @RBEntry("*Name:")
   public static final String REQUIRED_NAME_COLON = "311";

   @RBEntry("Category:")
   public static final String CATEGORY_COLON = "312";

   @RBEntry("Description:")
   public static final String DESCRIPTION_COLON = "313";

   @RBEntry(":")
   public static final String COLON = "4";

   @RBEntry("Compile Errors")
   public static final String COMPILE_ERRORS = "10";

   @RBEntry("Event")
   public static final String EVENT = "11";

   @RBEntry("Action")
   public static final String ACTION = "12";

   @RBEntry("...")
   public static final String MORE = "13";

   @RBEntry("?")
   public static final String UNKNOWN = "14";

   @RBEntry("Event in {0}")
   public static final String EVENT_IN = "17";

   @RBEntry("Action in {0}")
   public static final String ACTION_IN = "18";

   @RBEntry("Copying to {0}")
   public static final String COPYING_TO = "19";

   @RBEntry("Location")
   public static final String LOCATION = "20";

   @RBEntry("Enabled")
   public static final String ENABLED = "21";

   @RBEntry("Validating...")
   public static final String VALIDATING_PROCESS = "110";

   @RBEntry("Validation complete.")
   public static final String VALIDATION_COMPLETE = "111";

   @RBEntry("Validation Errors")
   public static final String VALIDATE_ERRORS = "113";

   @RBEntry("Validate")
   public static final String VALIDATE = "114";

   @RBEntry(" ")
   public static final String FIRE = "115";

   @RBEntry("Checkout As")
   public static final String CHECKOUT_AS = "122";

   @RBEntry("Variable Mapping")
   public static final String VARIABLE_MAPPING = "134";

   @RBEntry("While Expression")
   public static final String WHILE_EXPRESSION = "143";

   @RBEntry("This expression is evaluated every time the block is started.  As long as this expression returns true, the block will be re-executed.")
   public static final String WHILE_HELP = "144";

   @RBEntry("Optional While Expression:")
   public static final String OPTIONAL_WHILE_EXPRESSION = "145";

   @RBEntry("Convert to Block")
   public static final String TO_BLOCK = "146";

   @RBEntry("Expand Block")
   public static final String EXPAND_BLOCK = "147";

   @RBEntry("Robot")
   public static final String ROBOT = "149";

   @RBEntry("Robot Type")
   public static final String ROBOT_TYPE = "150";

   @RBEntry("Specific State")
   public static final String SPECIFIC_STATE = "151";

   @RBEntry("Ordinal Location")
   public static final String ORDINAL_LOCATION = "152";

   @RBEntry("Set State")
   public static final String SETSTATE = "153";

   @RBEntry("Parameter")
   public static final String PARAMETER = "154";

   @RBEntry("Loop Link")
   public static final String LOOP = "155";

   @RBEntry("View")
   public static final String VIEW = "156";

   @RBEntry("Undo Check Out")
   public static final String UNDO_CHECKOUT = "157";

   @RBEntry("Initiate")
   public static final String INITIATE = "158";

   @RBEntry("Finding Process Templates...")
   public static final String FINDING_TEMPLATES = "159";

   @RBEntry("Name")
   public static final String NAME = "160";

   @RBEntry("Category")
   public static final String CATEGORY = "161";

   @RBEntry("Iteration History")
   public static final String HISTORY = "162";

   @RBEntry("Cabinet")
   public static final String CABINET = "163";

   @RBEntry("Deadline")
   public static final String TIMING = "164";

   @RBEntry("At runtime, resolve roles from project")
   public static final String PROJECT_RESOLVE = "165";

   @RBEntry("or project variable")
   public static final String PROJECT_VAR_RESOLVE = "166";

   @RBEntry("Allow the user to choose multiple events")
   public static final String MANUAL_MANY = "167";

   @RBEntry("Set deadline to")
   public static final String DUE_LABEL = "168";

   @RBEntry("from the start of the activity, or")
   public static final String FROM_ACTIVITY_LABEL = "169";

   @RBEntry("from the start of the parent process.")
   public static final String FROM_PROCESS_LABEL = "170";

   @RBEntry("days,")
   public static final String DAYS = "171";

   @RBEntry("hours, and")
   public static final String HOURS = "172";

   @RBEntry("minutes")
   public static final String MINUTES = "173";

   @RBEntry("Description")
   public static final String DESCRIPTION = "174";

   @RBEntry("No process templates found.")
   public static final String NO_PROCESSES_FOUND = "175";

   @RBEntry("{0} uses")
   public static final String PROCESS_USES = "176";

   @RBEntry("{0} is used by")
   public static final String PROCESS_USED_BY = "177";

   @RBEntry("Responsible Role:")
   public static final String RESPONSIBLE_ROLE = "178";

   @RBEntry("Error Handling Policies")
   public static final String ERROR_POLICIES = "179";

   @RBEntry("Notify the responsible role if there is an error")
   public static final String ERROR_NOTIFY = "180";

   @RBEntry("Notify the responsible role on abort")
   public static final String ABORT_NOTIFY = "181";

   @RBEntry("Abort if there is an error")
   public static final String ERROR_ABORT = "182";

   @RBEntry("Abort the parent process (if any) on abort")
   public static final String ABORT_PROCESS = "183";

   @RBEntry("Overdue Consequences")
   public static final String OVERDUE_POLICIES = "184";

   @RBEntry("Mark complete")
   public static final String OVERDUE_COMPLETE = "185";

   @RBEntry("Skip")
   public static final String OVERDUE_SKIP = "186";

   @RBEntry("Reassign to the responsible role")
   public static final String OVERDUE_REASSIGN = "187";

   @RBEntry("Notify the responsible role:")
   public static final String OVERDUE_NOTIFY1 = "188";

   @RBEntry("before the deadline")
   public static final String OVERDUE_NOTIFY2 = "189";

   @RBEntry("Notify the selected roles:")
   public static final String OVERDUE_NOTIFY_LIST = "190";

   @RBEntry("Errors")
   public static final String ESCALATION_POLICIES = "191";

   @RBEntry("Variable")
   public static final String VARIABLE = "193";

   @RBEntry("Value")
   public static final String VALUE = "194";

   @RBEntry("Command")
   public static final String COMMAND = "195";

   @RBEntry("Process is")
   public static final String PROCESS_IS = "196";

   @RBEntry("Synchronous")
   public static final String RADIO_ONE = "197";

   @RBEntry("Asynchronous")
   public static final String RADIO_TWO = "198";

   @RBEntry("Arguments")
   public static final String ARGUMENTS = "199";

   @RBEntry("Environment")
   public static final String ENVIRONMENT = "200";

   @RBEntry("Available")
   public static final String AVAILABLE = "201";

   @RBEntry("Selected")
   public static final String SELECTED = "202";

   @RBEntry("Add->>")
   public static final String ADD = "203";

   @RBEntry("<<-Remove")
   public static final String REMOVE = "204";

   @RBEntry("Application")
   public static final String APPLICATION = "206";

   @RBEntry("Expression")
   public static final String EXPRESSION = "207";

   @RBEntry("Timer")
   public static final String TIMER_ROBOT = "208";

   @RBEntry("Wait for:")
   public static final String WAIT_LABEL = "209";

   @RBEntry("Wait until the parent process has run for:")
   public static final String PROCESS_WAIT_LABEL = "210";

   @RBEntry("Process Initiator")
   public static final String PROCESS_INITIATOR = "211";

   @RBEntry("Overdue Notifications")
   public static final String OVERDUE_NOTIFY = "212";

   @RBEntry("Synchronize")
   public static final String SYNCHRONIZE = "213";

   @RBEntry("You must supply routing events for a conditional or manual router.")
   public static final String NEED_EVENTS = "214";

   @RBEntry("User")
   public static final String USER = "215";

   @RBEntry("Group")
   public static final String GROUP = "216";

   @RBEntry("Actor")
   public static final String ACTOR = "217";

   @RBEntry("Project")
   public static final String PROJECT = "218";

   @RBEntry("Assignee")
   public static final String ASSIGNEE_NAME = "219";

   @RBEntry("Type")
   public static final String ASSIGNEE_TYPE = "220";

   @RBEntry("Required")
   public static final String ASSIGNEE_REQUIRED = "221";

   @RBEntry("Projects")
   public static final String PROJECTS = "222";

   @RBEntry("You can only specify {0} participants.")
   public static final String MAX_ASSIGNEES = "223";

   @RBEntry("Import...")
   public static final String IMPORT = "224";

   @RBEntry("Browse for file")
   public static final String FILE_DIALOG_TITLE = "224a";

   @RBEntry("Importing file...")
   public static final String IMPORT_PROGRESS = "224b";

   @RBEntry("Import of template failed. Please see detailed logs for additional information.")
   public static final String IMPORT_ERROR = "224c";

   @RBEntry("Export...")
   public static final String EXPORT = "225";

   @RBEntry("Export of template failed. Please see detailed logs for additional information.")
   public static final String EXPORT_ERROR = "225a";

   @RBEntry("Please enter or select files of type .zip or .jar only.")
   public static final String WRONG_FILE_ERROR = "225b";

   @RBEntry("after the deadline")
   public static final String OVERDUE_NOTIFY3 = "228";

   @RBEntry("All iterations of {0} will be deleted. Continue deleting {0}?")
   public static final String CONFIRM_DELETE_ITERATIONS = "229";

   @RBEntry("Rename...")
   public static final String RENAME = "230";

   @RBEntry("Rename <{0}>")
   public static final String RENAME_TITLE = "231";

   @RBEntry("Validate All")
   public static final String VALIDATE_ALL = "233";

   @RBEntry("There are open instances using process template <{0}>. You cannot make changes to this template unless you first complete or terminate these process instances. You can either terminate all running and suspended instances of this template or save this template under a new name and modify this new template.")
   public static final String IN_USE = "234";

   @RBEntry("Browse...")
   public static final String BROWSE = "235";

   @RBEntry("*Referenced Process:")
   public static final String REFERENCED_TEMPLATE = "236";

   @RBEntry("Unspecified")
   public static final String UNSPECIFIED = "237";

   @RBEntry("Use Latest Iteration")
   public static final String USE_LATEST = "238";

   @RBEntry("The variable {0} already exists, please use another name.")
   public static final String SAME_NAME = "239";

   @RBEntry("Routing Type:")
   public static final String ROUTING_TYPE = "240";

   @RBEntry("Connector Type:")
   public static final String CONNECTOR_TYPE = "241";

   @RBEntry("URL Robot")
   public static final String URL_ROBOT = "260";

   @RBEntry("URL")
   public static final String URL = "261";

   @RBEntry("Output Variable")
   public static final String OUTPUT_VARIABLE = "262";

   @RBEntry("Abort on error response code")
   public static final String ABORT_ON_ERROR = "263";

   @RBEntry("Emit event on selected error response code:")
   public static final String EMIT_ON_ERROR = "264";

   @RBEntry("Workflow Administrator")
   public static final String WORKFLOW_ADMINISTRATOR = "265";

   @RBEntry("Process Templates")
   public static final String PROCESS_TEMPLATES = "266";

   @RBEntry("At runtime, resolve roles from team")
   public static final String TEAM_RESOLVE = "267";

   @RBEntry("or team variable")
   public static final String TEAM_VAR_RESOLVE = "268";

   @RBEntry("Team")
   public static final String TEAM = "269";

   @RBEntry("Teams")
   public static final String TEAMS = "270";

   @RBEntry("Record task reassignment")
   @RBComment("Property that determines whether task assignments and reassignments are recorded in the database.")
   public static final String RECORD_REASSIGNMENT = "274";

   @RBEntry("Record voting")
   @RBComment("Property that determines whether task completion information is recorded in the database.")
   public static final String RECORD_VOTING = "275";

   @RBEntry("Ignore unresolved role")
   @RBComment("Property that determines whether roles that can't be mapped to a specific user are ignored.")
   public static final String IGNORE_UNRESOLVED_ROLE = "276";

   @RBEntry("Set dedicated queue")
   @RBComment("Property that determines whether dedicated queues are set for the process template in question.")
   public static final String DEDICATED_QUEUE = "277";

   @RBEntry("Execution Options")
   @RBComment("Label for tab that allows the setting of execution properties.")
   public static final String EXECUTION_OPTIONS = "278";

   @RBEntry("Execution Properties")
   @RBComment("Title of the panel that allows the setting of execution properties.")
   public static final String CONFIG_PROPERTIES = "279";

   @RBEntry("Record variable changes")
   @RBComment("Property that determines whether variable changes are recorded in the database.")
   public static final String RECORD_DATA = "280";

   @RBEntry("Only iterations of {0} will be deleted. Continue deleting {0}?")
   public static final String CONFIRM_DELETE_ONLYITERATIONS = "307";

   @RBEntry("Bad Request")
   public static final String HTTP_BAD_REQUEST = "400";

   @RBEntry("Unauthorized")
   public static final String HTTP_UNAUTHORIZED = "401";

   @RBEntry("Forbidden")
   public static final String HTTP_FORBIDDEN = "403";

   @RBEntry("Not Found")
   public static final String HTTP_NOT_FOUND = "404";

   @RBEntry("Changes were made to process {0} which have not been saved. Save these changes?")
   public static final String UNSAVED = "50";

   @RBEntry("Server Error")
   public static final String HTTP_SERVER_ERROR = "500";

   @RBEntry("Internal Error")
   public static final String HTTP_INTERAL_ERROR = "501";

   @RBEntry("Unavailable")
   public static final String HTTP_UNAVAILABLE = "503";

   @RBEntry("Gateway Timeout")
   public static final String HTTP_GATEWAY_TIMEOUT = "504";

   @RBEntry("Changes were made to process {0} which have not been saved. These changes need to be saved before a copy of the process can be created.  Save these changes before making the copy?")
   public static final String UNSAVED_SAVE_AS = "51";

   @RBEntry("New Process Name")
   public static final String NEW_PROCESS_NAME = "52";

   @RBEntry("Save As <{0}>")
   public static final String SAVE_PROCESS_AS = "53";

   @RBEntry("Save Progress")
   public static final String SAVE_PROGRESS = "54";

   @RBEntry("{0} Properties")
   public static final String PROPERTIES_TITLE = "55";

   @RBEntry("Windchill Classname")
   public static final String WINDCHILL_CLASSNAME = "59";

   @RBEntry("Other Classname")
   public static final String OTHER_CLASSNAME = "60";

   @RBEntry("Other Error")
   public static final String HTTP_OTHER_ERROR = "600";

   @RBEntry("Initialize From")
   public static final String INITIALIZE_FROM = "61";

   @RBEntry("Copy Into")
   public static final String COPY_INTO = "62";

   @RBEntry("General")
   public static final String GENERAL = "64";

   @RBEntry("Instructions")
   public static final String INSTRUCTIONS = "67";

   @RBEntry("Saving {0}")
   public static final String SAVING = "74";

   @RBEntry("Refreshing {0}")
   public static final String REFRESHING = "75";

   @RBEntry("Subject")
   public static final String SUBJECT = "76";

   @RBEntry("Routing Events:")
   public static final String CUSTOM_EVENTS = "78";

   @RBEntry("Check Syntax")
   public static final String CHECK_SYNTAX = "79";

   @RBEntry("Routing Expression:")
   public static final String ROUTING_EXPRESSION = "80";

   @RBEntry("Checking Syntax...")
   public static final String CHECKING_SYNTAX = "81";

   @RBEntry("Syntax check complete.")
   public static final String SYNTAX_CHECK_COMPLETE = "82";

   @RBEntry("Duration In Days")
   public static final String DURATION_IN_DAYS = "83";

   @RBEntry("General")
   public static final String IDENTIFY = "84";

   @RBEntry("Variables")
   public static final String VARIABLES = "85";

   @RBEntry("Transitions")
   public static final String TRANSITION_CONDITIONS = "86";

   @RBEntry("Routing")
   public static final String ROUTING_EVENTS = "87";

   @RBEntry("Transition")
   public static final String TRANSITION = "88";

   @RBEntry("Optional Condition For Transition: {0}")
   public static final String TRANSITION_CONDITION = "89";

   @RBEntry("Compile Progress")
   public static final String COMPILE_PROGRESS = "9";

   @RBEntry("x")
   public static final String CHECK = "90";

   @RBEntry("Unknown")
   public static final String UNKNOWN_MAPPING = "91";

   @RBEntry("Create Variable")
   public static final String CREATE_VARIABLE = "94";

   @RBEntry("Update Variable")
   public static final String UPDATE_VARIABLE = "95";

   @RBEntry("*")
   public static final String REQUIRED = "96";

   @RBEntry("Routing/Tallying Expression:")
   public static final String ROUTING_TALLYING_EXPRESSION = "97";

   /**
    * -------------------------------------------------
    * Menus
    **/
   @RBEntry("File")
   public static final String FILE = "22";

   @RBEntry("New")
   public static final String NEW = "108";

   @RBEntry("Open...")
   public static final String OPEN = "109";

   @RBEntry("Save As...")
   public static final String SAVE_AS = "23";

   @RBEntry("Exit")
   public static final String EXIT = "24";

   @RBEntry("Edit")
   public static final String EDIT = "25";

   @RBEntry("Cut")
   public static final String CUT = "26";

   @RBEntry("Copy")
   public static final String COPY = "27";

   @RBEntry("Paste")
   public static final String PASTE = "28";

   @RBEntry("Delete")
   public static final String DELETE = "29";

   @RBEntry("Select All")
   public static final String SELECT_ALL = "30";

   @RBEntry("Process")
   public static final String PROCESS = "31";

   @RBEntry("Uses...")
   public static final String USES = "135";

   @RBEntry("Used By...")
   public static final String USED_BY = "136";

   @RBEntry("Properties...")
   public static final String PROPERTIES = "32";

   @RBEntry("Object")
   public static final String OBJECT = "242";

   @RBEntry("Block")
   public static final String BLOCK = "137";

   @RBEntry("Format")
   public static final String LAYOUT = "33";

   @RBEntry("Align Horizontally")
   public static final String ALIGN_HORZ = "34";

   @RBEntry("Align Vertically")
   public static final String ALIGN_VERT = "35";

   @RBEntry("Distribute Horizontally")
   public static final String DIST_HORZ = "36";

   @RBEntry("Distribute Vertically")
   public static final String DIST_VERT = "37";

   @RBEntry("Help")
   public static final String HELP_MENU = "129";

   @RBEntry("Workflow Help")
   public static final String WORKFLOW_HELP = "38";

   @RBEntry("About Workflow")
   public static final String ABOUT_WORKFLOW = "49";

   @RBEntry("Windchill Classes...")
   public static final String WINDCHILL_CLASSES = "57";

   @RBEntry("Other Class...")
   public static final String OTHER_CLASS = "58";

   /**
    * -------------------------------------------------
    * Titles
    **/
   @RBEntry("Workflow")
   public static final String WORKFLOW = "5";

   @RBEntry("Activity")
   public static final String ACTIVITY = "63";

   @RBEntry("Activity:")
   public static final String ACTIVITY_COLON = "314";

   @RBEntry("Render As:")
   public static final String RENDER_AS = "315";

   @RBEntry("HTML")
   public static final String RENDER_AS_HTML = "316";

   @RBEntry("PDF")
   public static final String RENDER_AS_PDF = "317";

   @RBEntry("Template Name")
   public static final String TEMPLATE_NAME = "318";

   @RBEntry("Instructions:")
   public static final String INSTRUCTIONS_COLON = "319";

   @RBEntry("Generate XML")
   public static final String GENERATE_XML = "320";

   @RBEntry("Variables:")
   public static final String VARIABLES_COLON = "321";

   @RBEntry("Transition:")
   public static final String TRANSITION_COLON = "322";

   @RBEntry("Variable:")
   public static final String VARIABLE_COLON = "323";

   @RBEntry("Location:")
   public static final String LOCATION_COLON = "324";

   @RBEntry("Firing Threshold:")
   public static final String FIRING_THRESHOLD_COLON = "325";

   @RBEntry("And Properties")
   public static final String AND_PROPERTIES = "326";

   @RBEntry("Or Properties")
   public static final String OR_PROPERTIES = "327";

   @RBEntry("Conditional Properties")
   public static final String CONDITIONAL_PROPERTIES = "328";

   @RBEntry("Threshold Properties")
   public static final String THRESHOLD_PROPERTIES = "329";

   @RBEntry("Start Properties")
   public static final String START_PROPERTIES = "330";

   @RBEntry("Subject:")
   public static final String SUBJECT_COLON = "331";

   @RBEntry("Template:")
   public static final String TEMPLATE_COLON = "332";

   @RBEntry("Robot Type:")
   public static final String ROBOT_TYPE_COLON = "333";

   @RBEntry("Parameter:")
   public static final String PARAMETER_COLON = "334";

   @RBEntry("Specific State:")
   public static final String SPECIFIC_STATE_COLON = "335";

   @RBEntry("Ordinal Location:")
   public static final String ORDINAL_LOCATION_COLON = "336";

   @RBEntry("Checkout As:")
   public static final String CHECKOUT_AS_COLON = "337";

   @RBEntry("Checkin Properties")
   public static final String CHECKIN_PROPERTIES = "338";

   @RBEntry("Checkout Properties")
   public static final String CHECKOUT_PROPERTIES = "339";

   @RBEntry("Demote Properties")
   public static final String DEMOTE_PROPERTIES = "340";

   @RBEntry("Deny Properties")
   public static final String DENY_PROPERTIES = "341";

   @RBEntry("Drop Properties")
   public static final String DROP_PROPERTIES = "342";

   @RBEntry("Promote Properties")
   public static final String PROMOTE_PROPERTIES = "343";

   @RBEntry("Set State Properties")
   public static final String SETSTATE_PROPERTIES = "344";

   @RBEntry("Submit Properties")
   public static final String SUBMIT_PROPERTIES = "345";

   @RBEntry("Command:")
   public static final String COMMAND_COLON = "346";

   @RBEntry("Process is:")
   public static final String PROCESS_IS_COLON = "347";

   @RBEntry("Arguments:")
   public static final String ARGUMENTS_COLON = "348";

   @RBEntry("Environment:")
   public static final String ENVIRONMENT_COLON = "349";

   @RBEntry("Value:")
   public static final String VALUE_COLON = "350";

   @RBEntry("Windchill Class:")
   public static final String WINDCHILL_CLASS_COLON = "351";

   @RBEntry("Event:")
   public static final String EVENT_COLON = "352";

   @RBEntry("Windchill Object:")
   public static final String WINDCHILL_OBJECT_COLON = "353";

   @RBEntry("Type Name:")
   public static final String TYPE_NAME_COLON = "354";

   @RBEntry("Default Value:")
   public static final String DEFAULT_VALUE_COLON = "355";

   @RBEntry("Windchill Classname:")
   public static final String WINDCHILL_CLASSNAME_COLON = "356";

   @RBEntry("Other Classname:")
   public static final String OTHER_CLASSNAME_COLON = "357";

   @RBEntry("Initialize From:")
   public static final String INITIALIZE_FROM_COLON = "358";

   @RBEntry("Copy Into:")
   public static final String COPY_INTO_COLON = "359";

   @RBEntry("Defined Roles")
   public static final String DEFINED_ROLES = "360";

   @RBEntry("Remove")
   public static final String REMOVE_LABEL = "361";

   @RBEntry("Role Setup")
   public static final String ROLE_SETUP = "362";

   @RBEntry("Role:")
   public static final String ROLE_COLON = "363";

   @RBEntry("Pools:")
   public static final String POOLS_COLON = "364";

   @RBEntry("Resource Pool")
   public static final String RESOURCE_POOL = "365";

   @RBEntry("Container Team")
   public static final String CONTAINER_TEAM = "366";

   @RBEntry("English")
   public static final String LANGUAGE_ENGLISH = "367";

   @RBEntry("French")
   public static final String LANGUAGE_FRENCH = "368";

   @RBEntry("German")
   public static final String LANGUAGE_GERMAN = "369";

   @RBEntry("Italian")
   public static final String LANGUAGE_ITALIAN = "370";

   @RBEntry("Japanese")
   public static final String LANGUAGE_JAPANESE = "371";

   @RBEntry("Korean")
   public static final String LANGUAGE_KOREAN = "372";

   @RBEntry("Process Overview:")
   public static final String PROCESS_OVERVIEW_COLON = "373";

   @RBEntry("Select Locale")
   public static final String SELECT_LOCALE = "374";

   @RBEntry("Continue")
   public static final String CONTINUE = "375";

   @RBEntry("Locale:")
   public static final String LOCALE_COLON = "376";

   @RBEntry("Context Team")
   public static final String CONTEXT_TEAM = "377";

   @RBEntry("Ad Hoc")
   public static final String ADHOC = "232";

   @RBEntry("Internal Method")
   public static final String INTERNAL_METHOD = "68";

   @RBEntry("Notification")
   public static final String NOTIFY = "69";

   @RBEntry("Submit")
   public static final String SUBMIT = "98";

   @RBEntry("Promote")
   public static final String PROMOTE = "99";

   @RBEntry("Demote")
   public static final String DEMOTE = "100";

   @RBEntry("Deny")
   public static final String DENY = "101";

   @RBEntry("Drop")
   public static final String DROP = "102";

   @RBEntry("Print")
   public static final String PRINT = "103";

   @RBEntry("Check Out")
   public static final String CHECKOUT = "104";

   @RBEntry("Check In")
   public static final String CHECKIN = "105";

   @RBEntry("Invoked Appl")
   public static final String INVOKED_APPL = "72";

   @RBEntry("Link Properties")
   public static final String LINK_TITLE = "15";

   @RBEntry("Activity")
   public static final String TASK = "65";

   @RBEntry("Participants")
   public static final String PARTICIPANTS = "66";

   @RBEntry("Method Specification")
   public static final String METHOD_SPECIFICATION = "70";

   @RBEntry("Application Specification")
   public static final String APPLICATION_SPECIFICATION = "73";

   @RBEntry("Message")
   public static final String MESSAGE = "77";

   @RBEntry("Recipients")
   public static final String RECIPIENTS = "106";

   @RBEntry("Conditional")
   public static final String CONDITIONAL = "107";

   @RBEntry("Save Changes")
   public static final String SAVE_CHANGES = "119";

   @RBEntry("Do you wish to delete the following iteration: {0} ?")
   public static final String ITERATION_QUESTION = "257";

   @RBEntry("Delete Latest Iteration")
   public static final String ITERATION_BUTTON = "258";

   /**
    * -------------------------------------------------
    * Exceptions
    **/
   @RBEntry("The Help system could not be initialized: ")
   public static final String HELP_INITIALIZATION_FAILED = "6";

   @RBEntry("An exception occurred during localization: ")
   public static final String LOCALIZING_FAILED = "7";

   @RBEntry("An exception occurred initializing the data: ")
   public static final String INITIALIZATION_FAILED = "8";

   @RBEntry("The input string is invalid for Firing Threshold.")
   public static final String INVALID_INPUT_THRESHOLD = "56";

   @RBEntry("You must supply a return type and all parameters before a method can be saved.")
   public static final String NEED_ALL_PARAMETERS = "71";

   @RBEntry("The duration you have specified is incorrect.  You must supply a numeric value.")
   public static final String INVALID_DURATION = "116";

   @RBEntry("You must supply an routing expression to fire your routing events.")
   public static final String NEED_EXPRESSION = "117";

   @RBEntry("The name {0} is too large.  It must be {1} characters or less.")
   public static final String NAME_TOO_LARGE = "118";

   @RBEntry("The description is too large.  It must be {0} characters or less.")
   public static final String DESCRIPTION_TOO_LARGE = "123";

   @RBEntry("The expression is too large.  It must be {0} characters or less.")
   public static final String EXPRESSION_TOO_LARGE = "192";

   @RBEntry("A name is required before saving.")
   public static final String REQUIRED_NAME = "124";

   @RBEntry("This activity has no properties to modify.")
   public static final String NO_PROPERTIES = "125";

   @RBEntry("Deleting a Process Template is not allowed.")
   public static final String NO_DELETE = "126";

   @RBEntry("You are not allowed to create a Process Template.")
   public static final String CANT_CREATE = "127";

   @RBEntry("You are not allowed to modify the Process Template, but you can view it.")
   public static final String CANT_MODIFY = "128";

   @RBEntry("The subject {0} is too large.  It must be {1} characters or less.")
   public static final String SUBJECT_TOO_LARGE = "130";

   @RBEntry("A subject is required before saving.")
   public static final String REQUIRED_SUBJECT = "131";

   @RBEntry("Default Value")
   public static final String DEFAULT = "132";

   @RBEntry("You cannot draw a link from a Ground or End flag.")
   public static final String NO_LINK_TO = "138";

   @RBEntry("You cannot draw a link to a Start flag.")
   public static final String NO_LINK_FROM = "139";

   @RBEntry("You must first reference a Process Template in the Proxy before you can draw links to it.")
   public static final String NO_LINK_TO_REF = "140";

   @RBEntry("You must first reference a Process Template in the Proxy before you can draw links from it.")
   public static final String NO_LINK_FROM_REF = "141";

   @RBEntry("You must reference a Process Template before continuing. To supply a process, click on the Browse button, select a process, and click OK.")
   public static final String NO_REFERENCE = "142";

   @RBEntry("You must supply a command to execute.")
   public static final String REQUIRED_COMMAND = "205";

   @RBEntry("Terminate Open Predecessor Activities When Fired")
   public static final String TERMINATE_PREDECESSORS = "243";

   @RBEntry("A Process Template with this name already exists. Change the name and try again.")
   public static final String ALREADY_EXISTS = "259";

   @RBEntry("Insert:")
   public static final String INSERT = "244";

   @RBEntry("Class Event")
   public static final String CLASS_EVENT = "245";

   @RBEntry("Object Event")
   public static final String OBJECT_EVENT = "246";

   @RBEntry("Windchill Object")
   public static final String WINDCHILL_OBJECT = "247";

   @RBEntry("Windchill Class")
   public static final String WINDCHILL_CLASS = "248";

   @RBEntry("Synchronize On:")
   public static final String SYNCHRONIZE_ON = "249";

   @RBEntry("Read Only")
   public static final String READ_ONLY = "250";

   @RBEntry("Permissions:")
   public static final String PERMISSIONS = "251";

   @RBEntry("Full Control (All)")
   public static final String FULL_CONTROL = "252";

   @RBEntry("Other")
   public static final String OTHER = "253";

   @RBEntry("Read")
   public static final String READ = "254";

   @RBEntry("Modify")
   public static final String MODIFY = "255";

   @RBEntry("Administrative")
   public static final String ADMINISTRATIVE = "256";

   @RBEntry("Organization")
   public static final String Organization = "271";

   @RBEntry("Context")
   @RBComment("Represents the container that a given template is a part of. Eg. ClassicContainer or OrgContainer or ExchangeContainer")
   public static final String CONTEXT = "273";

   /**
    * Please translate into localize versions
    * of this files.
    **/
   @RBEntry("Initial Expression")
   public static final String INITIAL_EXPRESSION = "272";

   /**
    * Display name dialog
    **/
   @RBEntry("Language:")
   public static final String LANGUAGE_LABEL = "281";

   @RBEntry("Display Name:")
   public static final String DISPLAYNAME_LABEL = "282";

   @RBEntry("Chinese(Simplified)")
   public static final String SIMPLIFIED_CHINESE_LABEL = "283";

   @RBEntry("Chinese(Traditional)")
   public static final String TRADITIONAL_CHINESE_LABEL = "284";

   @RBEntry("Default")
   public static final String DEFAULT_LABEL = "285";

   @RBEntry("Define Display Name")
   public static final String DEFINE_DISPLAY_NAME = "286";

   @RBEntry("Display Name")
   public static final String DISPLAY_NAME = "287";

   /**
    * 
    **/
   @RBEntry("Invalid Number Format")
   public static final String INVALID_NUMBER_FORMAT = "288";

   @RBEntry("Overview")
   @RBComment("Label for tab that allows the management of Process Overview documents.")
   public static final String OVERVIEW = "289";

   @RBEntry("Add")
   public static final String ADD_OVERVIEW = "290";

   @RBEntry("Replace")
   public static final String REPLACE = "291";

   @RBEntry("Download")
   public static final String DOWNLOAD = "292";

   @RBEntry("Process Overview")
   public static final String PROCESS_OVERVIEW = "293";

   @RBEntry("Locale Language")
   public static final String LOCALE_LANGUAGE = "294";

   @RBEntry("Filename")
   public static final String FILENAME = "295";

   @RBEntry("Attachments")
   public static final String ATTACHMENTS = "296";

   @RBEntry("Attachment Properties")
   public static final String ATTACHMENT_PROPERTIES = "297";

   @RBEntry("Primary Business Object of Process")
   public static final String PBO_LABEL = "298";

   @RBEntry("Primary Content")
   public static final String PRIMARY_CONTENT = "299";

   @RBEntry("Secondary Content")
   public static final String SECONDARY_CONTENT = "300";

   @RBEntry("Attribute Information")
   public static final String ATTRIBUTE_INFO = "301";

   @RBEntry("Select Process Variables")
   public static final String SELECT_VARIABLES = "302";

   @RBEntry("Template")
   public static final String TEMPLATE = "303";

   @RBEntry("E-mail")
   public static final String EMAIL = "304";

   @RBEntry("E-mail Address")
   public static final String EMAIL_ADDR = "305";

   @RBEntry("User Name")
   public static final String EMAIL_NAME = "306";

   @RBEntry("A variable requires a Name and Value")
   public static final String VARAIBLE_DETAILS = "308";

   @RBEntry("Iteration(s) of {0} will be deleted. Continue deleting {0}?")
   public static final String WORKFLOW_DELETE_CONFIRMATION = "309";

   @RBEntry("Move")
   public static final String MOVE = "378";

   @RBEntry("Are you sure you want to move?")
   public static final String WORKFLOW_MOVE_CONFIRMATION = "379";

   @RBEntry("Can not move the process template as the template with same name already exist.")
   public static final String WORKFLOW_ALREADY_EXIST = "380";

   @RBEntry("Process template name cannot be greater than 200 characters. Change the name and try again.")
   public static final String CANT_EXCEED_NAME_CHARACTERS = "381";

   @RBEntry("The duration you have specified is incorrect. You must supply a positive numeric value.")
   public static final String NEGATIVE_DURATION = "382";

   @RBEntry("Externalize Expressions")
   public static final String EXTERNALIZE_EXPRESSIONS = "385";

   @RBEntry("Package name:")
   public static final String EXT_PACKAGE_NAME = "386";

   @RBEntry("Externalize latest iteration")
   public static final String EXT_LATEST_ITERATION = "387";

   @RBEntry("Externalize all iterations")
   public static final String EXT_ALL_ITERATIONS = "388";

   @RBEntry("Check out before externalize")
   public static final String CHECKOUT_BEFORE_EXTRENALIZE = "389";

   @RBEntry("Choose an option")
   public static final String CHOOSE_AN_OPTION = "390";

   @RBEntry("Notice")
   public static final String NOTICE = "391";

   @RBEntry("This workflow template is already externalized.")
   public static final String ALREADY_EXTERNALIZED = "392";

   @RBEntry("ATTENTION: The selected workflow template contains no expressions to externalize.")
   public static final String EXTLATEST_NO_EXPRESSIONS = "393";

   @RBEntry("All expressions in this template have already been externalized.")
   public static final String NO_MODIFICATION = "394";

   @RBEntry("Some of the expressions in the template have already been externalized. Those expressions which have not been externalized earlier have been externalized for this iteration.")
   public static final String SOME_MODIFICATION = "395";

   @RBEntry("The expressions for the selected workflow template have been successfully externalized and compiled. Please check the log file WfExternalization.log in the Windchill logs directory for details.")
   public static final String ALL_EXTERNALIZED = "396";

   @RBEntry("Checkpoint")
   public static final String CHECK_POINT = "397";

   @RBEntry("Checkpoint Properties")
   public static final String CHECK_POINT_PROPERTIES = "398";

   @RBEntry("Use subject from template")
   public static final String NOTIFICATION_SUBJECT_TEMPLATE = "399";

   @RBEntry("ATTENTION: None of the iterations of the selected workflow template contain any expressions to externalize.")
   public static final String EXTALL_NO_EXPRESSIONS = "405";

   @RBEntry("ATTENTION: Compilation Error. The expressions for this workflow template have been successfully externalized; however, one or more of the generated Java files could not be compiled. Check the WfExternalization.log file in the Windchill logs directory for details.")
   public static final String EXT_SUCCESS_NOT_COMPILED = "406";

   @RBEntry("ATTENTION: The externalization operation failed. Check the WfExternalization.log file for details.")
   public static final String EXT_FAILED = "407";
   
   
   @RBEntry("ATTENTION: Only Evolvable or Persistable classes can be set as workflow variables.")
   public static final String OBJECT_NOT_EVOLVABLE_PERSISTABLE = "408";
   
   @RBEntry("A {0} with this name already exists. Change the name and try again")
   public static final String NAME_ALREADY_EXIST = "410";
   
   @RBEntry("WARNING:Only Evolvable or Persistable classes can be set as workflow variables.")
   public static final String OBJECT_NOT_EVOLVABLE_PERSISTABLE_WARNING = "411";
   
   @RBEntry("The jar/zip file does not contains any object\'s XML file.")
   public static final String JAR_FILE_DOES_NOT_CONTAINS_XML = "412";
   
   @RBEntry("ATTENTION:Error in opening jar/zip file.")
   public static final String JAR_FILE_IO_ERROR = "413";
   
   @RBEntry("ATTENTION:Compilation Error.The expression failed to compile.The compilation error is:{0}")
   public static final String COMPILILATION_FAILED_AT_SAVING = "414";
   
   @RBEntry("Invalid URL : {0}")
   public static final String INVALID_URL = "415";
      
}
