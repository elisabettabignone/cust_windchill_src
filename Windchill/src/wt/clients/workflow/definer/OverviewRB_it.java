/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.workflow.definer;

import wt.util.resource.*;

@RBUUID("wt.clients.workflow.definer.OverviewRB")
public final class OverviewRB_it extends WTListResourceBundle {
   /**
    * -------------------------------------------------
    * List of Locale selection in Overview Locales dropdown button
    **/
   @RBEntry("Default")
   public static final String DEFAULT = "default";

   @RBEntry("Inglese")
   public static final String en = "en";

   @RBEntry("Inglese (Stati Uniti)")
   public static final String en_US = "en_US";

   @RBEntry("Inglese (Regno Unito)")
   public static final String en_GB = "en_GB";

   @RBEntry("Francese")
   public static final String fr = "fr";

   @RBEntry("Tedesco")
   public static final String de = "de";

   @RBEntry("Italiano")
   public static final String it = "it";

   @RBEntry("Spagnolo")
   public static final String es = "es";

   @RBEntry("Coreano")
   public static final String ko = "ko";

   @RBEntry("Cinese (semplificato)")
   public static final String zh_CN = "zh_CN";

   @RBEntry("Cinese (tradizionale)")
   public static final String zh_TW = "zh_TW";

   @RBEntry("Giapponese")
   public static final String ja = "ja";

   @RBEntry("Russo")
   public static final String ru = "ru";
}
