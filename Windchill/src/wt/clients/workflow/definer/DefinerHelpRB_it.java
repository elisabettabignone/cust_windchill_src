/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.workflow.definer;

import wt.util.resource.*;

@RBUUID("wt.clients.workflow.definer.DefinerHelpRB")
public final class DefinerHelpRB_it extends WTListResourceBundle {
   @RBEntry("Indietro")
   public static final String BACK = "0";

   @RBEntry("Avanti")
   public static final String FORWARD = "1";

   @RBEntry("Distribuisci verticalmente")
   public static final String DIST_VERT = "10";

   @RBEntry("Strumento di selezione")
   public static final String SELECTION_TOOL = "11";

   @RBEntry("Strumento di collegamento operazioni")
   public static final String LINK_TOOL = "12";

   @RBEntry("Attività assegnata")
   public static final String ASSIGNED_ACTIVITY = "13";

   @RBEntry("Processo")
   public static final String PROCESS = "14";

   @RBEntry("Metodo interno")
   public static final String INTERNAL_METHOD = "15";

   @RBEntry("Notifica per posta elettronica")
   public static final String NOTIFY = "16";

   @RBEntry("Applicazione chiamata")
   public static final String INVOKED_APPL = "17";

   @RBEntry("Connettore AND")
   public static final String AND_CONNECTOR = "18";

   @RBEntry("Connettore OR")
   public static final String OR_CONNECTOR = "19";

   @RBEntry("Su")
   public static final String UP = "2";

   @RBEntry("Connettore soglia")
   public static final String THRESHOLD_CONNECTOR = "20";

   @RBEntry("Inizio")
   public static final String START_CONNECTOR = "21";

   @RBEntry("Fine")
   public static final String END_CONNECTOR = "22";

   @RBEntry("Messa a terra")
   public static final String GROUND_CONNECTOR = "23";

   @RBEntry("Invia")
   public static final String SUBMIT = "24";

   @RBEntry("Promuovi")
   public static final String PROMOTE = "25";

   @RBEntry("Declassa")
   public static final String DEMOTE = "26";

   @RBEntry("Rifiuta")
   public static final String DENY = "27";

   @RBEntry("Abbandona")
   public static final String DROP = "28";

   @RBEntry("Stampa")
   public static final String PRINT = "29";

   @RBEntry("Taglia")
   public static final String CUT = "3";

   @RBEntry("Check-Out")
   public static final String CHECKOUT = "30";

   @RBEntry("Check-In")
   public static final String CHECKIN = "31";

   @RBEntry("Router condizionale")
   public static final String CONDITIONAL = "32";

   @RBEntry("Blocco")
   public static final String BLOCK = "33";

   @RBEntry("Collegamenti retti")
   public static final String STRAIT_LINES = "34";

   @RBEntry("Collegamenti ad angolo")
   public static final String SQUARE_LINES = "35";

   @RBEntry("Method robot")
   public static final String ROBOT = "36";

   @RBEntry("Avvia l'applicazione")
   public static final String APPLICATION = "37";

   @RBEntry("Esegui l'espressione")
   public static final String EXPRESSION = "38";

   @RBEntry("Timer")
   public static final String TIMER_ROBOT = "39";

   @RBEntry("Copia")
   public static final String COPY = "4";

   @RBEntry("Sincronizza")
   public static final String SYNCHRONIZE = "40";

   @RBEntry("Attività ad hoc")
   public static final String ADHOC_ACTIVITY = "41";

   @RBEntry("Robot URL")
   public static final String URL_ROBOT = "42";

   @RBEntry("Punto d'arresto")
   public static final String CHECK_POINT = "43";

   @RBEntry("Incolla")
   public static final String PASTE = "5";

   @RBEntry("Elimina")
   public static final String DELETE = "6";

   @RBEntry("Allinea orizzontalmente")
   public static final String ALIGN_HORZ = "7";

   @RBEntry("Allinea verticalmente")
   public static final String ALIGN_VERT = "8";

   @RBEntry("Distribuisci orizzontalmente")
   public static final String DIST_HORZ = "9";

   @RBEntry("AndHelp")
   @RBPseudo(false)
   public static final String AND = "AndHelp";

   @RBEntry("ApplicationHelp")
   @RBPseudo(false)
   public static final String APPLICATION_ROBOT = "ApplicationHelp";

   @RBEntry("AssignedDeadlineHelp")
   @RBPseudo(false)
   public static final String ASSIGNED_DEADLINE = "AssignedDeadlineHelp";

   @RBEntry("AssignedErrorsHelp")
   @RBPseudo(false)
   public static final String ASSIGNED_ERRORS = "AssignedErrorsHelp";

   @RBEntry("AssignedEventsHelp")
   @RBPseudo(false)
   public static final String ASSIGNED_EVENTS = "AssignedEventsHelp";

   @RBEntry("AssignedExpressionHelp")
   @RBPseudo(false)
   public static final String ASSIGNED_EXPRESSION = "AssignedExpressionHelp";

   @RBEntry("AssignedIdentityHelp")
   @RBPseudo(false)
   public static final String ASSIGNED_IDENTITY = "AssignedIdentityHelp";

   @RBEntry("AdhocIdentityHelp")
   @RBPseudo(false)
   public static final String ADHOC_IDENTITY = "AdhocIdentityHelp";

   @RBEntry("AssignedParticipantsHelp")
   @RBPseudo(false)
   public static final String ASSIGNED_PARTICIPANTS = "AssignedParticipantsHelp";

   @RBEntry("AssignedSynchronizationHelp")
   @RBPseudo(false)
   public static final String ASSIGNED_SYNCHRONIZATION = "AssignedSynchronizationHelp";

   @RBEntry("AssignedTaskHelp")
   @RBPseudo(false)
   public static final String ASSIGNED_TASK = "AssignedTaskHelp";

   @RBEntry("AssignedTransitionHelp")
   @RBPseudo(false)
   public static final String ASSIGNED_TRANSITION = "AssignedTransitionHelp";

   @RBEntry("AssignedVariablesHelp")
   @RBPseudo(false)
   public static final String ASSIGNED_VARIABLES = "AssignedVariablesHelp";

   @RBEntry("BlockExpressionHelp")
   @RBPseudo(false)
   public static final String BLOCK_EXPRESSION = "BlockExpressionHelp";

   @RBEntry("BlockIdentityHelp")
   @RBPseudo(false)
   public static final String BLOCK_IDENTITY = "BlockIdentityHelp";

   @RBEntry("CheckoutHelp")
   @RBPseudo(false)
   public static final String CHECKOUT_AS = "CheckoutHelp";

   @RBEntry("ConditionalHelp")
   @RBPseudo(false)
   public static final String CONDITION = "ConditionalHelp";

   @RBEntry("ConnectorEventsHelp")
   @RBPseudo(false)
   public static final String CONNECTOR_EVENTS = "ConnectorEventsHelp";

   @RBEntry(" ")
   @RBPseudo(false)
   public static final String PRIVATE_CONSTANT_0 = "Desc/Definer/WfDefiner";

   @RBEntry("ExpressionExpressionHelp")
   @RBPseudo(false)
   public static final String EXPRESSION_EXPRESSION = "ExpressionExpressionHelp";

   @RBEntry("ExpressionIdentityHelp")
   @RBPseudo(false)
   public static final String EXPRESSION_IDENTITY = "ExpressionIdentityHelp";

   @RBEntry("RoleSetupHelp")
   @RBPseudo(false)
   public static final String ASSIGNED_ROLESETUP = "RoleSetupHelp";

   @RBEntry("ResourcePoolHelp")
   @RBPseudo(false)
   public static final String ASSIGNED_POOL = "ResourcePoolHelp";

   @RBEntry("WFConnectorAnd")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_1 = "Help/Definer/AndHelp";

   @RBEntry("WFRobotLaunchAppl")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/Definer/ApplicationHelp";

   @RBEntry("WFTabAactDeadline")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_3 = "Help/Definer/AssignedDeadlineHelp";

   @RBEntry("WFTabAactExecOpt")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_4 = "Help/Definer/AssignedErrorsHelp";

   @RBEntry("WFTabAactRoute")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_5 = "Help/Definer/AssignedEventsHelp";

   @RBEntry("WFTabAactGen")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_6 = "Help/Definer/AssignedIdentityHelp";

   @RBEntry("WFAactAbout")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_7 = "Help/Definer/AdhocIdentityHelp";

   @RBEntry("WFTabAactPtcpnt")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_8 = "Help/Definer/AssignedParticipantsHelp";

   @RBEntry("WFTabAactActivity")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_9 = "Help/Definer/AssignedTaskHelp";

   @RBEntry("WFTabAactTran")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_10 = "Help/Definer/AssignedTransitionHelp";

   @RBEntry("WFTabAactVar")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_11 = "Help/Definer/AssignedVariablesHelp";

   @RBEntry("WFBlockActivity")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_12 = "Help/Definer/BlockExpressionHelp";

   @RBEntry("WFBlockActivity")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_13 = "Help/Definer/BlockIdentityHelp";

   @RBEntry("wt/clients/workflow/definer/help_it/CheckoutHelp.html")
   public static final String PRIVATE_CONSTANT_14 = "Help/Definer/CheckoutHelp";

   @RBEntry("WFRouterConditional")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_15 = "Help/Definer/ConditionalHelp";

   @RBEntry("WFRouting")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_16 = "Help/Definer/ConnectorEventsHelp";

   @RBEntry("WFRobotExecExpr")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_17 = "Help/Definer/ExpressionExpressionHelp";

   @RBEntry("WFRobotExecExpr")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_18 = "Help/Definer/ExpressionIdentityHelp";

   @RBEntry("wt/clients/workflow/definer/help_it/InternalMethodHelp.html#IMethodEventsHelp")
   public static final String PRIVATE_CONSTANT_19 = "Help/Definer/IMethodEventsHelp";

   @RBEntry("wt/clients/workflow/definer/help_it/InternalMethodHelp.html#IMethodIdentityHelp")
   public static final String PRIVATE_CONSTANT_20 = "Help/Definer/IMethodIdentityHelp";

   @RBEntry("wt/clients/workflow/definer/help_it/InternalMethodHelp.html#IMethodSpecHelp")
   public static final String PRIVATE_CONSTANT_21 = "Help/Definer/IMethodSpecHelp";

   @RBEntry("wt/clients/workflow/definer/help_it/InternalMethodHelp.html#IMethodTransitionHelp")
   public static final String PRIVATE_CONSTANT_22 = "Help/Definer/IMethodTransitionHelp";

   @RBEntry("wt/clients/workflow/definer/help_it/InternalMethodHelp.html#IMethodVariablesHelp")
   public static final String PRIVATE_CONSTANT_23 = "Help/Definer/IMethodVariablesHelp";

   @RBEntry("WFRobotMethod")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_24 = "Help/Definer/InternalMethodHelp";

   @RBEntry("WFLinkProcess")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_25 = "Help/Definer/LinkHelp";

   @RBEntry("WFMenusProcessEditorAbout")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_26 = "Help/Definer/MainHelp";

   @RBEntry("WFRobotNotif")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_27 = "Help/Definer/NotificationAttachmentsHelp";

   @RBEntry("WFRobotNotif")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_28 = "Help/Definer/NotificationGeneralHelp";

   @RBEntry("WFRobotNotif")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_29 = "Help/Definer/NotificationMessageHelp";

   @RBEntry("WFRobotNotif")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_30 = "Help/Definer/NotificationRecipientsHelp";

   @RBEntry("WFConnectorOr")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_31 = "Help/Definer/OrHelp";

   @RBEntry("WFTabProcessPropDeadline")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_32 = "Help/Definer/ProcessDeadlineHelp";

   @RBEntry("WFTabProcessPropProperties")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_33 = "Help/Definer/ProcessErrorsHelp";

   @RBEntry("WFTabProcessPropRoute")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_34 = "Help/Definer/ProcessEventsHelp";

   @RBEntry("WFTabProcessPropGen")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_35 = "Help/Definer/ProcessIdentityHelp";

   @RBEntry("WFDefProcessInstr")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_36 = "Help/Definer/ProcessInstructionsHelp";

   @RBEntry("WFTabProcessPropOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_37 = "Help/Definer/ProcessLPViewerHelp";

   @RBEntry("WFTabProcessPropTran")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_38 = "Help/Definer/ProcessTransitionHelp";

   @RBEntry("WFTabProcessPropVar")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_39 = "Help/Definer/ProcessVariablesHelp";

   @RBEntry("WFProcessProxy")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_40 = "Help/Definer/ProxyDeadlineHelp";

   @RBEntry("WFProcessProxy")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_41 = "Help/Definer/ProxyIdentityHelp";

   @RBEntry("WFProcessProxy")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_42 = "Help/Definer/ProxyVariablesHelp";

   @RBEntry("WFConnectorStart")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_43 = "Help/Definer/StartHelp";

   @RBEntry("WFTabSyncRobotSync")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_44 = "Help/Definer/SynchronizationExpressionHelp";

   @RBEntry("WFTabSyncRobotGen")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_45 = "Help/Definer/SynchronizationIdentityHelp";

   @RBEntry("WFConnectorThresh")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_46 = "Help/Definer/ThresholdHelp";

   @RBEntry("WFRobotTimer")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_47 = "Help/Definer/TimerHelp";

   @RBEntry("WFTabProcessPropVar")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_48 = "Help/Definer/VariableHelp";

   @RBEntry("WFAdminAbout")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_49 = "Help/Definer/WfAdminHelp";

   @RBEntry("WFRobotURL")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_50 = "Help/Definer/URLHelp";

   @RBEntry("WFTabProcessPropVar")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_51 = "Help/Definer/VarDisplayNameHelp";

   @RBEntry("WFTabAactRoleSetup")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_52 = "Help/Definer/RoleSetupHelp";

   @RBEntry("WFTabAactResourcePool")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_53 = "Help/Definer/ResourcePoolHelp";

   @RBEntry("IMethodEventsHelp")
   @RBPseudo(false)
   public static final String IMETHOD_EVENTS = "IMethodEventsHelp";

   @RBEntry("IMethodIdentityHelp")
   @RBPseudo(false)
   public static final String IMETHOD_IDENTITY = "IMethodIdentityHelp";

   @RBEntry("IMethodSpecHelp")
   @RBPseudo(false)
   public static final String IMETHOD_SPEC = "IMethodSpecHelp";

   @RBEntry("IMethodTransitionHelp")
   @RBPseudo(false)
   public static final String IMETHOD_TRANSITION = "IMethodTransitionHelp";

   @RBEntry("IMethodVariablesHelp")
   @RBPseudo(false)
   public static final String IMETHOD_VARIABLES = "IMethodVariablesHelp";

   @RBEntry("InternalMethodHelp")
   @RBPseudo(false)
   public static final String INTERNAL_METHOD_ROBOT = "InternalMethodHelp";

   @RBEntry("LinkHelp")
   @RBPseudo(false)
   public static final String LINK = "LinkHelp";

   @RBEntry("MainHelp")
   @RBPseudo(false)
   public static final String MAIN_HELP = "MainHelp";

   @RBEntry("NotificationMessageHelp")
   @RBPseudo(false)
   public static final String NOTIFICATION_MESSAGE = "NotificationMessageHelp";

   @RBEntry("NotificationRecipientsHelp")
   @RBPseudo(false)
   public static final String NOTIFICATION_RECIPIENTS = "NotificationRecipientsHelp";

   @RBEntry("NotificationGeneralHelp")
   @RBPseudo(false)
   public static final String NOTIFICATION_GENERAL = "NotificationGeneralHelp";

   @RBEntry("NotificationAttachmentsHelp")
   @RBPseudo(false)
   public static final String NOTIFICATION_ATTACHEMENTS = "NotificationAttachmentsHelp";

   @RBEntry("OrHelp")
   @RBPseudo(false)
   public static final String OR = "OrHelp";

   @RBEntry("ProcessDeadlineHelp")
   @RBPseudo(false)
   public static final String PROCESS_DEADLINE = "ProcessDeadlineHelp";

   @RBEntry("ProcessErrorsHelp")
   @RBPseudo(false)
   public static final String PROCESS_ERRORS = "ProcessErrorsHelp";

   @RBEntry("ProcessEventsHelp")
   @RBPseudo(false)
   public static final String PROCESS_EVENTS = "ProcessEventsHelp";

   @RBEntry("ProcessIdentityHelp")
   @RBPseudo(false)
   public static final String PROCESS_IDENTITY = "ProcessIdentityHelp";

   @RBEntry("ProcessInstructionsHelp")
   @RBPseudo(false)
   public static final String PROCESS_INSTRUCTIONS = "ProcessInstructionsHelp";

   @RBEntry("ProcessTransitionHelp")
   @RBPseudo(false)
   public static final String PROCESS_TRANSITION = "ProcessTransitionHelp";

   @RBEntry("ProcessVariablesHelp")
   @RBPseudo(false)
   public static final String PROCESS_VARIABLES = "ProcessVariablesHelp";

   @RBEntry("ProcessLPViewerHelp")
   @RBPseudo(false)
   public static final String PROCESS_LPVIEWER = "ProcessLPViewerHelp";

   @RBEntry("ProxyDeadlineHelp")
   @RBPseudo(false)
   public static final String PROXY_DEADLINE = "ProxyDeadlineHelp";

   @RBEntry("ProxyIdentityHelp")
   @RBPseudo(false)
   public static final String PROXY_IDENTITY = "ProxyIdentityHelp";

   @RBEntry("ProxyVariablesHelp")
   @RBPseudo(false)
   public static final String PROXY_VARIABLES = "ProxyVariablesHelp";

   @RBEntry("StartHelp")
   @RBPseudo(false)
   public static final String START = "StartHelp";

   @RBEntry("SynchronizationExpressionHelp")
   @RBPseudo(false)
   public static final String SYNCHRONIZATION_EXPRESSION = "SynchronizationExpressionHelp";

   @RBEntry("SynchronizationIdentityHelp")
   @RBPseudo(false)
   public static final String SYNCHRONIZATION_IDENTITY = "SynchronizationIdentityHelp";

   @RBEntry("ThresholdHelp")
   @RBPseudo(false)
   public static final String THRESHOLD = "ThresholdHelp";

   @RBEntry("TimerHelp")
   @RBPseudo(false)
   public static final String TIMER = "TimerHelp";

   @RBEntry("VariableHelp")
   @RBPseudo(false)
   public static final String VARIABLE = "VariableHelp";

   @RBEntry("WfAdminHelp")
   @RBPseudo(false)
   public static final String WFADMIN_HELP = "WfAdminHelp";

   @RBEntry("URLHelp")
   @RBPseudo(false)
   public static final String URL = "URLHelp";

   @RBEntry("WFDefProcessVarDisplayName")
   @RBPseudo(false)
   public static final String VARDISPLAYNAME_HELP = "VarDisplayNameHelp";

   @RBEntry("WFTemplateExtExpressionHelp")
   @RBPseudo(false)
   public static final String WFEXTER_HELP = "WFTemplateExtExpressionHelp";

   @RBEntry("WFRobotCheckpointHelp")
   @RBPseudo(false)
   public static final String INTERNAL_CHECK_POINT = "WFRobotCheckpointHelp";

   @RBEntry("WFRobotCheckpoint")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_54 = "Help/Definer/WFRobotCheckpointHelp";

   @RBEntry("WFTemplateExtExpression")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_55 = "Help/Definer/WFTemplateExtExpressionHelp";
}
