/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.workflow.tasks;

import wt.util.resource.*;

@RBUUID("wt.clients.workflow.tasks.TasksRB")
public final class TasksRB_it extends WTListResourceBundle {
   /**
    * -------------------------------------------------
    * Test string
    **/
   @RBEntry("..Localizzato..")
   public static final String TEST_STRING = "0";

   /**
    * -------------------------------------------------
    * Buttons
    **/
   @RBEntry("Salva")
   public static final String SAVE_BUTTON = "4";

   @RBEntry("Reimposta")
   public static final String RESET_BUTTON = "5";

   @RBEntry("OK")
   public static final String OK_BUTTON = "11";

   @RBEntry("Annulla")
   public static final String CANCEL_BUTTON = "12";

   @RBEntry("Guida")
   public static final String HELP_BUTTON = "6";

   /**
    * -------------------------------------------------
    * Labels
    **/
   @RBEntry("Ruolo")
   public static final String ROLE = "7";

   @RBEntry(":")
   public static final String COLON = "8";

   @RBEntry("Aggiorna variabili progetto")
   public static final String UPD_PROJECT_VARIABLES_LABEL = "13";

   /**
    * -------------------------------------------------
    * Titles
    **/
   @RBEntry("Workflow")
   public static final String WORKFLOW = "10";

   /**
    * -------------------------------------------------
    * Messages
    **/
   @RBEntry("Salvataggio in corso...")
   public static final String SAVING = "14";

   @RBEntry("Variabili progetto aggiornate")
   public static final String PROJECT_VARIABLES_UPDATED = "15";

   @RBEntry("Variabili progetto reimpostate")
   public static final String PROJECT_VALUES_RESET = "16";

   /**
    * -------------------------------------------------
    * Exceptions
    **/
   @RBEntry("Si è verificata un'eccezione durante l'inizializzazione dei dati: ")
   public static final String INITIALIZATION_FAILED = "1";

   @RBEntry("Impossibile inizializzare la Guida in linea: ")
   public static final String HELP_INITIALIZATION_FAILED = "2";

   @RBEntry("Si è verificata un'eccezione durante la localizzazione: ")
   public static final String LOCALIZING_FAILED = "3";

   /**
    * -------------------------------------------------
    * Tasks
    **/
   @RBEntry("Task utente 1")
   public static final String USERTASK1 = "UserTask1";

   @RBEntry("Task utente 2")
   public static final String USERTASK2 = "UserTask2";

   @RBEntry("Task utente 3")
   public static final String USERTASK3 = "UserTask3";

   @RBEntry("Task utente 4")
   public static final String USERTASK4 = "UserTask4";

   @RBEntry("Task utente 5")
   public static final String USERTASK5 = "UserTask5";

   @RBEntry("Task utente 6")
   public static final String USERTASK6 = "UserTask6";

   @RBEntry("Task utente 7")
   public static final String USERTASK7 = "UserTask7";

   @RBEntry("Ad hoc")
   public static final String WFADHOC = "WfAdHoc";

   @RBEntry("Imposta partecipanti")
   public static final String WFAUGMENT = "WfAugment";

   @RBEntry("Definisci progetti")
   public static final String WFDEFINEPROJECTS = "WfDefineProjects";

   @RBEntry("Default")
   public static final String WFTASK = "WfTask";

   @RBEntry("Aggiorna contenuto")
   public static final String WFUPDATECONTENT = "WfUpdateContent";

   @RBEntry("Osserva")
   public static final String OBSERVE = "observe";

   @RBEntry("Promuovi")
   public static final String PROMOTE = "promote";

   @RBEntry("Riesame")
   public static final String REVIEW = "review";

   @RBEntry("Invia")
   public static final String SUBMIT = "submit";

   @RBEntry("Aggiorna variabili team")
   public static final String UPD_TEAM_VARIABLES_LABEL = "20";

   @RBEntry("Variabili team aggiornate")
   public static final String TEAM_VARIABLES_UPDATED = "21";

   @RBEntry("Variabili team reimpostate")
   public static final String TEAM_VALUES_RESET = "22";

   @RBEntry("Aggiorna variabili modello team")
   public static final String UPD_TEAM_TEMPLATE_VARIABLES_LABEL = "23";

   @RBEntry("Le variabili del modello di team sono state aggiornate.")
   public static final String TEAM_TEMPLATE_VARIABLES_UPDATED = "24";

   @RBEntry("I valori del modello di team sono stati reimpostati.                                               ")
   public static final String TEAM_TEMPLATE_VALUES_RESET = "25";

   @RBEntry("Definisci team")
   public static final String WFDEFINETEAMS = "WfDefineTeams";

   @RBEntry("Gestione modifiche")
   public static final String WFCHGMGMT = "WfChgMgmt";

   @RBEntry("Task richiesta di promozione")
   public static final String WFPROMOTIONTASK = "WfPromotionRequestTask";
}
