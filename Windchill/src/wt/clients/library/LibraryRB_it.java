package wt.clients.library;

import wt.util.resource.*;

public final class LibraryRB_it extends WTListResourceBundle {
   /**
    * 
    * Headers
    * 
    **/
   @RBEntry("Libreria")
   public static final String LIBRARY = "LIBRARY";

   @RBEntry("Documentazione della release Windchill")
   public static final String WC_RELEASE_DOC = "WC_RELEASE_DOC";

   @RBEntry("Sourcing Factor!")
   public static final String WC_SF_DOC = "WC_SF_DOC";

   @RBEntry("Utilizzo di Windchill")
   public static final String WC_USER_DOC = "WC_USER_DOC";

   @RBEntry("Amministrazione Windchill")
   public static final String WC_ADMIN_DOC = "WC_ADMIN_DOC";

   @RBEntry("Personalizzazione Windchill")
   public static final String WC_CUSTOMIZATION_DOC = "WC_CUSTOMIZATION_DOC";

   @RBEntry("Production Factor!")
   public static final String WC_PF_DOC = "WC_PF_DOC";

   @RBEntry("Utilizzo Engineering Factor!")
   public static final String EF_USE_DOC = "EF_USE_DOC";

   @RBEntry("Amministrazione Engineering Factor!")
   public static final String EF_ADMIN_DOC = "EF_ADMIN_DOC";

   @RBEntry("Documentazione Engineering Factor! Windchill")
   public static final String WC_EF_DOC = "WC_EF_DOC";

   /**
    * 
    * Messages
    * 
    **/
   @RBEntry("La documentazione non è installata.")
   public static final String DOC_NOT_INSTALLED = "DOC_NOT_INSTALLED";

   /**
    * 
    * Manual Titles
    * 
    * There may be one or two title lines per manual.  The first line is generally the English
    * title if the manual is available only in English or the localized title if the manual
    * has been localized.  The second line is generally present only if the manual is only
    * available in English and contains the translated title (in parantheses).  For example:
    * {LibraryRB.INSTALL_GUIDE,                 "Windchill Installation Guide"},
    * {LibraryRB.INSTALL_GUIDE_T,               "(Guide d'installation Windchill)"},
    * 
    * This scheme varies by locale, however.
    * 
    * Administration Documentation
    **/
   @RBEntry("Windchill Installation and Configuration Guide")
   public static final String INSTALL_CONFIG_GUIDE = "INSTALL_CONFIG_GUIDE";

   @RBEntry("(Guida all'installazione e configurazione di Windchill)")
   public static final String INSTALL_CONFIG_GUIDE_T = "INSTALL_CONFIG_GUIDE_T";

   @RBEntry("Info*Engine Installation and Configuration Guide ")
   public static final String IE_INSTALL_CONFIG_GUIDE = "IE_INSTALL_CONFIG_GUIDE";

   @RBEntry("(Guida all'installazione e configurazione di Info*Engine)")
   public static final String IE_INSTALL_CONFIG_GUIDE_T = "IE_INSTALL_CONFIG_GUIDE_T";

   @RBEntry("Guida all'aggiornamento di Windchill")
   public static final String UPGRADE_MIG_GUIDE = "UPGRADE_MIG_GUIDE";

   @RBEntry(" ")
   public static final String UPGRADE_MIG_GUIDE_T = "UPGRADE_MIG_GUIDE_T";

   @RBEntry("Windchill Schema Changes")
   public static final String SCHEMA_CHANGES = "SCHEMA_CHANGES";

   @RBEntry("(Modifiche di schema Windchill)")
   public static final String SCHEMA_CHANGES_T = "SCHEMA_CHANGES_T";

   @RBEntry("Windchill Supported API Changes")
   public static final String SUPPORTED_API_CHANGES = "SUPPORTED_API_CHANGES";

   @RBEntry("(Modifiche API supportate Windchill)")
   public static final String SUPPORTED_API_CHANGES_T = "SUPPORTED_API_CHANGES_T";

   @RBEntry("Guida dell'amministratore di sistema Windchill")
   public static final String SYS_ADMIN_GUIDE = "SYS_ADMIN_GUIDE";

   @RBEntry(" ")
   public static final String SYS_ADMIN_GUIDE_T = "SYS_ADMIN_GUIDE_T";

   @RBEntry("Guida dell'amministratore aziendale Windchill")
   public static final String BUS_ADMIN_GUIDE = "BUS_ADMIN_GUIDE";

   @RBEntry(" ")
   public static final String BUS_ADMIN_GUIDE_T = "BUS_ADMIN_GUIDE_T";

   @RBEntry("Windchill Performance Tuning Guide")
   public static final String PERF_TUNING_GUIDE = "PERF_TUNING_GUIDE";

   @RBEntry("(Guida per il perfezionamento delle prestazioni Windchill)")
   public static final String PERF_TUNING_GUIDE_T = "PERF_TUNING_GUIDE_T";

   @RBEntry(" ")
   public static final String DATA_LOADER_INSTALL_GUIDE = "DATA_LOADER_INSTALL_GUIDE";

   @RBEntry("(Guida per l'installazione del caricatore di dati Windchill)")
   public static final String DATA_LOADER_INSTALL_GUIDE_T = "DATA_LOADER_INSTALL_GUIDE_T";

   @RBEntry(" ")
   public static final String LOADERS = "LOADERS";

   @RBEntry("(Caricatori per Windchill Foundation e Windchill Sourcing Factor)")
   public static final String LOADERS_T = "LOADERS_T";

   /**
    * User Documentation
    **/
   @RBEntry("Novità di Windchill Release 6.2")
   public static final String WHATS_NEW = "WHATS_NEW";

   @RBEntry(" ")
   public static final String WHATS_NEW_T = "WHATS_NEW_T";

   @RBEntry("Guida per l'utente Windchill")
   public static final String USERS_GUIDE = "USERS_GUIDE";

   @RBEntry(" ")
   public static final String USERS_GUIDE_T = "USERS_GUIDE_T";

   @RBEntry("Windchill Workflow Tutorial")
   public static final String WF_TUTORIAL = "WF_TUTORIAL";

   @RBEntry("(Esercitazione workflow Windchill)")
   public static final String WF_TUTORIAL_T = "WF_TUTORIAL_T";

   @RBEntry("Windchill Glossary")
   public static final String GLOSSARY = "GLOSSARY";

   @RBEntry("(Glossario Windchill)")
   public static final String GLOSSARY_T = "GLOSSARY_T";

   /**
    * Customization
    **/
   @RBEntry("Windchill Customizer's Guide")
   public static final String CUSTOMIZERS_GUIDE = "CUSTOMIZERS_GUIDE";

   @RBEntry("(Guida di personalizzazione Windchill)")
   public static final String CUSTOMIZERS_GUIDE_T = "CUSTOMIZERS_GUIDE_T";

   @RBEntry("Windchill Application Developer's Guide")
   public static final String APP_DEV_GUIDE = "APP_DEV_GUIDE";

   @RBEntry("(Guida dello sviluppatore di applicazioni Windchill)")
   public static final String APP_DEV_GUIDE_T = "APP_DEV_GUIDE_T";

   @RBEntry("Windchill Adapter Guide")
   public static final String ADAPTER_GUIDE = "ADAPTER_GUIDE";

   @RBEntry("(Guida dell'adattatore Windchill)")
   public static final String ADAPTER_GUIDE_T = "ADAPTER_GUIDE_T";

   @RBEntry("Info*Engine User's Guide")
   public static final String IE_USERS_GUIDE = "IE_USERS_GUIDE";

   @RBEntry("(Guida dell'utente Info*Engine)")
   public static final String IE_USERS_GUIDE_T = "IE_USERS_GUIDE_T";

   @RBEntry("JNDI Adapter Guide")
   public static final String JNDI_ADAPTER_GUIDE = "JNDI_ADAPTER_GUIDE";

   @RBEntry("(Guida dell'adattatore JNDI)")
   public static final String JNDI_ADAPTER_GUIDE_T = "JNDI_ADAPTER_GUIDE_T";

   @RBEntry("Windchill Javadoc")
   public static final String WC_JAVADOC = "WC_JAVADOC";

   @RBEntry("(Javadoc Windchill)")
   public static final String WC_JAVADOC_T = "WC_JAVADOC_T";

   @RBEntry("Info*Engine Javadoc")
   public static final String IE_JAVADOC = "IE_JAVADOC";

   @RBEntry("(Javadoc Info*Engine)")
   public static final String IE_JAVADOC_T = "IE_JAVADOC_T";

   @RBEntry("Windchill Client Technology Guide")
   public static final String WC_CLIENT_TECHNOLOGY_GUIDE = "WC_CLIENT_TECHNOLOGY_GUIDE";

   @RBEntry("(Guida del Client Windchill)")
   public static final String WC_CLIENT_TECHNOLOGY_GUIDE_T = "WC_CLIENT_TECHNOLOGY_GUIDE_T";

   @RBEntry("Workgroup Manager Customization Guide")
   public static final String WGM_CUSTOMIZATION_GUIDE = "WGM_CUSTOMIZATION_GUIDE";

   @RBEntry("(Guida alla personalizzazione di Workgroup Manager)")
   public static final String WGM_CUSTOMIZATION_GUIDE_T = "WGM_CUSTOMIZATION_GUIDE_T";

   /**
    * Production Factor
    **/
   @RBEntry("Windchill Production FACTOR! Installation and Configuration Guide")
   public static final String PF_INSTALL_CONFIG_GUIDE = "PF_INSTALL_CONFIG_GUIDE";

   @RBEntry("(Guida di configurazione ed installazione di Windchill Production FACTOR!)")
   public static final String PF_INSTALL_CONFIG_GUIDE_T = "PF_INSTALL_CONFIG_GUIDE_T";

   /**
    * Sourcing Factor
    **/
   @RBEntry("Windchill Sourcing FACTOR! Installation and Administration Guide ")
   public static final String SF_INSTALL_ADMIN_GUIDE = "SF_INSTALL_ADMIN_GUIDE";

   @RBEntry("(Guida all'installazione ed amministrazione di Windchill Sourcing FACTOR!)")
   public static final String SF_INSTALL_ADMIN_GUIDE_T = "SF_INSTALL_ADMIN_GUIDE_T";

   /**
    * Engineering Factor Use
    **/
   @RBEntry("Guida dell'utente di Workgroup Manager per PRO/ENGINEER")
   public static final String WG_MGR_PRO_ENGINEER_UG = "WG_MGR_PRO_ENGINEER_UG";

   @RBEntry("Workgroup Manager for CATIA User's Guide")
   public static final String WG_MGR_CATIA_UG = "WG_MGR_CATIA_UG";

   @RBEntry("(Guida dell'utente di Workgroup Manager per CATIA)")
   public static final String WG_MGR_CATIA_UG_T = "WG_MGR_CATIA_UG_T";

   @RBEntry("Workgroup Manager for CATIA Version 5 User's Guide")
   public static final String WG_MGR_CATIA_5_UG = "WG_MGR_CATIA_5_UG";

   @RBEntry("(Guida dell'utente di Workgroup Manager per CATIA Version 5)")
   public static final String WG_MGR_CATIA_5_UG_T = "WG_MGR_CATIA_5_UG_T";

   @RBEntry("Workgroup Manager for CADDS User's Guide")
   public static final String WG_MGR_CADDS_UG = "WG_MGR_CADDS_UG";

   @RBEntry("(Guida dell'utente di Workgroup Manager per CADDS)")
   public static final String WG_MGR_CADDS_UG_T = "WG_MGR_CADDS_UG_T";

   @RBEntry("Workgroup Manager for Helix User's Guide")
   public static final String WG_MGR_HELIX_UG = "WG_MGR_HELIX_UG";

   @RBEntry("(Guida dell'utente di Workgroup Manager per Helix)")
   public static final String WG_MGR_HELIX_UG_T = "WG_MGR_HELIX_UG_T";

   @RBEntry("Workgroup Manager for AutoCAD User's Guide")
   public static final String WG_MGR_AUTOCAD_UG = "WG_MGR_AUTOCAD_UG";

   @RBEntry("(Guida dell'utente di Workgroup Manager per AutoCAD)")
   public static final String WG_MGR_AUTOCAD_UG_T = "WG_MGR_AUTOCAD_UG_T";

   @RBEntry("Workgroup Manager for CADAM User's Guide")
   public static final String WG_MGR_CADAM_UG = "WG_MGR_CADAM_UG";

   @RBEntry("(Guida dell'utente di Workgroup Manager per CADAM)")
   public static final String WG_MGR_CADAM_UG_T = "WG_MGR_CADAM_UG_T";

   @RBEntry("Workgroup Manager for Unigraphics User's Guide ")
   public static final String WG_MGR_UNIGRAPHICS_UG = "WG_MGR_UNIGRAPHICS_UG";

   @RBEntry("(Guida dell''utente di Workgroup Manager per Unigraphics)")
   public static final String WG_MGR_UNIGRAPHICS_UG_T = "WG_MGR_UNIGRAPHICS_UG_T";

   @RBEntry("Workgroup Manager for I-DEAS User's Guide")
   public static final String WG_MGR_IDEAS_UG = "WG_MGR_IDEAS_UG";

   @RBEntry("(Guida dell'utente di Workgroup Manager per I-DEAS)")
   public static final String WG_MGR_IDEAS_UG_T = "WG_MGR_IDEAS_UG_T";

   @RBEntry("Workgroup Manager for SolidWorks User's Guide")
   public static final String WG_MGR_SOLIDWORKS_UG = "WG_MGR_SOLIDWORKS_UG";

   @RBEntry("(Guida dell'utente di Workgroup Manager per SolidWorks)")
   public static final String WG_MGR_SOLIDWORKS_UG_T = "WG_MGR_SOLIDWORKS_UG_T";

   @RBEntry("Workgroup Manager for Mentor Graphics Board Station User's Guide")
   public static final String WG_MGR_MENTOR_UG = "WG_MGR_MENTOR_UG";

   @RBEntry("(Guida dell'utente Workgroup Manager per Mentor Graphics Board Station )")
   public static final String WG_MGR_MENTOR_UG_T = "WG_MGR_MENTOR_UG_T";

   @RBEntry("Workgroup Manager for Cadence User's Guide")
   public static final String WG_MGR_CADENCE_UG = "WG_MGR_CADENCE_UG";

   @RBEntry("(Guida dell'utente di Workgroup Manager per Cadence)")
   public static final String WG_MGR_CADENCE_UG_T = "WG_MGR_CADENCE_UG_T";

   @RBEntry("Workgroup Manager for Autodesk Inventor User's Guide")
   public static final String WG_MGR_INVENTOR_UG = "WG_MGR_INVENTOR_UG";

   @RBEntry(" ")
   public static final String WG_MGR_INVENTOR_UG_T = "WG_MGR_INVENTOR_UG_T";

   /**
    * Engineering Factor Administration
    **/
   @RBEntry("Workgroup Manager Installation and Configuration Guide")
   public static final String WG_MGR_ICG = "WG_MGR_ICG";

   @RBEntry("(Guida per l'installazione e la configurazione dei Workgroup Manager)")
   public static final String WG_MGR_ICG_T = "WG_MGR_ICG_T";

   @RBEntry("Workgroup Manager for Pro/ENGINEER Installation and Configuration Guide")
   public static final String WG_MGR_PRO_ENGINEER_ICG = "WG_MGR_PRO_ENGINEER_ICG";

   @RBEntry("(Guida per l'installazione e la configurazione di Workgroup Manager per Pro/ENGINEER)")
   public static final String WG_MGR_PRO_ENGINEER_ICG_T = "WG_MGR_PRO_ENGINEER_ICG_T";

   @RBEntry("Workgroup Manager for CATIA Installation and Configuration Guide")
   public static final String WG_MGR_CATIA_ICG = "WG_MGR_CATIA_ICG";

   @RBEntry("(Guida per l'installazione e la configurazione di Workgroup Manager per CATIA)")
   public static final String WG_MGR_CATIA_ICG_T = "WG_MGR_CATIA_ICG_T";

   @RBEntry("Workgroup Manager for CATIA Version 5 Installation and Configuration Guide")
   public static final String WG_MGR_CATIA_5_ICG = "WG_MGR_CATIA_5_ICG";

   @RBEntry("(Guida per l'installazione e la configurazione di Workgroup Manager per CATIA Version 5)")
   public static final String WG_MGR_CATIA_5_ICG_T = "WG_MGR_CATIA_5_ICG_T";

   @RBEntry("Workgroup Manager for CADDS Installation and Configuration Guide")
   public static final String WG_MGR_CADDS_ICG = "WG_MGR_CADDS_ICG";

   @RBEntry("(Guida per l'installazione e la configurazione di Workgroup Manager per CADDS)")
   public static final String WG_MGR_CADDS_ICG_T = "WG_MGR_CADDS_ICG_T";

   @RBEntry("Workgroup Manager for Cadence Installation and Configuration Guide")
   public static final String WG_MGR_CADENCE_ICG = "WG_MGR_CADENCE_ICG";

   @RBEntry("(Guida per l'installazione e la configurazione di Workgroup Manager per Cadence)")
   public static final String WG_MGR_CADENCE_ICG_T = "WG_MGR_CADENCE_ICG_T";

   @RBEntry("Windchill Gateway for OneSpace Manager Installation and Configuration Guide")
   public static final String COCREATE_WORK_MGR_ICG = "COCREATE_WORK_MGR_ICG";

   @RBEntry("(Guida per l'installazione e la configurazione di Windchill Gateway per CoCreate WorkManager)")
   public static final String COCREATE_WORK_MGR_ICG_T = "COCREATE_WORK_MGR_ICG_T";

   @RBEntry("Optegra Gateway Installation and Configuration Guide")
   public static final String OPTEGRA_GW_ICG = "OPTEGRA_GW_ICG";

   @RBEntry("(Guida per l'installazione e la configurazione di Optegra Gateway)")
   public static final String OPTEGRA_GW_ICG_T = "OPTEGRA_GW_ICG_T";

   @RBEntry("Optegra Gateway Administration Guide")
   public static final String OPTEGRA_GW_AG = "OPTEGRA_GW_AG";

   @RBEntry("(Guida d'amministrazione di Optegra Gateway)")
   public static final String OPTEGRA_GW_AG_T = "OPTEGRA_GW_AG_T";

   @RBEntry("Pro/INTRALINK Gateway Installation and Configuration Guide")
   public static final String PRO_INTRALINK_GW_ICG = "PRO_INTRALINK_GW_ICG";

   @RBEntry("(Guida per l'installazione e la configurazione di Pro/INTRALINK Gateway)")
   public static final String PRO_INTRALINK_GW_ICG_T = "PRO_INTRALINK_GW_ICG_T";

   @RBEntry("Pro/INTRALINK Gateway Administration Guide")
   public static final String PRO_INTRALINK_GW_AG = "PRO_INTRALINK_GW_AG";

   @RBEntry("(Guida d'amministrazione Pro/INTRALINK Gateway)")
   public static final String PRO_INTRALINK_GW_AG_T = "PRO_INTRALINK_GW_AG_T";

   @RBEntry("Workgroup Manager for Mentor Graphics Board Station Installation and Configuration Guide")
   public static final String WG_MGR_MENTOR_ICG = "WG_MGR_MENTOR_ICG";

   @RBEntry("(Guida per l'installazione e la configurazione di Workgroup Manager per Mentor Graphics Board Station )")
   public static final String WG_MGR_MENTOR_ICG_T = "WG_MGR_MENTOR_ICG_T";

   @RBEntry("Workgroup Manager for Autodesk Inventor Installation and Configuration Guide")
   public static final String WG_MGR_INVENTOR_ICG = "WG_MGR_INVENTOR_ICG";

   @RBEntry(" ")
   public static final String WG_MGR_INVENTOR_ICG_T = "WG_MGR_INVENTOR_ICG_T";

   /**
    * 
    * Manual Paths (relative to \windchill\codebase)
    * 
    * These should not be translated.
    * 
    * User Documentation
    **/
   @RBEntry(" ")
   public static final String WHATS_NEW_HTML = "WHATS_NEW_HTML";

   @RBEntry("wt/clients/library/WCWhatsNew_it.pdf")
   public static final String WHATS_NEW_PDF = "WHATS_NEW_PDF";

   @RBEntry("wt/helpfiles/help_it/manuals/wcusersguide/WCUsersGuide.html")
   public static final String USERS_GUIDE_HTML = "USERS_GUIDE_HTML";

   @RBEntry("wt/clients/library/WCUsersGuide_it.pdf")
   public static final String USERS_GUIDE_PDF = "USERS_GUIDE_PDF";

   @RBEntry(" ")
   public static final String WF_TUTORIAL_HTML = "WF_TUTORIAL_HTML";

   @RBEntry("wt/helpfiles/help_en/online/workflowtutorial/WorkflowTutorial.pdf")
   public static final String WF_TUTORIAL_PDF = "WF_TUTORIAL_PDF";

   @RBEntry("wt/helpfiles/help_en/manuals/wcglossary/WCGlossary.html")
   public static final String GLOSSARY_HTML = "GLOSSARY_HTML";

   @RBEntry("wt/clients/library/WCGlossary.pdf")
   public static final String GLOSSARY_PDF = "GLOSSARY_PDF";

   /**
    * Windchill Administration
    **/
   @RBEntry("wt/clients/library/WCInstallConfigGuide.pdf")
   public static final String INSTALL_CONFIG_GUIDE_PDF = "INSTALL_CONFIG_GUIDE_PDF";

   @RBEntry(" ")
   public static final String INSTALL_CONFIG_GUIDE_HTML = "INSTALL_CONFIG_GUIDE_HTML";

   @RBEntry("infoengine/docs/IEInstallConfigGuide.pdf")
   public static final String IE_INSTALL_CONFIG_GUIDE_PDF = "IE_INSTALL_CONFIG_GUIDE_PDF";

   @RBEntry(" ")
   public static final String IE_INSTALL_CONFIG_GUIDE_HTML = "IE_INSTALL_CONFIG_GUIDE_HTML";

   @RBEntry("wt/clients/library/WCUpgradeMigrationGuide_it.pdf")
   public static final String UPGRADE_MIG_GUIDE_PDF = "UPGRADE_MIG_GUIDE_PDF";

   @RBEntry("wt/helpfiles/help_it/manuals/wcupgrademigrationguide/WCUpgradeMigrationGuide.html")
   public static final String UPGRADE_MIG_GUIDE_HTML = "UPGRADE_MIG_GUIDE_HTML";

   @RBEntry(" ")
   public static final String SCHEMA_CHANGES_PDF = "SCHEMA_CHANGES_PDF";

   @RBEntry("wt/clients/library/schema_changes.html")
   public static final String SCHEMA_CHANGES_HTML = "SCHEMA_CHANGES_HTML";

   @RBEntry(" ")
   public static final String SUPPORTED_API_CHANGES_PDF = "SUPPORTED_API_CHANGES_PDF";

   @RBEntry("wt/clients/library/sapi_changes.html")
   public static final String SUPPORTED_API_CHANGES_HTML = "SUPPORTED_API_CHANGES_HTML";

   @RBEntry("wt/clients/library/WCSysAdguide_it.pdf")
   public static final String SYS_ADMIN_GUIDE_PDF = "SYS_ADMIN_GUIDE_PDF";

   @RBEntry("wt/helpfiles/help_it/manuals/wcsysadguide/WCSysAdguide.html")
   public static final String SYS_ADMIN_GUIDE_HTML = "SYS_ADMIN_GUIDE_HTML";

   @RBEntry("wt/clients/library/WCBusAdguide_it.pdf ")
   public static final String BUS_ADMIN_GUIDE_PDF = "BUS_ADMIN_GUIDE_PDF";

   @RBEntry("wt/helpfiles/help_it/manuals/wcbusadguide/WCBusAdGuide.html")
   public static final String BUS_ADMIN_GUIDE_HTML = "BUS_ADMIN_GUIDE_HTML";

   @RBEntry("wt/clients/library/WCPerfTuningGuide.pdf")
   public static final String PERF_TUNING_GUIDE_PDF = "PERF_TUNING_GUIDE_PDF";

   @RBEntry("wt/helpfiles/help_en/manuals/wcperftuning/WCPerfTuningGuide.html")
   public static final String PERF_TUNING_GUIDE_HTML = "PERF_TUNING_GUIDE_HTML";

   @RBEntry(" ")
   public static final String DATA_LOADER_INSTALL_GUIDE_PDF = "DATA_LOADER_INSTALL_GUIDE_PDF";

   @RBEntry(" ")
   public static final String DATA_LOADER_INSTALL_GUIDE_HTML = "DATA_LOADER_INSTALL_GUIDE_HTML";

   @RBEntry(" ")
   public static final String LOADERS_PDF = "LOADERS_PDF";

   @RBEntry(" ")
   public static final String LOADERS_HTML = "LOADERS__HTML";

   /**
    * Customization
    **/
   @RBEntry("wt/helpfiles/help_en/manuals/wccustomizerguide/WCCustomizersGuide.html")
   public static final String CUSTOMIZERS_GUIDE_HTML = "CUSTOMIZERS_GUIDE_HTML";

   @RBEntry("wt/clients/library/WCCustomizersGuide.pdf")
   public static final String CUSTOMIZERS_GUIDE_PDF = "CUSTOMIZERS_GUIDE_PDF";

   @RBEntry("wt/helpfiles/help_en/manuals/wcappdevguide/WCAppDevGuide.html")
   public static final String APP_DEV_GUIDE_HTML = "APP_DEV_GUIDE_HTML";

   @RBEntry("wt/clients/library/WCAppDevGuide.pdf")
   public static final String APP_DEV_GUIDE_PDF = "APP_DEV_GUIDE_PDF";

   @RBEntry(" ")
   public static final String ADAPTER_GUIDE_HTML = "ADAPTER_REF_MANUAL_HTML";

   @RBEntry("wt/clients/library/WCAdapterGuide.pdf")
   public static final String ADAPTER_GUIDE_PDF = "ADAPTER_REF_MANUAL_PDF";

   @RBEntry("infoengine/docs/ieusersguide/ieusersguide.html")
   public static final String IE_USERS_GUIDE_HTML = "IE_USERS_GUIDE_HTML";

   @RBEntry("infoengine/docs/IEUsersGuide.pdf")
   public static final String IE_USERS_GUIDE_PDF = "IE_USERS_GUIDE_PDF";

   @RBEntry(" ")
   public static final String JNDI_ADAPTER_GUIDE_HTML = "JNDI_ADAPTER_GUIDE_HTML";

   @RBEntry("infoengine/docs/JNDIAdapterGuide.pdf")
   public static final String JNDI_ADAPTER_GUIDE_PDF = "JNDI_ADAPTER_GUIDE_PDF";

   @RBEntry("wt/clients/library/api/index.html")
   public static final String WC_JAVADOC_HTML = "WC_JAVADOC_HTML";

   @RBEntry(" ")
   public static final String WC_JAVADOC_PDF = "WC_JAVADOC_PDF";

   @RBEntry("infoengine/docs/apidocs/index.html")
   public static final String IE_JAVADOC_HTML = "IE_JAVADOC_HTML";

   @RBEntry(" ")
   public static final String IE_JAVADOC_PDF = "IE_JAVADOC_PDF";

   @RBEntry("wt/clients/library/WCClientTechGuide.pdf")
   public static final String WC_CLIENT_TECHNOLOGY_GUIDE_PDF = "WC_CLIENT_TECHNOLOGY_GUIDE_PDF";

   @RBEntry("wt/helpfiles/help_en/manuals/wcclienttechguide/WCClientTechGuide.html")
   public static final String WC_CLIENT_TECHNOLOGY_GUIDE_HTML = "WC_CLIENT_TECHNOLOGY_GUIDE_HTML";

   @RBEntry("wt/clients/library/WGM_CUSTOMIZATION_GUIDE.pdf")
   public static final String WGM_CUSTOMIZATION_GUIDE_PDF = "WGM_CUSTOMIZATION_GUIDE_PDF";

   @RBEntry("wt/clients/library/WGM_CUSTOMIZATION_GUIDE.html")
   public static final String WGM_CUSTOMIZATION_GUIDE_HTML = "WGM_CUSTOMIZATION_GUIDE_HTML";

   /**
    * Sourcing Factor
    **/
   @RBEntry("wt/clients/library/WSFInstallAdmin.pdf")
   public static final String SF_INSTALL_ADMIN_GUIDE_PDF = "SF_INSTALL_ADMIN_GUIDE_PDF";

   @RBEntry(" ")
   public static final String SF_INSTALL_ADMIN_GUIDE_HTML = "SF_INSTALL_ADMIN_GUIDE_HTML";

   /**
    * Production Factor
    **/
   @RBEntry("wt/clients/library/WCProdFactorInstallConfig.pdf")
   public static final String PF_INSTALL_CONFIG_GUIDE_PDF = "PF_INSTALL_CONFIG_GUIDE_PDF";

   @RBEntry(" ")
   public static final String PF_INSTALL_CONFIG_GUIDE_HTML = "PF_INSTALL_CONFIG_GUIDE_HTML";

   /**
    * Engineering Factor Use
    **/
   @RBEntry("wt/helpfiles/help_it/manuals/wgmproe/WGMProEUsersGuide.html")
   public static final String WG_MGR_PRO_ENGINEER_UG_HTML = "WG_MGR_PRO_ENGINEER_UG_HTML";

   @RBEntry("wt/clients/library/WGMProEUsersGuide_it.pdf")
   public static final String WG_MGR_PRO_ENGINEER_UG_PDF = "WG_MGR_PRO_ENGINEER_UG_PDF";

   @RBEntry("wt/helpfiles/help_en/manuals/wgmcatia/WGMCatiaUsersGuide.html")
   public static final String WG_MGR_CATIA_UG_HTML = "WG_MGR_CATIA_UG_HTML";

   @RBEntry("wt/clients/library/WGMCatiaUsersGuide.pdf")
   public static final String WG_MGR_CATIA_UG_PDF = "WG_MGR_CATIA_UG_PDF";

   @RBEntry("wt/helpfiles/help_en/manuals/wgmcatia5/WGMCatia5UsersGuide.html")
   public static final String WG_MGR_CATIA_5_UG_HTML = "WG_MGR_CATIA_5_UG_HTML";

   @RBEntry("wt/clients/library/WGMCatia5UsersGuide.pdf")
   public static final String WG_MGR_CATIA_5_UG_PDF = "WG_MGR_CATIA_5_UG_PDF";

   @RBEntry("wt/helpfiles/help_en/manuals/wgmcadds/WGMCaddsUsersGuide.html")
   public static final String WG_MGR_CADDS_UG_HTML = "WG_MGR_CADDS_UG_HTML";

   @RBEntry("wt/clients/library/WGMCaddsUsersGuide.pdf")
   public static final String WG_MGR_CADDS_UG_PDF = "WG_MGR_CADDS_UG_PDF";

   @RBEntry("wt/helpfiles/help_en/manuals/wgmhelix/WGMHelixUsersGuide.html")
   public static final String WG_MGR_HELIX_UG_HTML = "WG_MGR_HELIX_UG_HTML";

   @RBEntry("wt/clients/library/WGMHelixUsersGuide.pdf")
   public static final String WG_MGR_HELIX_UG_PDF = "WG_MGR_HELIX_UG_PDF";

   @RBEntry("wt/helpfiles/help_en/manuals/wgmautocad/WGMAutoCadUsersGuide.html")
   public static final String WG_MGR_AUTOCAD_UG_HTML = "WG_MGR_AUTOCAD_UG_HTML";

   @RBEntry("wt/clients/library/WGMAutoCadUsersGuide.pdf")
   public static final String WG_MGR_AUTOCAD_UG_PDF = "WG_MGR_AUTOCAD_UG_PDF";

   @RBEntry("wt/helpfiles/help_en/manuals/wgmcadam/WGMCadamUsersGuide.html")
   public static final String WG_MGR_CADAM_UG_HTML = "WG_MGR_CADAM_UG_HTML";

   @RBEntry("wt/clients/library/WGMCadamUsersGuide.pdf")
   public static final String WG_MGR_CADAM_UG_PDF = "WG_MGR_CADAM_UG_PDF";

   @RBEntry("wt/helpfiles/help_en/manuals/wgmunigraphics/WGMUnigraphicsUsersGuide.html")
   public static final String WG_MGR_UNIGRAPHICS_UG_HTML = "WG_MGR_UNIGRAPHICS_UG_HTML";

   @RBEntry("wt/clients/library/WGMUnigraphicsUsersGuide.pdf")
   public static final String WG_MGR_UNIGRAPHICS_UG_PDF = "WG_MGR_UNIGRAPHICS_UG_PDF";

   @RBEntry("wt/helpfiles/help_en/manuals/wgmideas/WGMIdeasUsersGuide.html")
   public static final String WG_MGR_IDEAS_UG_HTML = "WG_MGR_IDEAS_UG_HTML";

   @RBEntry("wt/clients/library/WGMIdeasUsersGuide.pdf")
   public static final String WG_MGR_IDEAS_UG_PDF = "WG_MGR_IDEAS_UG_PDF";

   @RBEntry("wt/helpfiles/help_en/manuals/wgmsolidworks/WGMSolidworksUsersGuide.html")
   public static final String WG_MGR_SOLIDWORKS_UG_HTML = "WG_MGR_SOLIDWORKS_UG_HTML";

   @RBEntry("wt/clients/library/WGMSolidWorksUsersGuide.pdf")
   public static final String WG_MGR_SOLIDWORKS_UG_PDF = "WG_MGR_SOLIDWORKS_UG_PDF";

   @RBEntry(" ")
   public static final String WG_MGR_MENTOR_UG_HTML = "WG_MGR_MENTOR_UG_HTML";

   @RBEntry("wt/clients/library/WGMMentorUsersGuide.pdf")
   public static final String WG_MGR_MENTOR_UG_PDF = "WG_MGR_MENTOR_UG_PDF";

   @RBEntry(" ")
   public static final String WG_MGR_CADENCE_UG_HTML = "WG_MGR_CADENCE_UG_HTML";

   @RBEntry("wt/clients/library/WGMCadenceUsersGuide.pdf")
   public static final String WG_MGR_CADENCE_UG_PDF = "WG_MGR_CADENCE_UG_PDF";

   @RBEntry("wt/helpfiles/help_en/manuals/wgmautodesk/WGMAutodeskUsersGuide.html")
   public static final String WG_MGR_INVENTOR_UG_HTML = "WG_MGR_INVENTOR_UG_HTML";

   @RBEntry("wt/clients/library/WGMAutodeskUsersGuide.pdf")
   public static final String WG_MGR_INVENTOR_UG_PDF = "WG_MGR_INVENTOR_UG_PDF";

   /**
    * Engineering Factor Administration
    **/
   @RBEntry(" ")
   public static final String WG_MGR_ICG_HTML = "WG_MGR_ICG_HTML";

   @RBEntry("wt/clients/library/WGMI3InstallConfigGuide.pdf")
   public static final String WG_MGR_ICG_PDF = "WG_MGR_ICG_PDF";

   @RBEntry(" ")
   public static final String WG_MGR_PRO_ENGINEER_ICG_HTML = "WG_MGR_PRO_ENGINEER_ICG_HTML";

   @RBEntry("wt/clients/library/WGMProEInstallConfig.pdf")
   public static final String WG_MGR_PRO_ENGINEER_ICG_PDF = "WG_MGR_PRO_ENGINEER_ICG_PDF";

   @RBEntry(" ")
   public static final String WG_MGR_CATIA_ICG_HTML = "WG_MGR_CATIA_ICG_HTML";

   @RBEntry("wt/clients/library/WGMCatiaInstallConfig.pdf")
   public static final String WG_MGR_CATIA_ICG_PDF = "WG_MGR_CATIA_ICG_PDF";

   @RBEntry(" ")
   public static final String WG_MGR_CATIA_5_ICG_HTML = "WG_MGR_CATIA_5_ICG_HTML";

   @RBEntry("wt/clients/library/WGMCatia5InstallConfig.pdf")
   public static final String WG_MGR_CATIA_5_ICG_PDF = "WG_MGR_CATIA_5_ICG_PDF";

   @RBEntry(" ")
   public static final String WG_MGR_CADDS_ICG_HTML = "WG_MGR_CADDS_ICG_HTML";

   @RBEntry("wt/clients/library/WGMCaddsInstallConfig.pdf")
   public static final String WG_MGR_CADDS_ICG_PDF = "WG_MGR_CADDS_ICG_PDF";

   @RBEntry(" ")
   public static final String WG_MGR_MENTOR_ICG_HTML = "WG_MGR_MENTOR_ICG_HTML";

   @RBEntry("wt/clients/library/WGMMentorInstallConfigGuide.pdf")
   public static final String WG_MGR_MENTOR_ICG_PDF = "WG_MGR_MENTOR_ICG_PDF";

   @RBEntry(" ")
   public static final String WG_MGR_CADENCE_ICG_HTML = "WG_MGR_CADENCE_ICG_HTML";

   @RBEntry("wt/clients/library/WGMCadenceInstallConfig.pdf")
   public static final String WG_MGR_CADENCE_ICG_PDF = "WG_MGR_CADENCE_ICG_PDF";

   @RBEntry(" ")
   public static final String COCREATE_WORK_MGR_ICG_HTML = "COCREATE_WORK_MGR_ICG_HTML";

   @RBEntry("wt/clients/library/OneSpcGWInstallConfigGuide.pdf")
   public static final String COCREATE_WORK_MGR_ICG_PDF = "COCREATE_WORK_MGR_ICG_PDF";

   @RBEntry(" ")
   public static final String OPTEGRA_GW_ICG_HTML = "OPTEGRA_GW_ICG_HTML";

   @RBEntry("wt/clients/library/OptGWInstallConfig.pdf")
   public static final String OPTEGRA_GW_ICG_PDF = "OPTEGRA_GW_ICG_PDF";

   @RBEntry("wt/helpfiles/help_en/manuals/optgateadminguide/OptGateAdminGuide.html")
   public static final String OPTEGRA_GW_AG_HTML = "OPTEGRA_GW_AG_HTML";

   @RBEntry("wt/clients/library/OptGateAdminGuide.pdf")
   public static final String OPTEGRA_GW_AG_PDF = "OPTEGRA_GW_AG_PDF";

   @RBEntry(" ")
   public static final String PRO_INTRALINK_GW_ICG_HTML = "PRO_INTRALINK_GW_ICG_HTML";

   @RBEntry("wt/clients/library/ProIGWInstallConfig.pdf ")
   public static final String PRO_INTRALINK_GW_ICG_PDF = "PRO_INTRALINK_GW_ICG_PDF";

   @RBEntry("wt/helpfiles/help_en/manuals/ilinkgateadminguide/ILinkGWAdminGuide.html")
   public static final String PRO_INTRALINK_GW_AG_HTML = "PRO_INTRALINK_GW_AG_HTML";

   @RBEntry("wt/clients/library/ILinkGWAdminGuide.pdf")
   public static final String PRO_INTRALINK_GW_AG_PDF = "PRO_INTRALINK_AG_GW_PDF";

   @RBEntry(" ")
   public static final String WG_MGR_INVENTOR_ICG_HTML = "WG_MGR_INVENTOR_ICG_HTML";

   @RBEntry("wt/clients/library/WGMAutodeskInstallGuide.pdf")
   public static final String WG_MGR_INVENTOR_ICG_PDF = "WG_MGR_INVENTOR_ICG_PDF";
}
