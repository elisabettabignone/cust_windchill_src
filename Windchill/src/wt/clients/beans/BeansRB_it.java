/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.beans;

import wt.util.resource.*;

@RBUUID("wt.clients.beans.BeansRB")
public final class BeansRB_it extends WTListResourceBundle {
   @RBEntry("{0}:")
   public static final String ATTRIBUTE_LABEL = "11";

   @RBEntry("*{0}:")
   public static final String REQUIRED_ATTRIBUTE_LABEL = "12";

   @RBEntry("*")
   public static final String REQUIRED_FLAG = "0";

   @RBEntry("...")
   public static final String ELLIPSES = "1";

   @RBEntry("Sfoglia")
   public static final String BROWSE_LABEL = "2";

   @RBEntry("{0}...")
   public static final String APPEND_ELLIPSES = "3";

   @RBEntry("*{0}")
   public static final String PREPEND_REQUIRED = "4";

   @RBEntry("Si è verificato un errore durante la localizzazione di {0}.  Il tentativo di  recuperare il valore della chiave {1} dal resource bundle {2} non è riuscito.  È possibile che la chiave {1} non sia stata trovata nel resource bundle {2}. Contattare un amministratore.")
   public static final String RESOURCE_BUNDLE_ERROR = "5";

   @RBEntry("Non si dispone dei permessi {0} per la cartella {1}. Selezionare una cartella di cui si dispone dei permessi {0}.")
   public static final String NO_FOLDER_ACCESS = "6";

   @RBEntry("Impossibile trovare lo schedario personale per {0}.")
   public static final String PERSONAL_CABINET_NOT_FOUND = "8";

   @RBEntry("Il seguente errore si è verificato durante il recupero dello schedario personale per {0}: {1}")
   public static final String ERROR_RETRIEVING_PERSONAL_CABINET = "9";

   @RBEntry("Il seguente errore si è verificato durante il recupero della cartella \"{0}\": {1}")
   public static final String ERROR_RETRIEVING_FOLDER = "10";

   @RBEntry("La cartella {0} non è inclusa nello schedario personale.  Selezionare una cartella inclusa nello schedario personale.")
   public static final String FOLDER_NOT_IN_PERSONAL_CABINET = "13";

   @RBEntry("{0}, ")
   public static final String COMMA_LIST = "14";

   @RBEntry("La cartella selezionata, {0}, non è inclusa in uno degli schedari obbligatori.  Selezionare una cartella inclusa in uno dei seguenti schedari: {1}.")
   public static final String NOT_IN_CABINETS = "15";

   @RBEntry("Anche se l'elemento {0} {1} può essere contenuto in una cartella, il metodo assignFolder non può essere utilizzato per assegnare la cartella dell'oggetto corrente. Potrebbe essere necessario richiamare direttamente il metodo setFolder sull'oggetto oppure accedere nella cartella padre al costruttore dell'oggetto.")
   public static final String OBJECT_NOT_FOLDERED = "16";

   @RBEntry("Aggiungi")
   public static final String ADD = "17";

   @RBEntry("Rimuovi")
   public static final String REMOVE = "18";

   @RBEntry("Visualizza")
   public static final String VIEW = "19";

   @RBEntry("Trova oggetto")
   public static final String FIND_OBJECT = "20";

   @RBEntry("Effettività {0}")
   public static final String TYPE_EFFECTIVITY = "21";

   @RBEntry("Configuration item:")
   public static final String CONFIGURATION_ITEM = "22";

   @RBEntry("Data")
   public static final String DATE = "23";

   @RBEntry("Inizio:")
   public static final String START = "24";

   @RBEntry("Fine:")
   public static final String END = "25";

   @RBEntry("Tipo di effettività:")
   public static final String EFFECTIVITY_TYPE = "26";

   @RBEntry("Trova configuration item")
   public static final String FIND_CONFIGURATION_ITEM = "27";

   @RBEntry("Recupera")
   public static final String RETRIEVE_BUTTON_LABEL = "28";

   @RBEntry("Arresta")
   public static final String STOP_BUTTON_LABEL = "33";

   @RBEntry("Chiave di ricerca:")
   public static final String CLASS_CHOICE_LABEL = "32";

   @RBEntry("Numero:")
   public static final String NUMBER_LABEL = "29";

   @RBEntry("Versione:")
   public static final String VERSION_LABEL = "30";

   @RBEntry("Ultima versione")
   public static final String GET_LATEST_VERSION_LABEL = "31";

   @RBEntry("Impossibile trovare un numero corrispondente a {0}: \"{1}\" e versione:  \"{2}\".  Immettere un altro numero e una versione diversa.")
   @RBArgComment0("a string representing the class of the object being retrieved.")
   @RBArgComment1("a string representing the number entered by the user.")
   @RBArgComment2("a string representing the version entered by the user.")
   public static final String OBJECT_NOT_FOUND = "34";

   @RBEntry("Gruppi")
   public static final String GROUP_TAB_LABEL = "35";

   @RBEntry("Utenti")
   public static final String USER_TAB_LABEL = "36";

   @RBEntry("Attori")
   public static final String ACTOR_TAB_LABEL = "37";

   @RBEntry("Ruoli")
   public static final String ROLE_TAB_LABEL = "38";

   @RBEntry("Utente:")
   public static final String USER_TEXT_FIELD_LABEL = "39";

   @RBEntry("Gruppo:")
   public static final String GROUP_CHOICE_LABEL = "40";

   @RBEntry("Trova")
   public static final String USER_FIND_BUTTON_LABEL = "41";

   @RBEntry("Aggiungi >>")
   public static final String ADD_BUTTON_LABEL = "42";

   @RBEntry("Aggiungi tutto >>")
   public static final String ADD_ALL_BUTTON_LABEL = "43";

   @RBEntry("<< Rimuovi")
   public static final String REMOVE_BUTTON_LABEL = "44";

   @RBEntry("<< Rimuovi tutto")
   public static final String REMOVE_ALL_BUTTON_LABEL = "45";

   @RBEntry("Partecipanti:")
   public static final String SELECTED_PARTICIPANTS_PANEL_LABEL = "46";

   @RBEntry("Impossibile trovare il partecipante, utente, gruppo, ruolo o organizzazione richiesto. Verificare di aver specificato il nome corretto.")
   public static final String VERIFY_PARTICIPANT_FAILED = "47";

   @RBEntry("Sfoglia...")
   public static final String BROWSE_BUTTON_LABEL = "48";

   @RBEntry("Gruppo:")
   public static final String GROUP_MODE_LABEL = "49";

   @RBEntry("Utente:")
   public static final String USER_MODE_LABEL = "50";

   @RBEntry("Utente/gruppo/ruolo:")
   public static final String PRINCIPAL_MODE_LABEL = "51";

   @RBEntry(" ")
   public static final String NO_PRINCIPAL_SELECTED_STRING = "52";

   @RBEntry("OK")
   public static final String PSB_OK_BUTTON_LABEL = "53";

   @RBEntry("Annulla")
   public static final String PSB_CANCEL_BUTTON_LABEL = "54";

   @RBEntry("Guida")
   public static final String PSB_HELP_BUTTON_LABEL = "55";

   @RBEntry("Sfoglia")
   public static final String BROWSE_DIALOG_TITLE = "56";

   @RBEntry("Criteri di ricerca")
   public static final String SEARCH_CRITERIA_LABEL = "57";

   @RBEntry("Nome utente")
   public static final String USER_NAME_LABEL = "58";

   @RBEntry("ID utente")
   public static final String USER_ID_LABEL = "59";

   @RBEntry("Origine")
   public static final String SOURCE_LABEL = "60";

   @RBEntry("Tutti")
   public static final String ALL_SERVICES = "61";

   @RBEntry("Gruppo")
   public static final String GROUP_LABEL = "62";

   @RBEntry("Locale")
   public static final String LOCAL_WINDCHILL_SERVICE = "63";

   @RBEntry("Partecipante")
   public static final String PARTICIPANT_LABEL = "64";

   @RBEntry("Nome distinto")
   public static final String DISTINGUISHEDNAME_LABEL = "65";

   @RBEntry("Utente/gruppo/ruolo")
   public static final String PRINCIPAL_LABEL = "66";

   @RBEntry("Posizione:")
   public static final String LOCATION_LABEL = "67";

   @RBEntry("Organizzazioni")
   public static final String ORG_TAB_LABEL = "68";

   @RBEntry("Organizzazione:")
   public static final String ORG_CHOICE_LABEL = "69";

   @RBEntry("Organizzazione:")
   public static final String ORG_MODE_LABEL = "70";

   @RBEntry("Organizzazione")
   public static final String ORG_LABEL = "71";

   @RBEntry("Cerca...")
   public static final String SEARCH_LABEL = "72";

   @RBEntry("Reimposta")
   public static final String CLEAR_LABEL = "74";

   @RBEntry("Nome")
   public static final String NAME_LABEL = "75";

   @RBEntry("ID")
   public static final String ID_LABEL = "76";

   @RBEntry("Risultati")
   public static final String RESULTS_LABEL = "77";

   @RBEntry("Trova organizzazione")
   public static final String FIND_ORGS_LABEL = "78";

   @RBEntry("Generato")
   @RBComment("Label to indicate that the value normally supplied to a disabled text component will be automatically generated by the system.  Used in 'Create' part/document/etc. applets to indicate that part/document/etc. number will be automatically generated.")
   public static final String GENERATED_LABEL = "79";

   @RBEntry("Il business object è stato automaticamente sottoposto a Check-Out per permettere all'utente di modificarne le relazioni. Impossibile effettuare il Check-In automatico del documento. Effettuare il Check-In manuale del documento.")
   @RBComment("Error message about not being to check in a document after checking it out to modify the doc to doc relationships.")
   public static final String CHECKIN_ASSOCIATION_ERROR = "80";

   @RBEntry("È stato effettuato un tentativo di Check-Out automatico del business object in modo da permettere la modifica delle relazioni. Il Check-Out non è riuscito. Effettuare il Check-Out e quindi aggiornare le relazioni.")
   @RBComment("Error message about not being to check out a document prior to modify the doc to doc relationships.")
   public static final String CHECKOUT_ASSOCIATION_ERROR = "81";

   @RBEntry("Servizio:")
   @RBComment("Label for principal repository service, replaces the SOURCE_LABEL resource to be consistant with the terminology used in the Principal Administrator")
   public static final String PRINCIPAL_SERVICE_LABEL = "82";

   @RBEntry("Nome completo")
   public static final String FULL_NAME_LABEL = "83";

   @RBEntry("Avviso")
   public static final String NOTICE = "84";

   @RBEntry("Il nome gruppo immesso non esiste. Se è selezionato il servizio 'Tutti', immettere il nome gruppo nel seguente formato: nome gruppo (nome servizio specifico).")
   public static final String INVALID_GROUP_NAME = "85";

   @RBEntry("Nome gruppo:")
   public static final String GROUP_NAME_LABEL = "86";

   @RBEntry("Trova")
   public static final String GROUP_FIND_BUTTON_LABEL = "87";

   @RBEntry("Nome organizzazione:")
   public static final String ORG_NAME_LABEL = "88";

   @RBEntry("Trova")
   public static final String ORG_FIND_BUTTON_LABEL = "89";

   @RBEntry("Servizio")
   public static final String PRINCIPAL_SERVICE_COLUMN_LABEL = "90";

   @RBEntry("Nome utente")
   public static final String USER_NAME_COLUMN_LABEL = "91";

   @RBEntry("Qualificatore")
   public static final String QUALIFIER_LABEL = "92";

   @RBEntry("Si applica a:")
   public static final String SELECTED_PRINCIPAL_TYPE_PANEL_LABEL = "93";

   @RBEntry("Utente/gruppo/ruolo selezionato")
   public static final String SELECT_PRINCIPAL = "94";

   @RBEntry("Tutti tranne utente/gruppo/ruolo selezionato")
   public static final String SELECT_ALL_EXCEPT_PRINCIPAL = "95";

   @RBEntry("Servizio di elenco")
   @RBComment("Label for principal repository service on workflow setup participants applet.")
   public static final String WF_PRINCIPAL_SERVICE_LABEL = "96";
}
