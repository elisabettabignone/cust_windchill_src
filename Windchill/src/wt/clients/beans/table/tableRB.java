/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.beans.table;

import wt.util.resource.*;

@RBUUID("wt.clients.beans.table.tableRB")
public final class tableRB extends WTListResourceBundle {
   /**
    * test string
    **/
   @RBEntry("..Localized..")
   public static final String TEST_STRING = "0";

   /**
    * Buttons
    * Labels
    **/
   @RBEntry("Root")
   public static final String ROOT_NODE_LABEL = "1";

   /**
    * Titles
    * Text
    **/
   @RBEntry("Column does not exist")
   public static final String COLUMN_NOT_EXIST = "2";

   /**
    * Tool Tips
    * Exceptions
    **/
   @RBEntry("Object does not contain %s attribute.")
   public static final String ITEM_ID_NOT_FOUND = "3";

   @RBEntry("An object with the same identifier already exist.")
   public static final String ITEM_ALREADY_EXIST = "4";
}
