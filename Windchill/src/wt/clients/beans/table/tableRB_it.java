/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.beans.table;

import wt.util.resource.*;

@RBUUID("wt.clients.beans.table.tableRB")
public final class tableRB_it extends WTListResourceBundle {
   /**
    * test string
    **/
   @RBEntry("..Localizzato..")
   public static final String TEST_STRING = "0";

   /**
    * Buttons
    * Labels
    **/
   @RBEntry("Radice")
   public static final String ROOT_NODE_LABEL = "1";

   /**
    * Titles
    * Text
    **/
   @RBEntry("Colonna inesistente")
   public static final String COLUMN_NOT_EXIST = "2";

   /**
    * Tool Tips
    * Exceptions
    **/
   @RBEntry("L'oggetto non contiene l'attributo %s.")
   public static final String ITEM_ID_NOT_FOUND = "3";

   @RBEntry("Esiste già un oggetto con lo stesso identificatore.")
   public static final String ITEM_ALREADY_EXIST = "4";
}
