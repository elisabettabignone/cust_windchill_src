/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.beans;

import wt.util.resource.*;

@RBUUID("wt.clients.beans.BeansRB")
public final class BeansRB extends WTListResourceBundle {
   @RBEntry("{0}:")
   public static final String ATTRIBUTE_LABEL = "11";

   @RBEntry("*{0}:")
   public static final String REQUIRED_ATTRIBUTE_LABEL = "12";

   @RBEntry("*")
   public static final String REQUIRED_FLAG = "0";

   @RBEntry("...")
   public static final String ELLIPSES = "1";

   @RBEntry("Browse")
   public static final String BROWSE_LABEL = "2";

   @RBEntry("{0}...")
   public static final String APPEND_ELLIPSES = "3";

   @RBEntry("*{0}")
   public static final String PREPEND_REQUIRED = "4";

   @RBEntry("An error occurred while trying to localize {0}.  The attempt to  retrieve the value for key {1} from resource bundle {2} was unsuccessful.  Key {1} may not have been found in resource bundle {2}. Please consult an administrator.")
   public static final String RESOURCE_BUNDLE_ERROR = "5";

   @RBEntry("You do not have {0} permission on folder {1}.  Please choose a folder that you have {0} permission on.")
   public static final String NO_FOLDER_ACCESS = "6";

   @RBEntry("The personal cabinet for {0} could not be found.")
   public static final String PERSONAL_CABINET_NOT_FOUND = "8";

   @RBEntry("The following error occurred while trying to retrieve the personal cabinet for {0}: {1}")
   public static final String ERROR_RETRIEVING_PERSONAL_CABINET = "9";

   @RBEntry("The following error occurred while trying to retrieve the folder \"{0}\": {1}")
   public static final String ERROR_RETRIEVING_FOLDER = "10";

   @RBEntry("Folder {0} is not in your personal cabinet.  Please choose a folder that is in your personal cabinet.")
   public static final String FOLDER_NOT_IN_PERSONAL_CABINET = "13";

   @RBEntry("{0}, ")
   public static final String COMMA_LIST = "14";

   @RBEntry("The chosen folder, {0}, is not in one of the required cabinets. Please choose a folder in one of the following cabinets: {1}.")
   public static final String NOT_IN_CABINETS = "15";

   @RBEntry("While the given {0} {1} can be an entry in a folder, the assignFolder method cannot be used to assign the folder of this object.  You may need to explicitly invoke setFolder on this object or may need to pass in the parent folder to the constructor of this object.")
   public static final String OBJECT_NOT_FOLDERED = "16";

   @RBEntry("Add")
   public static final String ADD = "17";

   @RBEntry("Remove")
   public static final String REMOVE = "18";

   @RBEntry("View")
   public static final String VIEW = "19";

   @RBEntry("Find Object")
   public static final String FIND_OBJECT = "20";

   @RBEntry("{0} Effectivity")
   public static final String TYPE_EFFECTIVITY = "21";

   @RBEntry("Configuration Item:")
   public static final String CONFIGURATION_ITEM = "22";

   @RBEntry("Date")
   public static final String DATE = "23";

   @RBEntry("Start:")
   public static final String START = "24";

   @RBEntry("End:")
   public static final String END = "25";

   @RBEntry("Effectivity Type:")
   public static final String EFFECTIVITY_TYPE = "26";

   @RBEntry("Find Configuration Item")
   public static final String FIND_CONFIGURATION_ITEM = "27";

   @RBEntry("Retrieve")
   public static final String RETRIEVE_BUTTON_LABEL = "28";

   @RBEntry("Stop")
   public static final String STOP_BUTTON_LABEL = "33";

   @RBEntry("Search on:")
   public static final String CLASS_CHOICE_LABEL = "32";

   @RBEntry("Number:")
   public static final String NUMBER_LABEL = "29";

   @RBEntry("Version:")
   public static final String VERSION_LABEL = "30";

   @RBEntry("Get latest?")
   public static final String GET_LATEST_VERSION_LABEL = "31";

   @RBEntry("Unable to find a {0} matching Number: \"{1}\" and Version:  \"{2}\".  Please try entering another Number or Version.")
   @RBArgComment0("a string representing the class of the object being retrieved.")
   @RBArgComment1("a string representing the number entered by the user.")
   @RBArgComment2("a string representing the version entered by the user.")
   public static final String OBJECT_NOT_FOUND = "34";

   @RBEntry("Groups")
   public static final String GROUP_TAB_LABEL = "35";

   @RBEntry("Users")
   public static final String USER_TAB_LABEL = "36";

   @RBEntry("Actors")
   public static final String ACTOR_TAB_LABEL = "37";

   @RBEntry("Roles")
   public static final String ROLE_TAB_LABEL = "38";

   @RBEntry("User:")
   public static final String USER_TEXT_FIELD_LABEL = "39";

   @RBEntry("Group:")
   public static final String GROUP_CHOICE_LABEL = "40";

   @RBEntry("Find")
   public static final String USER_FIND_BUTTON_LABEL = "41";

   @RBEntry("Add >>")
   public static final String ADD_BUTTON_LABEL = "42";

   @RBEntry("Add All >>")
   public static final String ADD_ALL_BUTTON_LABEL = "43";

   @RBEntry("<< Remove")
   public static final String REMOVE_BUTTON_LABEL = "44";

   @RBEntry("<< Remove All")
   public static final String REMOVE_ALL_BUTTON_LABEL = "45";

   @RBEntry("Participants:")
   public static final String SELECTED_PARTICIPANTS_PANEL_LABEL = "46";

   @RBEntry("Unable to find the Principal, User, Group, or Organization that was entered. Please verify that you entered the correct name for the desired Principal, User, Group, Organization.")
   public static final String VERIFY_PARTICIPANT_FAILED = "47";

   @RBEntry("Browse...")
   public static final String BROWSE_BUTTON_LABEL = "48";

   @RBEntry("Group:")
   public static final String GROUP_MODE_LABEL = "49";

   @RBEntry("User:")
   public static final String USER_MODE_LABEL = "50";

   @RBEntry("Principal:")
   public static final String PRINCIPAL_MODE_LABEL = "51";

   @RBEntry(" ")
   public static final String NO_PRINCIPAL_SELECTED_STRING = "52";

   @RBEntry("OK")
   public static final String PSB_OK_BUTTON_LABEL = "53";

   @RBEntry("Cancel")
   public static final String PSB_CANCEL_BUTTON_LABEL = "54";

   @RBEntry("Help")
   public static final String PSB_HELP_BUTTON_LABEL = "55";

   @RBEntry("Browse")
   public static final String BROWSE_DIALOG_TITLE = "56";

   @RBEntry("Search Criteria")
   public static final String SEARCH_CRITERIA_LABEL = "57";

   @RBEntry("User Name")
   public static final String USER_NAME_LABEL = "58";

   @RBEntry("User ID")
   public static final String USER_ID_LABEL = "59";

   @RBEntry("Source")
   public static final String SOURCE_LABEL = "60";

   @RBEntry("All")
   public static final String ALL_SERVICES = "61";

   @RBEntry("Group")
   public static final String GROUP_LABEL = "62";

   @RBEntry("Local")
   public static final String LOCAL_WINDCHILL_SERVICE = "63";

   @RBEntry("Participant")
   public static final String PARTICIPANT_LABEL = "64";

   @RBEntry("Distinguished Name")
   public static final String DISTINGUISHEDNAME_LABEL = "65";

   @RBEntry("Principal")
   public static final String PRINCIPAL_LABEL = "66";

   @RBEntry("Location:")
   public static final String LOCATION_LABEL = "67";

   @RBEntry("Organizations")
   public static final String ORG_TAB_LABEL = "68";

   @RBEntry("Organization:")
   public static final String ORG_CHOICE_LABEL = "69";

   @RBEntry("Organization:")
   public static final String ORG_MODE_LABEL = "70";

   @RBEntry("Organization")
   public static final String ORG_LABEL = "71";

   @RBEntry("Search...")
   public static final String SEARCH_LABEL = "72";

   @RBEntry("Clear")
   public static final String CLEAR_LABEL = "74";

   @RBEntry("Name")
   public static final String NAME_LABEL = "75";

   @RBEntry("ID")
   public static final String ID_LABEL = "76";

   @RBEntry("Results")
   public static final String RESULTS_LABEL = "77";

   @RBEntry("Find Organization")
   public static final String FIND_ORGS_LABEL = "78";

   @RBEntry("Generated")
   @RBComment("Label to indicate that the value normally supplied to a disabled text component will be automatically generated by the system.  Used in 'Create' part/document/etc. applets to indicate that part/document/etc. number will be automatically generated.")
   public static final String GENERATED_LABEL = "79";

   @RBEntry("The business object was automatically checked out to you in order to allow your changes to the relationships.  The document could not be automatically checked in.  Please check in the document manually.")
   @RBComment("Error message about not being to check in a document after checking it out to modify the doc to doc relationships.")
   public static final String CHECKIN_ASSOCIATION_ERROR = "80";

   @RBEntry("An attempt was made to automatically check out the business object to allow your changes to the relationships.  The check out has failed.  Please check out the document and then update the relationships.")
   @RBComment("Error message about not being to check out a document prior to modify the doc to doc relationships.")
   public static final String CHECKOUT_ASSOCIATION_ERROR = "81";

   @RBEntry("Service:")
   @RBComment("Label for principal repository service, replaces the SOURCE_LABEL resource to be consistant with the terminology used in the Principal Administrator")
   public static final String PRINCIPAL_SERVICE_LABEL = "82";

   @RBEntry("Full Name")
   public static final String FULL_NAME_LABEL = "83";

   @RBEntry("Notice")
   public static final String NOTICE = "84";

   @RBEntry("The entered group name does not exist. If 'All' service is selected, please enter group name in format: group name (specific service name).")
   public static final String INVALID_GROUP_NAME = "85";

   @RBEntry("Group Name:")
   public static final String GROUP_NAME_LABEL = "86";

   @RBEntry("Find")
   public static final String GROUP_FIND_BUTTON_LABEL = "87";

   @RBEntry("Organization Name:")
   public static final String ORG_NAME_LABEL = "88";

   @RBEntry("Find")
   public static final String ORG_FIND_BUTTON_LABEL = "89";

   @RBEntry("Service")
   public static final String PRINCIPAL_SERVICE_COLUMN_LABEL = "90";

   @RBEntry("User Name")
   public static final String USER_NAME_COLUMN_LABEL = "91";

   @RBEntry("Qualifier")
   public static final String QUALIFIER_LABEL = "92";

   @RBEntry("Applies To:")
   public static final String SELECTED_PRINCIPAL_TYPE_PANEL_LABEL = "93";

   @RBEntry("Selected principal")
   public static final String SELECT_PRINCIPAL = "94";

   @RBEntry("All except selected principal")
   public static final String SELECT_ALL_EXCEPT_PRINCIPAL = "95";

   @RBEntry("Directory Service")
   @RBComment("Label for principal repository service on workflow setup participants applet.")
   public static final String WF_PRINCIPAL_SERVICE_LABEL = "96";
}
