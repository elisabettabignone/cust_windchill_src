/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.beans.explorer;

import wt.util.resource.*;

@RBUUID("wt.clients.beans.explorer.ExplorerRB")
public final class ExplorerRB extends WTListResourceBundle {
   /**
    * Labels
    **/
   @RBEntry("Created:")
   public static final String PRIVATE_CONSTANT_0 = "createdLbl";

   @RBEntry("Working Copy")
   public static final String PRIVATE_CONSTANT_1 = "workingCopy";

   /**
    * Messages
    **/
   @RBEntry("Searching, please wait...")
   public static final String SEARCHING_DATABASE = "0";

   @RBEntry("Node=")
   public static final String NODE_EQUALS = "1";

   @RBEntry("Unknown")
   public static final String UNKNOWN = "2";

   @RBEntry("Contents of '{0}'")
   public static final String CONTENTS_OF = "3";

   @RBEntry("The {0} you are trying to view contains too many objects to display, and only the first {1} objects can be displayed.  If you are looking for a particular object, you might want to try searching for it instead.  Would you like to view the first {1} objects in this {0}?")
   public static final String PARTIAL_RESULTS_RETURNED = "4";

   @RBEntry("wt.doc.WTPartMaster")
   public static final String WT_PART_MASTER_CLASSNAME = "150";
}
