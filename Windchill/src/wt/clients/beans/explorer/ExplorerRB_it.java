/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.beans.explorer;

import wt.util.resource.*;

@RBUUID("wt.clients.beans.explorer.ExplorerRB")
public final class ExplorerRB_it extends WTListResourceBundle {
   /**
    * Labels
    **/
   @RBEntry("Data creazione:")
   public static final String PRIVATE_CONSTANT_0 = "createdLbl";

   @RBEntry("Copia in modifica")
   public static final String PRIVATE_CONSTANT_1 = "workingCopy";

   /**
    * Messages
    **/
   @RBEntry("Ricerca nel database in corso. Attendere...")
   public static final String SEARCHING_DATABASE = "0";

   @RBEntry("Nodo=")
   public static final String NODE_EQUALS = "1";

   @RBEntry("Sconosciuto")
   public static final String UNKNOWN = "2";

   @RBEntry("Contenuto di '{0}'")
   public static final String CONTENTS_OF = "3";

   @RBEntry("{0} contiene troppi oggetti da visualizzare e solo i primi {1} oggetti possono essere visualizzati. Se si cerca un oggetto particolare, procedere alla ricerca diretta dell'oggetto. Visualizzare i primi {1} oggetti di  {0}?")
   public static final String PARTIAL_RESULTS_RETURNED = "4";

   @RBEntry("wt.doc.WTPartMaster")
   public static final String WT_PART_MASTER_CLASSNAME = "150";
}
