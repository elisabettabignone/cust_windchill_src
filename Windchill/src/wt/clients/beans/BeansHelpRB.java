/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.beans;

import wt.util.resource.*;

@RBUUID("wt.clients.beans.BeansHelpRB")
public final class BeansHelpRB extends WTListResourceBundle {
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_0 = "Contents/beans";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_1 = "Help/beans";

   /**
    * ------Associations Panel-----
    **/
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_2 = "Help/beans/AssociationsPanel";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_3 = "Desc/beans/AssociationsPanel";

   @RBEntry("Click to add an association")
   public static final String PRIVATE_CONSTANT_4 = "Desc/beans/AssociationsPanel/Add";

   @RBEntry("Click to remove the associated object")
   public static final String PRIVATE_CONSTANT_5 = "Desc/beans/AssociationsPanel/Remove";

   @RBEntry("Click to view the associated object")
   public static final String PRIVATE_CONSTANT_6 = "Desc/beans/AssociationsPanel/View";

   @RBEntry("Displays the associated objects")
   public static final String PRIVATE_CONSTANT_7 = "Desc/beans/AssociationsPanel/MultiList";

   /**
    * -------Effectivity Panel-----------
    **/
   @RBEntry("Type or browse for a Configuration Item.  This field is optional")
   public static final String PRIVATE_CONSTANT_8 = "Desc/beans/EffectivityPanel/ConfigItem";

   @RBEntry("Click to browse for a Configuration Item")
   public static final String PRIVATE_CONSTANT_9 = "Desc/beans/EffectivityPanel/BrowseConfigItem";

   @RBEntry("Enter the start date or number for the effectivity")
   public static final String PRIVATE_CONSTANT_10 = "Desc/beans/EffectivityPanel/StartEffectivity";

   @RBEntry("Enter the end date or number for the effectivity")
   public static final String PRIVATE_CONSTANT_11 = "Desc/beans/EffectivityPanel/EndEffectivity";

   /**
    * -------UserSelectorPanel-----------
    **/
   @RBEntry("Type a User Name.")
   public static final String PRIVATE_CONSTANT_12 = "Desc/beans/UserSelectorPanel/UserName";

   @RBEntry("Find all User Names.")
   public static final String PRIVATE_CONSTANT_13 = "Desc/beans/UserSelectorPanel/SearchButton";

   @RBEntry("Select a User Group.")
   public static final String PRIVATE_CONSTANT_14 = "Desc/beans/UserSelectorPanel/GroupChoice";

   @RBEntry("Select a User Name and press OK.")
   public static final String PRIVATE_CONSTANT_15 = "Desc/beans/UserSelectorPanel/UserList";

   /**
    * -------------------------------------------------------------------
    * Folder Panel
    * -------------------------------------------------------------------
    **/
   @RBEntry("Displays the location of the folder")
   public static final String PRIVATE_CONSTANT_16 = "Desc/beans/FolderPanel/ViewLocation";

   @RBEntry("Type in a folder location")
   public static final String PRIVATE_CONSTANT_17 = "Desc/beans/FolderPanel/UpdateLocation";

   @RBEntry("Click to browse for a folder")
   public static final String PRIVATE_CONSTANT_18 = "Desc/beans/FolderPanel/Browse";

   /**
    * -----PrincipalSelectionBrowser-----
    **/
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_19 = "Help/beans/PrincipalSelectionBrowser";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_20 = "Desc/beans/PrincipalSelectionBrowser";

   @RBEntry("Displays the list of available groups.")
   public static final String PRIVATE_CONSTANT_21 = "Desc/beans/PrincipalSelectionBrowser/groupList";

   @RBEntry("Displays the list of available users.")
   public static final String PRIVATE_CONSTANT_22 = "Desc/beans/PrincipalSelectionBrowser/userList";

   @RBEntry("Enter search criteria for a user in this field.")
   public static final String PRIVATE_CONSTANT_23 = "Desc/beans/PrincipalSelectionBrowser/userTextField";

   @RBEntry("Click this button to perform a search for users.")
   public static final String PRIVATE_CONSTANT_24 = "Desc/beans/PrincipalSelectionBrowser/userFindButton";

   @RBEntry("Select a group from the choice box to get a list of all users in that group.")
   public static final String PRIVATE_CONSTANT_25 = "Desc/beans/PrincipalSelectionBrowser/groupChoice";

   @RBEntry("Displays the list of available actors.")
   public static final String PRIVATE_CONSTANT_26 = "Desc/beans/PrincipalSelectionBrowser/actorList";

   @RBEntry("Displays the list of available roles.")
   public static final String PRIVATE_CONSTANT_27 = "Desc/beans/PrincipalSelectionBrowser/roleList";

   @RBEntry("Adds the selected item in the selected tab to the participants list.")
   public static final String PRIVATE_CONSTANT_28 = "Desc/beans/PrincipalSelectionBrowser/addButton";

   @RBEntry("Adds all the items in the selected tab to the participants list.")
   public static final String PRIVATE_CONSTANT_29 = "Desc/beans/PrincipalSelectionBrowser/addAllButton";

   @RBEntry("Removes the selected item in the participants list from the participants list.")
   public static final String PRIVATE_CONSTANT_30 = "Desc/beans/PrincipalSelectionBrowser/removeButton";

   @RBEntry("Removes all items from the participants list.")
   public static final String PRIVATE_CONSTANT_31 = "Desc/beans/PrincipalSelectionBrowser/removeAllButton";

   @RBEntry("Displays the list of all selected participants.")
   public static final String PRIVATE_CONSTANT_32 = "Desc/beans/PrincipalSelectionBrowser/selectedParticipantsList";

   /**
    * -----PrincipalSelectionPanel-----
    **/
   @RBEntry("CalFindUserOrg")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_33 = "Help/beans/PrincipalSelectionPanel";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_34 = "Desc/beans/PrincipalSelectionPanel";

   @RBEntry("Click on this button to browse for the desired WTPrincipal.")
   public static final String PRIVATE_CONSTANT_35 = "Desc/beans/PrincipalSelectionPanel/browseButton";

   @RBEntry("Enter the name of the desired principal in this text field.")
   public static final String PRIVATE_CONSTANT_36 = "Desc/beans/PrincipalSelectionPanel/selectionTextField";

   /**
    * -----FindUserPanel-----
    **/
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_37 = "Help/beans/FindUserPanel";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_38 = "Desc/beans/FindUserPanel";

   @RBEntry("Displays the list of available users.")
   public static final String PRIVATE_CONSTANT_39 = "Desc/beans/FindUserPanel/userList";

   @RBEntry("Enter search criteria for a full user name in this field.")
   public static final String PRIVATE_CONSTANT_40 = "Desc/beans/FindUserPanel/userNameTextField";

   @RBEntry("Enter search criteria for a user id in this field.")
   public static final String PRIVATE_CONSTANT_41 = "Desc/beans/FindUserPanel/userIdTextField";

   @RBEntry("Click this button to perform a search for users.")
   public static final String PRIVATE_CONSTANT_42 = "Desc/beans/FindUserPanel/findButton";

   @RBEntry("Select a source from the choice box to select the source for finding users.")
   public static final String PRIVATE_CONSTANT_43 = "Desc/beans/FindUserPanel/serviceChoice";

   @RBEntry("Select a group from the choice box to get a list of all users in that group.")
   public static final String PRIVATE_CONSTANT_44 = "Desc/beans/FindUserPanel/groupChoice";

   /**
    * -----FindGroupPanel-----
    **/
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_45 = "Help/beans/FindGroupPanel";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_46 = "Desc/beans/FindGroupPanel";

   @RBEntry("Displays the list of available groups.")
   public static final String PRIVATE_CONSTANT_47 = "Desc/beans/FindGroupPanel/groupList";

   @RBEntry("Select a source from the choice box to select the source for finding users.")
   public static final String PRIVATE_CONSTANT_48 = "Desc/beans/FindGroupPanel/serviceChoice";

   /**
    * -----OrgChooserDialog-----
    **/
   @RBEntry("CalFindUser")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_49 = "Help/beans/OrgChooserDialog";
}
