/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.beans.graph;

import wt.util.resource.*;

@RBUUID("wt.clients.beans.graph.graphRB")
public final class graphRB extends WTListResourceBundle {
   /**
    * test string
    **/
   @RBEntry("..Localized..")
   public static final String TEST_STRING = "0";

   /**
    * Buttons
    * Labels
    **/
   @RBEntry("Cut")
   public static final String CUT = "9";

   @RBEntry("Copy")
   public static final String COPY = "10";

   @RBEntry("Paste")
   public static final String PASTE = "11";

   @RBEntry("Open")
   public static final String OPEN = "12";

   /**
    * Titles
    * Text
    **/
   @RBEntry("Impossible to use the model provided, creating a default one.")
   public static final String NEW_MODEL_REQUIRED = "1";

   @RBEntry("Impossible to use the selection model provided, creating a default one.")
   public static final String NEW_SELECTION_MODEL_REQUIRED = "2";

   @RBEntry("The specified link does not exist!")
   public static final String LINK_DOES_NOT_EXIST = "3";

   @RBEntry("The specified link already exists!")
   public static final String LINK_ALREADY_EXIST = "4";

   @RBEntry("The specified node does not exist!")
   public static final String NODE_DOES_NOT_EXIST = "5";

   @RBEntry("The specified node already exists!")
   public static final String NODE_ALREADY_EXIST = "6";

   /**
    * Tool Tips
    * Exceptions
    **/
   @RBEntry("The specified node is invalid!")
   public static final String INVALID_NODE = "7";

   @RBEntry("The specified link is invalid!")
   public static final String INVALID_LINK = "8";
}
