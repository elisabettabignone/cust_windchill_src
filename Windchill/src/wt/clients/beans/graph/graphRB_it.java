/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.beans.graph;

import wt.util.resource.*;

@RBUUID("wt.clients.beans.graph.graphRB")
public final class graphRB_it extends WTListResourceBundle {
   /**
    * test string
    **/
   @RBEntry("..Localizzato..")
   public static final String TEST_STRING = "0";

   /**
    * Buttons
    * Labels
    **/
   @RBEntry("Taglia")
   public static final String CUT = "9";

   @RBEntry("Copia")
   public static final String COPY = "10";

   @RBEntry("Incolla")
   public static final String PASTE = "11";

   @RBEntry("Apri")
   public static final String OPEN = "12";

   /**
    * Titles
    * Text
    **/
   @RBEntry("Impossibile utilizzare il modello fornito per la creazione di uno predefinito.")
   public static final String NEW_MODEL_REQUIRED = "1";

   @RBEntry("Impossibile utilizzare il modello di selezione fornito per la creazione di uno predefinito.")
   public static final String NEW_SELECTION_MODEL_REQUIRED = "2";

   @RBEntry("Il link specificato non esiste.")
   public static final String LINK_DOES_NOT_EXIST = "3";

   @RBEntry("Il link specificato è già esistente.")
   public static final String LINK_ALREADY_EXIST = "4";

   @RBEntry("Il nodo specificato non esiste.")
   public static final String NODE_DOES_NOT_EXIST = "5";

   @RBEntry("Il nodo specificato è già esistente.")
   public static final String NODE_ALREADY_EXIST = "6";

   /**
    * Tool Tips
    * Exceptions
    **/
   @RBEntry("Il nodo specificato non è valido.")
   public static final String INVALID_NODE = "7";

   @RBEntry("Il link specificato non è valido.")
   public static final String INVALID_LINK = "8";
}
