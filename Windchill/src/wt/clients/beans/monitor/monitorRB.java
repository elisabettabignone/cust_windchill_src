/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.beans.monitor;

import wt.util.resource.*;

@RBUUID("wt.clients.beans.monitor.monitorRB")
public final class monitorRB extends WTListResourceBundle {
   /**
    * test string
    **/
   @RBEntry("..Localized..")
   public static final String TEST_STRING = "0";

   /**
    * Buttons
    * Labels
    **/
   @RBEntry("Start :")
   public static final String START_LABEL = "2";

   @RBEntry("End :")
   public static final String END_LABEL = "3";

   /**
    * Titles
    * Text
    **/
   @RBEntry("There is no valid model accessible !")
   public static final String NO_VALID_MODEL = "1";

   @RBEntry("No Name")
   public static final String NO_NAME = "4";

   @RBEntry("Not Started")
   public static final String NOT_STARTED = "5";

   @RBEntry("Not Finished")
   public static final String NOT_FINISHED = "6";

   @RBEntry("N/A")
   public static final String NOT_AVAILABLE = "7";

   /**
    * Tool Tips
    * Exceptions
    **/
   @RBEntry("The node provided is not a monitor node!")
   public static final String NOT_MONITOR_NODE = "8";

   @RBEntry("The action could not be applied due to the current state of the object!")
   public static final String INVALID_TRANSITION = "INVALID_TRANSITION_EXC";
}
