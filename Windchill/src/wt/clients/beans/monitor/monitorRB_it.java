/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.beans.monitor;

import wt.util.resource.*;

@RBUUID("wt.clients.beans.monitor.monitorRB")
public final class monitorRB_it extends WTListResourceBundle {
   /**
    * test string
    **/
   @RBEntry("..Localizzato..")
   public static final String TEST_STRING = "0";

   /**
    * Buttons
    * Labels
    **/
   @RBEntry("Inizio:")
   public static final String START_LABEL = "2";

   @RBEntry("Fine:")
   public static final String END_LABEL = "3";

   /**
    * Titles
    * Text
    **/
   @RBEntry("Nessun modello valido accessibile.")
   public static final String NO_VALID_MODEL = "1";

   @RBEntry("Nessun nome")
   public static final String NO_NAME = "4";

   @RBEntry("Non avviato")
   public static final String NOT_STARTED = "5";

   @RBEntry("Non finito")
   public static final String NOT_FINISHED = "6";

   @RBEntry("N.D.")
   public static final String NOT_AVAILABLE = "7";

   /**
    * Tool Tips
    * Exceptions
    **/
   @RBEntry("Il nodo fornito non è un nodo di controllo.")
   public static final String NOT_MONITOR_NODE = "8";

   @RBEntry("L'azione non è stata effettuata a causa dello stato del ciclo di vita attuale dell'oggetto.")
   public static final String INVALID_TRANSITION = "INVALID_TRANSITION_EXC";
}
