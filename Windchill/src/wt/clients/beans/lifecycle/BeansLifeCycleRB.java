/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.beans.lifecycle;

import wt.util.resource.*;

@RBUUID("wt.clients.beans.lifecycle.BeansLifeCycleRB")
public final class BeansLifeCycleRB extends WTListResourceBundle {
   @RBEntry("Browse...")
   public static final String BROWSE_BUTTON_LABEL = "0";

   @RBEntry("Life Cycle")
   public static final String LIFE_CYCLE_LABEL = "1";

   @RBEntry("State")
   public static final String STATE_LABEL = "2";

   @RBEntry("*")
   @RBPseudo(false)
   public static final String REQUIRED_LABEL_PREFIX = "3";

   @RBEntry(":")
   @RBPseudo(false)
   public static final String LABEL_SUFFIX = "4";

   @RBEntry("Project")
   public static final String PROJECT_LABEL = "5";

   @RBEntry("Could not find the class for: \"{0}\"")
   public static final String CLASS_NOT_FOUND_ERROR = "6";

   @RBEntry("Assigning the object to a life cycle failed: {0}")
   public static final String LIFE_CYCLE_ASSIGNMENT_ERROR = "7";

   @RBEntry("Assigning the object to a project failed: {0}")
   public static final String PROJECT_ASSIGNMENT_ERROR = "8";

   @RBEntry("Obtaining the project failed: {0}")
   public static final String GETTING_PROJECT_ERROR = "9";

   @RBEntry("Find LifeCycle")
   public static final String FIND_LIFECYCLE = "10";

   @RBEntry("Search...")
   public static final String SEARCH_BUTTON_LABEL = "11";

   @RBEntry("Create")
   public static final String CREATE_BUTTON_LABEL = "12";

   @RBEntry("Find Project")
   public static final String FIND_PROJECT = "13";

   @RBEntry("Team")
   public static final String TEAM_LABEL = "14";

   @RBEntry("Find Team")
   public static final String FIND_TEAM = "15";

   @RBEntry("Assigning the object to a team failed: {0}")
   public static final String TEAM_ASSIGNMENT_ERROR = "16";

   @RBEntry("Obtaining the team failed: {0}")
   public static final String GETTING_TEAM_ERROR = "17";
}
