/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.beans.lifecycle;

import wt.util.resource.*;

@RBUUID("wt.clients.beans.lifecycle.LifeCycleHelpResources")
@RBNameException //Grandfathered by conversion
public final class LifeCycleHelpResources_it extends WTListResourceBundle {
   @RBEntry("Specifica un ciclo di vita")
   public static final String PRIVATE_CONSTANT_0 = "Desc/lifecycle//LifeCycleChoice";

   @RBEntry("Cerca un ciclo di vita")
   public static final String PRIVATE_CONSTANT_1 = "Desc/lifecycle//LifeCycleBrowse";

   @RBEntry("Specificare un progetto")
   public static final String PRIVATE_CONSTANT_2 = "Desc/lifecycle//ProjectChoice";

   @RBEntry("Cerca un progetto")
   public static final String PRIVATE_CONSTANT_3 = "Desc/lifecycle//ProjectBrowse";
}
