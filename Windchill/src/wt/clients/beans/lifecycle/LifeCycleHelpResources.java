/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.beans.lifecycle;

import wt.util.resource.*;

@RBUUID("wt.clients.beans.lifecycle.LifeCycleHelpResources")
@RBNameException //Grandfathered by conversion
public final class LifeCycleHelpResources extends WTListResourceBundle {
   @RBEntry("Enter a life cycle")
   public static final String PRIVATE_CONSTANT_0 = "Desc/lifecycle//LifeCycleChoice";

   @RBEntry("Browse for a life cycle")
   public static final String PRIVATE_CONSTANT_1 = "Desc/lifecycle//LifeCycleBrowse";

   @RBEntry("Enter a project")
   public static final String PRIVATE_CONSTANT_2 = "Desc/lifecycle//ProjectChoice";

   @RBEntry("Browse for a project")
   public static final String PRIVATE_CONSTANT_3 = "Desc/lifecycle//ProjectBrowse";
}
