/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.beans.lifecycle;

import wt.util.resource.*;

@RBUUID("wt.clients.beans.lifecycle.BeansLifeCycleRB")
public final class BeansLifeCycleRB_it extends WTListResourceBundle {
   @RBEntry("Sfoglia...")
   public static final String BROWSE_BUTTON_LABEL = "0";

   @RBEntry("Ciclo di vita")
   public static final String LIFE_CYCLE_LABEL = "1";

   @RBEntry("Stato del ciclo di vita")
   public static final String STATE_LABEL = "2";

   @RBEntry("*")
   @RBPseudo(false)
   public static final String REQUIRED_LABEL_PREFIX = "3";

   @RBEntry(":")
   @RBPseudo(false)
   public static final String LABEL_SUFFIX = "4";

   @RBEntry("Progetto")
   public static final String PROJECT_LABEL = "5";

   @RBEntry("Impossibile trovare la classe associata a: \"{0}\"")
   public static final String CLASS_NOT_FOUND_ERROR = "6";

   @RBEntry("Impossibile assegnare l'oggetto a un ciclo di vita: {0}")
   public static final String LIFE_CYCLE_ASSIGNMENT_ERROR = "7";

   @RBEntry("Impossibile assegnare l'oggetto a un progetto: {0}")
   public static final String PROJECT_ASSIGNMENT_ERROR = "8";

   @RBEntry("Impossibile ottenere il progetto: {0}")
   public static final String GETTING_PROJECT_ERROR = "9";

   @RBEntry("Trova ciclo di vita")
   public static final String FIND_LIFECYCLE = "10";

   @RBEntry("Cerca...")
   public static final String SEARCH_BUTTON_LABEL = "11";

   @RBEntry("Crea")
   public static final String CREATE_BUTTON_LABEL = "12";

   @RBEntry("Trova progetto")
   public static final String FIND_PROJECT = "13";

   @RBEntry("Team")
   public static final String TEAM_LABEL = "14";

   @RBEntry("Trova team")
   public static final String FIND_TEAM = "15";

   @RBEntry("Impossibile assegnare l'oggetto a un team: {0}")
   public static final String TEAM_ASSIGNMENT_ERROR = "16";

   @RBEntry("Impossibile ottenere il team: {0}")
   public static final String GETTING_TEAM_ERROR = "17";
}
