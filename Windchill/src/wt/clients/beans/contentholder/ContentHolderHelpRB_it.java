/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.beans.contentholder;

import wt.util.resource.*;

@RBUUID("wt.clients.beans.contentholder.ContentHolderHelpRB")
public final class ContentHolderHelpRB_it extends WTListResourceBundle {
   @RBEntry("DocMgrDocAttachAdd")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_0 = "Help/contentholder/AddUrl";

   @RBEntry("DocMgrOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_1 = "Help/contentholder/FileProperties";

   @RBEntry("Elenco di file associati e URL.")
   public static final String PRIVATE_CONSTANT_2 = "Desc/contentholder//ContentList";

   @RBEntry("Fare clic per aggiungere un nuovo file associato.")
   public static final String PRIVATE_CONSTANT_3 = "Desc/contentholder//AddFile";

   @RBEntry("Fare clic per aggiungere un nuovo URL.")
   public static final String PRIVATE_CONSTANT_4 = "Desc/contentholder//AddURL";

   @RBEntry("Fare clic per rimuovere l'oggetto di contenuto selezionato.")
   public static final String PRIVATE_CONSTANT_5 = "Desc/contentholder//Remove";

   @RBEntry("Fare clic per aggiornare l'oggetto di contenuto selezionato.")
   public static final String PRIVATE_CONSTANT_6 = "Desc/contentholder//Replace";

   @RBEntry("Fare clic per visualizzare le proprietà dell'oggetto di contenuto selezionato.")
   public static final String PRIVATE_CONSTANT_7 = "Desc/contentholder//Properties";

   @RBEntry("Fare clic per aggiornare le proprietà dell'oggetto di contenuto selezionato.")
   public static final String PRIVATE_CONSTANT_8 = "Desc/contentholder/Update/Properties";

   @RBEntry("Fare clic per visualizzare l'oggetto di contenuto selezionato.")
   public static final String PRIVATE_CONSTANT_9 = "Desc/contentholder//Get";

   @RBEntry("Fare clic per salvare il documento.")
   public static final String PRIVATE_CONSTANT_10 = "Desc/contentholder//Save";

   @RBEntry("Fare clic per reimpostare gli oggetti di contenuto.  Tutte le modifiche non memorizzate andranno perse.")
   public static final String PRIVATE_CONSTANT_11 = "Desc/contentholder//Reset";

   @RBEntry("Fare clic per accedere alla Guida in linea.")
   public static final String PRIVATE_CONSTANT_12 = "Desc/contentholder//Help";

   @RBEntry("Fare clic per chiudere la finestra.")
   public static final String PRIVATE_CONSTANT_13 = "Desc/contentholder//Close";
}
