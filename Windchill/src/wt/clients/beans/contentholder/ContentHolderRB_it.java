/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.beans.contentholder;

import wt.util.resource.*;

@RBUUID("wt.clients.beans.contentholder.ContentHolderRB")
public final class ContentHolderRB_it extends WTListResourceBundle {
   /**
    * FIELD LABELS
    **/
   @RBEntry("URL:")
   public static final String PRIVATE_CONSTANT_0 = "addressLabel";

   @RBEntry("Nome descrittivo:")
   public static final String PRIVATE_CONSTANT_1 = "descriptiveNameLabel";

   @RBEntry("Nome file:")
   public static final String PRIVATE_CONSTANT_2 = "fileNameLabel";

   @RBEntry("Ultimo prelievo da:")
   public static final String PRIVATE_CONSTANT_3 = "filePathLabel";

   @RBEntry("Formato:")
   public static final String PRIVATE_CONSTANT_4 = "formatLabel";

   @RBEntry("Dimensione:")
   public static final String PRIVATE_CONSTANT_5 = "sizeLabel";

   @RBEntry("Data ultima modifica:")
   public static final String PRIVATE_CONSTANT_6 = "updatedOnLabel";

   @RBEntry("Autore modifiche:")
   public static final String PRIVATE_CONSTANT_7 = "updatedByLabel";

   /**
    * BUTTON LABELS
    **/
   @RBEntry("Aggiungi file")
   public static final String PRIVATE_CONSTANT_8 = "addFileLabel";

   @RBEntry("Aggiungi URL")
   public static final String PRIVATE_CONSTANT_9 = "addAddressLabel";

   @RBEntry("Rimuovi")
   public static final String PRIVATE_CONSTANT_10 = "removeLabel";

   @RBEntry("Sostituisci")
   public static final String PRIVATE_CONSTANT_11 = "replaceLabel";

   @RBEntry("Proprietà")
   public static final String PRIVATE_CONSTANT_12 = "propertiesLabel";

   @RBEntry("Visualizza")
   public static final String PRIVATE_CONSTANT_13 = "getLabel";

   @RBEntry("Chiudi")
   public static final String PRIVATE_CONSTANT_14 = "closeButtonLabel";

   @RBEntry("OK")
   public static final String PRIVATE_CONSTANT_15 = "okButtonLabel";

   @RBEntry("Apri")
   public static final String PRIVATE_CONSTANT_16 = "openButtonLabel";

   @RBEntry("Applica")
   public static final String PRIVATE_CONSTANT_17 = "applyButtonLabel";

   @RBEntry("Annulla")
   public static final String PRIVATE_CONSTANT_18 = "cancelButtonLabel";

   @RBEntry("Guida")
   public static final String PRIVATE_CONSTANT_19 = "helpButtonLabel";

   @RBEntry("Salva")
   public static final String PRIVATE_CONSTANT_20 = "saveButtonLabel";

   @RBEntry("Reimposta")
   public static final String PRIVATE_CONSTANT_21 = "resetButtonLabel";

   /**
    * MULTILIST HEADINGS
    **/
   @RBEntry("Nome")
   public static final String PRIVATE_CONSTANT_22 = "nameLabel";

   @RBEntry("Modificato")
   public static final String PRIVATE_CONSTANT_23 = "modifiedLabel";

   @RBEntry("Da fare")
   public static final String PRIVATE_CONSTANT_24 = "actionLabel";

   @RBEntry("*** NUOVO FILE DA AGGIUNGERE ***")
   public static final String PRIVATE_CONSTANT_25 = "newFileLabel";

   @RBEntry("SOSTITUISCI")
   public static final String PRIVATE_CONSTANT_26 = "replaceFileAction";

   @RBEntry("AGGIUNGI")
   public static final String PRIVATE_CONSTANT_27 = "addFileAction";

   @RBEntry("Sconosciuto")
   public static final String PRIVATE_CONSTANT_28 = "unknownFormat";

   @RBEntry(" Link URL")
   public static final String PRIVATE_CONSTANT_29 = "urlLink";

   /**
    * SYMBOLS
    **/
   @RBEntry("*")
   @RBPseudo(false)
   public static final String PRIVATE_CONSTANT_30 = "required";

   /**
    * TITLES
    **/
   @RBEntry("Proprietà file")
   public static final String PRIVATE_CONSTANT_31 = "filePropertiesTitle";

   @RBEntry("Proprietà file <{0}>")
   public static final String PRIVATE_CONSTANT_32 = "filePropertiesObjTitle";

   @RBEntry("Proprietà URL")
   public static final String PRIVATE_CONSTANT_33 = "urlPropertiesTitle";

   @RBEntry("Proprietà URL <{0}>")
   public static final String PRIVATE_CONSTANT_34 = "urlPropertiesObjTitle";

   /**
    * UNITS
    **/
   @RBEntry("KB")
   public static final String KILOBYTES = "19";

   /**
    * ERROR MESSAGES
    **/
   @RBEntry("È stato fornito un oggetto di classe \"{0}\" al posto del tipo \"wt.content.ApplicationData\" richiesto.")
   public static final String NOT_APPLICATION_DATA_TYPE = "0";

   @RBEntry("È stato fornito un oggetto di classe \"{0}\" al posto del tipo \"wt.content.URLData\" richiesto.")
   public static final String NOT_URL_DATA_TYPE = "1";

   @RBEntry("Prima di manipolare il contenitore, è necessario fornirne uno.  Fornire un contenitore da manipolare.")
   public static final String NO_ASSOC_CONTENT_HOLDER = "2";

   @RBEntry("\"{0}\" non è un valore valido per la proprietà \"{1}\": {2}.")
   public static final String PROPERTY_VETO_EXCEPTION = "3";

   @RBEntry("Il contenitore fornito non è persistente, ma è necessario fornirne uno persistente prima di manipolare il contenitore.")
   public static final String CONTENT_NOT_PERSISTENT = "4";

   @RBEntry("Si è verificato un errore durante l'impostazione del formato di '{0}': {1}.")
   public static final String SET_FORMAT_ERROR = "5";

   @RBEntry("Si è verificato un errore durante il caricamento del file '{0}': {1}.")
   public static final String REPLACE_UPLOAD_FAILED = "6";

   @RBEntry("Si è verificato un errore durante il caricamento di un nuovo file: {0}.")
   public static final String ADD_UPLOAD_FAILED = "7";

   @RBEntry("Si è verificato un errore durante il caricamento del file '{0}'. Stato HTTP restituito: {1}.")
   public static final String REPLACE_NOT_SUCCESSFUL = "8";

   @RBEntry("Si è verificato un errore durante il caricamento di un nuovo file. Stato HTTP restituito: {0}.")
   public static final String ADD_NOT_SUCCESSFUL = "9";

   @RBEntry("L'oggetto fornito non è un contenitore.")
   public static final String OBJECT_NOT_CONTENTHOLDER = "10";

   @RBEntry("Si è verificato un errore durante il caricamento dei file: {0}.")
   public static final String UPLOAD_FILES_FAILED = "11";

   @RBEntry("Si è verificato un errore durante il caricamento dei file. Stato HTTP restituito: {0}.")
   public static final String UPLOAD_NOT_SUCCESSFUL = "12";

   @RBEntry("Si è verificato un errore durante l'impostazione della proposta di modifica di '{0}': {1}.")
   public static final String SET_INTENT_TO_CHANGE_FAILED = "13";

   @RBEntry("Si è verificato un errore durante il salvataggio: {0}.")
   public static final String SAVE_FAILED = "14";

   @RBEntry("Si è verificato un errore durante la creazione di un nuovo file: {0}. Contattare l'amministratore del sistema.")
   public static final String CREATE_APPLICATION_DATA_FAILED = "15";

   @RBEntry("L'URL specificato, '{0}', non può essere aperto.  Verificare che l'URL immesso sia valido.")
   public static final String MALFORMED_URL_EXCEPTION = "16";

   @RBEntry("Il formato del file '{0}' non può essere vuoto.  Selezionare un formato valido.")
   public static final String FILE_FORMAT_NOT_GIVEN = "17";

   @RBEntry("Si è verificato un errore durante lo scaricamento del file {0}: {1}.")
   public static final String DOWNLOAD_FILE_FAILED = "18";
}
