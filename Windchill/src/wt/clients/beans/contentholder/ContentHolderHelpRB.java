/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.beans.contentholder;

import wt.util.resource.*;

@RBUUID("wt.clients.beans.contentholder.ContentHolderHelpRB")
public final class ContentHolderHelpRB extends WTListResourceBundle {
   @RBEntry("DocMgrDocAttachAdd")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_0 = "Help/contentholder/AddUrl";

   @RBEntry("DocMgrOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_1 = "Help/contentholder/FileProperties";

   @RBEntry("List of content files and URLs.")
   public static final String PRIVATE_CONSTANT_2 = "Desc/contentholder//ContentList";

   @RBEntry("Click to add a new content file.")
   public static final String PRIVATE_CONSTANT_3 = "Desc/contentholder//AddFile";

   @RBEntry("Click to add a new URL.")
   public static final String PRIVATE_CONSTANT_4 = "Desc/contentholder//AddURL";

   @RBEntry("Click to remove the selected content object.")
   public static final String PRIVATE_CONSTANT_5 = "Desc/contentholder//Remove";

   @RBEntry("Click to update the selected content object.")
   public static final String PRIVATE_CONSTANT_6 = "Desc/contentholder//Replace";

   @RBEntry("Click to view the properties of the selected content object.")
   public static final String PRIVATE_CONSTANT_7 = "Desc/contentholder//Properties";

   @RBEntry("Click to update the properties of the selected content object.")
   public static final String PRIVATE_CONSTANT_8 = "Desc/contentholder/Update/Properties";

   @RBEntry("Click to view the content object.")
   public static final String PRIVATE_CONSTANT_9 = "Desc/contentholder//Get";

   @RBEntry("Click to save the document.")
   public static final String PRIVATE_CONSTANT_10 = "Desc/contentholder//Save";

   @RBEntry("Click to reset the content objects.  Any unsaved changes will be lost.")
   public static final String PRIVATE_CONSTANT_11 = "Desc/contentholder//Reset";

   @RBEntry("Click to access online help.")
   public static final String PRIVATE_CONSTANT_12 = "Desc/contentholder//Help";

   @RBEntry("Click to close this window.")
   public static final String PRIVATE_CONSTANT_13 = "Desc/contentholder//Close";
}
