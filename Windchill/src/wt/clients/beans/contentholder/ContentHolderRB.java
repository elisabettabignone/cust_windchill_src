/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.beans.contentholder;

import wt.util.resource.*;

@RBUUID("wt.clients.beans.contentholder.ContentHolderRB")
public final class ContentHolderRB extends WTListResourceBundle {
   /**
    * FIELD LABELS
    **/
   @RBEntry("URL:")
   public static final String PRIVATE_CONSTANT_0 = "addressLabel";

   @RBEntry("Descriptive Name:")
   public static final String PRIVATE_CONSTANT_1 = "descriptiveNameLabel";

   @RBEntry("File Name:")
   public static final String PRIVATE_CONSTANT_2 = "fileNameLabel";

   @RBEntry("Last Retrieved From:")
   public static final String PRIVATE_CONSTANT_3 = "filePathLabel";

   @RBEntry("Format:")
   public static final String PRIVATE_CONSTANT_4 = "formatLabel";

   @RBEntry("Size:")
   public static final String PRIVATE_CONSTANT_5 = "sizeLabel";

   @RBEntry("Last Modified:")
   public static final String PRIVATE_CONSTANT_6 = "updatedOnLabel";

   @RBEntry("Modified By:")
   public static final String PRIVATE_CONSTANT_7 = "updatedByLabel";

   /**
    * BUTTON LABELS
    **/
   @RBEntry("Add File")
   public static final String PRIVATE_CONSTANT_8 = "addFileLabel";

   @RBEntry("Add URL")
   public static final String PRIVATE_CONSTANT_9 = "addAddressLabel";

   @RBEntry("Remove")
   public static final String PRIVATE_CONSTANT_10 = "removeLabel";

   @RBEntry("Replace")
   public static final String PRIVATE_CONSTANT_11 = "replaceLabel";

   @RBEntry("Properties")
   public static final String PRIVATE_CONSTANT_12 = "propertiesLabel";

   @RBEntry("Get")
   public static final String PRIVATE_CONSTANT_13 = "getLabel";

   @RBEntry("Close")
   public static final String PRIVATE_CONSTANT_14 = "closeButtonLabel";

   @RBEntry("OK")
   public static final String PRIVATE_CONSTANT_15 = "okButtonLabel";

   @RBEntry("Open")
   public static final String PRIVATE_CONSTANT_16 = "openButtonLabel";

   @RBEntry("Apply")
   public static final String PRIVATE_CONSTANT_17 = "applyButtonLabel";

   @RBEntry("Cancel")
   public static final String PRIVATE_CONSTANT_18 = "cancelButtonLabel";

   @RBEntry("Help")
   public static final String PRIVATE_CONSTANT_19 = "helpButtonLabel";

   @RBEntry("Save")
   public static final String PRIVATE_CONSTANT_20 = "saveButtonLabel";

   @RBEntry("Reset")
   public static final String PRIVATE_CONSTANT_21 = "resetButtonLabel";

   /**
    * MULTILIST HEADINGS
    **/
   @RBEntry("Name")
   public static final String PRIVATE_CONSTANT_22 = "nameLabel";

   @RBEntry("Modified")
   public static final String PRIVATE_CONSTANT_23 = "modifiedLabel";

   @RBEntry("To-Do")
   public static final String PRIVATE_CONSTANT_24 = "actionLabel";

   @RBEntry("*** NEW FILE TO BE ADDED ***")
   public static final String PRIVATE_CONSTANT_25 = "newFileLabel";

   @RBEntry("REPLACE")
   public static final String PRIVATE_CONSTANT_26 = "replaceFileAction";

   @RBEntry("ADD")
   public static final String PRIVATE_CONSTANT_27 = "addFileAction";

   @RBEntry("Unknown")
   public static final String PRIVATE_CONSTANT_28 = "unknownFormat";

   @RBEntry(" URL Link")
   public static final String PRIVATE_CONSTANT_29 = "urlLink";

   /**
    * SYMBOLS
    **/
   @RBEntry("*")
   @RBPseudo(false)
   public static final String PRIVATE_CONSTANT_30 = "required";

   /**
    * TITLES
    **/
   @RBEntry("File Properties")
   public static final String PRIVATE_CONSTANT_31 = "filePropertiesTitle";

   @RBEntry("File Properties <{0}>")
   public static final String PRIVATE_CONSTANT_32 = "filePropertiesObjTitle";

   @RBEntry("URL Properties")
   public static final String PRIVATE_CONSTANT_33 = "urlPropertiesTitle";

   @RBEntry("URL Properties <{0}>")
   public static final String PRIVATE_CONSTANT_34 = "urlPropertiesObjTitle";

   /**
    * UNITS
    **/
   @RBEntry("KB")
   public static final String KILOBYTES = "19";

   /**
    * ERROR MESSAGES
    **/
   @RBEntry("An object of class \"{0}\" was given were a type \"wt.content.ApplicationData\" is required.")
   public static final String NOT_APPLICATION_DATA_TYPE = "0";

   @RBEntry("An object of class \"{0}\" was given were a type \"wt.content.URLData\" is required.")
   public static final String NOT_URL_DATA_TYPE = "1";

   @RBEntry("Before the content holder can be manipulated, a content holder must be supplied.  Please supply a content holder to manipulate.")
   public static final String NO_ASSOC_CONTENT_HOLDER = "2";

   @RBEntry("\"{0}\" is not a valid value for property \"{1}\": {2}.")
   public static final String PROPERTY_VETO_EXCEPTION = "3";

   @RBEntry("The given content holder is currently not persistent, but a persistent content holder is required before the content holder can be manipulated.")
   public static final String CONTENT_NOT_PERSISTENT = "4";

   @RBEntry("An error occurred while trying to set the format of '{0}': {1}.")
   public static final String SET_FORMAT_ERROR = "5";

   @RBEntry("An error occurred while trying to upload file '{0}': {1}.")
   public static final String REPLACE_UPLOAD_FAILED = "6";

   @RBEntry("An error occurred while trying to upload a new file: {0}.")
   public static final String ADD_UPLOAD_FAILED = "7";

   @RBEntry("An error occurred while trying to upload file '{0}'. The HTTP Status returned was: {1}.")
   public static final String REPLACE_NOT_SUCCESSFUL = "8";

   @RBEntry("An error occurred while trying to upload a new file. The HTTP Status returned was: {0}.")
   public static final String ADD_NOT_SUCCESSFUL = "9";

   @RBEntry("The given object is not a content holder.")
   public static final String OBJECT_NOT_CONTENTHOLDER = "10";

   @RBEntry("An error occurred while trying to upload files: {0}.")
   public static final String UPLOAD_FILES_FAILED = "11";

   @RBEntry("An error occurred while trying to upload files. The HTTP status returned was {0}.")
   public static final String UPLOAD_NOT_SUCCESSFUL = "12";

   @RBEntry("An error occurred while setting your intent to change '{0}': {1}.")
   public static final String SET_INTENT_TO_CHANGE_FAILED = "13";

   @RBEntry("An error occurred while saving: {0}.")
   public static final String SAVE_FAILED = "14";

   @RBEntry("An error occurred trying to create a new file: {0}. Please contact your System Administrator.")
   public static final String CREATE_APPLICATION_DATA_FAILED = "15";

   @RBEntry("The given URL, '{0}', could not be opened.  Please verify that the URL is valid.")
   public static final String MALFORMED_URL_EXCEPTION = "16";

   @RBEntry("The format of file '{0}' cannot be empty.  Please select a valid format.")
   public static final String FILE_FORMAT_NOT_GIVEN = "17";

   @RBEntry("An error occurred while downloading file {0}: {1}.")
   public static final String DOWNLOAD_FILE_FAILED = "18";
}
