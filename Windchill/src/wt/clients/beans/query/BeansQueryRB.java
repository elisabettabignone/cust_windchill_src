/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.beans.query;

import wt.util.resource.*;

@RBUUID("wt.clients.beans.query.BeansQueryRB")
public final class BeansQueryRB extends WTListResourceBundle {
   /**
    * FIELD LABELS
    **/
   @RBEntry("---")
   @RBPseudo(false)
   @RBComment("The string to display of an attribute does not have a value.")
   public static final String ATTRIBUTE_NOT_FOUND_STRING = "40";

   /**
    * BUTTON LABELS
    **/
   @RBEntry("Find")
   public static final String FIND_BUTTON_LABEL = "0";

   @RBEntry("Stop")
   public static final String STOP_BUTTON_LABEL = "1";

   @RBEntry("Clear")
   public static final String CLEAR_BUTTON_LABEL = "2";

   @RBEntry("OK")
   public static final String OK_BUTTON_LABEL = "3";

   @RBEntry("Cancel")
   public static final String QUIT_BUTTON_LABEL = "4";

   @RBEntry("Help")
   public static final String HELP_BUTTON_LABEL = "5";

   @RBEntry("Browse...")
   public static final String BROWSE_BUTTON_LABEL = "15";

   @RBEntry("Append Results")
   public static final String MORE_CHECKBOX = "6";

   @RBEntry("on")
   public static final String TIME_CONSTRAINT_ON = "8";

   @RBEntry("before")
   public static final String TIME_CONSTRAINT_BEFORE = "9";

   @RBEntry("after")
   public static final String TIME_CONSTRAINT_AFTER = "10";

   @RBEntry("between")
   public static final String TIME_CONSTRAINT_BETWEEN = "11";

   @RBEntry("false")
   public static final String FALSE_LABEL = "16";

   @RBEntry("true")
   public static final String TRUE_LABEL = "17";

   @RBEntry(" -to- ")
   public static final String RANGE_TO_LABEL = "12";

   @RBEntry("Not")
   public static final String NOT_LABEL = "14";

   /**
    * MULTILIST HEADINGS
    * SYMBOLS
    **/
   @RBEntry(":")
   @RBPseudo(false)
   public static final String LABEL_SUFFIX = "13";

   /**
    * TITLES
    **/
   @RBEntry("Windchill Search")
   @RBComment("The title for the WTQuery and WTChooser panels.")
   public static final String QUERY_TITLE = "7";

   @RBEntry("Browse for {0}")
   @RBArgComment0("refers to the name of the Search Panel")
   public static final String BROWSE_FOR_TITLE = "18";

   /**
    * ERROR MESSAGES
    **/
   @RBEntry("Invalid Criteria Specified for {0}")
   @RBArgComment0("refers to the name of the attribute")
   public static final String INVALID_CRITERIA = "20";

   @RBEntry("Invalid Schema Specified")
   public static final String INVALID_SCHEMA = "21";

   @RBEntry("The following error occurred initializing images: {0}")
   @RBArgComment0("refers to the stack trace")
   public static final String INITIALIZING_IMAGES_ERROR = "27";

   @RBEntry("Problem loading image: {0}")
   @RBArgComment0("refers to the name of the image being loaded")
   public static final String LOADING_IMAGE_ERROR = "28";

   @RBEntry("A property descriptor for attribute {0} could not be found.  Verify that this attribute name is correct.")
   @RBArgComment0("refers to the name of the attribute that a property descriptor is not available for.")
   public static final String ATTRIBUTE_NOT_FOUND_ERROR = "36";

   @RBEntry("WTSchema::setSchema(aValue) Format Error[{0}] at position={1}\nString Must look like C:wt.part.WTPart; G:Date & Time; A:timeStamp")
   @RBArgComment0("refers to String containing an erroneous value.")
   @RBArgComment1("refers to the position within the above string that is in error.")
   public static final String INVALID_SCHEMA_TYPE = "39";

   @RBEntry("The following error ocurred when processing the search: {0}")
   @RBArgComment0("refers to the error message from the server.")
   public static final String PROCESSING_SEARCH_ERROR = "45";

   /**
    * STATUS MESSAGES
    **/
   @RBEntry("Processing search criteria...")
   public static final String PROCESSING_ATTRIBUTES_STATUS = "19";

   @RBEntry("Searching...")
   public static final String SEARCHING_STATUS = "22";

   @RBEntry(" found")
   @RBComment("DEPRECATED. This value has been left for compatibility issue. Use the OBJECT_FOUND_SINGLE or OBJECT_FOUND_PLURAL instead.")
   @RBArgComment0("refers to the number of objects found")
   @RBArgComment1("refers to the Class Name of objects found")
   @RBArgComment2("refers to either FOUND_STATUS or FOUND_PLURAL_STATUS depending on the number of objects found.")
   public static final String FOUND_STATUS = "23";

   @RBEntry("s found")
   @RBComment("DEPRECATED. This value has been left for compatibility issue. Use the OBJECT_FOUND_SINGLE or OBJECT_FOUND_PLURAL instead.")
   public static final String FOUND_PLURAL_STATUS = "24";

   @RBEntry("{0} {1}{2}")
   @RBComment("DEPRECATED. This value has been left for compatibility issue. Use the OBJECT_FOUND_SINGLE or OBJECT_FOUND_PLURAL instead.")
   public static final String SEARCH_COUNT_STATUS = "25";

   @RBEntry("{0} additional {1}{2}")
   @RBComment("DEPRECATED. This value has been left for compatibility issue. Use the OBJECT_FOUND_SINGLE or OBJECT_FOUND_PLURAL instead.")
   public static final String ADDITIONAL_SEARCH_COUNT_STATUS = "26";

   /**
    * DEVELOPMENT TIME MESSAGES
    **/
   @RBEntry("Available Classes")
   public static final String AVAILABLE_CLASSES_HEADING = "29";

   @RBEntry("Class Attributes")
   public static final String CLASS_ATTRIBUTES_HEADING = "30";

   @RBEntry("Display Name")
   public static final String CLASS_ATTRIBUTES_DISPLAY_NAME_HEADING = "31";

   @RBEntry("Selected Class")
   public static final String SELECTED_CLASS_HEADING = "32";

   @RBEntry("Cannot find information for class: {0}")
   @RBArgComment0("refers to the class name that is not found.")
   public static final String CLASS_NAME_NOT_FOUND = "33";

   @RBEntry("Property descriptors for the given attributes could not be found.")
   public static final String ATTRIBUTES_NOT_FOUND_ERROR = "35";

   @RBEntry("Find Queryable Classes")
   public static final String FIND_CLASSES = "37";

   @RBEntry("Windchill Dialog")
   public static final String DEFAULT_CHOOSER_TITLE = "34";

   @RBEntry("Windchill Query Schema Editor")
   public static final String SCHEMA_EDITOR_TITLE = "38";

   @RBEntry("Query Group(s)")
   public static final String QUERY_GROUPS_LABEL = "41";

   @RBEntry("Query Attribute(s)")
   public static final String QUERY_ATTRIBUTES_LABEL = "42";

   @RBEntry("Edit Group:")
   public static final String EDIT_GROUP = "43";

   @RBEntry("No Class Selected")
   public static final String NO_CLASS_SELECTED = "44";

   @RBEntry("Attribute Name")
   public static final String CLASS_ATTRIBUTES_NAME_HEADING = "46";

   @RBEntry("equals")
   public static final String NUMBER_CONSTRAINT_EQUAL = "47";

   @RBEntry("less than")
   public static final String NUMBER_CONSTRAINT_LESS_THAN = "48";

   @RBEntry("greater than")
   public static final String NUMBER_CONSTRAINT_GREATER_THAN = "49";

   @RBEntry("Search On:")
   public static final String SEARCH_ON = "50";

   @RBEntry("Windchill Part Query Applet")
   public static final String APPLET_TITLE = "51";

   @RBEntry("The source.")
   public static final String APPLET_LINK_TO_SOURCE = "52";

   @RBEntry("Product Information Explorer")
   public static final String APPLETQUERY_TITLE = "53";

   @RBEntry("Product Information Search")
   public static final String APPLETQUERY_PRODUCT_INFO_SEARCH = "54";

   @RBEntry("Keyword:")
   @RBComment("Text label for the keyword field for entering search criteria for RetrievalWare search.")
   public static final String KEYWORD_LABEL = "55";

   @RBEntry("Error searching with the keyword or the content search engine.")
   public static final String CONTENT_ERROR = "56";

   @RBEntry("Object found: {0}")
   @RBComment("This constant is used to relate the number of object that has been found.")
   @RBArgComment0("refers to the number of objects found (0 or 1)")
   public static final String OBJECT_FOUND_SINGLE = "57";

   @RBEntry("Objects found: {0}")
   @RBComment("This constant is used to relate the number of object that has been found.")
   @RBArgComment0("refers to the number of objects found (more than 1)")
   public static final String OBJECT_FOUND_PLURAL = "58";

   @RBEntry("Additioanl object found: {0}")
   @RBComment("This constant is used to relate the number of object that has been found.")
   @RBArgComment0("refers to the number of additional object found (0 or 1)")
   public static final String ADDITIONAL_OBJECT_FOUND_SINGLE = "59";

   @RBEntry("Additional objects found: {0}")
   @RBComment("This constant is used to relate the number of object that has been found.")
   @RBArgComment0("refers to the number of objects found (more than 1)")
   public static final String ADDITIONAL_OBJECT_FOUND_PLURAL = "60";

   @RBEntry("Search Criteria")
   @RBComment("This constant is used to define the labels for the tabs on the search applet client.")
   public static final String SEARCH_CRITERIA = "61";

   @RBEntry("More Search Criteria")
   @RBComment("This constant is used to define the labels for the tabs on the search applet client.")
   public static final String MORE_SEARCH_CRITERIA = "62";

   @RBEntry("Applet must be passed a WTContainerRef.")
   @RBComment("Error message when no container is found in search picker.")
   public static final String CONTAINER_NOT_FOUND = "63";
}
