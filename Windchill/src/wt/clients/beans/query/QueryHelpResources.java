/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.beans.query;

import wt.util.resource.*;

@RBUUID("wt.clients.beans.query.QueryHelpResources")
@RBNameException //Grandfathered by conversion
public final class QueryHelpResources extends WTListResourceBundle {
   @RBEntry("ExportImportExpBaselineSearch")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_0 = "Help/query/QueryHelp";

   @RBEntry("Search for an object using specified search criteria")
   public static final String PRIVATE_CONSTANT_1 = "Desc/query//Find";

   @RBEntry("Stop the current search operation")
   public static final String PRIVATE_CONSTANT_2 = "Desc/query//Stop";

   @RBEntry("Clear the list area")
   public static final String PRIVATE_CONSTANT_3 = "Desc/query//Clear";

   @RBEntry("Close this selection task")
   public static final String PRIVATE_CONSTANT_4 = "Desc/query//Close";

   @RBEntry("Click on this after selecting one or more objects")
   public static final String PRIVATE_CONSTANT_5 = "Desc/query//OK";

   @RBEntry("Append to the current search list")
   public static final String PRIVATE_CONSTANT_6 = "Desc/query//More";

   @RBEntry("Access online Help")
   public static final String PRIVATE_CONSTANT_7 = "Desc/query//Help";
}
