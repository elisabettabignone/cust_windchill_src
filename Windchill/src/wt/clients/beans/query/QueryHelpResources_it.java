/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.beans.query;

import wt.util.resource.*;

@RBUUID("wt.clients.beans.query.QueryHelpResources")
@RBNameException //Grandfathered by conversion
public final class QueryHelpResources_it extends WTListResourceBundle {
   @RBEntry("ExportImportExpBaselineSearch")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_0 = "Help/query/QueryHelp";

   @RBEntry("Effettua la ricerca di un oggetto utilizzando i criteri di ricerca specificati")
   public static final String PRIVATE_CONSTANT_1 = "Desc/query//Find";

   @RBEntry("Arresta l'operazione di ricerca corrente")
   public static final String PRIVATE_CONSTANT_2 = "Desc/query//Stop";

   @RBEntry("Cancella il contenuto dell'area dell'elenco")
   public static final String PRIVATE_CONSTANT_3 = "Desc/query//Clear";

   @RBEntry("Termina il task di selezione corrente")
   public static final String PRIVATE_CONSTANT_4 = "Desc/query//Close";

   @RBEntry("Conferma la selezione di uno o più oggetti")
   public static final String PRIVATE_CONSTANT_5 = "Desc/query//OK";

   @RBEntry("Aggiunge altri elementi all'elenco della ricerca corrente")
   public static final String PRIVATE_CONSTANT_6 = "Desc/query//More";

   @RBEntry("Visualizza la Guida in linea")
   public static final String PRIVATE_CONSTANT_7 = "Desc/query//Help";
}
