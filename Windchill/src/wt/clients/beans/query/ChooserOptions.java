// $Header: /Windchill/current/wt/clients/beans/query/ChooserOptions.java 19    12/17/98 3:53p Tjb $

/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.beans.query;
import java.util.ListResourceBundle;

public class ChooserOptions extends ListResourceBundle{
   public static final String CHANGEABLE_SEARCH_LIST   = "1";
   public static final String DESCRIBED_BY_SEARCH_LIST = "2";
   public static final String EFF_CONTEXT_SEARCH_LIST  = "3";
   public static final String DOC_STRUCT_SEARCH_LIST   = "4";
   public static final String DOC_DEPEND_SEARCH_LIST   = "5";
   public static final String PIE_OPEN_SEARCH_LIST     = "6";
   public static final String PRODUCT_USES_LIST        = "7";
   public static final String PART_USES_LIST           = "8";

   public Object getContents()[][] {
      return contents;
   }

   public Object getFilters()[][] {
      return filters;
   }

   private final static String versionID = "$Header: /Windchill/current/wt/clients/beans/query/ChooserOptions.java 19    12/17/98 3:53p Tjb $";
   /**
    * LOCALIZATION Instructions:
    * This file uses the contents array in a slightly unique manner.  As a
    * result only parts of the text can be localized.   The second member of
    * each row in the contents array is a string.  Only the text following "K:"
    * and up to the ";" can be localized within this string.  The localization
    * value for this portion of the string must not contain a colon (":") or
    * semi-colon (";"). For example in the following string
    * "C:wt.org.WTUser; K:61; A:name; A:fullName;"
    * the "61" can be localized.  The rest of the string must remain
    * intact.
    */
   static final Object[][]contents = {

      {"UsrSCM", "C:wt.org.WTUser; K:61; A:name; A:fullName; " +
           "A:authenticationName"},
      //{"GrpSCM", "C:wt.org.WTGroup; K:61; A:name; " +
      //      "A:description; K:Advance Criteria; A:adminDomainName; " +
      //      "A:modifyTimestamp"},
      //{"PrnSCM", "C:wt.org.WTPrincipalReference; K:61; A:name"},

	  //Added a filter to remove items in Projects to a number of object types below
      {"DocSCM", "C:wt.doc.WTDocument; K:61; A:number; " +
            "A:name; A:docType; A:versionIdentifier; A:lifeCycleState; A:teamTemplateId; K:62; " +
            "A:cabinet; A:modifyTimestamp; D:organizationReference; D:organizationUniqueIdentifier; D:formatName; A:format;"},

      {"DocMasterSCM", "C:wt.doc.WTDocumentMaster; K:61; " +
            "A:name; A:number; A:organizationReference; D:organizationUniqueIdentifier;"},

      {"CabSCM", "C:wt.folder.CabinetReference; K:61; A:name"},

      {"PartSCM", "C:wt.part.WTPart; K:61; A:number; A:name; A:view; " +
            "A:versionIdentifier; A:partType; A:source; K:62; " +
            "A:lifeCycleState; A:teamTemplateId; A:cabinet; A:modifyTimestamp; D:organizationReference; D:organizationUniqueIdentifier;"},

      {"PartMasterSCM", "C:wt.part.WTPartMaster; K:61; " +
            "A:number; A:name; A:organizationReference; D:organizationUniqueIdentifier;"},

      //New lines added for R2 - localize only the strings between K: and ;
      //See localization instructions at top of file for more specific instructions
      {"ConfigItemSCM", "C:wt.effectivity.ConfigurationItem; K:61; " +
            "A:name; A:effectivityType; A:lifeCycleName; A:lifeCycleState; K:62; " +
            "A:lifeCycleAtGate; " +
            "A:modifyTimestamp; A:createTimestamp; A:description"},

      {"ProductInstanceSCM", "C:wt.effectivity.WTProductInstance; K:61; " +
            "A:configItemName; A:serialNumber; A:buildDate; K:62; " +
            "A:modifyTimestamp; A:createTimestamp; A:configItemDescription"},
      {CHANGEABLE_SEARCH_LIST, "wt.doc.WTDocument wt.part.WTPart"},
      { DESCRIBED_BY_SEARCH_LIST, "wt.doc.WTDocument"},
      //{ EFF_CONTEXT_SEARCH_LIST, "wt.effectivity.ConfigurationItem wt.part.WTProductMaster"},

      // <TRANSLATE>
      {"BaselineSCM", "C:wt.vc.baseline.ManagedBaseline; K:61; " +
            "A:number; A:name; A:lifeCycleState; A:teamTemplateId; K:62; " +
            "A:cabinet; A:modifyTimestamp; A:description"},

      // <TRANSLATE>
      {"LifeCycleTemplateMasterSCM", "C:wt.lifecycle.LifeCycleTemplateMaster; K:61; " +
            "A:name; A:description; D:containerReference"},

      // <TRANSLATE>
      {"WfProcessTemplateMasterSCM", "C:wt.workflow.definer.WfProcessTemplateMaster; K:61; " +
            "A:name; A:description; A:enabled"},

      // <TRANSLATE>
      {"BusinessEntitySCM", "C:wt.csm.businessentity.BusinessEntity; K:61; " +
            "A:name"},

      // <TRANSLATE>
      {"WTOrganizationSCM", "C:wt.org.WTOrganization; K:61; " +
            "A:name"},


      {"TeamTemplateSCM", "C:wt.team.TeamTemplate; K:61; " +
            "A:name; A:cabinet; A:description; D:containerReference"},

      {DOC_STRUCT_SEARCH_LIST, "wt.doc.WTDocumentMaster"},
      {DOC_DEPEND_SEARCH_LIST, "wt.doc.WTDocument"},

      {ChooserOptions.PIE_OPEN_SEARCH_LIST, "wt.part.WTPartMaster wt.part.WTProductConfiguration wt.part.WTProductInstance2 "},

      {"WTProductConfigurationSCM", "C:wt.part.WTProductConfiguration; " +
                                    "K:61; " +
                                    "A:configurationName; A:productNumber; A:productName;"},

      {"WTProductInstance2SCM", "C:wt.part.WTProductInstance2; " +
                                "K:61; " +
                                "A:serialNumber; A:versionIdentifier; A:productNumber; A:productName; D:organizationReference; D:organizationUniqueIdentifier;"},


      {ChooserOptions.PRODUCT_USES_LIST,  "wt.part.WTPartMaster"},
      {ChooserOptions.PART_USES_LIST,     "wt.part.WTPartMaster"},
   };

    //the labels at the front of this array should match the one above to filter out the containers listed
    static final Object [][]filters = {

		{"DocSCM", "wt.inf.library.WTLibrary"},

		{"DocMasterSCM", "wt.inf.library.WTLibrary"},

		{"PartSCM", "wt.inf.library.WTLibrary"},

		{"PartMasterSCM", "wt.inf.library.WTLibrary"},

	   	{"BaselineSCM", "wt.inf.library.WTLibrary"},

		{"LifeCycleTemplateMasterSCM", "wt.inf.library.WTLibrary,wt.inf.container.ExchangeContainer,wt.pdmlink.PDMLinkProduct,wt.inf.container.OrgContainer"},

		{"WfProcessTemplateMasterSCM", "wt.inf.library.WTLibrary"},

		{"TeamTemplateSCM", "wt.inf.library.WTLibrary,wt.inf.container.ExchangeContainer,wt.pdmlink.PDMLinkProduct,wt.inf.container.OrgContainer"},

		{"WTProductConfigurationSCM", "wt.inf.library.WTLibrary"},

		{"WTProductInstance2SCM", "wt.inf.library.WTLibrary"},

   };
}
