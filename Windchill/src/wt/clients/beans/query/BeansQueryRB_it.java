/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.beans.query;

import wt.util.resource.*;

@RBUUID("wt.clients.beans.query.BeansQueryRB")
public final class BeansQueryRB_it extends WTListResourceBundle {
   /**
    * FIELD LABELS
    **/
   @RBEntry("---")
   @RBPseudo(false)
   @RBComment("The string to display of an attribute does not have a value.")
   public static final String ATTRIBUTE_NOT_FOUND_STRING = "40";

   /**
    * BUTTON LABELS
    **/
   @RBEntry("Trova")
   public static final String FIND_BUTTON_LABEL = "0";

   @RBEntry("Arresta")
   public static final String STOP_BUTTON_LABEL = "1";

   @RBEntry("Reimposta")
   public static final String CLEAR_BUTTON_LABEL = "2";

   @RBEntry("OK")
   public static final String OK_BUTTON_LABEL = "3";

   @RBEntry("Annulla")
   public static final String QUIT_BUTTON_LABEL = "4";

   @RBEntry("Guida")
   public static final String HELP_BUTTON_LABEL = "5";

   @RBEntry("Sfoglia...")
   public static final String BROWSE_BUTTON_LABEL = "15";

   @RBEntry("Aggiungi risultati")
   public static final String MORE_CHECKBOX = "6";

   @RBEntry("il")
   public static final String TIME_CONSTRAINT_ON = "8";

   @RBEntry("prima")
   public static final String TIME_CONSTRAINT_BEFORE = "9";

   @RBEntry("dopo")
   public static final String TIME_CONSTRAINT_AFTER = "10";

   @RBEntry("tra")
   public static final String TIME_CONSTRAINT_BETWEEN = "11";

   @RBEntry("false")
   public static final String FALSE_LABEL = "16";

   @RBEntry("true")
   public static final String TRUE_LABEL = "17";

   @RBEntry(" -e- ")
   public static final String RANGE_TO_LABEL = "12";

   @RBEntry("non")
   public static final String NOT_LABEL = "14";

   /**
    * MULTILIST HEADINGS
    * SYMBOLS
    **/
   @RBEntry(":")
   @RBPseudo(false)
   public static final String LABEL_SUFFIX = "13";

   /**
    * TITLES
    **/
   @RBEntry("Ricerca Windchill")
   @RBComment("The title for the WTQuery and WTChooser panels.")
   public static final String QUERY_TITLE = "7";

   @RBEntry("Cerca {0}")
   @RBArgComment0("refers to the name of the Search Panel")
   public static final String BROWSE_FOR_TITLE = "18";

   /**
    * ERROR MESSAGES
    **/
   @RBEntry("Criteri specificati non validi per {0}")
   @RBArgComment0("refers to the name of the attribute")
   public static final String INVALID_CRITERIA = "20";

   @RBEntry("Schema specificato non valido")
   public static final String INVALID_SCHEMA = "21";

   @RBEntry("Si è verificato l'errore seguente durante l'inizializzazione delle immagini: {0}")
   @RBArgComment0("refers to the stack trace")
   public static final String INITIALIZING_IMAGES_ERROR = "27";

   @RBEntry("Si è verificato un problema durante il caricamento dell'immagine: {0}")
   @RBArgComment0("refers to the name of the image being loaded")
   public static final String LOADING_IMAGE_ERROR = "28";

   @RBEntry("Impossibile trovare il descrittore di proprietà per l'attributo {0}. Verificare che il nome dell'attributo sia corretto.")
   @RBArgComment0("refers to the name of the attribute that a property descriptor is not available for.")
   public static final String ATTRIBUTE_NOT_FOUND_ERROR = "36";

   @RBEntry("WTSchema::setSchema(aValue) Errore di formato[{0}] alla posizione={1}\nIl formato della stringa deve essere C:wt.part.WTPart; G:Data e ora; A:timeStamp")
   @RBArgComment0("refers to String containing an erroneous value.")
   @RBArgComment1("refers to the position within the above string that is in error.")
   public static final String INVALID_SCHEMA_TYPE = "39";

   @RBEntry("Si è verificato il seguente errore durante la ricerca: {0}")
   @RBArgComment0("refers to the error message from the server.")
   public static final String PROCESSING_SEARCH_ERROR = "45";

   /**
    * STATUS MESSAGES
    **/
   @RBEntry("Esame dei criteri di ricerca in corso...")
   public static final String PROCESSING_ATTRIBUTES_STATUS = "19";

   @RBEntry("Ricerca in corso...")
   public static final String SEARCHING_STATUS = "22";

   @RBEntry(" trovato")
   @RBComment("DEPRECATED. This value has been left for compatibility issue. Use the OBJECT_FOUND_SINGLE or OBJECT_FOUND_PLURAL instead.")
   @RBArgComment0("refers to the number of objects found")
   @RBArgComment1("refers to the Class Name of objects found")
   @RBArgComment2("refers to either FOUND_STATUS or FOUND_PLURAL_STATUS depending on the number of objects found.")
   public static final String FOUND_STATUS = "23";

   @RBEntry("trovati/e")
   @RBComment("DEPRECATED. This value has been left for compatibility issue. Use the OBJECT_FOUND_SINGLE or OBJECT_FOUND_PLURAL instead.")
   public static final String FOUND_PLURAL_STATUS = "24";

   @RBEntry("{0} {1}{2}")
   @RBComment("DEPRECATED. This value has been left for compatibility issue. Use the OBJECT_FOUND_SINGLE or OBJECT_FOUND_PLURAL instead.")
   public static final String SEARCH_COUNT_STATUS = "25";

   @RBEntry("{0} altri/e {1}{2}")
   @RBComment("DEPRECATED. This value has been left for compatibility issue. Use the OBJECT_FOUND_SINGLE or OBJECT_FOUND_PLURAL instead.")
   public static final String ADDITIONAL_SEARCH_COUNT_STATUS = "26";

   /**
    * DEVELOPMENT TIME MESSAGES
    **/
   @RBEntry("Classi disponibili")
   public static final String AVAILABLE_CLASSES_HEADING = "29";

   @RBEntry("Attributi della classe")
   public static final String CLASS_ATTRIBUTES_HEADING = "30";

   @RBEntry("Nome visualizzato")
   public static final String CLASS_ATTRIBUTES_DISPLAY_NAME_HEADING = "31";

   @RBEntry("Classe selezionata")
   public static final String SELECTED_CLASS_HEADING = "32";

   @RBEntry("Impossibile trovare informazioni per la classe: {0}")
   @RBArgComment0("refers to the class name that is not found.")
   public static final String CLASS_NAME_NOT_FOUND = "33";

   @RBEntry("Impossibile trovare i descrittori di proprietà per gli attributi indicati.")
   public static final String ATTRIBUTES_NOT_FOUND_ERROR = "35";

   @RBEntry("Trova classi interrogabili")
   public static final String FIND_CLASSES = "37";

   @RBEntry("Finestra di dialogo Windchill")
   public static final String DEFAULT_CHOOSER_TITLE = "34";

   @RBEntry("Editor schema di interrogazione Windchill")
   public static final String SCHEMA_EDITOR_TITLE = "38";

   @RBEntry("Gruppo/i di interrogazione")
   public static final String QUERY_GROUPS_LABEL = "41";

   @RBEntry("Attributo/i di interrogazione")
   public static final String QUERY_ATTRIBUTES_LABEL = "42";

   @RBEntry("Modifica gruppo:")
   public static final String EDIT_GROUP = "43";

   @RBEntry("Nessuna classe selezionata")
   public static final String NO_CLASS_SELECTED = "44";

   @RBEntry("Nome attributo")
   public static final String CLASS_ATTRIBUTES_NAME_HEADING = "46";

   @RBEntry("uguale a")
   public static final String NUMBER_CONSTRAINT_EQUAL = "47";

   @RBEntry("minore di")
   public static final String NUMBER_CONSTRAINT_LESS_THAN = "48";

   @RBEntry("maggiore di")
   public static final String NUMBER_CONSTRAINT_GREATER_THAN = "49";

   @RBEntry("Chiave di ricerca:")
   public static final String SEARCH_ON = "50";

   @RBEntry("Applet d'interrogazione parti Windchill")
   public static final String APPLET_TITLE = "51";

   @RBEntry("Sorgente")
   public static final String APPLET_LINK_TO_SOURCE = "52";

   @RBEntry("Navigatore dati di prodotto")
   public static final String APPLETQUERY_TITLE = "53";

   @RBEntry("Ricerca dei dati di prodotto")
   public static final String APPLETQUERY_PRODUCT_INFO_SEARCH = "54";

   @RBEntry("Parola chiave:")
   @RBComment("Text label for the keyword field for entering search criteria for RetrievalWare search.")
   public static final String KEYWORD_LABEL = "55";

   @RBEntry("Errore durante la ricerca con parola chiave o motore di ricerca dati")
   public static final String CONTENT_ERROR = "56";

   @RBEntry("Oggetto trovato: {0}")
   @RBComment("This constant is used to relate the number of object that has been found.")
   @RBArgComment0("refers to the number of objects found (0 or 1)")
   public static final String OBJECT_FOUND_SINGLE = "57";

   @RBEntry("Oggetti trovati: {0}")
   @RBComment("This constant is used to relate the number of object that has been found.")
   @RBArgComment0("refers to the number of objects found (more than 1)")
   public static final String OBJECT_FOUND_PLURAL = "58";

   @RBEntry("Altro oggetto trovato: {0}")
   @RBComment("This constant is used to relate the number of object that has been found.")
   @RBArgComment0("refers to the number of additional object found (0 or 1)")
   public static final String ADDITIONAL_OBJECT_FOUND_SINGLE = "59";

   @RBEntry("Altri oggetti trovati: {0}")
   @RBComment("This constant is used to relate the number of object that has been found.")
   @RBArgComment0("refers to the number of objects found (more than 1)")
   public static final String ADDITIONAL_OBJECT_FOUND_PLURAL = "60";

   @RBEntry("Criteri di ricerca")
   @RBComment("This constant is used to define the labels for the tabs on the search applet client.")
   public static final String SEARCH_CRITERIA = "61";

   @RBEntry("Altri criteri di ricerca")
   @RBComment("This constant is used to define the labels for the tabs on the search applet client.")
   public static final String MORE_SEARCH_CRITERIA = "62";

   @RBEntry("È necessario trasferire l'applet in WTContainerRef.")
   @RBComment("Error message when no container is found in search picker.")
   public static final String CONTAINER_NOT_FOUND = "63";
}
