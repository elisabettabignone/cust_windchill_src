/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.beans;

import wt.util.resource.*;

@RBUUID("wt.clients.beans.RapidFindRB")
public final class RapidFindRB_it extends WTListResourceBundle {
   /**
    * BUTTON LABELS
    **/
   @RBEntry("Sfoglia...")
   public static final String BROWSE_BUTTON_LABEL = "1";

   @RBEntry("Recupera")
   public static final String RETRIEVE_BUTTON_LABEL = "2";

   @RBEntry("Arresta")
   public static final String STOP_BUTTON_LABEL = "7";

   /**
    * FIELD LABELS
    **/
   @RBEntry("Chiave di ricerca:")
   public static final String CLASS_CHOICE_LABEL = "6";

   @RBEntry("Numero:")
   public static final String NUMBER_LABEL = "3";

   @RBEntry("Versione:")
   public static final String VERSION_LABEL = "4";

   @RBEntry("Ultima versione")
   public static final String GET_LATEST_VERSION_LABEL = "5";
   
   @RBEntry("Trova")
   public static final String TARGET_OBJECT_BROWSE_TITLE = "TARGET_OBJECT_BROWSE_TITLE";
}
