/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.beans;

import wt.util.resource.*;

@RBUUID("wt.clients.beans.BeansHelpRB")
public final class BeansHelpRB_it extends WTListResourceBundle {
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_0 = "Contents/beans";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_1 = "Help/beans";

   /**
    * ------Associations Panel-----
    **/
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_2 = "Help/beans/AssociationsPanel";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_3 = "Desc/beans/AssociationsPanel";

   @RBEntry("Fare clic per aggiungere un'associazione")
   public static final String PRIVATE_CONSTANT_4 = "Desc/beans/AssociationsPanel/Add";

   @RBEntry("Fare clic per rimuovere l'oggetto associato")
   public static final String PRIVATE_CONSTANT_5 = "Desc/beans/AssociationsPanel/Remove";

   @RBEntry("Fare clic per visualizzare l'oggetto associato")
   public static final String PRIVATE_CONSTANT_6 = "Desc/beans/AssociationsPanel/View";

   @RBEntry("Visualizza gli oggetti associati")
   public static final String PRIVATE_CONSTANT_7 = "Desc/beans/AssociationsPanel/MultiList";

   /**
    * -------Effectivity Panel-----------
    **/
   @RBEntry("Immettere o cercare un configuration item. Il campo è facoltativo")
   public static final String PRIVATE_CONSTANT_8 = "Desc/beans/EffectivityPanel/ConfigItem";

   @RBEntry("Fare clic per cercare un configuration item")
   public static final String PRIVATE_CONSTANT_9 = "Desc/beans/EffectivityPanel/BrowseConfigItem";

   @RBEntry("Immettere la data d'inizio o il numero per l'effettività")
   public static final String PRIVATE_CONSTANT_10 = "Desc/beans/EffectivityPanel/StartEffectivity";

   @RBEntry("Immettere la data di fine o il numero per l'effettività")
   public static final String PRIVATE_CONSTANT_11 = "Desc/beans/EffectivityPanel/EndEffectivity";

   /**
    * -------UserSelectorPanel-----------
    **/
   @RBEntry("Immettere un nome utente.")
   public static final String PRIVATE_CONSTANT_12 = "Desc/beans/UserSelectorPanel/UserName";

   @RBEntry("Individuare tutti i nomi utente.")
   public static final String PRIVATE_CONSTANT_13 = "Desc/beans/UserSelectorPanel/SearchButton";

   @RBEntry("Selezionare un gruppo di utenti.")
   public static final String PRIVATE_CONSTANT_14 = "Desc/beans/UserSelectorPanel/GroupChoice";

   @RBEntry("Selezionare un nome utente e premere OK.")
   public static final String PRIVATE_CONSTANT_15 = "Desc/beans/UserSelectorPanel/UserList";

   /**
    * -------------------------------------------------------------------
    * Folder Panel
    * -------------------------------------------------------------------
    **/
   @RBEntry("Visualizza la posizione della cartella")
   public static final String PRIVATE_CONSTANT_16 = "Desc/beans/FolderPanel/ViewLocation";

   @RBEntry("Specificare la posizione di una cartella")
   public static final String PRIVATE_CONSTANT_17 = "Desc/beans/FolderPanel/UpdateLocation";

   @RBEntry("Fare clic per cercare una cartella")
   public static final String PRIVATE_CONSTANT_18 = "Desc/beans/FolderPanel/Browse";

   /**
    * -----PrincipalSelectionBrowser-----
    **/
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_19 = "Help/beans/PrincipalSelectionBrowser";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_20 = "Desc/beans/PrincipalSelectionBrowser";

   @RBEntry("Visualizza l'elenco dei gruppi disponibili.")
   public static final String PRIVATE_CONSTANT_21 = "Desc/beans/PrincipalSelectionBrowser/groupList";

   @RBEntry("Visualizza l'elenco degli utenti disponibili.")
   public static final String PRIVATE_CONSTANT_22 = "Desc/beans/PrincipalSelectionBrowser/userList";

   @RBEntry("Specificare i criteri di ricerca per l'utente.")
   public static final String PRIVATE_CONSTANT_23 = "Desc/beans/PrincipalSelectionBrowser/userTextField";

   @RBEntry("Fare clic su questo pulsante per effettuare la ricerca degli utenti.")
   public static final String PRIVATE_CONSTANT_24 = "Desc/beans/PrincipalSelectionBrowser/userFindButton";

   @RBEntry("Selezionare un gruppo dalla finestra di scelta per visualizzare un elenco di tutti gli utenti del gruppo.")
   public static final String PRIVATE_CONSTANT_25 = "Desc/beans/PrincipalSelectionBrowser/groupChoice";

   @RBEntry("Visualizza l'elenco degli attori disponibili.")
   public static final String PRIVATE_CONSTANT_26 = "Desc/beans/PrincipalSelectionBrowser/actorList";

   @RBEntry("Visualizza l'elenco dei ruoli disponibili.")
   public static final String PRIVATE_CONSTANT_27 = "Desc/beans/PrincipalSelectionBrowser/roleList";

   @RBEntry("Aggiunge l'elemento selezionato nella scheda selezionata all'elenco dei partecipanti.")
   public static final String PRIVATE_CONSTANT_28 = "Desc/beans/PrincipalSelectionBrowser/addButton";

   @RBEntry("Aggiunge tutti gli elementi nella scheda selezionata all'elenco dei partecipanti.")
   public static final String PRIVATE_CONSTANT_29 = "Desc/beans/PrincipalSelectionBrowser/addAllButton";

   @RBEntry("Rimuove l'elemento selezionato dall'elenco dei partecipanti.")
   public static final String PRIVATE_CONSTANT_30 = "Desc/beans/PrincipalSelectionBrowser/removeButton";

   @RBEntry("Rimuove tutti gli elementi dall'elenco dei partecipanti.")
   public static final String PRIVATE_CONSTANT_31 = "Desc/beans/PrincipalSelectionBrowser/removeAllButton";

   @RBEntry("Visualizza l'elenco di tutti i partecipanti selezionati.")
   public static final String PRIVATE_CONSTANT_32 = "Desc/beans/PrincipalSelectionBrowser/selectedParticipantsList";

   /**
    * -----PrincipalSelectionPanel-----
    **/
   @RBEntry("CalFindUserOrg")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_33 = "Help/beans/PrincipalSelectionPanel";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_34 = "Desc/beans/PrincipalSelectionPanel";

   @RBEntry("Fare clic sul pulsante per cercare l'utente/gruppo/ruolo WT desiderato.")
   public static final String PRIVATE_CONSTANT_35 = "Desc/beans/PrincipalSelectionPanel/browseButton";

   @RBEntry("Immettere il nome dell'utente/gruppo/ruolo desiderato nel campo di testo.")
   public static final String PRIVATE_CONSTANT_36 = "Desc/beans/PrincipalSelectionPanel/selectionTextField";

   /**
    * -----FindUserPanel-----
    **/
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_37 = "Help/beans/FindUserPanel";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_38 = "Desc/beans/FindUserPanel";

   @RBEntry("Visualizza l'elenco degli utenti disponibili.")
   public static final String PRIVATE_CONSTANT_39 = "Desc/beans/FindUserPanel/userList";

   @RBEntry("Immettere i criteri di ricerca del nome completo dell'utente in questo campo.")
   public static final String PRIVATE_CONSTANT_40 = "Desc/beans/FindUserPanel/userNameTextField";

   @RBEntry("Immettere i criteri di ricerca dell'id utente in questo campo.")
   public static final String PRIVATE_CONSTANT_41 = "Desc/beans/FindUserPanel/userIdTextField";

   @RBEntry("Fare clic su questo pulsante per effettuare la ricerca degli utenti.")
   public static final String PRIVATE_CONSTANT_42 = "Desc/beans/FindUserPanel/findButton";

   @RBEntry("Selezionare un'origine dalla finestra di scelta per la ricerca degli utenti.")
   public static final String PRIVATE_CONSTANT_43 = "Desc/beans/FindUserPanel/serviceChoice";

   @RBEntry("Selezionare un gruppo dalla finestra di scelta per visualizzare un elenco di tutti gli utenti del gruppo.")
   public static final String PRIVATE_CONSTANT_44 = "Desc/beans/FindUserPanel/groupChoice";

   /**
    * -----FindGroupPanel-----
    **/
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_45 = "Help/beans/FindGroupPanel";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_46 = "Desc/beans/FindGroupPanel";

   @RBEntry("Visualizza l'elenco dei gruppi disponibili.")
   public static final String PRIVATE_CONSTANT_47 = "Desc/beans/FindGroupPanel/groupList";

   @RBEntry("Selezionare un'origine dalla finestra di scelta per la ricerca degli utenti.")
   public static final String PRIVATE_CONSTANT_48 = "Desc/beans/FindGroupPanel/serviceChoice";

   /**
    * -----OrgChooserDialog-----
    **/
   @RBEntry("CalFindUser")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_49 = "Help/beans/OrgChooserDialog";
}
