/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.rn;

import wt.util.resource.*;

@RBUUID("wt.clients.rn.rnResource")
public final class rnResource extends WTListResourceBundle {
   /**
    * ** NEW FOR 6.0
    * ****** entry #'s 5 & 6
    * ****** required
    * ****** locationLabel
    * ****** okButton
    * ****** browseButton
    * ****** ellipses
    * LABELS --------------------------------------------------------------------
    **/
   @RBEntry("Display URL")
   public static final String PRIVATE_CONSTANT_0 = "displayURLLabel";

   @RBEntry("Name")
   public static final String PRIVATE_CONSTANT_1 = "nameLabel";

   @RBEntry("Description")
   public static final String PRIVATE_CONSTANT_2 = "descriptionLabel";

   @RBEntry("Exception Received")
   public static final String PRIVATE_CONSTANT_3 = "exceptionLabel";

   @RBEntry("Receipt Acknowledged")
   public static final String PRIVATE_CONSTANT_4 = "acknowledgementLabel";

   @RBEntry("Response Received")
   public static final String PRIVATE_CONSTANT_5 = "responseLabel";

   @RBEntry("Created By")
   public static final String PRIVATE_CONSTANT_6 = "createdByLabel";

   @RBEntry("Created")
   public static final String PRIVATE_CONSTANT_7 = "createdLabel";

   /**
    * BUTTON LABELS -------------------------------------------------------------
    **/
   @RBEntry("Open")
   public static final String PRIVATE_CONSTANT_8 = "openButton";

   @RBEntry("Cancel")
   public static final String PRIVATE_CONSTANT_9 = "cancelButton";

   @RBEntry("Close")
   public static final String PRIVATE_CONSTANT_10 = "closeButton";

   @RBEntry("Help")
   public static final String PRIVATE_CONSTANT_11 = "helpButton";

   @RBEntry("Save")
   public static final String PRIVATE_CONSTANT_12 = "saveButton";

   /**
    * SYMBOLS -------------------------------------------------------------------
    **/
   @RBEntry("*")
   @RBPseudo(false)
   public static final String PRIVATE_CONSTANT_13 = "required";

   @RBEntry(":")
   @RBPseudo(false)
   public static final String PRIVATE_CONSTANT_14 = "colonLabel";

   /**
    * TITLES --------------------------------------------------------------------
    **/
   @RBEntry("Create RosettaNet Request")
   public static final String PRIVATE_CONSTANT_15 = "createTitle";

   @RBEntry("Update RosettaNet Request")
   public static final String PRIVATE_CONSTANT_16 = "updateTitle";

   @RBEntry("View RosettaNet Request")
   public static final String PRIVATE_CONSTANT_17 = "viewTitle";

   /**
    * STATUS BAR MESSAGES -------------------------------------------------------
    * ERROR MESSAGES ------------------------------------------------------------
    **/
   @RBEntry("\"{0}\" is not a valid URL.")
   public static final String MALFORMED_URL = "0";

   @RBEntry("Changes have not been saved.  Save changes now?")
   public static final String CONFIRM_UNSAVED_CHANGES = "1";

   @RBEntry("Unable to create new RosettaNet request object.")
   public static final String UNABLE_TO_CREATE_WTRNREQUEST = "2";

   @RBEntry("Object is not a RosettaNet request object.")
   public static final String OBJECT_NOT_WTRNREQUEST = "3";

   @RBEntry("Delete object \"{0}\"?")
   public static final String CONFIRM_DELETE = "4";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Please provide a value for \"{0}\".")
   public static final String NULL_ATTRIBUTE_VALUE = "5";

   @RBEntry("The folder in which to create the document must be specified.")
   public static final String PARENT_FOLDER_REQUIRED = "6";

   @RBEntry("Location:")
   public static final String PRIVATE_CONSTANT_18 = "locationLabel";

   @RBEntry("OK")
   public static final String PRIVATE_CONSTANT_19 = "okButton";

   @RBEntry("Browse")
   public static final String PRIVATE_CONSTANT_20 = "browseButton";

   @RBEntry("...")
   @RBPseudo(false)
   public static final String PRIVATE_CONSTANT_21 = "ellipses";
}
