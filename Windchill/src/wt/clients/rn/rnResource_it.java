/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.rn;

import wt.util.resource.*;

@RBUUID("wt.clients.rn.rnResource")
public final class rnResource_it extends WTListResourceBundle {
   /**
    * ** NEW FOR 6.0
    * ****** entry #'s 5 & 6
    * ****** required
    * ****** locationLabel
    * ****** okButton
    * ****** browseButton
    * ****** ellipses
    * LABELS --------------------------------------------------------------------
    **/
   @RBEntry("URL")
   public static final String PRIVATE_CONSTANT_0 = "displayURLLabel";

   @RBEntry("Nome")
   public static final String PRIVATE_CONSTANT_1 = "nameLabel";

   @RBEntry("Descrizione")
   public static final String PRIVATE_CONSTANT_2 = "descriptionLabel";

   @RBEntry("Eccezione ricevuta")
   public static final String PRIVATE_CONSTANT_3 = "exceptionLabel";

   @RBEntry("Ricezione confermata")
   public static final String PRIVATE_CONSTANT_4 = "acknowledgementLabel";

   @RBEntry("Risposta ricevuta")
   public static final String PRIVATE_CONSTANT_5 = "responseLabel";

   @RBEntry("Autore")
   public static final String PRIVATE_CONSTANT_6 = "createdByLabel";

   @RBEntry("Data creazione")
   public static final String PRIVATE_CONSTANT_7 = "createdLabel";

   /**
    * BUTTON LABELS -------------------------------------------------------------
    **/
   @RBEntry("Apri")
   public static final String PRIVATE_CONSTANT_8 = "openButton";

   @RBEntry("Annulla")
   public static final String PRIVATE_CONSTANT_9 = "cancelButton";

   @RBEntry("Chiudi")
   public static final String PRIVATE_CONSTANT_10 = "closeButton";

   @RBEntry("Guida")
   public static final String PRIVATE_CONSTANT_11 = "helpButton";

   @RBEntry("Salva")
   public static final String PRIVATE_CONSTANT_12 = "saveButton";

   /**
    * SYMBOLS -------------------------------------------------------------------
    **/
   @RBEntry("*")
   @RBPseudo(false)
   public static final String PRIVATE_CONSTANT_13 = "required";

   @RBEntry(":")
   @RBPseudo(false)
   public static final String PRIVATE_CONSTANT_14 = "colonLabel";

   /**
    * TITLES --------------------------------------------------------------------
    **/
   @RBEntry("Crea richiesta RosettaNet")
   public static final String PRIVATE_CONSTANT_15 = "createTitle";

   @RBEntry("Aggiorna richiesta RosettaNet")
   public static final String PRIVATE_CONSTANT_16 = "updateTitle";

   @RBEntry("Visualizza richiesta RosettaNet")
   public static final String PRIVATE_CONSTANT_17 = "viewTitle";

   /**
    * STATUS BAR MESSAGES -------------------------------------------------------
    * ERROR MESSAGES ------------------------------------------------------------
    **/
   @RBEntry("\"{0}\" non è un URL valido.")
   public static final String MALFORMED_URL = "0";

   @RBEntry("Le modifiche non sono state salvate. Salvarle ora?")
   public static final String CONFIRM_UNSAVED_CHANGES = "1";

   @RBEntry("Impossibile creare il nuovo oggetto richiesta RosettaNet.")
   public static final String UNABLE_TO_CREATE_WTRNREQUEST = "2";

   @RBEntry("L'oggetto non è una richiesta RosettaNet.")
   public static final String OBJECT_NOT_WTRNREQUEST = "3";

   @RBEntry("Eliminare \"{0}\"?")
   public static final String CONFIRM_DELETE = "4";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Fornire un valore per \"{0}\".")
   public static final String NULL_ATTRIBUTE_VALUE = "5";

   @RBEntry("Specificare la cartella in cui creare il documento.")
   public static final String PARENT_FOLDER_REQUIRED = "6";

   @RBEntry("Posizione:")
   public static final String PRIVATE_CONSTANT_18 = "locationLabel";

   @RBEntry("OK")
   public static final String PRIVATE_CONSTANT_19 = "okButton";

   @RBEntry("Sfoglia")
   public static final String PRIVATE_CONSTANT_20 = "browseButton";

   @RBEntry("...")
   @RBPseudo(false)
   public static final String PRIVATE_CONSTANT_21 = "ellipses";
}
