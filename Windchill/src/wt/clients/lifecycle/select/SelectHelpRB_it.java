/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.lifecycle.select;

import wt.util.resource.*;

@RBUUID("wt.clients.lifecycle.select.SelectHelpRB")
public final class SelectHelpRB_it extends WTListResourceBundle {
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_0 = "Desc/AssignLifeCycle/AssignLifeCycleHelp";

   @RBEntry("LCLCProjOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_1 = "Help/AssignLifeCycle/AssignLifeCycleHelp";

   @RBEntry("LCDefSelPtcpnt")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/AugmentLifeCycle/AugmentLifeCycleHelp";

   @RBEntry("LCAdvanceState")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_3 = "Help/SetLifeCycleState/SetLifeCycleStateHelp";
}
