/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.lifecycle.select;

import wt.util.resource.*;

@RBUUID("wt.clients.lifecycle.select.SelectRB")
public final class SelectRB extends WTListResourceBundle {
   /**
    * Utility -----------------------------------------------------------------
    **/
   @RBEntry(":")
   @RBPseudo(false)
   public static final String COLON = "31";

   /**
    * Buttons -----------------------------------------------------------------
    **/
   @RBEntry("OK")
   public static final String OK_BUTTON = "6";

   @RBEntry("Browse...")
   public static final String BROWSE_BUTTON = "7";

   @RBEntry("Cancel")
   public static final String CANCEL_BUTTON = "8";

   @RBEntry("Close")
   public static final String CLOSE_BUTTON = "9";

   @RBEntry("Help")
   public static final String HELP_BUTTON = "10";

   @RBEntry("Save")
   public static final String SAVE_BUTTON = "11";

   /**
    * Panels & Tabs & titles --------------------------------------------------
    **/
   @RBEntry("Role")
   public static final String ROLE = "19";

   @RBEntry("Current Members")
   public static final String CURRENT_MEMBERS = "22";

   @RBEntry("Assign Life Cycle")
   public static final String ASSIGN_LIFECYCLE_TITLE = "13";

   @RBEntry("Assign Project")
   public static final String ASSIGN_PROJECT_TITLE = "16";

   @RBEntry("Update Role Participants")
   public static final String AUGMENT_LIFECYCLE_TITLE = "15";

   @RBEntry("Life Cycle")
   public static final String LIFECYCLE = "12";

   @RBEntry("Reassign Team")
   public static final String REASSIGN_TEAM_TITLE = "17";

   @RBEntry("Reassign Life Cycle")
   public static final String REASSIGN_LIFECYCLE_TITLE = "18";

   @RBEntry("Set life cycle state")
   public static final String SET_LIFECYCLE_STATE_LABEL = "28";

   /**
    * Labels ------------------------------------------------------------------
    **/
   @RBEntry("Related Processes")
   public static final String RELATED_PROCESSES_LABEL = "27";

   @RBEntry("Set Life Cycle State")
   public static final String SET_LIFECYCLE_STATE_TITLE = "26";

   @RBEntry("Terminate Related Processes")
   public static final String TERMINATE_PROCESSES = "29";

   @RBEntry("{0} is at life cycle state: {1}")
   @RBArgComment0(" The display identity of the business object ")
   @RBArgComment1(" The LifeCycle state of the business object ")
   public static final String IS_AT_LIFECYCLE_STATE = "30";

   /**
    * Exceptions --------------------------------------------------------------
    **/
   @RBEntry("The Help system could not be initialized: ")
   public static final String HELP_INITIALIZATION_FAILED = "2";

   @RBEntry("An exception occurred during localization: ")
   public static final String LOCALIZING_FAILED = "3";

   @RBEntry("An exception occurred initializing the data: ")
   public static final String INITIALIZATION_FAILED = "1";

   @RBEntry("An exception occurred setting the project: ")
   public static final String PROJECT_NOT_SET = "4";

   @RBEntry("An exception occurred setting the life cycle: ")
   public static final String TEMPLATE_NOT_SET = "5";

   /**
    * ReProject Exceptions ----------------------------------------------------
    **/
   @RBEntry("Reassigning the project failed.  Exception: ")
   public static final String PROJECT_ASSIGNMENT_FAILED = "21";

   @RBEntry("You are not authorized to reassign the project.  You need modify rights on the object to complete this task.")
   public static final String NOT_AUTHORIZED_TO_REPROJECT = "24";

   /**
    * SetState Exception ------------------------------------------------------
    **/
   @RBEntry("You are not authorized to set the life cycle state of this object.  You need administrative rights on the object to complete this task.")
   public static final String NOT_AUTHORIZED_TO_SET_STATE = "32";

   /**
    * Reassign Lifecycle Template Exceptions ----------------------------------
    **/
   @RBEntry("Reassigning the object to another life cycle failed.  Exception: ")
   public static final String LIFECYCLE_ASSIGNMENT_FAILED = "20";

   @RBEntry("You are not authorized to reassign the Life Cycle.  You need modify rights on the object to complete this task.")
   public static final String NOT_AUTHORIZED_TO_REASSIGN = "25";

   /**
    * Augment Roles Exceptions ------------------------------------------------
    **/
   @RBEntry("You are not authorized to update any roles for this object")
   public static final String NOT_AUTHORIZED_TO_AUGMENT = "23";

   /**
    * test string -------------------------------------------------------------
    **/
   @RBEntry("..Localized..")
   public static final String TEST_STRING = "0";
}
