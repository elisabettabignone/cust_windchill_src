/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.lifecycle.select;

import wt.util.resource.*;

@RBUUID("wt.clients.lifecycle.select.SelectRB")
public final class SelectRB_it extends WTListResourceBundle {
   /**
    * Utility -----------------------------------------------------------------
    **/
   @RBEntry(":")
   @RBPseudo(false)
   public static final String COLON = "31";

   /**
    * Buttons -----------------------------------------------------------------
    **/
   @RBEntry("OK")
   public static final String OK_BUTTON = "6";

   @RBEntry("Sfoglia...")
   public static final String BROWSE_BUTTON = "7";

   @RBEntry("Annulla")
   public static final String CANCEL_BUTTON = "8";

   @RBEntry("Chiudi")
   public static final String CLOSE_BUTTON = "9";

   @RBEntry("Guida")
   public static final String HELP_BUTTON = "10";

   @RBEntry("Salva")
   public static final String SAVE_BUTTON = "11";

   /**
    * Panels & Tabs & titles --------------------------------------------------
    **/
   @RBEntry("Ruolo")
   public static final String ROLE = "19";

   @RBEntry("Membri attuali")
   public static final String CURRENT_MEMBERS = "22";

   @RBEntry("Assegna a ciclo di vita")
   public static final String ASSIGN_LIFECYCLE_TITLE = "13";

   @RBEntry("Assegna a progetto")
   public static final String ASSIGN_PROJECT_TITLE = "16";

   @RBEntry("Aggiorna partecipanti ai ruoli")
   public static final String AUGMENT_LIFECYCLE_TITLE = "15";

   @RBEntry("Ciclo di vita")
   public static final String LIFECYCLE = "12";

   @RBEntry("Assegna a un altro team")
   public static final String REASSIGN_TEAM_TITLE = "17";

   @RBEntry("Assegna a un altro ciclo di vita")
   public static final String REASSIGN_LIFECYCLE_TITLE = "18";

   @RBEntry("Imposta stato del ciclo di vita")
   public static final String SET_LIFECYCLE_STATE_LABEL = "28";

   /**
    * Labels ------------------------------------------------------------------
    **/
   @RBEntry("Processi correlati")
   public static final String RELATED_PROCESSES_LABEL = "27";

   @RBEntry("Imposta stato del ciclo di vita")
   public static final String SET_LIFECYCLE_STATE_TITLE = "26";

   @RBEntry("Termina processi collegati")
   public static final String TERMINATE_PROCESSES = "29";

   @RBEntry("{0} è allo stato del ciclo di vita: {1}")
   @RBArgComment0(" The display identity of the business object ")
   @RBArgComment1(" The LifeCycle state of the business object ")
   public static final String IS_AT_LIFECYCLE_STATE = "30";

   /**
    * Exceptions --------------------------------------------------------------
    **/
   @RBEntry("Impossibile inizializzare la Guida in linea: ")
   public static final String HELP_INITIALIZATION_FAILED = "2";

   @RBEntry("Si è verificata un'eccezione durante la localizzazione: ")
   public static final String LOCALIZING_FAILED = "3";

   @RBEntry("Si è verificata un'eccezione durante l'inizializzazione dei dati: ")
   public static final String INITIALIZATION_FAILED = "1";

   @RBEntry("Si è verificata un'eccezione durante l'impostazione del progetto:")
   public static final String PROJECT_NOT_SET = "4";

   @RBEntry("Si è verificata un'eccezione durante l'impostazione del ciclo di vita: ")
   public static final String TEMPLATE_NOT_SET = "5";

   /**
    * ReProject Exceptions ----------------------------------------------------
    **/
   @RBEntry("L'assegnazione a un altro progetto non è riuscita. Eccezione:")
   public static final String PROJECT_ASSIGNMENT_FAILED = "21";

   @RBEntry("Non si dispone dell'autorizzazione per la riassegnazione del progetto. Per completare il task è necessario disporre dei diritti di modifica per l'oggetto.")
   public static final String NOT_AUTHORIZED_TO_REPROJECT = "24";

   /**
    * SetState Exception ------------------------------------------------------
    **/
   @RBEntry("Non si dispone dell'autorizzazione ad impostare lo stato del ciclo di vita di questo oggetto. Per completare il task è necessario disporre dei diritti amministrativi per l'oggetto.")
   public static final String NOT_AUTHORIZED_TO_SET_STATE = "32";

   /**
    * Reassign Lifecycle Template Exceptions ----------------------------------
    **/
   @RBEntry("L'assegnazione dell'oggetto a un altro ciclo di vita non è riuscita. Eccezione: ")
   public static final String LIFECYCLE_ASSIGNMENT_FAILED = "20";

   @RBEntry("Non si dispone dell'autorizzazione ad assegnare l'oggetto ad un altro ciclo di vita. Per completare il task è necessario disporre dei diritti di modifica per l'oggetto.")
   public static final String NOT_AUTHORIZED_TO_REASSIGN = "25";

   /**
    * Augment Roles Exceptions ------------------------------------------------
    **/
   @RBEntry("Non si dispone dell'autorizzazione ad aggiornare i ruoli dell'oggetto.")
   public static final String NOT_AUTHORIZED_TO_AUGMENT = "23";

   /**
    * test string -------------------------------------------------------------
    **/
   @RBEntry("..Localizzato..")
   public static final String TEST_STRING = "0";
}
