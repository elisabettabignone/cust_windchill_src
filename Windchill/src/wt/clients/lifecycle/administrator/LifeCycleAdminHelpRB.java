/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.lifecycle.administrator;

import wt.util.resource.*;

@RBUUID("wt.clients.lifecycle.administrator.LifeCycleAdminHelpRB")
public final class LifeCycleAdminHelpRB extends WTListResourceBundle {
   /**
    * -----Life Cycle Admin Defaults--------------------------------------------
    **/
   @RBEntry("New Phase")
   public static final String PRIVATE_CONSTANT_0 = "Tip/LifeCycleAdmin/main/New";

   @RBEntry("Cut Phase")
   public static final String PRIVATE_CONSTANT_1 = "Tip/LifeCycleAdmin/main/Cut";

   @RBEntry("Copy Phase")
   public static final String PRIVATE_CONSTANT_2 = "Tip/LifeCycleAdmin/main/Copy";

   @RBEntry("Paste Phase")
   public static final String PRIVATE_CONSTANT_3 = "Tip/LifeCycleAdmin/main/Paste";

   @RBEntry("Delete Phase")
   public static final String PRIVATE_CONSTANT_4 = "Tip/LifeCycleAdmin/main/Delete";

   @RBEntry("Help")
   public static final String PRIVATE_CONSTANT_5 = "Tip/LifeCycleAdmin/main/ToolbarHelp";

   @RBEntry("A visual representation of the Life Cycle flow")
   public static final String PRIVATE_CONSTANT_6 = "Desc/LifeCycleAdmin/main/LifeCycleFlow";

   @RBEntry("Save all changes and close this window")
   public static final String PRIVATE_CONSTANT_7 = "Desc/LifeCycleAdmin/main/OK";

   @RBEntry("The state of the selected phase")
   public static final String PRIVATE_CONSTANT_8 = "Desc/LifeCycleAdmin/main/State";

   @RBEntry("The Version Series of the selected phase")
   public static final String PRIVATE_CONSTANT_9 = "Desc/LifeCycleAdmin/main/SeriesSelector";

   @RBEntry("The name of the Life Cycle")
   public static final String PRIVATE_CONSTANT_10 = "Desc/LifeCycleAdmin/main/LifeCycleName";

   @RBEntry("The folder location for the Life Cycle")
   public static final String PRIVATE_CONSTANT_11 = "Desc/LifeCycleAdmin/main/LifeCycleLocation";

   @RBEntry("Browse for the folder location of the Life Cycle")
   public static final String PRIVATE_CONSTANT_12 = "Desc/LifeCycleAdmin/main/Browse";

   @RBEntry("The description of the Life Cycle")
   public static final String PRIVATE_CONSTANT_13 = "Desc/LifeCycleAdmin/main/Description";

   @RBEntry("The class of objects that will use the Life Cycle")
   public static final String PRIVATE_CONSTANT_14 = "Desc/LifeCycleAdmin/main/ClassList";

   @RBEntry("Close this window without saving changes")
   public static final String PRIVATE_CONSTANT_15 = "Desc/LifeCycleAdmin/main/Cancel";

   @RBEntry("Close this window")
   public static final String PRIVATE_CONSTANT_16 = "Desc/LifeCycleAdmin/main/Close";

   @RBEntry("Save all changes and leave this window open")
   public static final String PRIVATE_CONSTANT_17 = "Desc/LifeCycleAdmin/main/Save";

   @RBEntry("Help for Life Cycle definition")
   public static final String PRIVATE_CONSTANT_18 = "Desc/LifeCycleAdmin/main/Help";

   @RBEntry("Permissions the selected role has for an object in this phase")
   public static final String PRIVATE_CONSTANT_19 = "Desc/LifeCycleAdmin/main/PermissionPanel";

   @RBEntry("The list of selected roles for which permissions can be set")
   public static final String PRIVATE_CONSTANT_20 = "Desc/LifeCycleAdmin/main/SelectedList";

   @RBEntry("Allow the selected role all existing and future permissions")
   public static final String PRIVATE_CONSTANT_21 = "Desc/LifeCycleAdmin/main/AllPermit";

   @RBEntry("Allow the selected role other, more specific permissions")
   public static final String PRIVATE_CONSTANT_22 = "Desc/LifeCycleAdmin/main/OtherPermit";

   @RBEntry("All roles need read access to the object in the Life Cycle")
   public static final String PRIVATE_CONSTANT_23 = "Desc/LifeCycleAdmin/main/ReadPermit";

   @RBEntry("Allow the selected role to create an object in this phase")
   public static final String PRIVATE_CONSTANT_24 = "Desc/LifeCycleAdmin/main/CreatePermit";

   @RBEntry("Allow the selected role to modify an object in this phase")
   public static final String PRIVATE_CONSTANT_25 = "Desc/LifeCycleAdmin/main/ModifyPermit";

   @RBEntry("Allow the selected role to delete an object in this phase")
   public static final String PRIVATE_CONSTANT_26 = "Desc/LifeCycleAdmin/main/DeletePermit";

   @RBEntry("Allow the selected role to use an object in this phase")
   public static final String PRIVATE_CONSTANT_27 = "Desc/LifeCycleAdmin/main/UsePermit";

   @RBEntry("Allow the selected role to attach to an object in this phase")
   public static final String PRIVATE_CONSTANT_28 = "Desc/LifeCycleAdmin/main/AttachPermit";

   @RBEntry("Allow the selected role administrative privileges to an object in this phase")
   public static final String PRIVATE_CONSTANT_29 = "Desc/LifeCycleAdmin/main/AdministrationPermit";

   @RBEntry("The promotion criteria for the selected phase")
   public static final String PRIVATE_CONSTANT_30 = "Desc/LifeCycleAdmin/main/CriteriaList";

   @RBEntry("Create a new criterion")
   public static final String PRIVATE_CONSTANT_31 = "Desc/LifeCycleAdmin/main/CreateCriteria";

   @RBEntry("Update the selected criterion")
   public static final String PRIVATE_CONSTANT_32 = "Desc/LifeCycleAdmin/main/UpdateCriteria";

   @RBEntry("Delete the selected criterion")
   public static final String PRIVATE_CONSTANT_33 = "Desc/LifeCycleAdmin/main/DeleteCriteria";

   @RBEntry("Browse for a workflow process")
   public static final String PRIVATE_CONSTANT_34 = "Desc/LifeCycleAdmin/main/BrowseWorkflow";

   @RBEntry("Select the workflow process associated with this gate")
   public static final String PRIVATE_CONSTANT_35 = "Desc/LifeCycleAdmin/main/SelectGateWorkflow";

   @RBEntry("Select the workflow process associated with this phase")
   public static final String PRIVATE_CONSTANT_36 = "Desc/LifeCycleAdmin/main/SelectPhaseWorkflow";

   @RBEntry("The workflow process associated with this gate")
   public static final String PRIVATE_CONSTANT_37 = "Desc/LifeCycleAdmin/main/GateWorkflow";

   @RBEntry("The workflow process associated with this phase")
   public static final String PRIVATE_CONSTANT_38 = "Desc/LifeCycleAdmin/main/PhaseWorkflow";

   @RBEntry("LCAdminAbout")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_39 = "Help/LifeCycleAdmin/MainHelp";

   @RBEntry("LCCreate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_40 = "Help/LifeCycleAdmin/LifeCycleHelp";

   @RBEntry("LCPropertiesLCEditView")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_41 = "Help/LifeCycleAdmin/LifeCycleHelpEV";

   @RBEntry("LCStateTransDefine")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_42 = "Help/LifeCycleAdmin/PhaseTransitionsHelp";

   @RBEntry("LCRolesSelect")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_43 = "Help/LifeCycleAdmin/PhaseRoleHelp";

   @RBEntry("LCAdHocRulesDefine")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_44 = "Help/LifeCycleAdmin/PhaseAccessHelp";

   @RBEntry("LCWFProcessPhaseGate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_45 = "Help/LifeCycleAdmin/PhaseWorkflowHelp";

   @RBEntry("LCPromoCriteriaDefine")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_46 = "Help/LifeCycleAdmin/PhaseCriteriaHelp";

   @RBEntry(" ")
   @RBPseudo(false)
   public static final String PRIVATE_CONSTANT_47 = "Desc/LifeCycleAdmin/LifeCycleAdminHelp";
}
