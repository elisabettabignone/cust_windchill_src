/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.lifecycle.administrator;

import wt.util.resource.*;

@RBUUID("wt.clients.lifecycle.administrator.LifeCycleAdminRB")
public final class LifeCycleAdminRB extends WTListResourceBundle {
   /**
    * ------------------------------------------------------------
    **/
   @RBEntry(":")
   @RBPseudo(false)
   public static final String COLON = "8";

   @RBEntry("Save As...")
   public static final String SAVEAS_BUTTON = "9";

   @RBEntry("Cancel")
   public static final String CANCEL_BUTTON = "10";

   @RBEntry("Organization")
   public static final String ORGANIZATION = "120";

   @RBEntry("Context")
   public static final String CONTEXT = "121";

   @RBEntry("You cannot rename a checked out template.")
   public static final String CANT_RENAME_CHECKEDOUT = "122";

   @RBEntry("Clear")
   public static final String CLEAR_BUTTON = "12";

   @RBEntry("Name")
   public static final String NAME = "13";

   @RBEntry("Rename...")
   public static final String RENAME_BUTTON = "16";

   @RBEntry("Phase Process:")
   public static final String PHASE_PROCESS_LABEL = "1";

   @RBEntry("Gate Process:")
   public static final String GATE_PROCESS_LABEL = "2";

   /**
    * ------------------------------------------------------------
    **/
   @RBEntry("Select Process Template")
   public static final String WORKFLOW_SEARCH_TITLE = "18";

   @RBEntry("Enabled")
   public static final String CHECK_BOX_ENABLED = "19";

   @RBEntry("Available for Routing")
   @RBComment("This is shown as a label for a checkbox. selecting the checkbox denotes that template can be used for routing other objects.")
   public static final String CHECK_BOX_ROUTING = "19a";

   @RBEntry("Finding Life Cycles...")
   public static final String FINDING_TEMPLATES = "20";

   @RBEntry("Use Latest Iteration")
   public static final String LATEST_ITERATION = "21";

   /**
    * ------------------------------------------------------------
    * Buttons
    **/
   @RBEntry("Create")
   public static final String NEW_BUTTON = "23";

   @RBEntry("View    ")
   public static final String VIEW_BUTTON = "24";

   @RBEntry("Edit")
   public static final String UPDATE_BUTTON = "25";

   @RBEntry("OK")
   public static final String OK_BUTTON = "26";

   @RBEntry("Save")
   public static final String SAVE_BUTTON = "27";

   @RBEntry("Close")
   public static final String CLOSE_BUTTON = "29";

   @RBEntry("Help")
   public static final String HELP_BUTTON = "30";

   @RBEntry("New")
   public static final String CREATE_BUTTON = "32";

   @RBEntry("Delete")
   public static final String DELETE_BUTTON = "34";

   @RBEntry("New")
   public static final String NEW = "35";

   @RBEntry("Edit")
   public static final String EDIT = "36";

   @RBEntry("Yes")
   public static final String YES_BUTTON = "37";

   @RBEntry("No")
   public static final String NO_BUTTON = "38";

   @RBEntry("Browse...")
   public static final String BROWSE_BUTTON_LABEL = "4";

   /**
    * ------------------------------------------------------------
    **/
   @RBEntry("Check Out")
   public static final String CHECKOUT_BUTTON = "40";

   @RBEntry("Undo Check Out")
   public static final String UNDOCHECK_OUT_BUTTON = "41";

   @RBEntry("Check In")
   public static final String CHECKIN_BUTTON = "42";

   @RBEntry("Enable")
   public static final String ENABLE_BUTTON = "43";

   @RBEntry("Disable")
   public static final String DISABLE_BUTTON = "44";

   @RBEntry("Iteration History")
   public static final String HISTORY_BUTTON = "45";

   @RBEntry("Import...")
   public static final String IMPORT_BUTTON = "46";

   @RBEntry("Importing file...")
   public static final String IMPORT_PROGRESS = "46a";

   @RBEntry("Browse for file")
   public static final String FILE_DIALOG_TITLE = "46b";

   @RBEntry("Import of template failed. Please see detailed logs for additional information.")
   public static final String IMPORT_ERROR = "46c";

   @RBEntry("Export...")
   public static final String EXPORT_BUTTON = "47";

   @RBEntry("Export of template failed. Please see detailed logs for additional information.")
   public static final String EXPORT_ERROR = "47a";

   @RBEntry("Please enter or select files of type .zip or .jar only.")
   public static final String WRONG_FILE_ERROR = "47b";

   /**
    * ------------------------------------------------------------
    * Labels
    **/
   @RBEntry("Life Cycle:")
   public static final String LIFE_CYCLE_TEMPLATE_LABEL = "48";

   @RBEntry("Phase")
   public static final String PHASE_LABEL = "49";

   @RBEntry("*State:")
   public static final String STATE_LABEL = "50";

   @RBEntry("*Name:")
   public static final String NAME_LABEL = "51";

   @RBEntry("*Location:")
   public static final String LOCATION_LABEL = "52";

   @RBEntry("Description:")
   public static final String DESCRIPTION_LABEL = "53";

   @RBEntry("*Class:")
   public static final String CLASS_LABEL = "54";

   @RBEntry("*Type:")
   public static final String TYPE_LABEL = "55";

   /**
    * ------------------------------------------------------------
    **/
   @RBEntry("Permissions")
   public static final String ROLE_PERMISSIONS_LABEL = "57";

   @RBEntry("Criteria")
   public static final String CRITERIA_LABEL = "58";

   /**
    * ------------------------------------------------------------
    **/
   @RBEntry("*Role")
   public static final String ROLE_LABEL = "59";

   @RBEntry("*Template")
   public static final String TEMPLATE_LABEL = "60";

   @RBEntry("Properties - Phase")
   public static final String PROPERTIES_PHASE_LABEL = "61";

   @RBEntry("Properties - Life Cycle")
   public static final String PROPERTIES_LIFE_CYCLE_LABEL = "62";

   @RBEntry("All roles have read access")
   public static final String READ_LABEL = "63";

   @RBEntry("Progress")
   public static final String PROGRESSTITLE = "64";

   @RBEntry(" ")
   public static final String SPACE = "66";

   @RBEntry(" - ")
   @RBPseudo(false)
   public static final String DASH = "67";

   @RBEntry("Version Series:")
   public static final String SELECTOR_LABEL = "68";

   @RBEntry("Choose Workflow Process")
   public static final String BROWSE_WORKFLOW_DIALOG_TITLE = "3";

   /**
    * ------------------------------------------------------------
    * Checkboxes
    **/
   @RBEntry("Required?")
   public static final String REQUIRED_CHECKBOX = "71";

   @RBEntry("Create")
   public static final String CREATE_CHECKBOX = "72";

   @RBEntry("Read")
   public static final String READ_CHECKBOX = "73";

   @RBEntry("Modify")
   public static final String MODIFY_CHECKBOX = "74";

   @RBEntry("Delete")
   public static final String DELETE_CHECKBOX = "75";

   @RBEntry("Attach")
   public static final String ATTACH_CHECKBOX = "76";

   @RBEntry("Administrative")
   public static final String ADMINISTRATIONCHECKBOX = "77";

   /**
    * ------------------------------------------------------------
    * Radio Buttons
    **/
   @RBEntry("Full Control (All)")
   public static final String ALL_RADIO_BUTTON = "78";

   @RBEntry("Other")
   public static final String OTHER_RADIO_BUTTON = "79";

   @RBEntry("Basic")
   public static final String BASIC_RADIO_BUTTON = "790";

   @RBEntry("Advanced")
   public static final String ADVANCED_RADIO_BUTTON = "791";

   /**
    * ------------------------------------------------------------
    * Columns
    **/
   @RBEntry("Name")
   public static final String COLUMN_NAME = "80";

   @RBEntry("Enabled")
   public static final String COLUMN_ENABLED = "81";

   @RBEntry("Cabinet")
   public static final String CABINET = "82";

   @RBEntry("Routing")
   @RBComment("This is used as a header for a column of checkboxes. It denotes whether or not the lifecycle template can be used to Route other objects")
   public static final String COLUMN_ROUTING = "810";

   /**
    * ------------------------------------------------------------
    **/
   @RBEntry("Selected Roles")
   public static final String SELECTED_ROLES_LABEL = "56";

   /**
    * ------------------------------------------------------------
    * Tab Panel labels
    **/
   @RBEntry("Roles")
   public static final String ROLESTAB = "83";

   @RBEntry("Access Control")
   public static final String ACCESS_CONTROL_TAB = "84";

   @RBEntry("Workflow")
   public static final String WORKFLOW_TAB = "85";

   @RBEntry("Promotion Criteria")
   public static final String PROMOTION_CRITERIA_TAB = "86";

   @RBEntry("Transitions")
   public static final String TRANSITIONS_TAB = "123";

   /**
    * ------------------------------------------------------------
    **/
   @RBEntry("Delete Latest Iteration")
   public static final String ITERATION_BUTTON = "113";

   @RBEntry("Do you wish to delete the following iteration: {0} ?")
   public static final String ITERATION_QUESTION = "114";

   /**
    * ------------------------------------------------------------
    * Messages
    **/
   @RBEntry(" will be notified using ")
   public static final String NOTIFIEDVIA = "87";

   @RBEntry(" (required)")
   public static final String REQUIRED_STRING = "88";

   @RBEntry("You must provide a state for each phase.")
   public static final String NO_STATE = "89";

   @RBEntry("You must have at least one Phase in the Life Cycle.")
   public static final String NO_PHASE = "90";

   @RBEntry("The Life Cycle requires a name.")
   public static final String NO_LIFECYCLE_NAME = "91";

   @RBEntry("Couldn't find Life Cycle ")
   public static final String LIFE_CYCLE_TEMPLATE_NOT_FOUND = "92";

   @RBEntry("You must specify the criteria.")
   public static final String MUST_SPECIFY_CRITERIA = "93";

   @RBEntry("Are you sure you want to delete this?")
   public static final String CONFIRM_CRITERIA_DELETION = "930";

   @RBEntry("All iterations of {0} will be deleted. Continue deleting {0}?")
   public static final String CONFIRM_DELETE_ITERATIONS = "17";

   @RBEntry("Selecting Basic will cause all roles, access, workflows and criteria to be removed.  Do you wish to continue?")
   public static final String CONFIRM_CHANGE_TO_BASIC_LIFECYCLE = "140";

   @RBEntry(" No workflow template(s) are associated to this lifecycle template. Do you wish to change this template type to Basic for better performance and scalability.")
   public static final String PERFORMANCE_MESSAGE_CHANGE_TO_BASIC_LIFECYCLE = "141";

   /**
    * ------------------------------------------------------------
    **/
   @RBEntry("Error")
   public static final String ERROR = "95";

   /**
    * ------------------------------------------------------------
    **/
   @RBEntry("Click here to display Life Cycle properties")
   public static final String CLICKHERE = "96";

   @RBEntry("Deleting a Life Cycle is not allowed.")
   public static final String NO_DELETE = "97";

   @RBEntry("You do not have permissions to update a Life Cycle.")
   public static final String NO_UPDATE = "98";

   @RBEntry("You do not have permissions to create a Life Cycle.")
   public static final String NO_CREATE = "99";

   @RBEntry("Life Cycles must be created in the System cabinet.")
   public static final String NOT_SYSTEM = "100";

   @RBEntry("You are not allowed to modify this Life Cycle.")
   public static final String MODIFICATION_NOT_ALLOWED = "101";

   @RBEntry("Your are not authorized to modify this Life Cycle.")
   public static final String MODIFICATION_NOT_AUTHORIZED = "102";

   @RBEntry("The Life Cycle you selected does not exist anymore.")
   public static final String OBJECT_DOES_NOT_EXIST = "103";

   /**
    * ------------------------------------------------------------
    **/
   @RBEntry("Edit Life Cycle")
   public static final String UPDATE_LIFE_CYCLE_TEMPLATE = "104";

   @RBEntry("Create Life Cycle")
   public static final String CREATE_LIFE_CYCLE_TEMPLATE = "105";

   @RBEntry("View Life Cycle")
   public static final String VIEW_LIFE_CYCLE_TEMPLATE = "106";

   @RBEntry("New Criteria")
   public static final String CREATE_CRITERIA = "107";

   @RBEntry("Edit Criteria")
   public static final String UPDATE_CRITERIA = "108";

   /**
    * ------------------------------------------------------------
    **/
   @RBEntry("Participants for ")
   public static final String PARTICIPANTS_FOR = "109";

   @RBEntry("Couldn't find toolbar images.")
   public static final String NO_IMAGES = "110";

   @RBEntry("Saving Life Cycle...")
   public static final String SAVING_LIFECYCLE = "111";

   @RBEntry("Saved.")
   public static final String SAVED = "112";

   /**
    * ------------------------------------------------------------
    **/
   @RBEntry("The workflow process \"{0}\" can not be used in this Life Cycle because it no longer exists.")
   public static final String INVALID_PROCESS = "7";

   @RBEntry("The default phase workflow ({0}) could not be found.  Either this workflow was not created during installation, or the property \"wt.lifecycle.defaultPhaseProcess\" found in wt.properties does not exist.  This problem should be fixed before proceeding.")
   public static final String MISSING_DEFAULT_PHASE_PROCESS_TEMPLATE = "5";

   @RBEntry("The default gate workflow ({0}) could not be found.  Either this workflow was not created during installation, or the property \"wt.lifecycle.defaultGateProcess\" found in wt.properties does not exist.  This problem should be fixed before proceeding.")
   public static final String MISSING_DEFAULT_GATE_PROCESS_TEMPLATE = "6";

   @RBEntry("Life Cycle - ")
   public static final String LIFECYCLE_NAME_LABEL = "115";

   /**
    * ------------------------------------------------------------
    **/
   @RBEntry("Changes were made to template {0} which have not been saved. Save these changes?")
   public static final String UNSAVED = "116";

   @RBEntry("Save Changes")
   public static final String SAVE_CHANGES = "117";

   @RBEntry("Life Cycle Template Administration")
   public static final String LIFE_CYCLE_ADMINISTRATOR = "118";
   
   @RBEntry("Life Cycle Template")
   public static final String LIFE_CYCLE_TEMPLATE = "LIFE_CYCLE_TEMPLATE";
   
   @RBEntry("Administration")
   public static final String ADMINISTRATION = "ADMINISTRATION";

   @RBEntry("Life Cycles")
   public static final String LIFE_CYCLES = "119";

   /**
    * -----------------------------------------
    **/
   @RBEntry("Notice")
   public static final String NOTICE = "130";

   @RBEntry("Lifecyle states must be unique.  Please remove the duplicated state.")
   public static final String UNIQUE_STATE_ERROR = "131";

   @RBEntry("Basic LifeCycle state should be unique. Please choose a unique state.")
   public static final String CHOOSE_STATE_UNIQUE_BASIC = "133";

   @RBEntry("A Basic LifeCycle template cannot be defined as routing.")
   public static final String NOT_ROUTE_BASIC = "134";

   @RBEntry("Move")
   public static final String MOVE_BUTTON = "135";

   @RBEntry("You cannot move a checked out template.")
   public static final String CANT_MOVE_CHECKEDOUT = "136";

   @RBEntry("Are you sure you want to move?")
   public static final String LIFECYCLE_MOVE_CONFIRMATION = "137";

   @RBEntry("Can not move the lifecycle template as the template with same name already exist.")
   public static final String LIFECYCLE_ALREADY_EXIST = "138";

   @RBEntry("Can not able to move the lifecycle template as the attached workflow is not available at target context.")
   public static final String WORKFLOW_CONTEXT_ERROR = "139";

   @RBEntry("The Life Cycle name entered is not unique.  ")
   public static final String LCT_NOT_UNIQUE = "142";
   
   @RBEntry("Do you wish to delete the latest iteration of {0} ?")
   public static final String DELETE_LATEST_ITERATION_QUESTION = "DELETE_LATEST_ITERATION_QUESTION";
}

