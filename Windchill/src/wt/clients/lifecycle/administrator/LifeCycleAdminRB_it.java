/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.lifecycle.administrator;

import wt.util.resource.*;

@RBUUID("wt.clients.lifecycle.administrator.LifeCycleAdminRB")
public final class LifeCycleAdminRB_it extends WTListResourceBundle {
   /**
    * ------------------------------------------------------------
    **/
   @RBEntry(":")
   @RBPseudo(false)
   public static final String COLON = "8";

   @RBEntry("Salva con nome...")
   public static final String SAVEAS_BUTTON = "9";

   @RBEntry("Annulla")
   public static final String CANCEL_BUTTON = "10";

   @RBEntry("Organizzazione")
   public static final String ORGANIZATION = "120";

   @RBEntry("Contesto")
   public static final String CONTEXT = "121";

   @RBEntry("Impossibile rinominare un modello sottoposto a Check-Out.")
   public static final String CANT_RENAME_CHECKEDOUT = "122";

   @RBEntry("Reimposta")
   public static final String CLEAR_BUTTON = "12";

   @RBEntry("Nome")
   public static final String NAME = "13";

   @RBEntry("Rinomina...")
   public static final String RENAME_BUTTON = "16";

   @RBEntry("Processo abbinato alla fase:")
   public static final String PHASE_PROCESS_LABEL = "1";

   @RBEntry("Processo abbinato al gate:")
   public static final String GATE_PROCESS_LABEL = "2";

   /**
    * ------------------------------------------------------------
    **/
   @RBEntry("Seleziona modello di processo")
   public static final String WORKFLOW_SEARCH_TITLE = "18";

   @RBEntry("Attivato")
   public static final String CHECK_BOX_ENABLED = "19";

   @RBEntry("Pronto per l'instradamento")
   @RBComment("This is shown as a label for a checkbox. selecting the checkbox denotes that template can be used for routing other objects.")
   public static final String CHECK_BOX_ROUTING = "19a";

   @RBEntry("Ricerca dei cicli di vita in corso...")
   public static final String FINDING_TEMPLATES = "20";

   @RBEntry("Usa l'ultima iterazione")
   public static final String LATEST_ITERATION = "21";

   /**
    * ------------------------------------------------------------
    * Buttons
    **/
   @RBEntry("Crea")
   public static final String NEW_BUTTON = "23";

   @RBEntry("Visualizza")
   public static final String VIEW_BUTTON = "24";

   @RBEntry("Modifica")
   public static final String UPDATE_BUTTON = "25";

   @RBEntry("OK")
   public static final String OK_BUTTON = "26";

   @RBEntry("Salva")
   public static final String SAVE_BUTTON = "27";

   @RBEntry("Chiudi")
   public static final String CLOSE_BUTTON = "29";

   @RBEntry("Guida")
   public static final String HELP_BUTTON = "30";

   @RBEntry("Nuovo")
   public static final String CREATE_BUTTON = "32";

   @RBEntry("Elimina")
   public static final String DELETE_BUTTON = "34";

   @RBEntry("Nuovo")
   public static final String NEW = "35";

   @RBEntry("Modifica")
   public static final String EDIT = "36";

   @RBEntry("Sì")
   public static final String YES_BUTTON = "37";

   @RBEntry("No")
   public static final String NO_BUTTON = "38";

   @RBEntry("Sfoglia...")
   public static final String BROWSE_BUTTON_LABEL = "4";

   /**
    * ------------------------------------------------------------
    **/
   @RBEntry("Check-Out")
   public static final String CHECKOUT_BUTTON = "40";

   @RBEntry("Annulla Check-Out")
   public static final String UNDOCHECK_OUT_BUTTON = "41";

   @RBEntry("Check-In")
   public static final String CHECKIN_BUTTON = "42";

   @RBEntry("Attiva")
   public static final String ENABLE_BUTTON = "43";

   @RBEntry("Disattiva")
   public static final String DISABLE_BUTTON = "44";

   @RBEntry("Cronologia iterazioni")
   public static final String HISTORY_BUTTON = "45";

   @RBEntry("Importa...")
   public static final String IMPORT_BUTTON = "46";

   @RBEntry("Importazione file in corso...")
   public static final String IMPORT_PROGRESS = "46a";

   @RBEntry("Cerca file")
   public static final String FILE_DIALOG_TITLE = "46b";

   @RBEntry("Importazione del modello non riuscita. Per ulteriori informazioni, vedere i log dei dettagli.")
   public static final String IMPORT_ERROR = "46c";

   @RBEntry("Esporta...")
   public static final String EXPORT_BUTTON = "47";

   @RBEntry("Esportazione del modello non riuscita. Vedere i log dei dettagli per informazioni supplementari.")
   public static final String EXPORT_ERROR = "47a";

   @RBEntry("Specificare o selezionare solo i file .zip o .jar.")
   public static final String WRONG_FILE_ERROR = "47b";

   /**
    * ------------------------------------------------------------
    * Labels
    **/
   @RBEntry("Ciclo di vita:")
   public static final String LIFE_CYCLE_TEMPLATE_LABEL = "48";

   @RBEntry("Fase")
   public static final String PHASE_LABEL = "49";

   @RBEntry("*Stato del ciclo di vita:")
   public static final String STATE_LABEL = "50";

   @RBEntry("*Nome:")
   public static final String NAME_LABEL = "51";

   @RBEntry("*Posizione:")
   public static final String LOCATION_LABEL = "52";

   @RBEntry("Descrizione:")
   public static final String DESCRIPTION_LABEL = "53";

   @RBEntry("*Classe:")
   public static final String CLASS_LABEL = "54";

   @RBEntry("*Tipo:")
   public static final String TYPE_LABEL = "55";

   /**
    * ------------------------------------------------------------
    **/
   @RBEntry("Permessi")
   public static final String ROLE_PERMISSIONS_LABEL = "57";

   @RBEntry("Criteri")
   public static final String CRITERIA_LABEL = "58";

   /**
    * ------------------------------------------------------------
    **/
   @RBEntry("*Ruolo")
   public static final String ROLE_LABEL = "59";

   @RBEntry("*Modello")
   public static final String TEMPLATE_LABEL = "60";

   @RBEntry("Proprietà - Fase")
   public static final String PROPERTIES_PHASE_LABEL = "61";

   @RBEntry("Proprietà - Ciclo di vita")
   public static final String PROPERTIES_LIFE_CYCLE_LABEL = "62";

   @RBEntry("Tutti i ruoli hanno accesso in lettura")
   public static final String READ_LABEL = "63";

   @RBEntry("Avanzamento")
   public static final String PROGRESSTITLE = "64";

   @RBEntry(" ")
   public static final String SPACE = "66";

   @RBEntry(" - ")
   @RBPseudo(false)
   public static final String DASH = "67";

   @RBEntry("Serie versione:")
   public static final String SELECTOR_LABEL = "68";

   @RBEntry("Scegli processo di workflow")
   public static final String BROWSE_WORKFLOW_DIALOG_TITLE = "3";

   /**
    * ------------------------------------------------------------
    * Checkboxes
    **/
   @RBEntry("Obbligatorio?")
   public static final String REQUIRED_CHECKBOX = "71";

   @RBEntry("Creazione")
   public static final String CREATE_CHECKBOX = "72";

   @RBEntry("Lettura")
   public static final String READ_CHECKBOX = "73";

   @RBEntry("Modifica")
   public static final String MODIFY_CHECKBOX = "74";

   @RBEntry("Eliminazione")
   public static final String DELETE_CHECKBOX = "75";

   @RBEntry("Associazione")
   public static final String ATTACH_CHECKBOX = "76";

   @RBEntry("Amministrazione")
   public static final String ADMINISTRATIONCHECKBOX = "77";

   /**
    * ------------------------------------------------------------
    * Radio Buttons
    **/
   @RBEntry("Controllo completo (tutti)")
   public static final String ALL_RADIO_BUTTON = "78";

   @RBEntry("Altro")
   public static final String OTHER_RADIO_BUTTON = "79";

   @RBEntry("Base")
   public static final String BASIC_RADIO_BUTTON = "790";

   @RBEntry("Avanzato")
   public static final String ADVANCED_RADIO_BUTTON = "791";

   /**
    * ------------------------------------------------------------
    * Columns
    **/
   @RBEntry("Nome")
   public static final String COLUMN_NAME = "80";

   @RBEntry("Attivato")
   public static final String COLUMN_ENABLED = "81";

   @RBEntry("Schedario")
   public static final String CABINET = "82";

   @RBEntry("Instradamento")
   @RBComment("This is used as a header for a column of checkboxes. It denotes whether or not the lifecycle template can be used to Route other objects")
   public static final String COLUMN_ROUTING = "810";

   /**
    * ------------------------------------------------------------
    **/
   @RBEntry("Ruoli selezionati")
   public static final String SELECTED_ROLES_LABEL = "56";

   /**
    * ------------------------------------------------------------
    * Tab Panel labels
    **/
   @RBEntry("Ruoli")
   public static final String ROLESTAB = "83";

   @RBEntry("Controllo d'accesso")
   public static final String ACCESS_CONTROL_TAB = "84";

   @RBEntry("Workflow")
   public static final String WORKFLOW_TAB = "85";

   @RBEntry("Criteri di promozione")
   public static final String PROMOTION_CRITERIA_TAB = "86";

   @RBEntry("Transizioni")
   public static final String TRANSITIONS_TAB = "123";

   /**
    * ------------------------------------------------------------
    **/
   @RBEntry("Elimina ultima iterazione")
   public static final String ITERATION_BUTTON = "113";

   @RBEntry("Eliminare l'iterazione {0}?")
   public static final String ITERATION_QUESTION = "114";

   /**
    * ------------------------------------------------------------
    * Messages
    **/
   @RBEntry(" verrà notificato utilizzando ")
   public static final String NOTIFIEDVIA = "87";

   @RBEntry(" (obbligatorio)")
   public static final String REQUIRED_STRING = "88";

   @RBEntry("Specificare uno stato per ciascuna fase.")
   public static final String NO_STATE = "89";

   @RBEntry("Specificare almeno una fase nel ciclo di vita.")
   public static final String NO_PHASE = "90";

   @RBEntry("Specificare un nome per il ciclo di vita.")
   public static final String NO_LIFECYCLE_NAME = "91";

   @RBEntry("Impossibile trovare il ciclo di vita ")
   public static final String LIFE_CYCLE_TEMPLATE_NOT_FOUND = "92";

   @RBEntry("Specificare i criteri.")
   public static final String MUST_SPECIFY_CRITERIA = "93";

   @RBEntry("Eliminare?")
   public static final String CONFIRM_CRITERIA_DELETION = "930";

   @RBEntry("Tutte le iterazioni di {0} saranno eliminate. Continuare l'eliminazione di {0}?")
   public static final String CONFIRM_DELETE_ITERATIONS = "17";

   @RBEntry("Se si seleziona Di base, tutti i ruoli, accessi, workflow e criteri verranno rimossi. Continuare?")
   public static final String CONFIRM_CHANGE_TO_BASIC_LIFECYCLE = "140";

   @RBEntry(" Nessun modello di workflow associato al modello di ciclo di vita. Utilizzare il tipo di modello di base per migliori prestazioni e scalabilità?")
   public static final String PERFORMANCE_MESSAGE_CHANGE_TO_BASIC_LIFECYCLE = "141";

   /**
    * ------------------------------------------------------------
    **/
   @RBEntry("Errore")
   public static final String ERROR = "95";

   /**
    * ------------------------------------------------------------
    **/
   @RBEntry("Fare clic per visualizzare le proprietà del ciclo di vita")
   public static final String CLICKHERE = "96";

   @RBEntry("Impossibile eliminare un ciclo di vita.")
   public static final String NO_DELETE = "97";

   @RBEntry("Non si dispone dei permessi per aggiornare un ciclo di vita.")
   public static final String NO_UPDATE = "98";

   @RBEntry("Non si dispone dei permessi per creare un ciclo di vita.")
   public static final String NO_CREATE = "99";

   @RBEntry("I cicli di vita devono essere creati nello schedario System.")
   public static final String NOT_SYSTEM = "100";

   @RBEntry("Impossibile modificare il ciclo di vita.")
   public static final String MODIFICATION_NOT_ALLOWED = "101";

   @RBEntry("Non si dispone dell'autorizzazione a modificare il ciclo di vita.")
   public static final String MODIFICATION_NOT_AUTHORIZED = "102";

   @RBEntry("Il ciclo di vita selezionato non è esistente.")
   public static final String OBJECT_DOES_NOT_EXIST = "103";

   /**
    * ------------------------------------------------------------
    **/
   @RBEntry("Modifica ciclo di vita")
   public static final String UPDATE_LIFE_CYCLE_TEMPLATE = "104";

   @RBEntry("Crea ciclo di vita")
   public static final String CREATE_LIFE_CYCLE_TEMPLATE = "105";

   @RBEntry("Visualizza ciclo di vita")
   public static final String VIEW_LIFE_CYCLE_TEMPLATE = "106";

   @RBEntry("Nuovi criteri")
   public static final String CREATE_CRITERIA = "107";

   @RBEntry("Modifica criteri")
   public static final String UPDATE_CRITERIA = "108";

   /**
    * ------------------------------------------------------------
    **/
   @RBEntry("Partecipanti per ")
   public static final String PARTICIPANTS_FOR = "109";

   @RBEntry("Impossibile trovare le immagini della barra degli strumenti.")
   public static final String NO_IMAGES = "110";

   @RBEntry("Salvataggio del ciclo di vita in corso...")
   public static final String SAVING_LIFECYCLE = "111";

   @RBEntry("Salvataggio eseguito.")
   public static final String SAVED = "112";

   /**
    * ------------------------------------------------------------
    **/
   @RBEntry("Processo di workflow \"{0}\" inesistente. Impossibile utilizzarlo nel ciclo di vita.")
   public static final String INVALID_PROCESS = "7";

   @RBEntry("Impossibile trovare il workflow di fase predefinito ({0}).  Il workflow non è stato creato durante l'installazione oppure non esiste la proprietà \"wt.lifecycle.defaultPhaseProcess\" in wt.properties. Risolvere il problema prima di procedere.")
   public static final String MISSING_DEFAULT_PHASE_PROCESS_TEMPLATE = "5";

   @RBEntry("Impossibile trovare il workflow del gate predefinito ({0}).  Il workflow non è stato creato durante l'installazione oppure non esiste la proprietà \"wt.lifecycle.defaultGateProcess\" in wt.properties. Risolvere il problema prima di procedere.")
   public static final String MISSING_DEFAULT_GATE_PROCESS_TEMPLATE = "6";

   @RBEntry("Ciclo di vita - ")
   public static final String LIFECYCLE_NAME_LABEL = "115";

   /**
    * ------------------------------------------------------------
    **/
   @RBEntry("Le modifiche apportate al modello {0} non sono state salvate. Salvare le modifiche?")
   public static final String UNSAVED = "116";

   @RBEntry("Salva modifiche")
   public static final String SAVE_CHANGES = "117";

   @RBEntry("Amministrazione modelli di ciclo di vita")
   public static final String LIFE_CYCLE_ADMINISTRATOR = "118";
   
   @RBEntry("Modello del ciclo di vita")
   public static final String LIFE_CYCLE_TEMPLATE = "LIFE_CYCLE_TEMPLATE";
   
   @RBEntry("Amministrazione")
   public static final String ADMINISTRATION = "ADMINISTRATION";

   @RBEntry("Cicli di vita")
   public static final String LIFE_CYCLES = "119";

   /**
    * -----------------------------------------
    **/
   @RBEntry("Avviso")
   public static final String NOTICE = "130";

   @RBEntry("Gli stati del ciclo di vita devono essere univoci. Rimuovere lo stato duplicato.")
   public static final String UNIQUE_STATE_ERROR = "131";

   @RBEntry("Lo stato del ciclo di vita di base deve essere univoco. Scegliere uno stato univoco.")
   public static final String CHOOSE_STATE_UNIQUE_BASIC = "133";

   @RBEntry("Impossibile definire un modello di ciclo di vita di base come instradamento.")
   public static final String NOT_ROUTE_BASIC = "134";

   @RBEntry("Sposta")
   public static final String MOVE_BUTTON = "135";

   @RBEntry("Impossibile spostare un modello sottoposto a Check-Out.")
   public static final String CANT_MOVE_CHECKEDOUT = "136";

   @RBEntry("Spostare?")
   public static final String LIFECYCLE_MOVE_CONFIRMATION = "137";

   @RBEntry("Impossibile spostare il modello di ciclo di vita: un modello con lo stesso nome esiste già.")
   public static final String LIFECYCLE_ALREADY_EXIST = "138";

   @RBEntry("Impossibile spostare il modello di ciclo di vita: il workflow allegato non è disponibile nel contesto di destinazione.")
   public static final String WORKFLOW_CONTEXT_ERROR = "139";

   @RBEntry("Il nome immesso per il ciclo di vita non è univoco.")
   public static final String LCT_NOT_UNIQUE = "142";
   
   @RBEntry("Eliminare l'ultima iterazione di {0}?")
   public static final String DELETE_LATEST_ITERATION_QUESTION = "DELETE_LATEST_ITERATION_QUESTION";
}

