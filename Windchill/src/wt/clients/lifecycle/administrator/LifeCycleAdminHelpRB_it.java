/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.lifecycle.administrator;

import wt.util.resource.*;

@RBUUID("wt.clients.lifecycle.administrator.LifeCycleAdminHelpRB")
public final class LifeCycleAdminHelpRB_it extends WTListResourceBundle {
   /**
    * -----Life Cycle Admin Defaults--------------------------------------------
    **/
   @RBEntry("Nuova fase")
   public static final String PRIVATE_CONSTANT_0 = "Tip/LifeCycleAdmin/main/New";

   @RBEntry("Taglia fase")
   public static final String PRIVATE_CONSTANT_1 = "Tip/LifeCycleAdmin/main/Cut";

   @RBEntry("Copia fase")
   public static final String PRIVATE_CONSTANT_2 = "Tip/LifeCycleAdmin/main/Copy";

   @RBEntry("Incolla fase")
   public static final String PRIVATE_CONSTANT_3 = "Tip/LifeCycleAdmin/main/Paste";

   @RBEntry("Elimina fase")
   public static final String PRIVATE_CONSTANT_4 = "Tip/LifeCycleAdmin/main/Delete";

   @RBEntry("Guida")
   public static final String PRIVATE_CONSTANT_5 = "Tip/LifeCycleAdmin/main/ToolbarHelp";

   @RBEntry("Rappresentazione grafica del flusso del ciclo di vita")
   public static final String PRIVATE_CONSTANT_6 = "Desc/LifeCycleAdmin/main/LifeCycleFlow";

   @RBEntry("Salva tutte le modifiche e chiude la finestra")
   public static final String PRIVATE_CONSTANT_7 = "Desc/LifeCycleAdmin/main/OK";

   @RBEntry("Stato della fase selezionata")
   public static final String PRIVATE_CONSTANT_8 = "Desc/LifeCycleAdmin/main/State";

   @RBEntry("Serie versione della fase selezionata")
   public static final String PRIVATE_CONSTANT_9 = "Desc/LifeCycleAdmin/main/SeriesSelector";

   @RBEntry("Nome del ciclo di vita")
   public static final String PRIVATE_CONSTANT_10 = "Desc/LifeCycleAdmin/main/LifeCycleName";

   @RBEntry("Cartella del ciclo di vita")
   public static final String PRIVATE_CONSTANT_11 = "Desc/LifeCycleAdmin/main/LifeCycleLocation";

   @RBEntry("Ricerca la cartella del ciclo di vita")
   public static final String PRIVATE_CONSTANT_12 = "Desc/LifeCycleAdmin/main/Browse";

   @RBEntry("Descrizione del ciclo di vita")
   public static final String PRIVATE_CONSTANT_13 = "Desc/LifeCycleAdmin/main/Description";

   @RBEntry("Classe degli oggetti che utilizzeranno il ciclo di vita")
   public static final String PRIVATE_CONSTANT_14 = "Desc/LifeCycleAdmin/main/ClassList";

   @RBEntry("Chiude la finestra senza salvare le modifiche")
   public static final String PRIVATE_CONSTANT_15 = "Desc/LifeCycleAdmin/main/Cancel";

   @RBEntry("Chiude la finestra")
   public static final String PRIVATE_CONSTANT_16 = "Desc/LifeCycleAdmin/main/Close";

   @RBEntry("Salva tutte le modifiche senza chiudere la finestra")
   public static final String PRIVATE_CONSTANT_17 = "Desc/LifeCycleAdmin/main/Save";

   @RBEntry("Guida per la definizione del ciclo di vita")
   public static final String PRIVATE_CONSTANT_18 = "Desc/LifeCycleAdmin/main/Help";

   @RBEntry("Permessi attribuiti al ruolo per un oggetto in questa fase")
   public static final String PRIVATE_CONSTANT_19 = "Desc/LifeCycleAdmin/main/PermissionPanel";

   @RBEntry("Elenco dei ruoli selezionati per cui è possibile impostare i permessi")
   public static final String PRIVATE_CONSTANT_20 = "Desc/LifeCycleAdmin/main/SelectedList";

   @RBEntry("Abilita tutti i permessi esistenti e futuri per il ruolo selezionato")
   public static final String PRIVATE_CONSTANT_21 = "Desc/LifeCycleAdmin/main/AllPermit";

   @RBEntry("Abilita altri permessi specifici per il ruolo selezionato")
   public static final String PRIVATE_CONSTANT_22 = "Desc/LifeCycleAdmin/main/OtherPermit";

   @RBEntry("Tutti i ruoli devono poter accedere in lettura all'oggetto nel ciclo di vita")
   public static final String PRIVATE_CONSTANT_23 = "Desc/LifeCycleAdmin/main/ReadPermit";

   @RBEntry("Consente al ruolo selezionato di creare un oggetto nella fase corrente")
   public static final String PRIVATE_CONSTANT_24 = "Desc/LifeCycleAdmin/main/CreatePermit";

   @RBEntry("Consente al ruolo selezionato di modificare un oggetto nella fase corrente")
   public static final String PRIVATE_CONSTANT_25 = "Desc/LifeCycleAdmin/main/ModifyPermit";

   @RBEntry("Consente al ruolo selezionato di eliminare un oggetto nella fase corrente")
   public static final String PRIVATE_CONSTANT_26 = "Desc/LifeCycleAdmin/main/DeletePermit";

   @RBEntry("Consente al ruolo selezionato di utilizzare un oggetto nella fase corrente")
   public static final String PRIVATE_CONSTANT_27 = "Desc/LifeCycleAdmin/main/UsePermit";

   @RBEntry("Consente al ruolo selezionato di associare un oggetto nella fase corrente")
   public static final String PRIVATE_CONSTANT_28 = "Desc/LifeCycleAdmin/main/AttachPermit";

   @RBEntry("Consente al ruolo selezionato di possedere privilegi amministrativi per un oggetto nella fase corrente")
   public static final String PRIVATE_CONSTANT_29 = "Desc/LifeCycleAdmin/main/AdministrationPermit";

   @RBEntry("Criterio di promozione per la fase selezionata")
   public static final String PRIVATE_CONSTANT_30 = "Desc/LifeCycleAdmin/main/CriteriaList";

   @RBEntry("Crea un nuovo criterio")
   public static final String PRIVATE_CONSTANT_31 = "Desc/LifeCycleAdmin/main/CreateCriteria";

   @RBEntry("Aggiorna il criterio selezionato")
   public static final String PRIVATE_CONSTANT_32 = "Desc/LifeCycleAdmin/main/UpdateCriteria";

   @RBEntry("Elimina il criterio selezionato")
   public static final String PRIVATE_CONSTANT_33 = "Desc/LifeCycleAdmin/main/DeleteCriteria";

   @RBEntry("Cerca un processo di workflow")
   public static final String PRIVATE_CONSTANT_34 = "Desc/LifeCycleAdmin/main/BrowseWorkflow";

   @RBEntry("Seleziona il processo di workflow associato al Gate corrente")
   public static final String PRIVATE_CONSTANT_35 = "Desc/LifeCycleAdmin/main/SelectGateWorkflow";

   @RBEntry("Seleziona il processo di workflow associato alla fase corrente")
   public static final String PRIVATE_CONSTANT_36 = "Desc/LifeCycleAdmin/main/SelectPhaseWorkflow";

   @RBEntry("Processo di workflow associato al Gate")
   public static final String PRIVATE_CONSTANT_37 = "Desc/LifeCycleAdmin/main/GateWorkflow";

   @RBEntry("Processo di workflow associato alla fase")
   public static final String PRIVATE_CONSTANT_38 = "Desc/LifeCycleAdmin/main/PhaseWorkflow";

   @RBEntry("LCAdminAbout")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_39 = "Help/LifeCycleAdmin/MainHelp";

   @RBEntry("LCCreate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_40 = "Help/LifeCycleAdmin/LifeCycleHelp";

   @RBEntry("LCPropertiesLCEditView")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_41 = "Help/LifeCycleAdmin/LifeCycleHelpEV";

   @RBEntry("LCStateTransDefine")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_42 = "Help/LifeCycleAdmin/PhaseTransitionsHelp";

   @RBEntry("LCRolesSelect")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_43 = "Help/LifeCycleAdmin/PhaseRoleHelp";

   @RBEntry("LCAdHocRulesDefine")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_44 = "Help/LifeCycleAdmin/PhaseAccessHelp";

   @RBEntry("LCWFProcessPhaseGate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_45 = "Help/LifeCycleAdmin/PhaseWorkflowHelp";

   @RBEntry("LCPromoCriteriaDefine")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_46 = "Help/LifeCycleAdmin/PhaseCriteriaHelp";

   @RBEntry(" ")
   @RBPseudo(false)
   public static final String PRIVATE_CONSTANT_47 = "Desc/LifeCycleAdmin/LifeCycleAdminHelp";
}
