/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.lifecycle.promote;

import wt.util.resource.*;

@RBUUID("wt.clients.lifecycle.promote.PromoteRB")
public final class PromoteRB extends WTListResourceBundle {
   /**
    * Button Labels -----------------------------------------------------------
    **/
   @RBEntry("Demote")
   public static final String DEMOTE_BUTTON = "0";

   @RBEntry("Drop")
   public static final String DROP_BUTTON = "1";

   @RBEntry("Help")
   public static final String HELP_BUTTON = "2";

   @RBEntry("No")
   public static final String NO_BUTTON = "3";

   @RBEntry("Yes")
   public static final String YES_BUTTON = "4";

   @RBEntry("Promote")
   public static final String PROMOTE_BUTTON = "5";

   @RBEntry("Deny")
   public static final String DENY_BUTTON = "6";

   /**
    * Tasks --------------------------------------------------------------------
    **/
   @RBEntry("Approve")
   public static final String APPROVE = "7";

   @RBEntry("Observe")
   public static final String OBSERVE = "16";

   @RBEntry("Promote")
   public static final String PROMOTE = "17";

   /**
    * Status -------------------------------------------------------------------
    **/
   @RBEntry("Approved")
   public static final String APPROVED_STATUS = "8";

   @RBEntry("Demoted")
   public static final String DEMOTED_STATUS = "11";

   @RBEntry("Denied")
   public static final String DENIED_STATUS = "12";

   @RBEntry("Dropped")
   public static final String DROPPED_STATUS = "13";

   @RBEntry("Promoted")
   public static final String PROMOTED_STATUS = "14";

   @RBEntry("Not Approved")
   public static final String NOT_APPROVED_STATUS = "9";

   /**
    * Labels & Captions --------------------------------------------------------
    **/
   @RBEntry("Life Cycle")
   public static final String LIFECYCLE_TITLE = "15";

   @RBEntry("Comments")
   public static final String COMMENTS = "10";

   @RBEntry("Promotion Criteria")
   public static final String PROMOTION_CRITERIA = "18";

   @RBEntry("Status")
   public static final String STATUS = "19";

   /**
    * Misc ---------------------------------------------------------------------
    **/
   @RBEntry("?")
   public static final String QUESTION_MARK = "20";

   @RBEntry(": ")
   @RBPseudo(false)
   public static final String COLON = "21";

   /**
    * Exceptions ---------------------------------------------------------------
    **/
   @RBEntry("This task cannot be done because a new Signature could not be created: ")
   public static final String CREATE_SIGNATURE_FAILED = "100";

   @RBEntry("Exception initialization the promotion criteria: ")
   public static final String CRITERIA_INITIALIZATION_FAILED = "101";

   @RBEntry("The promotion criteria was not updated: ")
   public static final String CRITERIA_UPDATE_FAILED = "102";

   @RBEntry("The vote was not recorded: ")
   public static final String VOTE_FAILED = "103";

   @RBEntry("Exception initializing the HelpSystem: ")
   public static final String HELP_INITIALIZATION_FAILED = "104";

   @RBEntry("Exception occurred during localization: ")
   public static final String LOCALIZING_FAILED = "105";

   @RBEntry("This task cannot be performed.  The target item has been promoted and the work item associated with this task has been deleted.")
   public static final String OBSOLETE_TASK = "106";

   @RBEntry("The promotion criteria has been updated by another user.  Review this change and continue with your vote.")
   public static final String STALE_CRITERIA = "107";

   @RBEntry("Exception initializing the task: ")
   public static final String INITIALIZATION_FAILED = "108";

   @RBEntry("..Localized..")
   public static final String TEST_STRING = "999";
}
