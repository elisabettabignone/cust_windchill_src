/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.lifecycle.promote;

import wt.util.resource.*;

@RBUUID("wt.clients.lifecycle.promote.PromoteRB")
public final class PromoteRB_it extends WTListResourceBundle {
   /**
    * Button Labels -----------------------------------------------------------
    **/
   @RBEntry("Declassa")
   public static final String DEMOTE_BUTTON = "0";

   @RBEntry("Abbandona")
   public static final String DROP_BUTTON = "1";

   @RBEntry("Guida")
   public static final String HELP_BUTTON = "2";

   @RBEntry("No")
   public static final String NO_BUTTON = "3";

   @RBEntry("Sì")
   public static final String YES_BUTTON = "4";

   @RBEntry("Promuovi")
   public static final String PROMOTE_BUTTON = "5";

   @RBEntry("Rifiuta")
   public static final String DENY_BUTTON = "6";

   /**
    * Tasks --------------------------------------------------------------------
    **/
   @RBEntry("Approva")
   public static final String APPROVE = "7";

   @RBEntry("Osserva")
   public static final String OBSERVE = "16";

   @RBEntry("Promuovi")
   public static final String PROMOTE = "17";

   /**
    * Status -------------------------------------------------------------------
    **/
   @RBEntry("Approvato")
   public static final String APPROVED_STATUS = "8";

   @RBEntry("Declassato")
   public static final String DEMOTED_STATUS = "11";

   @RBEntry("Rifiutato")
   public static final String DENIED_STATUS = "12";

   @RBEntry("Abbandonato")
   public static final String DROPPED_STATUS = "13";

   @RBEntry("Promosso")
   public static final String PROMOTED_STATUS = "14";

   @RBEntry("Non approvato")
   public static final String NOT_APPROVED_STATUS = "9";

   /**
    * Labels & Captions --------------------------------------------------------
    **/
   @RBEntry("Ciclo di vita")
   public static final String LIFECYCLE_TITLE = "15";

   @RBEntry("Commenti")
   public static final String COMMENTS = "10";

   @RBEntry("Criteri di promozione")
   public static final String PROMOTION_CRITERIA = "18";

   @RBEntry("Stato")
   public static final String STATUS = "19";

   /**
    * Misc ---------------------------------------------------------------------
    **/
   @RBEntry("?")
   public static final String QUESTION_MARK = "20";

   @RBEntry(": ")
   @RBPseudo(false)
   public static final String COLON = "21";

   /**
    * Exceptions ---------------------------------------------------------------
    **/
   @RBEntry("Il task non può essere eseguito poiché è impossibile creare una nuova firma: ")
   public static final String CREATE_SIGNATURE_FAILED = "100";

   @RBEntry("Eccezione durante l'inizializzazione dei criteri di promozione: ")
   public static final String CRITERIA_INITIALIZATION_FAILED = "101";

   @RBEntry("I criteri di promozione non sono stati aggiornati: ")
   public static final String CRITERIA_UPDATE_FAILED = "102";

   @RBEntry("Il voto non è stato registrato: ")
   public static final String VOTE_FAILED = "103";

   @RBEntry("Eccezione durante l'inizializzazione di HelpSystem: ")
   public static final String HELP_INITIALIZATION_FAILED = "104";

   @RBEntry("Eccezione durante la localizzazione: ")
   public static final String LOCALIZING_FAILED = "105";

   @RBEntry("Impossibile eseguire il task. L'elemento di destinazione è stato promosso e il task associato è stato eliminato.")
   public static final String OBSOLETE_TASK = "106";

   @RBEntry("I criteri di promozione sono stati aggiornati da un altro utente. Riesaminare la modifica e proseguire con il voto.")
   public static final String STALE_CRITERIA = "107";

   @RBEntry("Eccezione durante l'inizializzazione del task: ")
   public static final String INITIALIZATION_FAILED = "108";

   @RBEntry("..Localizzato..")
   public static final String TEST_STRING = "999";
}
