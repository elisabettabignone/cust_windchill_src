/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.lifecycle;

import wt.util.resource.*;

@RBUUID("wt.clients.lifecycle.LifeCycleRB")
public final class LifeCycleRB extends WTListResourceBundle {
   @RBEntry("Object {0} has already been submitted")
   public static final String LIFECYCLE_ALREADY_SUBMITTED = "1";
}
