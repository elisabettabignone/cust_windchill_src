/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.ixb;

import wt.util.resource.*;

@RBUUID("wt.clients.ixb.ixbResource")
public final class ixbResource_it extends WTListResourceBundle {
   @RBEntry("Esporta")
   public static final String EXPORT_FRAME_TITLE = "0";

   @RBEntry("Invia")
   public static final String SUBMIT_LABEL = "1";

   @RBEntry("Esci")
   public static final String EXIT_LABEL = "2";

   @RBEntry("Seleziona il tipo di struttura da aggiungere")
   public static final String ADD_ITEM_TO_LIST_DIALOG_TITLE = "3";

   @RBEntry("Oggetti da esportare")
   public static final String ELEMENTS_PANEL_TITLE = "4";

   @RBEntry("Aggiungi...")
   public static final String ADD_LABEL = "5";

   @RBEntry("Aggiunge l'oggetto all'elenco")
   public static final String ADD_TO_ELEMENTS_LIST_TOOLTIP = "6";

   @RBEntry("Elimina")
   public static final String DELETE_LABEL = "7";

   @RBEntry("Rimuove l'oggetto dall'elenco")
   public static final String DELETE_FROM_ELEMENT_LIST_TOOLTIP = "8";

   @RBEntry("Oggetti")
   public static final String ELEMENT_LIST_TITLE = "9";

   @RBEntry("Log dello stato di esportazione")
   public static final String EXPORT_STATUS_TITLE = "10";

   @RBEntry("Filtri")
   public static final String FILTER_LIST_TITLE = "11";

   @RBEntry("Aggiunge un filtro all'elenco")
   public static final String ADD_TO_FILTERS_LIST_TOOLTIP = "12";

   @RBEntry("Rimuove un filtro dall'elenco")
   public static final String DELETE_FROM_FILTER_LIST_TOOLTIP = "13";

   @RBEntry("Specificare un file per l'operazione \"Salva con nome\".")
   public static final String SPECIFY_SAVE_AS_FILE = "14";

   @RBEntry("Il file per l'operazione \"Salva con nome\" non è specificato.")
   public static final String SAVE_AS_DIALOG_TITLE = "15";

   @RBEntry("Aggiungere un oggetto all'elenco.")
   public static final String ADD_ITEM_TO_ELEMENT_LIST = "16";

   @RBEntry("L'elenco degli elementi è vuoto.")
   public static final String EMPTY_LIST_DIALOG_TITLE = "17";

   @RBEntry("Ricerca l'oggetto di livello superiore")
   public static final String NAVIGATION_DIALOG_TITLE = "18";

   @RBEntry("Oggetto di livello superiore:")
   public static final String TOP_LEVEL_OBJECT_LABEL = "19";

   @RBEntry("Cerca...")
   public static final String FIND_LABEL = "20";

   @RBEntry("Trova l'oggetto di livello superiore")
   public static final String NAVIGATOR_DIALOG_FIND_TOOLTIP = "21";

   @RBEntry("Finestra di ricerca navigatore")
   public static final String NAVIGATOR_SEARCH_TITLE = "22";

   @RBEntry("Esporta componenti")
   public static final String MESSAGE_COMPONENTS_PANEL_TITLE = "23";

   @RBEntry("Trova il messaggio partendo dai suoi componenti")
   public static final String NAVIGATOR_SEARCH_DIALOG_FIND_TOOLTIP = "24";

   @RBEntry("Log dello stato di importazione")
   public static final String IMPORT_STATUS_TITLE = "25";

   @RBEntry("Importa")
   public static final String IMPORT_FRAME_TITLE = "26";

   @RBEntry("File regole di importazione:")
   public static final String IMPORT_RULES_FILE = "27";

   @RBEntry("Nome file importazione:")
   public static final String IMPORT_DATA_FILE = "28";

   @RBEntry("Sfoglia...")
   public static final String BROWSE_LABEL = "29";

   @RBEntry("Cerca il file delle regole di mappatura")
   public static final String BROWSE_FOR_RULES_FILE_TOOLTIP = "30";

   @RBEntry("Cerca il file di importazione")
   public static final String BROWSE_FOR_DATA_FILE_TOOLTIP = "31";

   @RBEntry("File regole di esportazione:")
   public static final String MAPPING_RULES_LABEL = "32";

   @RBEntry("Cerca il file delle regole di mappatura")
   public static final String BROWSE_MAP_RULES_TOOLTIP = "34";

   @RBEntry("Nome file di esportazione:")
   public static final String SAVE_AS_LABEL = "35";

   @RBEntry("Cerca il percorso del file di esportazione")
   public static final String BROWSE_SAVE_AS_TOOLTIP = "36";

   @RBEntry("OK")
   public static final String OK_LABEL = "37";

   @RBEntry("Annulla")
   public static final String CANCEL_LABEL = "38";

   @RBEntry("Risolvi conflitti")
   public static final String RESOLVE_CONFLICTS_STRING = "39";

   @RBEntry("Impostazioni di importazione")
   public static final String IMPORT_SETTINGS_PANEL_TITLE = "40";

   @RBEntry("Gli elementi invisibili nella specifica di configurazione dell'utente saranno filtrati")
   public static final String FILTER_BY_CONFIG_SPEC_MESSAGE = "41";

   @RBEntry("Filtra con la specifica di configurazione")
   public static final String FILTER_BY_CONFIG_SPEC_TITLE = "42";

   @RBEntry("Seleziona")
   public static final String SELECT_LABEL = "43";

   @RBEntry("Data di modifica")
   public static final String FILTER_BY_TIME_TITLE = "44";

   @RBEntry("Includi tutti gli oggetti creati o modificati:")
   public static final String FILTER_BY_TIME_MESSAGE = "45";

   @RBEntry("Intervallo di tempo")
   public static final String TIME_RANGE_LABEL = "46";

   @RBEntry("Durante i precedenti")
   public static final String PREVIOUS_TIME_LABEL = "47";

   @RBEntry("Giorni")
   public static final String DAYS_LABEL = "49";

   @RBEntry("Mesi")
   public static final String MONTHS_LABEL = "50";

   @RBEntry("Da:")
   public static final String FROM_TIME = "51";

   @RBEntry("A:")
   public static final String TO_TIME = "52";

   @RBEntry("Specificare la data d'inizio")
   public static final String START_DATE_IS_NULL = "53";

   @RBEntry("Specificare la data di fine")
   public static final String END_DATE_IS_NULL = "54";

   @RBEntry("La data di fine precede la data d'inizio")
   public static final String TO_DATE_PRECEDES_FROM_DATE = "55";

   @RBEntry("Periodo non valido")
   public static final String TIME_PERIOD_IS_NOT_VALID = "56";

   @RBEntry("Selezionare un periodo valido.\nIl valore deve essere un numero intero positivo non superiore a 1000.")
   public static final String SELECT_VALID_TIME_PERIOD = "57";

   @RBEntry("Sleziona il momento d'inizio")
   public static final String SELECT_START_TIME_TOOLTIP = "58";

   @RBEntry("Seleziona il momento di fine")
   public static final String SELECT_END_TIME_TOOLTIP = "59";

   @RBEntry("Seleziona data e ora")
   public static final String TIME_CHOOSER_TITLE = "60";

   @RBEntry("Seleziona unità di tempo")
   public static final String SELECT_TIME_UNIT_TOOLTIP = "61";

   @RBEntry("Immetti intervallo di tempo")
   public static final String TYPE_TIME_INTERVAL_TOOLTIP = "62";

   @RBEntry("Ore:")
   public static final String HOURS_LABEL = "63";

   @RBEntry("Minuti:")
   public static final String MINUTES_LABEL = "64";

   @RBEntry("Immettere l'oggetto di livello superiore")
   public static final String NAVIGATION_DIALOG_PANEL_TITLE = "65";

   @RBEntry("Gestione importazioni/esportazioni")
   public static final String EXPORT_IMPORT_MANAGER_TITLE = "66";

   @RBEntry("Anteprima")
   public static final String PREVIEW_LABEL = "67";

   @RBEntry(" {0} esiste già.\n Sostituirlo?")
   public static final String REPLACE_EXISTING_EXPORT_FILE = "68";

   @RBEntry("Salva con nome")
   public static final String SAVE_AS_WARNING_DIALOG_TITLE = "69";

   @RBEntry("Sì")
   public static final String YES_LABEL = "70";

   @RBEntry("No")
   public static final String NO_LABEL = "71";

   @RBEntry("Guida")
   public static final String HELP_LABEL = "72";

   @RBEntry("Anteprima dei risultati dell'esportazione")
   public static final String EXPORT_PREVIEW_TOOLTIP = "73";

   @RBEntry("Avvia il processo di esportazione")
   public static final String EXPORT_SUBMIT_TOOLTIP = "74";

   @RBEntry("Chiude la finestra di esportazione")
   public static final String EXPORT_EXIT_TOOLTIP = "75";

   @RBEntry("Visualizza la guida in linea")
   public static final String HELP_TOOLTIP = "76";

   @RBEntry("Seleziona il tipo di struttura da aggiungere")
   public static final String SELECT_STRUCTURE_OK_TOOLTIP = "77";

   @RBEntry("Termina il task di selezione corrente")
   public static final String SELECT_STRUCTURE_CANCEL_TOOLTIP = "78";

   @RBEntry("Aggiunge l'oggetto selezionato all'elenco degli oggetti")
   public static final String NAVIGATOR_SEARCH_OK_TOOLTIP = "79";

   @RBEntry("Termina il task di ricerca")
   public static final String NAVIGATOR_SEARCH_CANCEL_TOOLTIP = "80";

   @RBEntry("Modifica la data")
   public static final String DATE_MODIFIED_OK_TOOLTIP = "81";

   @RBEntry("Termina il task di modifica data")
   public static final String DATE_MODIFIED_CANCEL_TOOLTIP = "82";

   @RBEntry("Avvia il processo d'importazione")
   public static final String IMPORT_SUBMIT_TOOLTIP = "83";

   @RBEntry("Chiude la finestra di importazione")
   public static final String IMPORT_EXIT_TOOLTIP = "84";

   @RBEntry("Log dettagliato")
   public static final String DETAILED_LOG_STRING = "85";

   @RBEntry("Seleziona filtro da aggiungere")
   public static final String ADD_FILTER_TO_LIST_DIALOG_TITLE = "86";

   @RBEntry("Messaggio di errore")
   public static final String ERROR_MESSAGE = "87";

   @RBEntry("Impossibile salvare il log. \nMotivo: {0}")
   public static final String SAVE_LOG_ERROR = "88";

   @RBEntry("Salva log...")
   public static final String SAVE_LOG_LABEL = "89";

   @RBEntry("Salva il log nel file")
   public static final String SAVE_LOG_TOOLTIP = "90";

   @RBEntry("Il file designato a contenere i dati di esportazione deve essere accessibile in scritttura")
   public static final String EXPORT_FILE_MUST_BE_WRITEABLE = "91";

   @RBEntry("L'utente non dispone dei permessi di scrittura per il file di esportazione")
   public static final String EXPORT_FILE_DIALOG_TITLE = "92";

   @RBEntry("File di regole inesistente")
   public static final String RULES_FILE_DIALOG_TITLE = "94";

   @RBEntry("Arresta processo esportazione")
   public static final String CANCEL_EXPORT_TOOLTIP = "95";

   @RBEntry("Arresta il processo d'importazione")
   public static final String CANCEL_IMPORT_TOOLTIP = "951";

   @RBEntry("Ore")
   public static final String HOURS_LABEL_2 = "96";

   /**
    * The following part is added to fix the SPR 884543
    **/
   @RBEntry("Esportazione in corso...")
   public static final String EXPORT_PROGRESS = "97";

   @RBEntry("RISULTATO ESPORTAZIONE :")
   public static final String EXPORT_RESULT = "98";

   @RBEntry("COMPLETATO")
   public static final String COMPLETED = "99";

   @RBEntry("NON RIUSCITO")
   public static final String FAILED = "100";

   @RBEntry("Importazione in corso...")
   public static final String IMPORT_PROGRESS = "101";

   @RBEntry("RISULTATO IMPORTAZIONE:")
   public static final String IMPORT_RESULT = "102";

   @RBEntry("Interruzione completata.")
   public static final String ABORT_COMPLETED = "103";

   @RBEntry("Errore durante la comunicazione con il server")
   public static final String ERROR_COMM_WITH_SERVER = "104";

   /**
    * End fix part of SPR 884543
    **/
   @RBEntry("Verificare i permessi di scrittura di file e cartella.\nAccesso in scrittura negato: {0}")
   @RBArgComment0(" argument is file name to be displayed")
   public static final String FILE_WRITE_ACCESS_DENIED = "105";

   /**
    * The sentence will look like "During the previous 5 Hours"
    **/
   @RBEntry("Durante le precedenti")
   public static final String PREVIOUS_HOURS_LABEL = "106";

   /**
    * The sentence will look like "During the previous 7 Days"
    **/
   @RBEntry("Durante i precedenti")
   public static final String PREVIOUS_DAYS_LABEL = "107";

   /**
    * The sentence will look like "During the previous 9 Months"
    **/
   @RBEntry("Durante i precedenti")
   public static final String PREVIOUS_MONTHS_LABEL = "108";

   @RBEntry("Non è stato trovato alcun oggetto")
   public static final String NO_OBJECTS_FOUND = "109";

   @RBEntry("Nessun oggetto corrispondente ai criteri di ricerca")
   public static final String NO_OBJECTS_MATCH_SEARCH_CRITERIA = "110";

   @RBEntry("Il file delle regole non esiste.\n")
   public static final String RULES_FILE_DOES_NOT_EXIST = "111";

   @RBEntry("Il file dei dati di importazione non esiste.\n")
   public static final String DATA_FILE_DOES_NOT_EXIST = "112";

   @RBEntry("Non è stato specificato il file dei dati di importazione.\n")
   public static final String DATA_FILE_IS_NOT_SPECIFIED = "113";

   @RBEntry("Impossibile procedere all'importazione.\n")
   public static final String CANNOT_PROCEED_TO_IMPORT = "114";

   @RBEntry("Chiamata server terminata con errore.\n")
   public static final String SERVER_INVOCATION_ERROR = "115";

   @RBEntry("Seleziona il filtro da aggiungere")
   public static final String SELECT_FILTER_OK_TOOLTIP = "116";

   @RBEntry("Specificare un file d'esportazione.")
   public static final String SPECIFY_EXPORT_FILE = "117";

   @RBEntry("Il file d'\"esportazione\" non è stato specificato.")
   public static final String EXPORT_DIALOG_TITLE = "118";

   @RBEntry("Errore: gestore esportazione mancante per la classe.")
   public static final String EXPORT_OBJECT_NO_HANDLER = "119";

   @RBEntry("Esporta oggetti")
   public static final String EXPORT_OBJECTS_LABEL = "120";

   @RBEntry("L'ora e la data indicate non sono valide. \nEsempio di ora e data valide :")
   public static final String INVALID_DATE_AND_TIME_STRING = "121";

   @RBEntry("Formato ora e data del campo \"Da\" non valido")
   public static final String INVALID_FROM_DATE_AND_TIME_TITLE = "122";

   @RBEntry("Formato ora e data del campo \"A\" non valido")
   public static final String INVALID_TO_DATE_AND_TIME_TITLE = "123";

   @RBEntry("Anteprima risultati importazione")
   public static final String IMPORT_PREVIEW_TOOLTIP = "124";

   @RBEntry("ID navigatore")
   public static final String NAVIGATOR_IDS_TITLE = "125";

   @RBEntry("Importazione :")
   public static final String IMPORT_ACTION_LABEL = "200";

   @RBEntry("File criteri d'importazione:")
   public static final String IMPORT_POLICY_FILE_LABEL = "201";

   @RBEntry("Cerca il file dei criteri")
   public static final String BROWSE_FOR_POLICY_FILE_TOOLTIP = "202";

   @RBEntry("Il file dei criteri non esiste.\n")
   public static final String POLICY_FILE_DOES_NOT_EXIST = "203";

   @RBEntry("File criteri d'esportazione :")
   public static final String EXPORT_POLICY_FILE_LABEL = "204";

   @RBEntry("Esportazione :")
   public static final String EXPORT_ACTION_LABEL = "205";

   @RBEntry("Non è stato specificato il file dei criteri di importazione. Importare gli oggetti come da default?")
   public static final String IMPORT_POLICY_FILE_IS_NOT_SPECIFIED = "206";

   @RBEntry("Specificare le date \"Da\" e \"A\".")
   public static final String BOTH_START_AND_END_DATES_ARE_NULL = "207";

   @RBEntry("Contesto di destinazione di default: \"{0}\"")
   public static final String DEFAULT_TARGET_CONTEXT_LABEL = "208";

   @RBEntry("Contesto di origine: \"{0}\"")
   public static final String SOURCE_CONTEXT_LABEL = "209";

   @RBEntry("File mappatura contesto:")
   public static final String CONTEXT_MAPPING_FILE_LABEL = "210";

   @RBEntry(" Cerca file di mappatura contesto")
   public static final String BROWSE_FOR_CONTEXT_MAPPING_FILE_TOOLTIP = "211";

   @RBEntry(" Cerca file")
   public static final String FILE_DIALOG_TITLE = "212";

   @RBEntry("Nessun oggetto selezionato")
   public static final String SELECT_NO_OBJECT_TITLE = "213";

   @RBEntry(" Selezionare almeno un oggetto o fare clic sul pulsante Annulla.")
   public static final String PLEASE_SELECT_OBJECT = "214";

   @RBEntry("Non è stato specificato il file dei criteri di esportazione. Esportare gli oggetti come da default?")
   public static final String EXPORT_POLICY_FILE_IS_NOT_SPECIFIED = "215";

   @RBEntry(" Non è stata selezionata alcuna struttura")
   public static final String SELECT_NO_STRUCTURE_TITLE = "216";

   @RBEntry(" Selezionare una struttura o fare clic sul pulsante Annulla.")
   public static final String PLEASE_SELECT_STRUCTURE = "217";

   @RBEntry(" Non è stato selezionato alcun filtro")
   public static final String SELECT_NO_FILTER_TITLE = "218";

   @RBEntry(" Selezionare un filtro o fare clic sul pulsante Annulla.")
   public static final String PLEASE_SELECT_FILTER = "219";

   @RBEntry("Specifica di configurazione")
   public static final String CONFIGSPEC_PANEL_LABEL = "300";

   @RBEntry("Imposta spec config")
   public static final String SET_CONFIGSPEC_LABEL = "301";

   @RBEntry("Imposta la spec di config per navigare nella struttura del prodotto")
   public static final String SET_CONFIGSPEC_TOOLTIP = "302";

   @RBEntry("Imposta la specifica di configurazione per navigare nella struttura di prodotto")
   public static final String CONFIGSPEC_DIALOG_TITLE = "303";

   @RBEntry("Esporta gli oggetti di livello superiore in un jar di jar")
   public static final String JAR_OF_JAR_STRING = "304";

   @RBEntry("Autenticazione non riuscita.")
   public static final String AUTH_FAILED = "305";

   @RBEntry("Autenticazione con il server non riuscita. Impossibile aprire la finestra di dialogo.")
   public static final String AUTH_FAILED_MESSAGE = "306";

   @RBEntry("Nel presente contesto, l'utente non dispone del permesso di eseguire esportazioni.")
   public static final String INSUFFICIENT_PERMISSION = "307";

   @RBEntry("(Informazioni protette)")
   @RBComment("Used in place of the display name for an attribute or item in a list, to indicate that the current user does not have access to the associated object.")
   public static final String SECURED_INFORMATION = "308";
   
   @RBEntry("dd/MM/yyyy, HH.mm")
   @RBPseudo(false)
   @RBComment("The format in which timestamp values will be displayed. Do not translate the date characters (M d y), but modify the date syntax according to your locale-specific date syntax.")
   public static final String TIME_FILTER_STANDARD_DATE_FORMAT = "309";
   
   @RBEntry("gg/MM/aaaa, HH.mm")
   @RBPseudo(false)
   @RBComment("This is an output display format showing the full date with hours and minutes.")
   public static final String TIME_FILTER_DATE_DISPLAY_FORMAT = "310";
}
