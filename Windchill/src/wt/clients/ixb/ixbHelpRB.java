/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.ixb;

import wt.util.resource.*;

@RBUUID("wt.clients.ixb.ixbHelpRB")
public final class ixbHelpRB extends WTListResourceBundle {
   @RBEntry("ExportImportExportAbout")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_0 = "Help/ExportImport/ExportFrameContext";

   @RBEntry("ExportImportExpSelectStruct")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_1 = "Help/ExportImport/SelectStructureTypeContext";

   @RBEntry("ExportImportExpSelectObjs")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/ExportImport/NavigationSearchContext";

   @RBEntry("ExportImportExpDateModDialog")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_3 = "Help/ExportImport/DateModifiedContext";

   @RBEntry("ExportImportImportAbout")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_4 = "Help/ExportImport/ImportFrameContext";

   @RBEntry("ExportImportExpInitFilter")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_5 = "Help/ExportImport/SelectFilterContext";
}