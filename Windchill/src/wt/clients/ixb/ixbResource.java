/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.ixb;

import wt.util.resource.*;

@RBUUID("wt.clients.ixb.ixbResource")
public final class ixbResource extends WTListResourceBundle {
   @RBEntry("Export")
   public static final String EXPORT_FRAME_TITLE = "0";

   @RBEntry("Submit")
   public static final String SUBMIT_LABEL = "1";

   @RBEntry("Exit")
   public static final String EXIT_LABEL = "2";

   @RBEntry("Select Structure Type to Add")
   public static final String ADD_ITEM_TO_LIST_DIALOG_TITLE = "3";

   @RBEntry("Objects for Export")
   public static final String ELEMENTS_PANEL_TITLE = "4";

   @RBEntry("Add...")
   public static final String ADD_LABEL = "5";

   @RBEntry("Add object to list")
   public static final String ADD_TO_ELEMENTS_LIST_TOOLTIP = "6";

   @RBEntry("Delete")
   public static final String DELETE_LABEL = "7";

   @RBEntry("Remove object from list")
   public static final String DELETE_FROM_ELEMENT_LIST_TOOLTIP = "8";

   @RBEntry("Objects")
   public static final String ELEMENT_LIST_TITLE = "9";

   @RBEntry("Export Status Log")
   public static final String EXPORT_STATUS_TITLE = "10";

   @RBEntry("Filters")
   public static final String FILTER_LIST_TITLE = "11";

   @RBEntry("Add filter(s) to list")
   public static final String ADD_TO_FILTERS_LIST_TOOLTIP = "12";

   @RBEntry("Remove filter(s) from list")
   public static final String DELETE_FROM_FILTER_LIST_TOOLTIP = "13";

   @RBEntry("Please specify a \"Save As\" file!")
   public static final String SPECIFY_SAVE_AS_FILE = "14";

   @RBEntry("\"\"Save As\" File is not specified!")
   public static final String SAVE_AS_DIALOG_TITLE = "15";

   @RBEntry("Please add an object to the element list!")
   public static final String ADD_ITEM_TO_ELEMENT_LIST = "16";

   @RBEntry("List of elements is empty!")
   public static final String EMPTY_LIST_DIALOG_TITLE = "17";

   @RBEntry("Search the top level object")
   public static final String NAVIGATION_DIALOG_TITLE = "18";

   @RBEntry("Top level object:")
   public static final String TOP_LEVEL_OBJECT_LABEL = "19";

   @RBEntry("Search...")
   public static final String FIND_LABEL = "20";

   @RBEntry("Find top level object")
   public static final String NAVIGATOR_DIALOG_FIND_TOOLTIP = "21";

   @RBEntry("Navigator Search Dialog")
   public static final String NAVIGATOR_SEARCH_TITLE = "22";

   @RBEntry("Export Components")
   public static final String MESSAGE_COMPONENTS_PANEL_TITLE = "23";

   @RBEntry("Find the message based on its components.")
   public static final String NAVIGATOR_SEARCH_DIALOG_FIND_TOOLTIP = "24";

   @RBEntry("Import Status Log")
   public static final String IMPORT_STATUS_TITLE = "25";

   @RBEntry("Import")
   public static final String IMPORT_FRAME_TITLE = "26";

   @RBEntry("Import Rule File:")
   public static final String IMPORT_RULES_FILE = "27";

   @RBEntry("Import File Name:")
   public static final String IMPORT_DATA_FILE = "28";

   @RBEntry("Browse...")
   public static final String BROWSE_LABEL = "29";

   @RBEntry("Browse for mapping rule file")
   public static final String BROWSE_FOR_RULES_FILE_TOOLTIP = "30";

   @RBEntry("Browse for import file")
   public static final String BROWSE_FOR_DATA_FILE_TOOLTIP = "31";

   @RBEntry("Export Rule File:")
   public static final String MAPPING_RULES_LABEL = "32";

   @RBEntry("Browse for mapping rule file")
   public static final String BROWSE_MAP_RULES_TOOLTIP = "34";

   @RBEntry("Export File Name:")
   public static final String SAVE_AS_LABEL = "35";

   @RBEntry("Browse for pathname of export file")
   public static final String BROWSE_SAVE_AS_TOOLTIP = "36";

   @RBEntry("OK")
   public static final String OK_LABEL = "37";

   @RBEntry("Cancel")
   public static final String CANCEL_LABEL = "38";

   @RBEntry("Resolve Overridable Conflicts")
   public static final String RESOLVE_CONFLICTS_STRING = "39";

   @RBEntry("Import Settings")
   public static final String IMPORT_SETTINGS_PANEL_TITLE = "40";

   @RBEntry("Elements invisible in the user's configuration specification will be filtered out")
   public static final String FILTER_BY_CONFIG_SPEC_MESSAGE = "41";

   @RBEntry("Filter by Configuration Specification")
   public static final String FILTER_BY_CONFIG_SPEC_TITLE = "42";

   @RBEntry("Select")
   public static final String SELECT_LABEL = "43";

   @RBEntry("Date Modified")
   public static final String FILTER_BY_TIME_TITLE = "44";

   @RBEntry("Include all objects created or modified:")
   public static final String FILTER_BY_TIME_MESSAGE = "45";

   @RBEntry("Time range")
   public static final String TIME_RANGE_LABEL = "46";

   @RBEntry("During the previous")
   public static final String PREVIOUS_TIME_LABEL = "47";

   @RBEntry("Days")
   public static final String DAYS_LABEL = "49";

   @RBEntry("Months")
   public static final String MONTHS_LABEL = "50";

   @RBEntry("From:")
   public static final String FROM_TIME = "51";

   @RBEntry("To:")
   public static final String TO_TIME = "52";

   @RBEntry("\"From\" date is not specified!")
   public static final String START_DATE_IS_NULL = "53";

   @RBEntry("\"To\" date is not specified!")
   public static final String END_DATE_IS_NULL = "54";

   @RBEntry("\"To\" date precedes the \"From\" date!")
   public static final String TO_DATE_PRECEDES_FROM_DATE = "55";

   @RBEntry("Time period is not valid!")
   public static final String TIME_PERIOD_IS_NOT_VALID = "56";

   @RBEntry("Please select a valid time period!\n The valid time period is a positive integer no greater than 1000.")
   public static final String SELECT_VALID_TIME_PERIOD = "57";

   @RBEntry("Select start time")
   public static final String SELECT_START_TIME_TOOLTIP = "58";

   @RBEntry("Select end time")
   public static final String SELECT_END_TIME_TOOLTIP = "59";

   @RBEntry("Select date and time")
   public static final String TIME_CHOOSER_TITLE = "60";

   @RBEntry("Select time unit")
   public static final String SELECT_TIME_UNIT_TOOLTIP = "61";

   @RBEntry("Type time interval")
   public static final String TYPE_TIME_INTERVAL_TOOLTIP = "62";

   @RBEntry("Hours:")
   public static final String HOURS_LABEL = "63";

   @RBEntry("Minutes:")
   public static final String MINUTES_LABEL = "64";

   @RBEntry("Enter the top level object")
   public static final String NAVIGATION_DIALOG_PANEL_TITLE = "65";

   @RBEntry("Import/Export Management")
   public static final String EXPORT_IMPORT_MANAGER_TITLE = "66";

   @RBEntry("Preview")
   public static final String PREVIEW_LABEL = "67";

   @RBEntry(" {0} already exists.\n Do you want to replace it?")
   public static final String REPLACE_EXISTING_EXPORT_FILE = "68";

   @RBEntry("Save As")
   public static final String SAVE_AS_WARNING_DIALOG_TITLE = "69";

   @RBEntry("Yes")
   public static final String YES_LABEL = "70";

   @RBEntry("No")
   public static final String NO_LABEL = "71";

   @RBEntry("Help")
   public static final String HELP_LABEL = "72";

   @RBEntry("Preview export results")
   public static final String EXPORT_PREVIEW_TOOLTIP = "73";

   @RBEntry("Start export process")
   public static final String EXPORT_SUBMIT_TOOLTIP = "74";

   @RBEntry("Exit Export window")
   public static final String EXPORT_EXIT_TOOLTIP = "75";

   @RBEntry("Display online help")
   public static final String HELP_TOOLTIP = "76";

   @RBEntry("Select structure type to add")
   public static final String SELECT_STRUCTURE_OK_TOOLTIP = "77";

   @RBEntry("Close this selection task")
   public static final String SELECT_STRUCTURE_CANCEL_TOOLTIP = "78";

   @RBEntry("Add selected object to the list of objects")
   public static final String NAVIGATOR_SEARCH_OK_TOOLTIP = "79";

   @RBEntry("Close this search task")
   public static final String NAVIGATOR_SEARCH_CANCEL_TOOLTIP = "80";

   @RBEntry("Modify the date")
   public static final String DATE_MODIFIED_OK_TOOLTIP = "81";

   @RBEntry("Close the modify date task")
   public static final String DATE_MODIFIED_CANCEL_TOOLTIP = "82";

   @RBEntry("Start import process")
   public static final String IMPORT_SUBMIT_TOOLTIP = "83";

   @RBEntry("Exit Import window")
   public static final String IMPORT_EXIT_TOOLTIP = "84";

   @RBEntry("Detailed Log")
   public static final String DETAILED_LOG_STRING = "85";

   @RBEntry("Select Filter to add")
   public static final String ADD_FILTER_TO_LIST_DIALOG_TITLE = "86";

   @RBEntry("Error Message")
   public static final String ERROR_MESSAGE = "87";

   @RBEntry("Can not save log. \nThe reason is: {0}")
   public static final String SAVE_LOG_ERROR = "88";

   @RBEntry("Save log...")
   public static final String SAVE_LOG_LABEL = "89";

   @RBEntry("Save log to file")
   public static final String SAVE_LOG_TOOLTIP = "90";

   @RBEntry("File designated to hold the export data must be writeable!")
   public static final String EXPORT_FILE_MUST_BE_WRITEABLE = "91";

   @RBEntry("You do not have write permission for the export file")
   public static final String EXPORT_FILE_DIALOG_TITLE = "92";

   @RBEntry("Rule file does not exist")
   public static final String RULES_FILE_DIALOG_TITLE = "94";

   @RBEntry("Stop export process")
   public static final String CANCEL_EXPORT_TOOLTIP = "95";

   @RBEntry("Stop import process")
   public static final String CANCEL_IMPORT_TOOLTIP = "951";

   @RBEntry("Hours")
   public static final String HOURS_LABEL_2 = "96";

   /**
    * The following part is added to fix the SPR 884543
    **/
   @RBEntry("Export is in progress ...")
   public static final String EXPORT_PROGRESS = "97";

   @RBEntry("EXPORT RESULT :")
   public static final String EXPORT_RESULT = "98";

   @RBEntry("COMPLETED")
   public static final String COMPLETED = "99";

   @RBEntry("FAILED")
   public static final String FAILED = "100";

   @RBEntry("Import is in progress ...")
   public static final String IMPORT_PROGRESS = "101";

   @RBEntry(" IMPORT RESULT :")
   public static final String IMPORT_RESULT = "102";

   @RBEntry(" Abort completed.")
   public static final String ABORT_COMPLETED = "103";

   @RBEntry("Error communicating with the server!!!")
   public static final String ERROR_COMM_WITH_SERVER = "104";

   /**
    * End fix part of SPR 884543
    **/
   @RBEntry("Please verify folder and file write permissions.\nWrite access denied: {0}")
   @RBArgComment0(" argument is file name to be displayed")
   public static final String FILE_WRITE_ACCESS_DENIED = "105";

   /**
    * The sentence will look like "During the previous 5 Hours"
    **/
   @RBEntry("During the previous")
   public static final String PREVIOUS_HOURS_LABEL = "106";

   /**
    * The sentence will look like "During the previous 7 Days"
    **/
   @RBEntry("During the previous")
   public static final String PREVIOUS_DAYS_LABEL = "107";

   /**
    * The sentence will look like "During the previous 9 Months"
    **/
   @RBEntry("During the previous")
   public static final String PREVIOUS_MONTHS_LABEL = "108";

   @RBEntry("No objects found")
   public static final String NO_OBJECTS_FOUND = "109";

   @RBEntry("No objects match specified search criteria")
   public static final String NO_OBJECTS_MATCH_SEARCH_CRITERIA = "110";

   @RBEntry("Rule File does not exist!!!\n")
   public static final String RULES_FILE_DOES_NOT_EXIST = "111";

   @RBEntry("Import Data File does not exist!!!\n")
   public static final String DATA_FILE_DOES_NOT_EXIST = "112";

   @RBEntry("Import Data File is not specified!!!\n")
   public static final String DATA_FILE_IS_NOT_SPECIFIED = "113";

   @RBEntry("Can not proceed to Import!\n")
   public static final String CANNOT_PROCEED_TO_IMPORT = "114";

   @RBEntry("Server Invocation Finished with error!!!\n")
   public static final String SERVER_INVOCATION_ERROR = "115";

   @RBEntry("Select filter to add")
   public static final String SELECT_FILTER_OK_TOOLTIP = "116";

   @RBEntry("Please specify an \"Export\" file!")
   public static final String SPECIFY_EXPORT_FILE = "117";

   @RBEntry("\"Export\" File is not specified!")
   public static final String EXPORT_DIALOG_TITLE = "118";

   @RBEntry("Error: There is no export handler for the class")
   public static final String EXPORT_OBJECT_NO_HANDLER = "119";

   @RBEntry("Export Objects")
   public static final String EXPORT_OBJECTS_LABEL = "120";

   @RBEntry("You entered invalid date and time. \nExample for a valid date and time format :")
   public static final String INVALID_DATE_AND_TIME_STRING = "121";

   @RBEntry("Invalid \"From\" date and time format")
   public static final String INVALID_FROM_DATE_AND_TIME_TITLE = "122";

   @RBEntry("Invalid \"To\" date and time format")
   public static final String INVALID_TO_DATE_AND_TIME_TITLE = "123";

   @RBEntry("Preview import results")
   public static final String IMPORT_PREVIEW_TOOLTIP = "124";

   @RBEntry("Navigator Ids")
   public static final String NAVIGATOR_IDS_TITLE = "125";

   @RBEntry("Import Action:")
   public static final String IMPORT_ACTION_LABEL = "200";

   @RBEntry("Import Policy File :")
   public static final String IMPORT_POLICY_FILE_LABEL = "201";

   @RBEntry("Browse for policy file")
   public static final String BROWSE_FOR_POLICY_FILE_TOOLTIP = "202";

   @RBEntry("Policy File does not exist!!!\n")
   public static final String POLICY_FILE_DOES_NOT_EXIST = "203";

   @RBEntry("Export Policy File :")
   public static final String EXPORT_POLICY_FILE_LABEL = "204";

   @RBEntry("Export Action :")
   public static final String EXPORT_ACTION_LABEL = "205";

   @RBEntry("Import policy file is not specified. Import objects as default?")
   public static final String IMPORT_POLICY_FILE_IS_NOT_SPECIFIED = "206";

   @RBEntry("Both \"From\" and \"To\" dates are not specified!")
   public static final String BOTH_START_AND_END_DATES_ARE_NULL = "207";

   @RBEntry("Default Target Context: \"{0}\"")
   public static final String DEFAULT_TARGET_CONTEXT_LABEL = "208";

   @RBEntry("Source Context: \"{0}\"")
   public static final String SOURCE_CONTEXT_LABEL = "209";

   @RBEntry("Context-mapping file :")
   public static final String CONTEXT_MAPPING_FILE_LABEL = "210";

   @RBEntry(" Browse for context-mapping file")
   public static final String BROWSE_FOR_CONTEXT_MAPPING_FILE_TOOLTIP = "211";

   @RBEntry(" Browse for file")
   public static final String FILE_DIALOG_TITLE = "212";

   @RBEntry(" No object selected")
   public static final String SELECT_NO_OBJECT_TITLE = "213";

   @RBEntry(" Please select at least one object or click the Cancel button.")
   public static final String PLEASE_SELECT_OBJECT = "214";

   @RBEntry("Export policy file is not specified. Export objects as default?")
   public static final String EXPORT_POLICY_FILE_IS_NOT_SPECIFIED = "215";

   @RBEntry(" No structure selected")
   public static final String SELECT_NO_STRUCTURE_TITLE = "216";

   @RBEntry(" Please select a structure or click the Cancel button.")
   public static final String PLEASE_SELECT_STRUCTURE = "217";

   @RBEntry(" No filter selected")
   public static final String SELECT_NO_FILTER_TITLE = "218";

   @RBEntry(" Please select a filter or click the Cancel button.")
   public static final String PLEASE_SELECT_FILTER = "219";

   @RBEntry("Configuration Specification")
   public static final String CONFIGSPEC_PANEL_LABEL = "300";

   @RBEntry("Set Config Spec")
   public static final String SET_CONFIGSPEC_LABEL = "301";

   @RBEntry("Set Config Spec for navigating Product Structure")
   public static final String SET_CONFIGSPEC_TOOLTIP = "302";

   @RBEntry("Set Configuration Specification for navigating Product Structure")
   public static final String CONFIGSPEC_DIALOG_TITLE = "303";

   @RBEntry("Export top level objects in jar of jars")
   public static final String JAR_OF_JAR_STRING = "304";

   @RBEntry("Authentication Failed.")
   public static final String AUTH_FAILED = "305";

   @RBEntry("Authenication with server failed, can not open dialog.")
   public static final String AUTH_FAILED_MESSAGE = "306";

   @RBEntry("You do not have necessary context permission for export.")
   public static final String INSUFFICIENT_PERMISSION = "307";

   @RBEntry("(Secured information)")
   @RBComment("Used in place of the display name for an attribute or item in a list, to indicate that the current user does not have access to the associated object.")
   public static final String SECURED_INFORMATION = "308";
   
   @RBEntry("yyyy-MM-dd HH:mm")
   @RBPseudo(false)
   @RBComment("The format in which timestamp values will be displayed. Do not translate the date characters (M d y), but modify the date syntax according to your locale-specific date syntax.")
   public static final String TIME_FILTER_STANDARD_DATE_FORMAT = "309";
   
   @RBEntry("yyyy-MM-dd HH:mm")
   @RBPseudo(false)
   @RBComment("This is an output display format showing the full date with hours and minutes.")
   public static final String TIME_FILTER_DATE_DISPLAY_FORMAT = "310";
}
