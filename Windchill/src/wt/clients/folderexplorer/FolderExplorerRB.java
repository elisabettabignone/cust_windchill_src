/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.folderexplorer;

import wt.util.resource.*;

@RBUUID("wt.clients.folderexplorer.FolderExplorerRB")
public final class FolderExplorerRB extends WTListResourceBundle {
   /**
    * FIELD LABELS
    **/
   @RBEntry("Type:")
   public static final String PRIVATE_CONSTANT_0 = "typeLbl";

   @RBEntry("Folder Name:")
   public static final String PRIVATE_CONSTANT_1 = "folderNameLbl";

   @RBEntry("Name")
   public static final String PRIVATE_CONSTANT_2 = "name";

   /**
    * BUTTON LABELS
    **/
   @RBEntry("Cancel")
   public static final String PRIVATE_CONSTANT_3 = "cancelButton";

   @RBEntry("OK")
   public static final String PRIVATE_CONSTANT_4 = "okButton";

   /**
    * MULTILIST HEADINGS
    **/
   @RBEntry("Number")
   public static final String PRIVATE_CONSTANT_5 = "number";

   @RBEntry("Object Type")
   public static final String PRIVATE_CONSTANT_6 = "objectType";

   @RBEntry("Version")
   public static final String PRIVATE_CONSTANT_7 = "version";

   @RBEntry("State")
   public static final String PRIVATE_CONSTANT_8 = "state";

   @RBEntry("Type")
   public static final String PRIVATE_CONSTANT_9 = "type";

   @RBEntry("Last Modified")
   public static final String PRIVATE_CONSTANT_10 = "modified";

   @RBEntry("Team")
   public static final String PRIVATE_CONSTANT_11 = "team";

   @RBEntry("Organization ID")
   public static final String PRIVATE_CONSTANT_12 = "organizationId";

   /**
    * MENU LABELS
    **/
   @RBEntry("File")
   public static final String PRIVATE_CONSTANT_13 = "file";

   @RBEntry("Edit")
   public static final String PRIVATE_CONSTANT_14 = "edit";

   @RBEntry("Object")
   public static final String PRIVATE_CONSTANT_15 = "object";

   @RBEntry("Life Cycle")
   public static final String PRIVATE_CONSTANT_16 = "lifecycle";

   @RBEntry("Tools")
   public static final String PRIVATE_CONSTANT_17 = "tools";

   @RBEntry("Windchill")
   public static final String PRIVATE_CONSTANT_18 = "windchill";

   @RBEntry("Window")
   public static final String PRIVATE_CONSTANT_19 = "window";

   @RBEntry("Help")
   public static final String PRIVATE_CONSTANT_20 = "help";

   /**
    * MENU ITEM LABELS
    **/
   @RBEntry("New")
   public static final String PRIVATE_CONSTANT_21 = "new";

   @RBEntry("Update")
   public static final String PRIVATE_CONSTANT_22 = "update";

   @RBEntry("Rename")
   public static final String PRIVATE_CONSTANT_23 = "rename";

   @RBEntry("Delete")
   public static final String PRIVATE_CONSTANT_24 = "delete";

   @RBEntry("Refresh")
   public static final String PRIVATE_CONSTANT_25 = "refresh";

   @RBEntry("Close")
   public static final String PRIVATE_CONSTANT_26 = "close";

   @RBEntry("Cut")
   public static final String PRIVATE_CONSTANT_27 = "cut";

   @RBEntry("Copy")
   public static final String PRIVATE_CONSTANT_28 = "copy";

   @RBEntry("Paste")
   public static final String PRIVATE_CONSTANT_29 = "paste";

   @RBEntry("Check In")
   public static final String PRIVATE_CONSTANT_30 = "checkin";

   @RBEntry("Check Out")
   public static final String PRIVATE_CONSTANT_31 = "checkout";

   @RBEntry("Undo Check Out")
   public static final String PRIVATE_CONSTANT_32 = "undoCheckout";

   @RBEntry("Get Content")
   public static final String PRIVATE_CONSTANT_33 = "getContent";

   @RBEntry("View")
   public static final String PRIVATE_CONSTANT_34 = "view";

   @RBEntry("Revise")
   public static final String PRIVATE_CONSTANT_35 = "revise";

   @RBEntry("New View Version")
   public static final String PRIVATE_CONSTANT_36 = "newViewVersion";

   @RBEntry("Submit")
   public static final String PRIVATE_CONSTANT_37 = "submit";

   @RBEntry("Update Team")
   public static final String PRIVATE_CONSTANT_38 = "augmentLifecycle";

   @RBEntry("Reassign Team")
   public static final String PRIVATE_CONSTANT_39 = "reassignTeam";

   @RBEntry("Reassign Life Cycle")
   public static final String PRIVATE_CONSTANT_40 = "reassignTemplate";

   @RBEntry("Set State")
   public static final String PRIVATE_CONSTANT_41 = "setLifeCycleState";

   @RBEntry("Iteration History")
   public static final String PRIVATE_CONSTANT_42 = "iterationHistory";

   @RBEntry("All Versions")
   public static final String PRIVATE_CONSTANT_43 = "versionHistory";

   @RBEntry("Life Cycle History")
   public static final String PRIVATE_CONSTANT_44 = "lcHistory";

   @RBEntry("Search")
   public static final String PRIVATE_CONSTANT_45 = "searchEnterprise";

   @RBEntry("Search in Folders")
   public static final String PRIVATE_CONSTANT_46 = "searchLocal";

   @RBEntry("Product Structure Explorer")
   public static final String PRIVATE_CONSTANT_47 = "productInfoExp";

   @RBEntry("Windchill Explorer")
   public static final String PRIVATE_CONSTANT_48 = "windchillExplorer";

   @RBEntry("Help Topics")
   public static final String PRIVATE_CONSTANT_49 = "helpTopics";

   @RBEntry("About Windchill")
   public static final String PRIVATE_CONSTANT_50 = "aboutWindchill";

   @RBEntry("Baseline")
   public static final String PRIVATE_CONSTANT_51 = "baseline";

   @RBEntry("Add...")
   public static final String PRIVATE_CONSTANT_52 = "addBaseline";

   @RBEntry("Remove...")
   public static final String PRIVATE_CONSTANT_53 = "removeBaseline";

   @RBEntry("Populate...")
   public static final String PRIVATE_CONSTANT_54 = "populateBaseline";

   @RBEntry("Show Team")
   public static final String PRIVATE_CONSTANT_55 = "roleParticipants";

   @RBEntry("History")
   public static final String PRIVATE_CONSTANT_56 = "history";

   /**
    * JSP PAGE MESSAG#ES
    **/
   @RBEntry("Document Manager")
   public static final String PRIVATE_CONSTANT_57 = "explorerTitle";

   @RBEntry("Windchill Explorer")
   public static final String PRIVATE_CONSTANT_58 = "explorerToolTip";

   @RBEntry("Go to Windchill Home Page")
   public static final String PRIVATE_CONSTANT_59 = "windchillHomeToolTip";

   @RBEntry("Windchill")
   public static final String PRIVATE_CONSTANT_60 = "windchillToolTip";

   /**
    * STATUS MESSAGES
    **/
   @RBEntry("Retrieving cabinets...  ")
   public static final String INITIALIZING_CABINETS = "51";

   @RBEntry("Displaying cabinets...")
   public static final String INITIALIZING_EXPLORER = "52";

   @RBEntry("Attempt to retrieve cabinets failed.")
   public static final String INITIALIZE_CABINETS_FAILED = "53";

   /**
    * SYMBOLS
    **/
   @RBEntry("...")
   public static final String PRIVATE_CONSTANT_61 = "ellipses";

   @RBEntry("*")
   public static final String PRIVATE_CONSTANT_62 = "required";

   /**
    * TITLES
    **/
   @RBEntry("Cabinets and Folders")
   public static final String PRIVATE_CONSTANT_63 = "cabinetsAndFolders";

   @RBEntry("Choose Type of Object to Create")
   public static final String PRIVATE_CONSTANT_64 = "chooseObjectType";

   @RBEntry("Contents of '{0}'")
   public static final String PRIVATE_CONSTANT_65 = "contentsOf";

   @RBEntry("Folder Name")
   public static final String PRIVATE_CONSTANT_66 = "folderNameTitle";

   @RBEntry("<Unidentied Object>")
   public static final String PRIVATE_CONSTANT_67 = "objectIdentity";

   @RBEntry("Windchill Explorer")
   public static final String PRIVATE_CONSTANT_68 = "windchillExpTitle";

   @RBEntry("Local Search")
   public static final String LOCAL_SEARCH_TITLE = "58";

   @RBEntry("Find Part")
   public static final String FIND_PART_TITLE = "63";

   /**
    * ERROR MESSAGES
    * ---------------------------------------------
    **/
   @RBEntry("The following error occurred while retrieving cabinets:  {0}")
   public static final String RETRIEVE_CABINET_ERROR = "0";

   @RBEntry("Please provide a value for \"{0}\".")
   public static final String NULL_ATTRIBUTE_VALUE = "1";

   @RBEntry("The attempt to rename the folder resulted in the following error: {0}")
   public static final String RENAME_FOLDER_ERROR = "2";

   @RBEntry("The following error occurred while trying to initiate the update task for {0}:  {1}")
   public static final String INITIATE_UPDATE_FAILED = "3";

   @RBEntry("The following error occurred while trying to initiate the create task for {0}:  {1}")
   public static final String INITIATE_CREATE_FAILED = "4";

   @RBEntry("The following error occurred while trying to initiate the view task for {0}:  {1}")
   public static final String INITIATE_VIEW_FAILED = "5";

   @RBEntry("The following error occurred while trying to initiate the delete task for {0}:  {1}")
   public static final String INITIATE_DELETE_FAILED = "36";

   @RBEntry("The following error occurred while attempting to intialize the Search URL: {0}")
   public static final String INIT_SEARCH_URL_FAILED = "6";

   @RBEntry("The property, \"{0}\", was not found in your WTProperties.  This property is necessary for creating the URL to the Enterprise Search.")
   public static final String NO_SEARCH_URL = "7";

   @RBEntry("Deleting folder \"{0}\" will delete all of its contents as well. Continue deleting folder \"{0}\"?")
   public static final String CONFIRM_DELETE_FOLDER = "8";

   @RBEntry("All iterations in this version of \"{0}\" will be deleted.  Continue deleting \"{0}\"?")
   public static final String CONFIRM_DELETE_VERSIONS = "9";

   @RBEntry("Are you sure you want to delete the shortcut to {0} \"{1}\"?")
   public static final String CONFIRM_DELETE_SHORTCUT = "10";

   @RBEntry("Are you sure you would like to delete \"{0}\"?")
   public static final String CONFIRM_DELETE_OBJECT = "11";

   @RBEntry("The following error occurred while attempting to delete \"{0}\": {1}")
   public static final String DELETE_FAILED = "12";

   @RBEntry("The following error occurred while attempting to get the URL for online help: {0}")
   public static final String INIT_HELP_URL_FAILED = "13";

   @RBEntry("You currently have \"{0}\" checked out.  Please update the working copy in your \"{1}\" folder.")
   public static final String UPDATE_WORKING_COPY = "14";

   @RBEntry("The following error occurred while attempting to submit \"{0}\" for approval:  {1}.")
   public static final String LIFECYCLE_SUBMIT_FAILED = "16";

   @RBEntry("Object {0} was submitted successfully.")
   public static final String LIFECYCLE_SUBMIT_SUCCEEDED = "17";

   @RBEntry("The following error occurred while attempting to change folders: {0}.")
   public static final String CHANGE_FOLDERS_FAILED = "18";

   @RBEntry("The following error occurred while attempting to show the life cycle history for {0}: {1}.")
   public static final String LIFECYCLE_HISTORY_FAILED = "19";

   @RBEntry("The following error occurred while attempting to launch the augment life cycle task for {0}: {1}.")
   public static final String AUGMENT_LIFECYCLE_FAILED = "20";

   @RBEntry("The following error occurred while attempting to launch the task to reassign a life cycle for {0}: {1}.")
   public static final String REASSIGN_LIFECYCLE_FAILED = "21";

   @RBEntry("The following error occurred while attempting to launch the task to reassign a project for {0}: {1}.")
   public static final String REASSIGN_PROJECT_FAILED = "22";

   @RBEntry("The Windchill Explorer could not be expanded to {0} - {1} was not found.")
   public static final String EXPAND_TO_OBJECT_FAILED = "23";

   @RBEntry("Currently, the Windchill Explorer does not support creating {0} objects.")
   public static final String CREATE_NOT_AVAILABLE = "24";

   @RBEntry("Currently, the Windchill Explorer does not support updating {0} objects.")
   public static final String UPDATE_NOT_AVAILABLE = "25";

   @RBEntry("Currently, the Windchill Explorer does not support viewing of {0} objects.")
   public static final String VIEW_NOT_AVAILABLE = "26";

   @RBEntry("Currently, the Windchill Explorer does not support deleting of {0} objects.")
   public static final String DELETE_NOT_AVAILABLE = "37";

   @RBEntry("You are not authorized to update {0}.")
   public static final String UPDATE_NOT_AUTHORIZED = "27";

   @RBEntry("Would you like to view {0}?")
   public static final String PROMPT_VIEW_OBJECT = "28";

   @RBEntry("An error occurred while attempting to launch the update task for {0}.  The object you are attempting to update no longer exists in the database.")
   public static final String UPDATE_OBJECT_DOES_NOT_EXIST = "29";

   @RBEntry("Cabinets cannot be moved using the Windchill Explorer.")
   public static final String CANNOT_MOVE_CABINET = "30";

   @RBEntry("Cabinets cannot be deleted using the Windchill Explorer.")
   public static final String CANNOT_DELETE_CABINET = "31";

   @RBEntry("An error occurred while attempting to launch the task to change the identity for {0}: {1}.")
   public static final String INITIATE_CHANGE_IDENTITY_FAILED = "32";

   @RBEntry("You must be a submitter for the current life cycle phase in order to be able to reassign the life cycle for {0}.")
   public static final String LC_TEMPLATE_NOT_SUBMITTER = "33";

   @RBEntry("You must be a submitter for the current life cycle phase in order to be able to reassign the Project for {0}.")
   public static final String PROJECT_NOT_SUBMITTER = "34";

   @RBEntry("You must be a submitter in order to be able to augment the Life Cycle for {0}.")
   public static final String AUGMENT_NOT_SUBMITTER = "35";

   @RBEntry("An error occurred trying to initialize the cache of icons: {0}")
   public static final String ICON_CACHE_ERROR = "38";

   @RBEntry("An error occurred while attempting to check out {0}: {1}")
   public static final String CHECK_OUT_FAILED = "39";

   @RBEntry("You already have {0} checked out.  The working copy of {0} is located in {1}.")
   public static final String ALREADY_CHECKOUT_OWNER = "40";

   @RBEntry("An error occurred trying to retrieve your check-out folder: {1}.")
   public static final String RETRIEVE_CHECKOUT_FOLDER_FAILED = "41";

   @RBEntry("You need to have {0} checked out in order to update it.  Would you like to check out {0} now?")
   public static final String UPDATE_CHECK_OUT_PROMPT = "42";

   @RBEntry("{0} is currently checked out.  Would you like to view {0}?")
   public static final String ALREADY_CHECKED_OUT_VIEW_PROMPT = "43";

   @RBEntry("\"{0}\" is not a valid value for property \"{1}\": {2}.")
   public static final String PROPERTY_VETO_EXCEPTION = "44";

   @RBEntry("An error occurred while attempting to initialize the online help system: {0}.")
   public static final String INITIALIZE_HELP_FAILED = "45";

   @RBEntry("An error occurred while localizing the Windchill Explorer.  The key, \"{0}\", is not a valid key in the resource bundle, \"{1}\".  Please contact your support staff.")
   public static final String RESOURCE_BUNDLE_ERROR = "46";

   @RBEntry("The given object, {0}, is not cabinet-based, and cannot be displayed in the Windchill Explorer.")
   public static final String OBJECT_NOT_CABINET_BASED = "47";

   @RBEntry("No Cabinets were retrieved.")
   public static final String NO_CABINETS_FOUND = "48";

   @RBEntry("The following error occurred while attempting to paste from the clipboard: {0}")
   public static final String PASTE_OBJECT_FAILED = "49";

   @RBEntry("The following error occurred while attempting to create a shortcut to {0} in {1}: {2}")
   public static final String CREATE_SHORTCUT_FAILED = "50";

   @RBEntry("The following error occurred while trying to populate the menu options in the 'File --> New' menu: {0}")
   public static final String INITIALIZE_NEW_MENU_FAILED = "54";

   @RBEntry("The following error occurred while trying to populate the list of objects that can be created from the Windchill Explorer: {0}")
   public static final String INITIALIZE_CLASSES_FAILED = "55";

   @RBEntry("No subclasses of {0} were found.")
   public static final String NO_CREATEABLE_OBJECTS_FOUND = "56";

   @RBEntry("The attempt to retrieve the cabinets to be displayed was not successful.")
   public static final String RETRIEVE_CABINETS_UNSUCCESSFUL = "57";

   @RBEntry("An error occurred while trying to download the contents of {0}: {1}")
   public static final String DOWNLOAD_CONTENT_FAILED = "59";

   @RBEntry("{0} {1} contains no contents.")
   public static final String NO_CONTENTS = "60";

   @RBEntry("An error occurred while trying to remove baselines from {0} {1}: {2}")
   public static final String REMOVE_BASELINE_ERROR = "61";

   @RBEntry("{0} {1} is not in a baseline.")
   public static final String OBJECT_NOT_IN_BASELINE = "62";

   @RBEntry("An error occurred while attempting to launch the view task for {0}.  The object you are attempting to view no longer exists in the database.")
   public static final String VIEW_OBJECT_DOES_NOT_EXIST = "64";

   @RBEntry("You are not authorized to view {0}.")
   public static final String VIEW_NOT_AUTHORIZED = "65";

   @RBEntry("An error occurred while attempting to check in {0}.  {0} no longer exists.")
   public static final String CHECKIN_OBJECT_DOES_NOT_EXIST = "66";

   @RBEntry("An error occurred while attempting to check out {0}.  {0} no longer exists.")
   public static final String CHECKOUT_OBJECT_DOES_NOT_EXIST = "67";

   @RBEntry("Currently, the Windchill Explorer does not support save as of {0} objects.")
   public static final String SAVEAS_NOT_AVAILABLE = "68";

   @RBEntry("The following error occurred while attempting to do a save as of \"{0}\": {1}")
   public static final String SAVEAS_FAILED = "69";

   @RBEntry("The following error occurred while trying to initiate the save as task for {0}:  {1}")
   public static final String INITIATE_SAVEAS_FAILED = "70";

   @RBEntry("Save As...")
   public static final String PRIVATE_CONSTANT_69 = "saveas";

   @RBEntry("{0} is currently checked out by {1}.  Would you like to view {0}?")
   public static final String CHECKED_OUT_BY_VIEW_PROMPT = "71";

   @RBEntry("{0} ({1})")
   public static final String DISPLAY_USER_NAME = "72";

   @RBEntry("Enterprise Search not installed.")
   public static final String ENTERPRISE_SEARCH_NOT_INSTALLED = "73";

   @RBEntry("The following error occurred while attempting to launch the task to reassign a team for {0}: {1}.")
   public static final String REASSIGN_TEAM_FAILED = "74";

   @RBEntry("You must be a submitter for the current life cycle phase in order to be able to reassign the Team for {0}.")
   public static final String TEAM_NOT_SUBMITTER = "75";

   @RBEntry("{0} is currently checked out to a project.  Would you like to view {0}?")
   @RBComment("Message received when user attempts to update an workable object that is currently checked out to a project.")
   public static final String CHECKED_OUT_TO_PROJECT_VIEW_PROMPT = "76";
}
