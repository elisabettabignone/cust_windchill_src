/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.folderexplorer;

import wt.util.resource.*;

@RBUUID("wt.clients.folderexplorer.FolderExplorerHelpRB")
public final class FolderExplorerHelpRB_it extends WTListResourceBundle {
   /**
    * Links to HTML files for Online Help
    **/
   @RBEntry("wt/clients/folderexplorer/help_it/index.htm")
   public static final String PRIVATE_CONSTANT_0 = "Contents/folderexplorer/WindchillExplorer";

   @RBEntry("WCExpOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_1 = "Help/folderexplorer/WindchillExplorer";

   @RBEntry("WCExpObjCreateOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/folderexplorer/new";

   @RBEntry("WCExpObjUpdateOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_3 = "Help/folderexplorer/update";

   @RBEntry("WCExpObjViewOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_4 = "Help/folderexplorer/view";

   @RBEntry("WCExpObjDeleteOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_5 = "Help/folderexplorer/delete";

   @RBEntry("WCExpObjCheckInOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_6 = "Help/folderexplorer/checkin";

   @RBEntry("WCExpObjCheckOutOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_7 = "Help/folderexplorer/checkout";

   @RBEntry("WCExpObjSearchOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_8 = "Help/folderexplorer/localsearch";

   @RBEntry("WCExpObjMoveOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_9 = "Help/folderexplorer/cut";

   @RBEntry("WCExpObjMoveOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_10 = "Help/folderexplorer/paste";

   @RBEntry("WCExpShortcutOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_11 = "Help/folderexplorer/shortcuts";

   @RBEntry("WCExpCabinetsOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_12 = "Help/folderexplorer/cabinetsandfolders";

   @RBEntry("WCExpContentHoldersOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_13 = "Help/folderexplorer/contentholder";

   @RBEntry("WCExpObjIdentityChange")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_14 = "Help/folderexplorer/changeidentity";

   @RBEntry("WCExpPathSyntaxRef")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_15 = "Help/folderexplorer/pathsyntax";

   @RBEntry("WCExpLifeCycleOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_16 = "Help/folderexplorer/lifecycle";

   /**
    * Tooltips displayed when the mouse is over a button in
    * the toolbar
    **/
   @RBEntry("Crea un nuovo oggetto")
   public static final String PRIVATE_CONSTANT_17 = "Tip/folderexplorer/WindchillExplorer/new";

   @RBEntry("Aggiorna l'oggetto selezionato")
   public static final String PRIVATE_CONSTANT_18 = "Tip/folderexplorer/WindchillExplorer/update";

   @RBEntry("Effettua Check-In dell'oggetto selezionato")
   public static final String PRIVATE_CONSTANT_19 = "Tip/folderexplorer/WindchillExplorer/checkin";

   @RBEntry("Effettua Check-Out dell'oggetto selezionato")
   public static final String PRIVATE_CONSTANT_20 = "Tip/folderexplorer/WindchillExplorer/checkout";

   @RBEntry("Annulla il Check-Out dell'oggetto selezionato")
   public static final String PRIVATE_CONSTANT_21 = "Tip/folderexplorer/WindchillExplorer/undocheckout";

   @RBEntry("Preleva i file associati all'oggetto selezionato")
   public static final String PRIVATE_CONSTANT_22 = "Tip/folderexplorer/WindchillExplorer/get";

   @RBEntry("Elimina l'oggetto selezionato")
   public static final String PRIVATE_CONSTANT_23 = "Tip/folderexplorer/WindchillExplorer/delete";

   @RBEntry("Visualizza le proprietà dell'oggetto selezionato")
   public static final String PRIVATE_CONSTANT_24 = "Tip/folderexplorer/WindchillExplorer/view";

   @RBEntry("Taglia l'oggetto selezionato e lo inserisce negli Appunti")
   public static final String PRIVATE_CONSTANT_25 = "Tip/folderexplorer/WindchillExplorer/cut";

   @RBEntry("Copia l'oggetto selezionato negli Appunti")
   public static final String PRIVATE_CONSTANT_26 = "Tip/folderexplorer/WindchillExplorer/copy";

   @RBEntry("Incolla il contenuto degli Appunti")
   public static final String PRIVATE_CONSTANT_27 = "Tip/folderexplorer/WindchillExplorer/paste";

   @RBEntry("Ricerca nelle cartelle")
   public static final String PRIVATE_CONSTANT_28 = "Tip/folderexplorer/WindchillExplorer/localsearch";

   @RBEntry("Cerca")
   public static final String PRIVATE_CONSTANT_29 = "Tip/folderexplorer/WindchillExplorer/search";

   @RBEntry("Avvia il Navigatore struttura di prodotto")
   public static final String PRIVATE_CONSTANT_30 = "Tip/folderexplorer/WindchillExplorer/pexplr";

   @RBEntry("Visualizza la Guida in linea")
   public static final String PRIVATE_CONSTANT_31 = "Tip/folderexplorer/WindchillExplorer/help";

   @RBEntry("Avvia una nuova istanza di Navigatore Windchill")
   public static final String PRIVATE_CONSTANT_32 = "Tip/folderexplorer/WindchillExplorer/wexplr";
}
