/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.folderexplorer;

import wt.util.resource.*;

@RBUUID("wt.clients.folderexplorer.FolderExplorerRB")
public final class FolderExplorerRB_it extends WTListResourceBundle {
   /**
    * FIELD LABELS
    **/
   @RBEntry("Tipo:")
   public static final String PRIVATE_CONSTANT_0 = "typeLbl";

   @RBEntry("Nome cartella:")
   public static final String PRIVATE_CONSTANT_1 = "folderNameLbl";

   @RBEntry("Nome")
   public static final String PRIVATE_CONSTANT_2 = "name";

   /**
    * BUTTON LABELS
    **/
   @RBEntry("Annulla")
   public static final String PRIVATE_CONSTANT_3 = "cancelButton";

   @RBEntry("OK")
   public static final String PRIVATE_CONSTANT_4 = "okButton";

   /**
    * MULTILIST HEADINGS
    **/
   @RBEntry("Numero")
   public static final String PRIVATE_CONSTANT_5 = "number";

   @RBEntry("Tipo di oggetto")
   public static final String PRIVATE_CONSTANT_6 = "objectType";

   @RBEntry("Versione")
   public static final String PRIVATE_CONSTANT_7 = "version";

   @RBEntry("Stato del ciclo di vita")
   public static final String PRIVATE_CONSTANT_8 = "state";

   @RBEntry("Tipo")
   public static final String PRIVATE_CONSTANT_9 = "type";

   @RBEntry("Data ultima modifica")
   public static final String PRIVATE_CONSTANT_10 = "modified";

   @RBEntry("Team")
   public static final String PRIVATE_CONSTANT_11 = "team";

   @RBEntry("ID organizzazione")
   public static final String PRIVATE_CONSTANT_12 = "organizationId";

   /**
    * MENU LABELS
    **/
   @RBEntry("File")
   public static final String PRIVATE_CONSTANT_13 = "file";

   @RBEntry("Modifica")
   public static final String PRIVATE_CONSTANT_14 = "edit";

   @RBEntry("Oggetto")
   public static final String PRIVATE_CONSTANT_15 = "object";

   @RBEntry("Ciclo di vita")
   public static final String PRIVATE_CONSTANT_16 = "lifecycle";

   @RBEntry("Strumenti")
   public static final String PRIVATE_CONSTANT_17 = "tools";

   @RBEntry("Windchill")
   public static final String PRIVATE_CONSTANT_18 = "windchill";

   @RBEntry("Finestra")
   public static final String PRIVATE_CONSTANT_19 = "window";

   @RBEntry("Guida")
   public static final String PRIVATE_CONSTANT_20 = "help";

   /**
    * MENU ITEM LABELS
    **/
   @RBEntry("Nuovo")
   public static final String PRIVATE_CONSTANT_21 = "new";

   @RBEntry("Aggiorna")
   public static final String PRIVATE_CONSTANT_22 = "update";

   @RBEntry("Rinomina")
   public static final String PRIVATE_CONSTANT_23 = "rename";

   @RBEntry("Elimina")
   public static final String PRIVATE_CONSTANT_24 = "delete";

   @RBEntry("Aggiorna")
   public static final String PRIVATE_CONSTANT_25 = "refresh";

   @RBEntry("Chiudi")
   public static final String PRIVATE_CONSTANT_26 = "close";

   @RBEntry("Taglia")
   public static final String PRIVATE_CONSTANT_27 = "cut";

   @RBEntry("Copia")
   public static final String PRIVATE_CONSTANT_28 = "copy";

   @RBEntry("Incolla")
   public static final String PRIVATE_CONSTANT_29 = "paste";

   @RBEntry("Check-In")
   public static final String PRIVATE_CONSTANT_30 = "checkin";

   @RBEntry("Check-Out")
   public static final String PRIVATE_CONSTANT_31 = "checkout";

   @RBEntry("Annulla Check-Out")
   public static final String PRIVATE_CONSTANT_32 = "undoCheckout";

   @RBEntry("Preleva il contenuto")
   public static final String PRIVATE_CONSTANT_33 = "getContent";

   @RBEntry("Visualizza")
   public static final String PRIVATE_CONSTANT_34 = "view";

   @RBEntry("Nuova revisione")
   public static final String PRIVATE_CONSTANT_35 = "revise";

   @RBEntry("Nuova versione vista")
   public static final String PRIVATE_CONSTANT_36 = "newViewVersion";

   @RBEntry("Invia")
   public static final String PRIVATE_CONSTANT_37 = "submit";

   @RBEntry("Aggiorna team")
   public static final String PRIVATE_CONSTANT_38 = "augmentLifecycle";

   @RBEntry("Assegna a un altro team")
   public static final String PRIVATE_CONSTANT_39 = "reassignTeam";

   @RBEntry("Assegna a un altro ciclo di vita")
   public static final String PRIVATE_CONSTANT_40 = "reassignTemplate";

   @RBEntry("Imposta stato del ciclo di vita")
   public static final String PRIVATE_CONSTANT_41 = "setLifeCycleState";

   @RBEntry("Cronologia iterazioni")
   public static final String PRIVATE_CONSTANT_42 = "iterationHistory";

   @RBEntry("Tutte le versioni")
   public static final String PRIVATE_CONSTANT_43 = "versionHistory";

   @RBEntry("Cronologia del ciclo di vita")
   public static final String PRIVATE_CONSTANT_44 = "lcHistory";

   @RBEntry("Ricerca")
   public static final String PRIVATE_CONSTANT_45 = "searchEnterprise";

   @RBEntry("Ricerca nelle cartelle")
   public static final String PRIVATE_CONSTANT_46 = "searchLocal";

   @RBEntry("Navigatore struttura di prodotto")
   public static final String PRIVATE_CONSTANT_47 = "productInfoExp";

   @RBEntry("Navigatore Windchill")
   public static final String PRIVATE_CONSTANT_48 = "windchillExplorer";

   @RBEntry("Argomenti della Guida")
   public static final String PRIVATE_CONSTANT_49 = "helpTopics";

   @RBEntry("Informazioni su Windchill")
   public static final String PRIVATE_CONSTANT_50 = "aboutWindchill";

   @RBEntry("Baseline")
   public static final String PRIVATE_CONSTANT_51 = "baseline";

   @RBEntry("Aggiungi...")
   public static final String PRIVATE_CONSTANT_52 = "addBaseline";

   @RBEntry("Rimuovi...")
   public static final String PRIVATE_CONSTANT_53 = "removeBaseline";

   @RBEntry("Completa...")
   public static final String PRIVATE_CONSTANT_54 = "populateBaseline";

   @RBEntry("Mostra team")
   public static final String PRIVATE_CONSTANT_55 = "roleParticipants";

   @RBEntry("Cronologia")
   public static final String PRIVATE_CONSTANT_56 = "history";

   /**
    * JSP PAGE MESSAG#ES
    **/
   @RBEntry("Gestione documenti")
   public static final String PRIVATE_CONSTANT_57 = "explorerTitle";

   @RBEntry("Navigatore Windchill")
   public static final String PRIVATE_CONSTANT_58 = "explorerToolTip";

   @RBEntry("Vai alla home page Windchill")
   public static final String PRIVATE_CONSTANT_59 = "windchillHomeToolTip";

   @RBEntry("Windchill")
   public static final String PRIVATE_CONSTANT_60 = "windchillToolTip";

   /**
    * STATUS MESSAGES
    **/
   @RBEntry("Recupero schedari in corso...")
   public static final String INITIALIZING_CABINETS = "51";

   @RBEntry("Visualizzazione degli schedari in corso...")
   public static final String INITIALIZING_EXPLORER = "52";

   @RBEntry("Il recupero degli schedari è fallito.")
   public static final String INITIALIZE_CABINETS_FAILED = "53";

   /**
    * SYMBOLS
    **/
   @RBEntry("...")
   public static final String PRIVATE_CONSTANT_61 = "ellipses";

   @RBEntry("*")
   public static final String PRIVATE_CONSTANT_62 = "required";

   /**
    * TITLES
    **/
   @RBEntry("Schedari e cartelle")
   public static final String PRIVATE_CONSTANT_63 = "cabinetsAndFolders";

   @RBEntry("Tipo di oggetto da creare")
   public static final String PRIVATE_CONSTANT_64 = "chooseObjectType";

   @RBEntry("Contenuto di '{0}'")
   public static final String PRIVATE_CONSTANT_65 = "contentsOf";

   @RBEntry("Nome cartella")
   public static final String PRIVATE_CONSTANT_66 = "folderNameTitle";

   @RBEntry("<Oggetto non identificato>")
   public static final String PRIVATE_CONSTANT_67 = "objectIdentity";

   @RBEntry("Navigatore Windchill")
   public static final String PRIVATE_CONSTANT_68 = "windchillExpTitle";

   @RBEntry("Ricerca locale")
   public static final String LOCAL_SEARCH_TITLE = "58";

   @RBEntry("Trova parte")
   public static final String FIND_PART_TITLE = "63";

   /**
    * ERROR MESSAGES
    * ---------------------------------------------
    **/
   @RBEntry("Errore durante il recupero degli schedari: {0}")
   public static final String RETRIEVE_CABINET_ERROR = "0";

   @RBEntry("Fornire un valore per \"{0}\".")
   public static final String NULL_ATTRIBUTE_VALUE = "1";

   @RBEntry("Errore durante la ridenominazione della cartella:  {0}")
   public static final String RENAME_FOLDER_ERROR = "2";

   @RBEntry("Errore durante l'avvio del task di aggiornamento per {0}:  {1}")
   public static final String INITIATE_UPDATE_FAILED = "3";

   @RBEntry("Errore durante l'avvio del task di creazione per {0}:  {1}")
   public static final String INITIATE_CREATE_FAILED = "4";

   @RBEntry("Errore durante l'avvio del task di visualizzazione per {0}:  {1}")
   public static final String INITIATE_VIEW_FAILED = "5";

   @RBEntry("Errore durante l'avvio del task di eliminazione per {0}:  {1}")
   public static final String INITIATE_DELETE_FAILED = "36";

   @RBEntry("Errore durante l'inizializzazione dell'URL per la Ricerca: {0}")
   public static final String INIT_SEARCH_URL_FAILED = "6";

   @RBEntry("Impossibile trovare la proprietà \"{0}\" in WTProperties. Questa proprietà è necessaria per la creazione dell'URL per la Ricerca globale.")
   public static final String NO_SEARCH_URL = "7";

   @RBEntry("Eliminando la cartella \"{0}\" verrà eliminato anche tutto il relativo contenuto. Eliminare la cartella \"{0}\"?")
   public static final String CONFIRM_DELETE_FOLDER = "8";

   @RBEntry("Tutte le iterazioni di questa versione di \"{0}\" verranno eliminate. Eliminare \"{0}\"?")
   public static final String CONFIRM_DELETE_VERSIONS = "9";

   @RBEntry("Eliminare il collegamento a {0} \"{1}\"?")
   public static final String CONFIRM_DELETE_SHORTCUT = "10";

   @RBEntry("Eliminare \"{0}\"?")
   public static final String CONFIRM_DELETE_OBJECT = "11";

   @RBEntry("Errore durante l'eliminazione di \"{0}\": {1}")
   public static final String DELETE_FAILED = "12";

   @RBEntry("Errore durante la ricerca dell'URL per la Guida in linea: {0}")
   public static final String INIT_HELP_URL_FAILED = "13";

   @RBEntry("Il documento \"{0}\" è attualmente sottoposto a Check-Out dall'utente. Aggiornare la copia in modifica nella cartella \"{1}\".")
   public static final String UPDATE_WORKING_COPY = "14";

   @RBEntry("Errore durante l'invio di \"{0}\" per approvazione:  {1}.")
   public static final String LIFECYCLE_SUBMIT_FAILED = "16";

   @RBEntry("Invio dell'oggetto {0} completato.")
   public static final String LIFECYCLE_SUBMIT_SUCCEEDED = "17";

   @RBEntry("Errore durante la modifica delle cartelle: {0}.")
   public static final String CHANGE_FOLDERS_FAILED = "18";

   @RBEntry("Errore durante la visualizzazione della cronologia del ciclo di vita per {0}: {1}.")
   public static final String LIFECYCLE_HISTORY_FAILED = "19";

   @RBEntry("Errore durante l'avvio del task di aumento del ciclo di vita per {0}: {1}.")
   public static final String AUGMENT_LIFECYCLE_FAILED = "20";

   @RBEntry("Errore durante il tentativo del task di riassegnazione di un ciclo di vita per {0}: {1}.")
   public static final String REASSIGN_LIFECYCLE_FAILED = "21";

   @RBEntry("Errore durante l'avvio del task di riassegnazione di un progetto per {0}: {1}.")
   public static final String REASSIGN_PROJECT_FAILED = "22";

   @RBEntry("Impossibile espandere il Navigatore Windchill a {0} - Impossibile trovare {1}.")
   public static final String EXPAND_TO_OBJECT_FAILED = "23";

   @RBEntry("Al momento, il Navigatore Windchill non supporta la creazione degli oggetti {0}.")
   public static final String CREATE_NOT_AVAILABLE = "24";

   @RBEntry("Al momento, il Navigatore Windchill non supporta l'aggiornamento degli oggetti {0}.")
   public static final String UPDATE_NOT_AVAILABLE = "25";

   @RBEntry("Al momento, il Navigatore Windchill non supporta la visualizzazione degli oggetti {0}.")
   public static final String VIEW_NOT_AVAILABLE = "26";

   @RBEntry("Al momento, il Navigatore Windchill non supporta l'eliminazione degli oggetti {0}.")
   public static final String DELETE_NOT_AVAILABLE = "37";

   @RBEntry("Per aggiornare {0} è necessario disporre di autorizzazione.")
   public static final String UPDATE_NOT_AUTHORIZED = "27";

   @RBEntry("Visualizzare {0}?")
   public static final String PROMPT_VIEW_OBJECT = "28";

   @RBEntry("Errore durante l'avvio del task di aggiornamento di {0}. L'oggetto che si sta cercando di aggiornare non è più presente nel database.")
   public static final String UPDATE_OBJECT_DOES_NOT_EXIST = "29";

   @RBEntry("Impossibile spostare gli schedari utilizzando il Navigatore Windchill.")
   public static final String CANNOT_MOVE_CABINET = "30";

   @RBEntry("Impossibile eliminare gli schedari utilizzando il Navigatore Windchill.")
   public static final String CANNOT_DELETE_CABINET = "31";

   @RBEntry("Errore durante l'avvio del task di modifica dell'identificativo per {0}: {1}.")
   public static final String INITIATE_CHANGE_IDENTITY_FAILED = "32";

   @RBEntry("È necessario essere un autore invio della fase del ciclo di vita corrente  per riassegnare il ciclo di vita per {0}.")
   public static final String LC_TEMPLATE_NOT_SUBMITTER = "33";

   @RBEntry("È necessario essere un autore invio della fase del ciclo di vita corrente  per riassegnare il progetto per {0}.")
   public static final String PROJECT_NOT_SUBMITTER = "34";

   @RBEntry("È necessario essere un autore invio per aumentare  il ciclo di vita per {0}.")
   public static final String AUGMENT_NOT_SUBMITTER = "35";

   @RBEntry("Si è verificato un errore durante l'inizializzazione della cache delle icone: {0}")
   public static final String ICON_CACHE_ERROR = "38";

   @RBEntry("Si è verificato un errore durante il Check-Out di {0}: {1}")
   public static final String CHECK_OUT_FAILED = "39";

   @RBEntry("{0} è già sottoposto a Check-Out. La copia in modifica di {0} si trova in {1}.")
   public static final String ALREADY_CHECKOUT_OWNER = "40";

   @RBEntry("Si è verificato un errore durante il recupero della cartella Checked-Out: {1}.")
   public static final String RETRIEVE_CHECKOUT_FOLDER_FAILED = "41";

   @RBEntry("Per aggiornare {0} è necessario sottoporlo a Check-Out. Effettuare il Check-Out {0} ora?")
   public static final String UPDATE_CHECK_OUT_PROMPT = "42";

   @RBEntry("{0} è attualmente sottoposto a Check-Out. Visualizzare {0}?")
   public static final String ALREADY_CHECKED_OUT_VIEW_PROMPT = "43";

   @RBEntry("\"{0}\" non è un valore valido per la proprietà \"{1}\": {2}.")
   public static final String PROPERTY_VETO_EXCEPTION = "44";

   @RBEntry("Si è verificato un errore durante l'inizializzazione della Guida in linea: {0}.")
   public static final String INITIALIZE_HELP_FAILED = "45";

   @RBEntry("Si è verificato un errore durante la localizzazione del Navigatore Windchill. La chiave \"{0}\" non è valida nel resource bundle \"{1}\". Contattare il personale di assistenza.")
   public static final String RESOURCE_BUNDLE_ERROR = "46";

   @RBEntry("L'oggetto {0} non è basato su schedari e non può essere visualizzato nel Navigatore Windchill.")
   public static final String OBJECT_NOT_CABINET_BASED = "47";

   @RBEntry("Nessuno schedario recuperato.")
   public static final String NO_CABINETS_FOUND = "48";

   @RBEntry("Errore durante il tentativo di incollare dagli Appunti: {0}")
   public static final String PASTE_OBJECT_FAILED = "49";

   @RBEntry("Errore durante la creazione di un collegamento a {0} in {1}: {2}")
   public static final String CREATE_SHORTCUT_FAILED = "50";

   @RBEntry("Errore durante il completamento delle opzioni nel menu 'File --> Nuovo': {0}")
   public static final String INITIALIZE_NEW_MENU_FAILED = "54";

   @RBEntry("Errore durante il completamento dell'elenco degli oggetti che è possibile creare dal Navigatore Windchill: {0}")
   public static final String INITIALIZE_CLASSES_FAILED = "55";

   @RBEntry("Non è stata trovata alcuna sottoclasse di {0}.")
   public static final String NO_CREATEABLE_OBJECTS_FOUND = "56";

   @RBEntry("Recupero degli schedari da visualizzare non riuscito.")
   public static final String RETRIEVE_CABINETS_UNSUCCESSFUL = "57";

   @RBEntry("Si è verificato un errore durante lo scaricamento del contenuto di {0}: {1}")
   public static final String DOWNLOAD_CONTENT_FAILED = "59";

   @RBEntry("{0} In {1} non è presente alcun contenuto.")
   public static final String NO_CONTENTS = "60";

   @RBEntry("Si è verificato un errore durante la rimozione delle baseline da {0} {1}: {2}")
   public static final String REMOVE_BASELINE_ERROR = "61";

   @RBEntry("{0} {1} non è una baseline.")
   public static final String OBJECT_NOT_IN_BASELINE = "62";

   @RBEntry("Errore durante l'avvio del task di visualizzazione per {0}. L'oggetto che si sta cercando di visualizzare non è più presente nel database.")
   public static final String VIEW_OBJECT_DOES_NOT_EXIST = "64";

   @RBEntry("Nessuna autorizzazione per visualizzare {0}.")
   public static final String VIEW_NOT_AUTHORIZED = "65";

   @RBEntry("Si è verificato un errore durante il Check-In di  {0}.  {0} non esiste più.")
   public static final String CHECKIN_OBJECT_DOES_NOT_EXIST = "66";

   @RBEntry("Si è verificato un errore durante il Check-Out di {0}. {0} non esiste più.")
   public static final String CHECKOUT_OBJECT_DOES_NOT_EXIST = "67";

   @RBEntry("Al momento il Navigatore Windchill non supporta Salva con nome per oggetti {0}.")
   public static final String SAVEAS_NOT_AVAILABLE = "68";

   @RBEntry("Si è verificato il seguente errore durante il salvataggio con nome di \"{0}\": {1}")
   public static final String SAVEAS_FAILED = "69";

   @RBEntry("Errore durante l'avvio del task di salvataggio con nome per {0}:  {1}")
   public static final String INITIATE_SAVEAS_FAILED = "70";

   @RBEntry("Salva con nome...")
   public static final String PRIVATE_CONSTANT_69 = "saveas";

   @RBEntry("{0} è stato sottoposto a Check-Out da {1}. Visualizzare {0}?")
   public static final String CHECKED_OUT_BY_VIEW_PROMPT = "71";

   @RBEntry("{0} ({1})")
   public static final String DISPLAY_USER_NAME = "72";

   @RBEntry("Ricerca globale non installata.")
   public static final String ENTERPRISE_SEARCH_NOT_INSTALLED = "73";

   @RBEntry("Errore durante l'avvio del task di riassegnazione del team per {0}: {1}.")
   public static final String REASSIGN_TEAM_FAILED = "74";

   @RBEntry("È necessario essere un autore invio della fase del ciclo di vita corrente  per riassegnare {0} ad un altro team.")
   public static final String TEAM_NOT_SUBMITTER = "75";

   @RBEntry("{0} è attualmente sottoposto a Check-Out in un progetto. Visualizzare {0}?")
   @RBComment("Message received when user attempts to update an workable object that is currently checked out to a project.")
   public static final String CHECKED_OUT_TO_PROJECT_VIEW_PROMPT = "76";
}
