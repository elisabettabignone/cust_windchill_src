/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.folderexplorer;

import wt.util.resource.*;

@RBUUID("wt.clients.folderexplorer.FolderExplorerHelpRB")
public final class FolderExplorerHelpRB extends WTListResourceBundle {
   /**
    * Links to HTML files for Online Help
    **/
   @RBEntry("wt/clients/folderexplorer/help_en/index.htm")
   public static final String PRIVATE_CONSTANT_0 = "Contents/folderexplorer/WindchillExplorer";

   @RBEntry("WCExpOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_1 = "Help/folderexplorer/WindchillExplorer";

   @RBEntry("WCExpObjCreateOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/folderexplorer/new";

   @RBEntry("WCExpObjUpdateOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_3 = "Help/folderexplorer/update";

   @RBEntry("WCExpObjViewOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_4 = "Help/folderexplorer/view";

   @RBEntry("WCExpObjDeleteOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_5 = "Help/folderexplorer/delete";

   @RBEntry("WCExpObjCheckInOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_6 = "Help/folderexplorer/checkin";

   @RBEntry("WCExpObjCheckOutOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_7 = "Help/folderexplorer/checkout";

   @RBEntry("WCExpObjSearchOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_8 = "Help/folderexplorer/localsearch";

   @RBEntry("WCExpObjMoveOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_9 = "Help/folderexplorer/cut";

   @RBEntry("WCExpObjMoveOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_10 = "Help/folderexplorer/paste";

   @RBEntry("WCExpShortcutOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_11 = "Help/folderexplorer/shortcuts";

   @RBEntry("WCExpCabinetsOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_12 = "Help/folderexplorer/cabinetsandfolders";

   @RBEntry("WCExpContentHoldersOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_13 = "Help/folderexplorer/contentholder";

   @RBEntry("WCExpObjIdentityChange")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_14 = "Help/folderexplorer/changeidentity";

   @RBEntry("WCExpPathSyntaxRef")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_15 = "Help/folderexplorer/pathsyntax";

   @RBEntry("WCExpLifeCycleOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_16 = "Help/folderexplorer/lifecycle";

   /**
    * Tooltips displayed when the mouse is over a button in
    * the toolbar
    **/
   @RBEntry("Create a new object")
   public static final String PRIVATE_CONSTANT_17 = "Tip/folderexplorer/WindchillExplorer/new";

   @RBEntry("Update the selected object")
   public static final String PRIVATE_CONSTANT_18 = "Tip/folderexplorer/WindchillExplorer/update";

   @RBEntry("Check in the selected checked out object")
   public static final String PRIVATE_CONSTANT_19 = "Tip/folderexplorer/WindchillExplorer/checkin";

   @RBEntry("Check out the selected object")
   public static final String PRIVATE_CONSTANT_20 = "Tip/folderexplorer/WindchillExplorer/checkout";

   @RBEntry("Undo check out of the selected object")
   public static final String PRIVATE_CONSTANT_21 = "Tip/folderexplorer/WindchillExplorer/undocheckout";

   @RBEntry("Get the contents of the selected object")
   public static final String PRIVATE_CONSTANT_22 = "Tip/folderexplorer/WindchillExplorer/get";

   @RBEntry("Delete the selected object")
   public static final String PRIVATE_CONSTANT_23 = "Tip/folderexplorer/WindchillExplorer/delete";

   @RBEntry("View the properties of the selected object")
   public static final String PRIVATE_CONSTANT_24 = "Tip/folderexplorer/WindchillExplorer/view";

   @RBEntry("Cut the selected object into the clipboard")
   public static final String PRIVATE_CONSTANT_25 = "Tip/folderexplorer/WindchillExplorer/cut";

   @RBEntry("Copy the selected object into the clipboard")
   public static final String PRIVATE_CONSTANT_26 = "Tip/folderexplorer/WindchillExplorer/copy";

   @RBEntry("Paste the contents of the clipboard")
   public static final String PRIVATE_CONSTANT_27 = "Tip/folderexplorer/WindchillExplorer/paste";

   @RBEntry("Search in folders")
   public static final String PRIVATE_CONSTANT_28 = "Tip/folderexplorer/WindchillExplorer/localsearch";

   @RBEntry("Search")
   public static final String PRIVATE_CONSTANT_29 = "Tip/folderexplorer/WindchillExplorer/search";

   @RBEntry("Launch the Product Structure Explorer")
   public static final String PRIVATE_CONSTANT_30 = "Tip/folderexplorer/WindchillExplorer/pexplr";

   @RBEntry("Access online Help")
   public static final String PRIVATE_CONSTANT_31 = "Tip/folderexplorer/WindchillExplorer/help";

   @RBEntry("Launch a new Windchill Explorer")
   public static final String PRIVATE_CONSTANT_32 = "Tip/folderexplorer/WindchillExplorer/wexplr";
}
