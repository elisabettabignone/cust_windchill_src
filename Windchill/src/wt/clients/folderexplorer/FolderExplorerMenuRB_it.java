/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.folderexplorer;

import wt.util.resource.*;

@RBUUID("wt.clients.folderexplorer.FolderExplorerMenuRB")
public final class FolderExplorerMenuRB_it extends WTListResourceBundle {
   @RBEntry(":")
   public static final String CLASS_NAME_SEPARATOR = "1";

   @RBEntry(";")
   public static final String PAIR_SEPARATOR = "2";

   @RBEntry("+")
   public static final String ADD_CLASS_FLAG = "3";

   @RBEntry("-")
   public static final String REMOVE_CLASS_FLAG = "4";

   @RBEntry("-wt.folder.ShortcutLink;-wt.folder.IteratedShortcutLink;+wt.folder.Shortcut;-wt.change2.WTChangeOrder2;-wt.change2.WTChangeActivity2;-wt.change2.WTChangeInvestigation;-wt.change2.WTChangeProposal;-wt.change2.WTAnalysisActivity;-wt.federation.ProxyDocument;-wt.epm.EPMDocument;-wt.workflow.notebook.FolderedBookmark;-wt.workflow.notebook.WfFolderedBookmark;-com.ptc.core.meta.type.mgmt.server.impl.WTTypeDefinition;-wt.annotation.AnnotationSet;-wt.annotation.StructuredAnnotationSet;-wt.project.Project;-wt.projmgmt.admin.ProjectTemplate;-wt.projmgmt.execution.ProjectPlan;-wt.replication.unit.WTUnit")
   public static final String NEW_MENU_OBJECTS = "0";
}
