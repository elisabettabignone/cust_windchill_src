/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.doc;

import wt.util.resource.*;

@RBUUID("wt.clients.doc.DocRB")
public final class DocRB_it extends WTListResourceBundle {
   /**
    * FIELD LABELS
    * Labels used to identify fields on the frames and dialogs
    **/
   @RBEntry("File")
   public static final String PRIVATE_CONSTANT_0 = "filesLbl";

   @RBEntry("URL")
   public static final String PRIVATE_CONSTANT_1 = "urlsLbl";

   @RBEntry("Commenti:")
   public static final String PRIVATE_CONSTANT_2 = "commentLbl";

   @RBEntry("Contenuto secondario:")
   public static final String PRIVATE_CONSTANT_3 = "contentsLbl";

   @RBEntry("Data creazione")
   public static final String PRIVATE_CONSTANT_4 = "createdLbl";

   @RBEntry("Autore:")
   public static final String PRIVATE_CONSTANT_5 = "createdByLbl";

   @RBEntry("Data creazione:")
   public static final String PRIVATE_CONSTANT_6 = "createdOnLbl";

   @RBEntry("Descrizione:")
   public static final String PRIVATE_CONSTANT_7 = "descriptionLbl";

   @RBEntry("Reparto:")
   public static final String PRIVATE_CONSTANT_8 = "docDepartmentLbl";

   @RBEntry("Formato:")
   public static final String PRIVATE_CONSTANT_9 = "docFormatLbl";

   @RBEntry("Iterazione:")
   public static final String PRIVATE_CONSTANT_10 = "docIterationLbl";

   @RBEntry("Posizione:")
   public static final String PRIVATE_CONSTANT_11 = "docLocationLbl";

   @RBEntry("ID organizzazione:")
   public static final String orgIdLbl = "orgIdLbl";

   @RBEntry("Nome:")
   public static final String PRIVATE_CONSTANT_12 = "docNameLbl";

   @RBEntry("Numero:")
   public static final String PRIVATE_CONSTANT_13 = "docNumberLbl";

   @RBEntry("Dimensione:")
   public static final String PRIVATE_CONSTANT_14 = "docSizeLbl";

   @RBEntry("Stato:")
   public static final String PRIVATE_CONSTANT_15 = "docStatusLbl";

   @RBEntry("Titolo:")
   public static final String PRIVATE_CONSTANT_16 = "docTitleLbl";

   @RBEntry("Tipo:")
   public static final String PRIVATE_CONSTANT_17 = "docTypeLbl";

   @RBEntry("Versione:")
   public static final String PRIVATE_CONSTANT_18 = "docVersionLbl";

   @RBEntry("Nome cartella:")
   public static final String PRIVATE_CONSTANT_19 = "folderNameLbl";

   @RBEntry("Posizione:")
   public static final String PRIVATE_CONSTANT_20 = "folderPathLbl";

   @RBEntry("Generale")
   public static final String PRIVATE_CONSTANT_21 = "generalLbl";

   @RBEntry("Nello schedario:")
   public static final String PRIVATE_CONSTANT_22 = "inCabinetLbl";

   @RBEntry("Ciclo di vita:")
   public static final String PRIVATE_CONSTANT_23 = "lifeCycleNameLbl";

   @RBEntry("Stato del ciclo di vita:")
   public static final String PRIVATE_CONSTANT_24 = "lifeCycleStateLbl";

   @RBEntry("Nome:")
   public static final String PRIVATE_CONSTANT_25 = "nameLbl";

   @RBEntry("Progetto:")
   public static final String PRIVATE_CONSTANT_26 = "projectLbl";

   @RBEntry("Team:")
   public static final String PRIVATE_CONSTANT_27 = "teamLbl";

   @RBEntry("Data ultima modifica:")
   public static final String PRIVATE_CONSTANT_28 = "updatedOnLbl";

   @RBEntry("Autore modifiche:")
   public static final String PRIVATE_CONSTANT_29 = "updatedByLbl";

   @RBEntry("Allega file:")
   public static final String PRIVATE_CONSTANT_30 = "attachFileLbl";

   @RBEntry("Allega URL:")
   public static final String PRIVATE_CONSTANT_31 = "attachURLLbl";

   @RBEntry("Descrizione URL:")
   public static final String PRIVATE_CONSTANT_32 = "urlDescLbl";

   /**
    * BUTTON LABELS
    * Labels on command buttons
    **/
   @RBEntry("Applica")
   public static final String PRIVATE_CONSTANT_33 = "applyButton";

   @RBEntry("Sfoglia")
   public static final String PRIVATE_CONSTANT_34 = "browseButton";

   @RBEntry("Annulla")
   public static final String PRIVATE_CONSTANT_35 = "cancelButton";

   @RBEntry("Chiudi")
   public static final String PRIVATE_CONSTANT_36 = "closeButton";

   @RBEntry("Contenuto")
   public static final String PRIVATE_CONSTANT_37 = "contentsButton";

   @RBEntry("Guida")
   public static final String PRIVATE_CONSTANT_38 = "helpButton";

   @RBEntry("OK")
   public static final String PRIVATE_CONSTANT_39 = "okButton";

   @RBEntry("Salva")
   public static final String PRIVATE_CONSTANT_40 = "saveButton";

   @RBEntry("Reimposta")
   public static final String PRIVATE_CONSTANT_41 = "resetButton";

   @RBEntry("Aggiungi allegati")
   public static final String PRIVATE_CONSTANT_42 = "addAttachmentsButton";

   /**
    * CHECKBOX LABELS
    **/
   @RBEntry("Richiedi promozione alla fase successiva del ciclo di vita per l'oggetto")
   @RBComment("Checkbox label.")
   public static final String PRIVATE_CONSTANT_43 = "submitLbl";

   /**
    * SYMBOLS
    **/
   @RBEntry("...")
   public static final String PRIVATE_CONSTANT_44 = "ellipses";

   @RBEntry("*")
   public static final String PRIVATE_CONSTANT_45 = "required";

   @RBEntry(":")
   public static final String PRIVATE_CONSTANT_46 = "labelColon";

   /**
    * TITLES
    **/
   @RBEntry("In stato di Check-In")
   public static final String PRIVATE_CONSTANT_47 = "docCheckedIn";

   @RBEntry("Sottoposto a Check-Out")
   public static final String PRIVATE_CONSTANT_48 = "docCheckedOut";

   @RBEntry("Check-Out effettuato da {0}")
   public static final String PRIVATE_CONSTANT_49 = "docCheckedOutBy";

   @RBEntry("Non sottoposto a Check-Out")
   public static final String PRIVATE_CONSTANT_50 = "docNotCheckedOut";

   @RBEntry("Visualizza documento <{0}>")
   public static final String PRIVATE_CONSTANT_51 = "viewDocument";

   @RBEntry("Copia in modifica di {0}")
   public static final String PRIVATE_CONSTANT_52 = "workingCopyTitle";

   @RBEntry("Crea documento")
   public static final String PRIVATE_CONSTANT_53 = "createDocument";

   @RBEntry("Crea il documento {0}")
   public static final String PRIVATE_CONSTANT_54 = "createDocumentNumber";

   @RBEntry("Crea il documento {0} ({1})")
   public static final String PRIVATE_CONSTANT_55 = "createDocumentNumberName";

   @RBEntry("La creazione del documento è stata annullata.")
   public static final String PRIVATE_CONSTANT_56 = "createDocumentCancelled";

   @RBEntry("Aggiorna documento <{0}>")
   public static final String PRIVATE_CONSTANT_57 = "updateDocument";

   @RBEntry("Aggiorna il documento {0}")
   public static final String PRIVATE_CONSTANT_58 = "updateDocumentNumberName";

   @RBEntry("Aggiornamento del documento {0} in corso...")
   public static final String PRIVATE_CONSTANT_59 = "updateDocumentProcessing";

   @RBEntry("{0} è stato aggiornato.")
   public static final String PRIVATE_CONSTANT_60 = "updateDocumentSuccess";

   @RBEntry("L'aggiornamento di {0} è stato annullato.")
   public static final String PRIVATE_CONSTANT_61 = "updateDocumentCancel";

   @RBEntry("Check-In {0}")
   public static final String PRIVATE_CONSTANT_62 = "checkinDocumentNumberName";

   @RBEntry("Check-In di {0} in corso...")
   public static final String PRIVATE_CONSTANT_63 = "checkinDocumentProcessing";

   @RBEntry("{0} è stato sottoposto a Check-In.")
   public static final String PRIVATE_CONSTANT_64 = "checkinDocumentSuccess";

   @RBEntry("Il Check-In di {0} è stato annullato.")
   public static final String PRIVATE_CONSTANT_65 = "checkinDocumentCancel";

   @RBEntry("Il Check-Out di {0} è stato completato.")
   public static final String PRIVATE_CONSTANT_66 = "checkoutDocumentSuccess";

   @RBEntry("Scaricamento del contenuto principale in corso...")
   public static final String PRIVATE_CONSTANT_67 = "downloading";

   @RBEntry("Rinomina il documento {0}")
   public static final String PRIVATE_CONSTANT_68 = "RenameDocumentNumberName";

   @RBEntry("Crea nuova revisione del documento {0}")
   public static final String PRIVATE_CONSTANT_69 = "ReviseDocumentNumberName";

   @RBEntry("Elimina il documento {0}")
   public static final String PRIVATE_CONSTANT_70 = "DeleteDocumentNumberName";

   @RBEntry("Annulla il Check-Out del documento {0}")
   public static final String PRIVATE_CONSTANT_71 = "UndoCheckoutDocumentNumberName";

   @RBEntry("Sposta il documento {0}")
   public static final String PRIVATE_CONSTANT_72 = "MoveDocumentNumberName";

   @RBEntry("Lo spostamento di {0} è stato annullato.")
   public static final String PRIVATE_CONSTANT_73 = "moveDocumentCancel";

   @RBEntry("Creazione nuova revisione di {0} annullata.")
   public static final String PRIVATE_CONSTANT_74 = "reviseDocumentCancel";

   @RBEntry("L'eliminazione di {0} è stata annullata.")
   public static final String PRIVATE_CONSTANT_75 = "deleteDocumentCancel";

   @RBEntry("L'annullamento del Check-Out di {0} è stato annullato.")
   public static final String PRIVATE_CONSTANT_76 = "undoCheckoutDocumentCancel";

   @RBEntry("La ridenominazione di {0} è stata annullata.")
   public static final String PRIVATE_CONSTANT_77 = "renameDocumentCancel";

   /**
    * TABS
    **/
   @RBEntry("Generale")
   public static final String PRIVATE_CONSTANT_78 = "generalTab";

   @RBEntry("Allegati")
   public static final String PRIVATE_CONSTANT_79 = "attachmentsTab";

   @RBEntry("Documenti referenziati")
   public static final String PRIVATE_CONSTANT_80 = "referencedTab";

   @RBEntry("Struttura")
   public static final String PRIVATE_CONSTANT_81 = "structureTab";

   /**
    * PANEL MESSAGES
    **/
   @RBEntry("Salvare prima di aggiungere il contenuto...")
   @RBComment("Panel Message")
   public static final String PRIVATE_CONSTANT_82 = "saveMessageLbl";

   /**
    * STATUS BAR MESSAGES
    **/
   @RBEntry("Creazione del documento {0} completata.")
   @RBComment("Status bar message.")
   public static final String CREATE_DOCUMENT_SUCCESSFUL = "1";

   @RBEntry("Creazione del documento non riuscita.")
   @RBComment("Status bar message.")
   public static final String CREATE_DOCUMENT_FAILED = "2";

   @RBEntry("Aggiornamento del documento {0} completato.")
   @RBComment("Status bar message.")
   public static final String UPDATE_DOCUMENT_SUCCESSFUL = "3";

   @RBEntry("Aggiornamento del documento {0} non riuscito.")
   @RBComment("Status bar message.")
   public static final String UPDATE_DOCUMENT_FAILURE = "4";

   @RBEntry("Impossibile trovare schedario personale.")
   @RBComment("Status bar message.")
   public static final String PERSONAL_CABINET_NOT_FOUND = "5";

   @RBEntry("Salvataggio annullato.")
   @RBComment("Status bar message.")
   public static final String SAVE_CANCELLED = "6";

   @RBEntry("Le modifiche apportate non richiedono salvataggio.")
   @RBComment("Status bar message.")
   public static final String NO_UPDATES_TO_SAVE = "7";

   @RBEntry("Fornire un valore per \"{0}\".")
   public static final String NULL_ATTRIBUTE_VALUE = "8";

   @RBEntry("Il documento \"{0}\" non è attualmente sottoposto a Check-Out dall'utente.")
   public static final String NOT_CHECKOUT_OWNER = "9";

   @RBEntry("Il documento \"{0}\" è attualmente sottoposto a Check-Out da {1}")
   public static final String CHECKED_OUT_BY_OTHER = "10";

   @RBEntry("\"{0}\" non è un percorso valido per la cartella.")
   public static final String INVALID_FOLDER_PATH = "11";

   @RBEntry("\"{0}\" non è un valore valido per la proprietà \"{1}\".")
   public static final String PROPERTY_VETO_EXCEPTION = "12";

   @RBEntry("Il documento \"{0}\" è attualmente sottoposto a Check-Out dall'utente. Aggiornare la copia in modifica nella cartella \"{1}\".")
   public static final String UPDATE_WORKING_COPY = "13";

   @RBEntry("Sottoporre a Check-Out il documento \"{0}\" per aggiornarlo.")
   public static final String UPDATE_NOT_ALLOWED = "14";

   @RBEntry("Il documento \"{0}\" è attualmente sottoposto a Check-Out dall'utente.")
   public static final String CHECKED_OUT_BY_USER = "15";

   @RBEntry("Specificare la cartella in cui creare il documento.")
   public static final String PARENT_FOLDER_REQUIRED = "16";

   @RBEntry("Si è verificato un errore durante l'inizializzazione del contenuto: {0}.")
   public static final String INIT_CONTENT_ERROR = "17";

   @RBEntry("Si è verificato un errore durante la ricerca dell'URL della Guida in linea: {0}.")
   public static final String INIT_HELP_URL_FAILED = "18";

   @RBEntry("Le modifiche apportate al contenuto non sono ancora state salvate. Salvarle ora?")
   public static final String CONFIRM_UNSAVED_CHANGES = "19";

   @RBEntry("Si è verificato un errore durante il salvataggio del contenuto di {0}: {1}.")
   public static final String CONTENT_SAVE_FAILED = "20";

   @RBEntry("È possibile creare documenti solo nello schedario personale '{0}'.  La cartella '{1}' non si trova nello schedario personale.")
   public static final String NOT_IN_PERSONAL_CABINET = "21";

   @RBEntry("Si è verificato un errore durante l'aggiornamento del documento  {0}: {1}.")
   public static final String UPDATE_DOCUMENT_FAILED = "23";

   @RBEntry("Non è stato specificato alcun documento da aggiornare.")
   public static final String NO_DOCUMENT_TO_UPDATE = "24";

   @RBEntry("È necessario che l'oggetto specificato sia un documento WT perché sia possibile l'avvio del task.")
   public static final String OBJECT_NOT_WTDOCUMENT = "25";

   @RBEntry("Il documento è nullo e non può essere visualizzato.")
   public static final String NULL_VIEW_DOCUMENT = "26";

   @RBEntry("È possibile creare un nuovo documento solo in una cartella  presente nello schedario personale dell'utente.  Impossibile trovare lo schedario personale dell'utente {0}.")
   public static final String NO_PERSONAL_CABINET = "27";

   @RBEntry("Si è verificato un errore durante il recupero dello schedario personale dell'utente {0}. È possibile salvare nuovi documenti solo in cartelle presenti nello schedario personale dell'utente.")
   public static final String GET_PERSONAL_CABINET_FAILED = "28";

   @RBEntry("Contattare l'amministratore del sistema.")
   public static final String CONTACT_SYSTEM_ADMINISTRATOR = "29";

   @RBEntry("Il documento è nullo e non può essere eliminato.")
   public static final String NULL_DELETE_DOCUMENT = "30";

   @RBEntry("Tutte le iterazioni di questa versione di {0} verranno eliminate. Continuare l'eliminazione di {0}?")
   public static final String CONFIRM_DELETE_VERSIONS = "31";

   @RBEntry("Non è stato specificato alcun documento da eliminare.")
   public static final String NO_DOCUMENT_TO_DELETE = "32";

   @RBEntry("Non è stato fornito alcun riquadro per visualizzare la finestra di dialogo. Per visualizzare una finestra di dialogo è necessario fornire un riquadro")
   public static final String NO_PARENT_FRAME = "33";

   @RBEntry("Si è verificato un errore durante l'inizializzazione della Guida in linea: {0}.")
   public static final String INITIALIZE_HELP_FAILED = "34";

   @RBEntry("Si è verificato il seguente errore durante la localizzazione in {0}: {1}.")
   public static final String RESOURCE_BUNDLE_ERROR = "35";

   @RBEntry("Si è verificato un errore durante il salvataggio del contenuto di {0}.")
   public static final String CONTENT_NOT_UPLOADED = "36";

   @RBEntry("Errore durante l'inizializzazione dei componenti: {0}.")
   public static final String INIT_STRUCTURE_ERROR = "37";

   @RBEntry("Errore durante il salvataggio delle relazioni dei componenti per {0}: {1}.")
   public static final String STRUCTURE_SAVE_FAILED = "38";

   @RBEntry("Errore durante l'inizializzazione dei riferimenti: {0}.")
   public static final String INIT_REFERENCE_ERROR = "39";

   @RBEntry("Errore durante il salvataggio delle relazioni di riferimento per {0}: {1}.")
   public static final String REFERENCE_SAVE_FAILED = "40";

   @RBEntry("Nome")
   public static final String NAME_LBL = "41";

   @RBEntry("Numero")
   public static final String NUMBER_LBL = "42";

   @RBEntry("Versione")
   public static final String VERSION_LBL = "43";

   @RBEntry("La creazione del documento è fallita. Errore: {0}")
   public static final String CREATE_DOCUMENT_FAILED_ERR = "44";

   @RBEntry("Errore: i campi contrassegnati da un asterisco devono essere completati.")
   public static final String MISSING_REQUIRED_FIELD = "45";

   @RBEntry("L'aggiornamento del documento è fallito. Errore: {0}")
   public static final String UPDATE_DOCUMENT_FAILED_ERR = "46";

   @RBEntry("{0} {1}")
   public static final String CONCATENATION = "47";

   @RBEntry("Aggiungi file")
   public static final String ADD_FILE_LINK = "48";

   @RBEntry("Aggiungi URL")
   public static final String ADD_URL_LINK = "49";

   @RBEntry("Il Check-In del documento è fallito. Errore: {0}")
   public static final String CHECKIN_DOCUMENT_FAILED_ERR = "50";

   @RBEntry("Il valore del parametro per docOperation non è valido o manca.")
   public static final String INVALID_DOC_OPERATION = "51";

   @RBEntry("Immettere un valore per il nome.")
   @RBComment("Error message when user doesn't enter name for create Document.")
   public static final String NAME_VALUE_NULL = "52";

   @RBEntry("Immettere un valore per il numero.")
   @RBComment("Error message when user doesn't enter number for create Document.")
   public static final String NUMBER_VALUE_NULL = "53";

   @RBEntry("Elimina")
   public static final String DELETE = "54";

   @RBEntry("Nuova revisione")
   public static final String REVISE = "55";

   @RBEntry("Rinomina")
   public static final String RENAME = "56";

   @RBEntry("Annulla Check-Out")
   public static final String UndoCheckout = "57";

   @RBEntry("Eliminazione completata")
   public static final String DELETE_COMPLETE = "58";

   /**
    * 57.value=Are you sure that you want to delete this document and all of its iterations?
    * 57.constant=CONTINUE_TO_DELETE
    **/
   @RBEntry("Rinomina il documento {0}")
   public static final String RenameDocumentNumberName = "59";

   @RBEntry("Elimina il documento {0}")
   public static final String DeleteDocumentNumberName = "60";

   @RBEntry("*")
   public static final String REQUIRED_INDICATOR = "61";

   @RBEntry("I file seguenti non sono stati caricati a causa di percorsi file non validi:")
   @RBComment("Feedback after upload, this message is followed by a list of filepaths, each on a separate line (i.e. \"\nC:path\filename.doc\nC:path\filename.xls\").")
   public static final String INVALID_FILEPATH_LIST_HEADER = "62";

   @RBEntry("Problema durante il caricamento di dati sul server. Contattare l'amministratore se il problema persiste.")
   @RBComment("Feedback after upload when filepaths were valid but upload failed for some other reason.  This message should not be seen unless there are server misconfigurations or horrendous network problems.")
   public static final String UPLOAD_FAILURE_MESSAGE = "63";

   @RBEntry("Specificare un nome diverso. Massimo numero di caratteri consentiti: {0} ")
   @RBComment("error message when the name field exceeds the maximum number of characters allowed.")
   @RBArgComment0("maximum number of characters allowed for the Name field.")
   public static final String LONG_NAME_ERROR = "64";

   @RBEntry("Specificare un numero diverso. Massimo numero di caratteri consentito per il numero: {0} ")
   @RBComment("error message when the number field exceeds the maximum number of characters allowed.")
   @RBArgComment0("maximum number of characters allowed for the Number field.")
   public static final String LONG_NUMBER_ERROR = "65";

   @RBEntry("Specificare un titolo diverso. Numero massimo di caratteri consentiti per il titolo: {0} ")
   @RBComment("error message when the title field exceeds the maximum number of characters allowed.")
   @RBArgComment0("maximum number of characters allowed for the Title field.")
   public static final String LONG_TITLE_ERROR = "66";

   @RBEntry("Impossibile creare il documento {0} ({1}) perché l'identificativo non è univoco. Specificare un numero diverso.")
   @RBComment("error message when a duplicate number is entered.")
   @RBArgComment0("document number")
   @RBArgComment1("document name")
   public static final String NUMBER_UNIQUENESS_EXCEPTION = "67";

   @RBEntry("Il modello di team è stato ignorato in quanto è stato utilizzato il ciclo di vita di base.")
   public static final String IGNORE_TEAM = "68";

   @RBEntry("Aggiungi")
   public static final String PRIVATE_CONSTANT_83 = "addButton";

   @RBEntry("Aggiungi file")
   public static final String PRIVATE_CONSTANT_84 = "addFileLbl";

   @RBEntry("Aggiungi allegato")
   public static final String PRIVATE_CONSTANT_85 = "addFileTitle";

   @RBEntry("Aggiungi URL")
   public static final String PRIVATE_CONSTANT_86 = "addURLLbl";

   @RBEntry("Aggiungi allegato URL")
   public static final String PRIVATE_CONSTANT_87 = "addURLTitle";

   @RBEntry("Utilizzare i pulsanti Sfoglia per individuare i file da aggiungere come allegati.")
   public static final String PRIVATE_CONSTANT_88 = "browseForFileMessage";

   @RBEntry("Immettere gli URL da includere come allegati.")
   public static final String PRIVATE_CONSTANT_89 = "enterUrlMessage";

   @RBEntry("File")
   public static final String PRIVATE_CONSTANT_90 = "fileChoice";

   @RBEntry("File/URL")
   public static final String PRIVATE_CONSTANT_91 = "fileUrlHeader";

   @RBEntry("Formato:")
   public static final String PRIVATE_CONSTANT_92 = "formatLbl";

   @RBEntry("Nessun contenuto principale")
   public static final String PRIVATE_CONSTANT_93 = "noPrimaryChoice";

   @RBEntry("Percorso/Descrizione")
   public static final String PRIVATE_CONSTANT_94 = "pathDescriptionHeader";

   @RBEntry("Contenuto principale:")
   public static final String PRIVATE_CONSTANT_95 = "primaryContentLbl";

   @RBEntry("Rimuovi")
   public static final String PRIVATE_CONSTANT_96 = "removeButton";

   @RBEntry("Sostituisci allegati")
   public static final String PRIVATE_CONSTANT_97 = "replaceAttachmentTitle";

   @RBEntry("Sostituisci")
   public static final String PRIVATE_CONSTANT_98 = "replaceButton";

   @RBEntry("Seleziona gli allegati da sostituire usando le caselle di spunta nella colonna Seleziona.  Solo gli allegati persistenti possono essere sostituiti.")
   @RBComment("Javascript message.")
   public static final String PRIVATE_CONSTANT_99 = "selectAttachmentsToReplace";

   @RBEntry("Specificare gli allegati da rimuovere utilizzando le caselle di controllo nella colonna Seleziona.")
   @RBComment("Javascript message.")
   public static final String PRIVATE_CONSTANT_100 = "selectAttachmentsToRemove";

   @RBEntry("Seleziona:")
   public static final String PRIVATE_CONSTANT_101 = "selectLbl";

   @RBEntry("Da fare:")
   public static final String PRIVATE_CONSTANT_102 = "toDoLbl";

   @RBEntry("URL")
   public static final String PRIVATE_CONSTANT_103 = "urlChoice";

   @RBEntry("URL:")
   public static final String PRIVATE_CONSTANT_104 = "urlLbl";

   @RBEntry("Eliminare il documento e tutte le iterazioni?")
   public static final String PRIVATE_CONSTANT_105 = "CONTINUE_TO_DELETE";

   @RBEntry("Annullare il Check-Out di questo documento?")
   public static final String PRIVATE_CONSTANT_106 = "CONTINUE_UNDO_CHECKOUT";

   @RBEntry("È stato trascinato più di un file. Selezionarne solo uno per il contenuto principale.")
   @RBComment("Used in JavaScript for drag-and-drop.")
   public static final String PRIVATE_CONSTANT_107 = "tooManyFilesDroppedMsg";

   @RBEntry("Attivato ")
   public static final String PRIVATE_CONSTANT_108 = "EnabledLbl";
}
