/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.doc;

import wt.util.resource.*;

@RBUUID("wt.clients.doc.DocHelpRB")
public final class DocHelpRB extends WTListResourceBundle {
   /**
    *
    * Links to HTML files for Online Help
    *
    **/
   @RBEntry("wt/clients/doc/help_en/index.htm")
   @RBComment("Links to HTML files for Online Help")
   public static final String PRIVATE_CONSTANT_0 = "Contents/doc";

   @RBEntry("DocMgrOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_1 = "Help/doc";

   @RBEntry("DocMgrDocCreate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/doc/Create";

   @RBEntry("DocMgrDocView")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_3 = "Help/doc/View";

   @RBEntry("DocMgrDocUpdate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_4 = "Help/doc/Update";

   @RBEntry("DocMgrDocDelete")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_5 = "Help/doc/Delete";

   @RBEntry("DocMgrDocMoveToSharedCabinet")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_6 = "Help/doc/Move";

   @RBEntry("VCIterationRevise")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_7 = "Help/doc/Revise";

   @RBEntry("DocManagerObjectRename")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_8 = "Help/doc/Rename";

   @RBEntry("DocMgrDocUndoCheckOut")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_9 = "Help/doc/UndoCheckout";

   /**
    *
    * Descriptions to appear in the status bar of a frame when the
    * mouse cursor is over the associated component
    *
    **/
   @RBEntry("Enter the name of the document. This field is required.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_10 = "Desc/doc/Create/Name";

   @RBEntry("The name of the document.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_11 = "Desc/doc//Name";

   @RBEntry("Enter the type of the document.  This field is required.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_12 = "Desc/doc/Create/GeneralType";

   @RBEntry("The type of the document.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_13 = "Desc/doc//Type";

   @RBEntry("Enter the title of the document.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_14 = "Desc/doc/Create/Title";

   @RBEntry("The title of the document.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_15 = "Desc/doc//Title";

   @RBEntry("Choose the department of the document. This field is required.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_16 = "Desc/doc/Create/Department";

   @RBEntry("The department of the document.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_17 = "Desc/doc//Department";

   @RBEntry("Enter the number of the document.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_18 = "Desc/doc/Create/Number";

   @RBEntry("The document number.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_19 = "Desc/doc//Number";

   @RBEntry("Enter a description for the document.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_20 = "Desc/doc/Create/Description";

   @RBEntry("The description of the document.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_21 = "Desc/doc//Description";

   @RBEntry("Enter the folder in which to save the document.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_22 = "Desc/doc/Create/Location";

   @RBEntry("The folder in which the document lives.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_23 = "Desc/doc//Location";

   @RBEntry("Click to browse for a folder in which to create the document.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_24 = "Desc/doc/Create/Browse";

   @RBEntry("Click to add contents to the document.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_25 = "Desc/doc/Create/Contents";

   @RBEntry("Click to view and update the document contents.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_26 = "Desc/doc/Update/Contents";

   @RBEntry("Click to view the document contents.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_27 = "Desc/doc/View/Contents";

   @RBEntry("Click to save the document and close this window.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_28 = "Desc/doc//OK";

   @RBEntry("Click to save the document.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_29 = "Desc/doc//Save";

   @RBEntry("Click to lose any unsaved changes and close the window.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_30 = "Desc/doc//Cancel";

   @RBEntry("Click to access online help.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_31 = "Desc/doc//Help";

   @RBEntry("Click to close this window.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_32 = "Desc/doc//Close";

   @RBEntry("The version of the document.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_33 = "Desc/doc//Version";

   @RBEntry("The date the document was initially created.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_34 = "Desc/doc//Created";

   @RBEntry("The initial creator of the document.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_35 = "Desc/doc//CreatedBy";

   @RBEntry("The checked out status of the document.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_36 = "Desc/doc//Status";

   @RBEntry("The date the document was last modified.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_37 = "Desc/doc//Modified";

   @RBEntry("The last person to modify the document.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_38 = "Desc/doc//ModifiedBy";
}
