/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.doc;

import wt.util.resource.*;

@RBUUID("wt.clients.doc.DocRB")
public final class DocRB extends WTListResourceBundle {
   /**
    * FIELD LABELS
    * Labels used to identify fields on the frames and dialogs
    **/
   @RBEntry("Files")
   public static final String PRIVATE_CONSTANT_0 = "filesLbl";

   @RBEntry("URLs")
   public static final String PRIVATE_CONSTANT_1 = "urlsLbl";

   @RBEntry("Comments:")
   public static final String PRIVATE_CONSTANT_2 = "commentLbl";

   @RBEntry("Secondary Contents:")
   public static final String PRIVATE_CONSTANT_3 = "contentsLbl";

   @RBEntry("Created")
   public static final String PRIVATE_CONSTANT_4 = "createdLbl";

   @RBEntry("Created By:")
   public static final String PRIVATE_CONSTANT_5 = "createdByLbl";

   @RBEntry("Created On:")
   public static final String PRIVATE_CONSTANT_6 = "createdOnLbl";

   @RBEntry("Description:")
   public static final String PRIVATE_CONSTANT_7 = "descriptionLbl";

   @RBEntry("Department:")
   public static final String PRIVATE_CONSTANT_8 = "docDepartmentLbl";

   @RBEntry("Format:")
   public static final String PRIVATE_CONSTANT_9 = "docFormatLbl";

   @RBEntry("Iteration:")
   public static final String PRIVATE_CONSTANT_10 = "docIterationLbl";

   @RBEntry("Location:")
   public static final String PRIVATE_CONSTANT_11 = "docLocationLbl";

   @RBEntry("Organization ID:")
   public static final String orgIdLbl = "orgIdLbl";

   @RBEntry("Name:")
   public static final String PRIVATE_CONSTANT_12 = "docNameLbl";

   @RBEntry("Number:")
   public static final String PRIVATE_CONSTANT_13 = "docNumberLbl";

   @RBEntry("Size:")
   public static final String PRIVATE_CONSTANT_14 = "docSizeLbl";

   @RBEntry("Status:")
   public static final String PRIVATE_CONSTANT_15 = "docStatusLbl";

   @RBEntry("Title:")
   public static final String PRIVATE_CONSTANT_16 = "docTitleLbl";

   @RBEntry("Type:")
   public static final String PRIVATE_CONSTANT_17 = "docTypeLbl";

   @RBEntry("Version:")
   public static final String PRIVATE_CONSTANT_18 = "docVersionLbl";

   @RBEntry("Folder Name:")
   public static final String PRIVATE_CONSTANT_19 = "folderNameLbl";

   @RBEntry("Location:")
   public static final String PRIVATE_CONSTANT_20 = "folderPathLbl";

   @RBEntry("General")
   public static final String PRIVATE_CONSTANT_21 = "generalLbl";

   @RBEntry("In Cabinet:")
   public static final String PRIVATE_CONSTANT_22 = "inCabinetLbl";

   @RBEntry("Life Cycle:")
   public static final String PRIVATE_CONSTANT_23 = "lifeCycleNameLbl";

   @RBEntry("State:")
   public static final String PRIVATE_CONSTANT_24 = "lifeCycleStateLbl";

   @RBEntry("Name:")
   public static final String PRIVATE_CONSTANT_25 = "nameLbl";

   @RBEntry("Project:")
   public static final String PRIVATE_CONSTANT_26 = "projectLbl";

   @RBEntry("Team:")
   public static final String PRIVATE_CONSTANT_27 = "teamLbl";

   @RBEntry("Last Modified:")
   public static final String PRIVATE_CONSTANT_28 = "updatedOnLbl";

   @RBEntry("Modified By:")
   public static final String PRIVATE_CONSTANT_29 = "updatedByLbl";

   @RBEntry("Attach File:")
   public static final String PRIVATE_CONSTANT_30 = "attachFileLbl";

   @RBEntry("Attach URL:")
   public static final String PRIVATE_CONSTANT_31 = "attachURLLbl";

   @RBEntry("URL Description:")
   public static final String PRIVATE_CONSTANT_32 = "urlDescLbl";

   /**
    * BUTTON LABELS
    * Labels on command buttons
    **/
   @RBEntry("Apply")
   public static final String PRIVATE_CONSTANT_33 = "applyButton";

   @RBEntry("Browse")
   public static final String PRIVATE_CONSTANT_34 = "browseButton";

   @RBEntry("Cancel")
   public static final String PRIVATE_CONSTANT_35 = "cancelButton";

   @RBEntry("Close")
   public static final String PRIVATE_CONSTANT_36 = "closeButton";

   @RBEntry("Contents")
   public static final String PRIVATE_CONSTANT_37 = "contentsButton";

   @RBEntry("Help")
   public static final String PRIVATE_CONSTANT_38 = "helpButton";

   @RBEntry("OK")
   public static final String PRIVATE_CONSTANT_39 = "okButton";

   @RBEntry("Save")
   public static final String PRIVATE_CONSTANT_40 = "saveButton";

   @RBEntry("Reset")
   public static final String PRIVATE_CONSTANT_41 = "resetButton";

   @RBEntry("Add Attachments")
   public static final String PRIVATE_CONSTANT_42 = "addAttachmentsButton";

   /**
    * CHECKBOX LABELS
    **/
   @RBEntry("Submit for Life Cycle Promotion")
   @RBComment("Checkbox label.")
   public static final String PRIVATE_CONSTANT_43 = "submitLbl";

   /**
    * SYMBOLS
    **/
   @RBEntry("...")
   public static final String PRIVATE_CONSTANT_44 = "ellipses";

   @RBEntry("*")
   public static final String PRIVATE_CONSTANT_45 = "required";

   @RBEntry(":")
   public static final String PRIVATE_CONSTANT_46 = "labelColon";

   /**
    * TITLES
    **/
   @RBEntry("Checked In")
   public static final String PRIVATE_CONSTANT_47 = "docCheckedIn";

   @RBEntry("Checked Out")
   public static final String PRIVATE_CONSTANT_48 = "docCheckedOut";

   @RBEntry("Checked Out by {0}")
   public static final String PRIVATE_CONSTANT_49 = "docCheckedOutBy";

   @RBEntry("Not Checked Out")
   public static final String PRIVATE_CONSTANT_50 = "docNotCheckedOut";

   @RBEntry("View Document <{0}>")
   public static final String PRIVATE_CONSTANT_51 = "viewDocument";

   @RBEntry("Working Copy of {0}")
   public static final String PRIVATE_CONSTANT_52 = "workingCopyTitle";

   @RBEntry("Create Document")
   public static final String PRIVATE_CONSTANT_53 = "createDocument";

   @RBEntry("Create Document {0}")
   public static final String PRIVATE_CONSTANT_54 = "createDocumentNumber";

   @RBEntry("Create document {0} ({1})")
   public static final String PRIVATE_CONSTANT_55 = "createDocumentNumberName";

   @RBEntry("Creation of document has been cancelled.")
   public static final String PRIVATE_CONSTANT_56 = "createDocumentCancelled";

   @RBEntry("Update Document <{0}>")
   public static final String PRIVATE_CONSTANT_57 = "updateDocument";

   @RBEntry("Update document {0}")
   public static final String PRIVATE_CONSTANT_58 = "updateDocumentNumberName";

   @RBEntry("Updating document {0}...")
   public static final String PRIVATE_CONSTANT_59 = "updateDocumentProcessing";

   @RBEntry("{0} has been successfully updated.")
   public static final String PRIVATE_CONSTANT_60 = "updateDocumentSuccess";

   @RBEntry("Update of {0} has been cancelled.")
   public static final String PRIVATE_CONSTANT_61 = "updateDocumentCancel";

   @RBEntry("Check in document {0}")
   public static final String PRIVATE_CONSTANT_62 = "checkinDocumentNumberName";

   @RBEntry("Checking in {0}...")
   public static final String PRIVATE_CONSTANT_63 = "checkinDocumentProcessing";

   @RBEntry("{0} has been successfully checked in.")
   public static final String PRIVATE_CONSTANT_64 = "checkinDocumentSuccess";

   @RBEntry("Check-in of {0} has been cancelled.")
   public static final String PRIVATE_CONSTANT_65 = "checkinDocumentCancel";

   @RBEntry("{0} has been successfully checked out.")
   public static final String PRIVATE_CONSTANT_66 = "checkoutDocumentSuccess";

   @RBEntry("Downloading primary content...")
   public static final String PRIVATE_CONSTANT_67 = "downloading";

   @RBEntry("Rename document {0}")
   public static final String PRIVATE_CONSTANT_68 = "RenameDocumentNumberName";

   @RBEntry("Revise document {0}")
   public static final String PRIVATE_CONSTANT_69 = "ReviseDocumentNumberName";

   @RBEntry("Delete document {0}")
   public static final String PRIVATE_CONSTANT_70 = "DeleteDocumentNumberName";

   @RBEntry("Undo checkout on document {0}")
   public static final String PRIVATE_CONSTANT_71 = "UndoCheckoutDocumentNumberName";

   @RBEntry("Move document {0}")
   public static final String PRIVATE_CONSTANT_72 = "MoveDocumentNumberName";

   @RBEntry("Move of {0} has been cancelled.")
   public static final String PRIVATE_CONSTANT_73 = "moveDocumentCancel";

   @RBEntry("Revision of {0} has been cancelled.")
   public static final String PRIVATE_CONSTANT_74 = "reviseDocumentCancel";

   @RBEntry("Delete of {0} has been cancelled.")
   public static final String PRIVATE_CONSTANT_75 = "deleteDocumentCancel";

   @RBEntry("Undo checkout of {0} has been cancelled.")
   public static final String PRIVATE_CONSTANT_76 = "undoCheckoutDocumentCancel";

   @RBEntry("Rename of {0} has been cancelled.")
   public static final String PRIVATE_CONSTANT_77 = "renameDocumentCancel";

   /**
    * TABS
    **/
   @RBEntry("General")
   public static final String PRIVATE_CONSTANT_78 = "generalTab";

   @RBEntry("Attachments")
   public static final String PRIVATE_CONSTANT_79 = "attachmentsTab";

   @RBEntry("Referenced Documents")
   public static final String PRIVATE_CONSTANT_80 = "referencedTab";

   @RBEntry("Structure")
   public static final String PRIVATE_CONSTANT_81 = "structureTab";

   /**
    * PANEL MESSAGES
    **/
   @RBEntry("Please save before adding contents...")
   @RBComment("Panel Message")
   public static final String PRIVATE_CONSTANT_82 = "saveMessageLbl";

   /**
    * STATUS BAR MESSAGES
    **/
   @RBEntry("Document {0} was successfully created.")
   @RBComment("Status bar message.")
   public static final String CREATE_DOCUMENT_SUCCESSFUL = "1";

   @RBEntry("Create document failed.")
   @RBComment("Status bar message.")
   public static final String CREATE_DOCUMENT_FAILED = "2";

   @RBEntry("Document {0} was successfully updated.")
   @RBComment("Status bar message.")
   public static final String UPDATE_DOCUMENT_SUCCESSFUL = "3";

   @RBEntry("Update document {0} failed.")
   @RBComment("Status bar message.")
   public static final String UPDATE_DOCUMENT_FAILURE = "4";

   @RBEntry("Personal cabinet was not found.")
   @RBComment("Status bar message.")
   public static final String PERSONAL_CABINET_NOT_FOUND = "5";

   @RBEntry("Save cancelled.")
   @RBComment("Status bar message.")
   public static final String SAVE_CANCELLED = "6";

   @RBEntry("No changes have been made that need saving.")
   @RBComment("Status bar message.")
   public static final String NO_UPDATES_TO_SAVE = "7";

   @RBEntry("Please provide a value for \"{0}\".")
   public static final String NULL_ATTRIBUTE_VALUE = "8";

   @RBEntry("You do not currently have document \"{0}\" checked out.")
   public static final String NOT_CHECKOUT_OWNER = "9";

   @RBEntry("Document \"{0}\" is currently checked out by {1}")
   public static final String CHECKED_OUT_BY_OTHER = "10";

   @RBEntry("\"{0}\" is not a valid folder path.")
   public static final String INVALID_FOLDER_PATH = "11";

   @RBEntry("\"{0}\" is not a valid value for property \"{1}\".")
   public static final String PROPERTY_VETO_EXCEPTION = "12";

   @RBEntry("You currently have document \"{0}\" checked out.  Please update the working copy in your \"{1}\" folder.")
   public static final String UPDATE_WORKING_COPY = "13";

   @RBEntry("You need to have document \"{0}\" checked out in order to update it.")
   public static final String UPDATE_NOT_ALLOWED = "14";

   @RBEntry("You currently have document \"{0}\" checked out. ")
   public static final String CHECKED_OUT_BY_USER = "15";

   @RBEntry("The folder in which to create the document must be specified.")
   public static final String PARENT_FOLDER_REQUIRED = "16";

   @RBEntry("An error occurred while initializing content: {0}.")
   public static final String INIT_CONTENT_ERROR = "17";

   @RBEntry("An error occurred while attempting to get the URL for online help: {0}.")
   public static final String INIT_HELP_URL_FAILED = "18";

   @RBEntry("You have made content changes that have not yet been saved.  Save changes now?")
   public static final String CONFIRM_UNSAVED_CHANGES = "19";

   @RBEntry("An error occurred while saving the contents of {0}: {1}.")
   public static final String CONTENT_SAVE_FAILED = "20";

   @RBEntry("Documents may only be created in your personal cabinet, '{0}'.  Folder '{1}' is not in your personal cabinet.")
   public static final String NOT_IN_PERSONAL_CABINET = "21";

   @RBEntry("An error occurred while attempting to update document {0}: {1}.")
   public static final String UPDATE_DOCUMENT_FAILED = "23";

   @RBEntry("No document has been specified to update.")
   public static final String NO_DOCUMENT_TO_UPDATE = "24";

   @RBEntry("The given object must be a WTDocument in order to initiate this task. ")
   public static final String OBJECT_NOT_WTDOCUMENT = "25";

   @RBEntry("The given document is null and cannot be viewed.")
   public static final String NULL_VIEW_DOCUMENT = "26";

   @RBEntry("A new document can only be created in a folder which is in the current user's personal cabinet.  But, the personal cabinet for user {0} could not be found.")
   public static final String NO_PERSONAL_CABINET = "27";

   @RBEntry("An error occurred retrieving the personal cabinet for user {0}.  New documents can only be saved in folders in the user's personal cabinet.")
   public static final String GET_PERSONAL_CABINET_FAILED = "28";

   @RBEntry("Please contact your system administrator.")
   public static final String CONTACT_SYSTEM_ADMINISTRATOR = "29";

   @RBEntry("The given document is null and cannot be deleted.")
   public static final String NULL_DELETE_DOCUMENT = "30";

   @RBEntry("All iterations in this version of {0} will be deleted. Continue deleting {0}?")
   public static final String CONFIRM_DELETE_VERSIONS = "31";

   @RBEntry("No document has been given to be deleted.")
   public static final String NO_DOCUMENT_TO_DELETE = "32";

   @RBEntry("No Frame has been provided to launch the dialog.  In order to display the dialog, a Frame must be given")
   public static final String NO_PARENT_FRAME = "33";

   @RBEntry("An error occurred attempting to initialize the online help system: {0}.")
   public static final String INITIALIZE_HELP_FAILED = "34";

   @RBEntry("The following error ocurred performing localization in {0}: {1}.")
   public static final String RESOURCE_BUNDLE_ERROR = "35";

   @RBEntry("An error occurred while saving the contents of {0}.")
   public static final String CONTENT_NOT_UPLOADED = "36";

   @RBEntry("An error occurred while initializing the uses: {0}.")
   public static final String INIT_STRUCTURE_ERROR = "37";

   @RBEntry("An error occurred while saving the uses relationships for {0}: {1}.")
   public static final String STRUCTURE_SAVE_FAILED = "38";

   @RBEntry("An error occurred while initializing the references: {0}.")
   public static final String INIT_REFERENCE_ERROR = "39";

   @RBEntry("An error occurred while saving the references relationships for {0}: {1}.")
   public static final String REFERENCE_SAVE_FAILED = "40";

   @RBEntry("Name")
   public static final String NAME_LBL = "41";

   @RBEntry("Number")
   public static final String NUMBER_LBL = "42";

   @RBEntry("Version")
   public static final String VERSION_LBL = "43";

   @RBEntry("Document creation failed with the following error: {0}")
   public static final String CREATE_DOCUMENT_FAILED_ERR = "44";

   @RBEntry("Error: you need to enter values for fields with asterisks by their names.")
   public static final String MISSING_REQUIRED_FIELD = "45";

   @RBEntry("Update of the document failed with the following error: {0}")
   public static final String UPDATE_DOCUMENT_FAILED_ERR = "46";

   @RBEntry("{0} {1}")
   public static final String CONCATENATION = "47";

   @RBEntry("Add File")
   public static final String ADD_FILE_LINK = "48";

   @RBEntry("Add URL")
   public static final String ADD_URL_LINK = "49";

   @RBEntry("Checkin of the document failed with the following error: {0}")
   public static final String CHECKIN_DOCUMENT_FAILED_ERR = "50";

   @RBEntry("Invalid or missing parameter value for docOperation.")
   public static final String INVALID_DOC_OPERATION = "51";

   @RBEntry("Please provide a value for name.")
   @RBComment("Error message when user doesn't enter name for create Document.")
   public static final String NAME_VALUE_NULL = "52";

   @RBEntry("Please provide a value for number.")
   @RBComment("Error message when user doesn't enter number for create Document.")
   public static final String NUMBER_VALUE_NULL = "53";

   @RBEntry("Delete")
   public static final String DELETE = "54";

   @RBEntry("Revise")
   public static final String REVISE = "55";

   @RBEntry("Rename")
   public static final String RENAME = "56";

   @RBEntry("Undo Checkout")
   public static final String UndoCheckout = "57";

   @RBEntry(" Delete Complete")
   public static final String DELETE_COMPLETE = "58";

   /**
    * 57.value=Are you sure that you want to delete this document and all of its iterations?
    * 57.constant=CONTINUE_TO_DELETE
    **/
   @RBEntry("Rename document {0}")
   public static final String RenameDocumentNumberName = "59";

   @RBEntry("Delete document {0}")
   public static final String DeleteDocumentNumberName = "60";

   @RBEntry("*")
   public static final String REQUIRED_INDICATOR = "61";

   @RBEntry("The following files were not uploaded due to invalid filepaths:")
   @RBComment("Feedback after upload, this message is followed by a list of filepaths, each on a separate line (i.e. \"\nC:path\filename.doc\nC:path\filename.xls\").")
   public static final String INVALID_FILEPATH_LIST_HEADER = "62";

   @RBEntry("A problem occurred while uploading content to server.  Please notify administrator if this problem continues.")
   @RBComment("Feedback after upload when filepaths were valid but upload failed for some other reason.  This message should not be seen unless there are server misconfigurations or horrendous network problems.")
   public static final String UPLOAD_FAILURE_MESSAGE = "63";

   @RBEntry("Please enter a different name. Maximum number of characters allowed for Name: {0} ")
   @RBComment("error message when the name field exceeds the maximum number of characters allowed.")
   @RBArgComment0("maximum number of characters allowed for the Name field.")
   public static final String LONG_NAME_ERROR = "64";

   @RBEntry("Please enter a different number. Maximum number of characters allowed for Number: {0} ")
   @RBComment("error message when the number field exceeds the maximum number of characters allowed.")
   @RBArgComment0("maximum number of characters allowed for the Number field.")
   public static final String LONG_NUMBER_ERROR = "65";

   @RBEntry("Please enter a different title. Maximum number of characters allowed for Title: {0} ")
   @RBComment("error message when the title field exceeds the maximum number of characters allowed.")
   @RBArgComment0("maximum number of characters allowed for the Title field.")
   public static final String LONG_TITLE_ERROR = "66";

   @RBEntry("Cannot create document {0} ({1}) because its identity is not unique. Please enter a different number.")
   @RBComment("error message when a duplicate number is entered.")
   @RBArgComment0("document number")
   @RBArgComment1("document name")
   public static final String NUMBER_UNIQUENESS_EXCEPTION = "67";

   @RBEntry("Team template ignored since basic lifecycle was used.")
   public static final String IGNORE_TEAM = "68";

   @RBEntry("Add")
   public static final String PRIVATE_CONSTANT_83 = "addButton";

   @RBEntry("Add File")
   public static final String PRIVATE_CONSTANT_84 = "addFileLbl";

   @RBEntry("Add File Attachment")
   public static final String PRIVATE_CONSTANT_85 = "addFileTitle";

   @RBEntry("Add URL")
   public static final String PRIVATE_CONSTANT_86 = "addURLLbl";

   @RBEntry("Add URL Attachment")
   public static final String PRIVATE_CONSTANT_87 = "addURLTitle";

   @RBEntry("Use the Browse buttons to locate the file(s) to add as attachment(s).")
   public static final String PRIVATE_CONSTANT_88 = "browseForFileMessage";

   @RBEntry("Enter the URL's to be included as attachments.")
   public static final String PRIVATE_CONSTANT_89 = "enterUrlMessage";

   @RBEntry("File")
   public static final String PRIVATE_CONSTANT_90 = "fileChoice";

   @RBEntry("File/URL")
   public static final String PRIVATE_CONSTANT_91 = "fileUrlHeader";

   @RBEntry("Format:")
   public static final String PRIVATE_CONSTANT_92 = "formatLbl";

   @RBEntry("No primary content")
   public static final String PRIVATE_CONSTANT_93 = "noPrimaryChoice";

   @RBEntry("Path/Description")
   public static final String PRIVATE_CONSTANT_94 = "pathDescriptionHeader";

   @RBEntry("Primary Content:")
   public static final String PRIVATE_CONSTANT_95 = "primaryContentLbl";

   @RBEntry("Remove")
   public static final String PRIVATE_CONSTANT_96 = "removeButton";

   @RBEntry("Replace Attachment(s)")
   public static final String PRIVATE_CONSTANT_97 = "replaceAttachmentTitle";

   @RBEntry("Replace")
   public static final String PRIVATE_CONSTANT_98 = "replaceButton";

   @RBEntry("Select the attachment(s) to replace using the checkboxes in the Select column.  Only persisted attachments can be replaced.")
   @RBComment("Javascript message.")
   public static final String PRIVATE_CONSTANT_99 = "selectAttachmentsToReplace";

   @RBEntry("Select the attachment(s) to remove using the checkboxes in the Select column.")
   @RBComment("Javascript message.")
   public static final String PRIVATE_CONSTANT_100 = "selectAttachmentsToRemove";

   @RBEntry("Select:")
   public static final String PRIVATE_CONSTANT_101 = "selectLbl";

   @RBEntry("To-Do:")
   public static final String PRIVATE_CONSTANT_102 = "toDoLbl";

   @RBEntry("URL")
   public static final String PRIVATE_CONSTANT_103 = "urlChoice";

   @RBEntry("URL:")
   public static final String PRIVATE_CONSTANT_104 = "urlLbl";

   @RBEntry("Are you sure that you want to delete this document and all of its iterations?")
   public static final String PRIVATE_CONSTANT_105 = "CONTINUE_TO_DELETE";

   @RBEntry("Are you sure that you want to undo the checkout on this document?")
   public static final String PRIVATE_CONSTANT_106 = "CONTINUE_UNDO_CHECKOUT";

   @RBEntry("More than one file has been dropped. Select only one file for primary content.")
   @RBComment("Used in JavaScript for drag-and-drop.")
   public static final String PRIVATE_CONSTANT_107 = "tooManyFilesDroppedMsg";

   @RBEntry("Enabled")
   public static final String PRIVATE_CONSTANT_108 = "EnabledLbl";
}
