/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.doc;

import wt.util.resource.*;

@RBUUID("wt.clients.doc.DocHelpRB")
public final class DocHelpRB_it extends WTListResourceBundle {
   /**
    *
    * Links to HTML files for Online Help
    *
    **/
   @RBEntry("wt/clients/doc/help_it/index.htm")
   @RBComment("Links to HTML files for Online Help")
   public static final String PRIVATE_CONSTANT_0 = "Contents/doc";

   @RBEntry("DocMgrOview")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_1 = "Help/doc";

   @RBEntry("DocMgrDocCreate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/doc/Create";

   @RBEntry("DocMgrDocView")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_3 = "Help/doc/View";

   @RBEntry("DocMgrDocUpdate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_4 = "Help/doc/Update";

   @RBEntry("DocMgrDocDelete")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_5 = "Help/doc/Delete";

   @RBEntry("DocMgrDocMoveToSharedCabinet")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_6 = "Help/doc/Move";

   @RBEntry("VCIterationRevise")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_7 = "Help/doc/Revise";

   @RBEntry("DocManagerObjectRename")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_8 = "Help/doc/Rename";

   @RBEntry("DocMgrDocUndoCheckOut")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_9 = "Help/doc/UndoCheckout";

   /**
    *
    * Descriptions to appear in the status bar of a frame when the
    * mouse cursor is over the associated component
    *
    **/
   @RBEntry("Immettere il nome del documento. Campo obbligatorio.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_10 = "Desc/doc/Create/Name";

   @RBEntry("Nome del documento.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_11 = "Desc/doc//Name";

   @RBEntry("Immettere il tipo del documento.  Campo obbligatorio.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_12 = "Desc/doc/Create/GeneralType";

   @RBEntry("Tipo del documento.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_13 = "Desc/doc//Type";

   @RBEntry("Immettere il titolo del documento.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_14 = "Desc/doc/Create/Title";

   @RBEntry("Titolo del documento.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_15 = "Desc/doc//Title";

   @RBEntry("Selezionare il reparto del documento. Campo obbligatorio.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_16 = "Desc/doc/Create/Department";

   @RBEntry("Reparto del documento.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_17 = "Desc/doc//Department";

   @RBEntry("Immettere il numero del documento.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_18 = "Desc/doc/Create/Number";

   @RBEntry("Numero del documento.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_19 = "Desc/doc//Number";

   @RBEntry("Immettere una descrizione per il documento.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_20 = "Desc/doc/Create/Description";

   @RBEntry("Descrizione del documento.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_21 = "Desc/doc//Description";

   @RBEntry("Immettere la cartella in cui salvare il documento.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_22 = "Desc/doc/Create/Location";

   @RBEntry("Cartella in cui è memorizzato il documento.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_23 = "Desc/doc//Location";

   @RBEntry("Fare clic per cercare una cartella in cui creare il documento.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_24 = "Desc/doc/Create/Browse";

   @RBEntry("Fare clic per aggiungere contenuto al documento.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_25 = "Desc/doc/Create/Contents";

   @RBEntry("Fare clic per visualizzare e aggiornare il contenuto del documento.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_26 = "Desc/doc/Update/Contents";

   @RBEntry("Fare clic per visualizzare il contenuto del documento.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_27 = "Desc/doc/View/Contents";

   @RBEntry("Fare clic per salvare il documento e chiudere la finestra.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_28 = "Desc/doc//OK";

   @RBEntry("Fare clic per salvare il documento.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_29 = "Desc/doc//Save";

   @RBEntry("Fare clic per annullare le modifiche non salvate e chiudere la finestra.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_30 = "Desc/doc//Cancel";

   @RBEntry("Fare clic per accedere alla Guida in linea.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_31 = "Desc/doc//Help";

   @RBEntry("Fare clic per chiudere la finestra.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_32 = "Desc/doc//Close";

   @RBEntry("Versione del documento.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_33 = "Desc/doc//Version";

   @RBEntry("Data di creazione del documento")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_34 = "Desc/doc//Created";

   @RBEntry("Autore del documento.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_35 = "Desc/doc//CreatedBy";

   @RBEntry("Documento sottoposto a Check-Out.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_36 = "Desc/doc//Status";

   @RBEntry("Data dell'ultima modifica del documento.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_37 = "Desc/doc//Modified";

   @RBEntry("Autore dell'ultima modifica.")
   @RBComment("Descriptions to appear in the status bar of a frame when the mouse cursor is over the associated component")
   public static final String PRIVATE_CONSTANT_38 = "Desc/doc//ModifiedBy";
}
