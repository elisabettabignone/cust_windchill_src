/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.epm;

import wt.util.resource.*;

@RBUUID("wt.clients.epm.EPMRB")
public final class EPMRB extends WTListResourceBundle {
   /**
    * Messages
    **/
   @RBEntry("Not yet supported.")
   public static final String NOT_YET_SUPPORTED = "0";

   @RBEntry("Wrong object type for EPMDocument task delegate")
   public static final String WRONG_OBJECT_TYPE = "1";

   /**
    * Labels
    **/
   @RBEntry("Description:")
   @RBComment("Label")
   public static final String PRIVATE_CONSTANT_0 = "descriptionLbl";

   @RBEntry("Owner Application:")
   @RBComment("Label")
   public static final String PRIVATE_CONSTANT_1 = "ownerAppLbl";

   /**
    * Button Labels
    **/
   @RBEntry("Placed")
   @RBComment("Button Label")
   public static final String PRIVATE_CONSTANT_2 = "placedHeading";

   @RBEntry("Required")
   @RBComment("Button Label")
   public static final String PRIVATE_CONSTANT_3 = "requiredHeading";

   @RBEntry("Suppressed")
   @RBComment("Button Label")
   public static final String PRIVATE_CONSTANT_4 = "suppressedHeading";

   /**
    * Symbols
    * Titles
    **/
   @RBEntry("Create CAD Document {0}")
   @RBComment("Title")
   public static final String PRIVATE_CONSTANT_5 = "createEPMDocumentTitle";

   @RBEntry("Update CAD Document <{0}>")
   @RBComment("Title")
   public static final String PRIVATE_CONSTANT_6 = "updateEPMDocumentTitle";

   @RBEntry("View CAD Document <{0}>")
   @RBComment("Title")
   public static final String PRIVATE_CONSTANT_7 = "viewEPMDocumentTitle";

   /**
    * Titles in tabbed panel
    **/
   @RBEntry("Describes")
   public static final String PRIVATE_CONSTANT_8 = "associatedTitle";

   @RBEntry("General")
   @RBComment("Title in tabbed panel")
   public static final String PRIVATE_CONSTANT_9 = "generalTitle";

   @RBEntry("Referenced By")
   @RBComment("Title in tabbed panel")
   public static final String PRIVATE_CONSTANT_10 = "referencedByTitle";
}
