/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.epm;

import wt.util.resource.*;

@RBUUID("wt.clients.epm.EPMRB")
public final class EPMRB_it extends WTListResourceBundle {
   /**
    * Messages
    **/
   @RBEntry("Non ancora supportato.")
   public static final String NOT_YET_SUPPORTED = "0";

   @RBEntry("Tipo di oggetto non corretto per il delegato di task di EPMDocument")
   public static final String WRONG_OBJECT_TYPE = "1";

   /**
    * Labels
    **/
   @RBEntry("Descrizione:")
   @RBComment("Label")
   public static final String PRIVATE_CONSTANT_0 = "descriptionLbl";

   @RBEntry("Applicazione proprietaria:")
   @RBComment("Label")
   public static final String PRIVATE_CONSTANT_1 = "ownerAppLbl";

   /**
    * Button Labels
    **/
   @RBEntry("Collocato")
   @RBComment("Button Label")
   public static final String PRIVATE_CONSTANT_2 = "placedHeading";

   @RBEntry("Obbligatorio")
   @RBComment("Button Label")
   public static final String PRIVATE_CONSTANT_3 = "requiredHeading";

   @RBEntry("Eliminato")
   @RBComment("Button Label")
   public static final String PRIVATE_CONSTANT_4 = "suppressedHeading";

   /**
    * Symbols
    * Titles
    **/
   @RBEntry("Crea documento CAD {0}")
   @RBComment("Title")
   public static final String PRIVATE_CONSTANT_5 = "createEPMDocumentTitle";

   @RBEntry("Aggiorna documento CAD <{0}>")
   @RBComment("Title")
   public static final String PRIVATE_CONSTANT_6 = "updateEPMDocumentTitle";

   @RBEntry("Visualizza documento CAD <{0}>")
   @RBComment("Title")
   public static final String PRIVATE_CONSTANT_7 = "viewEPMDocumentTitle";

   /**
    * Titles in tabbed panel
    **/
   @RBEntry("Descrive")
   public static final String PRIVATE_CONSTANT_8 = "associatedTitle";

   @RBEntry("Generale")
   @RBComment("Title in tabbed panel")
   public static final String PRIVATE_CONSTANT_9 = "generalTitle";

   @RBEntry("Referenziato da")
   @RBComment("Title in tabbed panel")
   public static final String PRIVATE_CONSTANT_10 = "referencedByTitle";
}
