/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.vc;

import wt.util.resource.*;

@RBUUID("wt.clients.vc.VcRB")
public final class VcRB_it extends WTListResourceBundle {
   /**
    * DIALOG LABELS
    * Labels used to identify fields on the dialogs
    **/
   @RBEntry("Check-In di:")
   public static final String PRIVATE_CONSTANT_0 = "checkingInLabel";

   @RBEntry("Check-Out di:")
   public static final String PRIVATE_CONSTANT_1 = "checkingOutLabel";

   @RBEntry("Commenti:")
   public static final String PRIVATE_CONSTANT_2 = "commentLabel";

   @RBEntry("Posizione:")
   public static final String PRIVATE_CONSTANT_3 = "locationLabel";

   @RBEntry("Nome:")
   public static final String PRIVATE_CONSTANT_4 = "nameLabel";

   @RBEntry("Tipo:")
   public static final String PRIVATE_CONSTANT_5 = "typeLabel";

   @RBEntry("Cronologia iterazioni")
   public static final String PRIVATE_CONSTANT_6 = "iterationHistoryOf";

   @RBEntry("Tutte le versioni di")
   public static final String PRIVATE_CONSTANT_7 = "allVersionsOf";

   @RBEntry("Vista:")
   public static final String PRIVATE_CONSTANT_8 = "viewLabel";

   /**
    * BUTTON LABELS
    * Labels used on command buttons
    **/
   @RBEntry("OK")
   public static final String PRIVATE_CONSTANT_9 = "okButton";

   @RBEntry("Annulla")
   public static final String PRIVATE_CONSTANT_10 = "cancelButton";

   @RBEntry("Continua")
   public static final String PRIVATE_CONSTANT_11 = "continueButton";

   @RBEntry("Guida")
   public static final String PRIVATE_CONSTANT_12 = "helpButton";

   @RBEntry("Visualizza")
   public static final String PRIVATE_CONSTANT_13 = "viewButton";

   @RBEntry("Navigatore Windchill")
   public static final String PRIVATE_CONSTANT_14 = "explorerButton";

   @RBEntry("Chiudi")
   public static final String PRIVATE_CONSTANT_15 = "closeButton";

   @RBEntry("Non scaricare")
   public static final String PRIVATE_CONSTANT_16 = "doNotDownloadButton";

   @RBEntry("Non caricare")
   public static final String PRIVATE_CONSTANT_17 = "doNotUploadButton";

   @RBEntry("Scarica")
   public static final String PRIVATE_CONSTANT_18 = "downloadButton";

   @RBEntry("Carica")
   public static final String PRIVATE_CONSTANT_19 = "uploadButton";

   @RBEntry("Sfoglia")
   public static final String PRIVATE_CONSTANT_20 = "browseButton";

   /**
    * CHOICEBOX LABELS
    * Labels used for the choices in a choicebox
    **/
   @RBEntry("Mantieni in stato di Check-Out")
   public static final String PRIVATE_CONSTANT_21 = "keepCheckedOut";

   @RBEntry("Invia")
   public static final String SUBMIT_LABEL = "22";

   /**
    * SYMBOLS
    * Symbols used to give additional information to a label.
    * "ellipses" is used to indicate more information will be
    * needed from the user (i.e. 'Save As...').
    **/
   @RBEntry("...")
   public static final String PRIVATE_CONSTANT_22 = "ellipses";

   @RBEntry("*")
   public static final String PRIVATE_CONSTANT_23 = "required";

   /**
    * DIALOG TITLES
    * Titles displayed on dialogs.  The parameter {0} is the identity
    * of the object being used in the dialog.  For example, if the
    * user is checking out a document titled "UI Design Spec", the
    * title will be "Check Out <UI Design Spec>".
    **/
   @RBEntry("Check-In di <{0}>")
   public static final String PRIVATE_CONSTANT_24 = "checkInObjTitle";

   @RBEntry("Check-In")
   public static final String PRIVATE_CONSTANT_25 = "checkInTitle";

   @RBEntry("Check-Out di <{0}>")
   public static final String PRIVATE_CONSTANT_26 = "checkOutTitle";

   @RBEntry("Sostituzione file")
   public static final String PRIVATE_CONSTANT_27 = "replaceFilesTitle";

   @RBEntry("Nuova revisione di <{0}>")
   public static final String PRIVATE_CONSTANT_28 = "reviseTitle";

   @RBEntry("Nuova versione variante <{0} {1}>")
   @RBArgComment0("Type of the object.")
   @RBArgComment1("Identity information about the object.")
   public static final String PRIVATE_CONSTANT_29 = "oneOffVersionTitle";

   @RBEntry("Nuova versione vista di <{0}>")
   public static final String PRIVATE_CONSTANT_30 = "newViewVersionTitle";

   @RBEntry("Cronologia iterazioni di <{0}>")
   public static final String PRIVATE_CONSTANT_31 = "iterationHistoryTitle";

   @RBEntry("Tutte le versioni di <{0}>")
   public static final String PRIVATE_CONSTANT_32 = "allVersionsTitle";

   /**
    * MULTILIST HEADINGS
    * Labels used as column headings in multilists
    **/
   @RBEntry("Data creazione")
   public static final String PRIVATE_CONSTANT_33 = "Create Date";

   @RBEntry("Autore")
   public static final String PRIVATE_CONSTANT_34 = "Created By";

   @RBEntry("Identificativo")
   public static final String PRIVATE_CONSTANT_35 = "Identity";

   @RBEntry("Iterazione")
   public static final String PRIVATE_CONSTANT_36 = "Iteration";

   @RBEntry("Data ultima modifica")
   public static final String PRIVATE_CONSTANT_37 = "Last Modified Date";

   @RBEntry("Team")
   public static final String PRIVATE_CONSTANT_38 = "TeamTemplate";

   @RBEntry("Stato del ciclo di vita")
   public static final String PRIVATE_CONSTANT_39 = "State";

   @RBEntry("Versione")
   public static final String PRIVATE_CONSTANT_40 = "Version";

   /**
    * ERROR MESSAGES
    **/
   @RBEntry("La cartella in cui collocare la copia dell'elemento originale non è stata specificata. Prima di eseguire un Check-Out è necessario specificare la cartella relativa alle operazioni di Check-Out.")
   public static final String CHECKOUT_FOLDER_REQUIRED = "0";

   @RBEntry("Errore durante l'inizializzazione delle risorse di localizzazione:")
   public static final String RESOURCE_BUNDLE_ERROR = "1";

   @RBEntry("\"{0}\" non è un valore valido per la proprietà \"{1}\".")
   public static final String PROPERTY_VETO_EXCEPTION = "2";

   @RBEntry("\"{0}\" non è stato sottoposto a Check-Out.")
   public static final String NOT_CHECKOUT_OWNER = "3";

   @RBEntry("\"{0}\" è attualmente sottoposto a Check-Out da {1}")
   public static final String CHECKED_OUT_BY_OTHER = "4";

   @RBEntry("\"{0}\" risulta attualmente sottoposto a Check-Out.")
   public static final String ALREADY_CHECKED_OUT = "5";

   @RBEntry("La revisione di \"{0}\" non è consentita.")
   public static final String REVISE_NOT_ALLOWED = "6";

   @RBEntry("\"{0}\" non può essere assegnato ad una vista.")
   public static final String NOT_VIEW_REVISABLE = "7";

   @RBEntry("Per \"{0}\" non è disponibile una cronologia iterazioni.")
   public static final String NO_ITERATION_HISTORY = "8";

   @RBEntry("Per \"{0}\" non è disponibile una cronologia delle versioni.")
   public static final String NO_VERSION_HISTORY = "9";

   @RBEntry("Nessun commento disponibile.")
   public static final String NOTE_NOT_AVAILABLE = "10";

   @RBEntry("La versione non è accessibile per l'iterazione.")
   public static final String VERSION_NOT_AVAILABLE = "11";

   @RBEntry("Impossibile trovare la cartella contenente la copia in modifica di \"{0}\". \"{0}\" non risulta sottoposto a Check-Out.")
   public static final String NO_CHECKOUT_FOLDER = "12";

   @RBEntry("L'oggetto è una copia in modifica.")
   public static final String WORKING_COPY = "13";

   @RBEntry("Nessuna vista per l'oggetto.")
   public static final String NO_VIEWS = "14";

   @RBEntry("Prima di effettuare il Check-In di {0}, è necessario specificare la cartella a cui appartiene. Selezionare la cartella in cui collocare {0}.")
   public static final String NO_CHECKIN_FOLDER = "15";

   @RBEntry("Versioni di {0}")
   public static final String VERSIONS_OF = "16";

   @RBEntry("Impossibile inizializzare la classe {0}")
   public static final String UNABLE_TO_INITIALIZE_CLASS = "17";

   @RBEntry("Errore durante l'avvio del task di visualizzazione per {0}:  {1}")
   public static final String INITIATE_VIEW_FAILED = "18";

   @RBEntry("L'applicazione non supporta la visualizzazione degli oggetti {0}.")
   public static final String VIEW_NOT_AVAILABLE = "19";

   @RBEntry("Il campo posizione contiene l'errore seguente:  {0}")
   @RBArgComment0("{0} contains the error from the server.")
   public static final String LOCATION_NOT_VALID = "20";

   @RBEntry("Si è verificato il seguente problema durante l'impostazione del team: {0}")
   @RBArgComment0("{0} contains the error from the server.")
   public static final String TEAMTEMPLATE_NOT_SET = "21";

   @RBEntry("Baseline:")
   public static final String BASELINES_LABEL = "23";

   @RBEntry("Confronta")
   public static final String COMPARE_BUTTON = "24";

   @RBEntry("{0} è la copia in modifica di {1}. Eliminare {0}?")
   public static final String CONFIRM_DELETE_WORKING_COPY = "25";

   @RBEntry("{0} non è stato sottoposto a Check-In a causa di errori di caricamento o memorizzazione del contenuto principale.")
   @RBArgComment0(" The identification of the document or object that wasn't checked in.")
   public static final String CHECKIN_NOT_COMPLETED = "26";

   @RBEntry("Effettuare Check-In in:")
   @RBComment("Label used for indicating where (which folder) an object is being checked in to")
   public static final String CHECK_IN_TO_LABEL = "27";

   @RBEntry("{0}{1}")
   @RBComment("Used for appending ellipses to a label - e.g. \"Browse...\"")
   @RBArgComment0("the label")
   @RBArgComment1("the ellipses")
   public static final String LABEL_AND_ELLIPSE = "28";

   @RBEntry("Annullare il Check-Out di {0} e perdere tutte le modifiche?")
   @RBComment("Prompt for undoing the checkout of an object")
   @RBArgComment0("folder location and name of object for which the checkout is being undone.  e.g. /Parts/Assembly/Engine")
   public static final String VERIFY_UNDO_CHECKOUT = "29";

   @RBEntry("Cronologia iterazioni di {0}")
   @RBComment("Label for Iteration History Dialog")
   @RBArgComment0("display identity of the object whose iteration history is shown")
   public static final String ITERATION_HISTORY_OF = "30";

   @RBEntry("{0} ({1})")
   @RBComment("Message for displaying the user name and id of a user.")
   @RBArgComment0("User Id (authentication name) of user")
   @RBArgComment1("Full name of user  Example:  00011ab (John Smith)")
   public static final String DISPLAY_USER_NAME = "31";

   @RBEntry("{0} è stato sottoposto a Check-Out da {1}. Annullare il Check-Out e perdere tutte le modifiche apportate a  {0}?")
   @RBComment("Message for prompting the user when undoing someone else's checkout.")
   public static final String VERIFY_UNDO_CHECKOUT_NOT_OWNER = "32";

   @RBEntry("Non è stato possibile sottoporre {0} a Check-Out. {0} è già stato sottoposto a Check-Out da {1}.")
   public static final String NOT_CHECKOUT_OWNER_DISPLAY = "33";

   @RBEntry("Sottoposto a Check-Out da:")
   public static final String CHECKED_OUT_BY_LABEL = "34";

   @RBEntry("L'oggetto {0} che si sta cercando di sottoporre a Check-In è stato sottoposto a Check-Out da {1}. Procedere con il Check-In di {0}?")
   @RBComment("Message for prompting the user who is attempting to check in an object not currently checked out to that user")
   @RBArgComment0("the type of object being checked in")
   @RBArgComment1("the name of the person with the object currently checked out")
   public static final String VERIFY_CHECKIN_NOT_OWNER = "35";

   @RBEntry("Non ripetere la domanda")
   public static final String DO_NOT_PROMPT_AGAIN_LABEL = "36";

   @RBEntry("L'oggetto è una iterazione della sessione.")
   public static final String SESSION_ITERATION = "37";

   @RBEntry("L'oggetto non può essere revisionato (non è Versioned)")
   public static final String NOT_REVISEABLE = "38";

   @RBEntry("L'oggetto non può avere una nuova versione di vista (non è ViewManageable)")
   public static final String NOT_VIEW_VERSIONABLE = "39";

   @RBEntry("Revisionare le parti associate al documento CAD {0}?")
   public static final String REVISE_PARTS_WITH_DOC_PROMPT = "40";

   @RBEntry("Revisionare i documenti CAD associati per {0}?")
   public static final String REVISE_DOCS_WITH_PART_PROMPT = "41";

   @RBEntry("Impossibile revisionare {0} perché è stato sottoposto a Check-Out da un altro utente.")
   public static final String NO_REVISE_BECAUSE_CHECKED_OUT = "42";
}
