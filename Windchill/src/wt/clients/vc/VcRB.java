/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.vc;

import wt.util.resource.*;

@RBUUID("wt.clients.vc.VcRB")
public final class VcRB extends WTListResourceBundle {
   /**
    * DIALOG LABELS
    * Labels used to identify fields on the dialogs
    **/
   @RBEntry("Checking In:")
   public static final String PRIVATE_CONSTANT_0 = "checkingInLabel";

   @RBEntry("Checking Out:")
   public static final String PRIVATE_CONSTANT_1 = "checkingOutLabel";

   @RBEntry("Comments:")
   public static final String PRIVATE_CONSTANT_2 = "commentLabel";

   @RBEntry("Location:")
   public static final String PRIVATE_CONSTANT_3 = "locationLabel";

   @RBEntry("Name:")
   public static final String PRIVATE_CONSTANT_4 = "nameLabel";

   @RBEntry("Type:")
   public static final String PRIVATE_CONSTANT_5 = "typeLabel";

   @RBEntry("Iteration History of")
   public static final String PRIVATE_CONSTANT_6 = "iterationHistoryOf";

   @RBEntry("All Versions of")
   public static final String PRIVATE_CONSTANT_7 = "allVersionsOf";

   @RBEntry("View:")
   public static final String PRIVATE_CONSTANT_8 = "viewLabel";

   /**
    * BUTTON LABELS
    * Labels used on command buttons
    **/
   @RBEntry("OK")
   public static final String PRIVATE_CONSTANT_9 = "okButton";

   @RBEntry("Cancel")
   public static final String PRIVATE_CONSTANT_10 = "cancelButton";

   @RBEntry("Continue")
   public static final String PRIVATE_CONSTANT_11 = "continueButton";

   @RBEntry("Help")
   public static final String PRIVATE_CONSTANT_12 = "helpButton";

   @RBEntry("View")
   public static final String PRIVATE_CONSTANT_13 = "viewButton";

   @RBEntry("Windchill Explorer")
   public static final String PRIVATE_CONSTANT_14 = "explorerButton";

   @RBEntry("Close")
   public static final String PRIVATE_CONSTANT_15 = "closeButton";

   @RBEntry("Do Not Download")
   public static final String PRIVATE_CONSTANT_16 = "doNotDownloadButton";

   @RBEntry("Do Not Upload")
   public static final String PRIVATE_CONSTANT_17 = "doNotUploadButton";

   @RBEntry("Download")
   public static final String PRIVATE_CONSTANT_18 = "downloadButton";

   @RBEntry("Upload")
   public static final String PRIVATE_CONSTANT_19 = "uploadButton";

   @RBEntry("Browse")
   public static final String PRIVATE_CONSTANT_20 = "browseButton";

   /**
    * CHOICEBOX LABELS
    * Labels used for the choices in a choicebox
    **/
   @RBEntry("Keep Checked Out")
   public static final String PRIVATE_CONSTANT_21 = "keepCheckedOut";

   @RBEntry("Submit")
   public static final String SUBMIT_LABEL = "22";

   /**
    * SYMBOLS
    * Symbols used to give additional information to a label.
    * "ellipses" is used to indicate more information will be
    * needed from the user (i.e. 'Save As...').
    **/
   @RBEntry("...")
   public static final String PRIVATE_CONSTANT_22 = "ellipses";

   @RBEntry("*")
   public static final String PRIVATE_CONSTANT_23 = "required";

   /**
    * DIALOG TITLES
    * Titles displayed on dialogs.  The parameter {0} is the identity
    * of the object being used in the dialog.  For example, if the
    * user is checking out a document titled "UI Design Spec", the
    * title will be "Check Out <UI Design Spec>".
    **/
   @RBEntry("Check In <{0}>")
   public static final String PRIVATE_CONSTANT_24 = "checkInObjTitle";

   @RBEntry("Check In")
   public static final String PRIVATE_CONSTANT_25 = "checkInTitle";

   @RBEntry("Check Out <{0}>")
   public static final String PRIVATE_CONSTANT_26 = "checkOutTitle";

   @RBEntry("Replace Files")
   public static final String PRIVATE_CONSTANT_27 = "replaceFilesTitle";

   @RBEntry("Revise <{0}>")
   public static final String PRIVATE_CONSTANT_28 = "reviseTitle";

   @RBEntry("New One-off Version <{0} {1}>")
   @RBArgComment0("Type of the object.")
   @RBArgComment1("Identity information about the object.")
   public static final String PRIVATE_CONSTANT_29 = "oneOffVersionTitle";

   @RBEntry("New View Version <{0}>")
   public static final String PRIVATE_CONSTANT_30 = "newViewVersionTitle";

   @RBEntry("Iteration History <{0}>")
   public static final String PRIVATE_CONSTANT_31 = "iterationHistoryTitle";

   @RBEntry("All Versions of <{0}>")
   public static final String PRIVATE_CONSTANT_32 = "allVersionsTitle";

   /**
    * MULTILIST HEADINGS
    * Labels used as column headings in multilists
    **/
   @RBEntry("Created On")
   public static final String PRIVATE_CONSTANT_33 = "Create Date";

   @RBEntry("Created By")
   public static final String PRIVATE_CONSTANT_34 = "Created By";

   @RBEntry("Identity")
   public static final String PRIVATE_CONSTANT_35 = "Identity";

   @RBEntry("Iteration")
   public static final String PRIVATE_CONSTANT_36 = "Iteration";

   @RBEntry("Last Modified")
   public static final String PRIVATE_CONSTANT_37 = "Last Modified Date";

   @RBEntry("Team")
   public static final String PRIVATE_CONSTANT_38 = "TeamTemplate";

   @RBEntry("State")
   public static final String PRIVATE_CONSTANT_39 = "State";

   @RBEntry("Version")
   public static final String PRIVATE_CONSTANT_40 = "Version";

   /**
    * ERROR MESSAGES
    **/
   @RBEntry("The folder to place the copy of the original item in has not been specified.  A checkout folder must be be given before a checkout can occur.")
   public static final String CHECKOUT_FOLDER_REQUIRED = "0";

   @RBEntry("An error occurred while initializing localization resources:  ")
   public static final String RESOURCE_BUNDLE_ERROR = "1";

   @RBEntry("\"{0}\" is not a valid value for property \"{1}\".")
   public static final String PROPERTY_VETO_EXCEPTION = "2";

   @RBEntry("You do not have {0} checked out.")
   public static final String NOT_CHECKOUT_OWNER = "3";

   @RBEntry("\"{0}\" is currently checked out by {1}")
   public static final String CHECKED_OUT_BY_OTHER = "4";

   @RBEntry("You currently have \"{0}\" checked out.")
   public static final String ALREADY_CHECKED_OUT = "5";

   @RBEntry("You are not allowed to revise \"{0}\".")
   public static final String REVISE_NOT_ALLOWED = "6";

   @RBEntry("\"{0}\" cannot be assigned to a view.")
   public static final String NOT_VIEW_REVISABLE = "7";

   @RBEntry("\"{0}\" does not have an iteration history.")
   public static final String NO_ITERATION_HISTORY = "8";

   @RBEntry("\"{0}\" does not have a version history.")
   public static final String NO_VERSION_HISTORY = "9";

   @RBEntry("No comment available.")
   public static final String NOTE_NOT_AVAILABLE = "10";

   @RBEntry("The version is not accessible for this iteration.")
   public static final String VERSION_NOT_AVAILABLE = "11";

   @RBEntry("The folder in which the working copy of \"{0}\" is contained could not be found because \"{0}\" is not checked out.")
   public static final String NO_CHECKOUT_FOLDER = "12";

   @RBEntry("Object is a working copy.")
   public static final String WORKING_COPY = "13";

   @RBEntry("No eligible views for object.")
   public static final String NO_VIEWS = "14";

   @RBEntry("Before {0} can be checked in, its containing folder must be given.  Please choose the folder in which to place {0}.")
   public static final String NO_CHECKIN_FOLDER = "15";

   @RBEntry("Versions of {0}")
   public static final String VERSIONS_OF = "16";

   @RBEntry("Unable to initialize class {0}")
   public static final String UNABLE_TO_INITIALIZE_CLASS = "17";

   @RBEntry("The following error occurred while trying to initiate the view task for {0}:  {1}")
   public static final String INITIATE_VIEW_FAILED = "18";

   @RBEntry("The application does not support viewing of {0} objects.")
   public static final String VIEW_NOT_AVAILABLE = "19";

   @RBEntry("The location field contains the following error:  {0}")
   @RBArgComment0("{0} contains the error from the server.")
   public static final String LOCATION_NOT_VALID = "20";

   @RBEntry("The following problem occurred setting the team: {0}")
   @RBArgComment0("{0} contains the error from the server.")
   public static final String TEAMTEMPLATE_NOT_SET = "21";

   @RBEntry("Baselines:")
   public static final String BASELINES_LABEL = "23";

   @RBEntry("Compare")
   public static final String COMPARE_BUTTON = "24";

   @RBEntry("{0} is the working copy of {1}. Continue deleting {0}?")
   public static final String CONFIRM_DELETE_WORKING_COPY = "25";

   @RBEntry("{0} was not checked in because there were errors with the upload or storing of the primary content.")
   @RBArgComment0(" The identification of the document or object that wasn't checked in.")
   public static final String CHECKIN_NOT_COMPLETED = "26";

   @RBEntry("Check In To:")
   @RBComment("Label used for indicating where (which folder) an object is being checked in to")
   public static final String CHECK_IN_TO_LABEL = "27";

   @RBEntry("{0}{1}")
   @RBComment("Used for appending ellipses to a label - e.g. \"Browse...\"")
   @RBArgComment0("the label")
   @RBArgComment1("the ellipses")
   public static final String LABEL_AND_ELLIPSE = "28";

   @RBEntry("Are you sure you want to undo the check-out of {0} and lose all changes?")
   @RBComment("Prompt for undoing the checkout of an object")
   @RBArgComment0("folder location and name of object for which the checkout is being undone.  e.g. /Parts/Assembly/Engine")
   public static final String VERIFY_UNDO_CHECKOUT = "29";

   @RBEntry("Iteration History of {0}")
   @RBComment("Label for Iteration History Dialog")
   @RBArgComment0("display identity of the object whose iteration history is shown")
   public static final String ITERATION_HISTORY_OF = "30";

   @RBEntry("{0} ({1})")
   @RBComment("Message for displaying the user name and id of a user.")
   @RBArgComment0("User Id (authentication name) of user")
   @RBArgComment1("Full name of user  Example:  00011ab (John Smith)")
   public static final String DISPLAY_USER_NAME = "31";

   @RBEntry("{0} is currently checked out to {1}.  Are you sure you want to undo the check-out and lose all changes to {0}?")
   @RBComment("Message for prompting the user when undoing someone else's checkout.")
   public static final String VERIFY_UNDO_CHECKOUT_NOT_OWNER = "32";

   @RBEntry("You do not have {0} checked out. {0} is currently checked out by {1}.")
   public static final String NOT_CHECKOUT_OWNER_DISPLAY = "33";

   @RBEntry("Checked Out By:")
   public static final String CHECKED_OUT_BY_LABEL = "34";

   @RBEntry("The {0} you are trying to check in is currently checked out by {1}.  Are you sure you want to check in this {0}?")
   @RBComment("Message for prompting the user who is attempting to check in an object not currently checked out to that user")
   @RBArgComment0("the type of object being checked in")
   @RBArgComment1("the name of the person with the object currently checked out")
   public static final String VERIFY_CHECKIN_NOT_OWNER = "35";

   @RBEntry("Do not ask me again")
   public static final String DO_NOT_PROMPT_AGAIN_LABEL = "36";

   @RBEntry("Object is a session iteration")
   public static final String SESSION_ITERATION = "37";

   @RBEntry("Object can not be revised (it is not Versioned)")
   public static final String NOT_REVISEABLE = "38";

   @RBEntry("Object can not be new view versioned (it is not ViewManageable)")
   public static final String NOT_VIEW_VERSIONABLE = "39";

   @RBEntry("Do you want to Revise associated build parts for CAD document {0}?")
   public static final String REVISE_PARTS_WITH_DOC_PROMPT = "40";

   @RBEntry("Do you want to Revise associated build CAD documents for {0}?")
   public static final String REVISE_DOCS_WITH_PART_PROMPT = "41";

   @RBEntry("The {0} can not be revised because it is checked out.")
   public static final String NO_REVISE_BECAUSE_CHECKED_OUT = "42";
}
