/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.vc;

import wt.util.resource.*;

@RBUUID("wt.clients.vc.VcHelpRB")
public final class VcHelpRB extends WTListResourceBundle {
   /**
    * -----vc Defaults-----
    **/
   @RBEntry("wt/clients/vc/help_en/cont.html")
   public static final String PRIVATE_CONSTANT_0 = "Contents/vc";

   @RBEntry("vc_applet")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_1 = "Help/vc";

   @RBEntry("LCCheckIn")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/vc/checkin";

   @RBEntry("WFTemplateCheckin")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_3 = "Help/vc/checkinWF";

   /**
    * -----Revise Dialog-----
    **/
   @RBEntry("VCApplVersionCreate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_4 = "Help/vc/Revise";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_5 = "Desc/vc/Revise";

   @RBEntry("PIMProdRevise")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_6 = "Help/vc/ReviseProduct";

   @RBEntry("PIMPIRevise")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_7 = "Help/vc/ReviseInstance";

   @RBEntry("PIMSNPRevise")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_8 = "Help/vc/ReviseSerialNumberedPart";

   /**
    * -----Revise View (or New View Version) Dialog-----
    **/
   @RBEntry("VCApplViewVersionCreate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_9 = "Help/vc/NewViewVersion";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_10 = "Desc/vc/NewViewVersion";

   @RBEntry("PIMProdNewView")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_11 = "Help/vc/NewViewVersionProduct";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_12 = "Desc/vc/NewViewVersionProduct";

   @RBEntry("PIMPINewView")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_13 = "Help/vc/NewViewVersionInstance";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_14 = "Desc/vc/NewViewVersionInstance";

   @RBEntry("PIMSNPNewView")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_15 = "Help/vc/NewViewVersionSerialNumberedPart";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_16 = "Desc/vc/NewViewVersionSerialNumberedPart";

   /**
    * -----Iteration History Dialog-----
    **/
   @RBEntry("WFIterationHistoryView")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_17 = "Help/vc/IterationHistory";

   @RBEntry("LCIterationHistoryView")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_18 = "Help/vc/IterationHistoryLC";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_19 = "Desc/vc/IterationHistory";

   /**
    * -----Version History Dialog-----
    **/
   @RBEntry("VCApplAllVersionsView")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_20 = "Help/vc/VersionHistory";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_21 = "Desc/vc/VersionHistory";

   /**
    * -----One OffDialog-----
    **/
   @RBEntry("PIMSelectOneOff")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_22 = "Help/vc/OneOff";
}
