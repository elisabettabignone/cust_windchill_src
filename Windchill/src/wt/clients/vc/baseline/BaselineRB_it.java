/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.vc.baseline;

import wt.util.resource.*;

@RBUUID("wt.clients.vc.baseline.BaselineRB")
public final class BaselineRB_it extends WTListResourceBundle {
   /**
    * Labels ------------------------------------------------------------------
    * 
    **/
   @RBEntry("Data creazione:")
   public static final String PRIVATE_CONSTANT_0 = "createdLbl";

   @RBEntry("Autore:")
   public static final String PRIVATE_CONSTANT_1 = "createdByLbl";

   @RBEntry("Data creazione:")
   public static final String PRIVATE_CONSTANT_2 = "createdOnLbl";

   @RBEntry("Origine:")
   public static final String PRIVATE_CONSTANT_3 = "sourceLbl";

   @RBEntry("Posizione:")
   public static final String PRIVATE_CONSTANT_4 = "locationLbl";

   @RBEntry("Nome:")
   public static final String PRIVATE_CONSTANT_5 = "nameLbl";

   @RBEntry("Numero:")
   public static final String PRIVATE_CONSTANT_6 = "numberLbl";

   @RBEntry("Stato:")
   public static final String PRIVATE_CONSTANT_7 = "statusLbl";

   @RBEntry("Tipo:")
   public static final String PRIVATE_CONSTANT_8 = "typeLbl";

   @RBEntry("Nome cartella:")
   public static final String PRIVATE_CONSTANT_9 = "folderNameLbl";

   @RBEntry("Data ultima modifica:")
   public static final String PRIVATE_CONSTANT_10 = "updatedOnLbl";

   @RBEntry("Autore modifiche:")
   public static final String PRIVATE_CONSTANT_11 = "updatedByLbl";

   @RBEntry("Progetto:")
   public static final String PRIVATE_CONSTANT_12 = "projectLbl";

   @RBEntry("Ciclo di vita:")
   public static final String PRIVATE_CONSTANT_13 = "lifecycleLbl";

   @RBEntry("Stato del ciclo di vita:")
   public static final String PRIVATE_CONSTANT_14 = "stateLbl";

   @RBEntry("Vista:")
   public static final String PRIVATE_CONSTANT_15 = "viewLbl";

   /**
    * Button Labels -----------------------------------------------------------
    * 
    **/
   @RBEntry("Annulla operazione")
   public static final String PRIVATE_CONSTANT_16 = "FeedbackCancelButton";

   @RBEntry("Chiudi")
   public static final String PRIVATE_CONSTANT_17 = "CloseButton";

   @RBEntry("Sfoglia...")
   public static final String PRIVATE_CONSTANT_18 = "browseButton";

   @RBEntry("Annulla")
   public static final String PRIVATE_CONSTANT_19 = "cancelButton";

   @RBEntry("Reimposta")
   public static final String PRIVATE_CONSTANT_20 = "clearButton";

   @RBEntry("Chiudi")
   public static final String PRIVATE_CONSTANT_21 = "closeButton";

   @RBEntry("Continua")
   public static final String PRIVATE_CONSTANT_22 = "continueButton";

   @RBEntry("Elimina")
   public static final String PRIVATE_CONSTANT_23 = "deleteButton";

   @RBEntry("Guida")
   public static final String PRIVATE_CONSTANT_24 = "helpButton";

   @RBEntry("OK")
   public static final String PRIVATE_CONSTANT_25 = "okButton";

   @RBEntry("Aggiorna")
   public static final String PRIVATE_CONSTANT_26 = "updateButton";

   @RBEntry("Salva")
   public static final String PRIVATE_CONSTANT_27 = "saveButton";

   @RBEntry("Rimuovi")
   public static final String PRIVATE_CONSTANT_28 = "removeButton";

   /**
    * Symbols ----------------------------------------------------------------
    **/
   @RBEntry("...")
   @RBPseudo(false)
   public static final String PRIVATE_CONSTANT_29 = "ellipses";

   @RBEntry("*")
   @RBPseudo(false)
   public static final String PRIVATE_CONSTANT_30 = "required";

   /**
    * Titles -----------------------------------------------------------------
    **/
   @RBEntry("Trova parte")
   public static final String PRIVATE_CONSTANT_31 = "findPartTitle";

   @RBEntry("Crea baseline")
   public static final String PRIVATE_CONSTANT_32 = "createTitle";

   @RBEntry("Elenco baseline")
   public static final String PRIVATE_CONSTANT_33 = "baselineListTitle";

   @RBEntry("Aggiorna baseline <{0}>")
   public static final String PRIVATE_CONSTANT_34 = "updateTitle";

   @RBEntry("Visualizza baseline <{0}>")
   public static final String PRIVATE_CONSTANT_35 = "viewTitle";

   @RBEntry("Feedback baseline")
   public static final String PRIVATE_CONSTANT_36 = "FeedbackTitle";

   /**
    * Messages ---------------------------------------------------------------
    **/
   @RBEntry("ANNULLATO")
   public static final String PRIVATE_CONSTANT_37 = "FeedbackCancelMessage";

   @RBEntry("COMPLETO")
   public static final String PRIVATE_CONSTANT_38 = "FeedbackCompleteMessage";

   @RBEntry("Fornire un valore per \"{0}\".")
   public static final String NULL_ATTRIBUTE_VALUE = "0";

   @RBEntry("\"{0}\" non è un percorso valido per la cartella.")
   public static final String INVALID_FOLDER_PATH = "1";

   @RBEntry("\"{0}\" non è un valore valido per la proprietà \"{1}\".")
   public static final String PROPERTY_VETO_EXCEPTION = "2";

   @RBEntry("Per aggiornare {0} è necessario disporre di autorizzazione.")
   public static final String UPDATE_NOT_AUTHORIZED = "3";

   @RBEntry("Visualizzare {0}?")
   public static final String PROMPT_VIEW_OBJECT = "4";

   @RBEntry("Errore durante l'avvio del task di aggiornamento di {0}. L'oggetto che si sta cercando di aggiornare non è più presente nel database.")
   public static final String OBJECT_DOES_NOT_EXIST = "5";

   @RBEntry("L'oggetto \"{0}\" è già nella baseline \"{1}\".")
   public static final String ITEM_IN_BASELINE = "6";

   @RBEntry("Un'iterazione dell'oggetto \"{0}\" è già nella baseline \"{1}\". Sostituire?")
   public static final String ITEM_ITERATION_IN_BASELINE = "7";

   @RBEntry("Trova baseline")
   public static final String FIND_BASELINE = "8";

   @RBEntry("Salvataggio non riuscito")
   public static final String SAVE_FAILED = "9";

   @RBEntry("Descrizione")
   public static final String PRIVATE_CONSTANT_39 = "descriptionLbl";
}
