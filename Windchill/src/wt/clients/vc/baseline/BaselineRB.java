/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.vc.baseline;

import wt.util.resource.*;

@RBUUID("wt.clients.vc.baseline.BaselineRB")
public final class BaselineRB extends WTListResourceBundle {
   /**
    * Labels ------------------------------------------------------------------
    * 
    **/
   @RBEntry("Created:")
   public static final String PRIVATE_CONSTANT_0 = "createdLbl";

   @RBEntry("Created By:")
   public static final String PRIVATE_CONSTANT_1 = "createdByLbl";

   @RBEntry("Created On:")
   public static final String PRIVATE_CONSTANT_2 = "createdOnLbl";

   @RBEntry("Source:")
   public static final String PRIVATE_CONSTANT_3 = "sourceLbl";

   @RBEntry("Location:")
   public static final String PRIVATE_CONSTANT_4 = "locationLbl";

   @RBEntry("Name:")
   public static final String PRIVATE_CONSTANT_5 = "nameLbl";

   @RBEntry("Number:")
   public static final String PRIVATE_CONSTANT_6 = "numberLbl";

   @RBEntry("Status:")
   public static final String PRIVATE_CONSTANT_7 = "statusLbl";

   @RBEntry("Type:")
   public static final String PRIVATE_CONSTANT_8 = "typeLbl";

   @RBEntry("Folder Name:")
   public static final String PRIVATE_CONSTANT_9 = "folderNameLbl";

   @RBEntry("Last Modified:")
   public static final String PRIVATE_CONSTANT_10 = "updatedOnLbl";

   @RBEntry("Modified By:")
   public static final String PRIVATE_CONSTANT_11 = "updatedByLbl";

   @RBEntry("Project:")
   public static final String PRIVATE_CONSTANT_12 = "projectLbl";

   @RBEntry("Lifecycle:")
   public static final String PRIVATE_CONSTANT_13 = "lifecycleLbl";

   @RBEntry("State:")
   public static final String PRIVATE_CONSTANT_14 = "stateLbl";

   @RBEntry("View:")
   public static final String PRIVATE_CONSTANT_15 = "viewLbl";

   /**
    * Button Labels -----------------------------------------------------------
    * 
    **/
   @RBEntry("Cancel Baseline Operation")
   public static final String PRIVATE_CONSTANT_16 = "FeedbackCancelButton";

   @RBEntry("Close")
   public static final String PRIVATE_CONSTANT_17 = "CloseButton";

   @RBEntry("Browse...")
   public static final String PRIVATE_CONSTANT_18 = "browseButton";

   @RBEntry("Cancel")
   public static final String PRIVATE_CONSTANT_19 = "cancelButton";

   @RBEntry("Clear")
   public static final String PRIVATE_CONSTANT_20 = "clearButton";

   @RBEntry("Close")
   public static final String PRIVATE_CONSTANT_21 = "closeButton";

   @RBEntry("Continue")
   public static final String PRIVATE_CONSTANT_22 = "continueButton";

   @RBEntry("Delete")
   public static final String PRIVATE_CONSTANT_23 = "deleteButton";

   @RBEntry("Help")
   public static final String PRIVATE_CONSTANT_24 = "helpButton";

   @RBEntry("OK")
   public static final String PRIVATE_CONSTANT_25 = "okButton";

   @RBEntry("Update")
   public static final String PRIVATE_CONSTANT_26 = "updateButton";

   @RBEntry("Save")
   public static final String PRIVATE_CONSTANT_27 = "saveButton";

   @RBEntry("Remove")
   public static final String PRIVATE_CONSTANT_28 = "removeButton";

   /**
    * Symbols ----------------------------------------------------------------
    **/
   @RBEntry("...")
   @RBPseudo(false)
   public static final String PRIVATE_CONSTANT_29 = "ellipses";

   @RBEntry("*")
   @RBPseudo(false)
   public static final String PRIVATE_CONSTANT_30 = "required";

   /**
    * Titles -----------------------------------------------------------------
    **/
   @RBEntry("Find Part")
   public static final String PRIVATE_CONSTANT_31 = "findPartTitle";

   @RBEntry("Create Baseline")
   public static final String PRIVATE_CONSTANT_32 = "createTitle";

   @RBEntry("Baseline List")
   public static final String PRIVATE_CONSTANT_33 = "baselineListTitle";

   @RBEntry("Update Baseline <{0}>")
   public static final String PRIVATE_CONSTANT_34 = "updateTitle";

   @RBEntry("View Baseline <{0}>")
   public static final String PRIVATE_CONSTANT_35 = "viewTitle";

   @RBEntry("Baseline FeedBack")
   public static final String PRIVATE_CONSTANT_36 = "FeedbackTitle";

   /**
    * Messages ---------------------------------------------------------------
    **/
   @RBEntry("CANCELLED")
   public static final String PRIVATE_CONSTANT_37 = "FeedbackCancelMessage";

   @RBEntry("COMPLETE")
   public static final String PRIVATE_CONSTANT_38 = "FeedbackCompleteMessage";

   @RBEntry("Please provide a value for \"{0}\".")
   public static final String NULL_ATTRIBUTE_VALUE = "0";

   @RBEntry("\"{0}\" is not a valid folder path.")
   public static final String INVALID_FOLDER_PATH = "1";

   @RBEntry("\"{0}\" is not a valid value for property \"{1}\".")
   public static final String PROPERTY_VETO_EXCEPTION = "2";

   @RBEntry("You are not authorized to update {0}.")
   public static final String UPDATE_NOT_AUTHORIZED = "3";

   @RBEntry("Would you like to view {0}?")
   public static final String PROMPT_VIEW_OBJECT = "4";

   @RBEntry("An error occurred while attempting to launch the update task for {0}.  The object you are attempting to update no longer exists in the database.")
   public static final String OBJECT_DOES_NOT_EXIST = "5";

   @RBEntry("The object \"{0}\" is already in the baseline \"{1}\".")
   public static final String ITEM_IN_BASELINE = "6";

   @RBEntry("An iteration of the object \"{0}\" is already in the baseline \"{1}\".  Replace the object?")
   public static final String ITEM_ITERATION_IN_BASELINE = "7";

   @RBEntry("Find Baseline")
   public static final String FIND_BASELINE = "8";

   @RBEntry("Save Failed")
   public static final String SAVE_FAILED = "9";

   @RBEntry("Description")
   public static final String PRIVATE_CONSTANT_39 = "descriptionLbl";
}
