/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.vc.baseline;

import wt.util.resource.*;

@RBUUID("wt.clients.vc.baseline.BaselineHelpRB")
public final class BaselineHelpRB_it extends WTListResourceBundle {
   @RBEntry("BaseCreate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_0 = "Help/baseline/CreateBaselineTask";

   @RBEntry("BasePopulate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_1 = "Help/baseline/PopulateBaselineTask";

   @RBEntry("BaseRemoveItems")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/baseline/RemoveBaselineTask";

   @RBEntry("BaseUpdate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_3 = "Help/baseline/UpdateBaselineTask";

   /**
    * ----------------------------------------------------------------------
    **/
   @RBEntry("Descrizione della baseline")
   public static final String PRIVATE_CONSTANT_4 = "Desc/baseline/BaselineTask/Description";

   @RBEntry("Posizione della baseline")
   public static final String PRIVATE_CONSTANT_5 = "Desc/baseline/BaselineTask/Location";

   @RBEntry("Nome della baseline")
   public static final String PRIVATE_CONSTANT_6 = "Desc/baseline/BaselineTask/Name";

   /**
    * ----------------------------------------------------------------------
    **/
   @RBEntry("Utente che ha creato la baseline")
   public static final String PRIVATE_CONSTANT_7 = "Desc/baseline/BaselineTask/CreatedByPersonName";

   @RBEntry("Data di creazione della baseline")
   public static final String PRIVATE_CONSTANT_8 = "Desc/baseline/BaselineTask/CreationDate";

   @RBEntry("Data di modifica della baseline")
   public static final String PRIVATE_CONSTANT_9 = "Desc/baseline/BaselineTask/LastUpdated";

   @RBEntry("Utente che ha apportato l'ultima modifica alla baseline")
   public static final String PRIVATE_CONSTANT_10 = "Desc/baseline/BaselineTask/ModifiedByPersonName";

   /**
    * ----------------------------------------------------------------------
    **/
   @RBEntry("Consente di eseguire la ricerca di una cartella")
   public static final String PRIVATE_CONSTANT_11 = "Desc/baseline/BaselineTask/Browse";

   /**
    * ----------------------------------------------------------------------
    **/
   @RBEntry("Chiude la finestra senza salvare le modifiche apportate alla baseline")
   public static final String PRIVATE_CONSTANT_12 = "Desc/baseline/BaselineTask/Cancel";

   @RBEntry("Visualizza le informazioni della guida relative al task")
   public static final String PRIVATE_CONSTANT_13 = "Desc/baseline/BaselineTask/Help";

   @RBEntry("Salva le modifiche apportate alla baseline e chiude la finestra")
   public static final String PRIVATE_CONSTANT_14 = "Desc/baseline/BaselineTask/OK";

   @RBEntry("Salva le modifiche apportate alla baseline")
   public static final String PRIVATE_CONSTANT_15 = "Desc/baseline/BaselineTask/Save";

   /**
    * ----------------------------------------------------------------------
    **/
   @RBEntry("Elenco delle baseline per l'oggetto selezionato")
   public static final String PRIVATE_CONSTANT_16 = "Desc/baseline/BaselineTask/BaselineList";
}
