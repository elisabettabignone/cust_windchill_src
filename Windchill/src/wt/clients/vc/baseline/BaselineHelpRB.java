/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.vc.baseline;

import wt.util.resource.*;

@RBUUID("wt.clients.vc.baseline.BaselineHelpRB")
public final class BaselineHelpRB extends WTListResourceBundle {
   @RBEntry("BaseCreate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_0 = "Help/baseline/CreateBaselineTask";

   @RBEntry("BasePopulate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_1 = "Help/baseline/PopulateBaselineTask";

   @RBEntry("BaseRemoveItems")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/baseline/RemoveBaselineTask";

   @RBEntry("BaseUpdate")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_3 = "Help/baseline/UpdateBaselineTask";

   /**
    * ----------------------------------------------------------------------
    **/
   @RBEntry("The description of the baseline")
   public static final String PRIVATE_CONSTANT_4 = "Desc/baseline/BaselineTask/Description";

   @RBEntry("The location of the baseline")
   public static final String PRIVATE_CONSTANT_5 = "Desc/baseline/BaselineTask/Location";

   @RBEntry("The name of the baseline")
   public static final String PRIVATE_CONSTANT_6 = "Desc/baseline/BaselineTask/Name";

   /**
    * ----------------------------------------------------------------------
    **/
   @RBEntry("The person who created the baseline")
   public static final String PRIVATE_CONSTANT_7 = "Desc/baseline/BaselineTask/CreatedByPersonName";

   @RBEntry("The creation date of the baseline")
   public static final String PRIVATE_CONSTANT_8 = "Desc/baseline/BaselineTask/CreationDate";

   @RBEntry("The modification date of the baseline")
   public static final String PRIVATE_CONSTANT_9 = "Desc/baseline/BaselineTask/LastUpdated";

   @RBEntry("The person who last modified the baseline")
   public static final String PRIVATE_CONSTANT_10 = "Desc/baseline/BaselineTask/ModifiedByPersonName";

   /**
    * ----------------------------------------------------------------------
    **/
   @RBEntry("Browse for a folder location")
   public static final String PRIVATE_CONSTANT_11 = "Desc/baseline/BaselineTask/Browse";

   /**
    * ----------------------------------------------------------------------
    **/
   @RBEntry("Do not save the changes to this baseline, just close the window")
   public static final String PRIVATE_CONSTANT_12 = "Desc/baseline/BaselineTask/Cancel";

   @RBEntry("Display help for this task")
   public static final String PRIVATE_CONSTANT_13 = "Desc/baseline/BaselineTask/Help";

   @RBEntry("Save the changes to this baseline and close the window")
   public static final String PRIVATE_CONSTANT_14 = "Desc/baseline/BaselineTask/OK";

   @RBEntry("Save the changes to this baseline")
   public static final String PRIVATE_CONSTANT_15 = "Desc/baseline/BaselineTask/Save";

   /**
    * ----------------------------------------------------------------------
    **/
   @RBEntry("The list of baselines for the selected object")
   public static final String PRIVATE_CONSTANT_16 = "Desc/baseline/BaselineTask/BaselineList";
}
