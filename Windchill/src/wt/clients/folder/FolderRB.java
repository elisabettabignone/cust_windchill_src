/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.folder;

import wt.util.resource.*;

@RBUUID("wt.clients.folder.FolderRB")
public final class FolderRB extends WTListResourceBundle {
   /**
    * DIALOG LABELS
    * Labels used to identify the fields on the dialogs
    **/
   @RBEntry("Cabinet Name: ")
   public static final String PRIVATE_CONSTANT_0 = "cabinetNameLbl";

   @RBEntry("Created By:")
   public static final String PRIVATE_CONSTANT_1 = "createdByLbl";

   @RBEntry("Created On:")
   public static final String PRIVATE_CONSTANT_2 = "createdOnLbl";

   @RBEntry("Domain:")
   public static final String PRIVATE_CONSTANT_3 = "domainPathLbl";

   @RBEntry("Folder Name:")
   public static final String PRIVATE_CONSTANT_4 = "folderNameLbl";

   @RBEntry("Location:")
   public static final String PRIVATE_CONSTANT_5 = "folderPathLbl";

   @RBEntry("Inherit domain from cabinet")
   public static final String PRIVATE_CONSTANT_6 = "inheritDomainCabinetLbl";

   @RBEntry("Inherit domain from parent folder")
   public static final String PRIVATE_CONSTANT_7 = "inheritDomainFolderLbl";

   @RBEntry("Create Shortcut To:")
   public static final String PRIVATE_CONSTANT_8 = "shortcutTargetLbl";

   @RBEntry("Create Shortcut In:")
   public static final String PRIVATE_CONSTANT_9 = "shortcutLocationLbl";

   /**
    * BUTTON LABELS
    * Labels used on command buttons
    **/
   @RBEntry("Browse")
   public static final String PRIVATE_CONSTANT_10 = "browseButton";

   @RBEntry("Cancel")
   public static final String PRIVATE_CONSTANT_11 = "cancelButton";

   @RBEntry("OK")
   public static final String PRIVATE_CONSTANT_12 = "okButton";

   /**
    * SYMBOLS
    * Symbols used in addition to dialog labels and command buttons.
    * The "ellipses" is used on command buttons to indicate that more
    * input will be needed (i.e. 'Save As...').  The "required" is
    * used as a symbol to mark those dialog fields for which user
    * input is required.
    **/
   @RBEntry("...")
   public static final String PRIVATE_CONSTANT_13 = "ellipses";

   @RBEntry("*")
   public static final String PRIVATE_CONSTANT_14 = "required";

   /**
    * DIALOG TITLES
    * Titles displayed on the dialogs.  The parameter {0} is the identity of
    * of the object.  For example, if the user is viewing a folder named
    * "Design", the title will be "View Folder <Design>".
    **/
   @RBEntry("Create Cabinet")
   public static final String PRIVATE_CONSTANT_15 = "createCabinetTitle";

   @RBEntry("Create Folder")
   public static final String PRIVATE_CONSTANT_16 = "createFolderTitle";

   @RBEntry("Create Shortcut")
   public static final String PRIVATE_CONSTANT_17 = "createShortcutTitle";

   @RBEntry("Select Domain")
   public static final String PRIVATE_CONSTANT_18 = "selectDomainTitle";

   @RBEntry("Update Cabinet")
   public static final String PRIVATE_CONSTANT_19 = "updateCabinetTitle";

   @RBEntry("Update Folder")
   public static final String PRIVATE_CONSTANT_20 = "updateFolderTitle";

   @RBEntry("View Folder")
   public static final String PRIVATE_CONSTANT_21 = "viewFolderTitle";

   @RBEntry("View Folder <{0}>")
   public static final String PRIVATE_CONSTANT_22 = "viewFolderObjTitle";

   /**
    * ERROR MESSAGES
    * 
    **/
   @RBEntry("\"{0}\" is not a valid folder path.")
   @RBComment("Message displayed when the user enters a path to a non-existent folder.")
   @RBArgComment0("the folder path")
   public static final String INVALID_FOLDER_PATH = "0";

   @RBEntry("The following error occurred while attempting to create a folder: {0}")
   @RBComment("Message displayed when an exception is thrown while attempting to create a folder.")
   @RBArgComment0("the message of the exception thrown")
   public static final String CREATE_FOLDER_FAILED = "2";

   @RBEntry("The given object is not a folder")
   @RBComment("Message displayed when the object passed in to the dialog is not a folder but a folder object is required.")
   public static final String OBJECT_NOT_FOLDER = "4";

   @RBEntry("The given object is not a shortcut")
   @RBComment("Message displayed when the object passed in to the dialog is not a shortcut but a shortcut object is required.")
   public static final String OBJECT_NOT_SHORTCUT = "5";

   @RBEntry("A folder must be specified before that folder can be viewed")
   @RBComment("Message displayed when attempting to view a folder but no folder has been specified to view.")
   public static final String NO_VIEW_FOLDER_GIVEN = "6";

   @RBEntry("A folder must be specified before that folder can be updated")
   @RBComment("Message displayed when attempting to update a folder but no folder has been specified to update.")
   public static final String NO_UPDATE_FOLDER_GIVEN = "7";

   @RBEntry("Please specify the folder in which to create the new folder.")
   @RBComment("Message displayed when the user attempts to create a new folder but has not given the folder in which to create the new folder.")
   public static final String NO_CREATE_FOLDER_GIVEN = "8";

   @RBEntry("A folder must be specified before that folder can be deleted")
   @RBComment("Message displayed when the user attempts to delete a folder but has not given the folder to be deleted.")
   public static final String NO_DELETE_FOLDER_GIVEN = "9";

   @RBEntry("A shortcut must be specified before that shortcut can be viewed")
   @RBComment("Message displayed when attempting to view a shortcut but no shortcut has been specified to view.")
   public static final String NO_VIEW_SHORTCUT_GIVEN = "10";

   @RBEntry("A shortcut must be specified before that shortcut can be updated")
   @RBComment("Message displayed when attempting to update a shortcut but no shortcut has been specified to update.")
   public static final String NO_UPDATE_SHORTCUT_GIVEN = "11";

   @RBEntry("A shortcut must be specified before that shortcut can be deleted")
   @RBComment("Message displayed when attempting to delete a shortcut but no shortcut has been specified to update.")
   public static final String NO_DELETE_SHORTCUT_GIVEN = "12";

   @RBEntry("No Frame has been provided to launch the dialog.  In order to display the dialog, a Frame must be given")
   @RBComment("Message displayed when the frame required to display a dialog has not been given.")
   public static final String NO_PARENT_FRAME = "13";

   @RBEntry("An error occurred while localizing {0}.  The key, \"{1}\", was not found in the resource bundle, \"{2}\".  Please contact your support staff.")
   @RBComment("Message displayed when an the key used to look up localized text in a resource bundle does not exist.")
   @RBArgComment0("the name of the dialog being localized")
   @RBArgComment1("the key not found in the resource bundle")
   @RBArgComment2("the name of the resource bundle being used")
   public static final String RESOURCE_BUNDLE_ERROR = "14";

   @RBEntry("Deleting {0} will delete all of its contents as well. Continue deleting {0}?")
   public static final String CONFIRM_DELETE_FOLDER = "15";

   @RBEntry("Are you sure you want to delete the shortcut to {0} \"{1}\"?")
   public static final String CONFIRM_DELETE_SHORTCUT = "16";

   @RBEntry("Browse for Shortcut Target")
   public static final String BROWSE_TARGET_TITLE = "17";

   @RBEntry("Shortcut Target")
   public static final String BROWSE_TARGET_LABEL = "18";

   @RBEntry(":")
   public static final String LABEL_SUFFIX = "19";

   @RBEntry("{0}{1}")
   public static final String APPEND_SUFFIX = "20";

   @RBEntry("Cabinet {0} is a shared cabinet and cannot be deleted.")
   public static final String CANNOT_DELETE_SHARED_CABINET = "21";

   @RBEntry("The following error occurred while attempting to create a cabinet: {0}")
   public static final String CREATE_CABINET_FAILED = "22";

   @RBEntry("The following error occurred while attempting to update a cabinet: {0}")
   public static final String UPDATE_CABINET_FAILED = "23";

   @RBEntry("The following error occurred while attempting to update a folder: {0}")
   public static final String UPDATE_FOLDER_FAILED = "24";

   @RBEntry("An error occurred while initializing the dialog: {0}")
   public static final String INITIALIZATION_FAILED = "25";
}
