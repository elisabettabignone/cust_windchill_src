/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.folder;

import wt.util.resource.*;

@RBUUID("wt.clients.folder.FolderRB")
public final class FolderRB_it extends WTListResourceBundle {
   /**
    * DIALOG LABELS
    * Labels used to identify the fields on the dialogs
    **/
   @RBEntry("Nome dello schedario:")
   public static final String PRIVATE_CONSTANT_0 = "cabinetNameLbl";

   @RBEntry("Autore:")
   public static final String PRIVATE_CONSTANT_1 = "createdByLbl";

   @RBEntry("Data creazione:")
   public static final String PRIVATE_CONSTANT_2 = "createdOnLbl";

   @RBEntry("Dominio:")
   public static final String PRIVATE_CONSTANT_3 = "domainPathLbl";

   @RBEntry("Nome cartella:")
   public static final String PRIVATE_CONSTANT_4 = "folderNameLbl";

   @RBEntry("Posizione:")
   public static final String PRIVATE_CONSTANT_5 = "folderPathLbl";

   @RBEntry("Eredita il dominio dall'archivio")
   public static final String PRIVATE_CONSTANT_6 = "inheritDomainCabinetLbl";

   @RBEntry("Eredita il dominio dalla cartella padre")
   public static final String PRIVATE_CONSTANT_7 = "inheritDomainFolderLbl";

   @RBEntry("Crea collegamento a:")
   public static final String PRIVATE_CONSTANT_8 = "shortcutTargetLbl";

   @RBEntry("Crea collegamento in:")
   public static final String PRIVATE_CONSTANT_9 = "shortcutLocationLbl";

   /**
    * BUTTON LABELS
    * Labels used on command buttons
    **/
   @RBEntry("Sfoglia")
   public static final String PRIVATE_CONSTANT_10 = "browseButton";

   @RBEntry("Annulla")
   public static final String PRIVATE_CONSTANT_11 = "cancelButton";

   @RBEntry("OK")
   public static final String PRIVATE_CONSTANT_12 = "okButton";

   /**
    * SYMBOLS
    * Symbols used in addition to dialog labels and command buttons.
    * The "ellipses" is used on command buttons to indicate that more
    * input will be needed (i.e. 'Save As...').  The "required" is
    * used as a symbol to mark those dialog fields for which user
    * input is required.
    **/
   @RBEntry("...")
   public static final String PRIVATE_CONSTANT_13 = "ellipses";

   @RBEntry("*")
   public static final String PRIVATE_CONSTANT_14 = "required";

   /**
    * DIALOG TITLES
    * Titles displayed on the dialogs.  The parameter {0} is the identity of
    * of the object.  For example, if the user is viewing a folder named
    * "Design", the title will be "View Folder <Design>".
    **/
   @RBEntry("Crea schedario")
   public static final String PRIVATE_CONSTANT_15 = "createCabinetTitle";

   @RBEntry("Crea cartella")
   public static final String PRIVATE_CONSTANT_16 = "createFolderTitle";

   @RBEntry("Crea collegamento")
   public static final String PRIVATE_CONSTANT_17 = "createShortcutTitle";

   @RBEntry("Seleziona dominio")
   public static final String PRIVATE_CONSTANT_18 = "selectDomainTitle";

   @RBEntry("Aggiorna schedario")
   public static final String PRIVATE_CONSTANT_19 = "updateCabinetTitle";

   @RBEntry("Aggiorna cartella")
   public static final String PRIVATE_CONSTANT_20 = "updateFolderTitle";

   @RBEntry("Visualizza cartella")
   public static final String PRIVATE_CONSTANT_21 = "viewFolderTitle";

   @RBEntry("Visualizza cartella <{0}>")
   public static final String PRIVATE_CONSTANT_22 = "viewFolderObjTitle";

   /**
    * ERROR MESSAGES
    * 
    **/
   @RBEntry("\"{0}\" non è un percorso valido per la cartella.")
   @RBComment("Message displayed when the user enters a path to a non-existent folder.")
   @RBArgComment0("the folder path")
   public static final String INVALID_FOLDER_PATH = "0";

   @RBEntry("Errore durante la creazione di una cartella: {0}")
   @RBComment("Message displayed when an exception is thrown while attempting to create a folder.")
   @RBArgComment0("the message of the exception thrown")
   public static final String CREATE_FOLDER_FAILED = "2";

   @RBEntry("L'oggetto specificato non è una cartella")
   @RBComment("Message displayed when the object passed in to the dialog is not a folder but a folder object is required.")
   public static final String OBJECT_NOT_FOLDER = "4";

   @RBEntry("L'oggetto specificato non è un collegamento")
   @RBComment("Message displayed when the object passed in to the dialog is not a shortcut but a shortcut object is required.")
   public static final String OBJECT_NOT_SHORTCUT = "5";

   @RBEntry("Per visualizzare una cartella è necessario prima specificarla")
   @RBComment("Message displayed when attempting to view a folder but no folder has been specified to view.")
   public static final String NO_VIEW_FOLDER_GIVEN = "6";

   @RBEntry("Per aggiornare una cartella è necessario prima specificarla")
   @RBComment("Message displayed when attempting to update a folder but no folder has been specified to update.")
   public static final String NO_UPDATE_FOLDER_GIVEN = "7";

   @RBEntry("Specificare la cartella in cui creare la nuova cartella")
   @RBComment("Message displayed when the user attempts to create a new folder but has not given the folder in which to create the new folder.")
   public static final String NO_CREATE_FOLDER_GIVEN = "8";

   @RBEntry("Per eliminare una cartella è necessario prima specificarla")
   @RBComment("Message displayed when the user attempts to delete a folder but has not given the folder to be deleted.")
   public static final String NO_DELETE_FOLDER_GIVEN = "9";

   @RBEntry("Per visualizzare un collegamento è necessario prima specificarlo")
   @RBComment("Message displayed when attempting to view a shortcut but no shortcut has been specified to view.")
   public static final String NO_VIEW_SHORTCUT_GIVEN = "10";

   @RBEntry("Per aggiornare un collegamento è necessario prima specificarlo")
   @RBComment("Message displayed when attempting to update a shortcut but no shortcut has been specified to update.")
   public static final String NO_UPDATE_SHORTCUT_GIVEN = "11";

   @RBEntry("Per eliminare un collegamento è necessario prima specificarlo")
   @RBComment("Message displayed when attempting to delete a shortcut but no shortcut has been specified to update.")
   public static final String NO_DELETE_SHORTCUT_GIVEN = "12";

   @RBEntry("Non è stato fornito alcun riquadro per visualizzare la finestra di dialogo. Per visualizzare una finestra di dialogo è necessario fornire un riquadro")
   @RBComment("Message displayed when the frame required to display a dialog has not been given.")
   public static final String NO_PARENT_FRAME = "13";

   @RBEntry("Errore durante la localizzazione di {0}.  Impossibile trovare la chiave, \"{1}\", nel resource bundle \"{2}\". Contattare il personale di supporto.")
   @RBComment("Message displayed when an the key used to look up localized text in a resource bundle does not exist.")
   @RBArgComment0("the name of the dialog being localized")
   @RBArgComment1("the key not found in the resource bundle")
   @RBArgComment2("the name of the resource bundle being used")
   public static final String RESOURCE_BUNDLE_ERROR = "14";

   @RBEntry("Eliminando la cartella \"{0}\" verrà eliminato anche tutto il relativo contenuto.  Eliminare la cartella \"{0}\"?")
   public static final String CONFIRM_DELETE_FOLDER = "15";

   @RBEntry("Eliminare il collegamento a {0} \"{1}\"?")
   public static final String CONFIRM_DELETE_SHORTCUT = "16";

   @RBEntry("Ricerca destinazione del collegamento")
   public static final String BROWSE_TARGET_TITLE = "17";

   @RBEntry("Destinazione collegamento")
   public static final String BROWSE_TARGET_LABEL = "18";

   @RBEntry(":")
   public static final String LABEL_SUFFIX = "19";

   @RBEntry("{0}{1}")
   public static final String APPEND_SUFFIX = "20";

   @RBEntry("Lo schedario {0} è condiviso e non può essere eliminato.")
   public static final String CANNOT_DELETE_SHARED_CABINET = "21";

   @RBEntry("Errore durante la creazione di uno schedario: {0}")
   public static final String CREATE_CABINET_FAILED = "22";

   @RBEntry("Errore durante l'aggiornamento di uno schedario: {0}")
   public static final String UPDATE_CABINET_FAILED = "23";

   @RBEntry("Errore durante l'aggiornamento di una cartella: {0}")
   public static final String UPDATE_FOLDER_FAILED = "24";

   @RBEntry("Errore durante l'inizializzazione della finestra: {0}.")
   public static final String INITIALIZATION_FAILED = "25";
}
