/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.site;

import wt.util.resource.*;

@RBUUID("wt.clients.site.SiteCreateHelpRB")
public final class SiteCreateHelpRB extends WTListResourceBundle {
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_0 = "Contents/sitecreate";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_1 = "Help/sitecreate";

   /**
    * Pointer to the help for this screen ---------------------------------------
    **/
   @RBEntry("FileServerAdmin_CreateNewSite")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/sitecreate/SiteCreate";

   @RBEntry("ContentReplicationCreateSite")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_3 = "Help/sitecreate/PDM_SiteCreate";

   @RBEntry("ContentReplicationCreateSite")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_4 = "Help/sitecreate/PJL_SiteCreate";

   @RBEntry("FileServerAdmin_UpdateExistingSites")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_26 = "Help/siteupdate/SiteUpdate";
   
   @RBEntry("FileServerAdmin_ViewSiteProperties")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_5 = "Help/siteupdate/SiteView";


   /**
    * status bar messages --------------------------------------------------------
    **/
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_6 = "Desc/sitecreate/SiteCreate";

   @RBEntry("Click to create the site and close the form")
   public static final String PRIVATE_CONSTANT_7 = "Desc/sitecreate/SiteCreate/OK";

   @RBEntry("Click to view the help on creating a site")
   public static final String PRIVATE_CONSTANT_8 = "Desc/sitecreate/SiteCreate/Help";

   @RBEntry("Click to close the form without creating a new site")
   public static final String PRIVATE_CONSTANT_9 = "Desc/sitecreate/SiteCreate/Cancel";

   @RBEntry("Click to update the site and close the form")
   public static final String PRIVATE_CONSTANT_10 = "Desc/siteupdate/SiteUpdate/OK";

   @RBEntry("Click to view the help on updating the site")
   public static final String PRIVATE_CONSTANT_11 = "Desc/siteupdate/SiteUpdate/Help";

   @RBEntry("Click to close the form without updating the site")
   public static final String PRIVATE_CONSTANT_12 = "Desc/siteupdate/SiteUpdate/Cancel";

   /**
    * ----------------------------------
    **/
   @RBEntry("String name for the site (Required)")
   public static final String PRIVATE_CONSTANT_13 = "Desc/sitecreate/SiteCreate/name";

   @RBEntry("URL address for the site (Required)")
   public static final String PRIVATE_CONSTANT_14 = "Desc/sitecreate/SiteCreate/url";

   @RBEntry("Text describing the site")
   public static final String PRIVATE_CONSTANT_15 = "Desc/sitecreate/SiteCreate/description";

   @RBEntry("Modify the name for the site ")
   public static final String PRIVATE_CONSTANT_16 = "Desc/siteupdate/SiteUpdate/name";

   @RBEntry("Modify the URL address for the site ")
   public static final String PRIVATE_CONSTANT_17 = "Desc/siteupdate/SiteUpdate/url";

   @RBEntry("Modify the description of the site")
   public static final String PRIVATE_CONSTANT_18 = "Desc/siteupdate/SiteUpdate/description";

   /**
    * ----------------------------------
    **/
   @RBEntry("Select a principal for this site")
   public static final String PRIVATE_CONSTANT_19 = "Desc/sitecreate/SiteCreate/principal";

   @RBEntry("Check it if this is a content replication master site")
   public static final String PRIVATE_CONSTANT_20 = "Desc/sitecreate/SiteCreate/master";

   @RBEntry("Check it to modify to a content replication master site")
   public static final String PRIVATE_CONSTANT_21 = "Desc/siteupdate/SiteUpdate/master";

   @RBEntry("Check it if this is a file server")
   public static final String PRIVATE_CONSTANT_22 = "Desc/sitecreate/SiteCreate/replica";

   @RBEntry("Check it to modify to a file server")
   public static final String PRIVATE_CONSTANT_23 = "Desc/siteupdate/SiteUpdate/replica";

   @RBEntry("Check it if this is a product replication peer site")
   public static final String PRIVATE_CONSTANT_24 = "Desc/sitecreate/SiteCreate/prodReplPeer";

   @RBEntry("Set the context for this site")
   public static final String PRIVATE_CONSTANT_25 = "Desc/sitecreate/SiteCreate/containerSelectPanel";
}
