/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.site;

import wt.util.resource.*;

@RBUUID("wt.clients.site.SiteResource")
public final class SiteResource_it extends WTListResourceBundle {
   /**
    * Button Labels ------------------------------------------------------------
    * 
    **/
   @RBEntry("OK")
   public static final String OK = "1";

   @RBEntry("Annulla")
   public static final String CANCEL = "2";

   @RBEntry("Guida")
   public static final String HELP = "3";

   @RBEntry("Chiudi")
   public static final String CLOSE_BUTT = "4";

   @RBEntry("Nuovo")
   public static final String CREATE = "5";

   @RBEntry("Aggiorna")
   public static final String UPDATE = "6";

   @RBEntry("Elimina")
   public static final String DELETE = "7";

   @RBEntry("Visualizza")
   public static final String VIEW = "8";

   @RBEntry("Recupera")
   public static final String RETRIEVE = "9";

   @RBEntry("Importa chiave")
   public static final String IMPORT_KEY = "10";

   @RBEntry("Esporta chiave")
   public static final String EXPORT_KEY = "11";

   @RBEntry("Aggiorna chiavi")
   public static final String UPDATE_KEYS = "12";

   /**
    * List box column names ----------------------------------------------------
    **/
   @RBEntry("Nome sito")
   public static final String SITE_NAME_LB = "15";

   @RBEntry("M")
   public static final String MASTER_LB = "16";

   @RBEntry("R")
   public static final String REPLICA_LB = "17";

   @RBEntry("P")
   public static final String PRODREPLPEER_LB = "18";

   @RBEntry("URL")
   public static final String URL_LB = "19";

   @RBEntry("Data di generazione della chiave")
   public static final String KEY_DATE_LB = "20";

   @RBEntry("Utente/gruppo/ruolo")
   public static final String SITE_PRINCIPAL = "37";

   /**
    * Screen Titles --------------------------------------------------------------
    **/
   @RBEntry("Nuovo sito")
   public static final String NEWSITE_TITLE = "25";

   @RBEntry("Specificare il nome del sito.")
   public static final String SPECIFY_SITE = "31";

   @RBEntry("Eliminare il sito selezionato?")
   public static final String DEL_SITE_OK = "32";

   @RBEntry("Siti")
   public static final String SITES = "30";

   @RBEntry("Nome sito:")
   public static final String SITE_NAME = "33";

   @RBEntry("URL:")
   public static final String URL = "34";

   @RBEntry("Descrizione:")
   public static final String SITE_DESC = "35";

   @RBEntry("Il sito \"{0}\" esiste già. Aggiornarlo?")
   public static final String SITE_UPDATE = "40";

   @RBEntry("Aggiorna il sito esistente")
   public static final String UPDATESITE_TITLE = "26";

   @RBEntry("Proprietà del sito")
   public static final String VIEWSITE_TITLE = "27";

   @RBEntry("Specificare un URL per questo sito.")
   public static final String SPECIFY_URL = "51";

   @RBEntry("La sintassi dell'URL di input per questo sito non è valida. Immettere nuovamente l'URL.")
   public static final String INVALID_URL = "55";

   /**
    * ---------------------------------------------------------------------
    **/
   @RBEntry("Sito")
   public static final String SITE = "60";

   @RBEntry("Elenco dei siti")
   public static final String SITE_LIST = "61";

   @RBEntry("Gestione dei siti")
   public static final String SITE_TABLE = "62";

   @RBEntry("Generazione di nuove chiavi di accesso in corso. Attendere...")
   public static final String GEN_KEY_MSG = "63";

   @RBEntry("Creazione di nuovo sito in corso. Attendere...")
   public static final String CREATE_SITE_MSG = "64";

   @RBEntry("Utente/gruppo/ruolo:")
   public static final String PRINCIPAL = "65";

   @RBEntry("Selezionare un utente/gruppo/ruolo per questo sito.")
   public static final String NO_USER = "66";

   @RBEntry("(Questa installazione)")
   public static final String MASTER_LABEL = "67";

   /**
    * ---------------------------------------------------------------------
    **/
   @RBEntry("Tipo di sito:")
   public static final String SITE_TYPE = "56";

   @RBEntry("Master")
   public static final String MASTER_SITE = "57";

   @RBEntry("File server")
   public static final String REPLICA_SITE = "58";

   @RBEntry("Peer replica di prodotto")
   public static final String PRODREPLPEER_SITE = "59";

   @RBEntry("Prossimità sito:")
   public static final String SITE_PROXIMITY = "68";

   @RBEntry("Sposta su")
   public static final String MOVE_UP = "69";

   @RBEntry("Sposta giù")
   public static final String MOVE_DOWN = "70";

   @RBEntry("Sposta all'inizio")
   public static final String MOVE_TOP = "71";

   @RBEntry("Sposta in fondo")
   public static final String MOVE_BOTTOM = "72";

   @RBEntry("Contesto:")
   public static final String CONTAINER = "73";

   @RBEntry("Aggiornamento del sito in corso. Attendere...")
   public static final String UPDATE_SITE_MSG = "74";

   @RBEntry("Selezionare non più di una riga durante il riordino delle prossimità sito")
   public static final String ONLY_ONE_WHEN_REORDER = "75";

   @RBEntry("Seleziona")
   public static final String SELECT = "76";

   @RBEntry("Seleziona utente/gruppo/ruolo")
   public static final String PRINCIPAL_TITLE = "77";
   
   @RBEntry("Il sito {0} non esiste")
   public static final String SITE_DELETED = "78";
}
