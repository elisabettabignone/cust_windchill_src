/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.site;

import wt.util.resource.*;

@RBUUID("wt.clients.site.SiteResource")
public final class SiteResource extends WTListResourceBundle {
   /**
    * Button Labels ------------------------------------------------------------
    * 
    **/
   @RBEntry("OK")
   public static final String OK = "1";

   @RBEntry("Cancel")
   public static final String CANCEL = "2";

   @RBEntry("Help")
   public static final String HELP = "3";

   @RBEntry("Close")
   public static final String CLOSE_BUTT = "4";

   @RBEntry("New")
   public static final String CREATE = "5";

   @RBEntry("Update")
   public static final String UPDATE = "6";

   @RBEntry("Delete")
   public static final String DELETE = "7";

   @RBEntry("View")
   public static final String VIEW = "8";

   @RBEntry("Retrieve")
   public static final String RETRIEVE = "9";

   @RBEntry("Import Key")
   public static final String IMPORT_KEY = "10";

   @RBEntry("Export Key")
   public static final String EXPORT_KEY = "11";

   @RBEntry("Update Keys")
   public static final String UPDATE_KEYS = "12";

   /**
    * List box column names ----------------------------------------------------
    **/
   @RBEntry("Site Name")
   public static final String SITE_NAME_LB = "15";

   @RBEntry("M")
   public static final String MASTER_LB = "16";

   @RBEntry("R")
   public static final String REPLICA_LB = "17";

   @RBEntry("P")
   public static final String PRODREPLPEER_LB = "18";

   @RBEntry("URL")
   public static final String URL_LB = "19";

   @RBEntry("Key Generation Date")
   public static final String KEY_DATE_LB = "20";

   @RBEntry("Principal")
   public static final String SITE_PRINCIPAL = "37";

   /**
    * Screen Titles --------------------------------------------------------------
    **/
   @RBEntry("New Site")
   public static final String NEWSITE_TITLE = "25";

   @RBEntry("Please specify a site name.")
   public static final String SPECIFY_SITE = "31";

   @RBEntry("Delete the selected site?")
   public static final String DEL_SITE_OK = "32";

   @RBEntry("Sites")
   public static final String SITES = "30";

   @RBEntry("Site Name:")
   public static final String SITE_NAME = "33";

   @RBEntry("URL:")
   public static final String URL = "34";

   @RBEntry("Description:")
   public static final String SITE_DESC = "35";

   @RBEntry("The site named \"{0}\" already exists. Do you want to update this site?")
   public static final String SITE_UPDATE = "40";

   @RBEntry("Update the Existing Site")
   public static final String UPDATESITE_TITLE = "26";

   @RBEntry("Site Properties")
   public static final String VIEWSITE_TITLE = "27";

   @RBEntry("Please specify a URL for this site.")
   public static final String SPECIFY_URL = "51";

   @RBEntry("The input URL for this site has an invalid syntax. Please retype the URL.")
   public static final String INVALID_URL = "55";

   /**
    * ---------------------------------------------------------------------
    **/
   @RBEntry("Site")
   public static final String SITE = "60";

   @RBEntry("List of Sites")
   public static final String SITE_LIST = "61";

   @RBEntry("Site Management")
   public static final String SITE_TABLE = "62";

   @RBEntry("Generating new access keys. Please wait.")
   public static final String GEN_KEY_MSG = "63";

   @RBEntry("Creating a new site. Please wait.")
   public static final String CREATE_SITE_MSG = "64";

   @RBEntry("Principal:")
   public static final String PRINCIPAL = "65";

   @RBEntry("Please select a principal for this site.")
   public static final String NO_USER = "66";

   @RBEntry("(This Installation)")
   public static final String MASTER_LABEL = "67";

   /**
    * ---------------------------------------------------------------------
    **/
   @RBEntry("Site Type:")
   public static final String SITE_TYPE = "56";

   @RBEntry("Master")
   public static final String MASTER_SITE = "57";

   @RBEntry("File Server")
   public static final String REPLICA_SITE = "58";

   @RBEntry("Product Replication Peer")
   public static final String PRODREPLPEER_SITE = "59";

   @RBEntry("Site Proximity:")
   public static final String SITE_PROXIMITY = "68";

   @RBEntry("Move Up")
   public static final String MOVE_UP = "69";

   @RBEntry("Move Down")
   public static final String MOVE_DOWN = "70";

   @RBEntry("Move Top")
   public static final String MOVE_TOP = "71";

   @RBEntry("Move Bottom")
   public static final String MOVE_BOTTOM = "72";

   @RBEntry("Context:")
   public static final String CONTAINER = "73";

   @RBEntry("Updating the site. Please wait.")
   public static final String UPDATE_SITE_MSG = "74";

   @RBEntry("Please select no more than one row when reordering the site proximities")
   public static final String ONLY_ONE_WHEN_REORDER = "75";

   @RBEntry("Select")
   public static final String SELECT = "76";

   @RBEntry("Select Principal")
   public static final String PRINCIPAL_TITLE = "77";
   
   @RBEntry("Site {0} does not exist")
   public static final String SITE_DELETED = "78";
}