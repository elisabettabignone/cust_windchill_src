/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.site;

import wt.util.resource.*;

@RBUUID("wt.clients.site.SiteCreateHelpRB")
public final class SiteCreateHelpRB_it extends WTListResourceBundle {
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_0 = "Contents/sitecreate";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_1 = "Help/sitecreate";

   /**
    * Pointer to the help for this screen ---------------------------------------
    **/
   @RBEntry("FileServerAdmin_CreateNewSite")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/sitecreate/SiteCreate";

   @RBEntry("ContentReplicationCreateSite")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_3 = "Help/sitecreate/PDM_SiteCreate";

   @RBEntry("ContentReplicationCreateSite")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_4 = "Help/sitecreate/PJL_SiteCreate";

   @RBEntry("FileServerAdmin_UpdateExistingSites")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_26 = "Help/siteupdate/SiteUpdate";
   
   @RBEntry("FileServerAdmin_ViewSiteProperties")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_5 = "Help/siteupdate/SiteView";


   /**
    * status bar messages --------------------------------------------------------
    **/
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_6 = "Desc/sitecreate/SiteCreate";

   @RBEntry("Fare clic per creare il sito e chiudere il modulo")
   public static final String PRIVATE_CONSTANT_7 = "Desc/sitecreate/SiteCreate/OK";

   @RBEntry("Fare clic per visualizzare la guida per la creazione di un sito")
   public static final String PRIVATE_CONSTANT_8 = "Desc/sitecreate/SiteCreate/Help";

   @RBEntry("Fare clic per chiudere il modulo senza creare un nuovo sito")
   public static final String PRIVATE_CONSTANT_9 = "Desc/sitecreate/SiteCreate/Cancel";

   @RBEntry("Fare clic per aggiornare il sito e chiudere il modulo")
   public static final String PRIVATE_CONSTANT_10 = "Desc/siteupdate/SiteUpdate/OK";

   @RBEntry("Fare clic per visualizzare le informazioni della Guida relative all'aggiornamento del sito")
   public static final String PRIVATE_CONSTANT_11 = "Desc/siteupdate/SiteUpdate/Help";

   @RBEntry("Fare clic per chiudere il modulo senza aggiornare il sito")
   public static final String PRIVATE_CONSTANT_12 = "Desc/siteupdate/SiteUpdate/Cancel";

   /**
    * ----------------------------------
    **/
   @RBEntry("Nome della stringa del sito (obbligatorio)")
   public static final String PRIVATE_CONSTANT_13 = "Desc/sitecreate/SiteCreate/name";

   @RBEntry("Indirizzo URL del sito (obbligatorio)")
   public static final String PRIVATE_CONSTANT_14 = "Desc/sitecreate/SiteCreate/url";

   @RBEntry("Testo descrittivo del sito")
   public static final String PRIVATE_CONSTANT_15 = "Desc/sitecreate/SiteCreate/description";

   @RBEntry("Consente di modificare il nome del sito")
   public static final String PRIVATE_CONSTANT_16 = "Desc/siteupdate/SiteUpdate/name";

   @RBEntry("Consente di modificare l'URL del sito")
   public static final String PRIVATE_CONSTANT_17 = "Desc/siteupdate/SiteUpdate/url";

   @RBEntry("Consente di modificare la descrizione del sito")
   public static final String PRIVATE_CONSTANT_18 = "Desc/siteupdate/SiteUpdate/description";

   /**
    * ----------------------------------
    **/
   @RBEntry("Selezionare un utente/gruppo/ruolo per questo sito")
   public static final String PRIVATE_CONSTANT_19 = "Desc/sitecreate/SiteCreate/principal";

   @RBEntry("Spuntare se si tratta di un sito master per replica di dati")
   public static final String PRIVATE_CONSTANT_20 = "Desc/sitecreate/SiteCreate/master";

   @RBEntry("Spuntare per cambiare a un sito master per replica di dati")
   public static final String PRIVATE_CONSTANT_21 = "Desc/siteupdate/SiteUpdate/master";

   @RBEntry("Spuntare se si tratta di un file server")
   public static final String PRIVATE_CONSTANT_22 = "Desc/sitecreate/SiteCreate/replica";

   @RBEntry("Spuntare per cambiare a un file server")
   public static final String PRIVATE_CONSTANT_23 = "Desc/siteupdate/SiteUpdate/replica";

   @RBEntry("Spuntare se si tratta di un sito peer per replica di prodotto")
   public static final String PRIVATE_CONSTANT_24 = "Desc/sitecreate/SiteCreate/prodReplPeer";

   @RBEntry("Imposta il contesto per il sito")
   public static final String PRIVATE_CONSTANT_25 = "Desc/sitecreate/SiteCreate/containerSelectPanel";
}
