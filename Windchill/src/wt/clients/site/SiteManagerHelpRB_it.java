/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.site;

import wt.util.resource.*;

@RBUUID("wt.clients.site.SiteManagerHelpRB")
public final class SiteManagerHelpRB_it extends WTListResourceBundle {
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_0 = "Contents/sitemanager";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_1 = "Help/sitemanager";

   /**
    * Pointer to the help for this screen -----------------------------------
    **/
   @RBEntry("FileServerAdmin_SiteManagementRef")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/sitemanager/SiteManager";

   @RBEntry("ContentReplicationAdministerSite")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_3 = "Help/sitemanager/PDM_SiteManager";

   @RBEntry("ContentReplicationAdministerSite")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_4 = "Help/sitemanager/PJL_SiteManager";

   /**
    * status bar messages ---------------------------------------------------
    **/
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_5 = "Desc/sitemanager/SiteManager";

   @RBEntry("Fare clic per chiudere il modulo")
   public static final String PRIVATE_CONSTANT_6 = "Desc/sitemanager/SiteManager/Close";

   @RBEntry("Fare clic per visualizzare la guida alla gestione dei siti")
   public static final String PRIVATE_CONSTANT_7 = "Desc/sitemanager/SiteManager/Help";

   @RBEntry("Fare clic per creare un nuovo sito")
   public static final String PRIVATE_CONSTANT_8 = "Desc/sitemanager/SiteManager/Create";

   @RBEntry("Fare clic per aggiornare il sito selezionato")
   public static final String PRIVATE_CONSTANT_9 = "Desc/sitemanager/SiteManager/Update";

   @RBEntry("Fare clic per visualizzare le proprietà del sito selezionato")
   public static final String PRIVATE_CONSTANT_10 = "Desc/sitemanager/SiteManager/View";

   @RBEntry("Fare clic per eliminare il sito selezionato")
   public static final String PRIVATE_CONSTANT_11 = "Desc/sitemanager/SiteManager/Delete";

   @RBEntry("Fare clic per visualizzare l'elenco aggiornato dei siti del database")
   public static final String PRIVATE_CONSTANT_12 = "Desc/sitemanager/SiteManager/Retrieve";

   @RBEntry("Fare clic per leggere la chiave pubblica di accesso per il sito selezionato da un file locale")
   public static final String PRIVATE_CONSTANT_13 = "Desc/sitemanager/SiteManager/Import Key";

   @RBEntry("Fare clic per scrivere la chiave pubblica di accesso per il sito selezionato su un file locale")
   public static final String PRIVATE_CONSTANT_14 = "Desc/sitemanager/SiteManager/Export Key";

   @RBEntry("Fare clic per generare una nuova coppia di chiavi pubbliche di accesso per il sito locale")
   public static final String PRIVATE_CONSTANT_15 = "Desc/sitemanager/SiteManager/Update Keys";

   @RBEntry("M/R: sito master/replica per la replica di dati; P: sito peer per la replica di prodotto; Utente/gruppo/ruolo: WTUser assegnato al sito")
   public static final String PRIVATE_CONSTANT_16 = "Desc/sitemanager/SiteManager/List Keys";
}
