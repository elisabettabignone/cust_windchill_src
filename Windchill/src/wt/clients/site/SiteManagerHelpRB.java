/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.site;

import wt.util.resource.*;

@RBUUID("wt.clients.site.SiteManagerHelpRB")
public final class SiteManagerHelpRB extends WTListResourceBundle {
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_0 = "Contents/sitemanager";

   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_1 = "Help/sitemanager";

   /**
    * Pointer to the help for this screen -----------------------------------
    **/
   @RBEntry("FileServerAdmin_SiteManagementRef")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_2 = "Help/sitemanager/SiteManager";

   @RBEntry("ContentReplicationAdministerSite")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_3 = "Help/sitemanager/PDM_SiteManager";

   @RBEntry("ContentReplicationAdministerSite")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_4 = "Help/sitemanager/PJL_SiteManager";

   /**
    * status bar messages ---------------------------------------------------
    **/
   @RBEntry(" ")
   public static final String PRIVATE_CONSTANT_5 = "Desc/sitemanager/SiteManager";

   @RBEntry("Click to close the form")
   public static final String PRIVATE_CONSTANT_6 = "Desc/sitemanager/SiteManager/Close";

   @RBEntry("Click to view the help on Managing Sites")
   public static final String PRIVATE_CONSTANT_7 = "Desc/sitemanager/SiteManager/Help";

   @RBEntry("Click to create a new site")
   public static final String PRIVATE_CONSTANT_8 = "Desc/sitemanager/SiteManager/Create";

   @RBEntry("Click to update the selected site")
   public static final String PRIVATE_CONSTANT_9 = "Desc/sitemanager/SiteManager/Update";

   @RBEntry("Click to view the properties of the selected site")
   public static final String PRIVATE_CONSTANT_10 = "Desc/sitemanager/SiteManager/View";

   @RBEntry("Click to delete the selected site")
   public static final String PRIVATE_CONSTANT_11 = "Desc/sitemanager/SiteManager/Delete";

   @RBEntry("Click to get the current list of sites from the database")
   public static final String PRIVATE_CONSTANT_12 = "Desc/sitemanager/SiteManager/Retrieve";

   @RBEntry("Click to read the public access key for the selected site from a local file")
   public static final String PRIVATE_CONSTANT_13 = "Desc/sitemanager/SiteManager/Import Key";

   @RBEntry("Click to write the public access key for the selected site to a local file")
   public static final String PRIVATE_CONSTANT_14 = "Desc/sitemanager/SiteManager/Export Key";

   @RBEntry("Click to generate a new access key pair for the local site")
   public static final String PRIVATE_CONSTANT_15 = "Desc/sitemanager/SiteManager/Update Keys";

   @RBEntry("M/R: Master/Replica site for content replication; P: Peer site for product replication; Principal: User assigned to the site")
   public static final String PRIVATE_CONSTANT_16 = "Desc/sitemanager/SiteManager/List Keys";
}