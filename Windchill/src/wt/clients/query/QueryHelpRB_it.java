/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.query;

import wt.util.resource.*;

@RBUUID("wt.clients.query.QueryHelpRB")
public final class QueryHelpRB_it extends WTListResourceBundle {
   @RBEntry("ExportImportExpBaselineSearch")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_0 = "Help/Query/QueryHelp";

   @RBEntry("Visualizza altre informazioni sull'elemento selezionato")
   public static final String PRIVATE_CONSTANT_1 = "Desc/Query/Query/View";

   @RBEntry("Aggiorna l'elemento selezionato")
   public static final String PRIVATE_CONSTANT_2 = "Desc/Query/Query/Update";

   @RBEntry("Effettua Check-In di un elemento sottoposto a Check-Out")
   public static final String PRIVATE_CONSTANT_3 = "Desc/Query/Query/Checkin";

   @RBEntry("Effettua Check-Out dell'elemento selezionato")
   public static final String PRIVATE_CONSTANT_4 = "Desc/Query/Query/Checkout";

   @RBEntry("Avvia una nuova istanza di Navigatore Windchill")
   public static final String PRIVATE_CONSTANT_5 = "Desc/Query/Query/Windchill Explorer";

   @RBEntry("Termina la ricerca locale corrente")
   public static final String PRIVATE_CONSTANT_6 = "Desc/Query/Query/Close";

   @RBEntry("Visualizza la Guida in linea")
   public static final String PRIVATE_CONSTANT_7 = "Desc/Query/Query/Help";

   @RBEntry("Avvia il Navigatore dati di prodotto")
   public static final String PRIVATE_CONSTANT_8 = "Desc/Query/Query/Product Information Explorer";

   @RBEntry("Annulla il Check-Out dell'elemento selezionato")
   public static final String PRIVATE_CONSTANT_9 = "Desc/Query/Query/Undo Checkout";

   @RBEntry("Seleziona un nuovo tipo di classe su cui effettuare la ricerca")
   public static final String PRIVATE_CONSTANT_10 = "Desc/Query/Query/Class Picker";
}
