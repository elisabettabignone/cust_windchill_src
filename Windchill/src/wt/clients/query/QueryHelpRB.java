/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.query;

import wt.util.resource.*;

@RBUUID("wt.clients.query.QueryHelpRB")
public final class QueryHelpRB extends WTListResourceBundle {
   @RBEntry("ExportImportExpBaselineSearch")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_0 = "Help/Query/QueryHelp";

   @RBEntry("View more information about the selected item")
   public static final String PRIVATE_CONSTANT_1 = "Desc/Query/Query/View";

   @RBEntry("Update the selected item")
   public static final String PRIVATE_CONSTANT_2 = "Desc/Query/Query/Update";

   @RBEntry("Check in a checked out item")
   public static final String PRIVATE_CONSTANT_3 = "Desc/Query/Query/Checkin";

   @RBEntry("Check out the selected item")
   public static final String PRIVATE_CONSTANT_4 = "Desc/Query/Query/Checkout";

   @RBEntry("Launch a new Windchill Explorer")
   public static final String PRIVATE_CONSTANT_5 = "Desc/Query/Query/Windchill Explorer";

   @RBEntry("Close this Local Search Task")
   public static final String PRIVATE_CONSTANT_6 = "Desc/Query/Query/Close";

   @RBEntry("Access online Help")
   public static final String PRIVATE_CONSTANT_7 = "Desc/Query/Query/Help";

   @RBEntry("Launch the Product Information Explorer")
   public static final String PRIVATE_CONSTANT_8 = "Desc/Query/Query/Product Information Explorer";

   @RBEntry("Undo check out of the selected item")
   public static final String PRIVATE_CONSTANT_9 = "Desc/Query/Query/Undo Checkout";

   @RBEntry("Select a new class type to search on")
   public static final String PRIVATE_CONSTANT_10 = "Desc/Query/Query/Class Picker";
}
