/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.query;

import wt.util.resource.*;

@RBUUID("wt.clients.query.QueryRB")
public final class QueryRB extends WTListResourceBundle {
   /**
    * BUTTON LABELS -------------------------------------------------------------
    * 
    **/
   @RBEntry("Update")
   @RBComment("Labels on command buttons on local search.")
   public static final String PRIVATE_CONSTANT_0 = "updateButton";

   @RBEntry("View")
   @RBComment("Labels on command buttons on local search.")
   public static final String PRIVATE_CONSTANT_1 = "viewButton";

   @RBEntry("Checkout")
   @RBComment("Labels on command buttons on local search.")
   public static final String PRIVATE_CONSTANT_2 = "checkoutButton";

   @RBEntry("Checkin")
   @RBComment("Labels on command buttons on local search.")
   public static final String PRIVATE_CONSTANT_3 = "checkinButton";

   @RBEntry("Close")
   @RBComment("Labels on command buttons on local search.")
   public static final String PRIVATE_CONSTANT_4 = "closeButton";

   @RBEntry("Windchill Explorer")
   @RBComment("Labels on command buttons on local search.")
   public static final String PRIVATE_CONSTANT_5 = "windchillExplorerButton";

   @RBEntry("Help")
   @RBComment("Labels on command buttons on local search.")
   public static final String PRIVATE_CONSTANT_6 = "helpButton";

   @RBEntry("Undo Checkout")
   @RBComment("Labels on command buttons on local search.")
   public static final String PRIVATE_CONSTANT_7 = "undoCheckoutButton";

   @RBEntry("Product Structure Explorer")
   @RBComment("Labels on command buttons on local search.")
   public static final String PRIVATE_CONSTANT_8 = "pIEButton";

   /**
    * PICKER LABEL ------------------------------------------------------------
    **/
   @RBEntry("Search on")
   @RBComment("Text label before class picker on local search.")
   public static final String PRIVATE_CONSTANT_9 = "label1";

   /**
    * Window label ------------------------------------------------------------
    **/
   @RBEntry("Product Information Explorer")
   @RBComment("Text label for top of Product Information Explorer when called from local search.")
   public static final String PRIVATE_CONSTANT_10 = "pIELabel";

   /**
    * SYMBOLS -----------------------------------------------------------------
    **/
   @RBEntry(":")
   public static final String PRIVATE_CONSTANT_11 = "labelColon";

   /**
    * ERROR MESSAGES ----------------------------------------------------------
    **/
   @RBEntry("Error, couldn't configure screen with attributes.")
   public static final String CONFIG_ATTRIB = "0";

   @RBEntry("Error, problem configuring query panel.")
   public static final String CONFIG_PANEL = "1";

   @RBEntry("Error, failure in initializing context sensitive help.")
   public static final String INIT_HELP = "2";

   @RBEntry("Failure setting StatusText while initializing Help listener.")
   public static final String SETTING_STATUS = "3";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("QueryPanel Event::")
   @RBComment("Used in printing event debug information out.")
   public static final String DEBUG_EVENT = "4";

   @RBEntry("Error, problem sending message to status bar.")
   public static final String STATUS_BAR = "5";
}
