/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.query;

import wt.util.resource.*;

@RBUUID("wt.clients.query.QueryRB")
public final class QueryRB_it extends WTListResourceBundle {
   /**
    * BUTTON LABELS -------------------------------------------------------------
    * 
    **/
   @RBEntry("Aggiorna")
   @RBComment("Labels on command buttons on local search.")
   public static final String PRIVATE_CONSTANT_0 = "updateButton";

   @RBEntry("Visualizza")
   @RBComment("Labels on command buttons on local search.")
   public static final String PRIVATE_CONSTANT_1 = "viewButton";

   @RBEntry("Check-Out")
   @RBComment("Labels on command buttons on local search.")
   public static final String PRIVATE_CONSTANT_2 = "checkoutButton";

   @RBEntry("Check-In")
   @RBComment("Labels on command buttons on local search.")
   public static final String PRIVATE_CONSTANT_3 = "checkinButton";

   @RBEntry("Chiudi")
   @RBComment("Labels on command buttons on local search.")
   public static final String PRIVATE_CONSTANT_4 = "closeButton";

   @RBEntry("Navigatore Windchill")
   @RBComment("Labels on command buttons on local search.")
   public static final String PRIVATE_CONSTANT_5 = "windchillExplorerButton";

   @RBEntry("Guida")
   @RBComment("Labels on command buttons on local search.")
   public static final String PRIVATE_CONSTANT_6 = "helpButton";

   @RBEntry("Annulla Check-Out")
   @RBComment("Labels on command buttons on local search.")
   public static final String PRIVATE_CONSTANT_7 = "undoCheckoutButton";

   @RBEntry("Navigatore struttura di prodotto")
   @RBComment("Labels on command buttons on local search.")
   public static final String PRIVATE_CONSTANT_8 = "pIEButton";

   /**
    * PICKER LABEL ------------------------------------------------------------
    **/
   @RBEntry("Chiave di ricerca")
   @RBComment("Text label before class picker on local search.")
   public static final String PRIVATE_CONSTANT_9 = "label1";

   /**
    * Window label ------------------------------------------------------------
    **/
   @RBEntry("Navigatore dati di prodotto")
   @RBComment("Text label for top of Product Information Explorer when called from local search.")
   public static final String PRIVATE_CONSTANT_10 = "pIELabel";

   /**
    * SYMBOLS -----------------------------------------------------------------
    **/
   @RBEntry(":")
   public static final String PRIVATE_CONSTANT_11 = "labelColon";

   /**
    * ERROR MESSAGES ----------------------------------------------------------
    **/
   @RBEntry("Errore. Impossibile configurare lo schermo con gli attributi.")
   public static final String CONFIG_ATTRIB = "0";

   @RBEntry("Errore durante la configurazione del riquadro di interrogazione.")
   public static final String CONFIG_PANEL = "1";

   @RBEntry("Errore durante l'inizializzazione della guida contestuale.")
   public static final String INIT_HELP = "2";

   @RBEntry("Errore nell'impostazione di StatusText durante l'inizializzazione della chiamata della Guida.")
   public static final String SETTING_STATUS = "3";

   /**
    * ---------------------------------------------------------------------------
    **/
   @RBEntry("Evento nel riquadro di interrogazione:")
   @RBComment("Used in printing event debug information out.")
   public static final String DEBUG_EVENT = "4";

   @RBEntry("Errore durante l'invio del messaggio alla barra di stato.")
   public static final String STATUS_BAR = "5";
}
