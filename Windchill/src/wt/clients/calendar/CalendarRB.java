/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.calendar;

import wt.util.resource.*;

@RBUUID("wt.clients.calendar.CalendarRB")
public final class CalendarRB extends WTListResourceBundle {
   @RBEntry("CalendarHelp")
   public static final String CALENDAR_HELP = "CalendarHelp";

   @RBEntry("CalAbout")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_0 = "Help/Calendar/CalendarHelp";

   @RBEntry("Mark Working")
   public static final String MARK_WORKING = "0";

   @RBEntry("Mark Nonworking")
   public static final String MARK_NONWORKING = "1";

   @RBEntry("Delegate")
   public static final String DELEGATE = "2";

   @RBEntry("Delegate Indefinitely")
   public static final String DELEGATE_INDEFINITELY = "3";

   @RBEntry("Reset")
   public static final String RESET = "4";

   @RBEntry("Reset All")
   public static final String RESET_ALL = "5";

   @RBEntry("Help")
   public static final String HELP = "6";

   @RBEntry("My Calendar")
   public static final String MY_CALENDAR = "7";

   @RBEntry("User Calendar")
   public static final String USER_CALENDAR = "8";

   @RBEntry("System Calendar")
   public static final String SYSTEM_CALENDAR = "9";

   @RBEntry("Delegate from {0} to {1}")
   public static final String TIME_SPAN = "10";
   
   @RBEntry("Calendar")
   public static final String CALENDAR_TITLE = "11";

   @RBEntry("ATTENTION: One or more of the selected dates are within an indefinite delegation period.\n Do you want to reset the entire indefinite delegation?")
   public static final String Remove_InDefinite_Delegate_Message = "12";
}
