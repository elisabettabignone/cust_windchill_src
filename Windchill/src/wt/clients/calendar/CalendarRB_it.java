/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.clients.calendar;

import wt.util.resource.*;

@RBUUID("wt.clients.calendar.CalendarRB")
public final class CalendarRB_it extends WTListResourceBundle {
   @RBEntry("Guida del calendario")
   public static final String CALENDAR_HELP = "CalendarHelp";

   @RBEntry("CalAbout")
   @RBPseudo(false)
   @RBComment("DO NOT TRANSLATE - this is a help topic name")
   public static final String PRIVATE_CONSTANT_0 = "Help/Calendar/CalendarHelp";

   @RBEntry("Contrassegna come lavorativo")
   public static final String MARK_WORKING = "0";

   @RBEntry("Contrassegna come non lavorativo")
   public static final String MARK_NONWORKING = "1";

   @RBEntry("Delega")
   public static final String DELEGATE = "2";

   @RBEntry("Delega indefinitamente")
   public static final String DELEGATE_INDEFINITELY = "3";

   @RBEntry("Reimposta")
   public static final String RESET = "4";

   @RBEntry("Reimposta tutto")
   public static final String RESET_ALL = "5";

   @RBEntry("Guida")
   public static final String HELP = "6";

   @RBEntry("Il mio calendario")
   public static final String MY_CALENDAR = "7";

   @RBEntry("Calendario utente")
   public static final String USER_CALENDAR = "8";

   @RBEntry("Calendario di sistema")
   public static final String SYSTEM_CALENDAR = "9";

   @RBEntry("Delega da {0} a {1}")
   public static final String TIME_SPAN = "10";
   
   @RBEntry("Calendario")
   public static final String CALENDAR_TITLE = "11";

   @RBEntry("ATTENZIONE: almeno una delle date selezionate ricade in un periodo di delega indefinito.\n Reimpostare l'intera delega indefinita?")
   public static final String Remove_InDefinite_Delegate_Message = "12";
}
