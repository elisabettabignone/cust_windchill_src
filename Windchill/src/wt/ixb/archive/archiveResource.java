/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.ixb.archive;

import wt.util.resource.*;

@RBUUID("wt.ixb.archive.archiveResource")
public final class archiveResource extends WTListResourceBundle {
   /**
    * ############################# 1000. Messages ##############################
    **/
   @RBEntry("Invalid object type \"{0}\", cannot perform archive operation.")
   @RBComment("archive cannot be performed on invalid type of object")
   @RBArgComment0("the class name of the invalid type")
   public static final String MSG_ARCHIVE_INVALID_OBJECT_TYPE = "1001";

   @RBEntry("Invalid object type \"{0}\", cannot perform restore operation.")
   @RBComment("restore cannot be performed on invalid type of object")
   @RBArgComment0("the class name of the invalid type")
   public static final String MSG_RESTORE_INVALID_OBJECT_TYPE = "1002";

   @RBEntry("Failed to find XML element \"{0}\" during restore operation.")
   @RBComment("this happens when restore fails find an xml element during an restore operation")
   @RBArgComment0("the name of the xml element")
   public static final String MSG_RESTORE_XML_ELEMENT_NOT_FOUND = "1003";

   @RBEntry("Failed to find Windchill principal corresponding to name \"{0}\".")
   @RBComment("this happens when a Windchill principal as identified by the name cannot be found")
   @RBArgComment0("the name of the Windchill principal")
   public static final String MSG_RESTORE_PRINCIPAL_NOT_FOUND = "1004";

   @RBEntry("Failed to assign iteration creator to \"{0}\" during restore operation.")
   @RBComment("Failed to assign iteration creator during restore operation")
   @RBArgComment0("the iteration creator name")
   public static final String MSG_RESTORE_CANNOT_SET_CREATED_BY = "1005";

   @RBEntry("Failed to assign iteration modifier to \"{0}\" during restore operation.")
   @RBComment("Failed to assign iteration modifier during restore operation")
   @RBArgComment0("the iteration creator name")
   public static final String MSG_RESTORE_CANNOT_SET_MODIFIED_BY = "1006";

   @RBEntry("Failed to assign iteration creator to \"{0}\" during restore operation.")
   @RBComment("Failed to assign iteration creator during restore operation")
   @RBArgComment0("the iteration creator name")
   public static final String MSG_RESTORE_CANNOT_SET_OWNED_BY = "1007";

   @RBEntry("Failed to archive create record.")
   @RBComment("Failed to archive the \"create record\", which contains create stamp, creator, modifier, ...")
   public static final String MSG_ARCHIVE_CREATE_RECORD_ARCHIVE_ERROR = "1008";

   @RBEntry("Invalid xml value \"{0}\" for xml element {1}.")
   @RBComment("During restore, an invalid value of xml element is encountered.")
   @RBArgComment0("the invalid value")
   @RBArgComment1("the xml element name")
   public static final String MSG_RESTORE_INVALID_XML_VALUE = "1009";

   @RBEntry("Internal error: invalid transaction context when restoring {0}")
   @RBComment("internal error on transaction context during restore operation")
   @RBArgComment1("the object id of the object being restored")
   public static final String MSG_RESTORE_INVALID_TRANSACTION_CONTEXT = "1010";

   @RBEntry("Internal error: null transaction when restoring {0}")
   @RBComment("internal error occured during restore of object, transaction is null")
   @RBArgComment1("the object id of the object being restored")
   public static final String MSG_RESTORE_TRANSACTION_IS_NULL = "1011";

   @RBEntry("Internal error: object is already persistent, cannot set object id for {0}")
   @RBComment("internal error occured during restore of object")
   @RBArgComment1("the object whose id is being set")
   public static final String MSG_RESTORE_OBJECT_ALREADY_PERSISTENT = "1012";

   @RBEntry("Previous iterations of the {0} {1} associated with location {2} in context {3} in the archive jar have been restored to the location {4} in context {5} as the object has, since the archive operation, been moved to this location.")
   @RBComment("message in restore.txt")
   @RBArgComment0("objectType")
   @RBArgComment1("displayIdentifier of the object")
   @RBArgComment2("previous folder location")
   @RBArgComment3("previous context")
   @RBArgComment4("new folder location")
   @RBArgComment5("new context")
   public static final String OBJ_RESTORED_IN_OTHER_FOLDER = "1013";
}