/* bcwti
 *
 * Copyright (c) 2011 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.ixb.epm;

import wt.util.resource.*;

@RBUUID("wt.ixb.epm.EPMResource")
public final class EPMResource_it extends WTListResourceBundle {

    @RBEntry("Generico istanza di family table \"{0}\" mancante. Impossibile importare la family table.")
    @RBComment("Error Message specific for Package Import.  EPMVariantLink can not be created because the generic master is not found on the target system.")
    @RBArgComment0("The identity of the Instance CAD document")
    public static final String PACKAGE_IMPORT_GENERIC_MASTER_NOT_FOUND = "1";   
    
    @RBEntry("L'oggetto che segue non è stato importato. Il tipo di oggetto non è supportato per l'importazione delle consegne ricevute nel sistema Windchill: {0}.")
    @RBComment("Error Message for Package Import.  The object is not getting imported.")
    @RBArgComment0("The display name of any EPM objects, EPMDocument, EPMSepFamilyTable, EPMMemberLink, etc.")    
    public static final String PACKAGE_IMPORT_IGNORE_OBJECT = "2";

    @RBEntry("Gli oggetti che seguono non sono stati importati. I tipi di oggetti non sono supportati per l'importazione delle consegne ricevute nel sistema Windchill: {0}.")
    @RBComment("Error Message for Package Import.  The objects are not getting imported.")
    @RBArgComment0("The identity of two or more EPM objects, EPMDocument, EPMSepFamilyTable, EPMMemberLink, etc.")    
    public static final String PACKAGE_IMPORT_IGNORE_OBJECT_PLURAL = "3";
    
    @RBEntry("Impossibile importare la relazione cronologia origine/immagine del seguente documento CAD/dinamico, poiché l'oggetto ad esso correlato non risiede nel package o nel sistema di destinazione: {0}")
    @RBComment("If the other side EPMDoc does not exist, then skip importing the link and provide a warning in the log.")
    public static final String DERIVED_REP_RULE_NOT_IMPOTRTED = "4";
    
    @RBEntry("Impossibile importare la relazione cronologia origine/immagine, poiché sia l'origine che l'immagine non risiedono nel package o nel sistema di destinazione: {0}")
    @RBComment("If both source and image EPMDoc do not exist, then skip importing the link and provide a error in the log.")
    public static final String DERIVED_REP_RULE_NOT_IMPOTRTED_ERROR = "5";

    @RBEntry("Il link cronologia build non è stato incluso nella consegna package. Origine e destinazione mancanti.")
    @RBComment("If both source and target does not exist, then skip importing the link and provide a error in the log.")
    public static final String HISTORY_IGNORED_BOTH_ENDS_MISSING = "6";
    
    @RBEntry("{0} aveva una relazione di tipo \"{1}\" con un altro oggetto non incluso nella consegna package. La relazione e l'oggetto mancante sono stati ignorati durante l'importazione.")
    @RBComment("If either source or target does not exist, then skip importing the history and provide a warning in the log.")
    public static final String HISTORY_IGNORED_MISSING_OTHER_END = "7";
    
    @RBEntry("Il link associazione regole d creazione non è stato incluso nella consegna package. Origine e destinazione mancanti.")
    @RBComment("If both source and target does not exist, then skip importing the link and provide a error in the log.")
    public static final String ASSOCIATION_LINK_IGNORED_BOTH_ENDS_MISSING = "8";
    
    @RBEntry("{0} aveva una relazione di tipo \"{1}\" con un altro oggetto non incluso nella consegna package. La relazione e l'oggetto mancante sono stati ignorati durante l'importazione.")
    @RBComment("If either source or target does not exist, then skip importing the EPMBuildRuleAssociationLink and provide a warning in the log.")
    public static final String ASSOCIATION_LINK_IGNORED_MISSING_OTHER_END = "9";
    
    @RBComment("Build Rule Id is not found between source and target")
    @RBEntry("ID regola di creazione non trovato tra {0} e {1}")
    public static final String BUILD_RULE_ID_NOT_FOUND = "10";
    
    @RBComment("Model Item Master not found.")
    @RBEntry("Impossibile trovare il master dell'elemento modello.")
    public static final String MODEL_ITEM_MASTER_NOT_FOUND = "11";
    
    @RBEntry("I seguenti oggetti vengono ignorati per la creazione del contesto di {0} come modulo CAD o per {1} non sono state trovate le immagini derivate nell'applicazione di creazione richiesta")
    @RBComment("If the cad module, or its derived EPMDocument image is not found, then skip the creation/update the link and provide information to the log.")
    public static final String SKIP_NOT_FOUND_IMAGES_OR_MODULES = "12";    

    @RBComment("Multiple EPMDescribeLink objects with the same parent, child and built flag found.  Package Import does not know which link object to update.")
    @RBEntry("Più oggetti EPMDescribeLink con le stesse informazioni. Impossibile identificare un link da aggiornare.")
    public static final String MULTIPLE_DESCRIBE_LINKS_WITH_SAME_INFO_FOUND = "13"; 

    @RBComment("Multiple EPMReferenceLink objects with the same information found.  Package Import does not know which link object to update.")
    @RBEntry("Più oggetti EPMReferenceLink con le stesse informazioni. Impossibile identificare un link da aggiornare.")
    public static final String MULTIPLE_REFERENCE_LINKS_WITH_SAME_INFO_FOUND = "14"; 
}
