/* bcwti
 *
 * Copyright (c) 2011 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.ixb.epm;

import wt.util.resource.*;

@RBUUID("wt.ixb.epm.EPMResource")
public final class EPMResource extends WTListResourceBundle {

    @RBEntry("The generic of family table instance, \"{0}\" is missing.  Cannot import family table.")
    @RBComment("Error Message specific for Package Import.  EPMVariantLink can not be created because the generic master is not found on the target system.")
    @RBArgComment0("The identity of the Instance CAD document")
    public static final String PACKAGE_IMPORT_GENERIC_MASTER_NOT_FOUND = "1";   
    
    @RBEntry("The following object was not imported because the object type is not supported for received delivery import in your Windchill system: {0}")
    @RBComment("Error Message for Package Import.  The object is not getting imported.")
    @RBArgComment0("The display name of any EPM objects, EPMDocument, EPMSepFamilyTable, EPMMemberLink, etc.")    
    public static final String PACKAGE_IMPORT_IGNORE_OBJECT = "2";

    @RBEntry("The following objects were not imported because the object types are not supported for received delivery import in your Windchill system: {0}")
    @RBComment("Error Message for Package Import.  The objects are not getting imported.")
    @RBArgComment0("The identity of two or more EPM objects, EPMDocument, EPMSepFamilyTable, EPMMemberLink, etc.")    
    public static final String PACKAGE_IMPORT_IGNORE_OBJECT_PLURAL = "3";
    
    @RBEntry("Could not import source/image history relationship for the following CAD/Dynamic Document since its related object is not in the package or target system: {0}")
    @RBComment("If the other side EPMDoc does not exist, then skip importing the link and provide a warning in the log.")
    public static final String DERIVED_REP_RULE_NOT_IMPOTRTED = "4";
    
    @RBEntry("Could not import source/image history relationship, because both the source and the image are not in the package or target system: {0}") 
    @RBComment("If both source and image EPMDoc do not exist, then skip importing the link and provide a error in the log.")
    public static final String DERIVED_REP_RULE_NOT_IMPOTRTED_ERROR = "5";

    @RBEntry("'BuildHistory Link' relationship was not included in the package delivery. Both, source and target were missing.")  
    @RBComment("If both source and target does not exist, then skip importing the link and provide a error in the log.")
    public static final String HISTORY_IGNORED_BOTH_ENDS_MISSING = "6";
    
    @RBEntry("{0} had \"{1}\" relationship with another object that was not included in the package delivery. The relationship and missing object were ignored on import.") 
    @RBComment("If either source or target does not exist, then skip importing the history and provide a warning in the log.")
    public static final String HISTORY_IGNORED_MISSING_OTHER_END = "7";
    
    @RBEntry("'BuildRuleAssociation Link' relationship was not included in the package delivery. Both, source and target were missing.")  
    @RBComment("If both source and target does not exist, then skip importing the link and provide a error in the log.")
    public static final String ASSOCIATION_LINK_IGNORED_BOTH_ENDS_MISSING = "8";
    
    @RBEntry("{0} had \"{1}\" relationship with another object that was not included in the package delivery. The relationship and missing object were ignored on import.") 
    @RBComment("If either source or target does not exist, then skip importing the EPMBuildRuleAssociationLink and provide a warning in the log.")
    public static final String ASSOCIATION_LINK_IGNORED_MISSING_OTHER_END = "9";
    
    @RBComment("Build Rule Id is not found between source and target")
    @RBEntry("Build Rule Id is not found between {0} and {1}")
    public static final String BUILD_RULE_ID_NOT_FOUND = "10";
    
    @RBComment("Model Item Master not found.")
    @RBEntry("Model Item Master not found.")
    public static final String MODEL_ITEM_MASTER_NOT_FOUND = "11";
    
    @RBEntry("The following objects are ignored for the context creation of the {0} as the cad module, or its derived images in the requested authoring application were not found for {1}")    
    @RBComment("If the cad module, or its derived EPMDocument image is not found, then skip the creation/update the link and provide information to the log.")
    public static final String SKIP_NOT_FOUND_IMAGES_OR_MODULES = "12";    

    @RBComment("Multiple EPMDescribeLink objects with the same parent, child and built flag found.  Package Import does not know which link object to update.")
    @RBEntry("Multiple EPMDescribeLink objects with the same information found.  Unable to identify a link to update.")
    public static final String MULTIPLE_DESCRIBE_LINKS_WITH_SAME_INFO_FOUND = "13"; 

    @RBComment("Multiple EPMReferenceLink objects with the same information found.  Package Import does not know which link object to update.")
    @RBEntry("Multiple EPMReferenceLink objects with the same information found.  Unable to identify a link to update.")
    public static final String MULTIPLE_REFERENCE_LINKS_WITH_SAME_INFO_FOUND = "14"; 
}
