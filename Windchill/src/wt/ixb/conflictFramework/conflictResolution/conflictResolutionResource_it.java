/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.ixb.conflictFramework.conflictResolution;

import wt.util.resource.*;

@RBUUID("wt.ixb.conflictFramework.conflictResolution.conflictResolutionResource")
public final class conflictResolutionResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile trovare un risolutore per il tipo di risoluzione \"{0}\".")
   public static final String RESOLVER_NOT_FOUND = "0";

   @RBEntry("Risoluzione non valida. La risoluzione deve essere del tipo IXReferenceConflictResolution.")
   public static final String INVALID_RESOLUTION = "1";

   @RBEntry("Il programma di risoluzione non è in grado di gestire il tipo di risoluzione \"{0}\".")
   public static final String INVALID_RESOLUTION_FOR_RESOLVER = "2";

   @RBEntry("\r\n")
   @RBPseudo(false)
   @RBComment("Default list separator used when combining localized messages (for example, a list of EnumeratedTypes) into a string")
   public static final String DEFAULT_LIST_SEPARATOR = "3";
}
