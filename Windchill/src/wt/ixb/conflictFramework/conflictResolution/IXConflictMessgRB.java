/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.ixb.conflictFramework.conflictResolution;

import wt.util.resource.*;

@RBUUID("wt.ixb.conflictFramework.conflictResolution.IXConflictMessgRB")
public final class IXConflictMessgRB extends WTListResourceBundle {
   @RBEntry("Organization of Global Id \"{0}\" cannot be found for objects \"{1}\".")
   public static final String ORGANIZATION_NOT_FOUND = "0";

   @RBEntry("Folder \"{0}\" cannot be found or the user doesn't have access to the folder from Container \"{1}\" for objects \"{2}\".")
   public static final String FOLDER_NOT_FOUND = "1";

   @RBEntry("Cabinet \"{0}\" cannot be found or the user doesn't have access to the cabinet from Container \"{1}\" for objects \"{2}\".")
   public static final String CABINET_NOT_FOUND = "2";

   @RBEntry("Team \"{0}\" cannot be found in the Administrative Domain \"{1}\" for objects \"{2}\".")
   public static final String TEAM_NOT_FOUND = "3";

   @RBEntry("Project \"{0}\" cannot be found in the Administrative Domain \"{1}\" for objects \"{2}\".")
   public static final String PROJECT_NOT_FOUND = "4";

   @RBEntry("Measurement system \"{0}\" does not exist. ")
   public static final String MEASUREMENT_SYSTEM_NOT_FOUND = "5";

   @RBEntry("Quantity of measure \"{0}\" does not exist.")
   public static final String QUANTITY_OF_MEASURE_NOT_FOUND = "6";

   @RBEntry("The following Authoring Application Version is not definied in the system, \"{0} {1} {2}\" for objects \"{3}\". ")
   public static final String AUTHORING_APP_VERSION_NOT_FOUND = "7";

   @RBEntry("BusinessEntity cannot be found: \"{0}\". ")
   public static final String BUSINESS_ENTITY_NOT_FOUND = "8";

   @RBEntry("ClassificationNode cannot be found: \"{0}\".")
   public static final String CLASSIFICATION_NODE_NOT_FOUND = "9";
}
