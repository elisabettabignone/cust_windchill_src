/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.ixb.conflictFramework.conflictResolution;

import wt.util.resource.*;

@RBUUID("wt.ixb.conflictFramework.conflictResolution.IXConflictMessgRB")
public final class IXConflictMessgRB_it extends WTListResourceBundle {
   @RBEntry("Impossibile trovare l'organizzazione con id globale \"{0}\" per gli oggetti \"{1}\".")
   public static final String ORGANIZATION_NOT_FOUND = "0";

   @RBEntry("La cartella \"{0}\" non è stata trovata oppure l'utente non dispone dei diritti per accedervi dal contenitore \"{1}\" per gli oggetti \"{2}\".")
   public static final String FOLDER_NOT_FOUND = "1";

   @RBEntry("Lo schedario personale \"{0}\" non è stato trovato oppure l'utente non dispone dei diritti per accedervi dal contenitore \"{1}\" per gli oggetti \"{2}\".")
   public static final String CABINET_NOT_FOUND = "2";

   @RBEntry("Impossibile trovare il team \"{0}\" nel dominio amministrativo \"{1}\" per gli oggetti \"{2}\".")
   public static final String TEAM_NOT_FOUND = "3";

   @RBEntry("Impossibile trovare il progetto \"{0}\" nel dominio amministrativo \"{1}\" per gli oggetti \"{2}\".")
   public static final String PROJECT_NOT_FOUND = "4";

   @RBEntry("Il sistema di misura \"{0}\" non esiste.")
   public static final String MEASUREMENT_SYSTEM_NOT_FOUND = "5";

   @RBEntry("La quantità di misura \"{0}\" non esiste.")
   public static final String QUANTITY_OF_MEASURE_NOT_FOUND = "6";

   @RBEntry("Versione dell'applicazione di creazione \"{0} {1} {2}\" non definita nel sistema per gli oggetti \"{3}\". ")
   public static final String AUTHORING_APP_VERSION_NOT_FOUND = "7";

   @RBEntry("Impossibile trovare l'oggetto BussinessEntity: \"{0}\".")
   public static final String BUSINESS_ENTITY_NOT_FOUND = "8";

   @RBEntry("Impossibile trovare l'oggetto ClassificationNode: \"{0}\".")
   public static final String CLASSIFICATION_NODE_NOT_FOUND = "9";
}
