/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.ixb.conflictFramework.conflictResolution;

import wt.util.resource.*;

@RBUUID("wt.ixb.conflictFramework.conflictResolution.conflictResolutionResource")
public final class conflictResolutionResource extends WTListResourceBundle {
   @RBEntry("Resolver not found for resolution type \"{0}\".")
   public static final String RESOLVER_NOT_FOUND = "0";

   @RBEntry("Invalid Resolution. Resolution should be of type IXReferenceConflictResolution.")
   public static final String INVALID_RESOLUTION = "1";

   @RBEntry("The resolver cannot handle the resolution type \"{0}\".")
   public static final String INVALID_RESOLUTION_FOR_RESOLVER = "2";

   @RBEntry("\r\n")
   @RBPseudo(false)
   @RBComment("Default list separator used when combining localized messages (for example, a list of EnumeratedTypes) into a string")
   public static final String DEFAULT_LIST_SEPARATOR = "3";
}
