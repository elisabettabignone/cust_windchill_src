/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.ixb.actor;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBArgComment1;
import wt.util.resource.RBArgComment2;
import wt.util.resource.RBArgComment3;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.ixb.actor.actorResource")
public final class actorResource extends WTListResourceBundle {
   /**
    * actor names
    **/
   @RBEntry("Default ")
   public static final String PICKINGEXISTINGOBJECT_ACTOR_NAME = "PICKINGEXISTINGOBJECT_ACTOR_NAME";

   @RBEntry("Import as latest iteration")
   public static final String NEWITERATION_ACTOR_NAME = "NEWITERATION_ACTOR_NAME";

   @RBEntry("Import as new version")
   public static final String NEWVERSION_ACTOR_NAME = "NEWVERSION_ACTOR_NAME";

   @RBEntry("Import as checked out")
   public static final String CHECKOUT_ACTOR_NAME = "CHECKOUT_ACTOR_NAME";

   @RBEntry("Modify non-versioned attributes")
   public static final String IMPORT_NON_VERSIONED_ATTR_ACTOR_NAME = "IMPORT_NON_VERSIONED_ATTR_ACTOR_NAME";

   @RBEntry("Update checked out object in place")
   public static final String UPDATE_IN_PLACE_ACTOR_NAME = "UPDATE_IN_PLACE_ACTOR_NAME";

   @RBEntry("Update checked out object for xls")
   public static final String UPDATE_IN_PLACE_FOR_XLS_ACTOR_NAME = "UPDATE_IN_PLACE_FOR_XLS_ACTOR_NAME";

   @RBEntry("Unlock and Iterate Object")
   public static final String UNLOCK_AND_ITERATE_ACTOR_NAME = "UNLOCK_AND_ITERATE_ACTOR_NAME";

   @RBEntry("Create a new object with new identities")
   public static final String CREATE_NEW_OBJECT_ACTOR_NAME = "CREATE_NEW_OBJECT_ACTOR_NAME";

   @RBEntry("Substitute by an existing object")
   public static final String SUBSTITUTE_OBJECT_ACTOR_NAME = "SUBSTITUTE_OBJECT_ACTOR_NAME";

   @RBEntry("Ignore, do not import this object")
   public static final String IGNORE_ACTOR_NAME = "IGNORE_ACTOR_NAME";

   @RBEntry("Restore")
   public static final String RESTORE_ACTOR_NAME = "RESTORE_ACTOR_NAME";

   @RBEntry("Only update master object")
   public static final String UPDATE_MASTER_ONLY_ACTOR_NAME = "UPDATE_MASTER_ONLY_ACTOR_NAME";

   @RBEntry("Create new iteration when the object doesn't exist")
   public static final String NEW_ITERATION_WHEN_NOT_EXIST_ACTOR_NAME = "NEW_ITERATION_WHEN_NOT_EXIST_ACTOR_NAME";

   @RBEntry("Gateway Import")
   public static final String GATEWAY_IMPORT_ACTOR_NAME = "GATEWAY_IMPORT_ACTOR_NAME";

   @RBEntry("Import from FORAN")
   public static final String FORAN_IMPORT_ACTOR_NAME = "FORAN_IMPORT_ACTOR_NAME";

   @RBEntry("Import from Creo Elements/Direct Model Manager")
   public static final String COCREATE_IMPORT_ACTOR_NAME = "COCREATE_IMPORT_ACTOR_NAME";

   /**
    * OFFLINE_EDIT_ACTOR_NAME.constant=OFFLINE_EDIT_ACTOR_NAME
    * OFFLINE_EDIT_ACTOR_NAME.value=Offline Edit Import
    **/
   @RBEntry("Federation Preview")
   public static final String PREVIEW_ACTOR_NAME = "PREVIEW_ACTOR_NAME";

   /**
    * Actor factory messages
    **/
   @RBEntry("Unknown Import action: {0}")
   public static final String UNKNOWN_ACTION = "UNKNOWN_ACTION";

   /**
    * Actor names for export
    **/
   @RBEntry("None")
   public static final String EXPORT_NO_ACTION_NAME = "EXPORT_NO_ACTION_NAME";

   @RBEntry("System default")
   public static final String EXPORT_SYSTEM_ACTION_NAME = "EXPORT_SYSTEM_ACTION_NAME";

   @RBEntry("Check out")
   public static final String EXPORT_CHECKOUT_NAME = "EXPORT_CHECKOUT_NAME";

   @RBEntry("Lock objects on export")
   public static final String EXPORT_LOCK_NAME = "EXPORT_LOCK_NAME";

   @RBEntry("Export content as URL")
   public static final String EXPORT_CONTENT_AS_URL_NAME = "EXPORT_CONTENT_AS_URL_NAME";

   @RBEntry("Federation Export")
   public static final String FEDERATION_EXPORT_ACTOR_NAME = "FEDERATION_EXPORT_ACTOR_NAME";

   /**
    * End of Actor names for export
    * actor messages
    **/
   @RBEntry("Object {0} not found.")
   public static final String OBJECT_NOT_FOUND = "OBJECT_NOT_FOUND";

   @RBEntry("Cannot check the object {0} out.")
   public static final String CANNOT_CHECK_OUT_OBJECT = "CANNOT_CHECK_OUT_OBJECT";

   @RBEntry("Object {0} is not checked out.")
   public static final String OBJECT_NOT_CHECKED_OUT = "OBJECT_NOT_CHECKED_OUT";

   @RBEntry("Object {0} is not new object or checked out to workspace {1}.")
   public static final String OBJECT_NOT_NEW_OR_CHECKED_OUT = "OBJECT_NOT_NEW_OR_CHECKED_OUT";

   @RBEntry("Object {0} is not locked.")
   public static final String OBJECT_NOT_LOCKED = "OBJECT_NOT_LOCKED";

   @RBEntry("There is not enough information to substitute the object.You must provide number,name, version and iteration.")
   public static final String NOT_ENOUGH_INFORMATION = "NOT_ENOUGH_INFORMATION";

   @RBEntry("Object {0} exists in database. Cannot create object with the same identities.")
   public static final String OBJECT_EXISTS = "OBJECT_EXISTS";

   @RBEntry("Object {0} is already checked out.")
   public static final String OBJECT_IS_CHECKED_OUT = "OBJECT_IS_CHECKED_OUT";

   @RBEntry("Object {0} is already checked out by user {1}.")
   public static final String OBJECT_IS_CHECKED_OUT_BY_USER = "OBJECT_IS_CHECKED_OUT_BY_USER";

   @RBEntry("Object with UFID {0} not found.")
   public static final String OBJECT_NOT_FOUND_BY_UFID = "OBJECT_NOT_FOUND_BY_UFID";

   @RBEntry("ContextData for import is invalid.")
   public static final String CONTEXT_DATA_IS_INVALID = "CONTEXT_DATA_IS_INVALID";

   @RBEntry("Workspace for import is not provided.")
   public static final String WORKSPACE_NOT_PROVIDED = "WORKSPACE_NOT_PROVIDED";

   @RBEntry("Workspace for import does not belong to user {0}.")
   public static final String WORKSPACE_NOT_BELONG_TO_USER = "WORKSPACE_NOT_BELONG_TO_USER";

   @RBEntry("Assembly type or cad module type is configured incorrectly, please check.")
   public static final String TYPE_CONFIGURED_INCORRECTLY = "TYPE_CONFIGURED_INCORRECTLY";

   @RBEntry("Invalid type, please use Assembly type or cad module type.")
   public static final String INVALID_TYPE = "INVALID_TYPE";

   @RBEntry("Default for Non Iterated")
   public static final String PICKINGEXISTINGNONITERATEDOBJECT_ACTOR_NAME = "PICKINGEXISTINGNONITERATEDOBJECT_ACTOR_NAME";

   @RBEntry("Restore")
   public static final String RESTOREIMPORTINPLACE_ACTOR_NAME = "RESTOREIMPORTINPLACE_ACTOR_NAME";

   @RBEntry("\"{0}\": Action is not valid for import of View Version as no version exists. Import file information (object= \"{1}\", version= \"{2}\", level= \"{3}\").")
   @RBArgComment0("The action used for Import")
   @RBArgComment1("The object being imported.")
   @RBArgComment2("The version Label being imported.")
   @RBArgComment3("The version level being imported.")
   public static final String EXISTING_VERSION_NOT_FOUND_FOR_VIEW_VERSION_IMPORT = "EXISTING_VERSION_NOT_FOUND_FOR_VIEW_VERSION_IMPORT";

}
