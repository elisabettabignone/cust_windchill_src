/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.ixb.actor.actions;

import wt.util.resource.*;

@RBUUID("wt.ixb.actor.actions.actionsResource")
public final class actionsResource extends WTListResourceBundle {
   @RBEntry("Cannot apply selected action for object \"{0}\" since it is locked by another principal")
   @RBArgComment0("The object identifier")
   public static final String CANNOT_APPLY_ACTION_LOCKED_BY_OTHER_PRINCIPAL = "1";

   @RBEntry("Cannot apply selected action for object \"{0}\" since it is not locked")
   @RBArgComment0("The object identifier")
   public static final String CANNOT_APPLY_ACTION_IS_NOT_LOCKED = "2";

   @RBEntry("Cannot apply selected action for object \"{0}\" since it is not checked out by current principal")
   @RBArgComment0("The object identifier")
   public static final String CANNOT_APPLY_ACTION_IS_NOT_CHECKED_OUT = "3";

   @RBEntry("Cannot apply selected action for object \"{0}\" since it is already checked out")
   @RBArgComment0("The object identifier")
   public static final String CANNOT_APPLY_ACTION_IS_ALREADY_CHECKED_OUT = "4";

   @RBEntry("Cannot apply CHECKOUT action for EPMDocuments ")
   public static final String CANNOT_APPLY_CHECKOUT_ACTION_TO_EPM = "5";
}
