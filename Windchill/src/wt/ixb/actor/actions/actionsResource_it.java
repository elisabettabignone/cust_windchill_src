/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.ixb.actor.actions;

import wt.util.resource.*;

@RBUUID("wt.ixb.actor.actions.actionsResource")
public final class actionsResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile applicare l'azione selezionata all'oggetto \"{0}\" perché è bloccato da un altro utente/gruppo/ruolo")
   @RBArgComment0("The object identifier")
   public static final String CANNOT_APPLY_ACTION_LOCKED_BY_OTHER_PRINCIPAL = "1";

   @RBEntry("Impossibile applicare l'azione selezionata all'oggetto \"{0}\" perché l'oggetto non è bloccato")
   @RBArgComment0("The object identifier")
   public static final String CANNOT_APPLY_ACTION_IS_NOT_LOCKED = "2";

   @RBEntry("Impossibile applicare l'azione selezionata all'oggetto \"{0}\" perché l'oggetto non è stato sottoposto a Check-Out dall'utente/gruppo/ruolo")
   @RBArgComment0("The object identifier")
   public static final String CANNOT_APPLY_ACTION_IS_NOT_CHECKED_OUT = "3";

   @RBEntry("Impossibile applicare l'azione selezionata all'oggetto \"{0}\" perché l'oggetto è già stato sottoposto a Check-Out")
   @RBArgComment0("The object identifier")
   public static final String CANNOT_APPLY_ACTION_IS_ALREADY_CHECKED_OUT = "4";

   @RBEntry("Impossibile effettuare CHECK-OUT su Documenti EPM")
   public static final String CANNOT_APPLY_CHECKOUT_ACTION_TO_EPM = "5";
}
