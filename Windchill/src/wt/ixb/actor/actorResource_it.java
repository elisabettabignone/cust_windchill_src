/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.ixb.actor;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBArgComment1;
import wt.util.resource.RBArgComment2;
import wt.util.resource.RBArgComment3;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.ixb.actor.actorResource")
public final class actorResource_it extends WTListResourceBundle {
   /**
    * actor names
    **/
   @RBEntry("Default")
   public static final String PICKINGEXISTINGOBJECT_ACTOR_NAME = "PICKINGEXISTINGOBJECT_ACTOR_NAME";

   @RBEntry("Importa come ultima iterazione")
   public static final String NEWITERATION_ACTOR_NAME = "NEWITERATION_ACTOR_NAME";

   @RBEntry("Importa come nuova versione")
   public static final String NEWVERSION_ACTOR_NAME = "NEWVERSION_ACTOR_NAME";

   @RBEntry("Importa come oggetto sottoposto a Check-Out")
   public static final String CHECKOUT_ACTOR_NAME = "CHECKOUT_ACTOR_NAME";

   @RBEntry("Modifica gli attributi senza versione")
   public static final String IMPORT_NON_VERSIONED_ATTR_ACTOR_NAME = "IMPORT_NON_VERSIONED_ATTR_ACTOR_NAME";

   @RBEntry("Aggiorna l'oggetto sottoposto a Check-Out")
   public static final String UPDATE_IN_PLACE_ACTOR_NAME = "UPDATE_IN_PLACE_ACTOR_NAME";

   @RBEntry("Aggiorna l'oggetto sottoposto a Check-Out per xls")
   public static final String UPDATE_IN_PLACE_FOR_XLS_ACTOR_NAME = "UPDATE_IN_PLACE_FOR_XLS_ACTOR_NAME";

   @RBEntry("Sblocca e crea iterazione oggetto")
   public static final String UNLOCK_AND_ITERATE_ACTOR_NAME = "UNLOCK_AND_ITERATE_ACTOR_NAME";

   @RBEntry("Crea nuovo oggetto con nuovi identificativi")
   public static final String CREATE_NEW_OBJECT_ACTOR_NAME = "CREATE_NEW_OBJECT_ACTOR_NAME";

   @RBEntry("Sustituisci con oggetto esistente")
   public static final String SUBSTITUTE_OBJECT_ACTOR_NAME = "SUBSTITUTE_OBJECT_ACTOR_NAME";

   @RBEntry("Ignora, non importare l'oggetto")
   public static final String IGNORE_ACTOR_NAME = "IGNORE_ACTOR_NAME";

   @RBEntry("Ripristina")
   public static final String RESTORE_ACTOR_NAME = "RESTORE_ACTOR_NAME";

   @RBEntry("Aggiorna solo oggetto master")
   public static final String UPDATE_MASTER_ONLY_ACTOR_NAME = "UPDATE_MASTER_ONLY_ACTOR_NAME";

   @RBEntry("Crea nuova iterazione quando l'oggetto non esiste")
   public static final String NEW_ITERATION_WHEN_NOT_EXIST_ACTOR_NAME = "NEW_ITERATION_WHEN_NOT_EXIST_ACTOR_NAME";

   @RBEntry("Importazione gateway")
   public static final String GATEWAY_IMPORT_ACTOR_NAME = "GATEWAY_IMPORT_ACTOR_NAME";

   @RBEntry("Importa da FORAN")
   public static final String FORAN_IMPORT_ACTOR_NAME = "FORAN_IMPORT_ACTOR_NAME";

   @RBEntry("Importa da Creo Elements/Direct Model Manager")
   public static final String COCREATE_IMPORT_ACTOR_NAME = "COCREATE_IMPORT_ACTOR_NAME";

   /**
    * OFFLINE_EDIT_ACTOR_NAME.constant=OFFLINE_EDIT_ACTOR_NAME
    * OFFLINE_EDIT_ACTOR_NAME.value=Offline Edit Import
    **/
   @RBEntry("Anteprima ambiente federato")
   public static final String PREVIEW_ACTOR_NAME = "PREVIEW_ACTOR_NAME";

   /**
    * Actor factory messages
    **/
   @RBEntry("Azione di importazione sconosciuta: {0}")
   public static final String UNKNOWN_ACTION = "UNKNOWN_ACTION";

   /**
    * Actor names for export
    **/
   @RBEntry("Nessuno")
   public static final String EXPORT_NO_ACTION_NAME = "EXPORT_NO_ACTION_NAME";

   @RBEntry("Default di sistema")
   public static final String EXPORT_SYSTEM_ACTION_NAME = "EXPORT_SYSTEM_ACTION_NAME";

   @RBEntry("Check-Out")
   public static final String EXPORT_CHECKOUT_NAME = "EXPORT_CHECKOUT_NAME";

   @RBEntry("Blocca gli oggetti all'esportazione")
   public static final String EXPORT_LOCK_NAME = "EXPORT_LOCK_NAME";

   @RBEntry("Esporta contenuto come URL")
   public static final String EXPORT_CONTENT_AS_URL_NAME = "EXPORT_CONTENT_AS_URL_NAME";

   @RBEntry("Esportazione ambiente federato")
   public static final String FEDERATION_EXPORT_ACTOR_NAME = "FEDERATION_EXPORT_ACTOR_NAME";

   /**
    * End of Actor names for export
    * actor messages
    **/
   @RBEntry("Oggetto {0} non trovato.")
   public static final String OBJECT_NOT_FOUND = "OBJECT_NOT_FOUND";

   @RBEntry("Impossibile effettuare il Check-Out dell'oggetto {0}.")
   public static final String CANNOT_CHECK_OUT_OBJECT = "CANNOT_CHECK_OUT_OBJECT";

   @RBEntry("L'oggetto {0} non è stato sottoposto a Check-Out.")
   public static final String OBJECT_NOT_CHECKED_OUT = "OBJECT_NOT_CHECKED_OUT";

   @RBEntry("L'oggetto {0} non è nuovo o è sottoposto a Check-Out nel workspace {1}.")
   public static final String OBJECT_NOT_NEW_OR_CHECKED_OUT = "OBJECT_NOT_NEW_OR_CHECKED_OUT";

   @RBEntry("L'oggetto {0} non è bloccato.")
   public static final String OBJECT_NOT_LOCKED = "OBJECT_NOT_LOCKED";

   @RBEntry("Le informazioni non sono sufficienti per la sostituzione dell'oggetto. Indicare numero, nome, versione e iterazione.")
   public static final String NOT_ENOUGH_INFORMATION = "NOT_ENOUGH_INFORMATION";

   @RBEntry("L'oggetto {0} esiste già nel database. Impossibile creare un oggetto con lo stesso identificativo.")
   public static final String OBJECT_EXISTS = "OBJECT_EXISTS";

   @RBEntry("L'oggetto {0} è già stato sottoposto a Check-Out.")
   public static final String OBJECT_IS_CHECKED_OUT = "OBJECT_IS_CHECKED_OUT";

   @RBEntry("L'oggetto {0} è già stato sottoposto a Check-Out dall'utente {1}.")
   public static final String OBJECT_IS_CHECKED_OUT_BY_USER = "OBJECT_IS_CHECKED_OUT_BY_USER";

   @RBEntry("Impossibile trovare l'oggetto con UFID {0}.")
   public static final String OBJECT_NOT_FOUND_BY_UFID = "OBJECT_NOT_FOUND_BY_UFID";

   @RBEntry("Dati di contesto per l'importazione non validi.")
   public static final String CONTEXT_DATA_IS_INVALID = "CONTEXT_DATA_IS_INVALID";

   @RBEntry("Workspace per l'importazione non specificato.")
   public static final String WORKSPACE_NOT_PROVIDED = "WORKSPACE_NOT_PROVIDED";

   @RBEntry("Il workspace per l'importazione non appartiene all'utente {0}.")
   public static final String WORKSPACE_NOT_BELONG_TO_USER = "WORKSPACE_NOT_BELONG_TO_USER";

   @RBEntry("Verificare che il tipo di assieme o il tipo di modulo CAD siano configurati correttamente.")
   public static final String TYPE_CONFIGURED_INCORRECTLY = "TYPE_CONFIGURED_INCORRECTLY";

   @RBEntry("Tipo non valido. Utilizzare il tipo di assieme o il tipo di modulo CAD.")
   public static final String INVALID_TYPE = "INVALID_TYPE";

   @RBEntry("Default per non iterato")
   public static final String PICKINGEXISTINGNONITERATEDOBJECT_ACTOR_NAME = "PICKINGEXISTINGNONITERATEDOBJECT_ACTOR_NAME";

   @RBEntry("Ripristina")
   public static final String RESTOREIMPORTINPLACE_ACTOR_NAME = "RESTOREIMPORTINPLACE_ACTOR_NAME";

   @RBEntry("\"{0}\": l'azione non è valida per l'importazione della versione vista, dal momento che non ne esiste una. Informazioni sul file di importazione: oggetto = \"{1}\", versione = \"{2}\", livello = \"{3}\".")
   @RBArgComment0("The action used for Import")
   @RBArgComment1("The object being imported.")
   @RBArgComment2("The version Label being imported.")
   @RBArgComment3("The version level being imported.")
   public static final String EXISTING_VERSION_NOT_FOUND_FOR_VIEW_VERSION_IMPORT = "EXISTING_VERSION_NOT_FOUND_FOR_VIEW_VERSION_IMPORT";

}
