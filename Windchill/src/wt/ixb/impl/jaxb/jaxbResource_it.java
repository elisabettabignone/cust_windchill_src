/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.ixb.impl.jaxb;

import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.ixb.impl.jaxb.jaxbResource")
public final class jaxbResource_it extends WTListResourceBundle {
    @RBEntry("Il nome del tag non può essere nullo.")
    public static final String TAG_NAME_IS_NULL = "TAG_NAME_IS_NULL";

    @RBEntry("Il metodo non deve essere chiamato.")
    public static final String METHOD_SHOULD_NOT_BE_CALLED = "METHOD_SHOULD_NOT_BE_CALLED";

    @RBEntry("Impossibile eseguire la ricerca della classe per {0} e {1}.")
    public static final String JAXB_CLASS_CAN_NOT_BE_FOUND = "JAXB_CLASS_CAN_NOT_BE_FOUND";

    @RBEntry("Impossibile determinare il nome del tag per {0}.")
    public static final String TAG_NAME_CAN_NOT_BE_DETERMINATED = "TAG_NAME_CAN_NOT_BE_DETERMINATED";

    @RBEntry("Impossibile trovare un tag adatto per {0}.")
    public static final String CAN_NOT_FIND_SUITABLE_TAG = "CAN_NOT_FIND_SUITABLE_TAG";

    @RBEntry("È stato trovato più di un nome dello stesso tipo di classe: {0} {1}.")
    public static final String MORE_THAN_ONE_TAG_ARE_FOUND = "MORE_THAN_ONE_TAG_ARE_FOUND";

    @RBEntry("Impossibile eseguire la ricerca del nome del package per tipo {0}.")
    public static final String PACKAGE_CAN_NOT_BE_FOUND = "PACKAGE_CAN_NOT_BE_FOUND";

    @RBEntry("Errore nell'oggetto jaxb: {0}.")
    public static final String INVALID_JAXB_OBJECT = "INVALID_JAXB_OBJECT";

    @RBEntry("Impossibile trovare il metodo previsto {0} per il tipo {1}.")
    public static final String EXPECTED_METHOD_CAN_NOT_BE_FOUND = "EXPECTED_METHOD_CAN_NOT_BE_FOUND";

    @RBEntry("Il risultato è un elenco la cui dimensione è maggiore di zero {0}.")
    public static final String RESULT_IS_A_LIST = "RESULT_IS_A_LIST";

    @RBEntry("Aggiunta impossibile per {0}.")
    public static final String ADD_OPERATION_CAN_NOT_BE_PERFORMED = "ADD_OPERATION_CAN_NOT_BE_PERFORMED";

    @RBEntry("L'elenco contiene più di un elemento. Impossibile determinare l'elemento da scegliere {0}.")
    public static final String LIST_CONTAINS_MORE_THAN_ONE_ELEMENTS = "LIST_CONTAINS_MORE_THAN_ONE_ELEMENTS";

    @RBEntry("Impossibile trovare l'oggetto con ID {0} in {1}.")
    public static final String CAN_NOT_FIND_OBJECT_BY_ID = "CAN_NOT_FIND_OBJECT_BY_ID";

    @RBEntry("Impossibile trovare l'oggetto con ID master {0} in {1}.")
    public static final String CAN_NOT_FIND_OBJECT_BY_MASTER_ID = "CAN_NOT_FIND_OBJECT_BY_MASTER_ID";

    @RBEntry("Formato scambio di dati {0} non supportato")
    public static final String UNSUPPORTED_FORMAT = "UNSUPPORTED_FORMAT";

    @RBEntry("Convalida del codice xml non riuscita. Sono presenti errori alla riga {0}. Di seguito il messaggio di errore dettagliato:\n {1}")
    public static final String XML_VALIDATION_ERROR = "XML_VALIDATION_ERROR";

    @RBEntry("Impossibile eseguire la ricerca del link per {0} poiché non è possibile trovare l'oggetto di riferimento denominato {1}.")
    public static final String CAN_NOT_FIND_LINK_OBJECT = "CAN_NOT_FIND_LINK_OBJECT";

    @RBEntry("Impossibile eseguire la ricerca dell'oggetto per {0}.")
    public static final String CAN_NOT_FIND_OBJECT = "CAN_NOT_FIND_OBJECT";

    @RBEntry("Impossibile trovare il tag windchill con tipo {0} e oggetto {1}.")
    public static final String CAN_NOT_FIND_WINDCHILL_TAG_BY_OBJECT = "CAN_NOT_FIND_WINDCHILL_TAG_BY_OBJECT";

    @RBEntry("File di mappatura non valido. Messaggio di errore dettagliato:\n {0}")
    public static final String MAPPING_FILE_IN_VALID = "MAPPING_FILE_IN_VALID";

    @RBEntry("Nessun elemento valido esportato.")
    public static final String NO_JAXB_ELEMENTS_EXPORTED = "NO_JAXB_ELEMENTS_EXPORTED";


}
