/* bcwti
 * Copyright (c) 2013 Parametric Technology Corporation (PTC). All Rights
 * Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.ixb.impl.jaxb;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

/**
 * Localized message specific to multi format document
 *
 * <BR><BR><B>Supported API: </B>false
 * <BR><BR><B>Extendable: </B>false
 *
 * @author spatnaik
 */
@RBUUID("wt.ixb.impl.jaxb.MultiFormatResource")
public final class MultiFormatResource_it extends WTListResourceBundle {

    /*
     @RBEntry("")
     @RBComment("")
     public static final String X = "X";
     */
    @RBEntry("Impossibile trovare la dichiarazione dello schema jaxb con tipo {0}")
    public static final String JAXB_SCHEMA_DECLARATION_CAN_NOT_BE_FOUND = "JAXB_SCHEMA_DECLARATION_CAN_NOT_BE_FOUND";

    @RBEntry("Impossibile trovare il package jaxb con tipo {0}")
    public static final String JAXB_PACKAGE_CAN_NOT_BE_FOUND = "JAXB_PACKAGE_CAN_NOT_BE_FOUND";

    @RBEntry("Impossibile trovare lo schema jaxb con tipo {0}")
    public static final String JAXB_SCHEMA_CAN_NOT_BE_FOUND = "JAXB_SCHEMA_CAN_NOT_BE_FOUND";

    @RBEntry("File di mappatura mancante per il formato di scambio dati {0}")
    public static final String MAPPING_FILE_CAN_NOT_BE_FOUND = "MAPPING_FILE_CAN_NOT_BE_FOUND";

    @RBEntry("File di mappatura di importazione mancante per il formato di scambio dati {0}")
    public static final String IMPORT_MAPPING_FILE_NOT_FOUND = "IMPORT_MAPPING_FILE_NOT_FOUND";

    @RBEntry("Impossibile trovare il nome del file di esportazione/importazione con tipo {0}.")
    public static final String EXPORT_FILE_NAME_NOT_FOUND = "EXPORT_FILE_NAME_NOT_FOUND";

    @RBEntry("File di mappatura di esportazione mancante per il formato di scambio dati {0}")
    public static final String EXPORT_MAPPING_FILE_NOT_FOUND = "EXPORT_MAPPING_FILE_NOT_FOUND";

    @RBEntry("Impossibile trovare le informazioni di mappatura per il formato di scambio dati {0}")
    @RBComment("Error message thrown when Export/Import Mapping Schema information is missing.")
    @RBArgComment0("Non translatable format type")
    public static final String NO_MAPPING_SCHEMA_INFORMATION = "NO_MAPPING_SCHEMA_INFORMATION";

    @RBEntry("Impossibile trovare le informazioni sullo schema di protocollo applicativo per il formato di scambio dati {0}")
    @RBComment("Error message thrown when Application Protocol Schema information is missing")
    @RBArgComment0("Format Type for which information is missing")
    public static final String NO_AP_SCHEMA_INFORMATION = "NO_AP_SCHEMA_INFORMATION";

    @RBEntry("Il formato di scambio dati {0} non contiene informazioni di configurazione")
    @RBComment("Display message when somebody asks for configuration for a format which does not exist")
    @RBArgComment0("will contain non translatable strings like PLM_FORMAT or STEP_FORMAT or PLCS_FORMAT")
    public static final String CONFIGURATION_INFORMATION_NOT_FOUND = "CONFIGURATION_INFORMATION_NOT_FOUND";

    @RBEntry("Il formato di scambio dati {0} non contiene alcuna informazione sulla specifica")
    @RBComment("Display message when somebody asks for specification for a format which does not exist")
    @RBArgComment0("will contain non translatable strings like PLM_FORMAT or STEP_FORMAT or PLCS_FORMAT")
    public static final String SPECIFICATION_INFORMATION_NOT_FOUND = "SPECIFICATION_INFORMATION_NOT_FOUND";

    @RBEntry("Impossibile trovare le informazioni di configurazione specifiche dell'esportazione per il formato di scambio dati {0}")
    @RBComment("Display message when export specific configuration does not exist")
    @RBArgComment0("will contain non translatable strings like PLM_FORMAT or STEP_FORMAT or PLCS_FORMAT")
    public static final String EXPORT_CONFIGURATION_NOT_FOUND = "EXPORT_CONFIGURATION_NOT_FOUND";

    @RBEntry("Più protocolli di default trovati per il formato di scambio dati {0}")
    @RBComment("Display this message when a format contains multiple default protocols")
    @RBArgComment0("will contain non translatable strings like PLM_FORMAT or STEP_FORMAT or PLCS_FORMAT")
    public static final String MULTIPLE_DEFAULT_PROTOCOLS_FOUND = "MULTIPLE_DEFAULT_PROTOCOLS_FOUND";

    @RBEntry("Più protocolli con lo stesso identificativo trovati per il formato di scambio dati {0}")
    @RBComment("Message to be displayed when mulitple protocols exist with same name")
    @RBArgComment0("will contain non translatable strings like PLM_FORMAT or STEP_FORMAT or PLCS_FORMAT")
    public static final String MULTIPLE_COMMON_PROTOCOLS_FOUND = "MULTIPLE_COMMON_PROTOCOLS_FOUND";

    @RBEntry("Più voci trovate per il formato di scambio dati {0}")
    @RBComment("Message to be displayed when mulitple formats exist with same name")
    @RBArgComment0("will contain non translatable strings like PLM_FORMAT or STEP_FORMAT or PLCS_FORMAT")
    public static final String DUPLICATE_FORMATS_FOUND = "DUPLICATE_FORMATS_FOUND";

    @RBEntry("Errore durante l'inizializzazione del documento multiformato")
    @RBComment("Message to be displayed when initialization related failures occur in multiformat document")
    public static final String MULTIFORMAT_DOCUMENT_INITIALIZATION_FAILURE = "MULTIFORMAT_DOCUMENT_INITIALIZATION_FAILURE";

    @RBEntry("Protocollo di default non trovato per il formato di scambio dati {0}")
    @RBComment("Message to be displayed when default protocol does not exists")
    @RBArgComment0("will contain non translatable strings like PLM_FORMAT or STEP_FORMAT or PLCS_FORMAT")
    public static final String DEFAULT_PROTOCOL_NOT_FOUND = "DEFAULT_PROTOCOL_NOT_FOUND";
}
