/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.ixb.impl.jaxb;

import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.ixb.impl.jaxb.jaxbResource")
public final class jaxbResource extends WTListResourceBundle {
    @RBEntry("The name of tag can not be null.")
    public static final String TAG_NAME_IS_NULL = "TAG_NAME_IS_NULL";

    @RBEntry("This method should not be called.")
    public static final String METHOD_SHOULD_NOT_BE_CALLED = "METHOD_SHOULD_NOT_BE_CALLED";

    @RBEntry("Can not find class by {0} and {1}.")
    public static final String JAXB_CLASS_CAN_NOT_BE_FOUND = "JAXB_CLASS_CAN_NOT_BE_FOUND";

    @RBEntry("Can not determine tag name for {0}.")
    public static final String TAG_NAME_CAN_NOT_BE_DETERMINATED = "TAG_NAME_CAN_NOT_BE_DETERMINATED";

    @RBEntry("Can not find suitable tag for {0}.")
    public static final String CAN_NOT_FIND_SUITABLE_TAG = "CAN_NOT_FIND_SUITABLE_TAG";

    @RBEntry("Found more than one name of same class type: {0} {1}.")
    public static final String MORE_THAN_ONE_TAG_ARE_FOUND = "MORE_THAN_ONE_TAG_ARE_FOUND";

    @RBEntry("Can not find package name by type {0}.")
    public static final String PACKAGE_CAN_NOT_BE_FOUND = "PACKAGE_CAN_NOT_BE_FOUND";

    @RBEntry("Error jaxb object : {0}.")
    public static final String INVALID_JAXB_OBJECT = "INVALID_JAXB_OBJECT";

    @RBEntry("Expected method {0} can not be found for type {1}.")
    public static final String EXPECTED_METHOD_CAN_NOT_BE_FOUND = "EXPECTED_METHOD_CAN_NOT_BE_FOUND";

    @RBEntry("Result is a list and size is more than zero {0}.")
    public static final String RESULT_IS_A_LIST = "RESULT_IS_A_LIST";

    @RBEntry("Can not perform add operation for {0}.")
    public static final String ADD_OPERATION_CAN_NOT_BE_PERFORMED = "ADD_OPERATION_CAN_NOT_BE_PERFORMED";

    @RBEntry("List cantains more than one elements, do not know which one to choose {0}.")
    public static final String LIST_CONTAINS_MORE_THAN_ONE_ELEMENTS = "LIST_CONTAINS_MORE_THAN_ONE_ELEMENTS";

    @RBEntry("Can not find Id by {0} in {1}.")
    public static final String CAN_NOT_FIND_OBJECT_BY_ID = "CAN_NOT_FIND_OBJECT_BY_ID";

    @RBEntry("Can not find MasterId by {0} in {1}.")
    public static final String CAN_NOT_FIND_OBJECT_BY_MASTER_ID = "CAN_NOT_FIND_OBJECT_BY_MASTER_ID";

    @RBEntry("Unsupported data exchange format {0}")
    public static final String UNSUPPORTED_FORMAT = "UNSUPPORTED_FORMAT";

    @RBEntry("Validation of xml failed, there are errors in line {0} , the detailed error message is :\n {1}")
    public static final String XML_VALIDATION_ERROR = "XML_VALIDATION_ERROR";

    @RBEntry("Can not find link object by {0} becasue reference object with name {1} is not found.")
    public static final String CAN_NOT_FIND_LINK_OBJECT = "CAN_NOT_FIND_LINK_OBJECT";

    @RBEntry("Can not find object by {0}.")
    public static final String CAN_NOT_FIND_OBJECT = "CAN_NOT_FIND_OBJECT";

    @RBEntry("Can not find windchill tag with type {0} and object {1}.")
    public static final String CAN_NOT_FIND_WINDCHILL_TAG_BY_OBJECT = "CAN_NOT_FIND_WINDCHILL_TAG_BY_OBJECT";

    @RBEntry("Mapping file is invalid, detailed error message is :\n {0}")
    public static final String MAPPING_FILE_IN_VALID = "MAPPING_FILE_IN_VALID";

    @RBEntry("There are no valid elements exported.")
    public static final String NO_JAXB_ELEMENTS_EXPORTED = "NO_JAXB_ELEMENTS_EXPORTED";


}
