/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.ixb.clientAccess;

import wt.util.resource.*;

@RBUUID("wt.ixb.clientAccess.clientAccessResource")
public final class clientAccessResource extends WTListResourceBundle {
	@RBEntry("The parameter clientFileName is required and can not be null.")
	public static final String CLIENT_FILE_NAME_IS_REQUIRED = "CLIENT_FILE_NAME_IS_REQUIRED";

	@RBEntry("Can't extract file names from zip/jar file.")
	public static final String CANT_EXTRACT_FILE_NAMES = "CANT_EXTRACT_FILE_NAMES";

	@RBEntry("Cannot set both generators and collection for export operation")
	public static final String EITHER_NAVIGATION_OR_COLLECTION = "EITHER_NAVIGATION_OR_COLLECTION";

	@RBEntry("Cannot set both ExportImportFormatType and IXFormatType object for export/import operation")
	public static final String EITHER_FORMAT_TYPE_OR_FORMAT_TYPE_OBJECT = "EITHER_FORMAT_TYPE_OR_FORMAT_TYPE_OBJECT";
}
