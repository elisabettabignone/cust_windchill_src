/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.ixb.clientAccess;

import wt.util.resource.*;

@RBUUID("wt.ixb.clientAccess.clientAccessResource")
public final class clientAccessResource_it extends WTListResourceBundle {
	@RBEntry("Il parametro clientFileName è obbligatorio e non può essere nullo.")
	public static final String CLIENT_FILE_NAME_IS_REQUIRED = "CLIENT_FILE_NAME_IS_REQUIRED";

	@RBEntry("Impossibile estrarre i nomi di file dal file zip/jar.")
	public static final String CANT_EXTRACT_FILE_NAMES = "CANT_EXTRACT_FILE_NAMES";

	@RBEntry("Impossibile impostare generatori e raccolta per l'operazione di esportazione")
	public static final String EITHER_NAVIGATION_OR_COLLECTION = "EITHER_NAVIGATION_OR_COLLECTION";

	@RBEntry("Impossibile impostare sia l'oggetto ExportImportFormatType che l'oggetto IXFormatType per l'operazione di esportazione o importazione")
	public static final String EITHER_FORMAT_TYPE_OR_FORMAT_TYPE_OBJECT = "EITHER_FORMAT_TYPE_OR_FORMAT_TYPE_OBJECT";
}
