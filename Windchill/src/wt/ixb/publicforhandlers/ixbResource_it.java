/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.ixb.publicforhandlers;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBArgComment1;
import wt.util.resource.RBArgComment2;
import wt.util.resource.RBArgComment3;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBPseudo;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.ixb.publicforhandlers.ixbResource")
public final class ixbResource_it extends WTListResourceBundle {
    /*
     * -*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-
     * WARNING:
     *     PLEASE AVOID MULTI-LINE MESSAGES in resource file as it causes issues while localization/translation!
     * -*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-
     */
    @RBEntry("L'attributo \"{0}\" non è definito.")
    public static final String IBA_DEFINITION_NOT_FOUND = "0";

    @RBEntry("Impossibile creare oggetto attributo d'istanza \"{0}\"")
    public static final String IBA_COULD_NOT_CREATE = "1";

    @RBEntry("Impossibile recuperare il campo \"{0}\" mentre si esporta l'oggetto \"{1}\"")
    public static final String IBA_COULD_NOT_RETRIEVE_FIELD = "2";

    @RBEntry("Impossibile replicare la cartella: il dominio \"{0}\" non esiste")
    public static final String DOMAIN_DOES_NOT_EXIST = "3";

    @RBEntry("Eccezione durante il tentativo di personalizzare le informazioni di replica tramite l'invocazione del metodo \"{0}\" per la classe \"{1}\"; i parametri effettivi sono: (\"{2}\" )")
    public static final String ERROR_IN_CUSTOM_METHOD_INVOCATION = "4";

    @RBEntry("La classe \"{0}\" non supporta l'interfaccia wt.ixb.handlers.ClassExporter")
    public static final String EXPORTER_CLASS_DOES_NOT_SUPPORT_INTERFACE_CLASSEXPORTER = "5";

    @RBEntry("La classe \"{0}\" non supporta l'interfaccia wt.ixb.handlers.ElementImporter")
    public static final String IMPORTER_CLASS_DOES_NOT_SUPPORT_INTERFACE_ELEMENTIMPORTER = "6";

    @RBEntry("La classe \"{0}\" non supporta l'interfaccia wt.ixb.publicforhandlers.AttributeExporterImporter")
    public static final String CLASS_DOES_NOT_SUPPORT_INTERFACE_ATTRIBUTE_EXPORTER_IMPORTER = "CLASS_DOES_NOT_SUPPORT_INTERFACE_ATTRIBUTE_EXPORTER_IMPORTER";

    @RBEntry("La classe \"{0}\" non supporta l'interfaccia wt.ixb.handlers.ClassExporterImporter")
    public static final String EXPORTER_IMPORTER_CLASS_DOES_NOT_SUPPORT_INTERFACE = "7";

    @RBEntry("Si è verificato un problema durante il processo di esportazione/importazione.")
    public static final String ERROR_IN_IXB = "8";

    @RBEntry("Eccezione: \"{0}\"")
    public static final String EXCEPTION = "9";

    @RBEntry("Impossibile esportare un elemento sconosciuto: \"{0}\"")
    public static final String UNKNOWN_CONTENT_ITEM_CLASS_IGNORED = "10";

    @RBEntry("Comando sconosciuto di regola di mappatura: \"{0}\"")
    public static final String UNKNOWN_MAPPING_RULE_COMMAND = "11";

    @RBEntry("Impossibile aprire il file di registro di importazione/esportazione necessario.")
    public static final String CAN_NOT_OPEN_IXB_HANDLERS_REGISTRY_FILE = "12";

    @RBEntry("Impossibile aprire il file di registro dei gestori dell'attributo di esportazione di più oggetti.")
    public static final String CAN_NOT_OPEN_IXB_MULTI_OBJ_ATTR_HANDLERS_REGISTRY_FILE = "CAN_NOT_OPEN_IXB_MULTI_OBJ_ATTR_HANDLERS_REGISTRY_FILE";

    @RBEntry("Impossibile aprire il file di installazione del servizio di importazione/esportazione \"{0}\".")
    public static final String CAN_NOT_OPEN_IXB_SETUP_FILE = "13";

    @RBEntry("Impossibile aprire file registro gestori.")
    public static final String CAN_NOT_OPEN_IXB_SET_HANDLERS_REGISTRY_FILE = "14";

    @RBEntry("Impossibile aprire il file DTD \"{0}\".")
    public static final String CAN_NOT_OPEN_DTD_FILE = "15";

    @RBEntry("{0}")
    @RBPseudo(false)
    public static final String EMPTY_STRING_WITH_PARAMETER = "16";

    @RBEntry("Problema verificatosi durante il parsing di xml (riga {0}, colonna {1}): {2}")
    public static final String XML_PARSE_ERROR = "17";

    @RBEntry("Errore nel file XML di input")
    public static final String ERROR_WHILE_PARSING_XML = "18";

    @RBEntry("InputStream nullo trasmesso a parser XML")
    public static final String XML_PARSER_NULL_INPUT_STREAM = "19";

    @RBEntry("InputStream vuoto trasmesso a parser XML")
    public static final String XML_PARSER_EMPTY_INPUT_STREAM = "20";

    @RBEntry("InputStream non valido trasmesso a parser XML: è formato da spazi ")
    public static final String XML_PARSER_BAD_INPUT_STREAM_WHITE_SPACE = "21";

    @RBEntry("InputStream invalido trasmesso a parser XML: non inizia con <?xml version=\"1.0\" ?>")
    public static final String XML_PARSER_BAD_INPUT_STREAM_NO_XML_VERSION = "22";

    @RBEntry("InputStream non valido trasmesso a parser XML: non ha ?> nella prima riga")
    public static final String XML_PARSER_BAD_INPUT_STREAM_HEAD_NOT_CLOSED = "23";

    @RBEntry("Esportazione oggetto in corso: {0}...")
    public static final String EXPORT_OBJECT = "24";

    @RBEntry("Avvertenza: non esiste alcun gestore di esportazione per la classe {0}; oggetto: {1}")
    public static final String EXPORT_OBJECT_NO_HANDLER = "25";

    @RBEntry("Il processo di esportazione ha elaborato {0} oggetti")
    public static final String EXPORT_OBJECT_ELABORATED_COUNT = "26";

    @RBEntry("È stata avviata la navigazione per la selezione degli oggetti da esportare...")
    public static final String EXPORT_OBJECT_SET_NAVIGATE_START = "27";

    @RBEntry("Navigazione: elabora oggetto: {0}")
    public static final String EXPORT_OBJECT_SET_NAVIGATE_ELABORATE = "28";

    @RBEntry("Selezionare {0} oggetti candidati all'esportazione (il numero di oggetti effettivamente esportati può essere inferiore)")
    public static final String EXPORT_OBJECT_SET_NAVIGATE_FOUND_N_OBJECTS = "29";

    @RBEntry("L'esportazione degli oggetti selezionati è stata avviata...")
    public static final String EXPORT_START_XMLIZE_OBJECT = "30";

    @RBEntry("L'invio dei risultati al client è stato iniziato...")
    public static final String EXPORT_START_SENDING_RESULT = "31";

    @RBEntry("Il processo di esportazione ha esportato {0} oggetti")
    public static final String EXPORT_OBJECT_REALLY_EXPORTED_COUNT = "32";

    @RBEntry("Numero di documenti XML da importare: {0}")
    public static final String IMPORT_OBJECT_START_N_XML_FILES = "41";

    @RBEntry("Il processo di importazione è stato completato")
    public static final String IMPORT_OBJECT_FINISH = "42";

    @RBEntry("Fornito per i documenti XML {0} di importazione")
    public static final String IMPORT_OBJECT_GOT_N_XML_FILES = "43";

    /**
     * #34.value=Amount of XML document supplied for import: {0}
     * #34.constant=IMPORT_OBJECT_GOT_AMOUNT_XML_FILES
     **/
    @RBEntry("Il processo di verifica dei conflitti è stato avviato...")
    public static final String IMPORT_OBJECT_START_CONFLICT_CHECK = "45";

    @RBEntry("Conflitti verificati per i file {0} XML")
    public static final String IMPORT_OBJECT_CONFLICT_CHECK_N_XML_FILES = "46";

    @RBEntry("Il processo di creazione degli oggetti è stato avviato...")
    public static final String IMPORT_OBJECT_START_REAL_IMPORT = "47";

    @RBEntry("Elaborati {0} file XML nel processo di creazione dell'oggetto")
    public static final String IMPORT_OBJECT_REAL_IMPORT_N_XML_FILES = "48";

    @RBEntry("Sono stati creati {0} nuovi oggetti, rappresentanti oggetti provenienti da file XML")
    public static final String IMPORT_CREATED_NEW_OBJECTS = "49";

    @RBEntry("Sono stati trovati {0} oggetti esistenti, rappresentanti oggetti provenienti da file XML")
    public static final String IMPORT_FOUND_OLD_OBJECTS = "50";

    @RBEntry("Verifica dei conflitti per il file XML in corso: {0}")
    public static final String IMPORT_CHECK_CONFLICT_FOR_DOC = "51";

    @RBEntry("Gestore di importazione mancante per il tag {0} nella DTD {1}")
    public static final String IMPORT_NO_HANDLER = "52";

    @RBEntry("Importazione di informazioni dal file XML: {0}. Azione eseguita : {1}")
    public static final String IMPORT_REAL_IMPORT_FOR_DOC = "53";

    @RBEntry("Nuovo oggetto ceato: {0}")
    public static final String IMPORT_OBJECT_IS_NEW = "54";

    @RBEntry("L'oggetto nuovo non è stato creato perché è stato creato un oggetto esistente corrispondente: {0}")
    public static final String IMPORT_OBJECT_IS_OLD = "55";

    @RBEntry("Errore: impossibile navigare nella struttura di prodotto perché non esiste una specifica di configurazione WTPart associata all'utente attuale")
    public static final String NO_WTPART_CONFIG_SPEC_WHILE_PRODUCT_STRUCTURE_NAVIGATION = "70";

    @RBEntry("Anteprima di esportazione - elenco degli oggetti che saranno esportati")
    public static final String LIST_OF_OBJECT_TO_BE_EXPORTED = "71";

    @RBEntry("{0} oggetti saranno esportati")
    public static final String THERE_ARE_N_OBJECTS_TO_BE_EXPORTED = "72";

    @RBEntry("   {0}")
    public static final String PARAMETER_WITH_SHIFT = "73";

    @RBEntry("Impossibile trovare il documento referenziato")
    public static final String REFERENCED_DOC_NOT_FOUND = "74";

    @RBEntry("Impossibile trovare il documento DescribedBy")
    public static final String DESCRIBEDBY_DOC_NOT_FOUND = "75";

    @RBEntry("Impossibile trovare la parte usata nella struttura di prodotto")
    public static final String USED_PART_NOT_FOUND = "76";

    @RBEntry("Impossibile trovare l'oggetto con id: {0}")
    @RBArgComment0("object identifier to denote the object; e.g. wt.part.WTPart: 23456")
    public static final String OBJ_NOT_FOUND = "77";

    @RBEntry("Attributo mancante nel file xml.\nVerificare tag <{0}>")
    @RBComment("the '\n' provides a new-line in the message")
    @RBArgComment0("Tag name of the attribute to be verified inside the xml file\"")
    public static final String ATTRIBUTE_NOT_FOUND_XML = "78";

    @RBEntry("Attributi di versione o iterazione mancanti nel file xml.\nVerificare i tag <versionId>, <iterationId>, <versionLevel> e <series>.")
    @RBComment("the '\n' provides a new-line in the message")
    public static final String VERSION_ITERATION_NOT_FOUND_XML = "79";

    @RBEntry("Attributi di stato o di nome del ciclo di vita mancanti nel file xml.\nVerificare i tag <lifecycleTemplateName> e <lifecycleState>.")
    @RBComment("the '\n' provides a new-line in the message")
    public static final String LIFECYCLE_NOT_FOUND_XML = "80";

    @RBEntry("Impossibile creare e salvare l'oggetto AbstractAttributeDefinition (tipo: {0}) con nome: {1}, perché esiste con il tipo: {2}")
    @RBArgComment0("name of the type of the AbstractAttributeDefinition object, e.g. wt.iba.definition.StringDefinition")
    @RBArgComment1("name of the AbstractAttributeDefinition object")
    @RBArgComment2("name of the type of the AbstractAttributeDefinition object, e.g. wt.iba.definition.StringDefinition")
    public static final String IBA_DEFINITION_EXIST_WITH_NAME = "81";

    @RBEntry("Impossibile trovare il documento EPM.")
    public static final String EPM_DOCUMENT_NOT_FOUND = "82";

    @RBEntry("Impossibile trovare il documento master EPM.")
    public static final String EPM_DOCUMENT_MASTER_NOT_FOUND = "83";

    @RBEntry("Impossibile esportare gli oggetti che si trovano in schedari personali")
    public static final String CANNOT_EXP_OBJECTS_IN_PERSONAL_CABINETS = "84";

    @RBEntry("Crea nuovo team : {0}")
    public static final String CREATE_NEW_TEAM = "85";

    @RBEntry("Nel processo di visualizzazione di anteprima d'importazione sono stati elaborati {0} XML file")
    public static final String IMPORT_PREVIEW_N_XML_FILES = "86";

    @RBEntry("L'oggetto {0} esiste già nel database.")
    public static final String OBJECT_EXISTS = "87";

    @RBEntry("Impossibile importare la versione dell'oggetto {0}.{1}, versione precedente della versione esistente {2} con l'importazione normale.\nÈ ancora possibile importarla con Importa come nuova iterazione, Importa come nuova versione o Importa come oggetto sottoposto a Check-Out.")
    public static final String CANNOT_IMPORT_PREVIOUS_OBJECT = "88";

    @RBEntry("Elenco di oggetti esportati:")
    public static final String LIST_OF_EXPORTED_OBJECT = "89";

    @RBEntry("Il documento usato nella struttura del documento non è stato trovato.")
    public static final String USED_DOC_NOT_FOUND = "90";

    @RBEntry("Impossibile trovare origine build")
    public static final String BUILD_SOURCE_NOT_FOUND = "91";

    @RBEntry("Impossibile trovare destinazione build")
    public static final String BUILD_TARGET_NOT_FOUND = "92";

    @RBEntry("Impossibile esportare il documento {0}. Il documento si trova in uno schedario personale. Spostare/sottoporre a Check-In il documento in una cartella condivisa.")
    public static final String CANNOT_EXP_CADDOCUMENT_IN_PERSONAL_CABINET = "93";

    @RBEntry("Impossibile trovare EPMUsesOccurrence che ha creato il caso d'impiego {0}")
    public static final String BUILT_FROM_OCCURRENCE_NOT_FOUND = "94";

    @RBEntry("Impossibile trovare link")
    public static final String BUILT_LINK_NOT_FOUND = "95";

    @RBEntry("Impossibile trovare il master del documento figlio")
    public static final String CHILD_DOC_MASTER_NOT_FOUND = "96";

    @RBEntry("Impossibile contrassegnare il link utilizzo come creato")
    public static final String CANNOT_MARK_LINK_AS_BUILT = "97";

    @RBEntry("Impossibile trovare il caso d'impiego di nome {0}")
    public static final String NAMED_OCCURRENCE_NOT_FOUND = "98";

    @RBEntry("Azione sconosciuta: \"{0}\"")
    @RBArgComment0("name of the action to be applied to object, such as \"Checkout\", \"Lock\".")
    public static final String UNKNOWN_ACTION_COMMAND = "99";

    @RBEntry("L'oggetto \"{0}\" è già stato sottoposto a Check-Out da un altro utente/gruppo/ruolo: \"{1}\"")
    @RBArgComment0("The object identifier")
    @RBArgComment1("The name of the other principal")
    public static final String ALREADY_CHECKEDOUT_BY_OTHER_PRINCIPAL = "100";

    @RBEntry("L'oggetto \"{0}\" è già stato bloccato da un altro utente/gruppo/ruolo: \"{1}\"")
    @RBArgComment0("The object identifier")
    @RBArgComment1("The name of the other principal")
    public static final String ALREADY_LOCKED_BY_OTHER_PRINCIPAL = "101";

    @RBEntry("ATTENZIONE: l'oggetto \"{0}\" è già stato sottoposto a Check-Out. L'operazione \"Check-Out\" sarà ignorata.")
    @RBArgComment0("The object identifier")
    public static final String ALREADY_CHECKEDOUT_BY_CURR_PRINCIPAL = "102";

    @RBEntry("ATTENZIONE: l'oggetto \"{0}\" è già bloccato. L'operazione \"Blocca\" sarà ignorata.")
    @RBArgComment0("The object identifier")
    public static final String ALREADY_LOCKED_BY_CURR_PRINCIPAL = "103";

    @RBEntry("Impossibile creare l'organizer di attributi perché il nome \"{0}\" esiste già per le definizioni degli attributi d'istanza.")
    @RBArgComment0("name of the AttributeOrganizer to be created")
    public static final String NAME_EXISTS_FOR_ATTRIBUTE_ORGANIZER = "104";

    @RBEntry("Importazione impossibile. Scegliere un'azione d'importazione diversa. I gestori EPM non supportano quesate azioni: \"{0}\".")
    @RBArgComment0("List of actions not supported by EPM handlers (Substiture, ignore, Create new identity).")
    public static final String EPM_ACTION_NOT_SUPPORTED = "105";

    @RBEntry("L'oggetto \"{0}\" non verrà esportato a causa della specifica di configurazione.")
    @RBArgComment0("Object identifier")
    public static final String EXPORT_OBJECT_SKIPPED_FOR_CONFIGSPEC = "106";

    @RBEntry("L'oggetto \"{0}\" viene ignorato per l'esportazione.")
    @RBArgComment0("Object identifier")
    public static final String EXPORT_OBJECT_IGNORED = "107";

    @RBEntry("Il file XML con tag \"{0}\" viene ignorato per l'esportazione.")
    @RBArgComment0("XML file tag")
    public static final String IMPORT_OBJECT_IGNORED = "108";

    @RBEntry(" Ignora, non importare l'oggetto \"{0}\".")
    @RBArgComment0("object id of the object which is ignored.")
    public static final String IGNORE_OBJECT = "109";

    @RBEntry("Ignora {0} oggetti")
    @RBArgComment0("amount of objects which are ignored during import process.")
    public static final String IMPORT_IGNORED_OBJECTS = "110";

    @RBEntry("Gli attributi authoringApplication, ownerApplication e docType di EPMDocument non sono modificabili una volta che l'oggetto è diventato persistente.")
    public static final String ATTRIBUTES_NOT_CHANGABLE = "111";

    @RBEntry("Impossibile trovare l'oggetto.")
    public static final String THE_OBJECT_NOT_FOUND = "112";

    @RBEntry("I documenti CAD/documenti dinamici sono stati sottoposti a Check-Out nel workspace  \"{0}\"")
    public static final String EPM_CHECKOUT_WORKSPACE_NAME = "113";

    @RBEntry("L'azione di importazione \"{0}\" non è applicabile a \"{1}\".")
    public static final String IMPORT_ACTION_IS_INAPPLICABLE = "114";

    @RBEntry("Impossibile applicare l'azione di importazione \"{0}\" all'oggetto \"{1}\".")
    public static final String IMPORT_ACTION_EXCEPTION = "115";

    @RBEntry("Impossibile esportare l'oggetto \"{0}\".")
    public static final String OBJECT_EXPORT_EXCEPTION = "116";

    @RBEntry("Oggetto \"{0}\" non trovato.")
    public static final String OBJECT_NOT_FOUND_EXCEPTION = "117";

    @RBEntry("Impossibile determinare la posizione dell'oggetto \"{0}\".")
    public static final String OBJECT_NOT_LOCATED_EXCEPTION = "118";

    @RBEntry("Impossibile creare \"{0}\".")
    public static final String OBJECT_CREATION_EXCEPTION = "119";

    @RBEntry("Impossibile importare gli attributi per l'oggetto \"{0}\".")
    public static final String ATTRIBUTE_IMPORT_EXCEPTION = "120";

    @RBEntry("Impossibile verificare il conflitto di attributi per \"{0}\".")
    public static final String ATTRIBUTE_CONFLICT_CHECK_EXCEPTION = "121";

    @RBEntry("Impossibile aggiornare l'attributo \"{1}\" per \"{0}\".")
    public static final String ATTRIBUTE_UPDATE_EXCEPTION = "122";

    @RBEntry("L'aggiornamento dell'attributo \"{0}\" al valore \"{1}\" non è consentito.")
    public static final String ATTRIBUTE_UPDATE_VETO_EXCEPTION = "123";

    @RBEntry("Impossibile recuperare l'attributo \"{1}\" per \"{0}\".")
    public static final String ATTRIBUTE_GET_EXCEPTION = "124";

    @RBEntry("Impossibile esportare il rirefimento per \"{0}\".")
    public static final String OBJECT_REFERENCE_EXPORT_EXCEPTION = "125";

    @RBEntry("Impossibile ottenere informazioni mediante il riferimento per determinare la posizione di \"{0}\".")
    public static final String OBJECT_REFERENCE_IMPORT_EXCEPTION = "126";

    @RBEntry("Impossibile istanziare \"{0}\".")
    public static final String OBJECT_REFERENCE_HANDLER_INSTATIATION_EXCEPTION = "127";

    @RBEntry("Gestore riferimento oggetto non specificato per \"{0}\".")
    public static final String OBJECT_REFERENCE_HANDLER_NOT_SPECIFIED_EXCEPTION = "128";

    @RBEntry("Impossibile trovare il gestore di esportazione per \"{0}\".")
    public static final String OBJECT_REFERENCE_EXPORT_HANDLER_NOT_FOUND_EXCEPTION = "129";

    @RBEntry("Impossibile aggiornare l'attributo \"{1}\" con il valore \"{0}\".")
    public static final String SIMPLE_ATTRIBUTE_UPDATE_EXCEPTION = "130";

    @RBEntry("L'aggiornamento dell'attributo \"{0}\" al valore \"{1}\" non è consentito.")
    public static final String SIMPLE_ATTRIBUTE_UPDATE_VETO_EXCEPTION = "131";

    @RBEntry("Impossibile determinare se \"{0}\" esiste già prima dell'importazione.")
    public static final String OBJECT_EXISTENCE_CAN_NOT_DETERMINE_EXCEPTION = "132";

    @RBEntry("Impossibile esportare l'oggeto gestito con schedari \"{0}\".")
    public static final String CABINETMANAGED_OBJECT_EXPORT_EXCEPTION = "133";

    @RBEntry("Impossibile importare l'oggetto gestito con schedari \"{0}\".")
    public static final String CABINETMANAGED_OBJECT_IMPORT_EXCEPTION = "134";

    @RBEntry("Impossibile trovare il dominio \"{0}\".")
    public static final String DOMAIN_NOT_FOUND_EXCEPTION = "135";

    @RBEntry("Impossibile esportare l'oggetto gestito \"{0}\".")
    public static final String MANAGED_OBJECT_EXPORT_EXCEPTION = "136";

    @RBEntry("Impossibile importare l'oggetto gestito \"{0}\".")
    public static final String MANAGED_OBJECT_IMPORT_EXCEPTION = "137";

    @RBEntry("Impossibile stabilire il percorso di classificazione per \"{0}\".")
    public static final String CLASSIFICATION_COULD_NOT_ESTABLISH_EXCEPTION = "138";

    @RBEntry("Identificativo di sostituzione mancante per \"{0}\".")
    public static final String MISSING_SUBSTITUTE_IDENTITY_EXCEPTION = "139";

    @RBEntry("Impossibile esportare gli attributi dell'oggetto \"{0}\".")
    public static final String ATTRIBUTE_EXPORT_EXCEPTION = "140";

    @RBEntry("Impossibile esportare il contenuto dell'oggetto \"{0}\".")
    public static final String CONTENT_EXPORT_EXCEPTION = "141";

    @RBEntry("Impossibile esportare l'oggetto \"{0}\". È stato sottoposto a iterazione da una sessione priva di proprietario.")
    public static final String OBJECT_NOT_SESSION_ITERATED_EXCEPTION = "142";

    @RBEntry("Impossibile esportare la baseline. Non dispone di membri per \"{0}\".")
    public static final String BASELINE_NO_MEMBER_FOUND_EXCEPTION = "143";

    @RBEntry("Impossibile creare una nuova iterazione. ConfigSpec non ha potuto determinare la posizione di un oggetto già esistente.")
    public static final String CONFIG_SPEC_BASED_ITERATION_NOT_FOUND_EXCEPTION = "144";

    @RBEntry("La creazione di una nuova iterazione non è consentita.")
    public static final String CONFIG_SPEC_BASED_NEW_ITERATION_CREATION_EXCEPTION = "145";

    @RBEntry("Impossibile creare una nuova versione. ConfigSpec non ha potuto determinare la posizione di un oggetto già esistente.")
    public static final String CONFIG_SPEC_BASED_VERSION_NOT_FOUND_EXCEPTION = "146";

    @RBEntry("L'oggetto esiste già. Non è possibile creare un nuovo oggetto con lo stesso identificativo.")
    public static final String CREATE_NEW_OBJECT_WITH_OBJECT_ALREADY_EXISTING_EXCEPTION = "147";

    @RBEntry("Impossibile trovare l'attore per \"{0}\".")
    public static final String ACTOR_NOT_FOUND_EXCEPTION = "148";

    @RBEntry("Impossibile inizializzare factory attore.")
    public static final String ACTOR_FACTORY_INITIALIZATION_EXCEPTION = "149";

    @RBEntry("L'oggetto di sostituzione non esiste.")
    public static final String SUBSTITUTE_OBJECT_DOES_NOT_EXIST_EXCEPTION = "150";

    @RBEntry("Specificare un'azione di importazione.")
    public static final String IMPORT_ACTION_NOT_SPECIFIED_EXCEPTION = "151";

    @RBEntry("Impossibile trovare il gestore di importazione per \"{0}\".")
    public static final String OBJECT_REFERENCE_IMPORT_HANDLER_NOT_FOUND_EXCEPTION = "152";

    @RBEntry("L'oggetto non esiste.")
    public static final String OBJECT_DOES_NOT_EXIST_EXCEPTION = "153";

    @RBEntry("\"{0}\" - Versione schema non corrispondente: \"{1}\"")
    public static final String VERSION_SCHEME_MISMATCH = "154";

    @RBEntry("Oggetto FormalizedBy non trovato.")
    public static final String FORMALIZEDBY_NOT_FOUND = "155";

    @RBEntry("Oggetto ResearchedBy non trovato.")
    public static final String RESEARCHEDBY_NOT_FOUND = "156";

    @RBEntry("Oggetto AddressedBy non trovato.")
    public static final String ADDRESSEDBY_NOT_FOUND = "157";

    @RBEntry("Oggetto IncludedIn non trovato.")
    public static final String INCLUDEDIN_NOT_FOUND = "158";

    @RBEntry("Oggetto DetailedBy non trovato.")
    public static final String DETAILEDBY_NOT_FOUND = "159";

    @RBEntry("Oggetto AcceptedStrategy non trovato.")
    public static final String ACCEPTEDSTRATEGY_NOT_FOUND = "160";

    @RBEntry("Oggetto ReportedAgainst non trovato.")
    public static final String REPORTEDAGAINST_NOT_FOUND = "161";

    @RBEntry("Oggetto ProblemProduct non trovato.")
    public static final String PROBLEMPRODUCT_NOT_FOUND = "162";

    @RBEntry("Oggetto SubjectProduct non trovato.")
    public static final String SUBJECTPRODUCT_NOT_FOUND = "163";

    @RBEntry("Oggetto AffectedActivityData non trovato.")
    public static final String AFFECTEDACTIVITYDATA_NOT_FOUND = "164";

    @RBEntry("Oggetto ChangeRecord non trovato.")
    public static final String CHANGERECORD2_NOT_FOUND = "165";

    @RBEntry("Oggetto RelevantRequestData non trovato.")
    public static final String RELEVANTREQUESTDATA_NOT_FOUND = "166";

    @RBEntry("Oggetto RelevantAnalysisData non trovato.")
    public static final String RELEVANTANALYSISDATA_NOT_FOUND = "167";

    @RBEntry("Esportazione oggetti in corso...")
    public static final String SUMMARY_EVENT_EXPORT_MESSAGE = "168";

    @RBEntry("Importazione oggetti in corso...")
    public static final String SUMMARY_EVENT_IMPORT_MESSAGE = "169";

    @RBEntry("Contenitore pubblicato o oggetto rappresentabile non trovato.")
    public static final String PUBLISHEDCONTENTHOLDER_OR_REPRESENTABLE_OBJECT_NOT_FOUND = "170";

    @RBEntry("Oggetto HangingChangeLink non trovato.")
    public static final String HANGINGCHANGE_NOT_FOUND = "171";

    @RBEntry("Dati di supporto per oggetto link non trovati.")
    public static final String SUPPORTINGDATAFOR_NOT_FOUND = "172";

    @RBEntry("Impossibile trovare l'elemento modello.")
    public static final String MODEL_ITEM_NOT_FOUND = "173";

    @RBEntry("Sono stati aggiornati {0} oggetti, rappresentanti oggetti provenienti da file XML")
    public static final String IMPORT_UPDATED_OBJECTS = "174";

    @RBEntry("È stato aggiornato un oggetto esistente: {0}")
    public static final String IMPORT_OBJECT_IS_UPDATED = "175";

    @RBEntry("Oggetto HangingChangeLink ignorato perché la preferenza Creazione modifiche non incorporate non è definita: {0}")
    public static final String HANGINGCHANGE_PREFERENCE_NOT_FOUND = "176";

    @RBEntry("L'oggetto \"{0}\" è stato sottoposto a Check-Out.")
    @RBArgComment0("Object identifier.")
    public static final String OBJECT_CHECKED_OUT = "exp_chkout";

    @RBEntry("L'oggetto \"{0}\" è bloccato.")
    @RBArgComment0("Object identifier.")
    public static final String OBJECT_LOCKED = "exp_lock";

    @RBEntry("All'oggetto \"{0}\" è stato applicato un blocco amministrativo.")
    @RBArgComment0("Object identifier.")
    public static final String OBJECT_ADMIN_LOCKED = "OBJECT_ADMIN_LOCKED";

    @RBEntry("L'oggetto \"{0}\" è bloccato e non può essere sottoposto a Check-Out.")
    @RBArgComment0("Object identifier.")
    public static final String OBJECT_LOCK_TO_CHECK_OUT = "lock_to_ckout";

    @RBEntry("Operazione interrotta dal client.")
    public static final String OP_ABORTED_FROM_CLIENT = "op_aborted_from_client";

    @RBEntry("Impossibile aprire il file di registro obbligatorio per priorità d'esportazione.")
    public static final String CAN_NOT_OPEN_IXB_EXPORT_PRIORITY_REGISTRY_FILE = "no_export_priority_registry";

    @RBEntry("Numero complessivo oggetti esportati:")
    public static final String TOTAL_NUMBER_OF_EXPORTED_OBJECT = "total_exp_objs";

    @RBEntry("L'azione \"{0}\" non è applicabile all'oggetto \"{1}\".")
    @RBArgComment0("Action name")
    @RBArgComment1("Object identifier or object type, class name, etc")
    public static final String IMPORT_ACTION_NOT_APPLICABLE_AND_CHANGED = "imp_action_not_appl_and_changed";

    @RBEntry("Attenzione! Si richiede l'eliminazione dell'attributo d'istanza da \"{0}\" durante l'importazione; l'attributo non è stato però trovato nel contenitore degli attributi. L'eliminazione dell'attributo d'istanza verrà ignorata: percorso: \"{1}\", tipo: \"{2}\", valore: \"{3}\".")
    @RBArgComment0("IBA Holder")
    public static final String IBA_SHLD_EXCLUDED_BUT_NOT = "iba_exc_but_not";

    @RBEntry("Impossibile trovare l'icona \"{0}\"")
    @RBArgComment0("Icon name, representing a file")
    public static final String ICON_COULD_NOT_FOUND = "icon_not_found";

    @RBEntry("A causa di un conflitto di nome, l'icona \"{0}\" viene rinominata: \"{1}\"")
    public static final String ICON_RENAMED = "icon_renamed";

    @RBEntry("Per importare l'oggetto \"{0}\", effettuare il Check-Out dell'ultima iterazione.")
    public static final String CHECKOUT_OBJECT = "checkout_object";

    @RBEntry("Check-Out di {0} oggetti")
    public static final String IMPORT_CHECKOUT_OBJECTS = "import_checkout_object";

    @RBEntry("Il modello di team del file XML si trova nel dominio \"{0}\", ma l'oggetto è nel dominio  \"{1}\". Il modello di team dell'oggetto è definito perché è stato spuntato \"Risolvi conflitti\".")
    public static final String TEAM_TEMPLATE_AND_OBJECT_DOMAIN_ARE_IN_DIFFERENT_POSITION = "team_template_and_object_domain_are_in_different_postion";

    @RBEntry("Mappatura mancante per il percorso del contenitore {0} oppure è impossibile trovare il riferimento del contenitore.")
    public static final String CONTAINER_REF_NOT_FOUND = "container_ref_not_found";

    @RBEntry("Errore durante l'elaborazione del file XML: {0}.")
    public static final String ERROR_PROCESS_XML_FILE = "error_process_xml_file";

    @RBEntry("Formato di identificatore organizzazione non valido: \"{0}\"")
    public static final String INVALID_ORG_ID_FORMAT = "invalid_org_id_format";

    @RBEntry("L'oggetto \"{0}\" è già stato sbloccato.")
    public static final String OBJECT_UNLOCKED = "obj_unlocked";

    @RBEntry("Il valore \"{1}\" deve essere impostato per CreateNewObjectActor per TypeDefinition \"{0}\"")
    @RBArgComment0("TypeDefinition full path")
    @RBArgComment1("Tag name: \"actionInfo/actionParams/newName\"")
    public static final String MISSING_INFO_FOR_CREATE_NEW_OBJ_ACTOR_FOR_TYPE = "missing_info_create_new_type";

    @RBEntry("Impostare il valore \"{1}\" e \"{2}\" per CreateNewObjectActor per TypeDefinition \"{0}\"")
    @RBArgComment0("TypeDefinition full path")
    @RBArgComment1("Tag name: \"actionInfo/actionParams/newName\"")
    @RBArgComment2("Tag name: \"actionInfo/actionParams/newPath\"\"")
    public static final String MISSING_INFO_FOR_SUBSTITUTE_ACTOR_FOR_TYPE = "missing_info_substitute_type";

    @RBEntry("L'oggetto \"{0}\" non è stato sottoposto a Check-Out.")
    public static final String OBJ_NOT_CHECK_OUT = "obj_not_checked_out";

    @RBEntry("L'oggetto \"{0}\" non supporta più RatioValue. Modificato a FloatValue.")
    @RBArgComment0("Object identifier holding this IBA value")
    public static final String IBA_VALUE_CHANGED_FROM_RATIO_TO_FLOAT = "value_changed_ratio_to_float";

    @RBEntry("RatioDefinition non è più supportata. Modificata a FloatDefinition: \"{0}\"")
    @RBArgComment0("Full path of the IBA Definition.")
    public static final String IBA_DEF_CHANGED_FROM_RATIO_TO_FLOAT = "def_changed_ratio_to_float";

    @RBEntry("Il file jar non è stato creato perché non è stato esportato alcun oggetto.")
    public static final String JAR_FILE_NOT_CREATED = "jar_not_created";

    @RBEntry("Il file jar {0} non contiene alcun file XML dell'oggetto.")
    @RBArgComment0("The name of the jar file.")
    public static final String JAR_FILE_DOES_NOT_CONTAINS_XML = "jar_file_does_not_contains_xml";

    @RBEntry("La cartella \"{0}\" non esiste.")
    public static final String FOLDER_DOES_NOT_EXIST = "folder_does_not_exist";

    @RBEntry("La cartella \"{0}\" è stata creata.")
    public static final String FOLDER_CREATED = "folder_created";

    @RBEntry("Lo schedario \"{0}\" è stato creato.")
    public static final String CABINET_CREATED = "cabinet_created";

    @RBEntry("La definizione tipo \"{0}\" è in stato di Check-Out. Importazione non consentita.")
    @RBArgComment0("The name and the path of the type definition.")
    public static final String IMPORT_NOT_ALLOWED_DUETO_TYPE_CHECKEDOUT = "imp_not_allowed_due_type_checked_out";

    @RBEntry("Non è consentito effettuare il Check-Out delle definizioni dei tipi al momento dell'esportazione.")
    public static final String CHECKOUT_TYPE_ON_EXPORT_NOT_ALLOWED = "chkout_type_on_exp_disallowed";

    @RBEntry("La definizione tipo \"{0}\" esiste senza conflitti non ignorabili. L'azione \"{1}\" viene ignorata.")
    public static final String USE_EXISTING_TYPE_BCOZ_NO_CONF = "use_existing_type_bcoz_no_conf";

    @RBEntry("L'azione Default non supporta l'importazione della versione vista. Informazioni file importato (oggetto = \"{0}\", versione = \"{1}\", livello = \"{2}\").")
    @RBArgComment0("The object being imported.")
    @RBArgComment1("The version Label being imported.")
    @RBArgComment2("The version level being imported.")
    public static final String BRANCHED_VERSION_IMPORT_NOT_SUPPORTED = "branched_version_imp_not_supported";

    @RBEntry("\"{0}\": importazione completata")
    @RBArgComment0("Name of the succeded jar in import jar file")
    public static final String IMPORT_RESULT_COMPLETED = "IMPORT_RESULT_COMPLETED";

    @RBEntry("\"{0}\": importazione non riuscita")
    @RBArgComment0("Name of the failed jar in import jar file")
    public static final String IMPORT_RESULT_FAILED = "IMPORT_RESULT_FAILED";

    @RBEntry("Numero importazioni file jar riuscite nel jar importato: {0}")
    @RBArgComment0("Successfully imported jar file count in import jar")
    public static final String IMP_JAR_IN_JAR_SUCCESS_ENTRY_COUNT = "IMP_JAR_IN_JAR_SUCCESS_ENTRY_COUNT";

    @RBEntry("Numero importazioni file jar non riuscite nel jar importato: {0}")
    @RBArgComment0("Un successfully imported jar file count in import jar")
    public static final String IMP_JAR_IN_JAR_FAILED_ENTRY_COUNT = "IMP_JAR_IN_JAR_FAILED_ENTRY_COUNT";

    @RBEntry("Riepilogo risultati importazione file jar in jar...")
    public static final String IMP_JAR_IN_JAR_RESULT_SUMMARY = "IMP_JAR_IN_JAR_RESULT_SUMMARY";

    @RBEntry("Elenco file jar nel jar importato per cui l'importazione non è riuscita : {0}")
    @RBArgComment0("List of jar files in the import jar for which import failed.")
    public static final String IMP_JAR_IN_JAR_FAILED_ENTRY_LIST = "IMP_JAR_IN_JAR_FAILED_ENTRY_LIST";

    @RBEntry("Nome file o contenuto non valido: {0}")
    @RBArgComment0("The content holder identity whose content or file name is invalid")
    public static final String INVALID_CONTENT_ERROR = "invalid_content_error";

    @RBEntry("\"{0}\": azione non consentita per la definizione di tipo. L'importazione verrà eseguita con l'azione di default. Per ulteriori informazioni sull'aggiornamento della definizione di tipo, consultare la Guida dell'amministratore di sistema Windchill.")
    public static final String TYPE_DEFINITION_UPDATE_NOT_ALLOWED = "type_definition_update_not_allowed";

    @RBEntry("(tipo di attributo: \"{0}\", percorso: \"{1}\"): L'attributo d'istanza è stato ignorato perché non è presente nella definizione di tipo: \"{2}\". Può essere creato ma non utilizzato.")
    public static final String IBA_IGNORED_DUE_TO_TYPEDEF_MISMATCH = "iba_ignored_due_to_typedef_mismatch";

    @RBEntry("Importazione fra release differenti non consentita. Consultare la Guida dell'amministratore di sistema Windchill per informazioni sull'attivazione di questa funzionalità.")
    public static final String CROSS_RELEASE_IMPORT_NOT_ALLOWED = "cross_release_import_not_allowed";

    @RBEntry("\"{0}\": l'azione non è valida per l'importazione della versione vista, dal momento che non ne esiste una. Informazioni sul file di importazione: oggetto = \"{1}\", versione = \"{2}\", livello = \"{3}\".")
    @RBArgComment0("The action used for Import")
    @RBArgComment1("The object being imported.")
    @RBArgComment2("The version Label being imported.")
    @RBArgComment3("The version level being imported.")
    public static final String EXISTING_VERSION_NOT_FOUND_FOR_VIEW_VERSION_IMPORT = "EXISTING_VERSION_NOT_FOUND_FOR_VIEW_VERSION_IMPORT";

    @RBEntry("La dimensione dei dati vincolo non corrisponde ai dati vincolo: {0}")
    @RBArgComment0("The constraint type for which there has been a data size mismatch")
    public static final String CONSTRAINT_DATA_SIZE_MISMATCH = "constraint_data_size_mismatch";

    @RBEntry("Impossibile trovare l'organizzazione \"{0}\". Consultare la guida Windchill System Administrator's Guide (Guida dell'amministratore di sistema Windchill) per ulteriori informazioni.")
    public static final String ORGANIZATION_NOT_FOUND = "organization_not_found";

    @RBEntry("\"{0}\": valore dell'attributo non disponibile durante l'esportazione per problemi legati ai permessi di accesso. Fornire un valore valido prima dell'importazione.")
    public static final String ACTION_NEEDED_FOR_SECURED_INFO = "action_needed_for_secured_info";

    @RBEntry("Configurazione errata per \"{0}\".")
    public static final String MALFORMED_RESOLVER = "malformed_resolver";

    @RBEntry("ID stream non trovato per il contenuto {0}")
    public static final String NO_STREAM_ID_FOR_CONTENT_ID = "NO_STREAM_ID_FOR_CONTENT_ID";

    @RBEntry("Contenuto non trovato per ID stream {0}")
    public static final String NO_CONTENT_ID_FOR_STREAM_ID = "NO_CONTENT_ID_FOR_STREAM_ID";

    @RBEntry("Informazioni sulla dimensione non trovate per l'elemento di contenuto {0}")
    public static final String CONTENT_SIZE_NOT_FOUND = "CONTENT_SIZE_NOT_FOUND";

    @RBEntry("Errore durante il calcolo del checksum per l'ID contenuto {0}")
    public static final String ERROR_GETTING_CHECKSUM_FOR_CONTENT_ID = "ERROR_GETTING_CHECKSUM_FOR_CONTENT_ID";

    @RBEntry("Impossibile aprire il file di registro multiformato importazione/esportazione.")
    public static final String CAN_NOT_OPEN_IXB_MULTIPLEFORMAT_REGISTRY_FILE = "CAN_NOT_OPEN_IXB_MULTIPLEFORMAT_REGISTRY_FILE";

    @RBEntry("Importazione di \"{0}\" completata.")
    public static final String OBJECT_IMPORT_SUCCESS = "object_import_success";

    @RBEntry("Importazione di \"{0}\" non eseguita. L'oggetto esiste già. ")
    public static final String OBJECT_IMPORT_EXIST = "object_import_exist";

    @RBEntry("Impossibile importare \"{0}\".")
    public static final String OBJECT_IMPORT_ERROR = "object_import_error";

    @RBEntry("Impossibile importare \"{0}\". Contattare l'amministratore.")
    public static final String OBJECT_IMPORT_ERROR_CONTACT_ADMINISTRATOR = "OBJECT_IMPORT_ERROR_CONTACT_ADMINISTRATOR";

    @RBEntry("Oggetto \"{0}\" ignorato per l'importazione.")
    public static final String OBJECT_IMPORT_WARNING = "object_import_warning";

    @RBEntry("Mappatura ambiente federato '{0}' esistente, da '{1}' a '{2}'.")
    public static final String FEDERATION_MAPPING_HAPPEN = "federation_mapping_happen";

    @RBEntry("Confronto di '{0}'. Valore precedente: '{1}'. Nuovo valore: '{2}'.")
    public static final String COMPARISON_MESSAGE = "comparison_message";

    @RBEntry("Esportazione di '{0}' completata")
    public static final String OBJECT_EXPORT_SUCCESS = "object_export_success";

    @RBEntry("Impossibile esportare '{0}'")
    public static final String OBJECT_EXPORT_ERROR = "object_export_error";

    @RBEntry("Blocco di file: '{0}'.")
    public static final String CHUNK_FILE = "chunk_file";

    @RBEntry("'{0}' esportato in blocco di file '{1}'.")
    public static final String EXPORT_TO_CHUNK_FILE = "export_to_chunk_file";

    @RBEntry("Risoluzione '{0}' selezionata per risolvere il conflitto '{1}'.")
    public static final String RESOLUTION_CONFLICT = "resolution_conflict";

    @RBEntry("Discordanza presente nel file collaboration_interest.xml. XPath '{0}'  risulta mappato più volte.")
    public static final String DUPLICATION_IN_COLLABORATION_INTEREST_FOR_XPATH = "duplication_in_collaboration_interest_for_xpath";

    @RBEntry("Discordanza presente nel file collaboration_interest.xml. Il percorso di destinazione '{0}' risulta mappato più volte.")
    public static final String DUPLICATION_IN_COLLABORATION_INTEREST_FOR_TARGET_PATH = "duplication_in_collaboration_interest_for_target_path";

    @RBEntry("Formato nativo non trovato per l'attributo d'istanza di destinazione '{0}' nel file collaboration_interest.xml")
    public static final String NATIVE_FORMAT_NOT_FOUND_FOR_TARGET_IBA = "native_format_not_found_for_target_iba";

    @RBEntry("'{0}': XPath non trovato nel file XML dell'oggetto o nel file XML delle informazioni di collaborazione")
    public static final String XPATH_NOT_PRESENT_IN_XML = "xpath_not_present_in_xml";

    @RBEntry("Mappatura non valida per XPath nel file collaboration_interest.xml: '{0}'")
    public static final String INCORRECT_MAPPING = "incorrect_mapping";

    @RBEntry("Il valore \"{0}\" viene modificato per default in \"{1}\" poiché il file jar di esportazione non contiene queste informazioni.")
    public static final String DEFAULT_PRINCIPAL_SET = "DEFAULT_PRINCIPAL_SET.";

    @RBEntry("Classe Tipo: '{0}' non valida. Impossibile importare il tipo: {1}. Creare dapprima il tipo.")
    public static final String INVALID_TYPE_CLASS = "invalid_type_class";

    @RBEntry("Impossibile impostare il tipo '{0}' nel sistema. Il nome '{1}' è stato usato da un altro tipo '{2}'")
    public static final String TYPE_NAME_UNIQUE_VIOLATION = "type_name_unique_violation";

    @RBEntry("Creazione del tipo '{0}' non riuscita durante il tipo di importazione '{1}'")
    public static final String CREATING_TYPE_FAILED = "creating_type_failed";

    @RBEntry("Il sistema non consente l'importazione di un nuovo tipo '{0}'")
    public static final String CANNOT_IMPORT_NEW_TYPE = "cannot_import_new_type";

    @RBEntry("Impossibile trovare il tipo '{0}' nel sistema")
    public static final String CANNOT_FIND_TYPE = "cannot_find_type";

    @RBEntry("Importazione dell'attributo '{0}' non riuscita. Il tipo di dati '{1}' specificato non corrisponde a quello esistente, '{2}'.")
    public static final String INVALID_DATATYPE = "invalid_datatype";

    @RBEntry("Il sistema non consente l'importazione di un nuovo attributo '{0}' in un tipo esistente '{1}'")
    public static final String CANNOT_IMPORT_NEW_ATTRIBUTE = "cannot_import_new_attribute";

    @RBEntry("Il nuovo attributo '{0}' viene importato nel tipo '{1}'.")
    public static final String IMPORTED_NEW_ATTRIBUTE = "imported_new_attribute";

    @RBEntry("Viene importato il nuovo tipo '{0}'.")
    public static final String IMPORTED_NEW_TYPE = "imported_new_type";

    @RBEntry("L'importazione di un nuovo attributo '{0}' in un tipo esistente '{1}' non è riuscita")
    public static final String IMPORTING_NEW_ATTRIBUTE_FAILED = "importing_new_attribute_failed";

    @RBEntry("Il tipo '{0}' non può creare un attributo standard.")
    public static final String CANNOT_CREATE_STANDARD_ATTRIBUTE = "cannot_create_standard_attribute";

    @RBEntry("Impossibile impostare il tipo '{0}' per l'oggetto tipizzato '{1}'")
    public static final String CANNOT_SET_TYPE = "cannot_set_type";

    @RBEntry("Questa operazione non è supportata dalla classe \"{0}\"")
    public static final String UNSUPPORTED_OPERATION_EX = "unsupported_operation_exception";

    @RBEntry("Non è supportata l'importazione esclusiva di contenuto.")
    public static final String CONTENT_ONLY_IMPORT_NOT_SUPPORTED = "content_only_import_not_supported";

    @RBEntry("L'oggetto esportazione/importazione \"{0}\" non è valido. Deve essere la classe \"{1}\".")
    public static final String INVALID_EXPORT_IMPORT_OBJECT = "invalid_export_import_object";

    @RBEntry("L'oggetto esportazione/importazione \"{0}\" non può avere l'attributo \"{1}\" con un valore nullo.")
    public static final String NULL_ATTRIBUTE_VALUE = "null_attribute_value";

    @RBEntry("L'argomento \"{0}\" non può essere nullo")
    public static final String NULL_ARGUMENT = "null_argument";

    @RBEntry("L'identificatore di tipo logico \"{0}\" nel contenitore \"{1}\" non è valido.")
    public static final String INVALID_LOGICAL_TYPE_ID = "invalid_logical_type_id";

    @RBEntry("L'identificatore di tipo persistente \"{0}\" non è valido.")
    public static final String INVALID_PERSISTED_TYPE_ID = "invalid_persisted_type_id";

    @RBEntry("L'attributo di tipo persistente \"{0}\" non è valido.")
    public static final String NULL_PERSISTED_TYPE_ID = "null_persisted_type_id";

    @RBEntry("Assicurarsi che i nodi di classificazione nel percorso specificato siano separati da '/'.")
    public static final String CLASSIFICATION_NODE_PATH_MESSAGE = "classification_node_path_message";

    @RBEntry("Impossibile trovare un nodo di classificazione radice - '{0}'.")
    public static final String CLASSIFICATION_ROOT_NODE_NOT_FOUND = "classification_root_node_not_found";

    @RBEntry("Impossibile trovare un nodo di classificazione - '{0}'.")
    public static final String CLASSIFICATION_NODE_NOT_FOUND = "classification_node_not_found";

    @RBEntry("Gestore di flusso non valido utilizzato per l'esportazione o l'importazione nel server. Utilizzare '{0}'")
    public static final String INVALID_STREAMER_TYPE_FOR_SERVERSIDE_EXPORT_IMPORT = "invalid_streamer_type_for_serverside_export_import";

    @RBEntry("Gestore di flusso non valido utilizzato per l'esportazione o l'importazione nel client. Utilizzare '{0}'")
    public static final String INVALID_STREAMER_TYPE_FOR_CLIENTSIDE_EXPORT_IMPORT = "invalid_streamer_type_for_clientside_export_import";

    @RBEntry("Impossibile utilizzare l'attore di default dell'oggetto esistente senza le informazioni sulla versione")
    public static final String DEFAULT_ACTOR_HAS_NO_VERSION_INFO = "default_actor_has_no_version_info";

    @RBEntry("L'oggetto \"{0}\" trasmesso al filtro non può essere elaborato da \"{1}\" .")
    public static final String INVALID_OBJECT_FOR_CONTENT_FILTER = "invalid_object_for_content_filter";

    @RBEntry("File non valido generato da \"{0}\"")
    public static final String INVALID_FILE = "invalid_file";

    @RBEntry("Il file di consegna package importabile non è formattato correttamente. Contattare il mittente della consegna.")
    public static final String INVALID_PACKAGE_FILE = "invalid_package_file";

    @RBEntry("Il file non è formattato correttamente. Se il problema persiste, contattare l'amministratore.")
    public static final String IX_INVALID_IMPORT_FILE = "ix_invalid_import_file";

    @RBEntry("Questo non è un file di consegna package importabile. Importare il file tramite l'utilità Gestione importazioni/esportazioni.")
    public static final String IX_INVALID_IMPORT_PACKAGE = "ix_invalid_import_package";

    @RBEntry("\"{0}\" aveva una relazione di tipo \"{1}\" con un altro oggetto non incluso nella consegna package. La relazione e l'oggetto mancante sono stati ignorati durante l'importazione.")
    @RBArgComment0("Non-missing end (object identity) of the link")
    @RBArgComment1("Type of the Link")
    public static final String REL_IGNORED_MISSING_OTHER_END = "REL_IGNORED_MISSING_OTHER_END";

    @RBEntry("La relazione di tipo \"{0}\" non è stata inclusa nella consegna package. La relazione e l'oggetto mancante sono stati ignorati durante l'importazione.")
    @RBArgComment0("Type of the Link")
    public static final String REL_IGNORED_BOTH_ENDS_MISSING = "REL_IGNORED_BOTH_ENDS_MISSING";

    @RBEntry("Per assegnare gli indicatori di riferimento è necessario definire la quantità.")
    public static final String QUANTITY_NOT_DEFINED = "QUANTITY_NOT_DEFINED";

    @RBEntry("Gli indicatori di riferimento possono essere assegnati solo alle parti con unità ciasc.")
    public static final String UNIT_HAS_TO_BE_EACH = "UNIT_HAS_TO_BE_EACH";

    @RBEntry("La quantità deve essere maggiore o uguale al numero degli indicatori di riferimento.")
    public static final String SUBSTITUTE_QUANTITY_LESS_THAN_REFDESIG = "SUBSTITUTE_QUANTITY_LESS_THAN_REFDESIG";

    @RBEntry("È stato superato il limite del numero di indicatori di riferimento di una parte specificato alla voce wt.part.MaxQuantity della proprietà. Per ulteriori informazioni, contattare l'amministratore.")
    public static final String REFDESIG_EXCEEDS_QUANTITY_LIMIT = "REFDESIG_EXCEEDS_QUANTITY_LIMIT";

    @RBEntry("Il valore deve essere un numero intero positivo quando l'unità è ciasc.")
    @RBComment("Error message for invalid quantity value")
    public static final String QUANTITY_AMOUNT_ZERO_NUMBER_VALUE_MSG = "Quantity_Amount_Zero_Number_Value_Msg";

    @RBEntry("La quantità con unità ciasc. deve essere un valore numerico intero positivo.")
    public static final String UnitMsg = "UnitMsg";

    @RBEntry("La quantità deve essere un numero intero quando l'unità è ciasc.")
    @RBComment("Error message for invalid quantity value")
    public static final String QUANTITY_AMOUNT_NOT_REAL_NUMBER_VALUE = "QUANTITY_AMOUNT_NOT_REAL_NUMBER_VALUE";

    @RBEntry("Quantità non definita.")
    @RBComment("Quantity amount is not defined.")
    public static final String QUANTITY_AMOUNT_NOT_DEFINED = "QUANTITY_AMOUNT_NOT_DEFINED";

    @RBEntry("Formato numero non valido: \"{0}\"")
    public static final String INVALID_NUMBER_FORMAT = "INVALID_NUMBER_FORMAT";

    @RBEntry("Unità di quantità non definita")
    @RBComment("Quantity unit is not defined.")
    public static final String QUANTITY_UNIT_NOT_DEFINED = "QUANTITY_UNIT_NOT_DEFINED";

    @RBEntry("Il contesto \"{0}\" non esiste.")
    public static final String CONTEXT_NOT_FOUND = "CONTEXT_NOT_FOUND";

    @RBEntry("Nome file di contenuto non valido: '{0}'. Il nome contiene caratteri non validi non ammessi dal sistema: '{1}'.")
    @RBArgComment0("Content File name.")
    @RBArgComment1("Invalid character set.")
    public static final String CONTENT_NAME_CONTAINS_INVALID_CHARS = "CONTENT_NAME_CONTAINS_INVALID_CHARS";

    @RBEntry("Nome file di contenuto non valido: '{0}'. Il numero di caratteri ('{1}') presenti nel nome del file è superiore al numero massimo ammesso dal sistema: '{2}'.")
    @RBArgComment0("Content File name.")
    @RBArgComment1("Actual number of characters in file name.")
    @RBArgComment2("Maximum number of characters allowed.")
    public static final String CONTENT_NAME_CONTAINS_INVALID_NO_OF_CHARS = "CONTENT_NAME_CONTAINS_INVALID_NO_OF_CHARS";

    @RBEntry("Nessun caso d'impiego trovato con UsesOccurrenceGlobalId {0}")
    public static final String USES_OCCURRENCE_GLOBAL_ID_NOT_FOUND = "USES_OCCURRENCE_GLOBAL_ID_NOT_FOUND";

    @RBEntry("Impossibile trovare la parte di sostituzione.")
    public static final String SUBSTITUTE_FOR_PART_NOT_FOUND = "SUBSTITUTE_FOR_PART_NOT_FOUND";

    @RBEntry("Impossibile trovare la parte alternativa.")
    public static final String ALTERNATE_FOR_PART_NOT_FOUND = "ALTERNATE_FOR_PART_NOT_FOUND";

    @RBEntry("File di importazione non valido")
    public static final String INVALID_IMPORT_FILE = "INVALID_IMPORT_FILE";

    @RBEntry("Impossibile trovare la parte obsoleta.")
    public static final String SUPERSEDED_PART_NOT_FOUND = "SUPERSEDED_PART_NOT_FOUND";

    @RBEntry("Impossibile importare \"{0}\". I file di importazione contengono un valore dell'attributo \"{1}\" non valido (\"{2}\") per l'elemento \"{3}\".")
    @RBComment("Exception that occurs when import fails because the import files contain invalid attributes.")
    @RBArgComment0("Identity of the object being imported.")
    @RBArgComment1("the attribute name")
    @RBArgComment2("the value of the invalid attribute")
    @RBArgComment3("the element name that contains the invalid attribute")
    public static final String UNKNOWN_ATTRIBUTE_EXCEPTION = "UNKNOWN_ATTRIBUTE_EXCEPTION";

    @RBEntry("Impossibile importare \"{0}\". Autorizzazione non disponibile per il partecipante identificato dall'elemento \"{1}\" o il partecipante non esiste.")
    @RBComment("Exception that occurs when import fails because the participant is not found and conflict handling was either not called or did not resolve this issue.")
    @RBArgComment0("Identity of the object being imported")
    @RBArgComment1("Name of the owner attribute")
    public static final String PARTICIPANT_NOT_FOUND_EXCEPTION = "PARTICIPANT_NOT_FOUND_EXCEPTION";

    @RBEntry("Il valore del parametro \"{0}\" non può includere \"{1}\". Questo valore è valido solo quando il valore del parametro \"{2}\" è impostato su \"{3}\".")
    @RBComment("WTInvalidParameterException: A conflict resolutions parameter value is not valid for the specified conflict type parameter.")
    @RBArgComment0("Name of conflict resolutions parameter")
    @RBArgComment1("Value of conflict resolutions parameter")
    @RBArgComment2("Name of conflict type parameter")
    @RBArgComment3("Value included in the set of conflict resolutions that is not valid")
    public static final String CONFLICTING_RESOLUTIONS_EXCEPTION = "CONFLICTING_RESOLUTIONS_EXCEPTION";

    @RBEntry("La risoluzione di default \"{0}\" non esiste nell'insieme di tutte le risoluzioni disponibili.")
    @RBComment("WTInvalidParameterException: the default resolution does not exist in the set of all available resolutions")
    @RBArgComment0("Name of the default resolution")
    public static final String NON_EXISTENT_DEFAULT_RESOLUTION_EXCEPTION = "NON_EXISTENT_DEFAULT_RESOLUTION_EXCEPTION";

    @RBEntry("Impossibile creare il campo aziendale ad hoc '{0}'.")
    @RBArgComment0("The adhoc business filed name")
    public static final String ADHOC_BUSINESS_FIELD_CANNOT_CREATED = "ADHOC_BUSINESS_FIELD_CANNOT_CREATED";

    @RBEntry("Impossibile trovare i campi aziendali '{0}' di '{1}' nel sistema corrente.")
    @RBArgComment0("Business field name.")
    @RBArgComment1("Object identifier of the object being imported.")
    public static final String BUSINESS_FIELD_NOT_FOUND = "BUSINESS_FIELD_NOT_FOUND";

    @RBEntry("I campi aziendali '{0}' di '{1}' non sono compatibili con le definizioni di campo aziendale nel sistema corrente.")
    @RBArgComment0("Business field names.")
    @RBArgComment1("Object identifier of the object being imported.")
    public static final String BUSINESS_FIELD_DATA_STRUCTURE_NOT_MATCHED = "BUSINESS_FIELD_DATA_STRUCTURE_NOT_MATCHED";

    @RBEntry("Impossibile trovare il gestore ObjectRef per la classe \"{0}\" referenziata da \"{1}\" di \"{2}\".")
    @RBArgComment0("name of the class of the referenced object.")
    @RBArgComment1("name of the XML attribute that refers the object reference.")
    @RBArgComment2("id of the object to be imported.")
    public static final String OBJECT_REF_HANDLER_NOT_FOUND = "OBJECT_REF_HANDLER_NOT_FOUND";
}
