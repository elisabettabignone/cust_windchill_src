/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.ixb.publicforhandlers.imp;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBArgComment1;
import wt.util.resource.RBArgComment2;
import wt.util.resource.RBArgComment3;
import wt.util.resource.RBArgComment4;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.ixb.publicforhandlers.imp.IXBImpConflictRB")
public final class IXBImpConflictRB extends WTListResourceBundle {
   @RBEntry("Exceptional Handling occured while performing import conflict diagnostics on the object\"{0}\"")
   public static final String ERROR_DURING_CONFLICT_CHECK = "0";

   @RBEntry("\"{0}\" cannot be imported because the folder \"{1}\" does not exist in the context \"{2}\". ")
   public static final String FOLDER_NOT_FOUND = "1";

   @RBEntry("\"{0}\" cannot be imported because the cabinet \"{1}\" cannot be found or the user does not have access to the cabinet from context \"{2}\". ")
   public static final String CABINET_NOT_FOUND = "2";

   @RBEntry("\"{0}\" cannot be imported because the team \"{1}\" cannot be found in the Administrative Domain \"{2}\".")
   public static final String TEAM_NOT_FOUND = "3";

   @RBEntry("Definition of View \"{0}\" cannot be found or the config spec is not set. See the Windchill System Administrator's Guide for further information.")
   public static final String VIEW_NOT_FOUND = "5";

   @RBEntry("\"{0}\" cannot be imported because the life cycle \"{1}\"  cannot be found.")
   public static final String LC_TEMPLATE_NOT_FOUND = "6";

   @RBEntry("\"{0}\" cannot be imported because the life cycle state \"{1}\" is not valid for life cycle \"{2}\". ")
   public static final String LC_STATE_NOT_FOUND = "7";

   @RBEntry("\"{0}\" cannot be imported because set state permission is not granted.")
   public static final String SET_STATE_DENIED = "SET_STATE_DENIED";

   @RBEntry("\"{0}\" cannot be imported because reassign lifecycle permission is not granted.")
   public static final String REASSIGN_LIFECYCLE_NOT_PERMITTED = "REASSIGN_LIFECYCLE_NOT_PERMITTED";

   @RBEntry("Life Cycle State \"{0}\" cannot be found because the Life Cycle Template \"{1}\", to which the State belongs, does not exist.")
   public static final String LC_STATE_TEMPLATE_IS_MISSING = "8";

   @RBEntry("Definition of Instance Based Attribute \"{0}\" cannot be found. See the Windchill System Administrator's Guide for further information.")
   public static final String IBA_DEFINITION_NOT_FOUND = "9";

   @RBEntry("Attribute definition \"{0}\" is defined locally as \"{1}\", but is imported as \"{2}\". See the Windchill System Administrator's Guide for further information.")
   public static final String IBA_DEFINITION_INCONSISTENT = "10";

   @RBEntry("(Object Id is: \"{0}\" )")
   public static final String OBJECT_ID_NAME = "11";

   @RBEntry("Conflicts occurred while performing import conflict diagnostics. Import is aborted.")
   public static final String CONFLICTS_FOUND_ABORT = "12";

   @RBEntry("Warning!!! Conflicts occured while performing import conflict diagnostics.")
   public static final String CONFLICTS_FOUND_WARN = "13";

   @RBEntry("\t")
   public static final String EMPTY_LINE = "14";

   @RBEntry("Folder path must start with \"/\" \n The current folder is : \"{0}\"  !!!")
   public static final String WRONG_FOLDER_START = "15";

   @RBEntry("Cabinet/Folder")
   public static final String FOLDER = "17";

   @RBEntry("Lifecycle")
   public static final String LIFECYCLE = "18";

   @RBEntry("Lifecycle State")
   public static final String LIFECYCLE_STATE = "19";

   @RBEntry("Team")
   public static final String TEAM = "20";

   @RBEntry("View")
   public static final String VIEW = "21";

   @RBEntry("BOM Type")
   public static final String VARIATION1 = "VARIATION1";

   @RBEntry("Alternate Number")
   public static final String VARIATION2 = "VARIATION2";

   @RBEntry("Version")
   public static final String VERSION = "22";

   @RBEntry("Version Level")
   public static final String VERSION_LEVEL = "23";

   @RBEntry("Iteration")
   public static final String ITERATION = "24";

   @RBEntry("Title")
   public static final String TITLE = "25";

   @RBEntry("Description")
   public static final String DESCRIPTION = "26";

   @RBEntry("Department")
   public static final String DEPARTMENT = "27";

   @RBEntry("Part Type")
   public static final String PART_TYPE = "28";

   @RBEntry("Source")
   public static final String SOURCE = "29";

   @RBEntry("Name")
   public static final String NAME = "30";

   @RBEntry("Document Type")
   public static final String DOCTYPE = "31";

   @RBEntry("Quantity: Amount")
   public static final String QUANTITY_AMOUNT = "32";

   @RBEntry("Quantity: Unit")
   public static final String QUANTITY_UNIT = "33";

   @RBEntry("Document Sub Type")
   public static final String DOC_SUBTYPE = "34";

   @RBEntry("CAD Name")
   public static final String CADNAME = "35";

   @RBEntry("Authoring Application")
   public static final String AUTHAPP = "36";

   @RBEntry("Owner Application")
   public static final String OWNERAPP = "37";

   @RBEntry("Default Trace Code")
   public static final String DEFAULT_TRACE_CODE = "38";

   @RBEntry("End Item")
   public static final String END_ITEM = "39";

   @RBEntry("Trace Code")
   public static final String TRACE_CODE = "40";

   @RBEntry("Job Authorization Number")
   public static final String JOB_AUTHORIZATION_NUMBER = "41";

   @RBEntry("Contract Number")
   public static final String CONTRACT_NUMBER = "42";

   @RBEntry("Phase")
   public static final String PHASE = "43";

   @RBEntry("Business Rule Unique Identifier")
   public static final String BUSINESS_RULE_KEY = "44";

   @RBEntry("Business Rule Set Unique Identifier")
   public static final String BUSINESS_RULE_SET_KEY = "45";

   @RBEntry("\"{1}\" already exists, but has a different value for the attribute: \"{0}\"; existing value: \"{3}\", new value: \"{2}\".")
   @RBComment("Used by IX Handlers when import fails because the object already exists, but has a different value for the specified attribute.")
   @RBArgComment0("name of the attribute that is different between that persisted and that mentioned in the import.")
   @RBArgComment1("Object identifier of the object being imported.")
   @RBArgComment2("value of the attribute from the import file.")
   @RBArgComment3("value of the attribute currently persisted.")
   public static final String EXISTING_OBJECT_ATTRIBUTE_CONFLICT = "50";

   @RBEntry("Object \"{1}\" already exists in database, but has different value for Instance Based Attribute \"{0}\": existing value is \"{3}\", new value  is \"{2}\".")
   public static final String EXISTING_OBJECT_IBA_VALUE_CONFLICT = "51";

   @RBEntry("Object \"{1}\" already exists in database, but has different type of Instance Based Attribute \"{0}\": existing type is \"{3}\", new type  is \"{2}\".")
   public static final String EXISTING_OBJECT_IBA_TYPE_CONFLICT = "52";

   @RBEntry("Object \"{1}\" already exists in database, but has different unit precision of Instance Based Attribute \"{0}\": existing unit precision is \"{3}\", new unit precision  is \"{2}\".")
   public static final String EXISTING_OBJECT_IBA_UNIT_PRECISION_CONFLICT = "53";

   @RBEntry("Object \"{1}\" already exists in database, but has different ratio denominator of Instance Based Attribute \"{0}\": existing ratio denominator is \"{3}\", new ratio denominator  is \"{2}\".")
   public static final String EXISTING_OBJECT_IBA_RATIO_DENOMINATOR_CONFLICT = "54";

   /**
    * Project contants for the implementation for backward compatible with R 6.0
    **/
   @RBEntry("Project \"{0}\" cannot be found in the Administrative Domain \"{1}\". See the Windchill System Administrator's Guide for further information.")
   public static final String PROJECT_NOT_FOUND = "55";

   @RBEntry("Project \"{0}\" cannot be found because the Administrative Domain \"{1}\", where the Project resides, does not exist.")
   public static final String PROJECT_DOMAIN_IS_MISSING = "56";

   @RBEntry("Full project name must consist of \"domain name\".\"project name\"\n The current full project name is : \"{0}\" !!!")
   public static final String WRONG_FULL_PROJECT_NAME = "57";

   @RBEntry("Project")
   public static final String PROJECT = "58";

   @RBEntry("Object number {0} exists with the UFID that is different than the UFID from XML")
   public static final String NUMBER_EXISTS_WITH_DIFFERENT_UFID = "59";

   @RBEntry("Object name {0} exists with the UFID that is different than the UFID from XML")
   public static final String NAME_EXISTS_WITH_DIFFERENT_UFID = "60";

   @RBEntry("Object {0} exists with the UFID that is different than the UFID from XML")
   public static final String IDENTICAL_OBJECT_EXISTS_WITH_DIFFERENT_UFID = "IDENTICAL_OBJECT_EXISTS_WITH_DIFFERENT_UFID";

   /**
    * For display Units....
    **/
   @RBEntry("Measurement system\"{0}\" exists with different base symbol value.")
   @RBArgComment0("The name of Measurement system.")
   public static final String MS_NAME_EXISTS_WITH_DIFFERENT_UNITS = "61";

   @RBEntry("Measurement system\"{0}\" does not exist. ")
   @RBArgComment0("The name of Measurement system.")
   public static final String MEASUREMENT_SYSTEM_NOT_EXIST = "62";

   @RBEntry("Quantity of measure \"{0}\" exists with different display units or overrided display units.")
   @RBArgComment0("The name of Quantity of measure .")
   public static final String QOM_NAME_EXISTS_WITH_DIFFERENT_UNITS = "qom_conflicts";

   @RBEntry("Quantity of measure \"{0}\" does not exist. ")
   @RBArgComment0("The name of Quantity of measure.")
   public static final String QUANTITY_OF_MEASURE_NOT_EXIST = "qom_not_exist";

   @RBEntry("The Quantity of measure \"{0}\" for UnitDefinition \"{1}\" exists with different display units or overrided display units.")
   @RBArgComment0("The name of Quantity of measure .")
   @RBArgComment1("The name of UnitDefinition.")
   public static final String UNIT_DEF_NAME_EXISTS_WITH_DIFFERENT_UNITS = "unit_def_conflicts";

   /**
    * For soft type defintions ...
    **/
   @RBEntry("Object existed with a different type. Existed type: {0}; expected type: {1}.")
   @RBArgComment0("External type identifier, e.g. WCTYPE|wt.part.WTPart|MyExistedSoftType")
   @RBArgComment1("External type identifier, e.g. WCTYPE|wt.part.WTPart|MyExpectedSoftType")
   public static final String INCOMPARTIBLE_TYPE_ID = "incompartible_type_id";

   @RBEntry("Type Definition cannot be found in target or parent contexts: \"{0}\"")
   @RBArgComment0("Path of the WTTypeDefinition, e.g. wt.part.WTPart/MyExpectedSoftType")
   public static final String TYPE_DEFINITION_NOT_EXIST = "type_def_not_exist";

   @RBEntry("Incompartible attribute \"instantiable\" for: \"{0}\", expected: \"{1}\", found: \"{2}\"")
   @RBArgComment0("Path of the WTTypeDefinition, e.g. wt.part.WTPart/MyExpectedSoftType")
   @RBArgComment1("Expected value for instantiable: true or false")
   @RBArgComment2("Existed value for instantiable: true or false")
   public static final String TYPE_DEF_INSTANTIABLE_INCOMPARTIBLE = "type_def_instantiable_incompartible";

   @RBEntry("Incompartible attribute \"deleted\" for: \"{0}\", expected: \"{1}\", found: \"{2}\"")
   @RBArgComment0("Path of the WTTypeDefinition, e.g. wt.part.WTPart/MyExpectedSoftType")
   @RBArgComment1("Expected value for deleted: true or false")
   @RBArgComment2("Existed value for deleted: true or false")
   public static final String TYPE_DEF_DELETED_INCOMPARTIBLE = "type_def_deleted_incompartible";

   @RBEntry("IBA value (attribute type: \"{1}\", path: \"{2}\", value: \"{3}\") is expected with respect to import for: \"{0}\"")
   @RBArgComment0("Path of the WTTypeDefinition, e.g. wt.part.WTPart/MyExistedSoftType")
   @RBArgComment1("Attribute type, e.g. IntegerValue, ReferenceValue")
   @RBArgComment2("Path (without AttributeOrganizer) of the IBA value")
   @RBArgComment3("value of the IBA value")
   public static final String IBA_VALUE_EXPECTED_FOR_TYPE_DEF = "ibavalue_expected_for_type_def";

   @RBEntry("Extra IBA value (attribute type: \"{1}\", path: \"{2}\", value: \"{3}\") is found with respect to import for: \"{0}\"")
   @RBArgComment0("Path of the WTTypeDefinition, e.g. wt.part.WTPart/MyExistedSoftType")
   @RBArgComment1("Attribute type, e.g. IntegerValue, ReferenceValue")
   @RBArgComment2("Path (without AttributeOrganizer) of the IBA value")
   @RBArgComment3("value of the IBA value")
   public static final String EXTRA_IBA_VALUE_FOR_TYPE_DEF = "extra_ibavalue_for_type_def";

   @RBEntry("Type constraint (enforcementRuleClassname: \"{1}\", bindingRuleClassName: \"{2}\", enforcementRuleData: \"{3}\", IBA definition path: : \"{4}\") is expected with respect to import for: \"{0}\"")
   @RBArgComment0("Path of the WTTypeDefinition, e.g. wt.part.WTPart/MyExistedSoftType")
   @RBArgComment4("Path of IBA definitions")
   public static final String TYPE_CONSTRAINT_EXPECTED_FOR_TYPE_DEF = "constraint_expected_for_type_def";

   @RBEntry("Extra type constraint (enforcementRuleClassname: \"{1}\", bindingRuleClassName: \"{2}\", enforcementRuleData: \"{3}\", IBA definition path: : \"{4}\") is found with respect to import for: \"{0}\"")
   @RBArgComment0("Path of the WTTypeDefinition, e.g. wt.part.WTPart/MyExistedSoftType")
   @RBArgComment4("Path of IBA definitions")
   public static final String EXTRA_TYPE_CONSTRAINT_FOR_TYPE_DEF = "extra_constraint_for_type_def";

   @RBEntry("The icon \"{0}\" already exists. Override this conflict will rename the icon to a different name.")
   @RBArgComment0("Path of the icon file, e.g. wt/icons/myTypeIconImage.gif")
   public static final String ICON_FILE_EXIST = "icon_file_exist";

   @RBEntry("Attributes are different for \"{0}\". The value of attribute \"{3}\" is different from the value as found in database. Expected: \"{1}\", found: \"{2}\"")
   @RBArgComment0("object id such as client image")
   @RBArgComment1("The value in the XML file")
   @RBArgComment2("The value in the database")
   @RBArgComment3("The attribute tag name")
   public static final String GENERAL_ATTR_DIFF = "general_attr_diff";

   @RBEntry("The identified group of logical identifier is different: existing: \"{0}\"; expected: \"{1}\".")
   public static final String INCONSISTENT_IDENTIFIED_GROUP = "inconsistent_id_grp";

   @RBEntry("The logical identifier is different: existing: \"{0}\"; expected: \"{1}\".")
   public static final String INCONSISTENT_LOGICAL_ID = "inconsistent_logic_id";

   /**
    * End of Type Definitions
    **/
   @RBEntry("\"{0}\" cannot be imported because the domain \"{1}\" cannot be found or the user does not have access to the domain from context \"{2}\". ")
   public static final String DOMAIN_NOT_FOUND = "63";

   @RBEntry("Line Number")
   public static final String LINE_NUMBER = "64";

   @RBEntry("Default Unit")
   public static final String DEFAULT_UNIT = "65";

   @RBEntry("Domain")
   public static final String DOMAIN = "66";

   @RBEntry("Domain path must start with \"/\". The current path is : \"{0}\"  !!!")
   public static final String WRONG_DOMAIN_START = "67";

   @RBEntry("Error getting folder path of object \"{0}\" or the user does not have access to some parent folder.")
   public static final String CANNOT_GET_FOLDER_PATH = "68";

   @RBEntry("One-off Version")
   public static final String ONE_OFF_VERSION = "69";

   @RBEntry("Version Series")
   public static final String VERSION_SERIES = "70";

   @RBEntry("Variance Category")
   public static final String VARIANCE_CATEGORY = "71";

   @RBEntry("Variance Owner")
   public static final String VARIANCE_OWNER = "72";

   @RBEntry("Need Date")
   public static final String NEED_DATE = "73";

   @RBEntry("Recurring")
   public static final String RECURRING = "74";

   @RBEntry("Effect On Support")
   public static final String EFFECT_ON_SUPPORT = "75";

   @RBEntry("Corrective Action")
   public static final String CORRECTIVE_ACTION = "76";

   @RBEntry("Effect On Cost")
   public static final String EFFECT_ON_COST = "77";

   @RBEntry("Effect On Schedule")
   public static final String EFFECT_ON_SCHEDULE = "78";

   @RBEntry("Business Decision Summary")
   public static final String BUSINESS_DECISION_SUMMARY = "79";

   @RBEntry("Recurring Cost Estimate")
   public static final String RECURRING_COST_EST = "80";

   @RBEntry("Non Recurring Cost Estimate")
   public static final String NON_RECURRING_COST_EST = "81";

   @RBEntry("Request Priority")
   public static final String REQUEST_PRIORITY = "82";

   @RBEntry("Category")
   public static final String CATEGORY = "83";

   @RBEntry("Complexity")
   public static final String COMPLEXITY = "84";

   @RBEntry("Cycle Time")
   public static final String CYCLE_TIME = "85";

   @RBEntry("Resolution Date")
   public static final String RESOLUTION_DATE = "86";

   @RBEntry("Occurrence Date")
   public static final String OCCURRENCE_DATE = "OCCURRENCE_DATE";

   @RBEntry("Investigation Required")
   public static final String INVESTIGATION_REQUIRED = "INVESTIGATION_REQUIRED";

   @RBEntry("Investigation Completed")
   public static final String INVESTIGATION_COMPLETED = "INVESTIGATION_COMPLETED";

   @RBEntry("New Value")
   public static final String NEW_VALUE = "87";

   @RBEntry("Old Value")
   public static final String OLD_VALUE = "88";

   @RBEntry("Change Date")
   public static final String CHANGE_DATE = "89";

   @RBEntry("User")
   public static final String USER = "90";

   @RBEntry("Business Decision Category")
   public static final String BUSINESS_DECISION_CATEGORY = "91";

   @RBEntry("Requester")
   public static final String REQUESTER = "92";

   @RBEntry("Confirmation Category")
   public static final String CONFIRMATION_CATEGORY = "93";

   @RBEntry("Issue Priority")
   public static final String ISSUE_PRIORITY = "94";

   @RBEntry("Issue Results")
   public static final String RESULTS = "95";

   @RBEntry("Change Notice Complexity")
   public static final String CHANGE_NOTICE_COMPLEXITY = "96";

   @RBEntry("Find Number")
   public static final String FIND_NUMBER = "97";

   @RBEntry("Allocation Type")
   public static final String ALLOCATION_TYPE = "98";

   @RBEntry("Control Branch")
   public static final String CONTROL_BRANCH = "99";

   @RBEntry("Execution Mode")
   public static final String EXECUTION_MODE = "100";

   @RBEntry("Populate Affected Objects")
   public static final String POPULATE_AFFECTED_OBJECTS = "populateAffectedObjects";

   @RBEntry("Proposed Solution")
   public static final String PROPOSED_SOLUTION = "101";

   @RBEntry("TeamTemplate from XML file is in the domain \"{0}\", but the object is in the domain \"{1}\" . You must check the box \"Resolve Overridable Conflicts\" if you want to import the object.")
   public static final String TEAM_TEMPLATE_AND_OBJECT_DOMAIN_ARE_IN_DIFFERENT_POSITION = "team_template_and_object_domain_are_in_different_postion";

   @RBEntry("\"{0}\" cannot be imported because the owning organization cannot be found with either the organization identifier \"{1}\" or the organization name \"{2}\". ")
   public static final String ORGANIZATION_NOT_FOUND = "org_not_found";

   @RBEntry("\"{0}\" cannot be imported because the owning organization \"{1}\" cannot be accessed.")
   public static final String ORGANIZATION_NO_ACCESS = "org_no_access";

   @RBEntry("Attribute Organization \"{0}\" cannot be found. See the Windchill Business Administrator's Guide for further information.")
   @RBArgComment0("Full path of the Attribute Organization")
   public static final String ATTRIBUTE_ORGANIZATION_NOT_FOUND = "attr_org_not_found";

   @RBEntry("Hierarchical Definition \"{1}\" is not allowed.")
   @RBArgComment0("Full path of the IBA")
   @RBArgComment1("FPath of the IBA without Attribute Organization")
   public static final String NESTED_IBA_NOT_ALLOWED = "nested_iba_not_allowed";

   @RBEntry("The existing Object \"{1}\" is in the database, but it has more values for Instance Based Attribute \"{0}\". The values are \"{2}\".")
   @RBArgComment0("The path of the IBA Definition  ")
   @RBArgComment1("Object identifier of the IBA holder")
   @RBArgComment2("The extra IBA value that the existing IBA holder has")
   public static final String EXISTING_OBJECT_HAS_MORE_IBA_VALUE = "has_more_iba_value";

   @RBEntry("The existing Object \"{1}\" is in the database, but it is mising a value for Instance Based Attribute \"{0}\". The value is \"{2}\".")
   @RBArgComment0("The path of the IBA Definition")
   @RBArgComment1("Object identifier of the IBA holder")
   @RBArgComment2("The missing IBA value that the existing IBA holder has")
   public static final String EXISTING_OBJECT_HAS_FEWER_IBA_VALUE = "has_fewer_iba_value";

   @RBEntry("The existing Object \"{1}\" is in the database, but it has a different value for Instance Based Attribute \"{0}\". The value is \"{2}\".")
   public static final String OBJID_MATHCES_IBA_PATH_VALUE_DIFFER = "iba_objid_match_path_value_differ";

   @RBEntry("BusinessEntity cannot be found: \"{0}\"")
   @RBArgComment0("name of the BusinessEntity object")
   public static final String BUSINESS_ENTITY_NOT_EXIST = "business_entity_not_exist";

   @RBEntry("ClassificationNode cannot be found: \"{0}\"")
   @RBArgComment0("Path of the ClassificationNode, e.g. node1/node2")
   public static final String CLASSIFICATION_NODE_NOT_EXIST = "classification_node_not_exist";

   @RBEntry("ClassificationStruct does not exist but presented in XML file for Classification Node: \"{0}\"")
   @RBArgComment0("Path of the ClassificationNode, e.g. node1/node2")
   public static final String CLASSIFICATION_STRUCT_NOT_EXIST = "classification_struct_not_exist";

   @RBEntry("ClassificationStruct exists but it is not presented in XML file for Classification Node: \"{0}\"")
   @RBArgComment0("Path of the ClassificationNode, e.g. node1/node2")
   public static final String CLASSIFICATION_STRUCT_EXTRA = "classification_struct_extra";

   @RBEntry("ClassificationStruct has a different attribute \"className\" for Classification Node \"{0}\": the value is \"{1}\" in XML file and \"{2}\" in database.")
   @RBArgComment0("Path of the ClassificationNode, e.g. node1/node2")
   public static final String CLASSIFICATION_STRUCT_CLASSNAME_DIFF = "classification_struct_classname_diff";

   @RBEntry("ClassificationStruct has a different ReferenceDefinition for Classification Node \"{0}\": the ReferenceDefinition is \"{1}\" in XML file and \"{2}\" in database.")
   @RBArgComment0("Path of the ClassificationNode, e.g. node1/node2")
   @RBArgComment1("Path of the RefernceDefinition without the Attribute Organizer")
   @RBArgComment2("Path of the RefernceDefinition without the Attribute Organizer")
   public static final String CLASSIFICATION_STRUCT_REFERENCE_DIFF = "classification_struct_refdef_diff";

   @RBEntry("Minimum Required")
   public static final String MINREQUIRED = "MINREQUIRED";

   @RBEntry("Maximum Allowed")
   public static final String MAXALLOWED = "MAXALLOWED";

   @RBEntry("\"{0}\" : attribute value was not available during export of the object \"{1}\" due to access permission. ")
   public static final String ATTRIB_VALUE_NOT_AVAILABLE = "attrib_value_not_available";

   @RBEntry("\"{0}\" : attribute value was not available during export of the object \"{1}\" due to access permission. The default value \"{2}\" is used.")
   public static final String ATTRIB_VALUE_NOT_AVAILABLE_DEFAULT = "attrib_value_not_available_default";

   @RBEntry("\"{0}\" : attribute value was not available during export due to access permission. It was ignored.")
   public static final String ATTRIB_VALUE_NOT_AVAILABLE_IGNORED = "attrib_value_not_available_ignored";

   @RBEntry("\"{0}\" : attribute value was not available during export due to access permission. Provide a valid value before import.")
   public static final String ACTION_NEEDED_FOR_SECURED_INFO = "action_needed_for_secured_info";

   @RBEntry("The existing owning repository for the object is \"{0}\" in the database, but it has a different value in XML. The value is \"{1}\".")
   public static final String DIFFERENT_OWNING_REPOSITORY = "owning_repository_diff";

   @RBEntry("The existing last known source repository for the object is \"{0}\" in the database, but it has a different value in xml. The value is \"{1}\".")
   public static final String DIFFERENT_LASTKNOWN_REPOSITORY = "lastknown_repository_diff";

   @RBEntry(" Object is not federatable object")
   public static final String OBJECT_NOT_FEDERATABLE = "object_not_federatable";

   @RBEntry("Phantom")
   public static final String PHANTOM = "PHANTOM";

   @RBEntry("Collapsible")
   public static final String COLLAPSIBLE = "COLLAPSIBLE";

   @RBEntry("\"{0}\" : object cannot be imported due to access permission.")
   public static final String SECURED_INFO_CONFLICT = "SECURED_INFO_CONFLICT";

   @RBEntry("\"{0}\" cannot be imported because the life cycle \"{1}\"  is not valid for this object type.")
   public static final String LC_TEMPLATE_NOT_SUPPORTED = "102";

   @RBEntry("\"{0}\" cannot be imported because the owning organization is marked as secured information. ")
   public static final String ORGANIZATION_ID_MARKED_AS_SECURED = "103";

   @RBEntry("\"{0}\" cannot be imported because the life cycle is marked as secured information. ")
   public static final String LIFECYCLE_MARKED_AS_SECURED = "104";

   @RBEntry("\"{0}\" cannot be imported because the life cycle state is marked as secured information. ")
   public static final String LIFECYCLE_STATE_MARKED_AS_SECURED = "105";

   @RBEntry("\"{0}\" cannot be imported because the revision \"{1}\" is invalid. ")
   public static final String VERSION_NOT_FOUND = "106";

   @RBEntry("\"{0}\" cannot be imported because the revision \"{1}\" (position {2}) is invalid. ")
   public static final String VERSION_POSITION_NOT_FOUND = "VERSION_POSITION_NOT_FOUND";

   @RBEntry("\"{0}\" cannot be imported because the folder \"{1}\" does not exist or the user does not have access to it in the context \"{2}\". ")
   public static final String FOLDER_NO_ACCESS = "107";

   @RBEntry("\"{0}\" cannot be imported because the folder is marked as secured information. ")
   public static final String FOLDER_MARKED_AS_SECURED = "108";

   @RBEntry("\"{0}\" already exists.")
   public static final String EXISTING_OBJECT_CONFLICT = "EXISTING_OBJECT_CONFLICT";

   @RBEntry(" \"{0}\" cannot be imported because the current user does not have the necessary permissions.")
   @RBArgComment0("Object identifier of the object being imported.")
   public static final String NOT_AUTHORIZED = "NOT_AUTHORIZED";

   @RBEntry("Context")
   public static final String CONTEXT = "CONTEXT";

   @RBEntry("\"{1}\" : User cannot be found on the target system for \"{0}\".")
   public static final String PRINCIPAL_NOT_FOUND = "PRINCIPAL_NOT_FOUND";

   @RBEntry("The datatype of importing attribute \"{0}\" in type \"{1}\" is different with the datatype of existing attribute.")
   public static final String INCOMPARTIBLE_ATTRIBUTE_DATATYPE = "INCOMPARTIBLE_ATTRIBUTE_DATATYPE";

   @RBEntry("Reason ")
   public static final String REASON = "109";

   @RBEntry("Approved Quantity ")
   public static final String APPROVED_QUANTITY = "110";

   @RBEntry(" \"{0}\" cannot be imported because the number is not valid.")
   public static final String NUMBER_NOT_FOUND = "111";

   @RBEntry("Hide Part In Structure")
   public static final String HIDE_PART_IN_STRUCTURE = "112";

   @RBEntry(" \"{0}\" cannot be imported because the context \"{1}\" does not exist.")
   public static final String CONTEXT_NOT_FOUND = "113";

   @RBEntry("\"{0}\" cannot be imported because the context is marked as secured information. ")
   public static final String CONTAINER_MARKED_AS_SECURED = "114";

   @RBEntry("\"{0}\" cannot be imported because the required property \"{1}\" for resolution \"{2}\" is not set. ")
   public static final String PROPERTY_NOT_FOUND = "115";

   @RBEntry("\"{0}\" cannot be imported because another locally created object already exists on the system with the same identifying information. ")
   public static final String LOCAL_OBJECT_EXISTS = "116";

   @RBEntry("\"{0}\" cannot be imported because an object imported from another system exists on the system with the same identifying information. ")
   public static final String REMOTE_OBJECT_EXISTS = "117";


   @RBEntry("Serviceable")
   public static final String XML_ATTR_SERVICEABLE = "SERVICEABLE";

   @RBEntry("Service Kit")
   public static final String XML_ATTR_SERVICE_KIT = "SERVICE_KIT";

   @RBEntry("\"{0}\" cannot be imported because the \"{1}\" object type does not exist on this system.")
   public static final String SOLUTION_NOT_FOUND = "SOLUTION_NOT_FOUND";

   @RBEntry("The context value was not available during export because the exporting user does not have the appropriate permissions. ")
   public static final String ATTRIBUTE_CONTAINER_MARKED_AS_SECURED = "118";

   @RBEntry("The context \"{0}\" does not exist.")
   public static final String ATTRIBUTE_CONTEXT_NOT_FOUND = "119";

   @RBEntry("The cabinet \"{0}\" cannot be found or the user does not have access to the cabinet from context \"{1}\". ")
   public static final String ATTRIBUTE_CABINET_NOT_FOUND = "120";

   @RBEntry("The folder \"{0}\" does not exist in the context \"{1}\". ")
   public static final String ATTRIBUTE_FOLDER_NOT_FOUND = "121";

   @RBEntry("The domain \"{0}\" cannot be found or the user does not have access to the domain from context \"{1}\". ")
   public static final String ATTRIBUTE_DOMAIN_NOT_FOUND = "122";

   @RBEntry("The folder value was not available during export because the exporting user does not have the appropriate permissions. ")
   public static final String ATTRIBUTE_FOLDER_MARKED_AS_SECURED = "123";

   @RBEntry("\"{0}\" : attribute value was not available during export because the exporting user does not have the appropriate permissions. The default value \"{1}\" is used.")
   public static final String ATTRIB_VALUE_NOT_AVAILABLE_DEFAULT_GENERIC = "attrib_value_not_available_default_generic";

   @RBEntry("\"{0}\" : attribute value was not available during export because the exporting user does not have the appropriate permissions. ")
   public static final String ATTRIB_VALUE_NOT_AVAILABLE_GENERIC = "attrib_value_not_available_generic";

   @RBEntry("The life cycle state value was not available during export because the exporting user does not have the appropriate permissions. ")
   public static final String ATTRIBUTE_LIFECYCLE_MARKED_AS_SECURED = "124";

   @RBEntry("The life cycle state value was not available during export because the exporting user does not have the appropriate permissions. ")
   public static final String ATTRIBUTE_LIFECYCLE_STATE_MARKED_AS_SECURED = "125";

   @RBEntry("The life cycle \"{0}\"  cannot be found.")
   public static final String ATTRIBUTE_LC_TEMPLATE_NOT_FOUND = "126";

   @RBEntry("The life cycle \"{0}\"  is not valid for the object type \"{1}\".")
   public static final String ATTRIBUTE_LC_TEMPLATE_NOT_SUPPORTED = "127";

   @RBEntry("The life cycle state \"{0}\" is not valid for life cycle \"{1}\". ")
   public static final String ATTRIBUTE_LC_STATE_NOT_FOUND = "128";

   @RBEntry("The team \"{0}\" cannot be found in the Administrative Domain \"{1}\".")
   public static final String ATTRIBUTE_TEAM_NOT_FOUND = "129";

   @RBEntry("The owning organization cannot be found with either the organization identifier \"{0}\" or the organization name \"{1}\". ")
   public static final String ATTRIBUTE_ORGANIZATION_NOT_FOUND_GENERIC = "org_not_found_generic";

   @RBEntry("Can't find sourcing context \"{0}\" on this server. ")
   public static final String SOURCING_CONTEXT_NOT_FOUND = "SOURCING_CONTEXT_NOT_FOUND";

   @RBEntry("You do not have modify access on {0}")
   @RBArgComment0("Object Identity")
   public static final String NO_MODIFY_ACCESS_ON_OBJECT = "130";

   @RBEntry("Build Status")
   public static final String BUILD_STATUS = "131";

   @RBEntry("The user does not have Read permission for the source folder \"{0}\" in the context \"{1}\". ")
   public static final String ATTRIBUTE_FOLDER_NOT_ACCESSIBLE = "132";

   @RBEntry("The user does not have Create and Modify permissions for the source folder \"{0}\" in the context \"{1}\". ")
   public static final String ATTRIBUTE_FOLDER_NOT_MODIFIABLE = "133";

   @RBEntry("The business fields \"{0}\" for \"{1}\" cannot be found in the current system.")
   @RBArgComment0("Business field name.")
   @RBArgComment1("Object identifier of the object being imported.")
   public static final String BUSINESS_FIELD_NOT_FOUND = "BUSINESS_FIELD_NOT_FOUND";

   @RBEntry("The business fields \"{0}\" for \"{1}\" are incompatible with the business field definitions in the current system.")
   @RBArgComment0("Business field name.")
   @RBArgComment1("Object identifier of the object being imported.")
   public static final String BUSINESS_FIELD_DATA_STRUCTURE_NOT_MATCHED = "BUSINESS_FIELD_DATA_STRUCTURE_NOT_MATCHED";

   @RBEntry("The \"{0}\" object referenced by \"{1}\" of \"{2}\" cannot be found.")
   @RBArgComment0("name of the class of the referenced object.")
   @RBArgComment1("name of the XML attribute that refers to the object reference.")
   @RBArgComment2("id of the object to be imported.")
   public static final String OBJECT_REF_NOT_FOUND = "OBJECT_REF_NOT_FOUND";

   @RBEntry("You do not have modify identity access on {0}")
   @RBArgComment0("Object Identity")
   public static final String NO_MODIFY_IDENTITY_ACCESS_ON_OBJECT = "NO_MODIFY_IDENTITY_ACCESS_ON_OBJECT";

   @RBEntry("You do not have modify content access on \"{0}\"")
   @RBArgComment0("Object identifier of the object being imported.")
   public static final String NO_MODIFY_CONTENT_ACCESS_ON_OBJECT = "NO_MODIFY_CONTENT_ACCESS_ON_OBJECT";

   @RBEntry("ATTENTION: Content role type \"{0}\" does not exist for \"{1}\"")
   @RBArgComment0("Missing content role type.")
   @RBArgComment1("Object identifier of the object being imported.")
   public static final String CONTENT_ROLE_TYPE_NOT_FOUND = "CONTENT_ROLE_TYPE_NOT_FOUND";

   @RBEntry("\"{0}\" cannot be imported because the participant was exported as \"{1}\".")
   @RBComment("The conflict message used when import fails because the participant was exported as " +
   		" IxbHndHelperConstants.XML_VALUE_SECURED_INFORMATION")
   @RBArgComment0("Identity of the object being imported.")
   @RBArgComment1("The string defined by IxbHndHelperConstants.XML_VALUE_SECURED_INFORMATION")
   public static final String PARTICIPANT_EXPORTED_AS_SECURED = "PARTICIPANT_EXPORTED_AS_SECURED";

   @RBEntry("The participant was exported as \"{0}\".")
   @RBComment("The conflict message used when called by preview and it is known the import will fail" +
   		" because the participant was exported as IxbHndHelperConstants.XML_VALUE_SECURED_INFORMATION")
   @RBArgComment0("The string defined by IxbHndHelperConstants.XML_VALUE_SECURED_INFORMATION")
   public static final String ATTRIBUTE_PARTICIPANT_EXPORTED_AS_SECURED = "ATTRIBUTE_PARTICIPANT_EXPORTED_AS_SECURED";

   @RBEntry("\"{0}\" already exists, but has a different value for the participant; existing value: \"{1}\", new value:\"{2}\".")
   @RBComment("The conflict message used when import fails because the object already exists, but has a different value for the participant.")
   @RBArgComment0("Identity of the object being imported.")
   @RBArgComment1("Identity of the existing participant value.")
   @RBArgComment2("Identity of the participant in the import file.")
   public static final String EXISTING_OBJECT_PARTICIPANT_CONFLICT = "EXISTING_OBJECT_PARTICIPANT_CONFLICT";

   @RBEntry("\"{0}\" cannot be imported because the user \"{1}\" does not exist.")
   @RBComment("The conflict message used when import fails because the user does not exist but can be replicated.")
   @RBArgComment0("Identity of the object being imported.")
   @RBArgComment1("Identity of the user that does not exist.")
   public static final String USER_NOT_FOUND_MAY_BE_REPLICATED = "USER_NOT_FOUND_MAY_BE_REPLICATED";

   @RBEntry("The user \"{0}\" does not exist.")
   @RBComment("The conflict message used when called by preview and it is known the import will fail " +
   		"because the user does not exist but can be replicated.")
   @RBArgComment0("Identity of the user that does not exist.")
   public static final String ATTRIBUTE_USER_NOT_FOUND_MAY_BE_REPLICATED = "ATTRIBUTE_USER_NOT_FOUND_MAY_BE_REPLICATED";

   @RBEntry("\"{0}\" cannot be imported because you do not have authorization for participant \"{1}\" or the participant does not exist.")
   @RBComment("The conflict message used when import fails because the importing user is not authorized to see the " +
   		"participant or the participant does not exist.")
   @RBArgComment0("Identity of the object being imported.")
   @RBArgComment1("Identity of the participant is not authorized to see or does not exist.")
   public static final String PARTICIPANT_NOT_FOUND = "PARTICIPANT_NOT_FOUND";

   @RBEntry("You do not have authorization for participant \"{0}\" or the participant does not exist.")
   @RBComment("The conflict message used when called by preview and it is known the import will fail because the importing user " +
   		"is not authoried to see the participant or the participant does not exist.")
   @RBArgComment0("Identity of the participant that is not authorized to see or does not exist.")
   public static final String ATTRIBUTE_PARTICIPANT_NOT_FOUND = "ATTRIBUTE_PARTICIPANT_NOT_FOUND";

   @RBEntry("\"{0}\" cannot be imported because you do not have authorization to modify the ownership.")
   @RBComment("The conflict message used when import fails because the importing user is not authorized to modify the ownership.")
   @RBArgComment0("Identity of the object being imported.")
   public static final String NOT_AUTHORIZED_TO_MODIFY_OWNERSHIP = "NOT_AUTHORIZED_TO_MODIFY_OWNERSHIP";

   @RBEntry("\"{0}\" cannot be imported because you do not have authorization to modify the security label value \"{1}\"" +
   		" for security label \"{2}\".")
   @RBComment("The conflict message used when import fails because the importing user is not authorized to modify a" +
   		" security label value.")
   @RBArgComment0("Identity of the object being imported.")
   @RBArgComment1("The security label value.")
   @RBArgComment2("The security label name.")
   public static final String NOT_AUTHORIZED_TO_MODIFY_SECURITY_LABELS = "NOT_AUTHORIZED_TO_MODIFY_SECURITY_LABELS";

   @RBEntry("\"{0}\" cannot be imported because the security label value \"{1}\" is not valid for security label \"{2}\".")
   @RBComment("The conflict message used when import fails because the security label value is not valid" +
   		"for the specifiec security label.")
   @RBArgComment0("Identity of the object being imported.")
   @RBArgComment1("The security label value.")
   @RBArgComment2("The security label name.")
   public static final String SECURITY_LABEL_VALUE_INVALID = "SECURITY_LABEL_VALUE_INVALID";

   @RBEntry("The security label value \"{0}\" is not valid for security label \"{1}\".")
   @RBComment("The conflict message used when called by preview and it is known the import will fail because the security label value is not valid for the " +
   		"specified security label")
   @RBArgComment0("The security label value.")
   @RBArgComment1("The security label name.")
   public static final String ATTRIBUTE_SECURITY_LABEL_VALUE_INVALID = "ATTRIBUTE_SECURITY_LABEL_VALUE_INVALID";

   @RBEntry("\"{0}\" cannot be imported because the security label \"{1}\" is not valid.")
   @RBComment("The conflict message used when import fails because the specified security label is not valid.")
   @RBArgComment0("Identity of the object being imported.")
   @RBArgComment1("The security label name.")
   public static final String SECURITY_LABEL_INVALID = "SECURITY_LABEL_INVALID";

   @RBEntry("The security label \"{0}\" is not valid.")
   @RBComment("The conflict message used when called by preview and it is known the import will fail because the security label " +
   		"is not valid.")
   @RBArgComment0("The security label name.")
   public static final String ATTRIBUTE_SECURITY_LABEL_INVALID = "ATTRIBUTE_SECURITY_LABEL_INVALID";

   @RBEntry("The owning Organization is marked as \"{0}\", it can not be imported")
   @RBComment("The conflict message used when called by preview and it is known the import will fail" +
      " because the organization was exported as IxbHndHelperConstants.XML_VALUE_SECURED_INFORMATION")
   @RBArgComment0("The string defined by IxbHndHelperConstants.XML_VALUE_SECURED_INFORMATION")
   public static final String ORGANIZATION_EXPORTED_AS_SECURED = "ORGANIZATION_EXPORTED_AS_SECURED";

    @RBEntry("The source or target language is null or not specified for the following master texts: \"{0}\".")
    @RBArgComment0("Master texts")
    public static final String TDE_LANGUAGE_NOT_SPECIFIED = "TDE_LANGUAGE_NOT_SPECIFIED";

    @RBEntry("The following source or target languages are invalid: \"{0}\".")
    @RBArgComment0("Provided source or target languages")
    public static final String TDE_LANGAUGE_INVALID = "TDE_LANGAUGE_INVALID";

    @RBEntry("The following specified source languages are not present in the Source Languages set: \"{0}\".")
    @RBArgComment0("Specified source languages")
    public static final String TDE_LANGUAGE_NOT_IN_SELECTABLE_SOURCE_LANGUAGE_SET = "TDE_LANGUAGE_NOT_IN_SELECTABLE_SOURCE_LANGUAGE_SET";

    @RBEntry("The following overlapping conflicts are found in translation dictionary \"{0}\": \"{1}\".")
    @RBArgComment0("Translation dictionary name")
    @RBArgComment1("Conflicts")
    public static final String TDE_OVERLAPPING_ENTRIES = "TDE_OVERLAPPING_ENTRIES";

    @RBEntry("The incoming translation dictionary entry \"{0}\" overlaps with the following existing persisted entries: \"{1}\".")
    @RBArgComment0("Incoming entry")
    @RBArgComment1("Persisted entries")
    public static final String TDE_OVERLAPPING_ENTRY = "TDE_OVERLAPPING_ENTRY";

    @RBEntry("The dictionary entries cannot be imported as the current user does not have the appropriate authorization for the following translation dictionary: \"{0}\".")
    @RBArgComment0("Translation dictionary name")
    public static final String TDE_NOT_AUTHORIZED = "TDE_NOT_AUTHORIZED";

    @RBEntry("The Authoring Language attribute value is not specified for the following object: \"{0}\".")
    @RBArgComment0("Object Id of the object for import")
    public static final String AUTHORING_LANGUAGE_NOT_SPECIFIED = "AUTHORING_LANGUAGE_NOT_SPECIFIED";

    @RBEntry("The Authoring Language attribute value is null for the following object: \"{0}\".")
    @RBArgComment0("Object Id of the object for import")
    public static final String AUTHORING_LANGUAGE_NULL = "AUTHORING_LANGUAGE_NULL";

    @RBEntry("The Authoring Language attribute value of \"{0}\" specified on the following object is invalid: \"{1}\".")
    @RBArgComment0("Specified authoring language attribute value")
    @RBArgComment1("Object Id of the object for import")
    public static final String AUTHORING_LANGUAGE_INVALID = "AUTHORING_LANGUAGE_INVALID";

    @RBEntry("The Authoring Language attribute value of \"{0}\" specified on the following object is not present in the Source Languages set: \"{1}\".")
    @RBArgComment0("Specified authoring language attribute value")
    @RBArgComment1("Object Id of the object for import")
    public static final String AUTHORING_LANGUAGE_NOT_IN_SELECTABLE_SOURCE_LANGUAGE_SET = "AUTHORING_LANGUAGE_NOT_IN_SELECTABLE_SOURCE_LANGUAGE_SET";
}
