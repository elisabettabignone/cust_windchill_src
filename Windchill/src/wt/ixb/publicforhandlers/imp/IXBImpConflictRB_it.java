/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.ixb.publicforhandlers.imp;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBArgComment1;
import wt.util.resource.RBArgComment2;
import wt.util.resource.RBArgComment3;
import wt.util.resource.RBArgComment4;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.ixb.publicforhandlers.imp.IXBImpConflictRB")
public final class IXBImpConflictRB_it extends WTListResourceBundle {
   @RBEntry("Evento eccezionale durante l'esecuzione della diagnosi dei conflitti di importazione sull'oggetto\"{0}\"")
   public static final String ERROR_DURING_CONFLICT_CHECK = "0";

   @RBEntry("Impossibile importare \"{0}\". La cartella \"{1}\" non è presente nel contesto \"{2}\". ")
   public static final String FOLDER_NOT_FOUND = "1";

   @RBEntry("Impossibile importare \"{0}\". Non è possibile trovare lo schedario \"{1}\" oppure l'utente non ha accesso allo schedario dal contesto \"{2}\". ")
   public static final String CABINET_NOT_FOUND = "2";

   @RBEntry("Impossibile importare \"{0}\". Non è possibile trovare il team \"{1}\" nel dominio amministrativo \"{2}\". ")
   public static final String TEAM_NOT_FOUND = "3";

   @RBEntry("Impossibile trovare la definizione di vista \"{0}\". La spec di configurazione potrebbe non essere impostata. Consultare la Guida dell'amministratore di sistema Windchill per ulteriori informazioni.")
   public static final String VIEW_NOT_FOUND = "5";

   @RBEntry("Impossibile importare \"{0}\". Ciclo di vita \"{1}\" non trovato.")
   public static final String LC_TEMPLATE_NOT_FOUND = "6";

   @RBEntry("Impossibile importare \"{0}\". Lo stato del ciclo di vita \"{1}\" non è valido per il ciclo di vita \"{2}\". ")
   public static final String LC_STATE_NOT_FOUND = "7";

   @RBEntry("Impossibile importare \"{0}\". Permesso di impostazione dello stato del ciclo di vita non concesso.")
   public static final String SET_STATE_DENIED = "SET_STATE_DENIED";

   @RBEntry("Impossibile importare \"{0}\". Permesso di riassegnazione del ciclo di vita non concesso.")
   public static final String REASSIGN_LIFECYCLE_NOT_PERMITTED = "REASSIGN_LIFECYCLE_NOT_PERMITTED";

   @RBEntry("Impossibile trovare lo stato di ciclo di vita \"{0}\" perché il modello di ciclo di vita \"{1}\", cui lo stato appartiene, non esiste")
   public static final String LC_STATE_TEMPLATE_IS_MISSING = "8";

   @RBEntry("Impossibile trovare la definizione di attributo d'istanza \"{0}\". Consultare la Guida dell'amministratore di sistema Windchill per ulteriori informazioni.")
   public static final String IBA_DEFINITION_NOT_FOUND = "9";

   @RBEntry("L'attributo \"{0}\" è definito localmente come \"{1}\", ma è importato come \"{2}\". Consultare la guida Windchill System Administrator's Guide (Guida dell'amministratore di sistema Windchill) per ulteriori informazioni.")
   public static final String IBA_DEFINITION_INCONSISTENT = "10";

   @RBEntry("(L'Id dell'oggetto è: \"{0}\" )")
   public static final String OBJECT_ID_NAME = "11";

   @RBEntry("Si sono verificati dei conflitti durante l'esecuzione della diagnosi dei conflitti di importazione. Importazione interrotta.")
   public static final String CONFLICTS_FOUND_ABORT = "12";

   @RBEntry("Avvertenza: si sono verifcati dei conflitti durante l'esecuzione della diagnosi dei conflitti di importazione")
   public static final String CONFLICTS_FOUND_WARN = "13";

   @RBEntry("\t")
   public static final String EMPTY_LINE = "14";

   @RBEntry("Percorso cartella deve iniziare con \"/\" \n La cartella attuale è: \"{0}\"")
   public static final String WRONG_FOLDER_START = "15";

   @RBEntry("Schedario/Cartella")
   public static final String FOLDER = "17";

   @RBEntry("Ciclo di vita")
   public static final String LIFECYCLE = "18";

   @RBEntry("Stato del ciclo di vita")
   public static final String LIFECYCLE_STATE = "19";

   @RBEntry("Team")
   public static final String TEAM = "20";

   @RBEntry("Vista")
   public static final String VIEW = "21";

   @RBEntry("Tipo distinta base")
   public static final String VARIATION1 = "VARIATION1";

   @RBEntry("Numero alternativo")
   public static final String VARIATION2 = "VARIATION2";

   @RBEntry("Versione")
   public static final String VERSION = "22";

   @RBEntry("Livello versione")
   public static final String VERSION_LEVEL = "23";

   @RBEntry("Iterazione")
   public static final String ITERATION = "24";

   @RBEntry("Titolo")
   public static final String TITLE = "25";

   @RBEntry("Descrizione")
   public static final String DESCRIPTION = "26";

   @RBEntry("Reparto")
   public static final String DEPARTMENT = "27";

   @RBEntry("Tipo di parte")
   public static final String PART_TYPE = "28";

   @RBEntry("Origine")
   public static final String SOURCE = "29";

   @RBEntry("Nome")
   public static final String NAME = "30";

   @RBEntry("Tipo di documento")
   public static final String DOCTYPE = "31";

   @RBEntry("Quantità: ammontare")
   public static final String QUANTITY_AMOUNT = "32";

   @RBEntry("Quantità: unità")
   public static final String QUANTITY_UNIT = "33";

   @RBEntry("Sottotipo di documento")
   public static final String DOC_SUBTYPE = "34";

   @RBEntry("Nome CAD")
   public static final String CADNAME = "35";

   @RBEntry("Applicazione di creazione")
   public static final String AUTHAPP = "36";

   @RBEntry("Applicazione proprietaria")
   public static final String OWNERAPP = "37";

   @RBEntry("Codice traccia di default")
   public static final String DEFAULT_TRACE_CODE = "38";

   @RBEntry("Prodotto finale")
   public static final String END_ITEM = "39";

   @RBEntry("Codice traccia")
   public static final String TRACE_CODE = "40";

   @RBEntry("Numero di autorizzazione operazione")
   public static final String JOB_AUTHORIZATION_NUMBER = "41";

   @RBEntry("Numero di contratto")
   public static final String CONTRACT_NUMBER = "42";

   @RBEntry("Fase")
   public static final String PHASE = "43";

   @RBEntry("Identificatore univoco regola aziendale")
   public static final String BUSINESS_RULE_KEY = "44";

   @RBEntry("Identificatore univoco insieme regole aziendali")
   public static final String BUSINESS_RULE_SET_KEY = "45";

   @RBEntry("\"{1}\" esiste già, ma ha un valore diverso per l'attributo \"{0}\". Valore esistente: \"{3}\", nuovo valore: \"{2}\".")
   @RBComment("Used by IX Handlers when import fails because the object already exists, but has a different value for the specified attribute.")
   @RBArgComment0("name of the attribute that is different between that persisted and that mentioned in the import.")
   @RBArgComment1("Object identifier of the object being imported.")
   @RBArgComment2("value of the attribute from the import file.")
   @RBArgComment3("value of the attribute currently persisted.")
   public static final String EXISTING_OBJECT_ATTRIBUTE_CONFLICT = "50";

   @RBEntry("L'oggetto \"{1}\" esiste già nel database, ma ha un valore diverso per l'attributo d'istanza \"{0}\": il valore esistente è \"{3}\", il nuovo valore è \"{2}\".")
   public static final String EXISTING_OBJECT_IBA_VALUE_CONFLICT = "51";

   @RBEntry("L'oggetto \"{1}\" esiste già nel database, ma ha un tipo diverso di attributo d'istanza \"{0}\": il tipo esistente è \"{3}\", il nuovo tipo è \"{2}\".")
   public static final String EXISTING_OBJECT_IBA_TYPE_CONFLICT = "52";

   @RBEntry("L'oggetto \"{1}\" esiste già nel database, ma ha una precisione d'unità diversa  dell'attributo d'istanza \"{0}\": la precisione unità esistente è \"{3}\", quella nuova è \"{2}\".")
   public static final String EXISTING_OBJECT_IBA_UNIT_PRECISION_CONFLICT = "53";

   @RBEntry("L'oggetto \"{1}\" esiste già nel database, ma ha un denominatore di rapporto diverso per l'attributo d'istanza \"{0}\": il denominatore di rapporto esistente è \"{3}\", quello nuovo è \"{2}\".")
   public static final String EXISTING_OBJECT_IBA_RATIO_DENOMINATOR_CONFLICT = "54";

   /**
    * Project contants for the implementation for backward compatible with R 6.0
    **/
   @RBEntry("Impossibile trovare il progetto \"{0}\" nel dominio amministrativo \"{1}\". Consultare la Guida dell'amministratore di sistema Windchill per ulteriori informazioni.")
   public static final String PROJECT_NOT_FOUND = "55";

   @RBEntry("Impossibile trovare il progetto \"{0}\" perché il dominio amministrativo \"{1}\", in cui risiede il progetto, non esiste.")
   public static final String PROJECT_DOMAIN_IS_MISSING = "56";

   @RBEntry("Il nome completo del progetto deve consistere di \"nome dominio\".\"nome progetto\"\n L'attuale nome completo del progetto è: \"{0}\" !")
   public static final String WRONG_FULL_PROJECT_NAME = "57";

   @RBEntry("Progetto")
   public static final String PROJECT = "58";

   @RBEntry("Il numero dell'oggetto {0} ha un UFID diverso dall'UFID XML")
   public static final String NUMBER_EXISTS_WITH_DIFFERENT_UFID = "59";

   @RBEntry("Il nome dell'oggetto {0} ha un UFID diverso dall'UFID XML")
   public static final String NAME_EXISTS_WITH_DIFFERENT_UFID = "60";

   @RBEntry("L'oggetto {0} ha un UFID diverso dall'UFID XML")
   public static final String IDENTICAL_OBJECT_EXISTS_WITH_DIFFERENT_UFID = "IDENTICAL_OBJECT_EXISTS_WITH_DIFFERENT_UFID";

   /**
    * For display Units....
    **/
   @RBEntry("Il sistema di misura \"{0}\" esiste con un valore simbolico di base diverso.")
   @RBArgComment0("The name of Measurement system.")
   public static final String MS_NAME_EXISTS_WITH_DIFFERENT_UNITS = "61";

   @RBEntry("Il sistema di misura \"{0}\" non esiste. ")
   @RBArgComment0("The name of Measurement system.")
   public static final String MEASUREMENT_SYSTEM_NOT_EXIST = "62";

   @RBEntry("La quantità di misura \"{0}\" esiste con unità visualizzate diverse o sovrascritte.")
   @RBArgComment0("The name of Quantity of measure .")
   public static final String QOM_NAME_EXISTS_WITH_DIFFERENT_UNITS = "qom_conflicts";

   @RBEntry("La quantità di misura \"{0}\" non esiste. ")
   @RBArgComment0("The name of Quantity of measure.")
   public static final String QUANTITY_OF_MEASURE_NOT_EXIST = "qom_not_exist";

   @RBEntry("La quantità di misura \"{0}\" per UnitDefinition \"{1}\" esiste con unità visualizzate diverse o sovrascritte.")
   @RBArgComment0("The name of Quantity of measure .")
   @RBArgComment1("The name of UnitDefinition.")
   public static final String UNIT_DEF_NAME_EXISTS_WITH_DIFFERENT_UNITS = "unit_def_conflicts";

   /**
    * For soft type defintions ...
    **/
   @RBEntry("L'oggetto esisteva con un tipo diverso. Tipo esistente: {0}; tipo atteso: {1}.")
   @RBArgComment0("External type identifier, e.g. WCTYPE|wt.part.WTPart|MyExistedSoftType")
   @RBArgComment1("External type identifier, e.g. WCTYPE|wt.part.WTPart|MyExpectedSoftType")
   public static final String INCOMPARTIBLE_TYPE_ID = "incompartible_type_id";

   @RBEntry("Impossibile trovare la definizione di tipo che segue nei contesti destinazione o padre: \"{0}\"")
   @RBArgComment0("Path of the WTTypeDefinition, e.g. wt.part.WTPart/MyExpectedSoftType")
   public static final String TYPE_DEFINITION_NOT_EXIST = "type_def_not_exist";

   @RBEntry("Attributo non suddivisibile \"instantiable\" per: \"{0}\", atteso: \"{1}\", trovato: \"{2}\"")
   @RBArgComment0("Path of the WTTypeDefinition, e.g. wt.part.WTPart/MyExpectedSoftType")
   @RBArgComment1("Expected value for instantiable: true or false")
   @RBArgComment2("Existed value for instantiable: true or false")
   public static final String TYPE_DEF_INSTANTIABLE_INCOMPARTIBLE = "type_def_instantiable_incompartible";

   @RBEntry("Attributo non suddivisibile \"deleted\" per: \"{0}\", atteso: \"{1}\", trovato: \"{2}\"")
   @RBArgComment0("Path of the WTTypeDefinition, e.g. wt.part.WTPart/MyExpectedSoftType")
   @RBArgComment1("Expected value for deleted: true or false")
   @RBArgComment2("Existed value for deleted: true or false")
   public static final String TYPE_DEF_DELETED_INCOMPARTIBLE = "type_def_deleted_incompartible";

   @RBEntry("Valore attributo d'istanza (tipo di attributo: \"{1}\", percorso: \"{2}\", valore: \"{3}\") atteso per l'importazione di: \"{0}\"")
   @RBArgComment0("Path of the WTTypeDefinition, e.g. wt.part.WTPart/MyExistedSoftType")
   @RBArgComment1("Attribute type, e.g. IntegerValue, ReferenceValue")
   @RBArgComment2("Path (without AttributeOrganizer) of the IBA value")
   @RBArgComment3("value of the IBA value")
   public static final String IBA_VALUE_EXPECTED_FOR_TYPE_DEF = "ibavalue_expected_for_type_def";

   @RBEntry("Altro valore attributo d'istanza (tipo di attributo: \"{1}\", percorso: \"{2}\", valore: \"{3}\") trovato per l'importazione di: \"{0}\"")
   @RBArgComment0("Path of the WTTypeDefinition, e.g. wt.part.WTPart/MyExistedSoftType")
   @RBArgComment1("Attribute type, e.g. IntegerValue, ReferenceValue")
   @RBArgComment2("Path (without AttributeOrganizer) of the IBA value")
   @RBArgComment3("value of the IBA value")
   public static final String EXTRA_IBA_VALUE_FOR_TYPE_DEF = "extra_ibavalue_for_type_def";

   @RBEntry("Vincolo di tipo (enforcementRuleClassname: \"{1}\", bindingRuleClassName: \"{2}\", enforcementRuleData: \"{3}\", percorso definizione attributo d'istanza: \"{4}\") atteso per l'importazione di \"{0}\"")
   @RBArgComment0("Path of the WTTypeDefinition, e.g. wt.part.WTPart/MyExistedSoftType")
   @RBArgComment4("Path of IBA definitions")
   public static final String TYPE_CONSTRAINT_EXPECTED_FOR_TYPE_DEF = "constraint_expected_for_type_def";

   @RBEntry("Vincolo di tipo extra (enforcementRuleClassname: \"{1}\", bindingRuleClassName: \"{2}\", enforcementRuleData: \"{3}\", percorso definizione IBA: \"{4}\") atteso per l'importazione di \"{0}\"")
   @RBArgComment0("Path of the WTTypeDefinition, e.g. wt.part.WTPart/MyExistedSoftType")
   @RBArgComment4("Path of IBA definitions")
   public static final String EXTRA_TYPE_CONSTRAINT_FOR_TYPE_DEF = "extra_constraint_for_type_def";

   @RBEntry("L'icona \"{0}\" esiste già. Se si ignora il conflitto l'icona verrà ridenominata.")
   @RBArgComment0("Path of the icon file, e.g. wt/icons/myTypeIconImage.gif")
   public static final String ICON_FILE_EXIST = "icon_file_exist";

   @RBEntry("Gli attributi sono diversi per \"{0}\". Il valore dell'attributo  \"{3}\" è diverso da quello trovato nel database. Atteso: \"{1}\", trovato: \"{2}\"")
   @RBArgComment0("object id such as client image")
   @RBArgComment1("The value in the XML file")
   @RBArgComment2("The value in the database")
   @RBArgComment3("The attribute tag name")
   public static final String GENERAL_ATTR_DIFF = "general_attr_diff";

   @RBEntry("Il gruppo identificato di identificatori logici è diverso: esistente: \"{0}\"; atteso: \"{1}\".")
   public static final String INCONSISTENT_IDENTIFIED_GROUP = "inconsistent_id_grp";

   @RBEntry("Identificatore logico diverso: esistente: \"{0}\"; atteso: \"{1}\".")
   public static final String INCONSISTENT_LOGICAL_ID = "inconsistent_logic_id";

   /**
    * End of Type Definitions
    **/
   @RBEntry("Impossibile importare \"{0}\". Non è possibile trovare il dominio \"{1}\" oppure l'utente non ha accesso al dominio dal contesto \"{2}\". ")
   public static final String DOMAIN_NOT_FOUND = "63";

   @RBEntry("Numero di riga")
   public static final String LINE_NUMBER = "64";

   @RBEntry("Unità di default")
   public static final String DEFAULT_UNIT = "65";

   @RBEntry("Dominio")
   public static final String DOMAIN = "66";

   @RBEntry("Il percorso del dominio deve iniziare con \"/\". Il percorso corrente è : \"{0}\".")
   public static final String WRONG_DOMAIN_START = "67";

   @RBEntry("Errore durante la lettura del percorso dell'oggetto \"{0}\" o non si dispone dei diritti di accesso a una cartella padre.")
   public static final String CANNOT_GET_FOLDER_PATH = "68";

   @RBEntry("Versione variante")
   public static final String ONE_OFF_VERSION = "69";

   @RBEntry("Serie versione")
   public static final String VERSION_SERIES = "70";

   @RBEntry("Categoria soluzione temporanea")
   public static final String VARIANCE_CATEGORY = "71";

   @RBEntry("Proprietario soluzione temporanea")
   public static final String VARIANCE_OWNER = "72";

   @RBEntry("Data consegna")
   public static final String NEED_DATE = "73";

   @RBEntry("Ricorrente")
   public static final String RECURRING = "74";

   @RBEntry("Effetto sul supporto")
   public static final String EFFECT_ON_SUPPORT = "75";

   @RBEntry("Azione correttiva")
   public static final String CORRECTIVE_ACTION = "76";

   @RBEntry("Effetto sui costi")
   public static final String EFFECT_ON_COST = "77";

   @RBEntry("Effetto sulla programmazione")
   public static final String EFFECT_ON_SCHEDULE = "78";

   @RBEntry("Riepilogo decisione aziendale")
   public static final String BUSINESS_DECISION_SUMMARY = "79";

   @RBEntry("Stima costi ricorrenti")
   public static final String RECURRING_COST_EST = "80";

   @RBEntry("Stima costi non ricorrenti")
   public static final String NON_RECURRING_COST_EST = "81";

   @RBEntry("Priorità richiesta")
   public static final String REQUEST_PRIORITY = "82";

   @RBEntry("Categoria")
   public static final String CATEGORY = "83";

   @RBEntry("Complessità")
   public static final String COMPLEXITY = "84";

   @RBEntry("Durata del ciclo")
   public static final String CYCLE_TIME = "85";

   @RBEntry("Data di risoluzione")
   public static final String RESOLUTION_DATE = "86";

   @RBEntry("Data caso d'impiego")
   public static final String OCCURRENCE_DATE = "OCCURRENCE_DATE";

   @RBEntry("Investigazione necessaria")
   public static final String INVESTIGATION_REQUIRED = "INVESTIGATION_REQUIRED";

   @RBEntry("Investigazione completata")
   public static final String INVESTIGATION_COMPLETED = "INVESTIGATION_COMPLETED";

   @RBEntry("Nuovo valore")
   public static final String NEW_VALUE = "87";

   @RBEntry("Valore precedente")
   public static final String OLD_VALUE = "88";

   @RBEntry("Data di modifica")
   public static final String CHANGE_DATE = "89";

   @RBEntry("Utente")
   public static final String USER = "90";

   @RBEntry("Categoria di decisione aziendale")
   public static final String BUSINESS_DECISION_CATEGORY = "91";

   @RBEntry("Richiedente")
   public static final String REQUESTER = "92";

   @RBEntry("Categoria di conferma")
   public static final String CONFIRMATION_CATEGORY = "93";

   @RBEntry("Priorità problema")
   public static final String ISSUE_PRIORITY = "94";

   @RBEntry("Risultati problema")
   public static final String RESULTS = "95";

   @RBEntry("Complessità notifica di modifica")
   public static final String CHANGE_NOTICE_COMPLEXITY = "96";

   @RBEntry("Indice bolla")
   public static final String FIND_NUMBER = "97";

   @RBEntry("Tipo di allocazione")
   public static final String ALLOCATION_TYPE = "98";

   @RBEntry("Ramo controllo")
   public static final String CONTROL_BRANCH = "99";

   @RBEntry("Modalità esecuzione")
   public static final String EXECUTION_MODE = "100";

   @RBEntry("Inserisci oggetti interessati")
   public static final String POPULATE_AFFECTED_OBJECTS = "populateAffectedObjects";

   @RBEntry("Soluzione proposta")
   public static final String PROPOSED_SOLUTION = "101";

   @RBEntry("Il TeamTemplate del file XML è nel dominio \"{0}\", ma l'oggetto si trova nel dominio \"{1}\". Spuntare la casella \"Risolvi conflitti\" se si desidera importare l'oggetto.")
   public static final String TEAM_TEMPLATE_AND_OBJECT_DOMAIN_ARE_IN_DIFFERENT_POSITION = "team_template_and_object_domain_are_in_different_postion";

   @RBEntry("Impossibile importare \"{0}\". Organizzazione proprietaria non trovata tramite l'identificatore \"{1}\" o il nome \"{2}\" dell'organizzazione. ")
   public static final String ORGANIZATION_NOT_FOUND = "org_not_found";

   @RBEntry("Impossibile importare \"{0}\". Non è possibile accedere all'organizzazione proprietaria \"{1}\".")
   public static final String ORGANIZATION_NO_ACCESS = "org_no_access";

   @RBEntry("Impossibile trovare l'attributo Organizzazione \"{0}\". Consultare la guida Windchill System Administrator's Guide (Guida dell'amministratore di sistema Windchill) per ulteriori informazioni.")
   @RBArgComment0("Full path of the Attribute Organization")
   public static final String ATTRIBUTE_ORGANIZATION_NOT_FOUND = "attr_org_not_found";

   @RBEntry("La definizione gerarchica \"{1}\" non è permessa.")
   @RBArgComment0("Full path of the IBA")
   @RBArgComment1("FPath of the IBA without Attribute Organization")
   public static final String NESTED_IBA_NOT_ALLOWED = "nested_iba_not_allowed";

   @RBEntry("L'oggetto \"{1}\" è presente nel database, ma contiene più valori per l'attributo d'istanza \"{0}\". I valori sono \"{2}\".")
   @RBArgComment0("The path of the IBA Definition  ")
   @RBArgComment1("Object identifier of the IBA holder")
   @RBArgComment2("The extra IBA value that the existing IBA holder has")
   public static final String EXISTING_OBJECT_HAS_MORE_IBA_VALUE = "has_more_iba_value";

   @RBEntry("L'oggetto \"{1}\" è presente nel database, ma non contiene alcun valore per l'attributo d'istanza \"{0}\". Il valore è \"{2}\".")
   @RBArgComment0("The path of the IBA Definition")
   @RBArgComment1("Object identifier of the IBA holder")
   @RBArgComment2("The missing IBA value that the existing IBA holder has")
   public static final String EXISTING_OBJECT_HAS_FEWER_IBA_VALUE = "has_fewer_iba_value";

   @RBEntry("L'oggetto \"{1}\" è presente nel database, ma contiene un valore differente per l'attributo d'istanza \"{0}\". Il valore è \"{2}\".")
   public static final String OBJID_MATHCES_IBA_PATH_VALUE_DIFFER = "iba_objid_match_path_value_differ";

   @RBEntry("Impossibile trovare l'oggetto BussinessEntity: \"{0}\"")
   @RBArgComment0("name of the BusinessEntity object")
   public static final String BUSINESS_ENTITY_NOT_EXIST = "business_entity_not_exist";

   @RBEntry("Impossibile trovare l'oggetto ClassificationNode: \"{0}\"")
   @RBArgComment0("Path of the ClassificationNode, e.g. node1/node2")
   public static final String CLASSIFICATION_NODE_NOT_EXIST = "classification_node_not_exist";

   @RBEntry("ClassificationStruct non esiste ma è presente nel file XML del nodo di classificazione: \"{0}\"")
   @RBArgComment0("Path of the ClassificationNode, e.g. node1/node2")
   public static final String CLASSIFICATION_STRUCT_NOT_EXIST = "classification_struct_not_exist";

   @RBEntry("ClassificationStruct esiste ma non è presente nel file XML del nodo di classificazione: \"{0}\"")
   @RBArgComment0("Path of the ClassificationNode, e.g. node1/node2")
   public static final String CLASSIFICATION_STRUCT_EXTRA = "classification_struct_extra";

   @RBEntry("ClassificationStruct contiene un diverso attributo \"className\" per il nodo di classificazione \"{0}\": il valore è \"{1}\" nel file XML e \"{2}\" nel database.")
   @RBArgComment0("Path of the ClassificationNode, e.g. node1/node2")
   public static final String CLASSIFICATION_STRUCT_CLASSNAME_DIFF = "classification_struct_classname_diff";

   @RBEntry("ClassificationStruct contiene una diversa definizione di riferimento per il nodo di classificazione \"{0}\": la definizione di riferimento è \"{1}\" nel file XML e  \"{2}\" nel database.")
   @RBArgComment0("Path of the ClassificationNode, e.g. node1/node2")
   @RBArgComment1("Path of the RefernceDefinition without the Attribute Organizer")
   @RBArgComment2("Path of the RefernceDefinition without the Attribute Organizer")
   public static final String CLASSIFICATION_STRUCT_REFERENCE_DIFF = "classification_struct_refdef_diff";

   @RBEntry("Minimo richiesto")
   public static final String MINREQUIRED = "MINREQUIRED";

   @RBEntry("Massimo consentito")
   public static final String MAXALLOWED = "MAXALLOWED";

   @RBEntry("\"{0}\": valore attributo non disponibile durante l'esportazione dell'oggetto \"{1}\" a causa dei permessi di accesso. ")
   public static final String ATTRIB_VALUE_NOT_AVAILABLE = "attrib_value_not_available";

   @RBEntry("\"{0}\": valore attributo non disponibile durante l'esportazione dell'oggetto \"{1}\" a causa dei permessi di accesso. Viene utilizzato il valore di default \"{2}\".")
   public static final String ATTRIB_VALUE_NOT_AVAILABLE_DEFAULT = "attrib_value_not_available_default";

   @RBEntry("\"{0}\": valore attributo non disponibile durante l'esportazione a causa dei permessi di accesso. È stato ignorato.")
   public static final String ATTRIB_VALUE_NOT_AVAILABLE_IGNORED = "attrib_value_not_available_ignored";

   @RBEntry("\"{0}\": valore attributo non disponibile durante l'esportazione a causa dei permessi di accesso. Fornire un valore valido prima dell'importazione.")
   public static final String ACTION_NEEDED_FOR_SECURED_INFO = "action_needed_for_secured_info";

   @RBEntry("Il repository di appartenenza esistente per l'oggetto è \"{0}\" nel database, ma presenta un valore differente in XML. Il valore è \"{1}\".")
   public static final String DIFFERENT_OWNING_REPOSITORY = "owning_repository_diff";

   @RBEntry("L'ultimo repository di origine noto esistente per l'oggetto è \"{0}\" nel database, ma presenta un valore differente in XML. Il valore è \"{1}\".")
   public static final String DIFFERENT_LASTKNOWN_REPOSITORY = "lastknown_repository_diff";

   @RBEntry(" Oggetto non Federatable")
   public static final String OBJECT_NOT_FEDERATABLE = "object_not_federatable";

   @RBEntry("Elemento fittizio o phantom")
   public static final String PHANTOM = "PHANTOM";

   @RBEntry("Comprimibile")
   public static final String COLLAPSIBLE = "COLLAPSIBLE";

   @RBEntry("\"{0}\": impossibile importare l'oggetto a causa dei permessi di accesso.")
   public static final String SECURED_INFO_CONFLICT = "SECURED_INFO_CONFLICT";

   @RBEntry("Impossibile importare \"{0}\". Il ciclo di vita \"{1}\" non è valido per questo tipo di oggetto.")
   public static final String LC_TEMPLATE_NOT_SUPPORTED = "102";

   @RBEntry("Impossibile importare \"{0}\". Le informazioni relative all'organizzazione proprietaria sono contrassegnate come protette.")
   public static final String ORGANIZATION_ID_MARKED_AS_SECURED = "103";

   @RBEntry("Impossibile importare \"{0}\". Le informazioni relative al ciclo di vita sono contrassegnate come protette.")
   public static final String LIFECYCLE_MARKED_AS_SECURED = "104";

   @RBEntry("Impossibile importare \"{0}\". Le informazioni relative allo stato del ciclo di vita sono contrassegnate come protette.")
   public static final String LIFECYCLE_STATE_MARKED_AS_SECURED = "105";

   @RBEntry("Impossibile importare \"{0}\". La revisione \"{1}\" non è valida.")
   public static final String VERSION_NOT_FOUND = "106";

   @RBEntry("Impossibile importare \"{0}\". La revisione \"{1}\" (posizione {2}) non è valida. ")
   public static final String VERSION_POSITION_NOT_FOUND = "VERSION_POSITION_NOT_FOUND";

   @RBEntry("Impossibile importare \"{0}\". La cartella \"{1}\" non esiste o l'utente non ha accesso alla cartella nel contesto \"{2}\".")
   public static final String FOLDER_NO_ACCESS = "107";

   @RBEntry("Impossibile importare \"{0}\". Le informazioni relative alla cartella sono contrassegnate come protette.")
   public static final String FOLDER_MARKED_AS_SECURED = "108";

   @RBEntry("\"{0}\" esiste già.")
   public static final String EXISTING_OBJECT_CONFLICT = "EXISTING_OBJECT_CONFLICT";

   @RBEntry(" Impossibile importare \"{0}\". L'utente corrente non dispone dei permessi necessari.")
   @RBArgComment0("Object identifier of the object being imported.")
   public static final String NOT_AUTHORIZED = "NOT_AUTHORIZED";

   @RBEntry("Contesto")
   public static final String CONTEXT = "CONTEXT";

   @RBEntry("\"{1}\": impossibile trovare l'utente nel sistema di destinazione in base a \"{0}\".")
   public static final String PRINCIPAL_NOT_FOUND = "PRINCIPAL_NOT_FOUND";

   @RBEntry("Il tipo di dati dell'attributo di importazione \"{0}\" nel tipo \"{1}\" è diverso con il tipo di dati dell'attributo esistente.")
   public static final String INCOMPARTIBLE_ATTRIBUTE_DATATYPE = "INCOMPARTIBLE_ATTRIBUTE_DATATYPE";

   @RBEntry("Motivo")
   public static final String REASON = "109";

   @RBEntry("Quantità approvata")
   public static final String APPROVED_QUANTITY = "110";

   @RBEntry(" Impossibile importare \"{0}\". Numero non valido.")
   public static final String NUMBER_NOT_FOUND = "111";

   @RBEntry("Nascondi parte nella struttura")
   public static final String HIDE_PART_IN_STRUCTURE = "112";

   @RBEntry(" Impossibile importare \"{0}\" poiché il contesto \"{1}\" non esiste.")
   public static final String CONTEXT_NOT_FOUND = "113";

   @RBEntry("Impossibile importare \"{0}\". Le informazioni relative al contesto sono contrassegnate come protette.")
   public static final String CONTAINER_MARKED_AS_SECURED = "114";

   @RBEntry("Impossibile importare \"{0}\". La proprietà necessaria \"{1}\" per la risoluzione \"{2}\" non è impostata. ")
   public static final String PROPERTY_NOT_FOUND = "115";

   @RBEntry("Impossibile importare \"{0}\". Un altro oggetto creato localmente con le stesse informazioni di identificazione è già presente nel sistema. ")
   public static final String LOCAL_OBJECT_EXISTS = "116";

   @RBEntry("Impossibile importare \"{0}\". Un oggetto con le stesse informazioni di identificazione importato da un altro sistema è già presente nel sistema. ")
   public static final String REMOTE_OBJECT_EXISTS = "117";


   @RBEntry("Soggetta a manutenzione")
   public static final String XML_ATTR_SERVICEABLE = "SERVICEABLE";

   @RBEntry("Kit ricambi")
   public static final String XML_ATTR_SERVICE_KIT = "SERVICE_KIT";

   @RBEntry("Impossibile importare \"{0}\". Il tipo di oggetto \"{1}\" non è presente nel sistema.")
   public static final String SOLUTION_NOT_FOUND = "SOLUTION_NOT_FOUND";

   @RBEntry("Valore contesto non disponibile durante l'esportazione. L'utente non dispone degli appositi permessi. ")
   public static final String ATTRIBUTE_CONTAINER_MARKED_AS_SECURED = "118";

   @RBEntry("Il contesto \"{0}\" non esiste.")
   public static final String ATTRIBUTE_CONTEXT_NOT_FOUND = "119";

   @RBEntry("Impossibile trovare lo schedario \"{0}\" oppure l'utente non ha accesso allo schedario dal contesto \"{1}\". ")
   public static final String ATTRIBUTE_CABINET_NOT_FOUND = "120";

   @RBEntry("La cartella \"{0}\" non esiste nel contesto \"{1}\". ")
   public static final String ATTRIBUTE_FOLDER_NOT_FOUND = "121";

   @RBEntry("Impossibile trovare il dominio \"{0}\" oppure l'utente non ha accesso al dominio dal contesto \"{1}\". ")
   public static final String ATTRIBUTE_DOMAIN_NOT_FOUND = "122";

   @RBEntry("Valore cartella non disponibile durante l'esportazione. L'utente non dispone degli appositi permessi. ")
   public static final String ATTRIBUTE_FOLDER_MARKED_AS_SECURED = "123";

   @RBEntry("\"{0}\": valore attributo non disponibile durante l'esportazione. L'utente non dispone degli appositi permessi. Viene utilizzato il valore di default \"{1}\".")
   public static final String ATTRIB_VALUE_NOT_AVAILABLE_DEFAULT_GENERIC = "attrib_value_not_available_default_generic";

   @RBEntry("\"{0}\": valore attributo non disponibile durante l'esportazione. L'utente non dispone degli appositi permessi. ")
   public static final String ATTRIB_VALUE_NOT_AVAILABLE_GENERIC = "attrib_value_not_available_generic";

   @RBEntry("Valore stato ciclo di vita non disponibile durante l'esportazione. L'utente non dispone degli appositi permessi. ")
   public static final String ATTRIBUTE_LIFECYCLE_MARKED_AS_SECURED = "124";

   @RBEntry("Valore stato ciclo di vita non disponibile durante l'esportazione. L'utente non dispone degli appositi permessi. ")
   public static final String ATTRIBUTE_LIFECYCLE_STATE_MARKED_AS_SECURED = "125";

   @RBEntry("Impossibile trovare il ciclo di vita \"{0}\".")
   public static final String ATTRIBUTE_LC_TEMPLATE_NOT_FOUND = "126";

   @RBEntry("Ciclo di vita \"{0}\" non valido per il tipo di oggetto \"{1}\".")
   public static final String ATTRIBUTE_LC_TEMPLATE_NOT_SUPPORTED = "127";

   @RBEntry("Stato del ciclo di vita \"{0}\" non valido per il ciclo di vita \"{1}\". ")
   public static final String ATTRIBUTE_LC_STATE_NOT_FOUND = "128";

   @RBEntry("Impossibile trovare il team \"{0}\" nel dominio amministrativo \"{1}\".")
   public static final String ATTRIBUTE_TEAM_NOT_FOUND = "129";

   @RBEntry("Impossibile trovare l'organizzazione proprietaria tramite l'identificatore \"{0}\" o il nome \"{1}\" dell'organizzazione. ")
   public static final String ATTRIBUTE_ORGANIZATION_NOT_FOUND_GENERIC = "org_not_found_generic";

   @RBEntry("Impossibile trovare il contesto Acquisti \"{0}\" nel server. ")
   public static final String SOURCING_CONTEXT_NOT_FOUND = "SOURCING_CONTEXT_NOT_FOUND";

   @RBEntry("L'utente non dispone dell'accesso in modifica per {0}")
   @RBArgComment0("Object Identity")
   public static final String NO_MODIFY_ACCESS_ON_OBJECT = "130";

   @RBEntry("Stato creazione")
   public static final String BUILD_STATUS = "131";

   @RBEntry("L'utente non dispone del permesso di lettura per la cartella di origine \"{0}\" nel contesto \"{1}\". ")
   public static final String ATTRIBUTE_FOLDER_NOT_ACCESSIBLE = "132";

   @RBEntry("L'utente non dispone dei permessi di creazione e di modifica per la cartella di origine \"{0}\" nel contesto \"{1}\". ")
   public static final String ATTRIBUTE_FOLDER_NOT_MODIFIABLE = "133";

   @RBEntry("Impossibile trovare i campi aziendali \"{0}\" di \"{1}\" nel sistema corrente.")
   @RBArgComment0("Business field name.")
   @RBArgComment1("Object identifier of the object being imported.")
   public static final String BUSINESS_FIELD_NOT_FOUND = "BUSINESS_FIELD_NOT_FOUND";

   @RBEntry("I campi aziendali \"{0}\" di \"{1}\" non sono compatibili con le definizioni di campo aziendale nel sistema corrente.")
   @RBArgComment0("Business field name.")
   @RBArgComment1("Object identifier of the object being imported.")
   public static final String BUSINESS_FIELD_DATA_STRUCTURE_NOT_MATCHED = "BUSINESS_FIELD_DATA_STRUCTURE_NOT_MATCHED";

   @RBEntry("Impossibile trovare l'oggetto \"{0}\" referenziato da \"{1}\" di \"{2}\".")
   @RBArgComment0("name of the class of the referenced object.")
   @RBArgComment1("name of the XML attribute that refers to the object reference.")
   @RBArgComment2("id of the object to be imported.")
   public static final String OBJECT_REF_NOT_FOUND = "OBJECT_REF_NOT_FOUND";

   @RBEntry("L'utente non dispone dei diritti di accesso per la modifica all'identificativo per {0}")
   @RBArgComment0("Object Identity")
   public static final String NO_MODIFY_IDENTITY_ACCESS_ON_OBJECT = "NO_MODIFY_IDENTITY_ACCESS_ON_OBJECT";

   @RBEntry("L'utente non dispone dei diritti di accesso per la modifica al contenuto per \"{0}\"")
   @RBArgComment0("Object identifier of the object being imported.")
   public static final String NO_MODIFY_CONTENT_ACCESS_ON_OBJECT = "NO_MODIFY_CONTENT_ACCESS_ON_OBJECT";

   @RBEntry("ATTENZIONE: il tipo ruolo del contenuto \"{0}\" non esiste per \"{1}\"")
   @RBArgComment0("Missing content role type.")
   @RBArgComment1("Object identifier of the object being imported.")
   public static final String CONTENT_ROLE_TYPE_NOT_FOUND = "CONTENT_ROLE_TYPE_NOT_FOUND";

   @RBComment("The conflict message used when import fails because the participant was exported as " +
   		" IxbHndHelperConstants.XML_VALUE_SECURED_INFORMATION")
   @RBEntry("Impossibile importare \"{0}\", perché il partecipante è stato esportato come \"{1}\".")
   @RBArgComment0("Identity of the object being imported.")
   @RBArgComment1("The string defined by IxbHndHelperConstants.XML_VALUE_SECURED_INFORMATION")
   public static final String PARTICIPANT_EXPORTED_AS_SECURED = "PARTICIPANT_EXPORTED_AS_SECURED";

   @RBComment("The conflict message used when called by preview and it is known the import will fail" +
   		" because the participant was exported as IxbHndHelperConstants.XML_VALUE_SECURED_INFORMATION")
   @RBEntry("Il partecipante è stato esportato come \"{0}\".")
   @RBArgComment0("The string defined by IxbHndHelperConstants.XML_VALUE_SECURED_INFORMATION")
   public static final String ATTRIBUTE_PARTICIPANT_EXPORTED_AS_SECURED = "ATTRIBUTE_PARTICIPANT_EXPORTED_AS_SECURED";

   @RBEntry("\"{0}\" esiste già, ma ha un valore diverso per il partecipante. Valore esistente: \"{1}\", nuovo valore: \"{2}\".")
   @RBComment("The conflict message used when import fails because the object already exists, but has a different value for the participant.")
   @RBArgComment0("Identity of the object being imported.")
   @RBArgComment1("Identity of the existing participant value.")
   @RBArgComment2("Identity of the participant in the import file.")
   public static final String EXISTING_OBJECT_PARTICIPANT_CONFLICT = "EXISTING_OBJECT_PARTICIPANT_CONFLICT";

   @RBEntry("Impossibile importare \"{0}\", perché l'utente \"{1}\" non esiste.")
   @RBComment("The conflict message used when import fails because the user does not exist but can be replicated.")
   @RBArgComment0("Identity of the object being imported.")
   @RBArgComment1("Identity of the user that does not exist.")
   public static final String USER_NOT_FOUND_MAY_BE_REPLICATED = "USER_NOT_FOUND_MAY_BE_REPLICATED";

   @RBComment("The conflict message used when called by preview and it is known the import will fail " +
   		"because the user does not exist but can be replicated.")
   @RBEntry("L'utente \"{0}\" non esiste.")
   @RBArgComment0("Identity of the user that does not exist.")
   public static final String ATTRIBUTE_USER_NOT_FOUND_MAY_BE_REPLICATED = "ATTRIBUTE_USER_NOT_FOUND_MAY_BE_REPLICATED";

   @RBComment("The conflict message used when import fails because the importing user is not authorized to see the " +
   		"participant or the participant does not exist.")
   @RBEntry("Impossibile importare \"{0}\". Autorizzazione non disponibile per il partecipante \"{1}\" o il partecipante non esiste.")
   @RBArgComment0("Identity of the object being imported.")
   @RBArgComment1("Identity of the participant is not authorized to see or does not exist.")
   public static final String PARTICIPANT_NOT_FOUND = "PARTICIPANT_NOT_FOUND";

   @RBComment("The conflict message used when called by preview and it is known the import will fail because the importing user " +
   		"is not authoried to see the participant or the participant does not exist.")
   @RBEntry("Autorizzazione non disponibile per il partecipante \"{0}\" o il partecipante non esiste.")
   @RBArgComment0("Identity of the participant that is not authorized to see or does not exist.")
   public static final String ATTRIBUTE_PARTICIPANT_NOT_FOUND = "ATTRIBUTE_PARTICIPANT_NOT_FOUND";

   @RBEntry("Impossibile importare \"{0}\". Autorizzazione non disponibile per modificare la proprietà.")
   @RBComment("The conflict message used when import fails because the importing user is not authorized to modify the ownership.")
   @RBArgComment0("Identity of the object being imported.")
   public static final String NOT_AUTHORIZED_TO_MODIFY_OWNERSHIP = "NOT_AUTHORIZED_TO_MODIFY_OWNERSHIP";

   @RBEntry("\"{0}\" cannot be imported because you do not have authorization to modify the security label value \"{1}\"" +
   		" for security label \"{2}\".")
   @RBComment("The conflict message used when import fails because the importing user is not authorized to modify a" +
   		" security label value.")
   @RBArgComment0("Identity of the object being imported.")
   @RBArgComment1("The security label value.")
   @RBArgComment2("The security label name.")
   public static final String NOT_AUTHORIZED_TO_MODIFY_SECURITY_LABELS = "NOT_AUTHORIZED_TO_MODIFY_SECURITY_LABELS";

   @RBComment("The conflict message used when import fails because the security label value is not valid" +
   		"for the specifiec security label.")
   @RBEntry("Impossibile importare \"{0}\". Il valore \"{1}\" non è valido per l'etichetta di sicurezza \"{2}\".")
   @RBArgComment0("Identity of the object being imported.")
   @RBArgComment1("The security label value.")
   @RBArgComment2("The security label name.")
   public static final String SECURITY_LABEL_VALUE_INVALID = "SECURITY_LABEL_VALUE_INVALID";

   @RBComment("The conflict message used when called by preview and it is known the import will fail because the security label value is not valid for the " +
   		"specified security label")
   @RBEntry("Il valore \"{0}\" non è valido per l'etichetta di sicurezza \"{1}\".")
   @RBArgComment0("The security label value.")
   @RBArgComment1("The security label name.")
   public static final String ATTRIBUTE_SECURITY_LABEL_VALUE_INVALID = "ATTRIBUTE_SECURITY_LABEL_VALUE_INVALID";

   @RBEntry("Impossibile importare \"{0}\". L'etichetta di sicurezza \"{1}\" non è valida.")
   @RBComment("The conflict message used when import fails because the specified security label is not valid.")
   @RBArgComment0("Identity of the object being imported.")
   @RBArgComment1("The security label name.")
   public static final String SECURITY_LABEL_INVALID = "SECURITY_LABEL_INVALID";

   @RBComment("The conflict message used when called by preview and it is known the import will fail because the security label " +
   		"is not valid.")
   @RBEntry("L'etichetta di sicurezza \"{0}\" non è valida.")
   @RBArgComment0("The security label name.")
   public static final String ATTRIBUTE_SECURITY_LABEL_INVALID = "ATTRIBUTE_SECURITY_LABEL_INVALID";

   @RBComment("The conflict message used when called by preview and it is known the import will fail" +
      " because the organization was exported as IxbHndHelperConstants.XML_VALUE_SECURED_INFORMATION")
   @RBEntry("Organizzazione proprietaria contrassegnata come \"{0}\". Impossibile importarla")
   @RBArgComment0("The string defined by IxbHndHelperConstants.XML_VALUE_SECURED_INFORMATION")
   public static final String ORGANIZATION_EXPORTED_AS_SECURED = "ORGANIZATION_EXPORTED_AS_SECURED";

    @RBEntry("La lingua di origine o di destinazione è nulla o non specificata per i seguenti testi master: \"{0}\".")
    @RBArgComment0("Master texts")
    public static final String TDE_LANGUAGE_NOT_SPECIFIED = "TDE_LANGUAGE_NOT_SPECIFIED";

    @RBEntry("Le seguenti lingue di origine o di destinazione non sono valide: \"{0}\".")
    @RBArgComment0("Provided source or target languages")
    public static final String TDE_LANGAUGE_INVALID = "TDE_LANGAUGE_INVALID";

    @RBEntry("Le seguenti lingue di origine specificate non sono presenti nel set di lingue di origine: \"{0}\".")
    @RBArgComment0("Specified source languages")
    public static final String TDE_LANGUAGE_NOT_IN_SELECTABLE_SOURCE_LANGUAGE_SET = "TDE_LANGUAGE_NOT_IN_SELECTABLE_SOURCE_LANGUAGE_SET";

    @RBEntry("Il dizionario di traduzione \"{0}\" presenta i seguenti conflitti sovrapposti: \"{1}\".")
    @RBArgComment0("Translation dictionary name")
    @RBArgComment1("Conflicts")
    public static final String TDE_OVERLAPPING_ENTRIES = "TDE_OVERLAPPING_ENTRIES";

    @RBEntry("La voce del dizionario di traduzione in ingresso \"{0}\" si sovrappone alle seguenti voci persistenti esistenti: \"{1}\".")
    @RBArgComment0("Incoming entry")
    @RBArgComment1("Persisted entries")
    public static final String TDE_OVERLAPPING_ENTRY = "TDE_OVERLAPPING_ENTRY";

    @RBEntry("Impossibile importare le voci di dizionario. L'utente corrente non dispone dell'autorizzazione appropriata per il seguente dizionario di traduzione: \"{0}\".")
    @RBArgComment0("Translation dictionary name")
    public static final String TDE_NOT_AUTHORIZED = "TDE_NOT_AUTHORIZED";

    @RBEntry("Il valore dell'attributo della lingua di creazione non è specificato per l'oggetto seguente: \"{0}\".")
    @RBArgComment0("Object Id of the object for import")
    public static final String AUTHORING_LANGUAGE_NOT_SPECIFIED = "AUTHORING_LANGUAGE_NOT_SPECIFIED";

    @RBEntry("Il valore dell'attributo della lingua di creazione è nullo per l'oggetto seguente: \"{0}\".")
    @RBArgComment0("Object Id of the object for import")
    public static final String AUTHORING_LANGUAGE_NULL = "AUTHORING_LANGUAGE_NULL";

    @RBEntry("Il valore dell'attributo della lingua di creazione \"{0}\" specificato per l'oggetto seguente non è valido: \"{1}\".")
    @RBArgComment0("Specified authoring language attribute value")
    @RBArgComment1("Object Id of the object for import")
    public static final String AUTHORING_LANGUAGE_INVALID = "AUTHORING_LANGUAGE_INVALID";

    @RBEntry("Il valore dell'attributo della lingua di creazione \"{0}\" specificato per l'oggetto seguente non è presente nel set di lingue di origine: \"{1}\".")
    @RBArgComment0("Specified authoring language attribute value")
    @RBArgComment1("Object Id of the object for import")
    public static final String AUTHORING_LANGUAGE_NOT_IN_SELECTABLE_SOURCE_LANGUAGE_SET = "AUTHORING_LANGUAGE_NOT_IN_SELECTABLE_SOURCE_LANGUAGE_SET";
}
