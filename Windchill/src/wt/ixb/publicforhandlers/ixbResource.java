/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.ixb.publicforhandlers;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBArgComment1;
import wt.util.resource.RBArgComment2;
import wt.util.resource.RBArgComment3;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBPseudo;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.ixb.publicforhandlers.ixbResource")
public final class ixbResource extends WTListResourceBundle {
    /*
     * -*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-
     * WARNING:
     *     PLEASE AVOID MULTI-LINE MESSAGES in resource file as it causes issues while localization/translation!
     * -*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-!-*-
     */
    @RBEntry("There is no attribute definition for attribute \"{0}\"")
    public static final String IBA_DEFINITION_NOT_FOUND = "0";

    @RBEntry("Could not create IB attributes object \"{0}\"")
    public static final String IBA_COULD_NOT_CREATE = "1";

    @RBEntry("Could not retrieve the field \"{0}\" while exporting the object \"{1}\"")
    public static final String IBA_COULD_NOT_RETRIEVE_FIELD = "2";

    @RBEntry("Could not replicate folder: domain \"{0}\" does not exist")
    public static final String DOMAIN_DOES_NOT_EXIST = "3";

    @RBEntry("Exception has been thrown while trying to customize replication information through an invocation of the method \"{0}\" for the class \"{1}\"; actual parameters are: ( \"{2}\" )")
    public static final String ERROR_IN_CUSTOM_METHOD_INVOCATION = "4";

    @RBEntry("Class \"{0}\" does not support interface wt.ixb.handlers.ClassExporter")
    public static final String EXPORTER_CLASS_DOES_NOT_SUPPORT_INTERFACE_CLASSEXPORTER = "5";

    @RBEntry("Class \"{0}\" does not support interface wt.ixb.handlers.ElementImporter")
    public static final String IMPORTER_CLASS_DOES_NOT_SUPPORT_INTERFACE_ELEMENTIMPORTER = "6";

    @RBEntry("Class \"{0}\" does not support interface wt.ixb.publicforhandlers.AttributeExporterImporter")
    public static final String CLASS_DOES_NOT_SUPPORT_INTERFACE_ATTRIBUTE_EXPORTER_IMPORTER = "CLASS_DOES_NOT_SUPPORT_INTERFACE_ATTRIBUTE_EXPORTER_IMPORTER";

    @RBEntry("Class \"{0}\" does not support interface wt.ixb.handlers.ClassExporterImporter")
    public static final String EXPORTER_IMPORTER_CLASS_DOES_NOT_SUPPORT_INTERFACE = "7";

    @RBEntry("Problem in export/import process.")
    public static final String ERROR_IN_IXB = "8";

    @RBEntry("Exception: \"{0}\"")
    public static final String EXCEPTION = "9";

    @RBEntry("Could not export unknown content item: \"{0}\"")
    public static final String UNKNOWN_CONTENT_ITEM_CLASS_IGNORED = "10";

    @RBEntry("Unknown mapping rule command: \"{0}\"")
    public static final String UNKNOWN_MAPPING_RULE_COMMAND = "11";

    @RBEntry("Can not open required import/export handlers registry file.")
    public static final String CAN_NOT_OPEN_IXB_HANDLERS_REGISTRY_FILE = "12";

    @RBEntry("Can not open multiple object export attribute handlers registry file.")
    public static final String CAN_NOT_OPEN_IXB_MULTI_OBJ_ATTR_HANDLERS_REGISTRY_FILE = "CAN_NOT_OPEN_IXB_MULTI_OBJ_ATTR_HANDLERS_REGISTRY_FILE";

    @RBEntry("Can not open import/export service setup file \"{0}\".")
    public static final String CAN_NOT_OPEN_IXB_SETUP_FILE = "13";

    @RBEntry("Can not open set handlers registry file.")
    public static final String CAN_NOT_OPEN_IXB_SET_HANDLERS_REGISTRY_FILE = "14";

    @RBEntry("Can not open DTD file \"{0}\".")
    public static final String CAN_NOT_OPEN_DTD_FILE = "15";

    @RBEntry("{0}")
    @RBPseudo(false)
    public static final String EMPTY_STRING_WITH_PARAMETER = "16";

    @RBEntry("Problem parsing xml (line {0}, column {1}): {2}")
    public static final String XML_PARSE_ERROR = "17";

    @RBEntry("Error in XML input file")
    public static final String ERROR_WHILE_PARSING_XML = "18";

    @RBEntry("Null InputStream passed to XML parser")
    public static final String XML_PARSER_NULL_INPUT_STREAM = "19";

    @RBEntry("Empty InputStream passed to XML parser")
    public static final String XML_PARSER_EMPTY_INPUT_STREAM = "20";

    @RBEntry("Bad InputStream passed to XML parser: it is all whitespace")
    public static final String XML_PARSER_BAD_INPUT_STREAM_WHITE_SPACE = "21";

    @RBEntry("Bad InputStream passed to XML parser: it does not start with <?xml version=\"1.0\" ?>")
    public static final String XML_PARSER_BAD_INPUT_STREAM_NO_XML_VERSION = "22";

    @RBEntry("Bad InputStream passed to XML parser: it does not have ?> in its first line")
    public static final String XML_PARSER_BAD_INPUT_STREAM_HEAD_NOT_CLOSED = "23";

    @RBEntry("Now exporting object: {0}...")
    public static final String EXPORT_OBJECT = "24";

    @RBEntry("Warning: there is no export handler for class {0}; object: {1}")
    public static final String EXPORT_OBJECT_NO_HANDLER = "25";

    @RBEntry("Export process elaborated {0} objects")
    public static final String EXPORT_OBJECT_ELABORATED_COUNT = "26";

    @RBEntry("Started navigation to choose objects for export...")
    public static final String EXPORT_OBJECT_SET_NAVIGATE_START = "27";

    @RBEntry("Navigation: elaborate object: {0}")
    public static final String EXPORT_OBJECT_SET_NAVIGATE_ELABORATE = "28";

    @RBEntry("Chose {0} objects that are candidates for export (the number of really exported objects can be less)")
    public static final String EXPORT_OBJECT_SET_NAVIGATE_FOUND_N_OBJECTS = "29";

    @RBEntry("Started export for chosen objects...")
    public static final String EXPORT_START_XMLIZE_OBJECT = "30";

    @RBEntry("Started sending result to client...")
    public static final String EXPORT_START_SENDING_RESULT = "31";

    @RBEntry("Export process exported {0} objects")
    public static final String EXPORT_OBJECT_REALLY_EXPORTED_COUNT = "32";

    @RBEntry("Amount of XML document(s) to be imported: {0}")
    public static final String IMPORT_OBJECT_START_N_XML_FILES = "41";

    @RBEntry("Import process finished")
    public static final String IMPORT_OBJECT_FINISH = "42";

    @RBEntry("Supplied for import {0} XML documents")
    public static final String IMPORT_OBJECT_GOT_N_XML_FILES = "43";

    /**
     * #34.value=Amount of XML document supplied for import: {0}
     * #34.constant=IMPORT_OBJECT_GOT_AMOUNT_XML_FILES
     **/
    @RBEntry("Started conflict check process...")
    public static final String IMPORT_OBJECT_START_CONFLICT_CHECK = "45";

    @RBEntry("Checked conflicts for {0} XML files")
    public static final String IMPORT_OBJECT_CONFLICT_CHECK_N_XML_FILES = "46";

    @RBEntry("Started object creation process...")
    public static final String IMPORT_OBJECT_START_REAL_IMPORT = "47";

    @RBEntry("Processed {0} XML files in object creation process")
    public static final String IMPORT_OBJECT_REAL_IMPORT_N_XML_FILES = "48";

    @RBEntry("Created {0} new objects, representing objects from XML files")
    public static final String IMPORT_CREATED_NEW_OBJECTS = "49";

    @RBEntry("Found {0} existing objects, representing objects from XML files")
    public static final String IMPORT_FOUND_OLD_OBJECTS = "50";

    @RBEntry("Checking conflicts for XML file: {0}")
    public static final String IMPORT_CHECK_CONFLICT_FOR_DOC = "51";

    @RBEntry("There is no import handler for tag: {0} in DTD: {1}")
    public static final String IMPORT_NO_HANDLER = "52";

    @RBEntry("Importing info from XML file: {0}. Applied action : {1}")
    public static final String IMPORT_REAL_IMPORT_FOR_DOC = "53";

    @RBEntry("Created new object: {0}")
    public static final String IMPORT_OBJECT_IS_NEW = "54";

    @RBEntry("Did not create new object because found corresponding existing object: {0}")
    public static final String IMPORT_OBJECT_IS_OLD = "55";

    @RBEntry("Error: can not navigate through product structure because there is no WTPart configuration specification, associated with current user")
    public static final String NO_WTPART_CONFIG_SPEC_WHILE_PRODUCT_STRUCTURE_NAVIGATION = "70";

    @RBEntry("Export preview - list of objects that will be exported:")
    public static final String LIST_OF_OBJECT_TO_BE_EXPORTED = "71";

    @RBEntry("There are {0} objects that will be exported")
    public static final String THERE_ARE_N_OBJECTS_TO_BE_EXPORTED = "72";

    @RBEntry("   {0}")
    public static final String PARAMETER_WITH_SHIFT = "73";

    @RBEntry("Referenced document not found.")
    public static final String REFERENCED_DOC_NOT_FOUND = "74";

    @RBEntry("DescribedBy document not found.")
    public static final String DESCRIBEDBY_DOC_NOT_FOUND = "75";

    @RBEntry("Part used in Part Structure not found.")
    public static final String USED_PART_NOT_FOUND = "76";

    @RBEntry("Unable to find object with object id: {0}")
    @RBArgComment0("object identifier to denote the object; e.g. wt.part.WTPart: 23456")
    public static final String OBJ_NOT_FOUND = "77";

    @RBEntry("Attribute missing in xml file.\nPlease verify tag <{0}>")
    @RBComment("the '\n' provides a new-line in the message")
    @RBArgComment0("Tag name of the attribute to be verified inside the xml file\"")
    public static final String ATTRIBUTE_NOT_FOUND_XML = "78";

    @RBEntry("Version or iteration attributes missing in xml file.\nPlease verify <versionId>, <iterationId>, <versionLevel> and <series> tags.")
    @RBComment("the '\n' provides a new-line in the message")
    public static final String VERSION_ITERATION_NOT_FOUND_XML = "79";

    @RBEntry("Life cycle name or state attributes missing in xml file.\nPlease verify <lifecycleTemplateName> and <lifecycleState> tags.")
    @RBComment("the '\n' provides a new-line in the message")
    public static final String LIFECYCLE_NOT_FOUND_XML = "80";

    @RBEntry("Can not create and save AbstractAttributeDefinition object (type: {0}) with name: {1}, because it exists with type: {2}")
    @RBArgComment0("name of the type of the AbstractAttributeDefinition object, e.g. wt.iba.definition.StringDefinition")
    @RBArgComment1("name of the AbstractAttributeDefinition object")
    @RBArgComment2("name of the type of the AbstractAttributeDefinition object, e.g. wt.iba.definition.StringDefinition")
    public static final String IBA_DEFINITION_EXIST_WITH_NAME = "81";

    @RBEntry("EPM Document not found.")
    public static final String EPM_DOCUMENT_NOT_FOUND = "82";

    @RBEntry("EPMDocumentMaster not found.")
    public static final String EPM_DOCUMENT_MASTER_NOT_FOUND = "83";

    @RBEntry("Can not export objects that are in personal cabinets")
    public static final String CANNOT_EXP_OBJECTS_IN_PERSONAL_CABINETS = "84";

    @RBEntry("Create new team : {0}")
    public static final String CREATE_NEW_TEAM = "85";

    @RBEntry("Processed {0} XML files in import preview process")
    public static final String IMPORT_PREVIEW_N_XML_FILES = "86";

    @RBEntry("Object {0} exists in database.")
    public static final String OBJECT_EXISTS = "87";

    @RBEntry("Cannot import the object version {0}.{1} which is previous version of the existed version {2} with Normal import.\nYou can still import it with Import as a new iteration, Import as a new version or Import as checked out object.")
    public static final String CANNOT_IMPORT_PREVIOUS_OBJECT = "88";

    @RBEntry("List of exported objects:")
    public static final String LIST_OF_EXPORTED_OBJECT = "89";

    @RBEntry("Document used in Document structure not found.")
    public static final String USED_DOC_NOT_FOUND = "90";

    @RBEntry("Build source not found")
    public static final String BUILD_SOURCE_NOT_FOUND = "91";

    @RBEntry("Build target not found")
    public static final String BUILD_TARGET_NOT_FOUND = "92";

    @RBEntry("Can not export document {0}. Document resides in personal cabinet. Please move/checkin the document to shared folder first.")
    public static final String CANNOT_EXP_CADDOCUMENT_IN_PERSONAL_CABINET = "93";

    @RBEntry("The EPMUsesOccurrence that built occurrence {0} not found")
    public static final String BUILT_FROM_OCCURRENCE_NOT_FOUND = "94";

    @RBEntry("Built link not found")
    public static final String BUILT_LINK_NOT_FOUND = "95";

    @RBEntry("Child document master not found")
    public static final String CHILD_DOC_MASTER_NOT_FOUND = "96";

    @RBEntry("Unable to mark usage link as built")
    public static final String CANNOT_MARK_LINK_AS_BUILT = "97";

    @RBEntry("Occurrence with name {0} not found")
    public static final String NAMED_OCCURRENCE_NOT_FOUND = "98";

    @RBEntry("Unknown action: \"{0}\"")
    @RBArgComment0("name of the action to be applied to object, such as \"Checkout\", \"Lock\".")
    public static final String UNKNOWN_ACTION_COMMAND = "99";

    @RBEntry("The object \"{0}\" is already checked out by other principal: \"{1}\"")
    @RBArgComment0("The object identifier")
    @RBArgComment1("The name of the other principal")
    public static final String ALREADY_CHECKEDOUT_BY_OTHER_PRINCIPAL = "100";

    @RBEntry("The object \"{0}\" is already locked by other principal: \"{1}\"")
    @RBArgComment0("The object identifier")
    @RBArgComment1("The name of the other principal")
    public static final String ALREADY_LOCKED_BY_OTHER_PRINCIPAL = "101";

    @RBEntry("WARNING: The object \"{0}\" is already checked out. The \"Checkout\" operation is ignored.")
    @RBArgComment0("The object identifier")
    public static final String ALREADY_CHECKEDOUT_BY_CURR_PRINCIPAL = "102";

    @RBEntry("WARNING: The object \"{0}\" is already locked. The \"Lock\" operation is ignored.")
    @RBArgComment0("The object identifier")
    public static final String ALREADY_LOCKED_BY_CURR_PRINCIPAL = "103";

    @RBEntry("Can not create AttributeOrganizer because the name exists for IBA Definitions: \"{0}\".")
    @RBArgComment0("name of the AttributeOrganizer to be created")
    public static final String NAME_EXISTS_FOR_ATTRIBUTE_ORGANIZER = "104";

    @RBEntry("Unable to import. Choose different import action. EPM Handlers do not support these actions: \"{0}\".")
    @RBArgComment0("List of actions not supported by EPM handlers (Substiture, ignore, Create new identity).")
    public static final String EPM_ACTION_NOT_SUPPORTED = "105";

    @RBEntry("The object \"{0}\" is excluded from export because of the configuration specification.")
    @RBArgComment0("Object identifier")
    public static final String EXPORT_OBJECT_SKIPPED_FOR_CONFIGSPEC = "106";

    @RBEntry("The object \"{0}\" is ignored for export.")
    @RBArgComment0("Object identifier")
    public static final String EXPORT_OBJECT_IGNORED = "107";

    @RBEntry("The XML file with tag \"{0}\" is ignored for import.")
    @RBArgComment0("XML file tag")
    public static final String IMPORT_OBJECT_IGNORED = "108";

    @RBEntry(" Ignore, do not import the object \"{0}\".")
    @RBArgComment0("object id of the object which is ignored.")
    public static final String IGNORE_OBJECT = "109";

    @RBEntry("Ignore {0} objects")
    @RBArgComment0("amount of objects which are ignored during import process.")
    public static final String IMPORT_IGNORED_OBJECTS = "110";

    @RBEntry("Attributes authoringApplication, ownerApplication and docType in EPMDocument are not changable once the object has been persisted.")
    public static final String ATTRIBUTES_NOT_CHANGABLE = "111";

    @RBEntry("The object is not found.")
    public static final String THE_OBJECT_NOT_FOUND = "112";

    @RBEntry("The CAD Documents / Dynamic Documents are checking out to the workspace named \"{0}\"")
    public static final String EPM_CHECKOUT_WORKSPACE_NAME = "113";

    @RBEntry("Import action \"{0}\" is not applicable for \"{1}\".")
    public static final String IMPORT_ACTION_IS_INAPPLICABLE = "114";

    @RBEntry("Could not apply import action \"{0}\" to object \"{1}\".")
    public static final String IMPORT_ACTION_EXCEPTION = "115";

    @RBEntry("Could not export object \"{0}\".")
    public static final String OBJECT_EXPORT_EXCEPTION = "116";

    @RBEntry("Could not find object \"{0}\".")
    public static final String OBJECT_NOT_FOUND_EXCEPTION = "117";

    @RBEntry("Could not locate object \"{0}\".")
    public static final String OBJECT_NOT_LOCATED_EXCEPTION = "118";

    @RBEntry("Could not create \"{0}\".")
    public static final String OBJECT_CREATION_EXCEPTION = "119";

    @RBEntry("Could not import attributes of object \"{0}\".")
    public static final String ATTRIBUTE_IMPORT_EXCEPTION = "120";

    @RBEntry("Could not check attribute conflict for \"{0}\".")
    public static final String ATTRIBUTE_CONFLICT_CHECK_EXCEPTION = "121";

    @RBEntry("Could not update attribute \"{1}\" for \"{0}\".")
    public static final String ATTRIBUTE_UPDATE_EXCEPTION = "122";

    @RBEntry("Updating attribute \"{0}\" to value \"{1}\" has been vetoed.")
    public static final String ATTRIBUTE_UPDATE_VETO_EXCEPTION = "123";

    @RBEntry("Could not fetch attribute \"{1}\" for \"{0}\".")
    public static final String ATTRIBUTE_GET_EXCEPTION = "124";

    @RBEntry("Could not export reference for \"{0}\".")
    public static final String OBJECT_REFERENCE_EXPORT_EXCEPTION = "125";

    @RBEntry("Could not get information through reference to locate \"{0}\".")
    public static final String OBJECT_REFERENCE_IMPORT_EXCEPTION = "126";

    @RBEntry("Could not instantiate \"{0}\".")
    public static final String OBJECT_REFERENCE_HANDLER_INSTATIATION_EXCEPTION = "127";

    @RBEntry("Object reference handler is not specified for \"{0}\".")
    public static final String OBJECT_REFERENCE_HANDLER_NOT_SPECIFIED_EXCEPTION = "128";

    @RBEntry("Could not locate export handler for \"{0}\".")
    public static final String OBJECT_REFERENCE_EXPORT_HANDLER_NOT_FOUND_EXCEPTION = "129";

    @RBEntry("Could not update attribute \"{1}\" with value \"{0}\".")
    public static final String SIMPLE_ATTRIBUTE_UPDATE_EXCEPTION = "130";

    @RBEntry("Updating attribute \"{0}\" to value \"{1}\" has been vetoed.")
    public static final String SIMPLE_ATTRIBUTE_UPDATE_VETO_EXCEPTION = "131";

    @RBEntry("Can not determine if \"{0}\" already exists prior to import.")
    public static final String OBJECT_EXISTENCE_CAN_NOT_DETERMINE_EXCEPTION = "132";

    @RBEntry("Could not export CabinetManaged object \"{0}\".")
    public static final String CABINETMANAGED_OBJECT_EXPORT_EXCEPTION = "133";

    @RBEntry("Could not import CabinetManaged object \"{0}\".")
    public static final String CABINETMANAGED_OBJECT_IMPORT_EXCEPTION = "134";

    @RBEntry("Could not locate domain \"{0}\".")
    public static final String DOMAIN_NOT_FOUND_EXCEPTION = "135";

    @RBEntry("Could not export Managed object \"{0}\".")
    public static final String MANAGED_OBJECT_EXPORT_EXCEPTION = "136";

    @RBEntry("Could not import Managed object \"{0}\".")
    public static final String MANAGED_OBJECT_IMPORT_EXCEPTION = "137";

    @RBEntry("Could not establish classification path for \"{0}\".")
    public static final String CLASSIFICATION_COULD_NOT_ESTABLISH_EXCEPTION = "138";

    @RBEntry("Missing substitute identity for \"{0}\".")
    public static final String MISSING_SUBSTITUTE_IDENTITY_EXCEPTION = "139";

    @RBEntry("Could not export attributes of object \"{0}\".")
    public static final String ATTRIBUTE_EXPORT_EXCEPTION = "140";

    @RBEntry("Could not export content of object \"{0}\".")
    public static final String CONTENT_EXPORT_EXCEPTION = "141";

    @RBEntry("Could not export session iterated \"{0}\" that has no session owner.")
    public static final String OBJECT_NOT_SESSION_ITERATED_EXCEPTION = "142";

    @RBEntry("Can not export baseline that has no member for \"{0}\".")
    public static final String BASELINE_NO_MEMBER_FOUND_EXCEPTION = "143";

    @RBEntry("Could not create a new iteration as configspec could not locate an already existing object.")
    public static final String CONFIG_SPEC_BASED_ITERATION_NOT_FOUND_EXCEPTION = "144";

    @RBEntry("Creation of a new iteration is vetoed out.")
    public static final String CONFIG_SPEC_BASED_NEW_ITERATION_CREATION_EXCEPTION = "145";

    @RBEntry("Could not create a new version as configspec could not locate an already existing object.")
    public static final String CONFIG_SPEC_BASED_VERSION_NOT_FOUND_EXCEPTION = "146";

    @RBEntry("Object already exists! Can not create a new object of same identity.")
    public static final String CREATE_NEW_OBJECT_WITH_OBJECT_ALREADY_EXISTING_EXCEPTION = "147";

    @RBEntry("Could not locate actor for \"{0}\".")
    public static final String ACTOR_NOT_FOUND_EXCEPTION = "148";

    @RBEntry("Could not initalize actor factory.")
    public static final String ACTOR_FACTORY_INITIALIZATION_EXCEPTION = "149";

    @RBEntry("Substitute object does not exist!")
    public static final String SUBSTITUTE_OBJECT_DOES_NOT_EXIST_EXCEPTION = "150";

    @RBEntry("No import action specified.")
    public static final String IMPORT_ACTION_NOT_SPECIFIED_EXCEPTION = "151";

    @RBEntry("Could not locate import handler for \"{0}\".")
    public static final String OBJECT_REFERENCE_IMPORT_HANDLER_NOT_FOUND_EXCEPTION = "152";

    @RBEntry("Object does not exist!")
    public static final String OBJECT_DOES_NOT_EXIST_EXCEPTION = "153";

    @RBEntry("\"{0}\": Version scheme mismatch : \"{1}\"")
    public static final String VERSION_SCHEME_MISMATCH = "154";

    @RBEntry("FormalizedBy object not found.")
    public static final String FORMALIZEDBY_NOT_FOUND = "155";

    @RBEntry("ResearchedBy object not found.")
    public static final String RESEARCHEDBY_NOT_FOUND = "156";

    @RBEntry("AddressedBy object not found.")
    public static final String ADDRESSEDBY_NOT_FOUND = "157";

    @RBEntry("IncludedIn object not found.")
    public static final String INCLUDEDIN_NOT_FOUND = "158";

    @RBEntry("DetailedBy object not found.")
    public static final String DETAILEDBY_NOT_FOUND = "159";

    @RBEntry("AcceptedStrategy object not found.")
    public static final String ACCEPTEDSTRATEGY_NOT_FOUND = "160";

    @RBEntry("ReportedAgainst object not found.")
    public static final String REPORTEDAGAINST_NOT_FOUND = "161";

    @RBEntry("ProblemProduct object not found.")
    public static final String PROBLEMPRODUCT_NOT_FOUND = "162";

    @RBEntry("SubjectProduct object not found.")
    public static final String SUBJECTPRODUCT_NOT_FOUND = "163";

    @RBEntry("AffectedActivityData object not found.")
    public static final String AFFECTEDACTIVITYDATA_NOT_FOUND = "164";

    @RBEntry("ChangeRecord object not found.")
    public static final String CHANGERECORD2_NOT_FOUND = "165";

    @RBEntry("RelevantRequestData object not found.")
    public static final String RELEVANTREQUESTDATA_NOT_FOUND = "166";

    @RBEntry("RelevantAnalysisData object not found.")
    public static final String RELEVANTANALYSISDATA_NOT_FOUND = "167";

    @RBEntry("Exporting objects.")
    public static final String SUMMARY_EVENT_EXPORT_MESSAGE = "168";

    @RBEntry("Importing objects.")
    public static final String SUMMARY_EVENT_IMPORT_MESSAGE = "169";

    @RBEntry("Published content holder or representable object not found.")
    public static final String PUBLISHEDCONTENTHOLDER_OR_REPRESENTABLE_OBJECT_NOT_FOUND = "170";

    @RBEntry("HangingChangeLink object not found.")
    public static final String HANGINGCHANGE_NOT_FOUND = "171";

    @RBEntry("Supporting data for link object is not found.")
    public static final String SUPPORTINGDATAFOR_NOT_FOUND = "172";

    @RBEntry("Model item not found.")
    public static final String MODEL_ITEM_NOT_FOUND = "173";

    @RBEntry("Updated {0} objects, representing objects from XML files")
    public static final String IMPORT_UPDATED_OBJECTS = "174";

    @RBEntry("Updated an existing object: {0}")
    public static final String IMPORT_OBJECT_IS_UPDATED = "175";

    @RBEntry("HangingChangeLink object ignored because Unincorporated Change Creation preference is not defined: {0}")
    public static final String HANGINGCHANGE_PREFERENCE_NOT_FOUND = "176";

    @RBEntry("The object \"{0}\" is checked out.")
    @RBArgComment0("Object identifier.")
    public static final String OBJECT_CHECKED_OUT = "exp_chkout";

    @RBEntry("The object \"{0}\" is locked.")
    @RBArgComment0("Object identifier.")
    public static final String OBJECT_LOCKED = "exp_lock";

    @RBEntry("The object \"{0}\" has an administrative lock applied.")
    @RBArgComment0("Object identifier.")
    public static final String OBJECT_ADMIN_LOCKED = "OBJECT_ADMIN_LOCKED";

    @RBEntry("The object \"{0}\" is locked and cannot be checked out.")
    @RBArgComment0("Object identifier.")
    public static final String OBJECT_LOCK_TO_CHECK_OUT = "lock_to_ckout";

    @RBEntry("Operation is aborted from client.")
    public static final String OP_ABORTED_FROM_CLIENT = "op_aborted_from_client";

    @RBEntry("Can not open the required registry file for export priority.")
    public static final String CAN_NOT_OPEN_IXB_EXPORT_PRIORITY_REGISTRY_FILE = "no_export_priority_registry";

    @RBEntry("Total number of exported objects:")
    public static final String TOTAL_NUMBER_OF_EXPORTED_OBJECT = "total_exp_objs";

    @RBEntry("Action \"{0}\" is not applicable to object \"{1}\".")
    @RBArgComment0("Action name")
    @RBArgComment1("Object identifier or object type, class name, etc")
    public static final String IMPORT_ACTION_NOT_APPLICABLE_AND_CHANGED = "imp_action_not_appl_and_changed";

    @RBEntry("Warning! The IBA attribute is expected to be deleted from \"{0}\" during import, however it is not found in the attribute container. Ignore deleting this IBA: path: \"{1}\", type: \"{2}\", value: \"{3}\".")
    @RBArgComment0("IBA Holder")
    public static final String IBA_SHLD_EXCLUDED_BUT_NOT = "iba_exc_but_not";

    @RBEntry("The Icon could not be found: \"{0}\"")
    @RBArgComment0("Icon name, representing a file")
    public static final String ICON_COULD_NOT_FOUND = "icon_not_found";

    @RBEntry("The Icon is renamed from \"{0}\" to \"{1}\" due to icon name conflicts")
    public static final String ICON_RENAMED = "icon_renamed";

    @RBEntry("Check out the latest iteration to import the object  \"{0}\"")
    public static final String CHECKOUT_OBJECT = "checkout_object";

    @RBEntry("Check out {0} objects")
    public static final String IMPORT_CHECKOUT_OBJECTS = "import_checkout_object";

    @RBEntry("TeamTemplate from XML file is in the domain \"{0}\", but the object is in the domain \"{1}\" . TeamTemplate of the object is set because \"Resolve Overridable Conflicts\" is checked.")
    public static final String TEAM_TEMPLATE_AND_OBJECT_DOMAIN_ARE_IN_DIFFERENT_POSITION = "team_template_and_object_domain_are_in_different_postion";

    @RBEntry("You do not map the container-path for the container path {0} or the WTContainerRef is not found.")
    public static final String CONTAINER_REF_NOT_FOUND = "container_ref_not_found";

    @RBEntry("Error while processing the XML file (s): {0}.")
    public static final String ERROR_PROCESS_XML_FILE = "error_process_xml_file";

    @RBEntry("Invalid organization identifier format: \"{0}\".")
    public static final String INVALID_ORG_ID_FORMAT = "invalid_org_id_format";

    @RBEntry("The object \"{0}\" is already unlocked.")
    public static final String OBJECT_UNLOCKED = "obj_unlocked";

    @RBEntry("The value \"{1}\" must be set for CreateNewObjectActor for TypeDefinition \"{0}\"")
    @RBArgComment0("TypeDefinition full path")
    @RBArgComment1("Tag name: \"actionInfo/actionParams/newName\"")
    public static final String MISSING_INFO_FOR_CREATE_NEW_OBJ_ACTOR_FOR_TYPE = "missing_info_create_new_type";

    @RBEntry("The value \"{1}\" and \"{2}\" must be set for CreateNewObjectActor for TypeDefinition \"{0}\"")
    @RBArgComment0("TypeDefinition full path")
    @RBArgComment1("Tag name: \"actionInfo/actionParams/newName\"")
    @RBArgComment2("Tag name: \"actionInfo/actionParams/newPath\"\"")
    public static final String MISSING_INFO_FOR_SUBSTITUTE_ACTOR_FOR_TYPE = "missing_info_substitute_type";

    @RBEntry("The object \"{0}\" is not checked out")
    public static final String OBJ_NOT_CHECK_OUT = "obj_not_checked_out";

    @RBEntry("For object \"{0}\": RatioValue is no longer supported. It is changed to FloatValue.")
    @RBArgComment0("Object identifier holding this IBA value")
    public static final String IBA_VALUE_CHANGED_FROM_RATIO_TO_FLOAT = "value_changed_ratio_to_float";

    @RBEntry("RatioDefinition is no longer supported. It is changed to FloatDefinition: \"{0}\"")
    @RBArgComment0("Full path of the IBA Definition.")
    public static final String IBA_DEF_CHANGED_FROM_RATIO_TO_FLOAT = "def_changed_ratio_to_float";

    @RBEntry("The jar file has not been created because no objects have been exported.")
    public static final String JAR_FILE_NOT_CREATED = "jar_not_created";

    @RBEntry("The jar file {0} does not contains any object's XML file.")
    @RBArgComment0("The name of the jar file.")
    public static final String JAR_FILE_DOES_NOT_CONTAINS_XML = "jar_file_does_not_contains_xml";

    @RBEntry("The folder \"{0}\" does not exist.")
    public static final String FOLDER_DOES_NOT_EXIST = "folder_does_not_exist";

    @RBEntry("The folder \"{0}\" is created.")
    public static final String FOLDER_CREATED = "folder_created";

    @RBEntry("The cabinet \"{0}\" is created.")
    public static final String CABINET_CREATED = "cabinet_created";

    @RBEntry("Import is not allowed because the type definition \"{0}\" is checked out.")
    @RBArgComment0("The name and the path of the type definition.")
    public static final String IMPORT_NOT_ALLOWED_DUETO_TYPE_CHECKEDOUT = "imp_not_allowed_due_type_checked_out";

    @RBEntry("Check out type definitions on export is not allowed.")
    public static final String CHECKOUT_TYPE_ON_EXPORT_NOT_ALLOWED = "chkout_type_on_exp_disallowed";

    @RBEntry("The type definition \"{0}\" exists without non-overridable conflicts. The action \"{1}\" is ignored.")
    public static final String USE_EXISTING_TYPE_BCOZ_NO_CONF = "use_existing_type_bcoz_no_conf";

    @RBEntry("Import of view version is not supported for Default action. Import file information (object= \"{0}\", version= \"{1}\", level= \"{2}\").")
    @RBArgComment0("The object being imported.")
    @RBArgComment1("The version Label being imported.")
    @RBArgComment2("The version level being imported.")
    public static final String BRANCHED_VERSION_IMPORT_NOT_SUPPORTED = "branched_version_imp_not_supported";

    @RBEntry("\"{0}\" : Import completed")
    @RBArgComment0("Name of the succeded jar in import jar file")
    public static final String IMPORT_RESULT_COMPLETED = "IMPORT_RESULT_COMPLETED";

    @RBEntry("\"{0}\" : Import failed")
    @RBArgComment0("Name of the failed jar in import jar file")
    public static final String IMPORT_RESULT_FAILED = "IMPORT_RESULT_FAILED";

    @RBEntry("Successfully imported jar file count in import jar: {0}")
    @RBArgComment0("Successfully imported jar file count in import jar")
    public static final String IMP_JAR_IN_JAR_SUCCESS_ENTRY_COUNT = "IMP_JAR_IN_JAR_SUCCESS_ENTRY_COUNT";

    @RBEntry("Failed to import jar file count in import jar : {0}")
    @RBArgComment0("Un successfully imported jar file count in import jar")
    public static final String IMP_JAR_IN_JAR_FAILED_ENTRY_COUNT = "IMP_JAR_IN_JAR_FAILED_ENTRY_COUNT";

    @RBEntry("Summary of jar in jar import result.....")
    public static final String IMP_JAR_IN_JAR_RESULT_SUMMARY = "IMP_JAR_IN_JAR_RESULT_SUMMARY";

    @RBEntry("Failed to import jar files list in import jar : {0}")
    @RBArgComment0("List of jar files in the import jar for which import failed.")
    public static final String IMP_JAR_IN_JAR_FAILED_ENTRY_LIST = "IMP_JAR_IN_JAR_FAILED_ENTRY_LIST";

    @RBEntry("File name or content invalid : {0}")
    @RBArgComment0("The content holder identity whose content or file name is invalid")
    public static final String INVALID_CONTENT_ERROR = "invalid_content_error";

    @RBEntry("\"{0}\": action is not allowed for type definition. Importing with Default action. See the Windchill System Administrator's Guide for further information on how to update type definition.")
    public static final String TYPE_DEFINITION_UPDATE_NOT_ALLOWED = "type_definition_update_not_allowed";

    @RBEntry("(attribute type: \"{0}\", path: \"{1}\"): Instance Based Attribute has been ignored because it does not exist in the type definition: \"{2}\". This may be created but not used.")
    public static final String IBA_IGNORED_DUE_TO_TYPEDEF_MISMATCH = "iba_ignored_due_to_typedef_mismatch";

    @RBEntry("Cross release import is not allowed. See the Windchill System Administrator's Guide for further information on how to enable this feature.")
    public static final String CROSS_RELEASE_IMPORT_NOT_ALLOWED = "cross_release_import_not_allowed";

    @RBEntry("\"{0}\": Action is not valid for import of View Version as no version exists. Import file information (object= \"{1}\", version= \"{2}\", level= \"{3}\").")
    @RBArgComment0("The action used for Import")
    @RBArgComment1("The object being imported.")
    @RBArgComment2("The version Label being imported.")
    @RBArgComment3("The version level being imported.")
    public static final String EXISTING_VERSION_NOT_FOUND_FOR_VIEW_VERSION_IMPORT = "EXISTING_VERSION_NOT_FOUND_FOR_VIEW_VERSION_IMPORT";

    @RBEntry("The constraint data size does not match the constraint data: {0}")
    @RBArgComment0("The constraint type for which there has been a data size mismatch")
    public static final String CONSTRAINT_DATA_SIZE_MISMATCH = "constraint_data_size_mismatch";

    @RBEntry("Organization \"{0}\" cannot be found. See the Windchill System Administrator's Guide for further information.")
    public static final String ORGANIZATION_NOT_FOUND = "organization_not_found";

    @RBEntry("\"{0}\" : attribute value was not available during export due to access permission. Please provide a valid value before import.")
    public static final String ACTION_NEEDED_FOR_SECURED_INFO = "action_needed_for_secured_info";

    @RBEntry("Malformed configuration for \"{0}\".")
    public static final String MALFORMED_RESOLVER = "malformed_resolver";

    @RBEntry("No stream ID found for content: {0}")
    public static final String NO_STREAM_ID_FOR_CONTENT_ID = "NO_STREAM_ID_FOR_CONTENT_ID";

    @RBEntry("No content found for stream ID: {0}")
    public static final String NO_CONTENT_ID_FOR_STREAM_ID = "NO_CONTENT_ID_FOR_STREAM_ID";

    @RBEntry("Size information not found for content item: {0}")
    public static final String CONTENT_SIZE_NOT_FOUND = "CONTENT_SIZE_NOT_FOUND";

    @RBEntry("Error getting checksum for content Id: {0}")
    public static final String ERROR_GETTING_CHECKSUM_FOR_CONTENT_ID = "ERROR_GETTING_CHECKSUM_FOR_CONTENT_ID";

    @RBEntry("Cannot open the required import/export multiformat registry file.")
    public static final String CAN_NOT_OPEN_IXB_MULTIPLEFORMAT_REGISTRY_FILE = "CAN_NOT_OPEN_IXB_MULTIPLEFORMAT_REGISTRY_FILE";

    @RBEntry("\"{0}\" is successfully imported.")
    public static final String OBJECT_IMPORT_SUCCESS = "object_import_success";

    @RBEntry("\"{0}\" is not imported as object already exists. ")
    public static final String OBJECT_IMPORT_EXIST = "object_import_exist";

    @RBEntry("\"{0}\" could not be imported.")
    public static final String OBJECT_IMPORT_ERROR = "object_import_error";

    @RBEntry("\"{0}\" could not be imported. Contact your administrator.")
    public static final String OBJECT_IMPORT_ERROR_CONTACT_ADMINISTRATOR = "OBJECT_IMPORT_ERROR_CONTACT_ADMINISTRATOR";

    @RBEntry("\"{0}\" is ignored for import.")
    public static final String OBJECT_IMPORT_WARNING = "object_import_warning";

    @RBEntry("'{0}' federation mapping existed, from '{1}' to '{2}'.")
    public static final String FEDERATION_MAPPING_HAPPEN = "federation_mapping_happen";

    @RBEntry("Comparing '{0}', old value is '{1}', new value is '{2}'.")
    public static final String COMPARISON_MESSAGE = "comparison_message";

    @RBEntry("'{0}' is successfully exported")
    public static final String OBJECT_EXPORT_SUCCESS = "object_export_success";

    @RBEntry("'{0}' could not be exported")
    public static final String OBJECT_EXPORT_ERROR = "object_export_error";

    @RBEntry("Chunk file is '{0}'.")
    public static final String CHUNK_FILE = "chunk_file";

    @RBEntry("'{0}' is exported to chunk file '{1}'.")
    public static final String EXPORT_TO_CHUNK_FILE = "export_to_chunk_file";

    @RBEntry("Resolution '{0}' is selected to resolve conflict '{1}'.")
    public static final String RESOLUTION_CONFLICT = "resolution_conflict";

    @RBEntry("Inconsistency in collaboration_interest.xml. '{0}' XPath is mapped multiple times.")
    public static final String DUPLICATION_IN_COLLABORATION_INTEREST_FOR_XPATH = "duplication_in_collaboration_interest_for_xpath";

    @RBEntry("Inconsistency in collaboration_interest.xml. '{0}' target path is mapped multiple times.")
    public static final String DUPLICATION_IN_COLLABORATION_INTEREST_FOR_TARGET_PATH = "duplication_in_collaboration_interest_for_target_path";

    @RBEntry("Native format not found for target iba '{0}' in collaboration_interest.xml")
    public static final String NATIVE_FORMAT_NOT_FOUND_FOR_TARGET_IBA = "native_format_not_found_for_target_iba";

    @RBEntry("'{0}': XPath not found in Object XML or CollaborationInfo XML")
    public static final String XPATH_NOT_PRESENT_IN_XML = "xpath_not_present_in_xml";

    @RBEntry("Incorrect mapping for xpath in collaboration_interest.xml: '{0}'")
    public static final String INCORRECT_MAPPING = "incorrect_mapping";

    @RBEntry("\"{0}\" value is defaulted to \"{1}\" as this information is missing in export jar.")
    public static final String DEFAULT_PRINCIPAL_SET = "DEFAULT_PRINCIPAL_SET.";

    @RBEntry("Type class: '{0}' is invalid. Cannot import this type: {1}. Please create this type first.")
    public static final String INVALID_TYPE_CLASS = "invalid_type_class";

    @RBEntry("The type '{0}' cannot be imported into this system. Its name '{1}' has been used by other type '{2}'")
    public static final String TYPE_NAME_UNIQUE_VIOLATION = "type_name_unique_violation";

    @RBEntry("Creating type '{0}' is failed during importing type '{1}'")
    public static final String CREATING_TYPE_FAILED = "creating_type_failed";

    @RBEntry("This system doesn't allow to import a new type '{0}'")
    public static final String CANNOT_IMPORT_NEW_TYPE = "cannot_import_new_type";

    @RBEntry("Cannot find type '{0}' in the system")
    public static final String CANNOT_FIND_TYPE = "cannot_find_type";

    @RBEntry("Import of attribute '{0}' failed. Specified datatype '{1}' does not match existing datatype '{2}'.")
    public static final String INVALID_DATATYPE = "invalid_datatype";

    @RBEntry("This system doesn't allow to import a new attribute '{0}' into an existing type '{1}'")
    public static final String CANNOT_IMPORT_NEW_ATTRIBUTE = "cannot_import_new_attribute";

    @RBEntry("New attribute '{0}' is imported into type '{1}'.")
    public static final String IMPORTED_NEW_ATTRIBUTE = "imported_new_attribute";

    @RBEntry("New type '{0}' is imported.")
    public static final String IMPORTED_NEW_TYPE = "imported_new_type";

    @RBEntry("Importing a new attribute '{0}' into an existing type '{1}' is failed")
    public static final String IMPORTING_NEW_ATTRIBUTE_FAILED = "importing_new_attribute_failed";

    @RBEntry("Type '{0}' cannot create a standard attribute.")
    public static final String CANNOT_CREATE_STANDARD_ATTRIBUTE = "cannot_create_standard_attribute";

    @RBEntry("Cannot set type '{0}' for Typed object: '{1}'")
    public static final String CANNOT_SET_TYPE = "cannot_set_type";

    @RBEntry("This operation is not supported from class: \"{0}\"")
    public static final String UNSUPPORTED_OPERATION_EX = "unsupported_operation_exception";

    @RBEntry("Import is not supported for content only.")
    public static final String CONTENT_ONLY_IMPORT_NOT_SUPPORTED = "content_only_import_not_supported";

    @RBEntry("The export/import object: \"{0}\" is invalid. Must be the class: \"{1}\".")
    public static final String INVALID_EXPORT_IMPORT_OBJECT = "invalid_export_import_object";

    @RBEntry("The export/import object: \"{0}\" cannot have the attribute: \"{1}\" with null value.")
    public static final String NULL_ATTRIBUTE_VALUE = "null_attribute_value";

    @RBEntry("Argument \"{0}\" cannot be null")
    public static final String NULL_ARGUMENT = "null_argument";

    @RBEntry("Logical type identifier \"{0}\" in the container \"{1}\" is invalid.")
    public static final String INVALID_LOGICAL_TYPE_ID = "invalid_logical_type_id";

    @RBEntry("Persisted type identifier \"{0}\" is invalid.")
    public static final String INVALID_PERSISTED_TYPE_ID = "invalid_persisted_type_id";

    @RBEntry("Persisted type attribute \"{0}\" has null value.")
    public static final String NULL_PERSISTED_TYPE_ID = "null_persisted_type_id";

    @RBEntry("Please ensure that Classification Nodes in specified path are separated by '/'.")
    public static final String CLASSIFICATION_NODE_PATH_MESSAGE = "classification_node_path_message";

    @RBEntry("Unable to find a Root Classification Node - '{0}'.")
    public static final String CLASSIFICATION_ROOT_NODE_NOT_FOUND = "classification_root_node_not_found";

    @RBEntry("Unable to find a Classification Node - '{0}'.")
    public static final String CLASSIFICATION_NODE_NOT_FOUND = "classification_node_not_found";

    @RBEntry("Invalid streamer used for server side Export/Import. Use '{0}'")
    public static final String INVALID_STREAMER_TYPE_FOR_SERVERSIDE_EXPORT_IMPORT = "invalid_streamer_type_for_serverside_export_import";

    @RBEntry("Invalid streamer used for client side Export/Import. Use '{0}'")
    public static final String INVALID_STREAMER_TYPE_FOR_CLIENTSIDE_EXPORT_IMPORT = "invalid_streamer_type_for_clientside_export_import";

    @RBEntry("For existing object default actor can not be used without version information")
    public static final String DEFAULT_ACTOR_HAS_NO_VERSION_INFO = "default_actor_has_no_version_info";

    @RBEntry("The object \"{0}\" passed to the filter can not be processed by \"{1}\" .")
    public static final String INVALID_OBJECT_FOR_CONTENT_FILTER = "invalid_object_for_content_filter";

    @RBEntry("Invalid file. File is generated from \"{0}\"")
    public static final String INVALID_FILE = "invalid_file";

    @RBEntry("This importable package delivery file is not formatted correctly. Contact the sender of the delivery.")
    public static final String INVALID_PACKAGE_FILE = "invalid_package_file";

    @RBEntry("This file is not formatted correctly. Contact your administrator if the problem persists.")
    public static final String IX_INVALID_IMPORT_FILE = "ix_invalid_import_file";

    @RBEntry("This is not an importable package delivery file. Import this file using the Import/Export Management utility.")
    public static final String IX_INVALID_IMPORT_PACKAGE = "ix_invalid_import_package";

    @RBEntry("\"{0}\" had \"{1}\" relationship with another object that was not included in the package delivery. The relationship and missing object were ignored on import.")
    @RBArgComment0("Non-missing end (object identity) of the link")
    @RBArgComment1("Type of the Link")
    public static final String REL_IGNORED_MISSING_OTHER_END = "REL_IGNORED_MISSING_OTHER_END";

    @RBEntry("\"{0}\" relationship was not included in the package delivery. The relationship and missing object were ignored on import.")
    @RBArgComment0("Type of the Link")
    public static final String REL_IGNORED_BOTH_ENDS_MISSING = "REL_IGNORED_BOTH_ENDS_MISSING";

    @RBEntry("Quantity must be defined if Reference Designators need to be assigned.")
    public static final String QUANTITY_NOT_DEFINED = "QUANTITY_NOT_DEFINED";

    @RBEntry("Reference designators can only be assigned to parts with units of each.")
    public static final String UNIT_HAS_TO_BE_EACH = "UNIT_HAS_TO_BE_EACH";

    @RBEntry("Quantity must be equal to or greater than the number of reference designators.")
    public static final String SUBSTITUTE_QUANTITY_LESS_THAN_REFDESIG = "SUBSTITUTE_QUANTITY_LESS_THAN_REFDESIG";

    @RBEntry("The number of reference designators for a single part has exceeded the limit specified in property entry wt.part.MaxQuantity. Contact your administrator for more details.")
    public static final String REFDESIG_EXCEEDS_QUANTITY_LIMIT = "REFDESIG_EXCEEDS_QUANTITY_LIMIT";

    @RBEntry("Value must be a positive whole number when the unit is Each")
    @RBComment("Error message for invalid quantity value")
    public static final String QUANTITY_AMOUNT_ZERO_NUMBER_VALUE_MSG = "Quantity_Amount_Zero_Number_Value_Msg";

    @RBEntry("Unit of each requires a non-negative number for quantity.")
    public static final String UnitMsg = "UnitMsg";

    @RBEntry("Quantity amount must be a whole number when the unit is Each")
    @RBComment("Error message for invalid quantity value")
    public static final String QUANTITY_AMOUNT_NOT_REAL_NUMBER_VALUE = "QUANTITY_AMOUNT_NOT_REAL_NUMBER_VALUE";

    @RBEntry("Quantity amount is not defined.")
    @RBComment("Quantity amount is not defined.")
    public static final String QUANTITY_AMOUNT_NOT_DEFINED = "QUANTITY_AMOUNT_NOT_DEFINED";

    @RBEntry("Invalid number format: \"{0}\"")
    public static final String INVALID_NUMBER_FORMAT = "INVALID_NUMBER_FORMAT";

    @RBEntry("Quantity unit is not defined.")
    @RBComment("Quantity unit is not defined.")
    public static final String QUANTITY_UNIT_NOT_DEFINED = "QUANTITY_UNIT_NOT_DEFINED";

    @RBEntry("Context \"{0}\" does not exist.")
    public static final String CONTEXT_NOT_FOUND = "CONTEXT_NOT_FOUND";

    @RBEntry("Invalid Content File name - '{0}'. Name contains invalid characters not allowed by system - '{1}'.")
    @RBArgComment0("Content File name.")
    @RBArgComment1("Invalid character set.")
    public static final String CONTENT_NAME_CONTAINS_INVALID_CHARS = "CONTENT_NAME_CONTAINS_INVALID_CHARS";

    @RBEntry("Invalid Content File name - '{0}'. Number of characters - '{1}' in file name exceeds the maximum allowed by system - '{2}'.")
    @RBArgComment0("Content File name.")
    @RBArgComment1("Actual number of characters in file name.")
    @RBArgComment2("Maximum number of characters allowed.")
    public static final String CONTENT_NAME_CONTAINS_INVALID_NO_OF_CHARS = "CONTENT_NAME_CONTAINS_INVALID_NO_OF_CHARS";

    @RBEntry("Occurrence with UsesOccurrenceGlobalId {0} not found")
    public static final String USES_OCCURRENCE_GLOBAL_ID_NOT_FOUND = "USES_OCCURRENCE_GLOBAL_ID_NOT_FOUND";

    @RBEntry("Substitute part not found.")
    public static final String SUBSTITUTE_FOR_PART_NOT_FOUND = "SUBSTITUTE_FOR_PART_NOT_FOUND";

    @RBEntry("Alternate part not found.")
    public static final String ALTERNATE_FOR_PART_NOT_FOUND = "ALTERNATE_FOR_PART_NOT_FOUND";

    @RBEntry("This import file is not valid")
    public static final String INVALID_IMPORT_FILE = "INVALID_IMPORT_FILE";

    @RBEntry("Superseded part not found")
    public static final String SUPERSEDED_PART_NOT_FOUND = "SUPERSEDED_PART_NOT_FOUND";

    @RBEntry("\"{0}\" cannot be imported because the import files contain an invalid \"{1}\" attribute value \"{2}\" for the \"{3}\" element.")
    @RBComment("Exception that occurs when import fails because the import files contain invalid attributes.")
    @RBArgComment0("Identity of the object being imported.")
    @RBArgComment1("the attribute name")
    @RBArgComment2("the value of the invalid attribute")
    @RBArgComment3("the element name that contains the invalid attribute")
    public static final String UNKNOWN_ATTRIBUTE_EXCEPTION = "UNKNOWN_ATTRIBUTE_EXCEPTION";

    @RBEntry("\"{0}\" cannot be imported because you do not have authorization for the participant identified by the \"{1}\" element or the participant does not exist.")
    @RBComment("Exception that occurs when import fails because the participant is not found and conflict handling was either not called or did not resolve this issue.")
    @RBArgComment0("Identity of the object being imported")
    @RBArgComment1("Name of the owner attribute")
    public static final String PARTICIPANT_NOT_FOUND_EXCEPTION = "PARTICIPANT_NOT_FOUND_EXCEPTION";

    @RBEntry("The \"{0}\" parameter value cannot include: \"{1}\". This value is only valid when the \"{2}\" parameter value is set to: \"{3}\".")
    @RBComment("WTInvalidParameterException: A conflict resolutions parameter value is not valid for the specified conflict type parameter.")
    @RBArgComment0("Name of conflict resolutions parameter")
    @RBArgComment1("Value of conflict resolutions parameter")
    @RBArgComment2("Name of conflict type parameter")
    @RBArgComment3("Value included in the set of conflict resolutions that is not valid")
    public static final String CONFLICTING_RESOLUTIONS_EXCEPTION = "CONFLICTING_RESOLUTIONS_EXCEPTION";

    @RBEntry("The default resolution \"{0}\" does not exist in the set of all available resolutions.")
    @RBComment("WTInvalidParameterException: the default resolution does not exist in the set of all available resolutions")
    @RBArgComment0("Name of the default resolution")
    public static final String NON_EXISTENT_DEFAULT_RESOLUTION_EXCEPTION = "NON_EXISTENT_DEFAULT_RESOLUTION_EXCEPTION";

    @RBEntry("The adhoc business field '{0}' cannot be created.")
    @RBArgComment0("The adhoc business filed name")
    public static final String ADHOC_BUSINESS_FIELD_CANNOT_CREATED = "ADHOC_BUSINESS_FIELD_CANNOT_CREATED";

    @RBEntry("The business fields '{0}' for '{1}' cannot be found in the current system.")
    @RBArgComment0("Business field name.")
    @RBArgComment1("Object identifier of the object being imported.")
    public static final String BUSINESS_FIELD_NOT_FOUND = "BUSINESS_FIELD_NOT_FOUND";

    @RBEntry("The business fields '{0}' for '{1}' are incompatible with the business field definitions in the current system.")
    @RBArgComment0("Business field names.")
    @RBArgComment1("Object identifier of the object being imported.")
    public static final String BUSINESS_FIELD_DATA_STRUCTURE_NOT_MATCHED = "BUSINESS_FIELD_DATA_STRUCTURE_NOT_MATCHED";

    @RBEntry("ObjectRef handler cannot be found for the \"{0}\" class referenced by \"{1}\" of \"{2}\".")
    @RBArgComment0("name of the class of the referenced object.")
    @RBArgComment1("name of the XML attribute that refers the object reference.")
    @RBArgComment2("id of the object to be imported.")
    public static final String OBJECT_REF_HANDLER_NOT_FOUND = "OBJECT_REF_HANDLER_NOT_FOUND";
}
