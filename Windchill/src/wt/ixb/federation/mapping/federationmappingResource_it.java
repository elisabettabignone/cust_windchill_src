package wt.ixb.federation.mapping;

import wt.util.resource.*;

public final class federationmappingResource_it extends WTListResourceBundle {
   /**
    * Entry
    **/
   @RBEntry(" FederationMapping")
   public static final String PRIVATE_CONSTANT_0 = "FEDERATIONMAPPING_CATEGORY";

   @RBEntry("Federation Mappings")
   public static final String PRIVATE_CONSTANT_1 = "FEDERATIONMAPPING_CATEGORY_DESC";

   @RBEntry(" FederationMapping Client")
   public static final String PRIVATE_CONSTANT_2 = "FEDERATIONMAPPING_CLIENT";

   @RBEntry("FederationMapping Client")
   public static final String PRIVATE_CONSTANT_3 = "FEDERATIONMAPPING_CLIENT_DESC";

   @RBEntry(" Lifecycle")
   public static final String PRIVATE_CONSTANT_4 = "LIFECYCLE";

   @RBEntry(" Lifecycle Mappings")
   public static final String PRIVATE_CONSTANT_5 = "LIFECYCLE_SHORT_DESC";

   @RBEntry(" Lifecycle Mappings")
   public static final String PRIVATE_CONSTANT_6 = "LIFECYCLE_LONG_DESC";

   @RBEntry(" Revision")
   public static final String PRIVATE_CONSTANT_7 = "REVISION";

   @RBEntry(" Revision Mappings")
   public static final String PRIVATE_CONSTANT_8 = "REVISION_SHORT_DESC";

   @RBEntry(" Revision Mappings")
   public static final String PRIVATE_CONSTANT_9 = "REVISION_LONG_DESC";

   @RBEntry(" Location")
   public static final String PRIVATE_CONSTANT_10 = "LOCATION";

   @RBEntry(" Location Mappings")
   public static final String PRIVATE_CONSTANT_11 = "LOCATION_SHORT_DESC";

   @RBEntry(" Location Mappings")
   public static final String PRIVATE_CONSTANT_12 = "LOCATION_LONG_DESC";
}
