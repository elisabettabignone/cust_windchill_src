/* bcwti
 *
 * Copyright (c) 2013 Parametric Technology Corporation (PTC). All Rights
 * Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.ixb.step;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBArgComment1;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.ixb.step.stepdexResource")
public class stepdexResource extends WTListResourceBundle {

    @RBEntry("{0} EXPRESS schema not found in EXPRESS Database")
    @RBArgComment0("Name of the EXPRESS Schema. Schema refers to grammatical representation of a data model.")
    @RBComment("Error message thrown if a given EXPRESS Schema is not loaded in EXPRESS Database.Here EXPRESS refers to a modelling language.")
    public static final String SCHEMA_NOT_FOUND = "SCHEMA_NOT_FOUND";

    @RBEntry("EXPRESS Database by name {0} does not exist at location {1}.")
    @RBArgComment0("Name of the Database")
    @RBArgComment1("Folder location of the EXPRESS Database")
    @RBComment("Error message thrown when the configured EXPRESS Database details are incorrect")
    public static final String EXPRESS_DB_NOT_FOUND = "EXPRESS_DB_NOT_FOUND";

    @RBEntry("{0} EXPRESS-X Mapping schema not found")
    @RBArgComment0("Name of the Mapping Schema")
    @RBComment("Error message thrown if Export Mapping Schema is not loaded in EXPRESS Database. Here EXPRESS-X refers to Mapping schema Language provided by third party. Mapping is a techincal term refers to element mapping between two distinct data models.")
    public static final String MAPPING_SCHEMA_NOT_FOUND = "EXPORT_MAPPING_SCHEMA_NOT_FOUND";

    @RBEntry("License key property is not configured. Configure property: {0}")
    @RBArgComment0("Name of the property used in configuration properties file")
    @RBComment("Error message thrown if EXPRESS Database License key property is not configured in configuration file.")
    public static final String LICENSE_KEY_NOT_FOUND = "LICENSE_KEY_NOT_FOUND";

    @RBEntry("Unable to set Runtime License key for EXPRESS Database. Check License key: {0}")
    @RBArgComment0("Configured License key")
    @RBComment("Error message thrown if EXPRESS Database License key is invalid.")
    public static final String INVALID_LICENCE_KEY = "INVALID_LICENCE_KEY";

    @RBEntry("Invalid Configuration for EXPRESS Database. Configure following required properties: {0}")
    @RBComment("Error message thrown if EXPRESS Database configuration file does not contain required properties")
    @RBArgComment0("List of required properties.")
    public static final String INVALID_CONFIGURATION = "INVALID_CONFIGURATION";

    @RBEntry("Invalid Method")
    public static final String INVALID_METHOD = "INVALID_METHOD";

    @RBEntry("Header schema does not exist in EDM. Please check the EDM configuration for header schema")
    @RBComment("Error message thrown if Header_section_schema is missing in EDM")
    public static final String HEADER_SCHEMA_NOT_EXIST = "HEADER_SCHEMA_NOT_EXIST";

}
