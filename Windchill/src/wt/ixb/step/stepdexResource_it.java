/* bcwti
 *
 * Copyright (c) 2013 Parametric Technology Corporation (PTC). All Rights
 * Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.ixb.step;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBArgComment1;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.ixb.step.stepdexResource")
public class stepdexResource_it extends WTListResourceBundle {

    @RBEntry("Impossibile trovare lo schema EXPRESS {0} nel database EXPRESS")
    @RBArgComment0("Name of the EXPRESS Schema. Schema refers to grammatical representation of a data model.")
    @RBComment("Error message thrown if a given EXPRESS Schema is not loaded in EXPRESS Database.Here EXPRESS refers to a modelling language.")
    public static final String SCHEMA_NOT_FOUND = "SCHEMA_NOT_FOUND";

    @RBEntry("Il database EXPRESS {0} non esiste nella posizione {1}.")
    @RBArgComment0("Name of the Database")
    @RBArgComment1("Folder location of the EXPRESS Database")
    @RBComment("Error message thrown when the configured EXPRESS Database details are incorrect")
    public static final String EXPRESS_DB_NOT_FOUND = "EXPRESS_DB_NOT_FOUND";

    @RBEntry("Impossibile trovare lo schema di mappatura EXPRESS-X {0}")
    @RBArgComment0("Name of the Mapping Schema")
    @RBComment("Error message thrown if Export Mapping Schema is not loaded in EXPRESS Database. Here EXPRESS-X refers to Mapping schema Language provided by third party. Mapping is a techincal term refers to element mapping between two distinct data models.")
    public static final String MAPPING_SCHEMA_NOT_FOUND = "EXPORT_MAPPING_SCHEMA_NOT_FOUND";

    @RBEntry("La proprietà del codice di licenza non è valida o non è configurata.")
    @RBComment("Error message thrown if EXPRESS Database License key is not configured properly.")
    public static final String LICENSE_KEY_NOT_FOUND = "LICENSE_KEY_NOT_FOUND";

    @RBEntry("Impossibile impostare il codice di licenza di esecuzione per il database EXPRESS. Verificare il codice di licenza: {0}")
    @RBArgComment0("Configured License key")
    @RBComment("Error message thrown if EXPRESS Database License key is invalid.")
    public static final String INVALID_LICENCE_KEY = "INVALID_LICENCE_KEY";

    @RBEntry("Configurazione non valida per il database EXPRESS. Configurare le proprietà obbligatorie seguenti: {0}")
    @RBComment("Error message thrown if EXPRESS Database configuration file does not contain required properties")
    @RBArgComment0("List of required properties.")
    public static final String INVALID_CONFIGURATION = "INVALID_CONFIGURATION";

    @RBEntry("Metodo non valido")
    public static final String INVALID_METHOD = "INVALID_METHOD";

    @RBEntry("Lo schema dell'intestazione non esiste in EDM. Verificare lo schema dell'intestazione nella configurazione EDM")
    @RBComment("Error message thrown if Header_section_schema is missing in EDM")
    public static final String HEADER_SCHEMA_NOT_EXIST = "HEADER_SCHEMA_NOT_EXIST";

    @RBEntry("Convalida dati non riuscita. Consultare i log disponibili in {0} per i dettagli")
    @RBComment("Error message thrown if validation failed during the process of export and import")
    @RBArgComment0("Folder location where the error log file are downloaded.")
    public static final String VALIDATION_FAIL = "VALIDATION_FAIL";

    @RBEntry("Il file jar non contiene il file specifico del metodo di implementazione STEP")
    @RBComment("Error message thrown when import repository unable to identify importable file. STEP is not translatable")
    public static final String IMPORTABLE_FILE_NOT_FOUND = "IMPORTABLE_FILE_NOT_FOUND";

    @RBEntry("La funzionalità di scambio di dati STEP è attualmente sospesa. Contattare l'amministratore")
    @RBComment("Word STEP is not translatable")
    public static final String STEPDEX_NOT_INSTALLED_OR_SUSPENDED = "STEPDEX_NOT_INSTALLED_OR_SUSPENDED";

    @RBEntry("La definizione del formato è ambigua. Fornire i dettagli del protocollo e del metodo di implementazione per lo scambio dei dati")
    @RBComment("Either protocol or implementation method is missing")
    public static final String AMBIGIUOS_FORMAT_TYPE = "AMBIGIUOS_FORMAT_TYPE";

    @RBEntry("Operazione non riuscita. L'accesso multiplo al database EXPRESS è negato.")
    @RBComment("Error message to be thrown when resource is busy")
    public static final String MULTIPLE_DB_ACCESS_DENIED = "MULTIPLE_DB_ACCESS_DENIED_EXCEPTION";

    @RBEntry("Operazione non riuscita. Il database EXPRESS è occupato.")
    @RBComment("Error message to be thrown when no resources are found to process")
    public static final String EXPRESS_DATABASES_BUSY = "EXPRESS_DATABASES_BUSY";

    @RBEntry("Errore di configurazione. Impossibile accedere al database EXPRESS.")
    @RBComment("Error message to be when no resources are found to process")
    public static final String EXPRESS_DATABASES_CONFIGURATION_EXCEPTION = "EXPRESS_DATABASES_CONFIGURATION_EXCEPTION";

    @RBEntry("Il modello convertito {0} non contiene alcuna entità dello schema Windchill EXPRESS.")
    @RBComment("The word EXPRESS is not translatable. 'Model' is the noun refering to java object in STEP database.")
    @RBArgComment0("Name of the model.")
    public static final String EMPTY_EDM_MODEL = "EMPTY_EDM_MODEL";


}
