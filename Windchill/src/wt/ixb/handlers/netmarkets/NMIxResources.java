/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.ixb.handlers.netmarkets;

import wt.util.resource.*;

@RBUUID("wt.ixb.handlers.netmarkets.NMIxResources")
@RBNameException //Grandfathered by conversion
public final class NMIxResources extends WTListResourceBundle {
   @RBEntry("Export of {0} is not supported for this release.")
   public static final String EXPORT_NOT_SUPPORTED = "0";

   @RBEntry("Illegal XML element, expected: {0} found {1}.")
   public static final String ILLEGAL_TAG = "1";

   @RBEntry("Cannot create folder structure inside of a non-existent cabinet: Missing cabinet = {0}")
   public static final String NO_CABINET_FOR_FOLDER = "2";

   @RBEntry("Cannot find root folder {0}")
   public static final String CANNOT_FIND_ROOT_FOLDER = "3";

   @RBEntry("Folder {0} already exists. Continuing...")
   public static final String FOLDER_EXISTS = "4";

   @RBEntry("Failed to create subFolder {0}")
   public static final String FAILED_SUBFOLDER = "5";

   @RBEntry("Principal {0} does not exist.")
   public static final String PRINCIPAL_DOES_NOT_EXIST = "6";

   @RBEntry("Class {0} is invalid for access control rules.")
   public static final String INVALID_ACL_CLASS         = "7";

   @RBEntry("Proposed project sponsor: {0} not found")
   public static final String NO_PROJECT_SPONSOR       = "8";

   @RBEntry("Null project context")
   public static final String NULL_PRJ_CONTAINER = "9";

   @RBEntry("Illegal XML element, expected {0} or {1} found {2}.")
   public static final String ILLEGAL_TAG_CHOICE   = "10";

   @RBEntry("Default")
   public static final String DEFAULT_NAME  = "11";

   @RBEntry("Seed object must be either a <WTPart> or <WTDocument>")
   public static final String ILLEGAL_RULE_SEED = "12";

   @RBEntry("No system domain for ProjectContainer {0}.")
   public static final String NO_SYSTEM_DOMAIN_FOR_PC = "13";

   @RBEntry("Illegal XML")
   public static final String ILLEGAL_XML = "14";

   @RBEntry("Null rule type in import rule.")
   public static final String NULL_RULE_TYPE = "15";

   @RBEntry("Cannot create a type based rule with an empty or missing <className> tag.")
   public static final String NO_CLASS = "16";

   @RBEntry("Class: {0} not found in codebase.")
   public static final String MISSING_CLASS = "17";

   @RBEntry("Bad class name in getObjectFromOid : {0}")
   public static final String BAD_CLASS_NAME = "18";

   @RBEntry("Problem parsing xml, line: {0} column: {1} message: {2}")
   public static final String XML_PARSE_ERROR = "19";

   @RBEntry("Loading {0}")
   public static final String LOAD_TYPE = "20";

   @RBEntry("{0} loaded.")
   public static final String TYPE_LOADED = "21";

   @RBEntry("Loading {0} {1}")
   public static final String LOAD_OBJECT = "22";

   @RBEntry("Object {0} {1} loaded.")
   public static final String OBJECT_LOADED = "23";

   @RBEntry("Workflows and Lifecycles")
   public static final String WORKFLOWS_LIFECYCLES = "24";

   @RBEntry("Executing rule {0}")
   public static final String EXECUTE_RULE = "25";

   @RBEntry("Rule {0} executed")
   public static final String RULE_EXECUTED = "26";

   @RBEntry("Import project completed successfully.")
   public static final String PROJECT_IMPORT_SUCCEEDED = "27";

   @RBEntry("Organization import succeeded successfully.")
   public static final String ORG_IMPORT_SUCCEEDED = "28";

   @RBEntry("Mapfile: {0} does not exist")
   public static final String NO_CSV_MAPFILE = "29";

   @RBEntry("Directory: {0} does not exist")
   public static final String NO_DIRECTORY = "30";

   @RBEntry("Processing file: {0}")
   public static final String PROCESSING_FILE = "31";

   @RBEntry("No files to process, returning.")
   public static final String NO_FILES_TO_PROCESS = "32";

   @RBEntry("Could not find default NotebookTemplate")
   public static final String DEF_NOTEBOOK_TEMPLATE_NOT_FOUND = "33";

   @RBEntry("DiscussionForumTemplate {0} not found, and no default template is available.")
   public static final String NO_FORUM_TEMPLATE = "34";

   @RBEntry("Rule {0} not found.")
   public static final String RULE_NOT_FOUND = "35";

   @RBEntry("Failed to create object from rule {0}")
   public static final String CREATE_FROM_RULE_FAILED = "36";

   @RBEntry("No handler for tag: {0}")
   public static final String NO_HANDLER_CSVTAG = "37";

   @RBEntry("Null or empty handler class name for NmLoader element.")
   public static final String NULL_CSV_CLASS_STRING = "38";

   @RBEntry("Null or empty handler method name for NmLoader element.")
   public static final String NULL_CSV_METHOD_STRING = "39";

   @RBEntry("<groupName></groupName> cannot be empty")
   public static final String GROUP_CANT_BE_EMPTY = "40";

   @RBEntry("Content file not specified.")
   public static final String CONTENT_NOT_SPECIFIED = "41";

   @RBEntry("Conflict during load. You cannot make use static instances of NMHandler")
   public static final String NMHANDLER_CANT_BE_STATIC = "42";

   @RBEntry("Domain must be specified for a team.")
   public static final String TEAM_NEEDS_DOMAIN = "43";

   @RBEntry("<folderPath> element cannot be empty")
   public static final String FOLDER_PATH_EMPTY = "44";

   @RBEntry("Actor and role must be specified when define a RoleActorMap")
   public static final String MISSING_ROLE_ACTOR = "45";

   @RBEntry("Role and Principal must be specified when defining a RolePrincipalMap")
   public static final String MISSING_ROLE_PRINCIPAL = "46";

   @RBEntry("<domainName> cannot be empty in the <Team> element")
   public static final String TEAM_HAS_NO_DOMAIN = "47";

   @RBEntry("No organization specified.")
   public static final String NO_ORG_SPECIFIED = "48";

   @RBEntry("File list {0}: {1}")
   public static final String FILE_LIST_ITEM = "49";

   @RBEntry("You must specify a set of XML files or a directory containing XML files.")
   public static final String WHERE_IS_MY_XML = "50";

   @RBEntry("Invalid basePath: {0}")
   public static final String INVALID_BASE_PATH = "51";

   @RBEntry("<typeName> element cannot be empty")
   public static final String TYPE_NAME_EMPTY = "52";

   @RBEntry("XML file is all whitespace, or has no content")
   public static final String XML_EMPTY = "53";

   @RBEntry("There is no <?xml string, this is not an XML file")
   public static final String XML_WITH_NO_XML_PI = "54";

   @RBEntry("Minimal XML file needs to start with <?xml version=1.0 ?>")
   public static final String XML_MINIMAL = "55";

   @RBEntry("Error closing input stream")
   public static final String ERROR_CLOSING_STREAM = "56";

   @RBEntry("Null InputStream passed into XML validator")
   public static final String XML_IS_NULL = "57";

   @RBEntry("InputStream is empty")
   public static final String EMPTY_INPUT_STREAM = "58";

   @RBEntry("Failed to create document from template file")
   public static final String FAILED_TEMPLATE_CREATE = "59";

   @RBEntry("Illegal aclState attribute: {0}")
   public static final String ILLEGAL_ACL_STATE_VALUE = "60";

   /**
    * more than one rule with the same name and type exists in the database
    **/
   @RBEntry("More than one {0} rule named {1} exists in the database.")
   public static final String TOO_MANY_RULE_WITH_SAME_NAME_AND_TYPE = "61";

   @RBEntry("There was no metadata to import, please try another file.")
   public static final String IMPORT_FILE_NEEDS_XML = "62";

   /**
    * Export messages
    **/
   @RBEntry("Exported {0} of {1} objects")
   public static final String EXPORTED_N_OF_M_OBJECTS = "63";

   @RBEntry("Exporting AdHoc ACLs")
   public static final String EXPORTING_AD_HOCS = "64";

   @RBEntry("Exporting Viewables")
   public static final String EXPORTING_VIEWABLES = "65";

   @RBEntry("Exported viewables for {0} of {1} objects with viewables.")
   public static final String EXPORTED_N_OF_M_VIEWABLES = "66";

   @RBEntry("Done exporting AdHoc ACLs")
   public static final String DONE_EXPORTING_AD_HOCS = "67";

   @RBEntry("Illegal document type for export DocumentTemplate")
   public static final String ILLEGAL_DOC_TYPE = "68";

   @RBEntry("Could not find rule name for Document with name= {0} and number= {1}")
   public static final String RULE_NOT_FOUND_FOR_DOC_TEMPLATE = "69";

   @RBEntry("Could not find ProjectConfig for ProjectTemplate import")
   public static final String PROJECT_CONFIG_NOT_AVAILABLE = "70";

   @RBEntry("{0} is not a supported rule type")
   public static final String ILLEGAL_RULE_TYPE = "71";

   @RBEntry("Rule name must contain at least one non-blank character")
   public static final String ILLEGAL_RULE_NAME = "73";

   @RBEntry("There is no {0} rule associated with the document template {1} ({2})")
   public static final String NO_TYPE_RULE_FOR_DOC_TEMPLATE = "74";

   /**
    * 
    **/
   @RBEntry("Document templates must be exported as DocumentTemplate not a TemplateDocument")
   public static final String CANT_EXPORT_DOCTEMPLATE_AS_TEMPLATEDOC = "75";

   @RBEntry("Illegal XML: location tag cannot be empty")
   public static final String ILLEGAL_FOLDERLINK_LOCATION_ENTRY = "76";

   @RBEntry("Illegal XML: {0} tag must contain a valid url, could not construct URL from {1}")
   public static final String ILLEGAL_FOLDERLINK_URL_ENTRY = "77";

   @RBEntry("Illegal XML: FolderLink {0} tag cannot be empty")
   public static final String ILLEGAL_FOLDERLINK_TAG_ENTRY = "78";

   @RBEntry("Illegal seed object type {0}")
   public static final String ILLEGAL_SEED_OBJECT_TYPE = "79";

   @RBEntry("Illegal ProjectTemplate XML: No template content specified")
   public static final String NO_PROJECT_TEMPLATE_SPEC = "80";

   @RBEntry("Import/Export of markup is currently only supported for Documents and Parts")
   public static final String CAN_ONLY_EXPORT_MARKUP_FOR_DOCS_AND_PARTS = "81";

   @RBEntry("Unsupported Markup type {0}")
   public static final String UNSUPPORTED_MARKUP_TYPE = "82";

   @RBEntry("Viewable {0} needs to have content item(s)")
   public static final String VIEWABLE_NEEDS_CONTENT = "83";

   @RBEntry("You must have administrative privileges in order to import objects.")
   public static final String IMPORT_NOT_ALLOWED = "84";

   @RBEntry("Warning, the user {0} does not exist in LDAP, assiging the person resource to {1}, you will need to re-assign or delete this resource")
   public static final String NO_LDAP_ENTRY = "85";

   @RBEntry(" The following enumerated type entries had bad characters, {0}, please consult the customizer's guide and repair the XML file")
   public static final String ILLEGAL_CHARACTRERS_IN_ROLE_NAME = "86";

   @RBEntry("Illegal XML element, expected one of the following values {0} instead found found {1}.")
   public static final String ILLEGAL_TAG_CHOICES = "87";

   @RBEntry("The value of the <containerClassName> in the <ContainerTemplate> XML element cannot be null or an empty string")
   public static final String CONTAINER_CLASS_NAME_NEEDED = "88";

   @RBEntry("The passed in class name, {0} is not present in the system")
   public static final String CONTAINER_CLASS_NOT_THERE = "89";

   @RBEntry("{0} is not a WTContainer")
   public static final String CONTAINER_CLASS_NAME_NOT_A_WTCONTAINER = "90";

   @RBEntry("Could not find team for {0}. Contact your administrator.")
   public static final String CTM_OBJECT_HAS_NO_TEAM = "91";

   @RBEntry("Export terminated because the export request would result in no objects being exported")
   public static final String NOTHING_TO_EXPORT = "92";

   @RBEntry("{0} is not an access group, so policy rule cannot be created")
   public static final String PRINCIPAL_NOT_AN_ACCESS_GROUP = "93";

   @RBEntry("Could not find domain: {0} contact your administrator")
   public static final String COULD_NOT_FIND_DOMAIN_FROM_PATH = "94";

   @RBEntry("Could not find domain: {0} with parent domain {1} contact your administrator")
   public static final String COUND_NOT_FIND_DOMAIN_UNDER_PARENT = "95";

   @RBEntry("Context template must have a name")
   public static final String CONTAINER_TEMPLATE_NEEDS_A_NAME = "96";

   @RBEntry("Illegal XML: The tag {0} cannot be empty or contain only white space")
   public static final String XML_VALUE_CANNOT_BE_EMPTY = "97";

   @RBEntry("There needs to be at least a single persistable in the export list")
   public static final String EXPORT_LIST_NEEDS_A_PERSISTABLE = "98";

   @RBEntry("Source container reference cannot be null")
   public static final String SRC_CONT_REF_CANT_BE_NULL = "99";

   @RBEntry("Source container must be persistent")
   public static final String SRC_CONT_MUST_BE_PERSISTENT = "100";

   @RBEntry("Tmpfile cannot be null for doExport")
   public static final String TMPFILE_CANT_BE_NULL = "101";

   @RBEntry("Export generator needs a persistable, contact your administrator")
   public static final String EXP_GEN_NEEDS_PERSISTABLE = "102";

   @RBEntry("Cannot add a null export generator, contact your administrator")
   public static final String CANT_ADD_NULL_GENERATOR = "103";

   @RBEntry("Illegal context type {0} Can only export Policy rules for a ContainerTeamManaged Context")
   public static final String CAN_ONLY_EXPORT_POLICIES_FOR_CTM = "104";

   @RBEntry("There is more than a single project forum for the project, contact your administrator")
   public static final String TOO_MANY_PROJECT_FORUM_TEMPLATES = "105";

   @RBEntry("Input error, you specified skipXML but passed in an XML file as a command line argument")
   public static final String SKIP_XML_BUT_ONLY_XML = "106";

   @RBEntry("Input error, you specified skipJars but passed in a jar file explicitly on the command line")
   public static final String SKIP_JARS_BUT_ONLY_JAR = "107";

   @RBEntry("Loader method {0}.{1} reported a failure")
   public static final String LOADER_METHOD_FAILED_2ARGS = "108";

   @RBEntry("A Context Template for {0} called {1} for locale {2} already exists in container {3} please use the create template UI to create your template")
   public static final String USE_CREATE_TEMPLATE_UI_INSTEAD_OF_IMPORT = "109";

   @RBEntry("Context Template has already been persisted")
   public static final String CONTEXT_TEMPLATE_ALREADY_PERSISTED = "110";

   @RBEntry("Could not find org container for context: {0}")
   public static final String COULD_NOT_FIND_ORGCONT_FOR_CONTEXT = "111";

   @RBEntry("Unexpected folder type")
   public static final String UNEXPECTED_FOLDER_TYPE = "112";

   @RBEntry("Cannot convert an XML file to XML file with this utility")
   public static final String CANT_CONVERT_XML_TO_XML = "113";

   @RBEntry("Attention: Folder {0} cannot be created.\n\nYou do not have access to create a folder in domain {1}.  Please try another domain or contact your administrator to modify your access.")
   @RBComment("domain is specified for a nested folder and the user does not have read access to the domain.")
   @RBArgComment0("Name of folder being created.")
   @RBArgComment1("Name of domain being assigned to folder being created.")
   public static final String FAILED_NESTED_FOLDER_DOMAIN_ACCESS = "114";

   @RBEntry("Attention: Folder {0} cannot be created.\n\nDomain {1} entered is not a subdomain of {2}. Please try another domain.")
   @RBComment("The domain is not the same domain or a subdomain as the domain of the parent folder.")
   @RBArgComment0("Name of folder being created.")
   @RBArgComment1("Name of domain being assigned to folder being created.")
   @RBArgComment2("Name of domain assigned to parent folder of the folder being created.")
   public static final String FAILED_NESTED_FOLDER_NOT_SUBDOMAIN = "115";

   @RBEntry("Parent domain name {0} cannot be resolved during import because it is not unique in container {1}.")
   public static final String NON_UNIQUE_DOMAIN_NAME = "116";

   @RBEntry("The shared team {0} exists. Import failed.")
   public static final String FOUND_IDENTICAL_SHAREDTEAM = "117";

   @RBEntry("Please remove all references to the shared team \"{0}\" from the template before attempting to create a container.  Example element uses are: <SharedTeamDef>{0}</SharedTeamDef>, <parentDomain>/Default/{0}</parentDomain>.  Review the Windchill Business Administrator's Guide for how to handle specific cases.")
   public static final String CANT_FIND_THE_REFERENCED_SHAREDTEAM = "118";

   @RBEntry("No shared team is associated with this container. The shared team definition can not be imported from the file.")
   public static final String NO_SHAREDTEAM_FOR_CONTAINER = "119";

   @RBEntry("All objects were imported successfully.")
   public static final String ALL_IMPORT_SUCCEEDED = "120";

   @RBEntry("No objects were imported due to the error.")
   public static final String IMPORT_NONE_DUE_TO_ERROR = "121";

   @RBEntry("The objects that have errors were not imported.")
   public static final String IMPORT_SOME_DUE_TO_ERROR = "122";

   @RBEntry("ProjectLink does not support export CAD Documents with no associated Parts")
   public static final String CANT_EXPORT_CAD_DOC_STANDALONE = "123";

   @RBEntry("Preference Definition: {0} not found")
   public static final String PREFERENCE_DEFINITION_NOT_FOUND = "124";

   @RBEntry("There is a mismatch between the transaction start and end tags. No block for the wt.load.oneTransactionBlock.tag.start and wt.load.oneTransactionBlock.tag.end takes effect.")
   public static final String TRANSACTION_TAG_START_END_MISMATCH = "125";

   @RBEntry("The transaction start or end tag is undefined. No block for the wt.load.oneTransactionBlock.tag.start and wt.load.oneTransactionBlock.tag.end takes effect.")
   public static final String TRANSACTION_TAG_START_END_UNDEFINED = "126";

   @RBEntry("Error in load file. No end tag found for start tag: {0}")
   public static final String NO_TRANSACTION_END_TAG = "127";

   @RBEntry("This is an invalid file be popped up for this action.")
   public static final String INVALID_FILE = "128";

   @RBEntry("A profile with name {0} already exists.  Import failed.")
   public static final String FOUND_IDENTICAL_PROFILE = "129";

   @RBEntry("An object initialization rule cannot be created for type: {0}. Container types are not allowed.")
   public static final String CONTAINER_NOT_ALLOWED_FOR_OIR_OBJTYPE = "130";
}
