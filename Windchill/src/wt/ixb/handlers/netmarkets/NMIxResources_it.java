/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.ixb.handlers.netmarkets;

import wt.util.resource.*;

@RBUUID("wt.ixb.handlers.netmarkets.NMIxResources")
@RBNameException //Grandfathered by conversion
public final class NMIxResources_it extends WTListResourceBundle {
   @RBEntry("L'esportazione di {0} non è supportata per questa release.")
   public static final String EXPORT_NOT_SUPPORTED = "0";

   @RBEntry("Elemento XML non valido. Atteso {0}. Trovato {1}.")
   public static final String ILLEGAL_TAG = "1";

   @RBEntry("Impossibile creare una struttura di cartelle dentro uno schedario non esistente: Schedario mancante: {0}")
   public static final String NO_CABINET_FOR_FOLDER = "2";

   @RBEntry("Impossibile trovare la cartella radice {0}")
   public static final String CANNOT_FIND_ROOT_FOLDER = "3";

   @RBEntry("La cartella {0} esiste già. Continuazione in corso...")
   public static final String FOLDER_EXISTS = "4";

   @RBEntry("Creazione della sottodirectory {0} non riuscita")
   public static final String FAILED_SUBFOLDER = "5";

   @RBEntry("L'utente/gruppo/ruolo {0} è inesistente")
   public static final String PRINCIPAL_DOES_NOT_EXIST = "6";

   @RBEntry("La classe {0} non è valida per le regole di controllo di accesso.")
   public static final String INVALID_ACL_CLASS         = "7";

   @RBEntry("Sponsor progetto proposto: {0} non trovato")
   public static final String NO_PROJECT_SPONSOR       = "8";

   @RBEntry("Contesto progetto nullo")
   public static final String NULL_PRJ_CONTAINER = "9";

   @RBEntry("Elemento XML non valido. Atteso {0} o {1}. Trovato {2}.")
   public static final String ILLEGAL_TAG_CHOICE   = "10";

   @RBEntry("Default")
   public static final String DEFAULT_NAME  = "11";

   @RBEntry("L'oggetto origine deve essere <WTPart> oppure <WTDocument>")
   public static final String ILLEGAL_RULE_SEED = "12";

   @RBEntry("Dominio di sistema inesistente per ProjectContainer {0}.")
   public static final String NO_SYSTEM_DOMAIN_FOR_PC = "13";

   @RBEntry("XML non valido")
   public static final String ILLEGAL_XML = "14";

   @RBEntry("Tipo nullo di regola di importazione.")
   public static final String NULL_RULE_TYPE = "15";

   @RBEntry("Impossibile creare una regola basata sul tipo se il tag <className> è vuoto o manca.")
   public static final String NO_CLASS = "16";

   @RBEntry("Classe {0} non trovata nel codebase.")
   public static final String MISSING_CLASS = "17";

   @RBEntry("Nome di classe non valido in getObjectFromOid: {0}")
   public static final String BAD_CLASS_NAME = "18";

   @RBEntry("Problema durante il parsing di xml, riga: {0} colonna: {1} messaggio: {2}")
   public static final String XML_PARSE_ERROR = "19";

   @RBEntry("Caricamento di {0} in corso")
   public static final String LOAD_TYPE = "20";

   @RBEntry("{0} caricato.")
   public static final String TYPE_LOADED = "21";

   @RBEntry("Caricamento di {0} {1} in corso")
   public static final String LOAD_OBJECT = "22";

   @RBEntry("L'oggetto {0}  {1} è stato caricato.")
   public static final String OBJECT_LOADED = "23";

   @RBEntry("Workflow e cicli di vita")
   public static final String WORKFLOWS_LIFECYCLES = "24";

   @RBEntry("Regola {0} in esecuzione")
   public static final String EXECUTE_RULE = "25";

   @RBEntry("Regola {0} eseguita")
   public static final String RULE_EXECUTED = "26";

   @RBEntry("L'importazione del progetto è stata completata.")
   public static final String PROJECT_IMPORT_SUCCEEDED = "27";

   @RBEntry("Importazione organizzazione avvenuta.")
   public static final String ORG_IMPORT_SUCCEEDED = "28";

   @RBEntry("File mappa {0} inesistente")
   public static final String NO_CSV_MAPFILE = "29";

   @RBEntry("La directory {0} è inesistente")
   public static final String NO_DIRECTORY = "30";

   @RBEntry("Elaborazione del file {0} in corso")
   public static final String PROCESSING_FILE = "31";

   @RBEntry("Non esistono file da elaborare.")
   public static final String NO_FILES_TO_PROCESS = "32";

   @RBEntry("Impossibile trovare NotebookTemplate di default")
   public static final String DEF_NOTEBOOK_TEMPLATE_NOT_FOUND = "33";

   @RBEntry("Impossibile trovare il DiscussionForumTemplate {0} e nessun modello di default è disponibile.")
   public static final String NO_FORUM_TEMPLATE = "34";

   @RBEntry("Regola \"{0}\" non trovata")
   public static final String RULE_NOT_FOUND = "35";

   @RBEntry("Creazione dell'oggetto dalla regola {0} non riuscita")
   public static final String CREATE_FROM_RULE_FAILED = "36";

   @RBEntry("Gestore mancante per il  tag: {0}")
   public static final String NO_HANDLER_CSVTAG = "37";

   @RBEntry("Nome di classe gestore nullo o vuoto per elemento NmLoader.")
   public static final String NULL_CSV_CLASS_STRING = "38";

   @RBEntry("Nome di metodo gestore nullo o vuoto per elemento NmLoader.")
   public static final String NULL_CSV_METHOD_STRING = "39";

   @RBEntry("<groupName></groupName> non può essere vuoto")
   public static final String GROUP_CANT_BE_EMPTY = "40";

   @RBEntry("File di contenuto non specificato.")
   public static final String CONTENT_NOT_SPECIFIED = "41";

   @RBEntry("Conflitto durante il caricamento. Non è consentita la creazione di istanze statiche di NMHandler")
   public static final String NMHANDLER_CANT_BE_STATIC = "42";

   @RBEntry("Specificare il dominio per il team.")
   public static final String TEAM_NEEDS_DOMAIN = "43";

   @RBEntry("L'elemento <percorso file> non può essere vuoto")
   public static final String FOLDER_PATH_EMPTY = "44";

   @RBEntry("Specificare attore e ruolo quando si definisce RoleActorMap")
   public static final String MISSING_ROLE_ACTOR = "45";

   @RBEntry("Specificare ruolo e utente/gruppo/ruolo quando si definisce RolePrincipaMap")
   public static final String MISSING_ROLE_PRINCIPAL = "46";

   @RBEntry("<Nome dominio> non può essere vuoto nell'elemento <Team>")
   public static final String TEAM_HAS_NO_DOMAIN = "47";

   @RBEntry("Specificare l'organizzazione.")
   public static final String NO_ORG_SPECIFIED = "48";

   @RBEntry("Elenco file {0}: {1}")
   public static final String FILE_LIST_ITEM = "49";

   @RBEntry("Specificare i file XML o una directory contenente file XML.")
   public static final String WHERE_IS_MY_XML = "50";

   @RBEntry("Percorso base non valido: {0}")
   public static final String INVALID_BASE_PATH = "51";

   @RBEntry("L'elemento <Nome tipo> non può essere vuoto.")
   public static final String TYPE_NAME_EMPTY = "52";

   @RBEntry("Il file XML contiene solo spazi o non ha contenuto")
   public static final String XML_EMPTY = "53";

   @RBEntry("La stringa <?xml è inesistente, non è un file XML")
   public static final String XML_WITH_NO_XML_PI = "54";

   @RBEntry("I file XML minimi devono iniziare con  <?xml versione=1.0 ?>")
   public static final String XML_MINIMAL = "55";

   @RBEntry("Errore durante la chiusura del flusso di input")
   public static final String ERROR_CLOSING_STREAM = "56";

   @RBEntry("Un InputStream nullo è passato al validatore XML")
   public static final String XML_IS_NULL = "57";

   @RBEntry("InputStream è vuoto")
   public static final String EMPTY_INPUT_STREAM = "58";

   @RBEntry("Impossibile creare un documento dal file modello")
   public static final String FAILED_TEMPLATE_CREATE = "59";

   @RBEntry("Attributo aclState non valido: {0}")
   public static final String ILLEGAL_ACL_STATE_VALUE = "60";

   /**
    * more than one rule with the same name and type exists in the database
    **/
   @RBEntry("Nel database esiste più di una regola {0} di nome {1}.")
   public static final String TOO_MANY_RULE_WITH_SAME_NAME_AND_TYPE = "61";

   @RBEntry("Nessun metadato da importare, provare con un altro file.")
   public static final String IMPORT_FILE_NEEDS_XML = "62";

   /**
    * Export messages
    **/
   @RBEntry("Sono stati esportati {0} dei {1} oggetti")
   public static final String EXPORTED_N_OF_M_OBJECTS = "63";

   @RBEntry("Esportazione di ACL ad hoc in corso")
   public static final String EXPORTING_AD_HOCS = "64";

   @RBEntry("Esportazione di file visualizzabili in corso")
   public static final String EXPORTING_VIEWABLES = "65";

   @RBEntry("Sono stati esportati i file visualizzabili di {0} dei {1} oggetti.")
   public static final String EXPORTED_N_OF_M_VIEWABLES = "66";

   @RBEntry("Esportazione di ACL ad hoc completata")
   public static final String DONE_EXPORTING_AD_HOCS = "67";

   @RBEntry("Tipo di documento non valido per il modello di documento d'esportazione")
   public static final String ILLEGAL_DOC_TYPE = "68";

   @RBEntry("Impossibile trovare il nome della regola per il documento con nome= {0} e numero= {1}")
   public static final String RULE_NOT_FOUND_FOR_DOC_TEMPLATE = "69";

   @RBEntry("Impossibile trovare la configurazione di progetto per l'importazione del modello di progetto")
   public static final String PROJECT_CONFIG_NOT_AVAILABLE = "70";

   @RBEntry("{0} non è un tipo di regola supportato")
   public static final String ILLEGAL_RULE_TYPE = "71";

   @RBEntry("Un nome di regola deve contenere almento un carattere diverso dallo spazio")
   public static final String ILLEGAL_RULE_NAME = "73";

   @RBEntry("Nessuna regola {0} associata al modello di documento {1} ({2})")
   public static final String NO_TYPE_RULE_FOR_DOC_TEMPLATE = "74";

   /**
    * 
    **/
   @RBEntry("I modelli di documento devono essere esportati come DocumentTemplate e non TemplateDocument")
   public static final String CANT_EXPORT_DOCTEMPLATE_AS_TEMPLATEDOC = "75";

   @RBEntry("XML non valido: tag posizione non può essere vuoto")
   public static final String ILLEGAL_FOLDERLINK_LOCATION_ENTRY = "76";

   @RBEntry("XML non valido: il tag {0} deve contenere un url valido. Impossibile costruire un URL con {1}")
   public static final String ILLEGAL_FOLDERLINK_URL_ENTRY = "77";

   @RBEntry("XML non valido: il tag {0} del link alla cartella (FolderLink) non può essere valido")
   public static final String ILLEGAL_FOLDERLINK_TAG_ENTRY = "78";

   @RBEntry("Tipo oggetto origine {0} non valido")
   public static final String ILLEGAL_SEED_OBJECT_TYPE = "79";

   @RBEntry("XML ProjectTemplate non valido: non è stato specificato alcun contenuto di modello")
   public static final String NO_PROJECT_TEMPLATE_SPEC = "80";

   @RBEntry("L'importazione/esportazione del markup è correntemente supportata solo per i documenti e le parti")
   public static final String CAN_ONLY_EXPORT_MARKUP_FOR_DOCS_AND_PARTS = "81";

   @RBEntry("Tipo di markup non supportato {0}")
   public static final String UNSUPPORTED_MARKUP_TYPE = "82";

   @RBEntry("L'elemento visualizzabile {0} deve avere un contenuto")
   public static final String VIEWABLE_NEEDS_CONTENT = "83";

   @RBEntry("L'utente deve avere privilegi amministrativi per importare oggetti.")
   public static final String IMPORT_NOT_ALLOWED = "84";

   @RBEntry("Attenzione, l'utente {0} non esiste nella LDAP. L'assegnatario viene impostato a {1}. L'utente deve riassegnare o eliminare la risorsa.")
   public static final String NO_LDAP_ENTRY = "85";

   @RBEntry(" Le voci seguenti del tipo enumerato avevano dei caratteri non validi {0}. Consultare la guida di personalizzazione e riparare il file XML.")
   public static final String ILLEGAL_CHARACTRERS_IN_ROLE_NAME = "86";

   @RBEntry("Elemento XML non valido. Atteso: {0}. Trovato: {1}.")
   public static final String ILLEGAL_TAG_CHOICES = "87";

   @RBEntry("Il valore di <containerClassName> nell'elemento XML di <ContainerTemplate> non può essere nullo.")
   public static final String CONTAINER_CLASS_NAME_NEEDED = "88";

   @RBEntry("Il nome di classe trasmesso {0} non è presente nel sistema")
   public static final String CONTAINER_CLASS_NOT_THERE = "89";

   @RBEntry("{0} non è un WTContainer")
   public static final String CONTAINER_CLASS_NAME_NOT_A_WTCONTAINER = "90";

   @RBEntry("IMpossibile trovare team per {0}. Contattare l'amministratore.")
   public static final String CTM_OBJECT_HAS_NO_TEAM = "91";

   @RBEntry("L'esportazione è stata terminata perché la richiesta non avrebbe portato all'esportazione di alcun oggetto")
   public static final String NOTHING_TO_EXPORT = "92";

   @RBEntry("{0} non è un gruppo d'accesso: impossibile creare la regola.")
   public static final String PRINCIPAL_NOT_AN_ACCESS_GROUP = "93";

   @RBEntry("Impossibile trovare il dominio {0}. Contattare l'amministratore.")
   public static final String COULD_NOT_FIND_DOMAIN_FROM_PATH = "94";

   @RBEntry("Impossibile trovare il dominio {0} con il dominio padre {1}. Contattare l'amministratore.")
   public static final String COUND_NOT_FIND_DOMAIN_UNDER_PARENT = "95";

   @RBEntry("Il modello di contesto deve avere un nome")
   public static final String CONTAINER_TEMPLATE_NEEDS_A_NAME = "96";

   @RBEntry("XML non valido: il tag {0} non può essere vuoto o contenere solo spazi.")
   public static final String XML_VALUE_CANNOT_BE_EMPTY = "97";

   @RBEntry("È necessario che nell'elenco di esportazione sia presente almeno una classe persistente")
   public static final String EXPORT_LIST_NEEDS_A_PERSISTABLE = "98";

   @RBEntry("Il riferimento del contenitore di origine non può essere nullo")
   public static final String SRC_CONT_REF_CANT_BE_NULL = "99";

   @RBEntry("Il contenitore di origine deve essere persistente")
   public static final String SRC_CONT_MUST_BE_PERSISTENT = "100";

   @RBEntry("Il file temporaneo non può essere nullo per doExport")
   public static final String TMPFILE_CANT_BE_NULL = "101";

   @RBEntry("Il generatore di esportazione richiede una classe persistente. Contattare l'amministratore")
   public static final String EXP_GEN_NEEDS_PERSISTABLE = "102";

   @RBEntry("Impossibile aggiungere un generatore di esportazione nullo. Contattare l'amministratore")
   public static final String CANT_ADD_NULL_GENERATOR = "103";

   @RBEntry("Tipo di contesto non valido {0} Per un contesto ContainerTeamManaged è possibile esportare solo regole di accesso")
   public static final String CAN_ONLY_EXPORT_POLICIES_FOR_CTM = "104";

   @RBEntry("Esiste più di un forum sul progetto. Contattare l'amministratore.")
   public static final String TOO_MANY_PROJECT_FORUM_TEMPLATES = "105";

   @RBEntry("Errore di input, è stato specificato skipXML ma è stato trasferito in un file XML come argomento della riga di comando")
   public static final String SKIP_XML_BUT_ONLY_XML = "106";

   @RBEntry("Errore di input, è stato specificato skipJars ma è stato trasferito esplicitamente un file jar sulla riga di comando")
   public static final String SKIP_JARS_BUT_ONLY_JAR = "107";

   @RBEntry("Metodo caricatore {0}.{1} ha riscontrato un errore")
   public static final String LOADER_METHOD_FAILED_2ARGS = "108";

   @RBEntry("Un modello di contesto per {0} denominato {1} per la lingua {2} è già presente nel contenitore {3}. Selezionare Crea modello per creare un modello")
   public static final String USE_CREATE_TEMPLATE_UI_INSTEAD_OF_IMPORT = "109";

   @RBEntry("Il modello di contesto è già stato reso persistente")
   public static final String CONTEXT_TEMPLATE_ALREADY_PERSISTED = "110";

   @RBEntry("Impossibile trovare il contenitore dell'organizzazione per il contesto: {0}")
   public static final String COULD_NOT_FIND_ORGCONT_FOR_CONTEXT = "111";

   @RBEntry("Tipo di cartella imprevisto")
   public static final String UNEXPECTED_FOLDER_TYPE = "112";

   @RBEntry("Impossibile convertire un file XML in file XML con questa utilità")
   public static final String CANT_CONVERT_XML_TO_XML = "113";

   @RBEntry("Attenzione: impossibile creare la cartella {0}.\n\nNon si dispone dei diritti necessari a creare una cartella nel dominio {1}. Riprovare l'operazione in un dominio differente o contattare l'amministratore per ottenere i diritti necessari.")
   @RBComment("domain is specified for a nested folder and the user does not have read access to the domain.")
   @RBArgComment0("Name of folder being created.")
   @RBArgComment1("Name of domain being assigned to folder being created.")
   public static final String FAILED_NESTED_FOLDER_DOMAIN_ACCESS = "114";

   @RBEntry("Attenzione: impossibile creare la cartella {0}.\n\nIl dominio {1} immesso non è un sottodominio di {2}. Riprovare specificando un dominio differente.")
   @RBComment("The domain is not the same domain or a subdomain as the domain of the parent folder.")
   @RBArgComment0("Name of folder being created.")
   @RBArgComment1("Name of domain being assigned to folder being created.")
   @RBArgComment2("Name of domain assigned to parent folder of the folder being created.")
   public static final String FAILED_NESTED_FOLDER_NOT_SUBDOMAIN = "115";

   @RBEntry("Impossibile risolvere il nome del dominio padre {0} in quanto non è univoco nel contenitore {1}.")
   public static final String NON_UNIQUE_DOMAIN_NAME = "116";

   @RBEntry("Il team condiviso {0} esiste già. Importazione non riuscita.")
   public static final String FOUND_IDENTICAL_SHAREDTEAM = "117";

   @RBEntry("Rimuovere dal modello tutti i riferimenti al team condiviso \"{0}\" prima di tentare la creazione di un contenitore. Esempi di componenti: <SharedTeamDef>{0}</SharedTeamDef>, <parentDomain>/Default/{0}</parentDomain>. Per informazioni sulla gestione di casi specifici, consultare il manuale Windchill Business Administrator's Guide (Guida dell'amministratore aziendale Windchill).")
   public static final String CANT_FIND_THE_REFERENCED_SHAREDTEAM = "118";

   @RBEntry("Nessun team condiviso associato a questo contenitore. Impossibile importare dal file la definizione di team condiviso.")
   public static final String NO_SHAREDTEAM_FOR_CONTAINER = "119";

   @RBEntry("Importazione oggetti riuscita.")
   public static final String ALL_IMPORT_SUCCEEDED = "120";

   @RBEntry("Nessun oggetto importato a causa dell'errore.")
   public static final String IMPORT_NONE_DUE_TO_ERROR = "121";

   @RBEntry("Gli oggetti con errori non sono stati importati.")
   public static final String IMPORT_SOME_DUE_TO_ERROR = "122";

   @RBEntry("ProjectLink non supporta documenti CAD privi di parti associate")
   public static final String CANT_EXPORT_CAD_DOC_STANDALONE = "123";

   @RBEntry("Definizione preferenza: impossibile trovare {0}")
   public static final String PREFERENCE_DEFINITION_NOT_FOUND = "124";

   @RBEntry("Mancata corrispondenza fra i tag di inizio e fine transazione. Nessun blocco avrà effetto per wt.load.oneTransactionBlock.tag.start e wt.load.oneTransactionBlock.tag.end.")
   public static final String TRANSACTION_TAG_START_END_MISMATCH = "125";

   @RBEntry("Il tag di inizio o fine transazione non è definito. Nessun blocco avrà effetto per wt.load.oneTransactionBlock.tag.start e wt.load.oneTransactionBlock.tag.end.")
   public static final String TRANSACTION_TAG_START_END_UNDEFINED = "126";

   @RBEntry("Errore nel file di caricamento. Impossibile trovare un tag di fine corrispondente al tag di inizio: {0}")
   public static final String NO_TRANSACTION_END_TAG = "127";

   @RBEntry("File non valido per questa azione.")
   public static final String INVALID_FILE = "128";

   @RBEntry("Il profilo {0} esiste già. Importazione non riuscita.")
   public static final String FOUND_IDENTICAL_PROFILE = "129";

   @RBEntry("Impossibile creare una regola di inizializzazione dell'oggetto per il tipo: {0}. I tipi di contenitore non sono consentiti.")
   public static final String CONTAINER_NOT_ALLOWED_FOR_OIR_OBJTYPE = "130";
}
