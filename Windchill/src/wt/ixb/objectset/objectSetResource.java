/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.ixb.objectset;

import wt.util.resource.*;

@RBUUID("wt.ixb.objectset.objectSetResource")
public final class objectSetResource extends WTListResourceBundle {
   @RBEntry("{0}")
   @RBPseudo(false)
   public static final String EMPTY_STRING_WITH_PARAMETER = "EMPTY_STRING_WITH_PARAMETER";

   @RBEntry("Name:")
   public static final String NAME = "NAME";

   @RBEntry("Number:")
   public static final String NUMBER = "NUMBER";

   @RBEntry("lifecycle state:")
   public static final String LIFE_CYCLE_STATE = "LIFE_CYCLE_STATE";

   @RBEntry("Product Structure (built with active configuration specification)")
   public static final String PRODUCT_STRUCTURE_NAME = "PRODUCT_STRUCTURE_NAME";

   @RBEntry("Product Structure for top level part {0}")
   public static final String PRODUCT_STRUCTURE_DESCRIPTION = "PRODUCT_STRUCTURE_DESCRIPTION";

   @RBEntry("Search for product structure top level object")
   public static final String PRODUCT_STRUCTURE_SEARCH_DESCRIPTION = "PRODUCT_STRUCTURE_SEARCH_DESCRIPTION";

   @RBEntry("Product Structure with CAD Documents / Dynamic Documents (built with active configuration specification)")
   public static final String PRODUCT_STRUCTURE_NAME_WITH_EPM = "PRODUCT_STRUCTURE_NAME_WITH_EPM";

   @RBEntry(" Product Structure with CAD Documents / Dynamic Documents for top level part {0}")
   public static final String PRODUCT_STRUCTURE_DESCRIPTION_W_EPM = "PRODUCT_STRUCTURE_DESCRIPTION_W_EPM";

   @RBEntry("Search for product structure top level object")
   public static final String PRODUCT_STRUCTURE_WITH_EPM_SEARCH_DESCRIPTION = "PRODUCT_STRUCTURE_WITH_EPM_SEARCH_DESCRIPTION";

   @RBEntry("WTPart:name={0},number={1}")
   public static final String PART_IMAGE = "PART_IMAGE";

   @RBEntry("CAD Document / Dynamic Document Structure (built with latest configuration specification)")
   public static final String EPM_PRODUCT_STRUCTURE_NAME = "EPM_PRODUCT_STRUCTURE_NAME";

   @RBEntry("CAD Documents / Dynamic Documents Structure for top level document {0}")
   public static final String EPM_PRODUCT_STRUCTURE_DESCRIPTION = "EPM_PRODUCT_STRUCTURE_DESCRIPTION";

   @RBEntry("CAD Document / Dynamic Document :name={0},number={1}")
   public static final String EPM_DOCUMENT_IMAGE = "EPM_DOCUMENT_IMAGE";

   @RBEntry("Document")
   public static final String SINGLE_DOCUMENT_NAME = "SINGLE_DOCUMENT_NAME";
   
   @RBEntry("Change Notice")
   public static final String CHANGE_NOTICE_TEMPLATE_NAME = "CHANGE_NOTICE_TEMPLATE_NAME";   

   /**
    * SINGLE_DOCUMENT_DESCRIPTION.value=Document {0}
    **/
   @RBEntry(" {0}")
   public static final String SINGLE_DOCUMENT_DESCRIPTION = "SINGLE_DOCUMENT_DESCRIPTION";

   @RBEntry("Search for Document")
   public static final String SINGLE_DOCUMENT_SEARCH_DESCRIPTION = "SINGLE_DOCUMENT_SEARCH_DESCRIPTION";

   @RBEntry("WTDocument:name={0},number={1}")
   public static final String DOCUMENT_IMAGE = "DOCUMENT_IMAGE";

   @RBEntry("Filter based on config spec")
   public static final String FILTER_BY_CONFIG_SPEC_NAME = "FILTER_BY_CONFIG_SPEC_NAME";

   @RBEntry("Filter based on modification time")
   public static final String FILTER_BY_TIME_NAME = "FILTER_BY_TIME_NAME";

   @RBEntry("Filter based on object's number")
   public static final String FILTER_BY_NUMBER_NAME = "FILTER_BY_NUMBER_NAME";

   @RBEntry("Exclude object with object's number {0}")
   public static final String FILTER_BY_NUMBER_DESCR = "FILTER_BY_NUMBER_DESCR";

   @RBEntry("Include objects updated between {0} and {1}")
   public static final String FILTER_BY_TIME_BETWEEN_DESCR = "FILTER_BY_TIME_BETWEEN_DESCR";

   @RBEntry("Include object updated after {0}")
   public static final String FILTER_BY_TIME_AFTER_DESCR = "FILTER_BY_TIME_AFTER_DESCR";

   @RBEntry("Include objects updated before {0}")
   public static final String FILTER_BY_TIME_BEFORE_DESCR = "FILTER_BY_TIME_BEFORE_DESCR";

   @RBEntry("Cabinet and Folder Contents")
   public static final String FOLDER_CONTENT_NAME = "FOLDER_CONTENT_NAME";

   @RBEntry("Search for Folder")
   public static final String FOLDER_CONTENT_SEARCH_DESCRIPTION = "FOLDER_CONTENT_SEARCH_DESCRIPTION";

   @RBEntry("All objects in Folder {0}")
   public static final String FOLDER_CONTENT_DESCRIPTION = "FOLDER_CONTENT_DESCRIPTION";

   @RBEntry("Product structure can not be exported because it is incomplete.\nIt may be caused by:\n - currently set config spec does not select any version for \"{0}\" \n - the user does not have necessary access rights for \"{0}\" \n - \"{0}\" is in the personal cabinet(or not checked in yet)  ")
   @RBComment("the '\n' provides a new line in the message")
   public static final String INCOMPLETE_PRODUCT_STRUCTURE = "INCOMPLETE_PRODUCT_STRUCTURE";

   @RBEntry("{0} WTPartDescribeLink object(s) were found corresponding to WTPart: <{1}> and WTDocument: <{2}>")
   @RBArgComment0("number of objects")
   @RBArgComment1("Object ID")
   @RBArgComment2("Object ID")
   public static final String WTPARTDESCRIBELINK_NOT_UNIQUE = "WTPARTDESCRIBELINK_NOT_UNIQUE";

   @RBEntry("{0} WTPartReferenceLink object(s) were found corresponding to WTPart: <{1}> and WTDocumentMaster: <{2}>")
   @RBArgComment0("number of objects")
   @RBArgComment1("Object ID")
   @RBArgComment2("Object ID")
   public static final String WTPARTREFERENCELINK_NOT_UNIQUE = "WTPARTREFERENCELINK_NOT_UNIQUE";

   @RBEntry("Single Part")
   public static final String SINGLE_PART_NAME = "SINGLE_PART_NAME";

   @RBEntry("Search for Part")
   public static final String SINGLE_PART_SEARCH_DESCRIPTION = "SINGLE_PART_SEARCH_DESCRIPTION";

   @RBEntry("Single Part : {0}")
   public static final String SINGLE_PART_DESCRIPTION = "SINGLE_PART_DESCRIPTION";

   @RBEntry("IBA Definition Node Structure")
   public static final String IBA_DEFINITION_NODE_STRUCTURE = "IBA_DEFINITION_NODE_STRUCTURE";

   @RBEntry("IBA Definition or Attribute Organizer structure")
   public static final String IBA_DEFINITION_SEARCH_DESCRIPTION = "IBA_DEFINITION_SEARCH_DESCRIPTION";

   @RBEntry("IBA : {0}")
   public static final String IBA_DEFINITION_IMAGE = "IBA_DEFINITION_IMAGE";

   @RBEntry("Soft Type Definition")
   public static final String WTTYPE_DEFINITION_NAME = "WTTYPE_DEFINITION_NAME";

   @RBEntry("Search Soft Type Definition")
   public static final String WTTYPE_DEFINITION_SEARCH_DESCRIPTION = "WTTYPE_DEFINITION_SEARCH_DESCRIPTION";

   @RBEntry("Soft Type Definition : {0}")
   public static final String WTTYPE_DEFINITION_IMAGE = "WTTYPE_DEFINITION_IMAGE";

   @RBEntry("Soft Type Definition")
   public static final String WTTYPE_DEFINITION_DESCRIPTION = "WTTYPE_DEFINITION_DESCRIPTION";

   @RBEntry("Project Plan Document")
   public static final String PROJECT_PLAN_DOCUMENT_NAME = "PROJECT_PLAN_DOCUMENT_NAME";

   @RBEntry("Project Plan Document {0}")
   public static final String PROJECT_PLAN_DOCUMENT_DESCRIPTION = "PROJECT_PLAN_DOCUMENT_DESCRIPTION";

   @RBEntry("Search for Project Plan Document")
   public static final String PROJECT_PLAN_DOCUMENT_SEARCH_DESCRIPTION = "PROJECT_PLAN_DOCUMENT_SEARCH_DESCRIPTION";

   @RBEntry("Managed Project Document")
   public static final String MANAGED_PROJECT_DOCUMENT_NAME = "MANAGED_PROJECT_DOCUMENT_NAME";

   @RBEntry("Managed Project Document {0}")
   public static final String MANAGED_PROJECT_DOCUMENT_DESCRIPTION = "MANAGED_PROJECT_DOCUMENT_DESCRIPTION";

   @RBEntry("Search for Managed Project Document")
   public static final String MANAGED_PROJECT_DOCUMENT_SEARCH_DESCRIPTION = "MANAGED_PROJECT_DOCUMENT_SEARCH_DESCRIPTION";

   @RBEntry("The document \"{0}\" describing \"{1}\" is a working copy or in personal cabinet and can not be exported.")
   @RBArgComment0("The localized object ID of the document")
   @RBArgComment1("The localized object ID of the object (such as WTPart) described by this document")
   public static final String DESC_DOC_IN_PERS_CAB_OR_WORKING_COPY = "descdoc_in_personal_cab_or_working_copy";

   @RBEntry("Project Team Document")
   public static final String PROJECT_TEAM_DOCUMENT_NAME = "PROJECT_TEAM_DOCUMENT_NAME";

   @RBEntry("Project Team Document {0}")
   public static final String PROJECT_TEAM_DOCUMENT_DESCRIPTION = "PROJECT_TEAM_DOCUMENT_DESCRIPTION";

   @RBEntry("Search for Project Team Document")
   public static final String PROJECT_TEAM_DOCUMENT_SEARCH_DESCRIPTION = "PROJECT_TEAM_DOCUMENT_SEARCH_DESCRIPTION";

   @RBEntry("Filter Template")
   public static final String NFT_NAME = "NFT_NAME";

   @RBEntry("Search for Filter Template")
   public static final String NFT_SEARCH_DESCRIPTION = "NFT_SEARCH_DESCRIPTION";

   @RBEntry("Filter Template - {0}")
   public static final String NFT_DESCRIPTION_FOR_EXPORT = "NFT_DESCRIPTION_FOR_EXPORT";
}
