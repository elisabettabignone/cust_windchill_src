/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.ixb.objectset;

import wt.util.resource.*;

@RBUUID("wt.ixb.objectset.objectSetResource")
public final class objectSetResource_it extends WTListResourceBundle {
   @RBEntry("{0}")
   @RBPseudo(false)
   public static final String EMPTY_STRING_WITH_PARAMETER = "EMPTY_STRING_WITH_PARAMETER";

   @RBEntry("Nome:")
   public static final String NAME = "NAME";

   @RBEntry("Numero:")
   public static final String NUMBER = "NUMBER";

   @RBEntry("stato ciclo di vita:")
   public static final String LIFE_CYCLE_STATE = "LIFE_CYCLE_STATE";

   @RBEntry("Struttura prodotto (creata usando la specifica di configurazione attiva)")
   public static final String PRODUCT_STRUCTURE_NAME = "PRODUCT_STRUCTURE_NAME";

   @RBEntry("Struttura di prodotto per parte di livello superiore {0}")
   public static final String PRODUCT_STRUCTURE_DESCRIPTION = "PRODUCT_STRUCTURE_DESCRIPTION";

   @RBEntry("Cerca l'oggetto di livello superiore della struttura di prodotto ")
   public static final String PRODUCT_STRUCTURE_SEARCH_DESCRIPTION = "PRODUCT_STRUCTURE_SEARCH_DESCRIPTION";

   @RBEntry("Struttura prodotto con documenti CAD/documenti dinamici (creata con specifica di configurazione attiva)")
   public static final String PRODUCT_STRUCTURE_NAME_WITH_EPM = "PRODUCT_STRUCTURE_NAME_WITH_EPM";

   @RBEntry(" Struttura di prodotto con documenti CAD/documenti dinamici per parte di livello superiore {0}")
   public static final String PRODUCT_STRUCTURE_DESCRIPTION_W_EPM = "PRODUCT_STRUCTURE_DESCRIPTION_W_EPM";

   @RBEntry("Cerca l'oggetto di livello superiore della struttura di prodotto ")
   public static final String PRODUCT_STRUCTURE_WITH_EPM_SEARCH_DESCRIPTION = "PRODUCT_STRUCTURE_WITH_EPM_SEARCH_DESCRIPTION";

   @RBEntry("WTPart:nome={0},numero={1}")
   public static final String PART_IMAGE = "PART_IMAGE";

   @RBEntry("Struttura documento CAD/documento dinamico (creata con la specifica di configurazione più recente)")
   public static final String EPM_PRODUCT_STRUCTURE_NAME = "EPM_PRODUCT_STRUCTURE_NAME";

   @RBEntry("Struttura documento CAD/documento dinamico per documento di livello superiore {0}")
   public static final String EPM_PRODUCT_STRUCTURE_DESCRIPTION = "EPM_PRODUCT_STRUCTURE_DESCRIPTION";

   @RBEntry("Documento CAD/documento dimanico :nome = {0}, numero = {1}")
   public static final String EPM_DOCUMENT_IMAGE = "EPM_DOCUMENT_IMAGE";

   @RBEntry("Documento")
   public static final String SINGLE_DOCUMENT_NAME = "SINGLE_DOCUMENT_NAME";
   
   @RBEntry("Notifica di modifica")
   public static final String CHANGE_NOTICE_TEMPLATE_NAME = "CHANGE_NOTICE_TEMPLATE_NAME";   

   /**
    * SINGLE_DOCUMENT_DESCRIPTION.value=Document {0}
    **/
   @RBEntry(" {0}")
   public static final String SINGLE_DOCUMENT_DESCRIPTION = "SINGLE_DOCUMENT_DESCRIPTION";

   @RBEntry("Cerca documento")
   public static final String SINGLE_DOCUMENT_SEARCH_DESCRIPTION = "SINGLE_DOCUMENT_SEARCH_DESCRIPTION";

   @RBEntry("WTDocument:nome={0},numero={1}")
   public static final String DOCUMENT_IMAGE = "DOCUMENT_IMAGE";

   @RBEntry("Filtra in base alla specifica di configurazione")
   public static final String FILTER_BY_CONFIG_SPEC_NAME = "FILTER_BY_CONFIG_SPEC_NAME";

   @RBEntry("Filtra in base all'ora di modifica")
   public static final String FILTER_BY_TIME_NAME = "FILTER_BY_TIME_NAME";

   @RBEntry("Filtra in base al numero dell'oggetto")
   public static final String FILTER_BY_NUMBER_NAME = "FILTER_BY_NUMBER_NAME";

   @RBEntry("Escludi l'oggetto con numero d'oggetto {0}")
   public static final String FILTER_BY_NUMBER_DESCR = "FILTER_BY_NUMBER_DESCR";

   @RBEntry("Includi gli oggetti aggiornati tra le {0} e le {1}")
   public static final String FILTER_BY_TIME_BETWEEN_DESCR = "FILTER_BY_TIME_BETWEEN_DESCR";

   @RBEntry("Includi gli oggetti aggiornati dopo le {0}")
   public static final String FILTER_BY_TIME_AFTER_DESCR = "FILTER_BY_TIME_AFTER_DESCR";

   @RBEntry("Includi gli oggetti aggiornati prima delle {0}")
   public static final String FILTER_BY_TIME_BEFORE_DESCR = "FILTER_BY_TIME_BEFORE_DESCR";

   @RBEntry("Contenuto schedari e cartelle")
   public static final String FOLDER_CONTENT_NAME = "FOLDER_CONTENT_NAME";

   @RBEntry("Cerca cartella")
   public static final String FOLDER_CONTENT_SEARCH_DESCRIPTION = "FOLDER_CONTENT_SEARCH_DESCRIPTION";

   @RBEntry("Tutti gli oggetti nella cartella {0}")
   public static final String FOLDER_CONTENT_DESCRIPTION = "FOLDER_CONTENT_DESCRIPTION";

   @RBEntry("Impossibile esportare la struttura di prodotto perché incompleta.\nÈ possibile che: \n- la specifica di configurazione attuale non seleziona alcuna versione di \"{0}\"\n- l'utente non dispone dei permessi appropriati per \"{0}\"\n- \"{0}\" si trova in uno schedario personale (o non è ancora stato sottoposto a Check-In). ")
   @RBComment("the '\n' provides a new line in the message")
   public static final String INCOMPLETE_PRODUCT_STRUCTURE = "INCOMPLETE_PRODUCT_STRUCTURE";

   @RBEntry("Sono stati trovati {0} oggetti WTPartDescribeLink corrispondenti a WTPart: <{1}> e WTDocument: <{2}>")
   @RBArgComment0("number of objects")
   @RBArgComment1("Object ID")
   @RBArgComment2("Object ID")
   public static final String WTPARTDESCRIBELINK_NOT_UNIQUE = "WTPARTDESCRIBELINK_NOT_UNIQUE";

   @RBEntry("Sono stati trovati {0} oggetti WTPartReferenceLink corrispondenti a WTPart: <{1}> e WTDocumentMaster: <{2}>")
   @RBArgComment0("number of objects")
   @RBArgComment1("Object ID")
   @RBArgComment2("Object ID")
   public static final String WTPARTREFERENCELINK_NOT_UNIQUE = "WTPARTREFERENCELINK_NOT_UNIQUE";

   @RBEntry("Parte singola")
   public static final String SINGLE_PART_NAME = "SINGLE_PART_NAME";

   @RBEntry("Cerca parte")
   public static final String SINGLE_PART_SEARCH_DESCRIPTION = "SINGLE_PART_SEARCH_DESCRIPTION";

   @RBEntry("Parte singola : {0}")
   public static final String SINGLE_PART_DESCRIPTION = "SINGLE_PART_DESCRIPTION";

   @RBEntry("Struttura nodi definizione attributi d'istanza")
   public static final String IBA_DEFINITION_NODE_STRUCTURE = "IBA_DEFINITION_NODE_STRUCTURE";

   @RBEntry("Struttura organizer attributi o definizioni attributi d'istanza")
   public static final String IBA_DEFINITION_SEARCH_DESCRIPTION = "IBA_DEFINITION_SEARCH_DESCRIPTION";

   @RBEntry("Attributo d'istanza: {0}")
   public static final String IBA_DEFINITION_IMAGE = "IBA_DEFINITION_IMAGE";

   @RBEntry("Definizione tipo soft")
   public static final String WTTYPE_DEFINITION_NAME = "WTTYPE_DEFINITION_NAME";

   @RBEntry("Cerca definizione tipo soft")
   public static final String WTTYPE_DEFINITION_SEARCH_DESCRIPTION = "WTTYPE_DEFINITION_SEARCH_DESCRIPTION";

   @RBEntry("Definizione tipo soft : {0}")
   public static final String WTTYPE_DEFINITION_IMAGE = "WTTYPE_DEFINITION_IMAGE";

   @RBEntry("Definizione tipo soft")
   public static final String WTTYPE_DEFINITION_DESCRIPTION = "WTTYPE_DEFINITION_DESCRIPTION";

   @RBEntry("Documento del piano di progetto")
   public static final String PROJECT_PLAN_DOCUMENT_NAME = "PROJECT_PLAN_DOCUMENT_NAME";

   @RBEntry("Documento del piano di progetto {0}")
   public static final String PROJECT_PLAN_DOCUMENT_DESCRIPTION = "PROJECT_PLAN_DOCUMENT_DESCRIPTION";

   @RBEntry("Cerca documento del piano di progetto")
   public static final String PROJECT_PLAN_DOCUMENT_SEARCH_DESCRIPTION = "PROJECT_PLAN_DOCUMENT_SEARCH_DESCRIPTION";

   @RBEntry("Documento progetto gestito")
   public static final String MANAGED_PROJECT_DOCUMENT_NAME = "MANAGED_PROJECT_DOCUMENT_NAME";

   @RBEntry("Documento progetto gestito {0}")
   public static final String MANAGED_PROJECT_DOCUMENT_DESCRIPTION = "MANAGED_PROJECT_DOCUMENT_DESCRIPTION";

   @RBEntry("Cerca documento progetto gestito")
   public static final String MANAGED_PROJECT_DOCUMENT_SEARCH_DESCRIPTION = "MANAGED_PROJECT_DOCUMENT_SEARCH_DESCRIPTION";

   @RBEntry("Il documento \"{0}\" che descrive \"{1}\" è una copia in modifica o è contenuto in uno schedario personale. Impossibile esportare il documento.")
   @RBArgComment0("The localized object ID of the document")
   @RBArgComment1("The localized object ID of the object (such as WTPart) described by this document")
   public static final String DESC_DOC_IN_PERS_CAB_OR_WORKING_COPY = "descdoc_in_personal_cab_or_working_copy";

   @RBEntry("Documento del team di progetto")
   public static final String PROJECT_TEAM_DOCUMENT_NAME = "PROJECT_TEAM_DOCUMENT_NAME";

   @RBEntry("Documento del team di progetto {0}")
   public static final String PROJECT_TEAM_DOCUMENT_DESCRIPTION = "PROJECT_TEAM_DOCUMENT_DESCRIPTION";

   @RBEntry("Cerca documento del team di progetto")
   public static final String PROJECT_TEAM_DOCUMENT_SEARCH_DESCRIPTION = "PROJECT_TEAM_DOCUMENT_SEARCH_DESCRIPTION";

   @RBEntry("Modello filtro")
   public static final String NFT_NAME = "NFT_NAME";

   @RBEntry("Cerca modello filtro")
   public static final String NFT_SEARCH_DESCRIPTION = "NFT_SEARCH_DESCRIPTION";

   @RBEntry("Modello filtro - {0}")
   public static final String NFT_DESCRIPTION_FOR_EXPORT = "NFT_DESCRIPTION_FOR_EXPORT";
}
