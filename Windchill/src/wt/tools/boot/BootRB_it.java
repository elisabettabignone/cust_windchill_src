/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.tools.boot;

import wt.util.resource.*;

@RBUUID("wt.tools.boot.BootRB")
public final class BootRB_it extends WTListResourceBundle {
   @RBEntry("Ricerca nella directory {0} in corso")
   public static final String SEARCHING = "1";

   @RBEntry("Creazione del file {0} in corso")
   public static final String CREATING = "2";

   @RBEntry("Aggiunta del file {0} in corso")
   public static final String ADDING = "3";

   @RBEntry("(dim. iniziale={0}) (dim. compressa={1}) (perc. compressione {2})")
   public static final String DEFLATED = "4";

   @RBEntry("Memoria insufficiente. Aumentare la dimensione della heap. Ex. java -mx64m wt.tools.boot.MakeJar")
   public static final String INCREASEHEAP = "5";

   @RBEntry("Elaborazione del file {0} in corso")
   public static final String PROCESSING = "6";

   @RBEntry("Controllo delle modifiche in corso: {0}")
   public static final String CHECKING = "7";

   @RBEntry("File aggiunti: {0}")
   public static final String FILESADDED = "8";

   @RBEntry("File Jar di produzione aggiornato: {0}")
   public static final String JARUPDATED = "9";

   @RBEntry("Nessuna modifica trovata: {0}")
   public static final String NOCHANGE = "10";

   @RBEntry("File trovati: {0}")
   public static final String FILESFOUND = "11";

   @RBEntry("L'opzione di ricorsività è valida solo se tutti i file jar e config si trovano al di sotto della directory radice")
   public static final String FILES_NOT_IN_ROOT = "12";

   @RBEntry("L'opzione updateVersionOnly richiede the l'istruzione updateVersion sia specificata nel file wt.properties o immessa dalla riga di comando")
   public static final String UPDATE_VERSION_NOT_SET = "13";

   @RBEntry("Opzione di ricorsività non valida con l'opzione updateVersionOnly")
   public static final String RECURSE_INVALID = "14";

   @RBEntry("Errore: versione jar [{0}] non nel formato X.X.X.X; verranno usate solo le 4 cifre più significative")
   public static final String ONLY_USING_4_SIG_DIGITS = "15";

   @RBEntry("Errore: impossibile aggiornare versione jar, versione più elevata trovata: {0}\"")
   public static final String MAX_VERSION_MET = "16";

   @RBEntry("Versione jar non valida [{0}]. Il formato non è X.X.X.X, dove X rappresenta una singola cifra esadecimale")
   public static final String NOT_IN_CORRECT_FORM = "17";

   @RBEntry("UpdateLegacyJars.execute()- rootPath={0}")
   @RBPseudo(false)
   public static final String EXECUTE_ROOTPATH = "18";

   @RBEntry("ECCEZIONE: {0}")
   public static final String EXCEPTION = "19";

   @RBEntry("Elaborazione in corso del file distinta base: {0}")
   public static final String PROCESSING_BOM = "20";

   @RBEntry("La directory della distinta base è vuota")
   public static final String BOM_EMPTY = "21";

   @RBEntry("La directory della distinta base non è valida")
   public static final String BOM_INVALID = "22";

   @RBEntry("Impossibile trovare il file della distinta base")
   public static final String BOM_NOT_FOUND = "23";

   @RBEntry("Verifica aggiornamenti non necessaria per il file jar legacy: {0}")
   public static final String NO_UPDATE_REQUIRED = "24";

   @RBEntry("Impossibile generare uno o più file jar.")
   public static final String BUILD_FAILED = "25";

   @RBEntry("UpdateLegacyJars, args[{0}] = {1}")
   @RBPseudo(false)
   public static final String ARGS = "26";

   @RBEntry("Classe {0} obsoleta. È stata sostituita da MakeJar.xml. Per ulteriori informazioni, vedere il manuale Windchill Customizer's Guide (Guida per la personalizzazione Windchill).")
   @RBComment("The \"Windchill Customizer's Guide\" is a document name.")
   @RBArgComment0("The name of the class throwing this exception")
   public static final String OBSOLETE_CLASS_EXCEPTION = "27";
}
