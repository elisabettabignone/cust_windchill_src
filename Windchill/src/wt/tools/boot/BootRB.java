/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.tools.boot;

import wt.util.resource.*;

@RBUUID("wt.tools.boot.BootRB")
public final class BootRB extends WTListResourceBundle {
   @RBEntry("Searching {0}")
   public static final String SEARCHING = "1";

   @RBEntry("Creating {0}")
   public static final String CREATING = "2";

   @RBEntry("Adding {0}")
   public static final String ADDING = "3";

   @RBEntry(" (in={0}) (out={1}) (deflated {2})")
   public static final String DEFLATED = "4";

   @RBEntry("Out of memory.  Please increase the heap size.  Ex. java -mx64m wt.tools.boot.MakeJar")
   public static final String INCREASEHEAP = "5";

   @RBEntry("Processing {0}")
   public static final String PROCESSING = "6";

   @RBEntry("Checking for changes: {0}")
   public static final String CHECKING = "7";

   @RBEntry("Files added: {0}")
   public static final String FILESADDED = "8";

   @RBEntry("Production Jar updated: {0}")
   public static final String JARUPDATED = "9";

   @RBEntry("No change found: {0}")
   public static final String NOCHANGE = "10";

   @RBEntry("Files found: {0}")
   public static final String FILESFOUND = "11";

   @RBEntry("Recurse option only valid when all jar and config files are under root directory")
   public static final String FILES_NOT_IN_ROOT = "12";

   @RBEntry("updateVersionOnly option requires updateVersion to be set in wt.properties or on command line")
   public static final String UPDATE_VERSION_NOT_SET = "13";

   @RBEntry("Recurse option not valid with updateVersionOnly")
   public static final String RECURSE_INVALID = "14";

   @RBEntry("Err: jar version [{0}] not of form X.X.X.X; only using 4 most significant digits")
   public static final String ONLY_USING_4_SIG_DIGITS = "15";

   @RBEntry("Err: unable to update jar version, maximum version met: {0}\"")
   public static final String MAX_VERSION_MET = "16";

   @RBEntry("Invalid jar version [{0}] not of form X.X.X.X where X is single digit hex")
   public static final String NOT_IN_CORRECT_FORM = "17";

   @RBEntry("UpdateLegacyJars.execute()- rootPath={0}")
   @RBPseudo(false)
   public static final String EXECUTE_ROOTPATH = "18";

   @RBEntry("EXCEPTION: {0}")
   public static final String EXCEPTION = "19";

   @RBEntry("Processing BOM file: {0}")
   public static final String PROCESSING_BOM = "20";

   @RBEntry("Bill of materials directory is empty!!")
   public static final String BOM_EMPTY = "21";

   @RBEntry("Bill of materials directory invalid!!")
   public static final String BOM_INVALID = "22";

   @RBEntry("Bill of materials file not found!!")
   public static final String BOM_NOT_FOUND = "23";

   @RBEntry("No update check required for legacy jar: {0}")
   public static final String NO_UPDATE_REQUIRED = "24";

   @RBEntry("Build failed for one or more jars.")
   public static final String BUILD_FAILED = "25";

   @RBEntry("UpdateLegacyJars, args[{0}] = {1}")
   @RBPseudo(false)
   public static final String ARGS = "26";

   @RBEntry("The {0} class is obsolete.  It's use has been replaced by MakeJar.xml. See the Windchill Customizer's Guide for more information.")
   @RBComment("The \"Windchill Customizer's Guide\" is a document name.")
   @RBArgComment0("The name of the class throwing this exception")
   public static final String OBSOLETE_CLASS_EXCEPTION = "27";
}
