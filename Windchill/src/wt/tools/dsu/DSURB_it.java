package wt.tools.dsu;

import wt.util.resource.*;

public final class DSURB_it extends WTListResourceBundle {
   @RBEntry("Aggiunta di file {0} a {1} in corso")
   public static final String PRIVATE_CONSTANT_0 = "adding";

   @RBEntry("DSU {0} è già stato installato -  _uninst.jar non sarà sovrascritto\n")
   public static final String PRIVATE_CONSTANT_1 = "alreadyinstalled";

   @RBEntry("Errore durante il tentativo di backup di sistema. DSU non installato.")
   public static final String PRIVATE_CONSTANT_2 = "backupfailed";

   @RBEntry("Impossibile aprire file jar di backup {0} - non esiste niente da ripristinare")
   public static final String PRIVATE_CONSTANT_3 = "backupnotfound";

   @RBEntry("Creazione dsu {0} in {1} in corso")
   public static final String PRIVATE_CONSTANT_4 = "creating";

   @RBEntry("Impossibile creare directory: {0}")
   public static final String PRIVATE_CONSTANT_5 = "dirfail";

   @RBEntry("MakeDSU chiamato con i file {0}")
   public static final String PRIVATE_CONSTANT_6 = "filecountmsg";

   @RBEntry("File {0} non trovato *IGNORED*")
   public static final String PRIVATE_CONSTANT_7 = "filenotfound";

   @RBEntry("File jar DSU {0} non trovato")
   public static final String PRIVATE_CONSTANT_8 = "jarfilenotfound";

   @RBEntry("DSU {0} caricato da: {1}, a: {2}")
   public static final String PRIVATE_CONSTANT_9 = "loaddsu";

   @RBEntry("Caricamento del file {0} da: {1}")
   public static final String PRIVATE_CONSTANT_10 = "loading";

   @RBEntry("Utilizzo di LoadDSU: LoadDSU dsu=[verbose= path=]\ndsu=<DSUID>\nverbose=[true|false] {default è false}\npath=<DSU_?.jar path> {default è .\\<DSUID>.jar}\nLoadDSU installa un insieme di file da un file jar DSU di Windchill. Prima di caricare i nuovi file, l'applicazione tenta di mantenere i file esistenti tramite la disinstallazione di DSU.  Il file jar di disinstallazione si trova nella stessa directory del DSU selezionato, con il suffisso \"_uninst.jar\". LoadDSU registra gli eventi selezionati in un file visualizzato nella finestra di dialogo Informazioni su Windchill.  Se l'indicatore verbose è impostato su true, tutti gli eventi di caricamento e mantenimento vengono registrati. La proprietà wt.tools.dsu.logfile imposta la posizione del file di log. Il file di registro predefinito è wt.homecodebasewtclientswindchillDSUHistory.log.\nAd esempio: java wt.tools.dsu.LoadDSU dsu=WNC_R21_01 verbose=true patchdir=Windchillpatches\n")
   public static final String PRIVATE_CONSTANT_11 = "loadusage";

   @RBEntry("Utilizzo di MakeDSU: MakeDSU dsu= [verbose= patchdir=]\ndsu=<DSUID>\nverbose=[true|false] {il default è false}\npatchdir=<Directory radice patch> {il default è wt.home\\patches\\<DSUID>}\nMakeDSU crea un file jar compresso utilizzato per distribuire gli aggiornamenti software Windchill. Tutte le directory comprese in patchdirdsu vengono aggiunte a un file jar in patchdirdsu. I nomi di file vengono modificati per impedire installazioni non controllate. Utilizzare LoadDSU per installare e registrare DSU installs.\nAd esempio: java wt.tools.dsu.MakeDSU dsu=WNC_R21_01 verbose=true patchdir=Windchillpatches\n")
   public static final String PRIVATE_CONSTANT_12 = "makeusage";

   @RBEntry("Il DSU non dispone del file {0} . Per l'assistenza, contattare il supporto tecnico.")
   public static final String PRIVATE_CONSTANT_13 = "missingdsufile";

   @RBEntry("Il file {0} manca dal sistema. Contattare il supporto tecnico.")
   public static final String PRIVATE_CONSTANT_14 = "missingfile";

   @RBEntry("È necessario specificare l'ID del dsu (ad esempio, dsu=WNC_R21_01).")
   public static final String PRIVATE_CONSTANT_15 = "nodsuid";

   @RBEntry("Per installare questo DSU è necessario essere al livello DSU {0}. Il livello DSU correntemente istallato è {1} .")
   public static final String PRIVATE_CONSTANT_16 = "nodsumatch";

   @RBEntry("Non verrà mantenuto alcun file")
   public static final String PRIVATE_CONSTANT_17 = "nofilestosave";

   @RBEntry("Questo DSU non può essere installato sul livello di release {0} .")
   public static final String PRIVATE_CONSTANT_18 = "noreleasematch";

   @RBEntry("L'utility UnloadDSU non è disponibile")
   public static final String PRIVATE_CONSTANT_19 = "nounload";

   @RBEntry("Il livello DSU correntemente installato è {0} ed è più recente di quello che si tenta di installare. L'installazione del DSU non è stata effettuata.")
   public static final String PRIVATE_CONSTANT_20 = "olderdsu";

   @RBEntry("Creazione di DSU con radice a {0}")
   public static final String PRIVATE_CONSTANT_21 = "pathmsg";

   @RBEntry("Errore durante la lettura del file {0} .")
   public static final String PRIVATE_CONSTANT_22 = "readerror";

   @RBEntry("Disinstallazione del file {0}")
   public static final String PRIVATE_CONSTANT_23 = "removing";

   @RBEntry("File ripristinati da {0} a {1}")
   public static final String PRIVATE_CONSTANT_24 = "restoremsg";

   @RBEntry("Ripristino del file {0} da: {1}")
   public static final String PRIVATE_CONSTANT_25 = "restoring";

   @RBEntry("Mantenimento dei file esistenti in {0}")
   public static final String PRIVATE_CONSTANT_26 = "saveto";

   @RBEntry("Disinstallazione del file {0} non riuscita *IGNORED*")
   public static final String PRIVATE_CONSTANT_27 = "uninstfail";

   @RBEntry("Oggetti DSU {0} scaricati elencati in: {1}")
   public static final String PRIVATE_CONSTANT_28 = "unloadedmsg";

   @RBEntry("Utilizzo di UnloadDSU: UnloadDSU dsu= [verbose= path=]\ndsu=<DSUID>\nverbose=[true|false] {il default è false}\npath=<percorso DSU_?.jar> {il default è .\\<DSUID>.jar}\nUnloadDSU tenta di disinstallare un file jar DSU di Windchill. Per eseguire correttamente la disinstallazione è necessario che i file DSU.jar e DSU_uninst.jar originali siano ancora disponibili.  UnloadDSU rimuove tutti gli elementi elencati nel DSU originale sotto wt.home, quindi ripristina i file da DSU_uninst.jar. UnloadDSU registra gli eventi selezionati in un file visualizzato nella finestra di dialogo Informazioni su Windchill. Se l'indicatore verbose è impostato su true, tutti gli eventi di disinstallazione e ripristino vengono registrati. La proprietà wt.tools.dsu.logfile imposta la posizione del file di log. Il file di log predefinito è wt.homecodebasewtclientswindchillDSUHistory.log.\nAd esempio: java wt.tools.dsu.UnloadDSU dsu=WNC_R21_01 verbose=true patchdir=Windchillpatches\n")
   public static final String PRIVATE_CONSTANT_29 = "unloadusage";
}
