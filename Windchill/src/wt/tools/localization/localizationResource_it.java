package wt.tools.localization;

import wt.util.resource.*;

public final class localizationResource_it extends WTListResourceBundle {
   @RBEntry("ATTENZIONE: {0} non copiato in {1}")
   public static final String COPY_WARNING = "1";

   @RBEntry("Il file {0} non esiste non è un file")
   public static final String FILE_NOT_FOUND_ERROR = "2";

   @RBEntry("Impossibile leggere {0}")
   public static final String FILE_NOT_READABLE_ERROR = "3";

   @RBEntry("Impossibile sovrascrivere {0}")
   public static final String FILE_NOT_WRITABLE_ERROR = "4";

   @RBEntry("Localizzazione eseguita")
   public static final String LOCALIZATION_COMPLETE = "5";

   /**
    * Button Labels
    **/
   @RBEntry("Annulla")
   public static final String PRIVATE_CONSTANT_0 = "BLCancel";

   @RBEntry("Localizza")
   public static final String PRIVATE_CONSTANT_1 = "BLLocalize";

   /**
    * Check Boxes
    **/
   @RBEntry("Copia solo su file predefinito")
   public static final String PRIVATE_CONSTANT_2 = "CBDefault";

   @RBEntry("Copia su file lingua secondaria")
   public static final String PRIVATE_CONSTANT_3 = "CBSubLocal";

   /**
    * Labels
    **/
   @RBEntry("Estensione da localizzare:")
   public static final String PRIVATE_CONSTANT_4 = "LExtension";

   @RBEntry("Lingua predefinita:")
   public static final String PRIVATE_CONSTANT_5 = "LLangDefault";
}
