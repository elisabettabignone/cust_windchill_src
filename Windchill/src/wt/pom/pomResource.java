package wt.pom;

import wt.util.resource.*;

@RBUUID("wt.pom.pomResource")
public final class pomResource extends WTListResourceBundle {
   /**
/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
    * pomResource message resource bundle [English/US]
    *
    * Usage notes:
    * DO NOT REUSE IDS.  If an message becomes obsolete, comment it out
    * but do not create a new message with that id.
    *
    *
    **/
   @RBEntry("A persistence error occurred. System message follows:")
   public static final String PERSIST_ERROR = "0";

   @RBEntry("A SQL error has occurred. Database system message follows:")
   public static final String SQL_ERROR = "1";

   @RBEntry("Class \"{0}\" is not a persistent class.")
   @RBArgComment0(" {0} is the name of the class:")
   public static final String CLASS_NOT_PERSISTENT = "2";

   @RBEntry("Object \"{0}\" is already persistent.")
   @RBArgComment0(" {0} is the identifier of the object:")
   public static final String OBJECT_IS_PERSISTENT = "3";

   @RBEntry("Object \"{0}\" is not a persistent.")
   @RBArgComment0(" {0} is the identifier of the object:")
   public static final String OBJECT_NOT_PERSISTENT = "4";

   @RBEntry("Cannot update object \"{0}\" ({1}) because it has been concurrently updated.")
   @RBArgComment0(" {0} is the displayable identifier of the object")
   @RBArgComment1(" {1} is the internal identifier ")
   public static final String CONCURRENT_UPDATE = "5";

   @RBEntry("Database  \"{0}\" is not supported.")
   @RBArgComment0(" {0} is the DBMS being requested on the connection:")
   public static final String DB_UNSUPPORTED = "7";

   @RBEntry("JDBC driver \"{0}\" is not supported.")
   @RBArgComment0(" {0} is the JDBC Driver being used on the conection:")
   public static final String JDBC_UNSUPPORTED = "8";

   @RBEntry("A nested transaction cannot be started when a rollback is in progress.  The following is the rollback stacktrace.")
   public static final String NESTED_TRANS_ERROR = "9";

   @RBEntry("A commit cannot be done when a rollback is in progress.  The following is the rollback stacktrace.")
   public static final String COMMIT_ERROR = "10";

   @RBEntry("The commit is mismatched. It corresponds to level \"{0}\". Current level is \"{1}\" ")
   public static final String COMMIT_MISMATCHED = "11";

   @RBEntry("There is no active transaction. ")
   public static final String NO_ACTIVE_TRANS = "12";

   @RBEntry("Attribute \"{1}\" for class \"{0}\" is not a persistent attribute.")
   @RBArgComment0(" {0} is the class name, ")
   @RBArgComment1(" {1} is the attribute name:")
   public static final String ATTR_NON_PERSISTENT = "13";

   @RBEntry("The {0} object could not be updated in the database.")
   @RBArgComment0(" {0} is the identifier of the object:")
   public static final String UPDATE_FAILED = "14";

   @RBEntry("The {0} object could not be inserted into the database.")
   @RBArgComment0(" {0} is the identifier of the object:")
   public static final String INSERT_FAILED = "15";

   @RBEntry("Cannot delete object \"{0}\" ({1}) because it has been concurrently updated.")
   @RBArgComment0(" {0} is the displayable identifier of the object; ")
   @RBArgComment1(" {1} is the internal identifier ")
   public static final String CONCURRENT_DELETE = "16";

   @RBEntry("A SQL error has occurred for the statement \"{0}\". Database system message follows:")
   @RBArgComment0(" {0} is the SQL statement that was executed when the error occurred ")
   public static final String SQL_ERROR_WITH_STATEMENT = "17";

   @RBEntry("The paging fetch results cannot be retrieved because the paging session has timed out (session ID={0}).")
   @RBArgComment0(" {0} is the paging session ID")
   public static final String PAGINGSESSION_CACHE_TIMEOUT = "20";

   @RBEntry("Paging fetch cannot be completed for this paging session (a background process may still be processing the paging session).")
   public static final String PAGING_FETCH_TIMEOUT = "21";

   @RBEntry("Search results size has exceeded the limit set by the System Administrator.")
   public static final String PAGING_SNAPSHOT_REACH_LIMIT = "22";

   @RBEntry("wt.pom.RefreshCache: size > 10922")
   public static final String MAX_SIZE = "23";

   @RBEntry("Name")
   public static final String NAME = "24";

   @RBEntry("The Transaction name cannot be set after the Transaction has started.")
   public static final String ILLEGAL_SET_TRANSACTION_NAME = "25";

   @RBEntry("The Transaction name \"{0}\" is invalid.")
   @RBArgComment0(" {0} is the name specified for a Transaction ")
   public static final String INVALID_TRANSACTION_NAME = "26";

   @RBEntry("Savepoint has already been set.")
   public static final String SAVEPOINT_ALREADY_SET = "27";

   @RBEntry("Paging query results are not accessible due to the failure of background insertion process.")
   public static final String BACKGROUND_INSERTION_CATCHING_EXCEPTION = "28";

   @RBEntry("Connection is not available and background insertion process is not completed.")
   public static final String BACKGROUND_INSERTION_CONNECTION_NOT_AVAILABLE = "29";

   @RBEntry("This paging session is invalid because the number of concurrent paging session has exceeded the configured limit. Contact System Administrator.")
   public static final String INVALID_PAGING_SESSION = "30";

   @RBEntry("An invalid operation has been invoked in a Transaction \"beforeCompletion\" event (object={0}).")
   @RBArgComment0("This argument is the object that the operation was attempted on. ")
   public static final String INVALID_BEFORE_COMPLETION_OPERATION = "31";

   @RBEntry("A datastore uniqueness constraint violation has occurred.")
   public static final String UNIQUENESS_CONSTRAINT_VIOLATION = "32";

   @RBEntry("A datastore uniqueness constraint violation has occurred on the following objects.")
   public static final String UNIQUENESS_CONSTRAINT_VIOLATION_WITH_MESSAGES = "33";

   @RBEntry("General")
   public static final String DEFAULT_TRANSACTION_DESCRIPTION = "40";

   @RBEntry("The Transaction description must implement Evolvable (class={0}).")
   @RBArgComment0("This argument is the class name of the description object. ")
   public static final String NON_EVOLVABLE_TRANSACTION_DESCRIPTION = "41";

   @RBEntry("A transaction rollback has occurred without aborting the transaction. The following is the rollback stacktrace.")
   public static final String ROLLBACK_ALLOWED_TO_CONTINUE = "42";

   @RBEntry("The paging fetch results cannot be retrieved because the paging session cache overflow (session ID={0}).")
   @RBArgComment0(" {0} is the paging session ID")
   public static final String PAGINGSESSION_CACHE_OVERFLOW = "44";

   @RBEntry("A persistence error occurred.")
   public static final String SECURE_PERSIST_ERROR = "50";

   @RBEntry("A transaction cannot be started because the connection is in an invalid auto-commit mode.")
   public static final String INVALID_AUTOCOMMIT_MODE = "51";

   @RBEntry("A database connection failure has occurred and the operation has been aborted.")
   public static final String CONNECTION_LOST = "101";
}
