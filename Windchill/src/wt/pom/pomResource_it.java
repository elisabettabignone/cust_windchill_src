package wt.pom;

import wt.util.resource.*;

@RBUUID("wt.pom.pomResource")
public final class pomResource_it extends WTListResourceBundle {
   /**
/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
    * pomResource message resource bundle [English/US]
    *
    * Usage notes:
    * DO NOT REUSE IDS.  If an message becomes obsolete, comment it out
    * but do not create a new message with that id.
    *
    *
    **/
   @RBEntry("Si è verificato un errore di persistenza. Messaggio di sistema:")
   public static final String PERSIST_ERROR = "0";

   @RBEntry("Si è verificato un errore SQL. Messaggio di sistema del database:")
   public static final String SQL_ERROR = "1";

   @RBEntry("La classe \"{0}\" non è una classe persistente.")
   @RBArgComment0(" {0} is the name of the class:")
   public static final String CLASS_NOT_PERSISTENT = "2";

   @RBEntry("L'oggetto \"{0}\" è già persistente.")
   @RBArgComment0(" {0} is the identifier of the object:")
   public static final String OBJECT_IS_PERSISTENT = "3";

   @RBEntry("L'oggetto \"{0}\" non è persistente.")
   @RBArgComment0(" {0} is the identifier of the object:")
   public static final String OBJECT_NOT_PERSISTENT = "4";

   @RBEntry("Impossibile aggiornare l'oggetto \"{0}\" ({1}) perché è già in corso un processo di aggiornamento.")
   @RBArgComment0(" {0} is the displayable identifier of the object")
   @RBArgComment1(" {1} is the internal identifier ")
   public static final String CONCURRENT_UPDATE = "5";

   @RBEntry("Il database \"{0}\" non è supportato.")
   @RBArgComment0(" {0} is the DBMS being requested on the connection:")
   public static final String DB_UNSUPPORTED = "7";

   @RBEntry("Il driver JDBC \"{0}\" non è supportato.")
   @RBArgComment0(" {0} is the JDBC Driver being used on the conection:")
   public static final String JDBC_UNSUPPORTED = "8";

   @RBEntry("Impossibile avviare una transazione annidata durante un rollback. Segue lo stackTrace per il rollback.")
   public static final String NESTED_TRANS_ERROR = "9";

   @RBEntry("Impossibile effettuare il commit durante un rollback. Segue lo stackTrace per il rollback.")
   public static final String COMMIT_ERROR = "10";

   @RBEntry("Commit non corrispondente. Corrisponde al livello \"{0}\". Il livello corrente è \"{1}\" ")
   public static final String COMMIT_MISMATCHED = "11";

   @RBEntry("Non sono presenti transazioni attive. ")
   public static final String NO_ACTIVE_TRANS = "12";

   @RBEntry("L'attributo \"{1}\" della classe \"{0}\" non è un attributo persistente.")
   @RBArgComment0(" {0} is the class name, ")
   @RBArgComment1(" {1} is the attribute name:")
   public static final String ATTR_NON_PERSISTENT = "13";

   @RBEntry("Impossibile aggiornare l'oggetto {0} nel database.")
   @RBArgComment0(" {0} is the identifier of the object:")
   public static final String UPDATE_FAILED = "14";

   @RBEntry("Impossibile inserire l'oggetto {0} nel database.")
   @RBArgComment0(" {0} is the identifier of the object:")
   public static final String INSERT_FAILED = "15";

   @RBEntry("Impossibile eliminare l'oggetto \"{0}\" ({1}) perché è già in corso un processo di aggiornamento.")
   @RBArgComment0(" {0} is the displayable identifier of the object; ")
   @RBArgComment1(" {1} is the internal identifier ")
   public static final String CONCURRENT_DELETE = "16";

   @RBEntry("Un errore SQL si è verificato per l'istruzione \"{0}\". Messaggio di sistema del database:")
   @RBArgComment0(" {0} is the SQL statement that was executed when the error occurred ")
   public static final String SQL_ERROR_WITH_STATEMENT = "17";

   @RBEntry("I risultati dell'impaginazione non possono essere recuperati perché la sessione d'impaginazione è scaduta (ID sessione={0}).")
   @RBArgComment0(" {0} is the paging session ID")
   public static final String PAGINGSESSION_CACHE_TIMEOUT = "20";

   @RBEntry("L'impaginazione non può essere completata in questa sessione (un processo in background potrebbe star elaborando l'impaginazione).")
   public static final String PAGING_FETCH_TIMEOUT = "21";

   @RBEntry("La dimensione dei risultati della ricerca è superiore al limite imposto dall'amministratore di sistema.")
   public static final String PAGING_SNAPSHOT_REACH_LIMIT = "22";

   @RBEntry("wt.pom.RefreshCache: size > 10922")
   public static final String MAX_SIZE = "23";

   @RBEntry("Nome")
   public static final String NAME = "24";

   @RBEntry("Non è possibile impostare un nome per la transazione dopo l'avvio della transazione.")
   public static final String ILLEGAL_SET_TRANSACTION_NAME = "25";

   @RBEntry("Il nome transazione \"{0}\" non è valido.")
   @RBArgComment0(" {0} is the name specified for a Transaction ")
   public static final String INVALID_TRANSACTION_NAME = "26";

   @RBEntry("Il punto di salvataggio è già stato impostato.")
   public static final String SAVEPOINT_ALREADY_SET = "27";

   @RBEntry("I risultati dell'interrogazione pagine non sono accessibili a causa del fallimento del processo di inserimento in background.")
   public static final String BACKGROUND_INSERTION_CATCHING_EXCEPTION = "28";

   @RBEntry("La connessione non è disponibile e il processo di inserimento in background non è completo.")
   public static final String BACKGROUND_INSERTION_CONNECTION_NOT_AVAILABLE = "29";

   @RBEntry("Sessione di impaginazione non valida: il numero di sessioni di impaginazione simultanee ha superato il limite impostato. Contattare l'amministratore di sistema.")
   public static final String INVALID_PAGING_SESSION = "30";

   @RBEntry("Richiamata operazione non valida in un evento transazione \"beforeCompletion\" (oggetto={0}).")
   @RBArgComment0("This argument is the object that the operation was attempted on. ")
   public static final String INVALID_BEFORE_COMPLETION_OPERATION = "31";

   @RBEntry("Si è verificata una violazione del vincolo di univocità per l'archivio dati.")
   public static final String UNIQUENESS_CONSTRAINT_VIOLATION = "32";

   @RBEntry("Si è verificata una violazione del vincolo di univocità per l'archivio dati in relazione agli oggetti che seguono.")
   public static final String UNIQUENESS_CONSTRAINT_VIOLATION_WITH_MESSAGES = "33";

   @RBEntry("Generale")
   public static final String DEFAULT_TRANSACTION_DESCRIPTION = "40";

   @RBEntry("La descrizione della transazione deve implementare Evolvable (classe={0}).")
   @RBArgComment0("This argument is the class name of the description object. ")
   public static final String NON_EVOLVABLE_TRANSACTION_DESCRIPTION = "41";

   @RBEntry("Si è verificato un rollback di una transazione senza che la transazione venisse interrotta. Segue lo stackTrace per il rollback.")
   public static final String ROLLBACK_ALLOWED_TO_CONTINUE = "42";

   @RBEntry("I risultati dell'impaginazione non possono essere recuperati perché nella sessione di impaginazione si è verificato un overflow della cache (ID sessione={0}).")
   @RBArgComment0(" {0} is the paging session ID")
   public static final String PAGINGSESSION_CACHE_OVERFLOW = "44";

   @RBEntry("Si è verificato un errore di persistenza.")
   public static final String SECURE_PERSIST_ERROR = "50";

   @RBEntry("Impossibile avviare una transazione. La modalità di esecuzione automatica della connessione non è valida.")
   public static final String INVALID_AUTOCOMMIT_MODE = "51";

   @RBEntry("Si è verificato un errore di connessione al database e l'operazione è stata interrotta.")
   public static final String CONNECTION_LOST = "101";
}
