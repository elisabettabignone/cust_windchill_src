package wt.pom.concurrent;

import wt.util.resource.*;

@RBUUID("wt.pom.concurrent.concurrentResource")
public final class concurrentResource_it extends WTListResourceBundle {
   /**
/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
    * concurrentResource message resource bundle [English/US]
    * 
    * Usage notes:
    * DO NOT REUSE IDS.  If an message becomes obsolete, comment it out
    * but do not create a new message with that id.
    * 
    * 
    **/
   @RBEntry("Si è verificato un rollback in un thread che condivide questa transazione.")
   public static final String SHARED_TRANSACTION_ROLLBACK = "0";

   @RBEntry("Si è verificato il rollback di un punto di salvataggio per una transazione condivisa.")
   public static final String SHARED_TRANSACTION_SAVEPOINT_ROLLBACK = "1";

   @RBEntry("La transazione corrente non è stata avviata. Impossibile condividerla.")
   public static final String SHARED_TRANSACTION_CURRENT_TRANSACTION_NOT_STARTED = "2";

   @RBEntry("La transazione condivisa è già stata completata.")
   public static final String SHARED_TRANSACTION_ALREADY_COMPLETED = "3";
}
