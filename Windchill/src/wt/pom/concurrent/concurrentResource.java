package wt.pom.concurrent;

import wt.util.resource.*;

@RBUUID("wt.pom.concurrent.concurrentResource")
public final class concurrentResource extends WTListResourceBundle {
   /**
/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
    * concurrentResource message resource bundle [English/US]
    * 
    * Usage notes:
    * DO NOT REUSE IDS.  If an message becomes obsolete, comment it out
    * but do not create a new message with that id.
    * 
    * 
    **/
   @RBEntry("A rollback has occurred in a thread sharing this transaction.")
   public static final String SHARED_TRANSACTION_ROLLBACK = "0";

   @RBEntry("A savepoint rollback has occurred for a shared transaction.")
   public static final String SHARED_TRANSACTION_SAVEPOINT_ROLLBACK = "1";

   @RBEntry("Cannnot share current transaction because it has not been started.")
   public static final String SHARED_TRANSACTION_CURRENT_TRANSACTION_NOT_STARTED = "2";

   @RBEntry("Shared transaction has already been completed.")
   public static final String SHARED_TRANSACTION_ALREADY_COMPLETED = "3";
}
