/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.mail;

import wt.util.resource.*;

@RBUUID("wt.mail.mailResource")
public final class mailResource_it extends WTListResourceBundle {
   @RBEntry("\"{0}\" ha già un valore.")
   @RBComment("Message stating that a specified object already has a value")
   @RBArgComment0("Object that already has a value")
   public static final String HEADER_VALUE_PRESENT = "0";

   @RBEntry("Errore nel destinatario \"{0}\".  Posta non inviata.")
   @RBComment("Message stating that an attempt to send an e mail to the specified address failed and was not sent")
   @RBArgComment0("Recipient e mail address with which there was an error")
   public static final String RECIPIENT_ADDRESS_ERROR = "1";

   @RBEntry("Errore nel mittente \"{0}\".  Posta non inviata.")
   @RBComment("Message stating that there was an error with the specified sender's e mail address and the mail was not sent")
   @RBArgComment0("String: sender e mail address with which there was an error")
   public static final String ORIGINATOR_ADDRESS_ERROR = "2";

   @RBEntry("Errore di connessione al server di posta. Posta non inviata.")
   @RBComment("Message stating that there was an error connecting to the mail server and that the mail was not sent")
   public static final String CONNECTION_ERROR = "3";

   @RBEntry("Errore nella trasmissione della posta durante la sessione HELO.  Posta non inviata.")
   @RBComment("Message stating that there was an error during the HELO session and that the mail was not sent.")
   public static final String HELO_TRANSMISSION_ERROR = "4";

   @RBEntry("Errore nella trasmissione della posta.")
   @RBComment("General message stating that there was an error in the mail transmission")
   public static final String TRANSMISSION_ERROR = "5";

   @RBEntry("Codifica \"{0}\" sconosciuta.")
   @RBComment("Message stating that the specified encoding is unknown to the mail server")
   @RBArgComment0("String: encoding that is unknown")
   public static final String UNKNOWN_ENCODING = "6";

   @RBEntry("Nessun dato")
   @RBComment("Message stating that there is no data")
   public static final String NO_DATA = "7";

   @RBEntry("Chiamata non valida")
   @RBComment("Message stating that an illegal call was made")
   public static final String ILLEGAL_CALL = "8";

   @RBEntry("Errore nell'impostazione del mittente del messaggio.  Mittente nullo.")
   @RBComment("Message stating that there was an error setting the sender, sender is set to null")
   public static final String NO_SENDER = "9";

   @RBEntry("Impossibile mettere in coda messaggio e-mail. Impossibile trovare l'amministratore.")
   @RBComment("Exception message stating that the administrator user was not be found for setting queue entry ownership ")
   public static final String ADMIN_USER_NOT_FOUND = "10";

   @RBEntry("Sì")
   @RBComment("When sending an attribute attachment with e-mails this is used to localize boolean values of true.")
   public static final String BOOLEAN_ATT_VALUE_TRUE = "11";

   @RBEntry("No")
   @RBComment("When sending an attribute attachment with e-mails this is used to localize boolean values of false.")
   public static final String BOOLEAN_ATT_VALUE_FALSE = "12";
}
