/* bcwti
 *
 * Copyright (c) 2013 PTC, Inc. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement.
 * You shall not disclose such confidential information and shall use it
 * only in accordance with the terms of the license agreement.
 *
 * ecwti
 */
package wt.org;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBArgComment1;
import wt.util.resource.RBArgComment2;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

/**
 * A resource bundle for license groups.
 *
 * <BR><BR><B>Supported API: </B>false
 * <BR><BR><B>Extendable: </B>false
 */
@RBUUID("wt.org.licenseGroupResource")
public final class licenseGroupResource extends WTListResourceBundle {
   @RBEntry("The system already contains a group with the name {0} but it has parent groups, which is not expected. Contact your system administrator to resolve this issue.")
   @RBArgComment0("License group that does not meet criteria")
   @RBArgComment1("Oid of above group")
   public static final String ROOT_GROUP_CANNOT_HAVE_PARENT_EXCEPTION = "ROOT_GROUP_CANNOT_HAVE_PARENT_EXCEPTION";

   @RBEntry("The system already contains a group with the name {0} but it does not belong to a parent group named {1} as the configuration requires it to. Contact your system administrator to resolve this issue.")
   @RBArgComment0("License group that does not meet criteria")
   @RBArgComment1("Oid of above group")
   @RBArgComment2("Root license group name")
   public static final String NO_ROOT_GROUP_AS_PARENT_EXCEPTION = "NO_ROOT_GROUP_AS_PARENT_EXCEPTION";

   @RBEntry("The license group {0} ({1}) contains an unexpected member.")
   @RBArgComment0("License group that does not meet criteria")
   @RBArgComment1("Oid of above group")
   public static final String GROUP_HAS_UNEXPECTED_MEMBER_EXCEPTION = "GROUP_HAS_UNEXPECTED_MEMBER_EXCEPTION";

   @RBEntry("The license group {0} ({1}) contains more than one member.")
   @RBArgComment0("License group that does not meet criteria")
   @RBArgComment1("Oid of above group")
   public static final String GROUP_HAS_MORE_THAN_ONE_MEMBER_EXCEPTION = "GROUP_HAS_MORE_THAN_ONE_MEMBER_EXCEPTION";

   @RBEntry("The system already contains multiple groups with the name {0}. This is not a valid configuration. Contact your system administrator to resolve this issue.")
   @RBArgComment0("License group that does not meet criteria")
   public static final String MULTIPLE_GROUPS_WITH_NAME_EXCEPTION = "MULTIPLE_GROUPS_WITH_NAME_EXCEPTION";

   @RBEntry("The external license group with the name {0} is disabled or disconnected.")
   @RBArgComment0("External License group that is disabled or disconnected in the system")
   public static final String EXTERNAL_LICENSE_GROUP_DISABLED_OR_DISCONNECTED = "EXTERNAL_LICENSE_GROUP_DISABLED_OR_DISCONNECTED";

   @RBEntry("The group {0} was not found. Contact your system administrator to resolve this issue.")
   @RBArgComment0("Name of missing license group")
   public static final String MISSING_INT_LICENSE_GROUP = "MISSING_INT_LICENSE_GROUP";

   @RBEntry("The license group corresponding to the internal group {0}, was not found. Contact your system administrator to resolve this issue.")
   @RBArgComment0("Name of missing license group")
   public static final String MISSING_EXT_LICENSE_GROUP = "MISSING_EXT_LICENSE_GROUP";

   /**
    * License Group Names and Descriptions
    */
   @RBEntry("System Licenses")
   @RBComment("DO NOT TRANSLATE")
   public static final String SYSTEM_LICENSES = "SystemLicenses";

   @RBEntry("System group for managing license groups")
   @RBComment("DO NOT TRANSLATE")
   public static final String SYSTEM_LICENSES_DESC = "SYSTEM_LICENSES_DESC";

   @RBEntry("View and Print Only")
   @RBComment("Name of a license group")
   public static final String VIEW_AND_PRINT_ONLY = "ViewandPrintOnly";

   @RBEntry("License group for limiting to viewing and printing objects")
   @RBComment("Description of a license group")
   public static final String VIEW_AND_PRINT_ONLY_DESC = "VIEW_AND_PRINT_ONLY_DESC";

   @RBEntry("PLM Connector Importable Package")
   @RBComment("Name of a license group")
   public static final String PLM_CONNECTOR_IMPORT_PACKAGE = "PLMConnectorImportablePackage";

   @RBEntry("License group for authorizing creation of package importable ZIP files")
   @RBComment("Description of a license group")
   public static final String PLM_CONNECTOR_IMPORT_PACKAGE_DESC = "PLM_CONNECTOR_IMPORT_PACKAGE_DESC";

   @RBEntry("Platform Structures")
   @RBComment("Name of a license group")
   public static final String PLATFORM_STRUCTURES = "PlatformStructures";

   @RBEntry("License group for working with expressions on platform structures")
   @RBComment("Description of a license group")
   public static final String PLATFORM_STRUCTURES_DESC = "PLATFORM_STRUCTURES_DESC";
}
