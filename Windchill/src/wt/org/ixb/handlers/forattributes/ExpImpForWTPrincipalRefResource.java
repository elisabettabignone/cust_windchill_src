/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.org.ixb.handlers.forattributes;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBArgComment1;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.org.ixb.handlers.forattributes")
public final class ExpImpForWTPrincipalRefResource extends WTListResourceBundle {
   @RBEntry("If the attribute \"{0}\" has a value of true, the \"{1}\" attribute must have a value from the following list: \"{2}\". \nThe object that references this participant will not import successfully.")
   public static final String MISSING_REQUIRED_ATTRIBUTE = "MISSING_REQUIRED_ATTRIBUTE";
}
