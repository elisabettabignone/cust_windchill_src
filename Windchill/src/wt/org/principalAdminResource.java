/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.org;

import wt.util.resource.*;

@RBUUID("wt.org.principalAdminResource")
public final class principalAdminResource extends WTListResourceBundle {
   @RBEntry("Help")
   @RBComment("Label for link to help")
   public static final String HELP = "0";

   @RBEntry("Home")
   @RBComment("Label for link to Windchill home page")
   public static final String HOME = "1";

   @RBEntry("View users and groups")
   public static final String VIEW_USERS_AND_GROUPS = "2";

   @RBEntry("Create user")
   public static final String CREATE_USER = "3";

   @RBEntry("Create group")
   public static final String CREATE_GROUP = "4";

   @RBEntry("Edit group membership")
   public static final String EDIT_GROUP_MEMBERSHIP = "5";

   @RBEntry("Cache management")
   public static final String CACHE_MANAGEMENT = "6";

   @RBEntry("Update group")
   public static final String UPDATE_GROUP_TITLE = "7";

   @RBEntry("Windchill system:")
   public static final String WINDCHILL_SYSTEM_LABEL = "8";

   @RBEntry("Name:")
   public static final String NAME_LABEL = "9";

   @RBEntry("Location:")
   public static final String LOCATION_LABEL = "10";

   @RBEntry("Description:")
   public static final String DESCRIPTION_LABEL = "11";

   @RBEntry("Members:")
   public static final String MEMBERS_LABEL = "12";

   @RBEntry("Parent groups:")
   public static final String PARENT_GROUPS_LABEL = "13";

   @RBEntry("Domain of user:")
   public static final String USER_DOMAIN_LABEL = "14";

   @RBEntry("OK")
   public static final String OK = "15";

   @RBEntry("Reset")
   public static final String RESET = "16";

   @RBEntry("(none)")
   public static final String NONE = "17";

   @RBEntry("Update user")
   public static final String UPDATE_USER_TITLE = "18";

   @RBEntry("Save user as")
   public static final String SAVE_USER_AS_TITLE = "19";

   @RBEntry("User ID:")
   public static final String USER_ID_LABEL = "20";

   @RBEntry("Full name:")
   public static final String FULL_NAME_LABEL = "21";

   @RBEntry("E-mail address:")
   public static final String EMAIL_ADDRESS_LABEL = "22";

   @RBEntry("Preferred language:")
   public static final String LANGUAGE_LABEL = "23";

   @RBEntry("Postal address:")
   public static final String POSTAL_ADDRESS_LABEL = "24";

   @RBEntry("Password:")
   public static final String PASSWORD_LABEL = "25";

   @RBEntry("Confirm password:")
   public static final String CONFIRM_PASSWORD_LABEL = "26";

   @RBEntry("The passwords do not match.")
   public static final String PASSWORD_MISMATCH = "27";

   @RBEntry("Find")
   @RBComment("Verb for querying users and groups")
   public static final String FIND = "28";

   @RBEntry("Users")
   @RBComment("The type of principal to be queried")
   public static final String USERS = "29";

   @RBEntry("Groups")
   @RBComment("The type of principal to be queried")
   public static final String GROUPS = "30";

   @RBEntry("in")
   public static final String IN = "31";

   @RBEntry("whose name contains")
   public static final String WHOSE_NAME_CONTAINS = "32";

   @RBEntry("Search")
   @RBComment("search button label")
   public static final String SEARCH = "33";

   @RBEntry("For Windchill system:")
   public static final String FOR_WINDCHILL_SYSTEM_LABEL = "34";

   @RBEntry("Create group here:")
   public static final String CREATE_GROUP_HERE_LABEL = "35";

   @RBEntry("Associate with Windchill system:")
   public static final String ASSOCIATE_WITH_WINDCHILL_SYSTEM_LABEL = "36";

   @RBEntry("A value is missing for a required field.")
   public static final String REQUIRED_FIELD_MISSING = "37";

   @RBEntry("Create user here:")
   public static final String CREATE_USER_HERE_LABEL = "38";

   @RBEntry("Edit group membership for")
   public static final String EDIT_GROUP_MEMBERSHIP_FOR = "39";

   @RBEntry("Remove")
   @RBComment("Label for remove button")
   public static final String REMOVE = "40";

   @RBEntry("(no members)")
   public static final String NO_MEMBERS = "41";

   @RBEntry("User ID")
   public static final String USER_ID = "42";

   @RBEntry("Full name")
   public static final String FULL_NAME = "43";

   @RBEntry("Actions")
   public static final String ACTIONS = "44";

   @RBEntry("Name")
   public static final String NAME = "45";

   @RBEntry("Description")
   public static final String DESCRIPTION = "46";

   @RBEntry("No matches.")
   public static final String NO_MATCHES = "47";

   @RBEntry("Delete")
   public static final String DELETE = "48";

   @RBEntry("Add to group")
   public static final String ADD_TO_GROUP = "49";

   @RBEntry("Purge the principal cache")
   public static final String PURGE_PRINCIPAL_CACHE = "50";

   @RBEntry("(all entries)")
   public static final String ALL_ENTRIES = "51";

   @RBEntry("Purge all cache entries associated with service:")
   public static final String PURGE_ALL_ENTRIES_LABEL = "52";

   @RBEntry("Restrict to cache entry with distinguished name:")
   public static final String PURGE_SINGLE_ENTRY_LABEL = "53";

   @RBEntry("Domain of group:")
   public static final String GROUP_DOMAIN_LABEL = "54";

   @RBEntry("Domain of personal cabinet:")
   public static final String CABINET_DOMAIN_LABEL = "55";

   @RBEntry("Location")
   public static final String DIRECTORY_SERVICE = "56";

   @RBEntry("Member")
   public static final String MEMBER = "57";

   @RBEntry("Parent")
   public static final String PARENT = "58";

   @RBEntry("Group")
   public static final String GROUP = "59";

   @RBEntry("Error reported by Info*Engine")
   public static final String INFO_ENGINE_EXCEPTION = "60";

   @RBEntry("Temporary error reported by Info*Engine")
   public static final String INFO_ENGINE_TEMPORARY_EXCEPTION = "61";

   @RBEntry("Error reported by Windchill")
   public static final String WINDCHILL_EXCEPTION = "62";

   @RBEntry("Error report")
   public static final String GENERIC_EXCEPTION = "63";

   @RBEntry("Updating electronic identification for")
   public static final String ELECTRONIC_ID_TITLE = "64";

   @RBEntry("Select Administrative Domain for {0}")
   @RBArgComment0("Name of user or group")
   public static final String DOMAIN_BROWSER_TITLE = "65";

   @RBEntry("new user")
   public static final String NEW_USER_LABEL = "66";

   @RBEntry("new user's cabinet")
   public static final String NEW_CABINET_LABEL = "67";

   @RBEntry("new group")
   public static final String NEW_GROUP_LABEL = "68";

   @RBEntry("Browse ...")
   public static final String CHANGE_LABEL = "69";

   @RBEntry("Select all")
   public static final String SELECT_ALL_LABEL = "70";

   @RBEntry("Edit electronic id")
   public static final String EDIT_ELECTRONIC_ID_LABEL = "71";

   @RBEntry("Edit parent groups")
   public static final String EDIT_PARENT_GROUPS_LABEL = "72";

   @RBEntry("Updating parent groups for")
   public static final String UPDATE_PARENTS_FOR = "73";

   @RBEntry("(no parent groups)")
   public static final String NO_PARENTS = "74";

   @RBEntry("or")
   public static final String OR = "75";

   @RBEntry("Repair tool")
   public static final String REPAIR_TOOL = "76";

   @RBEntry("Repair current Directory user {0}")
   @RBArgComment0("Name of user ")
   public static final String REPAIR_USER_TITLE = "77";

   @RBEntry("Repair current Directory group {0}")
   @RBArgComment0("Name of group")
   public static final String REPAIR_GROUP_TITLE = "78";

   @RBEntry("Old name within Windchill")
   public static final String OLDNAME_LABEL = "79";

   @RBEntry("Old DN within Directory")
   public static final String OLD_DN = "80";

   @RBEntry("Delete tool")
   public static final String DELETE_TOOL = "81";

   @RBEntry("No Invalid Principals")
   public static final String NO_INVALID_PRINCIPALS = "82";

   @RBEntry("The name is already assigned to another user or group")
   public static final String NAME_IN_USE = "83";

   @RBEntry("Failed to create User object. ")
   public static final String USER_CREATION_FAILED = "84";

   @RBEntry("This operation is not supported ")
   public static final String OPERATION_NOT_SUPPORTED = "85";
}
