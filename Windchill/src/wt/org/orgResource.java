/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.org;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBArgComment1;
import wt.util.resource.RBArgComment2;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.org.orgResource")
public final class orgResource extends WTListResourceBundle {
   @RBEntry("The operation:   \"{0}\" failed.")
   @RBComment("Message specifying an operation that failed.")
   @RBArgComment0("Operation that failed")
   public static final String OPERATION_FAILED = "0";

   @RBEntry("Name \"{0}\" is not unique.")
   @RBComment("Message specifying a name that is not unique.")
   @RBArgComment0("Name that is not unique")
   public static final String NAME_NOT_UNIQUE = "1";

   @RBEntry("Cannot delete administrator access.")
   @RBComment("Message stating that specified user cannot be deleted, i.e. default administrator.")
   @RBArgComment0("User which cannot be deleted(admin)")
   public static final String CANT_DELETE_ADMIN = "2";

   @RBEntry("Cannot change name of pre-defined principal: \"{0}\".")
   @RBComment("Message stating that the specified name cannot be changed.")
   @RBArgComment0("pre-defined principal to which a name change was attempted")
   public static final String CANT_CHANGE_NAME = "3";

   @RBEntry("WTPrincipal class expected.")
   @RBComment("Message stating that the class is not a WTPrincipal class.")
   public static final String WRONG_CLASS = "4";

   @RBEntry("Attempt to modify external LDAP directory.  Use external LDAP directory tools to make modifications to data.")
   public static final String LDAP_MODIFY = "5";

   @RBEntry("Name \"{0}\" was not found in the external LDAP directory \"{1}\".")
   @RBComment("Message stating that the specified principal name was not found in the specified LDAP directory.")
   @RBArgComment0("principal name that was not found")
   @RBArgComment1("LDAP directory name that does not contain specified principal name")
   public static final String LDAP_NOT_FOUND = "6";

   @RBEntry("Property \"{0}\" was not found in the properties files.")
   @RBComment("Message stating that the specified property was not found in the properties files.")
   @RBArgComment0("Property that was not found in properties files")
   public static final String NULL_PROPERTY = "7";

   @RBEntry("Principal \"{0}\" not valid.")
   @RBComment("Message stating that the specified principal is not valid.")
   @RBArgComment0("principal that is not valid")
   public static final String PRINCIPAL_NOT_VALID = "8";

   @RBEntry("Users cannot be deleted.")
   @RBComment("Message stating that users cannot be deleted.")
   public static final String CANT_DELETE_USERS = "9";

   @RBEntry("Cannot remove default administrator from Administrators group.")
   @RBComment("Message reporting that the default administrator cannot be removed from Administrators group--would be used when user tries to remove administrator.")
   public static final String CANT_REMOVE_ADMIN = "10";

   @RBEntry("Missing web server name for user: \"{0}\".")
   @RBComment("Message stating that the web server name for the specified user could not be found.")
   @RBArgComment0("User for whom a web server name is missing")
   public static final String MISSING_AUTH_NAME = "11";

   @RBEntry("Missing full name for user: \"{0}\".")
   @RBComment("Message stating that the user object has no value in the full name field")
   @RBArgComment0("User for whom a full name is missing")
   public static final String MISSING_FULL_NAME = "12";

   @RBEntry("{0}:  Exception was thrown trying to get the e-mail address for \"{1}\", probably related to LDAP.  E-Mail set to blank string.")
   @RBComment("Message specifying that a specified exception was trown trying to retrieve the e mail address from a user object.")
   @RBArgComment0("type of exception")
   @RBArgComment1("user whose address Windchill is trying to get")
   public static final String EMAIL_RETRIEVAL = "13";

   @RBEntry("Unable to fill in the attributes for the user or group in the object.")
   @RBComment("Message stating that an attempt to fill the attributes for a user or group failed.")
   public static final String INFLATE_FAILED = "14";

   @RBEntry("Unable to set search criteria to user or group = {0}.")
   @RBComment("Message stating that an attempt to set a specified search criteria to a user or group failed.")
   @RBArgComment0("Criterion to which Windchill was unable to set search")
   public static final String SEARCH_CRITERIA = "15";

   @RBEntry("The participant, {0}, no longer exists.")
   @RBComment("Message stating that an attempt to find a specified principal failed.")
   @RBArgComment0("Name of principal which could not be found")
   public static final String PRINCIPAL_NOT_FOUND = "16";

   @RBEntry("Unable to update deleted group \"{0}\".")
   @RBComment("Message stating that an attempt to update a specified user or group failed.")
   @RBArgComment0("Group Windchill was unable to delete")
   public static final String CANT_UPDATE_DISABLED_GROUP = "17";

   @RBEntry("Found multiple instances of principals with name \"{0}\".")
   @RBComment("Message stating that more than one principal has the same value for its name field.")
   @RBArgComment0("Name for which multiple instances of principals have")
   public static final String MULTI_DB_HIT = "18";

   @RBEntry("The user account you are trying to access matches more than one user. Contact your system administrator.")
   @RBComment("Error message encountered during user authentication if the system finds more than one matching pending or replicated user that can be activated")
   public static final String AMBIGUOUS_RESULTS_IN_USER_AUTHENTICATION = "AMBIGUOUS_RESULTS_IN_USER_AUTHENTICATION";

   @RBEntry("Cannot create user: {0}\n\nThere are multiple pending or replicated users that are candidates for activation because they match one or more attributes of the user being created: {1}\n\nIf you did not intend to activate an existing inactive user, or if multiple matches were unexpected, create the user again with modified attributes, or contact your system administrator.")
   @RBComment("Error message encountered during user creation if the system finds more than one matching pending or replicated user that can be activated")
   @RBArgComment0("Name of the user being created")
   @RBArgComment1("Attributes of the user that match other pending or replicated users")
   public static final String AMBIGUOUS_RESULTS_IN_USER_ACTIVATION = "AMBIGUOUS_RESULTS_IN_USER_ACTIVATION";

   @RBEntry("An unexpected error has occurred. An internal query found more than one participant when only one match was expected. Contact your administrator.")
   @RBComment("Error message used in conjunction with exception scenarios where a query produces ambiguous results")
   public static final String AMBIGUOUS_RESULTS_IN_INTERNAL_QUERY = "AMBIGUOUS_RESULTS_IN_INTERNAL_QUERY";

   @RBEntry("The principal matching the distinguished name \"{0}\" is not of the requested principal type \"{1}\".")
   @RBComment("Message stating that more than one user was found in the Pending User Directory Service by a query that was expecting only one result.")
   @RBArgComment0("Distingished name of the principal being searched")
   @RBArgComment1("Type of the principal being searched (user, group or organization)")
   public static final String TYPE_MISMATCH_IN_SEARCHING_BY_DN = "TYPE_MISMATCH_IN_SEARCHING_BY_DN";

   @RBEntry("Found multiple instances of principals with name \"{0}\" in service \"{1}\".")
   @RBComment("Message stating that more than one principal has the same value for its name field in the same server.")
   @RBArgComment0("Name for which multiple instances of principals have")
   @RBArgComment1("Service name which contains multiple instances of one name")
   public static final String MULTI_SERVICE_HIT = "19";

   @RBEntry("Missing Electronic Identity for user. See Windchill Administrator.")
   @RBComment("Message stating that there is no electronic identity for the user.")
   public static final String CONTENT_ITEM_IS_NULL = "20";

   @RBEntry("{0} (deleted)")
   @RBComment("Display for the name of a user that has been deleted")
   public static final String DELETED_USER = "21";

   @RBEntry("Adding {0} to {1} would create circularity.")
   @RBComment("Can not add a specified member to a group because the member is the group itself or a parent of the group.")
   public static final String CIRCULAR_MEMBERSHIP_PROHIBITED = "22";

   @RBEntry("{0} is already associated with a valid directory entry in service {1} with distinguished name {2}.")
   @RBComment("A specified principal does not need to be repaired because it has a valid association with a directory entry.")
   public static final String NAME_ALREADY_ASSOCIATED = "23";

   @RBEntry("Multiple principals match {0} in the database, and none of these are associated with the directory entry with distinguished name {1}.")
   public static final String INVALID_NAME_DN_ASSOCIATION = "24";

   @RBEntry("Unable to create entry in LDAP.")
   @RBComment("Can not create LDAP entry.")
   public static final String UNABLE_TO_CREATE_LDAP_ENTRY = "25";

   @RBEntry("JNDI Adapter not found for {0}.")
   @RBComment("Can not found JNDIADAPTER for the specified DN.")
   public static final String JNDIADAPTER_NOT_FOUND = "26";

   @RBEntry("{0} is an invalid email address.")
   @RBComment("Error indicating that an invalid email address was supplied.")
   @RBArgComment0("The email address that is invalid.")
   public static final String INVALID_EMAIL_ADDRESS = "27";

   @RBEntry("Property can not be set: {0}")
   public static final String PROPERTY_NOT_SETTABLE = "28";

   @RBEntry("Syntax error in query expression: {0}")
   public static final String QUERY_EXPRESSION_SYNTAX_ERROR = "29";

   @RBEntry("Unsupported object class: {0}")
   public static final String UNSUPPORTED_CLASS = "30";

   @RBEntry("No such directory service: {0}")
   public static final String NO_SUCH_SERVICE = "31";

   @RBEntry("Found multiple infrastructure nodes matching the name \"{0}\".")
   public static final String AMBIGUOUS_NODE_NAME = "32";

   @RBEntry("Principal already exists: {0}")
   public static final String PRINCIPAL_ALREADY_EXISTS = "33";

   @RBEntry("Invalid URL: {0}")
   @RBComment("Error indicating that an invalid URL was supplied.")
   @RBArgComment0("The URL that is invalid.")
   public static final String INVALID_URL = "34";

   @RBEntry("No disconnected principals found with name \"{0}\".")
   public static final String NO_DISCONNECTED_PRINCIPALS = "35";

   @RBEntry("CAGE Code")
   @RBComment("Label for the organization id referencing the industry standard name 'CAGE Code'")
   public static final String CAGE_CODE = "CAGE_CODE";

   @RBEntry("ISO 6523")
   @RBComment("Label for the organization id referencing the industry standard name 'ISO 6523'")
   public static final String ISO_6523 = "ISO_6523";

   @RBEntry("DUNS Number")
   @RBComment("Label for the organization id referencing the industry standard name 'DUNS Number'")
   public static final String DUNS_NUMBER = "DUNS_NUMBER";

   @RBEntry("CAGE Code")
   @RBComment("Label for the organization for A&D ")
   public static final String CAGECODELABEL = "36";

   @RBEntry("New CAGE Code")
   @RBComment("Label for New CAGE Code. \"36\" is the value that comes from the preference system")
   public static final String PRIVATE_CONSTANT_0 = "NEW_36";

   @RBEntry("Organization ID")
   @RBComment("Label for the organization for PDMLink,ProjectLink")
   public static final String ORGIDLABEL = "37";

   @RBEntry("New Organization ID")
   @RBComment("Label for New Organization ID. \"37\" is the value that comes from the preference system")
   public static final String NEW_ORGANIZATION_ID = "NEW_37";

   @RBEntry("Duns ID")
   @RBComment("Label for the organization")
   public static final String DUNSLABEL = "38";

   @RBEntry("New Duns ID")
   @RBComment("Label for New Duns ID. \"38\" is the value that comes from the preference system")
   public static final String NEW_DUNSLABEL = "NEW_38";

   @RBEntry("Manufacturer CAGE Code")
   @RBComment("Label for the Manufacturer organization for A&D ")
   public static final String MFGCAGECODELABEL = "39";

   @RBEntry("Manufacturer ID")
   @RBComment("Label for the Manufacturer organization ")
   public static final String MFGORGIDLABEL = "40";

   @RBEntry("Manufacturer Duns ID")
   @RBComment("Label for the Manufacturer organization")
   public static final String MFGDUNSLABEL = "41";

   @RBEntry("Assembly CAGE Code")
   @RBComment("Label for the organization for A&D ")
   public static final String ASSEMBLYCAGECODELABEL = "42";

   @RBEntry("Assembly Organization ID")
   @RBComment("Label for the organization for PDMLink,ProjectLink")
   public static final String ASSEMBLYORGIDLABEL = "43";

   @RBEntry("Site")
   public static final String SITE = "44";

   @RBEntry("The operation attempted to directory service {0} failed. (no privilage or not supported)")
   public static final String OPERATION_NOT_SUPPORTED = "45";

   @RBEntry("Current User is not authorized to access \"{0}\"")
   @RBComment("Message used in audit event")
   public static final String EX_CURRENT_USER_IS_NOT_AUTHORIZED = "46";

   @RBEntry("Vendor CAGE Code")
   @RBComment("Label for the Vendor organization for A&D ")
   public static final String VENDORCAGECODELABEL = "47";

   @RBEntry("Vendor ID")
   @RBComment("Label for the Vendor Organization")
   public static final String VENDORORGIDLABEL = "48";

   @RBEntry("The underscore character \"_\" is not allowed in an Internet domain.  Domain was \"{0}\".")
   @RBComment("This is an error message for when a WTPrincipal is created or updated and given an invalid name containing an underscore.")
   public static final String PRINCIPAL_DOMAIN_UNDERSCORE = "49";

   @RBEntry("Label for OrgID")
   public static final String PRIVATE_CONSTANT_1 = "ORGID_RESOURCE_CONST";

   @RBEntry("This is a Resource Bundle constant definition in orgResource which is evaluated and displayed as the label for the Organization. Holds the actual number associated with the constant definition in orgResource.")
   public static final String PRIVATE_CONSTANT_2 = "ORGID_RESOURCE_DESC";

   @RBEntry("Label for Manufacturer ID")
   public static final String PRIVATE_CONSTANT_3 = "MFGID_RESOURCE_CONST";

   @RBEntry("This is a Resource Bundle constant definition in orgResource which is evaluated and displayed as the label for the Manufacturing Organization of a part instance/Manufacturer Part. Holds the actual number associated with the constant definition in orgResource.")
   public static final String PRIVATE_CONSTANT_4 = "MFGID_RESOURCE_DESC";

   @RBEntry("Label for Vendor ID")
   public static final String PRIVATE_CONSTANT_5 = "VENDORID_RESOURCE_CONST";

   @RBEntry("This is a Resource Bundle constant definition in orgResource which is evaluated and displayed as the label for the Vendor Organization of a part instance/Vendor Part. Holds the actual number associated with the constant definition in orgResource.")
   public static final String PRIVATE_CONSTANT_6 = "VENDORID_RESOURCE_DESC";

   @RBEntry("Expose Organization")
   public static final String PRIVATE_CONSTANT_7 = "DISPLAY_ORGID";

   @RBEntry("Expose the organization for all objects.  If Yes, the organization will be exposed for all change management objects provided the preference, Expose Organization for Change Management Objects, is also set to Yes.")
   public static final String PRIVATE_CONSTANT_8 = "DISPLAY_ORGID_DESC";

   @RBEntry("Expose Organization for Change Management Objects")
   public static final String PRIVATE_CONSTANT_9 = "DISPLAY_CHANGE_MGMT_ORGID";

   @RBEntry("Expose the organization for all change management objects.  If Yes, the organization will be exposed for all change management objects provided the preference, Expose Organization, is also set to Yes.")
   public static final String PRIVATE_CONSTANT_10 = "DISPLAY_CHANGE_MGMT_ORGID_DESC";

   @RBEntry("Display type as icon or display type as text based on this preference.")
   public static final String PRIVATE_CONSTANT_11 = "DISPLAY_TYPE_ICON";

   @RBEntry("If true display type as icon otherwise display type as text based on the types display identity. Defaults to true. Hidden from user.")
   public static final String PRIVATE_CONSTANT_12 = "DISPLAY_TYPE_ICON_DESC";

   @RBEntry("Synchronize Domains for User Organization Changes")
   public static final String PRIVATE_CONSTANT_13 = "CHANGE_DOMAIN";

   @RBEntry("When this preference is set to true and the organization of a user changes, the user domain and the domain of the users personal cabinet are reassigned to the new organization root domain.  Additionally, the organization groups that are associated with the context teams for which the user is a member are updated.  The user is removed from the group for the old organization and added to the group for the new organization.")
   public static final String PRIVATE_CONSTANT_14 = "CHANGE_DOMAIN_SHORT_DESC";

   @RBEntry("True by default.  When this preference is set to true and the organization of a user changes, the user domain and the domain of the users personal cabinet are reassigned to the new organization root domain.  Additionally, the organization groups that are associated with the context teams for which the user is a member are updated.  The user is removed from the group for the old organization and added to the group for the new organization.")
   public static final String PRIVATE_CONSTANT_15 = "CHANGE_DOMAIN_LONG_DESC";

   @RBEntry("No principal was found for the dn: {0}")
   @RBComment("Message given when trying to modify group membership through a load file, but the dn of the principal is not valid, or cannot be found.")
   @RBArgComment0("The dn generated of the principal which cannot be found")
   public static final String PRINCIPAL_NOT_FOUND_BY_DN = "PRINCIPAL_NOT_FOUND_BY_DN";

   @RBEntry("No group was found for the name: {0} in the adapter: {1}")
   @RBComment("Message given when trying to query for a group through a load file, but the dn of the principal is not valid, or cannot be found in the specified adapter.")
   @RBArgComment0("The name of the group which cannot be found")
   @RBArgComment1("The adapter the principal is being searched in")
   public static final String GROUP_NOT_FOUND_BY_DN_IN_ADAPTER = "GROUP_NOT_FOUND_BY_DN_IN_ADAPTER";

   @RBEntry("More than one group was found with the name: {0} in the adapter: {1}")
   @RBComment("Message given when trying to modify group membership through a load file, and more than one group was found for the principal in the specified adapter.")
   @RBArgComment0("The dn generated of the principal which cannot be found")
   @RBArgComment1("The adapter the principal is being searched in")
   public static final String MULTIPLE_GROUPS_FOUND = "MULTIPLE_GROUPS_FOUND";

   @RBEntry("A configuration problem has been detected. The system is unable to retrieve a valid \"Name\" attribute for the participant with the distinguished name \"{0}\" from the participant's LDAP entry.\n\nContact your administrator.")
   @RBComment("Error message if the system is unable to retrieve any valid value for a principal's name from LDAP")
   @RBArgComment0("Distinguished name of the principal")
   public static final String NAME_MISSING_IN_PRINCIPAL = "NAME_MISSING_IN_PRINCIPAL";

   @RBEntry("Verify that the LDAP attribute value mapping \"{0}\" for the service \"{1}\" is valid.")
   @RBComment("Resolution suggestion if the system is unable to retrieve any valid value for a principal's name from LDAP")
   @RBArgComment0("The LDAP attribute mapping that is incorrect")
   @RBArgComment1("The JNDI Adapter with the incorrect mappings")
   public static final String RESOLUION_FOR_NAME_MISSING_IN_PRINCIPAL_ERROR = "RESOLUION_FOR_NAME_MISSING_IN_PRINCIPAL_ERROR";

   /**
    * Labels for WTRolePrincipal types
    **/
   @RBEntry("Context Team Role")
   public static final String PRIVATE_CONSTANT_16 = "CONTEXT_TEAM_ROLE";

   @RBEntry("Organization Role")
   public static final String PRIVATE_CONSTANT_17 = "ORGANIZATION_ROLE";

   @RBEntry(":")
   public static final String PRIVATE_CONSTANT_18 = "LABEL_SEPARATOR";

   @RBEntry("The name {0} is not valid. Participant names cannot contain the following characters: {1}.\nContact your administrator to resolve this issue.")
   @RBComment("Error indicating that an invalid principal name was provided.")
   @RBArgComment0("The name that is invalid.")
   @RBArgComment1("The characters that are invalid.")
   public static final String INVALID_PRINCIPAL_NAME = "INVALID_PRINCIPAL_NAME";

   @RBEntry("The system encountered one or more participants with invalid names. Participant names cannot contain the following characters: {1}.\nContact your administrator to resolve this issue.")
   @RBComment("Error indicating that one or more principals in a collection have invalid names.")
   @RBArgComment0("The characters that are considered invalid.")
   @RBArgComment1("The characters that are invalid.")
   public static final String INVALID_PRINCIPAL_NAMES = "INVALID_PRINCIPAL_NAMES";

   @RBEntry("The name {0} contains characters that are not permitted in user names and is not valid. Contact your administrator to resolve this issue.")
   @RBComment("Error indicating that an invalid user name was provided during login.")
   @RBArgComment0("The name that is invalid.")
   public static final String INVALID_LOGIN_NAME = "INVALID_LOGIN_NAME";

   @RBEntry("This is not a supported picture file. Only GIF and JPG file formats and file sizes smaller than 100KB are supported.")
   @RBComment("This error message is displayed if the picture file that is provided for associating with a user's profile, fails validation checks.")
   public static final String USER_PHOTO_INVALID = "50";

   @RBEntry("Allow Users to Edit their own Picture")
   @RBComment("Name of the preference that determines whether users are allowed to edit their profile picture")
   public static final String USER_PHOTO_UPDATE_PERMITTED_PREF = "USER_PHOTO_UPDATE_PERMITTED_PREF";

   @RBEntry("Determines if users will be permitted to edit their own picture")
   @RBComment("Short description for the preference that determines whether users are allowed to edit their own picture")
   public static final String USER_PHOTO_UPDATE_PERMITTED_PREF_SHORT_DESC = "USER_PHOTO_UPDATE_PERMITTED_PREF_SHORT_DESC";

   @RBEntry("Determines if users will be permitted to edit their own picture. If set to yes, users will have access to the action that allows them to edit their own picture. If set to no, the edit picture action will not be available to individual users. In either case, administrators will have the ability to assign pictures for users.")
   @RBComment("Description for the preference that determines whether users are allowed to edit their profile picture")
   public static final String USER_PHOTO_UPDATE_PERMITTED_PREF_DESC = "USER_PHOTO_UPDATE_PERMITTED_PREF_DESC";

   @RBEntry("Password can only contain ASCII characters (english letters, numbers, and basic symbols) as per your site's configuration. Modify the password and retry.")
   @RBComment("This error message is displayed the site has a setting to disallow use of non US-ACII characters in user password")
   public static final String USER_PASSWORD_RESTRICT_CHAR_SET_MSG = "USER_PASSWORD_RESTRICT_CHAR_SET_MSG";

   /**
    * Messages for principal repair notifications
    */
   @RBEntry("The principal is missing the Remote information in the database")
   public static final String MISSING_REMOTE_INFORMATION = "51";

   @RBEntry("The principal's domain information in the database does not have a corresponding service associated")
   public static final String MISSING_SERVICE = "52";

   @RBEntry("The principal is missing in LDAP")
   public static final String MISSING_IN_LDAP = "53";

   @RBEntry("Cannot process participant {0} because a related disconnected participant {1} exists in the database")
   @RBComment("Error indicates that the current participant or another participant with the same name is disconnected")
   @RBArgComment0("The participant name that is being queried")
   @RBArgComment1("The related disconnected participant oid.")
   public static final String PRINCIPAL_IS_DISCONNECTED = "54";

   @RBEntry("An Organization with the identifier {0} is already associated to the coding system {1}. Choose a different Organization Identifier.")
   @RBComment("Error indicating a duplicate Organization cannot be created with the same combination of Identifier and Coding System.")
   @RBArgComment0("Organization Identifier that is not unique.")
   @RBArgComment1("Organization Coding System")
   public static final String ORGANIZATION_IDENTIFIER_NOT_UNIQUE = "55";

   /**
    * Message for License Group Deletion
    */
   @RBEntry("Cannot delete license group.")
   @RBComment("Message stating that external group cannot be deleted.")
   public static final String CANT_DELETE_LICENSEGROUP = "56";
   
   @RBEntry("The \"{0}\" parameter value \"{1}\" is invalid.")
   @RBComment("WTInvalidParameterException: An invalid parameter value is passed to a method.")
   @RBArgComment0("Name of parameter")
   @RBArgComment1("Value of parameter that is invalid")
   public static final String INVALID_PARAMETER = "57";
   
   @RBEntry("Cannot process participant {0} because a related disconnected participant {1} exists in the database, and the User Automatic Reconnect preference is {2}")
   @RBComment("Error indicates that the current participant or another participant with the same name is disconnected and automatic reconnect for user is disabled/immediate/enable")
   @RBArgComment0("The participant name that is being queried")
   @RBArgComment1("The related disconnected participant oid.")
   @RBArgComment2("The value of User Automatic Reconnect preference")
   public static final String PRINCIPAL_IS_DISCONNECTED_AND_RECONNECT_DISABLED = "58";
   
   /**
    * Preference descriptions for Auto Reconnect
    */
   @RBEntry("User Automatic Reconnect")
   @RBComment("Name of the preference that determines whether user automatic reconnect is enabled or disabled or immediate")
   public static final String USER_AUTORECONNECT_PREF = "USER_AUTORECONNECT_PREF";

   @RBEntry("Controls Automatic Reconnect behavior of a disconnected user")
   @RBComment("Short description for the preference that determines whether user reconnect is enabled or disabled or immediate")
   public static final String USER_AUTORECONNECT_PREF_SHORT_DESC = "USER_AUTORECONNECT_PREF_SHORT_DESC";

   @RBEntry("Determines if a disconnected user will be automatically reconnected with his or her LDAP entry. If set to Enable, the disconnected user is automatically reconnected if they log into Windchill, if an administrator uses the Search Disconnected Participants action. If set to Disable, the disconnected user needs to be reconnected manually using the Reconnect Disconnected Participant action. If set to Immediate, disconnected users are automatically reconnected the first time a reconnection is attempted. If the reconnection attempt is not successful, the disconnected user must be manually reconnected.")
   @RBComment("Description for the preference that determines whether users reconnect is enabled or disabled or immediate")
   public static final String USER_AUTORECONNECT_PREF_DESC = "USER_AUTORECONNECT_PREF_DESC";
   
   /**
    *  list for Preferences for Auto Reconnect
    */
   @RBEntry("Enable")
   @RBComment("Enables AutoReconnect for Users")
   public static final String USER_AUTORECONNECT_ENABLE = "USER_AUTORECONNECT_ENABLE";
   
   @RBEntry("Disable")
   @RBComment("Disable AutoReconnect for Users")
   public static final String USER_AUTORECONNECT_DISABLE = "USER_AUTORECONNECT_DISABLE";
   
   @RBEntry("Immediate")
   @RBComment("AutoReconnects only Users that have repairNeeded flag set to false")
   public static final String USER_AUTORECONNECT_IMMEDIATE = "USER_AUTORECONNECT_IMMEDIATE";
   
   
}
