/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.org;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBArgComment1;
import wt.util.resource.RBArgComment2;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.org.orgResource")
public final class orgResource_it extends WTListResourceBundle {
   @RBEntry("L'operazione \"{0}\" non è riuscita.")
   @RBComment("Message specifying an operation that failed.")
   @RBArgComment0("Operation that failed")
   public static final String OPERATION_FAILED = "0";

   @RBEntry("Il nome \"{0}\" non è univoco.")
   @RBComment("Message specifying a name that is not unique.")
   @RBArgComment0("Name that is not unique")
   public static final String NAME_NOT_UNIQUE = "1";

   @RBEntry("Impossibile eliminare l'accesso all'amministratore.")
   @RBComment("Message stating that specified user cannot be deleted, i.e. default administrator.")
   @RBArgComment0("User which cannot be deleted(admin)")
   public static final String CANT_DELETE_ADMIN = "2";

   @RBEntry("Impossibile modificare il nome dell'utente/gruppo/ruolo predefinita: \"{0}\".")
   @RBComment("Message stating that the specified name cannot be changed.")
   @RBArgComment0("pre-defined principal to which a name change was attempted")
   public static final String CANT_CHANGE_NAME = "3";

   @RBEntry("È richiesta la classe WTPrincipal.")
   @RBComment("Message stating that the class is not a WTPrincipal class.")
   public static final String WRONG_CLASS = "4";

   @RBEntry("Tentativo di modificare la directory LDAP esterna.  Utilizzare gli strumenti della directory LDAP esterna per apportare modifiche ai dati.")
   public static final String LDAP_MODIFY = "5";

   @RBEntry("Il nome \"{0}\" non è stato trovato nella directory LDAP esterna \"{1}\".")
   @RBComment("Message stating that the specified principal name was not found in the specified LDAP directory.")
   @RBArgComment0("principal name that was not found")
   @RBArgComment1("LDAP directory name that does not contain specified principal name")
   public static final String LDAP_NOT_FOUND = "6";

   @RBEntry("La proprietà \"{0}\" non è stata trovata nei file delle proprietà.")
   @RBComment("Message stating that the specified property was not found in the properties files.")
   @RBArgComment0("Property that was not found in properties files")
   public static final String NULL_PROPERTY = "7";

   @RBEntry("L'utente/gruppo/ruolo \"{0}\" non è valido.")
   @RBComment("Message stating that the specified principal is not valid.")
   @RBArgComment0("principal that is not valid")
   public static final String PRINCIPAL_NOT_VALID = "8";

   @RBEntry("Impossibile eliminare gli utenti.")
   @RBComment("Message stating that users cannot be deleted.")
   public static final String CANT_DELETE_USERS = "9";

   @RBEntry("Impossibile rimuovere l'amministratore predefinito dal gruppo degli amministratori.")
   @RBComment("Message reporting that the default administrator cannot be removed from Administrators group--would be used when user tries to remove administrator.")
   public static final String CANT_REMOVE_ADMIN = "10";

   @RBEntry("Nome del server Web per l'utente mancante: \"{0}\".")
   @RBComment("Message stating that the web server name for the specified user could not be found.")
   @RBArgComment0("User for whom a web server name is missing")
   public static final String MISSING_AUTH_NAME = "11";

   @RBEntry("Nome completo dell'utente mancante: \"{0}\".")
   @RBComment("Message stating that the user object has no value in the full name field")
   @RBArgComment0("User for whom a full name is missing")
   public static final String MISSING_FULL_NAME = "12";

   @RBEntry("{0}: eccezione durante il tentativo di ricavare l'indirizzo di posta elettronica per \"{1}\", probabilmente a causa del protocollo LDAP.  L'indirizzo è stato impostato alla stringa vuota.")
   @RBComment("Message specifying that a specified exception was trown trying to retrieve the e mail address from a user object.")
   @RBArgComment0("type of exception")
   @RBArgComment1("user whose address Windchill is trying to get")
   public static final String EMAIL_RETRIEVAL = "13";

   @RBEntry("Impossibile completare gli attributi per l'utente o per il gruppo nell'oggetto.")
   @RBComment("Message stating that an attempt to fill the attributes for a user or group failed.")
   public static final String INFLATE_FAILED = "14";

   @RBEntry("Impossibile impostare il criterio di ricerca \"utente/gruppo = {0}\".")
   @RBComment("Message stating that an attempt to set a specified search criteria to a user or group failed.")
   @RBArgComment0("Criterion to which Windchill was unable to set search")
   public static final String SEARCH_CRITERIA = "15";

   @RBEntry("Il partecipante {0} non esiste più.")
   @RBComment("Message stating that an attempt to find a specified principal failed.")
   @RBArgComment0("Name of principal which could not be found")
   public static final String PRINCIPAL_NOT_FOUND = "16";

   @RBEntry("Impossibile aggiornare il gruppo \"{0}\" poiché è stato eliminato.")
   @RBComment("Message stating that an attempt to update a specified user or group failed.")
   @RBArgComment0("Group Windchill was unable to delete")
   public static final String CANT_UPDATE_DISABLED_GROUP = "17";

   @RBEntry("Sono stati trovati più utenti/gruppi/ruoli di nome \"{0}\".")
   @RBComment("Message stating that more than one principal has the same value for its name field.")
   @RBArgComment0("Name for which multiple instances of principals have")
   public static final String MULTI_DB_HIT = "18";

   @RBEntry("L'account utente a cui si sta tentando di accedere corrisponde a più di un utente. Contattare l'amministratore di sistema.")
   @RBComment("Error message encountered during user authentication if the system finds more than one matching pending or replicated user that can be activated")
   public static final String AMBIGUOUS_RESULTS_IN_USER_AUTHENTICATION = "AMBIGUOUS_RESULTS_IN_USER_AUTHENTICATION";

   @RBEntry("Impossibile creare l'utente {0}\n\nEsistono più utenti in sospeso o replicati che risultano candidati per l'attivazione, in quanto corrispondono a uno o più attributi dell'utente in fase di creazione: {1}\n\nSe non si era intenzionati ad attivare un utente inattivo esistente o se non si prevedevano più corrispondenze, ricreare l'utente con attributi modificati o contattare l'amministratore di sistema.")
   @RBComment("Error message encountered during user creation if the system finds more than one matching pending or replicated user that can be activated")
   @RBArgComment0("Name of the user being created")
   @RBArgComment1("Attributes of the user that match other pending or replicated users")
   public static final String AMBIGUOUS_RESULTS_IN_USER_ACTIVATION = "AMBIGUOUS_RESULTS_IN_USER_ACTIVATION";

   @RBEntry("Si è verificato un errore imprevisto. Il risultato di un'interrogazione interna ha restituito più partecipanti quando ne era previsto solo uno. Contattare l'amministratore.")
   @RBComment("Error message used in conjunction with exception scenarios where a query produces ambiguous results")
   public static final String AMBIGUOUS_RESULTS_IN_INTERNAL_QUERY = "AMBIGUOUS_RESULTS_IN_INTERNAL_QUERY";

   @RBEntry("L'utente/gruppo/ruolo corrispondente al nome distinto \"{0}\" non appartiene al tipo di utente/gruppo/ruolo richiesto \"{1}\".")
   @RBComment("Message stating that more than one user was found in the Pending User Directory Service by a query that was expecting only one result.")
   @RBArgComment0("Distingished name of the principal being searched")
   @RBArgComment1("Type of the principal being searched (user, group or organization)")
   public static final String TYPE_MISMATCH_IN_SEARCHING_BY_DN = "TYPE_MISMATCH_IN_SEARCHING_BY_DN";

   @RBEntry("Sono stati trovati più utenti/gruppi/ruoli di nome \"{0}\" nel servizio \"{1}\".")
   @RBComment("Message stating that more than one principal has the same value for its name field in the same server.")
   @RBArgComment0("Name for which multiple instances of principals have")
   @RBArgComment1("Service name which contains multiple instances of one name")
   public static final String MULTI_SERVICE_HIT = "19";

   @RBEntry("Identità elettronica mancante per l'utente. Vedere Amministrazione Windchill.")
   @RBComment("Message stating that there is no electronic identity for the user.")
   public static final String CONTENT_ITEM_IS_NULL = "20";

   @RBEntry("{0} (eliminato)")
   @RBComment("Display for the name of a user that has been deleted")
   public static final String DELETED_USER = "21";

   @RBEntry("L'aggiunta di {0} a {1} creerebbe circolarità.")
   @RBComment("Can not add a specified member to a group because the member is the group itself or a parent of the group.")
   public static final String CIRCULAR_MEMBERSHIP_PROHIBITED = "22";

   @RBEntry("{0} è già associata ad una voce di directory valida nel servizio  {1} con nome distinto {2}.")
   @RBComment("A specified principal does not need to be repaired because it has a valid association with a directory entry.")
   public static final String NAME_ALREADY_ASSOCIATED = "23";

   @RBEntry("Esistono più utenti/gruppi/ruoli corrispondenti a {0} nel database e nessuno di loro è associato alla voce di directory con nome distinto {1}.")
   public static final String INVALID_NAME_DN_ASSOCIATION = "24";

   @RBEntry("Impossibile creare la voce in LDAP.")
   @RBComment("Can not create LDAP entry.")
   public static final String UNABLE_TO_CREATE_LDAP_ENTRY = "25";

   @RBEntry("Impossibile trovare l'adattatore JNDI per {0}.")
   @RBComment("Can not found JNDIADAPTER for the specified DN.")
   public static final String JNDIADAPTER_NOT_FOUND = "26";

   @RBEntry("{0} non è un indirizzo e-mail valido.")
   @RBComment("Error indicating that an invalid email address was supplied.")
   @RBArgComment0("The email address that is invalid.")
   public static final String INVALID_EMAIL_ADDRESS = "27";

   @RBEntry("Impossibile impostare proprietà: {0}")
   public static final String PROPERTY_NOT_SETTABLE = "28";

   @RBEntry("Errore di sintassi nell'espressione dell'interrogazione: {0}")
   public static final String QUERY_EXPRESSION_SYNTAX_ERROR = "29";

   @RBEntry("Classe oggetto non supportata: {0}")
   public static final String UNSUPPORTED_CLASS = "30";

   @RBEntry("Servizio di elenco inesistente: {0}")
   public static final String NO_SUCH_SERVICE = "31";

   @RBEntry("Sono stati trovati più nodi di infrastruttura corrispondenti al nome \"{0}\".")
   public static final String AMBIGUOUS_NODE_NAME = "32";

   @RBEntry("L'utente/gruppo/ruolo esiste già: {0}")
   public static final String PRINCIPAL_ALREADY_EXISTS = "33";

   @RBEntry("URL non valido: {0}")
   @RBComment("Error indicating that an invalid URL was supplied.")
   @RBArgComment0("The URL that is invalid.")
   public static final String INVALID_URL = "34";

   @RBEntry("Non sono stati trovati utenti/gruppi/ruoli disconnessi denominati \"{0}\".")
   public static final String NO_DISCONNECTED_PRINCIPALS = "35";

   @RBEntry("Codice CAGE")
   @RBComment("Label for the organization id referencing the industry standard name 'CAGE Code'")
   public static final String CAGE_CODE = "CAGE_CODE";

   @RBEntry("ISO 6523")
   @RBComment("Label for the organization id referencing the industry standard name 'ISO 6523'")
   public static final String ISO_6523 = "ISO_6523";

   @RBEntry("Numero DUNS")
   @RBComment("Label for the organization id referencing the industry standard name 'DUNS Number'")
   public static final String DUNS_NUMBER = "DUNS_NUMBER";

   @RBEntry("Codice CAGE")
   @RBComment("Label for the organization for A&D ")
   public static final String CAGECODELABEL = "36";

   @RBEntry("Nuovo codice CAGE")
   @RBComment("Label for New CAGE Code. \"36\" is the value that comes from the preference system")
   public static final String PRIVATE_CONSTANT_0 = "NEW_36";

   @RBEntry("ID organizzazione")
   @RBComment("Label for the organization for PDMLink,ProjectLink")
   public static final String ORGIDLABEL = "37";

   @RBEntry("ID nuova organizzazione")
   @RBComment("Label for New Organization ID. \"37\" is the value that comes from the preference system")
   public static final String NEW_ORGANIZATION_ID = "NEW_37";

   @RBEntry("ID DUNS")
   @RBComment("Label for the organization")
   public static final String DUNSLABEL = "38";

   @RBEntry("Nuovo ID DUNS")
   @RBComment("Label for New Duns ID. \"38\" is the value that comes from the preference system")
   public static final String NEW_DUNSLABEL = "NEW_38";

   @RBEntry("Codice CAGE produttore")
   @RBComment("Label for the Manufacturer organization for A&D ")
   public static final String MFGCAGECODELABEL = "39";

   @RBEntry("ID produttore")
   @RBComment("Label for the Manufacturer organization ")
   public static final String MFGORGIDLABEL = "40";

   @RBEntry("ID DUNS produttore")
   @RBComment("Label for the Manufacturer organization")
   public static final String MFGDUNSLABEL = "41";

   @RBEntry("Codige CAGE assembl.")
   @RBComment("Label for the organization for A&D ")
   public static final String ASSEMBLYCAGECODELABEL = "42";

   @RBEntry("ID organizzazione assembl.")
   @RBComment("Label for the organization for PDMLink,ProjectLink")
   public static final String ASSEMBLYORGIDLABEL = "43";

   @RBEntry("Sito")
   public static final String SITE = "44";

   @RBEntry("Operazione tentata sul servizio di elenco {0} non riuscita. Non è supportata, o l'utente non dispone dei privilegi necessari.")
   public static final String OPERATION_NOT_SUPPORTED = "45";

   @RBEntry("L'utente corrente non è autorizzato ad accedere a \"{0}\".")
   @RBComment("Message used in audit event")
   public static final String EX_CURRENT_USER_IS_NOT_AUTHORIZED = "46";

   @RBEntry("Codice CAGE fornitore")
   @RBComment("Label for the Vendor organization for A&D ")
   public static final String VENDORCAGECODELABEL = "47";

   @RBEntry("ID fornitore")
   @RBComment("Label for the Vendor Organization")
   public static final String VENDORORGIDLABEL = "48";

   @RBEntry("Il carattere di sottolineatura \"_\" non è ammesso nei nomi di dominio Internet. Dominio: \"{0}\".")
   @RBComment("This is an error message for when a WTPrincipal is created or updated and given an invalid name containing an underscore.")
   public static final String PRINCIPAL_DOMAIN_UNDERSCORE = "49";

   @RBEntry("Etichetta per ID org.")
   public static final String PRIVATE_CONSTANT_1 = "ORGID_RESOURCE_CONST";

   @RBEntry("Definizione costante di resource bundle in orgResource che viene valutata e visualizzata come etichetta dell'organizzazione. Contiene il numero effettivo associato alla definizione costante in orgResource.")
   public static final String PRIVATE_CONSTANT_2 = "ORGID_RESOURCE_DESC";

   @RBEntry("Etichetta per ID produttore")
   public static final String PRIVATE_CONSTANT_3 = "MFGID_RESOURCE_CONST";

   @RBEntry("Definizione costante di resource bundle in orgResource che viene valutata e visualizzata come etichetta dell'organizzazione produttore di un'istanza di parte o di una parte produttore. Contiene il numero effettivo associato alla definizione costante in orgResource.")
   public static final String PRIVATE_CONSTANT_4 = "MFGID_RESOURCE_DESC";

   @RBEntry("Etichetta per ID fornitore")
   public static final String PRIVATE_CONSTANT_5 = "VENDORID_RESOURCE_CONST";

   @RBEntry("Definizione costante di resource bundle in orgResource che viene valutata e visualizzata come etichetta dell'organizzazione fornitore di un'istanza di parte o di una parte fornitore. Contiene il numero effettivo associato alla definizione costante in orgResource.")
   public static final String PRIVATE_CONSTANT_6 = "VENDORID_RESOURCE_DESC";

   @RBEntry("Rendi organizzazione visibile")
   public static final String PRIVATE_CONSTANT_7 = "DISPLAY_ORGID";

   @RBEntry("Rende l'organizzazione visibile per tutti gli oggetti. Se Sì, l'organizzazione è visibile per tutti gli oggetti di Gestione modifiche a condizione che anche la preferenza Rendi organizzazione visibile per gli oggetti di Gestione modifiche sia impostata su Sì.")
   public static final String PRIVATE_CONSTANT_8 = "DISPLAY_ORGID_DESC";

   @RBEntry("Rendi organizzazione visibile per gli oggetti di Gestione modifiche")
   public static final String PRIVATE_CONSTANT_9 = "DISPLAY_CHANGE_MGMT_ORGID";

   @RBEntry("Rende l'organizzazione visibile per tutti gli oggetti di Gestione modifiche. Se Sì, l'organizzazione è visibile per tutti gli oggetti di Gestione modifiche a condizione che anche la preferenza Rendi organizzazione visibile sia impostata su Sì.")
   public static final String PRIVATE_CONSTANT_10 = "DISPLAY_CHANGE_MGMT_ORGID_DESC";

   @RBEntry("Visualizza il tipo come icona o come testo in base alla preferenza.")
   public static final String PRIVATE_CONSTANT_11 = "DISPLAY_TYPE_ICON";

   @RBEntry("Se è impostata su true, il tipo viene visualizzato come icona, altrimenti come testo in base all'identità visualizzata del tipo. L'impostazione di default è true. Questa opzione non è visibile agli utenti.")
   public static final String PRIVATE_CONSTANT_12 = "DISPLAY_TYPE_ICON_DESC";

   @RBEntry("Sincronizza i domini in seguito a modifiche dell'organizzazione utente")
   public static final String PRIVATE_CONSTANT_13 = "CHANGE_DOMAIN";

   @RBEntry("Quando la preferenza è impostata su True e viene modificata l'organizzazione per un dato utente, il dominio dell'utente e il relativo schedario personale vengono riassegnati al dominio root della nuova organizzazione. Inoltre, vengono aggiornati i gruppi di organizzazione associati ai team di contesto dei quali l'utente è membro. L'utente viene rimosso dal gruppo della vecchia organizzazione e aggiunto a quello della nuova.")
   public static final String PRIVATE_CONSTANT_14 = "CHANGE_DOMAIN_SHORT_DESC";

   @RBEntry("Il default è True. Quando la preferenza è impostata su True e viene modificata l'organizzazione per un dato utente, il dominio dell'utente e il relativo schedario personale vengono riassegnati al dominio root della nuova organizzazione. Inoltre, vengono aggiornati i gruppi di organizzazione associati ai team di contesto dei quali l'utente è membro. L'utente viene rimosso dal gruppo della vecchia organizzazione e aggiunto a quello della nuova.")
   public static final String PRIVATE_CONSTANT_15 = "CHANGE_DOMAIN_LONG_DESC";

   @RBEntry("Nessun utente/gruppo/ruolo trovato per il nome distinto: {0}")
   @RBComment("Message given when trying to modify group membership through a load file, but the dn of the principal is not valid, or cannot be found.")
   @RBArgComment0("The dn generated of the principal which cannot be found")
   public static final String PRINCIPAL_NOT_FOUND_BY_DN = "PRINCIPAL_NOT_FOUND_BY_DN";

   @RBEntry("Nessun gruppo trovato con il nome {0} nell'adattatore {1}")
   @RBComment("Message given when trying to query for a group through a load file, but the dn of the principal is not valid, or cannot be found in the specified adapter.")
   @RBArgComment0("The name of the group which cannot be found")
   @RBArgComment1("The adapter the principal is being searched in")
   public static final String GROUP_NOT_FOUND_BY_DN_IN_ADAPTER = "GROUP_NOT_FOUND_BY_DN_IN_ADAPTER";

   @RBEntry("Più di un gruppo trovato con il nome {0} nell'adattatore {1}")
   @RBComment("Message given when trying to modify group membership through a load file, and more than one group was found for the principal in the specified adapter.")
   @RBArgComment0("The dn generated of the principal which cannot be found")
   @RBArgComment1("The adapter the principal is being searched in")
   public static final String MULTIPLE_GROUPS_FOUND = "MULTIPLE_GROUPS_FOUND";

   @RBEntry("È stato rilevato un problema di configurazione. Il sistema non è in grado di recuperare un attributo \"Name\" valido per il partecipante con il nome distinto \"{0}\" dalla voce LDPA del partecipante.\n\nContattare l'amministratore.")
   @RBComment("Error message if the system is unable to retrieve any valid value for a principal's name from LDAP")
   @RBArgComment0("Distinguished name of the principal")
   public static final String NAME_MISSING_IN_PRINCIPAL = "NAME_MISSING_IN_PRINCIPAL";

   @RBEntry("Verificare che la mappatura del valore dell'attributo LDAP \"{0}\" del servizio \"{1}\" sia valida.")
   @RBComment("Resolution suggestion if the system is unable to retrieve any valid value for a principal's name from LDAP")
   @RBArgComment0("The LDAP attribute mapping that is incorrect")
   @RBArgComment1("The JNDI Adapter with the incorrect mappings")
   public static final String RESOLUION_FOR_NAME_MISSING_IN_PRINCIPAL_ERROR = "RESOLUION_FOR_NAME_MISSING_IN_PRINCIPAL_ERROR";

   /**
    * Labels for WTRolePrincipal types
    **/
   @RBEntry("Ruolo team contesto")
   public static final String PRIVATE_CONSTANT_16 = "CONTEXT_TEAM_ROLE";

   @RBEntry("Ruolo organizzazione")
   public static final String PRIVATE_CONSTANT_17 = "ORGANIZATION_ROLE";

   @RBEntry(":")
   public static final String PRIVATE_CONSTANT_18 = "LABEL_SEPARATOR";

   @RBEntry("Il nome {0} non è valido. I nomi dei partecipanti non possono contenere i seguenti caratteri: {1}.\nContattare l'amministratore per risolvere il problema.")
   @RBComment("Error indicating that an invalid principal name was provided.")
   @RBArgComment0("The name that is invalid.")
   @RBArgComment1("The characters that are invalid.")
   public static final String INVALID_PRINCIPAL_NAME = "INVALID_PRINCIPAL_NAME";

   @RBEntry("I nomi di uno o più partecipanti non sono validi. I nomi dei partecipanti non possono contenere i seguenti caratteri: {1}.\nContattare l'amministratore per risolvere il problema.")
   @RBComment("Error indicating that one or more principals in a collection have invalid names.")
   @RBArgComment0("The characters that are considered invalid.")
   @RBArgComment1("The characters that are invalid.")
   public static final String INVALID_PRINCIPAL_NAMES = "INVALID_PRINCIPAL_NAMES";

   @RBEntry("Il nome {0} contiene caratteri non consentiti per i nomi utenti e non è valido. Contattare l'amministratore per risolvere il problema.")
   @RBComment("Error indicating that an invalid user name was provided during login.")
   @RBArgComment0("The name that is invalid.")
   public static final String INVALID_LOGIN_NAME = "INVALID_LOGIN_NAME";

   @RBEntry("File immagine non supportato. Si possono utilizzare solo file in formato GIF o JPG e di dimensioni inferiori a 100 KB.")
   @RBComment("This error message is displayed if the picture file that is provided for associating with a user's profile, fails validation checks.")
   public static final String USER_PHOTO_INVALID = "50";

   @RBEntry("Consenti modifica propria foto")
   @RBComment("Name of the preference that determines whether users are allowed to edit their profile picture")
   public static final String USER_PHOTO_UPDATE_PERMITTED_PREF = "USER_PHOTO_UPDATE_PERMITTED_PREF";

   @RBEntry("Determina se agli utenti è consentito modificare la propria foto")
   @RBComment("Short description for the preference that determines whether users are allowed to edit their own picture")
   public static final String USER_PHOTO_UPDATE_PERMITTED_PREF_SHORT_DESC = "USER_PHOTO_UPDATE_PERMITTED_PREF_SHORT_DESC";

   @RBEntry("Determina se agli utenti è consentito modificare la propria foto. Se la preferenza è impostata su Sì, gli utenti avranno accesso all'azione che consente di modificare la foto. Se la preferenza è impostata su No, l'azione di modifica della foto non sarà disponibile ai singoli utenti. In entrambi i casi, gli amministratori hanno la possibilità di assegnare foto o immagini per gli utenti.")
   @RBComment("Description for the preference that determines whether users are allowed to edit their profile picture")
   public static final String USER_PHOTO_UPDATE_PERMITTED_PREF_DESC = "USER_PHOTO_UPDATE_PERMITTED_PREF_DESC";

   @RBEntry("La password può contenere solo caratteri ASCII (lettere dell'alfabeto latino, numeri e simboli di base) in base alla configurazione del sito. Modificare la password e riprovare.")
   @RBComment("This error message is displayed the site has a setting to disallow use of non US-ACII characters in user password")
   public static final String USER_PASSWORD_RESTRICT_CHAR_SET_MSG = "USER_PASSWORD_RESTRICT_CHAR_SET_MSG";

   /**
    * Messages for principal repair notifications
    */
   @RBEntry("Utente/gruppo/ruolo mancante nelle informazioni remote del database")
   public static final String MISSING_REMOTE_INFORMATION = "51";

   @RBEntry("Per le informazioni di dominio nel database dell'utente/gruppo/ruolo non è disponibile un servizio associato corrispondente")
   public static final String MISSING_SERVICE = "52";

   @RBEntry("Utente/gruppo/ruolo mancante in LDAP")
   public static final String MISSING_IN_LDAP = "53";

   @RBEntry("Impossibile elaborare il partecipante {0} perché nel database esiste un partecipante disconnesso correlato {1}")
   @RBComment("Error indicates that the current participant or another participant with the same name is disconnected")
   @RBArgComment0("The participant name that is being queried")
   @RBArgComment1("The related disconnected participant oid.")
   public static final String PRINCIPAL_IS_DISCONNECTED = "54";

   @RBEntry("Un'organizzazione con identificatore {0} è già associato al sistema di codifica {1}. Scegliere un identificatore organizzazione diverso.")
   @RBComment("Error indicating a duplicate Organization cannot be created with the same combination of Identifier and Coding System.")
   @RBArgComment0("Organization Identifier that is not unique.")
   @RBArgComment1("Organization Coding System")
   public static final String ORGANIZATION_IDENTIFIER_NOT_UNIQUE = "55";

   /**
    * Message for License Group Deletion
    */
   @RBEntry("Impossibile eliminare il gruppo licenze.")
   @RBComment("Message stating that external group cannot be deleted.")
   public static final String CANT_DELETE_LICENSEGROUP = "56";
   
   @RBEntry("Il valore \"{1}\" del parametro \"{0}\" non è valido.")
   @RBComment("WTInvalidParameterException: An invalid parameter value is passed to a method.")
   @RBArgComment0("Name of parameter")
   @RBArgComment1("Value of parameter that is invalid")
   public static final String INVALID_PARAMETER = "57";
   
   @RBEntry("Impossibile elaborare il partecipante {0}, perché nel database esiste un partecipante disconnesso correlato {1} e la preferenza Riconnessione automatica utente è impostata su {2}")
   @RBComment("Error indicates that the current participant or another participant with the same name is disconnected and automatic reconnect for user is disabled/immediate/enable")
   @RBArgComment0("The participant name that is being queried")
   @RBArgComment1("The related disconnected participant oid.")
   @RBArgComment2("The value of User Automatic Reconnect preference")
   public static final String PRINCIPAL_IS_DISCONNECTED_AND_RECONNECT_DISABLED = "58";
   
   /**
    * Preference descriptions for Auto Reconnect
    */
   @RBEntry("Riconnessione automatica utente")
   @RBComment("Name of the preference that determines whether user automatic reconnect is enabled or disabled or immediate")
   public static final String USER_AUTORECONNECT_PREF = "USER_AUTORECONNECT_PREF";

   @RBEntry("Controlla la funzionalità di riconnessione automatica di un utente disconnesso")
   @RBComment("Short description for the preference that determines whether user reconnect is enabled or disabled or immediate")
   public static final String USER_AUTORECONNECT_PREF_SHORT_DESC = "USER_AUTORECONNECT_PREF_SHORT_DESC";

   @RBEntry("Determina se un utente disconnesso viene riconnesso automaticamente con la relativa voce LDAP. Se la preferenza è impostata su Attiva, l'utente disconnesso viene riconnesso automaticamente quando accede a Windchill se l'amministratore utilizza the l'azione Cerca partecipanti disconnessi. Se la preferenza è impostata su Disattiva, l'utente disconnesso deve connettersi manualmente tramite l'azione Riconnetti partecipante disconnesso. Se la preferenza è impostata su Immediata, gli utenti disconnessi vengono riconnessi automaticamente al primo tentativo di riconnessione. Se il tentativo di riconnessione ha esito negativo, l'utente disconnesso deve riconnettersi manualmente.")
   @RBComment("Description for the preference that determines whether users reconnect is enabled or disabled or immediate")
   public static final String USER_AUTORECONNECT_PREF_DESC = "USER_AUTORECONNECT_PREF_DESC";
   
   /**
    *  list for Preferences for Auto Reconnect
    */
   @RBEntry("Attiva")
   @RBComment("Enables AutoReconnect for Users")
   public static final String USER_AUTORECONNECT_ENABLE = "USER_AUTORECONNECT_ENABLE";
   
   @RBEntry("Disattiva")
   @RBComment("Disable AutoReconnect for Users")
   public static final String USER_AUTORECONNECT_DISABLE = "USER_AUTORECONNECT_DISABLE";
   
   @RBEntry("Immediata")
   @RBComment("AutoReconnects only Users that have repairNeeded flag set to false")
   public static final String USER_AUTORECONNECT_IMMEDIATE = "USER_AUTORECONNECT_IMMEDIATE";
   
   
}
