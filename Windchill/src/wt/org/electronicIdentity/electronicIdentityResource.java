/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.org.electronicIdentity;

import wt.util.resource.*;

@RBUUID("wt.org.electronicIdentity.electronicIdentityResource")
public final class electronicIdentityResource extends WTListResourceBundle {
   @RBEntry("The object is not Electronically Signable.")
   public static final String OBJECT_NOT_ELECTRONICALLY_SIGNABLE = "0";

   @RBEntry("The value passed for parameter - \"{0}\" was null.")
   public static final String NULL_PARAMETER = "1";

   @RBEntry("Signatures")
   public static final String ELECTRONIC_SIGNATURES = "2";

   @RBEntry("{1} was not electronically signed, because {0} has no electronic identification.")
   public static final String WTUSER_HAS_NO_SIGNATURE = "3";

   @RBEntry("Signature")
   public static final String SIGNATURE = "4";

   @RBEntry("**No Results**")
   public static final String NO_RESULTS = "5";

   /**
    * These below String is being externalized from wt/workflow/htmltmpl/NotificationRobot.html
    **/
   @RBEntry("Signatures")
   public static final String SIG = "6";

   @RBEntry("The \"{0}\" parameter value \"{1}\" is invalid.")
   @RBComment("WTInvalidParameterException: An invalid parameter value is passed to a method.")
   @RBArgComment0("Name of parameter")
   @RBArgComment1("Value of parameter that is invalid")
   public static final String INVALID_PARAMETER = "7";

   /**
    * moved from clients.login.loginRB
    **/
   @RBEntry("Login Failed -- Unknown Web Id")
   public static final String WEB_AUTH_FAILED = "14";
}
