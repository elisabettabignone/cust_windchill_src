/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.org.electronicIdentity.engines;

import wt.util.resource.*;

@RBUUID("wt.org.electronicIdentity.engines.EnginesRB")
public final class EnginesRB_it extends WTListResourceBundle {
   @RBEntry("Password")
   public static final String PASSWORD = "1";

   @RBEntry("La password per la firma non è valida.")
   public static final String INVALID = "2";

   @RBEntry("Per completare il task è necessaria un'ulteriore verifica.")
   public static final String VERIFICATION_REQUIRED = "3";

   @RBEntry("Per iniziare le attività ad hoc è necessaria una verifica supplementare.")
   public static final String ADHOC_VERIFICATION_REQUIRED = "4";

   @RBEntry("Nome utente")
   public static final String USERNAME = "5";

   @RBEntry("Nome utente non corrispondente all'utente a cui è assegnato il task. Solo l'utente assegnatario può completare il task.")
   public static final String USERNAME_INVALID = "6";

   @RBEntry("La combinazione di nome utente e password immessa non è valida.")
   public static final String USERNAME_PASSWORD_INVALID = "7";
}
