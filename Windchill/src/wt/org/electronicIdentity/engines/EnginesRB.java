/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.org.electronicIdentity.engines;

import wt.util.resource.*;

@RBUUID("wt.org.electronicIdentity.engines.EnginesRB")
public final class EnginesRB extends WTListResourceBundle {
   @RBEntry("Password")
   public static final String PASSWORD = "1";

   @RBEntry("Password for signature is invalid.")
   public static final String INVALID = "2";

   @RBEntry("Additional verification is required to complete this task.")
   public static final String VERIFICATION_REQUIRED = "3";

   @RBEntry("Additional verification is required to start the ad hoc activities.")
   public static final String ADHOC_VERIFICATION_REQUIRED = "4";

   @RBEntry("User Name")
   public static final String USERNAME = "5";

   @RBEntry("The user name entered does not match the user assigned to this task. Only the user assigned to this task can complete it.")
   public static final String USERNAME_INVALID = "6";

   @RBEntry("The user name and password combination entered is not valid.")
   public static final String USERNAME_PASSWORD_INVALID = "7";
}
