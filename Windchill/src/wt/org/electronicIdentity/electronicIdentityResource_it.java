/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.org.electronicIdentity;

import wt.util.resource.*;

@RBUUID("wt.org.electronicIdentity.electronicIdentityResource")
public final class electronicIdentityResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile apporre una firma elettronica all'oggetto.")
   public static final String OBJECT_NOT_ELECTRONICALLY_SIGNABLE = "0";

   @RBEntry("Il valore trasferito per il parametro \"{0}\" era nullo.")
   public static final String NULL_PARAMETER = "1";

   @RBEntry("Firme")
   public static final String ELECTRONIC_SIGNATURES = "2";

   @RBEntry("{1} non è stato firmato elettronicamente perché {0} non dispone di identificazione elettronica.")
   public static final String WTUSER_HAS_NO_SIGNATURE = "3";

   @RBEntry("Firma")
   public static final String SIGNATURE = "4";

   @RBEntry("**Nessun risultato**")
   public static final String NO_RESULTS = "5";

   /**
    * These below String is being externalized from wt/workflow/htmltmpl/NotificationRobot.html
    **/
   @RBEntry("Firme")
   public static final String SIG = "6";

   @RBEntry("Il valore \"{1}\" del parametro \"{0}\" non è valido.")
   @RBComment("WTInvalidParameterException: An invalid parameter value is passed to a method.")
   @RBArgComment0("Name of parameter")
   @RBArgComment1("Value of parameter that is invalid")
   public static final String INVALID_PARAMETER = "7";

   /**
    * moved from clients.login.loginRB
    **/
   @RBEntry("Accesso non riuscito. ID Web sconosciuto")
   public static final String WEB_AUTH_FAILED = "14";
}
