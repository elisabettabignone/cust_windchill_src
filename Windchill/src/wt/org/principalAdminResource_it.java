/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.org;

import wt.util.resource.*;

@RBUUID("wt.org.principalAdminResource")
public final class principalAdminResource_it extends WTListResourceBundle {
   @RBEntry("Guida")
   @RBComment("Label for link to help")
   public static final String HELP = "0";

   @RBEntry("Home")
   @RBComment("Label for link to Windchill home page")
   public static final String HOME = "1";

   @RBEntry("Visualizza utenti e gruppi")
   public static final String VIEW_USERS_AND_GROUPS = "2";

   @RBEntry("Crea utente")
   public static final String CREATE_USER = "3";

   @RBEntry("Crea gruppo")
   public static final String CREATE_GROUP = "4";

   @RBEntry("Modifica gruppi di appartenenza")
   public static final String EDIT_GROUP_MEMBERSHIP = "5";

   @RBEntry("Gestione cache")
   public static final String CACHE_MANAGEMENT = "6";

   @RBEntry("Aggiorna gruppo")
   public static final String UPDATE_GROUP_TITLE = "7";

   @RBEntry("Sistema Windchill:")
   public static final String WINDCHILL_SYSTEM_LABEL = "8";

   @RBEntry("Nome:")
   public static final String NAME_LABEL = "9";

   @RBEntry("Posizione:")
   public static final String LOCATION_LABEL = "10";

   @RBEntry("Descrizione:")
   public static final String DESCRIPTION_LABEL = "11";

   @RBEntry("Membri:")
   public static final String MEMBERS_LABEL = "12";

   @RBEntry("Gruppi padre:")
   public static final String PARENT_GROUPS_LABEL = "13";

   @RBEntry("Dominio dell'utente:")
   public static final String USER_DOMAIN_LABEL = "14";

   @RBEntry("OK")
   public static final String OK = "15";

   @RBEntry("Reimposta")
   public static final String RESET = "16";

   @RBEntry("(nessuno)")
   public static final String NONE = "17";

   @RBEntry("Aggiorna utente")
   public static final String UPDATE_USER_TITLE = "18";

   @RBEntry("Salva nuovo utente")
   public static final String SAVE_USER_AS_TITLE = "19";

   @RBEntry("ID utente:")
   public static final String USER_ID_LABEL = "20";

   @RBEntry("Nome completo:")
   public static final String FULL_NAME_LABEL = "21";

   @RBEntry("Indirizzo e-mail:")
   public static final String EMAIL_ADDRESS_LABEL = "22";

   @RBEntry("Lingua preferita:")
   public static final String LANGUAGE_LABEL = "23";

   @RBEntry("Indirizzo postale:")
   public static final String POSTAL_ADDRESS_LABEL = "24";

   @RBEntry("Password:")
   public static final String PASSWORD_LABEL = "25";

   @RBEntry("Conferma password:")
   public static final String CONFIRM_PASSWORD_LABEL = "26";

   @RBEntry("Le password non corrispondono.")
   public static final String PASSWORD_MISMATCH = "27";

   @RBEntry("Trova")
   @RBComment("Verb for querying users and groups")
   public static final String FIND = "28";

   @RBEntry("Utenti")
   @RBComment("The type of principal to be queried")
   public static final String USERS = "29";

   @RBEntry("Gruppi")
   @RBComment("The type of principal to be queried")
   public static final String GROUPS = "30";

   @RBEntry("in")
   public static final String IN = "31";

   @RBEntry("il cui nome contenga")
   public static final String WHOSE_NAME_CONTAINS = "32";

   @RBEntry("Cerca")
   @RBComment("search button label")
   public static final String SEARCH = "33";

   @RBEntry("Per il sistema Windchill:")
   public static final String FOR_WINDCHILL_SYSTEM_LABEL = "34";

   @RBEntry("Crea il gruppo qui:")
   public static final String CREATE_GROUP_HERE_LABEL = "35";

   @RBEntry("Associa al sistema Windchill:")
   public static final String ASSOCIATE_WITH_WINDCHILL_SYSTEM_LABEL = "36";

   @RBEntry("Valore mancante in campo obbligatorio.")
   public static final String REQUIRED_FIELD_MISSING = "37";

   @RBEntry("Crea l'utente qui:")
   public static final String CREATE_USER_HERE_LABEL = "38";

   @RBEntry("Modifica i gruppi di appartenenza per ")
   public static final String EDIT_GROUP_MEMBERSHIP_FOR = "39";

   @RBEntry("Rimuovi")
   @RBComment("Label for remove button")
   public static final String REMOVE = "40";

   @RBEntry("(nessun membro)")
   public static final String NO_MEMBERS = "41";

   @RBEntry("ID utente")
   public static final String USER_ID = "42";

   @RBEntry("Nome completo")
   public static final String FULL_NAME = "43";

   @RBEntry("Azioni")
   public static final String ACTIONS = "44";

   @RBEntry("Nome")
   public static final String NAME = "45";

   @RBEntry("Descrizione")
   public static final String DESCRIPTION = "46";

   @RBEntry("Nessuna corrispondenza")
   public static final String NO_MATCHES = "47";

   @RBEntry("Elimina")
   public static final String DELETE = "48";

   @RBEntry("Aggiungi al gruppo")
   public static final String ADD_TO_GROUP = "49";

   @RBEntry("Elimina contenuto cache utente/gruppo/ruolo")
   public static final String PURGE_PRINCIPAL_CACHE = "50";

   @RBEntry("(tutti i dati)")
   public static final String ALL_ENTRIES = "51";

   @RBEntry("Pulisce la cache da tutti i dati associati al servizio:")
   public static final String PURGE_ALL_ENTRIES_LABEL = "52";

   @RBEntry("Elimina solo i dati cache per l'utente/gruppo:")
   public static final String PURGE_SINGLE_ENTRY_LABEL = "53";

   @RBEntry("Dominio del gruppo:")
   public static final String GROUP_DOMAIN_LABEL = "54";

   @RBEntry("Dominio dello schedario personale:")
   public static final String CABINET_DOMAIN_LABEL = "55";

   @RBEntry("Posizione")
   public static final String DIRECTORY_SERVICE = "56";

   @RBEntry("Membro")
   public static final String MEMBER = "57";

   @RBEntry("Padre")
   public static final String PARENT = "58";

   @RBEntry("Gruppo")
   public static final String GROUP = "59";

   @RBEntry("Errore riportato da Info*Engine")
   public static final String INFO_ENGINE_EXCEPTION = "60";

   @RBEntry("Errore temporaneo riportato da Info*Engine")
   public static final String INFO_ENGINE_TEMPORARY_EXCEPTION = "61";

   @RBEntry("Errore riportato da Windchill")
   public static final String WINDCHILL_EXCEPTION = "62";

   @RBEntry("Report errori")
   public static final String GENERIC_EXCEPTION = "63";

   @RBEntry("Aggiornamento identificazione elettronica in corso per")
   public static final String ELECTRONIC_ID_TITLE = "64";

   @RBEntry("Aggiornamento dominio amministrativo per {0}")
   @RBArgComment0("Name of user or group")
   public static final String DOMAIN_BROWSER_TITLE = "65";

   @RBEntry("nuovo utente")
   public static final String NEW_USER_LABEL = "66";

   @RBEntry("schedario nuovo utente")
   public static final String NEW_CABINET_LABEL = "67";

   @RBEntry("nuovo gruppo")
   public static final String NEW_GROUP_LABEL = "68";

   @RBEntry("Sfoglia...")
   public static final String CHANGE_LABEL = "69";

   @RBEntry("Seleziona tutto")
   public static final String SELECT_ALL_LABEL = "70";

   @RBEntry("Modifica id elettronico")
   public static final String EDIT_ELECTRONIC_ID_LABEL = "71";

   @RBEntry("Modifica i gruppi padre")
   public static final String EDIT_PARENT_GROUPS_LABEL = "72";

   @RBEntry("Aggiornamento dei gruppi padre per ")
   public static final String UPDATE_PARENTS_FOR = "73";

   @RBEntry("(nessun gruppo padre)")
   public static final String NO_PARENTS = "74";

   @RBEntry("oppure")
   public static final String OR = "75";

   @RBEntry("Strumento di aggiornamento")
   public static final String REPAIR_TOOL = "76";

   @RBEntry("Aggiorna utente dell'elenco selezionato {0}")
   @RBArgComment0("Name of user ")
   public static final String REPAIR_USER_TITLE = "77";

   @RBEntry("Aggiorna gruppo dell'elenco selezionato {0}")
   @RBArgComment0("Name of group")
   public static final String REPAIR_GROUP_TITLE = "78";

   @RBEntry("Vecchio nome in Windchill")
   public static final String OLDNAME_LABEL = "79";

   @RBEntry("Vecchio DN nell'elenco")
   public static final String OLD_DN = "80";

   @RBEntry("Strumento di rimozione")
   public static final String DELETE_TOOL = "81";

   @RBEntry("Nessun utente/gruppo/ruolo non valido")
   public static final String NO_INVALID_PRINCIPALS = "82";

   @RBEntry("Nome già assegnato ad altro utente o gruppo")
   public static final String NAME_IN_USE = "83";

   @RBEntry("Impossibile creare l'oggetto utente.")
   public static final String USER_CREATION_FAILED = "84";

   @RBEntry("Operazione non supportata")
   public static final String OPERATION_NOT_SUPPORTED = "85";
}
