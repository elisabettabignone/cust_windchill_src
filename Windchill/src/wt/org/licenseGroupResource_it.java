/* bcwti
 *
 * Copyright (c) 2013 PTC, Inc. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement.
 * You shall not disclose such confidential information and shall use it
 * only in accordance with the terms of the license agreement.
 *
 * ecwti
 */
package wt.org;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBArgComment1;
import wt.util.resource.RBArgComment2;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

/**
 * A resource bundle for license groups.
 *
 * <BR><BR><B>Supported API: </B>false
 * <BR><BR><B>Extendable: </B>false
 */
@RBUUID("wt.org.licenseGroupResource")
public final class licenseGroupResource_it extends WTListResourceBundle {
   @RBEntry("Il sistema contiene già un gruppo con il nome {0} ma include gruppi padre, che non sono previsti. Contattare l'amministratore per risolvere il problema.")
   @RBArgComment0("License group that does not meet criteria")
   @RBArgComment1("Oid of above group")
   public static final String ROOT_GROUP_CANNOT_HAVE_PARENT_EXCEPTION = "ROOT_GROUP_CANNOT_HAVE_PARENT_EXCEPTION";

   @RBEntry("Il sistema contiene già un gruppo con il nome {0} ma non appartiene a un gruppo padre con il nome {1} come richiesto dalla configurazione. Contattare l'amministratore per risolvere il problema.")
   @RBArgComment0("License group that does not meet criteria")
   @RBArgComment1("Oid of above group")
   @RBArgComment2("Root license group name")
   public static final String NO_ROOT_GROUP_AS_PARENT_EXCEPTION = "NO_ROOT_GROUP_AS_PARENT_EXCEPTION";

   @RBEntry("Il gruppo licenze {0} ({1}) contiene un membro imprevisto.")
   @RBArgComment0("License group that does not meet criteria")
   @RBArgComment1("Oid of above group")
   public static final String GROUP_HAS_UNEXPECTED_MEMBER_EXCEPTION = "GROUP_HAS_UNEXPECTED_MEMBER_EXCEPTION";

   @RBEntry("Il gruppo licenze {0} ({1}) contiene più membri.")
   @RBArgComment0("License group that does not meet criteria")
   @RBArgComment1("Oid of above group")
   public static final String GROUP_HAS_MORE_THAN_ONE_MEMBER_EXCEPTION = "GROUP_HAS_MORE_THAN_ONE_MEMBER_EXCEPTION";

   @RBEntry("Il sistema contiene già più gruppi con il nome {0}. La configurazione non è valida. Contattare l'amministratore per risolvere il problema.")
   @RBArgComment0("License group that does not meet criteria")
   public static final String MULTIPLE_GROUPS_WITH_NAME_EXCEPTION = "MULTIPLE_GROUPS_WITH_NAME_EXCEPTION";

   @RBEntry("Il gruppo licenze esterno denominato {0} è disabilitato o disconnesso.")
   @RBArgComment0("External License group that is disabled or disconnected in the system")
   public static final String EXTERNAL_LICENSE_GROUP_DISABLED_OR_DISCONNECTED = "EXTERNAL_LICENSE_GROUP_DISABLED_OR_DISCONNECTED";

   @RBEntry("Impossibile trovare il gruppo {0}. Contattare l'amministratore di sistema per risolvere il problema.")
   @RBArgComment0("Name of missing license group")
   public static final String MISSING_INT_LICENSE_GROUP = "MISSING_INT_LICENSE_GROUP";

   @RBEntry("Impossibile trovare il gruppo licenze corrispondente al gruppo interno {0}. Contattare l'amministratore di sistema per risolvere il problema.")
   @RBArgComment0("Name of missing license group")
   public static final String MISSING_EXT_LICENSE_GROUP = "MISSING_EXT_LICENSE_GROUP";

   /**
    * License Group Names and Descriptions
    */
   @RBEntry("System Licenses")
   @RBComment("DO NOT TRANSLATE")
   public static final String SYSTEM_LICENSES = "SystemLicenses";

   @RBEntry("System group for managing license groups")
   @RBComment("DO NOT TRANSLATE")
   public static final String SYSTEM_LICENSES_DESC = "SYSTEM_LICENSES_DESC";

   @RBEntry("Visualizza e stampa soltanto")
   @RBComment("Name of a license group")
   public static final String VIEW_AND_PRINT_ONLY = "ViewandPrintOnly";

   @RBEntry("Gruppo di licenze che consente solo di visualizzare e stampare oggetti")
   @RBComment("Description of a license group")
   public static final String VIEW_AND_PRINT_ONLY_DESC = "VIEW_AND_PRINT_ONLY_DESC";

   @RBEntry("Package importabile PLM Connector")
   @RBComment("Name of a license group")
   public static final String PLM_CONNECTOR_IMPORT_PACKAGE = "PLMConnectorImportablePackage";

   @RBEntry("Gruppo di licenze che consente di creare file ZIP di package importabili")
   @RBComment("Description of a license group")
   public static final String PLM_CONNECTOR_IMPORT_PACKAGE_DESC = "PLM_CONNECTOR_IMPORT_PACKAGE_DESC";

   @RBEntry("Strutture piattaforma")
   @RBComment("Name of a license group")
   public static final String PLATFORM_STRUCTURES = "PlatformStructures";

   @RBEntry("Gruppo di licenze per utilizzo di espressioni nella strutture piattaforma")
   @RBComment("Description of a license group")
   public static final String PLATFORM_STRUCTURES_DESC = "PLATFORM_STRUCTURES_DESC";
}
