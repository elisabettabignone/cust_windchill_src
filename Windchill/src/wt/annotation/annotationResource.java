/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.annotation;

import wt.util.resource.*;

@RBUUID("wt.annotation.annotationResource")
public final class annotationResource extends WTListResourceBundle {
   @RBEntry("The name attribute must have a value")
   public static final String ANNOTATIONSET_NAME_NULL = "0";

   @RBEntry("({0})")
   @RBPseudo(false)
   @RBComment("Identity for a Annotation Set.")
   @RBArgComment0("The Annotation set name.")
   public static final String ANNOTATIONSET_NAME = "1";

   @RBEntry("Top Level Target")
   public static final String TOP_LEVEL_TARGET = "2";

   @RBEntry("Applied?")
   public static final String APPLIED = "3";

   @RBEntry("Must include needed parameters for apply.")
   public static final String MISSING_PARAMETER = "4";

   @RBEntry("New link not found.")
   public static final String NEWLINK_NOT_FOUND = "5";

   @RBEntry("Unable to change quantity of \"{0}\".  Parent \"{1}\" is checked out by another user.")
   @RBArgComment0("Child part")
   @RBArgComment1("Parent part")
   public static final String PARENT_CHILD_CQ_ALREADY_CHECKED_OUT = "10";

   @RBEntry("Unable to change quantity.  Parent \"{0}\" is checked out by another user.")
   @RBArgComment0("Parent part")
   public static final String PARENT_CQ_ALREADY_CHECKED_OUT = "11";

   @RBEntry("Unable to remove usage of \"{0}\".  Parent \"{1}\" is checked out by another user.")
   @RBArgComment0("Child part")
   @RBArgComment1("Parent part")
   public static final String PARENT_CHILD_RU_ALREADY_CHECKED_OUT = "12";

   @RBEntry("Unable to remove usage.  Parent \"{0}\" is checked out by another user.")
   @RBArgComment0("Parent part")
   public static final String PARENT_RU_ALREADY_CHECKED_OUT = "13";

   @RBEntry("Unable to modify APL.  \"{0}\" is checked out by another user.")
   @RBArgComment0("APL")
   public static final String APL_ALREADY_CHECKED_OUT = "14";

   @RBEntry("There are no annotations to apply.")
   public static final String EMPTY_ANNOTATIONS = "15";

   @RBEntry("The {0} annotation of {1} '{2}' failed to apply.")
   public static final String ANNOTATION_APPLY_FAILED = "16";

   @RBEntry("The Annotation Set has not been created.")
   public static final String EMPTY_ANNOTATION_SET = "17";
}
