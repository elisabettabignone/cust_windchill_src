/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.annotation;

import wt.util.resource.*;

@RBUUID("wt.annotation.annotationResource")
public final class annotationResource_it extends WTListResourceBundle {
   @RBEntry("All'attributo nome deve essere associato un valore")
   public static final String ANNOTATIONSET_NAME_NULL = "0";

   @RBEntry("({0})")
   @RBPseudo(false)
   @RBComment("Identity for a Annotation Set.")
   @RBArgComment0("The Annotation set name.")
   public static final String ANNOTATIONSET_NAME = "1";

   @RBEntry("Obiettivo di livello superiore")
   public static final String TOP_LEVEL_TARGET = "2";

   @RBEntry("Applicato?")
   public static final String APPLIED = "3";

   @RBEntry("Includere i parametri necessari da applicare.")
   public static final String MISSING_PARAMETER = "4";

   @RBEntry("Il nuovo link non è stato trovato.")
   public static final String NEWLINK_NOT_FOUND = "5";

   @RBEntry("Impossibile modificare la quantità di \"{0}\".  Il padre \"{1}\" è stato sottoposto a Check-Out da un altro utente.")
   @RBArgComment0("Child part")
   @RBArgComment1("Parent part")
   public static final String PARENT_CHILD_CQ_ALREADY_CHECKED_OUT = "10";

   @RBEntry("Impossibile modificare la quantità. Il padre \"{0}\" è stato sottoposto a Check-Out da un altro utente.")
   @RBArgComment0("Parent part")
   public static final String PARENT_CQ_ALREADY_CHECKED_OUT = "11";

   @RBEntry("Impossibile rimuovere l'utilizzo di \"{0}\". Il padre \"{1}\" è stato sottoposto a Check-Out da un altro utente.")
   @RBArgComment0("Child part")
   @RBArgComment1("Parent part")
   public static final String PARENT_CHILD_RU_ALREADY_CHECKED_OUT = "12";

   @RBEntry("Impossibile rimuovere l'utilizzo.  Il padre \"{0}\" è stato sottoposto a Check-Out da un altro utente.")
   @RBArgComment0("Parent part")
   public static final String PARENT_RU_ALREADY_CHECKED_OUT = "13";

   @RBEntry("Impossibile modificare l'elenco delle parti approvate.  \"{0}\" sottoposto a Check-Out da altro utente.")
   @RBArgComment0("APL")
   public static final String APL_ALREADY_CHECKED_OUT = "14";

   @RBEntry("Nessuna annotazione da applicare.")
   public static final String EMPTY_ANNOTATIONS = "15";

   @RBEntry("Impossibile applicare l'annotazione {0} di {1} '{2}'.")
   public static final String ANNOTATION_APPLY_FAILED = "16";

   @RBEntry("Il gruppo di annotazioni non è stato creato.")
   public static final String EMPTY_ANNOTATION_SET = "17";
}
