/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.phonehome;

import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("com.ptc.phonehome.phoneHomeResource")
public final class phoneHomeResource extends WTListResourceBundle {

	// Preference category display name for phone home.

	@RBEntry("System Quality Agent")
	@RBComment("The category name for the phone home preferences.")
	public static final String PRIVATE_CONSTANT_0 = "PREFERENCE_PHONEHOME_CATEGORY";

	// Entries for opt in/out choice preference.

	@RBEntry("System Quality Agent Activation ")
	@RBComment("turn on/off phone home")
	public static final String PRIVATE_CONSTANT_1 = "OPT_IN_OUT_LABEL";

	@RBEntry("Turns on or off the interaction mechanism that automates sending Windchill data to PTC and receiving email correspondence from PTC.")
	@RBComment("Activate/Deactivate the Phone Home Feature.")
	public static final String PRIVATE_CONSTANT_2 = "OPT_IN_OUT_DESC";

	@RBEntry("Selecting Yes activates the interaction mechanism. Selecting No deactivates the mechanism.  The other preferences in the Quality Agent category are only used when the interaction mechanism is turned on.")
	@RBComment("Activate/Deactivate the Phone Home Feature.")
	public static final String PRIVATE_CONSTANT_3 = "OPT_IN_OUT_LONG_DESC";

	// Entries for type of system preference.

	@RBEntry("Environment")
	@RBComment("define the type of system.")
	public static final String PRIVATE_CONSTANT_4 = "SYSTEM_TYPE_LABEL";

	@RBEntry("Select whether the Windchill solution is being used in a production environment. Information is only sent to PTC for production environments.")
	@RBComment("Define the type of system.  (Production or non-production).")
	public static final String PRIVATE_CONSTANT_5 = "SYSTEM_TYPE_DESC";

	@RBEntry("Identify whether the current Windchill solution is a production system. Environments that are not considered production environments include testing  or demo systems.")
	@RBComment("Define the type of system.  (Production or non-production)")
	public static final String PRIVATE_CONSTANT_6 = "SYSTEM_TYPE_LONG_DESC";

	// Entries for customer name preference.

	@RBEntry("Company Name")
	@RBComment("The name of the customer.")
	public static final String PRIVATE_CONSTANT_7 = "CUSTOMER_NAME_LABEL";

	@RBEntry("Sets the company name to use when interacting with PTC about the current Windchill solution.")
	@RBComment("The name of this company.")
	public static final String PRIVATE_CONSTANT_8 = "CUSTOMER_NAME_DESC";

	@RBEntry("Enter the company name that you want used when information about your Windchill solution is sent to PTC and when receiving email correspondence from PTC that is related to the current Windchill solution.")
	@RBComment("The name of this company.")
	public static final String PRIVATE_CONSTANT_9 = "CUSTOMER_NAME_LONG_DESC";

	// Entries for customer number preference.

	@RBEntry("Customer Number")
	@RBComment("the customer number.")
	public static final String PRIVATE_CONSTANT_10 = "CUSTOMER_NUMBER_LABEL";

	@RBEntry("Sets the customer number that PTC has assigned to the current Windchill solution.")
	@RBComment("Customer number assigned by PTC.")
	public static final String PRIVATE_CONSTANT_11 = "CUSTOMER_NUMBER_DESC";

	@RBEntry("Enter the customer number that PTC has assigned to your company when you purchased your Windchill solution.")
	@RBComment("Customer number assigned by PTC.")
	public static final String PRIVATE_CONSTANT_12 = "CUSTOMER_NUMBER_LONG_DESC";

	// Entries for report transmission frequency preference.

	@RBEntry("Frequency of Transmissions to PTC")
	@RBComment("The frequency of transmissions.")
	public static final String PRIVATE_CONSTANT_13 = "TRANS_FREQ_LABEL";

	@RBEntry("Select how often to send information to PTC.")
	@RBComment("Frequency of transmissions.")
	public static final String PRIVATE_CONSTANT_14 = "TRANS_FREQ_DESC";

	@RBEntry("You can choose to send information once every 30 days, 90 days, 180 days, or 365 days.")
	@RBComment("Frequency of transmissions.")
	public static final String PRIVATE_CONSTANT_15 = "TRANS_FREQ_LONG_DESC";

	// Entries for list of recipients for information from PTC

	@RBEntry("Email Addresses")
	@RBComment("list of email addresses.")
	public static final String PRIVATE_CONSTANT_16 = "EMAIL_LIST_LABEL";

	@RBEntry("Sets the list of email addresses that PTC should use when sending information from PTC.")
	@RBComment("List of email recipients for information from PTC.")
	public static final String PRIVATE_CONSTANT_17 = "EMAIL_LIST_DESC";

	@RBEntry("Enter valid email addresses for all users you want receiving correspondence from PTC about the current Windchill solution. Separate multiple addresses using the semicolon (;).")
	@RBComment("List of email recipients for information from PTC.")
	public static final String PRIVATE_CONSTANT_18 = "EMAIL_LIST_LONG_DESC";

	@RBEntry("Production")
	@RBComment("value for production system choice.")
	public static final String PRIVATE_CONSTANT_19 = "PRODUCTION_VALUE";

	@RBEntry("Non-Production")
	@RBComment("value for non production system choice.")
	public static final String PRIVATE_CONSTANT_20 = "NON_PRODUCTION_VALUE";

	@RBEntry("90 Days")
	@RBComment("value for quarterly choice.")
	public static final String PRIVATE_CONSTANT_21 = "90_DAYS_VALUE";

	@RBEntry("180 Days")
	@RBComment("value for semi annually choice.")
	public static final String PRIVATE_CONSTANT_22 = "180_DAYS_VALUE";

	@RBEntry("365 Days")
	@RBComment("value for annually choice.")
	public static final String PRIVATE_CONSTANT_23 = "365_DAYS_VALUE";
	
	@RBEntry("30 Days")
	@RBComment("value for monthly choice.")
	public static final String PRIVATE_CONSTANT_24 = "30_DAYS_VALUE";
	
	@RBEntry("Sales Order Number")
	@RBComment("the SON or sales order number")
	public static final String PRIVATE_CONSTANT_25 = "SON_LABEL";
	
	@RBEntry("Sets the sales order number to use when interacting with PTC about the current Windchill solution.")
	@RBComment("The sales order number of this company.")
	public static final String PRIVATE_CONSTANT_26 = "SON_DESC";

	@RBEntry("Enter the sales order number that you want used when information about your Windchill solution is sent to PTC .")
	@RBComment("The sales order number of this company.")
	public static final String PRIVATE_CONSTANT_27 = "SON_LONG_DESC";
	
	// Entries for sending the phone home report
	@RBEntry("Failed to obtain the send path {0}.")
	@RBComment("Fully qualified file path.")
	public static final String FAILED_TO_OBTAIN_PATH = "10";
	
	@RBEntry("Failed to obtain a Transport to send data.")
	@RBComment("Transport is a class and should be capitalized.")
	public static final String FAILED_TO_OBTAIN_TRANSPORT = "20";
	
	@RBEntry("Failed to send data with the error {0}.")
	@RBComment("Error that occurs during data transport.")
	public static final String FAILED_TO_SEND_DATA = "30";	
	
	@RBEntry("Could not delete all of the report files or directories")
	public static final String DELETE_REPORT_DIR_FAILED = "40";
}
