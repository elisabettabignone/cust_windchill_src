/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.phonehome.queue;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBArgComment1;
import wt.util.resource.RBArgComment2;
import wt.util.resource.RBArgComment3;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.phonehome.queue.PhoneHomeResource")
public final class QueueResource extends WTListResourceBundle {
	@RBEntry("{0} completed succesfully on attempt number {1} and took {2} seconds starting at {3}")
	@RBArgComment0("Name of the task that is running like Send Report or Generate Report")
	@RBArgComment1("A number that indicates which attempt")
	@RBArgComment2("The number of seconds this took to complete")
	@RBArgComment3("The time the task started")
	public static final String QUEUE_ENTRY_SUCCESS = "40";

	@RBEntry("{0} failed on attempt number {1} and took {2} seconds starting at {3}")
	@RBArgComment0("Name of the task that is running like Send Report or Generate Report")
	@RBArgComment1("A number that indicates which attempt")
	@RBArgComment2("The number of seconds this took to complete")
	@RBArgComment3("The time the task started")
	public static final String QUEUE_ENTRY_FAILURE_RETRY = "50";
	
	@RBEntry("{0} could not complete successfully after {1} tries")
	@RBArgComment0("Name of the task that is running like Send Report or Generate Report")
	@RBArgComment1("A number that indicates which attempt")
	public static final String QUEUE_ENTRY_FAILURE_END = "60";
	
	@RBEntry("Generate Report")
	@RBComment("The name of the task that generates the report")
	public static final String REPORT_CREATOR_NAME = "70";
	
	@RBEntry("Send Report")
	@RBComment("The name of the task that sends the report")
	public static final String REPORT_SENDER_NAME = "80";
}
