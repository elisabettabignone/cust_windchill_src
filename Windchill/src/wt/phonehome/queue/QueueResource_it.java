/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.phonehome.queue;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBArgComment1;
import wt.util.resource.RBArgComment2;
import wt.util.resource.RBArgComment3;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.phonehome.queue.PhoneHomeResource")
public final class QueueResource_it extends WTListResourceBundle {
	@RBEntry("Task {0} completato al tentativo numero {1}. Ha impiegato {2} secondi ed è iniziato alle {3}")
	@RBArgComment0("Name of the task that is running like Send Report or Generate Report")
	@RBArgComment1("A number that indicates which attempt")
	@RBArgComment2("The number of seconds this took to complete")
	@RBArgComment3("The time the task started")
	public static final String QUEUE_ENTRY_SUCCESS = "40";

	@RBEntry("Task {0} non riuscito al tentativo numero {1}. Ha impiegato {2} secondi ed è iniziato alle {3}")
	@RBArgComment0("Name of the task that is running like Send Report or Generate Report")
	@RBArgComment1("A number that indicates which attempt")
	@RBArgComment2("The number of seconds this took to complete")
	@RBArgComment3("The time the task started")
	public static final String QUEUE_ENTRY_FAILURE_RETRY = "50";
	
	@RBEntry("Il task {0} non è stato completato dopo {1} tentativi")
	@RBArgComment0("Name of the task that is running like Send Report or Generate Report")
	@RBArgComment1("A number that indicates which attempt")
	public static final String QUEUE_ENTRY_FAILURE_END = "60";
	
	@RBEntry("Genera report")
	@RBComment("The name of the task that generates the report")
	public static final String REPORT_CREATOR_NAME = "70";
	
	@RBEntry("Invia report")
	@RBComment("The name of the task that sends the report")
	public static final String REPORT_SENDER_NAME = "80";
}
