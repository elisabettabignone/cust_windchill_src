/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.phonehome;

import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("com.ptc.phonehome.phoneHomeResource")
public final class phoneHomeResource_it extends WTListResourceBundle {

	// Preference category display name for phone home.

	@RBEntry("Agente qualità di sistema")
	@RBComment("The category name for the phone home preferences.")
	public static final String PRIVATE_CONSTANT_0 = "PREFERENCE_PHONEHOME_CATEGORY";

	// Entries for opt in/out choice preference.

	@RBEntry("Attivazione agente qualità di sistema ")
	@RBComment("turn on/off phone home")
	public static final String PRIVATE_CONSTANT_1 = "OPT_IN_OUT_LABEL";

	@RBEntry("Attiva o disattiva il meccanismo di interazione che automatizza l'invio di dati Windchill a PTC e la ricezione di messaggi e-mail da PTC.")
	@RBComment("Activate/Deactivate the Phone Home Feature.")
	public static final String PRIVATE_CONSTANT_2 = "OPT_IN_OUT_DESC";

	@RBEntry("Selezionare Sì per attivare il meccanismo di interazione e No per disattivarlo. Le altre preferenze della categoria Agente qualità vengono utilizzate solo quando il meccanismo di interazione è attivato.")
	@RBComment("Activate/Deactivate the Phone Home Feature.")
	public static final String PRIVATE_CONSTANT_3 = "OPT_IN_OUT_LONG_DESC";

	// Entries for type of system preference.

	@RBEntry("Ambiente")
	@RBComment("define the type of system.")
	public static final String PRIVATE_CONSTANT_4 = "SYSTEM_TYPE_LABEL";

	@RBEntry("Selezionare se la soluzione Windchill è in uso in un ambiente di produzione. Le informazioni vengono inviate solo a PTC per gli ambienti di produzione.")
	@RBComment("Define the type of system.  (Production or non-production).")
	public static final String PRIVATE_CONSTANT_5 = "SYSTEM_TYPE_DESC";

	@RBEntry("Identificare se la soluzione Windchill corrente è un sistema di produzione. Gli ambienti che non sono considerati ambienti di produzione includono i sistemi di test o demo.")
	@RBComment("Define the type of system.  (Production or non-production)")
	public static final String PRIVATE_CONSTANT_6 = "SYSTEM_TYPE_LONG_DESC";

	// Entries for customer name preference.

	@RBEntry("Nome società")
	@RBComment("The name of the customer.")
	public static final String PRIVATE_CONSTANT_7 = "CUSTOMER_NAME_LABEL";

	@RBEntry("Imposta il nome della società da usare quando si interagisce con PTC per la soluzione Windchill corrente.")
	@RBComment("The name of this company.")
	public static final String PRIVATE_CONSTANT_8 = "CUSTOMER_NAME_DESC";

	@RBEntry("Immettere il nome della società da usare quando le informazioni sulla soluzione Windchill vengono inviate a PTC e quando si ricevono messaggi e-mail da PTC relativamente alla soluzione Windchill corrente.")
	@RBComment("The name of this company.")
	public static final String PRIVATE_CONSTANT_9 = "CUSTOMER_NAME_LONG_DESC";

	// Entries for customer number preference.

	@RBEntry("Codice cliente")
	@RBComment("the customer number.")
	public static final String PRIVATE_CONSTANT_10 = "CUSTOMER_NUMBER_LABEL";

	@RBEntry("Imposta il codice cliente che PTC ha assegnato alla soluzione Windchill corrente.")
	@RBComment("Customer number assigned by PTC.")
	public static final String PRIVATE_CONSTANT_11 = "CUSTOMER_NUMBER_DESC";

	@RBEntry("Immettere il codice cliente che PTC ha assegnato alla società al momento dell'acquisto della soluzione Windchill.")
	@RBComment("Customer number assigned by PTC.")
	public static final String PRIVATE_CONSTANT_12 = "CUSTOMER_NUMBER_LONG_DESC";

	// Entries for report transmission frequency preference.

	@RBEntry("Frequenza invio informazioni a PTC")
	@RBComment("The frequency of transmissions.")
	public static final String PRIVATE_CONSTANT_13 = "TRANS_FREQ_LABEL";

	@RBEntry("Selezionare la frequenza di invio delle informazioni a PTC.")
	@RBComment("Frequency of transmissions.")
	public static final String PRIVATE_CONSTANT_14 = "TRANS_FREQ_DESC";

	@RBEntry("È possibile inviare le informazioni ogni 30, 90, 180 o 365 giorni.")
	@RBComment("Frequency of transmissions.")
	public static final String PRIVATE_CONSTANT_15 = "TRANS_FREQ_LONG_DESC";

	// Entries for list of recipients for information from PTC

	@RBEntry("Indirizzi e-mail")
	@RBComment("list of email addresses.")
	public static final String PRIVATE_CONSTANT_16 = "EMAIL_LIST_LABEL";

	@RBEntry("Imposta l'elenco degli indirizzi e-mail che PTC deve usare per inviare le informazioni.")
	@RBComment("List of email recipients for information from PTC.")
	public static final String PRIVATE_CONSTANT_17 = "EMAIL_LIST_DESC";

	@RBEntry("Immettere un indirizzo e-mail valido per tutti gli utenti che si desidera ricevano messaggi e-mail da PTC riguardo la soluzione Windchill corrente. Separare gli indirizzi con un punto e virgola (;).")
	@RBComment("List of email recipients for information from PTC.")
	public static final String PRIVATE_CONSTANT_18 = "EMAIL_LIST_LONG_DESC";

	@RBEntry("Produzione")
	@RBComment("value for production system choice.")
	public static final String PRIVATE_CONSTANT_19 = "PRODUCTION_VALUE";

	@RBEntry("Non produzione")
	@RBComment("value for non production system choice.")
	public static final String PRIVATE_CONSTANT_20 = "NON_PRODUCTION_VALUE";

	@RBEntry("90 giorni")
	@RBComment("value for quarterly choice.")
	public static final String PRIVATE_CONSTANT_21 = "90_DAYS_VALUE";

	@RBEntry("180 giorni")
	@RBComment("value for semi annually choice.")
	public static final String PRIVATE_CONSTANT_22 = "180_DAYS_VALUE";

	@RBEntry("365 giorni")
	@RBComment("value for annually choice.")
	public static final String PRIVATE_CONSTANT_23 = "365_DAYS_VALUE";
	
	@RBEntry("30 giorni")
	@RBComment("value for monthly choice.")
	public static final String PRIVATE_CONSTANT_24 = "30_DAYS_VALUE";
	
	@RBEntry("Numero d'ordine (SON)")
	@RBComment("the SON or sales order number")
	public static final String PRIVATE_CONSTANT_25 = "SON_LABEL";
	
	@RBEntry("Imposta il numero d'ordine da usare quando si interagisce con PTC per la soluzione Windchill corrente.")
	@RBComment("The sales order number of this company.")
	public static final String PRIVATE_CONSTANT_26 = "SON_DESC";

	@RBEntry("Immettere il numero d'ordine da usare quando si inviano le informazioni sulla soluzione Windchill a PTC.")
	@RBComment("The sales order number of this company.")
	public static final String PRIVATE_CONSTANT_27 = "SON_LONG_DESC";
	
	// Entries for sending the phone home report
	@RBEntry("Impossibile ottenere il percorso di invio {0}.")
	@RBComment("Fully qualified file path.")
	public static final String FAILED_TO_OBTAIN_PATH = "10";
	
	@RBEntry("Impossibile ottenere un trasporto per inviare i dati.")
	@RBComment("Transport is a class and should be capitalized.")
	public static final String FAILED_TO_OBTAIN_TRANSPORT = "20";
	
	@RBEntry("Impossibile inviare i dati. Errore: {0}.")
	@RBComment("Error that occurs during data transport.")
	public static final String FAILED_TO_SEND_DATA = "30";	
	
	@RBEntry("Impossibile eliminare tutti i file o le directory di report")
	public static final String DELETE_REPORT_DIR_FAILED = "40";
}
