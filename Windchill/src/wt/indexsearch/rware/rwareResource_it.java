package wt.indexsearch.rware;

import wt.util.resource.*;

public final class rwareResource_it extends WTListResourceBundle {
   /**
    * bcwti
    * 
    * Copyright (c) 1997, 1998 FoundationWare. All Rights Reserved.
    * 
    * This software is the confidential and proprietary information of
    * FoundationWare. You shall not disclose such confidential information
    * and shall use it only in accordance with the terms of the license
    * agreement you entered into FoundationWare.
    * 
    * ecwti
    * rwareResource message resource bundle [English/US]
    **/
   @RBEntry("Impossibile effettuare la ricerca perché la proprietà {0} non è definita.")
   @RBComment("Error message displayed when an index property is not defined")
   @RBArgComment0("Name of the property that is not defined.")
   public static final String NO_INDEX_PROPERTY = "0";

   @RBEntry("L'interrogazione RetrievalWare non è stata completata. Lo stato è {0}.")
   @RBComment("Error message  if a query did not complete.")
   @RBArgComment0("Java constant for the state to which the query executed.")
   public static final String QUERY_NOT_COMPLETE = "1";

   @RBEntry("Il campo {0} RetrievalWare ha valore {1} e non è valido.")
   @RBComment("Error message if the value of the RetrievalWare field is not formatted as expected.")
   @RBArgComment0("Name of the field stored in RetrievalWare.")
   @RBArgComment1("Value of the field named in {0}")
   public static final String OID_FIELD_FORMAT = "2";

   @RBEntry("Impossibile accedere a RetrievalWare. Contattare l'amministratore comunicando questo messaggio e l'ora in cui si è verificato l'errore.")
   @RBComment("This error occurs when the system can not connect to Rware")
   public static final String RWARE_CONNECT = "3";

   @RBEntry("Livello di espansione concettuale")
   @RBComment("Title for the concept expansion preference")
   public static final String PRIVATE_CONSTANT_0 = "DISPLAY_CONCEPT_EXPANSION";

   @RBEntry("Scegliere il grado di espansione della modalità concettuale.")
   @RBComment("Short description for the concept expansion preference")
   public static final String PRIVATE_CONSTANT_1 = "SHORT_DESC_CONCEPT_EXPANSION";

   @RBEntry("Se si seleziona <B>Concettuale</B> come modalità di ricerca, è possibile scegliere il livello di espansione dei criteri di ricerca. Il default di sitema è Variazioni minime.")
   @RBComment("Description for the concept expansion preference")
   public static final String PRIVATE_CONSTANT_2 = "DESC_CONCEPT_EXPANSION";

   @RBEntry("Solo corrispondenze esatte")
   @RBComment("Title for exact matches concept expansion level preference")
   public static final String PRIVATE_CONSTANT_3 = "DISPLAY_EXACT_MATCH_EXPANSION";

   @RBEntry("Variazioni minime")
   @RBComment("Title for simple variations concept expansion level preference")
   public static final String PRIVATE_CONSTANT_4 = "DISPLAY_SIMPLE_EXPANSION";

   @RBEntry("Concetti identici")
   @RBComment("Title for most strongly related concepts concept expansion level preference")
   public static final String PRIVATE_CONSTANT_5 = "DISPLAY_MOST_STRONG_EXPANSION";

   @RBEntry("Concetti strettamente collegati")
   @RBComment("Title for strongly related concepts concept expansion level preference")
   public static final String PRIVATE_CONSTANT_6 = "DISPLAY_STRONG_EXPANSION";

   @RBEntry("Concetti vagamente collegati")
   @RBComment("Title for weakly related concepts concept expansion level preference")
   public static final String PRIVATE_CONSTANT_7 = "DISPLAY_WEAK_EXPANSION";

   @RBEntry("Modalità di ricerca")
   @RBComment("Title for the search mode preference")
   public static final String PRIVATE_CONSTANT_8 = "DISPLAY_SEARCH_MODE";

   @RBEntry("Scegliere il tipo di ricerca per parola chiave.")
   @RBComment("Short description for the search mode preference")
   public static final String PRIVATE_CONSTANT_9 = "SHORT_DESC_SEARCH_MODE";

   @RBEntry("La modalità di ricerca determina come interpretare il testo del campo <B>Parola chiave</B> di una pagina di ricerca. Il sistema di default è la modalità booleana.")
   @RBComment("Description for the search mode preference")
   public static final String PRIVATE_CONSTANT_10 = "DESC_SEARCH_MODE";

   @RBEntry("Booleana - ricerca le parole e le espressioni specificate usando gli operatori booleani standard (come AND, OR, NOT) che determinano le relazioni tra le parole immesse")
   @RBComment("Title for boolean search mode preference")
   public static final String PRIVATE_CONSTANT_11 = "DISPLAY_BOOLEAN_MODE";

   @RBEntry("Concettuale - ricerca le parole specificate e le parole di significato analogo")
   @RBComment("Title for concept search mode preference")
   public static final String PRIVATE_CONSTANT_12 = "DISPLAY_CONCEPT_MODE";

   @RBEntry("Ortografica - ricerca le parole specificate e le parole con simile ortografia")
   @RBComment("Title for pattern search mode preference")
   public static final String PRIVATE_CONSTANT_13 = "DISPLAY_PATTERN_MODE";
}
