package wt.indexsearch;

import wt.util.resource.*;

@RBUUID("wt.indexsearch.indexsearchResource")
public final class indexsearchResource_it extends WTListResourceBundle {
   /**
/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
    * rwareResource message resource bundle [English/US]
    **/
   @RBEntry("Impossibile effettuare la ricerca perché la proprietà {0} non è definita.")
   @RBComment("Error message displayed when an index property is not defined")
   @RBArgComment0("Name of the property that is not defined.")
   public static final String NO_INDEX_PROPERTY = "0";

   @RBEntry("L'interrogazione RetrievalWare non è stata completata. Lo stato del ciclo di vita è {0}.")
   @RBComment("Error message  if a query did not complete.")
   @RBArgComment0("Java constant for the state to which the query executed.")
   public static final String QUERY_NOT_COMPLETE = "1";

   @RBEntry("Il campo {0} RetrievalWare ha valore {1} e non è valido.")
   @RBComment("Error message if the value of the RetrievalWare field is not formatted as expected.")
   @RBArgComment0("Name of the field stored in RetrievalWare.")
   @RBArgComment1("Value of the field named in {0}")
   public static final String OID_FIELD_FORMAT = "2";

   @RBEntry("Si è verificato un problema con l'interrogazione o con la connessione al motore di indicizzazione. Comunicare all'amministratore questo messaggio e l'ora in cui si è verificato l'errore.")
   @RBComment("This error occurs when the system can not connect to SOLR")
   public static final String INDEX_ENGINE_CONNECT = "3";

   @RBEntry("Errore di esecuzione dell'interrogazione. Verificarne la sintassi.")
   @RBComment("This error occurs when there is an error executing the query")
   public static final String INDEX_ENGINE_BAD_REQUEST = "4";

   @RBEntry("Si è verificato un errore durante l'esecuzione della ricerca per indice. I criteri di ricerca sono poco selettivi per completare la ricerca. Per restringere la ricerca, selezionare dei criteri aggiuntivi e riprovare.")
   @RBComment("Message to display to a user when there is an too many boolean clauses error occurred and they need to make their criteria more specific to avoid this error.")
   public static final String OVER_MAX_BOOLEAN_CLAUSES_ERROR = "OVER_MAX_BOOLEAN_CLAUSES_ERROR";


   @RBEntry("Nome")
   @RBComment("Keyword to be used while searching for objects by name.")
   public static final String PRIVATE_CONSTANT_0 = "KEYWORD_SEARCH_NAME";

   @RBEntry("Numero")
   @RBComment("Keyword to be used while searching for objects by number.")
   public static final String PRIVATE_CONSTANT_1 = "KEYWORD_SEARCH_NUMBER";

   @RBEntry("Limite risorsa di ricerca superato. Il problema si manifesta di norma quando la ricerca con caratteri jolly è troppo vasta. Ritentare la ricerca usando una parola chiave più specifica.")
   @RBComment("InStream error with a too broad wildcard search")
   public static final String PRIVATE_CONSTANT_2 = "RESOURCE_LIMIT_EXCEEDED";

   @RBEntry("Errore nell'interrogazione. Impossibile analizzare la parola chiave come interrogazione Index Search valida.")
   @RBComment("InStream error for a syntax error in the query")
   public static final String PRIVATE_CONSTANT_3 = "QUERY_ERROR";

   @RBEntry("Timeout interrogazione. Intex Search non ha risposto entro il limite di timeout consentito.")
   @RBComment("InStream error for a timeout error")
   public static final String PRIVATE_CONSTANT_4 = "QUERY_TIMEOUT";

   @RBEntry("Livello di espansione concettuale")
   @RBComment("Title for the concept expansion preference")
   public static final String PRIVATE_CONSTANT_5 = "DISPLAY_CONCEPT_EXPANSION";

   @RBEntry("Scegliere il grado di espansione della modalità concettuale.")
   @RBComment("Short description for the concept expansion preference")
   public static final String PRIVATE_CONSTANT_6 = "SHORT_DESC_CONCEPT_EXPANSION";

   @RBEntry("Se si seleziona <B>Concettuale</B> come modalità di ricerca, è possibile scegliere il livello di espansione dei criteri di ricerca. Il default di sitema è Variazioni minime.")
   @RBComment("Description for the concept expansion preference")
   public static final String PRIVATE_CONSTANT_7 = "DESC_CONCEPT_EXPANSION";

   @RBEntry("Solo corrispondenze esatte")
   @RBComment("Title for exact matches concept expansion level preference")
   public static final String PRIVATE_CONSTANT_8 = "DISPLAY_EXACT_MATCH_EXPANSION";

   @RBEntry("Variazioni minime")
   @RBComment("Title for simple variations concept expansion level preference")
   public static final String PRIVATE_CONSTANT_9 = "DISPLAY_SIMPLE_EXPANSION";

   @RBEntry("Concetti identici")
   @RBComment("Title for most strongly related concepts concept expansion level preference")
   public static final String PRIVATE_CONSTANT_10 = "DISPLAY_MOST_STRONG_EXPANSION";

   @RBEntry("Concetti strettamente collegati")
   @RBComment("Title for strongly related concepts concept expansion level preference")
   public static final String PRIVATE_CONSTANT_11 = "DISPLAY_STRONG_EXPANSION";

   @RBEntry("Concetti vagamente collegati")
   @RBComment("Title for weakly related concepts concept expansion level preference")
   public static final String PRIVATE_CONSTANT_12 = "DISPLAY_WEAK_EXPANSION";

   @RBEntry("Modalità di ricerca")
   @RBComment("Title for the search mode preference")
   public static final String PRIVATE_CONSTANT_13 = "DISPLAY_SEARCH_MODE";

   @RBEntry("Scegliere il tipo di ricerca per parola chiave.")
   @RBComment("Short description for the search mode preference")
   public static final String PRIVATE_CONSTANT_14 = "SHORT_DESC_SEARCH_MODE";

   @RBEntry("La modalità di ricerca determina come viene interpretato il testo immesso nel campo <B>Parola chiave</B> di una pagina di ricerca. <br>Semplice - Ricerca le parole e le espressioni specificate usando gli operatori booleani standard AND/OR. Per scegliere l'operatore AND o OR, selezionare l'opzione Che soddisfano tutti i criteri (operatore AND) oppure Che soddisfano uno qualsiasi dei criteri (operatore OR) dal menu a discesa Trova nella pagina Ricerca avanzata. <br>Avanzata - Consente agli utenti di immettere un'interrogazione nel campo della parola chiave utilizzando la sintassi Solr Query Language.<br> Il default è la modalità di ricerca semplice.")
   @RBComment("Description for the search mode preference")
   public static final String PRIVATE_CONSTANT_15 = "DESC_SEARCH_MODE";

   @RBEntry("Semplice")
   @RBComment("Choice for simple search mode preference")
   public static final String PRIVATE_CONSTANT_16 = "DISPLAY_SIMPLE_MODE";

   @RBEntry("Avanzata")
   @RBComment("Choice for advanced search mode preference")
   public static final String PRIVATE_CONSTANT_17 = "DISPLAY_ADVANCED_MODE";

   @RBEntry("Preferenze libreria")
   @RBComment("Title for index library preferences")
   public static final String PRIVATE_CONSTANT_18 = "DISPLAY_INDEX_LIBRARIES";

   @RBEntry("Selezionare le librerie per la ricerca per parola chiave.")
   @RBComment("Short description for index library preferences")
   public static final String PRIVATE_CONSTANT_19 = "SHORT_DESC_INDEX_LIBRARIES";

   @RBEntry("Selezionare le librerie da includere nella ricerca per parola chiave. Per default, verranno selezionate tutte le librerie disponibili.")
   @RBComment("Full description for index library preferences")
   public static final String PRIVATE_CONSTANT_20 = "DESC_INDEX_LIBRARIES";

   @RBEntry("Ricerca rapida")
   @RBComment("Quick search")
   public static final String PRIVATE_CONSTANT_21 = "QUICK_SEARCH_NAME";

   @RBEntry("Ricerca rapida")
   @RBComment("Quick search")
   public static final String PRIVATE_CONSTANT_22 = "QUICK_SEARCH_DESCRIPTION";

   @RBEntry("Ricerca rapida")
   @RBComment("Quick search")
   public static final String PRIVATE_CONSTANT_23 = "QUICK_SEARCH_LONGDESCRIPTION";

   @RBEntry("Indice di massa interrotto")
   @RBComment("Bulk Index Stopped")
   public static final String PRIVATE_CONSTANT_24 = "BULK_INDEX_STOPPED_NAME";

   @RBEntry("Indice di massa interrotto")
   @RBComment("Bulk Index Stopped")
   public static final String PRIVATE_CONSTANT_25 = "BULK_INDEX_STOPPED_DESCRIPTION";

   @RBEntry("Indice di massa interrotto")
   @RBComment("Bulk Index Stopped")
   public static final String PRIVATE_CONSTANT_26 = "BULK_INDEX_STOPPED_LONGDESCRIPTION";
}
