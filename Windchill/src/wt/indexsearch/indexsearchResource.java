package wt.indexsearch;

import wt.util.resource.*;

@RBUUID("wt.indexsearch.indexsearchResource")
public final class indexsearchResource extends WTListResourceBundle {
   /**
/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
    * rwareResource message resource bundle [English/US]
    **/
   @RBEntry("Cannot perform search because property {0} is not defined.")
   @RBComment("Error message displayed when an index property is not defined")
   @RBArgComment0("Name of the property that is not defined.")
   public static final String NO_INDEX_PROPERTY = "0";

   @RBEntry("RetrievalWare query did not run to completion. State is {0}.")
   @RBComment("Error message  if a query did not complete.")
   @RBArgComment0("Java constant for the state to which the query executed.")
   public static final String QUERY_NOT_COMPLETE = "1";

   @RBEntry("RetrievalWare field {0} has value {1} and is not a valid value.")
   @RBComment("Error message if the value of the RetrievalWare field is not formatted as expected.")
   @RBArgComment0("Name of the field stored in RetrievalWare.")
   @RBArgComment1("Value of the field named in {0}")
   public static final String OID_FIELD_FORMAT = "2";

   @RBEntry("There is a problem with your query or the connection with the Indexing Engine. Provide your administrator with this message and the time that you encountered this error.")
   @RBComment("This error occurs when the system can not connect to SOLR")
   public static final String INDEX_ENGINE_CONNECT = "3";

   @RBEntry("There is an error executing the query. Check your query syntax.")
   @RBComment("This error occurs when there is an error executing the query")
   public static final String INDEX_ENGINE_BAD_REQUEST = "4";

   @RBEntry("An error occurred while performing this index search. The search criteria is too broad for the search to be completed. Select additional criteria to narrow your search and then try again.")
   @RBComment("Message to display to a user when there is an too many boolean clauses error occurred and they need to make their criteria more specific to avoid this error.")
   public static final String OVER_MAX_BOOLEAN_CLAUSES_ERROR = "OVER_MAX_BOOLEAN_CLAUSES_ERROR";


   @RBEntry("Name")
   @RBComment("Keyword to be used while searching for objects by name.")
   public static final String PRIVATE_CONSTANT_0 = "KEYWORD_SEARCH_NAME";

   @RBEntry("Number")
   @RBComment("Keyword to be used while searching for objects by number.")
   public static final String PRIVATE_CONSTANT_1 = "KEYWORD_SEARCH_NUMBER";

   @RBEntry("Search Resource Limit Exceeded.  This is usually caused by too broad of a wildcard search.  Retry your search with a more specific keyword.")
   @RBComment("InStream error with a too broad wildcard search")
   public static final String PRIVATE_CONSTANT_2 = "RESOURCE_LIMIT_EXCEEDED";

   @RBEntry("Query Error.  The keyword could not be parsed as a valid Index Search query.")
   @RBComment("InStream error for a syntax error in the query")
   public static final String PRIVATE_CONSTANT_3 = "QUERY_ERROR";

   @RBEntry("Query timeout. Index Search did not respond within the query timeout limit.")
   @RBComment("InStream error for a timeout error")
   public static final String PRIVATE_CONSTANT_4 = "QUERY_TIMEOUT";

   @RBEntry("Concept Expansion Level")
   @RBComment("Title for the concept expansion preference")
   public static final String PRIVATE_CONSTANT_5 = "DISPLAY_CONCEPT_EXPANSION";

   @RBEntry("Choose the scope of concept mode in Keyword search.")
   @RBComment("Short description for the concept expansion preference")
   public static final String PRIVATE_CONSTANT_6 = "SHORT_DESC_CONCEPT_EXPANSION";

   @RBEntry("If you select <B>Concept</B> as your search mode, choose the extent to which search criteria are expanded to include related topics.  The system default is Simple variations.")
   @RBComment("Description for the concept expansion preference")
   public static final String PRIVATE_CONSTANT_7 = "DESC_CONCEPT_EXPANSION";

   @RBEntry("Exact matches only")
   @RBComment("Title for exact matches concept expansion level preference")
   public static final String PRIVATE_CONSTANT_8 = "DISPLAY_EXACT_MATCH_EXPANSION";

   @RBEntry("Simple variations")
   @RBComment("Title for simple variations concept expansion level preference")
   public static final String PRIVATE_CONSTANT_9 = "DISPLAY_SIMPLE_EXPANSION";

   @RBEntry("Most strongly related concepts")
   @RBComment("Title for most strongly related concepts concept expansion level preference")
   public static final String PRIVATE_CONSTANT_10 = "DISPLAY_MOST_STRONG_EXPANSION";

   @RBEntry("Strongly related concepts")
   @RBComment("Title for strongly related concepts concept expansion level preference")
   public static final String PRIVATE_CONSTANT_11 = "DISPLAY_STRONG_EXPANSION";

   @RBEntry("Weakly related concepts")
   @RBComment("Title for weakly related concepts concept expansion level preference")
   public static final String PRIVATE_CONSTANT_12 = "DISPLAY_WEAK_EXPANSION";

   @RBEntry("Search Mode")
   @RBComment("Title for the search mode preference")
   public static final String PRIVATE_CONSTANT_13 = "DISPLAY_SEARCH_MODE";

   @RBEntry("Choose the type of Keyword search to perform.")
   @RBComment("Short description for the search mode preference")
   public static final String PRIVATE_CONSTANT_14 = "SHORT_DESC_SEARCH_MODE";

   @RBEntry("Search Mode determines how text entered in the <B>Keyword</B> field of a search page is interpreted by the Solr search engine. <br> Simple - searches for the words and phrases you specify using the standard AND/OR Boolean operators.  The user chooses the AND or OR operator by selecting 'With all of these criteria'(AND operator) or 'With any of these criteria' (OR operator) from the Find drop-down menu on the Advanced Search page. <br> Advanced - allows users to enter a query in Solr Query Language syntax in the Keyword field.  The system default is Simple search mode.")
   @RBComment("Description for the search mode preference")
   public static final String PRIVATE_CONSTANT_15 = "DESC_SEARCH_MODE";

   @RBEntry("Simple")
   @RBComment("Choice for simple search mode preference")
   public static final String PRIVATE_CONSTANT_16 = "DISPLAY_SIMPLE_MODE";

   @RBEntry("Advanced")
   @RBComment("Choice for advanced search mode preference")
   public static final String PRIVATE_CONSTANT_17 = "DISPLAY_ADVANCED_MODE";

   @RBEntry("Library Preferences")
   @RBComment("Title for index library preferences")
   public static final String PRIVATE_CONSTANT_18 = "DISPLAY_INDEX_LIBRARIES";

   @RBEntry("Choose the libraries for Keyword search.")
   @RBComment("Short description for index library preferences")
   public static final String PRIVATE_CONSTANT_19 = "SHORT_DESC_INDEX_LIBRARIES";

   @RBEntry("Choose the libraries to include in your Keyword search. The system default is to choose all available libraries.")
   @RBComment("Full description for index library preferences")
   public static final String PRIVATE_CONSTANT_20 = "DESC_INDEX_LIBRARIES";

   @RBEntry("Quick Search")
   @RBComment("Quick search")
   public static final String PRIVATE_CONSTANT_21 = "QUICK_SEARCH_NAME";

   @RBEntry("Quick search")
   @RBComment("Quick search")
   public static final String PRIVATE_CONSTANT_22 = "QUICK_SEARCH_DESCRIPTION";

   @RBEntry("Quick search")
   @RBComment("Quick search")
   public static final String PRIVATE_CONSTANT_23 = "QUICK_SEARCH_LONGDESCRIPTION";

   @RBEntry("Bulk Index Stopped")
   @RBComment("Bulk Index Stopped")
   public static final String PRIVATE_CONSTANT_24 = "BULK_INDEX_STOPPED_NAME";

   @RBEntry("Bulk Index Stopped")
   @RBComment("Bulk Index Stopped")
   public static final String PRIVATE_CONSTANT_25 = "BULK_INDEX_STOPPED_DESCRIPTION";

   @RBEntry("Bulk Index Stopped")
   @RBComment("Bulk Index Stopped")
   public static final String PRIVATE_CONSTANT_26 = "BULK_INDEX_STOPPED_LONGDESCRIPTION";
}
