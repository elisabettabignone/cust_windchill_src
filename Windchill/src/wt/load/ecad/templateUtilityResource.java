/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.load.ecad;

import wt.util.resource.WTListResourceBundle;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.RBPseudo;

@RBUUID ("wt.load.ecad.templateUtilityResource")
public final class templateUtilityResource extends WTListResourceBundle  {
  /**
   * $$NONE
   **/
  //=======================================================================================================
  // java
  // common:
  @RBEntry("Usage: CreateECADDesign {xmlTemplate=<template_xml> | templateHolder=<templateHolderNumber>} [Language [Country [Variant]]]")
  public static final String USAGE = "tmplUsage";

  @RBEntry("{0} does not exists or is not a file")
  public static final String ILLEGAL_ARGUMENT = "tmplIllegalArgument";

  @RBEntry("Folder not found: {0}")
  public static final String FOLDER_NOT_FOUND = "tmplFolderNotFound";

  @RBEntry("{0} is a mandatory attribute.")
  public static final String MANDATORY_ATTRIBUTE = "tmplMandatoryAttribute";

  @RBEntry("{0} and {1} are mandatory attributes.")
  public static final String MANDATORY_ATTRIBUTES = "tmplMandatoryAttributes";

  @RBEntry("Undefined mandatory attribute: {0}")
  public static final String UNDEFINED_MANDATORY_ATTRIBUTE = "tmplUndefinedMandatoryAttribute";

  @RBEntry("Unrecognized tag: {0}")
  public static final String UNRECOGNIZED_TAG = "tmplUnrecognizedTag";

  @RBEntry("Internal error: Root element is not a design.")
  public static final String ROOT_NOT_DESIGN = "tmplRootNotDesign";

  @RBEntry("Unexpected element: {0}")
  public static final String UNEXPECTED_ELEMENT = "tmplUnexpectedElement";

  @RBEntry("Expected root element of type design or ecad_uwgm_definitions: {0}")
  public static final String UNEXPECTED_ROOT_ELEMENT = "tmplUnexpectedRootElement";

  @RBEntry("Template does not exist: {0}")
  public static final String NO_TEMPLATE = "tmplNoTemplate";

  @RBEntry("Contents in a template may be attached to documents only.")
  public static final String NOT_CONTENT_HOLDER = "tmplNotContentHolder";

  @RBEntry("Authoring applications of Board and Schematic should be different.")
  public static final String SAME_AUTH_APP = "tmplSameAuthApp";

  @RBEntry("Internal error: structure source is neither board nor schematic: {0}")
  public static final String UNEXPECTED_STRUCTURE_SOURCE = "tmplUnexpectedStructureSource";

  @RBEntry("Cannot establish structural link: No document.")
  public static final String NO_DOCUMENT = "tmplNoDocument";

  @RBEntry("Second part is not allowed.")
  public static final String SECOND_PART = "tmplSecondPart";

  @RBEntry("Document does not exist: {0}")
  public static final String DOCUMENT_DOES_NOT_EXIST = "tmplDocumentDoesNotExist";

  @RBEntry("Undefined attribute: {0}")
  public static final String UNDEFINED_ATTRIBUTE = "tmplUndefinedAttribute";

  @RBEntry("Internal error: {0} should not be called.")
  public static final String INTERNAL_PROHIBITED_CALL = "tmplInternalProhibitedCall";

  @RBEntry("Version name = {0} number = {1} is unknown for {2}")
  public static final String UNKNOWN_VERSION = "tmplUnknownVersion";

  @RBEntry("{0} resides in another folder: {1}")
  public static final String ANOTHER_FOLDER = "tmplAnotherFolder";

  @RBEntry("{0} belongs to another organization: {1}")
  public static final String ANOTHER_ORGANIZATION = "tmplAnotherOrganization";

  @RBEntry("Document {0} has different authoring application: {1}")
  public static final String DIFFERENT_AUTH_APP = "tmplDifferentAuthApp";

  @RBEntry("Part number is not specified and autonumbering is disabled in the object initialization rules.")
  public static final String NO_PART_NUMBER = "tmplNoPartNumber";

  @RBEntry("View does not exist: {0}")
  public static final String NO_VIEW = "tmplNoView";

  @RBEntry("Part exists in another view: {0}")
  public static final String ANOTHER_VIEW = "tmplAnotherView";

  @RBEntry("Part has a different name: {0}")
  public static final String ANOTHER_PART_NAME = "tmplAnotherPartName";

  @RBEntry("Second part not allowed for {0}")
  public static final String SECOND_PART_NOT_ALLOWED = "tmplSecondPartNotAllowed";

  @RBEntry("Unexpected part number rule: {0}")
  public static final String UNEXPECTED_PART_NUMBER_RULE = "tmplUnexpectedPartNumberRule";

  @RBEntry("Derived document does not have an associated part.")
  public static final String NO_PART_FOR_DERIVED = "tmplNoPartForDerived";

  @RBEntry("Only one top generic document is allowed: {0}")
  public static final String MULTIPLE_GENERIC = "tmplMultipleGeneric";

  @RBEntry("Multiple assembly parts encountered.")
  public static final String MULTIPLE_ASSEMBLY = "tmplMultipleAssembly";

  @RBEntry("The same part cannot be referenced by two documents: {0}")
  public static final String MULTIPLY_REFERENCED_PART = "tmplMultiplyReferencedPart";

  @RBEntry("Either board or schematic is necessary to identify a design.")
  public static final String NO_BOARD_SCHEMATIC = "tmplNoBoardSchematic";

  @RBEntry("Internal error: wrong {0} iteration fetched.")
  public static final String INTERNAL_WRONG_ITERATION = "tmplInternalWrongIteration";

  @RBEntry("Unsupported hook type: {0}")
  public static final String UNSUPPORTED_HOOK_TYPE = "tmplUnsupportedHookType";

  @RBEntry("Attempt to redefine hook: {0}")
  public static final String REDEFINE_HOOK = "tmplRedefineHook";

  @RBEntry("{0} must be present in a non-separable design.")
  public static final String MANDATORY_DOC = "tmplMandatoryDoc";

  @RBEntry("Authoring applications of Board and Generic should be the same.")
  public static final String AUTH_APP_NON_SEP = "tmplAuthAppNonSep";

  @RBEntry("Internal error. The parentAction must be {0}, found {1}.")
  public static final String INTERNAL_WRONG_PARENT = "tmplInternalWrongParent";

  @RBEntry("Generic schematic having instances may not have a part.")
  public static final String UNEXPECTED_PART = "tmplUnexpectedPart";

  @RBEntry("Current participant has no modify access over {0}")
  public static final String NO_ACCESS = "tmplNoAccess";

  @RBEntry("Owner application must be {0}, given {1}")
  public static final String WRONG_OWNER_APPLICATION = "tmplWrongOwnerApplication";
}
