/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.load.ecad;

import wt.util.resource.WTListResourceBundle;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.RBPseudo;

@RBUUID ("wt.load.ecad.templateUtilityResource")
public final class templateUtilityResource_it extends WTListResourceBundle  {
  /**
   * $$NONE
   **/
  //=======================================================================================================
  // java
  // common:
  @RBEntry("Utilizzo: CreateECADDesign {xmlTemplate=<modello_xml> | templateHolder=<numeroContenitoreModelli>} [Lingua [Paese [Variante]]]")
  public static final String USAGE = "tmplUsage";

  @RBEntry("{0} non esiste o non è un file")
  public static final String ILLEGAL_ARGUMENT = "tmplIllegalArgument";

  @RBEntry("Impossibile trovare la cartella: {0}")
  public static final String FOLDER_NOT_FOUND = "tmplFolderNotFound";

  @RBEntry("{0} è un attributo obbligatorio.")
  public static final String MANDATORY_ATTRIBUTE = "tmplMandatoryAttribute";

  @RBEntry("{0} e {1} sono attributi obbligatori.")
  public static final String MANDATORY_ATTRIBUTES = "tmplMandatoryAttributes";

  @RBEntry("Attributo obbligatorio non definito: {0}")
  public static final String UNDEFINED_MANDATORY_ATTRIBUTE = "tmplUndefinedMandatoryAttribute";

  @RBEntry("Tag non riconosciuto: {0}")
  public static final String UNRECOGNIZED_TAG = "tmplUnrecognizedTag";

  @RBEntry("Errore interno: l'elemento radice non è un progetto.")
  public static final String ROOT_NOT_DESIGN = "tmplRootNotDesign";

  @RBEntry("Elemento non previsto: {0}")
  public static final String UNEXPECTED_ELEMENT = "tmplUnexpectedElement";

  @RBEntry("Elemento radice previsto di tipo progetto o ecad_uwgm_definitions: {0}")
  public static final String UNEXPECTED_ROOT_ELEMENT = "tmplUnexpectedRootElement";

  @RBEntry("Il modello non esiste: {0}")
  public static final String NO_TEMPLATE = "tmplNoTemplate";

  @RBEntry("I contenuti di un modello possono essere allegati solo ai documenti.")
  public static final String NOT_CONTENT_HOLDER = "tmplNotContentHolder";

  @RBEntry("Le applicazioni di creazione di elementi scheda e schema devono essere diverse.")
  public static final String SAME_AUTH_APP = "tmplSameAuthApp";

  @RBEntry("Errore interno: l'origine struttura non è né un elemento scheda né schema: {0}")
  public static final String UNEXPECTED_STRUCTURE_SOURCE = "tmplUnexpectedStructureSource";

  @RBEntry("Impossibile stabilire link strutturale: nessun documento presente.")
  public static final String NO_DOCUMENT = "tmplNoDocument";

  @RBEntry("Seconda parte non consentita.")
  public static final String SECOND_PART = "tmplSecondPart";

  @RBEntry("Documento non esistente: {0}")
  public static final String DOCUMENT_DOES_NOT_EXIST = "tmplDocumentDoesNotExist";

  @RBEntry("Attributo non definito: {0}")
  public static final String UNDEFINED_ATTRIBUTE = "tmplUndefinedAttribute";

  @RBEntry("Errore interno: {0} non deve essere chiamato.")
  public static final String INTERNAL_PROHIBITED_CALL = "tmplInternalProhibitedCall";

  @RBEntry("Nome versione = {0} numero = {1} sconosciuto per {2}")
  public static final String UNKNOWN_VERSION = "tmplUnknownVersion";

  @RBEntry("{0} si trova in un'altra cartella: {1}")
  public static final String ANOTHER_FOLDER = "tmplAnotherFolder";

  @RBEntry("{0} appartiene a un'altra organizzazione: {1}")
  public static final String ANOTHER_ORGANIZATION = "tmplAnotherOrganization";

  @RBEntry("Il documento {0} ha  un'applicazione di creazione diversa: {1}")
  public static final String DIFFERENT_AUTH_APP = "tmplDifferentAuthApp";

  @RBEntry("Il numero di parte non è specificato e la numerazione automatica è disattivata nelle regole di inizializzazione oggetto.")
  public static final String NO_PART_NUMBER = "tmplNoPartNumber";

  @RBEntry("Vista non esistente: {0}")
  public static final String NO_VIEW = "tmplNoView";

  @RBEntry("La parte esiste in un'altra vista: {0}")
  public static final String ANOTHER_VIEW = "tmplAnotherView";

  @RBEntry("La parte ha un nome diverso: {0}")
  public static final String ANOTHER_PART_NAME = "tmplAnotherPartName";

  @RBEntry("Seconda parte non consentita per {0}")
  public static final String SECOND_PART_NOT_ALLOWED = "tmplSecondPartNotAllowed";

  @RBEntry("Regola numero di parte imprevista: {0}")
  public static final String UNEXPECTED_PART_NUMBER_RULE = "tmplUnexpectedPartNumberRule";

  @RBEntry("Il documento derivato non dispone di una parte associata.")
  public static final String NO_PART_FOR_DERIVED = "tmplNoPartForDerived";

  @RBEntry("È consentito un unico documento generico di livello superiore: {0}")
  public static final String MULTIPLE_GENERIC = "tmplMultipleGeneric";

  @RBEntry("Rilevate più parti di assieme.")
  public static final String MULTIPLE_ASSEMBLY = "tmplMultipleAssembly";

  @RBEntry("Due documenti non possono fare riferimento alla stessa parte: {0}")
  public static final String MULTIPLY_REFERENCED_PART = "tmplMultiplyReferencedPart";

  @RBEntry("Elemento scheda o schema necessaria per identificare un progetto.")
  public static final String NO_BOARD_SCHEMATIC = "tmplNoBoardSchematic";

  @RBEntry("Errore interno: recuperata iterazione {0} errata.")
  public static final String INTERNAL_WRONG_ITERATION = "tmplInternalWrongIteration";

  @RBEntry("Tipo di hook non supportato: {0}")
  public static final String UNSUPPORTED_HOOK_TYPE = "tmplUnsupportedHookType";

  @RBEntry("Tenta di ridefinire l'hook: {0}")
  public static final String REDEFINE_HOOK = "tmplRedefineHook";

  @RBEntry("{0} deve essere presente in un progetto non separabile.")
  public static final String MANDATORY_DOC = "tmplMandatoryDoc";

  @RBEntry("Le applicazioni di creazione di scheda e generico devono essere uguali.")
  public static final String AUTH_APP_NON_SEP = "tmplAuthAppNonSep";

  @RBEntry("Errore interno. L'azione padre deve essere {0}, in {1}.")
  public static final String INTERNAL_WRONG_PARENT = "tmplInternalWrongParent";

  @RBEntry("Lo schema generico con istanze non può includere una parte.")
  public static final String UNEXPECTED_PART = "tmplUnexpectedPart";

  @RBEntry("Partecipante corrente privo dei diritti di accesso per la modifica a {0}")
  public static final String NO_ACCESS = "tmplNoAccess";

  @RBEntry("L'applicazione proprietario deve essere {0}, specificato {1}")
  public static final String WRONG_OWNER_APPLICATION = "tmplWrongOwnerApplication";
}
