/* bcwti
 *
 * Copyright (c) 2013 PTC, Inc. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement.
 * You shall not disclose such confidential information and shall use it
 * only in accordance with the terms of the license agreement.
 *
 * ecwti
 */
package wt.load;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBArgComment1;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

/**
 * The resource bundle for the {@link WindchillLoader}.
 *
 * <BR><BR><B>Supported API: </B>false
 * <BR><BR><B>Extendable: </B>false
 */
@RBUUID("wt.load.loadResource")
public final class loadResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile trovare il file \"{0}\"")
   @RBArgComment0("File name")
   public static final String CANNOT_FIND_FILE = "cannot_find_file";

   @RBEntry("È stato rilevato un errore durante l'inizializzazione dell'utente \"{0}\"")
   @RBArgComment0("User name")
   public static final String ERROR_INIT_USER = "error_init_user";

   @RBEntry("Il caricatore Windchill ha rilevato un'installazione incompleta. Installare almeno un'applicazione prima di effettuare il caricamento.")
   public static final String INCOMPLETE_INSTALLATION = "incomplete_installation";

   @RBEntry("Specificare il file di dati alla riga di comando.")
   public static final String NO_DATA_FILE = "0";

   @RBEntry("File di dati non leggibile \"{0}\".")
   @RBArgComment0("refers to data filename")
   public static final String DATA_FILE_NOT_READABLE = "1";

   @RBEntry("File mappa non leggibile \"{0}\".")
   @RBArgComment0("refers to map filename")
   public static final String MAP_FILE_NOT_READABLE = "2";

   @RBEntry("Campo obbligatorio \"{0}\" mancante nel file di dati.")
   @RBArgComment0("refers to fieldname")
   public static final String REQUIRED_FIELD_MISSING = "3";

   @RBEntry("{0} riuscito alla riga {1}:")
   @RBArgComment0("refers to generic method to be performed on data file")
   @RBArgComment1("refers to line number from data file")
   public static final String SUCCESS = "4";

   @RBEntry("ERRORE in {0} alla riga {1}:")
   @RBArgComment0("refers to generic method to be performed on data file")
   @RBArgComment1("refers to line number from data file")
   public static final String FAILURE = "5";

   @RBEntry("ERRORE nell'impostazione del valore di cache per chiave = {0}, {1}.")
   @RBArgComment0("refers to first key into the general load cache")
   @RBArgComment1("refers to second key into specific cache for this class")
   public static final String SETCACHE = "6";

   @RBEntry("Errore durante l'importazione dei dati dal file {1}. Importazione interrotta alla riga {0}. Controllare il log del method server per ulteriori informazioni.")
   @RBArgComment0("refers to line number from data file")
   @RBArgComment1("refers to the name of the data file")
   public static final String IMPORT_ERROR = "7";

   @RBEntry("L'importazione dei dati dal file {1} è completa. Sono stati riscontrati errori nelle righe {0}. È possibile che alcuni dati non siano stati importati correttamente. Controllare il log del method server per ulteriori informazioni.")
   @RBArgComment0("refers to line number from data file")
   @RBArgComment1("refers to the name of the data file")
   public static final String IMPORT_WARNING = "8";

   @RBEntry("Importazione della riga {1} del file {0}. Quando l'importazione è terminata, controllare l'esistenza di eventuali errori e avvertenze sul log del method server.")
   @RBArgComment0("refers to the name of the data file")
   @RBArgComment1("refers to line number from data file")
   public static final String IMPORT_STATUS = "9";

   @RBEntry("   Il caricatore Windchill caricherà nel database i dati necessari alle applicazioni Windchill correntemente installate.")
   public static final String LOAD_DESCRIPTION = "10";

   @RBEntry("Descrizione:")
   public static final String DESCRIPTION = "11";

   @RBEntry("Uso:")
   public static final String USAGE = "12";

   @RBEntry("Argomenti:")
   @RBComment("The command line switches for the application")
   public static final String SWITCHES = "13";

   @RBEntry("Installa SOLO i dati demo del prodotto installato")
   public static final String ONLYDEMO = "14";

   @RBEntry("Includi i dati demo nel carico (se disponibili)")
   public static final String DEMO = "15";

   @RBEntry("Installa SOLO i dati di test dei prodotti installati")
   public static final String ONLYTEST = "16";

   @RBEntry("Includi i dati di test nel carico (se disponibili)")
   public static final String TEST = "17";

   @RBEntry("Installa SOLO le applicazioni contenute nell'elenco seguente. Le applicazioni devono essere separate da ',' e gli ID delle applicazioni devono corrispondere ai valori delle applicazioni contenuti in Installed.properties in $WT_HOME/codebase.")
   public static final String APPLICATIONS = "18";

   @RBEntry("Installazione dei dati demo per TUTTI i prodotti installati")
   public static final String EXAMPLE1 = "19";

   @RBEntry("Installazione dei soli dati Windchill PDMLink")
   public static final String EXAMPLE2 = "20";

   @RBEntry("Esempi:")
   public static final String EXAMPLE = "21";

   @RBEntry("Esegui il caricatore Windchill in modalità automatica (nessun prompt)")
   public static final String UNATTENDED = "22";

   @RBEntry("Visualizza l'elenco delle applicazioni installate con load set validi.")
   public static final String INFO = "23";

   @RBEntry("Applicazioni caricabili:")
   public static final String LIST = "24";

   @RBEntry("ERRORE: sono stati specificati degli argomenti non validi.")
   public static final String ARG_ERROR = "25";

   @RBEntry("Carica tutti i set di dati per tutti i prodotti installati.")
   public static final String ALL = "26";

   @RBEntry("ERRORE: sono state specificate delle applicazioni non presenti nell'elenco di applicazioni caricabili:\n {0}")
   public static final String INVALID_APPLICATION = "27";

   @RBEntry("Argomenti per l'autenticazione (per evitare l'accesso tramite finestra di autenticazione):")
   public static final String AUTHENTICATION_ARGS = "28";

   @RBEntry("Nome utente per l'accesso")
   public static final String USER = "29";

   @RBEntry("Password utente")
   public static final String PASSWORD = "30";

   @RBEntry("Specificare le impostazioni locali per il caricamento dei dati. Per il caricamento dei dati in lingua giapponese, ad esempio, specificare -Locale=ja")
   public static final String LOCALE = "31";

   @RBEntry("Interrompi caricamento dei dati in caso di errore")
   public static final String ABORT_ON_ERROR = "32";

   @RBEntry("Caricare {0}? [y/n]")
   @RBComment("[y/n] in the above message should be left untranslated")
   @RBArgComment0("Localized name of application or preference to be loaded")
   public static final String LOAD_QUESTION = "33";

   @RBEntry("Riavviare i server? [y/n]")
   @RBComment("[y/n] in the above message should be left untranslated")
   public static final String RESTART_SERVER = "34";

   @RBEntry("Eseguire wt.admin.Install? [y/n]")
   @RBComment("[y/n] in the above message should be left untranslated")
   public static final String EXECUTE_INSTALL = "35";

   @RBEntry("Installa i nuovi dati aggiunti fra le release 9.0 e 9.1.  Specificare il nome e percorso completo dei file da caricare.")
   public static final String LOADFILE = "36";

   @RBEntry("Esempi di manutenzione:")
   public static final String MAINTENANCE_EXAMPLES = "37";

   @RBEntry(" Caricamento nuovi dati aggiunti fra le release 9.0 e 9.1 in corso...")
   public static final String MAINTENANCE_DESCRIPTION = "38";

   @RBEntry("Installa i nuovi dati introdotti con la release 9.0 M30 e tutti i nuovi dati per la release 9.1 per l'installazione di Foundation")
   public static final String MAINTENANCE_EXAMPLE1 = "39";

   @RBEntry("Installa i nuovi dati introdotti con la release 9.1 per Foundation e PDMLink ")
   public static final String MAINTENANCE_EXAMPLE2 = "40";

   @RBEntry("Installa il load set di manutenzione di Windchill Foundation per i sistemi di release comprese fra 9.0 e 9.1")
   public static final String PRIVATE_CONSTANT_0 = "wt/load/foundationLoadR90toR91.xml";

   @RBEntry("Installa il load set di manutenzione di Windchill Foundation introdotto con la release 9.0 M30")
   public static final String PRIVATE_CONSTANT_1 = "wt/load/foundationLoadR90thruR90M20.xml";

   @RBEntry("Installa il Load-Set di Windchill Foundation")
   public static final String PRIVATE_CONSTANT_2 = "wt/load/foundationLoad.xml";

   @RBEntry("Installa il Load-Set dimostrativo di Windchill Foundation")
   public static final String PRIVATE_CONSTANT_3 = "wt/load/foundationDemo.xml";

   @RBEntry("Installa il Load-Set di test di Windchill Foundation")
   public static final String PRIVATE_CONSTANT_4 = "wt/load/foundationTest.xml";

   @RBEntry("Installa il Load-Set di Windchill PDM")
   public static final String PRIVATE_CONSTANT_5 = "wt/load/fdnpdmLoad.xml";

   @RBEntry("Installa il Load-Set dimostrativo di Windchill PDM")
   public static final String PRIVATE_CONSTANT_6 = "wt/load/fdnpdmDemo.xml";

   @RBEntry("Installa il Load-Set di manutenzione di Windchill Foundation")
   public static final String PRIVATE_CONSTANT_7 = "wt/load/foundationLoadR9M020.xml";

   @RBEntry("Installa il Load-Set di Pianificazione modifiche")
   public static final String PRIVATE_CONSTANT_8 = "wt/change2/changeplanning/load/changeplanningLoad.xml";

   @RBEntry("Installa il Load-Set di Windchill Gateway for Cadence Allegro Design Workbench (ADW)")
   public static final String PRIVATE_CONSTANT_9 = "com/ptc/windchill/ecadlibsync/load/ECADLibSyncServerLoad.xml";

   @RBEntry("Installa il Load-Set di Arbortext Content Manager")
   public static final String PRIVATE_CONSTANT_10 = "com/ptc/windchill/pdmlink/load/atcmLoad.xml";

   @RBEntry("Installa il Load-Set di PDMLink")
   public static final String PRIVATE_CONSTANT_11 = "com/ptc/windchill/pdmlink/load/pdmlinkLoad.xml";

   @RBEntry("Installa il Load-Set dimostrativo di PDMLink")
   public static final String PRIVATE_CONSTANT_12 = "com/ptc/windchill/pdmlink/load/pdmlinkDemo.xml";

   @RBEntry("Installa il Load-Set di test di PDMLink")
   public static final String PRIVATE_CONSTANT_13 = "com/ptc/windchill/pdmlink/load/pdmlinkTest.xml";

   @RBEntry("Installa il Load-Set di manutenzione di PDMLink")
   public static final String PRIVATE_CONSTANT_14 = "com/ptc/windchill/pdmlink/load/pdmlinkLoadR91.xml";

   @RBEntry("Installa il Load-Set di manutenzione di PDMLink")
   public static final String PRIVATE_CONSTANT_15 = "com/ptc/windchill/pdmlink/load/pdmlinkLoadR9M020.xml";

   @RBEntry("Installa il Load-Set del processo di modifica a ciclo chiuso")
   public static final String PRIVATE_CONSTANT_16 = "com/ptc/windchill/pdmlink/load/cmiiLoad.xml";

   @RBEntry("Installa il set dimostrativo per il processo di modifica a ciclo chiuso")
   public static final String PRIVATE_CONSTANT_17 = "com/ptc/windchill/pdmlink/load/cmiiDemo.xml";

   @RBEntry("Installa il Load-Set di ProjectLink")
   public static final String PRIVATE_CONSTANT_18 = "com/ptc/windchill/projectlink/load/projectlinkLoad.xml";

   @RBEntry(" Installa il Load-Set dimostrativo di ProjectLink")
   public static final String PRIVATE_CONSTANT_19 = "com/ptc/windchill/projectlink/load/projectlinkDemo.xml";

   @RBEntry("Installa il Load-Set di test di ProjectLink")
   public static final String PRIVATE_CONSTANT_20 = "com/ptc/windchill/projectlink/load/projectlinkTest.xml";

   @RBEntry("Installa il Load-Set di ESI")
   public static final String PRIVATE_CONSTANT_21 = "com/ptc/windchill/esi/load/esiLoad.xml";

   @RBEntry("Installa il Load-Set dimostrativo di ESI")
   public static final String PRIVATE_CONSTANT_22 = "com/ptc/windchill/esi/load/esiDemo.xml";

   @RBEntry("Installa il Load-Set di manutenzione di ESI  per i sistemi in migrazione dalla release 9.0 alla 9.1")
   public static final String PRIVATE_CONSTANT_23 = "com/ptc/windchill/esi/load/esiLoadR91.xml";

   @RBEntry("Installa il Load-Set di PartsLink")
   public static final String PRIVATE_CONSTANT_24 = "com/ptc/windchill/partslink/load/partslinkLoad.xml";

   @RBEntry("Installa il Load-Set di SCMI")
   public static final String PRIVATE_CONSTANT_25 = "com/ptc/windchill/scm/load/scmLoad.xml";

   @RBEntry("Installa il Load-Set di test di Importazione/esportazione fogli di lavoro")
   @RBComment("This is used in test data loading. It is the CSV title of Spreadsheet Importer Exporter Test Load-Set installation.")
   public static final String PRIVATE_CONSTANT_26 = "com/ptc/windchill/ixb/load/ixbTest.xml";

   @RBEntry("Installa il Load-Set di Gestione fornitori")
   public static final String PRIVATE_CONSTANT_27 = "com/ptc/windchill/suma/load/sumaLoad.xml";

   @RBEntry("Installa il Load-Set dimostrativo di Gestione fornitori")
   public static final String PRIVATE_CONSTANT_28 = "com/ptc/windchill/suma/load/sumaDemo.xml";

   @RBEntry("Installa il Load-Set di test di Gestione fornitori")
   public static final String PRIVATE_CONSTANT_29 = "com/ptc/windchill/suma/load/sumaTest.xml";

   @RBEntry("Installa il Load-Set di Environmental Compliance")
   @RBComment("This is used in attributes and types data loading for Environmental Compliance. It is the CSV title of Environmental Compliance Load-Set installation.")
   public static final String PRIVATE_CONSTANT_30 = "com/ptc/windchill/insight/environment/load/environmentLoad.xml";

   @RBEntry("Installa il Load-Set di CostMgmt")
   @RBComment("The CSV title of CostMgmt Load-Set installation.")
   public static final String PRIVATE_CONSTANT_31 = "com/ptc/windchill/cost/load/costLoad.xml";

   @RBEntry("Installa il Load-Set di test di CostMgmt")
   @RBComment("The CSV title of CostMgmt Test Load-Set installation.")
   public static final String PRIVATE_CONSTANT_32 = "com/ptc/windchill/cost/load/costTest.xml";

   @RBEntry("Installa il Load-Set dimostrativo di CostMgmt")
   @RBComment("The CSV title of CostMgmt Demo Load-Set installation.")
   public static final String PRIVATE_CONSTANT_33 = "com/ptc/windchill/cost/load/costDemo.xml";

   @RBEntry("Installa il Load-Set di Options and Variants")
   public static final String PRIVATE_CONSTANT_34 = "com/ptc/wpcfg/load/wpcfgLoad.xml";

   @RBEntry("Installa il Load-Set dimostrativo di Options and Variants")
   public static final String PRIVATE_CONSTANT_35 = "com/ptc/wpcfg/load/wpcfgDemo.xml";

   @RBEntry("Installa il Load-Set di test di Options and Variants")
   public static final String PRIVATE_CONSTANT_36 = "com/ptc/wpcfg/load/wpcfgTest.xml";

   @RBEntry("Installa il Load-Set di QA test di Options and Variants")
   public static final String PRIVATE_CONSTANT_37 = "com/ptc/wpcfg/load/wpcfgQATest.xml";

   @RBEntry("Installa il Load-Set di MPMLink")
   public static final String PRIVATE_CONSTANT_38 = "com/ptc/windchill/mpml/load/mpmlLoad.xml";

   @RBEntry("Installa il Load-Set dimostrativo di MPMLink Management")
   public static final String PRIVATE_CONSTANT_39 = "com/ptc/windchill/mpml/load/mpmlDemo.xml";

   @RBEntry("Installa il Load-Set di manutenzione di MPMLink")
   public static final String PRIVATE_CONSTANT_40 = "com/ptc/windchill/mpml/load/mpmlinkLoadR91.xml";

   @RBEntry("Installa il Load-Set di manutenzione di MPMLink")
   public static final String PRIVATE_CONSTANT_41 = "com/ptc/windchill/mpml/load/mpmlLoadR9M020.xml";

   @RBEntry("Installa il Load-Set di manutenzione di MPMLink")
   public static final String PRIVATE_CONSTANT_42 = "com/ptc/windchill/mpml/load/mpmlinkLoadR10.xml";

   @RBEntry("Installa il Load-Set di WADM")
   public static final String PRIVATE_CONSTANT_43 = "com/ptc/windchill/wadm/load/wadmLoad.xml";

   @RBEntry("Installa il Load-Set dimostrativo di Visualization")
   public static final String PRIVATE_CONSTANT_44 = "com/ptc/wvs/wvsDemo.xml";

   @RBEntry("Installa il Load-Set di Assemble-to-Order")
   public static final String PRIVATE_CONSTANT_45 = "com/ptc/windchill/option/load/atoLoad.xml";

   @RBEntry("Installa il Load-Set dimostrativo di Assemble-to-Order")
   public static final String PRIVATE_CONSTANT_46 = "com/ptc/windchill/option/load/atoDemo.xml";

   @RBEntry("Installa il Load-Set di test di Assemble-to-Order")
   public static final String PRIVATE_CONSTANT_47 = "com/ptc/windchill/option/load/atoTest.xml";

   @RBEntry("Installa il Load-Set di Platform Structures")
   public static final String PLATFORM_STRUCTURES_LOAD = "com/ptc/windchill/option/load/wpsLoad.xml";

   @RBEntry("Installa il Load-Set dimostrativo di Platform Structures")
   public static final String PLATFORM_STRUCTURES_DEMO = "com/ptc/windchill/option/load/wpsDemo.xml";

   @RBEntry("Installa il Load-Set di test di Platform Structures")
   public static final String PLATFORM_STRUCTURES_TEST = "com/ptc/windchill/option/load/wpsTest.xml";

   @RBEntry("Installa il Load-Set di Airbus")
   public static final String PRIVATE_CONSTANT_48 = "com/ptc/windchill/enterprise/cids/load/airbusLoad.xml";

   @RBEntry("Installa il Load-Set dimostrativo di Airbus")
   public static final String PRIVATE_CONSTANT_49 = "com/ptc/windchill/enterprise/cids/load/airbusDemo.xml";

   @RBEntry("Installa il Load-Set di Windchill Gateway for I-deas TDM")
   public static final String PRIVATE_CONSTANT_50 = "com/ptc/windchill/dpimpl/load/dpimplLoad.xml";

   @RBEntry("Caricatore dati di Windchill Gateway for I-deas TDM")
   public static final String PRIVATE_CONSTANT_51 = "com/ptc/windchill/dpimpl/load/IdeasTDMLoad.xml";

   @RBEntry("Caricatore dati di WPC")
   public static final String PRIVATE_CONSTANT_52 = "com/ptc/wpc/load/WPCServerLoad.xml";

   @RBEntry("Installa il Load-Set di FORAN Integration")
   public static final String PRIVATE_CONSTANT_53 = "com/ptc/windchill/dpimpl/load/ForanLoad.xml";

   @RBEntry("Caricatore di Windchill Maturity Test")
   public static final String PRIVATE_CONSTANT_54 = "com/ptc/windchill/enterprise/maturity/load/maturityTestLoad.xml";

   @RBEntry("Cambia caricatore di Test")
   public static final String PRIVATE_CONSTANT_55 = "com/ptc/windchill/enterprise/change2/load/changeTestLoad.xml";

   @RBEntry("Installa il Load-Set di Windchill RequirementsLink")
   public static final String PRIVATE_CONSTANT_57 = "com/ptc/windchill/enterprise/requirement/load/requirementslinkLoad.xml";

   @RBEntry("\n\nBenvenuti nell'utilità d'impostazione database per Windchill e le soluzioni Windchill.\n\nVerranno visualizzate una serie di finestre di prompt.\nSe i dati di base sono già stati installati durante un'operazione\ndi caricamento precedente, non è necessario ricaricarli.\n----------------------------------------------------------------")
   public static final String PRIVATE_CONSTANT_58 = "OpeningMessage";

   @RBEntry("Utenti")
   public static final String PRIVATE_CONSTANT_59 = "users_csv_title";

   @RBEntry("Gruppi")
   public static final String PRIVATE_CONSTANT_60 = "groups_csv_title";

   @RBEntry("Domini")
   public static final String PRIVATE_CONSTANT_61 = "domains_csv_title";

   @RBEntry("Schedari")
   public static final String PRIVATE_CONSTANT_62 = "cabinets_csv_title";

   @RBEntry("Cartelle")
   public static final String PRIVATE_CONSTANT_63 = "folders_csv_title";

   @RBEntry("Regole d'accesso")
   public static final String PRIVATE_CONSTANT_64 = "accessrules_csv_title";

   @RBEntry("Dati demo ciclo di vita")
   public static final String PRIVATE_CONSTANT_65 = "lcdd_csv_title";

   @RBEntry("Doc generali")
   public static final String PRIVATE_CONSTANT_66 = "gendocs_csv_title";

   @RBEntry("Doc specifiche")
   public static final String PRIVATE_CONSTANT_67 = "specdocs_csv_title";

   @RBEntry("Doc requisiti")
   public static final String PRIVATE_CONSTANT_68 = "reqdocs_csv_title";

   @RBEntry("Viste")
   public static final String PRIVATE_CONSTANT_69 = "views_csv_title";

   @RBEntry("Parti")
   public static final String PRIVATE_CONSTANT_70 = "parts_csv_title";

   @RBEntry("Demo workflow")
   public static final String PRIVATE_CONSTANT_71 = "workdemo_csv_title";

   @RBEntry("Regole di indicizzazione")
   public static final String PRIVATE_CONSTANT_72 = "indexrules_csv_title";

   @RBEntry("Definizioni attributi d'istanza")
   public static final String PRIVATE_CONSTANT_73 = "ibaadef_csv_title";

   @RBEntry("Modelli di report")
   public static final String PRIVATE_CONSTANT_74 = "reptmpl_csv_title";

   @RBEntry("Dati amministratore AUT (Automated Unit Test)")
   public static final String PRIVATE_CONSTANT_75 = "autadmin_csv_title";

   @RBEntry("Formati dati")
   public static final String PRIVATE_CONSTANT_76 = "dataformats_csv_title";

   @RBEntry("Dati amministrativi vari e regole d'accesso")
   public static final String PRIVATE_CONSTANT_77 = "miscadmin_csv_title";

   @RBEntry("Dati di base di workflow")
   public static final String PRIVATE_CONSTANT_78 = "wrkflwbsdata_csv_title";

   @RBEntry("Gruppo autori workflow")
   public static final String PRIVATE_CONSTANT_79 = "wrkflauthors_csv_title";

   @RBEntry("Preferenze correlate al workflow")
   public static final String PRIVATE_CONSTANT_80 = "workflow_work_pref_title";

   @RBEntry("Preferenze correlate alla tabella delle assegnazioni")
   public static final String PRIVATE_CONSTANT_81 = "assignments_table_pref_title";

   @RBEntry("Dati di base del ciclo di vita")
   public static final String PRIVATE_CONSTANT_82 = "lfcyclebsdata_csv_title";

   @RBEntry("Definizioni sistemi di misura")
   public static final String PRIVATE_CONSTANT_83 = "msd_csv_title";

   @RBEntry("Definizioni quantità di misura")
   public static final String PRIVATE_CONSTANT_84 = "qomd_csv_title";

   @RBEntry("Preferenze")
   public static final String PRIVATE_CONSTANT_85 = "prefs_csv_title";

   @RBEntry("Gestione repliche - Cicli di vita e workflow")
   public static final String PRIVATE_CONSTANT_86 = "rmlaw_csv_title";

   @RBEntry("Modelli di report di workflow")
   public static final String PRIVATE_CONSTANT_87 = "wrkreptemp_csv_title";

   @RBEntry("Repository radice PDMLink e tipo di documento di riferimento")
   public static final String PRIVATE_CONSTANT_88 = "prrardt_csv_title";

   @RBEntry("Regole di modello team e ciclo di vita")
   public static final String PRIVATE_CONSTANT_89 = "lcattr_csv_title";

    @RBEntry("Preferenza gestione parti e indicatore di riferimento")
    public static final String PRIVATE_CONSTANT_90 = "partManagement_csv_title";

    @RBEntry("Gestione modifiche - Cicli di vita e workflow")
    public static final String PRIVATE_CONSTANT_91 = "ChangeManagement_csv_title";

    @RBEntry("Workflow Gestione modifiche")
    public static final String PRIVATE_CONSTANT_92 = "changemgmtworkflows_csv_title";

    @RBEntry("Cicli di vita Gestione modifiche")
    public static final String PRIVATE_CONSTANT_93 = "changemgmtlc_csv_title";

    @RBEntry("Modello di team Gestione modifiche")
    public static final String PRIVATE_CONSTANT_94 = "changemgmtett_csv_title";

    @RBEntry("Modelli di report Monitor modifiche")
    public static final String PRIVATE_CONSTANT_95 = "changemonreptpl_csv_title";

    @RBEntry("Preferenze PDMLink")
    public static final String PRIVATE_CONSTANT_96 = "pdmlinkprefs_csv_title";

    @RBEntry("Dati di produzione per oggetti PDMLink")
    public static final String PRIVATE_CONSTANT_97 = "pdmlinkproddata_csv_title";

    @RBEntry("Regole di accesso PDMLink")
    public static final String PRIVATE_CONSTANT_98 = "pdmlinkaccessrules_csv_title";

    @RBEntry("Regole d'indicizzazione PDMLink")
    public static final String PRIVATE_CONSTANT_99 = "pdmlinkindexrules_csv_title";

    @RBEntry("Contenitori")
    public static final String PRIVATE_CONSTANT_100 = "container_csv_title";

    @RBEntry("Modelli di contenitore")
    public static final String PRIVATE_CONSTANT_101 = "container_templates_csv_title";

    @RBEntry("Regole di inizializzazione del ciclo di vita")
    public static final String PRIVATE_CONSTANT_102 = "LifecycleInitRules_csv_title";

    @RBEntry("Regole d'inizializzazione")
    public static final String PRIVATE_CONSTANT_103 = "InitilizationRules_csv_title";

    @RBEntry("Cicli di vita comuni")
    public static final String PRIVATE_CONSTANT_104 = "CommonLifeCycles_csv_title";

    @RBEntry("Contenitore Windchill PDM")
    public static final String PRIVATE_CONSTANT_105 = "WindchillPDMContainer_csv_title";

    @RBEntry("Dati amministrativi Windchill PDM")
    public static final String PRIVATE_CONSTANT_106 = "WindchillPDMAdmin_csv_title";

    @RBEntry("Preferenze di visualizzazione")
    public static final String PRIVATE_CONSTANT_107 = "wvs_csv_title";

    @RBEntry("Preferenze report distinta base")
    public static final String PRIVATE_CONSTANT_108 = "bom_report_preferences";

    @RBEntry("Dati di base ProjectLink")
    public static final String PRIVATE_CONSTANT_109 = "pjl_base_data_title";

    @RBEntry("Documento ProjectLink - Tipi soft")
    public static final String PRIVATE_CONSTANT_110 = "pjl_softtypes_csv_title";

    @RBEntry("Definizioni criterio di base")
    public static final String PRIVATE_CONSTANT_111 = "basecriteriondef_csv_title";

    @RBEntry("Versioni applicazione di creazione CAD")
    public static final String PRIVATE_CONSTANT_112 = "epmauthoringappversion_csv_title";

    @RBEntry("Preferenze per Windchill Workgroup Manager")
    public static final String PRIVATE_CONSTANT_113 = "uwgmpreference_csv_title";

    @RBEntry("Preferenze client per Windchill Workgroup Manager")
    public static final String PRIVATE_CONSTANT_114 = "uwgmclientpreference_csv_title";

    @RBEntry("Report")
    public static final String PRIVATE_CONSTANT_115 = "reports_csv_title";

    @RBEntry("Regole d'accesso Reporting")
    public static final String PRIVATE_CONSTANT_116 = "load_reporting_access_rules_csv_title";

    @RBEntry("Demo report")
    public static final String PRIVATE_CONSTANT_117 = "reportsdeom_csv_title";

    @RBEntry("Modello team condiviso")
    public static final String PRIVATE_CONSTANT_118 = "shtm_csv_title";

    @RBEntry("Preferenze raccoglitore")
    public static final String PRIVATE_CONSTANT_119 = "collectorpreference_csv_title";

    @RBEntry("Vincolo di relazione di default")
    public static final String PRIVATE_CONSTANT_120 = "default_association_constraint";

    @RBEntry("Regole di accesso per la replica")
    public static final String PRIVATE_CONSTANT_121 = "replication_access_rules_title";

    @RBEntry("Tipo soft di programmazione processi distribuiti")
    public static final String PRIVATE_CONSTANT_122 = "DP_SCHEDULE_SOFTTYPE";

    @RBEntry("Preferenze mappatura ambiente federato")
    public static final String PRIVATE_CONSTANT_123 = "FederationMapping_csv_title";

    @RBEntry("Tipi soft di definizione rilevamento interferenze")
    public static final String PRIVATE_CONSTANT_124 = "clash_def_type_csv_title";

    @RBEntry("Layout attributi operazioni di modifica")
    public static final String PRIVATE_CONSTANT_125 = "type_attribute_change_activity";

    @RBEntry("Layout attributi report di problema")
    public static final String PRIVATE_CONSTANT_126 = "type_attribute_problem_report";

    @RBEntry("Layout attributi soluzioni temporanee")
    public static final String PRIVATE_CONSTANT_127 = "type_attribute_variance";

    @RBEntry("Layout attributi notifiche di modifica")
    public static final String PRIVATE_CONSTANT_128 = "type_attribute_change_notice";

    @RBEntry("Layout attributi richieste di modifica")
    public static final String PRIVATE_CONSTANT_129 = "type_attribute_change_request";

    @RBEntry("Layout attributi richieste di promozione")
    public static final String PRIVATE_CONSTANT_130 = "type_attribute_promotion_request";

    @RBEntry("Layout attributi work set")
    public static final String WORK_SET_ATTRIBUTE_LAYOUTS = "type_attribute_work_set";

    @RBEntry("Layout attributi azioni di modifica")
    public static final String PRIVATE_CONSTANT_131 = "type_attribute_changeActionItem";

    @RBEntry("Layout attributi report")
    public static final String PRIVATE_CONSTANT_132 = "type_attribute_report";

    @RBEntry("Layout attributi attività interessate")
    public static final String PRIVATE_CONSTANT_133 = "type_attribute_affected_activity_data";

    @RBEntry("Layout attributi parti")
    public static final String PRIVATE_CONSTANT_134 = "type_attribute_wtpart";

    @RBEntry("Layout attributi master parti")
    public static final String PRIVATE_CONSTANT_135 = "type_attribute_wtpartmaster";

    @RBEntry("Layout attributi baseline")
    public static final String PRIVATE_CONSTANT_136 = "type_attribute_managedbaseline";

    @RBEntry("Attributi ECAD")
    public static final String PRIVATE_CONSTANT_137 = "uwgmecad_attributes";

    @RBEntry("Regole di inizializzazione tipi soft EPM ECAD")
    public static final String PRIVATE_CONSTANT_138 = "uwgmecad_EPM_softTypes_InitRules";

    @RBEntry("Tipi soft ECAD")
    public static final String PRIVATE_CONSTANT_139 = "uwgmecad_softTypes";

    @RBEntry("ECAD - Modello di mappatura attributi distinta base")
    public static final String PRIVATE_CONSTANT_140 = "ecadBOMAttributeMapTemplate";

    @RBEntry("Modello di definizione contenuto ECAD")
    public static final String PRIVATE_CONSTANT_141 = "ecadContentDefinitionTemplate";

    @RBEntry("Modello hook ECAD per creazione elenco di file di progetto")
    public static final String PRIVATE_CONSTANT_142 = "ecadHookCreateDesignFileListTemplate";

    @RBEntry("Modello hook ECAD per creazione distinta base")
    public static final String PRIVATE_CONSTANT_143 = "ecadHookCreateGenericBOMTemplate";

    @RBEntry("Modello hook ECAD per creazione distinte base di varianti")
    public static final String PRIVATE_CONSTANT_144 = "ecadHookCreateVariantBOMTemplate";

    @RBEntry("Modello hook ECAD per creazione elenco di allegati")
    public static final String PRIVATE_CONSTANT_145 = "ecadHookCreateListAttachmentsTemplate";

    @RBEntry("Modello hook ECAD per creazione elemento visualizzabile")
    public static final String PRIVATE_CONSTANT_146 = "ecadHookCreateViewableTemplate";

    @RBEntry("Modello hook ECAD per recupero directory di progetti")
    public static final String PRIVATE_CONSTANT_147 = "ecadHookGetDesignDirectoryTemplate";

    @RBEntry("Modello hook ECAD per recupero elenco elementi di progetto")
    public static final String PRIVATE_CONSTANT_148 = "ecadHookGetDesignItemListTemplate";

    @RBEntry("Modello hook ECAD per recupero elenco elementi in formato neutro")
    public static final String PRIVATE_CONSTANT_149 = "ecadHookGetNeutralFormatItemListTemplate";

    @RBEntry("Modello hook ECAD per pre-aggiornamento")
    public static final String PRIVATE_CONSTANT_150 = "ecadHookPreUpdateTemplate";

    @RBEntry("Modello hook ECAD per propagazione attributi al progetto")
    public static final String PRIVATE_CONSTANT_151 = "ecadHookPropagateIBAsToDesignTemplate";

    @RBEntry("Modello hook ECAD per recupero attributi dal progetto")
    public static final String PRIVATE_CONSTANT_152 = "ecadHookRetrieveDesignAttributesTemplate";

    @RBEntry("Modello hook ECAD per convalida progetto")
    public static final String PRIVATE_CONSTANT_153 = "ecadHookValidateDesignTemplate";

    @RBEntry("Caricatore dati di Windchill Gateway for Creo Elements/Direct Model Manager")
    public static final String PRIVATE_CONSTANT_154 = "com/ptc/windchill/dpimpl/load/CocreateMMLoad.xml";

    @RBEntry("Layout attributi task")
    public static final String PRIVATE_CONSTANT_155 = "type_attribute_workitem";

    @RBEntry("Report distinta base")
    public static final String PRIVATE_CONSTANT_156 = "BOM_csv_title";

    @RBEntry("Layout attributi DiscussionPosting")
    public static final String PRIVATE_CONSTANT_157 = "type_attribute_discussionPosting";

    @RBEntry("Layout attributi MeetingCenterMeeting")
    public static final String PRIVATE_CONSTANT_158 = "type_attribute_meetingCenterMeeting";

    @RBEntry("Layout attributi TraditionalMeeting")
    public static final String PRIVATE_CONSTANT_159 = "type_attribute_traditionalMeeting";

    @RBEntry("Layout attributi proposta di modifica")
    public static final String PRIVATE_CONSTANT_160 = "type_attribute_changeProposal";

    @RBEntry("Layout attributi investigazione di modifica")
    public static final String PRIVATE_CONSTANT_161 = "type_attribute_changeInvestigation";

    @RBEntry("Layout attributi analisi")
    public static final String PRIVATE_CONSTANT_162 = "type_attribute_analysisActivity";

    @RBEntry("Layout attributi contesto di configurazione")
    public static final String PRIVATE_CONSTANT_163 = "type_attribute_wtpartalternaterep";

    @RBEntry("Tipi soft documento EPM ECAD")
    public static final String PRIVATE_CONSTANT_164 = "uwgmecad_EPM_softTypes";

    @RBEntry("Tabelle home page di default")
    public static final String PRIVATE_CONSTANT_165 = "default_homepage_tables";

    @RBEntry("Layout attributi link di equivalenza")
    public static final String PRIVATE_CONSTANT_166 = "type_attribute_equivalenceLink";

    @RBEntry("Layout attributi attività progetto")
    public static final String PRIVATE_CONSTANT_167 = "type_attribute_ProjectActivity";

    @RBEntry("Layout attributi fase cardine")
    public static final String PRIVATE_CONSTANT_168 = "type_attribute_Milestone";

    @RBEntry("Layout attributi attività di riepilogo")
    public static final String PRIVATE_CONSTANT_169 = "type_attribute_SummaryActivity";

    @RBEntry("Layout attributi risorse - Persona")
    public static final String PRIVATE_CONSTANT_170 = "type_attribute_PersonResource";

    @RBEntry("Layout attributi risorse - Ruolo")
    public static final String PRIVATE_CONSTANT_171 = "type_attribute_RoleResource";

    @RBEntry("Layout attributi risorse - Materiale")
    public static final String PRIVATE_CONSTANT_172 = "type_attribute_MaterialResource";

    @RBEntry("Layout attributi risorse - Attrezzatura")
    public static final String PRIVATE_CONSTANT_173 = "type_attribute_EquipmentResource";

    @RBEntry("Layout attributi risorse - Stabilimento")
    public static final String PRIVATE_CONSTANT_174 = "type_attribute_FacilityResource";

    @RBEntry("Layout attributi risultato finale")
    public static final String PRIVATE_CONSTANT_175 = "type_attribute_classicDeliverable";

    @RBEntry("Tipi di parte ECAD")
    public static final String PRIVATE_CONSTANT_176 = "uwgmecad_Part_softTypes";

    @RBEntry("Regole di inizializzazione tipi soft di parte ECAD")
    public static final String PRIVATE_CONSTANT_177 = "uwgmecad_Part_softTypes_InitRules";

    @RBEntry("Installa il Load-Set dei tipi soft di parte Shipbuilding")
    public static final String PRIVATE_CONSTANT_178 = "com/ptc/windchill/dpimpl/load/SBTLoad.xml";

    @RBEntry("Installa il Load-Set dei tipi soft di SoftwareLink")
    public static final String PRIVATE_CONSTANT_179 = "com/ptc/swlink/load/SoftwareLinkLoad.xml";

    @RBEntry("Tipo soft di definizione rilevamento interferenze")
    public static final String PRIVATE_CONSTANT_180 = "type_interference_detection";

    @RBEntry("Definizioni base WTDocument")
    public static final String PRIVATE_CONSTANT_181 = "type_attribute_documentType";

    @RBEntry("Definizioni base WTDocumentMaster")
    public static final String PRIVATE_CONSTANT_182 = "type_attribute_documentMasterType";

    @RBEntry("Layout attributi prodotto")
    public static final String PRIVATE_CONSTANT_183 = "type_attribute_wtproduct";

    @RBEntry("Layout attributi libreria")
    public static final String PRIVATE_CONSTANT_184 = "type_attribute_wtlibrary";

    @RBEntry("Layout attributi direttiva di modifica")
    public static final String PRIVATE_CONSTANT_185 = "type_attribute_change_directive";

    @RBEntry("Installa il Load-Set di gestione della qualità")
    public static final String QMS_DATA_LOAD = "com/ptc/qualitymanagement/qms/load/QMSLoad.xml";

    @RBEntry("Installa il Load-Set dei dati master di gestione della qualità")
    public static final String MASTERDATA_DATA_LOAD = "com/ptc/qualitymanagement/masterdata/load/MasterdataLoad.xml";

    @RBEntry("Installa il Load-Set CAPA di gestione della qualità")
    public static final String CAPA_DATA_LOAD = "com/ptc/qualitymanagement/capa/load/CAPALoad.xml";

    @RBEntry("Installa il Load-Set NC di gestione della qualità")
    public static final String NC_DATA_LOAD = "com/ptc/qualitymanagement/nc/load/NCLoad.xml";

    @RBEntry("Installa il Load-Set di risoluzione della qualità")
    public static final String DISPOSITION_DATA_LOAD = "com/ptc/qualitymanagement/disposition/load/DispositionLoad.xml";

    @RBEntry("Install Quality Customer Experience Management Load-Set")
    @RBComment("DO NOT TRANSLATE")
    public static final String CEM_DATA_LOAD = "com/ptc/qualitymanagement/cem/load/CEMLoad.xml";

    @RBEntry("Install Quality CEM EMDR Report Load-Set")
    @RBComment("DO NOT TRANSLATE")
    public static final String EMDR_DATA_LOAD = "com/ptc/qualitymanagement/cem/load/EMDRLoad.xml";

    @RBEntry("Install Quality CEM Canada Report Load-Set")
    @RBComment("DO NOT TRANSLATE")
    public static final String CANADA_DATA_LOAD = "com/ptc/qualitymanagement/cem/load/CanadaLoad.xml";

    @RBEntry("Install Quality CEM Vigilance Report Load-Set")
    @RBComment("DO NOT TRANSLATE")
    public static final String VIGILANCE_DATA_LOAD = "com/ptc/qualitymanagement/cem/load/VigilanceLoad.xml";

    @RBEntry("Installa il Load-Set UDI")
    public static final String UDI_DATA_LOAD = "com/ptc/qualitymanagement/udi/load/UDILoad.xml";

    @RBEntry("Installa il Load-Set GPD")
    public static final String GPD_DATA_LOAD = "com/ptc/smb/load/GPDLoad.xml";

    @RBEntry("Preferenze mappatura credenziali ambiente federato")
    public static final String PRIVATE_CONSTANT_186 = "wt_federation_credmap_preferences";

    @RBEntry("Modelli di ciclo di vita di Visualization")
    public static final String PRIVATE_CONSTANT_187 = "wvs_lifecycle_templates";

    @RBEntry("Layout attributi utilizzo parte")
    public static final String PRIVATE_CONSTANT_188 = "type_attribute_wtpartusagelink";

    @RBEntry("Layout attributi casi d'impiego componenti")
    public static final String PRIVATE_CONSTANT_189 = "type_attribute_usesocurrence";

    @RBEntry("ECAD - Modello di definizione filtri distinta base")
    public static final String PRIVATE_CONSTANT_190 = "ecadBOMFilterDefinitionTemplate";

    @RBEntry("Installa il Load-Set di Service Information Manager")
    public static final String PRIVATE_CONSTANT_191 = "com/ptc/arbortext/windchill/siscore/load/sisload.xml";

    @RBEntry("Installa il Load-Set di Parts Information Manager")
    public static final String PRIVATE_CONSTANT_192 = "com/ptc/arbortext/windchill/partlist/load/sisipcload.xml";

    @RBEntry("Layout attributi azioni di modifica")
    public static final String PRIVATE_CONSTANT_193 = "type_attribute_change_action";

    @RBEntry("Tipi soft di dati InterComm")
    public static final String PRIVATE_CONSTANT_194 = "intercomm_data_type_csv_title";

    @RBEntry("Modelli di Creo")
    public static final String PRIVATE_CONSTANT_195 = "creoTemplates";

    @RBEntry("Modelli di CATIAV5")
    public static final String PRIVATE_CONSTANT_196 = "catiav5Templates";

    @RBEntry("Modelli di NX")
    public static final String PRIVATE_CONSTANT_197 = "nxTemplates";

    @RBEntry("Attributi link riferimento documento EPM ECAD")
    public static final String PRIVATE_CONSTANT_198 = "uwgmecad_EPMRefLinkAttrs";

    @RBEntry("Regole di inizializzazione consegna ricevuta")
    public static final String PRIVATE_CONSTANT_200 = "RDInitRules_csv_title";

    @RBEntry("Attributi tipo consegna ricevuta")
    public static final String PRIVATE_CONSTANT_201 = "RDTypeAttr_csv_title";

    @RBEntry("Modello di contenuto ECAD")
    public static final String PRIVATE_CONSTANT_202 = "ecadContentTemplate";

    @RBEntry("Modello di disegno ECAD")
    public static final String PRIVATE_CONSTANT_203 = "ecadDrawingTemplate";

    @RBEntry("Modello di dati di scambio ECAD e MCAD")
    public static final String PRIVATE_CONSTANT_204 = "ecadMcadExchangeDataTemplate";

    @RBEntry("Modello di vista collocata ECAD")
    public static final String PRIVATE_CONSTANT_205 = "ecadPackagedViewTemplate";

    @RBEntry("Modelli di oggetti principali ECAD")
    public static final String PRIVATE_CONSTANT_206 = "ecadPrimaryObjectTemplates";

    @RBEntry("Layout attributi configurazione parte")
    public static final String PRIVATE_CONSTANT_207 = "type_attribute_wtproductconfiguration";

    @RBEntry("Layout attributi istanza parte")
    public static final String PRIVATE_CONSTANT_208 = "type_attribute_wtproductinstance";

    @RBEntry("Preferenze editor assiemi avanzato")
    public static final String PRIVATE_CONSTANT_209 = "ate_csv_title";

    @RBEntry("Layout attributi segnalibro")
    public static final String PRIVATE_CONSTANT_210 = "type_attribute_Bookmark";

    @RBEntry("Layout attributi azione")
    public static final String PRIVATE_CONSTANT_211 = "type_attribute_Actionitem";

    @RBEntry("Modello BluePrint-PCB ECAD")
    public static final String PRIVATE_CONSTANT_212 = "ecadBluePrintPCBTemplate";

    @RBEntry("Modello CAM350 ECAD")
    public static final String PRIVATE_CONSTANT_213 = "ecadCAM350Template";

    @RBEntry("Tipo soft documentazione ECAD")
    public static final String PRIVATE_CONSTANT_214 = "ecadDocumentationSoftType";

    @RBEntry("Tipo soft fabbricazione ECAD")
    public static final String PRIVATE_CONSTANT_215 = "ecadManufacturingSoftType";

    @RBEntry("Installa il Load-Set di manutenzione di MPMLink")
    public static final String PRIVATE_CONSTANT_216 = "com/ptc/windchill/mpml/load/mpmlinkLoadR10M020.xml";

    @RBEntry("Layout attributi DiscussionTopic")
    public static final String PRIVATE_CONSTANT_217 = "type_attribute_discussionTopic";

    @RBEntry("Preferenze applicazione mobile")
    public static final String PRIVATE_CONSTANT_218 = "mobileapp_csv_title";

    @RBEntry("Layout attributi link descrizione documento materiale ERP")
    public static final String PRIVATE_CONSTANT_219 = "type_attribute_erpMaterialDocumentDescribeLink";

    @RBEntry("Installa il Load-Set di Options and Variants")
    public static final String PRIVATE_CONSTANT_220 = "com/ptc/windchill/option/load/OptionsCoreLoad.xml";

    @RBEntry("Modelli AutoCAD")
    public static final String PRIVATE_CONSTANT_221 = "acadTemplates";

    @RBEntry("Modelli Inventor")
    public static final String PRIVATE_CONSTANT_222 = "inventorTemplates";

    @RBEntry("Modelli SolidWorks")
    public static final String PRIVATE_CONSTANT_223 = "solidworksTemplates";

    @RBEntry("Installa il Load-Set incrementale di MPMLink")
    public static final String PRIVATE_CONSTANT_224 = "com/ptc/windchill/mpml/load/mpmlinkLoadR102.xml";

    @RBEntry("Carica preferenze per importazione/esportazione da foglio di calcolo")
    public static final String PRIVATE_CONSTANT_225 = "Import Export from SpreadSheet Preferences";

    @RBEntry("Installa dati di test work package")
    public static final String PRIVATE_CONSTANT_226 = "com/ptc/windchill/wp/load/WorkPackageTestLoad.xml";

    @RBEntry("Caricamento di Configuration Items and Design Solutions")
    public static final String PRIVATE_CONSTANT_227 = "com/ptc/windchill/enterprise/cids/load/cidsLoad.xml";

    @RBEntry("Caricamento di Setup for Configuration Items and Design Solutions")
    public static final String PRIVATE_CONSTANT_228 = "com/ptc/windchill/enterprise/cids/load/cidscfgLoad.xml";

    @RBEntry("Caricamento di ESI for Options and Variants")
    public static final String PRIVATE_CONSTANT_229 = "com/ptc/windchill/esi/load/esiOVLoad.xml";

    @RBEntry("Installa il Load-Set di associatività")
    public static final String PRIVATE_CONSTANT_230 = "com/ptc/windchill/associativity/load/associativityLoadR102.xml";

    @RBEntry("Installa il Load-Set dello scambio di dati STEP")
    public static final String PRIVATE_CONSTANT_231 = "wt/ixb/step/stepdex.xml";

    @RBEntry("Installa il Load-Set di PTC Service Center")
    public static final String PRIVATE_CONSTANT_232 = "com/ptc/sc/loadfiles/servicecenterLoad.xml";

    @RBEntry("Installa il Load-Set dimostrativo di PTC Service Center")
    public static final String PRIVATE_CONSTANT_233 = "com/ptc/sc/loadfiles/servicecenterDemoLoad.xml";

    @RBEntry("Load-Set di PTC Windchill Service Information Module for S1000D")
    public static final String PRIVATE_CONSTANT_234 = "com/ptc/arbortext/windchill/rules/load/rulesload.xml";

    @RBEntry("Installa il Load-Set di manutenzione di MPMLink")
    public static final String PRIVATE_CONSTANT_235 = "com/ptc/windchill/mpml/load/mpmlinkLoadR102M020.xml";
}
