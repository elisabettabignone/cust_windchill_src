/* bcwti
 *
 * Copyright (c) 2013 PTC, Inc. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement.
 * You shall not disclose such confidential information and shall use it
 * only in accordance with the terms of the license agreement.
 *
 * ecwti
 */
package wt.load;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBArgComment1;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

/**
 * The resource bundle for the {@link WindchillLoader}.
 *
 * <BR><BR><B>Supported API: </B>false
 * <BR><BR><B>Extendable: </B>false
 */
@RBUUID("wt.load.loadResource")
public final class loadResource extends WTListResourceBundle {
   @RBEntry("Cannot find file \"{0}\"")
   @RBArgComment0("File name")
   public static final String CANNOT_FIND_FILE = "cannot_find_file";

   @RBEntry("Error detected in Initializing user \"{0}\"")
   @RBArgComment0("User name")
   public static final String ERROR_INIT_USER = "error_init_user";

   @RBEntry("WindchillLoader detected incomplete installation. Please install at least one Application before load.")
   public static final String INCOMPLETE_INSTALLATION = "incomplete_installation";

   @RBEntry("Data file must be supplied on command line.")
   public static final String NO_DATA_FILE = "0";

   @RBEntry("Data file not readable \"{0}\".")
   @RBArgComment0("refers to data filename")
   public static final String DATA_FILE_NOT_READABLE = "1";

   @RBEntry("Map file not readable \"{0}\".")
   @RBArgComment0("refers to map filename")
   public static final String MAP_FILE_NOT_READABLE = "2";

   @RBEntry("Required field \"{0}\" missing from data file.")
   @RBArgComment0("refers to fieldname")
   public static final String REQUIRED_FIELD_MISSING = "3";

   @RBEntry("Successful {0} line {1}:")
   @RBArgComment0("refers to generic method to be performed on data file")
   @RBArgComment1("refers to line number from data file")
   public static final String SUCCESS = "4";

   @RBEntry("ERROR in {0} at line {1}:")
   @RBArgComment0("refers to generic method to be performed on data file")
   @RBArgComment1("refers to line number from data file")
   public static final String FAILURE = "5";

   @RBEntry("ERROR setting cache value for key = {0}, {1}.")
   @RBArgComment0("refers to first key into the general load cache")
   @RBArgComment1("refers to second key into specific cache for this class")
   public static final String SETCACHE = "6";

   @RBEntry("An error occurred while importing data from the {1} file.  The import processing stopped at line {0}.  Check the method server log for more information.")
   @RBArgComment0("refers to line number from data file")
   @RBArgComment1("refers to the name of the data file")
   public static final String IMPORT_ERROR = "7";

   @RBEntry("The import of data from the {1} file is complete.  Errors were reported at lines {0}.  Some data may not have been imported correctly.  Check the method server log for more information.")
   @RBArgComment0("refers to line number from data file")
   @RBArgComment1("refers to the name of the data file")
   public static final String IMPORT_WARNING = "8";

   @RBEntry("Processing line {1} from file {0}.  When the import is complete, check the method server log for any errors or warnings.")
   @RBArgComment0("refers to the name of the data file")
   @RBArgComment1("refers to line number from data file")
   public static final String IMPORT_STATUS = "9";

   @RBEntry("   The WindchillLoader will load data sets into the Database according to the currently installed Windchill Applications.")
   public static final String LOAD_DESCRIPTION = "10";

   @RBEntry("Description:")
   public static final String DESCRIPTION = "11";

   @RBEntry("Usage:")
   public static final String USAGE = "12";

   @RBEntry("Arguments:")
   @RBComment("The command line switches for the application")
   public static final String SWITCHES = "13";

   @RBEntry("Install ONLY the demo data set for the installed Products")
   public static final String ONLYDEMO = "14";

   @RBEntry("Include demo data set with the load (where available)")
   public static final String DEMO = "15";

   @RBEntry("Install ONLY the test data set for the installed Products")
   public static final String ONLYTEST = "16";

   @RBEntry("Include test data set with the load (where available)")
   public static final String TEST = "17";

   @RBEntry("Install ONLY the following applications list.  This list is separated by ',' and the app IDs should match the value of desired application listed in Installed.properties in $WT_HOME/codebase")
   public static final String APPLICATIONS = "18";

   @RBEntry("Installing only demo data sets for ALL installed Products")
   public static final String EXAMPLE1 = "19";

   @RBEntry("Installing only the data set for Windchill PDMLink")
   public static final String EXAMPLE2 = "20";

   @RBEntry("Examples:")
   public static final String EXAMPLE = "21";

   @RBEntry("Run the WindchillLoader in unattended mode (no prompting)")
   public static final String UNATTENDED = "22";

   @RBEntry("Display a list of all Applications which are installed and have valid load sets.")
   public static final String INFO = "23";

   @RBEntry("The following applications are available for loading:")
   public static final String LIST = "24";

   @RBEntry("ERROR: Improper arguments specified.")
   public static final String ARG_ERROR = "25";

   @RBEntry("Loads all data set for all installed Products.")
   public static final String ALL = "26";

   @RBEntry("ERROR: The following Applications were specified which do not match the list of Applications available for loading:\n {0}")
   public static final String INVALID_APPLICATION = "27";

   @RBEntry("Authentication arguments (to avoid logging in via authentication window):")
   public static final String AUTHENTICATION_ARGS = "28";

   @RBEntry("User login name")
   public static final String USER = "29";

   @RBEntry("User password")
   public static final String PASSWORD = "30";

   @RBEntry("Provide the locale for the data load. For example, Japanese data load should provide -Locale=ja")
   public static final String LOCALE = "31";

   @RBEntry("Abort data loading on error ")
   public static final String ABORT_ON_ERROR = "32";

   @RBEntry("Do you want to load {0}? [y/n]")
   @RBComment("[y/n] in the above message should be left untranslated")
   @RBArgComment0("Localized name of application or preference to be loaded")
   public static final String LOAD_QUESTION = "33";

   @RBEntry("Do you want to restart the servers? [y/n]")
   @RBComment("[y/n] in the above message should be left untranslated")
   public static final String RESTART_SERVER = "34";

   @RBEntry("Do you want to execute wt.admin.Install? [y/n]")
   @RBComment("[y/n] in the above message should be left untranslated")
   public static final String EXECUTE_INSTALL = "35";

   @RBEntry("Installs new data going from 9.0 to 9.1.  Files to be loaded need to have the path and file name")
   public static final String LOADFILE = "36";

   @RBEntry("Maintenance Examples: ")
   public static final String MAINTENANCE_EXAMPLES = "37";

   @RBEntry(" Loading new data going from 9.0 to 9.1")
   public static final String MAINTENANCE_DESCRIPTION = "38";

   @RBEntry("Installs the new data that has been introduced at release 9.0 M30 and all the new data for 9.1 for Foundation installation")
   public static final String MAINTENANCE_EXAMPLE1 = "39";

   @RBEntry("Installs the new data that has been introduced at 9.1 for Foundation and PDMLink ")
   public static final String MAINTENANCE_EXAMPLE2 = "40";

   @RBEntry("Install Windchill Foundation Maintenance Load-Set for systems going from 9.0 to 9.1")
   public static final String PRIVATE_CONSTANT_0 = "wt/load/foundationLoadR90toR91.xml";

   @RBEntry("Install Windchill Foundation Maintenance Load-Set that was introduced at 9.0 M030")
   public static final String PRIVATE_CONSTANT_1 = "wt/load/foundationLoadR90thruR90M20.xml";

   @RBEntry("Install Windchill Foundation Load-Set")
   public static final String PRIVATE_CONSTANT_2 = "wt/load/foundationLoad.xml";

   @RBEntry("Install Windchill Foundation Demo Load-Set")
   public static final String PRIVATE_CONSTANT_3 = "wt/load/foundationDemo.xml";

   @RBEntry("Install Windchill Foundation Test Load-Set")
   public static final String PRIVATE_CONSTANT_4 = "wt/load/foundationTest.xml";

   @RBEntry("Install Windchill PDM Load-Set")
   public static final String PRIVATE_CONSTANT_5 = "wt/load/fdnpdmLoad.xml";

   @RBEntry("Install Windchill PDM Demo Load-Set")
   public static final String PRIVATE_CONSTANT_6 = "wt/load/fdnpdmDemo.xml";

   @RBEntry("Install Windchill Foundation Maintenance Load-Set")
   public static final String PRIVATE_CONSTANT_7 = "wt/load/foundationLoadR9M020.xml";

   @RBEntry("Install Change Planning Load-Set")
   public static final String PRIVATE_CONSTANT_8 = "wt/change2/changeplanning/load/changeplanningLoad.xml";

   @RBEntry("Install Windchill Gateway for Cadence Allegro Design Workbench (ADW) Load-Set")
   public static final String PRIVATE_CONSTANT_9 = "com/ptc/windchill/ecadlibsync/load/ECADLibSyncServerLoad.xml";

   @RBEntry("Install Arbortext Content Manager Load-Set")
   public static final String PRIVATE_CONSTANT_10 = "com/ptc/windchill/pdmlink/load/atcmLoad.xml";

   @RBEntry("Install PDMLink Load-Set")
   public static final String PRIVATE_CONSTANT_11 = "com/ptc/windchill/pdmlink/load/pdmlinkLoad.xml";

   @RBEntry("Install PDMLink Demo Load-Set")
   public static final String PRIVATE_CONSTANT_12 = "com/ptc/windchill/pdmlink/load/pdmlinkDemo.xml";

   @RBEntry("Install PDMLink Test Load-Set")
   public static final String PRIVATE_CONSTANT_13 = "com/ptc/windchill/pdmlink/load/pdmlinkTest.xml";

   @RBEntry("Install PDMLink Maintenance Load-Set")
   public static final String PRIVATE_CONSTANT_14 = "com/ptc/windchill/pdmlink/load/pdmlinkLoadR91.xml";

   @RBEntry("Install PDMLink Maintenance Load-Set")
   public static final String PRIVATE_CONSTANT_15 = "com/ptc/windchill/pdmlink/load/pdmlinkLoadR9M020.xml";

   @RBEntry("Install Closed-Loop Change Process Load-Set")
   public static final String PRIVATE_CONSTANT_16 = "com/ptc/windchill/pdmlink/load/cmiiLoad.xml";

   @RBEntry("Install Closed-Loop Change Process Demo-Set")
   public static final String PRIVATE_CONSTANT_17 = "com/ptc/windchill/pdmlink/load/cmiiDemo.xml";

   @RBEntry("Install ProjectLink Load-Set")
   public static final String PRIVATE_CONSTANT_18 = "com/ptc/windchill/projectlink/load/projectlinkLoad.xml";

   @RBEntry(" Install ProjectLink Demo Load-Set")
   public static final String PRIVATE_CONSTANT_19 = "com/ptc/windchill/projectlink/load/projectlinkDemo.xml";

   @RBEntry("Install ProjectLink Test Load-Set")
   public static final String PRIVATE_CONSTANT_20 = "com/ptc/windchill/projectlink/load/projectlinkTest.xml";

   @RBEntry("Install ESI Load-Set")
   public static final String PRIVATE_CONSTANT_21 = "com/ptc/windchill/esi/load/esiLoad.xml";

   @RBEntry("Install ESI Demo Load-Set")
   public static final String PRIVATE_CONSTANT_22 = "com/ptc/windchill/esi/load/esiDemo.xml";

   @RBEntry("Install ESI  Maintenance Load-Set for systems going from 9.0 to 9.1")
   public static final String PRIVATE_CONSTANT_23 = "com/ptc/windchill/esi/load/esiLoadR91.xml";

   @RBEntry("Install PartsLink Load-Set")
   public static final String PRIVATE_CONSTANT_24 = "com/ptc/windchill/partslink/load/partslinkLoad.xml";

   @RBEntry("Install SCMI Load-Set")
   public static final String PRIVATE_CONSTANT_25 = "com/ptc/windchill/scm/load/scmLoad.xml";

   @RBEntry("Install Spreadsheet Import Export Test Load-Set")
   @RBComment("This is used in test data loading. It is the CSV title of Spreadsheet Importer Exporter Test Load-Set installation.")
   public static final String PRIVATE_CONSTANT_26 = "com/ptc/windchill/ixb/load/ixbTest.xml";

   @RBEntry("Install Supplier Management Load-Set")
   public static final String PRIVATE_CONSTANT_27 = "com/ptc/windchill/suma/load/sumaLoad.xml";

   @RBEntry("Install Supplier Management Demo Load-Set")
   public static final String PRIVATE_CONSTANT_28 = "com/ptc/windchill/suma/load/sumaDemo.xml";

   @RBEntry("Install Supplier Management Test Load-Set")
   public static final String PRIVATE_CONSTANT_29 = "com/ptc/windchill/suma/load/sumaTest.xml";

   @RBEntry("Install Environmental Compliance Load-Set")
   @RBComment("This is used in attributes and types data loading for Environmental Compliance. It is the CSV title of Environmental Compliance Load-Set installation.")
   public static final String PRIVATE_CONSTANT_30 = "com/ptc/windchill/insight/environment/load/environmentLoad.xml";

   @RBEntry("Install CostMgmt Load-Set")
   @RBComment("The CSV title of CostMgmt Load-Set installation.")
   public static final String PRIVATE_CONSTANT_31 = "com/ptc/windchill/cost/load/costLoad.xml";

   @RBEntry("Install CostMgmt Test Load-Set")
   @RBComment("The CSV title of CostMgmt Test Load-Set installation.")
   public static final String PRIVATE_CONSTANT_32 = "com/ptc/windchill/cost/load/costTest.xml";

   @RBEntry("Install CostMgmt Demo Load-Set")
   @RBComment("The CSV title of CostMgmt Demo Load-Set installation.")
   public static final String PRIVATE_CONSTANT_33 = "com/ptc/windchill/cost/load/costDemo.xml";

   @RBEntry("Install Options and Variants Load-Set")
   public static final String PRIVATE_CONSTANT_34 = "com/ptc/wpcfg/load/wpcfgLoad.xml";

   @RBEntry("Install Options and Variants Demo Load-Set")
   public static final String PRIVATE_CONSTANT_35 = "com/ptc/wpcfg/load/wpcfgDemo.xml";

   @RBEntry("Install Options and Variants Test Load-Set")
   public static final String PRIVATE_CONSTANT_36 = "com/ptc/wpcfg/load/wpcfgTest.xml";

   @RBEntry("Install Options and Variants QA Test Load-Set")
   public static final String PRIVATE_CONSTANT_37 = "com/ptc/wpcfg/load/wpcfgQATest.xml";

   @RBEntry("Install MPMLink Load-Set")
   public static final String PRIVATE_CONSTANT_38 = "com/ptc/windchill/mpml/load/mpmlLoad.xml";

   @RBEntry("Install MPMLink Management Demo Load-Set")
   public static final String PRIVATE_CONSTANT_39 = "com/ptc/windchill/mpml/load/mpmlDemo.xml";

   @RBEntry("Install MPMLink Maintenance Load-Set")
   public static final String PRIVATE_CONSTANT_40 = "com/ptc/windchill/mpml/load/mpmlinkLoadR91.xml";

   @RBEntry("Install MPMLink Maintenance Load-Set")
   public static final String PRIVATE_CONSTANT_41 = "com/ptc/windchill/mpml/load/mpmlLoadR9M020.xml";

   @RBEntry("Install MPMLink Maintenance Load-Set")
   public static final String PRIVATE_CONSTANT_42 = "com/ptc/windchill/mpml/load/mpmlinkLoadR10.xml";

   @RBEntry("Install WADM Load-Set")
   public static final String PRIVATE_CONSTANT_43 = "com/ptc/windchill/wadm/load/wadmLoad.xml";

   @RBEntry("Install Visualization Demo Load-Set")
   public static final String PRIVATE_CONSTANT_44 = "com/ptc/wvs/wvsDemo.xml";

   @RBEntry("Install Assemble-to-Order Load-Set")
   public static final String PRIVATE_CONSTANT_45 = "com/ptc/windchill/option/load/atoLoad.xml";

   @RBEntry("Install Assemble-to-Order Demo Load-Set")
   public static final String PRIVATE_CONSTANT_46 = "com/ptc/windchill/option/load/atoDemo.xml";

   @RBEntry("Install Assemble-to-Order Test Load-Set")
   public static final String PRIVATE_CONSTANT_47 = "com/ptc/windchill/option/load/atoTest.xml";

   @RBEntry("Install Platform Structures Load-Set")
   public static final String PLATFORM_STRUCTURES_LOAD = "com/ptc/windchill/option/load/wpsLoad.xml";

   @RBEntry("Install Platform Structures Demo Load-Set")
   public static final String PLATFORM_STRUCTURES_DEMO = "com/ptc/windchill/option/load/wpsDemo.xml";

   @RBEntry("Install Platform Structures Test Load-Set")
   public static final String PLATFORM_STRUCTURES_TEST = "com/ptc/windchill/option/load/wpsTest.xml";

   @RBEntry("Install Airbus Load-Set")
   public static final String PRIVATE_CONSTANT_48 = "com/ptc/windchill/enterprise/cids/load/airbusLoad.xml";

   @RBEntry("Install Airbus Demo Load-Set")
   public static final String PRIVATE_CONSTANT_49 = "com/ptc/windchill/enterprise/cids/load/airbusDemo.xml";

   @RBEntry("Install Windchill Gateway for I-deas TDM Load-Set")
   public static final String PRIVATE_CONSTANT_50 = "com/ptc/windchill/dpimpl/load/dpimplLoad.xml";

   @RBEntry("Windchill Gateway for I-deas TDM data loader")
   public static final String PRIVATE_CONSTANT_51 = "com/ptc/windchill/dpimpl/load/IdeasTDMLoad.xml";

   @RBEntry("WPC data loader")
   public static final String PRIVATE_CONSTANT_52 = "com/ptc/wpc/load/WPCServerLoad.xml";

   @RBEntry("Install FORAN Integration Load-Set")
   public static final String PRIVATE_CONSTANT_53 = "com/ptc/windchill/dpimpl/load/ForanLoad.xml";

   @RBEntry("Windchill Maturity Test loader")
   public static final String PRIVATE_CONSTANT_54 = "com/ptc/windchill/enterprise/maturity/load/maturityTestLoad.xml";

   @RBEntry("Change Test loader")
   public static final String PRIVATE_CONSTANT_55 = "com/ptc/windchill/enterprise/change2/load/changeTestLoad.xml";

   @RBEntry("Install Windchill RequirementsLink Load-Set")
   public static final String PRIVATE_CONSTANT_57 = "com/ptc/windchill/enterprise/requirement/load/requirementslinkLoad.xml";

   @RBEntry("\n\nWelcome to Database Setup Utility for Windchill and Windchill Solutions.\n\nYou will be presented with a series of prompts.\nIf you have already installed a base data set as part of a previous\nloading sequence you do not need to load it again.\n----------------------------------------------------------------")
   public static final String PRIVATE_CONSTANT_58 = "OpeningMessage";

   @RBEntry("Users")
   public static final String PRIVATE_CONSTANT_59 = "users_csv_title";

   @RBEntry("Groups")
   public static final String PRIVATE_CONSTANT_60 = "groups_csv_title";

   @RBEntry("Domains")
   public static final String PRIVATE_CONSTANT_61 = "domains_csv_title";

   @RBEntry("Cabinets")
   public static final String PRIVATE_CONSTANT_62 = "cabinets_csv_title";

   @RBEntry("Folders")
   public static final String PRIVATE_CONSTANT_63 = "folders_csv_title";

   @RBEntry("Access Rules")
   public static final String PRIVATE_CONSTANT_64 = "accessrules_csv_title";

   @RBEntry("Life Cycle Demo Data")
   public static final String PRIVATE_CONSTANT_65 = "lcdd_csv_title";

   @RBEntry("General Docs")
   public static final String PRIVATE_CONSTANT_66 = "gendocs_csv_title";

   @RBEntry("Specification Docs")
   public static final String PRIVATE_CONSTANT_67 = "specdocs_csv_title";

   @RBEntry("Requirements Docs")
   public static final String PRIVATE_CONSTANT_68 = "reqdocs_csv_title";

   @RBEntry("Views")
   public static final String PRIVATE_CONSTANT_69 = "views_csv_title";

   @RBEntry("Parts")
   public static final String PRIVATE_CONSTANT_70 = "parts_csv_title";

   @RBEntry("Workflow Demo")
   public static final String PRIVATE_CONSTANT_71 = "workdemo_csv_title";

   @RBEntry("Indexing Rules")
   public static final String PRIVATE_CONSTANT_72 = "indexrules_csv_title";

   @RBEntry("IBA Attribute Definitions")
   public static final String PRIVATE_CONSTANT_73 = "ibaadef_csv_title";

   @RBEntry("Report Templates")
   public static final String PRIVATE_CONSTANT_74 = "reptmpl_csv_title";

   @RBEntry("AUT Administrator Data")
   public static final String PRIVATE_CONSTANT_75 = "autadmin_csv_title";

   @RBEntry("Data Formats")
   public static final String PRIVATE_CONSTANT_76 = "dataformats_csv_title";

   @RBEntry("Miscellaneous Admin Data and Access Rules")
   public static final String PRIVATE_CONSTANT_77 = "miscadmin_csv_title";

   @RBEntry("Workflow Base Data")
   public static final String PRIVATE_CONSTANT_78 = "wrkflwbsdata_csv_title";

   @RBEntry("WorkflowAuthor group")
   public static final String PRIVATE_CONSTANT_79 = "wrkflauthors_csv_title";

   @RBEntry("Workflow related preferences")
   public static final String PRIVATE_CONSTANT_80 = "workflow_work_pref_title";

   @RBEntry("Assignments table related preferences")
   public static final String PRIVATE_CONSTANT_81 = "assignments_table_pref_title";

   @RBEntry("Lifecycle Base Data")
   public static final String PRIVATE_CONSTANT_82 = "lfcyclebsdata_csv_title";

   @RBEntry("Measurement System Definitions")
   public static final String PRIVATE_CONSTANT_83 = "msd_csv_title";

   @RBEntry("Quantity of Measure Definitions")
   public static final String PRIVATE_CONSTANT_84 = "qomd_csv_title";

   @RBEntry("Preferences")
   public static final String PRIVATE_CONSTANT_85 = "prefs_csv_title";

   @RBEntry("Replication Management Lifecycles and Workflows")
   public static final String PRIVATE_CONSTANT_86 = "rmlaw_csv_title";

   @RBEntry("Workflow Report Templates")
   public static final String PRIVATE_CONSTANT_87 = "wrkreptemp_csv_title";

   @RBEntry("PDMLink Root Repository and Reference Document Type")
   public static final String PRIVATE_CONSTANT_88 = "prrardt_csv_title";

   @RBEntry("Life Cycle and Team Template Rules")
   public static final String PRIVATE_CONSTANT_89 = "lcattr_csv_title";

    @RBEntry("Part Management and Reference Designator Preference")
    public static final String PRIVATE_CONSTANT_90 = "partManagement_csv_title";

    @RBEntry("Change Management Lifecycles and Workflows")
    public static final String PRIVATE_CONSTANT_91 = "ChangeManagement_csv_title";

    @RBEntry("Change Management Workflows")
    public static final String PRIVATE_CONSTANT_92 = "changemgmtworkflows_csv_title";

    @RBEntry("Change Management Life Cycles")
    public static final String PRIVATE_CONSTANT_93 = "changemgmtlc_csv_title";

    @RBEntry("Change Management Example Team Template")
    public static final String PRIVATE_CONSTANT_94 = "changemgmtett_csv_title";

    @RBEntry("Change Monitor Report Templates")
    public static final String PRIVATE_CONSTANT_95 = "changemonreptpl_csv_title";

    @RBEntry("PDMLink Preferences")
    public static final String PRIVATE_CONSTANT_96 = "pdmlinkprefs_csv_title";

    @RBEntry("Production Data for PDMLink Objects")
    public static final String PRIVATE_CONSTANT_97 = "pdmlinkproddata_csv_title";

    @RBEntry("PDMLink Access Rules")
    public static final String PRIVATE_CONSTANT_98 = "pdmlinkaccessrules_csv_title";

    @RBEntry("PDMLink Indexing Rules")
    public static final String PRIVATE_CONSTANT_99 = "pdmlinkindexrules_csv_title";

    @RBEntry("Containers")
    public static final String PRIVATE_CONSTANT_100 = "container_csv_title";

    @RBEntry("Container Templates")
    public static final String PRIVATE_CONSTANT_101 = "container_templates_csv_title";

    @RBEntry("Life Cycle Initialization Rules")
    public static final String PRIVATE_CONSTANT_102 = "LifecycleInitRules_csv_title";

    @RBEntry("Initialization Rules")
    public static final String PRIVATE_CONSTANT_103 = "InitilizationRules_csv_title";

    @RBEntry("Common Life Cycles")
    public static final String PRIVATE_CONSTANT_104 = "CommonLifeCycles_csv_title";

    @RBEntry("Windchill PDM Container")
    public static final String PRIVATE_CONSTANT_105 = "WindchillPDMContainer_csv_title";

    @RBEntry("Windchill PDM Administrative Data")
    public static final String PRIVATE_CONSTANT_106 = "WindchillPDMAdmin_csv_title";

    @RBEntry("Visualization Preferences")
    public static final String PRIVATE_CONSTANT_107 = "wvs_csv_title";

    @RBEntry("BOM Report Preferences")
    public static final String PRIVATE_CONSTANT_108 = "bom_report_preferences";

    @RBEntry("ProjectLink Base Data")
    public static final String PRIVATE_CONSTANT_109 = "pjl_base_data_title";

    @RBEntry("ProjectLink Document Soft Types")
    public static final String PRIVATE_CONSTANT_110 = "pjl_softtypes_csv_title";

    @RBEntry("Base Criterion Definitions")
    public static final String PRIVATE_CONSTANT_111 = "basecriteriondef_csv_title";

    @RBEntry("CAD Authoring Application Versions")
    public static final String PRIVATE_CONSTANT_112 = "epmauthoringappversion_csv_title";

    @RBEntry("Windchill Workgroup Manager Preferences")
    public static final String PRIVATE_CONSTANT_113 = "uwgmpreference_csv_title";

    @RBEntry("Windchill Workgroup Manager Client Preferences")
    public static final String PRIVATE_CONSTANT_114 = "uwgmclientpreference_csv_title";

    @RBEntry("Reports")
    public static final String PRIVATE_CONSTANT_115 = "reports_csv_title";

    @RBEntry("Reporting Access Rules")
    public static final String PRIVATE_CONSTANT_116 = "load_reporting_access_rules_csv_title";

    @RBEntry("Reports Demo")
    public static final String PRIVATE_CONSTANT_117 = "reportsdeom_csv_title";

    @RBEntry("Shared Team Template")
    public static final String PRIVATE_CONSTANT_118 = "shtm_csv_title";

    @RBEntry("Collector Preferences")
    public static final String PRIVATE_CONSTANT_119 = "collectorpreference_csv_title";

    @RBEntry("Default Relationship Constraint")
    public static final String PRIVATE_CONSTANT_120 = "default_association_constraint";

    @RBEntry("Access Rules for Replication")
    public static final String PRIVATE_CONSTANT_121 = "replication_access_rules_title";

    @RBEntry("Distributed Process Schedule Soft Type")
    public static final String PRIVATE_CONSTANT_122 = "DP_SCHEDULE_SOFTTYPE";

    @RBEntry("Federation Mapping Preferences")
    public static final String PRIVATE_CONSTANT_123 = "FederationMapping_csv_title";

    @RBEntry("Interference Detection Definition Soft Types")
    public static final String PRIVATE_CONSTANT_124 = "clash_def_type_csv_title";

    @RBEntry("Change Activity Attribute Layouts")
    public static final String PRIVATE_CONSTANT_125 = "type_attribute_change_activity";

    @RBEntry("Problem Report Attribute Layouts")
    public static final String PRIVATE_CONSTANT_126 = "type_attribute_problem_report";

    @RBEntry("Variance Attribute Layouts")
    public static final String PRIVATE_CONSTANT_127 = "type_attribute_variance";

    @RBEntry("Change Notice Attribute Layouts")
    public static final String PRIVATE_CONSTANT_128 = "type_attribute_change_notice";

    @RBEntry("Change Request Attribute Layouts")
    public static final String PRIVATE_CONSTANT_129 = "type_attribute_change_request";

    @RBEntry("Promotion Request Attribute Layouts")
    public static final String PRIVATE_CONSTANT_130 = "type_attribute_promotion_request";

    @RBEntry("Work Set Attribute Layouts")
    public static final String WORK_SET_ATTRIBUTE_LAYOUTS = "type_attribute_work_set";

    @RBEntry("Change Action Item Attribute Layouts")
    public static final String PRIVATE_CONSTANT_131 = "type_attribute_changeActionItem";

    @RBEntry("Report Attribute Layouts")
    public static final String PRIVATE_CONSTANT_132 = "type_attribute_report";

    @RBEntry("Affected Activity Attribute Layouts")
    public static final String PRIVATE_CONSTANT_133 = "type_attribute_affected_activity_data";

    @RBEntry("Part Attribute Layouts")
    public static final String PRIVATE_CONSTANT_134 = "type_attribute_wtpart";

    @RBEntry("Part Master Attribute Layouts")
    public static final String PRIVATE_CONSTANT_135 = "type_attribute_wtpartmaster";

    @RBEntry("Managed Baseline Attribute Layouts")
    public static final String PRIVATE_CONSTANT_136 = "type_attribute_managedbaseline";

    @RBEntry("ECAD Attributes")
    public static final String PRIVATE_CONSTANT_137 = "uwgmecad_attributes";

    @RBEntry("ECAD EPM Soft Types Init Rules")
    public static final String PRIVATE_CONSTANT_138 = "uwgmecad_EPM_softTypes_InitRules";

    @RBEntry("ECAD Soft Types")
    public static final String PRIVATE_CONSTANT_139 = "uwgmecad_softTypes";

    @RBEntry("ECAD - BOM Attributes Mapping Template")
    public static final String PRIVATE_CONSTANT_140 = "ecadBOMAttributeMapTemplate";

    @RBEntry("ECAD Content Definition Template")
    public static final String PRIVATE_CONSTANT_141 = "ecadContentDefinitionTemplate";

    @RBEntry("ECAD Hook Template for Create List of Design Files")
    public static final String PRIVATE_CONSTANT_142 = "ecadHookCreateDesignFileListTemplate";

    @RBEntry("ECAD Hook Template for Create BOM")
    public static final String PRIVATE_CONSTANT_143 = "ecadHookCreateGenericBOMTemplate";

    @RBEntry("ECAD Hook Template for Create Variant BOMs")
    public static final String PRIVATE_CONSTANT_144 = "ecadHookCreateVariantBOMTemplate";

    @RBEntry("ECAD Hook Template for Create List of Attachments")
    public static final String PRIVATE_CONSTANT_145 = "ecadHookCreateListAttachmentsTemplate";

    @RBEntry("ECAD Hook Template for Create Viewable")
    public static final String PRIVATE_CONSTANT_146 = "ecadHookCreateViewableTemplate";

    @RBEntry("ECAD Hook Template for Get Design Directory")
    public static final String PRIVATE_CONSTANT_147 = "ecadHookGetDesignDirectoryTemplate";

    @RBEntry("ECAD Hook Template for Get Design Item List")
    public static final String PRIVATE_CONSTANT_148 = "ecadHookGetDesignItemListTemplate";

    @RBEntry("ECAD Hook Template for Get List of Neutral Format Items")
    public static final String PRIVATE_CONSTANT_149 = "ecadHookGetNeutralFormatItemListTemplate";

    @RBEntry("ECAD Hook Template for Pre Update")
    public static final String PRIVATE_CONSTANT_150 = "ecadHookPreUpdateTemplate";

    @RBEntry("ECAD Hook Template for Propagate Attributes to Design")
    public static final String PRIVATE_CONSTANT_151 = "ecadHookPropagateIBAsToDesignTemplate";

    @RBEntry("ECAD Hook Template for Retrieve Attributes from Design")
    public static final String PRIVATE_CONSTANT_152 = "ecadHookRetrieveDesignAttributesTemplate";

    @RBEntry("ECAD Hook Template for Validate Design")
    public static final String PRIVATE_CONSTANT_153 = "ecadHookValidateDesignTemplate";

    @RBEntry("Windchill Gateway for Creo Elements/Direct Model Manager data loader")
    public static final String PRIVATE_CONSTANT_154 = "com/ptc/windchill/dpimpl/load/CocreateMMLoad.xml";

    @RBEntry("WorkItem Attribute Layouts")
    public static final String PRIVATE_CONSTANT_155 = "type_attribute_workitem";

    @RBEntry("BOM Reports")
    public static final String PRIVATE_CONSTANT_156 = "BOM_csv_title";

    @RBEntry("DiscussionPosting Attribute Layouts")
    public static final String PRIVATE_CONSTANT_157 = "type_attribute_discussionPosting";

    @RBEntry("MeetingCenterMeeting Attribute Layouts")
    public static final String PRIVATE_CONSTANT_158 = "type_attribute_meetingCenterMeeting";

    @RBEntry("TraditionalMeeting Attribute Layouts")
    public static final String PRIVATE_CONSTANT_159 = "type_attribute_traditionalMeeting";

    @RBEntry("Change Proposal Attribute Layouts")
    public static final String PRIVATE_CONSTANT_160 = "type_attribute_changeProposal";

    @RBEntry("Change Investigation Attribute Layouts")
    public static final String PRIVATE_CONSTANT_161 = "type_attribute_changeInvestigation";

    @RBEntry("Analysis Activity Attribute Layouts")
    public static final String PRIVATE_CONSTANT_162 = "type_attribute_analysisActivity";

    @RBEntry("Configuration Context Attribute Layouts")
    public static final String PRIVATE_CONSTANT_163 = "type_attribute_wtpartalternaterep";

    @RBEntry("ECAD EPM Document Soft Types")
    public static final String PRIVATE_CONSTANT_164 = "uwgmecad_EPM_softTypes";

    @RBEntry("Default Home Page tables")
    public static final String PRIVATE_CONSTANT_165 = "default_homepage_tables";

    @RBEntry("Equivalence Link Attribute Layouts")
    public static final String PRIVATE_CONSTANT_166 = "type_attribute_equivalenceLink";

    @RBEntry("ProjectActivity Attribute Layouts")
    public static final String PRIVATE_CONSTANT_167 = "type_attribute_ProjectActivity";

    @RBEntry("Milestone Attribute Layouts")
    public static final String PRIVATE_CONSTANT_168 = "type_attribute_Milestone";

    @RBEntry("SummaryActivity Attribute Layouts")
    public static final String PRIVATE_CONSTANT_169 = "type_attribute_SummaryActivity";

    @RBEntry("Person Resource Attribute Layouts")
    public static final String PRIVATE_CONSTANT_170 = "type_attribute_PersonResource";

    @RBEntry("Role Resource Attribute Layouts")
    public static final String PRIVATE_CONSTANT_171 = "type_attribute_RoleResource";

    @RBEntry("Material Resource Attribute Layouts")
    public static final String PRIVATE_CONSTANT_172 = "type_attribute_MaterialResource";

    @RBEntry("Equipment Resource Attribute Layouts")
    public static final String PRIVATE_CONSTANT_173 = "type_attribute_EquipmentResource";

    @RBEntry("Facility Resource Attribute Layouts")
    public static final String PRIVATE_CONSTANT_174 = "type_attribute_FacilityResource";

    @RBEntry("Deliverable Attribute Layouts")
    public static final String PRIVATE_CONSTANT_175 = "type_attribute_classicDeliverable";

    @RBEntry("ECAD Part Types")
    public static final String PRIVATE_CONSTANT_176 = "uwgmecad_Part_softTypes";

    @RBEntry("ECAD Part Soft Types Init Rules")
    public static final String PRIVATE_CONSTANT_177 = "uwgmecad_Part_softTypes_InitRules";

    @RBEntry("Install Shipbuilding Part Soft Types Load-Set")
    public static final String PRIVATE_CONSTANT_178 = "com/ptc/windchill/dpimpl/load/SBTLoad.xml";

    @RBEntry("Install Software Link Soft Types Load-Set")
    public static final String PRIVATE_CONSTANT_179 = "com/ptc/swlink/load/SoftwareLinkLoad.xml";

    @RBEntry("Interference Detection Definition Soft Type")
    public static final String PRIVATE_CONSTANT_180 = "type_interference_detection";

    @RBEntry("WTDocument Base Definitions")
    public static final String PRIVATE_CONSTANT_181 = "type_attribute_documentType";

    @RBEntry("WTDocumentMaster Base Definitions")
    public static final String PRIVATE_CONSTANT_182 = "type_attribute_documentMasterType";

    @RBEntry("Product Attribute Layouts")
    public static final String PRIVATE_CONSTANT_183 = "type_attribute_wtproduct";

    @RBEntry("Library Attribute Layouts")
    public static final String PRIVATE_CONSTANT_184 = "type_attribute_wtlibrary";

    @RBEntry("Change Directive Attribute Layouts")
    public static final String PRIVATE_CONSTANT_185 = "type_attribute_change_directive";

    @RBEntry("Install Quality Load-Set")
    public static final String QMS_DATA_LOAD = "com/ptc/qualitymanagement/qms/load/QMSLoad.xml";

    @RBEntry("Install Quality Masterdata Load-Set")
    public static final String MASTERDATA_DATA_LOAD = "com/ptc/qualitymanagement/masterdata/load/MasterdataLoad.xml";

    @RBEntry("Install Quality CAPA Load-Set")
    public static final String CAPA_DATA_LOAD = "com/ptc/qualitymanagement/capa/load/CAPALoad.xml";

    @RBEntry("Install Quality NC Load-Set")
    public static final String NC_DATA_LOAD = "com/ptc/qualitymanagement/nc/load/NCLoad.xml";

    @RBEntry("Install Quality Disposition Load-Set")
    public static final String DISPOSITION_DATA_LOAD = "com/ptc/qualitymanagement/disposition/load/DispositionLoad.xml";

    @RBEntry("Install Quality Customer Experience Management Load-Set")
    @RBComment("DO NOT TRANSLATE")
    public static final String CEM_DATA_LOAD = "com/ptc/qualitymanagement/cem/load/CEMLoad.xml";

    @RBEntry("Install Quality CEM EMDR Report Load-Set")
    @RBComment("DO NOT TRANSLATE")
    public static final String EMDR_DATA_LOAD = "com/ptc/qualitymanagement/cem/load/EMDRLoad.xml";

    @RBEntry("Install Quality CEM Canada Report Load-Set")
    @RBComment("DO NOT TRANSLATE")
    public static final String CANADA_DATA_LOAD = "com/ptc/qualitymanagement/cem/load/CanadaLoad.xml";

    @RBEntry("Install Quality CEM Vigilance Report Load-Set")
    @RBComment("DO NOT TRANSLATE")
    public static final String VIGILANCE_DATA_LOAD = "com/ptc/qualitymanagement/cem/load/VigilanceLoad.xml";

    @RBEntry("Install UDI Load-Set")
    public static final String UDI_DATA_LOAD = "com/ptc/qualitymanagement/udi/load/UDILoad.xml";

    @RBEntry("Install GPD Load-Set")
    public static final String GPD_DATA_LOAD = "com/ptc/smb/load/GPDLoad.xml";

    @RBEntry("Federation Credentials Mapping Preference")
    public static final String PRIVATE_CONSTANT_186 = "wt_federation_credmap_preferences";

    @RBEntry("Visualization Life Cycle Templates")
    public static final String PRIVATE_CONSTANT_187 = "wvs_lifecycle_templates";

    @RBEntry("Part Usage Attribute Layouts")
    public static final String PRIVATE_CONSTANT_188 = "type_attribute_wtpartusagelink";

    @RBEntry("Uses Occurrence Attribute Layouts")
    public static final String PRIVATE_CONSTANT_189 = "type_attribute_usesocurrence";

    @RBEntry("ECAD - BOM Filter Definition Template")
    public static final String PRIVATE_CONSTANT_190 = "ecadBOMFilterDefinitionTemplate";

    @RBEntry("Install Service Information Manager Load-Set")
    public static final String PRIVATE_CONSTANT_191 = "com/ptc/arbortext/windchill/siscore/load/sisload.xml";

    @RBEntry("Install Parts Information Manager Load-Set")
    public static final String PRIVATE_CONSTANT_192 = "com/ptc/arbortext/windchill/partlist/load/sisipcload.xml";

    @RBEntry("Change Action Attribute Layouts")
    public static final String PRIVATE_CONSTANT_193 = "type_attribute_change_action";

    @RBEntry("InterComm Data Soft Types")
    public static final String PRIVATE_CONSTANT_194 = "intercomm_data_type_csv_title";

    @RBEntry("Creo Templates")
    public static final String PRIVATE_CONSTANT_195 = "creoTemplates";

    @RBEntry("CATIAV5 Templates")
    public static final String PRIVATE_CONSTANT_196 = "catiav5Templates";

    @RBEntry("NX Templates")
    public static final String PRIVATE_CONSTANT_197 = "nxTemplates";

    @RBEntry("ECAD EPM Reference Link Attributes")
    public static final String PRIVATE_CONSTANT_198 = "uwgmecad_EPMRefLinkAttrs";

    @RBEntry("Received Delivery Initialization Rules")
    public static final String PRIVATE_CONSTANT_200 = "RDInitRules_csv_title";

    @RBEntry("Received Delivery type attributes")
    public static final String PRIVATE_CONSTANT_201 = "RDTypeAttr_csv_title";

    @RBEntry("ECAD Content Template")
    public static final String PRIVATE_CONSTANT_202 = "ecadContentTemplate";

    @RBEntry("ECAD Drawing Template")
    public static final String PRIVATE_CONSTANT_203 = "ecadDrawingTemplate";

    @RBEntry("ECAD and MCAD Exchange Data Template")
    public static final String PRIVATE_CONSTANT_204 = "ecadMcadExchangeDataTemplate";

    @RBEntry("ECAD Packaged View Template")
    public static final String PRIVATE_CONSTANT_205 = "ecadPackagedViewTemplate";

    @RBEntry("ECAD Primary Object Templates")
    public static final String PRIVATE_CONSTANT_206 = "ecadPrimaryObjectTemplates";

    @RBEntry("Part Configuration Attribute Layouts")
    public static final String PRIVATE_CONSTANT_207 = "type_attribute_wtproductconfiguration";

    @RBEntry("Part Instance Attribute Layouts")
    public static final String PRIVATE_CONSTANT_208 = "type_attribute_wtproductinstance";

    @RBEntry("Advanced Assembly Editor Preferences")
    public static final String PRIVATE_CONSTANT_209 = "ate_csv_title";

    @RBEntry("Bookmark Attribute Layouts")
    public static final String PRIVATE_CONSTANT_210 = "type_attribute_Bookmark";

    @RBEntry("Action Item Attribute Layouts")
    public static final String PRIVATE_CONSTANT_211 = "type_attribute_Actionitem";

    @RBEntry("ECAD BluePrint-PCB Template")
    public static final String PRIVATE_CONSTANT_212 = "ecadBluePrintPCBTemplate";

    @RBEntry("ECAD CAM350 Template")
    public static final String PRIVATE_CONSTANT_213 = "ecadCAM350Template";

    @RBEntry("ECAD Documentation Soft Type")
    public static final String PRIVATE_CONSTANT_214 = "ecadDocumentationSoftType";

    @RBEntry("ECAD Manufacturing Soft Type")
    public static final String PRIVATE_CONSTANT_215 = "ecadManufacturingSoftType";

    @RBEntry("Install MPMLink Maintenance Load-Set")
    public static final String PRIVATE_CONSTANT_216 = "com/ptc/windchill/mpml/load/mpmlinkLoadR10M020.xml";

    @RBEntry("DiscussionTopic Attribute Layouts")
    public static final String PRIVATE_CONSTANT_217 = "type_attribute_discussionTopic";

    @RBEntry("Mobile Application Preferences")
    public static final String PRIVATE_CONSTANT_218 = "mobileapp_csv_title";

    @RBEntry("ERPMaterial Document Describe Link Attribute Layouts")
    public static final String PRIVATE_CONSTANT_219 = "type_attribute_erpMaterialDocumentDescribeLink";

    @RBEntry("Install Options and Variants Load-Set")
    public static final String PRIVATE_CONSTANT_220 = "com/ptc/windchill/option/load/OptionsCoreLoad.xml";

    @RBEntry("AutoCAD Templates")
    public static final String PRIVATE_CONSTANT_221 = "acadTemplates";

    @RBEntry("Inventor Templates")
    public static final String PRIVATE_CONSTANT_222 = "inventorTemplates";

    @RBEntry("Solidworks Templates")
    public static final String PRIVATE_CONSTANT_223 = "solidworksTemplates";

    @RBEntry("Install MPMLink Incremental Load-Set")
    public static final String PRIVATE_CONSTANT_224 = "com/ptc/windchill/mpml/load/mpmlinkLoadR102.xml";

    @RBEntry("Load preferences for Import/Export from Spreadsheet")
    public static final String PRIVATE_CONSTANT_225 = "Import Export from SpreadSheet Preferences";

    @RBEntry("Install work package Test Data")
    public static final String PRIVATE_CONSTANT_226 = "com/ptc/windchill/wp/load/WorkPackageTestLoad.xml";

    @RBEntry("Load for Configuration Items and Design Solutions")
    public static final String PRIVATE_CONSTANT_227 = "com/ptc/windchill/enterprise/cids/load/cidsLoad.xml";

    @RBEntry("Load for Setup for Configuration Items and Design Solutions")
    public static final String PRIVATE_CONSTANT_228 = "com/ptc/windchill/enterprise/cids/load/cidscfgLoad.xml";

    @RBEntry("Load for ESI for Options and Variants")
    public static final String PRIVATE_CONSTANT_229 = "com/ptc/windchill/esi/load/esiOVLoad.xml";

    @RBEntry("Install Associativity Load-Set")
    public static final String PRIVATE_CONSTANT_230 = "com/ptc/windchill/associativity/load/associativityLoadR102.xml";

    @RBEntry("Install STEP data exchange Load-Set")
    public static final String PRIVATE_CONSTANT_231 = "wt/ixb/step/stepdex.xml";

    @RBEntry("Install PTC Service Center Load-Set")
    public static final String PRIVATE_CONSTANT_232 = "com/ptc/sc/loadfiles/servicecenterLoad.xml";

    @RBEntry("Install PTC Service Center Demo Load-Set")
    public static final String PRIVATE_CONSTANT_233 = "com/ptc/sc/loadfiles/servicecenterDemoLoad.xml";

    @RBEntry("PTC Windchill Service Information Module for S1000D Load-Set")
    public static final String PRIVATE_CONSTANT_234 = "com/ptc/arbortext/windchill/rules/load/rulesload.xml";

    @RBEntry("Install MPMLink Maintenance Load-Set")
    public static final String PRIVATE_CONSTANT_235 = "com/ptc/windchill/mpml/load/mpmlinkLoadR102M020.xml";
}
