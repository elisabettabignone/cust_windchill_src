/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.auth;

import wt.util.resource.*;

@RBUUID("wt.auth.authResource")
public final class authResource extends WTListResourceBundle {
   @RBEntry("no applicable authentication handler")
   public static final String NO_AUTH_HANDLER = "0";

   @RBEntry("authentication failed")
   public static final String AUTHENTICATION_FAILED = "1";

   @RBEntry("re-authentication is not allowed")
   public static final String REAUTHENTICATION_NOT_ALLOWED = "2";

   @RBEntry("Null Authentication not enabled")
   public static final String NULL_AUTHENTICATION_NOT_ENABLED = "3";

   @RBEntry("Access denied")
   public static final String ACCESS_DENIED = "4";

   @RBEntry("Null login failed.")
   public static final String NULL_LOGIN_FAILED = "5";

   @RBEntry("Missing user name in simple authenticator")
   public static final String MISSING_USER_NAME = "6";
}
