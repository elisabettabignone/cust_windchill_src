/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.auth;

import wt.util.resource.*;

@RBUUID("wt.auth.authResource")
public final class authResource_it extends WTListResourceBundle {
   @RBEntry("nessun gestore autenticazione pertinente")
   public static final String NO_AUTH_HANDLER = "0";

   @RBEntry("autenticazione non riuscita")
   public static final String AUTHENTICATION_FAILED = "1";

   @RBEntry("riautenticazione non consentita")
   public static final String REAUTHENTICATION_NOT_ALLOWED = "2";

   @RBEntry("Autenticazione nulla non attivata")
   public static final String NULL_AUTHENTICATION_NOT_ENABLED = "3";

   @RBEntry("Accesso negato")
   public static final String ACCESS_DENIED = "4";

   @RBEntry("Accesso nullo non riuscito.")
   public static final String NULL_LOGIN_FAILED = "5";

   @RBEntry("Nome utente mancante in autenticatore base")
   public static final String MISSING_USER_NAME = "6";
}
