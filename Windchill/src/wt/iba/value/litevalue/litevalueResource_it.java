/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.iba.value.litevalue;

import wt.util.resource.*;

@RBUUID("wt.iba.value.litevalue.litevalueResource")
public final class litevalueResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile creare un riferimento per l'oggetto non persistente \"{0}\".")
   public static final String NOT_A_PERSISTABLE_OBJECT = "lv0";

   @RBEntry("Tipo di valore incompatible: \"{0}\"")
   public static final String INCOMPATIBLE_FLOAT_VIEW = "lv1";

   @RBEntry("Errore di confronto: valore vuoto.")
   public static final String NULL_VALUE = "lv10";

   @RBEntry("Aggiornamento non riuscito. Le classi dell'oggetto non sono compatibili. Classe 1: \"{0}\" Classe 2: \"{1}\"")
   public static final String UPDATE_FAILED_INCOMPATABLE_CLASSES = "lv11";

   @RBEntry("L'operazione non ha rispettato i vincoli. Il valore \"{0}\" non rispetta i vincoli.")
   public static final String FAILED_CONSTRAINT = "lv12";

   @RBEntry("Tipo di valore incompatible: \"{0}\"")
   public static final String INCOMPATIBLE_INTEGER_VIEW = "lv2";

   @RBEntry("Tipo di valore incompatible: \"{0}\"")
   public static final String INCOMPATIBLE_RATIO_VIEW = "lv3";

   @RBEntry("Tipo di valore incompatible: \"{0}\"")
   public static final String INCOMPATIBLE_REFERENCE_VIEW = "lv4";

   @RBEntry("Tipo di valore incompatible: \"{0}\"")
   public static final String INCOMPATIBLE_STRING_VIEW = "lv5";

   @RBEntry("Tipo di valore incompatible: \"{0}\"")
   public static final String INCOMPATIBLE_TIMESTAMP_VIEW = "lv6";

   @RBEntry("Tipo di valore incompatible: \"{0}\"")
   public static final String INCOMPATIBLE_UNIT_VIEW = "lv7";

   @RBEntry("Tipo di valore incompatible: \"{0}\"")
   public static final String INCOMPATIBLE_URL_VIEW = "lv8";

   @RBEntry("Tipo di valore incompatible: \"{0}\"")
   public static final String INCOMPATIBLE_BOOLEAN_VIEW = "lv9";

   @RBEntry("True")
   public static final String LABEL_TRUE = "lv91";

   @RBEntry("False")
   public static final String LABEL_FALSE = "lv92";
}
