/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.iba.value.litevalue;

import wt.util.resource.*;

@RBUUID("wt.iba.value.litevalue.litevalueResource")
public final class litevalueResource extends WTListResourceBundle {
   @RBEntry("Cannot create a reference to non-persistable object \"{0}\".")
   public static final String NOT_A_PERSISTABLE_OBJECT = "lv0";

   @RBEntry("Incompatible value type: \"{0}\"")
   public static final String INCOMPATIBLE_FLOAT_VIEW = "lv1";

   @RBEntry("Comparison Error: Empty value.")
   public static final String NULL_VALUE = "lv10";

   @RBEntry("Update failed.  Object Classes are not compatable.  Class 1: \"{0}\" Class 2: \"{1}\"")
   public static final String UPDATE_FAILED_INCOMPATABLE_CLASSES = "lv11";

   @RBEntry("Operation failed constraints. Value \"{0}\" failed constraint.")
   public static final String FAILED_CONSTRAINT = "lv12";

   @RBEntry("Incompatible value type: \"{0}\"")
   public static final String INCOMPATIBLE_INTEGER_VIEW = "lv2";

   @RBEntry("Incompatible value type: \"{0}\"")
   public static final String INCOMPATIBLE_RATIO_VIEW = "lv3";

   @RBEntry("Incompatible value type: \"{0}\"")
   public static final String INCOMPATIBLE_REFERENCE_VIEW = "lv4";

   @RBEntry("Incompatible value type: \"{0}\"")
   public static final String INCOMPATIBLE_STRING_VIEW = "lv5";

   @RBEntry("Incompatible value type: \"{0}\"")
   public static final String INCOMPATIBLE_TIMESTAMP_VIEW = "lv6";

   @RBEntry("Incompatible value type: \"{0}\"")
   public static final String INCOMPATIBLE_UNIT_VIEW = "lv7";

   @RBEntry("Incompatible value type: \"{0}\"")
   public static final String INCOMPATIBLE_URL_VIEW = "lv8";

   @RBEntry("Incompatible value type: \"{0}\"")
   public static final String INCOMPATIBLE_BOOLEAN_VIEW = "lv9";

   @RBEntry("True")
   public static final String LABEL_TRUE = "lv91";

   @RBEntry("False")
   public static final String LABEL_FALSE = "lv92";
}
