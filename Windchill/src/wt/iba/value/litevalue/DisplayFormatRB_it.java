/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.iba.value.litevalue;

import wt.util.resource.*;

@RBUUID("wt.iba.value.litevalue.DisplayFormat") //UUID remains unchanged when the file is moved
public final class DisplayFormatRB_it extends WTListResourceBundle {
   @RBEntry("dd/MM/yyyy HH.mm.ss")
   @RBPseudo(false)
   @RBComment("this field should be translated to the locale specific format")
   public static final String TIMESTAMP_OUTPUT_FORMAT = "timestampOutputFormat";
}
