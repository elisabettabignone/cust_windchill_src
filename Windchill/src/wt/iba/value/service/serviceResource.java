/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.iba.value.service;

import wt.util.resource.*;

@RBUUID("wt.iba.value.service.serviceResource")
public final class serviceResource extends WTListResourceBundle {
   @RBEntry("Values associated with incorrect object.")
   public static final String IBAHOLDER_ID_DIFFERENT = "0";

   @RBEntry("Object cannot be persisted.")
   public static final String CANT_PERSIST_LITE_OBJECT = "1";

   @RBEntry("Value \"{0}\" does not meet associated constraints.")
   public static final String CONSTRAINT_VIOLATION = "2";

   @RBEntry("Constraint validation failed.")
   public static final String CONSTRAINT_VALIDATION_FAILED = "3";

   @RBEntry("Referenceables associated with incorrect object.")
   public static final String IBAHOLDER_REFERENCEABLE_DIFFERENT = "4";
}
