/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.iba.value.service;

import wt.util.resource.*;

@RBUUID("wt.iba.value.service.serviceResource")
public final class serviceResource_it extends WTListResourceBundle {
   @RBEntry("Valori associati ad oggetti non corretti.")
   public static final String IBAHOLDER_ID_DIFFERENT = "0";

   @RBEntry("L'oggetto non può essere reso persistente.")
   public static final String CANT_PERSIST_LITE_OBJECT = "1";

   @RBEntry("Il valore \"{0}\" non rispetta i vincoli imposti.")
   public static final String CONSTRAINT_VIOLATION = "2";

   @RBEntry("La convalida dei vincoli è fallita.")
   public static final String CONSTRAINT_VALIDATION_FAILED = "3";

   @RBEntry("Oggetti di riferimento associati a un oggetto non corretto.")
   public static final String IBAHOLDER_REFERENCEABLE_DIFFERENT = "4";
}
