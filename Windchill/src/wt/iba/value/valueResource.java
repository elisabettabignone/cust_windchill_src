/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.iba.value;

import wt.util.resource.*;

@RBUUID("wt.iba.value.valueResource")
public final class valueResource extends WTListResourceBundle {
   @RBEntry("\"{0}\" is required.")
   public static final String DEFINITION_REQUIRED = "0";

   @RBEntry("Attributes")
   public static final String DISPLAY_ATTRIBUTES_HEADER = "1";

   @RBEntry("No differences were found")
   public static final String NO_DIFFERENCE_FOUND = "2";

   @RBEntry("Related Items")
   public static final String RELATED_ITEMS_HEADER = "3";

   @RBEntry("Attributes:")
   public static final String DISPLAY_COMPARISON_HEADER = "4";

   @RBEntry("Attribute Editor")
   public static final String IBA_DIALOG_TITLE = "5";
}
