/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.iba.definition;

import wt.util.resource.*;

@RBUUID("wt.iba.definition.definitionResource")
public final class definitionResource extends WTListResourceBundle {
   @RBEntry("Initialization of attribute definition with parameters : \"{0}\" , \"{1}\" , \"{2}\", \"{3}\" failed.")
   public static final String INITIALIZE_DEFINITION_FAILED = "0";

   @RBEntry("All attribute definitions must have a parent.")
   public static final String NULL_PARENT = "1";

   @RBEntry("Please provide a value for \"Name\".")
   public static final String NULL_NAME_VALUE = "2";
}
