/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.iba.definition;

import wt.util.resource.*;

@RBUUID("wt.iba.definition.definitionResource")
public final class definitionResource_it extends WTListResourceBundle {
   @RBEntry("L'inizializzazione della definizione degli attributi con i parametri \"{0}\" , \"{1}\" , \"{2}\", \"{3}\" è fallita.")
   public static final String INITIALIZE_DEFINITION_FAILED = "0";

   @RBEntry("Tutte le definizioni degli attributi devono avere una definizione padre.")
   public static final String NULL_PARENT = "1";

   @RBEntry("Fornire un valore per \"Nome\".")
   public static final String NULL_NAME_VALUE = "2";
}
