/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.iba.definition.litedefinition;

import wt.util.resource.*;

@RBUUID("wt.iba.definition.litedefinition.litedefinitionResource")
public final class litedefinitionResource extends WTListResourceBundle {
   @RBEntry("Failed to create attribute.")
   public static final String DEFAULT_VIEW_FAILED = "ld0";

   @RBEntry("Failed to create attribute or attribute organizer.")
   public static final String CANT_SET_PARENT = "ld1";

   @RBEntry("Failed to create attribute organizer.")
   public static final String ORGANIZER_VIEW_FAILED = "ld2";

   @RBEntry("An attribute organizer cannot be below an attribute.")
   public static final String ILLEGAL_ADD_PARENT = "ld3";

   @RBEntry("Failed to create attribute definition.")
   public static final String ATTR_DEF_FAILED = "ld4";
}
