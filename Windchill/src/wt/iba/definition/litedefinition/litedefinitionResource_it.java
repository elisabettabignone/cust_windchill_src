/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.iba.definition.litedefinition;

import wt.util.resource.*;

@RBUUID("wt.iba.definition.litedefinition.litedefinitionResource")
public final class litedefinitionResource_it extends WTListResourceBundle {
   @RBEntry("La creazione di attributi è fallita.")
   public static final String DEFAULT_VIEW_FAILED = "ld0";

   @RBEntry("Creazione di attributi o di organizer di attributi non riuscita.")
   public static final String CANT_SET_PARENT = "ld1";

   @RBEntry("Creazione di organizer di attributi non riuscita.")
   public static final String ORGANIZER_VIEW_FAILED = "ld2";

   @RBEntry("Un organizer di attributi non può essere al di sotto di un attributo.")
   public static final String ILLEGAL_ADD_PARENT = "ld3";

   @RBEntry("La creazione della definizione di attributi è fallita.")
   public static final String ATTR_DEF_FAILED = "ld4";
}
