/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.iba.definition.service;

import wt.util.resource.*;

@RBUUID("wt.iba.definition.service.serviceResource")
public final class serviceResource_it extends WTListResourceBundle {
   @RBEntry("L'inizializzazione della definizione degli attributi con i parametri \"{0}\" , \"{1}\" , \"{2}\", \"{3}\" è fallita.")
   public static final String INITIALIZE_DEFINITION_FAILED = "00";

   @RBEntry("L'attributo padre non può essere nullo.")
   public static final String NULL_PARENT = "01";

   @RBEntry("L'attributo nome è obbligatorio.")
   public static final String NULL_NAME_VALUE = "02";

   @RBEntry("\"{0}\" deve avere un padre.")
   public static final String NULL_TARGET_NODE = "03";

   @RBEntry("Un attributo con nome \"{0}\" esiste già sotto \"{1}\".")
   public static final String NAME_UNIQUENESS_VIOLATION = "04";

   @RBEntry("Il tipo di dati di \"{0}\" è incompatibile con il tipo di dati di \"{1}\".")
   public static final String INVALID_PARENT_DATA_TYPE = "05";

   @RBEntry("L'attributo padre \"{0}\" non è valido.")
   public static final String INVALID_PARENT = "06";

   @RBEntry("Impossibile aggiornare \"{0}\" perché esiste già una versione più recente.")
   public static final String INVALID_UPDATE_COUNT = "07";

   @RBEntry("L'unità visualizzata \"{0}\" è incompatibile con l'unità associata a \"{1}\". (\"{02}\")")
   public static final String INCOMPATIBLE_UNIT = "08";

   @RBEntry("\"{0}\" non può essere eliminato perché è in uso al momento.")
   public static final String DEFINITION_IN_USE = "09";

   @RBEntry("Impossibile trovare la classe \"{0}\".")
   public static final String CLASS_NOT_FOUND = "10";

   @RBEntry("Impossibile restituire l'istanza di \"{0}\": input invalido.")
   public static final String INVALID_INPUT = "11";

   @RBEntry("Tipo di attributo non riconosciuto.")
   public static final String INVALID_DEF_TYPE = "12";

   @RBEntry("\"{0}\" deve avere una quantità di misura.")
   public static final String QOM_CANNOT_BE_NULL = "13";

   @RBEntry("\"{0}\" e \"{1}\" non hanno la stessa quantità di misura.")
   public static final String QOM_ARENOT_THE_SAME = "14";

   @RBEntry("\"{0}\" é un attributo radice. Non può essere copiato su un organizer.")
   public static final String ROOT_DEF_CANT_COPY_TO_ORG = "15";

   @RBEntry("Gli attributi possono esistere solo sotto un organizer.")
   public static final String ROOT_DEF_MUST_HAVE_ORG_PARENT = "16";

   @RBEntry("Radice attributo")
   public static final String ATTRIBUTE_ROOT = "17";

   @RBEntry("L'utente non è autorizzato a creare attributi utilizzando il dominio \"com.ptc.\" ")
   public static final String EX_INVALID_INTERNET_DOMAIN = "18";

   @RBEntry("Nome non valido: \"{0}\" non è associato ad alcun contesto di organizzazione conosciuto.")
   public static final String EX_INVALID_ORGANIZATION_CONTEXT_ASSOCIATION = "19";

   @RBEntry("L'utente non è autorizzato ad accedere a \"{0}\"")
   public static final String EX_CURRENT_USER_IS_NOT_AUTHORIZED = "20";

   @RBEntry("\"{0}\" non è compatibile con il dominio dell'organizer padre.")
   public static final String EX_INCOMPATIBLE_WITH_PARENT_DOMAIN = "21";

   @RBEntry("\"{0}\" non è compatibile con il dominio dell'organizer figlio.")
   public static final String EX_INCOMPATIBLE_WITH_CHILDREN_DOMAIN = "22";

   @RBEntry("Il server ha aggiunto automaticamente il contesto del dominio \"{0}\" davanti al nome, per una lunghezza totale di {1} caratteri. Il numero massimo consentito è di {2} caratteri, incluso il contesto del dominio.")
   public static final String EX_APPEND_DOMAIN_NAME_TOO_LONG = "23";

   @RBEntry("Un organizer di nome \"{0}\" esiste già sotto \"{1}\".")
   public static final String NAME_UNIQUENESS_VIOLATION_ORGANIZER = "24";

   @RBEntry("Una cartella di nome \"{0}\" esiste già sotto \"Radice attributo\".")
   public static final String NAME_UNIQUENESS_VIOLATION_ORGANIZER_UNDER_ATTRIBUTE_ROOT = "25";

   @RBEntry("Nome non valido: \"{0}\" è incompatibile con il contesto del dominio \"{1}\".")
   public static final String EX_INCOMPATIBLE_WITH_DOMAIN_CONTEXT = "26";
}
