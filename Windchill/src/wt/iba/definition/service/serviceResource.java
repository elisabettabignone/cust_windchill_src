/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.iba.definition.service;

import wt.util.resource.*;

@RBUUID("wt.iba.definition.service.serviceResource")
public final class serviceResource extends WTListResourceBundle {
   @RBEntry("Initialization of attribute definition with parameters : \"{0}\" , \"{1}\" , \"{2}\", \"{3}\" failed.")
   public static final String INITIALIZE_DEFINITION_FAILED = "00";

   @RBEntry("Parent cannot be null.")
   public static final String NULL_PARENT = "01";

   @RBEntry("Attribute name is required.")
   public static final String NULL_NAME_VALUE = "02";

   @RBEntry("\"{0}\" must have a parent.")
   public static final String NULL_TARGET_NODE = "03";

   @RBEntry("An attribute with the name \"{0}\" already exists below \"{1}\".")
   public static final String NAME_UNIQUENESS_VIOLATION = "04";

   @RBEntry("The data type of \"{0}\" is incompatible with the data type of \"{1}\".")
   public static final String INVALID_PARENT_DATA_TYPE = "05";

   @RBEntry("Parent \"{0}\" is invalid.")
   public static final String INVALID_PARENT = "06";

   @RBEntry("Cannot update \"{0}\" because a newer version already exists.")
   public static final String INVALID_UPDATE_COUNT = "07";

   @RBEntry("Display unit \"{0}\" is incompatible with the unit associated with \"{1}\". (\"{02}\")")
   public static final String INCOMPATIBLE_UNIT = "08";

   @RBEntry("\"{0}\" cannot be deleted because it is currently in use.")
   public static final String DEFINITION_IN_USE = "09";

   @RBEntry("Could not find Class \"{0}\".")
   public static final String CLASS_NOT_FOUND = "10";

   @RBEntry("Unable to return instance of \"{0}\": Input is invalid.")
   public static final String INVALID_INPUT = "11";

   @RBEntry("Unrecognized attribute type.")
   public static final String INVALID_DEF_TYPE = "12";

   @RBEntry("\"{0}\" must have a Quantity of Measure.")
   public static final String QOM_CANNOT_BE_NULL = "13";

   @RBEntry("\"{0}\" and \"{1}\" do not have the same Quantity Of Measure.")
   public static final String QOM_ARENOT_THE_SAME = "14";

   @RBEntry("\"{0}\" is a root attribute.  It cannot be copied to an Organizer.")
   public static final String ROOT_DEF_CANT_COPY_TO_ORG = "15";

   @RBEntry("Attributes can only exist below an Organizer.")
   public static final String ROOT_DEF_MUST_HAVE_ORG_PARENT = "16";

   @RBEntry("Attribute Root")
   public static final String ATTRIBUTE_ROOT = "17";

   @RBEntry("You are not allowed to create attributes using the com.ptc domain.\" ")
   public static final String EX_INVALID_INTERNET_DOMAIN = "18";

   @RBEntry("Invalid Name: \"{0}\" is not associated with any known organization context.")
   public static final String EX_INVALID_ORGANIZATION_CONTEXT_ASSOCIATION = "19";

   @RBEntry("You are not authorized to access \"{0}\"")
   public static final String EX_CURRENT_USER_IS_NOT_AUTHORIZED = "20";

   @RBEntry("\"{0}\" is incompatible with the parent organizer domain.")
   public static final String EX_INCOMPATIBLE_WITH_PARENT_DOMAIN = "21";

   @RBEntry("\"{0}\" is incompatible with children organizer domain.")
   public static final String EX_INCOMPATIBLE_WITH_CHILDREN_DOMAIN = "22";

   @RBEntry("The domain context \"{0}\" was automatically prepended to the Name when it reached the server, resulting in \"{1}\" characters.  The maximum allowable is \"{2}\" characters including the domain context.")
   public static final String EX_APPEND_DOMAIN_NAME_TOO_LONG = "23";

   @RBEntry("An organizer with the name \"{0}\" already exists below \"{1}\".")
   public static final String NAME_UNIQUENESS_VIOLATION_ORGANIZER = "24";

   @RBEntry("An organizer with the name \"{0}\" already exists below \"Attribute Root\".")
   public static final String NAME_UNIQUENESS_VIOLATION_ORGANIZER_UNDER_ATTRIBUTE_ROOT = "25";

   @RBEntry("Invalid Name: \"{0}\" is incompatible with the domain context \"{1}\".")
   public static final String EX_INCOMPATIBLE_WITH_DOMAIN_CONTEXT = "26";
}
