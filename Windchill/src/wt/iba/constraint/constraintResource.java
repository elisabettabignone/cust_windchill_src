/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.iba.constraint;

import wt.util.resource.*;

@RBUUID("wt.iba.constraint.constraintResource")
public final class constraintResource extends WTListResourceBundle {
   @RBEntry("Number of values is restricted.")
   public static final String CARDINALITY_SHORT_DESCRIPTION = "card0";

   @RBEntry("Only a certain number of values is allowed.")
   public static final String CARDINALITY_LONG_DESCRIPTION = "card1";

   @RBEntry("Cardinality Constraint")
   public static final String CARDINALITY_DISPLAY_NAME = "card2";

   @RBEntry("Legal values are restricted.")
   public static final String CONTENT_SHORT_DESCRIPTION = "cont0";

   @RBEntry("Only certain attribute values are legal.")
   public static final String CONTENT_LONG_DESCRIPTION = "cont1";

   @RBEntry("Content Constraint")
   public static final String CONTENT_DISPLAY_NAME = "cont2";

   @RBEntry("value <= {0}")
   public static final String FCRSHORTDESCRIPTION_L = "fcrmsg00";

   @RBEntry("Attribute values must be less than or equal to {0}.")
   public static final String FCRLONGDESCRIPTION_L = "fcrmsg01";

   @RBEntry("value => {0}")
   public static final String FCRSHORTDESCRIPTION_G = "fcrmsg02";

   @RBEntry("Attribute values must be greater than or equal to {0}.")
   public static final String FCRLONGDESCRIPTION_G = "fcrmsg03";

   @RBEntry("{0} <= value <= {1}")
   public static final String FCRSHORTDESCRIPTION = "fcrmsg04";

   @RBEntry("Attribute values must be between {0} and {1}, inclusive.")
   public static final String FCRLONGDESCRIPTION = "fcrmsg05";

   @RBEntry("Real Number Range")
   public static final String FCRDISPLYNAME = "fcrmsg06";

   @RBEntry("Value in set")
   public static final String FDSSHORTDESCRIPTION = "fdsmsg0";

   @RBEntry("Attribute values must be an element in the set [ {0} ].")
   public static final String FDSLONGDESCRIPTION = "fdsmsg1";

   @RBEntry("Real Number Set")
   public static final String FDSDISPLYNAME = "fdsmsg2";

   @RBEntry("Value in set")
   public static final String FUSHORTDESCRIPTION = "fumsg0";

   @RBEntry("Attribute values must be an element in set [ {0} ].")
   public static final String FULONGDESCRIPTION = "fumsg1";

   @RBEntry("Real Number Set, with units")
   public static final String FUDISPLYNAME = "fumsg2";

   @RBEntry("value <= {0}")
   public static final String ICRSHORTDESCRIPTION_L = "icrmsg00";

   @RBEntry("Attribute values must be less than or equal to {0}.")
   public static final String ICRLONGDESCRIPTION_L = "icrmsg01";

   @RBEntry("value >= {0}")
   public static final String ICRSHORTDESCRIPTION_G = "icrmsg02";

   @RBEntry("Attribute values must be greater than or equal to {0}.")
   public static final String ICRLONGDESCRIPTION_G = "icrmsg03";

   @RBEntry("{0} <= value <= {1}")
   public static final String ICRSHORTDESCRIPTION = "icrmsg04";

   @RBEntry("Attribute values must be between {0} and {1}, inclusive.")
   public static final String ICRLONGDESCRIPTION = "icrmsg05";

   @RBEntry("Integer Range")
   public static final String ICRDISPLYNAME = "icrmsg06";

   @RBEntry("Value in set")
   public static final String IDSSHORTDESCRIPTION = "idsmsg0";

   @RBEntry("Attribute values must be an element in the set [ {0} ].")
   public static final String IDSLONGDESCRIPTION = "idsmsg1";

   @RBEntry("Integer Set")
   public static final String IDSDISPLYNAME = "idsmsg2";

   @RBEntry("Values are immutable.")
   public static final String IMSHORTDESCRIPTION = "immsg0";

   @RBEntry("Attribute values cannot be added, removed, or changed.")
   public static final String IMLONGDESCRIPTION = "immsg1";

   @RBEntry("Immutable")
   public static final String IMDISPLYNAME = "immsg2";

   @RBEntry("Value in set")
   public static final String IUSHORTDESCRIPTION = "iumsg0";

   @RBEntry("Attribute values must be an element in set [ {0} ].")
   public static final String IULONGDESCRIPTION = "iumsg1";

   @RBEntry("Integer Set, with units")
   public static final String IUDISPLYNAME = "iumsg2";

   @RBEntry("Changes are restricted.")
   public static final String MUTABILITY_SHORT_DESCRIPTION = "muta0";

   @RBEntry("Only certain changes can be made to attribute values.")
   public static final String MUTABILITY_LONG_DESCRIPTION = "muta1";

   @RBEntry("Mutability Constraint")
   public static final String MUTABILITY_DISPLAY_NAME = "muta2";

   @RBEntry("Value in [ {0} ]")
   public static final String SDSSHORTDESCRIPTION = "sdsmsg0";

   @RBEntry("Attribute values must be elements of [ {0} ].")
   public static final String SDSLONGDESCRIPTION = "sdsmsg1";

   @RBEntry("Legal String Set")
   public static final String SDSDISPLYNAME = "sdsmsg2";

   @RBEntry("Single valued")
   public static final String SVSHORTDESCRIPTION = "svmsg0";

   @RBEntry("No more than one attribute value is allowed.")
   public static final String SVLONGDESCRIPTION = "svmsg1";

   @RBEntry("Single Valued")
   public static final String SVDISPLYNAME = "svmsg2";

   @RBEntry("value <= {0}")
   public static final String UCRSHORTDESCRIPTION_L = "ucrmsg00";

   @RBEntry("Attribute values must be less than or equal to {0}.")
   public static final String UCRLONGDESCRIPTION_L = "ucrmsg01";

   @RBEntry("value >= {0}")
   public static final String UCRSHORTDESCRIPTION_G = "ucrmsg02";

   @RBEntry("Attribute values must be greater than or equal to {0}.")
   public static final String UCRLONGDESCRIPTION_G = "ucrmsg03";

   @RBEntry("{0} <= value <= {1}")
   public static final String UCRSHORTDESCRIPTION = "ucrmsg04";

   @RBEntry("Attribute values must be between {0} and {1}, inclusive.")
   public static final String UCRLONGDESCRIPTION = "ucrmsg05";

   @RBEntry("Real Number Range, with Units")
   public static final String UCRDISPLYNAME = "ucrmsg06";

   @RBEntry("Value required")
   public static final String VRSHORTDESCRIPTION = "vrmsg0";

   @RBEntry("At least one attribute value has to exist.")
   public static final String VRLONGDESCRIPTION = "vrmsg1";

   @RBEntry("Value Required")
   public static final String VRDISPLYNAME = "vrmsg2";
}
