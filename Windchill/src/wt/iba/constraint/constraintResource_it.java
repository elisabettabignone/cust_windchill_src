/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.iba.constraint;

import wt.util.resource.*;

@RBUUID("wt.iba.constraint.constraintResource")
public final class constraintResource_it extends WTListResourceBundle {
   @RBEntry("Il numero di valori è limitato.")
   public static final String CARDINALITY_SHORT_DESCRIPTION = "card0";

   @RBEntry("Solo un certo numero di valori è ammesso.")
   public static final String CARDINALITY_LONG_DESCRIPTION = "card1";

   @RBEntry("Vincolo di cardinalità")
   public static final String CARDINALITY_DISPLAY_NAME = "card2";

   @RBEntry("I valori ammessi sono limitati.")
   public static final String CONTENT_SHORT_DESCRIPTION = "cont0";

   @RBEntry("Sono ammessi solo alcuni valori d'attributo.")
   public static final String CONTENT_LONG_DESCRIPTION = "cont1";

   @RBEntry("Vincolo di contenuto")
   public static final String CONTENT_DISPLAY_NAME = "cont2";

   @RBEntry("valore <= {0}")
   public static final String FCRSHORTDESCRIPTION_L = "fcrmsg00";

   @RBEntry("I valori degli attributi devono essere uguali o minori di {0}.")
   public static final String FCRLONGDESCRIPTION_L = "fcrmsg01";

   @RBEntry("valore => {0}")
   public static final String FCRSHORTDESCRIPTION_G = "fcrmsg02";

   @RBEntry("I valori degli attributi devono essere uguali o maggiori di {0}.")
   public static final String FCRLONGDESCRIPTION_G = "fcrmsg03";

   @RBEntry("{0} <= valore <= {1}")
   public static final String FCRSHORTDESCRIPTION = "fcrmsg04";

   @RBEntry("I valori degli attributi devono essere compresi tra {0} e {1} compresi.")
   public static final String FCRLONGDESCRIPTION = "fcrmsg05";

   @RBEntry("Intervallo di numeri reali")
   public static final String FCRDISPLYNAME = "fcrmsg06";

   @RBEntry("Valore della serie")
   public static final String FDSSHORTDESCRIPTION = "fdsmsg0";

   @RBEntry("I valori degli attributi devono essere un elemento della serie [ {0} ].")
   public static final String FDSLONGDESCRIPTION = "fdsmsg1";

   @RBEntry("Serie di numeri reali")
   public static final String FDSDISPLYNAME = "fdsmsg2";

   @RBEntry("Valore della serie")
   public static final String FUSHORTDESCRIPTION = "fumsg0";

   @RBEntry("I valori degli attributi devono essere un elemento della serie [ {0} ].")
   public static final String FULONGDESCRIPTION = "fumsg1";

   @RBEntry("Serie di numeri reali, con unità")
   public static final String FUDISPLYNAME = "fumsg2";

   @RBEntry("valore <= {0}")
   public static final String ICRSHORTDESCRIPTION_L = "icrmsg00";

   @RBEntry("I valori degli attributi devono essere uguali o minori di {0}.")
   public static final String ICRLONGDESCRIPTION_L = "icrmsg01";

   @RBEntry("valore >= {0}")
   public static final String ICRSHORTDESCRIPTION_G = "icrmsg02";

   @RBEntry("I valori degli attributi devono essere uguali o maggiori di {0}.")
   public static final String ICRLONGDESCRIPTION_G = "icrmsg03";

   @RBEntry("{0} <= valore <= {1}")
   public static final String ICRSHORTDESCRIPTION = "icrmsg04";

   @RBEntry("I valori degli attributi devono essere compresi tra {0} e {1} compresi.")
   public static final String ICRLONGDESCRIPTION = "icrmsg05";

   @RBEntry("Intervallo numeri interi")
   public static final String ICRDISPLYNAME = "icrmsg06";

   @RBEntry("Valore della serie")
   public static final String IDSSHORTDESCRIPTION = "idsmsg0";

   @RBEntry("I valori degli attributi devono essere un elemento della serie [ {0} ].")
   public static final String IDSLONGDESCRIPTION = "idsmsg1";

   @RBEntry("Serie numeri interi")
   public static final String IDSDISPLYNAME = "idsmsg2";

   @RBEntry("I valori sono immutabili.")
   public static final String IMSHORTDESCRIPTION = "immsg0";

   @RBEntry("I valori degli attributi non possono essere aggiunti, rimossi o modificati.")
   public static final String IMLONGDESCRIPTION = "immsg1";

   @RBEntry("Immutabile")
   public static final String IMDISPLYNAME = "immsg2";

   @RBEntry("Valore della serie")
   public static final String IUSHORTDESCRIPTION = "iumsg0";

   @RBEntry("I valori degli attributi devono essere un elemento della serie [ {0} ].")
   public static final String IULONGDESCRIPTION = "iumsg1";

   @RBEntry("Serie numeri interi, con unità")
   public static final String IUDISPLYNAME = "iumsg2";

   @RBEntry("Le modifiche sono limitate.")
   public static final String MUTABILITY_SHORT_DESCRIPTION = "muta0";

   @RBEntry("Solo alcune modifiche possono essere apportate ai valori degli attributi.")
   public static final String MUTABILITY_LONG_DESCRIPTION = "muta1";

   @RBEntry("Vincolo di mutabilità")
   public static final String MUTABILITY_DISPLAY_NAME = "muta2";

   @RBEntry("Valore in [ {0} ]")
   public static final String SDSSHORTDESCRIPTION = "sdsmsg0";

   @RBEntry("I valori degli attributi devono essere elementi di [ {0} ].")
   public static final String SDSLONGDESCRIPTION = "sdsmsg1";

   @RBEntry("Serie di valori di testo validi")
   public static final String SDSDISPLYNAME = "sdsmsg2";

   @RBEntry("Valore unico")
   public static final String SVSHORTDESCRIPTION = "svmsg0";

   @RBEntry("Non è consentito più di un valore d'attributo.")
   public static final String SVLONGDESCRIPTION = "svmsg1";

   @RBEntry("Valore unico")
   public static final String SVDISPLYNAME = "svmsg2";

   @RBEntry("valore <= {0}")
   public static final String UCRSHORTDESCRIPTION_L = "ucrmsg00";

   @RBEntry("I valori degli attributi devono essere uguali o minori di {0}.")
   public static final String UCRLONGDESCRIPTION_L = "ucrmsg01";

   @RBEntry("valore >= {0}")
   public static final String UCRSHORTDESCRIPTION_G = "ucrmsg02";

   @RBEntry("I valori degli attributi devono essere uguali o maggiori di {0}.")
   public static final String UCRLONGDESCRIPTION_G = "ucrmsg03";

   @RBEntry("{0} <= valore <= {1}")
   public static final String UCRSHORTDESCRIPTION = "ucrmsg04";

   @RBEntry("I valori degli attributi devono essere compresi tra {0} e {1} compresi.")
   public static final String UCRLONGDESCRIPTION = "ucrmsg05";

   @RBEntry("Intervallo di numeri reali, con unità")
   public static final String UCRDISPLYNAME = "ucrmsg06";

   @RBEntry("Valore obbligatorio")
   public static final String VRSHORTDESCRIPTION = "vrmsg0";

   @RBEntry("Deve esistere almeno un valore di attributo.")
   public static final String VRLONGDESCRIPTION = "vrmsg1";

   @RBEntry("Valore obbligatorio")
   public static final String VRDISPLYNAME = "vrmsg2";
}
