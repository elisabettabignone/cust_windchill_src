/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.cache;

import wt.util.resource.*;

@RBUUID("wt.cache.cacheResource")
public final class cacheResource extends WTListResourceBundle {
   @RBEntry("Access denied")
   public static final String ACCESS_DENIED = "0";

   @RBEntry("Unable to register slave cache")
   public static final String SLAVE_CACHE = "1";

   @RBEntry("Unable to get remote cache entry")
   public static final String REMOTE_CACHE_GET = "2";

   @RBEntry("Unable to put remote cache entry")
   public static final String REMOTE_CACHE_PUT = "3";

   @RBEntry("Unable to update remote cache entry")
   public static final String REMOTE_CACHE_UPDATE = "4";

   @RBEntry("Unable to remove remote cache entry")
   public static final String REMOTE_CACHE_REMOVE = "5";

   @RBEntry("Unable to locate cache server")
   public static final String CACHE_SERVER = "6";
}
