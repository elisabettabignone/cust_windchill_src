/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.cache;

import wt.util.resource.*;

@RBUUID("wt.cache.icacheResource")
public final class icacheResource_it extends WTListResourceBundle {
   @RBEntry("L'età media delle voci in cache in {0} è inferiore a {1} secondi")
   public static final String AVERAGE_AGE_NOTIFY = "0";

   @RBEntry("L'età media di overflow delle voci in cache in {0} è inferiore a {1} secondi")
   public static final String AVERAGE_OVERFLOW_AGE_NOTIFY = "1";

   @RBEntry("Mostra l'età media delle voci in cache ed è attivato quando l'età media delle voci è inferiore alla soglia configurata")
   public static final String DESC_AVERAGE_AGE_NOTIFY = "2";

   @RBEntry("L'età media delle voci è {0} secondi")
   public static final String AVERAGE_AGE = "4";

   @RBEntry("L'età media di overflow delle voci è {0} secondi")
   public static final String AVERAGE_OVERFLOW_AGE = "5";

   @RBEntry("Possibile overflow della cache {0} - Aumentare le dimensioni della cache.")
   public static final String THRASHING_NOTIFICATION = "6";

   @RBEntry("Possibile sovradimensionamento della cache {0} - Ridurre le dimensioni della cache.")
   public static final String OVERSIZED_NOTIFICATION = "7";

   @RBEntry("Overflow della cache dopo {0} secondi")
   public static final String THRASHING_DATA = "8";

   @RBEntry("Oltre il {0}% della cache è in memoria da più di {1} secondi")
   public static final String  OVERSIZED_DATA = "9";

   @RBEntry("Notifica creata quando l'overflow della cache supera le soglie specificate")
   public static final String  DESC_OVERFLOW_ENTRY_YOUNGER_THAN_THRESHOLD_NOTIF = "10";

   @RBEntry("Notifica creata quando la cache è sovradimensionata in base alle soglie specificate")
   public static final String  DESC_PERCENT_CACHE_OLDER_THAN_THRESHOLD_NOTIF = "11";
}
