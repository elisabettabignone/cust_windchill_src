/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.cache;

import wt.util.resource.*;

@RBUUID("wt.cache.MasterICacheManagerResource")
public final class MasterICacheManagerResource extends WTListResourceBundle {
   /**
    * #####################################################
    * General strings used by MasterICacheManager        #
    * #####################################################
    **/
   @RBEntry("Notification relating to cache performance")
   @RBComment("General description of JMX notification emitted by MasterICacheManager")
   public static final String NOTIFICATION_DESC = "1";

   /**
    * ####################################
    * CACHE_OVERSIZED_NOTIFICATION info #
    * ####################################
    **/
   @RBEntry("Notifies that a particular cache may be oversized based on usage statistics")
   @RBComment("Description of a cacheOversizedNotification JMX notification")
   public static final String CACHE_OVERSIZED_DESC = "10";

   @RBEntry("Cache {0} may be oversized - consider resizing")
   @RBComment("Message relayed by a cacheOversizedNotification")
   public static final String CACHE_OVERSIZED_MSG = "11";

   @RBEntry("Over {0} percent of cache entries are older than {1} seconds")
   public static final String CACHE_OVERSIZED_DATA = "12";
}
