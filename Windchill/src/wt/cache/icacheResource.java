/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.cache;

import wt.util.resource.*;

@RBUUID("wt.cache.icacheResource")
public final class icacheResource extends WTListResourceBundle {
   @RBEntry("The average age of cache entries in {0} is less that {1} seconds")
   public static final String AVERAGE_AGE_NOTIFY = "0";

   @RBEntry("The average overflow age of cache entries in {0} is less that {1} seconds")
   public static final String AVERAGE_OVERFLOW_AGE_NOTIFY = "1";

   @RBEntry("Shows the average age of cache entries, triggered when average age of entries is less than the configured threshold")
   public static final String DESC_AVERAGE_AGE_NOTIFY = "2";

   @RBEntry("The average entry age is {0} seconds")
   public static final String AVERAGE_AGE = "4";

   @RBEntry("The average overflow entry age is {0} seconds")
   public static final String AVERAGE_OVERFLOW_AGE = "5";

   @RBEntry("Cache {0} may by thrashing - consider increasing cache size.")
   public static final String THRASHING_NOTIFICATION = "6";

   @RBEntry("Cache {0} may be oversized - consider reducing cache size.")
   public static final String OVERSIZED_NOTIFICATION = "7";

   @RBEntry("Overflowed entry from cache after {0} seconds")
   public static final String THRASHING_DATA = "8";

   @RBEntry("Over {0} percent of cache entries are older than {1} seconds")
   public static final String  OVERSIZED_DATA = "9";

   @RBEntry("Notification produced when cache overflow exceeds specified thresholds")
   public static final String  DESC_OVERFLOW_ENTRY_YOUNGER_THAN_THRESHOLD_NOTIF = "10";

   @RBEntry("Notification produced when cache is found to be oversized according to specified thresholds")
   public static final String  DESC_PERCENT_CACHE_OLDER_THAN_THRESHOLD_NOTIF = "11";
}
