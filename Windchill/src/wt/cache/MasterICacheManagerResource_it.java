/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.cache;

import wt.util.resource.*;

@RBUUID("wt.cache.MasterICacheManagerResource")
public final class MasterICacheManagerResource_it extends WTListResourceBundle {
   /**
    * #####################################################
    * General strings used by MasterICacheManager        #
    * #####################################################
    **/
   @RBEntry("Notifica relativa alle prestazioni della cache")
   @RBComment("General description of JMX notification emitted by MasterICacheManager")
   public static final String NOTIFICATION_DESC = "1";

   /**
    * ####################################
    * CACHE_OVERSIZED_NOTIFICATION info #
    * ####################################
    **/
   @RBEntry("Notifica che una specifica cache può essere sovradimensionata in base alle statistiche di utilizzo")
   @RBComment("Description of a cacheOversizedNotification JMX notification")
   public static final String CACHE_OVERSIZED_DESC = "10";

   @RBEntry("Possibile sovradimensionamento della cache {0} - Ridimensionare la cache.")
   @RBComment("Message relayed by a cacheOversizedNotification")
   public static final String CACHE_OVERSIZED_MSG = "11";

   @RBEntry("Oltre il {0}% della cache è in memoria da più di {1} secondi")
   public static final String CACHE_OVERSIZED_DATA = "12";
}
