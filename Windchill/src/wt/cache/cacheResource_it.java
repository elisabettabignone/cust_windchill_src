/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.cache;

import wt.util.resource.*;

@RBUUID("wt.cache.cacheResource")
public final class cacheResource_it extends WTListResourceBundle {
   @RBEntry("Accesso negato")
   public static final String ACCESS_DENIED = "0";

   @RBEntry("Impossibile registrare la cache dipendente")
   public static final String SLAVE_CACHE = "1";

   @RBEntry("Impossibile prelevare l'elemento della cache remota")
   public static final String REMOTE_CACHE_GET = "2";

   @RBEntry("Impossibile inserire l'elemento della cache remota")
   public static final String REMOTE_CACHE_PUT = "3";

   @RBEntry("Impossibile aggiornare l'elemento della cache remota")
   public static final String REMOTE_CACHE_UPDATE = "4";

   @RBEntry("Impossibile rimuovere l'elemento della cache remota")
   public static final String REMOTE_CACHE_REMOVE = "5";

   @RBEntry("Impossibile individuare il server della cache")
   public static final String CACHE_SERVER = "6";
}
