/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.templateutil.components;

import wt.util.resource.*;

@RBUUID("wt.templateutil.components.componentsResource")
public final class componentsResource extends WTListResourceBundle {
   @RBEntry("Go")
   public static final String URL_AWARE_TEXT_FIELD_BUTTON_VALUE = "0";

   @RBEntry("<BR><H3>\nTask : Retrieving an HTMLComponent from Factory\n</H3><BR><B>\nFactory : {0}\n<BR>\nString Selector : {1}\n<BR>\nClass Selector : {2}\n<BR>\nType of Failure : Corresponding entry in property file does not exist\n\n<B><BR><BR>\n")
   public static final String SNFE_HTMLCOMPONENT = "1";

   @RBEntry("<BR><H3>\nTask : Retrieving an HTMLComponent from Factory\n</H3><BR><B>\nFactory : {0}\n<BR>\nString Selector : {1}\n<BR>\nClass Selector : {2}\n<BR>\nType of Failure : No file matching entry in properties file exists. See stackTrace for more information.\n\n<B><BR><BR>\n")
   public static final String FNE_HTMLCOMPONENT = "2";
}
