/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.templateutil.components;

import wt.util.resource.*;

@RBUUID("wt.templateutil.components.componentsResource")
public final class componentsResource_it extends WTListResourceBundle {
   @RBEntry("Vai")
   public static final String URL_AWARE_TEXT_FIELD_BUTTON_VALUE = "0";

   @RBEntry("<BR><H3>\nTask: Recupero di HTMLComponent da Factory\n</H3><BR><B>\nFactory: {0}\n<BR>\nSelettore stringa: {1}\n<BR>\nSelettore classe: {2}\n<BR>\nTipo di errore: voce corrispondente inesistente nel file delle proprietà\n\n<B><BR><BR>\n")
   public static final String SNFE_HTMLCOMPONENT = "1";

   @RBEntry("<BR><H3>\nTask: Recupero di HTMLComponent da Factory\n</H3><BR><B>\nFactory: {0}\n<BR>\nSelettore stringa: {1}\n<BR>\nSelettore classe: {2}\n<BR>\nTipo di errore: nessun file corrispondente alla voce del file delle proprietà. Vedere stackTrace per ulteriori informazioni.\n\n<B><BR><BR>\n")
   public static final String FNE_HTMLCOMPONENT = "2";
}
