/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.templateutil.processor;

import wt.util.resource.*;

@RBUUID("wt.templateutil.processor.processorResource")
public final class processorResource extends WTListResourceBundle {
   @RBEntry("Specified Size is not in valid range of 1 - 7")
   public static final String SIZE_INVALID = "0";

   @RBEntry("The desired property is not available")
   public static final String PROPERTY_NOT_AVAILABLE = "1";

   @RBEntry("<BR><H3>\nTask : Retrieving an HTMLTemplate from Factory\n</H3><BR><B>\nFactory : {0}\n<BR>\nString Selector : {1}\n<BR>\nClass Selector : {2}\n<BR>\nType of Failure : Corresponding entry in property file does not exist\n\n<B><BR><BR>\n")
   public static final String SNFE_HTMLTEMPLATE = "2";

   @RBEntry("<BR><H3>\nTask : Retrieving an HTMLTemplate from Factory\n</H3><BR><B>\nFactory : {0}\n<BR>\nString Selector : {1}\n<BR>\nClass Selector : {2}\n<BR>\nType of Failure : No file matching entry in properties file exists. See stackTrace for more information.\n\n<B><BR><BR>\n")
   public static final String FNE_HTMLTEMPLATE = "3";

   @RBEntry("<BR><H3>\nTask : Retrieving an FormTaskDelegate from Factory\n</H3><BR><B>\nFactory : {0}\n<BR>\nString Selector : {1}\n<BR>\nClass Selector : {2}\n<BR>\nType of Failure : Corresponding entry in property file does not exist\n\n<B><BR><BR>\n")
   public static final String SNFE_FORMTASKDELEGATE = "4";

   @RBEntry("<BR><H3>\nTask : Retrieving an FormTaskDelegate from Factory\n</H3><BR><B>\nFactory : {0}\n<BR>\nString Selector : {1}\n<BR>\nClass Selector : {2}\n<BR>\nType of Failure : No file matching entry in properties file exists. See stackTrace for more information.\n\n<B><BR><BR>\n")
   public static final String FNE_FORMTASKDELEGATE = "5";

   @RBEntry("Template:")
   @RBComment("Used to indicate that the object is a template.")
   public static final String TEMPLATE = "6";

   @RBEntry("Yes - Enabled")
   @RBComment("\"Yes\" indicates that the object is a template; \"Enabled\" indictates that the object is available for use.")
   public static final String TEMPLATE_AVAILABLE = "7";

   @RBEntry("Yes - Disabled")
   @RBComment("\"Yes\" - indicates that the object is a template; \"Disabled\" indictates that the object is not available for use.")
   public static final String TEMPLATE_UNAVAILABLE = "8";
}
