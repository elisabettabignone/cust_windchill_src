/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.templateutil.processor;

import wt.util.resource.*;

@RBUUID("wt.templateutil.processor.processorResource")
public final class processorResource_it extends WTListResourceBundle {
   @RBEntry("La dimensione specificata non rientra nell'intervallo valido 1 - 7")
   public static final String SIZE_INVALID = "0";

   @RBEntry("La proprietà richiesta non è disponibile")
   public static final String PROPERTY_NOT_AVAILABLE = "1";

   @RBEntry("<BR><H3>\nTask: Recupero di HTMLTemplate da Factory\n</H3><BR><B>\nFactory: {0}\n<BR>\nSelettore stringa: {1}\n<BR>\nSelettore classe: {2}\n<BR>\nTipo di errore: voce corrispondente inesistente nel file delle proprietà\n\n<B><BR><BR>\n")
   public static final String SNFE_HTMLTEMPLATE = "2";

   @RBEntry("<BR><H3>\nTask: Recupero di HTMLTemplate da Factory\n</H3><BR><B>\nFactory: {0}\n<BR>\nSelettore stringa: {1}\n<BR>\nSelettore classe: {2}\n<BR>\nTipo di errore: nessun file corrispondente alla voce del file delle proprietà. Vedere stackTrace per ulteriori informazioni.\n\n<B><BR><BR>\n")
   public static final String FNE_HTMLTEMPLATE = "3";

   @RBEntry("<BR><H3>\nTask: Recupero di FormTaskDelegate da Factory\n</H3><BR><B>\nFactory: {0}\n<BR>\nSelettore stringa: {1}\n<BR>\nSelettore classe: {2}\n<BR>\nTipo di errore: voce corrispondente inesistente nel file delle proprietà\n\n<B><BR><BR>\n")
   public static final String SNFE_FORMTASKDELEGATE = "4";

   @RBEntry("<BR><H3>\nTask: Recupero di FormTaskDelegate da Factory\n</H3><BR><B>\nFactory: {0}\n<BR>\nSelettore stringa: {1}\n<BR>\nSelettore classe: {2}\n<BR>\nTipo di errore: nessun file corrispondente alla voce del file delle proprietà. Vedere stackTrace per ulteriori informazioni.\n\n<B><BR><BR>\n")
   public static final String FNE_FORMTASKDELEGATE = "5";

   @RBEntry("Modello:")
   @RBComment("Used to indicate that the object is a template.")
   public static final String TEMPLATE = "6";

   @RBEntry("Sì - Attivato")
   @RBComment("\"Yes\" indicates that the object is a template; \"Enabled\" indictates that the object is available for use.")
   public static final String TEMPLATE_AVAILABLE = "7";

   @RBEntry("Sì - Disattivato")
   @RBComment("\"Yes\" - indicates that the object is a template; \"Disabled\" indictates that the object is not available for use.")
   public static final String TEMPLATE_UNAVAILABLE = "8";
}
