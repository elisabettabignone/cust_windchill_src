/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.templateutil.processor;

import wt.util.resource.*;

@RBUUID("wt.templateutil.processor.shareStatusResource")
public final class shareStatusResource extends WTListResourceBundle {
   /**
    * 
    * Set Life Cycle State Template Entries
    * 
    **/
   @RBEntry("Share Status")
   @RBComment("Label for share status")
   public static final String SHARE_STATUS_LABEL = "1";

   @RBEntry("** Error **")
   public static final String ERROR_ALERT = "2";

   @RBEntry("Share Status for {0}")
   @RBComment("used ofr dispalying share status page detail header i.e: Share Status for 0000000041 - spr-test A")
   @RBArgComment0("display identifier for shared object")
   public static final String SHARE_STATUS_DETAIL_PAGE_HEADER = "3";

   @RBEntry("Shared")
   @RBComment("copied from com.ptc.netmarkets.model.modelResource.SHARE_STATUS_SHARED to remove dep.")
   public static final String SHARE_STATUS_SHARED = "4";

   @RBEntry("Unlisted")
   @RBComment("copied from com.ptc.netmarkets.model.modelResource.SHARE_STATUS_SHARED to remove dep")
   public static final String UNLISTED = "5";

   @RBEntry("Shared by folder {0}")
   @RBComment("copied from com.ptc.netmarkets.model.modelResource.SHARE_STATUS_SHARED to remove dep")
   public static final String SHARE_STATUS_SHARED_FOLDER = "6";
}
