/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.templateutil.table;

import wt.util.resource.*;

@RBUUID("wt.templateutil.table.tableResource")
public final class tableResource_it extends WTListResourceBundle {
   @RBEntry("**** L'interrogazione non ha prodotto alcun risultato ****")
   public static final String NO_ROWS = "0";

   @RBEntry("<H3><BR><BR>\n\nServizio tabella attuale = {0}\n</H3><BR><BR><B>\n Azione attuale  = {1}\n<BR><BR>\n Paramentro non valido = {2}\n</B><BR><BR>\n")
   public static final String INVALID_PARAMETER = "1";

   @RBEntry("<H3><BR><BR>\n\nServizio tabella attuale = {0}\n</H3><BR><BR><B>\n Azione attuale  = {1}\n<BR><BR>\n Paramentro mancante = {2}\n </B><BR><BR>\n")
   public static final String MISSING_REQUIRED_PARAMETER = "2";

   @RBEntry("<H3><BR><BR>\n\nServizio tabella attuale = {0}\n</H3><BR><BR><B>\n Azione attuale  = {1}\n<BR><BR>\n Specificare un nome o una posizione, ma non entrambi.\n</B><BR><BR>\n")
   public static final String EITHER_NAME_OR_POSITION = "3";

   @RBEntry("<H3><BR><BR>\n\nServizio tabella attuale = {0}\n</H3><BR><BR><B>\n Azione attuale  = {1}\n<BR><BR>\n Specificare almeno un nome o una posizione.\n</B><BR><BR>\n")
   public static final String AT_LEAST_NAME_OR_POSITION = "4";

   @RBEntry("**** L'interrogazione non ha prodotto alcun risultato ****")
   public static final String INVALID_ACTION = "5";

   @RBEntry("Nessun oggetto da visualizzare")
   public static final String NO_RESULTS_IN_TABLE = "6";

   @RBEntry("Azioni")
   public static final String ACTIONS = "7";
}
