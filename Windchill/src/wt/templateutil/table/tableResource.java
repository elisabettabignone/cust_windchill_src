/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.templateutil.table;

import wt.util.resource.*;

@RBUUID("wt.templateutil.table.tableResource")
public final class tableResource extends WTListResourceBundle {
   @RBEntry("**** No results returned from query ****")
   public static final String NO_ROWS = "0";

   @RBEntry("<H3><BR><BR>\n\nCurrent Table Service = {0}\n</H3><BR><BR><B>\n Current Action  = {1}\n<BR><BR>\n Invalid Parameter = {2}\n</B><BR><BR>\n")
   public static final String INVALID_PARAMETER = "1";

   @RBEntry("<H3><BR><BR>\n\nCurrent Table Service = {0}\n</H3><BR><BR><B>\n Current Action  = {1}\n<BR><BR>\n Missing Parameter = {2}\n</B><BR><BR>\n")
   public static final String MISSING_REQUIRED_PARAMETER = "2";

   @RBEntry("<H3><BR><BR>\n\nCurrent Table Service = {0}\n</H3><BR><BR><B>\n Current Action  = {1}\n<BR><BR>\n You must specify either a name or a position, but not both\n</B><BR><BR>\n")
   public static final String EITHER_NAME_OR_POSITION = "3";

   @RBEntry("<H3><BR><BR>\n\nCurrent Table Service = {0}\n</H3><BR><BR><B>\n Current Action  = {1}\n<BR><BR>\n You must specify at least a name or a position.\n</B><BR><BR>\n")
   public static final String AT_LEAST_NAME_OR_POSITION = "4";

   @RBEntry("**** No results returned from query ****")
   public static final String INVALID_ACTION = "5";

   @RBEntry("No Objects to Display")
   public static final String NO_RESULTS_IN_TABLE = "6";

   @RBEntry("Actions")
   public static final String ACTIONS = "7";
}
