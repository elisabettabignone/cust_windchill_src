/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.templateutil.navigationbar;

import wt.util.resource.*;

@RBUUID("wt.templateutil.navigationbar.PLMLinkNavResource")
public final class PLMLinkNavResource extends WTListResourceBundle {
   @RBEntry("Home")
   @RBComment("top level navigation link")
   public static final String HOME_TAB = "0";

   @RBEntry("Current Work")
   @RBComment("second level navigation link")
   public static final String CURRENT_WORK_LINK = "1";

   @RBEntry("Notebook")
   @RBComment("second level navigation link")
   public static final String NOTEBOOK_LINK = "2";

   @RBEntry("Subscriptions")
   @RBComment("second level navigation link")
   public static final String SUBSCRIPTIONS_LINK = "3";

   @RBEntry("Task List")
   @RBComment("second level navigation link")
   public static final String WORKLIST_LINK = "4";

   @RBEntry("Products")
   @RBComment("top level navigation link")
   public static final String PRODUCTS_TAB = "5";

   @RBEntry("Change Monitor")
   @RBComment("top level navigation link")
   public static final String CHANGE_MONITOR_TAB = "6";

   @RBEntry("Collaboration Center")
   @RBComment("top level navigation link")
   public static final String COLLABORATION_CENTER_TAB = "7";

   @RBEntry("Libraries")
   @RBComment("top level navigation link")
   public static final String LIBRARIES_TAB = "8";

   @RBEntry("Administration")
   @RBComment("top level navigation link")
   public static final String ADMINISTRATION_TAB = "9";

   @RBEntry("Overview")
   @RBComment("top level navigation link")
   public static final String OVERVIEW_LINK = "10";

   @RBEntry("Workspaces")
   @RBComment("Entry for workspace")
   public static final String MYWORKSPACE = "11";

   @RBEntry("Checked Out Items")
   @RBComment("second level navigation link on Home page (replaces #1 above)")
   public static final String CHECKED_OUT_ITEMS_LINK = "12";

   @RBEntry("Repositories")
   @RBComment("top level navigation link")
   public static final String REPOSITORIES_TAB = "13";
}
