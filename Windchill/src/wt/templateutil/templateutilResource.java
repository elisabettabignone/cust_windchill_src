/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.templateutil;

import wt.util.resource.*;

@RBUUID("wt.templateutil.templateutilResource")
public final class templateutilResource extends WTListResourceBundle {
   @RBEntry("Specified Size is not in valid range of 1 - 7")
   public static final String SIZE_INVALID = "0";

   @RBEntry("The desired property is not available")
   public static final String PROPERTY_NOT_AVAILABLE = "1";

   @RBEntry("Search For Associations")
   public static final String ASSOCIATION_SEARCH = "2";

   @RBEntry("Help For Parts")
   public static final String GET_PART_HELP = "3";

   @RBEntry("Redirecting Page...")
   public static final String REDIRECT_TITLE = "4";
}
