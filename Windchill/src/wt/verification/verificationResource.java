/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.verification;

import wt.util.resource.*;

@RBUUID("wt.verification.verificationResource")
public final class verificationResource extends WTListResourceBundle {
   @RBEntry("Illegal Operation.")
   public static final String VERIFICATION_FAILED = "0";

   @RBEntry("\"{0}\" is not a \"{1}\", and cannot be verified by \"{2}\".  A Verifier delegate has been invoked for an inappropriate object type.  The wt.verification.Verifier properties (in service.properties.xconf) may be misconfigured.")
   @RBComment("A Verifier delegate has been invoked for an inappropriate object type.  The wt.verification.Verifier properties (in service.properties.xconf) may be misconfigured.")
   @RBArgComment0("The object's actual class name")
   @RBArgComment1("The expected class name")
   @RBArgComment2("The delegate class name")
   public static final String INVALID_VERIFICATION_TYPE = "1";
}
