/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.verification;

import wt.util.resource.*;

@RBUUID("wt.verification.verificationResource")
public final class verificationResource_it extends WTListResourceBundle {
   @RBEntry("Operazione non valida")
   public static final String VERIFICATION_FAILED = "0";

   @RBEntry("\"{0}\" non è un oggetto di tipo \"{1}\" e non può essere verificato da \"{2}\".  È stato richiamato un verificatore delegato per un tipo di oggetto non appropriato. È possibile che le proprietà wt.verification.Verifier siano configurate erroneamente in service.properties.xconf.")
   @RBComment("A Verifier delegate has been invoked for an inappropriate object type.  The wt.verification.Verifier properties (in service.properties.xconf) may be misconfigured.")
   @RBArgComment0("The object's actual class name")
   @RBArgComment1("The expected class name")
   @RBArgComment2("The delegate class name")
   public static final String INVALID_VERIFICATION_TYPE = "1";
}
