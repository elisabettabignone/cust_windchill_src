/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.notify;

import wt.util.resource.*;

@RBUUID("wt.notify.notifyResource")
public final class notifyResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile creare un elenco delle regole di notifica: parametri non validi.")
   @RBComment("Message stating that the parameters given were insufficient to create a notification policy.")
   public static final String BAD_LIST_SELECTOR = "1";

   @RBEntry("Impossibile creare una regola di notifica: parametri non validi.")
   @RBComment("Message stating that a notification policy rule could not be created due to bad parameters.")
   public static final String BAD_RULE_SELECTOR = "2";

   @RBEntry("Impossibile inserire l'elemento nella coda: utente amministratore mancante.")
   @RBComment("Message stating that there is no administrator user, therefore it cannot queue the entry.")
   public static final String MISSING_ADMINISTRATOR = "3";

   @RBEntry("Impossibile trovare la coda notifiche.")
   public static final String MISSING_QUEUE = "4";

   @RBEntry("Impossibile leggere la regola di notifica, probabilmente per mancanza di autorizzazione.")
   public static final String MISSING_POLICY = "5";

   @RBEntry("Impossibile trovare utente/gruppo/ruolo: \"{0}\".")
   @RBComment("Message stating that a specified principal could not be found.")
   @RBArgComment0("principal that was not found")
   public static final String MISSING_PRINCIPAL = "6";

   @RBEntry("L'utente \"{0}\" non ha un indirizzo di e-mail.")
   @RBComment("Message stating that a specified user does not have an e-mail address.")
   @RBArgComment0("User who does not have an e-mail address")
   public static final String MISSING_EMAIL = "7";

   @RBEntry("Messaggio di notifica")
   public static final String EMAIL_SUBJECT = "8";

   @RBEntry("Numero di oggetti da inserire nel messaggio errato.")
   public static final String INVALID_INSERTS = "9";

   @RBEntry("Impossibile trovare il destinatario del messaggio.")
   @RBComment("Message stating that the intended recipient of the message was not found.")
   public static final String PRINCIPAL_NOT_FOUND = "10";

   @RBEntry("Tipo di settore aziendale: ")
   public static final String BUSINESS_TYPE = "11";

   @RBEntry(", ID oggetto: ")
   public static final String OBJECT_IDENTITY = "12";

   @RBEntry("Classe dell'oggetto: ")
   public static final String OBJECT_CLASS = "13";

   @RBEntry("Evento {0} per l'oggetto {1}")
   @RBComment("Subject for notification message specifying an action occurring on a specified object")
   @RBArgComment0("String: the event type")
   @RBArgComment1("String: the object type")
   public static final String POLICY_EMAIL_SUBJECT = "15";

   @RBEntry("{0} per {1} {2}")
   @RBComment("Subject for ad hoc notification stating that a specified action occurred on a specified object type with the specified identity.")
   @RBArgComment0("String: the life cycle role that the notification recipient is playing")
   @RBArgComment1("String: the object type")
   @RBArgComment2("String: the object identity")
   public static final String ADHOC_EMAIL_SUBJECT = "17";

   @RBEntry("Questa è la notifica di un evento previsto dal sistema.\nVedere l'allegato per informazioni dettagliate sull'evento.\n")
   @RBComment("Message stating that a policy based event triggered a notification and points to an attachment for more details.")
   public static final String POLICY_TEXTPART = "18";

   @RBEntry("Vedere l'allegato.")
   public static final String SEE_ATTACHMENT = "19";

   @RBEntry("nessun oggetto")
   public static final String NO_SUBJECT = "20";

   @RBEntry("{0}")
   @RBComment("Subject for message")
   @RBArgComment0("Subject for notification, i.e. event")
   public static final String OBJECT_NOTIFICATION_SUBJECT = "21";

   @RBEntry("Classe di sottoscrizione non valida: {0}")
   @RBComment("Message stating that a specified subscriber class is invalid.")
   @RBArgComment0("subscriber class that is invalid")
   public static final String INVALID_SUBSCRIBER = "22";

   @RBEntry("Errore durante l'inizializzazione di \"{0}\".")
   @RBComment("Error message stating that a specified object could not be initialized")
   @RBArgComment0("Object for which there was an error initializing")
   public static final String ERROR_INITIALIZING = "23";

   @RBEntry("Sottoscrizione")
   public static final String SUBSCRIPTION_URL_LABEL = "24";

   @RBEntry("Sottoscrizione")
   public static final String SUBSCRIPTION_SECTION_TITLE = "25";

   @RBEntry("Sottoscrizioni")
   public static final String SUBSCRIPTIONS_SECTION_TITLE = "26";

   @RBEntry("Altre sottoscrizioni")
   public static final String OTHER_SUBSCRIPTIONS_TITLE = "27";

   @RBEntry("Creazione della sottoscrizione completata")
   public static final String SUBSCRIPTIONS_COMPLETE_MESSAGE = "28";

   @RBEntry("Le sottoscrizioni sono state eliminate")
   public static final String UNSUBSCRIPTIONS_COMPLETE_MESSAGE = "29";

   @RBEntry("Impossibile completare la sottoscrizione senza un indirizzo di e-mail. Specificare l'indirizzo di e-mail.")
   @RBComment("Message stating that a name needs to be supplied for the subscriber.")
   public static final String RECIPIENT_REQUIRED_ERROR = "30";

   @RBEntry("{0} non è un nome completo, ID utente, gruppo o team valido.")
   @RBComment("Message stating that the specified String does not correspond with a valid full name, user ID, group or team name.")
   @RBArgComment0("String that is not a valid full name, user ID, group or team name")
   public static final String INVALID_RECIPIENT = "31";

   @RBEntry("Selezionare una casella di controllo per effettuare o eliminare una sottoscrizione.")
   @RBComment("Message stating that to create or remove a subscription, the user must select a check box.")
   public static final String MISSING_SUBSCRIPTIONS = "32";

   @RBEntry("Sottoscrizione")
   public static final String SUBSCRIPTION_PAGE_TITLE = "33";

   @RBEntry("Selezionare uno o più eventi per creare una sottoscrizione")
   public static final String SUBSCRIBE_TABLE_PROMPT = "34";

   @RBEntry("Selezionare uno o più eventi per eliminare una sottoscrizione:")
   public static final String MODIFIABLE_SUBSCRIPTIONS_TABLE_PROMPT = "35";

   @RBEntry("Data di scadenza:")
   public static final String EXPIRATION_DATE_LABEL = "36";

   @RBEntry("Altre sottoscrizioni:")
   public static final String VIEW_ONLY_SUBSCRIPTIONS_TABLE_PROMPT = "37";

   @RBEntry("Immettere le informazioni di notifica via e-mail:")
   public static final String EMAIL_NOTIFICATION_PROMPT = "38";

   @RBEntry("Nome:")
   public static final String EMAIL_NAME_LABEL = "39";

   @RBEntry("Oggetto:")
   public static final String EMAIL_SUBJECT_LABEL = "40";

   @RBEntry("Messaggio:")
   public static final String EMAIL_MESSAGE_BODY_LABEL = "41";

   @RBEntry("Immettere un elenco di nomi separati da virgola (nome completo, ID utente, team, gruppo)")
   public static final String EMAIL_NAME_PROMPT = "42";

   @RBEntry(" OK ")
   public static final String OK_BUTTON = "43";

   @RBEntry("Sottoscrivi")
   public static final String SUBSCRIBE_COLUMN_HEADER = "44";

   @RBEntry("Annulla sottoscrizione")
   public static final String UNSUBSCRIBE_COLUMN_HEADER = "45";

   @RBEntry("Valore")
   public static final String VALUE_COLUMN_HEADER = "46";

   @RBEntry("Destinatario")
   public static final String RECIPIENT_COLUMN_HEADER = "47";

   @RBEntry("Evento")
   public static final String EVENT_COLUMN_HEADER = "48";

   @RBEntry("Elimina sottoscrizione a {0}")
   @RBComment("Message prompting the user to confirm or cancel whether they want to remove a subscription")
   @RBArgComment0("the name of the target object of the subscription removal")
   public static final String REMOVE_SUBSCRIPTION_URL_LABEL = "49";

   @RBEntry("Identificativo di destinazione:")
   @RBComment("used in Policy Notification emails")
   public static final String TARGET_LABEL = "50";

   @RBEntry("Evento:")
   public static final String EVENT_LABEL = "52";

   @RBEntry("Sottoscrittore:")
   public static final String SUBSCRIBER_LABEL = "53";

   @RBEntry("Valore:")
   public static final String VALUE_LABEL = "54";

   @RBEntry("Notifica di sottoscrizione")
   public static final String SUBSCRIPTION_NOTIFICATION_TITLE = "55";

   @RBEntry("{{0} non dispone del permesso di sottoscrizione")
   @RBComment("Message stating that the specified user is not authorized to create subscriptions.")
   @RBArgComment0("User who does not currently have permission to create subscriptions")
   public static final String USER_NOT_AUTHORIZED = "56";

   @RBEntry("Un evento {0} si è verificato per {1}")
   @RBComment("Default subject message stating that a specified type of event occurred on a specified object.")
   @RBArgComment0(" type of event")
   @RBArgComment1(" object to which the event occurred")
   public static final String DEFAULT_SUBSCRIPTION_SUBJECT = "57";

   @RBEntry("La data di scadenza deve essere successiva al {0}.")
   @RBComment("Error message stating that the expiration date that the user specified is later than allowed.")
   @RBArgComment0("date after which the expiration date may occur")
   public static final String EXPIRATION_DATE_ERROR = "58";

   @RBEntry("Notifiche")
   public static final String POLICY_NOTIFICATION_TITLE = "59";

   @RBEntry("{0} non è un nome completo univoco. Sostituirlo con l'ID utente.")
   @RBComment("More than one user was found for the full name.  Must specify the correct user by typing in UserID.")
   @RBArgComment0("User's full name that is not unique")
   public static final String UNIQUE_USER_ERROR = "60";

   @RBEntry("{0} non è un nome univoco.")
   @RBComment("More than one principal was found for the name.  The principal cannot be uniquely identified by name.")
   @RBArgComment0("Principal name that is not unique")
   public static final String UNIQUE_NAME_ERROR = "61";

   @RBEntry("Una regola di notifica è già stata definita per il dominio \"{0}\", tipo: \"{1}\", evento: \"{2}\", utenti/gruppi/ruoli: \"{3}\".")
   @RBComment("PolicyRuleAlreadyExistsException: An attempt was made to create a notification rule that already exists")
   @RBArgComment0("Path name of the domain the rule applies to")
   @RBArgComment1("External type identifier of the type in the rule")
   @RBArgComment2("Event name (values defined in wt.admin.adminEventResource) of the event in the rule")
   @RBArgComment3("List of names of the principals in the existing rule")
   public static final String RULE_ALREADY_EXISTS = "62";

   @RBEntry("Una regola di notifica non esiste per il dominio \"{0}\", tipo: \"{1}\", evento: \"{2}\".")
   @RBComment("PolicyRuleDoesNotExistException: An attempt was made to update or delete a notification rule that does not exist")
   @RBArgComment0("Path name of the domain for a rule")
   @RBArgComment1("External type identifier of the type for a rule")
   @RBArgComment2("Event name (values defined in wt.admin.adminEventResource) of the event for a rule")
   public static final String RULE_DOES_NOT_EXIST = "63";

   @RBEntry("Nome destinazione:")
   @RBComment("used in Policy Notification emails")
   public static final String TARGET_NAME = "64";
}
