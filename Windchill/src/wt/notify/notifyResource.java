/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.notify;

import wt.util.resource.*;

@RBUUID("wt.notify.notifyResource")
public final class notifyResource extends WTListResourceBundle {
   @RBEntry("Cannot create notification policy list: bad parameters.")
   @RBComment("Message stating that the parameters given were insufficient to create a notification policy.")
   public static final String BAD_LIST_SELECTOR = "1";

   @RBEntry("Cannot create notification policy rule: bad parameters.")
   @RBComment("Message stating that a notification policy rule could not be created due to bad parameters.")
   public static final String BAD_RULE_SELECTOR = "2";

   @RBEntry("Cannot queue entry: no administrator user.")
   @RBComment("Message stating that there is no administrator user, therefore it cannot queue the entry.")
   public static final String MISSING_ADMINISTRATOR = "3";

   @RBEntry("Cannot find notification queue.")
   public static final String MISSING_QUEUE = "4";

   @RBEntry("Cannot read notification policy (possibly lack of authorization).")
   public static final String MISSING_POLICY = "5";

   @RBEntry("Cannot find principal: \"{0}\".")
   @RBComment("Message stating that a specified principal could not be found.")
   @RBArgComment0("principal that was not found")
   public static final String MISSING_PRINCIPAL = "6";

   @RBEntry("User \"{0}\" does not have an e-mail address.")
   @RBComment("Message stating that a specified user does not have an e-mail address.")
   @RBArgComment0("User who does not have an e-mail address")
   public static final String MISSING_EMAIL = "7";

   @RBEntry("Notification Message")
   public static final String EMAIL_SUBJECT = "8";

   @RBEntry("Wrong number of objects to insert into message.")
   public static final String INVALID_INSERTS = "9";

   @RBEntry("Message recipient was not found.")
   @RBComment("Message stating that the intended recipient of the message was not found.")
   public static final String PRINCIPAL_NOT_FOUND = "10";

   @RBEntry("Business Type: ")
   public static final String BUSINESS_TYPE = "11";

   @RBEntry(", Object's Identity: ")
   public static final String OBJECT_IDENTITY = "12";

   @RBEntry("Object's Class: ")
   public static final String OBJECT_CLASS = "13";

   @RBEntry("{0} event on {1}")
   @RBComment("Subject for notification message specifying an action occurring on a specified object")
   @RBArgComment0("String: the event type")
   @RBArgComment1("String: the object type")
   public static final String POLICY_EMAIL_SUBJECT = "15";

   @RBEntry("{0} for {1} {2}")
   @RBComment("Subject for ad hoc notification stating that a specified action occurred on a specified object type with the specified identity.")
   @RBArgComment0("String: the life cycle role that the notification recipient is playing")
   @RBArgComment1("String: the object type")
   @RBArgComment2("String: the object identity")
   public static final String ADHOC_EMAIL_SUBJECT = "17";

   @RBEntry("This is a notification for a policy-based event.\nSee attachment for details of the event.\n")
   @RBComment("Message stating that a policy based event triggered a notification and points to an attachment for more details.")
   public static final String POLICY_TEXTPART = "18";

   @RBEntry("See attachment.")
   public static final String SEE_ATTACHMENT = "19";

   @RBEntry("no subject")
   public static final String NO_SUBJECT = "20";

   @RBEntry("{0}")
   @RBComment("Subject for message")
   @RBArgComment0("Subject for notification, i.e. event")
   public static final String OBJECT_NOTIFICATION_SUBJECT = "21";

   @RBEntry("Invalid subscriber class: {0}")
   @RBComment("Message stating that a specified subscriber class is invalid.")
   @RBArgComment0("subscriber class that is invalid")
   public static final String INVALID_SUBSCRIBER = "22";

   @RBEntry("Error initializing \"{0}\".")
   @RBComment("Error message stating that a specified object could not be initialized")
   @RBArgComment0("Object for which there was an error initializing")
   public static final String ERROR_INITIALIZING = "23";

   @RBEntry("Subscription")
   public static final String SUBSCRIPTION_URL_LABEL = "24";

   @RBEntry("Subscribe to")
   public static final String SUBSCRIPTION_SECTION_TITLE = "25";

   @RBEntry("Subscriptions to")
   public static final String SUBSCRIPTIONS_SECTION_TITLE = "26";

   @RBEntry("Other Subscriptions to")
   public static final String OTHER_SUBSCRIPTIONS_TITLE = "27";

   @RBEntry("Subscription successfully created")
   public static final String SUBSCRIPTIONS_COMPLETE_MESSAGE = "28";

   @RBEntry("Subscriptions have been successfully removed")
   public static final String UNSUBSCRIPTIONS_COMPLETE_MESSAGE = "29";

   @RBEntry("Unable to complete subscription without an e-mail address, please specify an e-mail address")
   @RBComment("Message stating that a name needs to be supplied for the subscriber.")
   public static final String RECIPIENT_REQUIRED_ERROR = "30";

   @RBEntry("{0} is not a valid full name, user ID, group, or team.")
   @RBComment("Message stating that the specified String does not correspond with a valid full name, user ID, group or team name.")
   @RBArgComment0("String that is not a valid full name, user ID, group or team name")
   public static final String INVALID_RECIPIENT = "31";

   @RBEntry("Please select a check box to create or remove a subscription.")
   @RBComment("Message stating that to create or remove a subscription, the user must select a check box.")
   public static final String MISSING_SUBSCRIPTIONS = "32";

   @RBEntry("Subscription")
   public static final String SUBSCRIPTION_PAGE_TITLE = "33";

   @RBEntry("Select one or more events to create a subscription:")
   public static final String SUBSCRIBE_TABLE_PROMPT = "34";

   @RBEntry("Select one or more events to remove a subscription:")
   public static final String MODIFIABLE_SUBSCRIPTIONS_TABLE_PROMPT = "35";

   @RBEntry("Expiration Date:")
   public static final String EXPIRATION_DATE_LABEL = "36";

   @RBEntry("Other Subscriptions:")
   public static final String VIEW_ONLY_SUBSCRIPTIONS_TABLE_PROMPT = "37";

   @RBEntry("Enter e-mail notification information:")
   public static final String EMAIL_NOTIFICATION_PROMPT = "38";

   @RBEntry("Name:")
   public static final String EMAIL_NAME_LABEL = "39";

   @RBEntry("Subject:")
   public static final String EMAIL_SUBJECT_LABEL = "40";

   @RBEntry("Message:")
   public static final String EMAIL_MESSAGE_BODY_LABEL = "41";

   @RBEntry("Enter a comma separated list of names (full name, user ID, group, team)")
   public static final String EMAIL_NAME_PROMPT = "42";

   @RBEntry(" OK ")
   public static final String OK_BUTTON = "43";

   @RBEntry("Subscribe")
   public static final String SUBSCRIBE_COLUMN_HEADER = "44";

   @RBEntry("Unsubscribe")
   public static final String UNSUBSCRIBE_COLUMN_HEADER = "45";

   @RBEntry("Value")
   public static final String VALUE_COLUMN_HEADER = "46";

   @RBEntry("Recipient")
   public static final String RECIPIENT_COLUMN_HEADER = "47";

   @RBEntry("Event")
   public static final String EVENT_COLUMN_HEADER = "48";

   @RBEntry("Remove Subscription to {0}")
   @RBComment("Message prompting the user to confirm or cancel whether they want to remove a subscription")
   @RBArgComment0("the name of the target object of the subscription removal")
   public static final String REMOVE_SUBSCRIPTION_URL_LABEL = "49";

   @RBEntry("Target Identity:")
   @RBComment("used in Policy Notification emails")
   public static final String TARGET_LABEL = "50";

   @RBEntry("Event:")
   public static final String EVENT_LABEL = "52";

   @RBEntry("Subscriber:")
   public static final String SUBSCRIBER_LABEL = "53";

   @RBEntry("Value:")
   public static final String VALUE_LABEL = "54";

   @RBEntry("Subscription Notification")
   public static final String SUBSCRIPTION_NOTIFICATION_TITLE = "55";

   @RBEntry("{0} does not currently have permission to create subscriptions.")
   @RBComment("Message stating that the specified user is not authorized to create subscriptions.")
   @RBArgComment0("User who does not currently have permission to create subscriptions")
   public static final String USER_NOT_AUTHORIZED = "56";

   @RBEntry("A {0} event occurred on {1}")
   @RBComment("Default subject message stating that a specified type of event occurred on a specified object.")
   @RBArgComment0(" type of event")
   @RBArgComment1(" object to which the event occurred")
   public static final String DEFAULT_SUBSCRIPTION_SUBJECT = "57";

   @RBEntry("The Expiration Date must be later than {0}.")
   @RBComment("Error message stating that the expiration date that the user specified is later than allowed.")
   @RBArgComment0("date after which the expiration date may occur")
   public static final String EXPIRATION_DATE_ERROR = "58";

   @RBEntry("Policy Notification")
   public static final String POLICY_NOTIFICATION_TITLE = "59";

   @RBEntry("{0} is not a unique full name.  Please enter the user ID instead.")
   @RBComment("More than one user was found for the full name.  Must specify the correct user by typing in UserID.")
   @RBArgComment0("User's full name that is not unique")
   public static final String UNIQUE_USER_ERROR = "60";

   @RBEntry("{0} is not a unique name.")
   @RBComment("More than one principal was found for the name.  The principal cannot be uniquely identified by name.")
   @RBArgComment0("Principal name that is not unique")
   public static final String UNIQUE_NAME_ERROR = "61";

   @RBEntry("A notification rule is already defined for Domain: \"{0}\", Type: \"{1}\", Event: \"{2}\", Principals: \"{3}\".")
   @RBComment("PolicyRuleAlreadyExistsException: An attempt was made to create a notification rule that already exists")
   @RBArgComment0("Path name of the domain the rule applies to")
   @RBArgComment1("External type identifier of the type in the rule")
   @RBArgComment2("Event name (values defined in wt.admin.adminEventResource) of the event in the rule")
   @RBArgComment3("List of names of the principals in the existing rule")
   public static final String RULE_ALREADY_EXISTS = "62";

   @RBEntry("A notification rule does not exist for Domain: \"{0}\", Type: \"{1}\", Event: \"{2}\".")
   @RBComment("PolicyRuleDoesNotExistException: An attempt was made to update or delete a notification rule that does not exist")
   @RBArgComment0("Path name of the domain for a rule")
   @RBArgComment1("External type identifier of the type for a rule")
   @RBArgComment2("Event name (values defined in wt.admin.adminEventResource) of the event for a rule")
   public static final String RULE_DOES_NOT_EXIST = "63";

   @RBEntry("Target Name:")
   @RBComment("used in Policy Notification emails")
   public static final String TARGET_NAME = "64";
}
