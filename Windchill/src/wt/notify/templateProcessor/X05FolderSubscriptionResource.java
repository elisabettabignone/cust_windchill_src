/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.notify.templateProcessor;

import wt.util.resource.*;

@RBUUID("wt.notify.templateProcessor.X05FolderSubscriptionResource")
public final class X05FolderSubscriptionResource extends WTListResourceBundle {
   /**
    * This resource file is required to support migration of X-05 systems with Folder subscription
    * entries on the Notification queue to X-10 (or later). This file can be removed once support is
    * no longer required for migrating X-05 systems.
    **/
   @RBEntry("{0} Name:")
   public static final String OBJECT_CREATED_LABEL = "0";

   @RBEntry("Created By:")
   public static final String OBJECT_CREATOR_LABEL = "1";

   @RBEntry("Created:")
   public static final String DATE_CREATED_LABEL = "2";

   @RBEntry("Folder Name:")
   public static final String FOLDER_NAME_LABEL = "3";

   @RBEntry("Context:")
   public static final String CONTEXT_LABEL = "4";

   @RBEntry("Subscription Creator:")
   public static final String SUBSCRIBER_LABEL = "5";

   @RBEntry("Event:")
   public static final String EVENT_TYPE_LABEL = "6";

   @RBEntry("Folder Subscription Notification")
   public static final String SUBSCRIPTION_NOTIFICATION_TITLE = "7";

   @RBEntry("Last Modified:")
   public static final String LAST_MODIFIED_LABEL = "9";

   @RBEntry("Modified By:")
   public static final String OBJECT_MODIFIED_BY_LABEL = "10";

   @RBEntry("\"{0}\" was created in folder \"{1}\" of project \"{2}\"")
   public static final String CREATE_IN_FOLDER_SUBJECT_FOR_PROJECT = "11";

   @RBEntry("\"{0}\" was created in folder \"{1}\" of product \"{2}\"")
   public static final String CREATE_IN_FOLDER_SUBJECT_FOR_PRODUCT = "12";

   @RBEntry("\"{0}\" was created in folder \"{1}\" of library \"{2}\"")
   public static final String CREATE_IN_FOLDER_SUBJECT_FOR_LIBRARY = "13";

   @RBEntry("\"{0}\" was checked into folder \"{1}\" of project \"{2}\"")
   public static final String CHECK_IN_IN_FOLDER_SUBJECT_FOR_PROJECT = "14";

   @RBEntry("\"{0}\" was checked into folder \"{1}\" of product \"{2}\"")
   public static final String CHECK_IN_IN_FOLDER_SUBJECT_FOR_PRODUCT = "15";

   @RBEntry("\"{0}\" was checked into folder \"{1}\" of library \"{2}\"")
   public static final String CHECK_IN_IN_FOLDER_SUBJECT_FOR_LIBRARY = "16";

   @RBEntry("\"{0}\" has reached state \"{1}\" in folder \"{2}\" of project \"{3}\"")
   public static final String STATE_CHANGE_IN_FOLDER_SUBJECT_FOR_PROJECT = "17";

   @RBEntry("\"{0}\" has reached state \"{1}\" in folder \"{2}\" of product \"{3}\"")
   public static final String STATE_CHANGE_IN_FOLDER_SUBJECT_FOR_PRODUCT = "18";

   @RBEntry("\"{0}\" has reached state \"{1}\" in folder \"{2}\" of library \"{3}\"")
   public static final String STATE_CHANGE_IN_FOLDER_SUBJECT_FOR_LIBRARY = "19";

   @RBEntry("{0}")
   public static final String USER_SUBJECT = "20";

   @RBEntry("Description:")
   public static final String DESCRIPTION_LABEL = "21";

   @RBEntry("Check in Comment:")
   public static final String CHECK_IN_COMMENT_LABEL = "22";

   @RBEntry("Subfolder")
   public static final String SUB_FOLDER = "23";

   @RBEntry("Project Name:")
   public static final String PROJECT_NAME_LABEL = "24";

   @RBEntry("Product Name:")
   public static final String PRODUCT_NAME_LABEL = "25";

   @RBEntry("Library Name:")
   public static final String LIBRARY_NAME_LABEL = "26";

   @RBEntry("State:")
   @RBComment("Label for the state of the object")
   public static final String OBJECT_STATE_LABEL = "27";

   @RBEntry("Multiple items were created in folder \"{0}\" of project \"{1}\"")
   public static final String MULTI_CREATE_IN_FOLDER_SUBJECT_FOR_PROJECT = "28";

   @RBEntry("Multiple items were created in folder \"{0}\" of product \"{1}\"")
   public static final String MULTI_CREATE_IN_FOLDER_SUBJECT_FOR_PRODUCT = "29";

   @RBEntry("Multiple items were created in folder \"{0}\" of library \"{1}\"")
   public static final String MULTI_CREATE_IN_FOLDER_SUBJECT_FOR_LIBRARY = "30";

   @RBEntry("Multiple items were checked into folder \"{0}\" of project \"{1}\"")
   public static final String MULTI_CHECK_IN_IN_FOLDER_SUBJECT_FOR_PROJECT = "31";

   @RBEntry("Multiple items were checked into folder \"{0}\" of product \"{1}\"")
   public static final String MULTI_CHECK_IN_IN_FOLDER_SUBJECT_FOR_PRODUCT = "32";

   @RBEntry("Multiple items were checked into folder \"{0}\" of library \"{1}\"")
   public static final String MULTI_CHECK_IN_IN_FOLDER_SUBJECT_FOR_LIBRARY = "33";

   @RBEntry("Items Created:")
   public static final String CREATED_LABEL_PLURAL = "34";

   @RBEntry("Items Checked In:")
   public static final String CHECKED_IN_LABEL_PLURAL = "35";

   @RBEntry("Multiple items changed state in folder \"{0}\" of project \"{1}\"")
   public static final String MULTI_STATE_CHANGE_IN_FOLDER_SUBJECT_FOR_PROJECT = "36";

   @RBEntry("Multiple items changed state in folder \"{0}\" of product \"{1}\"")
   public static final String MULTI_STATE_CHANGE_IN_FOLDER_SUBJECT_FOR_PRODUCT = "37";

   @RBEntry("Multiple items changed state in folder \"{0}\" of library \"{1}\"")
   public static final String MULTI_STATE_CHANGE_IN_FOLDER_SUBJECT_FOR_LIBRARY = "38";

   @RBEntry("Items that Changed State:")
   public static final String STATE_CHANGE_LABEL_PLURAL = "39";

   @RBEntry("Name")
   public static final String NAME_TITLE = "41";

   @RBEntry("Type")
   public static final String TYPE_TITLE = "42";

   @RBEntry("Items Created By:")
   public static final String OBJECTS_CREATOR_LABEL = "43";

   @RBEntry("Number:")
   public static final String NUMBER_LABEL = "44";

   @RBEntry("Number")
   public static final String NUMBER_TITLE = "45";
}
