/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.notify.templateProcessor;

import wt.util.resource.*;

@RBUUID("wt.notify.templateProcessor.templateProcessorResource")
public final class templateProcessorResource extends WTListResourceBundle {
   /**
    * The following entries (1-50) are for the basic object event notification layout
    **/
   @RBEntry("Subscription Notification")
   public static final String NOTIFICATION_TITLE = "1";

   @RBEntry("Event:")
   public static final String EVENT_LABEL = "2";

   @RBEntry("Event Initiator:")
   public static final String EVENT_CAUSED_BY = "3";

   @RBEntry("Transaction Description:")
   public static final String TRANSACTION_DESCRIPTION = "4";

   @RBEntry("Check In Comment:")
   @RBComment("Check In Comment label that is dispalyed in Notifciation email for Check In event")
   public static final String CHECK_IN_COMMENT = "5";

   @RBEntry("Subject:")
   public static final String SUBJECT_LABLE = "6";

   @RBEntry("Message:")
   public static final String MESSAGE_LABEL = "7";

   @RBEntry("Name:")
   public static final String NAME_LABEL = "8";

   @RBEntry("Type:")
   public static final String TYPE_LABEL = "9";

   @RBEntry("Identity:")
   public static final String IDENTITY_LABEL = "10";

   @RBEntry("Description:")
   public static final String DESCRIPTION_LABEL = "11";

   @RBEntry("Context Name:")
   @RBComment("Context Name label that is dispalyed in Notification email")
   public static final String CONTAINER_NAME = "12";

   @RBEntry("Context Description:")
   @RBComment("Context Description label that is dispalyed in Notification email")
   public static final String CONTAINER_DESCRIPTION = "13";

   @RBEntry("Context Owner:")
   @RBComment("Context Owner label that is dispalyed in Notification email")
   public static final String CONTAINER_OWNER = "14";

   @RBEntry("Project Name:")
   @RBComment("Project Name label that is dispalyed in Notification email")
   public static final String PROJECT_NAME = "PROJECT_NAME";

   @RBEntry("Project Description:")
   @RBComment("Project Description label that is dispalyed in Notification email")
   public static final String PROJECT_DESCRIPTION = "PROJECT_DESCRIPTION";

   @RBEntry("Project Owner:")
   @RBComment("Project Owner label that is dispalyed in Notification email")
   public static final String PROJECT_OWNER = "PROJECT_OWNER";

   @RBEntry("Product Name:")
   @RBComment("Product Name label that is dispalyed in Notification email")
   public static final String PRODUCT_NAME = "PRODUCT_NAME";

   @RBEntry("Product Description:")
   @RBComment("Product Description label that is dispalyed in Notification email")
   public static final String PRODUCT_DESCRIPTION = "PRODUCT_DESCRIPTION";

   @RBEntry("Product Owner:")
   @RBComment("ProProductject Owner label that is dispalyed in Notification email")
   public static final String PRODUCT_OWNER = "PRODUCT_OWNER";

   @RBEntry("Library Name:")
   @RBComment("Library Name label that is dispalyed in Notification email")
   public static final String LIBRARY_NAME = "LIBRARY_NAME";

   @RBEntry("Library Description:")
   @RBComment("Library Description label that is dispalyed in Notification email")
   public static final String LIBRARY_DESCRIPTION = "LIBRARY_DESCRIPTION";

   @RBEntry("Library Owner:")
   @RBComment("Library Owner label that is dispalyed in Notification email")
   public static final String LIBRARY_OWNER = "LIBRARY_OWNER";

   @RBEntry("Program Name:")
   @RBComment("Program Name label that is dispalyed in Notification email")
   public static final String PROGRAM_NAME = "PROGRAM_NAME";

   @RBEntry("Program Description:")
   @RBComment("Program Description label that is dispalyed in Notification email")
   public static final String PROGRAM_DESCRIPTION = "PROGRAM_DESCRIPTION";

   @RBEntry("Program Owner:")
   @RBComment("Program Owner label that is dispalyed in Notification email")
   public static final String PROGRAM_OWNER = "PROGRAM_OWNER";

   @RBEntry("Organization Name:")
   @RBComment("Organization Name label that is dispalyed in Notification email")
   public static final String ORGANIZATION_NAME = "ORGANIZATION_NAME";

   @RBEntry("Organization Description:")
   @RBComment("Organization Description label that is dispalyed in Notification email")
   public static final String ORGANIZATION_DESCRIPTION = "ORGANIZATION_DESCRIPTION";

   @RBEntry("Organization Owner:")
   @RBComment("Organization Owner label that is dispalyed in Notification email")
   public static final String ORGANIZATION_OWNER = "ORGANIZATION_OWNER";

   @RBEntry("Subscription Owner:")
   public static final String SUBSCRIPTION_OWNER = "15";

   @RBEntry("Host Organization:")
   public static final String HOST_ORGANIZATION = "16";

   @RBEntry("Request to unsubscribe")
   public static final String UNSUBSCRIBE_LABEL = "17";

   @RBEntry("Event Category:")
   public static final String EVENTCATEGORY_LABEL = "18";

   @RBEntry("View Subscription Information")
   @RBComment("Link to the Subscription information page")
   public static final String VIEW_SUBSCRIPTION_INFORMATION = "21";

   @RBEntry("{0}")
   @RBComment("Subject for message")
   @RBArgComment0("Subject for notification, i.e. event")
   public static final String OBJECT_NOTIFICATION_SUBJECT = "22";

   @RBEntry("Name")
   public static final String OBJECT_NAME = "23";

   @RBEntry("Identity")
   public static final String OBJECT_IDENTITY = "24";

   @RBEntry("Folder Path:")
   public static final String FOLDER_LABEL = "26";

   @RBEntry("Folder Items")
   public static final String ITEMS_TABLE_TITLE = "27";

   @RBEntry("Folder")
   @RBComment("Folder object type")
   public static final String FOLDER_TYPE = "28";

   @RBEntry("List of deleted Workspaces")
   public static final String WORKSPACE_TABLE_TITLE = "32";

   @RBEntry("Workspace Name")
   public static final String WORKSPACE_NAME = "33";

   @RBEntry("Context Name")
   public static final String WORKSPACE_CONTEXT_NAME = "34";

   @RBEntry("yyyy-MM-dd HH:mm a z")
   @RBPseudoTrans("yyyy-MM-dd HH:mm a z")
   @RBComment("Format for the dates that appear in notifications (2006-08-16 15:41:50 PM GMT ).")
   public static final String NOTIFICATION_DATE_FORMAT = "36";


   @RBEntry("Life Cycle State:")
   @RBComment("Life cycle State label that is dispalyed in Notifciation email")
   public static final String LIFECYCLE_STATE = "39";

   @RBEntry("Old Life Cycle State:")
   @RBComment("Old Life cycle State label that is dispalyed in Notifciation email for Change Life cycle State event")
   public static final String OLD_STATE_LABEL = "40";

   @RBEntry("Working Copy Folder Path:")
   @RBComment("Working Copy Folder Path label that is dispalyed in Notifciation email for Checkout event ")
   public static final String WORKING_FOLDER_LABEL = "41";

   @RBEntry("Markup/Annotation Identity:")
   @RBComment("Markup Identity label that is dispalyed in Notifciation email for Markup & Annotate event")
   public static final String MARKUP_IDENTITY_LABEL = "42";

   @RBEntry("Content Item Added:")
   @RBComment("Content Item Added label that is dispalyed in Notifciation email for Modify Content event")
   public static final String CONTENT_ADDED_LABEL = "43";

   @RBEntry("Content Item Removed:")
   @RBComment("Content Item Removed label that is dispalyed in Notifciation email for Modify Content event")
   public static final String CONTENT_REMOVED_LABEL = "44";

   @RBEntry("Used Object Identity:")
   @RBComment("Used Object Identity label that is dispalyed in Notifciation email for Modify Structure event")
   public static final String USED_OBJECT_LABEL = "45";

   @RBEntry("Principal Added/Role:")
   @RBComment("label dispalyed in Notifciation email for Modify team event")
   public static final String PRINCIPAL_ADDED_ROLE_LABEL = "46";

   @RBEntry("Principal Removed/Role:")
   @RBComment("label dispalyed in Notifciation email for Modify team event")
   public static final String PRINCIPAL_REMOVED_ROLE_LABEL = "47";

   @RBEntry("New Object:")
   @RBComment("New Object label that is dispalyed in Notifciation email for Copy event")
   public static final String NEW_OBJECT_LABEL = "48";

   @RBEntry("From Folder Path:")
   @RBComment("From Folder Path label that is dispalyed in Notifciation email for Move and Share events")
   public static final String FROM_FOLDER_LABEL = "49";

   @RBEntry("Source Object Identity:")
   @RBComment("Source Object Identity label that is dispalyed in Notifciation email for New View Version, One-off _Version, and Revise New Version events")
   public static final String SOURCE_OBJECT_LABEL = "50";

   @RBEntry("{0}")
   public static final String NOTIFICATION_TEXT = "51";

   /**
    * "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
    **/
   @RBEntry("Sunday")
   public static final String DAYOFWEEK_1 = "DAYOFWEEK_1";

   @RBEntry("Monday")
   public static final String DAYOFWEEK_2 = "DAYOFWEEK_2";

   @RBEntry("Tuesday")
   public static final String DAYOFWEEK_3 = "DAYOFWEEK_3";

   @RBEntry("Wednesday")
   public static final String DAYOFWEEK_4 = "DAYOFWEEK_4";

   @RBEntry("Thursday")
   public static final String DAYOFWEEK_5 = "DAYOFWEEK_5";

   @RBEntry("Friday")
   public static final String DAYOFWEEK_6 = "DAYOFWEEK_6";

   @RBEntry("Saturday")
   public static final String DAYOFWEEK_7 = "DAYOFWEEK_7";

   /**
    * The following entries are formatting default notification email subject
    **/
   @RBEntry("Windchill Notification - ")
   public static final String DEFAULT_INIT_SUBJECT_1 = "60";

   @RBEntry("Windchill Notification")
   public static final String DEFAULT_INIT_SUBJECT_2 = "61";

   @RBEntry("Event: '{0}'")
   @RBArgComment0(" Event name")
   public static final String EVENT_SUBJECT_LABEL = "62";

   @RBEntry("Object: '{0}'")
   @RBArgComment0(" Object name")
   public static final String OBJECT_SUBJECT_LABEL = "63";

   @RBEntry("Type: '{0}'")
   @RBArgComment0(" Object type")
   public static final String TYPE_SUBJECT_LABEL = "64";

   @RBEntry("Folder: '{0}'")
   @RBArgComment0(" Folder name")
   public static final String FOLDER_SUBJECT_LABEL = "65";

   @RBEntry("Old Identity:")
   @RBArgComment0("Old identity label that is dispalyed in Notifciation email")
   public static final String OLD_IDENTITY_LABEL = "66";

   @RBEntry("Principal")
   @RBArgComment0("Principal label that is dispalyed in Notifciation email")
   public static final String PRINCIPAL_LABEL = "67";

   @RBEntry("Permissions")
   @RBArgComment0("Permission label that is dispalyed in Notifciation email")
   public static final String PERMISSION_LABEL = "68";

   @RBEntry("Object Type")
   @RBArgComment0("Object Type label that is dispalyed in Notifciation email")
   public static final String OBJECT_TYPE_LABEL = "69";

   @RBEntry("Life Cycle State")
   @RBArgComment0("Life cycle State label that is dispalyed in Notifciation email")
   public static final String LIFECYCLE_STATE_LABEL = "70";

   @RBEntry("Permissions Granted:")
   @RBComment("Permissions Granted label that is dispalyed in Notifciation email")
   public static final String PERMISSION_GRANTED = "71";

   @RBEntry("Permissions Denied:")
   @RBComment("Permissions Denied label that is dispalyed in Notifciation email")
   public static final String PERMISSION_DENIED = "72";

   @RBEntry("TO Folder Path:")
   @RBArgComment0("To Folder Path label that is dispalyed in Notifciation email")
   public static final String TO_FOLDER_PATH_LABEL = "73";

   /**
    * The following entries (251-300) are for the policy object detail notification layout
    **/
   @RBEntry("{0} event on {1}")
   @RBComment("Subject for notification message specifying an action occurring on a specified object")
   @RBArgComment0("String: the event type")
   @RBArgComment1("String: the object type")
   public static final String POLICY_EMAIL_SUBJECT = "251";

   /**
    * The following entries (301-350) are for error strings
    **/
   @RBEntry("Unable to obtain a localized resource string because the resource key: {0} can not be found.  Resource class is: {1}")
   public static final String NO_RESOURCEKEY_FIELD = "301";

   @RBEntry("Unable to obtain a localized resource string because the resource class: {1} can not be found.  Resource key is: {0}")
   public static final String NO_RESOURCEKEY_CLASS = "302";

   @RBEntry("<BR><H3>\nTask : Retrieving a TemplateProcessor from Factory\n</H3><BR><B>\nFactory : {0}\n<BR>\nString Selector : {1}\n<BR>\nClass Selector : {2}\n<BR>\nType of Failure : Either unable to find entry in properties file or unable to instantiate/find class listed in properties file\n\n<B><BR><BR>\n")
   public static final String UTCSE_TEMPLATEPROCESSOR = "303";

   @RBEntry("<BR><H3>\nTask : Retrieving a TemplateProcessor from Factory\n</H3><BR><B>\nFactory : {0}\n<BR>\nString Selector : {1}\n<BR>\nClass Selector : {2}\n<BR>\nType of Failure : Corresponding entry in property file does not exist\n<B><BR>\n")
   public static final String SNFE_TEMPLATEPROCESSOR = "304";

   @RBEntry("Error initializing \"{0}\".")
   public static final String ERROR_INITIALIZING = "305";

   @RBEntry("URL Post Error: Invalid HTTP Request")
   public static final String URL_POST_ERROR = "306";

   @RBEntry("Template Processor Invocation Failed")
   public static final String TP_FAILED_TITLE = "307";

   @RBEntry("An attempt to display a web page via a template processor has failed.  Following is the text message from the exception that was generated:")
   public static final String TP_FAILED_MESSAGE = "308";

   @RBEntry("Invalid Class Error:  String variable for Class represents an invalid Class")
   public static final String INVALID_CLASS_ERROR = "309";

   @RBEntry("Invalid URL Error: No action parameter passed")
   public static final String INVALID_URL_ERROR = "310";

   /**
    * The following entries (251-300) are for digest notification
    **/
   @RBEntry("Unable to queue digest notification, administrator user not found.")
   @RBComment("Exception message stating that the administrator user was not be found for setting queue entry ownership ")
   public static final String ADMIN_USER_NOT_FOUND = "351";

   @RBEntry("Subscription Notification Report")
   public static final String DIGEST_NOTIFICATION_TITLE = "352";

   @RBEntry("User Name:")
   public static final String USER_NAME_LABEL = "353";

   @RBEntry("Frequency:")
   public static final String FREQUENCY_LABEL = "354";

   @RBEntry("Notification Summary")
   public static final String SUMMARY_TABLE_TITLE = "355";

   @RBEntry("(select item ID for detail)")
   public static final String SELECT_LABEL = "356";

   @RBEntry("ID")
   @RBComment("Table column label")
   public static final String ID_COLUMN_LABEL = "357";

   @RBEntry("Date")
   @RBComment("Table column label")
   public static final String DATE_COLUMN_LABEL = "358";

   @RBEntry("Name")
   @RBComment("Table column label")
   public static final String NAME_COLUMN_LABEL = "359";

   @RBEntry("Event")
   @RBComment("Table column label")
   public static final String EVENT_COLUMN_LABEL = "360";

   @RBEntry("Type")
   @RBComment("Table column label")
   public static final String TYPE_COLUMN_LABEL = "361";

   @RBEntry("Subject")
   @RBComment("Table column label")
   public static final String SUBJECT_COLUMN_LABEL = "362";

   @RBEntry("Windchill Daily Subscription Notifications")
   public static final String DIALY_NOTIFICATION_SUBJECT = "363";

   @RBEntry("Windchill Weekly Subscription Notifications")
   public static final String WEEKLY_NOTIFICATION_SUBJECT = "364";

   @RBEntry("Daily at {0} {1}")
   @RBComment("Daily at 6:00 CTD")
   public static final String DAILY_FREQUENCY = "365";

   @RBEntry("Weekly on {0} at {1} {2}")
   @RBComment("Weekly on Monday at 6:00 CTD")
   public static final String WEEKLY_FREQUENCY = "366";

   @RBEntry("Digest Notification Schedule")
   @RBComment("Preference name")
   public static final String DIGEST_NOTIFICATION_SCHEDULE = "367";

   @RBEntry("Set up a schedule for Digest Notification")
   @RBComment("Preference description for Digest Notification Schedule")
   public static final String NOTIFICATION_SCHEDULE_DESCRIPTION = "368";

   @RBEntry("The valid format is *,hh:mm. * is a number (0-7) which represents the day of the week (0=Daily, 1=Sunday, 2=Monday, ...7=Saturday). The string after the comma identifies the time of the day. The valid mm is 00, 15, 30, or 45.")
   @RBComment("Long Preference description for Digest Notification Schedule")
   public static final String NOTIFICATION_SCHEDULE_LONG_DESCRIPTION = "369";

   @RBEntry("Digest Entries Size Limitation")
   @RBComment("Preference name for Digest Notification Schedule")
   public static final String MAXIMUM_DIGEST_ENTRIES = "370";

   @RBEntry("Maximum number of digest entries in a single email ")
   @RBComment("Preference description for Digest Notification Schedule")
   public static final String MAXIMUM_DIGEST_ENTRIES_DESCRIPTION = "371";

   @RBEntry("Maximum number of digest entries in a single email")
   @RBComment("Long Preference description for Digest Notification Schedule")
   public static final String MAXIMUM_DIGEST_ENTRIES_LONG_DESCRIPTION = "372";

   @RBEntry("Project Manager notification -")
   @RBComment("To be used in conjunction with email notification subjects to denote whether the email is sent to Owner or Project Manager")
   public static final String MANAGER = "373";

   @RBEntry("Owner notification -")
   @RBComment("To be used in conjunction with email notification subjects to denote whether the email is sent to Owner or Project Manager")
   public static final String OWNER = "374";

   @RBEntry("Previous Value")
   @RBComment("To be used in modify security labels notification template processor")
   public static final String OLD_VALUE = "401";

   @RBEntry("Current Value")
   public static final String NEW_VALUE = "402";

   @RBEntry("Security Labels:")
   @RBComment("To be used in modify security labels notification template processor")
   public static final String SECURITY_LABELS = "404";

   @RBEntry("Security Labels")
   @RBComment("To be used in modify security labels notification template processor")
   public static final String SECURITY_LABELS_1 = "405";

   @RBEntry("Link Type:")
   @RBComment("To be used in Link Marked Suspect event notification")
   public static final String LINK_TYPE_LABEL = "406";

   @RBEntry("Linked Object Identity:")
   @RBComment("To be used in Link Marked Suspect event notification")
   public static final String LINKED_OBJECT_IDENTITY_LABEL = "407";

   @RBEntry("Old Process State:")
   @RBComment("Old Workflow Process State label that is dispalyed in Notifciation email for Change Process State event")
   public static final String OLD_PROCESS_STATE_LABEL = "408";

   @RBEntry("Current Process State:")
   @RBComment("New Workflow Process State label that is dispalyed in Notifciation email for Change Process State event")
   public static final String CURRENT_PROCESS_STATE_LABEL = "409";

   @RBEntry("Checkpoint Name:")
   @RBComment("Check Point label that is dispalyed in Notifciation email for Change Process State event")
   public static final String CHECK_POINT_LABEL = "410";

   @RBEntry("Process State Change Event")
   @RBComment("Process State Change Event label that is dispalyed in Notifciation email for Change Process State event")
   public static final String PROCESS_STATE_LABEL = "411";

   @RBEntry("Process Checkpoint Event")
   @RBComment("Process Check Point Event label that is dispalyed in Notifciation email for Change Process State event")
   public static final String PROCESS_CHECKPOINT_LABEL = "412";

   @RBEntry("Process Name:")
   @RBComment("Process Name is dispalyed in Notifciation email for Change Process State event")
   public static final String PROCESS_NAME = "413";

   @RBEntry("Updated Data Sources:")
   @RBComment("Updated Data Sources is dispalyed in Notifciation email for Illustration Source Data Update event")
   public static final String UPDATED_DATA_SOURCES = "414";

}
