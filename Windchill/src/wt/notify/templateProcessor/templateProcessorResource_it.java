/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.notify.templateProcessor;

import wt.util.resource.*;

@RBUUID("wt.notify.templateProcessor.templateProcessorResource")
public final class templateProcessorResource_it extends WTListResourceBundle {
   /**
    * The following entries (1-50) are for the basic object event notification layout
    **/
   @RBEntry("Notifica di sottoscrizione")
   public static final String NOTIFICATION_TITLE = "1";

   @RBEntry("Evento:")
   public static final String EVENT_LABEL = "2";

   @RBEntry("Iniziatore evento:")
   public static final String EVENT_CAUSED_BY = "3";

   @RBEntry("Descrizione transazione:")
   public static final String TRANSACTION_DESCRIPTION = "4";

   @RBEntry("Commento per il Check-In:")
   @RBComment("Check In Comment label that is dispalyed in Notifciation email for Check In event")
   public static final String CHECK_IN_COMMENT = "5";

   @RBEntry("Oggetto:")
   public static final String SUBJECT_LABLE = "6";

   @RBEntry("Messaggio:")
   public static final String MESSAGE_LABEL = "7";

   @RBEntry("Nome:")
   public static final String NAME_LABEL = "8";

   @RBEntry("Tipo:")
   public static final String TYPE_LABEL = "9";

   @RBEntry("Identificativo")
   public static final String IDENTITY_LABEL = "10";

   @RBEntry("Descrizione:")
   public static final String DESCRIPTION_LABEL = "11";

   @RBEntry("Nome contesto:")
   @RBComment("Context Name label that is dispalyed in Notification email")
   public static final String CONTAINER_NAME = "12";

   @RBEntry("Descrizione contesto:")
   @RBComment("Context Description label that is dispalyed in Notification email")
   public static final String CONTAINER_DESCRIPTION = "13";

   @RBEntry("Proprietario contesto:")
   @RBComment("Context Owner label that is dispalyed in Notification email")
   public static final String CONTAINER_OWNER = "14";

   @RBEntry("Nome progetto:")
   @RBComment("Project Name label that is dispalyed in Notification email")
   public static final String PROJECT_NAME = "PROJECT_NAME";

   @RBEntry("Descrizione progetto:")
   @RBComment("Project Description label that is dispalyed in Notification email")
   public static final String PROJECT_DESCRIPTION = "PROJECT_DESCRIPTION";

   @RBEntry("Proprietario progetto:")
   @RBComment("Project Owner label that is dispalyed in Notification email")
   public static final String PROJECT_OWNER = "PROJECT_OWNER";

   @RBEntry("Nome prodotto:")
   @RBComment("Product Name label that is dispalyed in Notification email")
   public static final String PRODUCT_NAME = "PRODUCT_NAME";

   @RBEntry("Descrizione prodotto:")
   @RBComment("Product Description label that is dispalyed in Notification email")
   public static final String PRODUCT_DESCRIPTION = "PRODUCT_DESCRIPTION";

   @RBEntry("Proprietario prodotto:")
   @RBComment("ProProductject Owner label that is dispalyed in Notification email")
   public static final String PRODUCT_OWNER = "PRODUCT_OWNER";

   @RBEntry("Nome libreria:")
   @RBComment("Library Name label that is dispalyed in Notification email")
   public static final String LIBRARY_NAME = "LIBRARY_NAME";

   @RBEntry("Descrizione libreria:")
   @RBComment("Library Description label that is dispalyed in Notification email")
   public static final String LIBRARY_DESCRIPTION = "LIBRARY_DESCRIPTION";

   @RBEntry("Proprietario libreria:")
   @RBComment("Library Owner label that is dispalyed in Notification email")
   public static final String LIBRARY_OWNER = "LIBRARY_OWNER";

   @RBEntry("Nome programma:")
   @RBComment("Program Name label that is dispalyed in Notification email")
   public static final String PROGRAM_NAME = "PROGRAM_NAME";

   @RBEntry("Descrizione programma:")
   @RBComment("Program Description label that is dispalyed in Notification email")
   public static final String PROGRAM_DESCRIPTION = "PROGRAM_DESCRIPTION";

   @RBEntry("Proprietario programma:")
   @RBComment("Program Owner label that is dispalyed in Notification email")
   public static final String PROGRAM_OWNER = "PROGRAM_OWNER";

   @RBEntry("Nome organizzazione:")
   @RBComment("Organization Name label that is dispalyed in Notification email")
   public static final String ORGANIZATION_NAME = "ORGANIZATION_NAME";

   @RBEntry("Descrizione organizzazione:")
   @RBComment("Organization Description label that is dispalyed in Notification email")
   public static final String ORGANIZATION_DESCRIPTION = "ORGANIZATION_DESCRIPTION";

   @RBEntry("Proprietario organizzazione:")
   @RBComment("Organization Owner label that is dispalyed in Notification email")
   public static final String ORGANIZATION_OWNER = "ORGANIZATION_OWNER";

   @RBEntry("Proprietario sottoscrizione:")
   public static final String SUBSCRIPTION_OWNER = "15";

   @RBEntry("Organizzazione host:")
   public static final String HOST_ORGANIZATION = "16";

   @RBEntry("Richiesta annullamento sottoscrizione")
   public static final String UNSUBSCRIBE_LABEL = "17";

   @RBEntry("Categoria evento:")
   public static final String EVENTCATEGORY_LABEL = "18";

   @RBEntry("Visualizza informazioni sulla sottoscrizione")
   @RBComment("Link to the Subscription information page")
   public static final String VIEW_SUBSCRIPTION_INFORMATION = "21";

   @RBEntry("{0}")
   @RBComment("Subject for message")
   @RBArgComment0("Subject for notification, i.e. event")
   public static final String OBJECT_NOTIFICATION_SUBJECT = "22";

   @RBEntry("Nome")
   public static final String OBJECT_NAME = "23";

   @RBEntry("Identificativo")
   public static final String OBJECT_IDENTITY = "24";

   @RBEntry("Percorso della cartella:")
   public static final String FOLDER_LABEL = "26";

   @RBEntry("Elementi cartella")
   public static final String ITEMS_TABLE_TITLE = "27";

   @RBEntry("Cartella")
   @RBComment("Folder object type")
   public static final String FOLDER_TYPE = "28";

   @RBEntry("Lista dei workspace eliminati")
   public static final String WORKSPACE_TABLE_TITLE = "32";

   @RBEntry("Nome workspace")
   public static final String WORKSPACE_NAME = "33";

   @RBEntry("Nome contesto")
   public static final String WORKSPACE_CONTEXT_NAME = "34";

   @RBEntry("dd/MM/yyyy, HH.mm z")
   @RBPseudoTrans("yyyy-MM-dd HH:mm a z")
   @RBComment("Format for the dates that appear in notifications (2006-08-16 15:41:50 PM GMT ).")
   public static final String NOTIFICATION_DATE_FORMAT = "36";


   @RBEntry("Stato del ciclo di vita:")
   @RBComment("Life cycle State label that is dispalyed in Notifciation email")
   public static final String LIFECYCLE_STATE = "39";

   @RBEntry("Stato del ciclo di vita precedente:")
   @RBComment("Old Life cycle State label that is dispalyed in Notifciation email for Change Life cycle State event")
   public static final String OLD_STATE_LABEL = "40";

   @RBEntry("Percorso cartella della copia in modifica:")
   @RBComment("Working Copy Folder Path label that is dispalyed in Notifciation email for Checkout event ")
   public static final String WORKING_FOLDER_LABEL = "41";

   @RBEntry("Identificativo markup/annotazione:")
   @RBComment("Markup Identity label that is dispalyed in Notifciation email for Markup & Annotate event")
   public static final String MARKUP_IDENTITY_LABEL = "42";

   @RBEntry("Elemento contenuto aggiunto:")
   @RBComment("Content Item Added label that is dispalyed in Notifciation email for Modify Content event")
   public static final String CONTENT_ADDED_LABEL = "43";

   @RBEntry("Elemento contenuto rimosso:")
   @RBComment("Content Item Removed label that is dispalyed in Notifciation email for Modify Content event")
   public static final String CONTENT_REMOVED_LABEL = "44";

   @RBEntry("Identità oggetto utilizzato:")
   @RBComment("Used Object Identity label that is dispalyed in Notifciation email for Modify Structure event")
   public static final String USED_OBJECT_LABEL = "45";

   @RBEntry("Utente/gruppo aggiunto/ruolo:")
   @RBComment("label dispalyed in Notifciation email for Modify team event")
   public static final String PRINCIPAL_ADDED_ROLE_LABEL = "46";

   @RBEntry("Utente/gruppo rimosso/ruolo:")
   @RBComment("label dispalyed in Notifciation email for Modify team event")
   public static final String PRINCIPAL_REMOVED_ROLE_LABEL = "47";

   @RBEntry("Nuovo oggetto:")
   @RBComment("New Object label that is dispalyed in Notifciation email for Copy event")
   public static final String NEW_OBJECT_LABEL = "48";

   @RBEntry("Percorso cartella di origine:")
   @RBComment("From Folder Path label that is dispalyed in Notifciation email for Move and Share events")
   public static final String FROM_FOLDER_LABEL = "49";

   @RBEntry("Identificativo oggetto di origine:")
   @RBComment("Source Object Identity label that is dispalyed in Notifciation email for New View Version, One-off _Version, and Revise New Version events")
   public static final String SOURCE_OBJECT_LABEL = "50";

   @RBEntry("{0}")
   public static final String NOTIFICATION_TEXT = "51";

   /**
    * "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
    **/
   @RBEntry("Domenica")
   public static final String DAYOFWEEK_1 = "DAYOFWEEK_1";

   @RBEntry("Lunedì")
   public static final String DAYOFWEEK_2 = "DAYOFWEEK_2";

   @RBEntry("Martedì")
   public static final String DAYOFWEEK_3 = "DAYOFWEEK_3";

   @RBEntry("Mercoledì")
   public static final String DAYOFWEEK_4 = "DAYOFWEEK_4";

   @RBEntry("Giovedì")
   public static final String DAYOFWEEK_5 = "DAYOFWEEK_5";

   @RBEntry("Venerdì")
   public static final String DAYOFWEEK_6 = "DAYOFWEEK_6";

   @RBEntry("Sabato")
   public static final String DAYOFWEEK_7 = "DAYOFWEEK_7";

   /**
    * The following entries are formatting default notification email subject
    **/
   @RBEntry("Notifica Windchill - ")
   public static final String DEFAULT_INIT_SUBJECT_1 = "60";

   @RBEntry("Notifica Windchill")
   public static final String DEFAULT_INIT_SUBJECT_2 = "61";

   @RBEntry("Evento: '{0}'")
   @RBArgComment0(" Event name")
   public static final String EVENT_SUBJECT_LABEL = "62";

   @RBEntry("Oggetto: '{0}'")
   @RBArgComment0(" Object name")
   public static final String OBJECT_SUBJECT_LABEL = "63";

   @RBEntry("Tipo: '{0}'")
   @RBArgComment0(" Object type")
   public static final String TYPE_SUBJECT_LABEL = "64";

   @RBEntry("Cartella: '{0}'")
   @RBArgComment0(" Folder name")
   public static final String FOLDER_SUBJECT_LABEL = "65";

   @RBEntry("Identificativo precedente:")
   @RBArgComment0("Old identity label that is dispalyed in Notifciation email")
   public static final String OLD_IDENTITY_LABEL = "66";

   @RBEntry("Utente/gruppo/ruolo")
   @RBArgComment0("Principal label that is dispalyed in Notifciation email")
   public static final String PRINCIPAL_LABEL = "67";

   @RBEntry("Permessi")
   @RBArgComment0("Permission label that is dispalyed in Notifciation email")
   public static final String PERMISSION_LABEL = "68";

   @RBEntry("Tipo di oggetto")
   @RBArgComment0("Object Type label that is dispalyed in Notifciation email")
   public static final String OBJECT_TYPE_LABEL = "69";

   @RBEntry("Stato del ciclo di vita")
   @RBArgComment0("Life cycle State label that is dispalyed in Notifciation email")
   public static final String LIFECYCLE_STATE_LABEL = "70";

   @RBEntry("Permessi concessi:")
   @RBComment("Permissions Granted label that is dispalyed in Notifciation email")
   public static final String PERMISSION_GRANTED = "71";

   @RBEntry("Permessi negati:")
   @RBComment("Permissions Denied label that is dispalyed in Notifciation email")
   public static final String PERMISSION_DENIED = "72";

   @RBEntry("Percorso cartella di destinazione:")
   @RBArgComment0("To Folder Path label that is dispalyed in Notifciation email")
   public static final String TO_FOLDER_PATH_LABEL = "73";

   /**
    * The following entries (251-300) are for the policy object detail notification layout
    **/
   @RBEntry("Evento {0} per l'oggetto {1}")
   @RBComment("Subject for notification message specifying an action occurring on a specified object")
   @RBArgComment0("String: the event type")
   @RBArgComment1("String: the object type")
   public static final String POLICY_EMAIL_SUBJECT = "251";

   /**
    * The following entries (301-350) are for error strings
    **/
   @RBEntry("Impossibile accedere alla stringa localizzata: impossibile trovare la classe {0}. Classe risorsa: {1}.")
   public static final String NO_RESOURCEKEY_FIELD = "301";

   @RBEntry("Impossibile accedere alla stringa localizzata: impossibile trovare la classe {1}. Chiave risorsa: {0}.")
   public static final String NO_RESOURCEKEY_CLASS = "302";

   @RBEntry("<BR>                     <H3>\nTask: recupero di TemplateProcessor da Factory\n</H3> <BR> <B>\nFactory: {0}\n<BR>\nSelettore stringa: {1}\n <BR>\nSelettore classe: {2}\n <BR>\nTipo di errore: impossibile trovare dati nel file delle proprietà oppure istanziare/trovare la classe indicata nel file delle proprietà\n\n <B><BR><BR>\n")
   public static final String UTCSE_TEMPLATEPROCESSOR = "303";

   @RBEntry("<BR><H3>\nTask: Recupero di TemplateProcessor da Factory\n</H3><BR><B>\nFactory: {0}\n<BR>\nSelettore stringa: {1}\n<BR>\nSelettore classe: {2}\n<BR>\nTipo di errore: la voce corrispondente nel file delle proprietà non esiste\n<B><BR>\n")
   public static final String SNFE_TEMPLATEPROCESSOR = "304";

   @RBEntry("Errore durante l'inizializzazione di \"{0}\".")
   public static final String ERROR_INITIALIZING = "305";

   @RBEntry("Errore trasmissione URL: richiesta HTTP non valida")
   public static final String URL_POST_ERROR = "306";

   @RBEntry("Chiamata del processore modelli non riuscita")
   public static final String TP_FAILED_TITLE = "307";

   @RBEntry("Il tentativo di visualizzazione della pagina Web tramite un processore modelli è fallito. Eccezione:")
   public static final String TP_FAILED_MESSAGE = "308";

   @RBEntry("Errore: la variabile rappresenta una classe non valida")
   public static final String INVALID_CLASS_ERROR = "309";

   @RBEntry("URL non valido: non è stato trasmesso alcun parametro.")
   public static final String INVALID_URL_ERROR = "310";

   /**
    * The following entries (251-300) are for digest notification
    **/
   @RBEntry("Impossibile mettere in coda la notifica riepilogativa. Impossibile trovare l'amministratore.")
   @RBComment("Exception message stating that the administrator user was not be found for setting queue entry ownership ")
   public static final String ADMIN_USER_NOT_FOUND = "351";

   @RBEntry("Rapporto notifiche di sottoscrizione")
   public static final String DIGEST_NOTIFICATION_TITLE = "352";

   @RBEntry("Nome utente:")
   public static final String USER_NAME_LABEL = "353";

   @RBEntry("Frequenza:")
   public static final String FREQUENCY_LABEL = "354";

   @RBEntry("Riepilogo notifiche")
   public static final String SUMMARY_TABLE_TITLE = "355";

   @RBEntry("(selezionare l'ID elemento per dettagli)")
   public static final String SELECT_LABEL = "356";

   @RBEntry("ID")
   @RBComment("Table column label")
   public static final String ID_COLUMN_LABEL = "357";

   @RBEntry("Data")
   @RBComment("Table column label")
   public static final String DATE_COLUMN_LABEL = "358";

   @RBEntry("Nome")
   @RBComment("Table column label")
   public static final String NAME_COLUMN_LABEL = "359";

   @RBEntry("Evento")
   @RBComment("Table column label")
   public static final String EVENT_COLUMN_LABEL = "360";

   @RBEntry("Tipo")
   @RBComment("Table column label")
   public static final String TYPE_COLUMN_LABEL = "361";

   @RBEntry("Oggetto")
   @RBComment("Table column label")
   public static final String SUBJECT_COLUMN_LABEL = "362";

   @RBEntry("Notifiche di sottoscrizione quotidiane Windchill")
   public static final String DIALY_NOTIFICATION_SUBJECT = "363";

   @RBEntry("Notifiche di sottoscrizione settimanali Windchill")
   public static final String WEEKLY_NOTIFICATION_SUBJECT = "364";

   @RBEntry("Quotidianamente alle {0} {1}")
   @RBComment("Daily at 6:00 CTD")
   public static final String DAILY_FREQUENCY = "365";

   @RBEntry("Settimanalmente di {0} alle {1} {2}")
   @RBComment("Weekly on Monday at 6:00 CTD")
   public static final String WEEKLY_FREQUENCY = "366";

   @RBEntry("Programmazione notifiche riepilogative")
   @RBComment("Preference name")
   public static final String DIGEST_NOTIFICATION_SCHEDULE = "367";

   @RBEntry("Imposta una programmazione per notifiche riepilogative")
   @RBComment("Preference description for Digest Notification Schedule")
   public static final String NOTIFICATION_SCHEDULE_DESCRIPTION = "368";

   @RBEntry("Il formato valido è *,hh.mm, dove * è un numero (0-7) che rappresenta il giorno della settimana (0=Ogni giorno, 1=Domenica, 2=Lunedì, ...7=Sabato). La stringa dopo la virgola identifica l'ora del giorno. I valori validi per mm sono 00, 15, 30 o 45.")
   @RBComment("Long Preference description for Digest Notification Schedule")
   public static final String NOTIFICATION_SCHEDULE_LONG_DESCRIPTION = "369";

   @RBEntry("Limite dimensioni voci riepilogative")
   @RBComment("Preference name for Digest Notification Schedule")
   public static final String MAXIMUM_DIGEST_ENTRIES = "370";

   @RBEntry("Numero massimo di voci riepilogative in un singolo messaggio e-mail")
   @RBComment("Preference description for Digest Notification Schedule")
   public static final String MAXIMUM_DIGEST_ENTRIES_DESCRIPTION = "371";

   @RBEntry("Numero massimo di voci riepilogative in un singolo messaggio e-mail")
   @RBComment("Long Preference description for Digest Notification Schedule")
   public static final String MAXIMUM_DIGEST_ENTRIES_LONG_DESCRIPTION = "372";

   @RBEntry("Notifica per il project manager -")
   @RBComment("To be used in conjunction with email notification subjects to denote whether the email is sent to Owner or Project Manager")
   public static final String MANAGER = "373";

   @RBEntry("Notifica per il proprietario -")
   @RBComment("To be used in conjunction with email notification subjects to denote whether the email is sent to Owner or Project Manager")
   public static final String OWNER = "374";

   @RBEntry("Valore precedente")
   @RBComment("To be used in modify security labels notification template processor")
   public static final String OLD_VALUE = "401";

   @RBEntry("Valore corrente")
   public static final String NEW_VALUE = "402";

   @RBEntry("Etichette di sicurezza:")
   @RBComment("To be used in modify security labels notification template processor")
   public static final String SECURITY_LABELS = "404";

   @RBEntry("Etichette di sicurezza")
   @RBComment("To be used in modify security labels notification template processor")
   public static final String SECURITY_LABELS_1 = "405";

   @RBEntry("Tipo di link:")
   @RBComment("To be used in Link Marked Suspect event notification")
   public static final String LINK_TYPE_LABEL = "406";

   @RBEntry("Identità oggetto collegato:")
   @RBComment("To be used in Link Marked Suspect event notification")
   public static final String LINKED_OBJECT_IDENTITY_LABEL = "407";

   @RBEntry("Stato processo precedente:")
   @RBComment("Old Workflow Process State label that is dispalyed in Notifciation email for Change Process State event")
   public static final String OLD_PROCESS_STATE_LABEL = "408";

   @RBEntry("Stato processo corrente:")
   @RBComment("New Workflow Process State label that is dispalyed in Notifciation email for Change Process State event")
   public static final String CURRENT_PROCESS_STATE_LABEL = "409";

   @RBEntry("Nome punto d'arresto:")
   @RBComment("Check Point label that is dispalyed in Notifciation email for Change Process State event")
   public static final String CHECK_POINT_LABEL = "410";

   @RBEntry("Evento di modifica dello stato del processo")
   @RBComment("Process State Change Event label that is dispalyed in Notifciation email for Change Process State event")
   public static final String PROCESS_STATE_LABEL = "411";

   @RBEntry("Evento punto d'arresto del processo")
   @RBComment("Process Check Point Event label that is dispalyed in Notifciation email for Change Process State event")
   public static final String PROCESS_CHECKPOINT_LABEL = "412";

   @RBEntry("Nome processo:")
   @RBComment("Process Name is dispalyed in Notifciation email for Change Process State event")
   public static final String PROCESS_NAME = "413";

   @RBEntry("Origini dati aggiornate:")
   @RBComment("Updated Data Sources is dispalyed in Notifciation email for Illustration Source Data Update event")
   public static final String UPDATED_DATA_SOURCES = "414";

}
