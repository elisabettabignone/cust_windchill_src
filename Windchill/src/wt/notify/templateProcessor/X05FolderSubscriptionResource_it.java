/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.notify.templateProcessor;

import wt.util.resource.*;

@RBUUID("wt.notify.templateProcessor.X05FolderSubscriptionResource")
public final class X05FolderSubscriptionResource_it extends WTListResourceBundle {
   /**
    * This resource file is required to support migration of X-05 systems with Folder subscription
    * entries on the Notification queue to X-10 (or later). This file can be removed once support is
    * no longer required for migrating X-05 systems.
    **/
   @RBEntry("Nome {0}:")
   public static final String OBJECT_CREATED_LABEL = "0";

   @RBEntry("Autore:")
   public static final String OBJECT_CREATOR_LABEL = "1";

   @RBEntry("Data creazione:")
   public static final String DATE_CREATED_LABEL = "2";

   @RBEntry("Nome cartella:")
   public static final String FOLDER_NAME_LABEL = "3";

   @RBEntry("Contesto:")
   public static final String CONTEXT_LABEL = "4";

   @RBEntry("Autore sottoscrizione:")
   public static final String SUBSCRIBER_LABEL = "5";

   @RBEntry("Evento:")
   public static final String EVENT_TYPE_LABEL = "6";

   @RBEntry("Notifica di sottoscrizione cartella")
   public static final String SUBSCRIPTION_NOTIFICATION_TITLE = "7";

   @RBEntry("Data ultima modifica:")
   public static final String LAST_MODIFIED_LABEL = "9";

   @RBEntry("Autore modifiche:")
   public static final String OBJECT_MODIFIED_BY_LABEL = "10";

   @RBEntry("Creazione di \"{0}\" nella cartella \"{1}\" del progetto \"{2}\" completata")
   public static final String CREATE_IN_FOLDER_SUBJECT_FOR_PROJECT = "11";

   @RBEntry("Creazione di \"{0}\" nella cartella \"{1}\" del prodotto \"{2}\" completata")
   public static final String CREATE_IN_FOLDER_SUBJECT_FOR_PRODUCT = "12";

   @RBEntry("Creazione di \"{0}\" nella cartella \"{1}\" della libreria \"{2}\" completata")
   public static final String CREATE_IN_FOLDER_SUBJECT_FOR_LIBRARY = "13";

   @RBEntry("Check-In di \"{0}\" nella cartella \"{1}\" del progetto \"{2}\" effettuato")
   public static final String CHECK_IN_IN_FOLDER_SUBJECT_FOR_PROJECT = "14";

   @RBEntry("Check-In di \"{0}\" nella cartella \"{1}\" del prodotto \"{2}\" effettuato")
   public static final String CHECK_IN_IN_FOLDER_SUBJECT_FOR_PRODUCT = "15";

   @RBEntry("Check-In di \"{0}\" nella cartella \"{1}\" della libreria \"{2}\" effettuato")
   public static final String CHECK_IN_IN_FOLDER_SUBJECT_FOR_LIBRARY = "16";

   @RBEntry("Stato \"{1}\" raggiunto da \"{0}\" nella cartella \"{2}\" del progetto \"{3}\"")
   public static final String STATE_CHANGE_IN_FOLDER_SUBJECT_FOR_PROJECT = "17";

   @RBEntry("Stato \"{1}\" raggiunto da \"{0}\" nella cartella \"{2}\" del prodotto \"{3}\"")
   public static final String STATE_CHANGE_IN_FOLDER_SUBJECT_FOR_PRODUCT = "18";

   @RBEntry("Stato \"{1}\" raggiunto da \"{0}\" nella cartella \"{2}\" dellla libreria \"{3}\"")
   public static final String STATE_CHANGE_IN_FOLDER_SUBJECT_FOR_LIBRARY = "19";

   @RBEntry("{0}")
   public static final String USER_SUBJECT = "20";

   @RBEntry("Descrizione:")
   public static final String DESCRIPTION_LABEL = "21";

   @RBEntry("Commento per il Check-In:")
   public static final String CHECK_IN_COMMENT_LABEL = "22";

   @RBEntry("Sottocartella")
   public static final String SUB_FOLDER = "23";

   @RBEntry("Nome progetto:")
   public static final String PROJECT_NAME_LABEL = "24";

   @RBEntry("Nome prodotto:")
   public static final String PRODUCT_NAME_LABEL = "25";

   @RBEntry("Nome libreria:")
   public static final String LIBRARY_NAME_LABEL = "26";

   @RBEntry("Stato del ciclo di vita:")
   @RBComment("Label for the state of the object")
   public static final String OBJECT_STATE_LABEL = "27";

   @RBEntry("Creazione di più elementi nella cartella \"{0}\" del progetto \"{1}\" completata")
   public static final String MULTI_CREATE_IN_FOLDER_SUBJECT_FOR_PROJECT = "28";

   @RBEntry("Creazione di più elementi nella cartella \"{0}\" del prodotto \"{1}\" completata")
   public static final String MULTI_CREATE_IN_FOLDER_SUBJECT_FOR_PRODUCT = "29";

   @RBEntry("Creazione di più elementi nella cartella \"{0}\" della libreria \"{1}\" completata")
   public static final String MULTI_CREATE_IN_FOLDER_SUBJECT_FOR_LIBRARY = "30";

   @RBEntry("Check-In di più elementi nella cartella \"{0}\" del progetto \"{1}\" effettuato")
   public static final String MULTI_CHECK_IN_IN_FOLDER_SUBJECT_FOR_PROJECT = "31";

   @RBEntry("Check-In di più elementi nella cartella \"{0}\" del prodotto \"{1}\" effettuato")
   public static final String MULTI_CHECK_IN_IN_FOLDER_SUBJECT_FOR_PRODUCT = "32";

   @RBEntry("Check-In di più elementi nella cartella \"{0}\" della libreria \"{1}\" effettuato")
   public static final String MULTI_CHECK_IN_IN_FOLDER_SUBJECT_FOR_LIBRARY = "33";

   @RBEntry("Elementi creati:")
   public static final String CREATED_LABEL_PLURAL = "34";

   @RBEntry("Elementi sottoposti a Check-In:")
   public static final String CHECKED_IN_LABEL_PLURAL = "35";

   @RBEntry("Stato di più elementi modificato nella cartella \"{0}\" del progetto \"{1}\"")
   public static final String MULTI_STATE_CHANGE_IN_FOLDER_SUBJECT_FOR_PROJECT = "36";

   @RBEntry("Stato di più elementi modificato nella cartella \"{0}\" del prodotto \"{1}\"")
   public static final String MULTI_STATE_CHANGE_IN_FOLDER_SUBJECT_FOR_PRODUCT = "37";

   @RBEntry("Stato di più elementi modificato nella cartella \"{0}\" della libreria \"{1}\"")
   public static final String MULTI_STATE_CHANGE_IN_FOLDER_SUBJECT_FOR_LIBRARY = "38";

   @RBEntry("Elementi il cui stato è cambiato:")
   public static final String STATE_CHANGE_LABEL_PLURAL = "39";

   @RBEntry("Nome")
   public static final String NAME_TITLE = "41";

   @RBEntry("Tipo")
   public static final String TYPE_TITLE = "42";

   @RBEntry("Autore elementi:")
   public static final String OBJECTS_CREATOR_LABEL = "43";

   @RBEntry("Numero:")
   public static final String NUMBER_LABEL = "44";

   @RBEntry("Numero")
   public static final String NUMBER_TITLE = "45";
}
