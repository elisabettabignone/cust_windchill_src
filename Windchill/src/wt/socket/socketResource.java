package wt.socket;

import wt.util.resource.*;

@RBUUID("wt.util.jmx.jmxResource")
public final class socketResource extends WTListResourceBundle {
   @RBEntry("Notification produced when wrapped socket cache overflows and the oldest socket is automatically closed")
   public static final String WRAPPED_SOCKET_OVERFLOW_NOTIF_DESCR = "0";

   @RBEntry("Wrapped socket cache overflow; automatically closing '{0}'")
   public static final String WRAPPED_SOCKET_OVERFLOW_NOTIF_MSG = "1";
}
