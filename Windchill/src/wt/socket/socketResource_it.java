package wt.socket;

import wt.util.resource.*;

@RBUUID("wt.util.jmx.jmxResource")
public final class socketResource_it extends WTListResourceBundle {
   @RBEntry("Notifica creata in caso di overflow della cache socket con wrapper e chiusura automatica del socket meno recente")
   public static final String WRAPPED_SOCKET_OVERFLOW_NOTIF_DESCR = "0";

   @RBEntry("Overflow della cache socket con wrapper. Chiusura automatica di '{0}'")
   public static final String WRAPPED_SOCKET_OVERFLOW_NOTIF_MSG = "1";
}
