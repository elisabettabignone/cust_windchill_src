/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.pdmlink;

import wt.util.resource.*;

@RBUUID("wt.pdmlink.pdmlinkResource")
public final class pdmlinkResource extends WTListResourceBundle {
   @RBEntry("Delete of \"{0}\" failed.  This object is the end item of Product \"{1}\".")
   @RBArgComment0("is the identity of the end item")
   @RBArgComment1("is the identity of the container")
   public static final String CANNOT_DELETE_END_ITEM = "0";

   @RBEntry("Export of {0} is not supported for this release.")
   public static final String EXPORT_NOT_SUPPORTED = "1";
}
