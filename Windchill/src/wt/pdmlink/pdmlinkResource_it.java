/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.pdmlink;

import wt.util.resource.*;

@RBUUID("wt.pdmlink.pdmlinkResource")
public final class pdmlinkResource_it extends WTListResourceBundle {
   @RBEntry("L'eliminazione di \"{0}\" è fallita. L'oggetto è il prodotto finale del prodotto \"{1}\".")
   @RBArgComment0("is the identity of the end item")
   @RBArgComment1("is the identity of the container")
   public static final String CANNOT_DELETE_END_ITEM = "0";

   @RBEntry("L'esportazione di {0} non è supportata per questa release.")
   public static final String EXPORT_NOT_SUPPORTED = "1";
}
