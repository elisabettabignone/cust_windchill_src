/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.preference;

import wt.util.resource.*;

@RBUUID("wt.preference.PreferenceExpImpRB")
public final class PreferenceExpImpRB extends WTListResourceBundle {
   @RBEntry("Preference instance \"{0}\" already exists at the \"{1}\" level. The following values differ : PrefEmpImp \n \t attribute \"{2}\" : existing value: \"{3}\", new value: \"{4}\".")
   public static final String EXISTING_PREFERENCE_INSTANCE = "0";

   @RBEntry(" Cannot import \"{0}\" \"{1}\" as there in no matching preference definition with name \"{2}\".")
   public static final String NO_PREFERENCE_DEFINITION_EXIST = "1";

   @RBEntry("Cannot create a preference instance \"{0}\" at \"{1}\" level for preference definition defined at \"{2}\" level.")
   public static final String NOT_VISIBLE_FOR_CONTEXT = "2";

   @RBEntry("Preference Definition \"{0}\" already exists. PrefEmpImp The following values differ : \n attribute \"{1}\" : existing value: \"{2}\", new value: \"{3}\".")
   public static final String EXISTING_PREFERENCE_DEFINITION = "3";

   @RBEntry("Preference Category \"{0}\" already exists. PrefEmpImp The following values differ : \n attribute \"{1}\" : existing value: \"{2}\", new value: \"{3}\".")
   public static final String EXISTING_PREFERENCE_CATEGORY = "4";

   @RBEntry("Preference Client \"{0}\" already exists. PrefEmpImp The following values differ : \n attribute \"{1}\" : existing value: \"{2}\", new value: \"{3}\".")
   public static final String EXISTING_PREFERENCE_CLIENT = "5";

   @RBEntry("Preference Client Definition Link \"{0}\" already exists. PrefEmpImp The following values differ : \n attribute \"{1}\" : existing value: \"{2}\", new value: \"{3}\".")
   public static final String EXISTING_PREFERENCE_CLIENT_DEFINITION = "6";

   @RBEntry("Cannot import preference definition \"{0}\", as there in no matching preference category with name \"{1}\".")
   public static final String NO_PREFERENCE_CATEGORY_EXIST = "7";

   @RBEntry("Cannot import \"{0}\" \"{1}\", as there in no matching preference client with name \"{2}\".")
   public static final String NO_PREFERENCE_CLIENT_EXIST = "8";

   @RBEntry("Multivalued preference instance \"{0}\" already exists at the \"{1}\" level. The following values differ : PrefEmpImp \n \t attribute \"{2}\" : existing value: \"{3}\", new value: \"{4}\".")
   public static final String EXISTING_MV_PREFERENCE_INSTANCE = "9";

   @RBEntry("Multivalued preference instance \"{0}\" already exists at the \"{1}\" level. The following values differ : PrefEmpImp \n \t \t new value for key : \"{2}\"  attribute \"{3}\" : existing value: \"{4}\", new value: \"{5}\".")
   public static final String MV_PREFERENCE_INSTANCE_KEY_UPDATE = "10";

   @RBEntry("Multivalued preference instance \"{0}\" already exists at the \"{1}\" level. The following values differ : PrefEmpImp \n \t \t new key : \"{2}\".")
   public static final String MV_PREFERENCE_INSTANCE_KEY_ADDED = "11";

   @RBEntry("Multivalued preference instance \"{0}\" already exists at the \"{1}\" level. The following values differ : PrefEmpImp \n \t \t key missing : \"{2}\".")
   public static final String MV_PREFERENCE_INSTANCE_KEY_DELETE = "12";

   @RBEntry("Preference category definition link \"{0}\" already exists. PrefEmpImp The following values differ : \n attribute \"{1}\" : existing value: \"{2}\", new value: \"{3}\".")
   public static final String EXISTING_PREFERENCE_CATEGORY_DEFINITION = "13";

   @RBEntry("Cannot import preference definition \"{0}\", as there in no matching preference category role with name \"{1}\".")
   public static final String NO_PREFERENCE_CATEGORY_ROLE_EXIST = "14";
  
}
