/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.preference;

import wt.util.resource.*;

@RBUUID("wt.preference.preferenceResource")
public final class preferenceResource extends WTListResourceBundle {
   /**
    * Entry
    **/
   @RBEntry("Category")
   public static final String CATEGORY_LABEL = "CATEGORY_LABEL";

   @RBEntry("Client")
   public static final String CLIENT_LABEL = "CLIENT_LABEL";

   @RBEntry("Container")
   public static final String CONTAINER_CONTEXT = "CONTAINER_CONTEXT";

   @RBEntry("Default")
   public static final String DEFAULT_CONTEXT = "DEFAULT_CONTEXT";

   @RBEntry("Defintion")
   public static final String DEFINITION_LABEL = "DEFINITION_LABEL";

   @RBEntry("** ERROR [{0}:{1}]")
   public static final String GET_LOCALIZABLE_ERROR = "GET_LOCALIZABLE_ERROR";

   @RBEntry("Hidden")
   public static final String HIDDEN = "HIDDEN";

   @RBEntry("Organization")
   public static final String ORGANIZATION_CONTEXT = "ORGANIZATION_CONTEXT";

   @RBEntry("Full Description")
   public static final String PREFERENCE_FULL_DESC_LABEL = "PREFERENCE_FULL_DESC_LABEL";

   @RBEntry("Internal Name")
   public static final String PREFERENCE_INTERNAL_NAME_LABEL = "PREFERENCE_INTERNAL_NAME_LABEL";

   @RBEntry("Name")
   public static final String PREFERENCE_NAME_LABEL = "PREFERENCE_NAME_LABEL";

   @RBEntry("Description")
   public static final String PREFERENCE_SHORT_DESC_LABEL = "PREFERENCE_SHORT_DESC_LABEL";

   @RBEntry("Site")
   public static final String SITE_CONTEXT = "SITE_CONTEXT";

   @RBEntry("Other")
   public static final String Unassigned = "Unassigned";

   @RBEntry("User")
   public static final String USER_CONTEXT = "USER_CONTEXT";

   @RBEntry("User Only")
   public static final String USER_ONLY = "USER_ONLY";

   @RBEntry("Windchill")
   public static final String WINDCHILL_CLIENT = "WINDCHILL_CLIENT";

   @RBEntry("Unassigned ({0})")
   public static final String UNASSIGNED_WITH_NAME = "UNASSIGNED_WITH_NAME";

   @RBEntry("Configuration Specification")
   public static final String PREFERENCE_CONFIGSPEC_CATEGORY = "PREFERENCE_CONFIGSPEC_CATEGORY";

   @RBEntry("Context")
   public static final String PREFERENCE_CONTEXT_CATEGORY = "PREFERENCE_CONTEXT_CATEGORY";

   @RBEntry("Display")
   public static final String PREFERENCE_DISPLAY_CATEGORY = "PREFERENCE_DISPLAY_CATEGORY";

   @RBEntry("Search")
   public static final String PREFERENCE_SEARCH_CATEGORY = "PREFERENCE_SEARCH_CATEGORY";

   @RBEntry("Tables")
   public static final String PREFERENCE_TABLES_CATEGORY = "PREFERENCE_TABLES_CATEGORY";

   @RBEntry("Visualization")
   public static final String PREFERENCE_VISUALIZATION_CATEGORY = "PREFERENCE_VISUALIZATION_CATEGORY";

   @RBEntry("Security")
   public static final String PREFERENCE_SECURITY_CATEGORY = "PREFERENCE_SECURITY_CATEGORY";

   @RBEntry("Access Control Component")
   public static final String PREFERENCE_SECURITY_ACCESS_CONTROL_CATEGORY = "PREFERENCE_SECURITY_ACCESS_CONTROL_CATEGORY";

   @RBEntry("Permission Settings")
   public static final String PREFERENCE_SECURITY_PERMISSION_CATEGORY = "PREFERENCE_SECURITY_PERMISSION_CATEGORY";

   @RBEntry("Policy Administration ")
   public static final String PREFERENCE_SECURITY_POLICYADMIN_CATEGORY = "PREFERENCE_SECURITY_POLICYADMIN_CATEGORY";

   @RBEntry("Effectivity Management")
   public static final String PREFERENCE_EFFECTIVITY_MANAGEMENT_CATEGORY = "PREFERENCE_EFFECTIVITY_MANAGEMENT_CATEGORY";

   @RBEntry("Configuration")
   public static final String PREFERENCE_EFFECTIVITY_MANAGEMENT_CONFIG_CATEGORY = "PREFERENCE_EFFECTIVITY_MANAGEMENT_CONFIG_CATEGORY";

   @RBEntry("Attribute Handling")
   public static final String PREFERENCE_ATTRIBUTE_HANDLING_CATEGORY = "PREFERENCE_ATTRIBUTE_HANDLING_CATEGORY";

   @RBEntry("Truncation")
   public static final String PREFERENCE_TRUNCATION_CATEGORY = "PREFERENCE_TRUNCATION_CATEGORY";

   @RBEntry("On Tables")
   public static final String PREFERENCE_TABLE_TRUNCATION_CATEGORY = "PREFERENCE_TABLE_TRUNCATION_CATEGORY";

   @RBEntry("On Information Pages")
   public static final String PREFERENCE_INFOPAGE_TRUNCATION_CATEGORY = "PREFERENCE_INFOPAGE_TRUNCATION_CATEGORY";

   @RBEntry("Create and Edit")
   public static final String PREFERENCE_CREATE_EDIT_CATEGORY = "PREFERENCE_CREATE_EDIT_CATEGORY";

   @RBEntry("Notification")
   public static final String PREFERENCE_NOTIFICATION_CATEGORY = "PREFERENCE_NOTIFICATION_CATEGORY";

   @RBEntry("Promotion Process")
   public static final String PREFERENCE_PROMOTION_CATEGORY = "PREFERENCE_PROMOTION_CATEGORY";

   @RBEntry("Collector")
   public static final String PRIVATE_CONSTANT_0 = "PREFERENCE_PROMOTION_COLLECTOR_CATEGORY";

   @RBEntry("Promotion Collector Rule Values")
   public static final String PRIVATE_CONSTANT_1 = "PREFERENCE_PROMOTION_COLLECTOR_CATEGORY_DESC";

   @RBEntry("Content")
   public static final String PREFERENCE_CONTENT_CATEGORY = "PREFERENCE_CONTENT_CATEGORY";

   /**
    * Revision category used for Revise action related preferences
    **/
   @RBEntry("Revise")
   public static final String PRIVATE_CONSTANT_2 = "REVISION_CATEGORY";

   @RBEntry("Revise Preferences")
   public static final String REVISION_CATEGORY_DESC = "REVISION_CATEGORY_DESC";

   @RBEntry("Collector")
   public static final String PRIVATE_CONSTANT_3 = "REVISE_COLLECTOR_CATEGORY";

   @RBEntry("Revise Collector Rule Values")
   public static final String REVISE_COLLECTOR_CATEGORY_DESC = "REVISE_COLLECTOR_CATEGORY_DESC";


   /**
    * Save As category used for Save As action related preferences
    **/
   @RBEntry("Save As")
   public static final String PRIVATE_CONSTANT_4 = "SAVEAS_CAT_NAME";

   @RBEntry("Save As operation preferences")
   public static final String PRIVATE_CONSTANT_5 = "SAVEAS_CAT_DESCR";

   @RBEntry("Naming Patterns")
   public static final String PRIVATE_CONSTANT_6 = "SAVEAS_NAMING_PATTERN_CATEGORY";

   @RBEntry("Save As Naming Patterns")
   public static final String PRIVATE_CONSTANT_7 = "SAVEAS_NAMING_PATTERN_CATEGORY_DESC";

   @RBEntry("From Commonspace Collector")
   public static final String PRIVATE_CONSTANT_8 = "SAVEAS_IN_COMMONSPACE_COLLECTOR_CATEGORY";

   @RBEntry("Save As from Commonspace Collector Rule Values")
   public static final String PRIVATE_CONSTANT_9 = "SAVEAS_IN_COMMONSPACE_COLLECTOR_CATEGORY_DESC";

   @RBEntry("From Workspace Collector")
   public static final String PRIVATE_CONSTANT_10 = "SAVEAS_IN_WORKSPACE_COLLECTOR_CATEGORY";

   @RBEntry("Save As from Workspace Collector Rule Values")
   public static final String PRIVATE_CONSTANT_11 = "SAVEAS_IN_WORKSPACE_COLLECTOR_CATEGORY_DESC";

   /**
    * Delete object category used for Delete action related preferences
    **/
   @RBEntry("Delete")
   public static final String PRIVATE_CONSTANT_12 = "DELETE_OBJECT_CATEGORY";

   @RBEntry("Delete preferences")
   public static final String PRIVATE_CONSTANT_13 = "DELETE_OBJECT_CATEGORY_DESC";

   @RBEntry("Collector")
   public static final String PRIVATE_CONSTANT_14 = "DELETE_OBJECT_COLLECTOR_CATEGORY";

   @RBEntry("Delete Collector Rule Values")
   public static final String PRIVATE_CONSTANT_15 = "DELETE_OBJECT_COLLECTOR_CATEGORY_DESC";

   @RBEntry("General Collector")
   public static final String PRIVATE_CONSTANT_16 = "COLLECT_ITEMS_PICKER_COLLECTOR_CATEGORY";

   @RBEntry("Collector Rule Values for Agreements")
   public static final String PRIVATE_CONSTANT_17 = "COLLECT_ITEMS_PICKER_COLLECTOR_CATEGORY_DESC";

   /**
    * Integral Operations category used for integral operations related preferences for an action
    **/
   @RBEntry("Integral Operations")
   public static final String PRIVATE_CONSTANT_18 = "INTEGRAL_OPERATIONS_CATEGORY";

   @RBEntry("Integral Operations action preferences. Contains preferences like Move, Add to Project and Send to PDM collector.")
   public static final String PRIVATE_CONSTANT_19 = "INTEGRAL_OPERATIONS_CATEGORY_DESC";

   @RBEntry("Add to Project Collector")
   public static final String PRIVATE_CONSTANT_20 = "ADD_TO_PROJECT_COLLECTOR_CATEGORY";

   @RBEntry("Add to Project Collector Rule Values")
   public static final String PRIVATE_CONSTANT_21 = "ADD_TO_PROJECT_COLLECTOR_CATEGORY_DESC";

   @RBEntry("Move Collector")
   public static final String PRIVATE_CONSTANT_22 = "MOVE_COLLECTOR_CATEGORY";

   @RBEntry("Move Collector Rule Values")
   public static final String PRIVATE_CONSTANT_23 = "MOVE_COLLECTOR_CATEGORY_DESC";

   @RBEntry("Send to PDM Collector")
   public static final String PRIVATE_CONSTANT_24 = "SEND_TO_PDM_COLLECTOR_CATEGORY";

   @RBEntry("Send to PDM Collector Rule Values")
   public static final String PRIVATE_CONSTANT_25 = "SEND_TO_PDM_COLLECTOR_CATEGORY_DESC";

   @RBEntry("Convert to Share Collector")
   public static final String PRIVATE_CONSTANT_26 = "CONVERT_TO_SHARE_COLLECTOR_CATEGORY";

   @RBEntry("Convert to Share Collector Rule Values")
   public static final String PRIVATE_CONSTANT_27 = "CONVERT_TO_SHARE_COLLECTOR_CATEGORY_DESC";

   @RBEntry("Update Project Collector")
   public static final String UPDATE_PROJECT_COLLECTOR_CATEGORY = "UPDATE_PROJECT_COLLECTOR_CATEGORY";

   @RBEntry("Update Project Collector Rule Values")
   public static final String UPDATE_PROJECT_COLLECTOR_CATEGORY_DESC = "UPDATE_PROJECT_COLLECTOR_CATEGORY_DESC";

   /**
    * Test Collector category used for test type of Collector preferences
    **/
   @RBEntry("Test Collector")
   public static final String PRIVATE_CONSTANT_28 = "TEST_COLLECTOR_CATEGORY";

   @RBEntry("Test Collector Rule Values")
   public static final String PRIVATE_CONSTANT_29 = "TEST_COLLECTOR_CATEGORY_DESC";

   @RBEntry("Collection Component Collector ")
   public static final String PRIVATE_CONSTANT_30 = "TEST_COLLECTION_COMPONENT_COLLECTOR_CATEGORY";

   @RBEntry("Collection Component Collector Rule Values")
   public static final String PRIVATE_CONSTANT_31 = "TEST_COLLECTION_COMPONENT_COLLECTOR_CATEGORY_DESC";

   @RBEntry("Collection Picker Dialog Collector")
   public static final String PRIVATE_CONSTANT_32 = "TEST_COLLECTION_PICKER_DIALOG_COLLECTOR_CATEGORY";

   @RBEntry("Collection Picker Dialog Collector Rule Values")
   public static final String PRIVATE_CONSTANT_33 = "TEST_COLLECTION_PICKER_DIALOG_COLLECTOR_CATEGORY_DESC";

   @RBEntry("Simple Collection Collector")
   public static final String PRIVATE_CONSTANT_34 = "SIMPLE_TEST_COLLECTION_COLLECTOR_CATEGORY";

   @RBEntry("Simple Collection Collector Rule Values")
   public static final String PRIVATE_CONSTANT_35 = "SIMPLE_TEST_COLLECTION_COLLECTOR_CATEGORY_DESC";

   @RBEntry("Clipboard")
   public static final String PREFERENCE_CLIPBOARD_CATEGORY = "PREFERENCE_CLIPBOARD_CATEGORY";

   @RBEntry("Remove On Paste")
   public static final String CLIPBOARD_REMOVE_ON_PASTE_PREF_DISPLAYNAME = "CLIPBOARD_REMOVE_ON_PASTE_PREF_DISPLAYNAME";

   @RBEntry("Choose whether to remove items from the Clipboard when they are pasted or not")
   public static final String CLIPBOARD_REMOVE_ON_PASTE_PREF_DESCRIPTION = "CLIPBOARD_REMOVE_ON_PASTE_PREF_DESCRIPTION";

   @RBEntry("Choose whether to remove items from the Clipboard when they are pasted or not")
   public static final String CLIPBOARD_REMOVE_ON_PASTE_PREF_LONG_DESCRIPTION = "CLIPBOARD_REMOVE_ON_PASTE_PREF_LONG_DESCRIPTION";

   @RBEntry("Show Thumbnail")
   public static final String CLIPBOARD_SHOW_THUMBNAIL_PREF_DISPLAYNAME = "CLIPBOARD_SHOW_THUMBNAIL_PREF_DISPLAYNAME";

   @RBEntry("Choose whether to display thumbnail image column in Clipboard or not")
   public static final String CLIPBOARD_SHOW_THUMBNAIL_PREF_DESCRIPTION = "CLIPBOARD_SHOW_THUMBNAIL_PREF_DESCRIPTION";

   @RBEntry("Choose whether to display thumbnail image column in Clipboard or not")
   public static final String CLIPBOARD_SHOW_THUMBNAIL_PREF_LONG_DESCRIPTION = "CLIPBOARD_SHOW_THUMBNAIL_PREF_LONG_DESCRIPTION";

   @RBEntry("Preference Definition {0} does not exist.")
   @RBArgComment0("The string name of the Prefenence Definition object which does not exist.")
   public static final String PREFERENCE_ERROR_NO_PREF_DEFINITION = "PREFERENCE_ERROR_NO_PREF_DEFINITION";

   @RBEntry("The field \"{0}\" needs to be present to correctly display preference.")
   @RBArgComment0("The string name of the field missing from a definition in a preference load file.")
   public static final String PREFERENCE_LOAD_ERROR_MISSING_VALUE = "PREFERENCE_LOAD_ERROR_MISSING_VALUE";

   @RBEntry("Excluded Part Subtypes")
   @RBComment("Display name for preference /com/ptc/windchill/enterprise/search/excludes/wt.part.WTPart")
   public static final String PRIVATE_CONSTANT_36 = "EXCLUDE_PREFERENCE_NAME";

   @RBEntry("Configures the part subtypes that are excluded from searches for part")
   @RBComment("For preference /com/ptc/windchill/enterprise/search/excludes/wt.part.WTPart")
   public static final String PRIVATE_CONSTANT_37 = "EXCLUDE_PREFERENCE_SHORT_DESC";

   @RBEntry("Configures the part subtypes that are excluded from searches for part")
   @RBComment("For preference /com/ptc/windchill/enterprise/search/excludes/wt.part.WTPart")
   public static final String PRIVATE_CONSTANT_38 = "EXCLUDE_PREFERENCE_DESC";

   @RBEntry("Excluded Part Subtypes")
   @RBComment("Display label for preference /com/ptc/windchill/enterprise/search/excludes/wt.part.WTPart")
   public static final String PRIVATE_CONSTANT_39 = "EXCLUDE_PREFERENCE_LABEL";

   @RBEntry("None")
   @RBComment("Option for preference value. No subtypes excluded")
   public static final String PRIVATE_CONSTANT_40 = "EXCLUDE_PREFERENCE_NONE";

   @RBEntry("Invalid Value: \"{0}\".")
   @RBComment("Generic message to display when an inputted preference value is invalid, but there is no specific message returned from the preference value handler.")
   public static final String INVALID_VALUE = "INVALID_VALUE";

   /**
    * Add to baseline category used for Add to baseline action related preferences
    **/
   @RBEntry("Add to Baseline")
   public static final String PRIVATE_CONSTANT_41 = "ADDTOBASELINE_CATEGORY_CAT_NAME";

   @RBEntry("Add to Baseline operation preferences")
   public static final String PRIVATE_CONSTANT_42 = "ADDTOBASELINE_CATEGORY_CAT_DESCR";

   @RBEntry("Collector")
   public static final String PRIVATE_CONSTANT_43 = "ADDTOBASELINE_COLLECTOR_CATEGORY";

   @RBEntry("Add to Baseline Collector Rule Values")
   public static final String PRIVATE_CONSTANT_44 = "ADDTOBASELINE_COLLECTOR_CATEGORY_DESC";

   /**
    * Display Related Manufacturing Items category used for Display Related Manufacturing Items action related preferences
    **/
   @RBEntry("Display Related Manufacturing Objects")
   public static final String PRIVATE_CONSTANT_45 = "DISPLAYRELATEDMANUFACTURINGITEMS_CATEGORY_CAT_NAME";

   @RBEntry("Display Related Manufacturing Objects report preferences")
   public static final String PRIVATE_CONSTANT_46 = "DISPLAYRELATEDMANUFACTURINGITEMS_CATEGORY_CAT_DESCR";

   @RBEntry("Collector")
   public static final String PRIVATE_CONSTANT_47 = "DISPLAYRELATEDMANUFACTURINGITEMS_COLLECTOR_CATEGORY";

   @RBEntry("Display Related Manufacturing Objects Collector Rule Values")
   public static final String PRIVATE_CONSTANT_48 = "DISPLAYRELATEDMANUFACTURINGITEMS_COLLECTOR_CATEGORY_DESC";

   @RBEntry("ATTENTION: Required preference can not have undefined value.")
   @RBComment("The user must fill a value other than undefined before moving on to the next step or completing the action.")
   public static final String UNDEFINED_VALUE = "UNDEFINED_VALUE";

   @RBEntry("Display")
   @RBComment("Default Role Type for Preference Category")
   public static final String DISPLAY = "DISPLAY";

   @RBEntry("Download")
   @RBComment("Download Role Type for preference category")
   public static final String DOWNLOAD = "DOWNLOAD";

   @RBEntry("ATTENTION: You do not have the necessary authorization for this operation. Contact your administrator if you believe you have received this message in error.")
   @RBComment("NotAuthorizedException: The user does not have the required permission to access an object.")
   public static final String NOT_AUTHORIZED_EXCEPTION = "NOT_AUTHORIZED_EXCEPTION";

   /**
    * Meeting category used for
    */
   @RBEntry("Meeting")
   public static final String MEETING_CAT_NAME = "MEETING_CAT_NAME";


}
