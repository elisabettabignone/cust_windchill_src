/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.preference;

import wt.util.resource.*;

@RBUUID("wt.preference.PreferenceExpImpRB")
public final class PreferenceExpImpRB_it extends WTListResourceBundle {
   @RBEntry("L'istanza di preferenza \"{0}\" esiste già nel livello \"{1}\". Differiscono i seguenti valori: PrefEmpImp \n \t Attributo \"{2}\" - valore esistente: \"{3}\", nuovo valore: \"{4}\".")
   public static final String EXISTING_PREFERENCE_INSTANCE = "0";

   @RBEntry("Impossibile importare \"{0}\" \"{1}\". Nessuna definizione preferenza corrispondente con nome \"{2}\".")
   public static final String NO_PREFERENCE_DEFINITION_EXIST = "1";

   @RBEntry("Impossibile creare un'istanza di preferenza \"{0}\" al livello \"{1}\" per la definizione preferenza definita al livello \"{2}\".")
   public static final String NOT_VISIBLE_FOR_CONTEXT = "2";

   @RBEntry("La definizione preferenza \"{0}\" esiste già. PrefEmpImp. Differiscono i seguenti valori: \nAttributo \"{1}\" - valore esistente: \"{2}\", nuovo valore: \"{3}\".")
   public static final String EXISTING_PREFERENCE_DEFINITION = "3";

   @RBEntry("La categoria preferenze \"{0}\" esiste già. PrefEmpImp. Differiscono i seguenti valori: \nAttributo \"{1}\" - valore esistente: \"{2}\", nuovo valore: \"{3}\".")
   public static final String EXISTING_PREFERENCE_CATEGORY = "4";

   @RBEntry("Il client di preferenza \"{0}\" esiste già. PrefEmpImp. Differiscono i seguenti valori: \nAttributo \"{1}\" -  valore esistente: \"{2}\", nuovo valore: \"{3}\".")
   public static final String EXISTING_PREFERENCE_CLIENT = "5";

   @RBEntry("Il link di definizione del client di preferenza \"{0}\" esiste già. PrefEmpImp. Differiscono i seguenti valori: \nAttributo \"{1}\" - valore esistente: \"{2}\", nuovo valore: \"{3}\".")
   public static final String EXISTING_PREFERENCE_CLIENT_DEFINITION = "6";

   @RBEntry("Impossibile importare la definizione preferenza \"{0}\". Nessuna categoria preferenze corrispondente con nome \"{1}\".")
   public static final String NO_PREFERENCE_CATEGORY_EXIST = "7";

   @RBEntry("Impossibile importare \"{0}\" \"{1}\". Nessun client di preferenza corrispondente con nome \"{2}\".")
   public static final String NO_PREFERENCE_CLIENT_EXIST = "8";

   @RBEntry("L'istanza di preferenza con più valori \"{0}\" esiste già nel livello \"{1}\". Differiscono i seguenti valori: PrefEmpImp \n \t Attributo \"{2}\" - valore esistente: \"{3}\", nuovo valore: \"{4}\".")
   public static final String EXISTING_MV_PREFERENCE_INSTANCE = "9";

   @RBEntry("L'istanza di preferenza con più valori \"{0}\" esiste già nel livello \"{1}\". Differiscono i seguenti valori: PrefEmpImp \n \t \t Nuovo valore per chiave: \"{2}\" attributo \"{3}\" - valore esistente: \"{4}\", nuovo valore: \"{5}\".")
   public static final String MV_PREFERENCE_INSTANCE_KEY_UPDATE = "10";

   @RBEntry("L'istanza di preferenza con più valori \"{0}\" esiste già nel livello \"{1}\". Differiscono i seguenti valori: PrefEmpImp \n \t \t Nuova chiave: \"{2}\".")
   public static final String MV_PREFERENCE_INSTANCE_KEY_ADDED = "11";

   @RBEntry("L'istanza di preferenza con più valori \"{0}\" esiste già nel livello \"{1}\". Differiscono i seguenti valori: PrefEmpImp \n \t \t Chiave mancante: \"{2}\".")
   public static final String MV_PREFERENCE_INSTANCE_KEY_DELETE = "12";

   @RBEntry("Il link di definizione della categoria di preferenza \"{0}\" esiste già. PrefEmpImp. Differiscono i seguenti valori: \nAttributo \"{1}\" - valore esistente: \"{2}\", nuovo valore: \"{3}\".")
   public static final String EXISTING_PREFERENCE_CATEGORY_DEFINITION = "13";

   @RBEntry("Impossibile importare la definizione preferenza \"{0}\". Nessun ruolo di categoria preferenze corrispondente con nome \"{1}\".")
   public static final String NO_PREFERENCE_CATEGORY_ROLE_EXIST = "14";
  
}
