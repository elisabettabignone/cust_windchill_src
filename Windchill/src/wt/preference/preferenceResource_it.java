/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.preference;

import wt.util.resource.*;

@RBUUID("wt.preference.preferenceResource")
public final class preferenceResource_it extends WTListResourceBundle {
   /**
    * Entry
    **/
   @RBEntry("Categoria")
   public static final String CATEGORY_LABEL = "CATEGORY_LABEL";

   @RBEntry("Client")
   public static final String CLIENT_LABEL = "CLIENT_LABEL";

   @RBEntry("Contenitore")
   public static final String CONTAINER_CONTEXT = "CONTAINER_CONTEXT";

   @RBEntry("Default")
   public static final String DEFAULT_CONTEXT = "DEFAULT_CONTEXT";

   @RBEntry("Definizione")
   public static final String DEFINITION_LABEL = "DEFINITION_LABEL";

   @RBEntry("** ERRORE [{0}:{1}]")
   public static final String GET_LOCALIZABLE_ERROR = "GET_LOCALIZABLE_ERROR";

   @RBEntry("Nascosto")
   public static final String HIDDEN = "HIDDEN";

   @RBEntry("Organizzazione")
   public static final String ORGANIZATION_CONTEXT = "ORGANIZATION_CONTEXT";

   @RBEntry("Descrizione completa")
   public static final String PREFERENCE_FULL_DESC_LABEL = "PREFERENCE_FULL_DESC_LABEL";

   @RBEntry("Nome interno")
   public static final String PREFERENCE_INTERNAL_NAME_LABEL = "PREFERENCE_INTERNAL_NAME_LABEL";

   @RBEntry("Nome")
   public static final String PREFERENCE_NAME_LABEL = "PREFERENCE_NAME_LABEL";

   @RBEntry("Descrizione")
   public static final String PREFERENCE_SHORT_DESC_LABEL = "PREFERENCE_SHORT_DESC_LABEL";

   @RBEntry("Sito")
   public static final String SITE_CONTEXT = "SITE_CONTEXT";

   @RBEntry("Altro")
   public static final String Unassigned = "Unassigned";

   @RBEntry("Utente")
   public static final String USER_CONTEXT = "USER_CONTEXT";

   @RBEntry("Solo utente")
   public static final String USER_ONLY = "USER_ONLY";

   @RBEntry("Windchill")
   public static final String WINDCHILL_CLIENT = "WINDCHILL_CLIENT";

   @RBEntry("Non assegnato ({0})")
   public static final String UNASSIGNED_WITH_NAME = "UNASSIGNED_WITH_NAME";

   @RBEntry("Specifica di configurazione")
   public static final String PREFERENCE_CONFIGSPEC_CATEGORY = "PREFERENCE_CONFIGSPEC_CATEGORY";

   @RBEntry("Contesto")
   public static final String PREFERENCE_CONTEXT_CATEGORY = "PREFERENCE_CONTEXT_CATEGORY";

   @RBEntry("Visualizzazione")
   public static final String PREFERENCE_DISPLAY_CATEGORY = "PREFERENCE_DISPLAY_CATEGORY";

   @RBEntry("Ricerca")
   public static final String PREFERENCE_SEARCH_CATEGORY = "PREFERENCE_SEARCH_CATEGORY";

   @RBEntry("Tabelle")
   public static final String PREFERENCE_TABLES_CATEGORY = "PREFERENCE_TABLES_CATEGORY";

   @RBEntry("Visualization")
   public static final String PREFERENCE_VISUALIZATION_CATEGORY = "PREFERENCE_VISUALIZATION_CATEGORY";

   @RBEntry("Protezione")
   public static final String PREFERENCE_SECURITY_CATEGORY = "PREFERENCE_SECURITY_CATEGORY";

   @RBEntry("Componente Controllo d'accesso")
   public static final String PREFERENCE_SECURITY_ACCESS_CONTROL_CATEGORY = "PREFERENCE_SECURITY_ACCESS_CONTROL_CATEGORY";

   @RBEntry("Impostazioni permessi di accesso")
   public static final String PREFERENCE_SECURITY_PERMISSION_CATEGORY = "PREFERENCE_SECURITY_PERMISSION_CATEGORY";

   @RBEntry("Amministrazione regole")
   public static final String PREFERENCE_SECURITY_POLICYADMIN_CATEGORY = "PREFERENCE_SECURITY_POLICYADMIN_CATEGORY";

   @RBEntry("Gestione effettività")
   public static final String PREFERENCE_EFFECTIVITY_MANAGEMENT_CATEGORY = "PREFERENCE_EFFECTIVITY_MANAGEMENT_CATEGORY";

   @RBEntry("Configurazione")
   public static final String PREFERENCE_EFFECTIVITY_MANAGEMENT_CONFIG_CATEGORY = "PREFERENCE_EFFECTIVITY_MANAGEMENT_CONFIG_CATEGORY";

   @RBEntry("Gestione attributi")
   public static final String PREFERENCE_ATTRIBUTE_HANDLING_CATEGORY = "PREFERENCE_ATTRIBUTE_HANDLING_CATEGORY";

   @RBEntry("Troncamento")
   public static final String PREFERENCE_TRUNCATION_CATEGORY = "PREFERENCE_TRUNCATION_CATEGORY";

   @RBEntry("Su tabelle")
   public static final String PREFERENCE_TABLE_TRUNCATION_CATEGORY = "PREFERENCE_TABLE_TRUNCATION_CATEGORY";

   @RBEntry("Su pagine di informazioni")
   public static final String PREFERENCE_INFOPAGE_TRUNCATION_CATEGORY = "PREFERENCE_INFOPAGE_TRUNCATION_CATEGORY";

   @RBEntry("Crea e modifica")
   public static final String PREFERENCE_CREATE_EDIT_CATEGORY = "PREFERENCE_CREATE_EDIT_CATEGORY";

   @RBEntry("Notifica")
   public static final String PREFERENCE_NOTIFICATION_CATEGORY = "PREFERENCE_NOTIFICATION_CATEGORY";

   @RBEntry("Processo di promozione")
   public static final String PREFERENCE_PROMOTION_CATEGORY = "PREFERENCE_PROMOTION_CATEGORY";

   @RBEntry("Raccoglitore")
   public static final String PRIVATE_CONSTANT_0 = "PREFERENCE_PROMOTION_COLLECTOR_CATEGORY";

   @RBEntry("Valori regola Raccoglitore di promozione")
   public static final String PRIVATE_CONSTANT_1 = "PREFERENCE_PROMOTION_COLLECTOR_CATEGORY_DESC";

   @RBEntry("Contenuto")
   public static final String PREFERENCE_CONTENT_CATEGORY = "PREFERENCE_CONTENT_CATEGORY";

   /**
    * Revision category used for Revise action related preferences
    **/
   @RBEntry("Nuova revisione")
   public static final String PRIVATE_CONSTANT_2 = "REVISION_CATEGORY";

   @RBEntry("Preferenze nuova revisione")
   public static final String REVISION_CATEGORY_DESC = "REVISION_CATEGORY_DESC";

   @RBEntry("Raccoglitore")
   public static final String PRIVATE_CONSTANT_3 = "REVISE_COLLECTOR_CATEGORY";

   @RBEntry("Valori regola Raccoglitore di creazione nuova revisione")
   public static final String REVISE_COLLECTOR_CATEGORY_DESC = "REVISE_COLLECTOR_CATEGORY_DESC";


   /**
    * Save As category used for Save As action related preferences
    **/
   @RBEntry("Salva con nome")
   public static final String PRIVATE_CONSTANT_4 = "SAVEAS_CAT_NAME";

   @RBEntry("Preferenze per l'operazione Salva con nome")
   public static final String PRIVATE_CONSTANT_5 = "SAVEAS_CAT_DESCR";

   @RBEntry("Espressioni di denominazione")
   public static final String PRIVATE_CONSTANT_6 = "SAVEAS_NAMING_PATTERN_CATEGORY";

   @RBEntry("Salva come espressioni di denominazione")
   public static final String PRIVATE_CONSTANT_7 = "SAVEAS_NAMING_PATTERN_CATEGORY_DESC";

   @RBEntry("Raccoglitore Dal commonspace")
   public static final String PRIVATE_CONSTANT_8 = "SAVEAS_IN_COMMONSPACE_COLLECTOR_CATEGORY";

   @RBEntry("Valori regola Raccoglitore Salva con nome dal commonspace")
   public static final String PRIVATE_CONSTANT_9 = "SAVEAS_IN_COMMONSPACE_COLLECTOR_CATEGORY_DESC";

   @RBEntry("Raccoglitore Dal workspace")
   public static final String PRIVATE_CONSTANT_10 = "SAVEAS_IN_WORKSPACE_COLLECTOR_CATEGORY";

   @RBEntry("Valori regola Raccoglitore Salva con nome dal workspace")
   public static final String PRIVATE_CONSTANT_11 = "SAVEAS_IN_WORKSPACE_COLLECTOR_CATEGORY_DESC";

   /**
    * Delete object category used for Delete action related preferences
    **/
   @RBEntry("Elimina")
   public static final String PRIVATE_CONSTANT_12 = "DELETE_OBJECT_CATEGORY";

   @RBEntry("Preferenze per l'operazione Elimina")
   public static final String PRIVATE_CONSTANT_13 = "DELETE_OBJECT_CATEGORY_DESC";

   @RBEntry("Raccoglitore")
   public static final String PRIVATE_CONSTANT_14 = "DELETE_OBJECT_COLLECTOR_CATEGORY";

   @RBEntry("Valori regola Raccoglitore di eliminazione")
   public static final String PRIVATE_CONSTANT_15 = "DELETE_OBJECT_COLLECTOR_CATEGORY_DESC";

   @RBEntry("Raccoglitore generale")
   public static final String PRIVATE_CONSTANT_16 = "COLLECT_ITEMS_PICKER_COLLECTOR_CATEGORY";

   @RBEntry("Valori regola Raccoglitore per accordi")
   public static final String PRIVATE_CONSTANT_17 = "COLLECT_ITEMS_PICKER_COLLECTOR_CATEGORY_DESC";

   /**
    * Integral Operations category used for integral operations related preferences for an action
    **/
   @RBEntry("Operazioni integrali")
   public static final String PRIVATE_CONSTANT_18 = "INTEGRAL_OPERATIONS_CATEGORY";

   @RBEntry("Preferenze per le azioni Operazioni integrali. Comprende preferenze quali Sposta, Aggiungi a progetto e Raccoglitore di invio a PDM.")
   public static final String PRIVATE_CONSTANT_19 = "INTEGRAL_OPERATIONS_CATEGORY_DESC";

   @RBEntry("Raccoglitore di aggiunta a progetto")
   public static final String PRIVATE_CONSTANT_20 = "ADD_TO_PROJECT_COLLECTOR_CATEGORY";

   @RBEntry("Valori regola Raccoglitore di aggiunta a progetto")
   public static final String PRIVATE_CONSTANT_21 = "ADD_TO_PROJECT_COLLECTOR_CATEGORY_DESC";

   @RBEntry("Raccoglitore di spostamento")
   public static final String PRIVATE_CONSTANT_22 = "MOVE_COLLECTOR_CATEGORY";

   @RBEntry("Valori regola Raccoglitore di spostamento")
   public static final String PRIVATE_CONSTANT_23 = "MOVE_COLLECTOR_CATEGORY_DESC";

   @RBEntry("Raccoglitore di invio a PDM")
   public static final String PRIVATE_CONSTANT_24 = "SEND_TO_PDM_COLLECTOR_CATEGORY";

   @RBEntry("Valori regola Raccoglitore di invio a PDM")
   public static final String PRIVATE_CONSTANT_25 = "SEND_TO_PDM_COLLECTOR_CATEGORY_DESC";

   @RBEntry("Raccoglitore di conversione in oggetto condiviso")
   public static final String PRIVATE_CONSTANT_26 = "CONVERT_TO_SHARE_COLLECTOR_CATEGORY";

   @RBEntry("Valori regola Raccoglitore di conversione in oggetto condiviso")
   public static final String PRIVATE_CONSTANT_27 = "CONVERT_TO_SHARE_COLLECTOR_CATEGORY_DESC";

   @RBEntry("Raccoglitore di aggiornamento progetto")
   public static final String UPDATE_PROJECT_COLLECTOR_CATEGORY = "UPDATE_PROJECT_COLLECTOR_CATEGORY";

   @RBEntry("Valori regola Raccoglitore di aggiornamento progetto")
   public static final String UPDATE_PROJECT_COLLECTOR_CATEGORY_DESC = "UPDATE_PROJECT_COLLECTOR_CATEGORY_DESC";

   /**
    * Test Collector category used for test type of Collector preferences
    **/
   @RBEntry("Raccoglitore di test")
   public static final String PRIVATE_CONSTANT_28 = "TEST_COLLECTOR_CATEGORY";

   @RBEntry("Valori regola Raccoglitore di test")
   public static final String PRIVATE_CONSTANT_29 = "TEST_COLLECTOR_CATEGORY_DESC";

   @RBEntry("Raccoglitore componente raccolta")
   public static final String PRIVATE_CONSTANT_30 = "TEST_COLLECTION_COMPONENT_COLLECTOR_CATEGORY";

   @RBEntry("Valori regola Raccoglitore componente raccolta")
   public static final String PRIVATE_CONSTANT_31 = "TEST_COLLECTION_COMPONENT_COLLECTOR_CATEGORY_DESC";

   @RBEntry("Raccoglitore finestra selettore raccolta")
   public static final String PRIVATE_CONSTANT_32 = "TEST_COLLECTION_PICKER_DIALOG_COLLECTOR_CATEGORY";

   @RBEntry("Valori regola Raccoglitore finestra selettore raccolta")
   public static final String PRIVATE_CONSTANT_33 = "TEST_COLLECTION_PICKER_DIALOG_COLLECTOR_CATEGORY_DESC";

   @RBEntry("Raccoglitore raccolta semplice")
   public static final String PRIVATE_CONSTANT_34 = "SIMPLE_TEST_COLLECTION_COLLECTOR_CATEGORY";

   @RBEntry("Valori regola Raccoglitore raccolta semplice")
   public static final String PRIVATE_CONSTANT_35 = "SIMPLE_TEST_COLLECTION_COLLECTOR_CATEGORY_DESC";

   @RBEntry("Appunti")
   public static final String PREFERENCE_CLIPBOARD_CATEGORY = "PREFERENCE_CLIPBOARD_CATEGORY";

   @RBEntry("Rimuovi dopo Incolla")
   public static final String CLIPBOARD_REMOVE_ON_PASTE_PREF_DISPLAYNAME = "CLIPBOARD_REMOVE_ON_PASTE_PREF_DISPLAYNAME";

   @RBEntry("Stabilisce se gli elementi vengono o meno rimossi dagli Appunti quando vengono incollati")
   public static final String CLIPBOARD_REMOVE_ON_PASTE_PREF_DESCRIPTION = "CLIPBOARD_REMOVE_ON_PASTE_PREF_DESCRIPTION";

   @RBEntry("Stabilisce se gli elementi vengono o meno rimossi dagli Appunti quando vengono incollati")
   public static final String CLIPBOARD_REMOVE_ON_PASTE_PREF_LONG_DESCRIPTION = "CLIPBOARD_REMOVE_ON_PASTE_PREF_LONG_DESCRIPTION";

   @RBEntry("Mostra miniatura")
   public static final String CLIPBOARD_SHOW_THUMBNAIL_PREF_DISPLAYNAME = "CLIPBOARD_SHOW_THUMBNAIL_PREF_DISPLAYNAME";

   @RBEntry("Sceglie se visualizzare o meno una colonna di miniature negli Appunti")
   public static final String CLIPBOARD_SHOW_THUMBNAIL_PREF_DESCRIPTION = "CLIPBOARD_SHOW_THUMBNAIL_PREF_DESCRIPTION";

   @RBEntry("Sceglie se visualizzare o meno una colonna di miniature negli Appunti")
   public static final String CLIPBOARD_SHOW_THUMBNAIL_PREF_LONG_DESCRIPTION = "CLIPBOARD_SHOW_THUMBNAIL_PREF_LONG_DESCRIPTION";

   @RBEntry("La definizione preferenza {0} non esiste.")
   @RBArgComment0("The string name of the Prefenence Definition object which does not exist.")
   public static final String PREFERENCE_ERROR_NO_PREF_DEFINITION = "PREFERENCE_ERROR_NO_PREF_DEFINITION";

   @RBEntry("Per visualizzare correttamente la preferenza è necessario che il campo \"{0}\" sia presente.")
   @RBArgComment0("The string name of the field missing from a definition in a preference load file.")
   public static final String PREFERENCE_LOAD_ERROR_MISSING_VALUE = "PREFERENCE_LOAD_ERROR_MISSING_VALUE";

   @RBEntry("Sottotipi di parti esclusi")
   @RBComment("Display name for preference /com/ptc/windchill/enterprise/search/excludes/wt.part.WTPart")
   public static final String PRIVATE_CONSTANT_36 = "EXCLUDE_PREFERENCE_NAME";

   @RBEntry("Configura i sottotipi parte che saranno esclusi dalle ricerche della parte")
   @RBComment("For preference /com/ptc/windchill/enterprise/search/excludes/wt.part.WTPart")
   public static final String PRIVATE_CONSTANT_37 = "EXCLUDE_PREFERENCE_SHORT_DESC";

   @RBEntry("Configura i sottotipi parte che saranno esclusi dalle ricerche della parte")
   @RBComment("For preference /com/ptc/windchill/enterprise/search/excludes/wt.part.WTPart")
   public static final String PRIVATE_CONSTANT_38 = "EXCLUDE_PREFERENCE_DESC";

   @RBEntry("Sottotipi di parti esclusi")
   @RBComment("Display label for preference /com/ptc/windchill/enterprise/search/excludes/wt.part.WTPart")
   public static final String PRIVATE_CONSTANT_39 = "EXCLUDE_PREFERENCE_LABEL";

   @RBEntry("Nessuno")
   @RBComment("Option for preference value. No subtypes excluded")
   public static final String PRIVATE_CONSTANT_40 = "EXCLUDE_PREFERENCE_NONE";

   @RBEntry("Valore non valido: \"{0}\".")
   @RBComment("Generic message to display when an inputted preference value is invalid, but there is no specific message returned from the preference value handler.")
   public static final String INVALID_VALUE = "INVALID_VALUE";

   /**
    * Add to baseline category used for Add to baseline action related preferences
    **/
   @RBEntry("Aggiungi a baseline")
   public static final String PRIVATE_CONSTANT_41 = "ADDTOBASELINE_CATEGORY_CAT_NAME";

   @RBEntry("Preferenze per l'operazione Aggiungi a baseline")
   public static final String PRIVATE_CONSTANT_42 = "ADDTOBASELINE_CATEGORY_CAT_DESCR";

   @RBEntry("Raccoglitore")
   public static final String PRIVATE_CONSTANT_43 = "ADDTOBASELINE_COLLECTOR_CATEGORY";

   @RBEntry("Valori regola Raccoglitore di aggiunta a baseline")
   public static final String PRIVATE_CONSTANT_44 = "ADDTOBASELINE_COLLECTOR_CATEGORY_DESC";

   /**
    * Display Related Manufacturing Items category used for Display Related Manufacturing Items action related preferences
    **/
   @RBEntry("Visualizza oggetti di fabbricazione associati")
   public static final String PRIVATE_CONSTANT_45 = "DISPLAYRELATEDMANUFACTURINGITEMS_CATEGORY_CAT_NAME";

   @RBEntry("Preferenze per il report Visualizza oggetti di fabbricazione associati")
   public static final String PRIVATE_CONSTANT_46 = "DISPLAYRELATEDMANUFACTURINGITEMS_CATEGORY_CAT_DESCR";

   @RBEntry("Raccoglitore")
   public static final String PRIVATE_CONSTANT_47 = "DISPLAYRELATEDMANUFACTURINGITEMS_COLLECTOR_CATEGORY";

   @RBEntry("Valori regola Raccoglitore visualizzazione oggetti di fabbricazione associati")
   public static final String PRIVATE_CONSTANT_48 = "DISPLAYRELATEDMANUFACTURINGITEMS_COLLECTOR_CATEGORY_DESC";

   @RBEntry("ATTENZIONE: le preferenze obbligatorie non possono avere valori non definiti.")
   @RBComment("The user must fill a value other than undefined before moving on to the next step or completing the action.")
   public static final String UNDEFINED_VALUE = "UNDEFINED_VALUE";

   @RBEntry("Visualizzazione")
   @RBComment("Default Role Type for Preference Category")
   public static final String DISPLAY = "DISPLAY";

   @RBEntry("Scaricamento")
   @RBComment("Download Role Type for preference category")
   public static final String DOWNLOAD = "DOWNLOAD";

   @RBEntry("ATTENZIONE: l'utente non dispone dell'autorizzazione necessaria per questa operazione. Contattare l'amministratore se questo messaggio viene visualizzato erroneamente.")
   @RBComment("NotAuthorizedException: The user does not have the required permission to access an object.")
   public static final String NOT_AUTHORIZED_EXCEPTION = "NOT_AUTHORIZED_EXCEPTION";

   /**
    * Meeting category used for
    */
   @RBEntry("Meeting")
   public static final String MEETING_CAT_NAME = "MEETING_CAT_NAME";


}
