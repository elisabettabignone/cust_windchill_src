/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.note;

import wt.util.resource.*;

@RBUUID("wt.note.noteResource")
public final class noteResource_it extends WTListResourceBundle {
   @RBEntry("Errore, nome di modello di nota duplicato {0} non consentito nello stesso contesto.")
   @RBArgComment0("Note Template Name.")
   public static final String DUPNAME_PROHIBITED = "note.TemplateName";
}
