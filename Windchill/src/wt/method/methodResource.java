/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.method;

import wt.util.resource.*;@RBUUID("wt.method.methodResource")
public final class methodResource extends WTListResourceBundle {
   @RBEntry("nested exception is {0}")
   @RBArgComment0(" refers to the nested exception")
   public static final String NESTED_EXCEPTION = "0";

   @RBEntry("No active method context")
   public static final String NO_METHOD_CONTEXT = "1";

   @RBEntry("Unable to instantiate POM handler")
   public static final String NO_INSTANTIATE_POM_HANDLER = "2";

   @RBEntry("interrupt access denied")
   public static final String INTERRUPT_ACCESS_DENIED = "3";

   @RBEntry("Calling thread was interrupted")
   public static final String CALL_THREAD_INTERRUPT = "4";

   @RBEntry("{0} does not implement wt.method.RemoteAccess")
   @RBArgComment0(" refers to a class name.")
   public static final String NO_REMOTE_ACCESS_IMPLEMENT = "5";

   @RBEntry("access denied")
   public static final String ACCESS_DENIED = "6";

   @RBEntry("Method failed")
   public static final String METHOD_FAILED = "7";

   @RBEntry("Invocation failed")
   public static final String INVOCATION_FAILED = "8";

   @RBEntry("Server exception")
   public static final String SERVER_EXCEPTION = "9";

   @RBEntry("Unable to invoke remote method")
   public static final String NO_INVOKE_REMOTE_METHOD = "10";

   @RBEntry("Unable to get method server info")
   public static final String UNABLE_GET_SERVER_INFO = "11";

   @RBEntry("Unable to locate method server")
   public static final String CANNOT_LOCATE_METHOD_SERVER = "12";

   @RBEntry("ping failed")
   public static final String PING_FAILED = "13";

   @RBEntry("Maximum active method context threshold exceeded")
   public static final String METHOD_CONTEXT_MONITOR_NOTIF_INFO_DESCRIPTION = "14";

   @RBEntry("Threshold of average active method contexts exceeded; average active contexts: {0,number,0.####}")
   public static final String AVG_ACTIVE_CTXS_TE_NOTIF_MSG = "15";

   @RBEntry("Cannot capture method contexts while MethodContextMonitorMBean is stopped or stopping")
   public static final String CANNOT_CAPTURE_CONTEXTS_WHILE_NOT_STARTED = "16";

   @RBEntry("Method context statistics data")
   public static final String CONTEXT_STATS_DATA_TYPE_DESCR = "17";

   @RBEntry("Active method contexts at start of measurement interval")
   public static final String ACT_CTXTS_START_ITEM_DESCR = "18";

   @RBEntry("Active method contexts at end of measurement interval")
   public static final String ACT_CTXTS_END_ITEM_DESCR = "19";

   @RBEntry("Average active method contexts during measurement interval")
   public static final String ACT_CTXTS_AVG_ITEM_DESCR = "20";

   @RBEntry("Maximum active method contexts during measurement interval")
   public static final String ACT_CTXTS_MAX_ITEM_DESCR = "21";

   @RBEntry("Number of method contexts which completed during measurement interval")
   public static final String COMPLETED_CTXTS_ITEM_DESCR = "22";

   @RBEntry("Number of method contexts resulting in errors during measurement interval")
   public static final String ERROR_COUNT_ITEM_DESCR = "23";

   @RBEntry("Method contexts per second during measurement interval")
   public static final String CTXTS_PER_SEC_ITEM_DESCR = "24";

   @RBEntry("Average method context duration during measurement interval (in seconds)")
   public static final String CTXT_SECS_AVG_ITEM_DESCR = "25";

   @RBEntry("Maximum method context duration during measurement interval (in seconds)")
   public static final String CTXT_SECS_MAX_ITEM_DESCR = "26";

   @RBEntry("Average overall CPU time consumed by each method context during measurement interval (in seconds)")
   public static final String CTXT_CPU_SECS_AVG_ITEM_DESCR = "27";

   @RBEntry("Average user CPU time consumed by each method context during measurement interval (in seconds)")
   public static final String CTXT_USER_SECS_AVG_ITEM_DESCR = "28";

   @RBEntry("Average number of JDBC calls per method context during measurement interval")
   public static final String AVG_JDBC_CALLS_PER_CTXT_ITEM_DESCR = "29";

   @RBEntry("Average number of JNDI calls per method context during measurement interval")
   public static final String AVG_JNDI_CALLS_PER_CTXT_ITEM_DESCR = "30";

   @RBEntry("Percentage of overall method context time spent in JDBC calls during measurement interval")
   public static final String PERC_CTXT_TIME_IN_JDBC_ITEM_DESCR = "31";

   @RBEntry("Percentage of overall method context time spent in JNDI calls during measurement interval")
   public static final String PERC_CTXT_TIME_IN_JNDI_ITEM_DESCR = "32";

   @RBEntry("Raw aggregate method context monitoring data")
   public static final String CONTEXT_RAW_DATA_TYPE_DESCR = "33";

   @RBEntry("Time data was acquired measured as number of milliseconds since January 1, 1970, 00:00:00 GMT (as in java.util.Date)")
   public static final String TIME_MILLIS_ITEM_DESCR = "34";

   @RBEntry("Total duration of all method contexts in this process")
   public static final String TOTAL_ACTIVE_CTXT_SECS_ITEM_DESCR = "35";

   @RBEntry("Total number of method contexts completed in this process")
   public static final String TOTAL_COMPLETED_CTXTS_ITEM_DESCR = "36";

   @RBEntry("Total method contexts resulting in errors in this process")
   public static final String TOTAL_ERROR_COUNT_ITEM_DESCR = "37";

   @RBEntry("Total overall CPU consumed by completed method contexts in this process")
   public static final String TOTAL_CTXT_CPU_SECS_ITEM_DESCR = "38";

   @RBEntry("Total duration of all completed method contexts in this process")
   public static final String TOTAL_CTXT_SECS_ITEM_DESCR = "39";

   @RBEntry("Total user CPU consumed by completed method contexts in this process")
   public static final String TOTAL_CTXT_USER_SECS_ITEM_DESCR = "40";

   @RBEntry("Total JDBC calls made by method contexts in this process")
   public static final String TOTAL_JDBC_CALLS_ITEM_DESCR = "41";

   @RBEntry("Total time method contexts have spent in JDBC in this process")
   public static final String TOTAL_JDBC_SECS_ITEM_DESCR = "42";

   @RBEntry("Total JNDI calls made by method contexts in this process")
   public static final String TOTAL_JNDI_CALLS_ITEM_DESCR = "43";

   @RBEntry("Total time method contexts have spent in JNDI in this process")
   public static final String TOTAL_JNDI_SECS_ITEM_DESCR = "44";

   @RBEntry("Total time contexts have spent waiting to obtain JDBC connections in this process")
   public static final String TOTAL_JDBC_WAIT_SECS_ITEM_DESCR = "45";

   @RBEntry("Percentage of overall method context time spent waiting to obtain JDBC connections during measurement interval")
   public static final String PERC_CTXT_TIME_IN_JDBC_WAIT_ITEM_DESCR = "46";

   @RBEntry("Average time each method context spent waiting for a JDBC connection during measurement interval")
   public static final String CTXT_JDBC_WAIT_SECS_AVG_ITEM_DESCR = "47";

   @RBEntry("Number of method contexts that were redirected to other method servers during measurement interval")
   public static final String REDIRECT_COUNT_ITEM_DESCR = "48";

   @RBEntry("Total method contexts redirected in this process")
   public static final String TOTAL_REDIRECT_COUNT_ITEM_DESCR = "49";

   @RBEntry("Time data was acquired measured in nanoseconds (as per System.nanoTime())")
   public static final String TIME_NANOS_ITEM_DESCR = "50";

   @RBEntry("Total JDBC calls made by method contexts in this process")
   public static final String TOTAL_FG_JDBC_CALLS_ITEM_DESCR = "51";

   @RBEntry("Total foreground JNDI calls made by method contexts in this process")
   public static final String TOTAL_FG_JNDI_CALLS_ITEM_DESCR = "52";

   @RBEntry("Total number of foreground method contexts completed in this process")
   public static final String TOTAL_FG_CTXTS_ITEM_DESCR = "53";

   @RBEntry("Average number of remote cache calls per method context during measurement interval")
   public static final String AVG_CACHE_CALLS_PER_CTXT_ITEM_DESCR = "54";

   @RBEntry("Percentage of overall method context time spent in remote cache calls during measurement interval")
   public static final String PERC_CTXT_TIME_IN_CACHE_CALLS_ITEM_DESCR = "55";

   @RBEntry("Total remote cache calls made by method contexts in this process")
   public static final String TOTAL_CACHE_CALLS_ITEM_DESCR = "56";

   @RBEntry("Total time method contexts have spent in remote cache calls in this process")
   public static final String TOTAL_CACHE_SECS_ITEM_DESCR = "57";

   @RBEntry("Remote cache call data for a specific cache and operation")
   public static final String REMOTE_CACHE_DATA_ENTRY_TYPE_DESCR = "58";

   @RBEntry("Name of remote cache server")
   public static final String REMOTE_CACHE_CACHE_NAME_ITEM_DESCR = "59";

   @RBEntry("Name of remote cache operation")
   public static final String REMOTE_CACHE_OPER_NAME_ITEM_DESCR = "60";

   @RBEntry("Number of remote cache calls")
   public static final String REMOTE_CACHE_CALLS_ITEM_DESCR = "61";

   @RBEntry("Elapsed time (in seconds) of remote cache calls")
   public static final String REMOTE_CACHE_ELAPSED_SECS_ITEM_DESCR = "62";

   @RBEntry("Remote cache call data")
   public static final String REMOTE_CACHE_DATA_TYPE_DESCR = "63";

   @RBEntry("Total times context threads were in BLOCKED state")
   public static final String TOTAL_BLOCKED_COUNT_ITEM_DESCR = "64";

   @RBEntry("Total seconds context threads spent in BLOCKED state; 0 unless thread contention monitoring is enabled")
   public static final String TOTAL_BLOCKED_SECONDS_ITEM_DESCR = "65";

   @RBEntry("Total times context threads were in WAITING or TIMED_WAITING state")
   public static final String TOTAL_WAITED_COUNT_ITEM_DESCR = "66";

   @RBEntry("Total seconds context threads spent in WAITING or TIMED_WAITING state; 0 unless thread contention monitoring is enabled")
   public static final String TOTAL_WAITED_SECONDS_ITEM_DESCR = "67";

   @RBEntry("Percentage of context time spent in BLOCKED state; 0 unless thread contention monitoring is enabled")
   public static final String PERC_CTXT_TIME_BLOCKED_ITEM_DESCR = "68";

   @RBEntry("Percentage of context time spent in WAITING or TIMED_WAITING state; 0 unless thread contention monitoring is enabled")
   public static final String PERC_CTXT_TIME_WAITED_ITEM_DESCR = "69";

   @RBEntry("Average times (per context) contexts were in BLOCKED state")
   public static final String AVG_BLOCKED_COUNT_PER_CTXT_ITEM_DESCR = "70";

   @RBEntry("Average times (per context) contexts were in WAITING or TIMED_WAITING state")
   public static final String AVG_WAITED_COUNT_PER_CTXT_ITEM_DESCR = "71";
}
