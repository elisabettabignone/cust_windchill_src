/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.method;

import wt.util.resource.*;@RBUUID("wt.method.methodResource")
public final class methodResource_it extends WTListResourceBundle {
   @RBEntry("eccezione annidata {0}")
   @RBArgComment0(" refers to the nested exception")
   public static final String NESTED_EXCEPTION = "0";

   @RBEntry("Nessun contesto di metodo attivo")
   public static final String NO_METHOD_CONTEXT = "1";

   @RBEntry("Impossibile creare istanze di gestore POM")
   public static final String NO_INSTANTIATE_POM_HANDLER = "2";

   @RBEntry("accesso di interruzione negato")
   public static final String INTERRUPT_ACCESS_DENIED = "3";

   @RBEntry("Thread chiamante interrotto")
   public static final String CALL_THREAD_INTERRUPT = "4";

   @RBEntry("{0} non implementa wt.method.RemoteAccess")
   @RBArgComment0(" refers to a class name.")
   public static final String NO_REMOTE_ACCESS_IMPLEMENT = "5";

   @RBEntry("accesso negato")
   public static final String ACCESS_DENIED = "6";

   @RBEntry("Metodo non riuscito")
   public static final String METHOD_FAILED = "7";

   @RBEntry("Chiamata non riuscita")
   public static final String INVOCATION_FAILED = "8";

   @RBEntry("Eccezione di server")
   public static final String SERVER_EXCEPTION = "9";

   @RBEntry("Impossibile richiamare metodo remoto")
   public static final String NO_INVOKE_REMOTE_METHOD = "10";

   @RBEntry("Impossibile ottenere informazioni sul method server")
   public static final String UNABLE_GET_SERVER_INFO = "11";

   @RBEntry("Impossibile individuare il method server")
   public static final String CANNOT_LOCATE_METHOD_SERVER = "12";

   @RBEntry("ping non riuscito")
   public static final String PING_FAILED = "13";

   @RBEntry("Soglia massima contesti di metodo attivi superata")
   public static final String METHOD_CONTEXT_MONITOR_NOTIF_INFO_DESCRIPTION = "14";

   @RBEntry("Soglia numero medio dei contesti di metodo attivi superata; numero medio contesti attivi: {0,number,0.####}")
   public static final String AVG_ACTIVE_CTXS_TE_NOTIF_MSG = "15";

   @RBEntry("Impossibile catturare i contesti di metodo mentre MethodContextMonitorMBean è arrestato o in fase di arresto")
   public static final String CANNOT_CAPTURE_CONTEXTS_WHILE_NOT_STARTED = "16";

   @RBEntry("Dati statistici contesto di metodo")
   public static final String CONTEXT_STATS_DATA_TYPE_DESCR = "17";

   @RBEntry("Contesti di metodo attivi all'inizio dell'intervallo di misurazione")
   public static final String ACT_CTXTS_START_ITEM_DESCR = "18";

   @RBEntry("Contesti di metodo attivi alla fine dell'intervallo di misurazione")
   public static final String ACT_CTXTS_END_ITEM_DESCR = "19";

   @RBEntry("Numero medio contesti di metodo attivi durante l'intervallo di misurazione")
   public static final String ACT_CTXTS_AVG_ITEM_DESCR = "20";

   @RBEntry("Numero massimo contesti di metodo attivi durante l'intervallo di misurazione")
   public static final String ACT_CTXTS_MAX_ITEM_DESCR = "21";

   @RBEntry("Contesti di metodo completati durante l'intervallo di misurazione")
   public static final String COMPLETED_CTXTS_ITEM_DESCR = "22";

   @RBEntry("Contesti di metodo che hanno restituito errori durante l'intervallo di misurazione")
   public static final String ERROR_COUNT_ITEM_DESCR = "23";

   @RBEntry("Contesti di metodo per secondo durante l'intervallo di misurazione")
   public static final String CTXTS_PER_SEC_ITEM_DESCR = "24";

   @RBEntry("Durata media contesti di metodo durante l'intervallo di misurazione")
   public static final String CTXT_SECS_AVG_ITEM_DESCR = "25";

   @RBEntry("Durata massima contesti di metodo durante l'intervallo di misurazione")
   public static final String CTXT_SECS_MAX_ITEM_DESCR = "26";

   @RBEntry("Tempo CPU totale impiegato in media da ciascun contesto di metodo durante l'intervallo di misurazione (in secondi)")
   public static final String CTXT_CPU_SECS_AVG_ITEM_DESCR = "27";

   @RBEntry("Tempo CPU utente impiegato in media da ciascun contesto di metodo durante l'intervallo di misurazione (in secondi)")
   public static final String CTXT_USER_SECS_AVG_ITEM_DESCR = "28";

   @RBEntry("Numero medio chiamate JDBC per contesto di metodo durante l'intervallo di misurazione")
   public static final String AVG_JDBC_CALLS_PER_CTXT_ITEM_DESCR = "29";

   @RBEntry("Numero medio chiamate JNDI per contesto di metodo durante l'intervallo di misurazione")
   public static final String AVG_JNDI_CALLS_PER_CTXT_ITEM_DESCR = "30";

   @RBEntry("Percentuale di tempo totale dei contesti di metodo impiegato per chiamate JDBC durante l'intervallo di misurazione")
   public static final String PERC_CTXT_TIME_IN_JDBC_ITEM_DESCR = "31";

   @RBEntry("Percentuale di tempo totale dei contesti di metodo impiegato per chiamate JNDI durante l'intervallo di misurazione")
   public static final String PERC_CTXT_TIME_IN_JNDI_ITEM_DESCR = "32";

   @RBEntry("Dati aggregati non elaborati di monitoraggio contesti di metodo")
   public static final String CONTEXT_RAW_DATA_TYPE_DESCR = "33";

   @RBEntry("I dati relativi al tempo sono stati acquisiti misurando i millisecondi trascorsi dal 1 gennaio 1970, 00:00:00 ora di Greenwich (come in java.util.Date)")
   public static final String TIME_MILLIS_ITEM_DESCR = "34";

   @RBEntry("Durata totale di tutti i contesti di metodo in questo processo")
   public static final String TOTAL_ACTIVE_CTXT_SECS_ITEM_DESCR = "35";

   @RBEntry("Totale dei contesti di metodo completati in questo processo")
   public static final String TOTAL_COMPLETED_CTXTS_ITEM_DESCR = "36";

   @RBEntry("Totale dei contesti di metodo che hanno restituito errori durante questo processo")
   public static final String TOTAL_ERROR_COUNT_ITEM_DESCR = "37";

   @RBEntry("Tempo di CPU totale impiegato dai i contesti di metodo completati in questo processo")
   public static final String TOTAL_CTXT_CPU_SECS_ITEM_DESCR = "38";

   @RBEntry("Durata totale di tutti i contesti di metodo completati in questo processo")
   public static final String TOTAL_CTXT_SECS_ITEM_DESCR = "39";

   @RBEntry("Tempo di CPU utente totale impiegato dai i contesti di metodo completati in questo processo")
   public static final String TOTAL_CTXT_USER_SECS_ITEM_DESCR = "40";

   @RBEntry("Totale delle chiamate JDBC effettuate dai contesti di metodo in questo processo")
   public static final String TOTAL_JDBC_CALLS_ITEM_DESCR = "41";

   @RBEntry("Tempo totale trascorso dai contesti di metodo in attività JDBC in questo processo")
   public static final String TOTAL_JDBC_SECS_ITEM_DESCR = "42";

   @RBEntry("Totale delle chiamate JNDI effettuate dai contesti di metodo in questo processo")
   public static final String TOTAL_JNDI_CALLS_ITEM_DESCR = "43";

   @RBEntry("Tempo totale trascorso dai contesti di metodo in attività JNDI in questo processo")
   public static final String TOTAL_JNDI_SECS_ITEM_DESCR = "44";

   @RBEntry("Tempo totale trascorso dai contesti in attesa di ottenere connessioni JDBC in questo processo")
   public static final String TOTAL_JDBC_WAIT_SECS_ITEM_DESCR = "45";

   @RBEntry("Percentuale di tempo totale dei contesti di metodo trascorso in attesa di connessioni JDBC in questo processo")
   public static final String PERC_CTXT_TIME_IN_JDBC_WAIT_ITEM_DESCR = "46";

   @RBEntry("Tempo medio trascorso da ciascun contesto di metodo in attesa di una connessione JDBC durante l'intervallo di misurazione")
   public static final String CTXT_JDBC_WAIT_SECS_AVG_ITEM_DESCR = "47";

   @RBEntry("Numero di contesti di metodo reindirizzati ad altri method server durante l'intervallo di misurazione.")
   public static final String REDIRECT_COUNT_ITEM_DESCR = "48";

   @RBEntry("Totale dei contesti di metodo reindirizzati in questo processo")
   public static final String TOTAL_REDIRECT_COUNT_ITEM_DESCR = "49";

   @RBEntry("I dati relativi al tempo sono stati acquisiti in nanosecondi (in base a System.nanoTime())")
   public static final String TIME_NANOS_ITEM_DESCR = "50";

   @RBEntry("Totale delle chiamate JDBC effettuate dai contesti di metodo in questo processo")
   public static final String TOTAL_FG_JDBC_CALLS_ITEM_DESCR = "51";

   @RBEntry("Totale delle chiamate JNDI in primo piano effettuate dai contesti di metodo in questo processo")
   public static final String TOTAL_FG_JNDI_CALLS_ITEM_DESCR = "52";

   @RBEntry("Totale dei contesti di metodo in primo piano completati in questo processo")
   public static final String TOTAL_FG_CTXTS_ITEM_DESCR = "53";

   @RBEntry("Numero medio chiamate di cache remota per contesto di metodo durante l'intervallo di misurazione")
   public static final String AVG_CACHE_CALLS_PER_CTXT_ITEM_DESCR = "54";

   @RBEntry("Percentuale di tempo totale dei contesti di metodo impiegato per chiamate di cache remota durante l'intervallo di misurazione")
   public static final String PERC_CTXT_TIME_IN_CACHE_CALLS_ITEM_DESCR = "55";

   @RBEntry("Totale delle chiamate di cache remota effettuate dai contesti di metodo in questo processo")
   public static final String TOTAL_CACHE_CALLS_ITEM_DESCR = "56";

   @RBEntry("Tempo totale trascorso dai contesti di metodo in chiamate di cache remota in questo processo")
   public static final String TOTAL_CACHE_SECS_ITEM_DESCR = "57";

   @RBEntry("Dati chiamate di cache remota di un'operazione o cache specifica")
   public static final String REMOTE_CACHE_DATA_ENTRY_TYPE_DESCR = "58";

   @RBEntry("Nome server cache remota")
   public static final String REMOTE_CACHE_CACHE_NAME_ITEM_DESCR = "59";

   @RBEntry("Nome operazione cache remota")
   public static final String REMOTE_CACHE_OPER_NAME_ITEM_DESCR = "60";

   @RBEntry("Numero di chiamate di cache remota")
   public static final String REMOTE_CACHE_CALLS_ITEM_DESCR = "61";

   @RBEntry("Tempo trascorso (in secondi) delle chiamate di cache remota")
   public static final String REMOTE_CACHE_ELAPSED_SECS_ITEM_DESCR = "62";

   @RBEntry("Dati chiamate di cache remota")
   public static final String REMOTE_CACHE_DATA_TYPE_DESCR = "63";

   @RBEntry("Totale del numero di volte in cui i thread dei contesti sono stati in stato BLOCKED")
   public static final String TOTAL_BLOCKED_COUNT_ITEM_DESCR = "64";

   @RBEntry("Totale dei secondi trascorsi in stato BLOCKED da parte dei thread dei contesti; 0 a meno che non sia attivato il monitoraggio dei conflitti nei thread")
   public static final String TOTAL_BLOCKED_SECONDS_ITEM_DESCR = "65";

   @RBEntry("Totale del numero di volte in cui i thread dei contesti sono stati in stato WAITING o TIMED_WAITING")
   public static final String TOTAL_WAITED_COUNT_ITEM_DESCR = "66";

   @RBEntry("Totale dei secondi trascorsi in stato WAITING o TIMED_WAITING da parte dei thread dei contesti; 0 a meno che non sia attivato il monitoraggio dei conflitti nei thread")
   public static final String TOTAL_WAITED_SECONDS_ITEM_DESCR = "67";

   @RBEntry("Percentuale di tempo dei contesti trascorso in stato BLOCKED; 0 a meno che non sia attivato il monitoraggio dei conflitti nei thread")
   public static final String PERC_CTXT_TIME_BLOCKED_ITEM_DESCR = "68";

   @RBEntry("Percentuale di tempo dei contesti trascorso in stato WAITING o TIMED_WAITING; 0 a meno che non sia attivato il monitoraggio dei conflitti nei thread")
   public static final String PERC_CTXT_TIME_WAITED_ITEM_DESCR = "69";

   @RBEntry("Media del numero di volte (per contesto) in cui i contesti sono stati in stato BLOCKED")
   public static final String AVG_BLOCKED_COUNT_PER_CTXT_ITEM_DESCR = "70";

   @RBEntry("Media del numero di volte (per contesto) in cui i contesti sono stati in stato WAITING o TIMED_WAITING")
   public static final String AVG_WAITED_COUNT_PER_CTXT_ITEM_DESCR = "71";
}
