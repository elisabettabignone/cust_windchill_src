package wt.method.jmx;


import java.beans.DefaultPersistenceDelegate;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;

import wt.jmx.core.OverrideableBeanInfo;


public class  MCHistogrammerBeanInfo
  extends OverrideableBeanInfo
{
  public  MCHistogrammerBeanInfo()
    throws IntrospectionException
  {
    super( MCHistogrammer.class );

    // set up appropriate constructor and mark OwnerMBean property as transient for beans persistence

    wrappedBeanInfo.getBeanDescriptor().setValue( "persistenceDelegate", new DefaultPersistenceDelegate( new String[] { "name" } ) );

    /* We do not need to persist the OwnerMBean property with this MBean as the
      * parent will call setOwnerMBean.  We therefore mark this property as
      * transient to keep persistence file simpler, though this is by no means
      * necessary.
      */
    for ( PropertyDescriptor propDescr : wrappedBeanInfo.getPropertyDescriptors() )
    {
      final String  propName = propDescr.getName();
      if ( "OwnerMBean".equalsIgnoreCase( propName ) )
        propDescr.setValue( "transient", Boolean.TRUE );  // BEWARE: property getter or setter should be overriden in this class or you will impact base class behavior
    }
  }
}
