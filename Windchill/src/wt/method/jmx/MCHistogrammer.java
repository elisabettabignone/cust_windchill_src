/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.method.jmx;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.MBeanInfo;
import javax.management.NotCompliantMBeanException;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenDataException;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.TabularType;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;

import wt.jmx.core.AttributeListWrapper;
import wt.jmx.core.BaseObjectNamed;
import wt.jmx.core.MBeanUtilities;
import wt.jmx.core.PeriodicTaskSupport;
import wt.jmx.core.SelfAwareMBean;
import wt.jmx.core.mbeans.ContextListener;
import wt.jmx.core.mbeans.mbeansResource;
import wt.log4j.LogR;
import wt.method.MethodContext;
import wt.method.MethodContextMBean;
import wt.method.MethodContextMonitorMBean;
import wt.method.methodResource;


/* In addition to providing useful functionality, this class is intended to
 * serve as an example of a SelfAwareMBean ContextListener<MethodContext>
 * implementation -- to aid both developers and customizers in implementing such
 * classes.  This class also demonstrates the use of PeriodicTaskSupport, JMX
 * "open" data types, and tweaking JavaBeans persistence for a class.
 *
 * Even though source code for this class is provided, compilation of this
 * source under the existing class name (i.e. in place of the class delivered)
 * is still not supported.  Additionally, details in the actual implementation
 * of this class may change over time so one should not rely on such
 * implementation details.
 */


/** {@link wt.jmx.core.mbeans.ContextListener}&lt;{@link wt.method.MethodContext}&gt;
 *  MBean that produce histogram statistics of method contexts, grouping by target
 *  class and method.
 *  <p>
 *  To use this class specify the desired name for a listener instance and the
 *  full classname of this class to the addContextListener() operation of
 *  {@link wt.method.MethodContextMonitorMBean}.
 *
 *    <BR><BR><B>Supported API: </B>true
 *    <BR><BR><B>Extendable: </B>false
 */
public final class  MCHistogrammer
  extends SelfAwareMBean
  implements ContextListener<MethodContext>, MCHistogrammerMBean
{
  // Initial statics and static initializers

  // stash names of these classes in constants for re-use later
  private static final String  CLASSNAME = MCHistogrammer.class.getName();
  private static final String  LOGGER_CLASSNAME = Logger.class.getName();

  // stash ResourceBundle references in constants for re-use; all ResourceBundles obtained with server locale
  static final ResourceBundle  RESOURCE = ResourceBundle.getBundle( jmxResource.class.getName() );
  private static final ResourceBundle  METHOD_RESOURCE = ResourceBundle.getBundle( methodResource.class.getName() );
  private static final ResourceBundle  MB_RESOURCE = ResourceBundle.getBundle( mbeansResource.class.getName() );

  // Constructors

  /** Public constructor used by MethodContextMonitor MBean to pass name to
   *  instances.  Also used by {@link java.beans.XMLEncoder}-based persistence
   *  mechanism.
   */
  public  MCHistogrammer( final String name )
    throws NotCompliantMBeanException
  {
    super( MCHistogrammerMBean.class );
    this.name = name;
    logger = LogR.getLogger( CLASSNAME + '.' + name );
    structuredLogger = LogR.getLogger( CLASSNAME + ".structuredData." + name );
  }

  // SelfAwareMBean overrides

  // SelfAwareMBean method implementation
  @Override
  public String[][]  getObjectNameSuffix()
  {
    return (
      new String[][]
      {
        { "grouping", "Listeners" },
        { "name", name }
      }
    );
  }

  // SelfAwareMBean method override
  @Override
  public synchronized void  setOwnerMBean( final BaseObjectNamed ownerMBean )
  {
    super.setOwnerMBean( ownerMBean );
    monitor = (MethodContextMonitorMBean) ownerMBean;
  }

  // SelfAwareMBean method override
  @Override
  protected MBeanInfo  createMBeanInfo( MBeanInfo baseClassMBeanInfo )
  {
    // provide accurate type info for Data attribute
    final Map<String,? extends OpenType>  attrNameToTypeMap = Collections.singletonMap( "Data", STATS_DATA_TYPE );
    return ( MBeanUtilities.fixOpenMBeanAttrInfo( super.createMBeanInfo( baseClassMBeanInfo ), attrNameToTypeMap ) );
  }

  // SelfAwareMBean method override
  @Override
  protected void  onStart()
  {
    super.onStart();
    resetDataTask.start();
  }

  // SelfAwareMBean method override
  @Override
  protected void  onStop()
  {
    resetDataTask.stop();
    super.onStop();
  }

  // MCHistogrammerMBean implementation

  // MCHistogrammerMBean method implementation
  @Override
  public String  getName()
  {
    return ( name );
  }

  // MCHistogrammerMBean method implementation
  @Override
  public int  getAutoResetIntervalSeconds()
  {
    return ( resetDataTask.getTaskIntervalSeconds() );
  }

  // MCHistogrammerMBean method implementation
  @Override
  public void  setAutoResetIntervalSeconds( final int autoResetIntervalSeconds )
  {
    resetDataTask.setTaskIntervalSeconds( autoResetIntervalSeconds );
  }

  // MCHistogrammerMBean method implementation
  @Override
  public boolean  isLogOnReset()
  {
    return ( logOnReset );
  }

  // MCHistogrammerMBean method implementation
  @Override
  public void  setLogOnReset( final boolean logOnReset )
  {
    this.logOnReset = logOnReset;
  }

  // MCHistogrammerMBean method implementation
  @Override
  public String  getLoggerName()
  {
    return ( logger.getName() );
  }

  // RequestHistogrammerMBean method implementation
  @Override
  public String getStructuredDataLoggerName()
  {
    return ( structuredLogger.getName() );
  }

  // MCHistogrammerMBean method implementation
  @Override
  public CompositeData  getData()
    throws OpenDataException
  {
    return ( (new StatsData( data )).getCompositeData() );
  }

  // MCHistogrammerMBean method implementation
  @Override
  public String[]  getDataAsStrings()
  {
    return ( (new StatsData( data )).getDataAsStrings() );
  }

  // MCHistogrammerMBean method implementation
  @Override
  public void  resetData()
  {
    // reset data, obtaining previous data and time of reset in the process
    final Data  prevData;
    final long  nanoTime;
    masterDataResetLock.lock();  // only one reset at once and no overlaps with data updates in completed()
    try
    {
      prevData = this.data;
      final Data  newData = new Data();
      this.data = newData;
      nanoTime = newData.nanoTime;
    }
    finally
    {
      masterDataResetLock.unlock();
    }

    // log data as appropriate
    final boolean  logOnReset = this.logOnReset;
    final boolean  doStructuredLogging = structuredLogger.isInfoEnabled();
    if ( logOnReset || doStructuredLogging )
    {
      final StatsData  statsData = new StatsData( prevData, nanoTime );
      if ( doStructuredLogging )
        statsData.logStructuredData();
      if ( logOnReset )

        // only called when logOnReset is true, so we force logging at info level rather than requiring one to adjust the log level too
        logger.callAppenders( new LoggingEvent( LOGGER_CLASSNAME, logger, Level.INFO,
                                                getLineSeparatedString( statsData.getDataAsStrings() ), null ) );
    }
  }

  // ContextListener<MethodContext> implementation

  // ContextListener<MethodContext> method implementation
  @Override
  public void  started( final MethodContext methodContext )
  {
    // nothing we want to do here
  }

  // ContextListener<MethodContext> method implementation
  @Override
  public void  completed( final MethodContext methodContext )
  {
    // do what we can outside all locks
    final HistoBucket  key = new HistoBucket( methodContext );
    final BucketData  newBucketData = new BucketData( methodContext );

    masterDataUpdateLock.lock();  // allow different buckets to be update at the same time, but not overlapping with resetData()
    try
    {
      BucketData  oldBucketData = data.histoData.get( key );
      if ( oldBucketData == null )
        oldBucketData = data.histoData.putIfAbsent( key, newBucketData );
      if ( oldBucketData != null )
        oldBucketData.addDataFrom( newBucketData );
    }
    finally
    {
      masterDataUpdateLock.unlock();
    }
  }

  // Internal implementation

  /** Top-level data structure into which data on method contexts is accumulated
   */
  private final static class  Data
  {
    Data()
    {
      histoData = new ConcurrentHashMap<>( 128, 0.5f );  // start with a reasonable capacity and a moderately aggressive load factor
      nanoTime = System.nanoTime();
    }

    final long  nanoTime;
    final ConcurrentHashMap<HistoBucket,BucketData>  histoData;
  }

  /** Defines the histogram groupings/buckets into which data is collated and
   *  thus serves as a key in various maps.
   */
  private final static class  HistoBucket
    implements Comparable<HistoBucket>
  {
    HistoBucket( final MethodContext methodContext )
    {
      this( methodContext.getTargetClassName(), methodContext.getTargetMethodName() );
    }

    // attrMap is assumed to contain names and values of attributes from MethodContextMBean
    HistoBucket( final Map<String,Object> attrMap )
    {
      this( (String) attrMap.get( "TargetClass" ), (String) attrMap.get( "TargetMethod" ) );
    }

    private  HistoBucket( final String targetClass, final String targetMethod )
    {
      this.targetClass = targetClass;
      this.targetMethod = targetMethod;
      this.stringForm = ( ( targetClass != null ) ? targetClass + '.' + targetMethod
                                                  : "<" + RESOURCE.getString( jmxResource.OTHER_CONTEXTS ) + '>' );
    }

    @Override
    public boolean  equals( final Object obj )
    {
      if ( !( obj instanceof HistoBucket ) )
        return ( false );
      final HistoBucket  other = (HistoBucket) obj;
      return ( stringForm.equals( other.stringForm ) );
    }

    @Override
    public int  hashCode()
    {
      return ( stringForm.hashCode() );
    }

    @Override
    public String  toString()
    {
      return ( stringForm );
    }

    @Override
    public int  compareTo( final HistoBucket other )
    {
      return ( stringForm.compareTo( other.stringForm ) );
    }

    void  addAttrs( final AttributeList attrs )
    {
      attrs.add( new Attribute( "TargetClass", targetClass ) );
      attrs.add( new Attribute( "TargetMethod", targetMethod ) );
    }

    // openDataItemMap is to be used in constructing CompositeData of type HISTO_BUCKET_DATA_TYPE
    void  addItemsToMap( final Map<String,Object> openDataItemMap )
    {
      openDataItemMap.put( "targetClass", targetClass );
      openDataItemMap.put( "targetMethod", targetMethod );
    }

    final String  targetClass;
    final String  targetMethod;
    private final String  stringForm;
  }

  /** Defines raw per-bucket/grouping data for the histogram.
   */
  private static class  BucketData
  {
    BucketData()
    {
      // do nothing
    }

    BucketData( final MethodContext methodContext )
    {
      completedContexts = 1;
      final MethodContextMBean  mbean = methodContext.mbean;
      if ( mbean.isError() )
        errorCount = 1;
      if ( mbean.isRedirected() )
        redirectCount = 1;
      contextSeconds = mbean.getElapsedTotalSeconds();
      maxContextSeconds = contextSeconds;
      cpuSeconds = mbean.getElapsedTotalCpuSeconds();
      userSeconds = mbean.getElapsedTotalUserSeconds();
      blockedSeconds = mbean.getBlockedSeconds();
      waitedSeconds = mbean.getWaitedSeconds();
      jndiSeconds = mbean.getElapsedJNDISeconds();
      jdbcSeconds = mbean.getElapsedJDBCSeconds();
      jdbcConnWaitSeconds = mbean.getJDBCConnectionWaitTimeSeconds();
      remoteCacheSeconds = mbean.getElapsedRemoteCacheSeconds();
    }

    final synchronized void  addDataFrom( final BucketData otherData )  // can only allow one caller at a time here
    {
      completedContexts += otherData.completedContexts;
      errorCount += otherData.errorCount;
      redirectCount += otherData.redirectCount;
      contextSeconds += otherData.contextSeconds;
      final double  otherMaxContextSeconds = otherData.maxContextSeconds;  // copy field to local variable
      if ( otherMaxContextSeconds > maxContextSeconds )
        maxContextSeconds = otherMaxContextSeconds;
      cpuSeconds += otherData.cpuSeconds;
      userSeconds += otherData.userSeconds;
      blockedSeconds += otherData.blockedSeconds;
      waitedSeconds += otherData.waitedSeconds;
      jndiSeconds += otherData.jndiSeconds;
      jdbcSeconds += otherData.jdbcSeconds;
      jdbcConnWaitSeconds += otherData.jdbcConnWaitSeconds;
      remoteCacheSeconds += otherData.remoteCacheSeconds;
    }

    long  completedContexts;
    long  errorCount;
    long  redirectCount;
    double  contextSeconds;  // completed context seconds
    double  maxContextSeconds;
    double  cpuSeconds;
    double  userSeconds;
    double  blockedSeconds;
    double  waitedSeconds;
    double  jndiSeconds;
    double  jdbcSeconds;
    double  jdbcConnWaitSeconds;
    double  remoteCacheSeconds;
  }

  /** Collects, processes, and outputs final statistics from raw {@link Data}.
   */
  private final class  StatsData
  {
    StatsData( final Data data )
    {
      this( data, System.nanoTime() );
    }

    StatsData( final Data data, final long nowNanos )
    {
      final long  nowMillis = System.currentTimeMillis();

      histogram = new TreeMap<>();
      for ( Map.Entry<HistoBucket,BucketData> histoEntry : data.histoData.entrySet() )
        histogram.put( histoEntry.getKey(), new StatsDataBucket( histoEntry.getValue() ) );
      addCurrentContextData( histogram );  // merge in limited active/incomplete context data

      final long  elapsedNanos = nowNanos - data.nanoTime;
      final long  startMillis = nowMillis - ( elapsedNanos / 1000000 );
      startTime = new Date( startMillis );
      endTime = new Date( nowMillis );
      elapsedSeconds = elapsedNanos / 1.0e9;

      globalStats = new StatsDataBucket();
      for ( StatsDataBucket histoBucket : histogram.values() )
        globalStats.addDataFrom( histoBucket );

      for ( StatsDataBucket bucketData : histogram.values() )
        bucketData.process();
      globalStats.process();
    }

    void  logStructuredData()
    {
      logStructuredData( null, globalStats );
      for ( Map.Entry<HistoBucket,StatsDataBucket> histoEntry : histogram.entrySet() )
        logStructuredData( histoEntry.getKey(), histoEntry.getValue() );
    }

    /** Logs data in a structured form, specifically as an {@link AttributeListWrapper}, so that
     *  specialized log4j appenders and layouts like {@link wt.log4j.jmx.AsyncJDBCAppender} and
     *  {@link wt.log4j.jmx.TSVLayout} can retain the data in a structured, easily data-minable
     *  form.
     */
    private void  logStructuredData( final HistoBucket histoBucket, final StatsDataBucket statsBucket )
    {
      final AttributeList  attrs = new AttributeList( 17 );
      if ( histoBucket != null )
        histoBucket.addAttrs( attrs );
      else
        attrs.add( new Attribute( "OverallStats", Boolean.TRUE ) );
      attrs.add( new Attribute( "StartTime", startTime ) );
      attrs.add( new Attribute( "ElapsedSeconds", elapsedSeconds ) );
      statsBucket.addAttrs( attrs );
      // we already checked log level outside loop, so let's not check it again here
      structuredLogger.callAppenders( new LoggingEvent( LOGGER_CLASSNAME, structuredLogger, endTime.getTime(), Level.INFO,
                                                        AttributeListWrapper.newInstance( attrs ), null ) );
    }

    String[]  getDataAsStrings()
    {
      final ArrayList<String>  strings = new ArrayList<>();
      strings.add( formatDataMessage( jmxResource.START_TIME_MSG, startTime ) );
      strings.add( formatDataMessage( jmxResource.END_TIME_MSG, endTime ) );
      strings.add( formatDataMessage( jmxResource.ELAPSED_SECONDS_MSG, elapsedSeconds ) );
      addDataFor( strings, RESOURCE.getString( jmxResource.OVERALL_AGG_STATS ), globalStats );
      for ( Map.Entry<HistoBucket,StatsDataBucket> histoEntry : histogram.entrySet() )
        addDataFor( strings, histoEntry.getKey().toString(), histoEntry.getValue() );
      return ( strings.toArray( new String[strings.size()] ) );
    }

    private void  addDataFor( final List<String> strings, final String bucketName, final StatsDataBucket dataBucket )
    {
      strings.add( "" );
      strings.add( bucketName + ':' );
      dataBucket.addDataFor( strings );
    }

    CompositeData  getCompositeData()
      throws OpenDataException
    {
      final CompositeData  globalStatsCD;
      {
        final Map<String,Object>  globalStatsCDItems = MBeanUtilities.newHashMap( OVERALL_DATA_ITEMS.length, 0.75f );
        globalStats.addItemsToMap( globalStatsCDItems );
        checkItemMap( OVERALL_DATA_TYPE, globalStatsCDItems, OVERALL_DATA_ITEMS.length );
        globalStatsCD = new CompositeDataSupport( OVERALL_DATA_TYPE, globalStatsCDItems );
      }

      final TabularData  histogramTD = MBeanUtilities.newTabularDataSupport( HISTOGRAM_TYPE, histogram.size(), 0.75f );
      {
        for ( Map.Entry<HistoBucket,StatsDataBucket> histoEntry : histogram.entrySet() )
        {
          final Map<String,Object>  histoBucketCDItems = MBeanUtilities.newHashMap( HISTO_BUCKET_DATA_ITEMS.length, 0.75f );
          histoEntry.getKey().addItemsToMap( histoBucketCDItems );
          histoEntry.getValue().addItemsToMap( histoBucketCDItems );
          checkItemMap( HISTO_BUCKET_DATA_TYPE, histoBucketCDItems, HISTO_BUCKET_DATA_ITEMS.length );
          histogramTD.put( new CompositeDataSupport( HISTO_BUCKET_DATA_TYPE, histoBucketCDItems ) );
        }
      }

      final Map<String,Object>  statsDataCDItems = MBeanUtilities.newHashMap( STATS_DATA_ITEMS.length, 0.75f );
      statsDataCDItems.put( "startTime", startTime );
      statsDataCDItems.put( "endTime", endTime );
      statsDataCDItems.put( "elapsedSeconds", elapsedSeconds );
      statsDataCDItems.put( "overallStatistics", globalStatsCD );
      statsDataCDItems.put( "histogramStatistics", histogramTD );
      checkItemMap( STATS_DATA_TYPE, statsDataCDItems, STATS_DATA_ITEMS.length );
      return ( new CompositeDataSupport( STATS_DATA_TYPE, statsDataCDItems ) );
    }

    // log if number of map items will over or under populate the target CompositeType; we want to fully populate
    private void  checkItemMap( final CompositeType type, final Map<String,?> itemsMap, final int expectedItemCount )
    {
      final int  nItems = itemsMap.size();
      if ( nItems != expectedItemCount )
        logger.error( "Incorrect number of items for " + type.getTypeName() + "; was " + nItems +
                      ", should be " + expectedItemCount );
    }

    private final Date  startTime;
    private final Date  endTime;
    private final double  elapsedSeconds;
    private final StatsDataBucket  globalStats;
    private final Map<HistoBucket,StatsDataBucket>  histogram;
  }

  /** Extends {@link BucketData} to add processed statistics fields.  Also adds
   *  accmulator field for time spent in as of yet incompleted contexts -- to
   *  allow this data to be used downstream.
   */
  private static final class  StatsDataBucket
    extends BucketData
  {
    StatsDataBucket()
    {
      // do nothing
    }

    // attrMap is assumed to contain names and values of attributes from MethodContextMBean; only called for incomplete contexts
    StatsDataBucket( final Map<String,Object> partialAttrMap )
    {
      final double  mcSeconds = (Double) partialAttrMap.get( "ElapsedTotalSeconds" );
      incompleteContextSeconds = mcSeconds;
      maxContextSeconds = mcSeconds;
      blockedSeconds = (Double) partialAttrMap.get( "BlockedSeconds" );
      waitedSeconds = (Double) partialAttrMap.get( "WaitedSeconds" );
      jndiSeconds = (Double) partialAttrMap.get( "ElapsedJNDISeconds" );
      jdbcSeconds = (Double) partialAttrMap.get( "ElapsedJDBCSeconds" );
      jdbcConnWaitSeconds = (Double) partialAttrMap.get( "JDBCConnectionWaitTimeSeconds" );
      remoteCacheSeconds = (Double) partialAttrMap.get( "ElapsedRemoteCacheSeconds" );
    }

    // This method is only called with BucketData inputs, not subclasses thereof, so StatsDataBucket are not considered here
    StatsDataBucket( final BucketData otherData )
    {
      synchronized ( otherData )  // don't allow interference from addDataFrom() while copying (could allow concurrent copies, but does not seem worth a read/write lock here)
      {
        completedContexts = otherData.completedContexts;
        errorCount = otherData.errorCount;
        redirectCount = otherData.redirectCount;
        contextSeconds = otherData.contextSeconds;
        maxContextSeconds = otherData.maxContextSeconds;
        cpuSeconds = otherData.cpuSeconds;
        userSeconds = otherData.userSeconds;
        blockedSeconds = otherData.blockedSeconds;
        waitedSeconds = otherData.waitedSeconds;
        jndiSeconds = otherData.jndiSeconds;
        jdbcSeconds = otherData.jdbcSeconds;
        jdbcConnWaitSeconds = otherData.jdbcConnWaitSeconds;
        remoteCacheSeconds = otherData.remoteCacheSeconds;
      }
    }

    synchronized void  addDataFrom( final StatsDataBucket otherData )  // can only allow one caller at a time here
    {
      super.addDataFrom( (BucketData) otherData );
      incompleteContextSeconds += otherData.incompleteContextSeconds;
    }

    void  process()
    {
      avgContextSeconds = contextSeconds / completedContexts;
      avgCpuSeconds = cpuSeconds / completedContexts;
      avgUserSeconds = userSeconds / completedContexts;
      final double  totalContextSecondsDiv100 = ( contextSeconds + incompleteContextSeconds ) / 100.0;
      percTimeBlocked = blockedSeconds / totalContextSecondsDiv100;
      percTimeWaited = waitedSeconds / totalContextSecondsDiv100;
      percTimeInJndi = jndiSeconds / totalContextSecondsDiv100;
      percTimeInJdbc = jdbcSeconds / totalContextSecondsDiv100;
      percTimeInJdbcConnWait = jdbcConnWaitSeconds / totalContextSecondsDiv100;
      percTimeInRemoteCache = remoteCacheSeconds / totalContextSecondsDiv100;
    }

    void  addDataFor( final List<String> strings )
    {
      strings.add( formatDataMessage( jmxResource.COMPL_CONTEXTS_MSG, completedContexts ) );
      strings.add( formatDataMessage( jmxResource.ERRORS_MSG, errorCount ) );
      strings.add( formatDataMessage( jmxResource.REDIRECTS_MSG, redirectCount ) );
      strings.add( formatDataMessage( jmxResource.AVG_CONTEXT_SECONDS_MSG, avgContextSeconds ) );
      strings.add( formatDataMessage( jmxResource.MAX_CONTEXT_SECONDS_MSG, maxContextSeconds ) );
      strings.add( formatDataMessage( jmxResource.AVG_CPU_SECONDS_MSG, avgCpuSeconds ) );
      strings.add( formatDataMessage( jmxResource.AVG_USER_SECONDS_MSG, avgUserSeconds ) );
      strings.add( formatDataMessage( jmxResource.PERC_TIME_BLOCKED_MSG, percTimeBlocked ) );
      strings.add( formatDataMessage( jmxResource.PERC_TIME_WAITED_MSG, percTimeWaited ) );
      strings.add( formatDataMessage( jmxResource.PERC_TIME_IN_JDBC_MSG, percTimeInJdbc ) );
      strings.add( formatDataMessage( jmxResource.PERC_TIME_IN_JDBC_CONN_WAIT_MSG, percTimeInJdbcConnWait ) );
      strings.add( formatDataMessage( jmxResource.PERC_TIME_IN_JNDI_MSG, percTimeInJndi ) );
      strings.add( formatDataMessage( jmxResource.PERC_TIME_IN_REMOTE_CACHING_MSG, percTimeInRemoteCache ) );
    }

    void  addAttrs( final AttributeList attrs )
    {
      // besides being capitalized, some of the attribute names have been shortened here to be more amenable to database column naming restrictions
      attrs.add( new Attribute( "CompletedContexts", completedContexts ) );
      attrs.add( new Attribute( "ErrorCount", errorCount ) );
      attrs.add( new Attribute( "RedirectCount", redirectCount ) );
      attrs.add( new Attribute( "ContextSecondsAverage", avgContextSeconds ) );
      attrs.add( new Attribute( "ContextSecondsMax", maxContextSeconds ) );
      attrs.add( new Attribute( "ContextCpuSecondsAverage", avgCpuSeconds ) );
      attrs.add( new Attribute( "ContextUserSecondsAverage", avgUserSeconds ) );
      attrs.add( new Attribute( "PercContextTimeBlocked", percTimeBlocked ) );
      attrs.add( new Attribute( "PercContextTimeWaited", percTimeWaited ) );
      attrs.add( new Attribute( "PercContextTimeInJDBCCalls", percTimeInJdbc ) );
      attrs.add( new Attribute( "PercContextTimeInJDBCConnWait", percTimeInJdbcConnWait ) );
      attrs.add( new Attribute( "PercContextTimeInJNDICalls", percTimeInJndi ) );
      attrs.add( new Attribute( "PercContextTimeInRemoteCache", percTimeInRemoteCache ) );
    }

    // openDataItemMap is to be used in constructing CompositeData of type HISTO_BUCKET_DATA_TYPE or OVERALL_DATA_TYPE
    void  addItemsToMap( final Map<String,Object> openDataItemMap )
    {
      openDataItemMap.put( "completedContexts", completedContexts );
      openDataItemMap.put( "errorCount", errorCount );
      openDataItemMap.put( "redirectCount", redirectCount );
      openDataItemMap.put( "contextSecondsAverage", avgContextSeconds );
      openDataItemMap.put( "contextSecondsMax", maxContextSeconds );
      openDataItemMap.put( "contextCpuSecondsAverage", avgCpuSeconds );
      openDataItemMap.put( "contextUserSecondsAverage", avgUserSeconds );
      openDataItemMap.put( "percentageOfContextTimeBlocked", percTimeBlocked );
      openDataItemMap.put( "percentageOfContextTimeWaited", percTimeWaited );
      openDataItemMap.put( "percentageOfContextTimeInJDBCCalls", percTimeInJdbc );
      openDataItemMap.put( "percentageOfContextTimeInJDBCConnWait", percTimeInJdbcConnWait );
      openDataItemMap.put( "percentageOfContextTimeInJNDICalls", percTimeInJndi );
      openDataItemMap.put( "percentageOfContextTimeInRemoteCacheCalls", percTimeInRemoteCache );
    }

    double  incompleteContextSeconds;
    double  avgContextSeconds;
    double  avgCpuSeconds;
    double  avgUserSeconds;
    double  percTimeBlocked;
    double  percTimeWaited;
    double  percTimeInJndi;
    double  percTimeInJdbc;
    double  percTimeInJdbcConnWait;
    double  percTimeInRemoteCache;
  }

  // Names of MethodContextMBean attributes to gather from active/incomplete method contexts
  private static final String  CURRENT_CONTEXT_ATTRS[] =
  {
    "TargetClass",
    "TargetMethod",
    "ElapsedTotalSeconds",
    "BlockedSeconds",
    "WaitedSeconds",
    "ElapsedJNDISeconds",
    "ElapsedJDBCSeconds",
    "JDBCConnectionWaitTimeSeconds",
    "ElapsedRemoteCacheSeconds"
  };

  /** Merges limited set of data from currently active (and thus as of yet
   *  not completed) method contexts with existing data map.
   */
  void  addCurrentContextData( final Map<HistoBucket,StatsDataBucket> histoData )
  {
    final MethodContextMonitorMBean  monitor = this.monitor;
    if ( monitor == null )
      return;
    final AttributeList  contextAttrsArr[] = monitor.getActiveContextAttributes( CURRENT_CONTEXT_ATTRS );
    for ( AttributeList contextAttrs : contextAttrsArr )
    {
      final Map<String,Object>  attrMap = MBeanUtilities.newHashMap( CURRENT_CONTEXT_ATTRS.length, 0.75f );
      for ( Object contextAttrObj : contextAttrs )
      {
        final Attribute  contextAttr = (Attribute) contextAttrObj;
        attrMap.put( contextAttr.getName(), contextAttr.getValue() );
      }
      final HistoBucket  key = new HistoBucket( attrMap );
      final StatsDataBucket  newBucketData = new StatsDataBucket( attrMap );
      final StatsDataBucket  oldBucketData = histoData.get( key );
      if ( oldBucketData != null )
        oldBucketData.addDataFrom( newBucketData );
      else
        histoData.put( key, newBucketData );
    }
  }

  private static final String  lineSeparator = System.lineSeparator();

  private static String  getLineSeparatedString( final String strings[] )
  {
    int  strLen = 0;
    for ( String string : strings )
      strLen += string.length();
    strLen += lineSeparator.length() * ( strings.length + 1 );
    final StringBuilder  builder = new StringBuilder( strLen );
    builder.append( lineSeparator );
    for ( String string : strings )
    {
      builder.append( string );
      builder.append( lineSeparator );
    }
    return ( builder.toString() );
  }

  private static String  formatDataMessage( final String msgKey, final Object data )
  {
    return ( MBeanUtilities.formatMessage( RESOURCE.getString( msgKey ), data ) );
  }

  // JMX "open" type definitions

  /** Names of items in OVERALL_DATA_TYPE
   */
  static final String  OVERALL_DATA_ITEMS[] =
  {
    "completedContexts",
    "errorCount",
    "redirectCount",
    "contextSecondsAverage",
    "contextSecondsMax",
    "contextCpuSecondsAverage",
    "contextUserSecondsAverage",
    "percentageOfContextTimeBlocked",
    "percentageOfContextTimeWaited",
    "percentageOfContextTimeInJDBCCalls",
    "percentageOfContextTimeInJDBCConnWait",
    "percentageOfContextTimeInJNDICalls",
    "percentageOfContextTimeInRemoteCacheCalls"
  };

  /** Names of items in HISTO_BUCKET_DATA_TYPE and <i>not</i> in OVERALL_DATA_TYPE to be used as a key in HISTOGRAM_TYPE
   */
  static final String  HISTO_BUCKET_KEY_ITEMS[] =
  {
    "targetClass",
    "targetMethod"
  };

  /** Names of all items in HISTO_BUCKET_DATA_TYPE
   */
  static final String  HISTO_BUCKET_DATA_ITEMS[];
  static
  {
    HISTO_BUCKET_DATA_ITEMS = new String[OVERALL_DATA_ITEMS.length + HISTO_BUCKET_KEY_ITEMS.length];
    System.arraycopy( OVERALL_DATA_ITEMS, 0, HISTO_BUCKET_DATA_ITEMS, HISTO_BUCKET_KEY_ITEMS.length,
                      OVERALL_DATA_ITEMS.length );
    System.arraycopy( HISTO_BUCKET_KEY_ITEMS, 0, HISTO_BUCKET_DATA_ITEMS, 0, HISTO_BUCKET_KEY_ITEMS.length );
  };

  /** Overall aggregate statistics type
   */
  static final CompositeType  OVERALL_DATA_TYPE;

  /** Per-bucket/grouping histogram data type
   */
  static final CompositeType  HISTO_BUCKET_DATA_TYPE;

  static
  {
    try
    {
      final String  OVERALL_DATA_ITEM_DESCRS[] =
      {
        METHOD_RESOURCE.getString( methodResource.COMPLETED_CTXTS_ITEM_DESCR ),
        METHOD_RESOURCE.getString( methodResource.ERROR_COUNT_ITEM_DESCR ),
        METHOD_RESOURCE.getString( methodResource.REDIRECT_COUNT_ITEM_DESCR ),
        METHOD_RESOURCE.getString( methodResource.CTXT_SECS_AVG_ITEM_DESCR ),
        METHOD_RESOURCE.getString( methodResource.CTXT_SECS_MAX_ITEM_DESCR ),
        METHOD_RESOURCE.getString( methodResource.CTXT_CPU_SECS_AVG_ITEM_DESCR ),
        METHOD_RESOURCE.getString( methodResource.CTXT_USER_SECS_AVG_ITEM_DESCR ),
        METHOD_RESOURCE.getString( methodResource.PERC_CTXT_TIME_BLOCKED_ITEM_DESCR ),
        METHOD_RESOURCE.getString( methodResource.PERC_CTXT_TIME_WAITED_ITEM_DESCR ),
        METHOD_RESOURCE.getString( methodResource.PERC_CTXT_TIME_IN_JDBC_ITEM_DESCR ),
        METHOD_RESOURCE.getString( methodResource.PERC_CTXT_TIME_IN_JDBC_WAIT_ITEM_DESCR ),
        METHOD_RESOURCE.getString( methodResource.PERC_CTXT_TIME_IN_JNDI_ITEM_DESCR ),
        METHOD_RESOURCE.getString( methodResource.PERC_CTXT_TIME_IN_CACHE_CALLS_ITEM_DESCR ),
      };
      final OpenType  OVERALL_DATA_ITEM_TYPES[] =
      {
        SimpleType.LONG,
        SimpleType.LONG,
        SimpleType.LONG,
        SimpleType.DOUBLE,
        SimpleType.DOUBLE,
        SimpleType.DOUBLE,
        SimpleType.DOUBLE,
        SimpleType.DOUBLE,
        SimpleType.DOUBLE,
        SimpleType.DOUBLE,
        SimpleType.DOUBLE,
        SimpleType.DOUBLE,
        SimpleType.DOUBLE,
      };
      OVERALL_DATA_TYPE = new CompositeType( CLASSNAME + ".overallData",
                                             RESOURCE.getString( jmxResource.OVERALL_DATA_DESCR ),
                                             OVERALL_DATA_ITEMS, OVERALL_DATA_ITEM_DESCRS,
                                             OVERALL_DATA_ITEM_TYPES );

      final String  HISTO_BUCKET_KEY_ITEM_DESCRS[] =
      {
        RESOURCE.getString( jmxResource.TARGET_CLASS_DESCR ),
        RESOURCE.getString( jmxResource.TARGET_METHOD_DESCR )
      };
      final String  HISTO_BUCKET_DATA_ITEM_DESCRS[];
      {
        HISTO_BUCKET_DATA_ITEM_DESCRS = new String[OVERALL_DATA_ITEM_DESCRS.length + HISTO_BUCKET_KEY_ITEM_DESCRS.length];
        System.arraycopy( OVERALL_DATA_ITEM_DESCRS, 0, HISTO_BUCKET_DATA_ITEM_DESCRS, HISTO_BUCKET_KEY_ITEM_DESCRS.length,
                          OVERALL_DATA_ITEM_DESCRS.length );
        System.arraycopy( HISTO_BUCKET_KEY_ITEM_DESCRS, 0, HISTO_BUCKET_DATA_ITEM_DESCRS, 0, HISTO_BUCKET_KEY_ITEM_DESCRS.length );
      }
      final OpenType  HISTO_BUCKET_KEY_ITEM_TYPES[] =
      {
        SimpleType.STRING,
        SimpleType.STRING
      };
      final OpenType  HISTO_BUCKET_DATA_ITEM_TYPES[];
      {
        HISTO_BUCKET_DATA_ITEM_TYPES = new OpenType[OVERALL_DATA_ITEM_TYPES.length + HISTO_BUCKET_KEY_ITEM_TYPES.length];
        System.arraycopy( OVERALL_DATA_ITEM_TYPES, 0, HISTO_BUCKET_DATA_ITEM_TYPES, HISTO_BUCKET_KEY_ITEM_TYPES.length,
                          OVERALL_DATA_ITEM_TYPES.length );
        System.arraycopy( HISTO_BUCKET_KEY_ITEM_TYPES, 0, HISTO_BUCKET_DATA_ITEM_TYPES, 0, HISTO_BUCKET_KEY_ITEM_TYPES.length );
      }
      HISTO_BUCKET_DATA_TYPE = new CompositeType( CLASSNAME + ".histogramGroupingData",
                                                  RESOURCE.getString( jmxResource.HISTO_BUCKET_DATA_TYPE_DESCR ),
                                                  HISTO_BUCKET_DATA_ITEMS, HISTO_BUCKET_DATA_ITEM_DESCRS,
                                                  HISTO_BUCKET_DATA_ITEM_TYPES );
    }
    catch ( OpenDataException e )
    {
      LogR.getLogger( CLASSNAME ).error( "Failed to set up nested CompositeTypes", e );
      throw new ExceptionInInitializerError( e );
    }
  }

  /** Histogram data type
   */
  static final TabularType  HISTOGRAM_TYPE;
  static
  {
    try
    {
      HISTOGRAM_TYPE = new TabularType( CLASSNAME + ".histogramType",
                                        RESOURCE.getString( jmxResource.HISTOGRAM_DATA_DESCR ),
                                        HISTO_BUCKET_DATA_TYPE, HISTO_BUCKET_DATA_ITEMS );
    }
    catch ( OpenDataException e )
    {
      LogR.getLogger( CLASSNAME ).error( "Failed to set up TabularType", e );
      throw new ExceptionInInitializerError( e );
    }
  }

  /** Names of all items in STATS_DATA_TYPE
   */
  static final String  STATS_DATA_ITEMS[] =
  {
    "startTime",
    "endTime",
    "elapsedSeconds",
    "overallStatistics",
    "histogramStatistics"
  };

  /** Top-level statistics data type
   */
  static final CompositeType  STATS_DATA_TYPE;
  static
  {
    try
    {
      STATS_DATA_TYPE = new CompositeType( CLASSNAME + ".statsDataType",
                                           RESOURCE.getString( jmxResource.STATS_DATA_TYPE_DESCR ),
                                           STATS_DATA_ITEMS,
                                           new String[]
                                           {
                                             MB_RESOURCE.getString( mbeansResource.START_TIME_ITEM_DESCR ),
                                             MB_RESOURCE.getString( mbeansResource.END_TIME_ITEM_DESCR ),
                                             MB_RESOURCE.getString( mbeansResource.ELAPSED_TIME_ITEM_DESCR ),
                                             RESOURCE.getString( jmxResource.OVERALL_DATA_DESCR ),
                                             RESOURCE.getString( jmxResource.HISTOGRAM_DATA_DESCR ),
                                           },
                                           new OpenType[]
                                           {
                                             SimpleType.DATE,
                                             SimpleType.DATE,
                                             SimpleType.DOUBLE,
                                             OVERALL_DATA_TYPE,
                                             HISTOGRAM_TYPE
                                           }
      );
    }
    catch ( OpenDataException e )
    {
      LogR.getLogger( CLASSNAME ).error( "Failed to set up top-level CompositeType", e );
      throw new ExceptionInInitializerError( e );
    }
  }

  // Runnables for background tasks

  /** Runnable that simply calls resetData()
   */
  private final class  ResetDataTask
    implements Runnable
  {
    @Override
    public final void  run()
    {
      resetData();
    }
  }

  // Non-static fields

  // create a periodic task support instance with default interval of 0 seconds (never runs) and using the default worker thread
  private final PeriodicTaskSupport  resetDataTask = new PeriodicTaskSupport()
  {
    // Implementation of abstract PeriodicTaskSupport method
    @Override
    protected final Runnable  createTask()
    {
      // return background task as defined above
      return ( new ResetDataTask() );
    }
  };

  private final String  name;
  final Logger  logger;  // not private to allow for more efficient access from inner class
  final Logger  structuredLogger;  // not private to allow for more efficient access from inner class
  private final ReentrantReadWriteLock  masterDataReadWriteLock = new ReentrantReadWriteLock();
  private final Lock  masterDataUpdateLock = masterDataReadWriteLock.readLock();  // any number at once, but no overlaps with reset
  private final Lock  masterDataResetLock = masterDataReadWriteLock.writeLock();  // only one at once and no overlaps with update
  private MethodContextMonitorMBean  monitor;
  private volatile Data  data = new Data();
  private volatile boolean  logOnReset;
}
