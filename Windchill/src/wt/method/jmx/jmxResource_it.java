/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.method.jmx;

import wt.util.resource.*;

@RBUUID("wt.method.jmx.jmxResource")
public final class jmxResource_it extends WTListResourceBundle {
   @RBEntry("Statistiche globali consolidate contesto di metodo")
   public static final String OVERALL_DATA_DESCR = "1";

   @RBEntry("Classe di destinazione contesto di metodo")
   public static final String TARGET_CLASS_DESCR = "2";

   @RBEntry("Metodo di destinazione contesto di metodo")
   public static final String TARGET_METHOD_DESCR = "3";

   @RBEntry("Dati per raggruppamento in istogramma unico")
   public static final String HISTO_BUCKET_DATA_TYPE_DESCR = "4";

   @RBEntry("Dati istogramma contesto di metodo")
   public static final String HISTOGRAM_DATA_DESCR = "5";

   @RBEntry("Dati statistici contesto di metodo")
   public static final String STATS_DATA_TYPE_DESCR = "6";

   @RBEntry("Inizio: {0,date} {0,time,full}")
   @RBArgComment0("Start of time period (as localized date)")
   public static final String START_TIME_MSG = "7";

   @RBEntry("Fine: {0,date} {0,time,full}")
   @RBArgComment0("End of time period (as localized date)")
   public static final String END_TIME_MSG = "8";

   @RBEntry("Secondi trascorsi: {0,number,0.0#####}")
   @RBArgComment0("Number of elapsed seconds")
   public static final String ELAPSED_SECONDS_MSG = "9";

   @RBEntry("Statistiche globali consolidate")
   public static final String OVERALL_AGG_STATS = "10";

   @RBEntry("Contesti completati: {0,number}")
   @RBArgComment0("Number of completed contexts")
   public static final String COMPL_CONTEXTS_MSG = "11";

   @RBEntry("Errori: {0,number}")
   @RBArgComment0("Number of errors")
   public static final String ERRORS_MSG = "12";

   @RBEntry("Reindirizzamenti: {0,number}")
   @RBArgComment0("Number of redirected contexts")
   public static final String REDIRECTS_MSG = "13";

   @RBEntry("Secondi per contesto (media): {0,number,0.0#####}")
   @RBArgComment0("Average length of contexts in seconds")
   public static final String AVG_CONTEXT_SECONDS_MSG = "14";

   @RBEntry("Secondi per contesto (max): {0,number,0.0#####}")
   @RBArgComment0("Maximum length of contexts in seconds")
   public static final String MAX_CONTEXT_SECONDS_MSG = "15";

   @RBEntry("Secondi per CPU (media): {0,number,0.0#####}")
   @RBArgComment0("Average CPU time spent per context in seconds")
   public static final String AVG_CPU_SECONDS_MSG = "16";

   @RBEntry("Secondi per utente (media): {0,number,0.0#####}")
   @RBArgComment0("Average user (vs. kernel) CPU time spent per context in seconds")
   public static final String AVG_USER_SECONDS_MSG = "17";

   @RBEntry("% tempo trascorso in JDBC: {0}")
   @RBArgComment0("Percentage of time spent in JDBC calls")
   public static final String PERC_TIME_IN_JDBC_MSG = "18";

   @RBEntry("% tempo trascorso in attesa connessioni JDBC: {0}")
   @RBArgComment0("Percentage of time spent in JDBC connection wait")
   public static final String PERC_TIME_IN_JDBC_CONN_WAIT_MSG = "19";

   @RBEntry("% tempo trascorso in JNDI: {0}")
   @RBArgComment0("Percentage of time spent in JNDI calls")
   public static final String PERC_TIME_IN_JNDI_MSG = "20";

   @RBEntry("Altri contesti")
   public static final String OTHER_CONTEXTS = "21";

   @RBEntry("% tempo trascorso in memorizzazione nella cache remota: {0}")
   @RBArgComment0("Percentage of time spent in remote cache calls")
   public static final String PERC_TIME_IN_REMOTE_CACHING_MSG = "22";

   @RBEntry("% tempo trascorso in stato BLOCKED (0 se non disponibile): {0}")
   @RBArgComment0("Percentage of time spent in BLOCKED state")
   public static final String PERC_TIME_BLOCKED_MSG = "23";

   @RBEntry("% tempo trascorso in stato WAITING o TIMED_WAITING (0 se non disponibile): {0}")
   @RBArgComment0("Percentage of time spent in WAITING or TIMED_WAITING state")
   public static final String PERC_TIME_WAITED_MSG = "24";
}
