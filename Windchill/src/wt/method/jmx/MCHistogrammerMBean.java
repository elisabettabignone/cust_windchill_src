/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.method.jmx;

/* In addition to providing useful functionality, this interface is intended to
 * serve as a example MBean interface to aid developers and customizers author
 * their own MBean interfaces and implement these.
 *
 * Even though source code for this class is provided, compilation of this
 * source under the existing class name (i.e. in place of the class delivered)
 * is still not supported.
 */

/* For each of our MBean interface source files, xxxMBean.java, automated
 * tooling is used to produce a corresponding xxxMBeanResource.rbInfo file.
 * Thus in the case of this MBean inteface, MCHistogrammerMBeanResource.rbInfo
 * is produced by this tooling.
 *
 * To [re]generate the rbInfo for an MBean interface source file (upon creating
 * a new MBean interface file or making changes), invoke the following command
 * from the source root directory in a Windchill shell:
 *   javac -proc:only -processor wt.annotations.processors.MBeanInterfaceProcessor <path of source file>
 * For instance, for this MBean interface (on Windows) one would invoke
 *   javac -proc:only -processor wt.annotations.processors.MBeanInterfaceProcessor wt\mehod\jmx\MCHistogrammerMBean.java
 *
 * The tool garners descriptions and operation parameter names from the formal
 * parameter names and Javadoc comments in the MBean interface source file and
 * expresses these in the rbInfo.  At runtime, the wt.jmx.core.StandardMBean
 * reads these rbInfos and incorporates this additional metadata into the
 * MBeans' MBeanInfo.  This allows for localization, though no localized
 * versions of these particular bundles are currently produced for delivery
 * with the product.  Re-use of formal parameters and Javadoc comment information
 * avoids redundant documentation efforts and ensures consistency between
 * Javadoc documentation and MBean documentation at runtime.
 */

import javax.management.MBeanOperationInfo;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.OpenDataException;

import wt.jmx.annotations.MBeanOperationImpact;
import wt.jmx.core.mbeans.SelfEmailingMBean;


/** Produce histogram statistics for method contexts.
 *
 *    <BR><BR><B>Supported API: </B>true
 *    <BR><BR><B>Extendable: </B>true
 */
public interface  MCHistogrammerMBean
  extends SelfEmailingMBean
{
  /** Name of this MBean
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  public String  getName();

  /** Interval (in seconds) between automatic calls to resetData() operation
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  public int  getAutoResetIntervalSeconds();

  /** Interval (in seconds) between automatic calls to resetData() operation
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  public void  setAutoResetIntervalSeconds( int autoResetIntervalSeconds );

  /** Whether data is logged (to logger indicated by LoggerName attribute) on invocations of resetData() operation
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  public boolean  isLogOnReset();

  /** Whether data is logged (to logger indicated by LoggerName attribute) on invocations of resetData() operation
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  public void  setLogOnReset( boolean logOnReset );

  /** Name of logger this MBean uses for normal logging of data and errors;
   *  data output to this logger is controlled by the LogOnReset attribute
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  public String  getLoggerName();

  /** Name of logger this MBean uses for structured data logging;
   *  structured data logging is only performed when the verbosity level of this logger is at least INFO
   *  <p>
   *  For use by advanced log4j appenders and layouts like {@link wt.log4j.jmx.AsyncJDBCAppender}
   *  and {@link wt.log4j.jmx.TSVLayout}.
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  public String  getStructuredDataLoggerName();

  /** Histogram data for method contexts as structured CompositeData
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  public CompositeData  getData()
    throws OpenDataException;

  /** Histogram data for method contexts as array of strings
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  public String[]  getDataAsStrings();

  /** Reset collected data, starting aggregation of data afresh.  Logs data
   *  collected to this point when LogOnReset is true.
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  @MBeanOperationImpact( MBeanOperationInfo.ACTION )
  public void  resetData();
}
