/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.method.jmx;

import wt.util.resource.*;

@RBUUID("wt.method.jmx.jmxResource")
public final class jmxResource extends WTListResourceBundle {
   @RBEntry("Method context overall aggregate statistics")
   public static final String OVERALL_DATA_DESCR = "1";

   @RBEntry("Method context target class")
   public static final String TARGET_CLASS_DESCR = "2";

   @RBEntry("Method context target method")
   public static final String TARGET_METHOD_DESCR = "3";

   @RBEntry("Data for one histogram grouping")
   public static final String HISTO_BUCKET_DATA_TYPE_DESCR = "4";

   @RBEntry("Method context histogram data")
   public static final String HISTOGRAM_DATA_DESCR = "5";

   @RBEntry("Method context statistics data")
   public static final String STATS_DATA_TYPE_DESCR = "6";

   @RBEntry("Start Time: {0,date} {0,time,full}")
   @RBArgComment0("Start of time period (as localized date)")
   public static final String START_TIME_MSG = "7";

   @RBEntry("End Time: {0,date} {0,time,full}")
   @RBArgComment0("End of time period (as localized date)")
   public static final String END_TIME_MSG = "8";

   @RBEntry("Elapsed Seconds: {0,number,0.0#####}")
   @RBArgComment0("Number of elapsed seconds")
   public static final String ELAPSED_SECONDS_MSG = "9";

   @RBEntry("Overall Aggregate Statistics")
   public static final String OVERALL_AGG_STATS = "10";

   @RBEntry("Completed Contexts: {0,number}")
   @RBArgComment0("Number of completed contexts")
   public static final String COMPL_CONTEXTS_MSG = "11";

   @RBEntry("Errors: {0,number}")
   @RBArgComment0("Number of errors")
   public static final String ERRORS_MSG = "12";

   @RBEntry("Redirects: {0,number}")
   @RBArgComment0("Number of redirected contexts")
   public static final String REDIRECTS_MSG = "13";

   @RBEntry("Average Context Seconds: {0,number,0.0#####}")
   @RBArgComment0("Average length of contexts in seconds")
   public static final String AVG_CONTEXT_SECONDS_MSG = "14";

   @RBEntry("Maximum Context Seconds: {0,number,0.0#####}")
   @RBArgComment0("Maximum length of contexts in seconds")
   public static final String MAX_CONTEXT_SECONDS_MSG = "15";

   @RBEntry("Average CPU Seconds: {0,number,0.0#####}")
   @RBArgComment0("Average CPU time spent per context in seconds")
   public static final String AVG_CPU_SECONDS_MSG = "16";

   @RBEntry("Average User Seconds: {0,number,0.0#####}")
   @RBArgComment0("Average user (vs. kernel) CPU time spent per context in seconds")
   public static final String AVG_USER_SECONDS_MSG = "17";

   @RBEntry("% Time in JDBC: {0}")
   @RBArgComment0("Percentage of time spent in JDBC calls")
   public static final String PERC_TIME_IN_JDBC_MSG = "18";

   @RBEntry("% Time in JDBC Connection Wait: {0}")
   @RBArgComment0("Percentage of time spent in JDBC connection wait")
   public static final String PERC_TIME_IN_JDBC_CONN_WAIT_MSG = "19";

   @RBEntry("% Time in JNDI: {0}")
   @RBArgComment0("Percentage of time spent in JNDI calls")
   public static final String PERC_TIME_IN_JNDI_MSG = "20";

   @RBEntry("Other Contexts")
   public static final String OTHER_CONTEXTS = "21";

   @RBEntry("% Time in Remote Caching: {0}")
   @RBArgComment0("Percentage of time spent in remote cache calls")
   public static final String PERC_TIME_IN_REMOTE_CACHING_MSG = "22";

   @RBEntry("% Time in BLOCKED State (0 if unavailable): {0}")
   @RBArgComment0("Percentage of time spent in BLOCKED state")
   public static final String PERC_TIME_BLOCKED_MSG = "23";

   @RBEntry("% Time in WAITING or TIMED_WAITING State (0 if unavailable): {0}")
   @RBArgComment0("Percentage of time spent in WAITING or TIMED_WAITING state")
   public static final String PERC_TIME_WAITED_MSG = "24";
}
