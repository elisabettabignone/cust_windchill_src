/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.locks;

import wt.util.resource.*;

@RBUUID("wt.locks.locksResource")
public final class locksResource extends WTListResourceBundle {
   @RBEntry("The object's lock has not been initialized (i.e., it was null).")
   public static final String IMPROPER_LOCK_INITIALIZATION = "0";

   @RBEntry("The object has not been initialized (i.e., it was null).")
   public static final String IMPROPER_INITIALIZATION = "1";

   @RBEntry("The principal has not been initialized (i.e., it was null).")
   public static final String IMPROPER_PRINCIPAL_INITIALIZATION = "2";

   @RBEntry("The object was already locked.")
   public static final String OBJECT_ALREADY_LOCKED = "3";

   @RBEntry("The object was already unlocked.")
   public static final String OBJECT_ALREADY_UNLOCKED = "4";

   @RBEntry("The object was locked by a different principal and cannot be modified.")
   public static final String OBJECT_ALREADY_LOCKED_BY_ANOTHER = "5";

   @RBEntry("The principal is not authorized to modify (i.e., unlock) the object.")
   public static final String NOT_AUTHORIZED_TO_MODIFY = "6";

   @RBEntry("The principal is not the original locker, or is not authorized to modify (i.e., unlock) the object.")
   public static final String NOT_ORIGINAL_LOCKER = "7";
}
