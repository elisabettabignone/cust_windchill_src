/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.locks;

import wt.util.resource.*;

@RBUUID("wt.locks.locksResource")
public final class locksResource_it extends WTListResourceBundle {
   @RBEntry("Blocco dell'oggetto non inizializzato perché nullo.")
   public static final String IMPROPER_LOCK_INITIALIZATION = "0";

   @RBEntry("L'oggetto non è stato inizializzato perché nullo.")
   public static final String IMPROPER_INITIALIZATION = "1";

   @RBEntry("L'utente/gruppo/ruolo non è stato inizializzato perché nullo.")
   public static final String IMPROPER_PRINCIPAL_INITIALIZATION = "2";

   @RBEntry("Oggetto già bloccato.")
   public static final String OBJECT_ALREADY_LOCKED = "3";

   @RBEntry("Oggetto già sbloccato.")
   public static final String OBJECT_ALREADY_UNLOCKED = "4";

   @RBEntry("L'oggetto è stato bloccato da un utente/gruppo/ruolo diverso e quindi non è modificabile.")
   public static final String OBJECT_ALREADY_LOCKED_BY_ANOTHER = "5";

   @RBEntry("Utente/gruppo/ruolo non autorizzato a modificare o sbloccare l'oggetto.")
   public static final String NOT_AUTHORIZED_TO_MODIFY = "6";

   @RBEntry("L'utente/gruppo/ruolo non è autore del blocco iniziale o non è autorizzato a modificare e sbloccare l'oggetto.")
   public static final String NOT_ORIGINAL_LOCKER = "7";
}
