/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.portal;

import wt.util.resource.*;

@RBUUID("wt.portal.PortalRB")
public final class PortalRB extends WTListResourceBundle {
   @RBEntry("Site Map")
   public static final String SITE_MAP = "0";

   @RBEntry("Available Homes:")
   public static final String AVAILABLE_HOMES = "1";

   @RBEntry("Marketing")
   public static final String MARKETING = "2";

   @RBEntry("Product Design")
   public static final String PRODUCT_DESIGN = "3";

   @RBEntry("Technical Publications")
   public static final String TECH_PUBS = "4";

   @RBEntry("Purchasing")
   public static final String PURCHASING = "5";

   @RBEntry("Manufacturing Planning")
   public static final String MANF_PLANNING = "6";

   @RBEntry("Production")
   public static final String PRODUCTION = "7";

   @RBEntry("Service Support")
   public static final String SERVICE_SUPPORT = "8";

   @RBEntry("Administration")
   public static final String ADMINISTRATION = "9";

   @RBEntry("Customer")
   public static final String CUSTOMER = "10";

   @RBEntry("Supplier")
   public static final String SUPPLIER = "11";

   @RBEntry("Options")
   public static final String COMMON_HOME = "12";

   @RBEntry("User's Guide")
   public static final String USERS_GUIDE = "13";

   @RBEntry("Windchill Administrator")
   public static final String WIND_ADMIN = "14";

   @RBEntry("Process Administrator")
   public static final String PROCESS_ADMIN = "15";

   @RBEntry("Federation Administrator")
   public static final String FED_ADMIN = "16";

   @RBEntry("Sourcing Factor Administrator")
   public static final String SFACTOR_ADMIN = "17";

   @RBEntry("Optegra Gateway Administrator")
   public static final String OPTEGRA_GW_ADMIN = "18";

   @RBEntry("External Storage Administrator")
   public static final String EX_STORAGE_ADMIN = "19";

   @RBEntry("Replication Administrator")
   public static final String REPLICATION_ADMIN = "20";

   @RBEntry("Pro/INTRALINK Gateway Administrator")
   public static final String INTRALINK_GW_ADMIN = "21";

   @RBEntry("Pro/PDM Gateway Administrator")
   public static final String PDM_GW_ADMIN = "22";

   @RBEntry("Release to Production")
   public static final String RTP_ADMIN = "23";

   @RBEntry("Process Planner Administrator")
   public static final String PROCESS_PLNR_ADMIN = "24";

   @RBEntry("System Configurator")
   public static final String SYS_CONFIG = "25";

   @RBEntry("Library")
   public static final String LIBRARY = "26";

   @RBEntry("Process Planner")
   public static final String PROCESS_PLANNER = "27";

   @RBEntry("Windchill Product Configurator")
   public static final String WPC = "28";

   @RBEntry("Home")
   public static final String MY_HOME = "29";

   @RBEntry("If your browser will not take you there automatically, then follow this link:")
   public static final String FORWARD_MESSAGE = "30";

   @RBEntry("Your home page is being set to")
   public static final String SET_HOMEPAGE = "31";

   @RBEntry("Sourcing Factor")
   public static final String SOURCING_FACTOR = "32";

   @RBEntry("Proxy Manager")
   public static final String PROXY_MANAGER = "33";

   @RBEntry("Attribute Administrator")
   public static final String ATTRIBUTE_ADMINISTRATOR = "34";

   @RBEntry("Meeting Center")
   public static final String MEETING_CENTER = "35";

   /**
    * -----------------------------------------------------------------------
    * The following to entries are for the routed systems database project
    * 
    **/
   @RBEntry("RSExplorer")
   public static final String RS_EXPLORER = "36";

   @RBEntry("RSAdministrator")
   public static final String RS_ADMINISTRATOR = "37";

   /**
    * -----------------------------------------------------------------------
    * 
    **/
   @RBEntry("Import/Export Manager")
   public static final String IXB_ADMIN = "38";

   @RBEntry("Visualization")
   public static final String VISUALIZATION = "39";

   @RBEntry("Policy Administration")
   public static final String POLICY_ADMIN = "40";

   @RBEntry("Principal Administrator")
   public static final String USER_GROUP_ADMIN = "41";

   @RBEntry("Info*Engine Administration")
   public static final String INFOENGINE_ADMIN = "42";

   @RBEntry("Task Delegate Administration")
   public static final String TASK_DELEGATE_ADMIN = "43";

   @RBEntry("Business Administration")
   public static final String BUSINESS_ADMIN = "44";

   @RBEntry("System Administration")
   public static final String SYSTEM_ADMIN = "45";

   @RBEntry("Document Template Administrator")
   public static final String DOC_TEMPLATE_ADMIN = "46";

   @RBEntry("ProjectLink")
   public static final String PROJECTLINK = "47";

   @RBEntry("Publications")
   public static final String PUBLICATIONS = "48";
}
