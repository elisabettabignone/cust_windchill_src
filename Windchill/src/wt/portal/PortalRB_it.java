/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.portal;

import wt.util.resource.*;

@RBUUID("wt.portal.PortalRB")
public final class PortalRB_it extends WTListResourceBundle {
   @RBEntry("Mappa del sito")
   public static final String SITE_MAP = "0";

   @RBEntry("Home page disponibili:")
   public static final String AVAILABLE_HOMES = "1";

   @RBEntry("Marketing")
   public static final String MARKETING = "2";

   @RBEntry("Progettazione")
   public static final String PRODUCT_DESIGN = "3";

   @RBEntry("Pubblicazioni tecniche")
   public static final String TECH_PUBS = "4";

   @RBEntry("Approvvigionamento")
   public static final String PURCHASING = "5";

   @RBEntry("Pianificazione della produzione")
   public static final String MANF_PLANNING = "6";

   @RBEntry("Produzione")
   public static final String PRODUCTION = "7";

   @RBEntry("Supporto tecnico")
   public static final String SERVICE_SUPPORT = "8";

   @RBEntry("Amministrazione")
   public static final String ADMINISTRATION = "9";

   @RBEntry("Cliente")
   public static final String CUSTOMER = "10";

   @RBEntry("Fornitore")
   public static final String SUPPLIER = "11";

   @RBEntry("Opzioni")
   public static final String COMMON_HOME = "12";

   @RBEntry("Guida dell'utente")
   public static final String USERS_GUIDE = "13";

   @RBEntry("Amministrazione Windchill")
   public static final String WIND_ADMIN = "14";

   @RBEntry("Amministrazione processi")
   public static final String PROCESS_ADMIN = "15";

   @RBEntry("Amministrazione ambiente federato")
   public static final String FED_ADMIN = "16";

   @RBEntry("Amministrazione Sourcing Factor")
   public static final String SFACTOR_ADMIN = "17";

   @RBEntry("Amministrazione Optegra Gateway")
   public static final String OPTEGRA_GW_ADMIN = "18";

   @RBEntry("Amministrazione memorizzazione esterna")
   public static final String EX_STORAGE_ADMIN = "19";

   @RBEntry("Amministrazione repliche")
   public static final String REPLICATION_ADMIN = "20";

   @RBEntry("Amministrazione Gateway Pro/INTRALINK")
   public static final String INTRALINK_GW_ADMIN = "21";

   @RBEntry("Amministrazione Gateway Pro/PDM")
   public static final String PDM_GW_ADMIN = "22";

   @RBEntry("Produzione")
   public static final String RTP_ADMIN = "23";

   @RBEntry("Amministrazione pianificazione processi")
   public static final String PROCESS_PLNR_ADMIN = "24";

   @RBEntry("Configuratore di sistema")
   public static final String SYS_CONFIG = "25";

   @RBEntry("Libreria")
   public static final String LIBRARY = "26";

   @RBEntry("Pianificazione processi")
   public static final String PROCESS_PLANNER = "27";

   @RBEntry("Configurazione di prodotto Windchill")
   public static final String WPC = "28";

   @RBEntry("Home")
   public static final String MY_HOME = "29";

   @RBEntry("Se la pagina non si apre automaticamente, utilizzare questo link:")
   public static final String FORWARD_MESSAGE = "30";

   @RBEntry("Impostazione della home page:")
   public static final String SET_HOMEPAGE = "31";

   @RBEntry("Sourcing Factor")
   public static final String SOURCING_FACTOR = "32";

   @RBEntry("Gestione proxy")
   public static final String PROXY_MANAGER = "33";

   @RBEntry("Amministrazione attributi")
   public static final String ATTRIBUTE_ADMINISTRATOR = "34";

   @RBEntry("Centro meeting")
   public static final String MEETING_CENTER = "35";

   /**
    * -----------------------------------------------------------------------
    * The following to entries are for the routed systems database project
    * 
    **/
   @RBEntry("Navigatore RS")
   public static final String RS_EXPLORER = "36";

   @RBEntry("Amministrazione RS")
   public static final String RS_ADMINISTRATOR = "37";

   /**
    * -----------------------------------------------------------------------
    * 
    **/
   @RBEntry("Gestione importazioni/esportazioni")
   public static final String IXB_ADMIN = "38";

   @RBEntry("Visualization")
   public static final String VISUALIZATION = "39";

   @RBEntry("Amministrazione regole")
   public static final String POLICY_ADMIN = "40";

   @RBEntry("Amministrazione utenti/gruppi/ruoli")
   public static final String USER_GROUP_ADMIN = "41";

   @RBEntry("Amministrazione Info*Engine")
   public static final String INFOENGINE_ADMIN = "42";

   @RBEntry("Amministrazione delegati di task")
   public static final String TASK_DELEGATE_ADMIN = "43";

   @RBEntry("Amministrazione aziendale")
   public static final String BUSINESS_ADMIN = "44";

   @RBEntry("Amministrazione di sistema")
   public static final String SYSTEM_ADMIN = "45";

   @RBEntry("Amministrazione modelli di documento")
   public static final String DOC_TEMPLATE_ADMIN = "46";

   @RBEntry("ProjectLink")
   public static final String PROJECTLINK = "47";

   @RBEntry("Pubblicazioni")
   public static final String PUBLICATIONS = "48";
}
