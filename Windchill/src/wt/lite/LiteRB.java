/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.lite;

import wt.util.resource.*;

@RBUUID("wt.lite.LiteRB")
public final class LiteRB extends WTListResourceBundle {
   @RBEntry("Missing Object Identifier.")
   public static final String OID_CAN_NOT_BE_NULL = "lite0";
}
