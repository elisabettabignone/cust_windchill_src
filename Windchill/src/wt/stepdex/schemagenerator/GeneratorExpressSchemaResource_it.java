/* bcwti
 *
 * Copyright (c) 2012 Parametric Technology Corporation (PTC). All Rights
 * Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.stepdex.schemagenerator;

import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

/**
 *
 * Express schema generation specific resource bundle
 *
 * <BR><BR><B>Supported API: </B>false
 * <BR><BR><B>Extendable: </B>false
 */
@RBUUID("wt.stepdex.schemagenerator.GeneratorExpressSchemaResource")
public class GeneratorExpressSchemaResource_it extends WTListResourceBundle {

    @RBEntry("Directory di installazione Windchill mancante")
    @RBComment("Error message thrown, if Windchill installation directory is not provided as one of the argument")
    public static final String WINDCHILL_HOME_MISSING = "WINDCHILL_HOME_MISSING";

    @RBEntry("Genera lo schema Express del file DTD specificato.\n\njava wt.stepdex.schemagenerator.GenerateExpressSchema WT_HOME fileName [output] \nDove\n WT_HOME:\tSpecifica il percorso della directory di installazione Windchill \n fileName:\tSpecifica il nome del file DTD, che deve risiedere nella cartella [WT_HOME]/loadXMLFiles \n [output]:\tSpecifica la directory di output. Come valore di default viene utilizzata la posizione del file DTD")
    public static final String USAGE = "USAGE";

    @RBEntry("Nome file DTD mancante")
    public static final String DTD_FILE_NAME_MISSING = "DTD_FILE_NAME_MISSING";

    @RBEntry("Directory di output non valida")
    public static final String OUTPUT_DIRECTORY_MISSING = "OUTPUT_DIRECTORY_MISSING";

    @RBEntry("DTD non valida. Impossibile analizzare il file DTD")
    public static final String INVALID_DTD = "INVALID_DTD";

    @RBEntry("Nome file non valido o non disponibile")
    public static final String INVALID_FILE_NAME = "INVALID_FILE_NAME";

    @RBEntry("Impossibile individuare lo schema Express")
    public static final String FILE_NOT_FOUND = "FILE_NOT_FOUND";

    @RBEntry("Generazione dello schema Express completata")
    public static final String FILE_WRITTEN_SUCCESSFULL = "FILE_WRITTEN_SUCCESSFULL";
}
