/* bcwti
 *
 * Copyright (c) 2012 Parametric Technology Corporation (PTC). All Rights
 * Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.stepdex.schemagenerator;

import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

/**
 *
 * Express schema generation specific resource bundle
 *
 * <BR><BR><B>Supported API: </B>false
 * <BR><BR><B>Extendable: </B>false
 */
@RBUUID("wt.stepdex.schemagenerator.GeneratorExpressSchemaResource")
public class GeneratorExpressSchemaResource extends WTListResourceBundle {

    @RBEntry("Missing windchill installation directory")
    @RBComment("Error message thrown, if Windchill installation directory is not provided as one of the argument")
    public static final String WINDCHILL_HOME_MISSING = "WINDCHILL_HOME_MISSING";

    @RBEntry("Generates express schema for the specified DTD file.\n\njava wt.stepdex.schemagenerator.GenerateExpressSchema WT_HOME fileName [output] \nWhere\n WT_HOME:\tSpecify path to Windchill installation directory \n fileName:\tSpecify the name of dtd file. It should be located in [WT_HOME]/loadXMLFiles folder \n [output]:\tSpecify the output directory. Default value has been set to DTD file location")
    public static final String USAGE = "USAGE";

    @RBEntry("Missing DTD file name")
    public static final String DTD_FILE_NAME_MISSING = "DTD_FILE_NAME_MISSING";

    @RBEntry("Invalid output directory")
    public static final String OUTPUT_DIRECTORY_MISSING = "OUTPUT_DIRECTORY_MISSING";

    @RBEntry("Invalid DTD. Failed to parse the DTD file")
    public static final String INVALID_DTD = "INVALID_DTD";

    @RBEntry("File name is either invalid or not available")
    public static final String INVALID_FILE_NAME = "INVALID_FILE_NAME";

    @RBEntry("Unable to locate the express schema")
    public static final String FILE_NOT_FOUND = "FILE_NOT_FOUND";

    @RBEntry("Express schema generated successfully")
    public static final String FILE_WRITTEN_SUCCESSFULL = "FILE_WRITTEN_SUCCESSFULL";
}
