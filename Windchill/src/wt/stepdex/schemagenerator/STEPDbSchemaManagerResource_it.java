/* bcwti
 *
 * Copyright (c) 2012 Parametric Technology Corporation (PTC). All Rights
 * Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.stepdex.schemagenerator;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

/**
 * 
 * STEP database and schema activity specific resource bundle
 * 
 * <BR>
 * <BR>
 * <B>Supported API: </B>false <BR>
 * <BR>
 * <B>Extendable: </B>false
 */
@RBUUID("wt.stepdex.schemagenerator.STEPDbSchemaManagerResource")
public class STEPDbSchemaManagerResource_it extends WTListResourceBundle {

    @RBEntry("\nUtilizzo:\n\n1. Creazione di un database: java wt.stepdex.schemagenerator.STEPDbSchemaManager -createdb [-overwrite] DBName DBContextLoginName DBPassword DBLocation \nDove\n -createdb:\t\tComando per la creazione di un database \n -overwrite:\t\tOpzione per sovrascrivere il database esistente\n DBName:\t\tSpecificare il nome del database\n DBContextLoginName:\tSpecificare il nome di accesso al contesto del database\n DBPassword:\t\tSpecificare la password \n DBLocation:\t\tSpecificare il percorso del database \n\n2. Caricamento di uno schema: java wt.stepdex.schemagenerator.STEPDbSchemaManager -loadschema [-overwrite] -e|-x schemaPath schemaName DBName DBContextLoginName DBPassword DBLocation \nDove\n -loadschema:\t\tComando per il caricamento di uno schema \n -overwrite:\t\tOpzione per sovrascrivere lo schema esistente\n -x:\t\tOpzione per caricare lo schema Express-x\n -e:\t\tOpzione per caricare lo schema Express\n schemaPath:\t\tSpecificare il percorso assoluto dello schema \n schemaName:\t\tSpecificare il nome dello schema \n DBName:\t\tSpecificare il nome del database\n DBContextLoginName:\tSpecificare il nome di accesso al contesto del database\n DBPassword:\t\tSpecificare la password \n DBLocation:\t\tSpecificare il percorso del database \n\n3. Eliminazione di uno schema: java wt.stepdex.schemagenerator.STEPDbSchemaManager -deleteschema -e|-x schemaName DBName DBContextLoginName DBPassword DBLocation \nDove\n -deleteschema:\tComando per l'eliminazione di uno schema \n -x:\t\tOpzione per eliminare lo schema Express-x\n -e:\t\tOpzione per eliminare lo schema Express\n schemaName:\t\tSpecificare il nome dello schema \n DBName:\t\tSpecificare il nome del database\n DBContextLoginName:\tSpecificare il nome di accesso al contesto del database\n DBPassword:\t\tSpecificare la password \n DBLocation:\t\tSpecificare il percorso del database \n\n4. Guida: java wt.stepdex.schemagenerator.STEPDbSchemaManager  -help")
    @RBComment("Error/Alert message thrown when need help to do schema or database related activities")
    public static final String OPTIONS = "OPTIONS";

    @RBEntry("Lo schema {0} esiste già nel database EXPRESS")
    @RBArgComment0("Name of the EXPRESS schema. schema refers to grammatical representation of a data model.")
    @RBComment("Error message thrown if a given EXPRESS schema is exist in EXPRESS database.Here EXPRESS refers to a modelling language.")
    public static final String SCHEMA_ALREADY_EXIST = "SCHEMA_ALREADY_EXIST";

    @RBEntry("Tipo di schema {0} non corretto")
    @RBArgComment0("schema type. schema refers to grammatical representation of a data model.")
    @RBComment("Error message thrown if a given EXPRESS schema type is supported. Here EXPRESS refers to a modelling language.")
    public static final String INCORRECT_SCHEMA_TYPE = "INCORRECT_SCHEMA_TYPE";

    @RBEntry("Caricamento dello schema {0} completato")
    @RBArgComment0("Name of the schema")
    @RBComment("Confirmation message thrown if Mapping schema is loaded in EXPRESS database. Here EXPRESS-X/EXPRESS refers to schema Language provided by third party. Mapping is a technical term refers to element mapping between two distinct data models.")
    public static final String SCHEMA_LOADED_SUCCESSFULLY = "SCHEMA_LOADED_SUCCESSFULLY";

    @RBEntry("Eliminazione dello schema {0} completata")
    @RBArgComment0("Name of the schema")
    @RBComment("Confirmation message thrown if Mapping schema is deleted from EXPRESS database. Here EXPRESS-X/EXPRESS refers to schema Language provided by third party. Mapping is a technical term refers to element mapping between two distinct data models.")
    public static final String SCHEMA_DELETED_SUCCESSFULLY = "SCHEMA_DELETED_SUCCESSFULLY";
    
    @RBEntry("Percorso o directory file specificato {0} non corretto.")
    public static final String FILE_NOT_FOUND = "FILE_NOT_FOUND";
    
    @RBEntry("Argomenti non validi")
    public static final String INVALID_ARGUMENT = "INVALID_ARGUMENT";
    
    @RBEntry("Log EDM eliminati disponibili in {0}")
    @RBArgComment0("EDM log file location")
    @RBComment("EDM is not translatable")
    public static final String EDM_LOG_LOCATION = "EDM_LOG_LOCATION";

    @RBEntry("Creazione del database {0} completata")
    @RBArgComment0("Name of database")
    public static final String DB_CREATED_SUCCESSFULLY = "DB_CREATED_SUCCESSFULLY";

}
