/* bcwti
 *
 * Copyright (c) 2012 Parametric Technology Corporation (PTC). All Rights
 * Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.stepdex.schemagenerator;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

/**
 * 
 * STEP database and schema activity specific resource bundle
 * 
 * <BR>
 * <BR>
 * <B>Supported API: </B>false <BR>
 * <BR>
 * <B>Extendable: </B>false
 */
@RBUUID("wt.stepdex.schemagenerator.STEPDbSchemaManagerResource")
public class STEPDbSchemaManagerResource extends WTListResourceBundle {

    @RBEntry("\nUsage:\n\n1. Create Database:  java wt.stepdex.schemagenerator.STEPDbSchemaManager -createdb [-overwrite] DBName DBContextLoginName DBPassword DBLocation \nWhere\n -createdb:\t\tCommand to create database \n -overwrite:\t\tTo overwrite existing database\n DBName:\t\tSpecify database name\n DBContextLoginName:\tSpecify database context login name\n DBPassword:\t\tSpecify the database password \n DBLocation:\t\tSpecify path to database \n \n2. Load schema: java wt.stepdex.schemagenerator.STEPDbSchemaManager -loadschema [-overwrite] -e|-x schemaPath schemaName DBName DBContextLoginName DBPassword DBLocation \nWhere\n -loadschema:\t\tCommand to load schema \n -overwrite:\t\tTo overwrite existing schema\n -x:\t\t\tTo provide Express-x Schema to load\n -e:\t\t\tTo provide Express schema to load\n schemaPath:\tSpecify absolute path to schema \n schemaName:\t\tSpecify the name of schema \n DBName:\t\tSpecify database name\n DBContextLoginName:\tSpecify database context login name\n DBPassword:\t\tSpecify the database password\n DBLocation:\t\tSpecify path to database \n\n3. Delete schema: java wt.stepdex.schemagenerator.STEPDbSchemaManager -deleteschema -e|-x schemaName DBName DBContextLoginName DBPassword DBLocation \nWhere\n -deleteschema:\t\tCommand to delete Schema \n -x:\t\t\tTo provide Express-x schema to delete\n -e:\t\t\tTo provide Express schema to delete\n schemaName:\t\tSpecify the name of schema \n DBName:\t\tSpecify database name\n DBContextLoginName:\tSpecify database context login name\n DBPassword:\t\tSpecify the database password\n DBLocation:\t\tSpecify path to database \n\n4. Help: java wt.stepdex.schemagenerator.STEPDbSchemaManager  -help")
    @RBComment("Error/Alert message thrown when need help to do schema or database related activities")
    public static final String OPTIONS = "OPTIONS";

    @RBEntry("{0} schema already exist in EXPRESS database")
    @RBArgComment0("Name of the EXPRESS schema. schema refers to grammatical representation of a data model.")
    @RBComment("Error message thrown if a given EXPRESS schema is exist in EXPRESS database.Here EXPRESS refers to a modelling language.")
    public static final String SCHEMA_ALREADY_EXIST = "SCHEMA_ALREADY_EXIST";

    @RBEntry("{0} incorrect schema type found")
    @RBArgComment0("schema type. schema refers to grammatical representation of a data model.")
    @RBComment("Error message thrown if a given EXPRESS schema type is supported. Here EXPRESS refers to a modelling language.")
    public static final String INCORRECT_SCHEMA_TYPE = "INCORRECT_SCHEMA_TYPE";

    @RBEntry("{0} schema loaded successfully")
    @RBArgComment0("Name of the schema")
    @RBComment("Confirmation message thrown if Mapping schema is loaded in EXPRESS database. Here EXPRESS-X/EXPRESS refers to schema Language provided by third party. Mapping is a technical term refers to element mapping between two distinct data models.")
    public static final String SCHEMA_LOADED_SUCCESSFULLY = "SCHEMA_LOADED_SUCCESSFULLY";

    @RBEntry("{0} schema deleted successfully")
    @RBArgComment0("Name of the schema")
    @RBComment("Confirmation message thrown if Mapping schema is deleted from EXPRESS database. Here EXPRESS-X/EXPRESS refers to schema Language provided by third party. Mapping is a technical term refers to element mapping between two distinct data models.")
    public static final String SCHEMA_DELETED_SUCCESSFULLY = "SCHEMA_DELETED_SUCCESSFULLY";
    
    @RBEntry("Specified file path/directory {0} is incorrect.")
    public static final String FILE_NOT_FOUND = "FILE_NOT_FOUND";
    
    @RBEntry("Invalid arguments found")
    public static final String INVALID_ARGUMENT = "INVALID_ARGUMENT";
    
    @RBEntry("Detailed EDM logs can be found at {0}")
    @RBArgComment0("EDM log file location")
    @RBComment("EDM is not translatable")
    public static final String EDM_LOG_LOCATION = "EDM_LOG_LOCATION";

    @RBEntry("{0} database created successfully")
    @RBArgComment0("Name of database")
    public static final String DB_CREATED_SUCCESSFULLY = "DB_CREATED_SUCCESSFULLY";

}
