/* bcwti
 *
 * Copyright (c) 2012 Parametric Technology Corporation (PTC). All Rights
 * Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.stepdex.expressschemautil;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBArgComment1;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

/**
 * 
 * STEP database and schema activity specific resource bundle
 * 
 * <BR>
 * <BR>
 * <B>Supported API: </B>false <BR>
 * <BR>
 * <B>Extendable: </B>false
 */
@RBUUID("wt.stepdex.expressschemautil.ExpressDbSchemaResource")
public class ExpressDbSchemaResource_it extends WTListResourceBundle {

    @RBEntry("Tipo di schema {0} non corretto")
    @RBArgComment0("schema type. schema refers to grammatical representation of a data model.")
    @RBComment("Error message thrown if a given EXPRESS schema type is supported. Here EXPRESS refers to a modelling language.")
    public static final String INCORRECT_SCHEMA_TYPE = "INCORRECT_SCHEMA_TYPE";

    @RBEntry("Caricamento dello schema completato")
    public static final String SCHEMA_LOADED_SUCCESSFULLY = "SCHEMA_LOADED_SUCCESSFULLY";

    @RBEntry("Eliminazione dello schema {0} completata")
    @RBArgComment0("Name of the schema")
    public static final String SCHEMA_DELETED_SUCCESSFULLY = "SCHEMA_DELETED_SUCCESSFULLY";

    @RBEntry("Percorso o directory file specificato {0} non corretto.")
    public static final String FILE_NOT_FOUND = "FILE_NOT_FOUND";

    @RBEntry("Argomenti non validi")
    public static final String INVALID_ARGUMENT = "INVALID_ARGUMENT";

    @RBEntry("Log EDM eliminati disponibili in {0}")
    @RBArgComment0("EDM log file location")
    @RBComment("EDM is not translatable")
    public static final String EDM_LOG_LOCATION = "EDM_LOG_LOCATION";

    @RBEntry("Creazione del database {0} completata")
    @RBArgComment0("Name of database")
    public static final String DB_CREATED_SUCCESSFULLY = "DB_CREATED_SUCCESSFULLY";

    @RBEntry("Il file delle proprietà non contiene tutte o una delle configurazioni obbligatorie seguenti: nome del database, posizione del database, nome utente e password.")
    public static final String INVALID_PROPERTY_FILE_CONFIGURATION = "INVALID_PROPERTY_FILE_CONFIGURATION";

    @RBEntry("Caricamento delle proprietà del server completato")
    public static final String SERVER_PROPERTY_LOADED_SUCCESSFULLY = "SERVER_PROPERTY_LOADED_SUCCESSFULLY";
    
    @RBEntry("Il file STEP {0} non esiste")
    @RBComment("Error message thrown if STEP file is not found")
    @RBArgComment0("STEP file path")
    public static final String LOAD_MODEL_FILE_NOT_FOUND = "LOAD_MODEL_FILE_NOT_FOUND";

    @RBEntry("Lo schema EXPRESS {0} non esiste")
    @RBComment("Error message thrown if EXPRESS Schema not found in STEP Database")
    @RBArgComment0("Name of the EXPRESS schema")
    public static final String LOAD_MODEL_SCHEMA_NOT_FOUND = "LOAD_MODEL_SCHEMA_NOT_FOUND";

    @RBEntry("Il modello {0} esiste già. Verificare il nome del modello o utilizzare l'opzione di sovrascrittura")
    @RBComment("Error message thrown a model with given name is already exist in STEP Database. Here Model is noun refers to java object name used in STEP Database")
    @RBArgComment0("Name of the EXPRESS schema")
    public static final String LOAD_MODEL_ALREADY_EXISTS = "LOAD_MODEL_ALREADY_EXISTS";
    
    @RBEntry("\nUtilizzo caricamento schema:\n\n java wt.stepdex.expressschemautil.ExpressDbSchemaManager -loadschema [-overwrite] -type(-t) -e|-x|-q -schemafile(-s)  [-dbname(-d) -user(-u) -password(-p) -dblocation(-l)] \nDove\n -loadschema:\t\tOpzione per caricare un nuovo schema \n -overwrite:\t\tOpzione per sovrascrivere uno schema esistente\n -t:\t\t\tTipo di schema. Può essere -e per lo schema EXPRESS, -x per lo schema EXPRESS-X e -q per lo schema INTERROGAZIONE.\n schemafile:\t\tSpecificare il percorso assoluto dello schema\n dbname:\t\tSpecificare il nome del database\n user:\t\t\tSpecificare il nome di accesso al contesto del database\n password:\t\tSpecificare la password del database\n dblocation:\t\tSpecificare il percorso del database \n")
    public static final String LOAD_SCHEMA_USAGE = "LOAD_SCHEMA_USAGE";

    @RBEntry("\n\nUtilizzo eliminazione schema:\n\n java wt.stepdex.expressschemautil.ExpressDbSchemaManager -deleteschema -type(-t) -e|-x -schemaname(-n) [-dbname(-d)  -user(-u) -password(-p) -dblocation(-l)] \nDove\n -deleteschema:\t\tOpzione per eliminare uno schema \n -t:\t\t\tTipo di schema. Può essere -e per lo schema EXPRESS e -x per lo schema EXPRESS-X\n schemaname:\t\tSpecificare il nome dello schema \n dbname:\t\tSpecificare il nome del database\n user:\t\t\tSpecificare il nome di accesso al contesto del database\n password:\t\tSpecificare la password del database\n dblocation:\t\tSpecificare il percorso del database \n")
    public static final String DELETE_SCHAME_USGAE = "DELETE_SCHAME_USGAE";

    @RBEntry("\nUtilizzo creazione database:\n\n java wt.stepdex.expressschemautil.ExpressDbSchemaManager -createdb [-overwrite] [-dbname(-d) -user(-u) -password(-p)  -dblocation(-l)] \nDove\n -createdb:\t\tOpzione per creare un nuovo database EDM \n -overwrite:\t\tOpzione per sovrascrivere un database esistente\n dbname:\t\tSpecificare il nome del database\n user:\t\t\tSpecificare il nome di accesso al contesto del database\n password:\t\tSpecificare la password del database \n dblocation:\t\tSpecificare il percorso del database \n")
    public static final String CREATE_DB_OPTIONS = "CREATE_DB_OPTIONS";
    
    @RBEntry("\nUtilizzo caricamento modello:\n\n java wt.stepdex.expressschemautil.ExpressDbSchemaManager -loadmodel [-overwrite] -modelname(-m) -stepfile(-f)  [-dbname(-d) -user(-u) -password(-p) -dblocation(-l)] \nDove\n -loadmodel:\t\tOpzione per caricare un modello \n -overwrite:\t\tOpzione per sovrascrivere un modello esistente\n modelname:\t\tNome del modello\n stepfile:\t\tPercorso assoluto del file STEP. Può essere un file Part 28 o un file Part 21. Il tipo di file è identificato dall'estensione\n dbname:\t\tSpecificare il nome del database\n user:\t\t\tSpecificare il nome di accesso al contesto del database\n password:\t\tSpecificare la password del database\n dblocation:\t\tSpecificare il percorso del database \n")
    public static final String LOAD_MODEL_USAGE = "LOAD_MODEL_OPTIONS";
    
    @RBEntry("Caricamento del modello {0} completato.")
    @RBArgComment0("Name of the model")
    @RBComment("The word Model is not translatable")
    public static final String LOAD_MODEL_SUCCESS = "LOAD_MODEL_SUCCESS";
    
    @RBEntry("I seguenti argomenti si escludono a vicenda e non possono essere forniti insieme: {0}")
    @RBComment("Error message thrown when mutually exclusive arguments are provided for commands")
    @RBArgComment0("Mutually exclusive pair of arguments")
    public static final String MUTUALLY_EXCLUSIVE_ARGUMENTS = "MUTUALLY_EXCLUSIVE_ARGUMENTS";

    @RBEntry("\nUtilizzo esecuzione interrogazione:\n\n java wt.stepdex.expressschemautil.ExpressDbSchemaManager -executequery -modelname(-m) -repository(-r) -queryfunction(-c) -queryschema(-y) [-dbname(-d) -user(-u) -password(-p) -dblocation(-l)] \nDove\n -executequery:\t\tOpzione per eseguire la funzione di interrogazione senza parametri. \n modelname:\t\tNome del modello su cui viene eseguita l'interrogazione.\n repository:\t\tRepository del modello.\n queryfunction:\t\tNome della funzione di interrogazione.\n queryschema:\t\tNome dello schema di interrogazione.\n dbname:\t\tSpecificare il nome del database\n user:\t\t\tSpecificare il nome di accesso al contesto del database\n password:\t\tSpecificare la password del database\n dblocation:\t\tSpecificare il percorso del database \n")
    public static final String EXECUTE_QUERY_USAGE = "EXECUTE_QUERY_USAGE";
    
    @RBEntry("L'esecuzione dell'interrogazione {0} è riuscita sul modello {1}")
    @RBArgComment0("Name of the query function")
    @RBArgComment1("Name of the model")
    public static final String EXECUTE_QUERY_SUCCESS = "EXECUTE_QUERY_SUCCESS";
}
