/**
 * 
 */
package wt.stepdex;

import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

/**
 * @author dbhandar
 *
 */
@RBUUID("wt.stepdex.stepdexPreferenceResource")
public class stepdexPreferenceResource_it extends WTListResourceBundle {
    
      /**
        * STEP common messages
        * PREF_CATEGORY_STEP.constant=PREF_CATEGORY_STEP
        **/
        @RBEntry("STEP")
        @RBComment("Do not translate STEP")
        public static final String PRIVATE_CONSTANT_0 = "PREF_CATEGORY_STEP";

        @RBEntry("Attiva esportazione/importazione dati basata su STEP")
        @RBComment("Do not translate STEP")
        public static final String PRIVATE_CONSTANT_1 = "PREF_STEP_DISPALY";

        @RBEntry("Consente di utilizzare la funzionalità di esportazione/importazione basata sul toolkit STEP (schema EXPRESS)")
        @RBComment("Do not translate STEP toolkit and EXPRESS schema")
        public static final String PRIVATE_CONSTANT_2 = "PREF_STEP_DESCRIPTION";

        @RBEntry("Consente di utilizzare la funzionalità di esportazione/importazione basata sul toolkit STEP (schema EXPRESS). Per attivare la funzionalità, impostare questa preferenza su Sì. Per disattivarla, impostare questa preferenza su No.")
        @RBComment("Do not translate STEP toolkit and EXPRESS schema")
        public static final String PRIVATE_CONSTANT_3 = "PREF_STEP_LONGDESCRIPTION";

        @RBEntry("Preferenza scambio di dati STEP")
        @RBComment("Do not translate STEP")
        public static final String STEPDEX_PREFERENCES = "STEPDEX_PREFERENCES";

}
