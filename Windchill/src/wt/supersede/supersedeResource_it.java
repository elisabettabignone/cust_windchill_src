package wt.supersede;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBArgComment1;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.supersede.supersedeResource")
public final class supersedeResource_it extends WTListResourceBundle {
   @RBEntry("L'oggetto {0} è già sostituito dall'oggetto {1} e non può essere sostituito dalla stessa parte.")
   @RBComment("This exception is thrown if the system attempts to create a duplicate link.")
   @RBArgComment0("Display identity of the Role A object")
   @RBArgComment1("Display identity of the Role B object")
   public static final String SUPERSEDE_LINK_NOT_UNIQUE = "SUPERSEDE_LINK_NOT_UNIQUE";
   
   @RBEntry("Input raccolta oggetti obsoleti non valido. La raccolta deve contenere solo oggetti iterati.")
   @RBComment("This message is part of an exception that is thrown if an invalid collection is passed to the SupersedeService.createSupersedeGroup() API")
   public static final String INVALID_SUPERSEDED_COLLECTION = "INVALID_SUPERSEDED_COLLECTION";
   
   @RBEntry("Input non valido. Devono esserci almeno 1 oggetto obsoleto e 1 oggetto di sostituzione.")
   @RBComment("This message is part of an exception that is thrown if an attempt is made to do a supersession without providing at least 1 superseded object and 1 superseeding object")
   public static final String INVALID_SUPERSEDE_ROLES = "INVALID_SUPERSEDE_ROLES";

   /**
    * Load Supersede Strings
    */
   @RBEntry("L'oggetto obsoleto \"{0} {1}\" non esiste.")
   public static final String LOAD_SUPERSEDED_NOT_FOUND = "LOAD_SUPERSEDED_NOT_FOUND";
   
   @RBEntry("L'oggetto di sostituzione \"{0} {1}\" non esiste.")
   public static final String LOAD_SUPERSEDING_NOT_FOUND = "LOAD_SUPERSEDING_NOT_FOUND";
   
   @RBEntry("Uno o entrambi gli oggetti seguenti non esistono: \"{0} {1}\"/\"{2} {3}\"")
   public static final String SUPERSEDE_OBJECTS_NOT_FOUND = "SUPERSEDE_OBJECTS_NOT_FOUND";
   
   @RBEntry("Non esiste alcuna relazione di sostituzione tra gli oggetti \"{0}\" e \"{1}\".")
   public static final String NO_SUPERSEDE_RELATION_BETWEEN_OBJECTS= "NO_SUPERSEDE_RELATION_BETWEEN_OBJECTS";
   
   @RBEntry("Impossibile caricare la relazione di sostituzione a causa degli errori elencati sopra.")
   public static final String SUPERSEDE_RELATION_FAILED = "SUPERSEDE_RELATION_FAILED";
   
   @RBEntry("{0} non può essere sostituito da se stesso.")
   @RBComment("This exception is thrown if the system attempts to create a SupersedeLink where the superseded and superseding roles are played by the same object")
   @RBArgComment0("Display identity of the Role A object")
   public static final String CANNOT_SUPERSEDE_OBJECT_BY_ITSELF = "CANNOT_SUPERSEDE_OBJECT_BY_ITSELF";
}
