package wt.supersede;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBArgComment1;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.supersede.supersedeResource")
public final class supersedeResource extends WTListResourceBundle {
   @RBEntry("{0} is already superseded by {1} and cannot be superseded by the same part.")
   @RBComment("This exception is thrown if the system attempts to create a duplicate link.")
   @RBArgComment0("Display identity of the Role A object")
   @RBArgComment1("Display identity of the Role B object")
   public static final String SUPERSEDE_LINK_NOT_UNIQUE = "SUPERSEDE_LINK_NOT_UNIQUE";
   
   @RBEntry("Invalid superseded collection input.  The collection must only contain Iterated objects.")
   @RBComment("This message is part of an exception that is thrown if an invalid collection is passed to the SupersedeService.createSupersedeGroup() API")
   public static final String INVALID_SUPERSEDED_COLLECTION = "INVALID_SUPERSEDED_COLLECTION";
   
   @RBEntry("Invalid input.  There must be at least 1 superseded object and 1 superseding object.")
   @RBComment("This message is part of an exception that is thrown if an attempt is made to do a supersession without providing at least 1 superseded object and 1 superseeding object")
   public static final String INVALID_SUPERSEDE_ROLES = "INVALID_SUPERSEDE_ROLES";

   /**
    * Load Supersede Strings
    */
   @RBEntry("Superseded object \"{0} {1}\" does not exist.")
   public static final String LOAD_SUPERSEDED_NOT_FOUND = "LOAD_SUPERSEDED_NOT_FOUND";
   
   @RBEntry("Superseding object \"{0} {1}\" does not exist.")
   public static final String LOAD_SUPERSEDING_NOT_FOUND = "LOAD_SUPERSEDING_NOT_FOUND";
   
   @RBEntry("One or both of the following objects do not exist: \"{0} {1}\" / \"{2} {3}\"")
   public static final String SUPERSEDE_OBJECTS_NOT_FOUND = "SUPERSEDE_OBJECTS_NOT_FOUND";
   
   @RBEntry("No supersede relationship exists between objects \"{0}\" and \"{1}\".")
   public static final String NO_SUPERSEDE_RELATION_BETWEEN_OBJECTS= "NO_SUPERSEDE_RELATION_BETWEEN_OBJECTS";
   
   @RBEntry("Did not load the supersede relationship because of the errors listed above.")
   public static final String SUPERSEDE_RELATION_FAILED = "SUPERSEDE_RELATION_FAILED";
   
   @RBEntry("{0} cannot be superseded by itself.")
   @RBComment("This exception is thrown if the system attempts to create a SupersedeLink where the superseded and superseding roles are played by the same object")
   @RBArgComment0("Display identity of the Role A object")
   public static final String CANNOT_SUPERSEDE_OBJECT_BY_ITSELF = "CANNOT_SUPERSEDE_OBJECT_BY_ITSELF";
}
