/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.content;

import wt.util.resource.*;

@RBUUID("wt.content.FormatNameRB")
public final class FormatNameRB_it extends WTListResourceBundle {
   @RBEntry("File audio")
   public static final String PRIVATE_CONSTANT_0 = "Audio Sound Clip";

   @RBEntry("Immagine bitmap")
   public static final String PRIVATE_CONSTANT_1 = "Bitmap Image";

   @RBEntry("Cals")
   public static final String PRIVATE_CONSTANT_2 = "Cals";

   @RBEntry("Eseguibile")
   public static final String PRIVATE_CONSTANT_3 = "Executable";

   @RBEntry("Immagine GIF")
   public static final String PRIVATE_CONSTANT_4 = "GIF Image";

   @RBEntry("File HTML")
   public static final String PRIVATE_CONSTANT_5 = "HTML File";

   @RBEntry("Immagine JPEG")
   public static final String PRIVATE_CONSTANT_6 = "JPEG Image";

   @RBEntry("Immagine PNG")
   public static final String PRIVATE_CONSTANT_7 = "PNG Image";

   @RBEntry("Microsoft Excel")
   public static final String PRIVATE_CONSTANT_8 = "Microsoft Excel";

   @RBEntry("Microsoft PowerPoint")
   public static final String PRIVATE_CONSTANT_9 = "Microsoft PowerPoint";

   @RBEntry("Microsoft Word")
   public static final String PRIVATE_CONSTANT_10 = "Microsoft Word";

   @RBEntry("Microsoft Project")
   public static final String PRIVATE_CONSTANT_11 = "Microsoft Project";

   @RBEntry("Filmato MPEG")
   public static final String PRIVATE_CONSTANT_12 = "MPEG Movie Clip";

   @RBEntry("PDF")
   public static final String PRIVATE_CONSTANT_13 = "PDF";

   @RBEntry("Postscript")
   public static final String PRIVATE_CONSTANT_14 = "Postscript";

   @RBEntry("Rich Text")
   public static final String PRIVATE_CONSTANT_15 = "Rich Text";

   @RBEntry("SGML")
   public static final String PRIVATE_CONSTANT_16 = "SGML";

   @RBEntry("File testo")
   public static final String PRIVATE_CONSTANT_17 = "Text File";

   @RBEntry("TIFF")
   public static final String PRIVATE_CONSTANT_18 = "TIFF";

   @RBEntry("Suono formato Wave")
   public static final String PRIVATE_CONSTANT_19 = "Wave Sound";

   @RBEntry("ZIP")
   public static final String PRIVATE_CONSTANT_20 = "Zip";

   @RBEntry("Sconosciuto")
   public static final String PRIVATE_CONSTANT_21 = "Unknown";

   @RBEntry("VPF")
   public static final String PRIVATE_CONSTANT_22 = "VPF";

   @RBEntry("GBF")
   public static final String PRIVATE_CONSTANT_23 = "GBF";

   @RBEntry("GAF")
   public static final String PRIVATE_CONSTANT_24 = "GAF";

   @RBEntry("VRML")
   public static final String PRIVATE_CONSTANT_25 = "VRML";

   @RBEntry("Immagine CGM")
   public static final String PRIVATE_CONSTANT_26 = "CGM Image";

   @RBEntry("Immagine SLP")
   public static final String PRIVATE_CONSTANT_27 = "SLP Image";

   @RBEntry("Immagine STL")
   public static final String PRIVATE_CONSTANT_28 = "STL Image";

   @RBEntry("FRM")
   public static final String PRIVATE_CONSTANT_29 = "FRM";

   @RBEntry("REP")
   public static final String PRIVATE_CONSTANT_30 = "REP";

   @RBEntry("DGM")
   public static final String PRIVATE_CONSTANT_31 = "DGM";

   @RBEntry("LAY")
   public static final String PRIVATE_CONSTANT_32 = "LAY";

   @RBEntry("DRW")
   public static final String PRIVATE_CONSTANT_33 = "DRW";

   @RBEntry("PIC")
   public static final String PRIVATE_CONSTANT_34 = "PIC";

   @RBEntry("Package")
   public static final String PRIVATE_CONSTANT_35 = "Package";

   @RBEntry("Parte UGC")
   public static final String PRIVATE_CONSTANT_36 = "UGC Part";

   @RBEntry("Assieme UGC")
   public static final String PRIVATE_CONSTANT_37 = "UGC Assembly";

   @RBEntry("XML")
   public static final String PRIVATE_CONSTANT_38 = "XML";

   @RBEntry("DWG")
   public static final String PRIVATE_CONSTANT_39 = "DWG";

   @RBEntry("DXF")
   public static final String PRIVATE_CONSTANT_40 = "DXF";

   @RBEntry("CC")
   public static final String PRIVATE_CONSTANT_41 = "CC";

   @RBEntry("CCZ")
   public static final String PRIVATE_CONSTANT_42 = "CCZ";

   @RBEntry("EDZ")
   public static final String PRIVATE_CONSTANT_43 = "EDZ";

   @RBEntry("ED")
   public static final String PRIVATE_CONSTANT_44 = "ED";

   @RBEntry("PVZ")
   public static final String PRIVATE_CONSTANT_45 = "PVZ";

   @RBEntry("OL")
   public static final String PRIVATE_CONSTANT_46 = "OL";

   @RBEntry("NESSUNO")
   public static final String PRIVATE_CONSTANT_47 = "NONE";

   @RBEntry("Fileset")
   public static final String PRIVATE_CONSTANT_48 = "File Set";

   @RBEntry("Link URL")
   public static final String PRIVATE_CONSTANT_49 = "URL Link";

   @RBEntry("PROE")
   public static final String PRIVATE_CONSTANT_50 = "PROE";

   @RBEntry("CADDS5")
   public static final String PRIVATE_CONSTANT_51 = "CADDS5";

   @RBEntry("CATIA")
   public static final String PRIVATE_CONSTANT_52 = "CATIA";

   @RBEntry("Memorizzazione esterna")
   public static final String PRIVATE_CONSTANT_53 = "External Storage";

   @RBEntry("Microsoft Visio")
   public static final String PRIVATE_CONSTANT_54 = "Microsoft Visio";

   @RBEntry("Mathcad")
   public static final String PRIVATE_CONSTANT_55 = "Mathcad";

   @RBEntry("Definizione rilevamento interferenze")
   public static final String PRIVATE_CONSTANT_56 = "Interference Detection Definition";
   
   @RBEntry("Formati messaggio di Microsoft Outlook")
   public static final String PRIVATE_CONSTANT_57 = "Outlook Message Formats";

   @RBEntry("Cartella di lavoro di Microsoft Office Excel")
   public static final String PRIVATE_CONSTANT_58 = "Microsoft Office Excel 2007 workbook";

   @RBEntry("Cartella di lavoro con attivazione macro di Microsoft Office Excel")
   public static final String PRIVATE_CONSTANT_59 = "Office Excel 2007 macro-enabled workbook";

   @RBEntry("Modello di Microsoft Office Excel")
   public static final String PRIVATE_CONSTANT_60 = "Office Excel 2007 template";

   @RBEntry("Modello con attivazione macro di Microsoft Office Excel")
   public static final String PRIVATE_CONSTANT_61 = "Office Excel 2007 macro-enabled template";

   @RBEntry("Cartella di lavoro binaria di Microsoft Office Excel")
   public static final String PRIVATE_CONSTANT_62 = "Office Excel 2007 binary workbook";

   @RBEntry("Presentazione standard di Microsoft Office PowerPoint")
   public static final String PRIVATE_CONSTANT_63 = "Microsoft Office PowerPoint 2007 presentation";

   @RBEntry("Presentazione standard con attivazione macro di Microsoft Office PowerPoint")
   public static final String PRIVATE_CONSTANT_64 = "Office PowerPoint 2007 macro-enabled presentation";

   @RBEntry("Modello di Microsoft Office PowerPoint")
   public static final String PRIVATE_CONSTANT_65 = "Office PowerPoint 2007 template";

   @RBEntry("Modello con attivazione macro di Microsoft Office PowerPoint")
   public static final String PRIVATE_CONSTANT_66 = "Office PowerPoint 2007 macro-enabled template";

   @RBEntry("Solo presentazione di Microsoft Office PowerPoint")
   public static final String PRIVATE_CONSTANT_67 = "Office PowerPoint 2007 slide show";

   @RBEntry("Solo presentazione con attivazione macro di Microsoft Office PowerPoint")
   public static final String PRIVATE_CONSTANT_68 = "Office PowerPoint 2007 macro-enabled slide show";

   @RBEntry("Documento di Microsoft Office Word")
   public static final String PRIVATE_CONSTANT_69 = "Microsoft Office Word 2007 document";

   @RBEntry("Documento con attivazione macro di Microsoft Office Word")
   public static final String PRIVATE_CONSTANT_70 = "Office Word 2007 macro-enabled docu";

   @RBEntry("Modello di Microsoft Office Word")
   public static final String PRIVATE_CONSTANT_71 = "Office Word 2007 template";

   @RBEntry("Modello di documento con attivazione macro di Microsoft Office Word")
   public static final String PRIVATE_CONSTANT_72 = "Office Word 2007 macro-enabled document template";
}
