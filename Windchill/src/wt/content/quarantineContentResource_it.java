/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.content;

import wt.util.resource.*;

@RBUUID("wt.content.quarantineContentResource")
public final class quarantineContentResource_it extends WTListResourceBundle {
   @RBEntry("File di contenuto non trovato.")
   public static final String REASON_FILE_DNE = "REASON_FILE_DNE";

   @RBEntry("Le dimensioni effettive del file di contenuto non corrispondono a quelle specificate nei metadati.")
   public static final String REASON_FILE_SIZE_DIFF = "REASON_FILE_SIZE_DIFF";

   @RBEntry("Si è tentato di scaricare contenuto in quarantena. Le impostazioni correnti del sistema non consentono questa operazione.")
   public static final String QUARANTINED_CONTENT_MAYNOT_DOWNLOAD = "QUARANTINED_CONTENT_MAYNOT_DOWNLOAD";

   @RBEntry("Il contenuto è stato messo in quarantena.")
   public static final String NOTIFY_QUARANTINED_SUBJECT = "NOTIFY_QUARANTINED_SUBJECT";

   @RBEntry("Il sistema ha messo uno o più file di contenuto in quarantena. Per ulteriori informazioni, consultare la Guida dell'amministratore di Windchill.")
   public static final String NOTIFY_QUARANTINED_MSG_BODY = "NOTIFY_QUARANTINED_MSG_BODY";
}
