/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.content;

import wt.util.resource.*;

@RBUUID("wt.content.contentResource")
public final class contentResource extends WTListResourceBundle {
   @RBEntry("invalid format name passed - \"{0}\"")
   @RBArgComment0("refers to the name of a DataFormat object")
   public static final String INVALID_FORMAT_NAME = "1";

   @RBEntry("invalid request for icon")
   public static final String INVALID_ICON_REQUEST = "2";

   @RBEntry("HttpContentOperation does not contain a valid operation")
   public static final String INVALID_OPERATION = "3";

   @RBEntry("Cannot find ContentHolder for this operation")
   public static final String CONTENT_HOLDER_NOT_FOUND = "4";

   @RBEntry("Cannot find HttpContentOperation")
   public static final String HTTP_OP_NOT_FOUND = "5";

   @RBEntry("Cannot find file - \"{0}\"")
   @RBArgComment0("refers to the name of a file")
   public static final String INVALID_PATH = "6";

   @RBEntry("Query string passed is not valid")
   public static final String EMPTY_QUERY_STRING = "7";

   @RBEntry("The format of this content has not been set")
   public static final String FORMAT_NOT_SET = "8";

   @RBEntry("Cannot load properties file")
   public static final String CANNOT_LOAD_PROPERTIES = "9";

   @RBEntry("Cannot delete DataFormat objects")
   public static final String CANNOT_DELETE_DATAFORMAT = "10";

   @RBEntry("Initiate Upload")
   public static final String INITIATE_UPLOAD = "11";

   @RBEntry("Windchill Upload Results")
   public static final String UPLOAD_RESULTS = "12";

   @RBEntry("Upload was successful")
   public static final String UPLOAD_SUCCESS = "13";

   @RBEntry("The data passed up on an html form was not in the format expected")
   public static final String INVALID_FORM_DATA = "14";

   @RBEntry("Cannot initialize HttpContentOperation object")
   public static final String CANNOT_INIT_HTTP_OP = "15";

   @RBEntry("Access rules do not permit you to add content to this object")
   public static final String ACCESS_MODIFY_ERROR = "16";

   @RBEntry("Nothing passed for download.  See Windchill Administrator")
   public static final String NO_CONTENTS_TO_DOWNLOAD = "17";

   @RBEntry("Content Holder in operation object is null.  See Windchill Administrator")
   public static final String HOLDER_IS_NULL = "18";

   @RBEntry("ATTENTION: Secured Action. Access rules do not permit you to retrieve the content of this object")
   public static final String ACCESS_READ_ERROR = "19";

   @RBEntry("Cannot find HttpOperationItem")
   public static final String HTTP_OP_ITEM_NOT_FOUND = "20";

   @RBEntry("Cannot find ApplicationData object")
   public static final String APP_DATA_NOT_FOUND = "21";

   @RBEntry("Cannot find ContentHolder object")
   public static final String CONTENT_HOLDER_NOT_FOUND_ID = "22";

   @RBEntry("Cannot open template file")
   public static final String CANNOT_OPEN_TEMPLATE = "23";

   @RBEntry("ContentHtml class cannot generate the html in method - \"{0}\" without proper instatiation")
   @RBArgComment0("is a method name in the ContentHtml class")
   public static final String CONTENT_HTML_NOT_INIT = "24";

   @RBEntry("DataFormat with format name - \"{0}\" not found")
   public static final String DATA_FORMAT_NOT_FOUND = "25";

   @RBEntry("Added")
   public static final String ADDED = "26";

   @RBEntry("Replaced")
   public static final String REPLACED = "27";

   @RBEntry("NEW FILE")
   public static final String NEW_FILE = "28";

   @RBEntry("Your upload has timed out")
   public static final String UPLOAD_TIME_OUT = "29";

   @RBEntry("Your download has timed out")
   public static final String DOWNLOAD_TIME_OUT = "30";

   @RBEntry("The thread to wait for an upload or download cannot be initialized.  See Windchill Administrator")
   public static final String CANNOT_INIT_WAIT = "31";

   @RBEntry("The icon for format {0} is null.  See Windchill Administrator")
   @RBArgComment0("is the FormatName of a DataFormat object")
   public static final String NULL_ICON = "32";

   @RBEntry("Error initializing class {0}")
   @RBArgComment0("is the name of the class")
   public static final String ERROR_INITIALIZING = "33";

   @RBEntry("Unable to fill in the attributes for the data format in the object.")
   public static final String INFLATE_FAILED = "34";

   @RBEntry("KB")
   public static final String KILOBYTES = "35";

   @RBEntry("NEW LINK")
   public static final String NEW_URL = "36";

   @RBEntry("See Administrator.  Content Stream not correctly initialized")
   public static final String UNITIALIZED_CONTENT_STREAM = "37";

   @RBEntry("Aggregate")
   public static final String AGGREGATE_DISPLAY_NAME = "38";

   @RBEntry("Primary")
   public static final String PRIMARY_DISPLAY_NAME = "39";

   @RBEntry("Contents")
   public static final String CONTENT_TABLE_HEADER = "40";

   @RBEntry("The Aggregate is not persistant.  The Aggregate needs to be persisted first")
   public static final String AGG_NOT_PERSISTANT = "41";

   @RBEntry("The file with the name {0} does not exist or is of zero size")
   public static final String NON_EXISTANT_FILE = "42";

   @RBEntry("More than one thumbnail was found")
   public static final String MORE_THAN_ONE_THUMBNAIL = "43";

   @RBEntry("Cannot copy contents of a ContentHolder to another ContentHolder that already has contents")
   public static final String CANNOT_COPY_CONTENTS = "44";

   @RBEntry("Cannot copy a Primary content item to a ContentHolder that already has a Primary content item")
   public static final String CANNOT_COPY_PRIMARY_CONTENT = "45";

   @RBEntry("Cannot copy a content item that does not belong to a ContentHolder")
   public static final String CANNOT_COPY_CONTENT_ITEM = "46";

   @RBEntry("File Size")
   public static final String PRIMARY_FILE_SIZE_DISPLAY_NAME = "47";

   @RBEntry("Latest Iteration:")
   public static final String LATESTCONTENTLABEL = "48";

   @RBEntry("Downloading primary content...")
   public static final String PRIMARY_CONTENT_DOWNLOAD_MSG = "49";

   @RBEntry("ERROR: This function will only work on instances of FormatContentHolder.")
   public static final String NOT_FORMAT_CONTENTHOLDER = "50";

   @RBEntry("ERROR: This function will only work on instances of Iterated.")
   public static final String NOT_ITERATED = "51";

   @RBEntry("No primary content for this object.")
   public static final String NO_PRIMARY_CONTENT = "52";

   @RBEntry("ERROR: Item is type URLData but URL Location is NULL.")
   public static final String NO_URL_LOCATION = "53";

   @RBEntry("ERROR: Content is not downloadable.")
   public static final String NOT_DOWNLOADABLE = "54";

   @RBEntry("There is no content on cache server.")
   public static final String NO_CONTENT_ON_CACHE_SERVER = "55";

   @RBEntry("Primary Content")
   public static final String PRIMARY_LABEL = "56";

   @RBEntry("Downloading...")
   public static final String DOWNLOADING = "57";

   @RBEntry("Duplicate file name:\"{0}\". Set Content's filename failed.")
   public static final String DUPLICATE_FILE_NAME = "58";

   @RBEntry("...")
   @RBPseudo(false)
   public static final String ELLIPSES = "59";

   @RBEntry("Click OK to install Desktop Integration or Cancel to continue the download without installing Desktop Integration.")
   public static final String DTI_INSTALL_PROMPT = "60";

   @RBEntry("Operation could not be completed because content transfer is in progress. Please re-try shortly.")
   public static final String OPERATION_NOT_COMPLETE_CONTENT_TRANSFER = "61";

   @RBEntry("Click OK to allow Java Plug-In installation for intelligent applet download, or Cancel to use default browser download.")
   public static final String JPI_INSTALL_PROMPT = "62";

   @RBEntry("ATTENTION: This Windchill download operation was not completed because the download window could not be launched.  Please consult your system administrator or see Windchill TPI bulletin #127827 for current browser configuration instructions.")
   @RBComment("Displayed when popup blocker prevents window launch.  ")
   public static final String POPUP_BLOCKER_RESPONSE = "63";

   @RBEntry("ATTENTION: Secured Action. Content modify access denied. You do not have permission to modify \"{0}\". ")
   public static final String ACCESS_MODIFY_CONTENT_ERROR = "64";

   @RBEntry("ATTENTION: Secured Action. File download access denied. You do not have permission to download this file. ")
   public static final String ACCESS_DOWNLOAD_ERROR = "65";

   @RBEntry("Can not add more than one primary content to the object.")
   public static final String HAS_PRIMARY = "66";

   @RBEntry("Empty file is not allowed.")
   public static final String EMPTY_FILE = "67";

   @RBEntry("File upload failed - Error while writing to the folder.\n Please retry or contact your administrator if this issue persists.")
   public static final String FOLDER_ERROR_ON_CACHE_SERVER = "68";

   @RBEntry("ATTENTION: Secured Action. File download access denied. Content requested is not a content of the content holder object.")
   public static final String ACCESS_DOWNLOAD_MISMATCH_ERROR = "69";

   @RBEntry("Operation cannot be performed as some objects are locked by another operation.")
   public static final String OBJECTS_LOCKED_BY_ANOTHER_OPERATION = "70";

   @RBEntry("Lock cannot be obtained for some objects.")
   public static final String LOCK_CANNOT_BE_OBTAINED = "71";

   @RBEntry("ATTENTION: Secured Action. File download access denied. You do not have permission to download content of document - {0}.")
   public static final String DOC_ACCESS_DOWNLOAD_ERROR = "72";

   @RBEntry("Required Information Is Missing \n\nOne or more required fields are empty. \nEnter information for all fields indicated by an asterisk (*).")
   public static final String REQUIRED_FIELDS_WARNING = "73";
   
   @RBEntry("Cannot find chapter - \"{0}\"")
   public static final String INVALID_CHAPTER = "74";
}
