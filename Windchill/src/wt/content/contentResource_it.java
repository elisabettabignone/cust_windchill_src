/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.content;

import wt.util.resource.*;

@RBUUID("wt.content.contentResource")
public final class contentResource_it extends WTListResourceBundle {
   @RBEntry("Nome formato inviato non valido - \"{0}\"")
   @RBArgComment0("refers to the name of a DataFormat object")
   public static final String INVALID_FORMAT_NAME = "1";

   @RBEntry("richiesta di icona non valida")
   public static final String INVALID_ICON_REQUEST = "2";

   @RBEntry("L'oggetto HttpContentOperation non contiene un'operazione valida")
   public static final String INVALID_OPERATION = "3";

   @RBEntry("Impossibile trovare l'oggetto ContentHolder per l'operazione")
   public static final String CONTENT_HOLDER_NOT_FOUND = "4";

   @RBEntry("Impossibile trovare l'oggetto HttpContentOperation")
   public static final String HTTP_OP_NOT_FOUND = "5";

   @RBEntry("Impossibile trovare il file: \"{0}\"")
   @RBArgComment0("refers to the name of a file")
   public static final String INVALID_PATH = "6";

   @RBEntry("La stringa trasferita per l'interrogazione non è valida")
   public static final String EMPTY_QUERY_STRING = "7";

   @RBEntry("Il formato di questo contenuto non è stato impostato")
   public static final String FORMAT_NOT_SET = "8";

   @RBEntry("Impossibile caricare il file delle proprietà")
   public static final String CANNOT_LOAD_PROPERTIES = "9";

   @RBEntry("Impossibile eliminare gli oggetti DataFormat")
   public static final String CANNOT_DELETE_DATAFORMAT = "10";

   @RBEntry("Avvio del caricamento")
   public static final String INITIATE_UPLOAD = "11";

   @RBEntry("Risultati caricamento Windchill")
   public static final String UPLOAD_RESULTS = "12";

   @RBEntry("Caricamento completato")
   public static final String UPLOAD_SUCCESS = "13";

   @RBEntry("I dati trasferiti in un modulo html non erano nel formato richiesto")
   public static final String INVALID_FORM_DATA = "14";

   @RBEntry("Impossibile inizializzare l'oggetto HttpContentOperation")
   public static final String CANNOT_INIT_HTTP_OP = "15";

   @RBEntry("Le regole di accesso non consentono di aggiungere il contenuto all'oggetto")
   public static final String ACCESS_MODIFY_ERROR = "16";

   @RBEntry("Impossibile scaricare il contenuto. Vedere Amministrazione Windchill")
   public static final String NO_CONTENTS_TO_DOWNLOAD = "17";

   @RBEntry("L'oggetto ContentHolder dell'operazione è nullo. Vedere Amministrazione Windchill")
   public static final String HOLDER_IS_NULL = "18";

   @RBEntry("ATTENZIONE: azione protetta. Le regole di accesso non consentono di recuperare il contenuto dell'oggetto")
   public static final String ACCESS_READ_ERROR = "19";

   @RBEntry("Impossibile trovare l'elemento HttpOperationItem")
   public static final String HTTP_OP_ITEM_NOT_FOUND = "20";

   @RBEntry("Impossibile trovare l'oggetto ApplicationData")
   public static final String APP_DATA_NOT_FOUND = "21";

   @RBEntry("Impossibile trovare l'oggetto ContentHolder")
   public static final String CONTENT_HOLDER_NOT_FOUND_ID = "22";

   @RBEntry("Impossibile aprire il file modello")
   public static final String CANNOT_OPEN_TEMPLATE = "23";

   @RBEntry("La classe ContentHtml non può generare l'html nel metodo \"{0}\" senza una corretta istanziazione")
   @RBArgComment0("is a method name in the ContentHtml class")
   public static final String CONTENT_HTML_NOT_INIT = "24";

   @RBEntry("Oggetto DataFormat con nome di formato \"{0}\" non trovato")
   public static final String DATA_FORMAT_NOT_FOUND = "25";

   @RBEntry("Aggiunto")
   public static final String ADDED = "26";

   @RBEntry("Sostituito")
   public static final String REPLACED = "27";

   @RBEntry("NUOVO FILE")
   public static final String NEW_FILE = "28";

   @RBEntry("Raggiunto tempo massimo per il caricamento")
   public static final String UPLOAD_TIME_OUT = "29";

   @RBEntry("Raggiunto tempo massimo per lo scaricamento")
   public static final String DOWNLOAD_TIME_OUT = "30";

   @RBEntry("Impossibile inizializzare il thread di attesa per un'operazione di caricamento o scaricamento.  Vedere Amministrazione Windchill")
   public static final String CANNOT_INIT_WAIT = "31";

   @RBEntry("L'icona per il formato {0} è nulla. Vedere Amministrazione Windchill")
   @RBArgComment0("is the FormatName of a DataFormat object")
   public static final String NULL_ICON = "32";

   @RBEntry("Errore durante l'inizializzazione della classe {0}")
   @RBArgComment0("is the name of the class")
   public static final String ERROR_INITIALIZING = "33";

   @RBEntry("Impossibile completare gli attributi per il formato dati nell'oggetto.")
   public static final String INFLATE_FAILED = "34";

   @RBEntry("KB")
   public static final String KILOBYTES = "35";

   @RBEntry("NUOVO LINK")
   public static final String NEW_URL = "36";

   @RBEntry("Consultare l'Amministratore. Il flusso dei file associati all'oggetto non è stato correttamente inizializzato.")
   public static final String UNITIALIZED_CONTENT_STREAM = "37";

   @RBEntry("Aggrega")
   public static final String AGGREGATE_DISPLAY_NAME = "38";

   @RBEntry("Principale")
   public static final String PRIMARY_DISPLAY_NAME = "39";

   @RBEntry("Contenuto")
   public static final String CONTENT_TABLE_HEADER = "40";

   @RBEntry("Aggregato non persistente. L'aggregato deve essere reso persistente.")
   public static final String AGG_NOT_PERSISTANT = "41";

   @RBEntry("Il file {0} non esiste o ha dimensione zero.")
   public static final String NON_EXISTANT_FILE = "42";

   @RBEntry("È stata trovata più diuna miniatura")
   public static final String MORE_THAN_ONE_THUMBNAIL = "43";

   @RBEntry("Impossibile copiare il contenuto di un contenitore in un altro che ne ha già uno")
   public static final String CANNOT_COPY_CONTENTS = "44";

   @RBEntry("Impossibile copiare un elemento di contenuto principale in un contenitore che ne ha già uno")
   public static final String CANNOT_COPY_PRIMARY_CONTENT = "45";

   @RBEntry("Impossibile copiare un elemento di contenuto che non appartiene ad un contenitore")
   public static final String CANNOT_COPY_CONTENT_ITEM = "46";

   @RBEntry("Dimensione file")
   public static final String PRIMARY_FILE_SIZE_DISPLAY_NAME = "47";

   @RBEntry("Ultima iterazione:")
   public static final String LATESTCONTENTLABEL = "48";

   @RBEntry("Scaricamento del contenuto principale in corso...")
   public static final String PRIMARY_CONTENT_DOWNLOAD_MSG = "49";

   @RBEntry("ERRORE: questa funzione opera esclusivamente su istanze di FormatContentHolder.")
   public static final String NOT_FORMAT_CONTENTHOLDER = "50";

   @RBEntry("ERRORE: questa funzione opera esclusivamente su istanze di Iterated.")
   public static final String NOT_ITERATED = "51";

   @RBEntry("L'oggetto non ha un contenuto principale.")
   public static final String NO_PRIMARY_CONTENT = "52";

   @RBEntry("ERRORE: l'elemento è di tipo dati URL ma la posizione dell'URL è NULLA.")
   public static final String NO_URL_LOCATION = "53";

   @RBEntry("ERRORE: contenuto non scaricabile")
   public static final String NOT_DOWNLOADABLE = "54";

   @RBEntry("Nessun contenuto sul server cache")
   public static final String NO_CONTENT_ON_CACHE_SERVER = "55";

   @RBEntry("Contenuto principale")
   public static final String PRIMARY_LABEL = "56";

   @RBEntry("Scaricamento in corso...")
   public static final String DOWNLOADING = "57";

   @RBEntry("Nome file duplicato:\"{0}\". L'impostazione del nome di file di contenuto è fallita.")
   public static final String DUPLICATE_FILE_NAME = "58";

   @RBEntry("...")
   @RBPseudo(false)
   public static final String ELLIPSES = "59";

   @RBEntry("Fare clic su OK per installare Desktop Integration o Annulla per continuare a scaricare senza l'installazione di Desktop Integration.")
   public static final String DTI_INSTALL_PROMPT = "60";

   @RBEntry("Impossibile completare l'operazione. Trasferimento dei dati in corso. Riprovare più tardi.")
   public static final String OPERATION_NOT_COMPLETE_CONTENT_TRANSFER = "61";

   @RBEntry("Fare clic su OK per consentire l'installazione del plug-in Java per il download intelligente di applet, oppure su Annulla per utilizzare la funzionalità di download del browser.")
   public static final String JPI_INSTALL_PROMPT = "62";

   @RBEntry("ATTENZIONE: l'operazione di scaricamento non è stata completata in quanto non è stato possibile aprire la finestra di scaricamento. Contattare l'amministratore di sistema o consultare il bollettino Windchill TPI n. 127827 per istruzioni aggiornate sulla configurazione del browser.")
   @RBComment("Displayed when popup blocker prevents window launch.  ")
   public static final String POPUP_BLOCKER_RESPONSE = "63";

   @RBEntry("ATTENZIONE: azione protetta. Accesso al contenuto per la modifica negato. Non si dispone dei permessi per modificare \"{0}\". ")
   public static final String ACCESS_MODIFY_CONTENT_ERROR = "64";

   @RBEntry("ATTENZIONE: azione protetta. Accesso al file per lo scaricamento negato. Non si dispone dei permessi per scaricare il file.")
   public static final String ACCESS_DOWNLOAD_ERROR = "65";

   @RBEntry("Impossibile assegnare più di un contenuto principale per l'oggetto.")
   public static final String HAS_PRIMARY = "66";

   @RBEntry("Non sono consentiti file vuoti.")
   public static final String EMPTY_FILE = "67";

   @RBEntry("Caricamento file non riuscito - errore durante la scrittura nella cartella.\n Riprovare. Se il problema persiste, contattare l'amministratore.")
   public static final String FOLDER_ERROR_ON_CACHE_SERVER = "68";

   @RBEntry("ATTENZIONE: azione protetta. Accesso al file per lo scaricamento negato. Il contenuto richiesto non fa parte dell'oggetto contenitore.")
   public static final String ACCESS_DOWNLOAD_MISMATCH_ERROR = "69";

   @RBEntry("Impossibile eseguire l'operazione. Alcuni oggetti sono bloccati da un'altra operazione.")
   public static final String OBJECTS_LOCKED_BY_ANOTHER_OPERATION = "70";

   @RBEntry("Impossibile ottenere un blocco per alcuni oggetti.")
   public static final String LOCK_CANNOT_BE_OBTAINED = "71";

   @RBEntry("ATTENZIONE: azione protetta. Accesso al file per lo scaricamento negato. Non si dispone dei permessi per scaricare il contenuto del documento - {0}.")
   public static final String DOC_ACCESS_DOWNLOAD_ERROR = "72";

   @RBEntry("Informazioni necessarie mancanti.\n\nUno o più campi obbligatori sono vuoti. \nCompletare tutti i campi contrassegnati da un asterisco (*).")
   public static final String REQUIRED_FIELDS_WARNING = "73";
   
   @RBEntry("Impossibile trovare il capitolo - \"{0}\"")
   public static final String INVALID_CHAPTER = "74";
}
