/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.content;

import wt.util.resource.*;

@RBUUID("wt.content.quarantineContentResource")
public final class quarantineContentResource extends WTListResourceBundle {
   @RBEntry("Content file not found.")
   public static final String REASON_FILE_DNE = "REASON_FILE_DNE";

   @RBEntry("Actual content file size does not match the size referenced in meta-data")
   public static final String REASON_FILE_SIZE_DIFF = "REASON_FILE_SIZE_DIFF";

   @RBEntry("Attempt to download quarantined content.  Download of quarantined content is not permitted under current system settings.")
   public static final String QUARANTINED_CONTENT_MAYNOT_DOWNLOAD = "QUARANTINED_CONTENT_MAYNOT_DOWNLOAD";

   @RBEntry("Content has been quarantined!")
   public static final String NOTIFY_QUARANTINED_SUBJECT = "NOTIFY_QUARANTINED_SUBJECT";

   @RBEntry("System marked one or more content files as quarantined.  For more information please see Windchill Administrator's Guide.")
   public static final String NOTIFY_QUARANTINED_MSG_BODY = "NOTIFY_QUARANTINED_MSG_BODY";
}
