/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.calendar;

import wt.util.resource.*;

@RBUUID("wt.calendar.calendarResource")
public final class calendarResource extends WTListResourceBundle {
   @RBEntry("Calendar")
   public static final String CALENDAR = "1";

   @RBEntry("ERROR! Cyclic delegations not allowed.")
   public static final String CYCLIC = "2";

   @RBEntry("Delegation Notification for {0}")
   public static final String DELEGATION_NOTIFICATION_SUBJECT = "3";

   @RBEntry("ERROR! Delegation to non-working day is not allowed.")
   public static final String DELEGATION_TO_NON_WORKING_DAY = "4";

   @RBEntry("Delegation Notification")
   public static final String DELEGATION_NOTIFICATION = "5";
}
