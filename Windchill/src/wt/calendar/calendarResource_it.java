/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.calendar;

import wt.util.resource.*;

@RBUUID("wt.calendar.calendarResource")
public final class calendarResource_it extends WTListResourceBundle {
   @RBEntry("Calendario")
   public static final String CALENDAR = "1";

   @RBEntry("ERRORE! Le delegazioni cicliche non sono consentite.")
   public static final String CYCLIC = "2";

   @RBEntry("Notifica di delega per {0}")
   public static final String DELEGATION_NOTIFICATION_SUBJECT = "3";

   @RBEntry("ERRORE. La delega a un giorno non lavorativo non è consentita.")
   public static final String DELEGATION_TO_NON_WORKING_DAY = "4";

   @RBEntry("Notifica di delega")
   public static final String DELEGATION_NOTIFICATION = "5";
}
