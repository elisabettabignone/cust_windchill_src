/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.help;

import wt.util.resource.*;

@RBUUID("wt.help.helpResource")
public final class helpResource_it extends WTListResourceBundle {
   @RBEntry("Differimento invalido")
   public static final String INVALID_DELAY = "0";
}
