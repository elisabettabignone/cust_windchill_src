package wt.help.testhelp;

import wt.util.resource.*;

public final class TestHelpResources_it extends WTListResourceBundle {
   /**
    * -----Helpdesk Defaults-----
    **/
   @RBEntry("wt/help/testhelp/contents.html")
   public static final String PRIVATE_CONSTANT_0 = "Contents/TestHelpSystem";

   @RBEntry("wt/help/testhelp/contents.html")
   public static final String PRIVATE_CONSTANT_1 = "Help/TestHelpSystem";

   @RBEntry("Acquista titoli")
   public static final String PRIVATE_CONSTANT_2 = "Tip/TestHelpSystem";

   @RBEntry("Acquista a basso prezzo, vendi ad un prezzo elevato; se non sale non comprare.")
   public static final String PRIVATE_CONSTANT_3 = "Desc/TestHelpSystem";

   /**
    * ------Menu Options-----
    **/
   @RBEntry("Crea un nuovo report dell'incidente.")
   public static final String PRIVATE_CONSTANT_4 = "Desc/TestHelpSystem//createButton";

   @RBEntry("Descrizione di createContextField.")
   public static final String PRIVATE_CONSTANT_5 = "Desc/TestHelpSystem//createContextField";

   @RBEntry("Visualizza i report degli incidenti esistenti")
   public static final String PRIVATE_CONSTANT_6 = "Desc/TestHelpSystem//viewButton";

   @RBEntry("Esci dall'applicazione TestHelp")
   public static final String PRIVATE_CONSTANT_7 = "Desc/TestHelpSystem/TestHelpContext/Exit";

   @RBEntry("Mostra l'indice della guida per questo contesto")
   public static final String PRIVATE_CONSTANT_8 = "Desc/TestHelpSystem/TestHelpContext/Help";

   @RBEntry("Cerca i report degli incidenti esistenti")
   public static final String PRIVATE_CONSTANT_9 = "Desc/TestHelpSystem/TestHelpContext/Search";

   @RBEntry("Descrizione di createContextField.")
   public static final String PRIVATE_CONSTANT_10 = "Desc/TestHelpSystem/TestHelpContext/searchContextField";

   @RBEntry("Crea report dell'incidente")
   public static final String PRIVATE_CONSTANT_11 = "Tip/TestHelpSystem//createButton";

   @RBEntry("Suggerimento per createContextField")
   public static final String PRIVATE_CONSTANT_12 = "Tip/TestHelpSystem//createContextField";

   @RBEntry("Visualizza i report degli incidenti")
   public static final String PRIVATE_CONSTANT_13 = "Tip/TestHelpSystem//viewButton";

   @RBEntry("Esci da TestHelp")
   public static final String PRIVATE_CONSTANT_14 = "Tip/TestHelpSystem/TestHelpContext/Exit";

   @RBEntry("Mostra l'indice della guida")
   public static final String PRIVATE_CONSTANT_15 = "Tip/TestHelpSystem/TestHelpContext/Help";

   @RBEntry("Cerca i report degli incidenti")
   public static final String PRIVATE_CONSTANT_16 = "Tip/TestHelpSystem/TestHelpContext/Search";

   @RBEntry("Suggerimento per createContextField")
   public static final String PRIVATE_CONSTANT_17 = "Tip/TestHelpSystem/TestHelpContext/searchContextField";
}
