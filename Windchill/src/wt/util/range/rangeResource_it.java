/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.util.range;

import wt.util.resource.*;

@RBUUID("wt.util.range.rangeResource")
public final class rangeResource_it extends WTListResourceBundle {
   @RBEntry("L'operazione {0} non è riuscita.")
   public static final String OPERATION_FAILED = "0";

   @RBEntry("I limiti di un intervallo (BounderyPoints) non possono essere riferimenti nulli.")
   public static final String NULL_RANGE_NOT_ALLOWED = "1";

   @RBEntry("Il limite superiore ({0}) di un intervallo deve essere superiore al limite inferiore ({1}).")
   @RBComment("An error message used when enters an invalid range (e.g. from 10 to 5).")
   @RBArgComment0("The value the user entered for the upper bound.")
   @RBArgComment1("The value the user entered for the lower bound.")
   public static final String LOWER_GT_UPPER_BOUND = "2";

   @RBEntry("Nullo non è un argomento valido per {0}.{1}.")
   public static final String NULL_ARGUMENT = "3";
}
