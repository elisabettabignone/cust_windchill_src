/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.util.jmx;

import wt.util.resource.*;

@RBUUID("wt.util.jmx.chartLabelsResource")
public final class chartLabelsResource extends WTListResourceBundle
{
   @RBEntry("Live Heap Memory Usage - Server Manager {0}")
   @RBComment("Message for amount of heap memory used by a given server manager process")
   @RBArgComment0("Server manager process identifier")
   public static final String SERVER_MANAGER_HEAP_USAGE_MSG = "0";

   @RBEntry("Time")
   @RBComment("Label for chart axis showing time")
   public static final String TIME = "1";

   @RBEntry("Heap Usage (%)")
   @RBComment("Label for chart axis showing heap usage as a percentage")
   public static final String PERC_HEAP_USAGE = "2";

   @RBEntry("Active Users")
   @RBComment("Label for chart showing number of active users over time")
   public static final String ACTIVE_USERS = "3";

   @RBEntry("Users")
   @RBComment("Label for chart axis showing active users")
   public static final String USERS = "4";

   @RBEntry("Historic Heap Memory Usage - Method Server {0}")
   @RBComment("Message for amount of heap memory used by a given method server process")
   @RBArgComment0("ID of the method server")
   public static final String METHOD_SERVER_HEAP_USAGE_MSG = "5";

   @RBEntry("Live Heap Memory Usage - {0}")
   @RBComment("Message for amount of heap memory used by a given Java process")
   @RBArgComment0("name of the method server")
   public static final String HEAP_USAGE_MSG = "6";

   @RBEntry("Live Time In Garbage Collection - Server Manager {0}")
   @RBComment("Message for percentage of server manager time spent in garbage collection")
   @RBArgComment0("ID of the server manager")
   public static final String SERVER_MANAGER_GC_MSG = "7";

   @RBEntry("Time In Garbage Collection (%)")
   @RBComment("Label for chart axis showing percentage of time spent in garbage collection")
   public static final String PERC_GC = "8";

   @RBEntry("Total")
   public static final String TOTAL = "9";

   @RBEntry("Historic Time In Garbage Collection - Method Server {0}")
   @RBArgComment0("ID of the method server")
   public static final String METHOD_SERVER_GC_MSG_HISTORIC = "10";

   @RBEntry("Live Time In Garbage Collection - {0}")
   @RBArgComment0("name of the method server")
   public static final String GC_TIME_MSG = "11";

   @RBEntry("CPU Time (%)")
   public static final String PERC_CPU_USAGE = "12";

   @RBEntry("Live CPU Usage - Server Manager {0}")
   @RBArgComment0("ID of the server manager")
   public static final String SERVER_MANAGER_CPU_USAGE_MSG = "13";

   @RBEntry("Live CPU Usage - {0}")
   @RBArgComment0("name of the method server")
   public static final String CPU_USAGE_MSG = "14";

   @RBEntry("Historic CPU Usage - Method Server {0}")
   @RBArgComment0("id of the method server")
   public static final String METHOD_SERVER_CPU_USAGE_MSG = "15";

   @RBEntry("Perm Gen Usage (%)")
   public static final String PERC_PERM_GEN_USAGE = "16";

   @RBEntry("Collection Usage")
   public static final String COLL_USAGE = "17";

   @RBEntry("Usage")
   public static final String USAGE = "18";

   @RBEntry("Live Perm Gen Memory Usage - Server Manager {0}")
   @RBArgComment0("ID of the server manager")
   public static final String SERVER_MANAGER_PERM_GEN_USAGE_MSG = "19";

   @RBEntry("Historic Perm Gen Memory Usage - {0}")
   @RBArgComment0("name of the method server")
   public static final String PERM_GEN_USAGE_MSG = "20";

   @RBEntry("Historic Perm Gen Memory Usage - Method Server {0}")
   @RBArgComment0("ID of the method server")
   public static final String METHOD_SERVER_PERM_GEN_USAGE_MSG = "21";

   @RBEntry("Completed Servlet Requests")
   public static final String COMPLETED_SERVLET_REQUESTS = "22";

   @RBEntry("Errors")
   public static final String ERRORS = "23";

   @RBEntry("Live Servlet Requests - Context {1} - {0}")
   public static final String SERVLET_CONTEXT_TITLE_MSG = "24";

   @RBEntry("Average Completed (Per Minute)")
   public static final String AVERAGE_PER_MINUTE = "25";

   @RBEntry("Average")
   public static final String AVERAGE = "26";

   @RBEntry("Maximum")
   public static final String MAXIMUM = "27";

   @RBEntry("Active Servlet Sessions")
   public static final String ACTIVE_SERVLET_SESSIONS = "28";

   @RBEntry("Average Response Time (Seconds)")
   public static final String AVERAGE_RESPONSE_TIME_SECONDS = "29";

   @RBEntry("Maximum Concurrent Requests")
   public static final String MAX_CONCURRENT_REQUESTS = "30";

   @RBEntry("Average Concurrent Requests")
   public static final String AVG_CONCURRENT_REQUESTS = "31";

   @RBEntry("Completed Method Contexts")
   public static final String COMPLETED_METHOD_CONTEXTS = "32";

   @RBEntry("Live Method Contexts - {0}")
   @RBArgComment0("name of the method server")
   public static final String METHOD_CONTEXTS_MSG = "33";

   @RBEntry("Maximum Concurrent Contexts")
   public static final String MAX_CONCURRENT_CONTEXTS = "34";

   @RBEntry("Average Concurrent Contexts")
   public static final String AVG_CONCURRENT_CONTEXTS = "35";

   @RBEntry("Context Time in JDBC (%)")
   public static final String PERC_TIME_IN_JDBC = "36";

   @RBEntry("Context Time in JDBC Connection Wait (%)")
   public static final String PERC_TIME_IN_JDBC_CONN_WAIT = "37";

   @RBEntry("Context Time in JNDI (%)")
   public static final String PERC_TIME_IN_JNDI = "38";

   @RBEntry("Current")
   public static final String CURRENT = "39";

   @RBEntry("Live Cluster Servlet Requests - Context {0}")
   public static final String CLUSTER_SERVLET_REQUESTS_MSG = "41";

   @RBEntry("Cluster Method Contexts")
   public static final String CLUSTER_METHOD_CONTEXTS = "42";

   @RBEntry("Live CPU Usage By Method Server - Server Manager {0}")
   @RBArgComment0("ID of the server manager")
   public static final String SM_MS_CPU_USAGE_MSG = "43";

   @RBEntry("Live Perm Gen Memory Usage By Method Server - Server Manager {0}")
   @RBArgComment0("ID of the server manager")
   public static final String SM_MS_PERM_GEN_USAGE_MSG = "44";

   @RBEntry("Live Heap Memory Usage By Method Server - Server Manager {0}")
   @RBArgComment0("ID of the server manager")
   public static final String SM_MS_HEAP_USAGE_MSG = "45";

   @RBEntry("Live Servlet Requests - Context {1} - Server Manager {0}")
   @RBArgComment0("ID of the server manager")
   @RBArgComment1("servlet context like /Windchill")
   public static final String SM_SERVLET_REQUESTS_MSG = "46";

   @RBEntry("Live Method Contexts - Server Manager {0}")
   @RBArgComment0("ID of the server manager")
   public static final String SM_METHOD_CONTEXTS_MSG = "47";

   @RBEntry("Live Perm Gen Memory Usage - Windchill Directory Server")
   public static final String WDS_PERM_GEN_USAGE = "48";

   @RBEntry("Live Heap Memory Usage - Windchill Directory Server")
   public static final String WDS_HEAP_USAGE = "49";

   @RBEntry("Live CPU Usage - Windchill Directory Server")
   public static final String WDS_CPU_USAGE = "50";

   @RBEntry("Live Time In Garbage Collection - Windchill Directory Server")
   public static final String WDS_GC_TIME = "51";

   @RBEntry("Load Average")
   public static final String LOAD_AVERAGE = "52";

   @RBEntry("{0} Load Average")
   public static final String LOAD_AVERAGE_TITLE_MSG = "53";

   @RBEntry("Historic Time In Garbage Collection - Server Manager {0}")
   @RBComment("Message for percentage of server manager time spent in garbage collection")
   @RBArgComment0("ID of the server manager")
   public static final String SERVER_MANAGER_GC_MSG_HISTORIC = "54";

   @RBEntry("Historic Time In Garbage Collection - {0}")
   @RBArgComment0("name of the method server")
   public static final String GC_TIME_MSG_HISTORIC = "55";

   @RBEntry("Historic CPU Usage - Server Manager {0}")
   @RBArgComment0("ID of the server manager")
   public static final String SERVER_MANAGER_CPU_USAGE_MSG_HISTORIC = "56";

   @RBEntry("Historic CPU Usage - {0}")
   @RBArgComment0("name of the method server")
   public static final String CPU_USAGE_MSG_HISTORIC = "57";

   @RBEntry("Historic Heap Memory Usage - {0}")
   @RBComment("Message for amount of heap memory used by a given Java process")
   @RBArgComment0("name of the method server")
   public static final String HEAP_USAGE_MSG_HOSTORIC = "58";

   @RBEntry("Historic Perm Gen Memory Usage - Server Manager {0}")
   @RBArgComment0("ID of the server manager")
   public static final String SERVER_MANAGER_PERM_GEN_USAGE_MSG_HISTORIC = "59";

   @RBEntry("Historic Heap Memory Usage - Server Manager {0}")
   @RBComment("Message for amount of heap memory used by a given server manager process")
   @RBArgComment0("Server manager process identifier")
   public static final String SERVER_MANAGER_HEAP_USAGE_MSG_HISTORIC = "60";

   @RBEntry("Historic Servlet Requests - Context {1} - {0}")
   public static final String SERVLET_CONTEXT_TITLE_MSG_HISTORIC = "61";

   @RBEntry("Historic Method Contexts - {0}")
   @RBArgComment0("name of the method server")
   public static final String METHOD_CONTEXTS_MSG_HISTORIC = "62";

   @RBEntry("Historic Method Context Time - {0}")
   @RBArgComment0("name of the method server")
   public static final String METHOD_CONTEXTS_TIME_MSG_HISTORIC = "63";

   @RBEntry("Server Status Chart")
   @RBArgComment0("Title of HTML page used to wrap server status page chart images")
   public static final String SERVER_STATUS_CHART_WRAPPER_PAGE_TITLE = "64";

   @RBEntry("Loading")
   public static final String LOADING_LABEL = "65";

   @RBEntry("Apply")
   public static final String APPLY = "66";

   @RBEntry("Days")
   public static final String DAYS = "67";

   @RBEntry("Minutes")
   public static final String MINUTES = "68";

   @RBEntry("Hours")
   public static final String HOURS = "69";

   @RBEntry("Range")
   public static final String RANGE = "70";

   @RBEntry("Context Time in Remote Caching (%)")
   public static final String PERC_TIME_IN_REMOTE_CACHING = "71";

   @RBEntry("Hits")
   public static final String CACHE_HITS = "72";

   @RBEntry("Misses")
   public static final String CACHE_MISSES = "73";

   @RBEntry("Per Minute")
   public static final String PER_MINUTE = "74";

   @RBEntry("{1} Statistics - Method Server {0}")
   @RBArgComment0("ID of the method server")
   @RBArgComment1("Name of cache statistics are for")
   public static final String METHOD_SERVER_CACHE_STATISTICS_MSG_HISTORIC = "75";

   @RBEntry("{1} Statistics - {0}")
   @RBArgComment0("name of the method server")
   @RBArgComment1("Name of cache statistics are for")
   public static final String CACHE_STATISTICS_MSG_HISTORIC = "76";

   @RBEntry("{1} Statistics - Server Manager {0}")
   @RBArgComment0("ID of the server manager")
   @RBArgComment1("Name of cache statistics are for")
   public static final String SERVER_MANAGER_CACHE_STATISTICS_MSG_HISTORIC = "77";

   @RBEntry("Cache Statistics Chart")
   @RBArgComment0("Title of HTML page used to wrap cache statistics chart images")
   public static final String CACHE_STATS_CHART_WRAPPER_TITLE = "78";

   @RBEntry("Overflows")
   public static final String CACHE_OVERFLOWS = "79";
}
