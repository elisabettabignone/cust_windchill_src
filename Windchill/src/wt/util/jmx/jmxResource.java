/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.util.jmx;

import wt.util.resource.*;

@RBUUID("wt.util.jmx.jmxResource")
public final class jmxResource extends WTListResourceBundle {
   @RBEntry("Error retrieving property '{0}'")
   public static final String ERROR_RETRIEVING_PROPERTY = "0";

   @RBEntry("Unknown change found '{0}'")
   public static final String UNKNOWN_CHANGE_FOUND = "1";

   @RBEntry("comment")
   public static final String COMMENT = "2";

   @RBEntry("Add to property")
   public static final String ADD_PROPERTY = "3";

   @RBEntry("Remove from property")
   public static final String REMOVE_FROM_PROPERTY = "4";

   @RBEntry("Reset property")
   public static final String RESET_PROPERTY = "5";

   @RBEntry("Undefine property")
   public static final String UNDEFINE_PROPERTY = "6";

   @RBEntry("Unknown action")
   public static final String UNKNOWN_ACTION = "7";

   @RBEntry("(Secured Information)")
   public static final String SECURED_INFORMATION = "8";

   @RBEntry("'{0}' is a secured property")
   public static final String SECURED_PROPERTY = "9";

   @RBEntry("Set property")
   public static final String SET_SITE_PROPERTY = "10";

   @RBEntry("Related data for Property modification event notification")
   public static final String PROPMGR_NOTIF_USER_DATA_TYPE_DESCR = "11";

   @RBEntry("Target file of Property modification")
   public static final String PROPMGR_NOTIF_OPEN_DATA_TARGETFILE_ITEM_DESCR = "12";

   @RBEntry("Property modification action")
   public static final String PROPMGR_NOTIF_OPEN_DATA_ACTION_ITEM_DESCR = "13";

   @RBEntry("Name of modified property")
   public static final String PROPMGR_NOTIF_OPEN_DATA_PROPERTYNAME_ITEM_DESCR = "14";

   @RBEntry("Original value of modified property")
   public static final String PROPMGR_NOTIF_OPEN_DATA_ORIGPROPERTYVALUE_ITEM_DESCR = "15";

   @RBEntry("New value of modified property")
   public static final String PROPMGR_NOTIF_OPEN_DATA_NEWPROPERTYVALUE_ITEM_DESCR = "16";

   @RBEntry("Comment for added property")
   public static final String PROPMGR_NOTIF_OPEN_DATA_COMMENT_ITEM_DESCR = "17";

   @RBEntry("Property values modified")
   public static final String PROPMGR_NOTIF_MSG = "18";

   @RBEntry("Important Property file events")
   public static final String PROPMGR_NOTIF_DESCR = "19";

   @RBEntry("Open data for Property modification event notification")
   public static final String PROPMGR_NOTIF_OPEN_DATA_TYPE_DESCR = "20";

   @RBEntry("Open data for Property modification event notification")
   public static final String PROPMGR_NOTIF_USER_DATA_OPENDATA_ITEM_DESCR = "21";

   @RBEntry("String data for Property modification event notification")
   public static final String PROPMGR_NOTIF_USER_DATA_STRING_ITEM_DESCR = "22";

   @RBEntry("Xconf file name")
   public static final String XCONF_CONTENTS_STRUCT_ITEM_NAME_DESCR = "23";

   @RBEntry("Xconf file contents")
   public static final String XCONF_CONTENTS_STRUCT_ITEM_VALUE_DESCR = "24";

   @RBEntry("Data for individual Xconf file")
   public static final String XCONF_CONTENTS_STRUCT_TYPE_DESCR = "25";

   @RBEntry("Tabular data for Xconf files")
   public static final String XCONF_CONTENTS_TABLE_TYPE_DESCR = "26";

   @RBEntry("Property Names")
   @RBComment("Label used for the property name column in the property comparison results.")
   public static final String PROPERTY_NAMES_COL_NAME = "27";

   @RBEntry("Cluster Property Comparison Results")
   @RBComment("Title for cluster property comparison results page. Also used as the subject of the results email.")
   public static final String CLUSTER_PROP_CMP_RESULTS_TITLE = "28";

   @RBEntry("No differences were found.")
   @RBComment("Message displayed when no differences were found in cluster property comparison.")
   public static final String NO_DIFFERENCES_FOUND_MSG = "29";

   @RBEntry("NA")
   @RBComment("Value displayed in compare properties results when their is no value specified on a node.")
   public static final String PROP_VALUE_NOT_AVAILABLE = "30";

   @RBEntry("No differences were detected.")
   @RBComment("Value displayed when comparison of property files resulted in no differences being found.")
   public static final String NO_DIF_DETECTED_MSG = "31";

   @RBEntry("Sends JMX management bean (MBean) data and registered log directories to PTC technical support for all nodes in the cluster.  This is done via the server manager if possible.  If not, then this utility will gather what information it can from the machine on which it is run and e-mail that.")
   public static final String TECH_SUPPORT_MAIN_DESCR = "32";

   @RBEntry("USAGE:")
   public static final String USAGE_LABEL = "33";

   @RBEntry("Support Call # Without 'C' Prefix")
   @RBComment("Shorthand description of argument to be used in command line usage description, e.g. \"java wt.util.jmx.TechSupportMain <Support Call # Without 'C' Prefix>\"")
   public static final String TECH_SUPPORT_MAIN_ARG_DESCR = "34";

   @RBEntry("Outputs value for a specified wt.properties property name.  This value will be of a \"fully evaluated\" form, meaning that all $(...) substitutions have already been performed.")
   public static final String WT_PROP_EVAL_MAIN_DESCR = "35";

   @RBEntry("wt.properties property name")
   @RBComment("Shorthand description of argument to be used in command line usage description, e.g. \"java wt.util.jmx.WTPropEval <wt.properties property name>\"")
   public static final String WT_PROP_EVAL_MAIN_ARG_DESCR = "36";

   @RBEntry("Default Property Value Comparison Results")
   @RBComment("Title for default property value comparison results page.  Also used as the subject of the results email.")
   public static final String DEFAULT_PROP_CMP_RESULTS_TITLE = "37";

   @RBEntry("Current Value")
   @RBComment("Label used for the current property value column in the comparison results.")
   public static final String CUR_PROP_VALUE_COL_NAME = "38";

   @RBEntry("Default Value")
   @RBComment("Label used for the default property value column in the comparison results.")
   public static final String DEFAULT_PROP_VALUE_COL_NAME = "39";

   @RBEntry("Return all occurrences (in order) of a specified resource in the classpath or, when the -cluster option is used, within the classpaths of all the server managers in the cluster. Note that the resource must be specified as per Java's rules (for example, wt/util/jmx/Which.class).")
   public static final String WHICH_MAIN_DESCR = "40";

   @RBEntry("resource name")
   public static final String WHICH_MAIN_ARG_DESCR = "41";

   @RBEntry("Resets all logger verbosity levels and/or sets the verbosity level of a specified logger in server processes specified by any combination of -sm, -ms, -se, and -all options.  These options target all server managers in the cluster, all method servers in the cluster, all servlet engines in the cluster, or all of these processes, respectively.  If none of these options is specified, -all is assumed.  The -resetAll option will reset all log4j verbosities in the target processes.  If both -resetAll and a logger name are specified the reset will be performed prior to setting the specified logger's verbosity level.  Either -resetAll or a logger name must be specified.  Valid log levels are ALL, TRACE, DEBUG, INFO, WARN, ERROR, FATAL, and OFF; the log level can also be left unspecified, in which case it will be inherited from the parent logger.  WARNING: The -resetAll option should be used with great caution as it resets the entire log4j configuration to that specified in the log4j configuration file, which does not include numerous settings which are dynamically configured under normal operation.")
   public static final String SET_LOG_LEVEL_DESCR = "42";

   @RBEntry("logger name")
   public static final String SET_LOG_LEVEL_LOGGER_ARG_DESCR = "43";

   @RBEntry("log level")
   public static final String SET_LOG_LEVEL_LEVEL_ARG_DESCR = "44";

   @RBEntry("Remote server manager must not be null here.")
   public static final String REMOTE_SERVER_MANAGER_CANNOT_BE_NULL = "45";

   @RBEntry("Remote method server cannot be null here.")
   public static final String REMOTE_METHOD_SERVER_CANNOT_BE_NULL = "46";

   @RBEntry("JMXConnectInfo cannot be null here.")
   public static final String JMX_CONNECT_INFO_CANNOT_BE_NULL = "47";

   @RBEntry("Local JMX URL string cannot be null here.")
   public static final String LOCAL_JMX_URL_STRING_CANNOT_BE_NULL = "48";

   @RBEntry("Sends the specified file or directory to PTC Technical Support, associating it with a specified call number.")
   public static final String SEND_FILE_TO_SUPPORT_DESCR = "49";

   @RBEntry("File or directory to send")
   public static final String SEND_FILE_TO_SUPPORT_FILE_ARG_DESCR = "50";

   @RBEntry("Description or comments")
   public static final String SEND_FILE_TO_SUPPORT_DESCR_ARG_DESCR = "51";
}
