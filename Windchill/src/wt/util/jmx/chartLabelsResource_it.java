/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.util.jmx;

import wt.util.resource.*;

@RBUUID("wt.util.jmx.chartLabelsResource")
public final class chartLabelsResource_it extends WTListResourceBundle
{
   @RBEntry("Utilizzo corrente memoria heap - Server manager {0}")
   @RBComment("Message for amount of heap memory used by a given server manager process")
   @RBArgComment0("Server manager process identifier")
   public static final String SERVER_MANAGER_HEAP_USAGE_MSG = "0";

   @RBEntry("Ora")
   @RBComment("Label for chart axis showing time")
   public static final String TIME = "1";

   @RBEntry("Utilizzo heap (%)")
   @RBComment("Label for chart axis showing heap usage as a percentage")
   public static final String PERC_HEAP_USAGE = "2";

   @RBEntry("Utenti attivi")
   @RBComment("Label for chart showing number of active users over time")
   public static final String ACTIVE_USERS = "3";

   @RBEntry("Utenti")
   @RBComment("Label for chart axis showing active users")
   public static final String USERS = "4";

   @RBEntry("Cronologia utilizzo memoria heap - Method server {0}")
   @RBComment("Message for amount of heap memory used by a given method server process")
   @RBArgComment0("ID of the method server")
   public static final String METHOD_SERVER_HEAP_USAGE_MSG = "5";

   @RBEntry("Utilizzo corrente memoria heap - {0}")
   @RBComment("Message for amount of heap memory used by a given Java process")
   @RBArgComment0("name of the method server")
   public static final String HEAP_USAGE_MSG = "6";

   @RBEntry("Tempo corrente trascorso in garbage collection - Server manager {0}")
   @RBComment("Message for percentage of server manager time spent in garbage collection")
   @RBArgComment0("ID of the server manager")
   public static final String SERVER_MANAGER_GC_MSG = "7";

   @RBEntry("Tempo trascorso in garbage collection (%)")
   @RBComment("Label for chart axis showing percentage of time spent in garbage collection")
   public static final String PERC_GC = "8";

   @RBEntry("Totale")
   public static final String TOTAL = "9";

   @RBEntry("Cronologia tempo trascorso in garbage collection - Method server {0}")
   @RBArgComment0("ID of the method server")
   public static final String METHOD_SERVER_GC_MSG_HISTORIC = "10";

   @RBEntry("Tempo corrente trascorso in garbage collection - {0}")
   @RBArgComment0("name of the method server")
   public static final String GC_TIME_MSG = "11";

   @RBEntry("Tempo CPU (%)")
   public static final String PERC_CPU_USAGE = "12";

   @RBEntry("Utilizzo corrente CPU - Server manager {0}")
   @RBArgComment0("ID of the server manager")
   public static final String SERVER_MANAGER_CPU_USAGE_MSG = "13";

   @RBEntry("Utilizzo corrente CPU - {0}")
   @RBArgComment0("name of the method server")
   public static final String CPU_USAGE_MSG = "14";

   @RBEntry("Cronologia utilizzo CPU - Method server {0}")
   @RBArgComment0("id of the method server")
   public static final String METHOD_SERVER_CPU_USAGE_MSG = "15";

   @RBEntry("Utilizzo memoria PermGen (%)")
   public static final String PERC_PERM_GEN_USAGE = "16";

   @RBEntry("Utilizzo raccolta")
   public static final String COLL_USAGE = "17";

   @RBEntry("Uso")
   public static final String USAGE = "18";

   @RBEntry("Utilizzo corrente memoria PermGen - Server manager {0}")
   @RBArgComment0("ID of the server manager")
   public static final String SERVER_MANAGER_PERM_GEN_USAGE_MSG = "19";

   @RBEntry("Cronologia utilizzo memoria PermGen - {0}")
   @RBArgComment0("name of the method server")
   public static final String PERM_GEN_USAGE_MSG = "20";

   @RBEntry("Cronologia utilizzo memoria PermGen - Method server {0}")
   @RBArgComment0("ID of the method server")
   public static final String METHOD_SERVER_PERM_GEN_USAGE_MSG = "21";

   @RBEntry("Richieste servlet completate")
   public static final String COMPLETED_SERVLET_REQUESTS = "22";

   @RBEntry("Errori")
   public static final String ERRORS = "23";

   @RBEntry("Richieste servlet correnti - Contesto {1} - {0}")
   public static final String SERVLET_CONTEXT_TITLE_MSG = "24";

   @RBEntry("Media completata (al minuto)")
   public static final String AVERAGE_PER_MINUTE = "25";

   @RBEntry("Media")
   public static final String AVERAGE = "26";

   @RBEntry("Massimo")
   public static final String MAXIMUM = "27";

   @RBEntry("Sessioni servlet attive")
   public static final String ACTIVE_SERVLET_SESSIONS = "28";

   @RBEntry("Tempo medio di risposta (secondi)")
   public static final String AVERAGE_RESPONSE_TIME_SECONDS = "29";

   @RBEntry("Numero massimo richieste simultanee")
   public static final String MAX_CONCURRENT_REQUESTS = "30";

   @RBEntry("Media richieste simultanee")
   public static final String AVG_CONCURRENT_REQUESTS = "31";

   @RBEntry("Contesti metodo completati")
   public static final String COMPLETED_METHOD_CONTEXTS = "32";

   @RBEntry("Contesti metodo correnti - {0}")
   @RBArgComment0("name of the method server")
   public static final String METHOD_CONTEXTS_MSG = "33";

   @RBEntry("Numero massimo contesti simultanei")
   public static final String MAX_CONCURRENT_CONTEXTS = "34";

   @RBEntry("Media contesti simultanei")
   public static final String AVG_CONCURRENT_CONTEXTS = "35";

   @RBEntry("Tempo contesto trascorso in JDBC (%)")
   public static final String PERC_TIME_IN_JDBC = "36";

   @RBEntry("Tempo contesto trascorso in attesa connessioni JDBC (%)")
   public static final String PERC_TIME_IN_JDBC_CONN_WAIT = "37";

   @RBEntry("Tempo contesto trascorso in JNDI (%)")
   public static final String PERC_TIME_IN_JNDI = "38";

   @RBEntry("Corrente")
   public static final String CURRENT = "39";

   @RBEntry("Richieste servlet cluster correnti - Contesto {0}")
   public static final String CLUSTER_SERVLET_REQUESTS_MSG = "41";

   @RBEntry("Contesti metodo cluster")
   public static final String CLUSTER_METHOD_CONTEXTS = "42";

   @RBEntry("Utilizzo corrente CPU da parte del method server - Server manager {0}")
   @RBArgComment0("ID of the server manager")
   public static final String SM_MS_CPU_USAGE_MSG = "43";

   @RBEntry("Utilizzo corrente memoria PermGen da parte del method server - Server manager {0}")
   @RBArgComment0("ID of the server manager")
   public static final String SM_MS_PERM_GEN_USAGE_MSG = "44";

   @RBEntry("Utilizzo corrente memoria heap da parte del method server - Server manager {0}")
   @RBArgComment0("ID of the server manager")
   public static final String SM_MS_HEAP_USAGE_MSG = "45";

   @RBEntry("Richieste servlet correnti - Contesto {1} - Server manager {0}")
   @RBArgComment0("ID of the server manager")
   @RBArgComment1("servlet context like /Windchill")
   public static final String SM_SERVLET_REQUESTS_MSG = "46";

   @RBEntry("Contesti metodo correnti - Server manager {0}")
   @RBArgComment0("ID of the server manager")
   public static final String SM_METHOD_CONTEXTS_MSG = "47";

   @RBEntry("Utilizzo corrente memoria PermGen - Windchill Directory Server")
   public static final String WDS_PERM_GEN_USAGE = "48";

   @RBEntry("Utilizzo corrente memoria heap - Windchill Directory Server")
   public static final String WDS_HEAP_USAGE = "49";

   @RBEntry("Utilizzo corrente CPU - Windchill Directory Server")
   public static final String WDS_CPU_USAGE = "50";

   @RBEntry("Tempo corrente trascorso in garbage collection - Windchill Directory Server")
   public static final String WDS_GC_TIME = "51";

   @RBEntry("Carico medio")
   public static final String LOAD_AVERAGE = "52";

   @RBEntry("Carico medio {0}")
   public static final String LOAD_AVERAGE_TITLE_MSG = "53";

   @RBEntry("Cronologia tempo trascorso in garbage collection - Server manager {0}")
   @RBComment("Message for percentage of server manager time spent in garbage collection")
   @RBArgComment0("ID of the server manager")
   public static final String SERVER_MANAGER_GC_MSG_HISTORIC = "54";

   @RBEntry("Cronologia tempo trascorso in garbage collection - {0}")
   @RBArgComment0("name of the method server")
   public static final String GC_TIME_MSG_HISTORIC = "55";

   @RBEntry("Cronologia utilizzo CPU - Server manager {0}")
   @RBArgComment0("ID of the server manager")
   public static final String SERVER_MANAGER_CPU_USAGE_MSG_HISTORIC = "56";

   @RBEntry("Cronologia utilizzo CPU - {0}")
   @RBArgComment0("name of the method server")
   public static final String CPU_USAGE_MSG_HISTORIC = "57";

   @RBEntry("Cronologia utilizzo memoria heap - {0}")
   @RBComment("Message for amount of heap memory used by a given Java process")
   @RBArgComment0("name of the method server")
   public static final String HEAP_USAGE_MSG_HOSTORIC = "58";

   @RBEntry("Cronologia utilizzo memoria PermGen - Server manager {0}")
   @RBArgComment0("ID of the server manager")
   public static final String SERVER_MANAGER_PERM_GEN_USAGE_MSG_HISTORIC = "59";

   @RBEntry("Cronologia utilizzo memoria heap - Server manager {0}")
   @RBComment("Message for amount of heap memory used by a given server manager process")
   @RBArgComment0("Server manager process identifier")
   public static final String SERVER_MANAGER_HEAP_USAGE_MSG_HISTORIC = "60";

   @RBEntry("Cronologia richieste servlet - Contesto {1} - {0}")
   public static final String SERVLET_CONTEXT_TITLE_MSG_HISTORIC = "61";

   @RBEntry("Cronologia contesti metodo - {0}")
   @RBArgComment0("name of the method server")
   public static final String METHOD_CONTEXTS_MSG_HISTORIC = "62";

   @RBEntry("Cronologia tempo contesto metodo - {0}")
   @RBArgComment0("name of the method server")
   public static final String METHOD_CONTEXTS_TIME_MSG_HISTORIC = "63";

   @RBEntry("Diagramma stato server")
   @RBArgComment0("Title of HTML page used to wrap server status page chart images")
   public static final String SERVER_STATUS_CHART_WRAPPER_PAGE_TITLE = "64";

   @RBEntry("Caricamento")
   public static final String LOADING_LABEL = "65";

   @RBEntry("Applica")
   public static final String APPLY = "66";

   @RBEntry("Giorni")
   public static final String DAYS = "67";

   @RBEntry("Minuti")
   public static final String MINUTES = "68";

   @RBEntry("Ore")
   public static final String HOURS = "69";

   @RBEntry("Intervallo")
   public static final String RANGE = "70";

   @RBEntry("Tempo contesto trascorso in memorizzazione nella cache remota {%}")
   public static final String PERC_TIME_IN_REMOTE_CACHING = "71";

   @RBEntry("Riscontri")
   public static final String CACHE_HITS = "72";

   @RBEntry("Mancati riscontri")
   public static final String CACHE_MISSES = "73";

   @RBEntry("Al minuto")
   public static final String PER_MINUTE = "74";

   @RBEntry("Statistiche {1} - Method server {0}")
   @RBArgComment0("ID of the method server")
   @RBArgComment1("Name of cache statistics are for")
   public static final String METHOD_SERVER_CACHE_STATISTICS_MSG_HISTORIC = "75";

   @RBEntry("Statistiche {1} - {0}")
   @RBArgComment0("name of the method server")
   @RBArgComment1("Name of cache statistics are for")
   public static final String CACHE_STATISTICS_MSG_HISTORIC = "76";

   @RBEntry("Statistiche {1} - Server manager {0}")
   @RBArgComment0("ID of the server manager")
   @RBArgComment1("Name of cache statistics are for")
   public static final String SERVER_MANAGER_CACHE_STATISTICS_MSG_HISTORIC = "77";

   @RBEntry("Grafico statistiche cache")
   @RBArgComment0("Title of HTML page used to wrap cache statistics chart images")
   public static final String CACHE_STATS_CHART_WRAPPER_TITLE = "78";

   @RBEntry("Overflow")
   public static final String CACHE_OVERFLOWS = "79";
}
