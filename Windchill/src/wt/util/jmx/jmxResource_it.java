/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.util.jmx;

import wt.util.resource.*;

@RBUUID("wt.util.jmx.jmxResource")
public final class jmxResource_it extends WTListResourceBundle {
   @RBEntry("Errore durante il recupero della proprietà '{0}'")
   public static final String ERROR_RETRIEVING_PROPERTY = "0";

   @RBEntry("Trovata modifica sconosciuta '{0}'")
   public static final String UNKNOWN_CHANGE_FOUND = "1";

   @RBEntry("commento")
   public static final String COMMENT = "2";

   @RBEntry("Aggiungi a proprietà")
   public static final String ADD_PROPERTY = "3";

   @RBEntry("Rimuovi da proprietà")
   public static final String REMOVE_FROM_PROPERTY = "4";

   @RBEntry("Reimposta proprietà")
   public static final String RESET_PROPERTY = "5";

   @RBEntry("Annulla definizione proprietà")
   public static final String UNDEFINE_PROPERTY = "6";

   @RBEntry("Azione sconosciuta")
   public static final String UNKNOWN_ACTION = "7";

   @RBEntry("(Informazioni protette)")
   public static final String SECURED_INFORMATION = "8";

   @RBEntry("'{0}' è una proprietà protetta")
   public static final String SECURED_PROPERTY = "9";

   @RBEntry("Imposta proprietà")
   public static final String SET_SITE_PROPERTY = "10";

   @RBEntry("Dati correlati relativi alla notifica degli eventi di modifica della proprietà")
   public static final String PROPMGR_NOTIF_USER_DATA_TYPE_DESCR = "11";

   @RBEntry("File di destinazione della modifica di proprietà")
   public static final String PROPMGR_NOTIF_OPEN_DATA_TARGETFILE_ITEM_DESCR = "12";

   @RBEntry("Azione modifica proprietà")
   public static final String PROPMGR_NOTIF_OPEN_DATA_ACTION_ITEM_DESCR = "13";

   @RBEntry("Nome proprietà modificata")
   public static final String PROPMGR_NOTIF_OPEN_DATA_PROPERTYNAME_ITEM_DESCR = "14";

   @RBEntry("Valore originale proprietà modificata")
   public static final String PROPMGR_NOTIF_OPEN_DATA_ORIGPROPERTYVALUE_ITEM_DESCR = "15";

   @RBEntry("Nuovo valore proprietà modificata")
   public static final String PROPMGR_NOTIF_OPEN_DATA_NEWPROPERTYVALUE_ITEM_DESCR = "16";

   @RBEntry("Commento per proprietà aggiunta")
   public static final String PROPMGR_NOTIF_OPEN_DATA_COMMENT_ITEM_DESCR = "17";

   @RBEntry("Valori proprietà modificati")
   public static final String PROPMGR_NOTIF_MSG = "18";

   @RBEntry("Eventi notevoli file proprietà")
   public static final String PROPMGR_NOTIF_DESCR = "19";

   @RBEntry("Dati liberi relativi alla notifica degli eventi di modifica della proprietà")
   public static final String PROPMGR_NOTIF_OPEN_DATA_TYPE_DESCR = "20";

   @RBEntry("Dati liberi relativi alla notifica degli eventi di modifica della proprietà")
   public static final String PROPMGR_NOTIF_USER_DATA_OPENDATA_ITEM_DESCR = "21";

   @RBEntry("Dati stringa relativi alla notifica degli eventi di modifica della proprietà")
   public static final String PROPMGR_NOTIF_USER_DATA_STRING_ITEM_DESCR = "22";

   @RBEntry("Nome file Xconf")
   public static final String XCONF_CONTENTS_STRUCT_ITEM_NAME_DESCR = "23";

   @RBEntry("Contenuto file Xconf")
   public static final String XCONF_CONTENTS_STRUCT_ITEM_VALUE_DESCR = "24";

   @RBEntry("Dati per singolo file Xconf")
   public static final String XCONF_CONTENTS_STRUCT_TYPE_DESCR = "25";

   @RBEntry("Dati in forma tabellare per file Xconf")
   public static final String XCONF_CONTENTS_TABLE_TYPE_DESCR = "26";

   @RBEntry("Nomi proprietà")
   @RBComment("Label used for the property name column in the property comparison results.")
   public static final String PROPERTY_NAMES_COL_NAME = "27";

   @RBEntry("Risultati confronto proprietà nel cluster")
   @RBComment("Title for cluster property comparison results page. Also used as the subject of the results email.")
   public static final String CLUSTER_PROP_CMP_RESULTS_TITLE = "28";

   @RBEntry("Non sono state trovate differenze")
   @RBComment("Message displayed when no differences were found in cluster property comparison.")
   public static final String NO_DIFFERENCES_FOUND_MSG = "29";

   @RBEntry("N/D")
   @RBComment("Value displayed in compare properties results when their is no value specified on a node.")
   public static final String PROP_VALUE_NOT_AVAILABLE = "30";

   @RBEntry("Non sono state rilevate differenze.")
   @RBComment("Value displayed when comparison of property files resulted in no differences being found.")
   public static final String NO_DIF_DETECTED_MSG = "31";

   @RBEntry("Invia i dati relativi ai bean di gestione JMX (MBean) e le directory di log registrate per tutti i nodi nel cluster al supporto tecnico PTC. Se possibile l'invio avviene tramite il server manager. Qualora non sia possibile, l'utilità raccoglie le informazioni disponibili dal computer in cui è in esecuzione e le invia per e-mail.")
   public static final String TECH_SUPPORT_MAIN_DESCR = "32";

   @RBEntry("Uso:")
   public static final String USAGE_LABEL = "33";

   @RBEntry("N. chiamata al supporto senza il prefisso 'C'")
   @RBComment("Shorthand description of argument to be used in command line usage description, e.g. \"java wt.util.jmx.TechSupportMain <Support Call # Without 'C' Prefix>\"")
   public static final String TECH_SUPPORT_MAIN_ARG_DESCR = "34";

   @RBEntry("Restituisce il valore per un nome proprietà wt.properties specifico. Il valore sarà in forma \"completamente valutata\", ossia con tutte le sostituzioni $(...) già effettuate.")
   public static final String WT_PROP_EVAL_MAIN_DESCR = "35";

   @RBEntry("Nome proprietà wt.properties")
   @RBComment("Shorthand description of argument to be used in command line usage description, e.g. \"java wt.util.jmx.WTPropEval <wt.properties property name>\"")
   public static final String WT_PROP_EVAL_MAIN_ARG_DESCR = "36";

   @RBEntry("Risultati confronto valore proprietà di default")
   @RBComment("Title for default property value comparison results page.  Also used as the subject of the results email.")
   public static final String DEFAULT_PROP_CMP_RESULTS_TITLE = "37";

   @RBEntry("Valore corrente")
   @RBComment("Label used for the current property value column in the comparison results.")
   public static final String CUR_PROP_VALUE_COL_NAME = "38";

   @RBEntry("Valore di default")
   @RBComment("Label used for the default property value column in the comparison results.")
   public static final String DEFAULT_PROP_VALUE_COL_NAME = "39";

   @RBEntry("Restituisce i casi d'impiego (ordinati) di una specifica risorsa nel percorso classe oppure, qualora si utilizzi l'opzione -cluster, nei percorsi classe di tutti i server manager nel cluster. Nota: la risorsa deve essere specificata in base alle regole Java (ad esempio wt/util/jmx/Which.class).")
   public static final String WHICH_MAIN_DESCR = "40";

   @RBEntry("Nome risorsa")
   public static final String WHICH_MAIN_ARG_DESCR = "41";

   @RBEntry("Reimposta i livelli di dettaglio per tutti i logger o per un logger specifico nei processi server, utilizzando una combinazione qualsiasi delle opzioni  -sm, -ms, -se e -all. Le opzioni interessano rispettivamente tutti i server manager nel cluster, tutti i method server nel cluster, tutti i motori servlet nel cluster oppure tutti questi processi. Se non si specifica un'opzione, il default di sistema è -all. L'opzione -resetAll reimposta tutti i livelli di dettaglio log4j nei processi interessati. Se si specifica sia -resetAll che un nome logger, la reimpostazione viene eseguita prima dell'impostazione del livello di dettaglio del logger specificato. È necessario specificare -resetAll o un nome logger. I livelli di log validi sono ALL, TRACE, DEBUG, INFO, WARN, ERROR, FATAL e OFF; se non si specifica un livello di log, verrà ereditato dal logger padre. AVVERTENZA: utilizzare con attenzione l'opzione -resetAll in quanto ripristina la configurazione di log4j specificata nel file di configurazione di log4j che non include numerose impostazioni configurate in modo dinamico durante il normale funzionamento.")
   public static final String SET_LOG_LEVEL_DESCR = "42";

   @RBEntry("Nome logger")
   public static final String SET_LOG_LEVEL_LOGGER_ARG_DESCR = "43";

   @RBEntry("Livello di log")
   public static final String SET_LOG_LEVEL_LEVEL_ARG_DESCR = "44";

   @RBEntry("Il server manager remoto non deve essere nullo.")
   public static final String REMOTE_SERVER_MANAGER_CANNOT_BE_NULL = "45";

   @RBEntry("Il method server remoto non può essere nullo.")
   public static final String REMOTE_METHOD_SERVER_CANNOT_BE_NULL = "46";

   @RBEntry("JMXConnectInfo non può essere nullo.")
   public static final String JMX_CONNECT_INFO_CANNOT_BE_NULL = "47";

   @RBEntry("La stringa dell'URL JMX locale non può essere nulla.")
   public static final String LOCAL_JMX_URL_STRING_CANNOT_BE_NULL = "48";

   @RBEntry("Invia la directory o il file specificato al supporto tecnico PTC associando un numero di chiamata.")
   public static final String SEND_FILE_TO_SUPPORT_DESCR = "49";

   @RBEntry("File o directory da inviare")
   public static final String SEND_FILE_TO_SUPPORT_FILE_ARG_DESCR = "50";

   @RBEntry("Descrizione o commenti")
   public static final String SEND_FILE_TO_SUPPORT_DESCR_ARG_DESCR = "51";
}
