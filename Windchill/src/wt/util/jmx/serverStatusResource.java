/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.util.jmx;

import wt.util.resource.*;

@RBUUID("wt.util.jmx.serverStatusResource")
public final class serverStatusResource extends WTListResourceBundle {
   @RBEntry("{0} days, {1}:{2}:{3}.{4}")
   @RBComment("Format for duration of time in days, hours, minutes, seconds, and milliseconds")
   @RBArgComment0("Number of days")
   @RBArgComment1("Number of hours")
   @RBArgComment2("Number of milliseconds")
   public static final String TIME_DURATION_FORMAT_W_DAYS = "0";

   @RBEntry("{1}:{2}:{3}.{4}")
   @RBComment("Format for duration of time in hours, minutes, seconds, and milliseconds")
   @RBArgComment0("Number of days (ignored)")
   @RBArgComment1("Number of hours")
   @RBArgComment2("Number of minutes")
   @RBArgComment3("Number of seconds")
   @RBArgComment4("Number of milliseconds")
   public static final String TIME_DURATION_FORMAT_WO_DAYS = "1";

   @RBEntry("MB")
   @RBComment("Abbreviation for megabytes; used as unit of measure in tabular data")
   public static final String MB = "2";

   @RBEntry("Server Status")
   @RBComment("Title of server status page; refers to health status of Windchill server")
   public static final String SERVER_STATUS = "3";

   @RBEntry("Current Active Users")
   @RBComment("Label for data field containing number of currently active Windchill users")
   public static final String CURRENT_ACTIVE_USERS = "4";

   @RBEntry("Server Managers")
   @RBComment("Label for data field containing list of server manager names")
   public static final String SERVER_MANAGERS = "5";

   @RBEntry("master")
   @RBComment("Parenthetical description attached to the name of the server manager which is the cache master")
   public static final String MASTER = "6";

   @RBEntry("JMX URL")
   @RBComment("Label for data field JMX JSR 160 protocol connection URL for a given server")
   public static final String JMX_URL = "8";

   @RBEntry("Uptime")
   @RBComment("Label for data field containing duration of time for which a server has been up and running")
   public static final String UPTIME = "9";

   @RBEntry("Recent")
   @RBComment("Label for data column containing recent data")
   public static final String RECENT = "10";

   @RBEntry("Baseline")
   @RBComment("Label for data column containing aggregate data obtained since last aggregator reset, i.e. baseline, time")
   public static final String BASELINE = "11";

   @RBEntry("Time In Garbage Collection")
   @RBComment("Label for data fields containing time spent in garbage collection (GC) as a percentage of overall time")
   public static final String TIME_IN_GC = "12";

   @RBEntry("CPU Used by Process")
   @RBComment("Label for data fields containing CPU time used by a given process as a percentage of overall CPU time")
   public static final String CPU_USED_BY_PROCESS = "13";

   @RBEntry("Active Sessions")
   @RBComment("Label for data fields containing the number of active servlet engine sessions")
   public static final String ACTIVE_SESSIONS = "14";

   @RBEntry("Average Response Time")
   @RBComment("Label for data fields containing the average response time for servlet engine requests")
   public static final String AVERAGE_RESPONSE_TIME = "15";

   @RBEntry("In JNDI Calls")
   @RBComment("Label for data fields containing percentage of time spent in JNDI calls")
   public static final String IN_JNDI_CALLS = "23";

   @RBEntry("Memory In Use")
   @RBComment("Label for data fields containing amount of memory used")
   public static final String MEMORY_USED = "24";

   @RBEntry("Heap")
   @RBComment("Label for data field containing amount of heap memory used")
   public static final String HEAP = "25";

   @RBEntry("Perm Gen")
   @RBComment("Label for data field containing amount of perm gen memory used")
   public static final String PERM_GEN = "26";

   @RBEntry("Available System Memory")
   @RBComment("Label for data fields containing amount of system memory available")
   public static final String AVAILABLE_SYSTEM_MEMORY = "27";

   @RBEntry("Physical")
   @RBComment("Label for data field containing amount of physical memory available")
   public static final String PHYSICAL = "28";

   @RBEntry("Swap")
   @RBComment("Label for data field containing amount of swap memory available")
   public static final String SWAP = "29";

   @RBEntry("Other System Info")
   @RBComment("Label for data fields containing miscellaneous other system information")
   public static final String OTHER_SYSTEM_INFO = "30";

   @RBEntry("Load Average")
   @RBComment("Label for data field containing system load average metric")
   public static final String LOAD_AVERAGE = "31";

   @RBEntry("Back to top")
   @RBComment("Label for hyperlink to top of HTML page")
   public static final String BACK_TO_TOP = "32";

   @RBEntry("Failed to retrieve server manager data")
   @RBComment("Message noting failure to retrieve server manager data")
   public static final String FAILED_TO_RETRIEVE_SERVER_MANAGER_DATA = "33";

   @RBEntry("ERROR")
   @RBComment("Label for data field containing details on an error -- with emphasis")
   public static final String ERROR_EMPHASIZED = "34";

   @RBEntry("Server Manager")
   @RBComment("Label for data field containing name of server manager")
   public static final String SERVER_MANAGER = "35";

   @RBEntry("Master Server Manager")
   @RBComment("Label for data field containing name of master server manager")
   public static final String MASTER_SERVER_MANAGER = "36";

   @RBEntry("Method Server Data")
   @RBComment("Label for table containing method server data")
   public static final String METHOD_SERVER_DATA = "37";

   @RBEntry("Method Context Time")
   @RBComment("Label for data fields containing metrics on time spent in method contexts")
   public static final String METHOD_CONTEXT_TIME = "39";

   @RBEntry("In JDBC Calls")
   @RBComment("Label for data fields containing time spent in JDBC calls")
   public static final String IN_JDBC_CALLS = "40";

   @RBEntry("In JDBC Connection Wait")
   @RBComment("Label for data fields containing time spent waiting for a JDBC connection")
   public static final String IN_JDBC_CONN_WAIT = "41";

   @RBEntry("Method Server")
   @RBComment("Label for fields containing method server names")
   public static final String METHOD_SERVER = "42";

   @RBEntry("Error")
   @RBComment("Label for data field containing details on an error -- without emphasis")
   public static final String ERROR = "43";

   @RBEntry("Data collected between {0} and {1}")
   @RBComment("Message format used to state that data was collected between one time and another")
   public static final String DATA_COLLECTED_BETWEEN_MSG = "44";

   @RBEntry("Deadlocked")
   @RBComment("Label for fields denoting whether a given Java process has deadlocked threads")
   public static final String DEADLOCKED = "45";

   @RBEntry("No")
   public static final String NO = "46";

   @RBEntry("Yes")
   public static final String YES = "47";

   @RBEntry("Contact Technical Support")
   @RBComment("Label for button through which customer may send logs and other system data to technical support")
   public static final String CONTACT_TECHNICAL_SUPPORT = "50";

   @RBEntry("Successfully sent system data to Technical Support for call number: {0}")
   @RBComment("Message given upon having successful sent system data to technical support")
   public static final String TECH_SUPPORT_CONTACTED_SUCCESSFULLY = "51";

   @RBEntry("OK")
   @RBComment("OK button label")
   public static final String OK = "52";

   @RBEntry("Call number")
   @RBComment("Label for text field in which user may enter a technical support call number")
   public static final String CALL_NUMBER = "53";

   @RBEntry("Invalid technical support call number: {0}")
   @RBComment("Message given when an invalid technical support call number has been provided")
   public static final String INVALID_CALL_NUMBER_MSG = "54";

   @RBEntry("Send system data to Technical Support and associate it with an existing support call number.")
   @RBComment("Tooltip for contact technical support action buttons")
   public static final String CONTACT_TECHNICAL_SUPPORT_TOOLTIP = "55";

   @RBEntry("Cancel")
   @RBComment("Cancel button label")
   public static final String CANCEL = "56";

   @RBEntry("Close window")
   @RBComment("Tooltip for buttons that close/dismiss the current browser window")
   public static final String DISMISS_WINDOW_TOOLTIP = "57";

   @RBEntry("Enter call number to which the system data to be sent is related.")
   @RBComment("Tooltip for call number input field")
   public static final String CALL_NUMBER_TOOLTIP = "58";

   @RBEntry("Must be system administrator")
   @RBComment("Exception message given if non-system administrator attempts system-administrator-only operation")
   public static final String MUST_BE_SYS_ADMIN_MSG = "59";

   @RBEntry("Help")
   @RBComment("Alt text for help button")
   public static final String HELP = "60";

   @RBEntry("Failed to retrieve method server data")
   @RBComment("Message noting failure to retrieve method server data")
   public static final String FAILED_TO_RETRIEVE_METHOD_SERVER_DATA = "61";

   @RBEntry("Servlet Requests: General")
   @RBComment("Label for general servlet request data section")
   public static final String GENERAL_SERVLET_REQUESTS_LABEL = "62";

   @RBEntry("Servlet Requests: Help Center")
   @RBComment("Label for help servlet request data section")
   public static final String HELP_SERVLET_REQUESTS_LABEL = "63";

   @RBEntry("Servlet Requests: Solr")
   @RBComment("Label for Solr servlet request data section (Solr is the proper name of the search engine being used)")
   public static final String SOLR_SERVLET_REQUESTS_LABEL = "64";

   @RBEntry("Maximum Concurrency")
   @RBComment("Label for maximum concurrency data")
   public static final String MAXIMUM_CONCURRENCY = "65";

   @RBEntry("Average Concurrency")
   @RBComment("Label for average concurrency data")
   public static final String AVERAGE_CONCURRENCY = "66";

   @RBEntry("Completed Requests")
   @RBComment("Label for completed request count data")
   public static final String COMPLETED_REQUESTS = "67";

   @RBEntry("Method Contexts")
   @RBComment("Label for method contexts data section")
   public static final String METHOD_CONTEXTS = "68";

   @RBEntry("Completed Contexts")
   @RBComment("Label for count of completed method contexts")
   public static final String COMPLETED_CONTEXTS = "69";

   @RBEntry("System Configuration Collector")
   @RBComment("The displayed text for the Site Information page link.")
   public static final String SITE_INFORMATION_LINK = "70";

   @RBEntry("Available")
   @RBComment("Up in the sense of a server being up and running and reachable")
   public static final String UP = "71";

   @RBEntry("Unavailable")
   @RBComment("Unreachable as in a server cannot successfully be reached/accessed")
   public static final String UNREACHABLE = "72";

   @RBEntry("Available")
   @RBComment("Reachable as in a server can successfully be reached/accessed")
   public static final String REACHABLE = "73";

   @RBEntry("{0} Available; {1} Unavailable")
   @RBComment("This message is intended as a short status summary")
   @RBArgComment0("Number of sites which are up, running, and reachable")
   @RBArgComment1("Number of sites which are unreachable")
   public static final String REACHABLE_SITES_MSG = "74";

   @RBEntry("Windchill Directory Server")
   @RBComment("Name of Windchill Directory Server product")
   public static final String WINDCHILL_DS = "75";

   @RBEntry("File Servers")
   @RBComment("Label for file vault sites")
   public static final String FILE_VAULT_SITES = "76";

   @RBEntry("Site URL")
   @RBComment("Label for vaulting site URL column")
   public static final String SITE_URL = "77";

   @RBEntry("Name")
   @RBComment("Label for a name column")
   public static final String NAME = "78";

   @RBEntry("Status")
   @RBComment("Label for a status column")
   public static final String STATUS = "79";

   @RBEntry("Time of Last Ping")
   @RBComment("Label for a column containing the last time at which a ping was performed")
   public static final String TIME_OF_LAST_PING = "80";

   @RBEntry("Other Statistics")
   @RBComment("Label for a column containing other statistics like Time in GC and CPU Used")
   public static final String OTHER_STATS = "81";

   @RBEntry("sec")
   @RBComment("Abbreviation for seconds used after a time value")
   public static final String SECONDS = "82";

   @RBEntry("Availability")
   @RBComment("Label for a percent of time a server is up and reachable")
   public static final String AVAILABILITY = "83";

   @RBEntry("Monitoring Tools")
   @RBComment("Label for hyperlink to page containing links to more system health monitoring tools")
   public static final String MONITORING_TOOLS = "84";

   @RBEntry("System Health Monitoring Tools")
   @RBComment("Tooltip for hyperlink to page containing links to more system health monitoring tools")
   public static final String MONITORING_TOOLS_TOOLTIP = "85";

   @RBEntry("In Remote Cache Calls")
   @RBComment("Label for data fields containing percentage of time spent in remote cache calls")
   public static final String IN_REMOTE_CACHE_CALLS = "86";
}
