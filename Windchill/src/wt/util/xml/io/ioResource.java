/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.util.xml.io;

import wt.util.resource.*;

@RBUUID("wt.util.xml.io.ioResource")
public final class ioResource extends WTListResourceBundle {
   /**
    * Example: Cannot determine which Java character encoding should be used from the character encoding UTF-8.
    **/
   @RBEntry("Cannot determine which {1} character encoding should be used from the character encoding {0}.")
   @RBArgComment0("The character encoding string encountered during input of an XML document (e.g. \"UTF-8\", \"8859_1\").")
   @RBArgComment1("The name of the character encoding system in which the encoding string is expected to be a valid encoding (e.g. MIME, Java).")
   public static final String ENCODING_NOT_FOUND = "0";
}
