/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.util.xml.xpath;

import wt.util.resource.*;

@RBUUID("wt.util.xml.xpath.xpathResource")
public final class xpathResource extends WTListResourceBundle {
   @RBEntry("The specified XPathFactory class {0} cannot be found.")
   public static final String SPECIFIED_CLASS_NOT_FOUND = "0";

   @RBEntry("The specficied XPathFactory class {0} does not extend {1}.")
   public static final String FACTORY_IMPL_DOES_NOT_EXTEND_FACTORY_CLASS = "1";

   @RBEntry("The XPathFactory class {0} is not instantiable as a concrete class.")
   public static final String FACTORY_IMPL_NOT_INSTANTIABLE = "2";

   @RBEntry("The XPathFactory class {0} is not declared public or does not have a public no-arg constructor.")
   public static final String FACTORY_IMPL_HAS_NO_PUB_NOARG_CTOR = "3";

   @RBEntry("Error creating new XPath for expression {0}")
   public static final String ERROR_CREATING_NEW_XPATH = "4";

   @RBEntry("Error creating new XPath match pattern for expression {0}")
   public static final String ERROR_CREATING_NEW_XPATH_PATTERN = "5";

   @RBEntry("Error evaluating XPath {0}")
   public static final String ERROR_EVALUATING_XPATH = "6";

   @RBEntry("Error determining XPath match score.  XPath is {0}")
   public static final String ERROR_GETTING_XPATH_MATCH_SCORE = "7";
}
