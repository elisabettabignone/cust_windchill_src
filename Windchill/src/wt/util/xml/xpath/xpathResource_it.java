/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.util.xml.xpath;

import wt.util.resource.*;

@RBUUID("wt.util.xml.xpath.xpathResource")
public final class xpathResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile trovare la classe specificata XPathFactory {0}.")
   public static final String SPECIFIED_CLASS_NOT_FOUND = "0";

   @RBEntry("La classe XPathFactory {0} specificata non estende {1}.")
   public static final String FACTORY_IMPL_DOES_NOT_EXTEND_FACTORY_CLASS = "1";

   @RBEntry("La classe XPathFactory {0} non è istanziabile come classe concreta.")
   public static final String FACTORY_IMPL_NOT_INSTANTIABLE = "2";

   @RBEntry("La classe XPathFactory {0} non è dichiarata pubblica o non ha un costruttore pubblico senza argomenti.")
   public static final String FACTORY_IMPL_HAS_NO_PUB_NOARG_CTOR = "3";

   @RBEntry("Errore durante la creazione del nuovo XPath per l'espressione {0}")
   public static final String ERROR_CREATING_NEW_XPATH = "4";

   @RBEntry("Errore durante la creazione della nuova espressione di corrispondenza XPath per l'espressione {0}")
   public static final String ERROR_CREATING_NEW_XPATH_PATTERN = "5";

   @RBEntry("Errore durante la valutazione di XPath {0}")
   public static final String ERROR_EVALUATING_XPATH = "6";

   @RBEntry("Errore durante la determinazione del risultato di corrispondenza XPath. XPath è {0}")
   public static final String ERROR_GETTING_XPATH_MATCH_SCORE = "7";
}
