/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.util.version;

import wt.util.resource.*;

@RBUUID("wt.util.version.versionResource")
public final class versionResource_it extends WTListResourceBundle {
   @RBEntry("Una versione nulla non è consentita.")
   public static final String NULL_VERSION = "0";

   @RBEntry("Una versione principale nulla non è consentita")
   public static final String NULL_MAJOR = "1";

   @RBEntry("Nella stringa della versione è stato trovato un carattere non numerico: {0}")
   public static final String NONNUMERIC_VERSION = "2";

   @RBEntry("La versione immessa non è valida: {0}")
   public static final String INVALID_VERSION = "3";

   @RBEntry("La versione contiene un valore negativo: {0}.")
   public static final String NEGATIVE_VERSION = "4";

   @RBEntry("Esiste una versione secondaria o micro nulla con una versione build o micro non nulla")
   public static final String INCOMPLETE_VERSION_INFO = "5";
}
