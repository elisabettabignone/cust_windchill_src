/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.util.version;

import wt.util.resource.*;

@RBUUID("wt.util.version.versionResource")
public final class versionResource extends WTListResourceBundle {
   @RBEntry("Null version is not allowed.")
   public static final String NULL_VERSION = "0";

   @RBEntry("Null major is not allowed")
   public static final String NULL_MAJOR = "1";

   @RBEntry("Non-numeric character found in version string: {0}")
   public static final String NONNUMERIC_VERSION = "2";

   @RBEntry("The version you have entered is invalid: {0}")
   public static final String INVALID_VERSION = "3";

   @RBEntry("The version contains a negative value: {0}")
   public static final String NEGATIVE_VERSION = "4";

   @RBEntry("There is a null minor or micro version with a non-null micro or build version")
   public static final String INCOMPLETE_VERSION_INFO = "5";
}
