/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.util;

import wt.util.resource.*;

@RBUUID("wt.util.utilResource")
public final class utilResource_it extends WTListResourceBundle {
   @RBEntry("Il file {0} non esiste o non è un file")
   public static final String FILE_NOT_FOUND_ERROR = "0";

   @RBEntry(" Eccezione annidata:")
   public static final String NESTED_EXCEPTION_1 = "1";

   @RBEntry("; l'eccezione annidata è:")
   public static final String NESTED_EXCEPTION_2 = "2";

   /**
    * 1998-05-15 13:25:58
    **/
   @RBEntry("dd/MM/yyyy HH.mm.ss z")
   @RBPseudo(false)
   public static final String LONG_STANDARD_DATE_FORMAT = "3";

   /**
    * 15 Mar 98 13:25
    **/
   @RBEntry("dd MMM yy HH.mm")
   @RBPseudo(false)
   public static final String SHORT_STANDARD_DATE_FORMAT = "4";

   /**
    * SHORT_STANDARD_DATE_FORMAT above is primarily being used by the Java code for parsing date in the
    * specified format. This means that the date format cannot be completely localized (i.e. modify
    * the letters as well as the sequence and separators), because Java could not parse it.
    * Separate "display version" of this string is created
    * below. This string is to display next to date fields in our html forms where
    * user needs to see the syntactically correct way to enter the date.
    **/
   @RBEntry("gg MMM aa HH.mm")
   public static final String SHORT_STANDARD_DATE_INPUT_FORMAT = "4_INPUT";

   @RBEntry("wt.util.Cache: dimensione > 10922")
   public static final String MAX_SIZE = "5";

   @RBEntry("Errore nella lettura delle intestazioni del corpo MIME")
   public static final String MIME_READ_ERROR = "6";

   @RBEntry("Rilevata un'interruzione prematura del flusso di input.")
   public static final String INPUT_EOF = "7";

   @RBEntry("Intestazione dell'oggetto MIME errata.")
   public static final String MALFORMED_MIME = "8";

   @RBEntry("Eccezione di I/O in MappedRegistry.read() per il file {0}")
   public static final String MAPPED_REGISTRY_READ_ERROR = "9";

   @RBEntry("chiave non individuata")
   public static final String KEY_NOT_FOUND = "10";

   @RBEntry("Eccezione di file non trovato in MappedRegistry.write()")
   public static final String MAPPED_REGISTRY_WRITE_ERROR = "11";

   @RBEntry("Eccezione di I/O in MappedRegistry.write() per il file {0}")
   public static final String MAPPED_REGISTRY_WRITE_IOERROR = "12";

   @RBEntry("La clonazione non è riuscita.")
   public static final String CLONE_FAILED = "13";

   @RBEntry("Il riferimento non è un java.rmi.server.ServerRef")
   public static final String REFERENCE_NOT_SERVERREF = "14";

   @RBEntry("Impossibile creare un riferimento remoto")
   public static final String CREATE_REMOTE_REF_ERROR = "15";

   @RBEntry("Proprietà di sistema mancante: java.class.path")
   public static final String MISSING_PROPERTY = "16";

   /**
    * Only localize "no" and "yes" (not "choice").
    **/
   @RBEntry("{0, choice, 0#no|1#sì}")
   public static final String YESNO_STANDARD_BOOLEAN_FORMAT = "17";

   /**
    * Only localize "false" and "true" (not "choice").
    **/
   @RBEntry("{0, choice, 0#false|1#true}")
   public static final String TRUEFALSE_STANDARD_BOOLEAN_FORMAT = "18";

   @RBEntry("dd/MM/yy HH.mm")
   @RBPseudo(false)
   public static final String EXPLORER_STANDARD_DATE_FORMAT = "19";

   @RBEntry("gg/mm/aa hh.mm")
   @RBComment("This is displayed next to date input fields in our html forms to show user the syntactically correct way to enter the date and time. The format and elements of the string should match the locale-specific EXPLORER_STANDARD_DATE_FORMAT but the letters may be translated for each locale.")
   public static final String EXPLORER_STANDARD_DATE_INPUT_FORMAT = "19_INPUT_DISPLAY_STRING";

   @RBEntry("dd/MM/yy")
   @RBPseudo(false)
   public static final String EXPLORER_STANDARD_DATE_ONLY_FORMAT = "20";

   @RBEntry("gg/mm/aa")
   @RBComment("This value should be the same as EXPLORER_STANDARD_DATE_INPUT_FORMAT for this locale except that time should be omitted.")
   public static final String EXPLORER_STANDARD_DATE_ONLY_INPUT_FORMAT = "20_INPUT_DISPLAY_STRING";

   @RBEntry("HH.mm")
   @RBPseudo(false)
   public static final String EXPLORER_STANDARD_TIME_ONLY_FORMAT = "21";

   @RBEntry("HH.mm")
   @RBComment("This value should be the same as EXPLORER_STANDARD_DATE_INPUT_FORMAT for this locale except that time only should be shown.")
   public static final String EXPLORER_STANDARD_TIME_ONLY_INPUT_FORMAT = "21_INPUT_DISPLAY_STRING";

   @RBEntry("dd/MM/yyyy")
   @RBPseudo(false)
   public static final String WF_STANDARD_DATE_ONLY_FORMAT = "22";

   @RBEntry("gg/mm/aaaa")
   @RBComment("This is displayed next to date input fields in our html forms to show user the syntactically correct way to enter the date and time. The format and elements of the string should match the locale-specific WF_STANDARD_DATE_ONLY_FORMAT but the letters may be translated for each locale.")
   public static final String WF_STANDARD_DATE_ONLY_INPUT_FORMAT = "22_INPUT_DISPLAY_STRING";

   @RBEntry(", ")
   @RBPseudo(false)
   @RBComment("Default list separator used when combining localized messages (for example, a list of EnumeratedTypes) into a string")
   public static final String DEFAULT_LIST_SEPARATOR = "23";

   @RBEntry("Il parametro \"collection\" non è valido. È nullo oppure la raccolta contiene un oggetto che non è un wt.util.LocalizableMessage.")
   @RBComment("The collection parameter (type java.util.Collection) for the wt.util.LocalizableMessageCollection constructor is invalid")
   public static final String INVALID_COLLECTION = "24";

   @RBEntry("dd/MM/yyyy HH.mm.ss")
   @RBPseudo(false)
   @RBComment("This value should be the same as LONG_STANDARD_DATE_FORMAT (#3) for this locale except that timezone should be omitted")
   public static final String LONG_STANDARD_DATE_FORMAT_MINUS_TIMEZONE = "25";

   @RBEntry("gg/mm/aaaa hh.mm.ss")
   @RBComment("This value should be the same as LONG_STANDARD_DATE_INPUT_FORMAT for this locale except that timezone should be omitted")
   public static final String LONG_STANDARD_DATE_INPUT_FORMAT_MINUS_TIMEZONE = "25_INPUT_DISPLAY_STRING";

   @RBEntry("dd/MM/yyyy")
   @RBPseudo(false)
   @RBComment("This value should be the same as LONG_STANDARD_DATE_FORMAT (#3)for this locale except that time and timezone should be omitted")
   public static final String LONG_STANDARD_DATE_FORMAT_MINUS_TIME = "26";

   @RBEntry("gg/mm/aaaa")
   @RBComment("This value should be the same as LONG_STANDARD_DATE_INPUT_FORMAT for this locale except that time and timezone should be omitted.")
   public static final String LONG_STANDARD_DATE_INPUT_FORMAT_MINUS_TIME = "26_INPUT_DISPLAY_STRING";

   @RBEntry("GG/MM/AAAA HH.MM.SS")
   @RBComment("This is displayed next to date input fields in our html forms to show user the syntactically correct way to enter the date and time. The format and elements of the string should match the locale-specific LONG_STANDARD_DATE_FORMAT (#3) but the letters may be translated for each locale.  For example, in German the LONG_STANDARD_DATE_FORMAT is \"dd.MM.yyyy HH:mm:ss z\" so the LONG_STANDARD_DATE_INPUT_FORMAT might begin \"TT.MM.JJJJ ...\"")
   public static final String LONG_STANDARD_DATE_INPUT_FORMAT = "27";

   @RBEntry("Eccezione annidata:")
   public static final String NESTED_EXCEPTION_3 = "28";

   @RBEntry("Contesto InstallAnywhere: {0}")
   public static final String INSTALL_ANYWHERE_CONTEXT = "29";

   @RBEntry("La stringa della data \"{0}\" ha un formato non valido. Rilevato un errore alla posizione {1} della stringa di input. ")
   public static final String DATE_PARSE_ERROR = "30";

   @RBEntry("Si è verificato un errore. Consultare il log del server per i dettagli.")
   @RBComment("Alerts the user that an error occurred on the server and if they need more details, to see the server log.")
   public static final String CLIENT_ERROR_MSG = "31";

   @RBEntry("La data \"{0}\" non è valida. Le date devono essere nel formato {1}. ")
   public static final String DATE_FORMAT_ERROR = "32";

   @RBEntry("URI non valido: {0}")
   public static final String INVALID_URI = "33";

   @RBEntry("Non consentito")
   public static final String UNALLOWED = "34";

   @RBEntry("Errore durante l'inizializzazione di \"{0}\".")
   public static final String ERROR_INITIALIZING = "100";
}
