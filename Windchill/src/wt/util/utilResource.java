/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.util;

import wt.util.resource.*;

@RBUUID("wt.util.utilResource")
public final class utilResource extends WTListResourceBundle {
   @RBEntry("File {0} does not exist, or is not a file")
   public static final String FILE_NOT_FOUND_ERROR = "0";

   @RBEntry(" Nested exception is:")
   public static final String NESTED_EXCEPTION_1 = "1";

   @RBEntry("; nested exception is:")
   public static final String NESTED_EXCEPTION_2 = "2";

   /**
    * 1998-05-15 13:25:58
    **/
   @RBEntry("yyyy-MM-dd HH:mm:ss z")
   @RBPseudo(false)
   public static final String LONG_STANDARD_DATE_FORMAT = "3";

   /**
    * 15 Mar 98 13:25
    **/
   @RBEntry("dd MMM yy HH:mm")
   @RBPseudo(false)
   public static final String SHORT_STANDARD_DATE_FORMAT = "4";

   /**
    * SHORT_STANDARD_DATE_FORMAT above is primarily being used by the Java code for parsing date in the
    * specified format. This means that the date format cannot be completely localized (i.e. modify
    * the letters as well as the sequence and separators), because Java could not parse it.
    * Separate "display version" of this string is created
    * below. This string is to display next to date fields in our html forms where
    * user needs to see the syntactically correct way to enter the date.
    **/
   @RBEntry("dd MMM yy HH:mm")
   public static final String SHORT_STANDARD_DATE_INPUT_FORMAT = "4_INPUT";

   @RBEntry("wt.util.Cache: size > 10922")
   public static final String MAX_SIZE = "5";

   @RBEntry("Error reading MIME body headers")
   public static final String MIME_READ_ERROR = "6";

   @RBEntry("End of input stream prematurely encountered.")
   public static final String INPUT_EOF = "7";

   @RBEntry("Malformed MIME Object Header.")
   public static final String MALFORMED_MIME = "8";

   @RBEntry("IOException in MappedRegistry.read() for {0}")
   public static final String MAPPED_REGISTRY_READ_ERROR = "9";

   @RBEntry("key unexpectedly not found")
   public static final String KEY_NOT_FOUND = "10";

   @RBEntry("FileNotFoundException in MappedRegistry.write()")
   public static final String MAPPED_REGISTRY_WRITE_ERROR = "11";

   @RBEntry("IOException in MappedRegistry.write() for {0}")
   public static final String MAPPED_REGISTRY_WRITE_IOERROR = "12";

   @RBEntry("Clone failed")
   public static final String CLONE_FAILED = "13";

   @RBEntry("Reference is not a java.rmi.server.ServerRef")
   public static final String REFERENCE_NOT_SERVERREF = "14";

   @RBEntry("Unable to create remote reference")
   public static final String CREATE_REMOTE_REF_ERROR = "15";

   @RBEntry("Missing system property: java.class.path")
   public static final String MISSING_PROPERTY = "16";

   /**
    * Only localize "no" and "yes" (not "choice").
    **/
   @RBEntry("{0, choice, 0#no|1#yes}")
   public static final String YESNO_STANDARD_BOOLEAN_FORMAT = "17";

   /**
    * Only localize "false" and "true" (not "choice").
    **/
   @RBEntry("{0, choice, 0#false|1#true}")
   public static final String TRUEFALSE_STANDARD_BOOLEAN_FORMAT = "18";

   @RBEntry("M/dd/yy h:mm a")
   @RBPseudo(false)
   public static final String EXPLORER_STANDARD_DATE_FORMAT = "19";

   @RBEntry("M/dd/yy h:mm a")
   @RBComment("This is displayed next to date input fields in our html forms to show user the syntactically correct way to enter the date and time. The format and elements of the string should match the locale-specific EXPLORER_STANDARD_DATE_FORMAT but the letters may be translated for each locale.")
   public static final String EXPLORER_STANDARD_DATE_INPUT_FORMAT = "19_INPUT_DISPLAY_STRING";

   @RBEntry("M/dd/yy")
   @RBPseudo(false)
   public static final String EXPLORER_STANDARD_DATE_ONLY_FORMAT = "20";

   @RBEntry("M/dd/yy")
   @RBComment("This value should be the same as EXPLORER_STANDARD_DATE_INPUT_FORMAT for this locale except that time should be omitted.")
   public static final String EXPLORER_STANDARD_DATE_ONLY_INPUT_FORMAT = "20_INPUT_DISPLAY_STRING";

   @RBEntry("h:mm a")
   @RBPseudo(false)
   public static final String EXPLORER_STANDARD_TIME_ONLY_FORMAT = "21";

   @RBEntry("h:mm a")
   @RBComment("This value should be the same as EXPLORER_STANDARD_DATE_INPUT_FORMAT for this locale except that time only should be shown.")
   public static final String EXPLORER_STANDARD_TIME_ONLY_INPUT_FORMAT = "21_INPUT_DISPLAY_STRING";

   @RBEntry("MM/dd/yyyy")
   @RBPseudo(false)
   public static final String WF_STANDARD_DATE_ONLY_FORMAT = "22";

   @RBEntry("MM/dd/yyyy")
   @RBComment("This is displayed next to date input fields in our html forms to show user the syntactically correct way to enter the date and time. The format and elements of the string should match the locale-specific WF_STANDARD_DATE_ONLY_FORMAT but the letters may be translated for each locale.")
   public static final String WF_STANDARD_DATE_ONLY_INPUT_FORMAT = "22_INPUT_DISPLAY_STRING";

   @RBEntry(", ")
   @RBPseudo(false)
   @RBComment("Default list separator used when combining localized messages (for example, a list of EnumeratedTypes) into a string")
   public static final String DEFAULT_LIST_SEPARATOR = "23";

   @RBEntry("The \"collection\" parameter is invalid.  It is either null, or the collection contains an object that is not a wt.util.LocalizableMessage.")
   @RBComment("The collection parameter (type java.util.Collection) for the wt.util.LocalizableMessageCollection constructor is invalid")
   public static final String INVALID_COLLECTION = "24";

   @RBEntry("yyyy-MM-dd HH:mm:ss")
   @RBPseudo(false)
   @RBComment("This value should be the same as LONG_STANDARD_DATE_FORMAT (#3) for this locale except that timezone should be omitted")
   public static final String LONG_STANDARD_DATE_FORMAT_MINUS_TIMEZONE = "25";

   @RBEntry("yyyy-MM-dd HH:mm:ss")
   @RBComment("This value should be the same as LONG_STANDARD_DATE_INPUT_FORMAT for this locale except that timezone should be omitted")
   public static final String LONG_STANDARD_DATE_INPUT_FORMAT_MINUS_TIMEZONE = "25_INPUT_DISPLAY_STRING";

   @RBEntry("yyyy-MM-dd")
   @RBPseudo(false)
   @RBComment("This value should be the same as LONG_STANDARD_DATE_FORMAT (#3)for this locale except that time and timezone should be omitted")
   public static final String LONG_STANDARD_DATE_FORMAT_MINUS_TIME = "26";

   @RBEntry("yyyy-MM-dd")
   @RBComment("This value should be the same as LONG_STANDARD_DATE_INPUT_FORMAT for this locale except that time and timezone should be omitted.")
   public static final String LONG_STANDARD_DATE_INPUT_FORMAT_MINUS_TIME = "26_INPUT_DISPLAY_STRING";

   @RBEntry("YYYY-MM-DD HH:MM:SS TZ")
   @RBComment("This is displayed next to date input fields in our html forms to show user the syntactically correct way to enter the date and time. The format and elements of the string should match the locale-specific LONG_STANDARD_DATE_FORMAT (#3) but the letters may be translated for each locale.  For example, in German the LONG_STANDARD_DATE_FORMAT is \"dd.MM.yyyy HH:mm:ss z\" so the LONG_STANDARD_DATE_INPUT_FORMAT might begin \"TT.MM.JJJJ ...\"")
   public static final String LONG_STANDARD_DATE_INPUT_FORMAT = "27";

   @RBEntry("Nested exception is:")
   public static final String NESTED_EXCEPTION_3 = "28";

   @RBEntry("InstallAnywhere context: {0}")
   public static final String INSTALL_ANYWHERE_CONTEXT = "29";

   @RBEntry("The date string, \"{0}\", has an invalid format. An error was detected at position {1} in the input string. ")
   public static final String DATE_PARSE_ERROR = "30";

   @RBEntry("An error occurred.  Please see server log for more details.")
   @RBComment("Alerts the user that an error occurred on the server and if they need more details, to see the server log.")
   public static final String CLIENT_ERROR_MSG = "31";

   @RBEntry("The date \"{0}\", is invalid. Dates must be in the format {1}. ")
   public static final String DATE_FORMAT_ERROR = "32";

   @RBEntry("An invalid URI was encountered: {0}")
   public static final String INVALID_URI = "33";

   @RBEntry("Unallowed")
   public static final String UNALLOWED = "34";

   @RBEntry("Error initializing \"{0}\".")
   public static final String ERROR_INITIALIZING = "100";
}
