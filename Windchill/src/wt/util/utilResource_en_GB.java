/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.util;

import wt.util.resource.*;

public final class utilResource_en_GB extends WTListResourceBundle {
   @RBEntry("File {0} does not exist, or is not a file")
   public static final String FILE_NOT_FOUND_ERROR = "0";

   @RBEntry(" Nested exception is:")
   public static final String NESTED_EXCEPTION_1 = "1";

   @RBEntry("; nested exception is:")
   public static final String NESTED_EXCEPTION_2 = "2";

   /**
    * 1998-05-15 13:25:58
    **/
   @RBEntry("dd/MM/yyyy HH:mm:ss z")
   public static final String LONG_STANDARD_DATE_FORMAT = "3";

   /**
    * 15 Mar 98 13:25
    **/
   @RBEntry("dd MMM yy HH:mm")
   public static final String SHORT_STANDARD_DATE_FORMAT = "4";

   @RBEntry("wt.util.Cache: size > 10922")
   public static final String MAX_SIZE = "5";

   @RBEntry("Error reading MIME body headers")
   public static final String MIME_READ_ERROR = "6";

   @RBEntry("End of input stream prematurely encountered.")
   public static final String INPUT_EOF = "7";

   @RBEntry("Malformed MIME Object Header.")
   public static final String MALFORMED_MIME = "8";

   @RBEntry("IOException in MappedRegistry.read() for {0}")
   public static final String MAPPED_REGISTRY_READ_ERROR = "9";

   @RBEntry("key unexpectedly not found")
   public static final String KEY_NOT_FOUND = "10";

   @RBEntry("FileNotFoundException in MappedRegistry.write()")
   public static final String MAPPED_REGISTRY_WRITE_ERROR = "11";

   @RBEntry("IOException in MappedRegistry.write() for {0}")
   public static final String MAPPED_REGISTRY_WRITE_IOERROR = "12";

   @RBEntry("Clone failed")
   public static final String CLONE_FAILED = "13";

   @RBEntry("Reference is not a java.rmi.server.ServerRef")
   public static final String REFERENCE_NOT_SERVERREF = "14";

   @RBEntry("Unable to create remote reference")
   public static final String CREATE_REMOTE_REF_ERROR = "15";

   @RBEntry("Missing system property: java.class.path")
   public static final String MISSING_PROPERTY = "16";

   /**
    * Only localize "no" and "yes" (not "choice").
    **/
   @RBEntry("{0, choice, 0#no|1#yes}")
   public static final String YESNO_STANDARD_BOOLEAN_FORMAT = "17";

   /**
    * Only localize "false" and "true" (not "choice").
    **/
   @RBEntry("{0, choice, 0#false|1#true}")
   public static final String TRUEFALSE_STANDARD_BOOLEAN_FORMAT = "18";

   @RBEntry("dd/M/yy HH:mm")
   public static final String EXPLORER_STANDARD_DATE_FORMAT = "19";

   @RBEntry("dd/M/yy")
   public static final String EXPLORER_STANDARD_DATE_ONLY_FORMAT = "20";

   @RBEntry("HH:mm")
   public static final String EXPLORER_STANDARD_TIME_ONLY_FORMAT = "21";

   @RBEntry("dd/MM/yyyy")
   public static final String WF_STANDARD_DATE_ONLY_FORMAT = "22";

   @RBEntry("dd/MM/yyyy")
   @RBComment("This is displayed next to date input fields in our html forms to show user the syntactically correct way to enter the date and time. The format and elements of the string should match the locale-specific WF_STANDARD_DATE_ONLY_FORMAT but the letters may be translated for each locale.")
   public static final String WF_STANDARD_DATE_ONLY_INPUT_FORMAT = "22_INPUT_DISPLAY_STRING";

   @RBEntry(", ")
   @RBComment("Default list separator used when combining localized messages (for example, a list of EnumeratedTypes) into a string")
   public static final String DEFAULT_LIST_SEPARATOR = "23";

   @RBEntry("The \"collection\" parameter is invalid.  It is either null, or the collection contains an object that is not a wt.util.LocalizableMessage.")
   @RBComment("The collection parameter (type java.util.Collection) for the wt.util.LocalizableMessageCollection constructor is invalid")
   public static final String INVALID_COLLECTION = "24";

   @RBEntry("dd/MM/yyyy HH:mm:ss")
   @RBComment("This value should be the same as LONG_STANDARD_DATE_FORMAT (#3) for this locale except that timezone should be omitted")
   public static final String LONG_STANDARD_DATE_FORMAT_MINUS_TIMEZONE = "25";

   @RBEntry("dd/MM/yyyy")
   @RBComment("This value should be the same as LONG_STANDARD_DATE_FORMAT (#3)for this locale except that time and timezone should be omitted")
   public static final String LONG_STANDARD_DATE_FORMAT_MINUS_TIME = "26";

   @RBEntry("DD/MM/YYYY HH:MM:SS TZ")
   @RBComment("This is displayed next to date input fields in our html forms to show user the syntactically correct way to enter the date and time. The format and elements of the string should match the locale-specific LONG_STANDARD_DATE_FORMAT (#3) but the letters may be translated for each locale.  For example, in German the LONG_STANDARD_DATE_FORMAT is \"dd.MM.yyyy HH:mm:ss z\" so the LONG_STANDARD_DATE_INPUT_FORMAT might begin \"TT.MM.JJJJ ...\"")
   public static final String LONG_STANDARD_DATE_INPUT_FORMAT = "27";
}
