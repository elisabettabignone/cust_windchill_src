/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.adapter;

import wt.util.resource.*;

@RBUUID("wt.adapter.adapterResource")
public final class adapterResource extends WTListResourceBundle {
   @RBEntry("The required webject parameter \"WHERE\" was not provided.")
   @RBComment("Missing webject parameter")
   public static final String MISSING_WHERE_CLAUSE = "0";

   @RBEntry("Webject processor \"{0}\" did not return output.")
   @RBComment("Webject processor did not return output")
   @RBArgComment0("Webject processor")
   public static final String NO_OUTPUT_GROUP = "1";

   @RBEntry("The required webject parameter \"CLASS\" was not provided.")
   @RBComment("Missing webject parameter")
   public static final String MISSING_CLASS = "2";

   @RBEntry("The required webject parameter \"DIRECTION\" was not provided.")
   @RBComment("Missing webject parameter")
   public static final String MISSING_DIRECTION = "3";

   @RBEntry("The required webject parameter \"RELATION\" was not provided.")
   @RBComment("Missing webject parameter")
   public static final String MISSING_RELATION = "4";

   @RBEntry("An error occured when trying to find the parent object.")
   @RBComment("Error message")
   public static final String PARENT_OBJECT_ERROR = "5";

   @RBEntry("Multiple parent objects were found.")
   @RBComment("Sucessful message")
   public static final String MULTIPLE_PARENT_OBJECTS = "6";

   @RBEntry("The required webject parameter \"{0}\" was not provided.")
   @RBComment("Missing required parameter")
   @RBArgComment0("The name of required webject parameter")
   public static final String MISSING_REQUIRED_PARAMETER = "8";

   @RBEntry("Unable to find target object \"{0}\".")
   @RBComment("Failed message")
   @RBArgComment0("target object")
   public static final String UNABLE_TO_FIND_TARGET_OBJECT = "9";

   @RBEntry("The required webject parameter \"ATTRIBUTE\" was not provided.")
   @RBComment("Missing webject parameter")
   public static final String MISSING_ATTRIBUTE = "10";

   @RBEntry("The required webject parameter \"FIELD\" was not provided.")
   @RBComment("Missing webject parameter")
   public static final String MISSING_FIELD = "11";

   @RBEntry("Connection attempt from \"{0}\", host not found in trusted list: {1}")
   @RBComment("Failed message")
   @RBArgComment0("The name of connecting host")
   @RBArgComment1("The list name of trusted hosts")
   public static final String UNTRUSTED_HOST = "12";

   @RBEntry("No webject named \"{0}\" applies to the class \"{1}\".")
   @RBComment("No webject delegate for the combination of webject name and class parameter.")
   public static final String NO_WEBJECT_DELEGATE = "13";

   @RBEntry("The session with id \"{0}\" is not owned by user \"{1}\".")
   @RBComment("No sesssion with the specified id is owned by another user.")
   public static final String NOT_SESSION_OWNER = "14";

   @RBEntry("No session exists with id \"{0}\".")
   @RBComment("No sesssion with the specified id exists.")
   public static final String NO_SUCH_SESSION = "15";

   @RBEntry("The session with id \"{0}\" is busy.")
   @RBComment("The specified session is in use.")
   public static final String SESSION_BUSY = "16";

   @RBEntry("Anonymous access to the session with id \"{0}\" is not allowed.")
   @RBComment("Access to sessions requires authentication.")
   public static final String ANONYMOUS_SESSION_ACCESS = "17";

   @RBEntry("No such group: \"{0}\"")
   @RBComment("The specified group does not exist.")
   public static final String NO_SUCH_GROUP = "18";

   @RBEntry("Access to Windchill denied.")
   @RBComment("The calling user does not have access to Windchill.")
   public static final String WINDCHILL_ACCESS_DENIED = "19";

   @RBEntry("The selection criteria did not match any objects.")
   public static final String NO_MATCHING_OBJECTS = "20";

   @RBEntry("The parameter \"{0}\" does not accept the value \"{1}\".")
   public static final String UNACCEPTABLE_PARAMETER_VALUE = "21";

   @RBEntry("No such object: \"{0}\".")
   @RBComment("No object with the specified UFID exits.")
   public static final String NO_SUCH_OBJECT = "22";

   @RBEntry("{0} is not Iterated.")
   @RBArgComment0("The specified object doesn't implement the Iterated interface.")
   public static final String OBJECT_NOT_ITERATED = "23";

   @RBEntry("{0} is not LifeCycleManaged.")
   @RBArgComment0("The specified object doesn't implement LifeCycleManaged.")
   public static final String OBJECT_NOT_LIFECYCLEMANAGED = "24";

   @RBEntry("The container {0} does not support principals of type {1} in the service {2}.")
   public static final String UNSUPPORTED_CLASS_AND_SERVICE = "25";

   @RBEntry("The attribute \"{0}\" does not accept the value \"{1}\".")
   public static final String UNACCEPTABLE_ATTRIBUTE_VALUE = "26";

   @RBEntry("{0} is not Notifiable.")
   @RBArgComment0("The Object name.")
   public static final String OBJECT_NOT_NOTIFIABLE = "27";

   @RBEntry("Cannot update attributes with multiple values ")
   public static final String UPDATE_MULTIPLE_VALUES_MESSAGE = "28";

   @RBEntry("Cannot update attributes values of this type - ")
   public static final String UNSUPPORTED_UPDATE_TYPE = "29";

   @RBEntry("Object is not an IBAHolder, invalid attribute - ")
   public static final String OBJECT_NOT_IBAHOLDER = "30";

   @RBEntry("Invalid attribute name - ")
   public static final String INVALID_ATTRIBUTE_NAME_MESSAGE = "31";

   @RBEntry("Object is already checked out by another user ")
   public static final String OBJECT_ALREADY_CHECKED_OUT = "32";
}
