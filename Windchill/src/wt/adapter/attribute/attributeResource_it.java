/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.adapter.attribute;

import wt.util.resource.*;

@RBUUID("wt.adapter.attribute.attributeResource")
public final class attributeResource_it extends WTListResourceBundle {
   @RBEntry("{0} {1}")
   @RBComment("The Quantity message will show the 'amount' and the 'units' for a quantity. For example, \"1 each\", \"2.5 liters\", \"5.0 kilograms\"")
   @RBArgComment0("The string represents the amount")
   @RBArgComment1("The string represents the units")
   public static final String QUANTITY = "0";

   @RBEntry("Gli aggiornamenti degli attributi di tipo: \"{0}\" non sono supportati")
   @RBComment("This message is for developers only; should not been seen by the user. The message indicated that the specified attribute data type cannot be updated")
   @RBArgComment0("The name of the attribute type")
   public static final String UPDATE_NOT_SUPPORTED = "1";

   @RBEntry("Impossibile inizializzare la classe.")
   @RBComment("Failed message")
   public static final String UNABLE_TO_INITIALIZE_CLASS = "7";
}
