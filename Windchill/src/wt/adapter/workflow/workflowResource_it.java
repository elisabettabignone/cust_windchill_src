/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.adapter.workflow;

import wt.util.resource.*;

@RBUUID("wt.adapter.workflow.workflowResource")
public final class workflowResource_it extends WTListResourceBundle {
   @RBEntry("\"{0}\" non è una priorità valida")
   @RBArgComment0("The priority.")
   public static final String INVALID_PRIORITY = "0";

   @RBEntry("\"{0}\" non è un nome di progetto valido")
   @RBArgComment0("The name of the project.")
   public static final String INVALID_PROJECT = "2";

   @RBEntry("\"{0}\" non è un oggetto di destinazione valido")
   @RBArgComment0("The name of the target object.")
   public static final String INVALID_TARGET_OBJECT = "3";

   @RBEntry("\"{0}\" non è un nome di modello valido")
   @RBArgComment0("The name of the team template.")
   public static final String INVALID_TEAM_TEMPLATE = "5";
}
