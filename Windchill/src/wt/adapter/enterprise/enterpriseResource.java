/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.adapter.enterprise;

import wt.util.resource.*;

@RBUUID("wt.adapter.enterprise.enterpriseResource")
public final class enterpriseResource extends WTListResourceBundle {
   @RBEntry("Either the \"object_ref\" webject parameter or the \"class\" and \"where\" parameters must be provided.")
   public static final String NO_TARGET_OBJECT_PARAMETERS = "0";

   @RBEntry("The \"{0}\" and \"{1}\" webject parameters cannot both be provided.")
   @RBArgComment0("The webject parameter")
   @RBArgComment1("The webject parameter")
   public static final String NOT_BOTH_PARAMETERS = "1";

   @RBEntry("Unable to uniquely identify the target object with class \"{0}\" using search criteria \"{1}\".")
   @RBArgComment0("The name of target class")
   @RBArgComment1("The search criteria")
   public static final String TARGET_OBJECT_NOT_UNIQUE = "2";

   @RBEntry("Unable to find the target object with class \"{0}\" using search criteria \"{1}\".")
   @RBArgComment0("The name of target class")
   @RBArgComment1("The search criteria")
   public static final String TARGET_OBJECT_NOT_FOUND = "3";

   @RBEntry("{0} is not checked out.")
   @RBArgComment0("The Object")
   public static final String OBJECT_NOT_CHECKED_OUT = "4";

   @RBEntry("{0} is not a content holder.")
   @RBArgComment0("The Object name.")
   public static final String OBJECT_NOT_CONTENT_HOLDER = "5";

   @RBEntry("The file content for file {0} was not provided.")
   @RBArgComment0("The file name.")
   public static final String NO_FILE_DATA = "6";

   @RBEntry("Content has been successfully added to {0}.")
   @RBArgComment0("The content holder.")
   public static final String ADD_CONTENT_SUCCESS = "7";

   @RBEntry("Checkout is not allowed for {0}.")
   @RBArgComment0("The class name of the object.")
   public static final String CHECKOUT_NOT_ALLOWED = "8";

   @RBEntry("{0} has been successfully checked in.")
   @RBArgComment0("Name of the object.")
   public static final String CHECKIN_SUCCESS = "9";
}
