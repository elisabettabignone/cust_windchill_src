/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.adapter.enterprise;

import wt.util.resource.*;

@RBUUID("wt.adapter.enterprise.enterpriseResource")
public final class enterpriseResource_it extends WTListResourceBundle {
   @RBEntry("È necessario fornire il parametro webject \"object_ref\" o i parametri \"class\" e \"where\".")
   public static final String NO_TARGET_OBJECT_PARAMETERS = "0";

   @RBEntry("Non è possibile fornire entrambi i parametri webject \"{0}\" e \"{1}\".")
   @RBArgComment0("The webject parameter")
   @RBArgComment1("The webject parameter")
   public static final String NOT_BOTH_PARAMETERS = "1";

   @RBEntry("Impossibile identificare in modo univoco l'oggetto di destinazione di classe \"{0}\" usando i criteri di ricerca \"{1}\".")
   @RBArgComment0("The name of target class")
   @RBArgComment1("The search criteria")
   public static final String TARGET_OBJECT_NOT_UNIQUE = "2";

   @RBEntry("Impossibile trovare l'oggetto di destinazione di classe \"{0}\" usando i criteri di ricerca \"{1}\".")
   @RBArgComment0("The name of target class")
   @RBArgComment1("The search criteria")
   public static final String TARGET_OBJECT_NOT_FOUND = "3";

   @RBEntry("{0} non è sottoposto a Check-Out.")
   @RBArgComment0("The Object")
   public static final String OBJECT_NOT_CHECKED_OUT = "4";

   @RBEntry("{0} non è un contenitore.")
   @RBArgComment0("The Object name.")
   public static final String OBJECT_NOT_CONTENT_HOLDER = "5";

   @RBEntry("Non è stato fornito il contenuto per il file {0}.")
   @RBArgComment0("The file name.")
   public static final String NO_FILE_DATA = "6";

   @RBEntry("È stato aggiunto contenuto a {0}.")
   @RBArgComment0("The content holder.")
   public static final String ADD_CONTENT_SUCCESS = "7";

   @RBEntry("Check-Out non consentito per {0}.")
   @RBArgComment0("The class name of the object.")
   public static final String CHECKOUT_NOT_ALLOWED = "8";

   @RBEntry("{0} è stato sottoposto a Check-In.")
   @RBArgComment0("Name of the object.")
   public static final String CHECKIN_SUCCESS = "9";
}
