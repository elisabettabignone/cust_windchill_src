/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.adapter;

import wt.util.resource.*;

@RBUUID("wt.adapter.adapterResource")
public final class adapterResource_it extends WTListResourceBundle {
   @RBEntry("Non è stato fornito il parametro webject  \"WHERE\" richiesto.")
   @RBComment("Missing webject parameter")
   public static final String MISSING_WHERE_CLAUSE = "0";

   @RBEntry("Il processore Webject \"{0}\" non ha restituito output.")
   @RBComment("Webject processor did not return output")
   @RBArgComment0("Webject processor")
   public static final String NO_OUTPUT_GROUP = "1";

   @RBEntry("Non è stato fornito il parametro webject \"CLASS\" richiesto.")
   @RBComment("Missing webject parameter")
   public static final String MISSING_CLASS = "2";

   @RBEntry("Non è stato fornito il parametro webject \"DIRECTION\" richiesto.")
   @RBComment("Missing webject parameter")
   public static final String MISSING_DIRECTION = "3";

   @RBEntry("Non è stato fornito il parametro webject \"RELATION\" richiesto.")
   @RBComment("Missing webject parameter")
   public static final String MISSING_RELATION = "4";

   @RBEntry("Errore durante la ricerca dell'oggetto padre.")
   @RBComment("Error message")
   public static final String PARENT_OBJECT_ERROR = "5";

   @RBEntry("Sono stati ritrovati più oggetti padre.")
   @RBComment("Sucessful message")
   public static final String MULTIPLE_PARENT_OBJECTS = "6";

   @RBEntry("Non è stato fornito il parametro webject \"{0}\" richiesto.")
   @RBComment("Missing required parameter")
   @RBArgComment0("The name of required webject parameter")
   public static final String MISSING_REQUIRED_PARAMETER = "8";

   @RBEntry("Impossibile trovare l'oggetto di destinazione \"{0}\".")
   @RBComment("Failed message")
   @RBArgComment0("target object")
   public static final String UNABLE_TO_FIND_TARGET_OBJECT = "9";

   @RBEntry("Non è stato fornito il parametro webject \"ATTRIBUTE\" richiesto.")
   @RBComment("Missing webject parameter")
   public static final String MISSING_ATTRIBUTE = "10";

   @RBEntry("Non è stato fornito il parametro webject \"FIELD\" richiesto.")
   @RBComment("Missing webject parameter")
   public static final String MISSING_FIELD = "11";

   @RBEntry("Tentativo di connessione da \"{0}\", host non trovato nell'elenco di fiducia: {1}")
   @RBComment("Failed message")
   @RBArgComment0("The name of connecting host")
   @RBArgComment1("The list name of trusted hosts")
   public static final String UNTRUSTED_HOST = "12";

   @RBEntry("Nessun  webject di nome \"{0}\" si applica alla classe \"{1}\".")
   @RBComment("No webject delegate for the combination of webject name and class parameter.")
   public static final String NO_WEBJECT_DELEGATE = "13";

   @RBEntry("La sessione con id \"{0}\" non è controllata dall'utente \"{1}\".")
   @RBComment("No sesssion with the specified id is owned by another user.")
   public static final String NOT_SESSION_OWNER = "14";

   @RBEntry("Non esiste alcuna sessione con id \"{0}\".")
   @RBComment("No sesssion with the specified id exists.")
   public static final String NO_SUCH_SESSION = "15";

   @RBEntry("La sessione con id \"{0}\" è occupata.")
   @RBComment("The specified session is in use.")
   public static final String SESSION_BUSY = "16";

   @RBEntry("L'accesso anonimo alla sessione con id \"{0}\" non è consentito.")
   @RBComment("Access to sessions requires authentication.")
   public static final String ANONYMOUS_SESSION_ACCESS = "17";

   @RBEntry("L'utente/gruppo non esiste: \"{0}\"")
   @RBComment("The specified group does not exist.")
   public static final String NO_SUCH_GROUP = "18";

   @RBEntry("Accesso a Windchill negato.")
   @RBComment("The calling user does not have access to Windchill.")
   public static final String WINDCHILL_ACCESS_DENIED = "19";

   @RBEntry("Non è stato trovato alcun oggetto corrispondente ai criteri di selezione.")
   public static final String NO_MATCHING_OBJECTS = "20";

   @RBEntry("Il parametro \"{0}\" non accetta il valore \"{1}\".")
   public static final String UNACCEPTABLE_PARAMETER_VALUE = "21";

   @RBEntry("L'oggetto \"{0}\" è inesistente.")
   @RBComment("No object with the specified UFID exits.")
   public static final String NO_SUCH_OBJECT = "22";

   @RBEntry("{0} non ha iterazioni.")
   @RBArgComment0("The specified object doesn't implement the Iterated interface.")
   public static final String OBJECT_NOT_ITERATED = "23";

   @RBEntry("{0} non è gestito da cicli di vita.")
   @RBArgComment0("The specified object doesn't implement LifeCycleManaged.")
   public static final String OBJECT_NOT_LIFECYCLEMANAGED = "24";

   @RBEntry("Il contenitore {0} non supporta utenti/gruppi/ruoli di tipo {1} nel servizio {2}.")
   public static final String UNSUPPORTED_CLASS_AND_SERVICE = "25";

   @RBEntry("L'attributo \"{0}\" non accetta il valore \"{1}\".")
   public static final String UNACCEPTABLE_ATTRIBUTE_VALUE = "26";

   @RBEntry("{0} non è notificabile.")
   @RBArgComment0("The Object name.")
   public static final String OBJECT_NOT_NOTIFIABLE = "27";

   @RBEntry("Impossibile aggiornare attributi con più valori")
   public static final String UPDATE_MULTIPLE_VALUES_MESSAGE = "28";

   @RBEntry("Impossibile aggiornare attributi di questo tipo - ")
   public static final String UNSUPPORTED_UPDATE_TYPE = "29";

   @RBEntry("L'oggetto non è di tipo IBAHolder. Attributo non valido - ")
   public static final String OBJECT_NOT_IBAHOLDER = "30";

   @RBEntry("Nome attributo non valido - ")
   public static final String INVALID_ATTRIBUTE_NAME_MESSAGE = "31";

   @RBEntry("L'oggetto è già stato sottoposto a Check-Out da un altro utente")
   public static final String OBJECT_ALREADY_CHECKED_OUT = "32";
}
