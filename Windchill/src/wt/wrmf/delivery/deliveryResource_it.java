/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.wrmf.delivery;

import wt.util.resource.*;

@RBUUID("wt.wrmf.delivery.deliveryResource")
public final class deliveryResource_it extends WTListResourceBundle {
   @RBEntry("DeliveryType non usato correttamente o corrotto durante il trasporto")
   public static final String SEND_ITEM_METHOD_MISUSED = "0";

   @RBEntry("L'oggetto  ricevuto nel sito remoto è del tipo non valido \"{0}\". Il tipo corretto è \"{1}\".")
   public static final String WRONG_RECEIVED_OBJECT_TYPE = "1";

   @RBEntry("Impossibile trovare l'oggetto ApplicationData per l'oggetto Streamed replicato.")
   public static final String NOAPPDATAFOUND = "10";

   @RBEntry("Impossibile trovare la Posta in uscita. Impossibile inviare il messaggio.")
   public static final String OUTBOX_NOT_FOUND = "11";

   @RBEntry("Alcune informazioni sono andate perse durante il trasporto")
   public static final String INFO_LOST_IN_TRANSPORT = "12";

   @RBEntry("L'oggetto \"{0}\" eseguito sul sito remoto non è del tipo previsto.")
   public static final String WRONG_EXECUTED_OBJECT_TYPE = "13";

   @RBEntry("Impossibile trovare la posta in arrivo. Impossibile elaborare il feedback del sito remoto.")
   public static final String INBOX_NOT_FOUND = "14";

   @RBEntry("Consegna impossibile al sito di destinazione: \"{0}\".")
   public static final String ALERT_RECEIVED = "15";

   @RBEntry("Il metodo invocato \"{1}\" nella classe \"{0}\" non esiste o non è usato correttamente.")
   public static final String METHOD_NOT_EXIST = "16";

   @RBEntry("Impossibile trovare la classe \"{0}\" per il metodo invocato \"{1}\".")
   public static final String CLASS_NOT_FOUND = "17";

   @RBEntry("Impossibile invocare il metodo \"{1}\" dichiarato nella classe \"{0}\".")
   public static final String METHOD_NOT_INVOKED = "18";

   @RBEntry("Gli argomenti \"{1}\" del metodo \"{0}\" non sono stati usati correttamente.")
   public static final String ARGUMENT_MISUSED = "19";

   @RBEntry("TrackingID non ha una corrispondenza univoca nel database. \"{0}\" corrispondenze.")
   public static final String TRACKINGID_NOT_UNIQUE = "2";

   @RBEntry("InputStream nullo ricevuto per l'ActionPullItem con URL completo: {0}.")
   public static final String PULL_ITEM_FAILED = "20";

   @RBEntry("Il nome della posta in uscita \"{0}\" è già usato.")
   public static final String OUTBOX_NAME_TAKEN = "21";

   @RBEntry("Il nome della posta in arrivo \"{0}\" è già usato.")
   public static final String INBOX_NAME_TAKEN = "22";

   @RBEntry("Il feedback \"{0}\" ricevuto non è di tipo ShippingItem.")
   public static final String WRONG_RECEIVED_FEEDBACK = "3";

   @RBEntry("Impossibile trovare la posta in arrivo per elaborare il feedback \"{0}\".")
   public static final String INBOXID_NOT_FOUND = "4";

   @RBEntry("Il feedback \"{0}\" indica che il TrackingID è andato perso.")
   public static final String TRACKINGID_LOST_IN_TRANSPORT = "5";

   @RBEntry("Il feedback \"{0}\" indica che il MethodInvocationDesc è andato perso.")
   public static final String METHODINVOCATIONDESC_NOT_FOUND = "6";

   @RBEntry("Il feedback \"{0}\" indica che è impossibile trovare ShippingLabel.")
   public static final String SHIPPINGLABEL_NOT_FOUND = "7";

   @RBEntry("Il feedback \"{0}\" indica che è impossibile trovare PayloadInputStreamDesc.")
   public static final String PAYLOAD_NOT_FOUND = "8";

   @RBEntry("Il feedback \"{0}\" indica che sendImmediateItem() è fallito.")
   public static final String SEND_IMMEDIATE_FAILED = "9";
}
