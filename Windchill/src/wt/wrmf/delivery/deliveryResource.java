/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.wrmf.delivery;

import wt.util.resource.*;

@RBUUID("wt.wrmf.delivery.deliveryResource")
public final class deliveryResource extends WTListResourceBundle {
   @RBEntry("DeliveryType misused or corrupted during transportation")
   public static final String SEND_ITEM_METHOD_MISUSED = "0";

   @RBEntry("The object received at remote side is of unexpected type \"{0}\". Type of \"{1}\" is expected.")
   public static final String WRONG_RECEIVED_OBJECT_TYPE = "1";

   @RBEntry("ApplicationData object was not found for the Streamed object being replicated.")
   public static final String NOAPPDATAFOUND = "10";

   @RBEntry("Outbox not found; can not send message out.")
   public static final String OUTBOX_NOT_FOUND = "11";

   @RBEntry("Some information has been lost during transportation")
   public static final String INFO_LOST_IN_TRANSPORT = "12";

   @RBEntry("The object \"{0}\" executed at remote side is of unexpected type .")
   public static final String WRONG_EXECUTED_OBJECT_TYPE = "13";

   @RBEntry("Inbox not found; can not process feedback from remote side.")
   public static final String INBOX_NOT_FOUND = "14";

   @RBEntry("Delivery service indicated failure on the target site: \"{0}\" ")
   public static final String ALERT_RECEIVED = "15";

   @RBEntry("The invoked method \"{1}\" in class \"{0}\" does not exist or misused.")
   public static final String METHOD_NOT_EXIST = "16";

   @RBEntry("The class \"{0}\" is not found for the invoked method \"{1}\".")
   public static final String CLASS_NOT_FOUND = "17";

   @RBEntry("Can not invoke the method \"{1}\" declared in class \"{0}\".")
   public static final String METHOD_NOT_INVOKED = "18";

   @RBEntry("The method \"{0}\" has misused argument(s) \"{1}\".")
   public static final String ARGUMENT_MISUSED = "19";

   @RBEntry("TrackingID is not uniquely matched in the database. We have \"{0}\" matches.")
   public static final String TRACKINGID_NOT_UNIQUE = "2";

   @RBEntry("Null InputStream received for the ActionPullItem with full URL: {0}.")
   public static final String PULL_ITEM_FAILED = "20";

   @RBEntry("The outbox name \"{0}\" has been taken.")
   public static final String OUTBOX_NAME_TAKEN = "21";

   @RBEntry("The inbox name \"{0}\" has been taken.")
   public static final String INBOX_NAME_TAKEN = "22";

   @RBEntry("The received feedback \"{0}\" is not type of ShippingItem.")
   public static final String WRONG_RECEIVED_FEEDBACK = "3";

   @RBEntry("Can not find Inbox to process feedback \"{0}\".")
   public static final String INBOXID_NOT_FOUND = "4";

   @RBEntry("The feedback \"{0}\" indicates the TrackingID has been lost.")
   public static final String TRACKINGID_LOST_IN_TRANSPORT = "5";

   @RBEntry("The feedback \"{0}\" indicates the MethodInvocationDesc has been lost.")
   public static final String METHODINVOCATIONDESC_NOT_FOUND = "6";

   @RBEntry("The feedback \"{0}\" indicates the ShippingLabel is not found.")
   public static final String SHIPPINGLABEL_NOT_FOUND = "7";

   @RBEntry("The feedback \"{0}\" indicates the PayloadInputStreamDesc is not found.")
   public static final String PAYLOAD_NOT_FOUND = "8";

   @RBEntry("The feedback \"{0}\" indicates that sendImmediateItem() failed.")
   public static final String SEND_IMMEDIATE_FAILED = "9";
}
