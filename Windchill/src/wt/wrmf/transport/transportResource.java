/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.wrmf.transport;

import wt.util.resource.*;

@RBUUID("wt.wrmf.transport.transportResource")
public final class transportResource extends WTListResourceBundle {
   @RBEntry("HTTP request failed for URL: \"{0}\", response code: \"{1}\".")
   public static final String HTTP_REQUEST_FAILED = "0";

   @RBEntry("The requested transport type with the key \"{0}\" misused or not specified in wt.properties.")
   public static final String TRANSPORT_TYPE_MISUSED = "1";

   @RBEntry("The received InputStream from \"{0}\" is corrupted because the trailer can not be verified.")
   public static final String TRAILER_NOT_OK = "2";

   @RBEntry("ActionPullItem misused: remote MethodInvocationDesc not specified, or pull method not specified.")
   public static final String PULLITEM_MISUSED = "3";
}
