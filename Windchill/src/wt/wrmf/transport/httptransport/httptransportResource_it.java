/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.wrmf.transport.httptransport;

import wt.util.resource.*;

@RBUUID("wt.wrmf.transport.httptransport.httptransportResource")
public final class httptransportResource_it extends WTListResourceBundle {
   @RBEntry("La richiesta HTTP di {0} è fallita. Il codice di risposta HTTP è {1}.")
   public static final String HTTP_REQUEST_FAILED = "0";

   @RBEntry("L'oggetto sito non esiste o non è univoco per l'URL \"{1}\" -- {0} corrispondenze trovate")
   public static final String SITE_NOT_UNIQUE = "1";

   @RBEntry("Autenticazione di \"{0}\" è fallita.")
   public static final String AUTHENTICATION_FAILED = "2";

   @RBEntry("La password è nulla; impossibile trovare una password per \"{0}\" sia nel file che nel database.")
   public static final String PASSWORD_IS_NULL = "3";

   @RBEntry("Impossibile trovare l'url del sito nell'URL in arrivo dall'host <{0}>.")
   public static final String NO_SITE_IN_URL = "4";

   @RBEntry("WTPrincipal non impostato per il sito: <{0}>.")
   public static final String NO_PRINCIPAL = "5";
}
