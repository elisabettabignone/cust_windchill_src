/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.wrmf.transport.httptransport;

import wt.util.resource.*;

@RBUUID("wt.wrmf.transport.httptransport.httptransportResource")
public final class httptransportResource extends WTListResourceBundle {
   @RBEntry("HTTP Request to {0} failed. HTTP response code is {1}.")
   public static final String HTTP_REQUEST_FAILED = "0";

   @RBEntry("The Site object does not exist or not unique for the URL \"{1}\" -- Found {0} matches.")
   public static final String SITE_NOT_UNIQUE = "1";

   @RBEntry("Authentication to \"{0}\" Failed.")
   public static final String AUTHENTICATION_FAILED = "2";

   @RBEntry("Password is null; can not find password for \"{0}\" either from file or database.")
   public static final String PASSWORD_IS_NULL = "3";

   @RBEntry("Site url not found in the incoming URL from host <{0}>.")
   public static final String NO_SITE_IN_URL = "4";

   @RBEntry("WTPrincipal not set for Site: <{0}>.")
   public static final String NO_PRINCIPAL = "5";
}
