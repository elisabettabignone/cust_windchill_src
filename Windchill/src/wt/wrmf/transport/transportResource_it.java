/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.wrmf.transport;

import wt.util.resource.*;

@RBUUID("wt.wrmf.transport.transportResource")
public final class transportResource_it extends WTListResourceBundle {
   @RBEntry("La richiesta HTTP dell'URL \"{0}\" è fallita, codice di risposta: \"{1}\".")
   public static final String HTTP_REQUEST_FAILED = "0";

   @RBEntry("Il tipo di trasporto richiesto con la chiave \"{0}\" non è stato usato correttamente o non è stato specificato in wt.properties.")
   public static final String TRANSPORT_TYPE_MISUSED = "1";

   @RBEntry("L'InputStream ricevuto da \"{0}\" non è valido perché il trailer non può essere verificato.")
   public static final String TRAILER_NOT_OK = "2";

   @RBEntry("ActionPullItem non è stato usato correttamente: MethodInvocationDesc remoto non specificato o metodo di estrazione non specificato.")
   public static final String PULLITEM_MISUSED = "3";
}
