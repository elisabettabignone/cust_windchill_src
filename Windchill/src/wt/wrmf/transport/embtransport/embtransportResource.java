/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.wrmf.transport.embtransport;

import wt.util.resource.*;

@RBUUID("wt.wrmf.transport.embtransport.embtransportResource")
public final class embtransportResource extends WTListResourceBundle {
   @RBEntry("Stream is corrupted for destination <{0}>.")
   public static final String STREAM_CORRUPTED = "0";

   @RBEntry("The url <{0}> for EMBPipe must be started with \"file://\".")
   public static final String BAD_URL_SYNTAX = "1";
}
