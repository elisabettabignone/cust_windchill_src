/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.scheduler;

import wt.util.resource.*;

@RBUUID("wt.scheduler.schedulerResource")
public final class schedulerResource_it extends WTListResourceBundle {
   @RBEntry("L'operazione \"{0}\" non esiste per la classe \"{1}\"")
   public static final String BAD_OPERATION = "0";

   @RBEntry("Il metodo \"{0}\" non è statico")
   public static final String NON_STATIC_METHOD = "1";

   @RBEntry("Impossibile trovare l'utente")
   public static final String MISSING_PRINCIPAL = "2";

   @RBEntry("Il metodo di destinazione dell'attività programmata è in esecuzione")
   public static final String METHOD_EXECUTING = "3";

   @RBEntry("Impossibile creare cronologia delle esecuzioni")
   public static final String CANNOT_CREATE_HISTORY = "4";

   @RBEntry(" \"{0}\" esecuzioni di \"{1}\" completate")
   public static final String COUNTED_HISTORY_STATUS = "5";

   @RBEntry("Impossibile aggiungere l'attività programmata")
   public static final String CANNOT_ADD_ITEM = "6";

   @RBEntry("Impossibile modificare l'attività programmata")
   public static final String CANNOT_MODIFY_ITEM = "7";

   @RBEntry("Impossibile annullare l'attività programmata")
   public static final String CANNOT_CANCEL_ITEM = "8";

   @RBEntry("Pronto")
   public static final String READY = "9";

   @RBEntry("Completato")
   public static final String COMPLETED = "10";

   @RBEntry("Non riuscito")
   public static final String FAILED = "11";

   @RBEntry("In esecuzione")
   public static final String EXECUTING = "12";

   @RBEntry("Sospeso")
   public static final String SUSPENDED = "13";

   @RBEntry("Riprogramma")
   public static final String RESCHEDULE = "14";

   @RBEntry("Interrotto")
   public static final String ABORTED = "15";

   @RBEntry("Continuato con errore")
   public static final String ERROR_CONTINUED = "16";

   @RBEntry("Completato con errore")
   public static final String ERROR_COMPLETED = "17";

   @RBEntry("Arrestato con errore")
   public static final String ERROR_STOPPED = "18";
}
