/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.scheduler;

import wt.util.resource.*;

@RBUUID("wt.scheduler.schedulerResource")
public final class schedulerResource extends WTListResourceBundle {
   @RBEntry("Operation: \"{0}\" does not exist for class \"{1}\"")
   public static final String BAD_OPERATION = "0";

   @RBEntry("Method \"{0}\" is not static")
   public static final String NON_STATIC_METHOD = "1";

   @RBEntry("Cannot find user")
   public static final String MISSING_PRINCIPAL = "2";

   @RBEntry("Target method of schedule item is currently executing")
   public static final String METHOD_EXECUTING = "3";

   @RBEntry("Could not create execution history")
   public static final String CANNOT_CREATE_HISTORY = "4";

   @RBEntry(" \"{0}\" runs of \"{1}\" completed")
   public static final String COUNTED_HISTORY_STATUS = "5";

   @RBEntry("Cannot add schedule item")
   public static final String CANNOT_ADD_ITEM = "6";

   @RBEntry("Cannot modify schedule item")
   public static final String CANNOT_MODIFY_ITEM = "7";

   @RBEntry("Cannot cancel schedule item")
   public static final String CANNOT_CANCEL_ITEM = "8";

   @RBEntry("Ready")
   public static final String READY = "9";

   @RBEntry("Completed")
   public static final String COMPLETED = "10";

   @RBEntry("Failed")
   public static final String FAILED = "11";

   @RBEntry("Executing")
   public static final String EXECUTING = "12";

   @RBEntry("Suspended")
   public static final String SUSPENDED = "13";

   @RBEntry("Reschedule")
   public static final String RESCHEDULE = "14";

   @RBEntry("Aborted")
   public static final String ABORTED = "15";

   @RBEntry("Continued with error")
   public static final String ERROR_CONTINUED = "16";

   @RBEntry("Completed with error")
   public static final String ERROR_COMPLETED = "17";

   @RBEntry("Stopped with error")
   public static final String ERROR_STOPPED = "18";
}
