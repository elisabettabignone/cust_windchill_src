/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.queue;

import wt.util.resource.*;

@RBUUID("wt.queue.composite.qCompResource")
public final class qcompositeResource extends WTListResourceBundle {

   @RBEntry("Total number of entries executed")
   public static final String TOTAL_ENTRIES_EXECUTED = "0";

   @RBEntry("Total accumulated execution time (in milliseconds)")
   public static final String TOTAL_EXECUTION_TIME = "1";

   @RBEntry("Raw queue entry execution data for one queue")
   public static final String COMPOSITE_TYPE_DESC= "2";

   @RBEntry("Name of the queue")
   public static final String QUEUE_NAME = "5";

   @RBEntry("Raw queue entry execution data for queues")
   public static final String TABULAR_TYPE_DESC= "6";
}
