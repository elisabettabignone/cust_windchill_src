/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.queue;

import wt.util.resource.*;

@RBUUID("wt.queue.queueManagerRB")
public final class queueManagerRB extends WTListResourceBundle {
   /**
    * Entry
    **/
   @RBEntry("Queue Management")
   public static final String PRIVATE_CONSTANT_0 = "LQueueManagerTitle";

   @RBEntry("unable to stringify argument({0})")
   public static final String PRIVATE_CONSTANT_1 = "LQueueUnableToString";

   @RBEntry("Entry Detail")
   public static final String PRIVATE_CONSTANT_2 = "LQueueEntryDetail";

   @RBEntry("Entry detail for {0}")
   public static final String PRIVATE_CONSTANT_3 = "LQueueEntryDetailFor";

   @RBEntry("Cancel")
   public static final String PRIVATE_CONSTANT_4 = "LQueueCancelLabel";

   @RBEntry("OK")
   public static final String PRIVATE_CONSTANT_5 = "LQueueOKLabel";

   @RBEntry("Reset")
   public static final String PRIVATE_CONSTANT_6 = "LQueueResetLabel";

   @RBEntry("Update")
   public static final String PRIVATE_CONSTANT_7 = "LQueueUpdateLabel";

   @RBEntry("Create Queue")
   public static final String PRIVATE_CONSTANT_8 = "LQueueCreateTitle";

   @RBEntry("Queue Name")
   public static final String PRIVATE_CONSTANT_9 = "LQueueName";

   @RBEntry("Type")
   public static final String PRIVATE_CONSTANT_10 = "LQueueType";

   @RBEntry("Group")
   public static final String PRIVATE_CONSTANT_11 = "LQueueHost";

   @RBEntry("Process")
   public static final String PRIVATE_CONSTANT_12 = "LQueueTypeProcess";

   @RBEntry("Schedule")
   public static final String PRIVATE_CONSTANT_13 = "LQueueTypeSchedule";

   @RBEntry("Name")
   public static final String PRIVATE_CONSTANT_14 = "LQueueLabelName";

   @RBEntry("Enabled")
   public static final String PRIVATE_CONSTANT_15 = "LQueueLabelEnabled";

   @RBEntry("State")
   public static final String PRIVATE_CONSTANT_16 = "LQueueLabelState";

   @RBEntry("Entries")
   public static final String PRIVATE_CONSTANT_17 = "LQueueLabelEntries";

   @RBEntry("Yes")
   public static final String PRIVATE_CONSTANT_18 = "LQueueLabelYes";

   @RBEntry("No")
   public static final String PRIVATE_CONSTANT_19 = "LQueueLabelNo";

   @RBEntry("Stopped")
   public static final String PRIVATE_CONSTANT_20 = "LQueueLabelStopped";

   @RBEntry("Started")
   public static final String PRIVATE_CONSTANT_21 = "LQueueLabelStarted";

   @RBEntry("Suspended")
   public static final String PRIVATE_CONSTANT_22 = "LQueueLabelSuspended";

   @RBEntry("View")
   public static final String PRIVATE_CONSTANT_23 = "LQueueLabelView";

   @RBEntry("Queue Error")
   public static final String PRIVATE_CONSTANT_24 = "LQueueTitleError";

   @RBEntry("An error occurred while processing your request")
   public static final String PRIVATE_CONSTANT_25 = "LQueueMsgError";

   @RBEntry("Polling Interval")
   public static final String PRIVATE_CONSTANT_26 = "LQueuePollInt";

   @RBEntry("seconds")
   public static final String PRIVATE_CONSTANT_27 = "LQueueLabelSecs";

   @RBEntry("Failure Retries")
   public static final String PRIVATE_CONSTANT_28 = "LQueueFailRet";

   @RBEntry("Notify WTUser")
   public static final String PRIVATE_CONSTANT_29 = "LQueueNotifyUser";

   @RBEntry("in case of problems")
   public static final String PRIVATE_CONSTANT_30 = "LQueueMsgProblem";

   @RBEntry("Last Notified")
   public static final String PRIVATE_CONSTANT_31 = "LQueueLastNotified";

   @RBEntry("Suspend Duration")
   public static final String PRIVATE_CONSTANT_32 = "LQueueSuspendDuration";

   @RBEntry("Start")
   public static final String PRIVATE_CONSTANT_33 = "LQueueLabelStart";

   @RBEntry("Create")
   public static final String PRIVATE_CONSTANT_34 = "LQueueLabelCreate";

   @RBEntry("Start All")
   public static final String PRIVATE_CONSTANT_35 = "LQueueLabelStartAll";

   @RBEntry("Stop All")
   public static final String PRIVATE_CONSTANT_36 = "LQueueLabelStopAll";

   @RBEntry("True")
   public static final String PRIVATE_CONSTANT_37 = "LQueueMsgTrue";

   @RBEntry("Status")
   public static final String PRIVATE_CONSTANT_38 = "LQueueMsgStatus";

   @RBEntry("Delete")
   public static final String PRIVATE_CONSTANT_39 = "LQueueLabelDelete";

   @RBEntry("View Entries")
   public static final String PRIVATE_CONSTANT_40 = "LQueueLabelVwEnt";

   @RBEntry("Home")
   public static final String PRIVATE_CONSTANT_41 = "LQueueLabelHome";

   @RBEntry("Help")
   public static final String PRIVATE_CONSTANT_42 = "LQueueLabelHelp";

   @RBEntry("Queue Properties")
   public static final String PRIVATE_CONSTANT_43 = "LQueueLabelProps";

   @RBEntry("Properties of {0}")
   public static final String PRIVATE_CONSTANT_44 = "LQueuePropsOf";

   @RBEntry("\"Are you sure you want\\n\" +\n \"to delete this queue ?\"")
   public static final String PRIVATE_CONSTANT_45 = "LQueueConfirmDel";

   @RBEntry("Queue Entries")
   public static final String PRIVATE_CONSTANT_46 = "LQueueEntries";

   @RBEntry("Entries in {0}")
   public static final String PRIVATE_CONSTANT_47 = "LQueueEntriesIn";

   @RBEntry("View/Update/Delete queue entries")
   public static final String PRIVATE_CONSTANT_48 = "LQueueChgEntries";

   @RBEntry("View entries with status")
   public static final String PRIVATE_CONSTANT_49 = "LQueueVwEntWStat";

   @RBEntry("All")
   public static final String PRIVATE_CONSTANT_50 = "LQueueLabelAll";

   @RBEntry("Failed")
   public static final String PRIVATE_CONSTANT_51 = "LQueueLabelFailed";

   @RBEntry("Ready")
   public static final String PRIVATE_CONSTANT_52 = "LQueueLabelReady";

   @RBEntry("Suspended")
   public static final String PRIVATE_CONSTANT_53 = "LQueueLabelSusp";

   @RBEntry("Executing")
   public static final String PRIVATE_CONSTANT_54 = "LQueueLabelExec";

   @RBEntry("Reschedule")
   public static final String PRIVATE_CONSTANT_55 = "LQueueLabelResch";

   @RBEntry("Completed")
   public static final String PRIVATE_CONSTANT_56 = "LQueueLabelComp";

   @RBEntry("Show")
   public static final String PRIVATE_CONSTANT_57 = "LQueueLabelShow";

   @RBEntry("Change all entries from")
   public static final String PRIVATE_CONSTANT_58 = "LQueueChgAllEnt";

   @RBEntry("to")
   public static final String PRIVATE_CONSTANT_59 = "LQueueLabel_to";

   @RBEntry("Delete entries with status")
   public static final String PRIVATE_CONSTANT_60 = "LQueueDelEntWStat";

   @RBEntry("Selected queue:")
   public static final String PRIVATE_CONSTANT_61 = "LQueueLabelSelQ";

   @RBEntry("Owner")
   public static final String PRIVATE_CONSTANT_62 = "LQueueLabelOwner";

   @RBEntry("Message")
   public static final String PRIVATE_CONSTANT_63 = "LQueueLabelMess";

   @RBEntry("Arguments")
   public static final String PRIVATE_CONSTANT_64 = "LQueueLabelArgs";

   @RBEntry("Entry #")
   public static final String PRIVATE_CONSTANT_65 = "LQueueLabelEntryNo";

   @RBEntry("Schedule Time")
   public static final String PRIVATE_CONSTANT_66 = "LQueueLabelSchTime";

   @RBEntry("Target Class")
   public static final String PRIVATE_CONSTANT_67 = "LQueueLabelTClass";

   @RBEntry("Target Method")
   public static final String PRIVATE_CONSTANT_68 = "LQueueLabelTMethod";

   @RBEntry("Actions")
   public static final String PRIVATE_CONSTANT_69 = "LQueueLabelActions";

   @RBEntry("Queue has no entries at this time")
   public static final String PRIVATE_CONSTANT_70 = "LQueueLabelNoQs";

   @RBEntry("Queue has no {0} entries at this time")
   public static final String PRIVATE_CONSTANT_71 = "LQueueLabelNoSelQs";

   @RBEntry("localhost")
   public static final String PRIVATE_CONSTANT_72 = "LQueueLocalHost";

   @RBEntry("Suspend Until")
   public static final String PRIVATE_CONSTANT_73 = "LQueueSuspendUntil";

   @RBEntry("False")
   public static final String PRIVATE_CONSTANT_74 = "LQueueMsgFalse";

   @RBEntry("Stop")
   public static final String PRIVATE_CONSTANT_75 = "LQueueLabelStop";

   @RBEntry("The reason might be one or more arguments are not found")
   public static final String PRIVATE_CONSTANT_76 = "LQueueArgsException";

   @RBEntry("Severe")
   public static final String PRIVATE_CONSTANT_77 = "LQueueLabelSevere";

   @RBEntry("--Select Action--")
   public static final String PRIVATE_CONSTANT_78 = "LQueueLabelSelectAction";

   @RBEntry("Enable")
   public static final String PRIVATE_CONSTANT_79 = "LQueueLabelEnable";

   @RBEntry("Disable")
   public static final String PRIVATE_CONSTANT_80 = "LQueueLabelDisable";

   @RBEntry("Windchill Queue Management")
   public static final String PRIVATE_CONSTANT_81 = "LQueueLabelQMgt";

   @RBEntry("View Queue Entries")
   public static final String PRIVATE_CONSTANT_82 = "LQueueLabelViewQEntries";

   @RBEntry("Delete Confirmation")
   public static final String PRIVATE_CONSTANT_83 = "LQueueTitleDelete";

   @RBEntry("Delete Queue?")
   public static final String PRIVATE_CONSTANT_84 = "LQueueHeadingDelete";

   @RBEntry("Confirmation")
   public static final String PRIVATE_CONSTANT_85 = "LQueueConfirmationDelete";

   @RBEntry("Total entries")
   public static final String PRIVATE_CONSTANT_86 = "LQueueTotalEntries";

   @RBEntry("Waiting entries")
   public static final String PRIVATE_CONSTANT_87 = "LQueueWaitingEntries";

   @RBEntry("not set")
   @RBComment("Displayed if queue's last notified date is not set.")
   public static final String PRIVATE_CONSTANT_88 = "LQueueLastNotifiedNotSet";

   @RBEntry("N/A")
   @RBComment("Displayed if queue's suspend until date is not set.")
   public static final String PRIVATE_CONSTANT_89 = "LQueueSuspendUntilNotSet";
}
