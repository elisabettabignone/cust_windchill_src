/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.queue;

import wt.util.resource.*;

@RBUUID("wt.queue.queueResource")
public final class queueResource extends WTListResourceBundle {
   @RBEntry("The operation:   \"{0}\" failed")
   public static final String OPERATION_FAILED = "0";

   @RBEntry("Operation: \"{0}\" does not exist for class \"{1}\"")
   public static final String BAD_OPERATION = "1";

   @RBEntry("Cannot find user")
   public static final String MISSING_PRINCIPAL = "2";

   @RBEntry("Status set by operator")
   public static final String SET_BY_OPERATOR = "3";

   @RBEntry("Invalid status")
   public static final String INVALID_STATUS = "4";

   @RBEntry("Couldn't set queue entry status to EXECUTING: {0}")
   public static final String NO_EXECUTING_STATUS = "5";

   @RBEntry("Couldn't set queue entry status to COMPLETED: {0}")
   public static final String NO_COMPLETED_STATUS = "6";

   @RBEntry("Couldn't set queue entry status to FAILED: {0}")
   public static final String NO_FAILED_STATUS = "7";

   @RBEntry("Method \"{0}\" is not static")
   public static final String NON_STATIC_METHOD = "8";

   @RBEntry("Couldn't remove completed queue entry: {0}")
   public static final String CANT_REMOVE = "9";

   @RBEntry("Queue name was not specified")
   public static final String QUEUE_NAME_EMPTY = "10";

   @RBEntry("Specified queue name \"{0}\" already exists")
   public static final String DUPLICATE_QUEUE_NAME = "11";

   @RBEntry("Entry failed due to database rollback.")
   public static final String ROLLBACK_FAILURE = "12";

   @RBEntry("Unknown queue type.")
   public static final String UNKNOWN_TYPE = "13";

   @RBEntry("Max process queues exceeded")
   public static final String MAX_PROCESS_QUEUES = "14";

   @RBEntry("Max schedule queues exceeded")
   public static final String MAX_SCHEDULE_QUEUES = "15";
}
