/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.queue;

import wt.util.resource.*;

@RBUUID("wt.queue.composite.qCompResource")
public final class qcompositeResource_it extends WTListResourceBundle {

   @RBEntry("Numero totale di elementi eseguiti")
   public static final String TOTAL_ENTRIES_EXECUTED = "0";

   @RBEntry("Tempo di esecuzione totale accumulato (in millisecondi)")
   public static final String TOTAL_EXECUTION_TIME = "1";

   @RBEntry("Dati non elaborati di esecuzione elementi coda per una coda")
   public static final String COMPOSITE_TYPE_DESC= "2";

   @RBEntry("Nome della coda")
   public static final String QUEUE_NAME = "5";

   @RBEntry("Dati non elaborati di esecuzione elementi coda per più code")
   public static final String TABULAR_TYPE_DESC= "6";
}
