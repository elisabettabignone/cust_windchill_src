/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.queue;

import wt.util.resource.*;

@RBUUID("wt.queue.queueManagerRB")
public final class queueManagerRB_it extends WTListResourceBundle {
   /**
    * Entry
    **/
   @RBEntry("Gestione code")
   public static final String PRIVATE_CONSTANT_0 = "LQueueManagerTitle";

   @RBEntry("stringify argument - impossibile - ({0})")
   public static final String PRIVATE_CONSTANT_1 = "LQueueUnableToString";

   @RBEntry("Dettaglio elemento")
   public static final String PRIVATE_CONSTANT_2 = "LQueueEntryDetail";

   @RBEntry("Specifica dettagli per {0}")
   public static final String PRIVATE_CONSTANT_3 = "LQueueEntryDetailFor";

   @RBEntry("Annulla")
   public static final String PRIVATE_CONSTANT_4 = "LQueueCancelLabel";

   @RBEntry("OK")
   public static final String PRIVATE_CONSTANT_5 = "LQueueOKLabel";

   @RBEntry("Reimposta")
   public static final String PRIVATE_CONSTANT_6 = "LQueueResetLabel";

   @RBEntry("Aggiorna")
   public static final String PRIVATE_CONSTANT_7 = "LQueueUpdateLabel";

   @RBEntry("Crea coda")
   public static final String PRIVATE_CONSTANT_8 = "LQueueCreateTitle";

   @RBEntry("Nome coda")
   public static final String PRIVATE_CONSTANT_9 = "LQueueName";

   @RBEntry("Tipo")
   public static final String PRIVATE_CONSTANT_10 = "LQueueType";

   @RBEntry("Gruppo")
   public static final String PRIVATE_CONSTANT_11 = "LQueueHost";

   @RBEntry("Processo")
   public static final String PRIVATE_CONSTANT_12 = "LQueueTypeProcess";

   @RBEntry("Programma")
   public static final String PRIVATE_CONSTANT_13 = "LQueueTypeSchedule";

   @RBEntry("Nome")
   public static final String PRIVATE_CONSTANT_14 = "LQueueLabelName";

   @RBEntry("Attivato")
   public static final String PRIVATE_CONSTANT_15 = "LQueueLabelEnabled";

   @RBEntry("Stato del ciclo di vita")
   public static final String PRIVATE_CONSTANT_16 = "LQueueLabelState";

   @RBEntry("Elementi")
   public static final String PRIVATE_CONSTANT_17 = "LQueueLabelEntries";

   @RBEntry("Sì")
   public static final String PRIVATE_CONSTANT_18 = "LQueueLabelYes";

   @RBEntry("No")
   public static final String PRIVATE_CONSTANT_19 = "LQueueLabelNo";

   @RBEntry("Arrestato")
   public static final String PRIVATE_CONSTANT_20 = "LQueueLabelStopped";

   @RBEntry("Avviato")
   public static final String PRIVATE_CONSTANT_21 = "LQueueLabelStarted";

   @RBEntry("Sospeso")
   public static final String PRIVATE_CONSTANT_22 = "LQueueLabelSuspended";

   @RBEntry("Vista")
   public static final String PRIVATE_CONSTANT_23 = "LQueueLabelView";

   @RBEntry("Errore coda")
   public static final String PRIVATE_CONSTANT_24 = "LQueueTitleError";

   @RBEntry("Errore durante l'elaborazione della richiesta")
   public static final String PRIVATE_CONSTANT_25 = "LQueueMsgError";

   @RBEntry("Intervallo di polling")
   public static final String PRIVATE_CONSTANT_26 = "LQueuePollInt";

   @RBEntry("secondi")
   public static final String PRIVATE_CONSTANT_27 = "LQueueLabelSecs";

   @RBEntry("Tentativi in caso di errore")
   public static final String PRIVATE_CONSTANT_28 = "LQueueFailRet";

   @RBEntry("Notifica WTUser")
   public static final String PRIVATE_CONSTANT_29 = "LQueueNotifyUser";

   @RBEntry("in caso di problemi")
   public static final String PRIVATE_CONSTANT_30 = "LQueueMsgProblem";

   @RBEntry("Ultima notifica")
   public static final String PRIVATE_CONSTANT_31 = "LQueueLastNotified";

   @RBEntry("Durata sospensione")
   public static final String PRIVATE_CONSTANT_32 = "LQueueSuspendDuration";

   @RBEntry("Avvia")
   public static final String PRIVATE_CONSTANT_33 = "LQueueLabelStart";

   @RBEntry("Crea")
   public static final String PRIVATE_CONSTANT_34 = "LQueueLabelCreate";

   @RBEntry("Avvia tutto")
   public static final String PRIVATE_CONSTANT_35 = "LQueueLabelStartAll";

   @RBEntry("Arresta tutto")
   public static final String PRIVATE_CONSTANT_36 = "LQueueLabelStopAll";

   @RBEntry("True")
   public static final String PRIVATE_CONSTANT_37 = "LQueueMsgTrue";

   @RBEntry("Stato")
   public static final String PRIVATE_CONSTANT_38 = "LQueueMsgStatus";

   @RBEntry("Elimina")
   public static final String PRIVATE_CONSTANT_39 = "LQueueLabelDelete";

   @RBEntry("Visualizza elementi")
   public static final String PRIVATE_CONSTANT_40 = "LQueueLabelVwEnt";

   @RBEntry("Home")
   public static final String PRIVATE_CONSTANT_41 = "LQueueLabelHome";

   @RBEntry("Guida")
   public static final String PRIVATE_CONSTANT_42 = "LQueueLabelHelp";

   @RBEntry("Proprietà coda")
   public static final String PRIVATE_CONSTANT_43 = "LQueueLabelProps";

   @RBEntry("Proprietà di {0}")
   public static final String PRIVATE_CONSTANT_44 = "LQueuePropsOf";

   @RBEntry("\"Eliminare la coda?\"")
   public static final String PRIVATE_CONSTANT_45 = "LQueueConfirmDel";

   @RBEntry("Elementi coda")
   public static final String PRIVATE_CONSTANT_46 = "LQueueEntries";

   @RBEntry("Elementi in {0}")
   public static final String PRIVATE_CONSTANT_47 = "LQueueEntriesIn";

   @RBEntry("Visualizza/Aggiorna/Elimina elementi coda")
   public static final String PRIVATE_CONSTANT_48 = "LQueueChgEntries";

   @RBEntry("Visualizza stato elementi")
   public static final String PRIVATE_CONSTANT_49 = "LQueueVwEntWStat";

   @RBEntry("Tutti")
   public static final String PRIVATE_CONSTANT_50 = "LQueueLabelAll";

   @RBEntry("Non riuscito")
   public static final String PRIVATE_CONSTANT_51 = "LQueueLabelFailed";

   @RBEntry("Pronto")
   public static final String PRIVATE_CONSTANT_52 = "LQueueLabelReady";

   @RBEntry("Sospeso")
   public static final String PRIVATE_CONSTANT_53 = "LQueueLabelSusp";

   @RBEntry("In esecuzione")
   public static final String PRIVATE_CONSTANT_54 = "LQueueLabelExec";

   @RBEntry("Riprogramma")
   public static final String PRIVATE_CONSTANT_55 = "LQueueLabelResch";

   @RBEntry("Completato")
   public static final String PRIVATE_CONSTANT_56 = "LQueueLabelComp";

   @RBEntry("Mostra")
   public static final String PRIVATE_CONSTANT_57 = "LQueueLabelShow";

   @RBEntry("Modifica tutti gli elementi da")
   public static final String PRIVATE_CONSTANT_58 = "LQueueChgAllEnt";

   @RBEntry("a")
   public static final String PRIVATE_CONSTANT_59 = "LQueueLabel_to";

   @RBEntry("Elimina elementi con stato")
   public static final String PRIVATE_CONSTANT_60 = "LQueueDelEntWStat";

   @RBEntry("Code selezionate:")
   public static final String PRIVATE_CONSTANT_61 = "LQueueLabelSelQ";

   @RBEntry("Proprietario")
   public static final String PRIVATE_CONSTANT_62 = "LQueueLabelOwner";

   @RBEntry("Messaggio")
   public static final String PRIVATE_CONSTANT_63 = "LQueueLabelMess";

   @RBEntry("Argomenti")
   public static final String PRIVATE_CONSTANT_64 = "LQueueLabelArgs";

   @RBEntry("N. elemento")
   public static final String PRIVATE_CONSTANT_65 = "LQueueLabelEntryNo";

   @RBEntry("Ora di programmazione")
   public static final String PRIVATE_CONSTANT_66 = "LQueueLabelSchTime";

   @RBEntry("Classe di destinazione")
   public static final String PRIVATE_CONSTANT_67 = "LQueueLabelTClass";

   @RBEntry("Metodo di destinazione")
   public static final String PRIVATE_CONSTANT_68 = "LQueueLabelTMethod";

   @RBEntry("Azioni")
   public static final String PRIVATE_CONSTANT_69 = "LQueueLabelActions";

   @RBEntry("Nessun elemento in coda")
   public static final String PRIVATE_CONSTANT_70 = "LQueueLabelNoQs";

   @RBEntry("Correntemente nessun elemento {0} in coda")
   public static final String PRIVATE_CONSTANT_71 = "LQueueLabelNoSelQs";

   @RBEntry("host locale")
   public static final String PRIVATE_CONSTANT_72 = "LQueueLocalHost";

   @RBEntry("Sospendi fino")
   public static final String PRIVATE_CONSTANT_73 = "LQueueSuspendUntil";

   @RBEntry("False")
   public static final String PRIVATE_CONSTANT_74 = "LQueueMsgFalse";

   @RBEntry("Arresta")
   public static final String PRIVATE_CONSTANT_75 = "LQueueLabelStop";

   @RBEntry("È possibile che uno o più argomenti non siano stati trovati")
   public static final String PRIVATE_CONSTANT_76 = "LQueueArgsException";

   @RBEntry("Grave")
   public static final String PRIVATE_CONSTANT_77 = "LQueueLabelSevere";

   @RBEntry("--Selezionare un'azione--")
   public static final String PRIVATE_CONSTANT_78 = "LQueueLabelSelectAction";

   @RBEntry("Attiva")
   public static final String PRIVATE_CONSTANT_79 = "LQueueLabelEnable";

   @RBEntry("Disattiva")
   public static final String PRIVATE_CONSTANT_80 = "LQueueLabelDisable";

   @RBEntry("Gestione code Windchill")
   public static final String PRIVATE_CONSTANT_81 = "LQueueLabelQMgt";

   @RBEntry("Visualizza elementi coda")
   public static final String PRIVATE_CONSTANT_82 = "LQueueLabelViewQEntries";

   @RBEntry("Elimina conferma")
   public static final String PRIVATE_CONSTANT_83 = "LQueueTitleDelete";

   @RBEntry("Eliminare coda?")
   public static final String PRIVATE_CONSTANT_84 = "LQueueHeadingDelete";

   @RBEntry("Conferma")
   public static final String PRIVATE_CONSTANT_85 = "LQueueConfirmationDelete";

   @RBEntry("Totale elementi")
   public static final String PRIVATE_CONSTANT_86 = "LQueueTotalEntries";

   @RBEntry("Elementi in attesa")
   public static final String PRIVATE_CONSTANT_87 = "LQueueWaitingEntries";

   @RBEntry("non impostata")
   @RBComment("Displayed if queue's last notified date is not set.")
   public static final String PRIVATE_CONSTANT_88 = "LQueueLastNotifiedNotSet";

   @RBEntry("N.D.")
   @RBComment("Displayed if queue's suspend until date is not set.")
   public static final String PRIVATE_CONSTANT_89 = "LQueueSuspendUntilNotSet";
}
