/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.queue;

import wt.util.resource.*;

@RBUUID("wt.queue.queueResource")
public final class queueResource_it extends WTListResourceBundle {
   @RBEntry("L'operazione   \"{0}\" non è riuscita")
   public static final String OPERATION_FAILED = "0";

   @RBEntry("L'operazione \"{0}\" non esiste per la classe \"{1}\"")
   public static final String BAD_OPERATION = "1";

   @RBEntry("Impossibile trovare l'utente")
   public static final String MISSING_PRINCIPAL = "2";

   @RBEntry("Stato impostato dall'operatore")
   public static final String SET_BY_OPERATOR = "3";

   @RBEntry("Stato non valido")
   public static final String INVALID_STATUS = "4";

   @RBEntry("Impossibile impostare lo stato dell'elemento della coda a IN ESECUZIONE: {0}")
   public static final String NO_EXECUTING_STATUS = "5";

   @RBEntry("Impossibile impostare lo stato dell'elemento della coda a COMPLETATO: {0}")
   public static final String NO_COMPLETED_STATUS = "6";

   @RBEntry("Impossibile impostare lo stato dell'elemento della coda a NON RIUSCITO: {0}")
   public static final String NO_FAILED_STATUS = "7";

   @RBEntry("Il metodo \"{0}\" non è statico")
   public static final String NON_STATIC_METHOD = "8";

   @RBEntry("Impossibile rimuovere l'elemento completato della coda: {0}")
   public static final String CANT_REMOVE = "9";

   @RBEntry("Il nome della coda non è stato specificato")
   public static final String QUEUE_NAME_EMPTY = "10";

   @RBEntry("Il nome di coda specificato \"{0}\" esiste già")
   public static final String DUPLICATE_QUEUE_NAME = "11";

   @RBEntry("Immissione non riuscita a causa del rollback del database.")
   public static final String ROLLBACK_FAILURE = "12";

   @RBEntry("Tipo di coda sconosciuto.")
   public static final String UNKNOWN_TYPE = "13";

   @RBEntry("Numero massimo di code da elaborare superato")
   public static final String MAX_PROCESS_QUEUES = "14";

   @RBEntry("Numero massimo di code da programmare superato")
   public static final String MAX_SCHEDULE_QUEUES = "15";
}
