/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.queue;

import wt.util.resource.*;

@RBUUID("wt.queue.queueJmxResource")
public final class queueJmxResource_it extends WTListResourceBundle {
   @RBEntry("Il numero di elementi pronti e in attesa in coda ha superato la soglia di notifica.")
   public static final String EXCEEDED_WAITING_ENTRIES_THRESHOLD = "0";

   @RBEntry("Il numero totale di elementi in coda ha superato la soglia di notifica.")
   public static final String EXCEEDED_TOTAL_ENTRIES_THRESHOLD = "1";

   @RBEntry("Il tempo di esecuzione per un elemento in coda ha superato la soglia di notifica.")
   public static final String EXCEEDED_EXECUTION_TIME_THRESHOLD = "2";

   @RBEntry("{0} elementi in attesa di essere eseguiti nella coda {1}")
   public static final String EXCESSIVE_WAITING_ENTRIES = "3";

   @RBEntry("{0} elementi in totale nella coda {1}")
   public static final String EXCESSIVE_TOTAL_ENTRIES = "4";

   @RBEntry("Il tempo di esecuzione dell'elemento {0} nella coda {1} ha superato il tempo di esecuzione massimo consentito")
   public static final String EXCESSIVE_EXEC_TIME = "5";

   @RBEntry("Coda")
   public static final String QUEUE = "6";

   @RBEntry("si sono verificati problemi durante l'esecuzione dell'elemento")
   public static final String PROBLEMS = "7";

   @RBEntry("Fornisce notifiche agli amministratori relativamente a problemi di esecuzione degli elementi in coda.")
   public static final String PROBLEM_NOTIFICATION = "8";

   @RBEntry("La coda {0} è stata interrotta")
   public static final String QUEUE_STOPPED = "9";

   @RBEntry("La coda {0} è stata disattivata")
   public static final String QUEUE_DISABLED = "10";
   
   @RBEntry("La coda è stata interrotta")
   public static final String QUEUE_STOPPED_NOTIF_DESCR = "11";

   @RBEntry("La coda è stata disattivata")
   public static final String QUEUE_DISABLED_NOTIF_DESCR = "12";
}
