/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.queue;

import wt.util.resource.*;

@RBUUID("wt.queue.queueJmxResource")
public final class queueJmxResource extends WTListResourceBundle {
   @RBEntry("The number of ready & waiting queue entries has exceeded the notification threshold.")
   public static final String EXCEEDED_WAITING_ENTRIES_THRESHOLD = "0";

   @RBEntry("The total number of queue entries has exceeded the notification threshold.")
   public static final String EXCEEDED_TOTAL_ENTRIES_THRESHOLD = "1";

   @RBEntry("The execution time for a queue entry has exceeded the notification threshold.")
   public static final String EXCEEDED_EXECUTION_TIME_THRESHOLD = "2";

   @RBEntry("There are {0} queue entries waiting to be executed in queue {1}")
   public static final String EXCESSIVE_WAITING_ENTRIES = "3";

   @RBEntry("There are {0} total queue entries in queue {1}")
   public static final String EXCESSIVE_TOTAL_ENTRIES = "4";

   @RBEntry("The execution time of queue entry {0} in {1} has exceeded the max allowable execution time")
   public static final String EXCESSIVE_EXEC_TIME = "5";

   @RBEntry("Queue")
   public static final String QUEUE = "6";

   @RBEntry("experiencing entry execution problems")
   public static final String PROBLEMS = "7";

   @RBEntry("provides admin notifications for queue entry execution problems.")
   public static final String PROBLEM_NOTIFICATION = "8";

   @RBEntry("Queue {0} has been stopped")
   public static final String QUEUE_STOPPED = "9";

   @RBEntry("Queue {0} has been disabled")
   public static final String QUEUE_DISABLED = "10";
   
   @RBEntry("Queue has been stopped")
   public static final String QUEUE_STOPPED_NOTIF_DESCR = "11";

   @RBEntry("Queue has been disabled")
   public static final String QUEUE_DISABLED_NOTIF_DESCR = "12";
}
