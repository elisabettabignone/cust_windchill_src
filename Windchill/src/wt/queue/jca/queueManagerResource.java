/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.queue.jca;

import wt.util.resource.*;

@RBUUID("wt.queue.jca.queueManagerResource")
public final class queueManagerResource extends WTListResourceBundle {
   @RBEntry("Queue Management")
   public static final String QUEUE_MANAGER_LABEL = "0";

   @RBEntry("Name")
   public static final String QUEUE_NAME_LABEL = "1";

   @RBEntry("Type")
   public static final String QUEUE_TYPE_LABEL = "2";

   @RBEntry("Process")
   public static final String QUEUE_PROCESS_QUEUE_LABEL = "3";

   @RBEntry("Process Queue")
   public static final String QUEUE_PROCESS_INFO_LABEL = "QUEUE_PROCESS_INFO_LABEL";

   @RBEntry("Process Queue Entry")
   public static final String QUEUE_PROCESS_ENTRY_INFO_LABEL = "QUEUE_PROCESS_ENTRY_INFO_LABEL";

   @RBEntry("Schedule")
   public static final String QUEUE_SCHEDULE_QUEUE_LABEL = "4";

   @RBEntry("Schedule Queue")
   public static final String QUEUE_SCHEDULE_INFO_LABEL = "QUEUE_SCHEDULE_INFO_LABEL";

   @RBEntry("Schedule Queue Entry")
   public static final String QUEUE_SCEDULE_ENTRY_INFO_LABEL = "QUEUE_SCHEDULE_ENTRY_INFO_LABEL";

   @RBEntry("Status")
   public static final String QUEUE_STATUS_LABEL = "5";

   @RBEntry("Started")
   public static final String QUEUE_STARTED_LABEL = "6";

   @RBEntry("Stopped")
   public static final String QUEUE_STOPPED_LABEL = "7";

   @RBEntry("Enabled")
   public static final String QUEUE_ENABLED_LABEL = "8";

   @RBEntry("Disabled")
   public static final String QUEUE_DISABLED_LABEL = "9";

   @RBEntry("Group")
   public static final String QUEUE_GROUP_LABEL = "10";

   @RBEntry("Default")
   public static final String QUEUE_DEFAULT_GROUP_LABEL = "11";

   @RBEntry("Total Entries")
   public static final String QUEUE_TOTAL_ENTRIES_LABEL = "12";

   @RBEntry("Waiting Entries")
   public static final String QUEUE_WAITING_ENTRIES_LABEL = "13";

   @RBEntry("Severe/Failed Entries")
   public static final String QUEUE_SEVERE_FAILED_ENTRIES_LABEL = "14";

   @RBEntry("Queue Entries")
   public static final String QUEUE_ENTRIES_LABEL = "15";

   @RBEntry("Number")
   public static final String QUEUE_ENTRY_NUMBER_LABEL = "16";

   @RBEntry("Queue")
   public static final String QUEUE_LABEL = "17";

   @RBEntry("Status")
   public static final String QUEUE_ENTRY_STATUS_LABEL = "18";

   @RBEntry("Target Class")
   public static final String ENTRY_TARGET_CLASS_LABEL = "19";

   @RBEntry("Target Method")
   public static final String ENTRY_TARGET_METHOD_LABEL = "20";

   @RBEntry("Arguments")
   public static final String ENTRY_ARGUMENT_LABEL = "21";

   @RBEntry("Schedule Time")
   public static final String ENTRY_SCHEDULE_TIME_LABEL = "22";

   @RBEntry("Attributes")
   public static final String QUEUE_ATTRIBUTES_LABEL = "23";

   @RBEntry("Polling Interval")
   public static final String QUEUE_POLLING_INTERVAL_LABEL = "24";

   @RBEntry("Failure Retries")
   public static final String QUEUE_FAILURE_RETRIES_LABEL = "25";

   @RBEntry("Suspend Duration")
   public static final String QUEUE_SUSPEND_DURATION_LABEL = "26";

   @RBEntry("Suspend Until")
   public static final String QUEUE_SUSPEND_UNTIL_LABEL = "27";

   @RBEntry("N/A")
   public static final String QUEUE_NOT_APPLICABLE_LABEL = "28";

   @RBEntry("Number")
   public static final String QUEUE_ENTRY_ARG_NUMBER_LABEL = "29";

   @RBEntry("Argument")
   public static final String QUEUE_ENTRY_ARG_LABEL = "30";

   @RBEntry("Argument Type")
   public static final String QUEUE_ENTRY_ARG_TYPE_LABEL = "31";

   @RBEntry("Queue Entry")
   public static final String QUEUE_ENTRY_LABEL = "32";

   @RBEntry("Owner")
   public static final String QUEUE_ENTRY_OWNER_LABEL = "33";

   @RBEntry("Message")
   public static final String QUEUE_ENTRY_MESSAGE_LABEL = "34";

   @RBEntry("Arguments")
   public static final String QUEUE_ENTRY_ARG_S_LABEL = "35";

   @RBEntry("Primary Object")
   public static final String QUEUE_ENTRY_PRIMARY_OBJECT_LABEL = "36";

   @RBEntry("Edit")
   public static final String EDIT_QUEUE_ATTRIBUTES = "37";

   @RBEntry("Suspended")
   public static final String QUEUE_SUSPENDED_LABEL = "38";

   @RBEntry("Pool")
   public static final String QUEUE_RANDOMACCESS_QUEUE_LABEL = "39";

   @RBEntry("Pool Queue")
   public static final String QUEUE_RANDOMACCESS_INFO_LABEL = "QUEUE_RANDOMACCESS_INFO_LABEL";

   @RBEntry("Pool Queue Entry")
   public static final String QUEUE_RANDOMACCESS_ENTRY_INFO_LABEL = "QUEUE_RANDOMACCESS_ENTRY_INFO_LABEL";

   @RBEntry("Starting")
   public static final String QUEUE_STARTING_LABEL = "40";

   @RBEntry("Starting")
   public static final String STARTING = "41";

   @RBEntry("Started")
   public static final String STARTED = "42";

   @RBEntry("Stopping")
   public static final String STOPPING = "43";

   @RBEntry("Deleting")
   public static final String DELETING = "44";

   @RBEntry("Suspended")
   public static final String SUSPENDED = "45";

   @RBEntry("Stopped")
   public static final String STOPPED = "46";

   @RBEntry("New Queue")
   public static final String CREATE_QUEUE = "47";

   @RBEntry("Yes")
   public static final String YES_LABEL = "48";

   @RBEntry("No")
   public static final String NO_LABEL = "49";

   @RBEntry("Execution Host")
   public static final String QUEUE_HOST_LABEL = "50";

   @RBEntry("Queue {0} successfully created")
   public static final String CREATE_QUEUE_SUCCESS = "51";

   @RBEntry("Error Starting Queues")
   public static final String QUEUE_START_ERROR_TITLE = "52";

   @RBEntry("A communication or configuration error has occurred that has prevented the selected queues from being started. Check your method server status, system configuration, and database connections.")
   public static final String QUEUE_START_ERROR_EXCEPTION_MESSAGE = "53";

   @RBEntry("The selected queues cannot be started because they are disabled.")
   public static final String QUEUE_START_ERROR_ALL_DISABLED_MESSAGE = "54";

   @RBEntry("Some of the selected queues cannot be started because they are disabled.")
   public static final String QUEUE_START_ERROR_SOME_DISABLED_MESSAGE = "55";

   @RBEntry("Cannot create queue: max process queues exceeded.  To create queue, increase wt.queue.max.processQueues")
   public static final String CREATE_QUEUE_MAX_PROC_ERROR = "56";

   @RBEntry("Cannot create queue: max schedule queues exceeded.  To create queue, increase wt.queue.max.scheduleQueues")
   public static final String CREATE_QUEUE_MAX_SCHED_ERROR = "57";

   @RBEntry("Cannot create queue: name already exists.  Enter a different name to create queue.")
   public static final String CREATE_QUEUE_QUEUE_EXISTS = "58";

   @RBEntry("Error: you must enter an integer in the range of 0 to {0}")
   public static final String ERROR_NON_INTEGER = "59";

   @RBEntry("Error: value out of range, you must enter an integer in the range of 0 to {0}")
   public static final String ERROR_INTEGER_OUT_OF_RANGE = "60";

   @RBEntry("Error: field can not be left blank")  
   public static final String ERROR_BLANK_FIELD= "61";

   @RBEntry("Ready")  
   public static final String QUEUE_READY_LABEL = "62";

   @RBEntry("Error Stopping Queues")
   public static final String QUEUE_STOP_ERROR_TITLE = "63";

   @RBEntry("The selected queues cannot be stopped because they are disabled")
   public static final String QUEUE_STOP_ERROR_ALL_DISABLED_MESSAGE = "64";

   @RBEntry("Some of the selected queues cannot be stopped because they are disabled.")
   public static final String QUEUE_STOP_ERROR_SOME_DISABLED_MESSAGE = "65";

   @RBEntry("A communication or configuration error has occurred that has prevented the selected queues from being stopped. Check your method server status, system configuration, and database connections.")
   public static final String QUEUE_STOP_ERROR_EXCEPTION_MESSAGE = "66";

   @RBEntry("Stopping")
   public static final String QUEUE_STOPPING_LABEL = "67";

   @RBEntry("Error Enabling Queues")
   public static final String QUEUE_ENABLE_ERROR_TITLE = "68";

   @RBEntry("Some of the selected queues could not be enabled.")
   public static final String QUEUE_ENABLE_ERROR_SOME_ENABLED_MESSAGE = "69";

   @RBEntry("Error Disabling Queues")
   public static final String QUEUE_DISABLE_ERROR_TITLE = "70";

   @RBEntry("Some of the selected queues could not be disabled.")
   public static final String QUEUE_DISABLE_ERROR_SOME_DISABLED_MESSAGE = "71";

}
