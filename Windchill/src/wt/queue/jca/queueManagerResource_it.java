/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.queue.jca;

import wt.util.resource.*;

@RBUUID("wt.queue.jca.queueManagerResource")
public final class queueManagerResource_it extends WTListResourceBundle {
   @RBEntry("Gestione code")
   public static final String QUEUE_MANAGER_LABEL = "0";

   @RBEntry("Nome")
   public static final String QUEUE_NAME_LABEL = "1";

   @RBEntry("Tipo")
   public static final String QUEUE_TYPE_LABEL = "2";

   @RBEntry("Processo")
   public static final String QUEUE_PROCESS_QUEUE_LABEL = "3";

   @RBEntry("Coda di processo immediato")
   public static final String QUEUE_PROCESS_INFO_LABEL = "QUEUE_PROCESS_INFO_LABEL";

   @RBEntry("Elemento coda di processo immediato")
   public static final String QUEUE_PROCESS_ENTRY_INFO_LABEL = "QUEUE_PROCESS_ENTRY_INFO_LABEL";

   @RBEntry("Programmazione")
   public static final String QUEUE_SCHEDULE_QUEUE_LABEL = "4";

   @RBEntry("Coda programmata")
   public static final String QUEUE_SCHEDULE_INFO_LABEL = "QUEUE_SCHEDULE_INFO_LABEL";

   @RBEntry("Elemento coda programmata")
   public static final String QUEUE_SCEDULE_ENTRY_INFO_LABEL = "QUEUE_SCHEDULE_ENTRY_INFO_LABEL";

   @RBEntry("Stato")
   public static final String QUEUE_STATUS_LABEL = "5";

   @RBEntry("Avviata")
   public static final String QUEUE_STARTED_LABEL = "6";

   @RBEntry("Arrestata")
   public static final String QUEUE_STOPPED_LABEL = "7";

   @RBEntry("Attivata")
   public static final String QUEUE_ENABLED_LABEL = "8";

   @RBEntry("Disattivata")
   public static final String QUEUE_DISABLED_LABEL = "9";

   @RBEntry("Gruppo")
   public static final String QUEUE_GROUP_LABEL = "10";

   @RBEntry("Default")
   public static final String QUEUE_DEFAULT_GROUP_LABEL = "11";

   @RBEntry("Totale elementi")
   public static final String QUEUE_TOTAL_ENTRIES_LABEL = "12";

   @RBEntry("Elementi in attesa")
   public static final String QUEUE_WAITING_ENTRIES_LABEL = "13";

   @RBEntry("Elementi gravi/con errore")
   public static final String QUEUE_SEVERE_FAILED_ENTRIES_LABEL = "14";

   @RBEntry("Elementi coda")
   public static final String QUEUE_ENTRIES_LABEL = "15";

   @RBEntry("Numero")
   public static final String QUEUE_ENTRY_NUMBER_LABEL = "16";

   @RBEntry("Coda")
   public static final String QUEUE_LABEL = "17";

   @RBEntry("Stato")
   public static final String QUEUE_ENTRY_STATUS_LABEL = "18";

   @RBEntry("Classe di destinazione")
   public static final String ENTRY_TARGET_CLASS_LABEL = "19";

   @RBEntry("Metodo di destinazione")
   public static final String ENTRY_TARGET_METHOD_LABEL = "20";

   @RBEntry("Argomenti")
   public static final String ENTRY_ARGUMENT_LABEL = "21";

   @RBEntry("Ora di programmazione")
   public static final String ENTRY_SCHEDULE_TIME_LABEL = "22";

   @RBEntry("Attributi")
   public static final String QUEUE_ATTRIBUTES_LABEL = "23";

   @RBEntry("Intervallo di polling")
   public static final String QUEUE_POLLING_INTERVAL_LABEL = "24";

   @RBEntry("Tentativi in caso di errore")
   public static final String QUEUE_FAILURE_RETRIES_LABEL = "25";

   @RBEntry("Durata sospensione")
   public static final String QUEUE_SUSPEND_DURATION_LABEL = "26";

   @RBEntry("Sospendi fino")
   public static final String QUEUE_SUSPEND_UNTIL_LABEL = "27";

   @RBEntry("N/A")
   public static final String QUEUE_NOT_APPLICABLE_LABEL = "28";

   @RBEntry("Numero")
   public static final String QUEUE_ENTRY_ARG_NUMBER_LABEL = "29";

   @RBEntry("Argomento")
   public static final String QUEUE_ENTRY_ARG_LABEL = "30";

   @RBEntry("Tipo di argomento")
   public static final String QUEUE_ENTRY_ARG_TYPE_LABEL = "31";

   @RBEntry("Elemento della coda")
   public static final String QUEUE_ENTRY_LABEL = "32";

   @RBEntry("Proprietario")
   public static final String QUEUE_ENTRY_OWNER_LABEL = "33";

   @RBEntry("Messaggio")
   public static final String QUEUE_ENTRY_MESSAGE_LABEL = "34";

   @RBEntry("Argomenti")
   public static final String QUEUE_ENTRY_ARG_S_LABEL = "35";

   @RBEntry("Oggetto principale")
   public static final String QUEUE_ENTRY_PRIMARY_OBJECT_LABEL = "36";

   @RBEntry("Modifica")
   public static final String EDIT_QUEUE_ATTRIBUTES = "37";

   @RBEntry("Sospesa")
   public static final String QUEUE_SUSPENDED_LABEL = "38";

   @RBEntry("Pool")
   public static final String QUEUE_RANDOMACCESS_QUEUE_LABEL = "39";

   @RBEntry("Coda pool")
   public static final String QUEUE_RANDOMACCESS_INFO_LABEL = "QUEUE_RANDOMACCESS_INFO_LABEL";

   @RBEntry("Elemento coda pool")
   public static final String QUEUE_RANDOMACCESS_ENTRY_INFO_LABEL = "QUEUE_RANDOMACCESS_ENTRY_INFO_LABEL";

   @RBEntry("Avvio")
   public static final String QUEUE_STARTING_LABEL = "40";

   @RBEntry("Avvio")
   public static final String STARTING = "41";

   @RBEntry("Avviata")
   public static final String STARTED = "42";

   @RBEntry("Arresto")
   public static final String STOPPING = "43";

   @RBEntry("Eliminazione")
   public static final String DELETING = "44";

   @RBEntry("Sospesa")
   public static final String SUSPENDED = "45";

   @RBEntry("Arrestata")
   public static final String STOPPED = "46";

   @RBEntry("Nuova coda")
   public static final String CREATE_QUEUE = "47";

   @RBEntry("Sì")
   public static final String YES_LABEL = "48";

   @RBEntry("No")
   public static final String NO_LABEL = "49";

   @RBEntry("Host esecuzione")
   public static final String QUEUE_HOST_LABEL = "50";

   @RBEntry("La coda {0} è stata creata")
   public static final String CREATE_QUEUE_SUCCESS = "51";

   @RBEntry("Errore di avvio delle code")
   public static final String QUEUE_START_ERROR_TITLE = "52";

   @RBEntry("Un errore di comunicazione o configurazione ha impedito l'avvio delle code selezionate. Verificare lo stato del method server, della configurazione del sistema e delle connessioni al database.")
   public static final String QUEUE_START_ERROR_EXCEPTION_MESSAGE = "53";

   @RBEntry("Le code selezionate sono disattivate. Impossibile avviarle.")
   public static final String QUEUE_START_ERROR_ALL_DISABLED_MESSAGE = "54";

   @RBEntry("Alcune delle code selezionate sono disattivate. Impossibile avviarle.")
   public static final String QUEUE_START_ERROR_SOME_DISABLED_MESSAGE = "55";

   @RBEntry("Impossibile creare la coda: numero massimo di code da elaborare superato. Per creare la coda, aumentare il valore di wt.queue.max.processQueues")
   public static final String CREATE_QUEUE_MAX_PROC_ERROR = "56";

   @RBEntry("Impossibile creare la coda: numero massimo di code da programmare superato. Per creare la coda, aumentare il valore di wt.queue.max.scheduleQueues")
   public static final String CREATE_QUEUE_MAX_SCHED_ERROR = "57";

   @RBEntry("Impossibile creare la coda: il nome della coda esiste già. Per creare la coda, specificare un nome diverso.")
   public static final String CREATE_QUEUE_QUEUE_EXISTS = "58";

   @RBEntry("Errore: immettere un intero compreso tra 0 e {0}")
   public static final String ERROR_NON_INTEGER = "59";

   @RBEntry("Errore: valore fuori intervallo. Immettere un intero compreso tra 0 e {0}")
   public static final String ERROR_INTEGER_OUT_OF_RANGE = "60";

   @RBEntry("Errore: il campo non può essere lasciato vuoto")
   public static final String ERROR_BLANK_FIELD= "61";

   @RBEntry("Pronto")
   public static final String QUEUE_READY_LABEL = "62";

   @RBEntry("Errore di arresto delle code")
   public static final String QUEUE_STOP_ERROR_TITLE = "63";

   @RBEntry("Le code selezionate sono disattivate. Impossibile arrestarle.")
   public static final String QUEUE_STOP_ERROR_ALL_DISABLED_MESSAGE = "64";

   @RBEntry("Alcune delle code selezionate sono disattivate. Impossibile arrestarle.")
   public static final String QUEUE_STOP_ERROR_SOME_DISABLED_MESSAGE = "65";

   @RBEntry("Un errore di comunicazione o configurazione ha impedito l'arresto delle code selezionate. Verificare lo stato del method server, della configurazione del sistema e delle connessioni al database.")
   public static final String QUEUE_STOP_ERROR_EXCEPTION_MESSAGE = "66";

   @RBEntry("Arresto")
   public static final String QUEUE_STOPPING_LABEL = "67";

   @RBEntry("Errore di attivazione code")
   public static final String QUEUE_ENABLE_ERROR_TITLE = "68";

   @RBEntry("Impossibile attivare alcune delle code selezionate.")
   public static final String QUEUE_ENABLE_ERROR_SOME_ENABLED_MESSAGE = "69";

   @RBEntry("Errore di disattivazione code")
   public static final String QUEUE_DISABLE_ERROR_TITLE = "70";

   @RBEntry("Impossibile disattivare alcune delle code selezionate.")
   public static final String QUEUE_DISABLE_ERROR_SOME_DISABLED_MESSAGE = "71";

}
