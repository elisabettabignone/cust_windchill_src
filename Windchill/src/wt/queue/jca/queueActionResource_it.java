/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.queue.jca;

import wt.util.resource.*;

@RBUUID("wt.queue.jca.queueActionResource")
public final class queueActionResource_it extends WTListResourceBundle {
   @RBEntry("Attiva")
   public static final String PRIVATE_CONSTANT_0 = "queue.enableQueue.description";

   @RBEntry("Attiva")
   public static final String PRIVATE_CONSTANT_1 = "queue.enableQueue.tooltip";

   @RBEntry("netmarkets/images/queue_enable.gif")
   public static final String PRIVATE_CONSTANT_2 = "queue.enableQueue.icon";

   @RBEntry("Disattiva")
   public static final String PRIVATE_CONSTANT_3 = "queue.disableQueue.description";

   @RBEntry("Disattiva")
   public static final String PRIVATE_CONSTANT_4 = "queue.disableQueue.tooltip";

   @RBEntry("netmarkets/images/queue_disable.gif")
   public static final String PRIVATE_CONSTANT_5 = "queue.disableQueue.icon";

   @RBEntry("Avvia")
   public static final String PRIVATE_CONSTANT_6 = "queue.startQueue.description";

   @RBEntry("Avvia")
   public static final String PRIVATE_CONSTANT_7 = "queue.startQueue.tooltip";

   @RBEntry("netmarkets/images/queue_start.gif")
   public static final String PRIVATE_CONSTANT_8 = "queue.startQueue.icon";

   @RBEntry("Arresta")
   public static final String PRIVATE_CONSTANT_9 = "queue.stopQueue.description";

   @RBEntry("Arresta")
   public static final String PRIVATE_CONSTANT_10 = "queue.stopQueue.tooltip";

   @RBEntry("netmarkets/images/queue_stop.gif")
   public static final String PRIVATE_CONSTANT_11 = "queue.stopQueue.icon";

   @RBEntry("Visualizza informazioni")
   public static final String PRIVATE_CONSTANT_12 = "queue.queueInfo.description";

   @RBEntry("Visualizza informazioni")
   public static final String PRIVATE_CONSTANT_13 = "queue.queueInfo.tooltip";

   @RBEntry("netmarkets/images/details.gif")
   public static final String PRIVATE_CONSTANT_14 = "queue.queueInfo.icon";

   @RBEntry("Modifica")
   public static final String PRIVATE_CONSTANT_15 = "queue.editAttributesFP.description";

   @RBEntry("Modifica")
   public static final String PRIVATE_CONSTANT_16 = "queue.editAttributesFP.tooltip";

   @RBEntry("height=415,width=630")
   @RBComment("DO NOT TRANSLATE")
   @RBPseudo(false)
   public static final String QUEUE_EDITATTRIBUTES_MOREURLINFO = "queue.editAttributesFP.moreurlinfo";

   @RBEntry("Aggiorna")
   public static final String PRIVATE_CONSTANT_17 = "queue.updateEntriesFromTo.describe";

   @RBEntry("Attiva")
   public static final String PRIVATE_CONSTANT_18 = "queueTable.enableSelectedQueues.description";

   @RBEntry("Attiva")
   public static final String PRIVATE_CONSTANT_19 = "queueTable.enableSelectedQueues.tooltip";

   @RBEntry("netmarkets/images/queue_enable.gif")
   public static final String PRIVATE_CONSTANT_20 = "queueTable.enableSelectedQueues.icon";

   @RBEntry("Disattiva")
   public static final String PRIVATE_CONSTANT_21 = "queueTable.disableSelectedQueues.description";

   @RBEntry("Disattiva")
   public static final String PRIVATE_CONSTANT_22 = "queueTable.disableSelectedQueues.tooltip";

   @RBEntry("netmarkets/images/queue_disable.gif")
   public static final String PRIVATE_CONSTANT_23 = "queueTable.disableSelectedQueues.icon";

   @RBEntry("Avvia")
   public static final String PRIVATE_CONSTANT_24 = "queueTable.startSelectedQueues.description";

   @RBEntry("Avvia")
   public static final String PRIVATE_CONSTANT_25 = "queueTable.startSelectedQueues.tooltip";

   @RBEntry("netmarkets/images/queue_start.gif")
   public static final String PRIVATE_CONSTANT_26 = "queueTable.startSelectedQueues.icon";

   @RBEntry("Arresta")
   public static final String PRIVATE_CONSTANT_27 = "queueTable.stopSelectedQueues.description";

   @RBEntry("Arresta")
   public static final String PRIVATE_CONSTANT_28 = "queueTable.stopSelectedQueues.tooltip";

   @RBEntry("netmarkets/images/queue_stop.gif")
   public static final String PRIVATE_CONSTANT_29 = "queueTable.stopSelectedQueues.icon";

   @RBEntry("Nuova coda")
   public static final String PRIVATE_CONSTANT_30 = "queueTable.createQueue.description";

   @RBEntry("Nuova coda")
   public static final String PRIVATE_CONSTANT_31 = "queueTable.createQueue.tooltip";

   @RBEntry("height=415,width=630")
   @RBComment("DO NOT TRANSLATE")
   @RBPseudo(false)
   public static final String QUEUETABLE_CREATEQUEUE_MOREURLINFO = "queueTable.createQueue.moreurlinfo";

   @RBEntry("netmarkets/images/queue_new.png")
   public static final String PRIVATE_CONSTANT_32 = "queueTable.createQueue.icon";

   @RBEntry("Elimina")
   public static final String PRIVATE_CONSTANT_33 = "queueEntries.deleteEntry.description";

   @RBEntry("Elimina gli elementi selezionati della coda")
   public static final String PRIVATE_CONSTANT_34 = "queueEntries.deleteEntry.tooltip";

   @RBEntry("netmarkets/images/delete.png")
   public static final String PRIVATE_CONSTANT_35 = "queueEntries.deleteEntry.icon";

   @RBEntry("Elimina")
   public static final String PRIVATE_CONSTANT_36 = "queueEntries.deleteEntryRedirect.description";

   @RBEntry("Elimina gli elementi selezionati della coda")
   public static final String PRIVATE_CONSTANT_37 = "queueEntries.deleteEntryRedirect.tooltip";

   @RBEntry("netmarkets/images/delete.png")
   public static final String PRIVATE_CONSTANT_38 = "queueEntries.deleteEntryRedirect.icon";

   @RBEntry("Elimina")
   public static final String PRIVATE_CONSTANT_39 = "queueEntries.deleteEntries.description";

   @RBEntry("Elimina gli elementi selezionati della coda")
   public static final String PRIVATE_CONSTANT_40 = "queueEntries.deleteEntries.tooltip";

   @RBEntry("netmarkets/images/delete.png")
   public static final String PRIVATE_CONSTANT_41 = "queueEntries.deleteEntries.icon";

   @RBEntry("Visualizza informazioni")
   public static final String PRIVATE_CONSTANT_42 = "queueEntries.viewInfo.description";

   @RBEntry("Visualizza informazioni")
   public static final String PRIVATE_CONSTANT_43 = "queueEntries.viewInfo.tooltip";

   @RBEntry("netmarkets/images/details.gif")
   public static final String PRIVATE_CONSTANT_44 = "queueEntries.viewInfo.icon";

   @RBEntry("Reimposta")
   public static final String PRIVATE_CONSTANT_45 = "queueEntries.resetReady.description";

   @RBEntry("netmarkets/images/reset.gif")
   public static final String PRIVATE_CONSTANT_46 = "queueEntries.resetReady.icon";

   @RBEntry("Reimposta su pronto gli elementi selezionati della coda")
   public static final String PRIVATE_CONSTANT_47 = "queueEntries.resetReady.tooltip";

   @RBEntry("Attributi")
   public static final String PRIVATE_CONSTANT_48 = "queueEntries.queueEntryAttributes.description";

   @RBEntry("Attributi")
   public static final String PRIVATE_CONSTANT_49 = "queueEntries.queueEntryAttributes.tooltip";

   @RBEntry("Elementi coda")
   public static final String PRIVATE_CONSTANT_50 = "queueTableEntries.queueEntryTable.description";

   @RBEntry("Attributi")
   public static final String PRIVATE_CONSTANT_51 = "queueTableEntries.queueAttributesTable.description";

   @RBEntry("Argomenti elemento")
   public static final String PRIVATE_CONSTANT_52 = "queueEntryArgsType.queueEntryArgs.description";

   @RBEntry("Elimina")
   public static final String PRIVATE_CONSTANT_53 = "queueTableEntries.deleteTableEntries.description";

   @RBEntry("Elimina")
   public static final String PRIVATE_CONSTANT_54 = "queueTableEntries.deleteTableEntries.tooltip";

   @RBEntry("netmarkets/images/delete.png")
   public static final String PRIVATE_CONSTANT_55 = "queueTableEntries.deleteTableEntries.icon";

   @RBEntry("Dettagli")
   public static final String PRIVATE_CONSTANT_56 = "object.queueInfoDefaultDetails.description";

   @RBEntry("Dettagli")
   public static final String PRIVATE_CONSTANT_57 = "object.queueEntryInfoDefaultDetails.description";

   @RBEntry("Ignora")
   public static final String PRIVATE_CONSTANT_58 = "queueEntries.piks.description";

   @RBEntry("Ignora")
   public static final String PRIVATE_CONSTANT_59 = "queueEntries.piks.tooltip";

   @RBEntry("netmarkets/images/arrow_rightright.gif")
   public static final String PRIVATE_CONSTANT_60 = "queueEntries.piks.icon";
}
