/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.servlet;

import wt.util.resource.*;

@RBUUID("wt.servlet.errorPageResource")
public final class errorPageResource_it extends WTListResourceBundle {
   @RBEntry("Errore Windchill")
   @RBComment("Title of generic servlet request error page")
   public static final String WINDCHILL_ERROR = "0";

   @RBEntry("Contattare l'amministratore per assistenza.")
   @RBComment("Message given on generic servlet request error page")
   public static final String CONTACT_YOUR_ADMIN_FOR_ASSISTANCE = "1";

   @RBEntry("Di seguito sono riportate le informazioni di riferimento.")
   @RBComment("Message given on generic servlet request error page")
   public static final String INFO_FOR_REFERENCE_FOLLOWS = "1.5";

   @RBEntry("ID richiesta")
   @RBComment("Label for servlet request id information")
   public static final String REQUEST_ID = "2";

   @RBEntry("URI richiesta")
   @RBComment("Label for servlet request URI information")
   public static final String REQUEST_URI = "3";

   @RBEntry("Stringa interrogazione")
   @RBComment("Label for servlet request query string information")
   public static final String QUERY_STRING = "4";

   @RBEntry("Codice stato")
   @RBComment("Label for servlet request status code information")
   public static final String STATUS_CODE = "5";

   @RBEntry("Eccezione")
   @RBComment("Label for servlet request exception information")
   public static final String EXCEPTION = "6";

   @RBEntry("Messaggio")
   @RBComment("Label for servlet request status message information")
   public static final String STATUS_MESSAGE = "7";

   @RBEntry("Data e ora correnti")
   @RBComment("Label for current time information")
   public static final String CURRENT_TIME = "8";

   @RBEntry("Errore Windchill - ID richiesta servlet: {0}")
   @RBComment("Subject to use for e-mail to an adminstrator about an error")
   public static final String ERROR_EMAIL_SUBJ_MESG = "9";

   @RBEntry("Vedere {0}")
   @RBComment("Body to use for e-mail to an adminstrator about an error")
   public static final String ERROR_EMAIL_BODY_MESG = "10";
}
