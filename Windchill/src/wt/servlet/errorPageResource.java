/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.servlet;

import wt.util.resource.*;

@RBUUID("wt.servlet.errorPageResource")
public final class errorPageResource extends WTListResourceBundle {
   @RBEntry("Windchill Error")
   @RBComment("Title of generic servlet request error page")
   public static final String WINDCHILL_ERROR = "0";

   @RBEntry("Contact your administrator for assistance.")
   @RBComment("Message given on generic servlet request error page")
   public static final String CONTACT_YOUR_ADMIN_FOR_ASSISTANCE = "1";

   @RBEntry("Information for their reference follows.")
   @RBComment("Message given on generic servlet request error page")
   public static final String INFO_FOR_REFERENCE_FOLLOWS = "1.5";

   @RBEntry("Request Id")
   @RBComment("Label for servlet request id information")
   public static final String REQUEST_ID = "2";

   @RBEntry("Request URI")
   @RBComment("Label for servlet request URI information")
   public static final String REQUEST_URI = "3";

   @RBEntry("Query String")
   @RBComment("Label for servlet request query string information")
   public static final String QUERY_STRING = "4";

   @RBEntry("Status Code")
   @RBComment("Label for servlet request status code information")
   public static final String STATUS_CODE = "5";

   @RBEntry("Exception")
   @RBComment("Label for servlet request exception information")
   public static final String EXCEPTION = "6";

   @RBEntry("Message")
   @RBComment("Label for servlet request status message information")
   public static final String STATUS_MESSAGE = "7";

   @RBEntry("Current Time")
   @RBComment("Label for current time information")
   public static final String CURRENT_TIME = "8";

   @RBEntry("Windchill Error - Servlet request id: {0}")
   @RBComment("Subject to use for e-mail to an adminstrator about an error")
   public static final String ERROR_EMAIL_SUBJ_MESG = "9";

   @RBEntry("See {0}")
   @RBComment("Body to use for e-mail to an adminstrator about an error")
   public static final String ERROR_EMAIL_BODY_MESG = "10";
}
