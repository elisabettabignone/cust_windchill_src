/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.servlet;


import java.util.regex.Pattern;

import javax.management.AttributeList;
import javax.management.DynamicMBean;
import javax.management.NotCompliantMBeanException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import wt.jmx.core.AttributeListWrapper;
import wt.jmx.core.BaseObjectNamed;
import wt.jmx.core.MBeanUtilities;
import wt.jmx.core.SelfAwareMBean;
import wt.jmx.core.mbeans.ContextListener;
import wt.log4j.Log4jUtilities;
import wt.log4j.LogR;


/* In addition to providing useful functionality, this class is intended to
 * serve as an example of a SelfAwareMBean ContextListener<RequestMBean>
 * implementation -- to aid both developers and customizers in implementing such
 * classes.  This class also demonstrates the use of AttributeListWrapper and
 * tweaking JavaBeans persistence for a class.
 *
 * Even though source code for this class is provided, compilation of this
 * source under the existing class name (i.e. in place of the class delivered)
 * is still not supported.  Additionally, details in the actual implementation
 * of this class may change over time so one should not rely on such
 * implementation details.
 */


/** {@link wt.jmx.core.mbeans.ContextListener}&lt;{@link wt.servlet.RequestMBean}&gt;
 *  MBean that allows selective logging of servlet requests that match specific criteria.
 *  <p>
 *  To use this class specify the desired name for a listener instance and the
 *  full classname of this class to the addRequestListener() operation of
 *  {@link wt.servlet.ServletRequestMonitorMBean}.
 *
 *    <BR><BR><B>Supported API: </B>true
 *    <BR><BR><B>Extendable: </B>false
 */
public final class  FilteredRequestLogger
  extends SelfAwareMBean
  implements FilteredRequestLoggerMBean, ContextListener<RequestMBean>
{
  // Constructors

  /** Public constructor used by ServletRequestMonitor MBean to pass name to
   *  instances.  Also used by {@link java.beans.XMLEncoder}-based persistence
   *  mechanism.
   */
  public  FilteredRequestLogger( final String name )
    throws NotCompliantMBeanException
  {
    super( FilteredRequestLoggerMBean.class );
    this.name = name;
    logger = LogR.getLogger( FilteredRequestLogger.class.getName() + '.' + name );
  }

  // SelfAware MBean overrides

  // Required SelfAwareMBean method override
  @Override
  public String[][]  getObjectNameSuffix()
  {
    return (
      new String[][]
      {
        { "grouping", "Listeners" },
        { "name", name }
      }
    );
  }

  // SelfAwareMBean method override
  @Override
  public synchronized void  setOwnerMBean( final BaseObjectNamed ownerMBean )
  {
    super.setOwnerMBean( ownerMBean );
    requestMonitor = (ServletRequestMonitorMBean) ownerMBean;
  }

  // FilteredRequestLoggerMBean implementation

  // FilteredRequestLoggerMBean method implementation
  @Override
  public String  getName()
  {
    return ( name );
  }

  // FilteredRequestLoggerMBean method implementation
  @Override
  public boolean  isEnabled()
  {
    return ( enabled );
  }

  // FilteredRequestLoggerMBean method implementation
  @Override
  public void  setEnabled( final boolean enabled )
  {
    this.enabled = enabled;
  }

  // FilteredRequestLoggerMBean method implementation
  @Override
  public String  getLoggerName()
  {
    return ( logger.getName() );
  }

  // FilteredRequestLoggerMBean method implementation
  @Override
  public String  getLoggerLevel()
  {
    return ( Log4jUtilities.getLevelAsString( logger ) );
  }

  // FilteredRequestLoggerMBean method implementation
  @Override
  public void  setLoggerLevel( final String level )
  {
    Log4jUtilities.setLoggerLevel( logger, level );
  }

  // FilteredRequestLoggerMBean method implementation
  @Override
  public String  getUserNamesToLog()
  {
    final String  userNamesToLog[] = this.userNamesToLog;
    return ( ( userNamesToLog != null ) ? MBeanUtilities.getStringArrayAsString( userNamesToLog ) : null );
  }

  // FilteredRequestLoggerMBean method implementation
  @Override
  public void  setUserNamesToLog( final String userNames )
  {
    userNamesToLog = MBeanUtilities.getStringArrayFromString( userNames, null );
  }

  // FilteredRequestLoggerMBean method implementation
  @Override
  public String  getURIPatternToLog()
  {
    return ( toString( uriPatternToLog ) );
  }

  // FilteredRequestLoggerMBean method implementation
  @Override
  public void  setURIPatternToLog( final String uriPatternToLog )
  {
    this.uriPatternToLog = compile( uriPatternToLog );
  }

  // FilteredRequestLoggerMBean method implementation
  @Override
  public boolean  isLogAsDynamicMBean()
  {
    return ( logAsDynamicMBean );
  }

  // FilteredRequestLoggerMBean method implementation
  @Override
  public void  setLogAsDynamicMBean( final boolean logAsDynamicMBean )
  {
    this.logAsDynamicMBean = logAsDynamicMBean;
  }

  // ContextListener<RequestMBean> implementation

  // ContextListener<RequestMBean> method implementation
  @Override
  public void  started( final RequestMBean request )
  {
    if ( !enabled )
      return;
    if ( !logger.isDebugEnabled() )  // check before isRequestOfInterest() due to speed of check and likelihood of returning false
      return;
    if ( !isRequestOfInterest( request ) )
      return;

    logger.debug( "REQUEST STARTED: " + request.getId() );
  }

  // ContextListener<RequestMBean> method implementation
  @Override
  public void  completed( final RequestMBean request )
  {
    if ( !enabled )
      return;
    if ( !isRequestOfInterest( request ) )
      return;

    final ServletRequestMonitorMBean  requestMonitor = this.requestMonitor;  // copy from field to final local variable

    final Throwable  throwableThrown = ((Request)request).getThrowableThrown();  // assume Request implementation of RequestMBean here
    final int  statusCode = request.getStatusCode();
    final boolean  error = ( ( throwableThrown != null ) || ( statusCode >= 500 ) );  // log requests with exceptions and >=500 statuses as errors
    final double  requestTimeWarnThreshold = requestMonitor.getRequestTimeWarnThreshold();
    final boolean  warn = ( ( ( requestTimeWarnThreshold > 0.0 ) && ( request.getElapsedSeconds() > requestTimeWarnThreshold ) ) ||
                            ( ( statusCode >= 400 ) && ( statusCode != 404 ) ) );  // log long running requests and >=400, !=404 responses as warnings
    final Level  level = ( error ? ( ( throwableThrown instanceof VirtualMachineError ) ? Level.FATAL
                                                                                        : Level.ERROR )
                                 : ( warn ? Level.WARN
                                          : Level.INFO ) );
    if ( !logger.isEnabledFor( level ) )
      return;

    final Object  logMessage;
    if ( logAsDynamicMBean )
      logMessage = request;
    else
    {
      final String  attributeNames[] = MBeanUtilities.getStringArrayFromString( requestMonitor.getRequestLoggerOutputAttributes(), null );
      final AttributeList  attributes = ((DynamicMBean)request).getAttributes( attributeNames );
      final AttributeListWrapper  logData = AttributeListWrapper.newInstance( attributes );
      logData.setShortFormat( requestMonitor.isRequestLoggerUseShortFormat() );
      logData.setMultiLineFormat( requestMonitor.isRequestLoggerMultiLineFormat() );
      logData.setSeparator( requestMonitor.getRequestLoggerSeparatorString() );
      logData.setFormatString( requestMonitor.getRequestLoggerFormatString() );
      logMessage = logData;
    }
    logger.log( level, logMessage, throwableThrown );
  }

  // Internal implementation methods

  // Determines whether a given servlet request is of interest for logging
  private boolean  isRequestOfInterest( final RequestMBean request )
  {
    // check if remote user matches criteria
    final String  userNamesToLog[] = this.userNamesToLog;  // copy from volatile field to final local variable
    if ( userNamesToLog != null )
    {
      final String  remoteUser = request.getRemoteUser();
      if ( remoteUser == null )
        return ( false );
      if ( !contains( userNamesToLog, remoteUser ) )
        return ( false );
    }

    // check if request URI matches criteria
    final Pattern  uriPatternToLog = this.uriPatternToLog;  // copy from volatile field to final local variable
    if ( uriPatternToLog != null )
      if ( !matches( uriPatternToLog, request.getContextRelativeRequestURI() ) )
        return ( false );

    // if we matched everything, then return true
    return ( true );
  }

  // assumes none of the strings are null, which is valid for our usage here
  private static boolean  contains( final String strings[], final String string )
  {
    for ( String aString : strings )
      if ( string.equals( aString ) )
        return ( true );
    return ( false );
  }

  private static boolean  matches( final Pattern pattern, final String string )
  {
    return ( ( string != null ) && pattern.matcher( string ).matches() );
  }

  private static String  toString( final Pattern pattern )
  {
    return ( ( pattern != null ) ? pattern.toString() : null );
  }

  private static Pattern  compile( final String patternString )
  {
    if ( ( patternString == null ) || ( patternString.length() == 0 ) )
      return ( null );
    return ( Pattern.compile( patternString ) );
  }

  // Instance fields

  private final String  name;
  private final Logger  logger;
  private ServletRequestMonitorMBean  requestMonitor;
  private volatile boolean  enabled;
  private volatile String  userNamesToLog[];
  private volatile Pattern  uriPatternToLog;  // context relative request URI matching pattern
  private boolean  logAsDynamicMBean;
}
