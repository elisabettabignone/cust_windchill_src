package wt.servlet;


import java.beans.DefaultPersistenceDelegate;
import java.beans.Encoder;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.beans.Statement;

import wt.jmx.core.OverrideableBeanInfo;


public class  FilteredRequestLoggerBeanInfo
  extends OverrideableBeanInfo
{
  public  FilteredRequestLoggerBeanInfo()
    throws IntrospectionException
  {
    super( FilteredRequestLogger.class );

    // set up bean persistence to use appropriate constructor, persist logger level, and not persist OwnerMBean

    wrappedBeanInfo.getBeanDescriptor().setValue( "persistenceDelegate",
      new DefaultPersistenceDelegate( new String[] { "name" } )  // set up bean persistence to use the FilteredRequestLogger(String) constructor and the "name" JavaBean property value as the argument
      {
        @Override
        protected final void  initialize( final Class<?> type, final Object oldInstance, final Object newInstance, final Encoder out )
        {
          super.initialize( type, oldInstance, newInstance, out );

          // logger level ordinarily would not be saved as bean encoder discovers that it is not really this object's property, but we want it saved here
          final FilteredRequestLogger  frl = (FilteredRequestLogger) oldInstance;
          final String  loggerLevel = frl.getLoggerLevel();
          if ( loggerLevel != null )
            out.writeStatement( new Statement( oldInstance, "setLoggerLevel", new Object[] { loggerLevel } ) );
        }
      }
    );

    /* We do not need to persist the OwnerMBean property with this MBean as the
      * parent will call setOwnerMBean.  In the case of ServletRequestMonitor,
      * we normally do not persist this directly and an error will occur in the
      * MBeanLoader when this is attempted.  The MBeanLoader will recover by
      * simply not persisting the OwnerMBean information, but will log an ugly
      * looking error, so while not strictly necessary it is much cleaner to
      * mark the OwnerMBean property as transient.
      */
    for ( PropertyDescriptor propDescr : wrappedBeanInfo.getPropertyDescriptors() )
    {
      final String  propName = propDescr.getName();
      if ( "OwnerMBean".equalsIgnoreCase( propName ) )
        propDescr.setValue( "transient", Boolean.TRUE );  // BEWARE: property getter or setter should be overriden in this class or you will impact base class behavior
    }
  }
}
