/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.servlet;

/* In addition to providing useful functionality, this interface is intended to
 * serve as a example MBean interface to aid developers and customizers author
 * their own MBean interfaces and implement these.
 *
 * Even though source code for this class is provided, compilation of this
 * source under the existing class name (i.e. in place of the class delivered)
 * is still not supported.
 */

/* For each of our MBean interface source files, xxxMBean.java, automated
 * tooling is used to produce a corresponding xxxMBeanResource.rbInfo file.
 * Thus in the case of this MBean inteface, FilteredRequestLoggerMBeanResource.rbInfo
 * is produced by this tooling.
 *
 * To [re]generate the rbInfo for an MBean interface source file (upon creating
 * a new MBean interface file or making changes), invoke the following command
 * from the source root directory in a Windchill shell:
 *   javac -proc:only -processor wt.annotations.processors.MBeanInterfaceProcessor <path of source file>
 * For instance, for this MBean interface (on Windows) one would invoke
 *   javac -proc:only -processor wt.annotations.processors.MBeanInterfaceProcessor wt\jmx\core\mbeans\FilteredRequestLoggerMBean.java
 *
 * The tool garners descriptions and operation parameter names from the formal
 * parameter names and Javadoc comments in the MBean interface source file and
 * expresses these in the rbInfo.  At runtime, the wt.jmx.core.StandardMBean
 * reads these rbInfos and incorporates this additional metadata into the
 * MBeans' MBeanInfo.  This allows for localization, though no localized
 * versions of these particular bundles are currently produced for delivery
 * with the product.  Re-use of formal parameters and Javadoc comment information
 * avoids redundant documentation efforts and ensures consistency between
 * Javadoc documentation and MBean documentation at runtime.
 */

import wt.jmx.core.mbeans.SelfEmailingMBean;


/** Allows selective logging of servlet requests that match specific criteria.
 *
 *    <BR><BR><B>Supported API: </B>true
 *    <BR><BR><B>Extendable: </B>true
 */
public interface  FilteredRequestLoggerMBean
  extends SelfEmailingMBean
{
  /** Name of this MBean
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  public String  getName();

  /** Whether this MBean is enabled; when disabled this MBean will not log, irrespective of the LoggerLevel setting
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  public boolean  isEnabled();

  /** Whether this MBean is enabled; when disabled this MBean will not log, irrespective of the LoggerLevel setting
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  public void  setEnabled( boolean enabled );

  /** Name of logger this MBean uses
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  public String  getLoggerName();

  /** Logging level; valid values are ALL, TRACE, DEBUG, INFO, WARN, ERROR, FATAL, OFF (or blank/unspecified)
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  public String  getLoggerLevel();

  /** Logging level; valid values are ALL, TRACE, DEBUG, INFO, WARN, ERROR, FATAL, OFF (or blank/unspecified)
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  public void  setLoggerLevel( String level );

  /** User names for which requests should be logged; when this is null/empty, no user name filter is applied
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  public String  getUserNamesToLog();

  /** User names for which requests should be logged; when this is null/empty, no user name filter is applied
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  public void  setUserNamesToLog( String userNames );

  /** Regular expression which request's ContextRelativeRequestURI must match in
   *  order to be logged; when this is null/empty no request URI filter is applied
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  public String  getURIPatternToLog();

  /** Regular expression which request's ContextRelativeRequestURI must match in
   *  order to be logged; when this is null/empty no request URI filter is applied
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  public void  setURIPatternToLog( String uriPatternToLog );

  /** Whether servlet request data should be logged as a wt.servlet.RequestMBean or instead formatted as specified
   *  by the ServletRequestMonitor MBean's RequestLogger* atttributes (and thus as an AttributeListWrapper).
   *  Normally the latter approach is useful in order to allow selection of servlet request data to
   *  log and formatting thereof, but in cases one might wish to log as a RequestMBean in order to allow
   *  a log4j Appender like wt.log4j.jmx.AsyncJDBCAppender to independently select the data of interest.
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  public boolean  isLogAsDynamicMBean();

  /** Whether servlet request data should be logged as a wt.servlet.RequestMBean or instead formatted as specified
   *  by the ServletRequestMonitor MBean's RequestLogger* atttributes (and thus as an AttributeListWrapper).
   *  Normally the latter approach is useful in order to allow selection of servlet request data to
   *  log and formatting thereof, but in cases one might wish to log as a RequestMBean in order to allow
   *  a log4j Appender like wt.log4j.jmx.AsyncJDBCAppender to independently select the data of interest.
   *
   *    <BR><BR><B>Supported API: </B>true
   */
  public void  setLogAsDynamicMBean( boolean logAsDynamicMBean );
}
