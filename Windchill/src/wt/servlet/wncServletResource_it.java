/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.servlet;

import wt.util.resource.*;

@RBUUID("wt.servlet.wncServletResource")
public final class wncServletResource_it extends WTListResourceBundle {
   @RBEntry("L'operazione 'registerProcess' è stata richiamata da un host non attendibile.")
   public static final String REGISTER_PROCESS_CALLED_FROM_UNTRUSTED_HOST = "1";
}
