/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.effectivity;

import wt.util.resource.*;

@RBUUID("wt.effectivity.effectivityResource")
public final class effectivityResource extends WTListResourceBundle {
   @RBEntry("The passed configuration item name was null.")
   public static final String CONFIG_ITEM_NAME_NULL = "0";

   @RBEntry("Unable to find a configuration item with name: {0}")
   @RBArgComment0("is a Configuration Item name, for example: \"Honda Accord\"")
   public static final String CONFIG_ITEM_NOT_FOUND = "1";

   @RBEntry("The TransactionContainer passed to ChangeHelper.service.persistEffectivityBatch is null.")
   public static final String NULL_EFF_TRANS_CONTAINER = "2";

   @RBEntry("Invalid assertion encountered.  Refer to the Javadoc for the StandardChangeService.persistEffectivityBatch method.")
   public static final String INVALID_BATCH_ASSERTION = "3";

   @RBEntry("Null value passed for Effectivity (parameter 1) in ChangeHelper.setConfigurationItem.")
   public static final String EFFECTIVITY_NULL_ARG = "4";

   @RBEntry("Null value passed for Configuration Item (parameter 2) in ChangeHelper.setConfigurationItem.")
   public static final String CONFIG_ITEM_NULL_ARG = "5";

   @RBEntry("The effective context set on WTPartEffectivityConfigSpec must be persistent.")
   public static final String CONFIG_ITEM_NON_PERSIST = "6";

   @RBEntry("An error was encountered while setting the effective context in WTPartEffectivityConfigSpec.  This message should never occur.")
   public static final String ERROR_SETTING_CONFIG_ITEM = "7";

   @RBEntry("LoadEffectivity: The field \"{0}\" is not a valid field for this record type. Aborting this record.")
   @RBArgComment0("an invalid field")
   public static final String LOAD_INVALID_FIELD = "10";

   @RBEntry("LoadEffectivity: A value for field \"{0}\" was not found in the input file.")
   @RBArgComment0("a field which did not have a value")
   public static final String LOAD_NO_VALUE = "11";

   @RBEntry("LoadEffectivity: A value for \"{0}\" is required to construct the current object.  Aborting this record.")
   @RBArgComment0("a field which requires a value")
   public static final String LOAD_NULL_ILLEGAL = "12";

   @RBEntry("LoadEffectivity: Error detected in {0}.  Check Method Server log.")
   @RBArgComment0("name of Java method in which error occured")
   public static final String LOAD_EXCEPTION = "13";

   @RBEntry("LoadEffectivity: There is no current part.  The sucessfull creation of a part must preceede the creation of an effectivity object.")
   public static final String LOAD_NO_CURRENT_PART = "14";

   @RBEntry("The Configuration Item: \"{0}\" may not be deleted because it is referenced by an existing effectivity.")
   @RBArgComment0("is a Configuration Item name, for example: \"Honda Accord\"")
   public static final String INVALID_CONFIG_ITEM_DELETE_SINGULAR = "15";

   @RBEntry("The Configuration Item: \"{0}\" may not be deleted because it is referenced by {1} existing effectivities.")
   public static final String INVALID_CONFIG_ITEM_DELETE_PLURAL = "16";

   @RBEntry("{0} {1}")
   public static final String CONFIGURATION_ITEM_TYPE = "18";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String CREATE_WTPRODUCTINSTANCE = "19";

   @RBEntry("Create Product Instance")
   public static final String CREATE_WTPRODUCTINSTANCE_URL_LABEL = "20";

   @RBEntry("Instances of")
   @RBComment("Used when navigating product instances (from a configuration item).  Context is \"Instances of Configuration Item XYZ\"")
   public static final String INSTANCES_HEADER = "21";

   @RBEntry("Instances")
   @RBComment("Table name for the table of product instances of a configuration item.")
   public static final String INSTANCES_TABLE = "22";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String CREATE_WTPRODUCTINSTANCE_HEADER = "23";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String INVALID_SERIAL_NUMBER = "24";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String UPDATE_WTPRODUCTINSTANCE = "25";

   @RBEntry("Set Build Date")
   public static final String UPDATE_WTPRODUCTINSTANCE_URL_LABEL = "26";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String UPDATE_WTPRODUCTINSTANCE_HEADER = "27";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String INVALID_NULL_BUILD_DATE = "28";

   @RBEntry("Future dates are not allowed; the build date records an actual date, not a planning date.")
   @RBComment("Error condition used by WTProductInstance's setBuildDate.")
   public static final String INVALID_FUTURE_BUILD_DATE = "29";

   @RBEntry("You can not update \"{0}\" because it has been built")
   @RBComment("Error condition used caused by a user attempting to update a product instances that has been built.")
   public static final String CANNOT_UPDATE_BUILT_PRODUCT_INSTANCE = "30";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String DELETE_WTPRODUCTINSTANCE = "31";

   @RBEntry("Delete")
   public static final String DELETE_WTPRODUCTINSTANCE_URL_LABEL = "32";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String DELETE_WTPRODUCTINSTANCE_HEADER = "33";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String DELETE_WTPRODUCTINSTANCE_CONFIRMATION = "34";

   @RBEntry("You can not delete \"{0}\" because it has been built")
   @RBComment("Error condition used caused by a user attempting to update a product instances that has been built.")
   public static final String CANNOT_DELETE_BUILT_PRODUCT_INSTANCE = "35";

   @RBEntry("{0} has been deleted")
   @RBComment("Indicates that the product instance has been deleted.")
   public static final String WTPRODUCTINSTANCE_DELETED_MESSAGE = "36";

   @RBEntry("The effective unit cannot be set to a value when the effective context is a product instance")
   public static final String INVALID_EFFECTIVE_UNIT_FOR_PI = "40";

   @RBEntry("The effective unit cannot be set to a null value when the effective context is not a Date Configuration Item")
   public static final String INVALID_EFFECTIVE_UNIT_FOR_CI = "41";

   @RBEntry("{0} {1}")
   @RBComment("The configuration item's identity.  Example:  \"Serial Number Configuration Item engine")
   public static final String CONFIGURATIONITEM_IDENTITY = "42";
}
