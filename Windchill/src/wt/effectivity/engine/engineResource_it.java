/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.effectivity.engine;

import wt.util.resource.*;

@RBUUID("wt.effectivity.engine.engineResource")
public final class engineResource_it extends WTListResourceBundle {
   @RBEntry("Intervallo \"{0}\" non valido.")
   public static final String INVALID_RANGE = "0";

   @RBEntry("Impossibile rimuovere\"{0}\", poiché soddisfa la soluzione di progettazione di una direttiva di modifica che influisce sul configuration item padre.")
   @RBComment("Cannot remove part usage with change actions in a solved state.")
   public static final String REMOVEUSAGE_ERROR = "1";

   @RBEntry("Impossibile annullare la soddisfazione per l'azione di modifica \"{0}\" in quanto è associata al configuration item \"{1}\" che è sottoposto a Check-Out.")
   @RBComment("Cannot unfulfill change actions associated with ci's in the checked out state.")
   public static final String UNFULFILL_ERROR = "2";

   @RBEntry("Impossibile soddisfare l'azione di modifica \"{0}\" in quanto è associata al configuration item \"{1}\" che è sottoposto a Check-Out.")
   @RBComment("Cannot fulfill change actions associated with ci's in the checked out state.")
   public static final String FULFILL_ERROR = "3";
}
