/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.effectivity.engine;

import wt.util.resource.*;

@RBUUID("wt.effectivity.engine.engineResource")
public final class engineResource extends WTListResourceBundle {
   @RBEntry("Range \"{0}\" is invalid.")
   public static final String INVALID_RANGE = "0";

   @RBEntry("Cannot remove \"{0}\" since it fulfills a Design Solution for a Change Directive that impacts the parent Configuration Item.")
   @RBComment("Cannot remove part usage with change actions in a solved state.")
   public static final String REMOVEUSAGE_ERROR = "1";

   @RBEntry("The change action \"{0}\" cannot be unfulfilled, since it is associated with a configuration item, \"{1}\", in the checked out state.")
   @RBComment("Cannot unfulfill change actions associated with ci's in the checked out state.")
   public static final String UNFULFILL_ERROR = "2";

   @RBEntry("The change action \"{0}\" cannot be fulfilled, since it is associated with a configuration item, \"{1}\", in the checked out state.")
   @RBComment("Cannot fulfill change actions associated with ci's in the checked out state.")
   public static final String FULFILL_ERROR = "3";
}
