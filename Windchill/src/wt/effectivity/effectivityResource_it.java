/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.effectivity;

import wt.util.resource.*;

@RBUUID("wt.effectivity.effectivityResource")
public final class effectivityResource_it extends WTListResourceBundle {
   @RBEntry("Nome del configuration item trasferito nullo.")
   public static final String CONFIG_ITEM_NAME_NULL = "0";

   @RBEntry("Impossibile individuare configuration item con il nome:  {0}")
   @RBArgComment0("is a Configuration Item name, for example: \"Honda Accord\"")
   public static final String CONFIG_ITEM_NOT_FOUND = "1";

   @RBEntry("Il TransactionContainer trasferito a ChangeHelper.service.persistEffectivityBatch è nullo.")
   public static final String NULL_EFF_TRANS_CONTAINER = "2";

   @RBEntry("È stata rilevata una condizione non valida. Fare riferimento a Javadoc per informazioni sul metodo StandardChangeService.persistEffectivityBatch. ")
   public static final String INVALID_BATCH_ASSERTION = "3";

   @RBEntry("In ChangeHelper.setConfigurationItem è stato trasferito un valore nullo per l'effettività (parametro 1).")
   public static final String EFFECTIVITY_NULL_ARG = "4";

   @RBEntry("In ChangeHelper.setConfigurationItem è stato trasferito un valore nullo per il configuration item (parametro 2). ")
   public static final String CONFIG_ITEM_NULL_ARG = "5";

   @RBEntry("Il contesto di effettività impostato su WTPartEffectivityConfigSpec deve essere persistente.")
   public static final String CONFIG_ITEM_NON_PERSIST = "6";

   @RBEntry("Si è verificato un errore durante l'impostazione del contesto di effettività in WTPartEffectivityConfigSpec. Questo messaggio non dovrebbe mai venire visualizzato.")
   public static final String ERROR_SETTING_CONFIG_ITEM = "7";

   @RBEntry("LoadEffectivity. Il campo \"{0}\" non è valido per questo tipo di registrazione.  Registrazione interrotta.")
   @RBArgComment0("an invalid field")
   public static final String LOAD_INVALID_FIELD = "10";

   @RBEntry("LoadEffectivity. Impossibile trovare nel file di input un valore per il campo \"{0}\". ")
   @RBArgComment0("a field which did not have a value")
   public static final String LOAD_NO_VALUE = "11";

   @RBEntry("LoadEffectivity. Per costruire l'oggetto corrente è obbligatorio specificare un valore per \"{0}\".   Registrazione interrotta.")
   @RBArgComment0("a field which requires a value")
   public static final String LOAD_NULL_ILLEGAL = "12";

   @RBEntry("LoadEffectivity. È stato rilevato un errore in {0}. Verificare log del method server.")
   @RBArgComment0("name of Java method in which error occured")
   public static final String LOAD_EXCEPTION = "13";

   @RBEntry("LoadEffectivity. Nessuna parte corrente.  È necessario creare una parte prima di creare un oggetto di effettività. ")
   public static final String LOAD_NO_CURRENT_PART = "14";

   @RBEntry("Impossibile eliminare il configuration item \"{0}\" perché è referenziato da un'effettività esistente. ")
   @RBArgComment0("is a Configuration Item name, for example: \"Honda Accord\"")
   public static final String INVALID_CONFIG_ITEM_DELETE_SINGULAR = "15";

   @RBEntry("Impossibile eliminare il configuration item \"{0}\" perché è referenziato da {1} effettività esistenti. ")
   public static final String INVALID_CONFIG_ITEM_DELETE_PLURAL = "16";

   @RBEntry("{0} {1}")
   public static final String CONFIGURATION_ITEM_TYPE = "18";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String CREATE_WTPRODUCTINSTANCE = "19";

   @RBEntry("Crea istanza di prodotto")
   public static final String CREATE_WTPRODUCTINSTANCE_URL_LABEL = "20";

   @RBEntry("Istanza di")
   @RBComment("Used when navigating product instances (from a configuration item).  Context is \"Instances of Configuration Item XYZ\"")
   public static final String INSTANCES_HEADER = "21";

   @RBEntry("Istanze")
   @RBComment("Table name for the table of product instances of a configuration item.")
   public static final String INSTANCES_TABLE = "22";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String CREATE_WTPRODUCTINSTANCE_HEADER = "23";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String INVALID_SERIAL_NUMBER = "24";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String UPDATE_WTPRODUCTINSTANCE = "25";

   @RBEntry("Imposta data build")
   public static final String UPDATE_WTPRODUCTINSTANCE_URL_LABEL = "26";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String UPDATE_WTPRODUCTINSTANCE_HEADER = "27";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String INVALID_NULL_BUILD_DATE = "28";

   @RBEntry("Non sono consentite date future; la data di build costituisce la registrazione della data effettiva, non di una data programmata.")
   @RBComment("Error condition used by WTProductInstance's setBuildDate.")
   public static final String INVALID_FUTURE_BUILD_DATE = "29";

   @RBEntry("Impossibile aggiornare \"{0}\" perché ne è stato effettuato il build.")
   @RBComment("Error condition used caused by a user attempting to update a product instances that has been built.")
   public static final String CANNOT_UPDATE_BUILT_PRODUCT_INSTANCE = "30";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String DELETE_WTPRODUCTINSTANCE = "31";

   @RBEntry("Elimina")
   public static final String DELETE_WTPRODUCTINSTANCE_URL_LABEL = "32";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String DELETE_WTPRODUCTINSTANCE_HEADER = "33";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String DELETE_WTPRODUCTINSTANCE_CONFIRMATION = "34";

   @RBEntry("Impossibile eliminare \"{0}\" perché ne è stato effettuato il build.")
   @RBComment("Error condition used caused by a user attempting to update a product instances that has been built.")
   public static final String CANNOT_DELETE_BUILT_PRODUCT_INSTANCE = "35";

   @RBEntry("{0} è stato eliminato")
   @RBComment("Indicates that the product instance has been deleted.")
   public static final String WTPRODUCTINSTANCE_DELETED_MESSAGE = "36";

   @RBEntry("L'unità effettiva non può essere impostata ad un valore quando il contesto effettivo è un'istanza di prodotto")
   public static final String INVALID_EFFECTIVE_UNIT_FOR_PI = "40";

   @RBEntry("L'unità effettiva non può essere impostata ad un valore nullo quando il contesto effettivo non è un configuration item di data")
   public static final String INVALID_EFFECTIVE_UNIT_FOR_CI = "41";

   @RBEntry("{0} {1}")
   @RBComment("The configuration item's identity.  Example:  \"Serial Number Configuration Item engine")
   public static final String CONFIGURATIONITEM_IDENTITY = "42";
}
