package wt.effectivity.ixb.handlers.forclasses;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.effectivity.ixb.handlers.forclasses.ixbEffectivityResource")
public final class ixbEffectivityResource extends WTListResourceBundle {

    @RBEntry("Skipping import of the following effectivity since the target object could not be found on the system: \"{0}\"")
    @RBArgComment0("Identifying information about the effectivity")
    public static final String TARGET_OBJECT_NOT_FOUND = "TARGET_OBJECT_NOT_FOUND";
    
    @RBEntry("Skipping import of the following effectivity since the context object could not be found on the system: \"{0}\"")
    @RBArgComment0("Identifying information about the effectivity")
    public static final String CONTEXT_OBJECT_NOT_FOUND = "CONTEXT_OBJECT_NOT_FOUND";
    
    @RBEntry("Skipping import of the following effectivity since it is deleted on the source system: \"{0}\"")
    @RBArgComment0("Identifying information about the effectivity")
    public static final String EFFECTIVITY_HAS_DELETION_REFERENCE = "EFFECTIVITY_HAS_DELETION_REFERENCE";
    
    @RBEntry("The following effectivity was deleted on the source system and should have been skipped for import on this system: \"{0}\"")
    @RBArgComment0("Identifying information about the effectivity")
    public static final String EFFECTIVITY_SHOULD_HAVE_BEEN_SKIPPED = "EFFECTIVITY_SHOULD_HAVE_BEEN_SKIPPED";
    
    @RBEntry("The following effectivity was deleted: \"{0}\"")
    @RBArgComment0("Identifying information about the effectivity")
    public static final String EFFECTIVITY_WAS_DELETED = "EFFECTIVITY_WAS_DELETED";
    
    @RBEntry("The effectivity range settings on this system should match the settings on the source system.")
    public static final String RANGE_SETTINGS_SHOULD_MATCH_SOURCE_SYSTEM = "RANGE_SETTINGS_SHOULD_MATCH_SOURCE_SYSTEM";
    
    @RBEntry("Skipping import of the following effectivity since the effectivity format does not match the system: \"{0}\"")
    @RBArgComment0("Identifying information about the effectivity")
    public static final String FORMAT_DOES_NOT_MATCH_SYSTEM = "FORMAT_DOES_NOT_MATCH_SYSTEM";
    
    @RBEntry("Effectivity UFID")
    @RBComment("The ufid for the effectivity will be displayed after this message. The ufid uniquely identifies the effectivity across multiple systems.")
    public static final String EFFECTIVITY_UFID = "EFFECTIVITY_UFID";
    
    @RBEntry("Target Object UFID")
    @RBComment("The ufid for the target object of the effectivity will be displayed after this message. The ufid uniquely identifies the object across multiple systems.")
    public static final String TARGET_OBJECT_UFID = "TARGET_OBJECT_UFID";
    
    @RBEntry("Effectivity Context UFID")
    @RBComment("The ufid for the context object of the effectivity will be displayed after this message. The ufid uniquely identifies the object across multiple systems.")
    public static final String CONTEXT_OBJECT_UFID = "CONTEXT_OBJECT_UFID";
    
    @RBEntry("Type")
    @RBComment("The type of the effectivity will be displayed after this message.")
    public static final String TYPE = "TYPE";
    
    @RBEntry("Target Object")
    @RBComment("The identity for the target object of the effectivity will be displayed after this message.")
    public static final String TARGET_OBJECT = "TARGET_OBJECT";
    
    @RBEntry("Effectivity Context Number")
    @RBComment("The number for the context object of the effectivity will be displayed after this message.")
    public static final String CONTEXT_OBJECT = "CONTEXT_OBJECT";
    
    @RBEntry("Range")
    @RBComment("The range of the effectivity will be displayed after this message.")
    public static final String RANGE = "RANGE";
    
    @RBEntry("open")
    @RBComment("Needs to be localized.  This will be displayed when an effectivity range has no end value specified. For instance, an effectivity with a range of \"003 - \" will use this entry to display instead as \"003 - <open>\" so that it is easier for the user to see that the effectivity is open ended.")
    public static final String OPEN_RANGE = "OPEN_RANGE";
}
