package wt.effectivity.ixb.handlers.forclasses;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.effectivity.ixb.handlers.forclasses.ixbEffectivityResource")
public final class ixbEffectivityResource_it extends WTListResourceBundle {

    @RBEntry("L'importazione dell'effettività seguente viene ignorata, perché non è possibile trovare l'oggetto di destinazione nel sistema: \"{0}\"")
    @RBArgComment0("Identifying information about the effectivity")
    public static final String TARGET_OBJECT_NOT_FOUND = "TARGET_OBJECT_NOT_FOUND";
    
    @RBEntry("L'importazione dell'effettività seguente viene ignorata, perché non è possibile trovare l'oggetto del contesto nel sistema: \"{0}\"")
    @RBArgComment0("Identifying information about the effectivity")
    public static final String CONTEXT_OBJECT_NOT_FOUND = "CONTEXT_OBJECT_NOT_FOUND";
    
    @RBEntry("L'importazione dell'effettività seguente viene ignorata, perché è eliminata nel sistema di origine: \"{0}\"")
    @RBArgComment0("Identifying information about the effectivity")
    public static final String EFFECTIVITY_HAS_DELETION_REFERENCE = "EFFECTIVITY_HAS_DELETION_REFERENCE";
    
    @RBEntry("L'effettività seguente è stata eliminata nel sistema di origine e avrebbe dovuto essere ignorata per l'importazione nel sistema: \"{0}\"")
    @RBArgComment0("Identifying information about the effectivity")
    public static final String EFFECTIVITY_SHOULD_HAVE_BEEN_SKIPPED = "EFFECTIVITY_SHOULD_HAVE_BEEN_SKIPPED";
    
    @RBEntry("L'effettività seguente è stata eliminata: \"{0}\"")
    @RBArgComment0("Identifying information about the effectivity")
    public static final String EFFECTIVITY_WAS_DELETED = "EFFECTIVITY_WAS_DELETED";
    
    @RBEntry("Le impostazioni dell'intervallo di effettività nel sistema devono corrispondere alle impostazioni nel sistema di origine.")
    public static final String RANGE_SETTINGS_SHOULD_MATCH_SOURCE_SYSTEM = "RANGE_SETTINGS_SHOULD_MATCH_SOURCE_SYSTEM";
    
    @RBEntry("L'importazione dell'effettività seguente viene ignorata, perché il formato dell'effettività non corrisponde al sistema: \"{0}\"")
    @RBArgComment0("Identifying information about the effectivity")
    public static final String FORMAT_DOES_NOT_MATCH_SYSTEM = "FORMAT_DOES_NOT_MATCH_SYSTEM";
    
    @RBEntry("UFID effettività")
    @RBComment("The ufid for the effectivity will be displayed after this message. The ufid uniquely identifies the effectivity across multiple systems.")
    public static final String EFFECTIVITY_UFID = "EFFECTIVITY_UFID";
    
    @RBEntry("UFID oggetto di destinazione")
    @RBComment("The ufid for the target object of the effectivity will be displayed after this message. The ufid uniquely identifies the object across multiple systems.")
    public static final String TARGET_OBJECT_UFID = "TARGET_OBJECT_UFID";
    
    @RBEntry("UFID contesto di effettività")
    @RBComment("The ufid for the context object of the effectivity will be displayed after this message. The ufid uniquely identifies the object across multiple systems.")
    public static final String CONTEXT_OBJECT_UFID = "CONTEXT_OBJECT_UFID";
    
    @RBEntry("Tipo")
    @RBComment("The type of the effectivity will be displayed after this message.")
    public static final String TYPE = "TYPE";
    
    @RBEntry("Oggetto di destinazione")
    @RBComment("The identity for the target object of the effectivity will be displayed after this message.")
    public static final String TARGET_OBJECT = "TARGET_OBJECT";
    
    @RBEntry("Numero contesto effettività")
    @RBComment("The number for the context object of the effectivity will be displayed after this message.")
    public static final String CONTEXT_OBJECT = "CONTEXT_OBJECT";
    
    @RBEntry("Intervallo")
    @RBComment("The range of the effectivity will be displayed after this message.")
    public static final String RANGE = "RANGE";
    
    @RBEntry("aperto")
    @RBComment("Needs to be localized.  This will be displayed when an effectivity range has no end value specified. For instance, an effectivity with a range of \"003 - \" will use this entry to display instead as \"003 - <open>\" so that it is easier for the user to see that the effectivity is open ended.")
    public static final String OPEN_RANGE = "OPEN_RANGE";
}
