/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.configurablelink;

import wt.util.resource.*;

@RBUUID("wt.configurablelink.configurablelinkResource")
public final class configurablelinkResource_it extends WTListResourceBundle {
   @RBEntry("Creato link riferimento configurabile di tipo \"{0}\". Aggiunto \"{1}\" a \"{2}\".")
   @RBComment("{0} is the soft type name of the link and {1} and {2} are the parent and child object identities")
   public static final String LOAD_CONFIGURABLE_REFERENCE_LINK = "0";

   @RBEntry("Impossibile trovare oggetti padre. Atteso un oggetto")
   public static final String NO_ROLE_A_OBJ_FOUND = "1";

   @RBEntry("Trovati più oggetti padre. Atteso un solo oggetto.")
   public static final String MULTIPLE_ROLE_A_OBJS_FOUND = "2";

   @RBEntry("Impossibile trovare l'ID organizzazione specificato nel file di caricamento.")
   public static final String NO_ORG_FOUND = "3";

   @RBEntry("Trovati più valori per l'ID organizzazione \"{0}\" specificato nel file di caricamento. Atteso un solo valore.")
   public static final String MULTIPLE_ORGS_FOUND = "4";

   @RBEntry("Il tipo di link \"{0}\" non è valido.")
   public static final String INVALID_LINKTYPE = "5";

   @RBEntry("Impossibile trovare oggetti figlio. Atteso un oggetto")
   public static final String NO_ROLE_B_OBJ_FOUND = "6";

   @RBEntry("Trovati più oggetti figlio. Atteso un solo oggetto.")
   public static final String MULTIPLE_ROLE_B_OBJS_FOUND = "7";

   @RBEntry("Impossibile trovare un vincolo di relazione per il tipo di link \"{0}\".")
   public static final String NO_ASSOCIATION_CONSTRAINT_FOR_LINKTYPE = "8";

   @RBEntry("Tipo di oggetto padre non valido. Gli oggetti padre devono essere iterati o con master.")
   public static final String INVALID_ROLE_A_TYPE = "9";

   @RBEntry("Tipo di oggetto padre non valido. Gli oggetti padre devono essere iterati.")
   public static final String INVALID_ROLE_A_TYPE_FOR_DESCRIBE_LINK = "10";

   @RBEntry("Tipo di oggetto figlio non valido. Gli oggetti figlio devono essere iterati o con master.")
   public static final String INVALID_ROLE_B_TYPE = "11";

   @RBEntry("Tipo di oggetto figlio non valido. Gli oggetti figlio devono essere iterati.")
   public static final String INVALID_ROLE_B_TYPE_FOR_DESCRIBE_LINK = "12";

   @RBEntry("Il parametro \"{0}\" non può essere nullo.")
   public static final String NULL_PARAMETER = "13";

   @RBEntry("Creato link master configurabile di tipo \"{0}\". Aggiunto \"{1}\" a \"{2}\".")
   @RBComment("{0} is the soft type name of the link and {1} and {2} are the parent and child object identities")
   public static final String LOAD_CONFIGURABLE_MASTERS_LINK = "14";

   @RBEntry("Creato link descrizione configurabile di tipo \"{0}\". Aggiunto \"{1}\" a \"{2}\".")
   @RBComment("{0} is the soft type name of the link and {1} and {2} are the parent and child object identities")
   public static final String LOAD_CONFIGURABLE_DESCRIBE_LINK = "15";

   @RBEntry("\"{0}\" non è un tipo di oggetto con ruolo supportato per i link configurabili.")
   public static final String ROLE_CLASS_NOT_SUPPORTED = "16";

   @RBEntry("Impossibile creare un vincolo di relazione per il tipo di link configurabile con tipo di ruolo \"{0}\" in quanto non è un tipo di oggetto con ruolo supportato per i link configurabili.")
   public static final String INVALID_ASSOCIATION_CONSTRAINT_ROLE_TYPE = "17";

   @RBEntry("Impossibile trovare un vincolo di relazione valido per il tipo di padre \"{0}\", tipo di link \"{1}\" e tipo di figlio \"{2}\".")
   public static final String NO_VALID_ASSOCIATION_CONSTRAINT = "18";

   @RBEntry("Il numero \"{0}\" e l'iterazione \"{1}\" sono stati definiti ma la versione è nulla.  Se si specificano numero e iterazione è necessario specificare anche una versione valida.")
   public static final String VERSION_MUST_BE_SPECIFIED = "19";

   @RBEntry("Saltato: esiste un link duplicato per il tipo di link \"{0}\" con \"{1}\" a \"{2}\".")
   @RBComment("{0} is the soft type name of the link, {1} is the parent object identity and {2} is the child object identity")
   public static final String DUPLICATED_CONFIGURABLE_LINK = "20";

   @RBEntry("Impossibile creare un link per l'oggetto padre in quanto è sottoposto a Check-Out da un altro utente.")
   public static final String ROLE_A_OBJECT_CHECKED_OUT_BY_ANOTHER_USER = "21";
   
   @RBEntry("Impossibile creare un link per l'oggetto padre in quanto è sottoposto a Check-Out e non è la copia in modifica.")
   public static final String ROLE_A_OBJECT_NOT_WORKING_COPY = "22";
   
   @RBEntry("Creato link revisione configurabile di tipo \"{0}\". Aggiunto \"{1}\" a \"{2}\".")
   @RBComment("{0} is the soft type name of the link and {1} and {2} are the parent and child object identities")
   public static final String LOAD_CONFIGURABLE_REVISION_LINK = "23";
   
   @RBEntry("Aggiornato link revisione configurabile di tipo \"{0}\". Link tra \"{1}\" e \"{2}\".")
   @RBComment("{0} is the soft type name of the link and {1} and {2} are the parent and child object identities")
   public static final String UPDATE_LOAD_CONFIGURABLE_REVISION_LINK = "24";
}
