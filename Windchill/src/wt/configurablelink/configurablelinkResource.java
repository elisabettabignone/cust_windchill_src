/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.configurablelink;

import wt.util.resource.*;

@RBUUID("wt.configurablelink.configurablelinkResource")
public final class configurablelinkResource extends WTListResourceBundle {
   @RBEntry("Created configurable reference link with link type \"{0}\". Added \"{1}\" to \"{2}\".")
   @RBComment("{0} is the soft type name of the link and {1} and {2} are the parent and child object identities")
   public static final String LOAD_CONFIGURABLE_REFERENCE_LINK = "0";

   @RBEntry("No parent object found. One expected.")
   public static final String NO_ROLE_A_OBJ_FOUND = "1";

   @RBEntry("Multiple parent objects found. One expected.")
   public static final String MULTIPLE_ROLE_A_OBJS_FOUND = "2";

   @RBEntry("The specified organization ID cannot be found in the load file.")
   public static final String NO_ORG_FOUND = "3";

   @RBEntry("Multiple values found for organization ID \"{0}\" specified in load file. One expected.")
   public static final String MULTIPLE_ORGS_FOUND = "4";

   @RBEntry("\"{0}\" is an invalid link type.")
   public static final String INVALID_LINKTYPE = "5";

   @RBEntry("No child object found. One expected.")
   public static final String NO_ROLE_B_OBJ_FOUND = "6";

   @RBEntry("Multiple child objects found. One expected.")
   public static final String MULTIPLE_ROLE_B_OBJS_FOUND = "7";

   @RBEntry("No relationship constraint found for link type \"{0}\".")
   public static final String NO_ASSOCIATION_CONSTRAINT_FOR_LINKTYPE = "8";

   @RBEntry("Invalid parent object type.  Parent object must be an iterated or mastered object.")
   public static final String INVALID_ROLE_A_TYPE = "9";

   @RBEntry("Invalid parent object type.  Parent object must be an iterated object.")
   public static final String INVALID_ROLE_A_TYPE_FOR_DESCRIBE_LINK = "10";

   @RBEntry("Invalid child object type.  Child object must be an iterated or mastered object.")
   public static final String INVALID_ROLE_B_TYPE = "11";

   @RBEntry("Invalid child object type.  Child object must be an iterated object.")
   public static final String INVALID_ROLE_B_TYPE_FOR_DESCRIBE_LINK = "12";

   @RBEntry("Parameter \"{0}\" cannot be null.")
   public static final String NULL_PARAMETER = "13";

   @RBEntry("Created configurable masters link with link type \"{0}\". Added \"{1}\" to \"{2}\".")
   @RBComment("{0} is the soft type name of the link and {1} and {2} are the parent and child object identities")
   public static final String LOAD_CONFIGURABLE_MASTERS_LINK = "14";

   @RBEntry("Created configurable describe link with link type \"{0}\". Added \"{1}\" to \"{2}\".")
   @RBComment("{0} is the soft type name of the link and {1} and {2} are the parent and child object identities")
   public static final String LOAD_CONFIGURABLE_DESCRIBE_LINK = "15";

   @RBEntry("\"{0}\" is not a supported role object type for configurable links.")
   public static final String ROLE_CLASS_NOT_SUPPORTED = "16";

   @RBEntry("Cannot create relationship constraint for the configurable link type with the role type \"{0}\" because it is not a supported role object type for configurable links.")
   public static final String INVALID_ASSOCIATION_CONSTRAINT_ROLE_TYPE = "17";

   @RBEntry("No valid relationship constraint found for parent type \"{0}\", link type \"{1}\", and child type \"{2}\".")
   public static final String NO_VALID_ASSOCIATION_CONSTRAINT = "18";

   @RBEntry("Number \"{0}\" and iteration \"{1}\" were defined, but the version is null.  A valid version must be specified if number and iteration are specified.")
   public static final String VERSION_MUST_BE_SPECIFIED = "19";

   @RBEntry("Skipped: Duplicate link exists for link type \"{0}\" with \"{1}\" to \"{2}\".")
   @RBComment("{0} is the soft type name of the link, {1} is the parent object identity and {2} is the child object identity")
   public static final String DUPLICATED_CONFIGURABLE_LINK = "20";

   @RBEntry("Unable to create link for this parent object. It is checked out by another user.")
   public static final String ROLE_A_OBJECT_CHECKED_OUT_BY_ANOTHER_USER = "21";
   
   @RBEntry("Unable to create link for this parent object. It is checked out and this is not the working copy.")
   public static final String ROLE_A_OBJECT_NOT_WORKING_COPY = "22";
   
   @RBEntry("Created configurable revision link with link type \"{0}\". Added \"{1}\" to \"{2}\".")
   @RBComment("{0} is the soft type name of the link and {1} and {2} are the parent and child object identities")
   public static final String LOAD_CONFIGURABLE_REVISION_LINK = "23";
   
   @RBEntry("Updated configurable revision link with link type \"{0}\". Link between \"{1}\" and \"{2}\".")
   @RBComment("{0} is the soft type name of the link and {1} and {2} are the parent and child object identities")
   public static final String UPDATE_LOAD_CONFIGURABLE_REVISION_LINK = "24";
}
