/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.occurrence;

import wt.util.resource.*;

@RBUUID("wt.occurrence.occurrenceResource")
public final class occurrenceResource extends WTListResourceBundle {
   @RBEntry("An Occurrence cannot be copied before it is saved")
   public static final String NO_COPY_WHEN_NOT_PERSISTENT = "0";

   @RBEntry("{0} {1} of {2}")
   @RBArgComment0("is a quantity")
   @RBArgComment1("is a unit")
   @RBArgComment2("is and identity")
   public static final String PART_USAGE = "2";

   @RBEntry("Cannot create more than one uses occurrence")
   public static final String USES_OCCURRENCE_LIMIT_IS_1 = "3";

   @RBEntry("Reference Designator(s) are not unique: {0}")
   @RBArgComment0("is a reference designator (a name or label)")
   public static final String REFERENCE_DESIGNATOR_IS_NOT_UNIQUE = "4";
}
