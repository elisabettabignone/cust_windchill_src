/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.aum;

import wt.util.resource.*;

@RBUUID("wt.aum.aumResource")
public final class aumResource_it extends WTListResourceBundle {
   /**
    * Entry  Format (values equal to default value are not included)
    **/
   @RBEntry("L'oggetto dispone già di un'unità di misura alternativa con lo stesso valore di unità.")
   public static final String AUM_ALREADY_EXISTS_ERROR = "001";

   @RBEntry("Un'unità di misura alternativa non può esistere autonomamente ma deve essere associata a un oggetto che possa disporre di unità di misura alternative.")
   public static final String AUM_STANDALONE_ERROR = "002";

   @RBEntry("L'unità di default dell'oggetto deve corrispondere alla quantità fornita.")
   public static final String DEFAULT_UNIT_NOT_SAME = "003";

   @RBEntry("Impossibile trovare un'unità di misura alternativa con l'unità specificata per l'oggetto corrente.")
   public static final String AUM_UNIT_NOT_FOUND = "004";

   @RBEntry("L'unità di default dell'oggetto è uguale all'unità di misura alternativa. Le due unità devono differire.")
   public static final String DEFAULT_UNIT_SAME_AS_AUM_ERROR = "005";

   @RBEntry("L'oggetto dispone già di un'unità di misura alternativa con l'unità {0}.")
   public static final String DEFAULT_UNIT_CANNOT_BE_SET_SAME_AS_AUM = "006";
}
