/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.aum;

import wt.util.resource.*;

@RBUUID("wt.aum.aumResource")
public final class aumResource extends WTListResourceBundle {
   /**
    * Entry  Format (values equal to default value are not included)
    **/
   @RBEntry("An alternate unit of measure with same unit already exists for the object.")
   public static final String AUM_ALREADY_EXISTS_ERROR = "001";

   @RBEntry("An alternate unit of measure cannot be standalone. It must be associated with a object that can have alternate unit of measure.")
   public static final String AUM_STANDALONE_ERROR = "002";

   @RBEntry("The default unit of the object must be the same as the provided quantity.")
   public static final String DEFAULT_UNIT_NOT_SAME = "003";

   @RBEntry("An alternate unit of measure with the specified unit cannot be found for the current object.")
   public static final String AUM_UNIT_NOT_FOUND = "004";

   @RBEntry("The default unit of the object is same as the current alternate unit of measure unit. It is not allowed to be same.")
   public static final String DEFAULT_UNIT_SAME_AS_AUM_ERROR = "005";

   @RBEntry("An Alternate Unit of Measure with unit '{0}' already exists for the current Object.")
   public static final String DEFAULT_UNIT_CANNOT_BE_SET_SAME_AS_AUM = "006";
}
