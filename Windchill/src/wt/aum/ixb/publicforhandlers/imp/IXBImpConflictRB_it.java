/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.aum.ixb.publicforhandlers.imp;

import wt.util.resource.*;

@RBUUID("wt.aum.ixb.publicforhandlers.imp.IXBImpConflictRB")
public final class IXBImpConflictRB_it extends WTListResourceBundle {
   @RBEntry("Numeratore")
   public static final String NUMERATOR = "0";

   @RBEntry("Denominatore")
   public static final String DENOMINATOR = "1";

   @RBEntry("Unità")
   public static final String UNIT = "2";
}
