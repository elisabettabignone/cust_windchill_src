/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.series;

import wt.util.resource.*;

@RBUUID("wt.series.seriesResource")
public final class seriesResource extends WTListResourceBundle {
   @RBEntry("The series value has reached its maximum value: {0}, series={1}")
   @RBArgComment0("Overflow revision ID value")
   @RBArgComment1("The name of the series")
   public static final String OVERFLOW = "0";

   @RBEntry("The series value has reached its minimum value: {0}, series={1}")
   @RBArgComment0("Underflow revision ID value")
   @RBArgComment1("The name of the series")
   public static final String UNDERFLOW = "1";

   @RBEntry("The revision value is not valid within the series: {0}, series={1}")
   @RBArgComment0("Invalid revision ID value")
   @RBArgComment1("The name of the series")
   public static final String INVALID_VALUE = "2";

   @RBEntry("The revision value is not within a valid range for the series: value={0}")
   @RBArgComment0("The out of range value")
   public static final String INVALID_RANGE = "3";

   @RBEntry("The series level has reached its maximum depth.")
   public static final String LEVEL_OVERFLOW = "4";

   @RBEntry("The series level has reached its minimum depth.")
   public static final String LEVEL_UNDERFLOW = "5";

   @RBEntry("The series was unable to be constructed.")
   public static final String BAD_CONSTRUCTION = "6";

   @RBEntry("The XML string is invalid.")
   public static final String INVALID_XML = "7";

   @RBEntry("The series or seed is undefined: {0}")
   public static final String INVALID_SERIES = "8";

   @RBEntry("Welcome to FileBasedSeries install.")
   public static final String SERIES_LOAD_WELCOME = "9";

   @RBEntry("You must login as a member of the Administrator group.")
   public static final String SERIES_LOAD_LOGIN = "10";

   @RBEntry("Successfully loaded file-based series XML file.")
   public static final String SERIES_SUCCESSFUL_LOAD = "11";

   @RBEntry("WARNING: Loading will overwrite existing Series XML.")
   public static final String SERIES_LOAD_WARNING = "12";

   @RBEntry("ERROR: All values within a series need to be unique: series={0}, value={1}")
   @RBArgComment0("The name of the series")
   @RBArgComment1("The duplicated value (string) of the given series")
   public static final String SERIES_DUPLICATE_VALUE_ERROR = "13";

   @RBEntry("ERROR: All seed names within a series need to be unique: series={0}, seed={1}")
   @RBArgComment0("The name of the series")
   @RBArgComment1("The duplicated seed (string) of the given series")
   public static final String SERIES_DUPLICATE_SEED_ERROR = "14";

   @RBEntry("ERROR: All series names need to be unique: {0}")
   @RBArgComment0("The name of the series")
   public static final String SERIES_DUPLICATE_SERIES_ERROR = "15";

   @RBEntry("Unable to instantiate series from SeriesSortValue with uniqueSeriesName = \"{0}\" and value = \"{1}\".")
   public static final String INVALID_SERIES_SORT_VALUE = "16";

   @RBEntry("ERROR: Series names must be non-blank and may not contain the characters: \"{0}\"  series={1}")
   @RBArgComment0("Illegal series name characters")
   @RBArgComment1("Illegal series name")
   public static final String ILLEGAL_SERIES_NAME = "17";

   @RBEntry("ERROR: Seed names must be non-blank and may not contain the characters: \"{0}\"  series={1}, seed={2}")
   @RBArgComment0("Illegal series name characters")
   @RBArgComment1("The name of the series")
   @RBArgComment2("Illegal seed name")
   public static final String ILLEGAL_SEED_NAME = "18";

   @RBEntry("ERROR: Revision ID values must be non-blank: series={0}")
   @RBArgComment0("Name of series with blank values")
   public static final String BLANK_REVISON_ID_VALUE = "19";

   @RBEntry("ERROR: A series must have at least one revision value: {0}")
   @RBArgComment0("Name of the empty series")
   public static final String EMPTY_SERIES = "20";

   @RBEntry("ERROR: A seed must have at least one revision value: series={0}, seed={1}")
   @RBArgComment0("Name of the empty series")
   @RBArgComment1("Name of the empty seed")
   public static final String EMPTY_SEED = "21";

   @RBEntry("The revision value index is not valid within the series: {0}, series={1}")
   @RBArgComment0("Invalid revision ID index")
   @RBArgComment1("The name of the series")
   public static final String INVALID_VALUE_INDEX = "22";

   @RBEntry("File-based series XML file could not be loaded: {0}")
   @RBArgComment0("Name of file containing XML")
   public static final String SERIES_UNSUCCESSFUL_LOAD = "23";

   @RBEntry("The series has one or more errors and can not be used: {0}")
   @RBArgComment0("Name of the series with errors")
   public static final String CANNOT_USE_SERIES = "24";

   @RBEntry("Usage: wt.series.LoadFileBasedSeries <XML File Name> <-load | -read>")
   public static final String SERIES_LOAD_USAGE = "25";

   @RBEntry("Successfully read file-based series XML into file: {0}")
   @RBArgComment0("Name of file containing XML")
   public static final String SERIES_SUCCESSFUL_READ = "26";

   @RBEntry("Could not read file-based series XML into file: {0}")
   @RBArgComment0("Name of file containing XML")
   public static final String SERIES_UNSUCCESSFUL_READ = "27";

   @RBEntry("There is no file-based series XML stored. \nTo load file-based series XML use: wt.series.LoadFileBasedSeries <XML File Name> -load")
   public static final String SERIES_NO_XML = "28";

   @RBEntry("File-based series XML file was not found or is empty: {0}")
   @RBArgComment0("Name of file containing XML")
   public static final String SERIES_FILE_NOT_FOUND = "29";

   @RBEntry("ERROR: Only one revision value can be designated as the initial revision for the series: {0}.")
   @RBArgComment0("Name of the series")
   public static final String SERIES_DUPLICATE_INITIAL_REVISION_IDS = "30";

   @RBEntry("ERROR: For revision {0}, revTo value {1} is undefined.")
   @RBArgComment0("Name of the revision ID")
   @RBArgComment1("Name of the revision ID specified in the revTo field")
   public static final String SERIES_UNDEFINED_REVTO = "31";

   @RBEntry("ERROR: For revision {0}, revTo value {1} must be defined later in the series.")
   @RBArgComment0("Name of the revision ID")
   @RBArgComment1("Name of the revision ID specified in the revTo field")
   public static final String SERIES_BACKWARDS_REVTO = "32";

   @RBEntry("ERROR: The revision ID value may not contain the series delimiter character: \"{0}\"  value={1}, series={2}")
   @RBArgComment0("The series delimiter character")
   @RBArgComment1("Illegal revision ID value")
   @RBArgComment2("The name of the series")
   public static final String ILLEGAL_REVISON_ID_VALUE = "33";

   @RBEntry("ERROR: Illegal revision ID value detected containing the series delimiter character: \"{0}\" value={1}, series={2}")
   @RBArgComment0("The series delimiter character")
   @RBArgComment1("Illegal revision ID value")
   @RBArgComment2("The name of the series")
   public static final String ILLEGAL_REVISON_ID_VALUE_DETECTED = "34";

   @RBEntry("ERROR: The new revision ID value must not contain leading zeros: {0}, series={1}")
   @RBArgComment0("Illegal revision ID value")
   @RBArgComment1("The name of the series")
   public static final String ILLEGAL_LEADING_ZEROS = "35";
   
   @RBEntry("ERROR: The file-based series you are attempting to load is missing a revision ID that is being used in the system: ID={0}, series={1}")
   @RBArgComment0("Missing revision ID value")
   @RBArgComment1("The name of the series")
   public static final String MISSING_USED_REVISION_VALUE = "36";
}
