/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.series;

import wt.util.resource.*;

@RBUUID("wt.series.seriesResource")
public final class seriesResource_it extends WTListResourceBundle {
   @RBEntry("Il valore della serie ha raggiunto il massimo: {0}, serie = {1}")
   @RBArgComment0("Overflow revision ID value")
   @RBArgComment1("The name of the series")
   public static final String OVERFLOW = "0";

   @RBEntry("Il valore della serie ha raggiunto il minimo: {0}, serie = {1}")
   @RBArgComment0("Underflow revision ID value")
   @RBArgComment1("The name of the series")
   public static final String UNDERFLOW = "1";

   @RBEntry("Il valore di revisione non è valido nella serie: valore = {0}, serie = {1} .")
   @RBArgComment0("Invalid revision ID value")
   @RBArgComment1("The name of the series")
   public static final String INVALID_VALUE = "2";

   @RBEntry("Il valore di revisione non rientra in un intervallo valido per la serie: valore =  {0}.")
   @RBArgComment0("The out of range value")
   public static final String INVALID_RANGE = "3";

   @RBEntry("Il livello della serie ha raggiunto la profondità massima.")
   public static final String LEVEL_OVERFLOW = "4";

   @RBEntry("Il livello della serie ha raggiunto la profondità minima.")
   public static final String LEVEL_UNDERFLOW = "5";

   @RBEntry("Impossibile costruire la serie.")
   public static final String BAD_CONSTRUCTION = "6";

   @RBEntry("La stringa XML non è valida.")
   public static final String INVALID_XML = "7";

   @RBEntry("Serie o oggetto origine non definito: {0}.")
   public static final String INVALID_SERIES = "8";

   @RBEntry("Benvenuti nell'installazione di FileBasedSeries.")
   public static final String SERIES_LOAD_WELCOME = "9";

   @RBEntry("È necessario accedere come membro del gruppo Administrators.")
   public static final String SERIES_LOAD_LOGIN = "10";

   @RBEntry("File XML della serie basata su file caricato.")
   public static final String SERIES_SUCCESSFUL_LOAD = "11";

   @RBEntry("AVVERTENZA: il caricamento sovrascriverà i dati XML esistenti per la serie.")
   public static final String SERIES_LOAD_WARNING = "12";

   @RBEntry("ERRORE - tutti i valori in una serie devono essere univoci: serie = {0}, valore  = {1}")
   @RBArgComment0("The name of the series")
   @RBArgComment1("The duplicated value (string) of the given series")
   public static final String SERIES_DUPLICATE_VALUE_ERROR = "13";

   @RBEntry("ERRORE - tutti i nomi di oggetti origine in una serie devono essere univoci: serie = {0}, oggetto origine  = {1}")
   @RBArgComment0("The name of the series")
   @RBArgComment1("The duplicated seed (string) of the given series")
   public static final String SERIES_DUPLICATE_SEED_ERROR = "14";

   @RBEntry("ERRORE - tutti i nomi di serie devono essere univoci: {0}")
   @RBArgComment0("The name of the series")
   public static final String SERIES_DUPLICATE_SERIES_ERROR = "15";

   @RBEntry("Impossibile creare istanza per serie da SeriesSortValue con uniqueSeriesName = \"{0}\" e valore = \"{1}\".")
   public static final String INVALID_SERIES_SORT_VALUE = "16";

   @RBEntry("ERRORE - i nomi di serie non devono essere vuoti e non devono contenere i caratteri: \"{0}\"  serie = {1}")
   @RBArgComment0("Illegal series name characters")
   @RBArgComment1("Illegal series name")
   public static final String ILLEGAL_SERIES_NAME = "17";

   @RBEntry("ERRORE - i nomi di oggetto origine non devono essere vuoti e non devono contenere i caratteri: \"{0}\" serie = {1}, oggetto origine = {2}")
   @RBArgComment0("Illegal series name characters")
   @RBArgComment1("The name of the series")
   @RBArgComment2("Illegal seed name")
   public static final String ILLEGAL_SEED_NAME = "18";

   @RBEntry("ERRORE - i valori ID revisione non devono essere vuoti: serie = {0}")
   @RBArgComment0("Name of series with blank values")
   public static final String BLANK_REVISON_ID_VALUE = "19";

   @RBEntry("ERRORE - una serie deve avere almeno un valore di revisione: {0}")
   @RBArgComment0("Name of the empty series")
   public static final String EMPTY_SERIES = "20";

   @RBEntry("ERRORE - un oggetto origine deve avere almeno un valore di revisione: serie = {0}, oggetto origine = {1}")
   @RBArgComment0("Name of the empty series")
   @RBArgComment1("Name of the empty seed")
   public static final String EMPTY_SEED = "21";

   @RBEntry("L'indice del valore di revisione non è valido nella serie: valore = {0}, serie = {1} .")
   @RBArgComment0("Invalid revision ID index")
   @RBArgComment1("The name of the series")
   public static final String INVALID_VALUE_INDEX = "22";

   @RBEntry("Impossibile caricare il file XML della serie basata su file: {0}")
   @RBArgComment0("Name of file containing XML")
   public static final String SERIES_UNSUCCESSFUL_LOAD = "23";

   @RBEntry("La serie contiene uno o più errori e non può essere usata: {0}")
   @RBArgComment0("Name of the series with errors")
   public static final String CANNOT_USE_SERIES = "24";

   @RBEntry("Uso: wt.series.LoadFileBasedSeries <Nome file XML> <-load | -read>")
   public static final String SERIES_LOAD_USAGE = "25";

   @RBEntry("File XML della serie basata su file trascritto nel file: {0}")
   @RBArgComment0("Name of file containing XML")
   public static final String SERIES_SUCCESSFUL_READ = "26";

   @RBEntry("Impossibile trascrivere il file XML della serie basata su file nel file: {0}")
   @RBArgComment0("Name of file containing XML")
   public static final String SERIES_UNSUCCESSFUL_READ = "27";

   @RBEntry("Non esiste un file XML di serie basata su file in archivio. \nPer caricare un file XML di serie basata su file, usare: wt.series.LoadFileBasedSeries <Nome file XML> -load")
   public static final String SERIES_NO_XML = "28";

   @RBEntry("File XML della serie basata su file inesistente o vuoto: {0}")
   @RBArgComment0("Name of file containing XML")
   public static final String SERIES_FILE_NOT_FOUND = "29";

   @RBEntry("ERRORE - È possibile designare solo un valore di revisione come iniziale per la serie {0}.")
   @RBArgComment0("Name of the series")
   public static final String SERIES_DUPLICATE_INITIAL_REVISION_IDS = "30";

   @RBEntry("ERRORE: valore revTo {1} non definito per la revisione {0}.")
   @RBArgComment0("Name of the revision ID")
   @RBArgComment1("Name of the revision ID specified in the revTo field")
   public static final String SERIES_UNDEFINED_REVTO = "31";

   @RBEntry("ERRORE: il valore revTo {1} deve essere definito più tardi nella serie per la revisione {0}.")
   @RBArgComment0("Name of the revision ID")
   @RBArgComment1("Name of the revision ID specified in the revTo field")
   public static final String SERIES_BACKWARDS_REVTO = "32";

   @RBEntry("ERRORE - il valore ID di revisione non può contenere il carattere di delimitazione serie: \"{0}\" valore = {1}, serie = {2}")
   @RBArgComment0("The series delimiter character")
   @RBArgComment1("Illegal revision ID value")
   @RBArgComment2("The name of the series")
   public static final String ILLEGAL_REVISON_ID_VALUE = "33";

   @RBEntry("ERRORE - rilevato un valore ID di revisione non valido contenente il carattere di delimitazione serie: \"{0}\" valore = {1}, serie = {2}")
   @RBArgComment0("The series delimiter character")
   @RBArgComment1("Illegal revision ID value")
   @RBArgComment2("The name of the series")
   public static final String ILLEGAL_REVISON_ID_VALUE_DETECTED = "34";

   @RBEntry("ERRORE: il valore del nuovo ID di revisione non può essere preceduto da zeri: {0}, serie = {1}")
   @RBArgComment0("Illegal revision ID value")
   @RBArgComment1("The name of the series")
   public static final String ILLEGAL_LEADING_ZEROS = "35";
   
   @RBEntry("ERRORE: nella serie di file che si sta tentando di caricare manca un ID di revisione utilizzato nel sistema: ID={0}, serie={1}")
   @RBArgComment0("Missing revision ID value")
   @RBArgComment1("The name of the series")
   public static final String MISSING_USED_REVISION_VALUE = "36";
}
