/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.esi;

import wt.util.resource.*;

@RBUUID("wt.esi.esiResource")
public final class esiResource_it extends WTListResourceBundle {
   @RBEntry("Il valore per l'argomento \"{0}\" non può essere nullo.")
   public static final String NULL_ARGUMENT = "NULL_ARGUMENT";

   @RBEntry("Uno stabilimento denominato \"{0}\" esiste già per questo materiale ERP.")
   public static final String PLANT_ALREADY_EXISTS = "PLANT_ALREADY_EXISTS";

   @RBEntry("Preferenze dati ERP")
   public static final String PREFERENCES_CSV_TITLE = "PREFERENCES_CSV_TITLE";

   @RBEntry("Regole di inizializzazione oggetto materiale ERP ESI")
   @RBComment("Display title for csv load of ESI ERPMaterial object initialization rules")
   public static final String ESI_ERPMATERIAL_CSV_TITLE = "ESI_ERPMATERIAL_CSV_TITLE";

   @RBEntry("Regole di inizializzazione attributi materiale ERP ESI")
   @RBComment("Display title for csv load of ESI ERPMaterial attributes initialization rules")
   public static final String ESI_ERPMATERIAL_ATTRIBUTES_CSV_TITLE = "ESI_ERPMATERIAL_ATTRIBUTES_CSV_TITLE";

   @RBEntry("Dati ERP")
   public static final String PREFERENCE_ERP_DATA_CATEGORY = "PREFERENCE_ERP_DATA_CATEGORY";

   @RBEntry("Forza il Check-Out dell'iterazione alla creazione o modifica di attributi specifici della vista")
   @RBComment("Preference msg to indicate with Soft Types will have an ERPMaterial created automatically for them")
   public static final String FORCE_CHECK_OUT_ON_ALTER_VIEW_SPECIFIC_ATTRIBUTES_NAME = "FORCE_CHECK_OUT_ON_ALTER_VIEW_SPECIFIC_ATTRIBUTES_NAME";

   @RBEntry("Forza il Check-Out dell'iterazione alla creazione o modifica di attributi specifici della vista.")
   @RBComment("Msg to explain the preference")
   public static final String FORCE_CHECK_OUT_ON_ALTER_VIEW_SPECIFIC_ATTRIBUTES_DESC_SHORT = "FORCE_CHECK_OUT_ON_ALTER_VIEW_SPECIFIC_ATTRIBUTES_DESC_SHORT";

   @RBEntry("Forza il Check-Out dell'iterazione alla creazione o modifica di attributi specifici della vista.")
   @RBComment("Long msg to explain the preference")
   public static final String FORCE_CHECK_OUT_ON_ALTER_VIEW_SPECIFIC_ATTRIBUTES_DESC_LONG = "FORCE_CHECK_OUT_ON_ALTER_VIEW_SPECIFIC_ATTRIBUTES_DESC_LONG";

   @RBEntry("Tipi soft per creazione automatica materiale ERP")
   @RBComment("Preference msg to indicate with Soft Types will have an ERPMaterial created automatically for them")
   public static final String SOFT_TYPES_FOR_AUTOMATIC_ERPMATERIAL_CREATION_NAME = "SOFT_TYPES_FOR_AUTOMATIC_ERPMATERIAL_CREATION_NAME";

   @RBEntry("Imposta un elenco di stringhe separate da virgole, ciascuna delle quali definisce un tipo soft della parte. Un materiale ERP viene creato automaticamente alla creazione di queste parti.")
   @RBComment("Msg to explain the preference")
   public static final String SOFT_TYPES_FOR_AUTOMATIC_ERPMATERIAL_CREATION_DESC_SHORT = "SOFT_TYPES_FOR_AUTOMATIC_ERPMATERIAL_CREATION_DESC_SHORT";

   @RBEntry("Imposta un elenco di stringhe separate da virgole, ciascuna delle quali definisce un tipo soft della parte. Un materiale ERP viene creato automaticamente alla creazione di queste parti.")
   @RBComment("Long msg to explain the preference")
   public static final String SOFT_TYPES_FOR_AUTOMATIC_ERPMATERIAL_CREATION_DESC_LONG = "SOFT_TYPES_FOR_AUTOMATIC_ERPMATERIAL_CREATION_DESC_LONG";
   
   @RBEntry("Documento associato (materiale ERP)")
   public static final String COLLECTED_AS_ERP_MATERIAL_ASSOCIATED_WTDOC = "COLLECTED_AS_ERP_MATERIAL_ASSOCIATED_WTDOC";
}
