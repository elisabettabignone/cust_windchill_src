/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.esi;

import wt.util.resource.*;

@RBUUID("wt.esi.esiResource")
public final class esiResource extends WTListResourceBundle {
   @RBEntry("The value for the argument \"{0}\" cannot be null.")
   public static final String NULL_ARGUMENT = "NULL_ARGUMENT";

   @RBEntry("A plant named \"{0}\" already exists for this ERP material.")
   public static final String PLANT_ALREADY_EXISTS = "PLANT_ALREADY_EXISTS";

   @RBEntry("ERP Data Preferences")
   public static final String PREFERENCES_CSV_TITLE = "PREFERENCES_CSV_TITLE";

   @RBEntry("ESI ERP Material Object Initialization Rules")
   @RBComment("Display title for csv load of ESI ERPMaterial object initialization rules")
   public static final String ESI_ERPMATERIAL_CSV_TITLE = "ESI_ERPMATERIAL_CSV_TITLE";

   @RBEntry("ESI ERP Material Attributes Initialization Rules")
   @RBComment("Display title for csv load of ESI ERPMaterial attributes initialization rules")
   public static final String ESI_ERPMATERIAL_ATTRIBUTES_CSV_TITLE = "ESI_ERPMATERIAL_ATTRIBUTES_CSV_TITLE";

   @RBEntry("ERP Data")
   public static final String PREFERENCE_ERP_DATA_CATEGORY = "PREFERENCE_ERP_DATA_CATEGORY";

   @RBEntry("Force check out iteration when creating or modifying View Specific attributes")
   @RBComment("Preference msg to indicate with Soft Types will have an ERPMaterial created automatically for them")
   public static final String FORCE_CHECK_OUT_ON_ALTER_VIEW_SPECIFIC_ATTRIBUTES_NAME = "FORCE_CHECK_OUT_ON_ALTER_VIEW_SPECIFIC_ATTRIBUTES_NAME";

   @RBEntry("Force check out iteration when creating or modifying View Specific attributes.")
   @RBComment("Msg to explain the preference")
   public static final String FORCE_CHECK_OUT_ON_ALTER_VIEW_SPECIFIC_ATTRIBUTES_DESC_SHORT = "FORCE_CHECK_OUT_ON_ALTER_VIEW_SPECIFIC_ATTRIBUTES_DESC_SHORT";

   @RBEntry("Force check out iteration when creating or modifying View Specific attributes.")
   @RBComment("Long msg to explain the preference")
   public static final String FORCE_CHECK_OUT_ON_ALTER_VIEW_SPECIFIC_ATTRIBUTES_DESC_LONG = "FORCE_CHECK_OUT_ON_ALTER_VIEW_SPECIFIC_ATTRIBUTES_DESC_LONG";

   @RBEntry("Soft Types for Automatic ERP Material Creation")
   @RBComment("Preference msg to indicate with Soft Types will have an ERPMaterial created automatically for them")
   public static final String SOFT_TYPES_FOR_AUTOMATIC_ERPMATERIAL_CREATION_NAME = "SOFT_TYPES_FOR_AUTOMATIC_ERPMATERIAL_CREATION_NAME";

   @RBEntry("Sets a list of comma-separated strings, each defining a soft type of part. An ERP material is created automatically upon creation of these parts.")
   @RBComment("Msg to explain the preference")
   public static final String SOFT_TYPES_FOR_AUTOMATIC_ERPMATERIAL_CREATION_DESC_SHORT = "SOFT_TYPES_FOR_AUTOMATIC_ERPMATERIAL_CREATION_DESC_SHORT";

   @RBEntry("Sets a list of comma-separated strings, each defining a soft type of part. An ERP material is created automatically upon creation of these parts.")
   @RBComment("Long msg to explain the preference")
   public static final String SOFT_TYPES_FOR_AUTOMATIC_ERPMATERIAL_CREATION_DESC_LONG = "SOFT_TYPES_FOR_AUTOMATIC_ERPMATERIAL_CREATION_DESC_LONG";
   
   @RBEntry("Associated Document (ERP Material)")
   public static final String COLLECTED_AS_ERP_MATERIAL_ASSOCIATED_WTDOC = "COLLECTED_AS_ERP_MATERIAL_ASSOCIATED_WTDOC";
}
