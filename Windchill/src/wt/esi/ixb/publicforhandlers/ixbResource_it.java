/* bcwti
 *
 * Copyright (c) 2011 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.esi.ixb.publicforhandlers;

import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.esi.ixb.publicforhandlers.ixbResource")
public final class ixbResource_it extends WTListResourceBundle {
    @RBEntry("Gli oggetti che seguono non sono stati importati. I tipi di oggetti non sono supportati per l'importazione delle consegne ricevute nel sistema Windchill: {0}.")
    public static final String IMPORTABLE_PACKAGES_NOT_SUPPORTED_MESSAGE = "IMPORTABLE_PACKAGES_NOT_SUPPORTED_MESSAGE";
}
