/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.esi.ixb.publicforhandlers.imp;

import wt.util.resource.*;

@RBUUID("wt.esi.ixb.publicforhandlers.imp.ESIIXBImpConflictRB")
public final class ESIIXBImpConflictRB extends WTListResourceBundle {
   @RBEntry("ERPMaterial not found.")
   public static final String ERP_MATERIAL_NOT_FOUND = "0";

   @RBEntry("ERPMaterialSet not found.")
   public static final String ERP_MATERIAL_SET_NOT_FOUND = "1";

   @RBEntry("ERPPartSpecificPlantData not found.")
   public static final String ERP_PART_SPECIFIC_PLANT_DATA_NOT_FOUND = "2";

   @RBEntry("ERPMaterial Set Object Id")
   public static final String ERP_MATERIAL_SET_OID = "3";

   @RBEntry("Revision Label")
   public static final String REVISION_LABEL = "4";

   @RBEntry("Storage Location")
   public static final String STORAGE_LOCATION = "5";
}
