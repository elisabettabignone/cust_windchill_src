/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.esi.ixb.publicforhandlers.imp;

import wt.util.resource.*;

@RBUUID("wt.esi.ixb.publicforhandlers.imp.ESIIXBImpConflictRB")
public final class ESIIXBImpConflictRB_it extends WTListResourceBundle {
   @RBEntry("Impossibile trovare ERPMaterial.")
   public static final String ERP_MATERIAL_NOT_FOUND = "0";

   @RBEntry("Impossibile trovare ERPMaterialSet.")
   public static final String ERP_MATERIAL_SET_NOT_FOUND = "1";

   @RBEntry("Impossibile trovare ERPPartSpecificPlantData.")
   public static final String ERP_PART_SPECIFIC_PLANT_DATA_NOT_FOUND = "2";

   @RBEntry("ID oggetto insieme ERPMaterial")
   public static final String ERP_MATERIAL_SET_OID = "3";

   @RBEntry("Etichetta di revisione")
   public static final String REVISION_LABEL = "4";

   @RBEntry("Posizione di archiviazione")
   public static final String STORAGE_LOCATION = "5";
}
