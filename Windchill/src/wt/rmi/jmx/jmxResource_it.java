package wt.rmi.jmx;

import wt.util.resource.*;

@RBUUID("wt.rmi.jmx.jmxResource")
public final class jmxResource_it extends WTListResourceBundle {
   @RBEntry("Inizio: {0,date} {0,time,full}")
   @RBArgComment0("Start of time period (as localized date)")
   public static final String START_TIME_MSG = "1";

   @RBEntry("Fine: {0,date} {0,time,full}")
   @RBArgComment0("End of time period (as localized date)")
   public static final String END_TIME_MSG = "2";

   @RBEntry("Secondi trascorsi: {0,number,0.0#####}")
   @RBArgComment0("Number of elapsed seconds")
   public static final String ELAPSED_SECONDS_MSG = "3";

   @RBEntry("Statistiche globali consolidate")
   public static final String OVERALL_AGG_STATS = "4";

   @RBEntry("Chiamate completate: {0,number}")
   @RBArgComment0("Number of completed RMI calls")
   public static final String COMPL_CALLS_MSG = "5";

   @RBEntry("Secondi per CPU (totale): {0,number,0.0#####}")
   @RBArgComment0("Total number of CPU seconds")
   public static final String TOTAL_CPU_SECONDS_MSG = "6";

   @RBEntry("Secondi per utente (totale): {0,number,0.0#####}")
   @RBArgComment0("Total number of user seconds")
   public static final String TOTAL_USER_SECONDS_MSG = "7";

   @RBEntry("Secondi per chiamata (totale): {0,number,0.0#####}")
   @RBArgComment0("Total number of RMI call seconds")
   public static final String TOTAL_CALL_SECONDS_MSG = "8";

   @RBEntry("Secondi per chiamata (max): {0,number,0.0#####}")
   @RBArgComment0("Length of longest RMI call in seconds")
   public static final String MAX_CALL_SECONDS_MSG = "9";

   @RBEntry("Secondi per CPU (media): {0,number,0.0#####}")
   @RBArgComment0("Average CPU seconds")
   public static final String AVG_CPU_SECONDS_MSG = "10";

   @RBEntry("Secondi per utente (media): {0,number,0.0#####}")
   @RBArgComment0("Average user seconds")
   public static final String AVG_USER_SECONDS_MSG = "11";

   @RBEntry("Secondi per chiamata (media): {0,number,0.0#####}")
   @RBArgComment0("Average length of RMI call in seconds")
   public static final String AVG_CALL_SECONDS_MSG = "12";

   @RBEntry("Chiamate RMI completate")
   public static final String COMPLETED_CALLS_ITEM_DESCR = "13";

   @RBEntry("Secondi per CPU (totale)")
   public static final String TOTAL_CPU_SECONDS_ITEM_DESCR = "14";

   @RBEntry("Secondi per utente (totale)")
   public static final String TOTAL_USER_SECONDS_ITEM_DESCR = "15";

   @RBEntry("Secondi per chiamata (totale)")
   public static final String TOTAL_CALL_SECONDS_ITEM_DESCR = "16";

   @RBEntry("Durata massima chiamate (in secondi)")
   public static final String MAX_CALL_SECONDS_ITEM_DESCR = "17";

   @RBEntry("Secondi per CPU (media)")
   public static final String AVG_CPU_SECONDS_ITEM_DESCR = "18";

   @RBEntry("Secondi per utente (media)")
   public static final String AVG_USER_SECONDS_ITEM_DESCR = "19";

   @RBEntry("Durata media chiamate (in secondi)")
   public static final String AVG_CALL_SECONDS_ITEM_DESCR = "20";

   @RBEntry("Statistiche globali consolidate per richieste RMI in arrivo")
   public static final String OVERALL_DATA_DESCR = "21";

   @RBEntry("Classe di destinazione della chiamata RMI")
   public static final String TARGET_CLASS_ITEM_DESCR = "22";

   @RBEntry("Metodo di destinazione della chiamata RMI")
   public static final String TARGET_METHOD_ITEM_DESCR = "23";

   @RBEntry("Dati per raggruppamento in istogramma unico")
   public static final String HISTO_BUCKET_DATA_TYPE_DESCR = "24";

   @RBEntry("Dati istogramma per richiesta RMI in arrivo")
   public static final String HISTOGRAM_DATA_DESCR = "25";

   @RBEntry("Dati statistici sulle chiamate RMI in arrivo")
   public static final String STATS_DATA_TYPE_DESCR = "26";

   @RBEntry("Dati consolidati non elaborati delle chiamate RMI in arrivo")
   public static final String RAW_TOTALS_TYPE_DESCR = "27";

   @RBEntry("I dati relativi al tempo sono stati acquisiti misurando i millisecondi trascorsi dal 1 gennaio 1970, 00:00:00 ora di Greenwich (come in java.util.Date)")
   public static final String TIME_MILLIS_ITEM_DESCR = "28";

   @RBEntry("I dati relativi al tempo sono stati acquisiti in nanosecondi (in base a System.nanoTime())")
   public static final String TIME_NANOS_ITEM_DESCR = "29";

   @RBEntry("Tempo totale CPU (in nanosecondi)")
   public static final String TOTAL_CPU_NANOS_ITEM_DESCR = "30";

   @RBEntry("Tempo totale utente (in nanosecondi)")
   public static final String TOTAL_USER_NANOS_ITEM_DESCR = "31";

   @RBEntry("Tempo totale chiamate (in nanosecondi)")
   public static final String TOTAL_CALL_NANOS_ITEM_DESCR = "32";

   @RBEntry("Totale secondi in stato BLOCKED: {0}")
   @RBArgComment0("Total number of seconds spent in BLOCKED state")
   public static final String TOTAL_BLOCKED_SECONDS_MSG = "33";

   @RBEntry("Totale secondi in stato WAITING o TIMED_WAITING: {0}")
   @RBArgComment0("Total number of seconds spent in WAITING or TIMED_WAITING state")
   public static final String TOTAL_WAITED_SECONDS_MSG = "34";

   @RBEntry("Totale dei secondi trascorsi in stato BLOCKED")
   public static final String TOTAL_BLOCKED_SECONDS_ITEM_DESCR = "35";

   @RBEntry("Totale dei secondi trascorsi in stato WAITING")
   public static final String TOTAL_WAITED_SECONDS_ITEM_DESCR = "36";

   @RBEntry("Tempo totale trascorso in stato BLOCKED (in millisecondi)")
   public static final String TOTAL_BLOCKED_MILLIS_ITEM_DESCR = "37";

   @RBEntry("Tempo totale trascorso in stato WAITING o TIMED_WAITING (in millisecondi)")
   public static final String TOTAL_WAITED_MILLIS_ITEM_DESCR = "38";
}
