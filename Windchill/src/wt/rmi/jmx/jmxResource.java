package wt.rmi.jmx;

import wt.util.resource.*;

@RBUUID("wt.rmi.jmx.jmxResource")
public final class jmxResource extends WTListResourceBundle {
   @RBEntry("Start Time: {0,date} {0,time,full}")
   @RBArgComment0("Start of time period (as localized date)")
   public static final String START_TIME_MSG = "1";

   @RBEntry("End Time: {0,date} {0,time,full}")
   @RBArgComment0("End of time period (as localized date)")
   public static final String END_TIME_MSG = "2";

   @RBEntry("Elapsed Seconds: {0,number,0.0#####}")
   @RBArgComment0("Number of elapsed seconds")
   public static final String ELAPSED_SECONDS_MSG = "3";

   @RBEntry("Overall Aggregate Statistics")
   public static final String OVERALL_AGG_STATS = "4";

   @RBEntry("Completed Calls: {0,number}")
   @RBArgComment0("Number of completed RMI calls")
   public static final String COMPL_CALLS_MSG = "5";

   @RBEntry("Total CPU Seconds: {0,number,0.0#####}")
   @RBArgComment0("Total number of CPU seconds")
   public static final String TOTAL_CPU_SECONDS_MSG = "6";

   @RBEntry("Total User Seconds: {0,number,0.0#####}")
   @RBArgComment0("Total number of user seconds")
   public static final String TOTAL_USER_SECONDS_MSG = "7";

   @RBEntry("Total Call Seconds: {0,number,0.0#####}")
   @RBArgComment0("Total number of RMI call seconds")
   public static final String TOTAL_CALL_SECONDS_MSG = "8";

   @RBEntry("Maximum Call Seconds: {0,number,0.0#####}")
   @RBArgComment0("Length of longest RMI call in seconds")
   public static final String MAX_CALL_SECONDS_MSG = "9";

   @RBEntry("Average CPU Seconds: {0,number,0.0#####}")
   @RBArgComment0("Average CPU seconds")
   public static final String AVG_CPU_SECONDS_MSG = "10";

   @RBEntry("Average User Seconds: {0,number,0.0#####}")
   @RBArgComment0("Average user seconds")
   public static final String AVG_USER_SECONDS_MSG = "11";

   @RBEntry("Average Call Seconds: {0,number,0.0#####}")
   @RBArgComment0("Average length of RMI call in seconds")
   public static final String AVG_CALL_SECONDS_MSG = "12";

   @RBEntry("Completed RMI calls")
   public static final String COMPLETED_CALLS_ITEM_DESCR = "13";

   @RBEntry("Total CPU seconds")
   public static final String TOTAL_CPU_SECONDS_ITEM_DESCR = "14";

   @RBEntry("Total user seconds")
   public static final String TOTAL_USER_SECONDS_ITEM_DESCR = "15";

   @RBEntry("Total call seconds")
   public static final String TOTAL_CALL_SECONDS_ITEM_DESCR = "16";

   @RBEntry("Maximum call duration (in seconds)")
   public static final String MAX_CALL_SECONDS_ITEM_DESCR = "17";

   @RBEntry("Average CPU seconds")
   public static final String AVG_CPU_SECONDS_ITEM_DESCR = "18";

   @RBEntry("Average user seconds")
   public static final String AVG_USER_SECONDS_ITEM_DESCR = "19";

   @RBEntry("Average call duration (in seconds)")
   public static final String AVG_CALL_SECONDS_ITEM_DESCR = "20";

   @RBEntry("Overall aggregate statistics for incoming RMI requests")
   public static final String OVERALL_DATA_DESCR = "21";

   @RBEntry("Target class of RMI call")
   public static final String TARGET_CLASS_ITEM_DESCR = "22";

   @RBEntry("Target method of RMI call")
   public static final String TARGET_METHOD_ITEM_DESCR = "23";

   @RBEntry("Data for one histogram grouping")
   public static final String HISTO_BUCKET_DATA_TYPE_DESCR = "24";

   @RBEntry("Histogram data for incoming RMI calls")
   public static final String HISTOGRAM_DATA_DESCR = "25";

   @RBEntry("Incoming RMI call statistics data")
   public static final String STATS_DATA_TYPE_DESCR = "26";

   @RBEntry("Raw aggregate data for incoming RMI calls")
   public static final String RAW_TOTALS_TYPE_DESCR = "27";

   @RBEntry("Time data was acquired measured as number of milliseconds since January 1, 1970, 00:00:00 GMT (as in java.util.Date)")
   public static final String TIME_MILLIS_ITEM_DESCR = "28";

   @RBEntry("Time data was acquired measured in nanoseconds (as per System.nanoTime())")
   public static final String TIME_NANOS_ITEM_DESCR = "29";

   @RBEntry("Total CPU time (in nanoseconds)")
   public static final String TOTAL_CPU_NANOS_ITEM_DESCR = "30";

   @RBEntry("Total user time (in nanoseconds)")
   public static final String TOTAL_USER_NANOS_ITEM_DESCR = "31";

   @RBEntry("Total call time (in nanoseconds)")
   public static final String TOTAL_CALL_NANOS_ITEM_DESCR = "32";

   @RBEntry("Total BLOCKED Seconds: {0}")
   @RBArgComment0("Total number of seconds spent in BLOCKED state")
   public static final String TOTAL_BLOCKED_SECONDS_MSG = "33";

   @RBEntry("Total WAITING and TIMED_WAITING Seconds: {0}")
   @RBArgComment0("Total number of seconds spent in WAITING or TIMED_WAITING state")
   public static final String TOTAL_WAITED_SECONDS_MSG = "34";

   @RBEntry("Total seconds spent in BLOCKED state")
   public static final String TOTAL_BLOCKED_SECONDS_ITEM_DESCR = "35";

   @RBEntry("Total seconds spent in WAITING state")
   public static final String TOTAL_WAITED_SECONDS_ITEM_DESCR = "36";

   @RBEntry("Total time spent in BLOCKED state (in milliseconds)")
   public static final String TOTAL_BLOCKED_MILLIS_ITEM_DESCR = "37";

   @RBEntry("Total time spent in WAITING or TIMED_WAITING state (in milliseconds)")
   public static final String TOTAL_WAITED_MILLIS_ITEM_DESCR = "38";
}
