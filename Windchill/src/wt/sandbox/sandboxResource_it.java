/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.sandbox;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.sandbox.sandboxResource")
public final class sandboxResource_it extends WTListResourceBundle {
   @RBEntry("Non è possibile eliminare elementi di cui è stato effettuato il Check-Out in un progetto da PDM. Annullare il Check-Out degli elementi per rimuoverli dal progetto.")
   public static final String CANNOT_DELETE_SANDBOX_WORKING_COPY = "0";

   @RBEntry("Impossibile effettuare il Check-Out nei progetti di elementi attualmente sottoposti a Check-Out da parte di singoli utenti.")
   public static final String CANNOT_CHECK_OUT_WIP_CHECKEDOUT_OBJECT = "3";

    @RBEntry("Impossibile effettuare il Check-Out nei progetti di elementi attualmente sottoposti a Check-Out da parte di singoli utenti all'interno del prodotto o della libreria.")
    public static final String CANNOT_SB_CHECKOUT_WHILE_WIP_CHECKED_OUT = "CANNOT_SB_CHECKOUT_WHILE_WIP_CHECKED_OUT";

   @RBEntry("Impossibile effettuare il Check-Out nei progetti di elementi attualmente sottoposti a Check-Out in altri progetti.")
   public static final String OBJECT_CHECKED_OUT_TO_ANOTHER_SANDBOX = "4";

   @RBEntry("Nella richiesta di Check-In sono stati trovati dei conflitti di vincoli di univocità.")
   public static final String UNIQUENESS_CONFLICTS_FOUND_IN_CHECK_IN = "10";

   @RBEntry("Il Check-Out degli elementi nel progetto specificato è stato già effettuato.")
   public static final String SANDBOX_VERSION_ALREADY_CHECKED_OUT = "12";

   @RBEntry("Gli elementi sono stati sostituiti nel progetto specificato.")
   public static final String  SANDBOX_VERSION_SUPERSEDED = "12_1";

   @RBEntry("Il Check-Out di altre versioni degli elementi nel progetto specificato è stato già effettuato.")
   public static final String OTHER_VERSION_CHECKED_OUT_TO_SANDBOX = "13";

   @RBEntry("La baseline del progetto non può essere spostata o sostituita.")
   public static final String SANDBOX_BASELINE_CONTAINER_IS_IMMUTABLE = "14";

   @RBEntry("La specifica di configurazione di sandbox (SandboxConfigSpec) non può essere applicata a '{0}'; deve implementare sia Workable che WTContained.")
   public static final String SANDBOX_CONFIG_SPEC_REQUIRES_CONTAINED_WORKABLE = "15";

   @RBEntry("L'elemento '{0}' si trova in un progetto. Impossibile effettuarne il Check-In in uno schedario personale.")
   public static final String SANDBOX_CHECKIN_TO_PERSONAL_CABINET_NOT_ALLOWED = "16";

   @RBEntry("Impossibile effettuare il Check-Out nei progetti di elementi che si trovano in schedari personali.")
   public static final String CANNOT_SB_CHECK_OUT_PERSONAL_CABINET_ENTRY = "17";

   @RBEntry("Impossibile eliminare progetti che contengono oggetti in stato di Check-Out.")
   public static final String CANNOT_DELETE_CONTAINER_WITH_WORKING_COPIES = "18";

   @RBEntry("Sono state rilevate più condizioni di errore.")
   public static final String MULTIPLE_CAUSES = "20";

   @RBEntry("È possibile sottoporre a Check-Out nei progetti solo le iterazioni più recenti.")
   public static final String CANNOT_SB_CHECKOUT_NON_LATEST_ITERATIONS = "21";

   @RBEntry("Non si dispone dei privilegi necessari a modificare gli oggetti.")
   public static final String USER_HAS_NO_MODIFY_PRIVILEGES = "22";

    @RBEntry("Impossibile effettuare il Check-Out di oggetti attualmente bloccati da un singolo utente.")
   public static final String CANNOT_CHECK_OUT_LOCKED_OBJECTS = "23";

   @RBEntry("Impossibile annullare il Check-Out per gli oggetti richiesti: non risultano correttamente in Check-Out.")
   public static final String CANNOT_UNDO_SB_CHECKOUT_INVALID_DATA_STATE = "24";

   @RBEntry("È possibile effettuare l'annullamento del Check-Out solo per l'iterazione più recente.")
   public static final String CANNOT_UNDO_SB_CHECKOUT_NON_LATEST_ITERATIONS = "25";

   @RBEntry("Impossibile annullare il Check-Out per gli elementi di cui i singoli utenti hanno effettuato il Check-Out come copia di lavoro dall'interno del progetto.")
   public static final String CANNOT_UNDO_SB_CHECKOUT_WHILE_WIP = "26";

   @RBEntry("Impossibile annullare il Check-Out per gli elementi di cui non sia stato effettuato il Check-Out in un progetto.")
   public static final String CANNOT_UNDO_SB_CHECKOUT_NOT_CHECKED_OUT = "27";

   @RBEntry("L'utente non può sbloccare alcuni oggetti.")
   public static final String USER_CANNOT_UNLOCK_OBJECTS = "28";

   @RBEntry("L'operazione di invio a PDM può essere effettuata solo sull'iterazione più recente.")
   public static final String CANNOT_SB_CHECKIN_WITH_NON_LATEST_ITERATIONS = "29";

   @RBEntry("Impossibile inviare a PDM oggetti sottoposti a Check-Out da utenti singoli all'interno del progetto.")
   public static final String CANNOT_SB_CHECKIN_WHILE_WIP_CHECKED_OUT = "30";

   @RBEntry("Impossibile effettuare l'invio a PDM di oggetti con stato del ciclo di vita Inviato a PDM, Abbandonato o Obsoleto.")
   public static final String CANNOT_SB_CHECKIN_TERMINAL_OBJ = "31";

   @RBEntry("È possibile effettuare l'invio a PDM di oggetti solo dall'interno di progetti.")
   public static final String CANNOT_SB_CHECKIN_OBJ_NOT_IN_PROJECT = "32";

   @RBEntry("Impossibile inviare a PDM oggetti per cui si sia già effettuato l'invio e di cui non sia stato effettuato successivamente il Check-Out in un progetto.")
   public static final String CANNOT_SB_CHECKIN_OBJ_NOT_CHECKED_OUT = "33";

   @RBEntry("Impossibile effettuare l'invio a PDM degli oggetti creati nel progetto senza i dati iniziali di Check-In.")
   public static final String CANNOT_SB_CHECKIN_WITHOUT_INITIAL_CHECKIN_DATA = "34";

   @RBEntry("Impossibile sottoporre a Check-Out più versioni di un oggetto in uno stesso progetto simultaneamente.")
   public static final String MULTI_VERSION_SB_CHECKOUT = "35";

   @RBEntry("Gli elementi che seguono non possono essere eliminati permanentemente in quanto sono condivisi in progetti e sono contrassegnati come le iterazioni che gli utenti possono aggiungere ai workspace per tali progetti")
   public static final String CANNOT_DELETE_SANDBOXBASELINE_MEMBERS = "36";

   @RBEntry("Il sistema non è configurato per consentire il Check-Out attraverso i limiti di organizzazione.")
   public static final String DIFFERENT_NAMESPACES = "37";

    @RBEntry("Impossibile eliminare l'oggetto {0}. L'oggetto è sottoposto a Check-Out in uno o più progetti.")
   public static final String CANNOT_DELETE_SB_CHECKOUT = "38";

   @RBEntry("ATTENZIONE: azione protetta. Non si dispone dell'autorizzazione necessaria per effettuare il Check-Out.")
   public static final String NO_ACCESS_TO_CHECKOUT_OPERATION = "39";

    @RBEntry("ATTENZIONE: azione protetta. Non si dispone del permesso necessario per inviare i seguenti elementi a PDM.")
   public static final String NO_ACCESS_TO_CHECKIN_OPERATION = "40";

   @RBEntry("ATTENZIONE: azione protetta. Non si dispone dell'autorizzazione necessaria per annullare il Check-Out PDM.")
   public static final String NO_ACCESS_TO_UNDO_CHECKOUT_OPERATION = "41";

   @RBEntry("ATTENZIONE: azione protetta. Non si dispone dei permessi per aggiornare il progetto {0}.")
   @RBComment("Text of the message of the exception thrown when an attempt is made to refresh a project to which user doesn't have sufficient permissions.")
   public static final String NO_PERMISSIONS_TO_REFRESH = "50";

   @RBEntry("Impossibile sottoporre a Check-Out gli oggetti poiché uno o più oggetti sono di tipo non consentito per il Check-Out.")
   @RBComment("One or more selected objects are of types that don't support PDM Checkout.")
   public static final String WRONG_OBJECT_TYPE = "51";

   @RBEntry("ATTENZIONE: l'elemento {0} deve essere già sottoposto a Check-Out perché sia possibile sottoporlo a Check-In.")
   @RBArgComment0("Identity of invalid element.")
   public static final String INVALID_CHECKIN_ATTEMPT_NOT_CHECKED_OUT = "52";

   @RBEntry("ATTENZIONE: uno o più oggetti sono di tipo non consentito per questa azione.")
   public static final String INVALID_OBJECT_TYPE = "53";

   @RBEntry("Impossibile inviare l'oggetto a PDM in caso di associazione a oggetto con stato del ciclo di vita Abbandonato o Obsoleto.")
   public static final String CANNOT_SB_CHECKIN_OBJ_ASSOC_TO_SUPERSEDED_OR_ABANDONED = "54";

   /**
    * fix SPR 1863512,  need a meaningful error message on invoking the Send to PDM action on two one-off versions of the same object.
    **/
   @RBEntry("Impossibile inviare più versioni di variante dello stesso oggetto a PDM")
   public static final String CANNOT_SB_CHECKIN_MULTIPLE_ONEOFF_VERSIONS_OF_SAME_OBJECT = "56";

    @RBEntry("L'oggetto è già condiviso nel progetto ma l'iterazione utilizzata come baseline non è la più recente. Per effettuare il Check-Out dell'oggetto è prima necessario eseguire l'aggiornamento all'iterazione più recente.")
    @RBComment("Error message when user wants to PDM checkout an object when an earlier iteration of the object has already been shared to the same project")
    public static final String CANNOT_CO_OUT_OF_DATE_SHARE = "CANNOT_CO_OUT_OF_DATE_SHARE";

}
