/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.sandbox;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.sandbox.sandboxResource")
public final class sandboxResource extends WTListResourceBundle {
   @RBEntry("Cannot delete items that are currently checked out to a project from PDM. Please undo these check-outs instead.")
   public static final String CANNOT_DELETE_SANDBOX_WORKING_COPY = "0";

   @RBEntry("Cannot check out items to projects that are currently checked out to individual users.")
   public static final String CANNOT_CHECK_OUT_WIP_CHECKEDOUT_OBJECT = "3";

    @RBEntry("Cannot check out items to projects while they are checked out to individual users from within the product/library.")
    public static final String CANNOT_SB_CHECKOUT_WHILE_WIP_CHECKED_OUT = "CANNOT_SB_CHECKOUT_WHILE_WIP_CHECKED_OUT";

   @RBEntry("Cannot check out items to projects that are currently checked out to other projects.")
   public static final String OBJECT_CHECKED_OUT_TO_ANOTHER_SANDBOX = "4";

   @RBEntry("Uniqueness constraint conflicts were found in check-in request.")
   public static final String UNIQUENESS_CONFLICTS_FOUND_IN_CHECK_IN = "10";

   @RBEntry("Items have already been checked out to the specified project.")
   public static final String SANDBOX_VERSION_ALREADY_CHECKED_OUT = "12";

   @RBEntry("Items have been deprecated in the specified project.")
   public static final String  SANDBOX_VERSION_SUPERSEDED = "12_1";

   @RBEntry("Other versions of items have already been checked out to the specified project.")
   public static final String OTHER_VERSION_CHECKED_OUT_TO_SANDBOX = "13";

   @RBEntry("The project baseline cannot be moved or replaced.")
   public static final String SANDBOX_BASELINE_CONTAINER_IS_IMMUTABLE = "14";

   @RBEntry("SandboxConfigSpec cannot be applied to '{0}'; it must implement both Workable and WTContained.")
   public static final String SANDBOX_CONFIG_SPEC_REQUIRES_CONTAINED_WORKABLE = "15";

   @RBEntry("Item '{0}' cannot be checked into a personal cabinet from a project.")
   public static final String SANDBOX_CHECKIN_TO_PERSONAL_CABINET_NOT_ALLOWED = "16";

   @RBEntry("Cannot check out items to projects while those items reside in personal cabinets.")
   public static final String CANNOT_SB_CHECK_OUT_PERSONAL_CABINET_ENTRY = "17";

   @RBEntry("Cannot delete projects which contain active project checkouts.")
   public static final String CANNOT_DELETE_CONTAINER_WITH_WORKING_COPIES = "18";

   @RBEntry("Multiple errors conditions were detected.")
   public static final String MULTIPLE_CAUSES = "20";

   @RBEntry("Cannot check out non-latest iterations to a project.")
   public static final String CANNOT_SB_CHECKOUT_NON_LATEST_ITERATIONS = "21";

   @RBEntry("User lacks privileges to modify objects.")
   public static final String USER_HAS_NO_MODIFY_PRIVILEGES = "22";

    @RBEntry("Cannot check out objects which are currently locked by an individual user.")
   public static final String CANNOT_CHECK_OUT_LOCKED_OBJECTS = "23";

   @RBEntry("Cannot undo project checkouts; objects do not appear to be proper project checkouts.")
   public static final String CANNOT_UNDO_SB_CHECKOUT_INVALID_DATA_STATE = "24";

   @RBEntry("Cannot perform undo project checkout operation on non-latest iterations.")
   public static final String CANNOT_UNDO_SB_CHECKOUT_NON_LATEST_ITERATIONS = "25";

   @RBEntry("Cannot undo the project check out of items which individual users have checked out as a working copies from within the project.")
   public static final String CANNOT_UNDO_SB_CHECKOUT_WHILE_WIP = "26";

   @RBEntry("Cannot undo the project check out of items which are not checked out to projects.")
   public static final String CANNOT_UNDO_SB_CHECKOUT_NOT_CHECKED_OUT = "27";

   @RBEntry("User cannot unlock some objects.")
   public static final String USER_CANNOT_UNLOCK_OBJECTS = "28";

   @RBEntry("Cannot perform send-to-PDM operation on non-latest iterations.")
   public static final String CANNOT_SB_CHECKIN_WITH_NON_LATEST_ITERATIONS = "29";

   @RBEntry("Cannot send objects to PDM while they are checked out to individual users from within the project.")
   public static final String CANNOT_SB_CHECKIN_WHILE_WIP_CHECKED_OUT = "30";

   @RBEntry("Cannot send objects in a Sent to PDM, Abandoned, or Deprecated state to PDM.")
   public static final String CANNOT_SB_CHECKIN_TERMINAL_OBJ = "31";

   @RBEntry("Can only send objects from within projects to PDM.")
   public static final String CANNOT_SB_CHECKIN_OBJ_NOT_IN_PROJECT = "32";

   @RBEntry("Cannot send objects to PDM that have been sent to PDM and not checked out to a project since.")
   public static final String CANNOT_SB_CHECKIN_OBJ_NOT_CHECKED_OUT = "33";

   @RBEntry("Cannot perform send object created in project to PDM without initial project check in data.")
   public static final String CANNOT_SB_CHECKIN_WITHOUT_INITIAL_CHECKIN_DATA = "34";

   @RBEntry("Cannot have more than one version of an object checked out to the same project at the same time.")
   public static final String MULTI_VERSION_SB_CHECKOUT = "35";

   @RBEntry("Cannot purge the following items because they are shared to projects and are identified as the iterations that users can add to workspaces for those projects")
   public static final String CANNOT_DELETE_SANDBOXBASELINE_MEMBERS = "36";

   @RBEntry("This system is not configured to allow checkout across organizational boundaries.")
   public static final String DIFFERENT_NAMESPACES = "37";

    @RBEntry("Unable to delete object {0}, the object has been checked out to one or more projects.")
   public static final String CANNOT_DELETE_SB_CHECKOUT = "38";

   @RBEntry("ATTENTION: Secured Action. You do not have the necessary authorization for this checkout operation.")
   public static final String NO_ACCESS_TO_CHECKOUT_OPERATION = "39";

    @RBEntry("ATTENTION: Secured Action. You do not have the necessary authorization to send the following items to PDM:")
   public static final String NO_ACCESS_TO_CHECKIN_OPERATION = "40";

   @RBEntry("ATTENTION: Secured Action. You do not have the necessary authorization for Undo PDM Checkout operation.")
   public static final String NO_ACCESS_TO_UNDO_CHECKOUT_OPERATION = "41";

   @RBEntry("ATTENTION: Secured Action. You do not have permissions to refresh project: {0}.")
   @RBComment("Text of the message of the exception thrown when an attempt is made to refresh a project to which user doesn't have sufficient permissions.")
   public static final String NO_PERMISSIONS_TO_REFRESH = "50";

   @RBEntry("Cannot check out objects, as one or more of the objects is of an object type that is not allowed to be checked out.")
   @RBComment("One or more selected objects are of types that don't support PDM Checkout.")
   public static final String WRONG_OBJECT_TYPE = "51";

   @RBEntry("ATTENTION: Item {0} must be checked out before checking it in.")
   @RBArgComment0("Identity of invalid element.")
   public static final String INVALID_CHECKIN_ATTEMPT_NOT_CHECKED_OUT = "52";

   @RBEntry("ATTENTION: One or more of the objects is of an object type that is not allowed for this action.")
   public static final String INVALID_OBJECT_TYPE = "53";

   @RBEntry("Cannot send object to PDM that is associated with object in Abandoned or Deprecated state.")
   public static final String CANNOT_SB_CHECKIN_OBJ_ASSOC_TO_SUPERSEDED_OR_ABANDONED = "54";

   /**
    * fix SPR 1863512,  need a meaningful error message on invoking the Send to PDM action on two one-off versions of the same object.
    **/
   @RBEntry("Cannot send multiple one-off versions of the same object to PDM")
   public static final String CANNOT_SB_CHECKIN_MULTIPLE_ONEOFF_VERSIONS_OF_SAME_OBJECT = "56";

    @RBEntry("This object is already shared to this project; however, the baselined iteration is not the latest.  You must update to the latest iteration before this object can be checked out.")
    @RBComment("Error message when user wants to PDM checkout an object when an earlier iteration of the object has already been shared to the same project")
    public static final String CANNOT_CO_OUT_OF_DATE_SHARE = "CANNOT_CO_OUT_OF_DATE_SHARE";

}
