/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.access.ixb.handlers.forattributes;

import wt.util.resource.*;

@RBUUID("wt.access.ixb.handlers.forattributes.forattributesResource")
public final class forattributesResource extends WTListResourceBundle {
   @RBEntry("Owner not found for imported adhoc access permissions ")
   public static final String NO_OWNER = "0";

   @RBEntry("Principal not found for imported adhoc access permissions: {0}")
   public static final String NO_PRINCIPAL = "1";

   @RBEntry("No permissions were found for imported adhoc permissions")
   public static final String NO_PERMISSIONS = "2";

   @RBEntry("Cannot import adhoc permissions because they conflict with existing permissions for object: {0}  principal: {1} owner: {2}.")
   public static final String ADHOCS_ALREADY_EXIST_1 = "3";

   @RBEntry("Cannot import adhoc permissions because they conflict with existing permissions for object: {0}  principal: {1} owner: {2} owner ID: {3}.")
   public static final String ADHOCS_ALREADY_EXIST_2 = "4";

   @RBEntry("Invalid access control permission name found during import: {0}")
   public static final String INVALID_PERMISSION_NAME = "5";

   @RBEntry("Invalid access control permission key found during import: {0}")
   public static final String INVALID_PERMISSION_KEY = "6";

   @RBEntry("Invalid adhoc permission owner name found during import: {0}")
   public static final String INVALID_OWNER_NAME = "7";

   @RBEntry("Invalid adhoc permission owner key found during import: {0}")
   public static final String INVALID_OWNER_KEY = "8";

   @RBEntry("Skipping export of access control rule with null principal.")
   public static final String NULL_PRINCIPAL = "12";

   @RBEntry("Skipping export of access control rule because principal is disabled: {0}")
   public static final String PRINCIPAL_IS_DISABLED = "13";

   @RBEntry("Skipping export of access control rule because principal no longer exists: {0}")
   public static final String PRINCIPAL_NO_LONGER_EXISTS = "14";
}
