/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.access.ixb.handlers.forattributes;

import wt.util.resource.*;

@RBUUID("wt.access.ixb.handlers.forattributes.forattributesResource")
public final class forattributesResource_it extends WTListResourceBundle {
   @RBEntry("Proprietario non trovato per i permessi di accesso ad hoc importati")
   public static final String NO_OWNER = "0";

   @RBEntry("Utente/gruppo/ruolo non trovato per i permessi di accesso ad hoc importati: {0}")
   public static final String NO_PRINCIPAL = "1";

   @RBEntry("Permessi non trovati per i permessi di accesso ad hoc importati")
   public static final String NO_PERMISSIONS = "2";

   @RBEntry("Impossibile importare i permessi ad hoc perché sono in conflitto con i permessi esistenti per l'oggetto: {0} Utente/gruppo/ruolo: {1} Proprietario: {2}.")
   public static final String ADHOCS_ALREADY_EXIST_1 = "3";

   @RBEntry("Impossibile importare i permessi ad hoc perché sono in conflitto con i permessi esistenti per l'oggetto: {0} Utente/gruppo/ruolo: {1} Proprietario: {2} ID proprietario: {3}.")
   public static final String ADHOCS_ALREADY_EXIST_2 = "4";

   @RBEntry("Nome dei permessi di controllo d'accesso non valido durante l'importazione: {0}")
   public static final String INVALID_PERMISSION_NAME = "5";

   @RBEntry("Chiave dei permessi di controllo d'accesso non valida durante l'importazione: {0}")
   public static final String INVALID_PERMISSION_KEY = "6";

   @RBEntry("Nome proprietario dei permessi ad hoc non valido durante l'importazione: {0}")
   public static final String INVALID_OWNER_NAME = "7";

   @RBEntry("Chiave proprietario dei permessi ad hoc non valida durante l'importazione: {0}")
   public static final String INVALID_OWNER_KEY = "8";

   @RBEntry("L'esportazione della regola di controllo d'accesso con utente/gruppo/ruolo nullo non viene eseguita.")
   public static final String NULL_PRINCIPAL = "12";

   @RBEntry("L'esportazione della regola di controllo d'accesso non viene eseguita poiché l'utente/gruppo/ruolo è disattivato: {0}")
   public static final String PRINCIPAL_IS_DISABLED = "13";

   @RBEntry("L'esportazione della regola di controllo d'accesso non viene eseguita poiché l'utente/gruppo/ruolo non esiste più: {0}")
   public static final String PRINCIPAL_NO_LONGER_EXISTS = "14";
}
