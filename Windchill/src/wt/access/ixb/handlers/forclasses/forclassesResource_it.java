/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.access.ixb.handlers.forclasses;

import wt.util.resource.*;

@RBUUID("wt.access.ixb.handlers.forclasses.forclassesResource")
public final class forclassesResource_it extends WTListResourceBundle {
   @RBEntry("È necessario specificare un oggetto AdHocControlled durante l'importazione dei permessi per le regole di controllo di accesso ad hoc.")
   @RBComment("This situation indicates a coding error in using the class wt.access.ixb.handlers.forclasses.ImpForAdHocControlled.")
   public static final String NO_BUSINESS_OBJECT = "0";

   @RBEntry("È stata trovata una regola di controllo di accesso esistente i cui permessi non corrispondono a quelli specificati nel file di caricamento. Le regole di controllo di accesso esistenti non possono essere sovrascritte dall'importazione. La regola è relativa all'utente/gruppo/ruolo {0} e contiene il selettore di controllo di accesso {1}.")
   @RBComment("this message reports a conflict when importing access control rules")
   public static final String NON_MATCHING_PERMISSIONS = "1";

   @RBEntry("Impossibile trovare il dominio \"{0}\" oppure l'utente non ha accesso al dominio. Per ulteriori informazioni consultare il manuale Windchill Business Administrator's Guide (Guida dell'amministratore aziendale Windchill).")
   public static final String DOMAIN_PATH_NOT_FOUND = "2";

   @RBEntry("Impossibile trovare lo stato del ciclo di vita \"{0}\". Consultare il manuale Windchill Business Administrator's Guide (Guida dell'amministratore aziendale Windchill) per ulteriori informazioni.")
   public static final String LIFECYCLE_STATE_NOT_FOUND = "3";

   @RBEntry("Impossibile trovare l'identificatore tipo \"{0}\". Consultare il manuale Windchill Business Administrator's Guide (Guida dell'amministratore aziendale Windchill) per ulteriori informazioni.")
   public static final String TYPE_NOT_FOUND = "4";

   @RBEntry("Impossibile trovare un utente/gruppo/ruolo con UFID \"{0}\".")
   public static final String UFID_NOT_FOUND = "5";

   @RBEntry("Impossibile trovare un gruppo \"{0}\" di tipo \"{1}\".")
   public static final String GROUP_NOT_FOUND = "6";

   @RBEntry("Impossibile trovare un utente/gruppo/ruolo \"{0}\".")
   public static final String PRINCIPAL_NAME_NOT_FOUND = "7";
}
