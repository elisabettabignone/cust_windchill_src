/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.access.ixb.handlers.forclasses;

import wt.util.resource.*;

@RBUUID("wt.access.ixb.handlers.forclasses.forclassesResource")
public final class forclassesResource extends WTListResourceBundle {
   @RBEntry("An AdHocControlled object must be specified when importing adhoc access control permissions.")
   @RBComment("This situation indicates a coding error in using the class wt.access.ixb.handlers.forclasses.ImpForAdHocControlled.")
   public static final String NO_BUSINESS_OBJECT = "0";

   @RBEntry("An existing access control rule has permissions that do not match those specified in the load file.  Imports cannot overwrite existing access control rules.  The rule is for principal: {0} and has access control selector: {1}.")
   @RBComment("this message reports a conflict when importing access control rules")
   public static final String NON_MATCHING_PERMISSIONS = "1";

   @RBEntry("Domain \"{0}\" cannot be found or the user doesn't have access to the Domain. See the Windchill Business Administrator's Guide for further information.")
   public static final String DOMAIN_PATH_NOT_FOUND = "2";

   @RBEntry("Life Cycle State \"{0}\" cannot be found. See the Windchill Business Administrator's Guide for further information.")
   public static final String LIFECYCLE_STATE_NOT_FOUND = "3";

   @RBEntry("Type Identifier \"{0}\" cannot be found. See the Windchill Business Administrator's Guide for further information.")
   public static final String TYPE_NOT_FOUND = "4";

   @RBEntry("A principal with ufid \"{0}\" was not found.")
   public static final String UFID_NOT_FOUND = "5";

   @RBEntry("A group was not found with name:\"{0}\" and type:\"{1}\".")
   public static final String GROUP_NOT_FOUND = "6";

   @RBEntry("A principal with name: \"{0}\" was not found.")
   public static final String PRINCIPAL_NAME_NOT_FOUND = "7";
}
