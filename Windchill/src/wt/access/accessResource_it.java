/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.access;

import wt.util.resource.*;

@RBUUID("wt.access.accessResource")
public final class accessResource_it extends WTListResourceBundle {
   @RBEntry("Permesso non valido \"{0}\".")
   @RBComment("Specified permission is invalid")
   @RBArgComment0("Permission Name")
   public static final String INVALID_PERMISSION = "2";

   @RBEntry("Impossibile creare ACL. Parametri non validi.")
   @RBComment("AccessControlException: Cannot create ACL with the parameters given")
   public static final String BAD_ACL_SELECTOR = "3";

   @RBEntry("Impossibile creare regola di accesso. Parametri non validi.")
   @RBComment("AccessControlException: Cannot create access policy rule with the parameters given")
   public static final String BAD_RULE_SELECTOR = "4";

   @RBEntry("Utente/gruppo/ruolo della sessione non impostato.")
   @RBComment("AccessControlException: Session principal needs to be set.")
   public static final String PRINCIPAL_NOT_SET = "6";

   @RBEntry("\"{0}\" non dispone dei permessi di \"{1}\" per l'oggetto \"{2}\".")
   @RBComment("DO NOT USE THIS DEPRECATED MESSAGE.")
   @RBArgComment0("Actor--The attempting user")
   @RBArgComment1("Action--The attempted operation")
   @RBArgComment2("Direct Object--The object to which the operation was attempted")
   public static final String NOT_AUTHORIZED = "8";

   @RBEntry("\"{0}\" non dispone del permesso di {1} per \"{2}\".")
   @RBComment("AccessControlEvent.NOT_AUTHORIZED message: The user does not have a necessary permission for an object.")
   @RBArgComment0("The user who is attempting the operation")
   @RBArgComment1("The required permission (values defined in wt.access.WTPermissionRB)")
   @RBArgComment2("The object for which the permission is required")
   public static final String NOT_PERMITTED = "14";

   @RBEntry("\"{0}\" non dispone dei permessi di {1} per \"{2}\" nel dominio amministrativo \"{3}\".")
   @RBComment("AccessControlEvent.NOT_AUTHORIZED message: The user does not have a necessary permission for an object in the named domain.")
   @RBArgComment0("The user who is attempting the operation")
   @RBArgComment1("The required permission (values defined in wt.access.WTPermissionRB)")
   @RBArgComment2("The object for which the permission is required")
   @RBArgComment3("The Administrative Domain in which the permission is required")
   public static final String NOT_PERMITTED_IN_DOMAIN = "15";

   @RBEntry("Una regola di controllo d'accesso è già stata definita per il dominio: \"{0}\", tipo: \"{1}\", stato del ciclo di vita: \"{2}\", utente/gruppo/ruolo: \"{3}\", permessi concessi: \"{4}\", permessi negati: {5}\".")
   @RBComment("DO NOT USE THIS DEPRECATED MESSAGE.")
   @RBArgComment0("Path name of the domain the rule applies to")
   @RBArgComment1("External type identifier of the type in the rule")
   @RBArgComment2("State name (values defined in wt.lifecycle.StateRB) of the state in the rule")
   @RBArgComment3("Name of the principal in the rule")
   @RBArgComment4("List of permissions granted by the existing rule, values defined in AccessPermissionRB")
   @RBArgComment5("List of permissions denied by the existing rule, values defined in AccessPermissionRB")
   public static final String ACCESS_RULE_ALREADY_EXISTS = "17";

   @RBEntry("(Informazioni protette)")
   @RBComment("Used in place of the display name for an attribute or item in a list, to indicate that the current user does not have access to the associated object.")
   public static final String SECURED_INFORMATION = "18";

   @RBEntry("ATTENZIONE: azione protetta. L'utente non dispone dell'autorizzazione necessaria per questa operazione. Contattare l'amministratore se questo messaggio viene visualizzato erroneamente.")
   @RBComment("NotAuthorizedException: The user does not have the required permission to access an object.")
   public static final String NOT_AUTHORIZED_EXCEPTION = "19";

   @RBEntry("\"{0}\" non è autorizzato a modificare i permessi per \"{1}\" perché l'utente non dispone dei permessi da modificare per l'oggetto. Permessi dell'utente: \"{2}\". Permessi da modificare: \"{3}\".")
   @RBComment("AccessControlEvent.NOT_AUTHORIZED message: To change permissions for an object, the user must have those permissions and does not.")
   @RBArgComment0("The user who is attempting the operation")
   @RBArgComment1("The object for which the permissions are required")
   @RBArgComment2("List of permissions the user has")
   @RBArgComment3("List of permissions the user is attempting to change")
   public static final String CHANGE_PERMISSIONS_NOT_PERMITTED = "20";

   @RBEntry("L'identificatore oggetto di \"{0}\" deve essere assegnato ad un oggetto persistente.")
   @RBComment("WTPropertyVetoException: An ObjectReference attribute contains an ObjectIdentifier that has not been assigned to any Persistable object.")
   @RBArgComment0("The attribute name")
   public static final String UNASSIGNED_REFERENCE = "21";

   @RBEntry("(Eliminato)")
   @RBComment("Used in place of the display name for a referenced object that no longer exists.")
   public static final String DELETED_OBJECT = "22";

   @RBEntry("Il valore del parametro \"{0}\" non può essere impostato su 0.")
   @RBComment("WTInvalidParameterException: A parameter value of zero is passed to a method requiring a nonzero value.")
   @RBArgComment0("Name of parameter")
   public static final String NONZERO_PARAMETER = "23";

   @RBEntry("Il valore del parametro \"{0}\" non può essere nullo o vuoto.")
   @RBComment("WTInvalidParameterException: An empty or null value is passed to a method for a required parameter.")
   @RBArgComment0("Name of parameter")
   public static final String EMPTY_OR_NULL_PARAMETER = "24";

   @RBEntry("Il valore parametro \"{0}\" non può essere un utente/gruppo/ruolo eliminato. Riferimento utente/gruppo/ruolo: {1}")
   @RBComment("WTInvalidParameterException: A parameter value specifying a deleted principal (a principal object marked as disabled) is passed to a method requiring an enabled principal.")
   @RBArgComment0("Name of parameter")
   @RBArgComment1("Reference to the specified principal")
   public static final String DELETED_PRINCIPAL = "25";

   @RBEntry("ATTENZIONE: etichette di sicurezza non valide. Impossibile accedere a un oggetto in quanto le relative etichette di sicurezza non sono valide. Segnalare il problema all'amministratore.")
   @RBComment("WTRuntimeException: The security labels for an object are accessed and they are not valid. The string encoded representation of the set of labels has an unexpected version number or is in an unexpected format.")
   public static final String SECURITY_LABELS_NOT_VALID = "26";

   @RBEntry("ATTENZIONE: rilevato un errore nella configurazione delle etichette di sicurezza. Segnalare il problema all'amministratore.")
   @RBComment("WTRuntimeException: The security labels for an object are accessed and they include undefined label names and/or values.")
   public static final String SECURITY_LABELS_CONFIG_ERROR = "27";

   @RBEntry("ATTENZIONE: impossibile impostare le etichette di sicurezza. Non è stato possibile impostare un valore per l'etichetta di sicurezza dell'oggetto perché si trova in una cartella o workspace personale e il valore limiterebbe i diritti di accesso del proprietario: {0} = {1}")
   @RBComment("AccessControlException: The security labels are being set for one or more objects in a personal cabinet and one or more of the specified label values restrict the cabinet owner")
   @RBArgComment0("localizable security label name")
   @RBArgComment1("localizable security label value")
   public static final String SECURITY_LABELS_RESTRICT_OWNER_ACCESS = "28";

   @RBEntry("È necessario richiamare il metodo {0} nelle transazioni per oggetti persistenti.")
   @RBComment("WTException: There is no active transaction for a method that requires one when dealing with persisted objects.")
   @RBArgComment0("method name")
   public static final String TRANSACTION_REQUIRED = "29";

   @RBEntry("\"{0}\" non è autorizzato a modificare le etichette di sicurezza per \"{1}\". {2}")
   @RBComment("AccessControlEvent.NOT_AUTHORIZED message: A user tries to modify the security labels for an object and is not authorized by one or more of the security label values being modified.")
   @RBArgComment0("The user who is attempting the operation")
   @RBArgComment1("The object for which authorization is required")
   @RBArgComment2("Collection of localizable RESTRICTED_LABEL_INFO messages")
   public static final String MODIFY_SECURITY_LABELS_NOT_PERMITTED = "30";

   @RBEntry("Nome: \"{0}\"; valore: \"{1}\"; partecipante autorizzato: \"{2}\"")
   @RBComment("Detailed information to be included in the MODIFY_SECURITY_LABELS_NOT_PERMITTED message.")
   @RBArgComment0("localizable security label name")
   @RBArgComment1("localizable security label value")
   @RBArgComment2("authorized participant's distinguished name")
   public static final String RESTRICTED_LABEL_INFO = "31";

   @RBEntry("Il valore del parametro \"{0}\" non può essere un oggetto persistente.")
   @RBComment("WTInvalidParameterException: A parameter value specifying a persistent object is passed to a method requiring a non-persistent object.")
   @RBArgComment0("Name of parameter")
   public static final String PERSISTENT_OBJECT = "32";

   @RBEntry("Il valore del parametro \"{0}\" non può contenere oggetti persistenti nei valori per la mappa.")
   @RBComment("WTInvalidParameterException: A parameter value specifying a map containing one or more persistent objects in the values is passed to a method requiring non-persistent objects.")
   @RBArgComment0("Name of parameter")
   public static final String PERSISTENT_VALUES = "33";

   @RBEntry("Il valore del parametro \"{0}\" non può contenere oggetti non persistenti nelle chiavi o valori per la mappa.")
   @RBComment("WTInvalidParameterException: A parameter value specifying a map containing one or more non-persistent objects in the keys or values is passed to a method requiring persistent objects.")
   @RBArgComment0("Name of parameter")
   public static final String NON_PERSISTENT_KEYS_OR_VALUES = "34";

   @RBEntry("Partecipante")
   @RBComment("display value for the WTAclEntry.ALL_EXCEPT_PRINCIPAL boolean attribute (false); the entry is for the specified principal (participant)")
   public static final String PRINCIPAL = "35";

   @RBEntry("Tutti tranne partecipante")
   @RBComment("display value for the WTAclEntry.ALL_EXCEPT_PRINCIPAL boolean attribute (true); the entry is for all principals except the specified principal (participant)")
   public static final String ALL_EXCEPT_PRINCIPAL = "36";

   @RBEntry("Una regola di controllo d'accesso è già stata definita per il dominio: \"{0}\", tipo: \"{1}\", stato del ciclo di vita: \"{2}\", utente/gruppo/ruolo: \"{3}\", si applica a: \"{4}\", permessi concessi: \"{5}\", permessi negati: \"{6}\", permessi negati in modo non modificabile: \"{7}\".")
   @RBComment("PolicyRuleAlreadyExistsException: An attempt was made to create an access control rule that already exists")
   @RBArgComment0("Path name of the domain the rule applies to")
   @RBArgComment1("External type identifier of the type in the rule")
   @RBArgComment2("State name (values defined in wt.lifecycle.StateRB) of the state in the rule")
   @RBArgComment3("Name of the principal in the rule")
   @RBArgComment4("POLICY_PRINCIPAL or POLICY_ALL_EXCEPT_PRINCIPAL resource string")
   @RBArgComment5("List of permissions granted by the existing rule, values defined in AccessPermissionRB")
   @RBArgComment6("List of permissions denied by the existing rule, values defined in AccessPermissionRB")
   @RBArgComment7("List of permissions absolutely denied by the existing rule, values defined in AccessPermissionRB")
   public static final String POLICY_RULE_ALREADY_EXISTS = "37";

   @RBEntry("Una regola di controllo d'accesso è già stata definita per il dominio: \"{0}\", tipo: \"{1}\", stato del ciclo di vita: \"{2}\", utente/gruppo/ruolo: \"{3}\", si applica a: \"{4}\".")
   @RBComment("PolicyRuleDoesNotExistException: An attempt was made to delete an access control rule that does not exist")
   @RBArgComment0("Path name of the domain the rule applies to")
   @RBArgComment1("External type identifier of the type in the rule")
   @RBArgComment2("State name (values defined in wt.lifecycle.StateRB) of the state in the rule")
   @RBArgComment3("Name of the principal in the rule")
   @RBArgComment4("POLICY_PRINCIPAL or POLICY_ALL_EXCEPT_PRINCIPAL resource string")
   public static final String POLICY_RULE_DOES_NOT_EXIST = "38";

   @RBEntry("Utente/gruppo/ruolo")
   @RBComment("The same as PRINCIPAL resource string except for use in Policy Administration which refers to principal rather than participant")
   public static final String POLICY_PRINCIPAL = "39";

   @RBEntry("Tutti tranne utente/gruppo/ruolo")
   @RBComment("The same as ALL_EXCEPT_PRINCIPAL resource string except for use in Policy Administration which refers to principal rather than participant")
   public static final String POLICY_ALL_EXCEPT_PRINCIPAL = "40";

   @RBEntry("Il valore del parametro \"{0}\" non può essere impostato su true quando l'utente/gruppo/ruolo specificato corrisponde allo pseudoruolo \"{1}\".")
   @RBComment("WTInvalidParameterException: A parameter value of true is not supported for a parameter when the specified principal is a pseudo role. ")
   @RBArgComment0("Name of parameter")
   @RBArgComment1("Name of pseudo role")
   public static final String ALL_EXCEPT_PRINCIPAL_NOT_FOR_PSEUDO_ROLES = "41";

   @RBEntry("Il valore del parametro \"{0}\" deve essere vuoto o nullo quando l'utente/gruppo/ruolo specificato corrisponde allo pseudoruolo \"{1}\".")
   @RBComment("WTInvalidParameterException: An empty or null parameter value is required for a parameter when the specified principal is a pseudo role.")
   @RBArgComment0("Name of parameter")
   @RBArgComment1("Name of pseudo role")
   public static final String ABSOLUTE_DENY_NOT_FOR_PSEUDO_ROLES = "42";

   @RBEntry("I valori devono specificare almeno un permesso applicabile per i parametri {0}, {1}, {2}  ")
   @RBComment("WTInvalidParameterException: A parameter value specifying an applicable permission must be specified for at least one of the named parameters.")
   @RBArgComment0("Name of parameter")
   @RBArgComment1("Name of parameter")
   @RBArgComment2("Name of parameter")
   public static final String APPLICABLE_PERMISSION_REQUIRED = "43";

   @RBEntry("Impossibile eliminare alcuni partecipanti poiché sono referenziati da regole di controllo di accesso. Contattare l'amministratore per assistenza.")
   @RBComment("AccessControlException: An attempt was made to delete a participant that is referenced by an access control rule.")
   public static final String PARTICIPANTS_NOT_AVAILABLE_FOR_DELETION = "44";

   @RBEntry("Impossibile eliminare il tipo perché è impiegato nella configurazione delle etichette di sicurezza: \"{0}\".")
   @RBComment("Message displayed in the exception thrown when user attempts to delete a type which is specified as the Authorization Agreement type for a Security Label value.")
   @RBArgComment0("Name of the type whose deletion was prevented")
   public static final String CANNOT_DELETE_SL_RELATED_TYPE = "45";

   @RBEntry("Impossibile rinominare il tipo perché è impiegato nella configurazione delle etichette di sicurezza: \"{0}\".")
   @RBComment("Message displayed in the exception thrown when user attempts to rename a type which is specified as the Authorization Agreement type for a Security Label value.")
   @RBArgComment0("Name of the type whose rename was prevented")
   public static final String CANNOT_RENAME_SL_RELATED_TYPE = "46";

   @RBEntry("L'etichetta di sicurezza {0} non esiste.")
   @RBComment("WTInvalidParameterException: A security label name that is not defined in the security labels configuration file is passed to a method for a required parameter.")
   @RBArgComment0("Name of security label")
   public static final String SECURITY_LABEL_NOT_EXISTS = "47";

   @RBEntry("L'etichetta di sicurezza per il parametro {0} dell'applicazione di creazione non esiste.")
   @RBComment("WTInvalidParameterException: A security label does not exist for an authoring application parameter name passed to a method for a required parameter.")
   @RBArgComment0("Name of parameter")
   public static final String SECURITY_LABEL_NOT_EXISTS_FOR_PARAMETER = "48";

   @RBEntry("L'etichetta di sicurezza {0} non è di tipo personalizzato.")
   @RBComment("WTInvalidParameterException: A security label name that is not a custom label is passed to a method expecting a custom label.")
   @RBArgComment0("Name of parameter")
   public static final String SECURITY_LABEL_NOT_CUSTOM = "49";

   @RBEntry("È stato rilevato un errore che non consente di impostare i valori dell'etichetta di sicurezza. Contattare l'amministratore.")
   @RBComment("WTRuntimeException: An exception was encountered when calling a custom translator class provided by a customizer.")
   public static final String SECURITY_LABELS_TRANSLATOR_ERROR = "50";

   @RBEntry("ATTENZIONE: valore non valido \n \nValore {0} non valido per {1}.")
   @RBComment("WTRuntimeException: An exception was encountered when setting a security label. The user set an invalid value such as NULL.")
   @RBArgComment0("Value of Security Label that the user attempted to set")
   @RBArgComment1("Name of Security Label that the user attempted to set")
   public static final String SECURITY_LABELS_VALUE_INVALID = "51";

   @RBEntry("ATTENZIONE: caratteri non validi \n \nValore di {0} non valido. Il valore non può contenere i seguenti caratteri: , oppure =")
   @RBComment("WTRuntimeException: An exception was encountered when setting a security label.")
   @RBArgComment0("Name of Security Label that the user attempted to set")
   public static final String SECURITY_LABELS_CHARACTERS_INVALID = "52";

   @RBEntry("Il partecipante che esegue l'utilità della riga di comando UpdateSecurityLabels non dispone dell'autorizzazione necessaria. Elenco dei partecipanti autorizzati: {1}.")
   @RBComment("AccessControlEvent.NOT_AUTHORIZED message: The user does not have a necessary permission to run the UpdateSecurityLabels tool")
   @RBArgComment1("The authorized participants allowed to run the UpdateSecurityLabels tool")
   public static final String NOT_AUTHORIZED_FOR_UPDATE_SECURITY_LABELS_TOOL = "NOT_AUTHORIZED_FOR_UPDATE_SECURITY_LABELS_TOOL";

   @RBEntry("L'utente {0} non è autorizzato a eseguire l'utilità della riga di comando wt.access.UpdateSecurityLabels. L'utente deve essere un partecipante autorizzato configurato nella proprietà wt.access.UpdateSecurityLabels.authorizedParticipant. Elenco dei partecipanti autorizzati: {1}")
   @RBComment("AccessControlEvent.NOT_AUTHORIZED message: The user does not have a necessary permission to run the UpdateSecurityLabels tool")
   @RBArgComment0("User running the tool")
   @RBArgComment1("The authorized participants allowed to run the UpdateSecurityLabels tool")
   public static final String NOT_AUTHORIZED_EXCEPTION_FOR_UPDATE_SECURITY_LABELS_TOOL = "NOT_AUTHORIZED_EXCEPTION_FOR_UPDATE_SECURITY_LABELS_TOOL";

}
