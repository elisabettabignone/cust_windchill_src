/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.access.agreement;

import wt.util.resource.*;

@RBUUID("wt.access.agreement.agreementResource")
public final class agreementResource extends WTListResourceBundle {
   /**
    * Entries for parameter violations that are specific to agreements
    **/
   @RBEntry("ATTENTION:  The \"{0}\" parameter value does not represent a document.")
   @RBComment("AgreementException: This error is shown when a user tries to link an object that is not a WTDocument with an AuthorizationAgreement.")
   @RBArgComment0("Name of parameter")
   public static final String NOT_A_DOCUMENT_PARAMETER = "NOT_A_DOCUMENT_PARAMETER";

   @RBEntry("ATTENTION:  The \"{0}\" parameter value does not represent an agreement.")
   @RBComment("AgreementException: This error is shown when a user tries to link something to a non-AuthorizationAgreement object.")
   @RBArgComment0("Name of parameter")
   public static final String NOT_AN_AGREEMENT_PARAMETER = "NOT_AN_AGREEMENT_PARAMETER";

   @RBEntry("ATTENTION:  The \"{0}\" attribute value cannot be set for the agreement authorized object.")
   @RBComment("AgreementException: This error is shown when a valid value cannot be set for an agreement authorized object attribute.")
   @RBArgComment0("Name of parameter")
   public static final String INVALID_AUTHORIZED_OBJECT_ATTRIBUTE_VALUE = "INVALID_AUTHORIZED_OBJECT_ATTRIBUTE_VALUE";

   @RBEntry("ATTENTION:  The \"{0}\" parameter value must not be a working copy of a document.")
   @RBComment("AgreementException: This error is shown when a user tries to link a working copy of a WTDocument with an AuthorizationAgreement.")
   @RBArgComment0("Name of parameter")
   public static final String WORKING_DOCUMENT_PARAMETER = "WORKING_DOCUMENT_PARAMETER";

   @RBEntry("ATTENTION:  The \"{0}\" parameter value must not be a working copy.")
   @RBComment("AgreementException: This error is shown when a user tries to link a working copy of an object with an AuthorizationAgreement as an authorized object.")
   @RBArgComment0("Name of parameter")
   public static final String WORKING_AUTHORIZED_OBJECT_PARAMETER = "WORKING_AUTHORIZED_OBJECT_PARAMETER";

   @RBEntry("ATTENTION:  Multiple version ranges have been specified for the same master reference.")
   @RBComment("AgreementException: This error is shown when a user attempts to set multiple version ranges for the same master reference associated with a specific AuthorizationAgreement.")
   public static final String DUPLICATE_MASTER_REFERENCE = "DUPLICATE_MASTER_REFERENCE";

   @RBEntry("ATTENTION: Revision range settings specify an invalid revision range.")
   @RBComment("AgreementException: This error is shown when a user attempts to set a revision range for an AgreementAuthorizedObject that specifies an invalid revision range.")
   public static final String INVALID_REVISION_RANGE_REFERENCE = "INVALID_REVISION_RANGE_REFERENCE";

   @RBEntry("ATTENTION:  The \"{0}\" parameter value represents an agreement object that does not exist in the database.")
   @RBComment("AgreementException: This error is shown when a user tries to link something with a non-persistent AuthorizationAgreement.")
   @RBArgComment0("Name of parameter")
   public static final String NON_PERSISTENT_AGREEMENT_PARAMETER = "NON_PERSISTENT_AGREEMENT_PARAMETER";

   @RBEntry("ATTENTION:  The \"{0}\" parameter value represents a document that does not exist in the database.")
   @RBComment("AgreementException: This error is shown when a user tries to link an AuthorizationAgreement with a non-persistent WTDocument.")
   @RBArgComment0("Name of parameter")
   public static final String NON_PERSISTENT_DOCUMENT_PARAMETER = "NON_PERSISTENT_DOCUMENT_PARAMETER";

   @RBEntry("ATTENTION:  The \"{0}\" parameter value contains a principal that does not exist in the database.")
   @RBComment("AgreementException: This error is shown when a user tries to link a non-persistent WTPrincipal with an AuthorizationAgreement.")
   @RBArgComment0("Name of parameter")
   public static final String NON_PERSISTENT_PRINCIPAL_PARAMETER = "NON_PERSISTENT_PRINCIPAL_PARAMETER";

   @RBEntry("ATTENTION:  The \"{0}\" parameter value contains a principal that does not represent either a user or a group.")
   @RBComment("AgreementException: This error is shown when a user tries to link a WTPrincipal with an AuthorizationAgreement that is not a WTUser and is not a WTGroup.")
   @RBArgComment0("Name of parameter")
   public static final String NON_SUPPORTED_PRINCIPAL_PARAMETER = "NON_SUPPORTED_PRINCIPAL_PARAMETER";

   @RBEntry("The default cabinet for a context's \"Authorization Agreement\" objects.")
   @RBComment("The total length of the description must not exceed 200 characters, as that is the size limit for cabinet descriptions")
   public static final String AGREEMENT_CABINET_DESCRIPTION = "AGREEMENT_CABINET_DESCRIPTION";

   @RBEntry("ATTENTION:  The \"{0}\" parameter value represents an object that does not support the security label feature.")
   @RBComment("AgreementException: This error is shown when a user tries to link a non-SecurityLabeled object with an AuthorizationAgreement.")
   @RBArgComment0("Name of parameter")
   public static final String NON_SECURITY_LABELED_PARAMETER = "NON_SECURITY_LABELED_PARAMETER";

   @RBEntry("ATTENTION:  The \"{0}\" parameter value must be an object reference or a version reference link.")
   @RBComment("WTPropertyVetoException: This error is shown when a user tries to link a SecurityLabeled object with an AuthorizationAgreement when the reference to the SecurityLabeled object is not an ObjectReference or VersionReference.")
   @RBArgComment0("Name of parameter")
   public static final String NON_SUPPORTED_REFERENCE_PARAMETER = "NON_SUPPORTED_REFERENCE_PARAMETER";

   @RBEntry("ATTENTION:  Agreements cannot be initialized when the agreements feature is not enabled.")
   @RBComment("AgreementException: This error is shown when a user tries to initialize an AuthorizationAgreement object while the agreement feature is not enabled.  These objects can only be initialized when the agreement feature is enabled.")
   public static final String AGREEMENTS_CANNOT_BE_INITIALIZED = "AGREEMENTS_CANNOT_BE_INITIALIZED";

   @RBEntry("Invalid location. An agreement must be created in a folder that descends from the contexts's Agreements cabinet.")
   @RBComment("AgreementException: This error is shown when a user tries to create an Agreement in a folder that does not descend from the context's Agreements cabinet. The context is the context in which the Agreement is being created.")
   public static final String INVALID_AGREEMENT_LOCATION = "INVALID_AGREEMENT_LOCATION";

   @RBEntry("ATTENTION: Authorized objects are not allowed for context based agreements.")
   @RBComment("AgreementException: This error is shown when a user tries to create an Agreement Authorized Object for a Context Based Agreement. Context Based Agreements do not use Authorized Objects so we don't allow Authorized Objects to be created for Context Based Agreements.")
   public static final String AUTH_OBJECT_NOT_ALLOWED_FOR_CONTEXT_AGREEMENT = "AUTH_OBJECT_NOT_ALLOWED_FOR_CONTEXT_AGREEMENT";

   @RBEntry("The Document \"{0}\" cannot be deleted because it is the authorization document for the agreement \"{1}\".")
   @RBComment("AgreementException: This error is shown when a user tries to delete documents that are authorization documents for existing Agreements.")
   @RBArgComment0("The identity of the document being deleted. A document identity has the format <type> - <number>, <version> (e.g., Document - 0000000168, A.1)")
   @RBArgComment1("The identiry of the agreement for which the document is the authorization document.")
   public static final String CANNOT_DELETE_AGREEMENT_AUTHORIZATION_DOCUMENTS = "CANNOT_DELETE_AGREEMENT_AUTHORIZATION_DOCUMENTS";

   @RBEntry("Administrative domain \"{0}\" does not exist for context \"{1}\".")
   @RBComment("AgreementException: This error is shown when creating the Agreement Cabinet for a container and the domain specified for the cabinet does not exist for the container.")
   @RBArgComment0("The path of the AdministrativeDomain")
   @RBArgComment1("The context name")
   public static final String ADMIN_DOMAIN_DOES_NOT_EXIST = "ADMIN_DOMAIN_DOES_NOT_EXIST";

   @RBEntry("ATTENTION:  The \"{0}\" parameter contains a security labeled object that does not have a context associated with it.")
   @RBComment("AgreementException: This error is shown when the agreement service tries to determine if a security labeled object is in the scope of a particular agreement, but cannot figure out its context.")
   @RBArgComment0("Name of parameter")
   public static final String CANNOT_DETERMINE_CONTAINER = "CANNOT_DETERMINE_CONTAINER";

   @RBEntry("Agreement Life Cycle")
   @RBComment("Agreement Life Cycle")
   public static final String AGREEMENT_LIFECYCLE = "AGREEMENT_LIFECYCLE";

   @RBEntry("Agreement Initialization Rules")
   @RBComment("Agreement Initialization Rules")
   public static final String AGREEMENT_INIT_RULES = "AGREEMENT_INIT_RULES";

   @RBEntry("\"{0}\" is not authorized to perform database operations on the selected agreement because the user is not an agreement administrator.")
   @RBComment("AccessControlEvent.NOT_AUTHORIZED message: This error is shown when the current principal is not authorized to perform database operations on an agreement object.")
   @RBArgComment0("The current principal.")
   public static final String NOT_AUTHORIZED = "NOT_AUTHORIZED";

   @RBEntry("Trying to record more than one association of related object in agreements table.")
   @RBComment("This error shows up when trying to store the association of the related object to more than one guardian object.")
   public static final String AGREEMENT_MAP_INVALID_ENTRY = "AGREEMENT_MAP_INVALID_ENTRY";
}
