/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.access;

import wt.util.resource.*;

@RBUUID("wt.access.accessResource")
public final class accessResource extends WTListResourceBundle {
   @RBEntry("Invalid permission: \"{0}\".")
   @RBComment("Specified permission is invalid")
   @RBArgComment0("Permission Name")
   public static final String INVALID_PERMISSION = "2";

   @RBEntry("Cannot create ACL: bad parameters.")
   @RBComment("AccessControlException: Cannot create ACL with the parameters given")
   public static final String BAD_ACL_SELECTOR = "3";

   @RBEntry("Cannot create access policy rule: bad parameters.")
   @RBComment("AccessControlException: Cannot create access policy rule with the parameters given")
   public static final String BAD_RULE_SELECTOR = "4";

   @RBEntry("Session principal has not been set.")
   @RBComment("AccessControlException: Session principal needs to be set.")
   public static final String PRINCIPAL_NOT_SET = "6";

   @RBEntry("\"{0}\" is not authorized to \"{1}\" \"{2}\" object.")
   @RBComment("DO NOT USE THIS DEPRECATED MESSAGE.")
   @RBArgComment0("Actor--The attempting user")
   @RBArgComment1("Action--The attempted operation")
   @RBArgComment2("Direct Object--The object to which the operation was attempted")
   public static final String NOT_AUTHORIZED = "8";

   @RBEntry("\"{0}\" does not have {1} permission for \"{2}\".")
   @RBComment("AccessControlEvent.NOT_AUTHORIZED message: The user does not have a necessary permission for an object.")
   @RBArgComment0("The user who is attempting the operation")
   @RBArgComment1("The required permission (values defined in wt.access.WTPermissionRB)")
   @RBArgComment2("The object for which the permission is required")
   public static final String NOT_PERMITTED = "14";

   @RBEntry("\"{0}\" does not have {1} permission for \"{2}\" in Administrative Domain \"{3}\".")
   @RBComment("AccessControlEvent.NOT_AUTHORIZED message: The user does not have a necessary permission for an object in the named domain.")
   @RBArgComment0("The user who is attempting the operation")
   @RBArgComment1("The required permission (values defined in wt.access.WTPermissionRB)")
   @RBArgComment2("The object for which the permission is required")
   @RBArgComment3("The Administrative Domain in which the permission is required")
   public static final String NOT_PERMITTED_IN_DOMAIN = "15";

   @RBEntry("An access control rule is already defined for Domain: \"{0}\", Type: \"{1}\", State: \"{2}\", Principal: \"{3}\", Grant Permissions: \"{4}\", Deny Permissions: \"{5}\".")
   @RBComment("DO NOT USE THIS DEPRECATED MESSAGE.")
   @RBArgComment0("Path name of the domain the rule applies to")
   @RBArgComment1("External type identifier of the type in the rule")
   @RBArgComment2("State name (values defined in wt.lifecycle.StateRB) of the state in the rule")
   @RBArgComment3("Name of the principal in the rule")
   @RBArgComment4("List of permissions granted by the existing rule, values defined in AccessPermissionRB")
   @RBArgComment5("List of permissions denied by the existing rule, values defined in AccessPermissionRB")
   public static final String ACCESS_RULE_ALREADY_EXISTS = "17";

   @RBEntry("(Secured information)")
   @RBComment("Used in place of the display name for an attribute or item in a list, to indicate that the current user does not have access to the associated object.")
   public static final String SECURED_INFORMATION = "18";

   @RBEntry("ATTENTION:  Secured Action. You do not have the necessary authorization for this operation. Contact your administrator if you believe you have received this message in error.")
   @RBComment("NotAuthorizedException: The user does not have the required permission to access an object.")
   public static final String NOT_AUTHORIZED_EXCEPTION = "19";

   @RBEntry("\"{0}\" is not authorized to change permissions for \"{1}\" because the user does not have the permissions to be changed for the object. User's permissions: \"{2}\". Permissions to be changed: \"{3}\".")
   @RBComment("AccessControlEvent.NOT_AUTHORIZED message: To change permissions for an object, the user must have those permissions and does not.")
   @RBArgComment0("The user who is attempting the operation")
   @RBArgComment1("The object for which the permissions are required")
   @RBArgComment2("List of permissions the user has")
   @RBArgComment3("List of permissions the user is attempting to change")
   public static final String CHANGE_PERMISSIONS_NOT_PERMITTED = "20";

   @RBEntry("The object identifier for \"{0}\" must be assigned to a persistable object.")
   @RBComment("WTPropertyVetoException: An ObjectReference attribute contains an ObjectIdentifier that has not been assigned to any Persistable object.")
   @RBArgComment0("The attribute name")
   public static final String UNASSIGNED_REFERENCE = "21";

   @RBEntry("(Deleted)")
   @RBComment("Used in place of the display name for a referenced object that no longer exists.")
   public static final String DELETED_OBJECT = "22";

   @RBEntry("The \"{0}\" parameter value cannot be set to 0.")
   @RBComment("WTInvalidParameterException: A parameter value of zero is passed to a method requiring a nonzero value.")
   @RBArgComment0("Name of parameter")
   public static final String NONZERO_PARAMETER = "23";

   @RBEntry("The \"{0}\" parameter value cannot be empty or null.")
   @RBComment("WTInvalidParameterException: An empty or null value is passed to a method for a required parameter.")
   @RBArgComment0("Name of parameter")
   public static final String EMPTY_OR_NULL_PARAMETER = "24";

   @RBEntry("The \"{0}\" parameter value cannot be a deleted principal. Principal reference: {1}")
   @RBComment("WTInvalidParameterException: A parameter value specifying a deleted principal (a principal object marked as disabled) is passed to a method requiring an enabled principal.")
   @RBArgComment0("Name of parameter")
   @RBArgComment1("Reference to the specified principal")
   public static final String DELETED_PRINCIPAL = "25";

   @RBEntry("ATTENTION:  Security labels are not valid. An object could not be accessed because its security labels are not valid. Report this issue to your administrator.")
   @RBComment("WTRuntimeException: The security labels for an object are accessed and they are not valid. The string encoded representation of the set of labels has an unexpected version number or is in an unexpected format.")
   public static final String SECURITY_LABELS_NOT_VALID = "26";

   @RBEntry("ATTENTION:  A security labels configuration error was detected. Report this issue to your administrator.")
   @RBComment("WTRuntimeException: The security labels for an object are accessed and they include undefined label names and/or values.")
   public static final String SECURITY_LABELS_CONFIG_ERROR = "27";

   @RBEntry("ATTENTION:  Security labels could not be set. A security label value could not be set for the object because it is in a personal folder or workspace and the value would restrict the owner's access rights: {0} = {1}")
   @RBComment("AccessControlException: The security labels are being set for one or more objects in a personal cabinet and one or more of the specified label values restrict the cabinet owner")
   @RBArgComment0("localizable security label name")
   @RBArgComment1("localizable security label value")
   public static final String SECURITY_LABELS_RESTRICT_OWNER_ACCESS = "28";

   @RBEntry("{0} must be called within a transaction for persisted objects.")
   @RBComment("WTException: There is no active transaction for a method that requires one when dealing with persisted objects.")
   @RBArgComment0("method name")
   public static final String TRANSACTION_REQUIRED = "29";

   @RBEntry("\"{0}\" is not authorized to modify a security label for \"{1}\". {2}")
   @RBComment("AccessControlEvent.NOT_AUTHORIZED message: A user tries to modify the security labels for an object and is not authorized by one or more of the security label values being modified.")
   @RBArgComment0("The user who is attempting the operation")
   @RBArgComment1("The object for which authorization is required")
   @RBArgComment2("Collection of localizable RESTRICTED_LABEL_INFO messages")
   public static final String MODIFY_SECURITY_LABELS_NOT_PERMITTED = "30";

   @RBEntry("Name: \"{0}\", Value: \"{1}\", Authorized Participant: \"{2}\"")
   @RBComment("Detailed information to be included in the MODIFY_SECURITY_LABELS_NOT_PERMITTED message.")
   @RBArgComment0("localizable security label name")
   @RBArgComment1("localizable security label value")
   @RBArgComment2("authorized participant's distinguished name")
   public static final String RESTRICTED_LABEL_INFO = "31";

   @RBEntry("The \"{0}\" parameter value cannot be a persistent object.")
   @RBComment("WTInvalidParameterException: A parameter value specifying a persistent object is passed to a method requiring a non-persistent object.")
   @RBArgComment0("Name of parameter")
   public static final String PERSISTENT_OBJECT = "32";

   @RBEntry("The \"{0}\" parameter value cannot contain persistent objects in the values for the map.")
   @RBComment("WTInvalidParameterException: A parameter value specifying a map containing one or more persistent objects in the values is passed to a method requiring non-persistent objects.")
   @RBArgComment0("Name of parameter")
   public static final String PERSISTENT_VALUES = "33";

   @RBEntry("The \"{0}\" parameter value cannot contain non-persistent objects in the keys or values for the map.")
   @RBComment("WTInvalidParameterException: A parameter value specifying a map containing one or more non-persistent objects in the keys or values is passed to a method requiring persistent objects.")
   @RBArgComment0("Name of parameter")
   public static final String NON_PERSISTENT_KEYS_OR_VALUES = "34";

   @RBEntry("Participant")
   @RBComment("display value for the WTAclEntry.ALL_EXCEPT_PRINCIPAL boolean attribute (false); the entry is for the specified principal (participant)")
   public static final String PRINCIPAL = "35";

   @RBEntry("All except participant")
   @RBComment("display value for the WTAclEntry.ALL_EXCEPT_PRINCIPAL boolean attribute (true); the entry is for all principals except the specified principal (participant)")
   public static final String ALL_EXCEPT_PRINCIPAL = "36";

   @RBEntry("An access control rule is already defined for Domain: \"{0}\", Type: \"{1}\", State: \"{2}\", Principal: \"{3}\", Applies To: \"{4}\", Grant Permissions: \"{5}\", Deny Permissions: \"{6}\", Absolute Deny Permissions: \"{7}\".")
   @RBComment("PolicyRuleAlreadyExistsException: An attempt was made to create an access control rule that already exists")
   @RBArgComment0("Path name of the domain the rule applies to")
   @RBArgComment1("External type identifier of the type in the rule")
   @RBArgComment2("State name (values defined in wt.lifecycle.StateRB) of the state in the rule")
   @RBArgComment3("Name of the principal in the rule")
   @RBArgComment4("POLICY_PRINCIPAL or POLICY_ALL_EXCEPT_PRINCIPAL resource string")
   @RBArgComment5("List of permissions granted by the existing rule, values defined in AccessPermissionRB")
   @RBArgComment6("List of permissions denied by the existing rule, values defined in AccessPermissionRB")
   @RBArgComment7("List of permissions absolutely denied by the existing rule, values defined in AccessPermissionRB")
   public static final String POLICY_RULE_ALREADY_EXISTS = "37";

   @RBEntry("An access control rule is not defined for Domain: \"{0}\", Type: \"{1}\", State: \"{2}\", Principal: \"{3}\", Applies To: \"{4}\".")
   @RBComment("PolicyRuleDoesNotExistException: An attempt was made to delete an access control rule that does not exist")
   @RBArgComment0("Path name of the domain the rule applies to")
   @RBArgComment1("External type identifier of the type in the rule")
   @RBArgComment2("State name (values defined in wt.lifecycle.StateRB) of the state in the rule")
   @RBArgComment3("Name of the principal in the rule")
   @RBArgComment4("POLICY_PRINCIPAL or POLICY_ALL_EXCEPT_PRINCIPAL resource string")
   public static final String POLICY_RULE_DOES_NOT_EXIST = "38";

   @RBEntry("Principal")
   @RBComment("The same as PRINCIPAL resource string except for use in Policy Administration which refers to principal rather than participant")
   public static final String POLICY_PRINCIPAL = "39";

   @RBEntry("All except principal")
   @RBComment("The same as ALL_EXCEPT_PRINCIPAL resource string except for use in Policy Administration which refers to principal rather than participant")
   public static final String POLICY_ALL_EXCEPT_PRINCIPAL = "40";

   @RBEntry("The \"{0}\" parameter value cannot be set to true when the specified principal is the pseudo role \"{1}\".")
   @RBComment("WTInvalidParameterException: A parameter value of true is not supported for a parameter when the specified principal is a pseudo role. ")
   @RBArgComment0("Name of parameter")
   @RBArgComment1("Name of pseudo role")
   public static final String ALL_EXCEPT_PRINCIPAL_NOT_FOR_PSEUDO_ROLES = "41";

   @RBEntry("The \"{0}\" parameter value must be empty or null when the specified principal is the pseudo role \"{1}\".")
   @RBComment("WTInvalidParameterException: An empty or null parameter value is required for a parameter when the specified principal is a pseudo role.")
   @RBArgComment0("Name of parameter")
   @RBArgComment1("Name of pseudo role")
   public static final String ABSOLUTE_DENY_NOT_FOR_PSEUDO_ROLES = "42";

   @RBEntry("At least one applicable permission must be specified by the values for parameters: {0}, {1}, {2}  ")
   @RBComment("WTInvalidParameterException: A parameter value specifying an applicable permission must be specified for at least one of the named parameters.")
   @RBArgComment0("Name of parameter")
   @RBArgComment1("Name of parameter")
   @RBArgComment2("Name of parameter")
   public static final String APPLICABLE_PERMISSION_REQUIRED = "43";

   @RBEntry("Some participants are not available for deletion because they are referenced by access control rules. Contact your administrator for assistance.")
   @RBComment("AccessControlException: An attempt was made to delete a participant that is referenced by an access control rule.")
   public static final String PARTICIPANTS_NOT_AVAILABLE_FOR_DELETION = "44";

   @RBEntry("Type cannot be deleted because it is used in the Security Labels configuration: \"{0}\".")
   @RBComment("Message displayed in the exception thrown when user attempts to delete a type which is specified as the Authorization Agreement type for a Security Label value.")
   @RBArgComment0("Name of the type whose deletion was prevented")
   public static final String CANNOT_DELETE_SL_RELATED_TYPE = "45";

   @RBEntry("Type cannot be renamed because it is used in the Security Labels configuration: \"{0}\".")
   @RBComment("Message displayed in the exception thrown when user attempts to rename a type which is specified as the Authorization Agreement type for a Security Label value.")
   @RBArgComment0("Name of the type whose rename was prevented")
   public static final String CANNOT_RENAME_SL_RELATED_TYPE = "46";

   @RBEntry("The security label {0} does not exist.")
   @RBComment("WTInvalidParameterException: A security label name that is not defined in the security labels configuration file is passed to a method for a required parameter.")
   @RBArgComment0("Name of security label")
   public static final String SECURITY_LABEL_NOT_EXISTS = "47";

   @RBEntry("The security label for authoring application parameter {0} does not exist.")
   @RBComment("WTInvalidParameterException: A security label does not exist for an authoring application parameter name passed to a method for a required parameter.")
   @RBArgComment0("Name of parameter")
   public static final String SECURITY_LABEL_NOT_EXISTS_FOR_PARAMETER = "48";

   @RBEntry("The security label {0} is not a custom label.")
   @RBComment("WTInvalidParameterException: A security label name that is not a custom label is passed to a method expecting a custom label.")
   @RBArgComment0("Name of parameter")
   public static final String SECURITY_LABEL_NOT_CUSTOM = "49";

   @RBEntry("Security label values cannot be set because an error was detected.  Contact your administrator.")
   @RBComment("WTRuntimeException: An exception was encountered when calling a custom translator class provided by a customizer.")
   public static final String SECURITY_LABELS_TRANSLATOR_ERROR = "50";

   @RBEntry("ATTENTION:  Invalid Value \n \nThe value {0} is not a valid value for {1}.")
   @RBComment("WTRuntimeException: An exception was encountered when setting a security label. The user set an invalid value such as NULL.")
   @RBArgComment0("Value of Security Label that the user attempted to set")
   @RBArgComment1("Name of Security Label that the user attempted to set")
   public static final String SECURITY_LABELS_VALUE_INVALID = "51";

   @RBEntry("ATTENTION:  Invalid Characters \n \nThe value for {0} is invalid. The value cannot contain the following characters: , or =")
   @RBComment("WTRuntimeException: An exception was encountered when setting a security label.")
   @RBArgComment0("Name of Security Label that the user attempted to set")
   public static final String SECURITY_LABELS_CHARACTERS_INVALID = "52";

   @RBEntry("The participant running the UpdateSecurityLabels command line utility does not have the necessary authorization. Current authorized participants:  {1}.")
   @RBComment("AccessControlEvent.NOT_AUTHORIZED message: The user does not have a necessary permission to run the UpdateSecurityLabels tool")
   @RBArgComment1("The authorized participants allowed to run the UpdateSecurityLabels tool")
   public static final String NOT_AUTHORIZED_FOR_UPDATE_SECURITY_LABELS_TOOL = "NOT_AUTHORIZED_FOR_UPDATE_SECURITY_LABELS_TOOL";

   @RBEntry("{0} is not authorized to run the wt.access.UpdateSecurityLabels command line utility. The user must be an authorized participant configured in the wt.access.UpdateSecurityLabels.authorizedParticipant property. Current authorized participants: {1}")
   @RBComment("AccessControlEvent.NOT_AUTHORIZED message: The user does not have a necessary permission to run the UpdateSecurityLabels tool")
   @RBArgComment0("User running the tool")
   @RBArgComment1("The authorized participants allowed to run the UpdateSecurityLabels tool")
   public static final String NOT_AUTHORIZED_EXCEPTION_FOR_UPDATE_SECURITY_LABELS_TOOL = "NOT_AUTHORIZED_EXCEPTION_FOR_UPDATE_SECURITY_LABELS_TOOL";

}
