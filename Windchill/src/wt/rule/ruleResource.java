/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.rule;

import wt.util.resource.*;

@RBUUID("wt.rule.ruleResource")
public final class ruleResource extends WTListResourceBundle {
   /**
    * an attempt was made to apply a rule that accesses information on the
    * current project when the current project is not a Project2 object (it
    * is either null or an Organization)
    **/
   @RBEntry("You must select a project before applying a rule that accesses project or team information.")
   public static final String NO_CURRENT_PROJECT = "100";

   /**
    * an illegal value was assigned to an attribute of an XML element
    * element in a rule specification
    **/
   @RBEntry("The value \"{0}\" is not a legal value for the \"{1}\" attribute of a \"{2}\" element in a rule specification.")
   public static final String ILLEGAL_ATTR_VALUE = "110";

   /**
    * an attempt was made to apply a rule to an object with an applier delegate that
    * does not support that type of object
    **/
   @RBEntry("The wrong delegate was used to apply the {0} {1} rule to {2}.  The services.properties file needs to be configured to specify the correct rule applier delegate.")
   public static final String OBJECT_TYPE_NOT_SUPPORTED_BY_DELEGATE = "120";

   /**
    * the character encoding used for the rule specification is not supported
    **/
   @RBEntry("The character encoding {0} used to encode the {1} {2} rule specification is not supported.")
   public static final String UNSUPPORTED_CHAR_ENCODING = "130";

   /**
    * the root element of the rule specification is not correct
    **/
   @RBEntry("The root element {0} of the {1} {2} rule specification is incompatible with the configured rule applier delegate.  Either the root element is incorrect or the services.properties file is not configured properly.")
   public static final String ROOT_ELEMENT_NOT_SUPPORTED_BY_DELEGATE = "140";

   /**
    * the rule's XML specification is either invalid or not well-formed
    **/
   @RBEntry("The XML specification for the {0} {1} rule is either not well-formed or invalid.")
   public static final String INVALID_RULE_SPEC = "150";

   /**
    * an unknown problem occurred while the rule's XML specification was being parsed
    **/
   @RBEntry("An unknown error occurred while parsing the {0} {1} rule.")
   public static final String UNKNOWN_PARSE_ERROR = "160";

   /**
    * an attempt was made to query the rule history for the rule that was
    * applied to produce a non-persistent object - an object must be persistent
    * in order for history to exist for it
    **/
   @RBEntry("The object {0} is not persistent and therefore it does not have any rule history entries.")
   public static final String QUERY_FOR_RULE_HISTORY_OF_NON_PERSISTENT = "170";

   /**
    * an attempt was made to store a rule history entry that references a
    * non-persistent object - an object must be persistent in order for
    * history to exist for it
    **/
   @RBEntry("A rule history entry cannot be created for {0} because it is not persistent.")
   public static final String CREATE_RULE_HISTORY_FOR_NON_PERSISTENT = "180";

   /**
    * more than one rule with the same name and type exists in the database
    **/
   @RBEntry("More than one {0} rule named {1} exists in the database.")
   public static final String TOO_MANY_RULE_WITH_SAME_NAME_AND_TYPE = "200";

   /**
    * the rule is not in the System domain
    **/
   @RBEntry("The rule must be stored in the {0} domain.  Its current domain is {1}.")
   public static final String RULE_DOMAIN_INVALID = "210";

   /**
    * an InstanceBasedRule that references a deleted object cannot be enabled
    **/
   @RBEntry("The {0} {1} rule cannot be enabled because it references a deleted object.")
   public static final String OBJREF_DELETED = "220";

   /**
    * an InstanceBasedRule's reference cannot be null
    **/
   @RBEntry("The {0} {1} rule cannot reference a null object.")
   public static final String OBJREF_NULL = "230";

   /**
    * there can only be one rule for each rule group that is enabled and the default rule
    * A rule group for InstanceBasedRules consists of all InstanceBasedRules in a domain
    * that share the same ruleType and the same objRef.  A rule group for TypeBasedRules
    * consists of all TypeBasedRules in a domain that share the same ruleType and the
    * same objType.
    **/
   @RBEntry("An enabled default {0} rule already exists that references {1}.")
   public static final String DEFAULT_ENABLED_RULE_ALREADY_EXISTS = "240";

   /**
    * if more than one rule already exists in the database, then the database
    * has been corrupted
    **/
   @RBEntry("More than one enabled default {0} rule exists that references {1}.")
   public static final String TOO_MANY_DEFAULT_ENABLED_RULES = "245";

   /**
    * an attempt was made to delete a rule that still has rule history entries
    **/
   @RBEntry("The {0} {1} rule cannot be deleted because it still has rule histories entries.  The rule history entries that reference the rule must be deleted before the rule can be deleted.")
   public static final String CANNOT_DELETE_RULE_WITH_HISTORY_ENTRIES = "250";

   /**
    * given the context, the type of the supplied rule is incorrect
    **/
   @RBEntry("The type of the {0} rule is incorrect.  The rule's type is {1}.  The expected rule type is {2}.")
   public static final String WRONG_RULE_TYPE = "260";

   /**
    * a lifeccycle with the specified name was not found
    **/
   @RBEntry("A lifecycle named '{0}' was not found.")
   public static final String LIFECYCLE_NOT_FOUND = "270";

   /**
    * RuleHistoryEntry Exception handling for history entry creation
    **/
   @RBEntry("History entry cannot be created because '{0}' is not a rule.")
   public static final String NON_RULE_HISTORY = "271";

   /**
    * InitRuleHelper Exception handling message
    **/
   @RBEntry("Internal Error: A context cannot be null.")
   public static final String CANNOT_BE_NULL_CONTAINER_REF = "272";

   /**
    * QueryUtils Exception handling message
    **/
   @RBEntry(" Internal Error: Problem building database query.")
   public static final String COULD_NOT_BUILD_QUERY_SPEC = "273";

   /**
    * TypeBasedRule Exception handling for non-valid rule type
    **/
   @RBEntry("This is not a valid Rule Object Type!")
   public static final String WRONG_OBJ_TYPE = "280";

   /**
    * TypeHelper Exception handling for value too long
    **/
   @RBEntry("Exception is generated due to value too long. See detail below:")
   public static final String VALUE_TOO_LONG = "282";

   /**
    * QueryUtils Exception handling message
    **/
   @RBEntry(" Internal Error: Problem setting the name of the dummy rule object.")
   public static final String PROBLEM_WITH_DUMMY_RULE = "290";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry(" Argument passed in is NOT Boolean.")
   public static final String NOT_BOOLEAN = "300";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry(" Two arguments passed in are of different type.")
   public static final String TWO_DIFFERENT_TYPE = "310";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry(" The number of arguments passed in is not two.")
   public static final String NOT_TWO = "320";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry(" The number of arguments passed in is zero.")
   public static final String NO_ARG_PASSED = "330";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry(" The number of arguments passed in is not an odd number.Case Branch requires an odd number of args.")
   public static final String NOT_ODD = "340";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry(" The number of arguments passed in is not three.")
   public static final String NOT_THREE = "350";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry(" The argument passed in is not a String, but a String is required.")
   public static final String NOT_STRING = "360";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry(" The case number {0} passed in is not a Boolean, but a Boolean is required.")
   public static final String NOT_BOOLEAN_CASE = "370";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry(" The first argument passed in {0} is NULL, but a not NULL value is required.")
   public static final String NULL_FIRST_ARG = "380";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry(" The second argument passed in {0} is NULL, but a not NULL value is required.")
   public static final String NULL_SECOND_ARG = "382";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry(" The top-level algorithm of AttrConstraint tag has to be GatherAttributeConstraints algorithm.")
   public static final String ATTRCONSTRAINT_TAG = "386";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry(" Rule argument {0} is null, please make sure the argument passed is not null, then try again.")
   public static final String NULL_ARG = "387";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry(" The first argument passed in {0} is invalid type id, or the second argument passed in {1} is invalid attribute type id.")
   public static final String INVALID_ARG = "388";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry(" The first argument passed in {0} is invalid type id.")
   public static final String INVALID_TYPE_ID = "389";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry(" The second argument passed in {0} is invalid attribute type id.")
   public static final String INVALID_ATTR_ID = "390";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry(" The rule algorithm requires {0} arguments, but was supplied with {1} arguments.")
   public static final String INVALID_ARGUMNET_NUMBER = "400";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry("Type {0} is invalid for rule algorithm argument {1}. The type must be {2}.")
   public static final String INVALID_ARGUMNET_TYPE = "401";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry("A single argument must be supplied when the algorithm is not specified.")
   public static final String NEED_SINGLE_ARGUMNET = "402";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry("The value \"{0}\" for rule algorithm argument {1} is invalid. The value must be an integer greater than or equal to 0.")
   public static final String ARGUMNET_INVALID_FORMAT = "403";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry("A single argument must be supplied for the algorithm {0}")
   public static final String ALGORITHM_NEED_ARGUMENT = "404";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry("A folder name '{0}' passed to the algorithm {1} does not exist.")
   public static final String FOLDER_NOT_FOUND = "405";

   /**
    * Every Variable reference have to have a corresponding variable definition.
    **/
   @RBEntry("Variable reference {0} does not have a corresponding variable definition.")
   public static final String NEED_VARIABLE_DEFINITION = "604";

   /**
    * Rule name is required
    **/
   @RBEntry("A name is required for creating object initialization rules.")
   @RBComment("Message given when the user tries to create/update a rule with no name.")
   public static final String NAME_REQUIRED = "605";

   @RBEntry("Object type is required for creating object initialization rules.")
   @RBComment("Message given when the user tries to create/update a rule with no object type.")
   public static final String OBJECT_TYPE_REQUIRED = "606";

   @RBEntry("A XML file is required for creating object initialization rules.")
   @RBComment("Message given when the user tries to create/update a rule with no XML file.")
   public static final String FILE_PATH_REQUIRED = "FILE_PATH_REQUIRED";

   @RBEntry("Cannot create an object initialization rule for this object type since Arbortext Content Manager is installed.")
   public static final String CANNOT_CREATE_OIR = "608";

   @RBEntry("XML file is too large:")
   public static final String XML_FILE_TOO_LARGE = "609";

   @RBEntry("Container cannot be null.")
   public static final String CONTAINER_CANNOT_BE_NULL = "610";

   /**
    * Message about invalid object initialization rule
    **/
   @RBEntry("ATTENTION: An object initialization rule could not be applied because an error was detected. Report this issue to your administrator.")
   public static final String RULE_CANNOT_APPLY = "611";

   @RBEntry("Type {0} is not supported by object initialization rules.")
   @RBComment("Message given when target type given for createRule is invalid.")
   public static final String INVALID_TARGET_TYPE = "INVALID_TARGET_TYPE";

   @RBEntry("{0} is not found.")
   @RBComment("Message given when XML File for createRule does not exist.")
   public static final String FILE_NOT_FOUND = "FILE_NOT_FOUND";

   @RBEntry("The content of the selected XML file is not valid. A parsing error has occurred.")
   @RBComment("Message given when parsing of the Rule has occurred for call to createRule or modifyRule.")
   public static final String PARSING_ERROR = "PARSING_ERROR";

   @RBEntry("A system error has occurred. Please contact your administrator if this issue continues.")
   @RBComment("Message given when createRule or modifyRule server code runs into an error.")
   public static final String NULL_PARAMETER = "NULL_PARAMETER";
}
