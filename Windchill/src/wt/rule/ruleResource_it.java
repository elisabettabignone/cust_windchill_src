/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.rule;

import wt.util.resource.*;

@RBUUID("wt.rule.ruleResource")
public final class ruleResource_it extends WTListResourceBundle {
   /**
    * an attempt was made to apply a rule that accesses information on the
    * current project when the current project is not a Project2 object (it
    * is either null or an Organization)
    **/
   @RBEntry("Selezionare un progetto prima di applicare una regola che accede alle informazioni di progetto o team.")
   public static final String NO_CURRENT_PROJECT = "100";

   /**
    * an illegal value was assigned to an attribute of an XML element
    * element in a rule specification
    **/
   @RBEntry("Il valore \"{0}\" non è valido per l'attributo \"{1}\" di un elemento \"{2}\" di una regola.")
   public static final String ILLEGAL_ATTR_VALUE = "110";

   /**
    * an attempt was made to apply a rule to an object with an applier delegate that
    * does not support that type of object
    **/
   @RBEntry("È stato usato un delegato non corretto per applicare la regola {1} {0} a {2}.  Il file services.properties deve essere configurato in modo da specificare il delegato corretto.")
   public static final String OBJECT_TYPE_NOT_SUPPORTED_BY_DELEGATE = "120";

   /**
    * the character encoding used for the rule specification is not supported
    **/
   @RBEntry("La codifica dei caratteri {0} usata per la regola {2} {1} non è supportata")
   public static final String UNSUPPORTED_CHAR_ENCODING = "130";

   /**
    * the root element of the rule specification is not correct
    **/
   @RBEntry("L'elemento radice {0} della regola {2} {1} non è compatibile con il delegato di applicazione regole configurato. L'elemento radice non è corretto oppure il file services.properties non è configurato in modo corretto.")
   public static final String ROOT_ELEMENT_NOT_SUPPORTED_BY_DELEGATE = "140";

   /**
    * the rule's XML specification is either invalid or not well-formed
    **/
   @RBEntry("La specifica XML per la regola {1} {0} non è completa o non è valida.")
   public static final String INVALID_RULE_SPEC = "150";

   /**
    * an unknown problem occurred while the rule's XML specification was being parsed
    **/
   @RBEntry("Errore sconosciuto durante l'analisi della regola {1} {0}.")
   public static final String UNKNOWN_PARSE_ERROR = "160";

   /**
    * an attempt was made to query the rule history for the rule that was
    * applied to produce a non-persistent object - an object must be persistent
    * in order for history to exist for it
    **/
   @RBEntry("L'oggetto {0} non è persistente e perciò non contiene voci di cronologia regole.")
   public static final String QUERY_FOR_RULE_HISTORY_OF_NON_PERSISTENT = "170";

   /**
    * an attempt was made to store a rule history entry that references a
    * non-persistent object - an object must be persistent in order for
    * history to exist for it
    **/
   @RBEntry("Impossibile creare voce di cronologia regole per {0} perché non persistente.")
   public static final String CREATE_RULE_HISTORY_FOR_NON_PERSISTENT = "180";

   /**
    * more than one rule with the same name and type exists in the database
    **/
   @RBEntry("Nel database esiste più di una regola {0} di nome {1}.")
   public static final String TOO_MANY_RULE_WITH_SAME_NAME_AND_TYPE = "200";

   /**
    * the rule is not in the System domain
    **/
   @RBEntry("La regola deve essere memorizzata nel dominio {0}. Il dominio attuale è {1}.")
   public static final String RULE_DOMAIN_INVALID = "210";

   /**
    * an InstanceBasedRule that references a deleted object cannot be enabled
    **/
   @RBEntry("La regola {1} {0} non può essere attivata perché fa riferimento a un oggetto eliminato.")
   public static final String OBJREF_DELETED = "220";

   /**
    * an InstanceBasedRule's reference cannot be null
    **/
   @RBEntry("La regola {1} {0} non può referenziare un oggetto nullo.")
   public static final String OBJREF_NULL = "230";

   /**
    * there can only be one rule for each rule group that is enabled and the default rule
    * A rule group for InstanceBasedRules consists of all InstanceBasedRules in a domain
    * that share the same ruleType and the same objRef.  A rule group for TypeBasedRules
    * consists of all TypeBasedRules in a domain that share the same ruleType and the
    * same objType.
    **/
   @RBEntry("Una regola di default {0} attivata e che fa riferimento a {1} esiste già.")
   public static final String DEFAULT_ENABLED_RULE_ALREADY_EXISTS = "240";

   /**
    * if more than one rule already exists in the database, then the database
    * has been corrupted
    **/
   @RBEntry("Esiste più di una regola di default {0} attivata e che fa riferimento a {1}.")
   public static final String TOO_MANY_DEFAULT_ENABLED_RULES = "245";

   /**
    * an attempt was made to delete a rule that still has rule history entries
    **/
   @RBEntry("La regola {1} {0} non può essere eliminata perché contiene ancora voci di cronologia regole. Le voci di cronologia regole che referenziano la regola devono essere eliminate prima dell'eliminazione della regola.")
   public static final String CANNOT_DELETE_RULE_WITH_HISTORY_ENTRIES = "250";

   /**
    * given the context, the type of the supplied rule is incorrect
    **/
   @RBEntry("Il tipo di regola {0} non è corretto. Il tipo di regola è {1}.  Il tipo atteso era {2}.")
   public static final String WRONG_RULE_TYPE = "260";

   /**
    * a lifeccycle with the specified name was not found
    **/
   @RBEntry("Impossibile trovare il ciclo di vita '{0}'.")
   public static final String LIFECYCLE_NOT_FOUND = "270";

   /**
    * RuleHistoryEntry Exception handling for history entry creation
    **/
   @RBEntry("Impossibile creare la voce Cronologia perché '{0}' non è una regola.")
   public static final String NON_RULE_HISTORY = "271";

   /**
    * InitRuleHelper Exception handling message
    **/
   @RBEntry("Errore interno: il contesto non può essere nullo.")
   public static final String CANNOT_BE_NULL_CONTAINER_REF = "272";

   /**
    * QueryUtils Exception handling message
    **/
   @RBEntry(" Errore interno: problema durante la creazione dell'interrogazione del database.")
   public static final String COULD_NOT_BUILD_QUERY_SPEC = "273";

   /**
    * TypeBasedRule Exception handling for non-valid rule type
    **/
   @RBEntry("Tipo di oggetto regola non valido.")
   public static final String WRONG_OBJ_TYPE = "280";

   /**
    * TypeHelper Exception handling for value too long
    **/
   @RBEntry("Eccezione dovuta a valore troppo lungo. Vedere di seguito per i dettagli:")
   public static final String VALUE_TOO_LONG = "282";

   /**
    * QueryUtils Exception handling message
    **/
   @RBEntry(" Errore interno: si è verificato un problema durante l'impostazione del nome per l'oggetto regola fittizio.")
   public static final String PROBLEM_WITH_DUMMY_RULE = "290";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry(" L'argomento trasmesso non è booleano.")
   public static final String NOT_BOOLEAN = "300";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry(" I due argomenti passati sono di tipo differente.")
   public static final String TWO_DIFFERENT_TYPE = "310";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry(" Il numero degli argomenti passati non è due.")
   public static final String NOT_TWO = "320";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry(" Il numero degli argomenti passati è zero.")
   public static final String NO_ARG_PASSED = "330";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry(" Il numero degli argomenti passati non è dispari. Case Branch necessita di argomenti in numero dispari.")
   public static final String NOT_ODD = "340";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry(" Il numero degli argomenti non è tre.")
   public static final String NOT_THREE = "350";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry("L'argomento passato non è una stringa. È richiesta una stringa.")
   public static final String NOT_STRING = "360";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry(" Il caso numero {0} passato non è un valore booleano. Richiesto valore booleano.")
   public static final String NOT_BOOLEAN_CASE = "370";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry(" Il primo argomento passato in {0} è NULL. Richiesto valore non nullo.")
   public static final String NULL_FIRST_ARG = "380";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry(" Il secondo argomento passato in {0} è NULL. Richiesto valore non nullo.")
   public static final String NULL_SECOND_ARG = "382";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry("È necessario che l'algoritmo di livello superiore del tag AttrConstraint sia GatherAttributeConstraints.")
   public static final String ATTRCONSTRAINT_TAG = "386";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry(" L'argomento di regola {0} è nullo. Accertare che l'argomento passato non sia nullo e riprovare.")
   public static final String NULL_ARG = "387";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry(" Il primo argomento passato in {0} è un ID di tipo non valido o il secondo argomento passato in {1} è un ID di tipo attributo non valido.")
   public static final String INVALID_ARG = "388";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry(" Il primo argomento passato in {0} è un ID di tipo non valido.")
   public static final String INVALID_TYPE_ID = "389";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry(" Il secondo argomento passato in {0} è un ID di tipo attributo non valido.")
   public static final String INVALID_ATTR_ID = "390";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry(" L'algoritmo regola richiede {0} argomenti, ma sono stati forniti {1} argomenti.")
   public static final String INVALID_ARGUMNET_NUMBER = "400";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry("Tipo {0} non valido per l'argomento di regola algoritmo {1}. Il tipo deve essere {2}.")
   public static final String INVALID_ARGUMNET_TYPE = "401";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry("Fornire un solo argomento quando l'algoritmo non è specificato.")
   public static final String NEED_SINGLE_ARGUMNET = "402";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry("Il valore \"{0}\" dell'argomento di regola algoritmo {1} non è valido. Il valore deve essere un intero maggiore o uguale a zero.")
   public static final String ARGUMNET_INVALID_FORMAT = "403";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry("Specificare un solo argomento per l'algoritmo {0}")
   public static final String ALGORITHM_NEED_ARGUMENT = "404";

   /**
    * Rule Algorithm Exception handling message
    **/
   @RBEntry("Il nome della cartella '{0}' passato all'algoritmo {1} non esiste.")
   public static final String FOLDER_NOT_FOUND = "405";

   /**
    * Every Variable reference have to have a corresponding variable definition.
    **/
   @RBEntry("Il riferimento variabile {0} non dispone di una definizione variabile corrispondente.")
   public static final String NEED_VARIABLE_DEFINITION = "604";

   /**
    * Rule name is required
    **/
   @RBEntry("Il nome è obbligatorio per la creazione di regole di inizializzazione oggetto.")
   @RBComment("Message given when the user tries to create/update a rule with no name.")
   public static final String NAME_REQUIRED = "605";

   @RBEntry("Il tipo di oggetto è obbligatorio per la creazione di regole di inizializzazione oggetto.")
   @RBComment("Message given when the user tries to create/update a rule with no object type.")
   public static final String OBJECT_TYPE_REQUIRED = "606";

   @RBEntry("Il file XML è obbligatorio per la creazione di regole di inizializzazione oggetto.")
   @RBComment("Message given when the user tries to create/update a rule with no XML file.")
   public static final String FILE_PATH_REQUIRED = "FILE_PATH_REQUIRED";

   @RBEntry("Impossibile creare una regola di inizializzazione oggetto per questo tipo di oggetto in quanto è installato Arbortext Content Manager.")
   public static final String CANNOT_CREATE_OIR = "608";

   @RBEntry("Il file XML è troppo grande:")
   public static final String XML_FILE_TOO_LARGE = "609";

   @RBEntry("Il contenitore non può essere nullo.")
   public static final String CONTAINER_CANNOT_BE_NULL = "610";

   /**
    * Message about invalid object initialization rule
    **/
   @RBEntry("ATTENZIONE: è stato rilevato un errore nella regola di inizializzazione dell'oggetto che non ne consente l'applicazione. Segnalare il problema all'amministratore.")
   public static final String RULE_CANNOT_APPLY = "611";

   @RBEntry("Il tipo {0} non è supportato dalle regole di inizializzazione oggetto.")
   @RBComment("Message given when target type given for createRule is invalid.")
   public static final String INVALID_TARGET_TYPE = "INVALID_TARGET_TYPE";

   @RBEntry("Impossibile trovare {0}.")
   @RBComment("Message given when XML File for createRule does not exist.")
   public static final String FILE_NOT_FOUND = "FILE_NOT_FOUND";

   @RBEntry("Il contenuto del file XML selezionato non è valido. Si è verificato un errore di analisi.")
   @RBComment("Message given when parsing of the Rule has occurred for call to createRule or modifyRule.")
   public static final String PARSING_ERROR = "PARSING_ERROR";

   @RBEntry("Si è verificato un errore di sistema. Se il problema persiste, contattare l'amministratore.")
   @RBComment("Message given when createRule or modifyRule server code runs into an error.")
   public static final String NULL_PARAMETER = "NULL_PARAMETER";
}
