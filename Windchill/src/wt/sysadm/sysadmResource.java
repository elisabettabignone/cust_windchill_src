/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.sysadm;

import wt.util.resource.*;

@RBUUID("wt.sysadm.sysadmResource")
public final class sysadmResource extends WTListResourceBundle {
   @RBEntry("true")
   public static final String TRUE_STR = "0";

   @RBEntry("false")
   public static final String FALSE_STR = "1";

   @RBEntry("Add")
   public static final String ADD_BUTTON = "2";

   @RBEntry("Delete")
   public static final String DELETE_BUTTON = "3";

   @RBEntry("Submit type error")
   public static final String SUBMIT_TYPE_ERROR = "4";

   @RBEntry("Save property error")
   public static final String SAVE_PROP_ERROR = "5";

   @RBEntry("Sysadm could not find wt.properties")
   public static final String MISS_WTPROPERTIES = "6";

   @RBEntry("Array to sort is null.")
   public static final String NULL_ARRAY = "7";

   @RBEntry("Index to end sort out of bounds.")
   public static final String END_INDEX = "8";

   @RBEntry("Index to begin sort out of bounds.")
   public static final String BEGIN_INDEX = "9";

   @RBEntry("UP")
   public static final String UP_STR = "10";

   @RBEntry("DOWN")
   public static final String DOWN_STR = "11";

   @RBEntry("Server Manager: ")
   public static final String SM_STR = "12";

   @RBEntry("Method Server: ")
   public static final String MS_STR = "13";

   @RBEntry("Server Status")
   public static final String WIND_SRVR = "14";

   @RBEntry("Edit Logs")
   public static final String EDIT_LOGS = "15";

   @RBEntry("Edit Properties")
   public static final String EDIT_PROPS = "16";

   @RBEntry("Edit Preferences")
   public static final String EDIT_PREFS = "17";

   @RBEntry("Queue Manager")
   public static final String QUEUE_MGR = "18";

   @RBEntry("Windchill Home")
   public static final String WIND_HOME = "19";

   @RBEntry("Help")
   public static final String SYS_HELP = "20";

   @RBEntry("Edit")
   public static final String EDIT_STR = "21";

   @RBEntry("log")
   public static final String LOG_STR = "22";

   @RBEntry("properties")
   public static final String PROP_STR = "23";

   @RBEntry("Edit {0}")
   @RBArgComment0(" refers to a filename.")
   public static final String EDIT_FILE = "24";

   @RBEntry("Changes to {0} saved successfully")
   @RBArgComment0(" refers to a filename.")
   public static final String SAVE_SUCCESS = "25";

   @RBEntry("Unable to save changes to {0}.  See the SysAdmin log for details.")
   @RBArgComment0(" refers to a filename.")
   public static final String SAVE_FAILED = "26";

   @RBEntry("System Configurator")
   public static final String TITLE = "27";

   @RBEntry("User Preferences Administrator")
   public static final String USER_PREF_ADMIN = "28";

   @RBEntry("Queue Manager Administrator")
   public static final String QUEUE_MGR_ADMIN = "29";

   @RBEntry("Advanced")
   public static final String SYS_ADV = "30";

   @RBEntry("Basic")
   public static final String SYS_BAS = "31";

   @RBEntry("OK")
   public static final String SYS_OK = "32";

   @RBEntry("Reset")
   public static final String SYS_RESET = "33";

   @RBEntry("Send")
   public static final String SYS_SEND = "34";

   @RBEntry("Property:")
   public static final String SYS_PROP = "35";

   @RBEntry("Search")
   public static final String SYS_SEARCH = "36";

   @RBEntry("Value")
   public static final String SYS_VALUE = "37";

   @RBEntry("Default")
   public static final String SYS_DEFAULT = "38";

   @RBEntry("Read From:")
   public static final String SYS_REDF = "39";

   @RBEntry("Start")
   public static final String SYS_START = "40";

   @RBEntry("End")
   public static final String SYS_END = "41";

   @RBEntry("Number of Messages to Return:")
   public static final String SYS_MES = "42";

   @RBEntry("E-mail To:")
   public static final String SYS_EMAILTO = "43";

   @RBEntry("E-mail Search Results")
   public static final String SYS_EMAIL_SRES = "44";

   @RBEntry("Copy E-mail To:")
   public static final String SYS_CC = "45";

   @RBEntry("Subject:")
   public static final String SYS_SUB = "46";

   @RBEntry("Separate multiple recipients in each field with a space character.")
   public static final String SYS_NOTE = "47";

   @RBEntry("Stop")
   public static final String SYS_STOP = "48";

   @RBEntry("Stop Windchill")
   public static final String SYS_STOPWIND = "49";

   @RBEntry("Start Windchill")
   public static final String SYS_STARTWIND = "50";

   @RBEntry("Restart")
   public static final String SYS_RESTART = "51";

   @RBEntry("Restart Windchill")
   public static final String SYS_RESTARTWIND = "52";

   @RBEntry("Connect to this Server")
   public static final String SYS_CON = "53";

   @RBEntry("Windchill HOME")
   public static final String SYS_HOME = "54";

   @RBEntry("Search For Messages Containing:")
   public static final String SYS_CONT = "55";

   @RBEntry("Failed to save property file")
   public static final String SAVE_FAIL = "56";

   @RBEntry("Property file successfully saved.")
   public static final String SAVE_SUCC = "57";

   @RBEntry("You are not authorized to use the System Configurator. Please request the administrator to add the appropriate user name to the <code>wt.sysadm.Administrators</code> property in the wt.properties file.")
   public static final String AUTH_FAIL = "58";

   @RBEntry("System Administrator Error")
   public static final String ERRTITLE = "59";

   @RBEntry("An error occured while processing your request")
   public static final String ERRMSG = "60";

   @RBEntry("[Close this window]")
   public static final String CLS_WIN = "61";

   @RBEntry("HTTP Request Log Utility Start Page")
   @RBComment("Title for the page that start the log utility")
   public static final String LOG_UTIL_START_TITLE = "62";

   @RBEntry("The Http Request Log Utility has been started.")
   @RBComment("Message stating that the log utility was started successfully")
   public static final String LOG_UTIL_START_SUCC = "63";

   @RBEntry("Click below to view the results.")
   @RBComment("Message indicating how to stop the Log Utility and view it's results")
   public static final String LOG_UTIL_STOP_MES = "64";

   @RBEntry("View all client's results")
   @RBComment("Link to stop the Log Utility and view the results for all clients")
   public static final String LOG_UTIL_CLIENTS_LINK = "65";

   @RBEntry("An error occurred while stopping the Log Utility and collecting the results.")
   @RBComment("Message displayed when the Log Utility fails while stopping and collecting results.")
   public static final String LOG_UTIL_STOP_ERR = "66";

   @RBEntry("No log file specified.  Please define wt.apache.access.log.path before using this utility.")
   @RBComment("Error message displayed when the logPath variable is not defined.")
   public static final String LOG_PATH_ERROR_MSG = "67";

   @RBEntry("View single client results")
   @RBComment("Link to stop the log utility and view the results for a single client.")
   public static final String LOG_UTIL_CLIENT_LINK = "68";

   @RBEntry("Home")
   public static final String HOME = "69";

   @RBEntry("Name")
   public static final String NAME = "70";

   @RBEntry("Search For:")
   public static final String SEARCH_FOR = "71";

   @RBEntry("Go")
   public static final String GO = "72";

   @RBEntry("Display Results From:")
   public static final String DISPLAY_RES_FROM = "73";

   @RBEntry("Please click on the links above to Edit")
   public static final String CLICK_LINK = "74";

   @RBEntry("Current View:")
   public static final String CURR_VIEW = "75";
}
