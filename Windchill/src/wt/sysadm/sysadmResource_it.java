/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.sysadm;

import wt.util.resource.*;

@RBUUID("wt.sysadm.sysadmResource")
public final class sysadmResource_it extends WTListResourceBundle {
   @RBEntry("true")
   public static final String TRUE_STR = "0";

   @RBEntry("false")
   public static final String FALSE_STR = "1";

   @RBEntry("Aggiungi")
   public static final String ADD_BUTTON = "2";

   @RBEntry("Elimina")
   public static final String DELETE_BUTTON = "3";

   @RBEntry("Errore durante l'invio del tipo")
   public static final String SUBMIT_TYPE_ERROR = "4";

   @RBEntry("Errore durante il salvataggio delle proprietà")
   public static final String SAVE_PROP_ERROR = "5";

   @RBEntry("L'amministratore del sistema non ha trovato wt.properties")
   public static final String MISS_WTPROPERTIES = "6";

   @RBEntry("L'array da ordinare è nullo.")
   public static final String NULL_ARRAY = "7";

   @RBEntry("L'indice per terminare l'ordinamento è al di fuori dell'intervallo consentito.")
   public static final String END_INDEX = "8";

   @RBEntry("L'indice per iniziare l'ordinamento è al di fuori dell'intervallo consentito.")
   public static final String BEGIN_INDEX = "9";

   @RBEntry("ATTIVO")
   public static final String UP_STR = "10";

   @RBEntry("INATTIVO")
   public static final String DOWN_STR = "11";

   @RBEntry("Server manager:")
   public static final String SM_STR = "12";

   @RBEntry("Method server: ")
   public static final String MS_STR = "13";

   @RBEntry("Stato server")
   public static final String WIND_SRVR = "14";

   @RBEntry("Modifica file di log")
   public static final String EDIT_LOGS = "15";

   @RBEntry("Modifica proprietà")
   public static final String EDIT_PROPS = "16";

   @RBEntry("Modifica preferenze")
   public static final String EDIT_PREFS = "17";

   @RBEntry("Gestione code")
   public static final String QUEUE_MGR = "18";

   @RBEntry("Windchill - Home")
   public static final String WIND_HOME = "19";

   @RBEntry("Guida")
   public static final String SYS_HELP = "20";

   @RBEntry("Modifica")
   public static final String EDIT_STR = "21";

   @RBEntry("log")
   public static final String LOG_STR = "22";

   @RBEntry("proprietà")
   public static final String PROP_STR = "23";

   @RBEntry("Modifica {0}")
   @RBArgComment0(" refers to a filename.")
   public static final String EDIT_FILE = "24";

   @RBEntry("Le modifiche di {0} sono state salvate")
   @RBArgComment0(" refers to a filename.")
   public static final String SAVE_SUCCESS = "25";

   @RBEntry("Impossibile salvare le modifiche di  {0}.  Vedere il file di log SysAdmin per i dettagli.")
   @RBArgComment0(" refers to a filename.")
   public static final String SAVE_FAILED = "26";

   @RBEntry("Configuratore di sistema")
   public static final String TITLE = "27";

   @RBEntry("Amministrazione preferenze utente")
   public static final String USER_PREF_ADMIN = "28";

   @RBEntry("Amministrazione gestione code")
   public static final String QUEUE_MGR_ADMIN = "29";

   @RBEntry("Avanzata")
   public static final String SYS_ADV = "30";

   @RBEntry("Base")
   public static final String SYS_BAS = "31";

   @RBEntry("OK")
   public static final String SYS_OK = "32";

   @RBEntry("Reimposta")
   public static final String SYS_RESET = "33";

   @RBEntry("Invia")
   public static final String SYS_SEND = "34";

   @RBEntry("Proprietà:")
   public static final String SYS_PROP = "35";

   @RBEntry("Cerca")
   public static final String SYS_SEARCH = "36";

   @RBEntry("Valore")
   public static final String SYS_VALUE = "37";

   @RBEntry("Default")
   public static final String SYS_DEFAULT = "38";

   @RBEntry("Leggi da:")
   public static final String SYS_REDF = "39";

   @RBEntry("Avvia")
   public static final String SYS_START = "40";

   @RBEntry("Fine")
   public static final String SYS_END = "41";

   @RBEntry("Numero messaggi da restituire:")
   public static final String SYS_MES = "42";

   @RBEntry("Invia a:")
   public static final String SYS_EMAILTO = "43";

   @RBEntry("Invia risultati della ricerca")
   public static final String SYS_EMAIL_SRES = "44";

   @RBEntry("Invia in copia a:")
   public static final String SYS_CC = "45";

   @RBEntry("Oggetto:")
   public static final String SYS_SUB = "46";

   @RBEntry("Separare i destinatari in ogni campo con uno spazio.")
   public static final String SYS_NOTE = "47";

   @RBEntry("Arresta")
   public static final String SYS_STOP = "48";

   @RBEntry("Arresta Windchill")
   public static final String SYS_STOPWIND = "49";

   @RBEntry("Arresta Windchill")
   public static final String SYS_STARTWIND = "50";

   @RBEntry("Riavvia")
   public static final String SYS_RESTART = "51";

   @RBEntry("Riavvia Windchill")
   public static final String SYS_RESTARTWIND = "52";

   @RBEntry("Connessione a questo server")
   public static final String SYS_CON = "53";

   @RBEntry("Windchill - Home")
   public static final String SYS_HOME = "54";

   @RBEntry("Cerca i messaggi contenenti:")
   public static final String SYS_CONT = "55";

   @RBEntry("Impossibile salvare il file delle proprietà")
   public static final String SAVE_FAIL = "56";

   @RBEntry("Il file delle proprietà è stato salvato.")
   public static final String SAVE_SUCC = "57";

   @RBEntry("L'utente non dispone di autorizzazione all'utilizzo del Configuratore di sistema. Richiedere all'amministratore di aggiungere il proprio nome utente per la proprietà <code>wt.sysadm.Administrators</code> nel file the wt.properties file.")
   public static final String AUTH_FAIL = "58";

   @RBEntry("Errore amministratore di sistema")
   public static final String ERRTITLE = "59";

   @RBEntry("Errore durante l'elaborazione della richiesta")
   public static final String ERRMSG = "60";

   @RBEntry("[Chiudi la finestra]")
   public static final String CLS_WIN = "61";

   @RBEntry("Pagina di avvio dell'utilità di log delle richieste HTTP")
   @RBComment("Title for the page that start the log utility")
   public static final String LOG_UTIL_START_TITLE = "62";

   @RBEntry("L'utilità di log delle richieste Http è stata avviata.")
   @RBComment("Message stating that the log utility was started successfully")
   public static final String LOG_UTIL_START_SUCC = "63";

   @RBEntry("Fare clic sotto per visualizzare i risultati.")
   @RBComment("Message indicating how to stop the Log Utility and view it's results")
   public static final String LOG_UTIL_STOP_MES = "64";

   @RBEntry("Visualizza tutti i risultati client")
   @RBComment("Link to stop the Log Utility and view the results for all clients")
   public static final String LOG_UTIL_CLIENTS_LINK = "65";

   @RBEntry("Errore durante l'arresto dell'utilità di log e la raccolta dei risultati.")
   @RBComment("Message displayed when the Log Utility fails while stopping and collecting results.")
   public static final String LOG_UTIL_STOP_ERR = "66";

   @RBEntry("Non è stato specificato alcun file di log. Definire  wt.apache.access.log.path prima di usare l'utilità.")
   @RBComment("Error message displayed when the logPath variable is not defined.")
   public static final String LOG_PATH_ERROR_MSG = "67";

   @RBEntry("Visualizza i risultati del client singolo")
   @RBComment("Link to stop the log utility and view the results for a single client.")
   public static final String LOG_UTIL_CLIENT_LINK = "68";

   @RBEntry("Home")
   public static final String HOME = "69";

   @RBEntry("Nome")
   public static final String NAME = "70";

   @RBEntry("Cerca:")
   public static final String SEARCH_FOR = "71";

   @RBEntry("Vai")
   public static final String GO = "72";

   @RBEntry("Mostra risultati di:")
   public static final String DISPLAY_RES_FROM = "73";

   @RBEntry("Per modificare, fare clic sui link sopra specificati")
   public static final String CLICK_LINK = "74";

   @RBEntry("Vista corrente:")
   public static final String CURR_VIEW = "75";
}
