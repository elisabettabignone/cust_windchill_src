/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.prefs;

import wt.util.resource.*;

@RBUUID("wt.prefs.prefsResource")
public final class prefsResource extends WTListResourceBundle {
   /**
    * Entry
    **/
   @RBEntry("System Default")
   public static final String PRIVATE_CONSTANT_0 = "LDefaultPreferences";

   @RBEntry("User")
   public static final String PRIVATE_CONSTANT_1 = "LUserPreferences";

   @RBEntry("Windchill Enterprise")
   public static final String PRIVATE_CONSTANT_2 = "LWindchillEnterprise";

   @RBEntry("Scope")
   public static final String PRIVATE_CONSTANT_3 = "LScope";

   @RBEntry("Below are the configured Windchill System Default Preferences.")
   public static final String PRIVATE_CONSTANT_4 = "LPrefHelpDescription";

   @RBEntry("Selected scope:")
   public static final String PRIVATE_CONSTANT_5 = "LChangeScope";

   @RBEntry("Search for:")
   public static final String PRIVATE_CONSTANT_6 = "LSearchFor";

   @RBEntry("Preferences error")
   public static final String PRIVATE_CONSTANT_7 = "LPreferencesError";

   @RBEntry("There was an error while attempting to update the Preference Value.")
   public static final String PRIVATE_CONSTANT_8 = "LUpdateError";

   @RBEntry("An Authenticated User was not found. This is required by the Preferences Administration Interface. Please add appropriate Authentication Access to the Preferences directory. ")
   public static final String PRIVATE_CONSTANT_9 = "LAuthorizedError";

   @RBEntry("You are not authorized to modify Preference Settings.")
   public static final String PRIVATE_CONSTANT_10 = "LAuthorizedConfigError";

   @RBEntry("Key:")
   public static final String PRIVATE_CONSTANT_11 = "LPreferenceName";

   @RBEntry("Value:")
   public static final String PRIVATE_CONSTANT_12 = "LPreferenceValue";

   @RBEntry("Preferences System Configuration")
   public static final String PRIVATE_CONSTANT_13 = "LPreferenceConfig";

   @RBEntry("An Error occured saving the preference.")
   public static final String PRIVATE_CONSTANT_14 = "LCreateError";

   @RBEntry("This name already exists in Preferences.")
   public static final String PRIVATE_CONSTANT_15 = "LCreateErrorAlreadyExists";

   @RBEntry("Preference name")
   public static final String PRIVATE_CONSTANT_16 = "LPreferenceNameHeader";

   @RBEntry("Description")
   public static final String PRIVATE_CONSTANT_17 = "LPreferenceDescriptionHeader";

   @RBEntry("Default value")
   public static final String PRIVATE_CONSTANT_18 = "LPreferenceDefaultHeader";

   @RBEntry("Home")
   public static final String PRIVATE_CONSTANT_19 = "LWindchillHome";

   @RBEntry("Help")
   public static final String PRIVATE_CONSTANT_20 = "LWindchillHelp";

   @RBEntry("Override allowed")
   public static final String PRIVATE_CONSTANT_21 = "LOverrideAllowed";

   @RBEntry("Changeable")
   public static final String PRIVATE_CONSTANT_22 = "LChangeable";

   @RBEntry("Key")
   public static final String PRIVATE_CONSTANT_23 = "LKey";

   @RBEntry("Value")
   public static final String PRIVATE_CONSTANT_24 = "LValue";

   @RBEntry("Preference Administrator ")
   public static final String PRIVATE_CONSTANT_25 = "LPreferenceHome";

   @RBEntry("Preference Administrator")
   public static final String PRIVATE_CONSTANT_26 = "LUserPreferencesTitle";

   @RBEntry("Edit")
   public static final String PRIVATE_CONSTANT_27 = "LEditPreference";

   @RBEntry("Create Preference")
   public static final String PRIVATE_CONSTANT_28 = "LNewPreference";

   @RBEntry("Ok")
   public static final String PRIVATE_CONSTANT_29 = "BLOk";

   @RBEntry("Cancel")
   public static final String PRIVATE_CONSTANT_30 = "BLCancel";

   @RBEntry("Search")
   public static final String PRIVATE_CONSTANT_31 = "BLSearch";

   @RBEntry("Apply")
   public static final String PRIVATE_CONSTANT_32 = "BLApply";

   @RBEntry("Remove from this scope")
   @RBComment("Indicating the removal of a preference at a given heirarchial level (scope)")
   public static final String PRIVATE_CONSTANT_33 = "BLRemovePreference";

   @RBEntry("Are you sure you wish to delete this Preference?")
   public static final String PRIVATE_CONSTANT_34 = "DLConfirmDelete";

   @RBEntry("yes")
   public static final String PRIVATE_CONSTANT_35 = "LOverrideable_true";

   @RBEntry("no")
   public static final String PRIVATE_CONSTANT_36 = "LOverrideable_false";

   @RBEntry("Attempting to reload the Preference Registry properties files.")
   public static final String PRIVATE_CONSTANT_37 = "SMPropertiesLoadAttempt";

   @RBEntry("All Preference Registry properties files loaded successfully.")
   public static final String PRIVATE_CONSTANT_38 = "SMPropertiesLoadSuccess";

   @RBEntry("An error occured while trying to load the Preference Registry properties files.  Please see below for the error:")
   public static final String PRIVATE_CONSTANT_39 = "SMPropertiesLoadFail";

   @RBEntry("WARNING: An Error has occured in the formatting of the \"Preference Service Delegate\" refresh value.  Using default value. ")
   public static final String PRIVATE_CONSTANT_40 = "SMRefreshValue";

   @RBEntry("A Customized Resource bundle could not be located.  If this Windchill Installation does not include any customizations, this message may be ignored.")
   public static final String PRIVATE_CONSTANT_41 = "SMMissingResource";

   @RBEntry("ERROR: The Preference Hierarchy contains no delegates.  Please check the configuration of the delegates.properties file located in codebase/wt/prefs/delegates for possible errors.")
   @RBComment("Occurs only if the delegates.properties file is invalid and wt.prefs.verbose=true for debugging")
   public static final String SMEmptyHierarchy = "SMEmptyHierarchy";

   @RBEntry("An error occured while parsing of the Preference Mapping file. Check the file syntax and restart the Method Servers in order to enable proper Preference Mapping handling within the system.")
   public static final String PRIVATE_CONSTANT_42 = "SMPrefMappingError";

   @RBEntry("Provide a key for the preference.")
   public static final String PRIVATE_CONSTANT_43 = "LNoPreferenceKey";

   @RBEntry("Show External:")
   public static final String PRIVATE_CONSTANT_44 = "LExternalPreference";
}
