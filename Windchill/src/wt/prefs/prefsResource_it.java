/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.prefs;

import wt.util.resource.*;

@RBUUID("wt.prefs.prefsResource")
public final class prefsResource_it extends WTListResourceBundle {
   /**
    * Entry
    **/
   @RBEntry("Default di sistema")
   public static final String PRIVATE_CONSTANT_0 = "LDefaultPreferences";

   @RBEntry("Utente")
   public static final String PRIVATE_CONSTANT_1 = "LUserPreferences";

   @RBEntry("Windchill Enterprise")
   public static final String PRIVATE_CONSTANT_2 = "LWindchillEnterprise";

   @RBEntry("Ambito")
   public static final String PRIVATE_CONSTANT_3 = "LScope";

   @RBEntry("Seguono le preferenze configurate di default per il sistema Windchill.")
   public static final String PRIVATE_CONSTANT_4 = "LPrefHelpDescription";

   @RBEntry("Ambito selezionato:")
   public static final String PRIVATE_CONSTANT_5 = "LChangeScope";

   @RBEntry("Cerca:")
   public static final String PRIVATE_CONSTANT_6 = "LSearchFor";

   @RBEntry("Errore preferenze")
   public static final String PRIVATE_CONSTANT_7 = "LPreferencesError";

   @RBEntry("Errore durante il tentativo di aggiornare il valore della preferenza.")
   public static final String PRIVATE_CONSTANT_8 = "LUpdateError";

   @RBEntry("Impossibile trovare un utente autenticato. L'autenticazione è richiesta dall'interfaccia dell'amministrazione preferenze. Aggiungere l'accesso all'autenticazione alla directory delle preferenze. ")
   public static final String PRIVATE_CONSTANT_9 = "LAuthorizedError";

   @RBEntry("L'utente non è autorizzato a modificare le impostazioni delle preferenze.")
   public static final String PRIVATE_CONSTANT_10 = "LAuthorizedConfigError";

   @RBEntry("Chiave:")
   public static final String PRIVATE_CONSTANT_11 = "LPreferenceName";

   @RBEntry("Valore:")
   public static final String PRIVATE_CONSTANT_12 = "LPreferenceValue";

   @RBEntry("Configurazione del sistema delle preferenze")
   public static final String PRIVATE_CONSTANT_13 = "LPreferenceConfig";

   @RBEntry("Errore durante il salvataggio delle preferenze")
   public static final String PRIVATE_CONSTANT_14 = "LCreateError";

   @RBEntry("Il nome esiste già nelle preferenze")
   public static final String PRIVATE_CONSTANT_15 = "LCreateErrorAlreadyExists";

   @RBEntry("Nome preferenza")
   public static final String PRIVATE_CONSTANT_16 = "LPreferenceNameHeader";

   @RBEntry("Descrizione")
   public static final String PRIVATE_CONSTANT_17 = "LPreferenceDescriptionHeader";

   @RBEntry("Valore di default")
   public static final String PRIVATE_CONSTANT_18 = "LPreferenceDefaultHeader";

   @RBEntry("Home")
   public static final String PRIVATE_CONSTANT_19 = "LWindchillHome";

   @RBEntry("Guida")
   public static final String PRIVATE_CONSTANT_20 = "LWindchillHelp";

   @RBEntry("Sovrascrittura consentita")
   public static final String PRIVATE_CONSTANT_21 = "LOverrideAllowed";

   @RBEntry("Modificabile")
   public static final String PRIVATE_CONSTANT_22 = "LChangeable";

   @RBEntry("Chiave")
   public static final String PRIVATE_CONSTANT_23 = "LKey";

   @RBEntry("Valore")
   public static final String PRIVATE_CONSTANT_24 = "LValue";

   @RBEntry("Amministrazione preferenze")
   public static final String PRIVATE_CONSTANT_25 = "LPreferenceHome";

   @RBEntry("Amministrazione preferenze")
   public static final String PRIVATE_CONSTANT_26 = "LUserPreferencesTitle";

   @RBEntry("Modifica")
   public static final String PRIVATE_CONSTANT_27 = "LEditPreference";

   @RBEntry("Crea preferenza")
   public static final String PRIVATE_CONSTANT_28 = "LNewPreference";

   @RBEntry("OK")
   public static final String PRIVATE_CONSTANT_29 = "BLOk";

   @RBEntry("Annulla")
   public static final String PRIVATE_CONSTANT_30 = "BLCancel";

   @RBEntry("Cerca")
   public static final String PRIVATE_CONSTANT_31 = "BLSearch";

   @RBEntry("Applica")
   public static final String PRIVATE_CONSTANT_32 = "BLApply";

   @RBEntry("Rimuovi da questo ambito")
   @RBComment("Indicating the removal of a preference at a given heirarchial level (scope)")
   public static final String PRIVATE_CONSTANT_33 = "BLRemovePreference";

   @RBEntry("Eliminare questa preferenza?")
   public static final String PRIVATE_CONSTANT_34 = "DLConfirmDelete";

   @RBEntry("sì")
   public static final String PRIVATE_CONSTANT_35 = "LOverrideable_true";

   @RBEntry("no")
   public static final String PRIVATE_CONSTANT_36 = "LOverrideable_false";

   @RBEntry("Tentativo di ricaricare i file delle proprietà del registro preferenze.")
   public static final String PRIVATE_CONSTANT_37 = "SMPropertiesLoadAttempt";

   @RBEntry("Tutti i file delle proprietà del registro preferenze sono stati caricati. ")
   public static final String PRIVATE_CONSTANT_38 = "SMPropertiesLoadSuccess";

   @RBEntry("Errore durante il caricamento dei file delle proprietà del registro preferenze. Errore:")
   public static final String PRIVATE_CONSTANT_39 = "SMPropertiesLoadFail";

   @RBEntry("AVVERTENZA: si è verificato un errore nella formattazione del valore aggiornato di \"Delegato servizio preferenze\".  Verrà usato il valore di default. ")
   public static final String PRIVATE_CONSTANT_40 = "SMRefreshValue";

   @RBEntry("Impossibile individuare un resource bundle personalizzato. Ignorare il messaggio se la presente installazione Windchill non contiene personalizzazioni.")
   public static final String PRIVATE_CONSTANT_41 = "SMMissingResource";

   @RBEntry("ERRORE: la gerarchia delle preferenze non contiene delegati. Controllare la configurazione del file delegates.properties collocato in codebase/wt/prefs/delegates.")
   @RBComment("Occurs only if the delegates.properties file is invalid and wt.prefs.verbose=true for debugging")
   public static final String SMEmptyHierarchy = "SMEmptyHierarchy";

   @RBEntry("Errore durante l'analisi del file di mappatura delle preferenze. Controllare la sintassi del file e riavviare i method server per permettere una gestione corretta della mappatura delle preferenze nel sistema.")
   public static final String PRIVATE_CONSTANT_42 = "SMPrefMappingError";

   @RBEntry("Indicare la chiave della preferenza.")
   public static final String PRIVATE_CONSTANT_43 = "LNoPreferenceKey";

   @RBEntry("Mostra esterno:")
   public static final String PRIVATE_CONSTANT_44 = "LExternalPreference";
}
