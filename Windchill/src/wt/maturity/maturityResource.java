/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.maturity;

import wt.util.resource.*;

@RBUUID("wt.maturity.maturityResource")
public final class maturityResource extends WTListResourceBundle {
   @RBEntry("A name must be supplied for the promotion notice")
   public static final String NO_PN_NAME = "0";

   @RBEntry("Null invalid for \"{0}\" (parameter \"{1}\") in \"{2}\"")
   public static final String NULL_ARGUMENT = "1";

   @RBEntry("The name for the new promotion notice is not valid")
   public static final String INVALID_PN_NAME = "2";

   @RBEntry("Empty Vector invalid for \"{0}\" (parameter \"{1}\") in \"{2}\"")
   public static final String EMPTY_ARGUMENT = "3";

   @RBEntry("The following objects cannot be deleted since they are members of the Baseline in Promotion Request \"{1}\". \"{0}\"")
   public static final String CANNOT_DELETE_MATURITY_BASELINED_OBJECT = "4";

   @RBEntry("The following objects are invalid promotion targets. \n {0}")
   @RBArgComment0("{0} are the objects are invalid.")
   public static final String INVALID_TARGETS = "5";

   @RBEntry("A Baseline must be associated to promotion notice \"{0}\"")
   public static final String NO_MATURITYBASELINE = "6";

   @RBEntry("You cannot \"Set Life Cycle State\" for the following objects because they are currently checked out to a project from PDM. \n {0}")
   public static final String PDM_CHECKOUT = "7";

   @RBEntry("One or more of the objects cannot be promoted to \"{0}\".")
   @RBArgComment0("The target state for promotion.")
   public static final String FAILED_TO_PROMOTE = "FAILED_TO_PROMOTE";

   @RBEntry("A workflow process attached to the promotion request \"{0}\" failed to terminate. The error is: {1}.")
   @RBArgComment0("The name of the Promotion Request object")
   @RBArgComment1("The message from the exception.")
   public static final String WORKFLOW_FAILED_TO_TERMINATE = "WORKFLOW_FAILED_TO_TERMINATE";

   /**
    * ------------------------------------------------------------
    * UI constants
    **/
   @RBEntry("Promotion Request automatically processed by system")
   @RBComment("This message is displayed in the Routing History table for Promotion Requests that are not associated to a workflow process.")
   public static final String PROMOTION_REQUEST_AUTO_PROCESSED = "100";

   @RBEntry("Default Promotion Processes")
   public static final String DEFAULT_PROMOTION_PROCESSES_Display = "DEFAULT_PROMOTION_PROCESSES_Display";

   @RBEntry("The specified list of workflow processes from the promotion process.")
   public static final String DEFAULT_PROMOTION_PROCESSES_ShortDescription = "DEFAULT_PROMOTION_PROCESSES_ShortDescription";

   @RBEntry("The default list of workflow processes used by the promotion process.  Each workflow process should be listed by name and separated with a \"~\" character.")
   public static final String DEFAULT_PROMOTION_PROCESSES_Description = "DEFAULT_PROMOTION_PROCESSES_Description";

   @RBEntry("The default list of workflow processes used by the promotion process.  This preference is used for all states within the promotion process unless a specific promotion preference is defined. \n\n Do the following: \n 1. Select the desired processes from the available processes list below. \n 2. Select the default process from the default process drop-down list, populated by your selection in step 1. \n\n If no processes are selected, the objects are promoted immediately upon successful creation of the promotion request.")
   public static final String DEFAULT_PROMOTION_PROCESSES_Description_Preference = "DEFAULT_PROMOTION_PROCESSES_Description_Preference";

   @RBEntry("Fixed Roles For Promote")
   public static final String IMMUTABLE_ROLES_Display = "IMMUTABLE_ROLES_Display";

   @RBEntry("The mapping of fixed roles used during promotion.")
   public static final String IMMUTABLE_ROLES_ShortDescription = "IMMUTABLE_ROLES_ShortDescription";

   @RBEntry("The mapping of container team roles to workflow process roles used during promotion.  The value consists of a series of [key]=[values] separated by the \"|\" character.  Each value in [values] should be separated by a \"~\" character.   Both the keys and values are the Role constants as defined by the file wt/project/RoleRB.rbInfo")
   public static final String IMMUTABLE_ROLES_Description = "IMMUTABLE_ROLES_Description";

   @RBEntry("Group Members Display")
   public static final String FLATTENED_MEMBERS_Display = "FLATTENED_MEMBERS_Display";

   @RBEntry("Controls the ability to select group members in the Define Participants table")
   public static final String FLATTENED_MEMBERS_ShortDescription = "FLATTENED_MEMBERS_ShortDescription";

   @RBEntry("When set to Yes, the Define Participants table allows the selection of members (users and sub-groups) of the existing groups in the table. The default is \"No\".")
   public static final String FLATTENED_MEMBERS_Description = "FLATTENED_MEMBERS_Description";

   @RBEntry("Transition \"{0}\" is required for \"{1}\" from state \"{2}\".")
   @RBComment("This error message indicates that there is no transition defined from a given state to another and one is required.")
   @RBArgComment0("{0} refers to a life cycle transition.")
   @RBArgComment1("{1} refers to the object that needs the transition")
   @RBArgComment2("{2} refers to the source life cycle state.")
   public static final String UNDEFINED_TRANSITION = "UNDEFINED_TRANSITION";

   @RBEntry("Access Check for Promote")
   @RBComment("Display name for preference for access check on the objects to promote. ")
   public static final String PRIVATE_CONSTANT_0 = "AccessCheckForPromote.displayName";

   @RBEntry("Checks if the user has either modify or set state permission to ensure the objects can be promoted.")
   @RBComment("Short description for the preference to modify or set state access check.")
   public static final String PRIVATE_CONSTANT_1 = "AccessCheckForPromote.description";

   @RBEntry("When set to No, the user's access permission to modify or set the state of the object being promoted is not verified. The object is promoted without validation of modify or set state permission. If Yes, the user's access permission to modify or set the state of the object is verified. If the user does not have modify or set state permission, the object cannot be promoted. The default is Yes.")
   @RBComment("Long description for preference for modify or set state access check for promote objects.")
   public static final String PRIVATE_CONSTANT_2 = "AccessCheckForPromote.longDescription";
}
