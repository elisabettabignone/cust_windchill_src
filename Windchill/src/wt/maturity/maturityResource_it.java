/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.maturity;

import wt.util.resource.*;

@RBUUID("wt.maturity.maturityResource")
public final class maturityResource_it extends WTListResourceBundle {
   @RBEntry("Indicare un nome per la notifica di promozione")
   public static final String NO_PN_NAME = "0";

   @RBEntry("Valore nullo non valido per \"{0}\" (parametro \"{1}\") in \"{2}\"")
   public static final String NULL_ARGUMENT = "1";

   @RBEntry("Il nome indicato per la notifica di promozione non è valido")
   public static final String INVALID_PN_NAME = "2";

   @RBEntry("Vettore vuoto non valido per \"{0}\" (parametro \"{1}\") in \"{2}\"")
   public static final String EMPTY_ARGUMENT = "3";

   @RBEntry("Impossibile eliminare gli oggetti che seguono perché sono membri della baseline nella richiesta di promozione \"{1}\". \"{0}\"")
   public static final String CANNOT_DELETE_MATURITY_BASELINED_OBJECT = "4";

   @RBEntry("Gli oggetti che seguono sono destinazioni di promozione non valide.\n {0}")
   @RBArgComment0("{0} are the objects are invalid.")
   public static final String INVALID_TARGETS = "5";

   @RBEntry("È necessario associare una baseline alla notifica di promozione \"{0}\"")
   public static final String NO_MATURITYBASELINE = "6";

   @RBEntry("Impossibile impostare lo stato del ciclo di vita per gli oggetti che seguono in quanto sono attualmente sottoposti a Check-Out in un progetto da PDM. \n {0}")
   public static final String PDM_CHECKOUT = "7";

   @RBEntry("Impossibile promuovere uno o più oggetti a \"{0}\".")
   @RBArgComment0("The target state for promotion.")
   public static final String FAILED_TO_PROMOTE = "FAILED_TO_PROMOTE";

   @RBEntry("Non è stato possibile terminare un processo di workflow allegato alla richiesta di promozione \"{0}\". Errore: {1}.")
   @RBArgComment0("The name of the Promotion Request object")
   @RBArgComment1("The message from the exception.")
   public static final String WORKFLOW_FAILED_TO_TERMINATE = "WORKFLOW_FAILED_TO_TERMINATE";

   /**
    * ------------------------------------------------------------
    * UI constants
    **/
   @RBEntry("Richiesta di promozione elaborata automaticamente dal sistema")
   @RBComment("This message is displayed in the Routing History table for Promotion Requests that are not associated to a workflow process.")
   public static final String PROMOTION_REQUEST_AUTO_PROCESSED = "100";

   @RBEntry("Processi di promozione di default")
   public static final String DEFAULT_PROMOTION_PROCESSES_Display = "DEFAULT_PROMOTION_PROCESSES_Display";

   @RBEntry("Elenco specificato dei processi di workflow del processo di promozione.")
   public static final String DEFAULT_PROMOTION_PROCESSES_ShortDescription = "DEFAULT_PROMOTION_PROCESSES_ShortDescription";

   @RBEntry("Elenco di default dei processi di workflow utilizzati dal processo di promozione. Ciascun processo di workflow deve essere elencato per nome e separato con un carattere \"~\".")
   public static final String DEFAULT_PROMOTION_PROCESSES_Description = "DEFAULT_PROMOTION_PROCESSES_Description";

   @RBEntry("Elenco di default dei processi di workflow utilizzati dal processo di promozione. La preferenza è usata per tutti gli stati previsti dal processo di promozione a meno che non sia definita una specifica preferenza di promozione. \n\n Eseguire quanto segue: \n 1. Selezionare i processi desiderati dalla lista di processi disponibili sottostante. \n 2. Selezionare il processo di default dall'elenco a discesa che conterrà i processi selezionati durante il passo 1. \n\n Se non si selezionano processi, gli oggetti vengono promossi immediatamente alla creazione della richiesta di promozione.")
   public static final String DEFAULT_PROMOTION_PROCESSES_Description_Preference = "DEFAULT_PROMOTION_PROCESSES_Description_Preference";

   @RBEntry("Ruoli fissi per la promozione")
   public static final String IMMUTABLE_ROLES_Display = "IMMUTABLE_ROLES_Display";

   @RBEntry("Mappatura dei ruoli fissi utilizzati durante la promozione.")
   public static final String IMMUTABLE_ROLES_ShortDescription = "IMMUTABLE_ROLES_ShortDescription";

   @RBEntry("Mappatura dei ruoli del team del contenitore ai ruoli del processo di workflow utilizzata durante la promozione. Il valore è costituito da una serie di voci [chiave]=[valori] separate dal carattere \"|\". Ciascun valore in [valori] deve essere separato da un carattere \"~\". Sia le chiavi che i valori sono le costanti Ruolo definite nel file wt/project/RoleRB.rbInfo")
   public static final String IMMUTABLE_ROLES_Description = "IMMUTABLE_ROLES_Description";

   @RBEntry("Visualizzazione membri del gruppo")
   public static final String FLATTENED_MEMBERS_Display = "FLATTENED_MEMBERS_Display";

   @RBEntry("Controlla la possibilità di selezionare i membri del gruppo nella tabella Definisci partecipanti")
   public static final String FLATTENED_MEMBERS_ShortDescription = "FLATTENED_MEMBERS_ShortDescription";

   @RBEntry("Se impostata su Sì, la tabella Definisci partecipanti consente di selezionare i membri (utenti e sottogruppi) dei gruppi esistenti nella tabella. Il default è No.")
   public static final String FLATTENED_MEMBERS_Description = "FLATTENED_MEMBERS_Description";

   @RBEntry("La transizione \"{0}\" è obbligatoria per \"{1}\" dallo stato \"{2}\".")
   @RBComment("This error message indicates that there is no transition defined from a given state to another and one is required.")
   @RBArgComment0("{0} refers to a life cycle transition.")
   @RBArgComment1("{1} refers to the object that needs the transition")
   @RBArgComment2("{2} refers to the source life cycle state.")
   public static final String UNDEFINED_TRANSITION = "UNDEFINED_TRANSITION";

   @RBEntry("Controllo di accesso per la promozione")
   @RBComment("Display name for preference for access check on the objects to promote. ")
   public static final String PRIVATE_CONSTANT_0 = "AccessCheckForPromote.displayName";

   @RBEntry("Verifica se l'utente dispone del permesso di modifica o di impostazione dello stato per assicurare la promozione degli oggetti.")
   @RBComment("Short description for the preference to modify or set state access check.")
   public static final String PRIVATE_CONSTANT_1 = "AccessCheckForPromote.description";

   @RBEntry("Se questa opzione è impostata su No, il permesso di accesso dell'utente per la modifica o l'impostazione dello stato del ciclo di vita dell'oggetto non viene verificato. L'oggetto viene promosso senza convalida del permesso di modifica o di impostazione dello stato. Se è impostata su Sì, il sistema verifica che l'utente disponga del permesso di accesso per la modifica o l'impostazione dello stato del ciclo di vita dell'oggetto. In caso negativo, l'oggetto non può essere promosso. Il default è Sì.")
   @RBComment("Long description for preference for modify or set state access check for promote objects.")
   public static final String PRIVATE_CONSTANT_2 = "AccessCheckForPromote.longDescription";
}
