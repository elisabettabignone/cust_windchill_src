/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.version.jmx;

import wt.util.resource.*;

@RBUUID("wt.version.jmx.jmxResource")
public final class jmxResource extends WTListResourceBundle {
   @RBEntry("Tabular data on installed Windchill product assemblies")
   public static final String INSTALLED_ASSY_TABLE_TYPE_DESCR = "1";

   @RBEntry("Data on an installed Windchill product assembly")
   public static final String INSTALLED_ASSY_STRUCT_TYPE_DESCR = "2";

   @RBEntry("Release id of installed assembly")
   public static final String INST_ASSY_REL_ID_DESCR = "3";

   @RBEntry("Display label for installed assembly")
   public static final String INST_ASSY_DISPLAY_LABEL_DESCR = "4";

   @RBEntry("Support release number for installed assembly")
   public static final String INST_ASSY_REL_NUMBER_DESCR = "5";

   @RBEntry("Support date code for installed assembly")
   public static final String INST_ASSY_DATE_CODE_DESCR = "6";

   @RBEntry("Installer sequence for installed assembly")
   public static final String INST_ASSY_INST_SEQ_DESCR = "7";

   @RBEntry("Whether installed assembly is complete")
   public static final String INST_ASSY_IS_COMPL_DESCR = "8";

   @RBEntry("Temporary patch label")
   public static final String TEMP_PATCH_LABEL_DESCR = "10";

   @RBEntry("Temporary patch description")
   public static final String TEMP_PATCH_DESCR_DESCR = "11";

   @RBEntry("Data on an installed Windchill temporary patch")
   public static final String TEMP_PATCH_STRUCT_TYPE_DESCR = "12";

   @RBEntry("Tabular data on installed Windchill temporary patches")
   public static final String TEMP_PATCH_TABLE_TYPE_DESCR = "13";

   @RBEntry("Locale code")
   public static final String LOCALE_CODE_DESCR = "14";

   @RBEntry("Locale name")
   public static final String LOCALE_NAME_DESCR = "15";

   @RBEntry("Data on a locale")
   public static final String LOCALE_STRUCT_TYPE_DESCR = "16";

   @RBEntry("Tabular data on installed Windchill non-default locale support")
   public static final String LOCALE_TABLE_TYPE_DESCR = "17";

   @RBEntry("Version independent Windchill product assembly identification code")
   public static final String INST_ASSY_ID_DESCR = "18";
}
