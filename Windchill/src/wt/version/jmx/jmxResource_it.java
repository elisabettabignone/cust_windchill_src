/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.version.jmx;

import wt.util.resource.*;

@RBUUID("wt.version.jmx.jmxResource")
public final class jmxResource_it extends WTListResourceBundle {
   @RBEntry("Dati tabulari sugli assiemi di prodotti Windchill installati")
   public static final String INSTALLED_ASSY_TABLE_TYPE_DESCR = "1";

   @RBEntry("Dati su un assieme di prodotti Windchill installato")
   public static final String INSTALLED_ASSY_STRUCT_TYPE_DESCR = "2";

   @RBEntry("ID release dell'assieme installato")
   public static final String INST_ASSY_REL_ID_DESCR = "3";

   @RBEntry("Visualizza l'etichetta per l'assieme installato")
   public static final String INST_ASSY_DISPLAY_LABEL_DESCR = "4";

   @RBEntry("Numero release di supporto dell'assieme installato")
   public static final String INST_ASSY_REL_NUMBER_DESCR = "5";

   @RBEntry("Codice data di supporto dell'assieme installato")
   public static final String INST_ASSY_DATE_CODE_DESCR = "6";

   @RBEntry("Sequenza di installazione dell'assieme installato")
   public static final String INST_ASSY_INST_SEQ_DESCR = "7";

   @RBEntry("Indica se l'assieme installato è completo")
   public static final String INST_ASSY_IS_COMPL_DESCR = "8";

   @RBEntry("Etichetta patch temporanea")
   public static final String TEMP_PATCH_LABEL_DESCR = "10";

   @RBEntry("Descrizione patch temporanea")
   public static final String TEMP_PATCH_DESCR_DESCR = "11";

   @RBEntry("Dati su una patch temporanea di Windchill installata")
   public static final String TEMP_PATCH_STRUCT_TYPE_DESCR = "12";

   @RBEntry("Dati tabulari sulle patch temporanee di Windchill installate")
   public static final String TEMP_PATCH_TABLE_TYPE_DESCR = "13";

   @RBEntry("Codice lingua")
   public static final String LOCALE_CODE_DESCR = "14";

   @RBEntry("Nome impostazioni locali")
   public static final String LOCALE_NAME_DESCR = "15";

   @RBEntry("Dati relativi a un'impostazione locale")
   public static final String LOCALE_STRUCT_TYPE_DESCR = "16";

   @RBEntry("Dati tabulari sul supporto delle impostazioni locali non di default di Windchill installato")
   public static final String LOCALE_TABLE_TYPE_DESCR = "17";

   @RBEntry("Codice di identificazione dell'assieme di prodotti Windchill indipendente dalla versione")
   public static final String INST_ASSY_ID_DESCR = "18";
}
