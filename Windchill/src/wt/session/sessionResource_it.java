/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.session;

import wt.util.resource.*;

@RBUUID("wt.session.sessionResource")
public final class sessionResource_it extends WTListResourceBundle {
   @RBEntry("L'accesso di un client non autenticato non è consentito: \"{0}\".")
   @RBArgComment0(" refers to the login name.")
   public static final String CLIENT_AUTHENTICATED_LOGIN = "0";

   @RBEntry("Utente non trovato: nome utente sconosciuto: \"{0}\".")
   @RBArgComment0(" refers to the user name.")
   public static final String MISSING_USER_NAME = "1";

   @RBEntry("Utente non trovato: nome Web sconosciuto: \"{0}\".")
   @RBArgComment0("} refers to the web name.")
   public static final String MISSING_WEB_NAME = "2";

   @RBEntry("Nome Web duplicato: \"{0}\".")
   @RBArgComment0(" refers to the web name.")
   public static final String DUPLICATE_WEB_NAME = "3";

   @RBEntry("Nuova autenticazione imprevista")
   public static final String UNEXPECTED_REAUTHENTICATION = "4";

   @RBEntry("ID sessione mancante nell'autenticatore di sessione")
   public static final String MISSING_SESSION_ID = "5";

   @RBEntry("Impossibile creare la cache di sessione")
   public static final String CREATE_SESSION_CACHE_ERROR = "6";
}
