/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.session;

import wt.util.resource.*;

@RBUUID("wt.session.sessionResource")
public final class sessionResource extends WTListResourceBundle {
   @RBEntry("Client non-authenticated login not allowed: \"{0}\".")
   @RBArgComment0(" refers to the login name.")
   public static final String CLIENT_AUTHENTICATED_LOGIN = "0";

   @RBEntry("User not found: unknown user name: \"{0}\".")
   @RBArgComment0(" refers to the user name.")
   public static final String MISSING_USER_NAME = "1";

   @RBEntry("User not found: unknown web name: \"{0}\".")
   @RBArgComment0("} refers to the web name.")
   public static final String MISSING_WEB_NAME = "2";

   @RBEntry("Duplicate web name: \"{0}\".")
   @RBArgComment0(" refers to the web name.")
   public static final String DUPLICATE_WEB_NAME = "3";

   @RBEntry("Unexpected re-authentication")
   public static final String UNEXPECTED_REAUTHENTICATION = "4";

   @RBEntry("Missing session id in session authenticator")
   public static final String MISSING_SESSION_ID = "5";

   @RBEntry("Unable to create session cache")
   public static final String CREATE_SESSION_CACHE_ERROR = "6";
}
