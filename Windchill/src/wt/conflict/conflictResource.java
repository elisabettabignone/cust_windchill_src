/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.conflict;

import wt.util.resource.*;

@RBUUID("wt.conflict.conflictResource")
public final class conflictResource extends WTListResourceBundle {
   @RBEntry("Resolution selected")
   @RBArgComment0("refers to container name")
   public static final String CONFLICT_RETURNED = "0";

   @RBEntry("Messages follow.")
   @RBArgComment0("Top level message.")
   public static final String INDIVIDUAL_CONFLICT = "1";
}
