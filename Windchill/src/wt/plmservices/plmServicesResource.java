/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.plmservices;

import wt.util.resource.*;

@RBUUID("wt.plmservices.plmServicesResource")
public final class plmServicesResource extends WTListResourceBundle {
   /**
    * actor names
    **/
   @RBEntry("GENERALCONNECTIONPROPERTIES ")
   public static final String GENERAL_CONNECTION_PROPERTIES = "GENERAL_CONNECTION_PROPERTIES";

   @RBEntry("GENERAL CONNECTION PROPERTIES")
   public static final String GENERAL_CONNECTION_PROPERTIES_DESC = "GENERAL_CONNECTION_PROPERTIES_DESC";

   @RBEntry("Internal Service Error")
   public static final String INTERNAL_SERVICE_ERROR = "INTERNAL_SERVICE_ERROR";

   @RBEntry("Supported import actions are: {0}")
   public static final String IMPORT_ACTION_DESC = "IMPORT_ACTION_DESC";

   @RBEntry("ImportAction")
   public static final String IMPORT_ACTION = "IMPORT_ACTION";

   @RBEntry("ImportDataProperties")
   public static final String IMPORT_DATA_PROPS = "IMPORT_DATA_PROPS";

   @RBEntry("Import Data Properties")
   public static final String IMPORT_DATA_PROPS_DESC = "IMPORT_DATA_PROPS_DESC";

   @RBEntry("ImportRules")
   public static final String RULES = "RULES";

   @RBEntry("comma (,) seperated list of rules for import")
   public static final String RULES_DESC = "RULES_DESC";

   @RBEntry("ContainerPath")
   public static final String CONTAINERPATH = "CONTAINERPATH";

   @RBEntry("Path of target Container to import given objects")
   public static final String CONTAINERPATH_DESC = "CONTAINERPATH_DESC";

   @RBEntry("Invalid Property {0}")
   public static final String INVALID_POROPERTY = "INVALID_POROPERTY";

   @RBEntry("Missing Property {0}")
   public static final String MISSING_POROPERTY = "MISSING_POROPERTY";

   @RBEntry("Invalid import action {0}")
   public static final String INVALID_IMPORT_ACTION = "INVALID_IMPORT_ACTION";

   @RBEntry("Supported Navigators are: {0}. For example if you have EPM Structure with A as main assembly and B as sub assembly then to navigate and export the whole structure provide UFID of object A in ObjectByUid Query and select NavigatorId as: productStructureNavigatorEPM ")
   public static final String NAVIGATORID_DESC = "NAVIGATORID_DESC";

   @RBEntry("NavigatorId")
   public static final String NAVIGATORID = "NAVIGATORID";

   @RBEntry("ExportDataProperties")
   public static final String EXPORT_DATA_PROPS = "EXPORT_DATA_PROPS";

   @RBEntry("Export Data Properties")
   public static final String EXPORT_DATA_PROPS_DESC = "EXPORT_DATA_PROPS_DESC";

   @RBEntry("Supported export actions are: {0}")
   public static final String EXPORT_ACTION_DESC = "EXPORT_ACTION_DESC";

   @RBEntry("ExportAction")
   public static final String EXPORT_ACTION = "EXPORT_ACTION";

   @RBEntry("OID of root level node")
   public static final String ROOT_NODE_OID_DESC = "ROOT_NODE_OID_DESC";

   @RBEntry("rootNodeOID")
   public static final String ROOT_NODE_OID = "ROOT_NODE_OID";

   @RBEntry("exportContentAsURL")
   public static final String EXPORT_CONTENT_AS_URL = "EXPORT_CONTENT_AS_URL";

   @RBEntry("Whether to export content as URL or not. This flag shoud not be set true when either exportContentOnly or offlineContentDir property is passed.")
   public static final String EXPORT_CONTENT_AS_URL_DESC = "EXPORT_CONTENT_AS_URL_DESC";

   @RBEntry("Invalid export action {0}")
   public static final String INVALID_EXPORT_ACTION = "INVALID_EXPORT_ACTION";

   @RBEntry("Top Level Object class :")
   public static final String TOP_LEVEL_SEARCH_CLASS = "TOP_LEVEL_SEARCH_CLASS";

   @RBEntry("Invalid navigator Id {0}")
   public static final String INVALID_NAVIGATOR_ID = "INVALID_NAVIGATOR_ID";

   @RBEntry("Unsupported PLMQuery {0}. Queries supported are: ObjectByUidQuery, ObjectsByUidsQuery, ItemQuery, DocumentQuery, GeometricModelQuery")
   public static final String UNSUPPORTED_QUERY = "UNSUPPORTED_QUERY";

   @RBEntry("All contents should be provided in single jar")
   public static final String INVALID_CONTENT = "INVALID_CONTENT";

   @RBEntry("No Object is exported. Please check following logs of export process :")
   public static final String NO_OBJECT_EXPORTED_MSG_WITH_LOGS = "NO_OBJECT_EXPORTED_MSG_WITH_LOGS";

   @RBEntry("No Object is exported")
   public static final String NO_OBJECT_EXPORTED_MSG = "NO_OBJECT_EXPORTED_MSG";

   @RBEntry("Problem in import process.Please check following logs of import process :")
   public static final String NO_OBJECT_IMPORTED_MSG_WITH_LOGS = "NO_OBJECT_IMPORTED_MSG_WITH_LOGS";

   @RBEntry("Problem in import process.")
   public static final String NO_OBJECT_IMPORTED_MSG = "NO_OBJECT_IMPORTED_MSG";

   @RBEntry("Object with UFID {0} not found")
   public static final String OBJECT_NOT_FOUND = "OBJECT_NOT_FOUND";

   @RBEntry("Object found ")
   public static final String OBJECT_FOUND = "OBJECT_FOUND";

   @RBEntry("Temp Directory created ")
   public static final String TEMP_DIRECTORY_MSG = "TEMP_DIRECTORY_MSG";

   @RBEntry("Export Status ")
   public static final String EXPORT_STATUS = "EXPORT_STATUS";

   @RBEntry("Sending Content ")
   public static final String CONTENT_MSG = "CONTENT_MSG";

   @RBEntry("Inside Export Data ")
   public static final String EXPORT_DATA_MSG = "EXPORT_DATA_MSG";

   @RBEntry("ExportAPIUsage ")
   public static final String EXPORT_API_USAGE = "EXPORT_API_USAGE";

   @RBEntry("export_data api takes PLMQuery and list of PLM properties as input. Supported queries are : ObjectByUIDQuery, ObjectByUIDsQuery, ItemQuery, DocumentQuery, GeometricModelQuery. To use ObjectByUID query, Client should provide root object UFID in ObjectByUID query and one of the navigator id to navigate through structure of this node.\n To use ObjectByUIDs query client should provide list of object UFIDs in ObjectbyUIDs query. In this case navigatorId should not be passed. \n To use ItemQuery,DocumentQuery or GeometricModelQuery client should provide number or name in respective query and should pass path of Organization for object as PLMProperty ")
   public static final String EXPORT_API_DESC = "EXPORT_API_DESC";

   @RBEntry("Attachments Found : ")
   public static final String ATTACHMENTS_MSG = "ATTACHMENTS_MSG";

   @RBEntry("ImportAPIUsage ")
   public static final String IMPORT_API_USAGE = "IMPORT_API_USAGE";

   @RBEntry("This API takes PLMContainer object and list of PLM properties as input. To send content for the objects to be imported,it should be packaged as Jar file and should be attached to SOAP message. ")
   public static final String IMPORT_API_DESC = "IMPORT_API_DESC";

   @RBEntry("Provided ObjectByUIDQuery so checking Navigator Id")
   public static final String OBJECT_BY_UID_QUERY_MSG = "OBJECT_BY_UID_QUERY_MSG";

   @RBEntry("Provided ObjectByUIDsQuery so ignoring Navigator Id")
   public static final String OBJECT_BY_UIDS_QUERY_MSG = "OBJECT_BY_UIDS_QUERY_MSG";

   @RBEntry("UFID of object to be exported is not provided")
   public static final String INVALID_ROOT_NODE_UFID = "INVALID_ROOT_NODE_UFID";

   @RBEntry("Provided ItemQuery so searching by number and organization")
   public static final String ITEMQUERY_MSG = "ITEMQUERY_MSG";

   @RBEntry("OrgPath")
   public static final String ORGPATH = "ORGPATH";

   @RBEntry("ContainerOid")
   public static final String CONTAINEROID = "CONTAINEROID";

   @RBEntry("Path of Organization of object to be searched. This property is required in case of ItemQuery,DocumentQuery or GeometricModelQuery")
   public static final String ORGPATH_DESC = "ORGPATH_DESC";

   @RBEntry("number provided: {0}")
   public static final String NUMBER_PROVIDED = "ITEM_NUMBER";

   @RBEntry("name provided: {0}")
   public static final String NAME_PROVIDED = "NAME_PROVIDED";

   @RBEntry("version provided: {0}")
   public static final String VERSION_PROVIDED = "VERSION_PROVIDED";

   @RBEntry("Class of Object: {0}")
   public static final String OBJECT_CLASS = "OBJECT_CLASS";

   @RBEntry("Invalid Organization path: {0}")
   public static final String INVALID_ORG_PATH = "INVALID_ORG_PATH";

   @RBEntry("Object with number: {0}, name: {1} and version: {2} not found in context {3}")
   public static final String OBJECT_NOT_FOUND_WITH_NUMBER_NAME = "OBJECT_NOT_FOUND_WITH_NUMBER_NAME";

   @RBEntry("Object with number: {0}, name: {1},filename: {2} and version: {3} not found in context {4}")
   public static final String OBJECT_NOT_FOUND_WITH_NUMBER_NAME_FILENAME = "OBJECT_NOT_FOUND_WITH_NUMBER_NAME_FILENAME";

   @RBEntry("Entering Generic Query")
   public static final String GENERICQUERY_MSG = "GENERICQUERY_MSG";

   @RBEntry("Provided DocumentQuery so searching by number and organization")
   public static final String DOCUMENTQUERY_MSG = "DOCUMENTQUERY_MSG";

   @RBEntry("Both number and name are not provided in query. Atleast one of them should be provided")
   public static final String INVALID_NUMBER_NAME = "INVALID_NUMBER_NAME";

   @RBEntry("Number,name or filename is not provided in query. Atleast one of them should be provided")
   public static final String INVALID_NUMBER_NAME_FILENAME = "INVALID_NUMBER_NAME_FILENAME";

   @RBEntry("Number provided in query: {0}")
   public static final String NUMBER_MSG = "NUMBER_MSG";

   @RBEntry("Name provided in query: {0}")
   public static final String NAME_MSG = "NAME_MSG";

   @RBEntry("NameSpace for container {0}is {1}")
   public static final String NAMESPACE_MSG = "NAMESPACE_MSG";

   @RBEntry("Provided GeometricModelQuery so searching by number/name and organization")
   public static final String GEOMETRIC_MODEL_QUERY_MSG = "GEOMETRIC_MODEL_QUERY_MSG";

   @RBEntry("Revision Id:  {0}")
   public static final String REVISION_ID_MSG = "REVISION_ID_MSG";

   @RBEntry("Iteration Id:  {0}")
   public static final String ITERATION_ID_MSG = "ITERATION_ID_MSG";

   @RBEntry("wt/plmServices/searchObjects.xml")
   public static final String SEARCH_OBJECT_TASK_PATH = "SEARCH_OBJECT_TASK_PATH";

   @RBEntry("number")
   public static final String NUMBER = "NUMBER";

   @RBEntry("name")
   public static final String NAME = "NAME";

   @RBEntry("CADName")
   public static final String CADNAME = "CADNAME";

   @RBEntry("TYPE")
   public static final String TYPE = "TYPE";

   @RBEntry("revisionId")
   public static final String REVISION_ID = "REVISION_ID";

   @RBEntry("iterationId")
   public static final String ITERATION_ID = "ITERATION_ID";

   @RBEntry("group_out")
   public static final String GROUP_OUT_TAG = "GROUP_OUT_TAG";

   @RBEntry("objects")
   public static final String GROUP_OUT_NAME = "GROUP_OUT_NAME";

   @RBEntry("PLM_FORMAT")
   public static final String PLM_FORMAT = "PLM_FORMAT";

   @RBEntry("Objects mapped to Item class are : {0}")
   public static final String PLM_MAPPED_ITEM_WINDCHILL_TAGS = "PLM_MAPPED_ITEM_WINDCHILL_TAGS";

   @RBEntry("Objects mapped to Document class are : {0}")
   public static final String PLM_MAPPED_DOCUMENT_WINDCHILL_TAGS = "PLM_MAPPED_DOCUMENT_WINDCHILL_TAGS";

   @RBEntry("Objects mapped to Geometric Model class are : {0}")
   public static final String PLM_MAPPED_GEOMETRIC_MODEL_WINDCHILL_TAGS = "PLM_MAPPED_GEOMETRIC_MODEL_WINDCHILL_TAGS";

   @RBEntry("No Windchill object is mapped to Item")
   public static final String NO_TAG_FOR_ITEM = "NO_TAG_FOR_ITEM";

   @RBEntry("No Windchill object is mapped to Document")
   public static final String NO_TAG_FOR_DOCUMENT = "NO_TAG_FOR_DOCUMENT";

   @RBEntry("No Windchill object is mapped to Geometric Model")
   public static final String NO_TAG_FOR_GEOMETRIC_MODEL = "NO_TAG_FOR_GEOMETRIC_MODEL";

   @RBEntry("supporting-adapter")
   public static final String INSTANCE_TAG = "INSTANCE_TAG";

   @RBEntry("Adapter found : {0}")
   public static final String INSTANCE_MSG = "INSTANCE_MSG";

   @RBEntry("Invalid Version Id provided : {0}")
   public static final String INVALID_VERSION_ID = "INVALID_VERSION_ID";

   @RBEntry("offlineContentDir")
   public static final String OFFLINE_CONTENT_DIR_NAME = "OFFLINE_CONTENT_DIR_NAME";

   @RBEntry("Directory name where content should be stored. User should provide this property when offline download of content is intended.")
   public static final String OFFLINE_CONTENT_DIR_NAME_DESC = "OFFLINE_CONTENT_DIR_NAME_DESC";

   @RBEntry("Found offlineContentDir property.Creating a directry with name {0} in temp folder and storing content jar into it.")
   public static final String OFFLINE_CONTENT_LOG = "OFFLINE_CONTENT_LOG";

   @RBEntry("exportContentOnly")
   public static final String EXPORT_CONTENT_ONLY = "EXPORT_CONTENT_ONLY";

   @RBEntry("Flag to indicate if only content should be exported.")
   public static final String EXPORT_CONTENT_ONLY_DESC = "EXPORT_CONTENT_ONLY_DESC";

   @RBEntry("exportContentAsURL property is not allowed with value true when either exportContentOnly or offlineContentDir property is set.")
   public static final String CONTRADICTORY_PROPERTIES_ERROR = "CONTRADICTORY_PROPERTIES_ERROR";







}
