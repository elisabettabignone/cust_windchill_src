/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.plmservices;

import wt.util.resource.*;

@RBUUID("wt.plmservices.plmServicesResource")
public final class plmServicesResource_it extends WTListResourceBundle {
   /**
    * actor names
    **/
   @RBEntry("GENERALCONNECTIONPROPERTIES ")
   public static final String GENERAL_CONNECTION_PROPERTIES = "GENERAL_CONNECTION_PROPERTIES";

   @RBEntry("PROPRIETÀ CONNESSIONE GENERALI")
   public static final String GENERAL_CONNECTION_PROPERTIES_DESC = "GENERAL_CONNECTION_PROPERTIES_DESC";

   @RBEntry("Errore servizio interno")
   public static final String INTERNAL_SERVICE_ERROR = "INTERNAL_SERVICE_ERROR";

   @RBEntry("Le azioni di importazione supportate sono: {0}")
   public static final String IMPORT_ACTION_DESC = "IMPORT_ACTION_DESC";

   @RBEntry("ImportAction")
   public static final String IMPORT_ACTION = "IMPORT_ACTION";

   @RBEntry("ImportDataProperties")
   public static final String IMPORT_DATA_PROPS = "IMPORT_DATA_PROPS";

   @RBEntry("Proprietà dati importazione")
   public static final String IMPORT_DATA_PROPS_DESC = "IMPORT_DATA_PROPS_DESC";

   @RBEntry("ImportRules")
   public static final String RULES = "RULES";

   @RBEntry("elenco di regole per l'importazione separate da virgola (,)")
   public static final String RULES_DESC = "RULES_DESC";

   @RBEntry("ContainerPath")
   public static final String CONTAINERPATH = "CONTAINERPATH";

   @RBEntry("Percorso del contenitore di destinazione per importare gli oggetti specificati")
   public static final String CONTAINERPATH_DESC = "CONTAINERPATH_DESC";

   @RBEntry("Proprietà {0} non valida")
   public static final String INVALID_POROPERTY = "INVALID_POROPERTY";

   @RBEntry("Proprietà {0} mancante")
   public static final String MISSING_POROPERTY = "MISSING_POROPERTY";

   @RBEntry("Azione di importazione {0} non valida")
   public static final String INVALID_IMPORT_ACTION = "INVALID_IMPORT_ACTION";

   @RBEntry("I navigatori supportati sono: {0}. Ad esempio, se si dispone di una struttura EPM con A come assieme principale e B come sottoassieme, per spostare ed esportare l'intera struttura, fornire l'UFID dell'oggetto A nell'interrogazione ObjectByUid e selezionare NavigatorId come: productStructureNavigatorEPM ")
   public static final String NAVIGATORID_DESC = "NAVIGATORID_DESC";

   @RBEntry("NavigatorId")
   public static final String NAVIGATORID = "NAVIGATORID";

   @RBEntry("ExportDataProperties")
   public static final String EXPORT_DATA_PROPS = "EXPORT_DATA_PROPS";

   @RBEntry("Proprietà dati esportazione")
   public static final String EXPORT_DATA_PROPS_DESC = "EXPORT_DATA_PROPS_DESC";

   @RBEntry("Le azioni di esportazione supportate sono: {0}")
   public static final String EXPORT_ACTION_DESC = "EXPORT_ACTION_DESC";

   @RBEntry("ExportAction")
   public static final String EXPORT_ACTION = "EXPORT_ACTION";

   @RBEntry("OID del nodo del livello radice")
   public static final String ROOT_NODE_OID_DESC = "ROOT_NODE_OID_DESC";

   @RBEntry("rootNodeOID")
   public static final String ROOT_NODE_OID = "ROOT_NODE_OID";

   @RBEntry("exportContentAsURL")
   public static final String EXPORT_CONTENT_AS_URL = "EXPORT_CONTENT_AS_URL";

   @RBEntry("Esportazione o meno del contenuto come URL. Non impostare questo flag su true quando viene trasferita la proprietà exportContentOnly oppure offlineContentDir.")
   public static final String EXPORT_CONTENT_AS_URL_DESC = "EXPORT_CONTENT_AS_URL_DESC";

   @RBEntry("Azione di esportazione {0} non valida")
   public static final String INVALID_EXPORT_ACTION = "INVALID_EXPORT_ACTION";

   @RBEntry("Classe oggetto di livello superiore:")
   public static final String TOP_LEVEL_SEARCH_CLASS = "TOP_LEVEL_SEARCH_CLASS";

   @RBEntry("ID navigatore {0} non valido")
   public static final String INVALID_NAVIGATOR_ID = "INVALID_NAVIGATOR_ID";

   @RBEntry("PLMQuery {0} non supportata. Le interrogazioni supportate sono: ObjectByUidQuery, ObjectsByUidsQuery, ItemQuery, DocumentQuery, GeometricModelQuery")
   public static final String UNSUPPORTED_QUERY = "UNSUPPORTED_QUERY";

   @RBEntry("Tutto il contenuto deve essere fornito in un unico file jar")
   public static final String INVALID_CONTENT = "INVALID_CONTENT";

   @RBEntry("Non è stato esportato alcun oggetto. Controllare i seguenti log del processo di esportazione:")
   public static final String NO_OBJECT_EXPORTED_MSG_WITH_LOGS = "NO_OBJECT_EXPORTED_MSG_WITH_LOGS";

   @RBEntry("Non è stato esportato alcun oggetto")
   public static final String NO_OBJECT_EXPORTED_MSG = "NO_OBJECT_EXPORTED_MSG";

   @RBEntry("Si è verificato un problema durante l'importazione. Controllare i seguenti log del processo di importazione:")
   public static final String NO_OBJECT_IMPORTED_MSG_WITH_LOGS = "NO_OBJECT_IMPORTED_MSG_WITH_LOGS";

   @RBEntry("Si è verificato un problema durante il processo di importazione.")
   public static final String NO_OBJECT_IMPORTED_MSG = "NO_OBJECT_IMPORTED_MSG";

   @RBEntry("Impossibile trovare l'oggetto con UFID {0}")
   public static final String OBJECT_NOT_FOUND = "OBJECT_NOT_FOUND";

   @RBEntry("Oggetto trovato ")
   public static final String OBJECT_FOUND = "OBJECT_FOUND";

   @RBEntry("Directory Temp creata ")
   public static final String TEMP_DIRECTORY_MSG = "TEMP_DIRECTORY_MSG";

   @RBEntry("Stato esportazione ")
   public static final String EXPORT_STATUS = "EXPORT_STATUS";

   @RBEntry("Invio contenuto ")
   public static final String CONTENT_MSG = "CONTENT_MSG";

   @RBEntry("Tra i dati di esportazione ")
   public static final String EXPORT_DATA_MSG = "EXPORT_DATA_MSG";

   @RBEntry("ExportAPIUsage ")
   public static final String EXPORT_API_USAGE = "EXPORT_API_USAGE";

   @RBEntry("L'API export_data accetta PLMQuery e l'elenco delle proprietà PLM come input. Le interrogazioni supportate sono: ObjectByUIDQuery, ObjectByUIDsQuery, ItemQuery, DocumentQuery, GeometricModelQuery. Per utilizzare l'interrogazione ObjectByUID, il client deve fornire l'UFID dell'oggetto radice nell'interrogazione ObjectByUID e uno degli ID navigatore per navigare nella struttura del nodo.\n Per utilizzare l'interrogazione ObjectByUIDs, il client deve fornire l'elenco degli UFID degli oggetti nell'interrogazione ObjectbyUIDs. In questo caso non trasferire navigatorId. \n Per utilizzare ItemQuery, DocumentQuery o GeometricModelQuery, il client deve fornire il numero o il nome nella rispettiva interrogazione e trasferire il percorso dell'organizzazione per gli oggetti come PLMProperty ")
   public static final String EXPORT_API_DESC = "EXPORT_API_DESC";

   @RBEntry("Allegati trovati: ")
   public static final String ATTACHMENTS_MSG = "ATTACHMENTS_MSG";

   @RBEntry("ImportAPIUsage ")
   public static final String IMPORT_API_USAGE = "IMPORT_API_USAGE";

   @RBEntry("Questa API accetta l'oggetto PLMContainer e l'elenco di proprietà PLM come input. Per inviare il contenuto per gli oggetti da importare, deve essere incluso in un package come file Jar e deve essere allegato al messaggio SOAP. ")
   public static final String IMPORT_API_DESC = "IMPORT_API_DESC";

   @RBEntry("ObjectByUIDQuery fornita, controllo ID navigatore")
   public static final String OBJECT_BY_UID_QUERY_MSG = "OBJECT_BY_UID_QUERY_MSG";

   @RBEntry("ObjectByUIDsQuery fornita, ID navigatore ignorato")
   public static final String OBJECT_BY_UIDS_QUERY_MSG = "OBJECT_BY_UIDS_QUERY_MSG";

   @RBEntry("L'UFID dell'oggetto da esportare non è stato fornito")
   public static final String INVALID_ROOT_NODE_UFID = "INVALID_ROOT_NODE_UFID";

   @RBEntry("ItemQuery fornita, ricerca in base a numero e organizzazione")
   public static final String ITEMQUERY_MSG = "ITEMQUERY_MSG";

   @RBEntry("OrgPath")
   public static final String ORGPATH = "ORGPATH";

   @RBEntry("ContainerOid")
   public static final String CONTAINEROID = "CONTAINEROID";

   @RBEntry("Percorso dell'organizzazione dell'oggetto da cercare. Questa proprietà è obbligatoria per ItemQuery, DocumentQuery o GeometricModelQuery")
   public static final String ORGPATH_DESC = "ORGPATH_DESC";

   @RBEntry("numero fornito: {0}")
   public static final String NUMBER_PROVIDED = "ITEM_NUMBER";

   @RBEntry("nome fornito: {0}")
   public static final String NAME_PROVIDED = "NAME_PROVIDED";

   @RBEntry("versione fornita: {0}")
   public static final String VERSION_PROVIDED = "VERSION_PROVIDED";

   @RBEntry("Classe di oggetto: {0}")
   public static final String OBJECT_CLASS = "OBJECT_CLASS";

   @RBEntry("Percorso organizzazione non valido: {0}")
   public static final String INVALID_ORG_PATH = "INVALID_ORG_PATH";

   @RBEntry("Impossibile trovare l'oggetto con numero {0}, nome {1} e versione {2} nel contesto {3}")
   public static final String OBJECT_NOT_FOUND_WITH_NUMBER_NAME = "OBJECT_NOT_FOUND_WITH_NUMBER_NAME";

   @RBEntry("Impossibile trovare l'oggetto con numero {0}, nome {1}, nome file {2} e versione {3} nel contesto {4}")
   public static final String OBJECT_NOT_FOUND_WITH_NUMBER_NAME_FILENAME = "OBJECT_NOT_FOUND_WITH_NUMBER_NAME_FILENAME";

   @RBEntry("Immissione interrogazione generica")
   public static final String GENERICQUERY_MSG = "GENERICQUERY_MSG";

   @RBEntry("DocumentQuery fornita, ricerca in base a numero e organizzazione")
   public static final String DOCUMENTQUERY_MSG = "DOCUMENTQUERY_MSG";

   @RBEntry("Nell'interrogazione non sono stati forniti il numero e il nome. Fornire almeno uno dei due")
   public static final String INVALID_NUMBER_NAME = "INVALID_NUMBER_NAME";

   @RBEntry("Nell'interrogazione non sono stati forniti numero, nome o nome file. Fornirne almeno uno")
   public static final String INVALID_NUMBER_NAME_FILENAME = "INVALID_NUMBER_NAME_FILENAME";

   @RBEntry("Numero fornito nell'interrogazione: {0}")
   public static final String NUMBER_MSG = "NUMBER_MSG";

   @RBEntry("Nome fornito nell'interrogazione: {0}")
   public static final String NAME_MSG = "NAME_MSG";

   @RBEntry("Il namespace per il contenitore {0} è {1}")
   public static final String NAMESPACE_MSG = "NAMESPACE_MSG";

   @RBEntry("GeometricModelQuery fornita, ricerca in base a numero/nome e organizzazione")
   public static final String GEOMETRIC_MODEL_QUERY_MSG = "GEOMETRIC_MODEL_QUERY_MSG";

   @RBEntry("ID revisione: {0}")
   public static final String REVISION_ID_MSG = "REVISION_ID_MSG";

   @RBEntry("ID iterazione: {0}")
   public static final String ITERATION_ID_MSG = "ITERATION_ID_MSG";

   @RBEntry("wt/plmServices/searchObjects.xml")
   public static final String SEARCH_OBJECT_TASK_PATH = "SEARCH_OBJECT_TASK_PATH";

   @RBEntry("number")
   public static final String NUMBER = "NUMBER";

   @RBEntry("name")
   public static final String NAME = "NAME";

   @RBEntry("CADName")
   public static final String CADNAME = "CADNAME";

   @RBEntry("TYPE")
   public static final String TYPE = "TYPE";

   @RBEntry("revisionId")
   public static final String REVISION_ID = "REVISION_ID";

   @RBEntry("iterationId")
   public static final String ITERATION_ID = "ITERATION_ID";

   @RBEntry("group_out")
   public static final String GROUP_OUT_TAG = "GROUP_OUT_TAG";

   @RBEntry("objects")
   public static final String GROUP_OUT_NAME = "GROUP_OUT_NAME";

   @RBEntry("PLM_FORMAT")
   public static final String PLM_FORMAT = "PLM_FORMAT";

   @RBEntry("Gli oggetti mappati alla classe Item sono: {0}")
   public static final String PLM_MAPPED_ITEM_WINDCHILL_TAGS = "PLM_MAPPED_ITEM_WINDCHILL_TAGS";

   @RBEntry("Gli oggetti mappati alla classe Document sono: {0}")
   public static final String PLM_MAPPED_DOCUMENT_WINDCHILL_TAGS = "PLM_MAPPED_DOCUMENT_WINDCHILL_TAGS";

   @RBEntry("Gli oggetti mappati alla classe Geometric Model sono: {0}")
   public static final String PLM_MAPPED_GEOMETRIC_MODEL_WINDCHILL_TAGS = "PLM_MAPPED_GEOMETRIC_MODEL_WINDCHILL_TAGS";

   @RBEntry("Nessun oggetto Windchill è mappato all'elemento")
   public static final String NO_TAG_FOR_ITEM = "NO_TAG_FOR_ITEM";

   @RBEntry("Nessun oggetto Windchill è mappato al documento")
   public static final String NO_TAG_FOR_DOCUMENT = "NO_TAG_FOR_DOCUMENT";

   @RBEntry("Nessun oggetto Windchill è mappato al modello geometrico")
   public static final String NO_TAG_FOR_GEOMETRIC_MODEL = "NO_TAG_FOR_GEOMETRIC_MODEL";

   @RBEntry("supporting-adapter")
   public static final String INSTANCE_TAG = "INSTANCE_TAG";

   @RBEntry("Adattatore trovato: {0}")
   public static final String INSTANCE_MSG = "INSTANCE_MSG";

   @RBEntry("ID versione fornito non valido: {0}")
   public static final String INVALID_VERSION_ID = "INVALID_VERSION_ID";

   @RBEntry("offlineContentDir")
   public static final String OFFLINE_CONTENT_DIR_NAME = "OFFLINE_CONTENT_DIR_NAME";

   @RBEntry("Nome della directory in cui va memorizzato il contenuto. L'utente deve fornire questa proprietà quando intende scaricare il contenuto non in linea.")
   public static final String OFFLINE_CONTENT_DIR_NAME_DESC = "OFFLINE_CONTENT_DIR_NAME_DESC";

   @RBEntry("Trovata la proprietà offlineContentDir. Viene creata la directory {0} nella cartella Temp in cui viene memorizzato il file jar del contenuto.")
   public static final String OFFLINE_CONTENT_LOG = "OFFLINE_CONTENT_LOG";

   @RBEntry("exportContentOnly")
   public static final String EXPORT_CONTENT_ONLY = "EXPORT_CONTENT_ONLY";

   @RBEntry("Flag che indica se deve essere esportato solo il contenuto.")
   public static final String EXPORT_CONTENT_ONLY_DESC = "EXPORT_CONTENT_ONLY_DESC";

   @RBEntry("La proprietà exportContentAsURL non è consentita con il valore true quando è impostata la proprietà exportContentOnly oppure offlineContentDir.")
   public static final String CONTRADICTORY_PROPERTIES_ERROR = "CONTRADICTORY_PROPERTIES_ERROR";







}
