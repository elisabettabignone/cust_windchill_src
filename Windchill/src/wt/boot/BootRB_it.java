/* bcwti
*
* Copyright (c) 2008 Windchill Technology Inc. All Rights Reserved.
*
* This software is the confidential and proprietary information of PTC
* and is subject to the terms of a software license agreement. You shall
* not disclose such confidential information and shall use it only in accordance
* with the terms of the license agreement.
*
* ecwti
*/

package wt.boot;

import java.util.ListResourceBundle;

/**
* Default BootRB message resource bundle [English/US]
*
* Usage notes:
* DO NOT REUSE IDS.  If an message becomes obsolete, comment it out
* but do not create a new message with that id.
*
**/
public class BootRB_it extends ListResourceBundle {

public static final String SETUP_HELP = "1";
public static final String DOWNLOAD_HELP = "2";
public static final String NEW_VERSION_HELP = "3";
public static final String SETUP_LABEL = "4";
public static final String SETUP_INSTRUCTIONS = "5";
public static final String SETUP_CACHE_LOCATION_LABEL = "6";
public static final String SETUP_PROPERTIES_LOCATION_LABEL = "7";
public static final String SETUP_CACHE_LOCATION_BROWSE_BUTTON = "8";
public static final String SETUP_OK_BUTTON = "9";
public static final String SETUP_CANCEL_BUTTON = "10";
public static final String SETUP_HELP_BUTTON = "11";
public static final String SETUP_DIALOG_TITLE = "12";
public static final String SETUP_BROWSE_DIALOG_TITLE = "13";
public static final String SETUP_BROWSE_DIALOG_FILE = "14";
public static final String DOWNLOAD_LABEL = "15";
public static final String DOWNLOAD_INSTRUCTIONS = "16";
public static final String DOWNLOAD_REMOTE_FILE_LABEL = "17";
public static final String DOWNLOAD_REMOTE_MODIFIED_LABEL = "18";
public static final String DOWNLOAD_SIZE_LABEL = "19";
public static final String DOWNLOAD_LOCAL_FILE_LABEL = "20";
public static final String DOWNLOAD_LOCAL_MODIFIED_LABEL = "21";
public static final String DOWNLOAD_INFLATE_RADIO_BUTTON = "22";
public static final String DOWNLOAD_AS_IS_RADIO_BUTTON = "23";
public static final String DOWNLOAD_IGNORE_RADIO_BUTTON = "24";
public static final String DOWNLOAD_CONTINUE_BUTTON = "25";
public static final String DOWNLOAD_HELP_BUTTON = "26";
public static final String DOWNLOAD_DIALOG_TITLE = "27";
public static final String DOWNLOAD_PROGRESS_LABEL = "28";
public static final String DOWNLOAD_PROGRESS_TOTAL_LABEL = "29";
public static final String DOWNLOAD_PROGRESS_RECEIVED_LABEL = "30";
public static final String DOWNLOAD_PROGRESS_PERCENT_LABEL = "31";
public static final String DOWNLOAD_PROGRESS_CANCEL_BUTTON = "32";
public static final String DOWNLOAD_PROGRESS_DIALOG_TITLE = "33";
public static final String INFLATE_PROGRESS_LABEL = "34";
public static final String INFLATE_PROGRESS_INFLATED_ENTRIES_LABEL = "35";
public static final String INFLATE_PROGRESS_CANCEL_BUTTON = "36";
public static final String INFLATE_PROGRESS_DIALOG_TITLE = "37";
public static final String EXCEPTION_LABEL = "38";
public static final String EXCEPTION_DESCRIPTION = "39";
public static final String EXCEPTION_CONTINUE_BUTTON = "40";
public static final String EXCEPTION_DIALOG_TITLE = "41";
public static final String HELP_OK_BUTTON = "42";
public static final String HELP_DIALOG_TITLE = "43";
public static final String NEW_VERSION_ANNOUNCEMENT_LABEL = "44";
public static final String NEW_VERSION_REMOTE_VERSION_LABEL = "45";
public static final String NEW_VERSION_LOCAL_VERSION_LABEL = "46";
public static final String NEW_VERSION_DOWNLOAD_URL_LABEL = "47";
public static final String NEW_VERSION_CONTINUE_BUTTON = "48";
public static final String NEW_VERSION_DIALOG_TITLE = "49";
public static final String MISSING_PARAMETER_EXCEPTION = "50";
public static final String INFLATE_PROGRESS_TOTAL_LABEL = "51";
public static final String INFLATE_PROGRESS_PERCENT_LABEL = "52";
public static final String NEW_VERSION_DOWNLOAD_BUTTON = "53";
public static final String NEW_VERSION_HELP_BUTTON = "54";
public static final String DOWNLOAD_OLD_FILE_RADIO_BUTTON = "55";
public static final String BOOTSTRAP_LOADER_INSTALLED = "56";
public static final String BOOTSTRAP_LOADER_NOT_INSTALLED = "57";
public static final String NOT_FOUND_OR_MISSING_DATE = "58";
public static final String NOT_AVAILABLE = "59";
public static final String IGNORING_LOCAL_CACHE = "60";

public Object[][] getContents()
{
return contents;
}

static final String[] setupHelp =
{
"Il caricatore per bootstrap Windchill utilizza una cache locale di",
"file JAR o ZIP per caricare classi e file di risorsa",
"durante l'esecuzione di moduli applicativi o applicazioni Java.  Ogni",
"file JAR o ZIP corrisponde al contenuto di una base di codice",
"remota.",
"",
"Immettere la posizione di una directory locale da utilizzare",
"come cache per memorizzare i file locali.  Il caricatore per bootstrap",
"memorizzer\u00e0 i file locali nelle sottodirectory di questa directory",
"corrispondenti a ciascuna base di codice remota.",
"",
"La posizione della cache viene memorizzata in un file delle propriet\u00e0,",
"denominato .wtboot.properties, contenuto nella directory",
"identificata dalla propriet\u00e0 di sistema Java user.home."
};

static final String[] downloadHelp =
{
"Una versione pi\u00f9 recente di un file JAR o ZIP memorizzato nella cache locale",
"\u00e8 stata rilevata nella base di codice remota.  \u00c8 possibile",
"scaricare il file adesso o in un secondo momento.",
"",
"Durante lo scaricamento,, \u00e8 possibile specificare di decomprimere il file,",
"se questo \u00e8 stato creato come file compresso.",
"\u00c8 consigliabile procedere subito alla decompressione del file poich\u00e9",
"ci\u00f2 riduce il sovraccarico di CPU durante la successiva lettura degli",
"elementi, Tuttavia, poich\u00e9 le dimensioni del file decompresso sono maggiori, \u00e8 possibile",
"che siano necessarie pi\u00f9 operazioni di I/O sul disco."
};

static final String[] newVersionHelp =
{
"La versione corrente del caricatore per bootstrap",
"Windchill \u00e8 contenuta nel percorso locale per le classi Java.",
"",
"\u00c8 possibile scaricare una versione pi\u00f9 recente del caricatore,",
"ma non \u00e8 necessario eseguirne l'installazione adesso.",
"",
"Questo messaggio \u00e8 solo informativo.  Continuer\u00e0 a essere attivo,",
"il caricatore attualmente installato."
};

static final Object[][] contents =
{
{SETUP_HELP, setupHelp},
{DOWNLOAD_HELP, downloadHelp},
{NEW_VERSION_HELP, newVersionHelp},
{SETUP_LABEL, "Impostazione bootstrap Windchill"},
{SETUP_INSTRUCTIONS, "Salvataggio delle proprietà di bootstrap. Selezionare la posizione della cache locale."},
{SETUP_CACHE_LOCATION_LABEL, "Posizione cache:"},
{SETUP_PROPERTIES_LOCATION_LABEL, "File delle proprietà di bootstrap:"},
{SETUP_CACHE_LOCATION_BROWSE_BUTTON, "Sfoglia..."},
{SETUP_OK_BUTTON, "Ok"},
{SETUP_CANCEL_BUTTON, "Annulla"},
{SETUP_HELP_BUTTON, "Guida"},
{SETUP_DIALOG_TITLE, "Installazione"},
{SETUP_BROWSE_DIALOG_TITLE, "Posizione cache"},
{SETUP_BROWSE_DIALOG_FILE, "File in cache"},
{DOWNLOAD_LABEL, "Scaricamento bootstrap Windchill"},
{DOWNLOAD_INSTRUCTIONS, "È possibile scaricare una nuova versione del seguente file collocato nella cache locale:"},
{DOWNLOAD_REMOTE_FILE_LABEL, "File remoto:"},
{DOWNLOAD_REMOTE_MODIFIED_LABEL, "Data ultima modifica:"},
{DOWNLOAD_SIZE_LABEL, "Dimensione:"},
{DOWNLOAD_LOCAL_FILE_LABEL, "File locale:"},
{DOWNLOAD_LOCAL_MODIFIED_LABEL, "Data ultima modifica:"},
{DOWNLOAD_INFLATE_RADIO_BUTTON, "Scarica e completa il nuovo file"},
{DOWNLOAD_AS_IS_RADIO_BUTTON, "Scarica il nuovo file senza modifiche"},
{DOWNLOAD_OLD_FILE_RADIO_BUTTON, "Usa il vecchio file locale"},
{DOWNLOAD_IGNORE_RADIO_BUTTON, "Ignora il file locale"},
{DOWNLOAD_CONTINUE_BUTTON, "Continua"},
{DOWNLOAD_HELP_BUTTON, "Guida"},
{DOWNLOAD_DIALOG_TITLE, "Scarica"},
{DOWNLOAD_PROGRESS_LABEL, "Scaricamento in corso"},
{DOWNLOAD_PROGRESS_TOTAL_LABEL, "Dimensione totale:"},
{DOWNLOAD_PROGRESS_RECEIVED_LABEL, "Ricevuto:"},
{DOWNLOAD_PROGRESS_PERCENT_LABEL, "Completo:"},
{DOWNLOAD_PROGRESS_CANCEL_BUTTON, "Annulla"},
{DOWNLOAD_PROGRESS_DIALOG_TITLE, "Avanzamento"},
{INFLATE_PROGRESS_LABEL, "Completamento"},
{INFLATE_PROGRESS_TOTAL_LABEL, "Totale voci:"},
{INFLATE_PROGRESS_INFLATED_ENTRIES_LABEL, "Voci inserite:"},
{INFLATE_PROGRESS_PERCENT_LABEL, "Completo:"},
{INFLATE_PROGRESS_CANCEL_BUTTON, "Annulla"},
{INFLATE_PROGRESS_DIALOG_TITLE, "Avanzamento"},
{EXCEPTION_LABEL, "Eccezione di bootstrap Windchill"},
{EXCEPTION_DESCRIPTION, "La cache locale di bootstrap sarà ignorata."},
{EXCEPTION_CONTINUE_BUTTON, "Continua"},
{EXCEPTION_DIALOG_TITLE, "Eccezione di bootstrap"},
{HELP_OK_BUTTON, "Ok"},
{HELP_DIALOG_TITLE, "Guida bootstrap"},
{NEW_VERSION_ANNOUNCEMENT_LABEL, "È disponibile una nuova versione del caricatore di bootstrap Windchill."},
{NEW_VERSION_REMOTE_VERSION_LABEL, "Versione remota:"},
{NEW_VERSION_LOCAL_VERSION_LABEL, "Versione locale:"},
{NEW_VERSION_DOWNLOAD_URL_LABEL, "Scarica URL:"},
{NEW_VERSION_CONTINUE_BUTTON, "Continua"},
{NEW_VERSION_DOWNLOAD_BUTTON, "Scarica"},
{NEW_VERSION_HELP_BUTTON, "Guida"},
{NEW_VERSION_DIALOG_TITLE, "Nuovo caricatore disponibile"},
{MISSING_PARAMETER_EXCEPTION, "parametro {0} mancante"},
{BOOTSTRAP_LOADER_INSTALLED, "Il caricatore di bootstrap Windchill è stato installato."},
{BOOTSTRAP_LOADER_NOT_INSTALLED, "Il caricatore di bootstrap Windchill non è stato installato."},
{NOT_FOUND_OR_MISSING_DATE, "{0} non è stato trovato o è privo di data di modifica."},
{NOT_AVAILABLE, "{0} non disponibile"},
{IGNORING_LOCAL_CACHE, "La cache locale verrà ignorata."},
};
}
