/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */

package wt.boot;

import java.util.ListResourceBundle;

/**
 * Default BootRB message resource bundle [English/US]
 *
 * Usage notes:
 * DO NOT REUSE IDS.  If an message becomes obsolete, comment it out
 * but do not create a new message with that id.
 *
 **/
public class BootRB extends ListResourceBundle {

   public static final String SETUP_HELP = "1";
   public static final String DOWNLOAD_HELP = "2";
   public static final String NEW_VERSION_HELP = "3";
   public static final String SETUP_LABEL = "4";
   public static final String SETUP_INSTRUCTIONS = "5";
   public static final String SETUP_CACHE_LOCATION_LABEL = "6";
   public static final String SETUP_PROPERTIES_LOCATION_LABEL = "7";
   public static final String SETUP_CACHE_LOCATION_BROWSE_BUTTON = "8";
   public static final String SETUP_OK_BUTTON = "9";
   public static final String SETUP_CANCEL_BUTTON = "10";
   public static final String SETUP_HELP_BUTTON = "11";
   public static final String SETUP_DIALOG_TITLE = "12";
   public static final String SETUP_BROWSE_DIALOG_TITLE = "13";
   public static final String SETUP_BROWSE_DIALOG_FILE = "14";
   public static final String DOWNLOAD_LABEL = "15";
   public static final String DOWNLOAD_INSTRUCTIONS = "16";
   public static final String DOWNLOAD_REMOTE_FILE_LABEL = "17";
   public static final String DOWNLOAD_REMOTE_MODIFIED_LABEL = "18";
   public static final String DOWNLOAD_SIZE_LABEL = "19";
   public static final String DOWNLOAD_LOCAL_FILE_LABEL = "20";
   public static final String DOWNLOAD_LOCAL_MODIFIED_LABEL = "21";
   public static final String DOWNLOAD_INFLATE_RADIO_BUTTON = "22";
   public static final String DOWNLOAD_AS_IS_RADIO_BUTTON = "23";
   public static final String DOWNLOAD_IGNORE_RADIO_BUTTON = "24";
   public static final String DOWNLOAD_CONTINUE_BUTTON = "25";
   public static final String DOWNLOAD_HELP_BUTTON = "26";
   public static final String DOWNLOAD_DIALOG_TITLE = "27";
   public static final String DOWNLOAD_PROGRESS_LABEL = "28";
   public static final String DOWNLOAD_PROGRESS_TOTAL_LABEL = "29";
   public static final String DOWNLOAD_PROGRESS_RECEIVED_LABEL = "30";
   public static final String DOWNLOAD_PROGRESS_PERCENT_LABEL = "31";
   public static final String DOWNLOAD_PROGRESS_CANCEL_BUTTON = "32";
   public static final String DOWNLOAD_PROGRESS_DIALOG_TITLE = "33";
   public static final String INFLATE_PROGRESS_LABEL = "34";
   public static final String INFLATE_PROGRESS_INFLATED_ENTRIES_LABEL = "35";
   public static final String INFLATE_PROGRESS_CANCEL_BUTTON = "36";
   public static final String INFLATE_PROGRESS_DIALOG_TITLE = "37";
   public static final String EXCEPTION_LABEL = "38";
   public static final String EXCEPTION_DESCRIPTION = "39";
   public static final String EXCEPTION_CONTINUE_BUTTON = "40";
   public static final String EXCEPTION_DIALOG_TITLE = "41";
   public static final String HELP_OK_BUTTON = "42";
   public static final String HELP_DIALOG_TITLE = "43";
   public static final String NEW_VERSION_ANNOUNCEMENT_LABEL = "44";
   public static final String NEW_VERSION_REMOTE_VERSION_LABEL = "45";
   public static final String NEW_VERSION_LOCAL_VERSION_LABEL = "46";
   public static final String NEW_VERSION_DOWNLOAD_URL_LABEL = "47";
   public static final String NEW_VERSION_CONTINUE_BUTTON = "48";
   public static final String NEW_VERSION_DIALOG_TITLE = "49";
   public static final String MISSING_PARAMETER_EXCEPTION = "50";
   public static final String INFLATE_PROGRESS_TOTAL_LABEL = "51";
   public static final String INFLATE_PROGRESS_PERCENT_LABEL = "52";
   public static final String NEW_VERSION_DOWNLOAD_BUTTON = "53";
   public static final String NEW_VERSION_HELP_BUTTON = "54";
   public static final String DOWNLOAD_OLD_FILE_RADIO_BUTTON = "55";
   public static final String BOOTSTRAP_LOADER_INSTALLED = "56";
   public static final String BOOTSTRAP_LOADER_NOT_INSTALLED = "57";
   public static final String NOT_FOUND_OR_MISSING_DATE = "58";
   public static final String NOT_AVAILABLE = "59";
   public static final String IGNORING_LOCAL_CACHE = "60";
   
   public Object[][] getContents()
   {
      return contents;
   }

   static final String[] setupHelp =
   {
      "The Windchill Bootstrap Loader uses a local cache of",
      "JAR or ZIP files to load classes and resource files",
      "when executing Java applets or applications.  Each",
      "JAR or ZIP file corresponds to the contents of a remote",
      "codebase.",
      "",
      "Please enter the location of a local directory where",
      "the local files will be cached.  The bootstrap loader",
      "will store local files in subdirectories of this directory",
      "corresponding to each remote codebase.",
      "",
      "The cache location is stored in a properties file",
      "called .wtboot.properties found in the directory",
      "identified by the user.home Java system property."
   };

   static final String[] downloadHelp =
   {
      "A newer version of a locally cached JAR or ZIP file",
      "has been detected in the remote codebase.  You may",
      "download the file now or chose to ignore it until later.",
      "",
      "When downloading, the file may optionally be inflated",
      "since it was most likely created as a compressed file.",
      "Inflating the file immediately is recommended because",
      "it reduces the CPU overhead when reading entries",
      "later, but the resulting file is larger which possibly",
      "results in more disk I/O."
   };

   static final String[] newVersionHelp =
   {
      "You currently have a version of the Windchill Bootstrap",
      "Loader installed in your local Java class path.",
      "",
      "A newer version of the loader is available for download,",
      "but you are not required to install it at this time.",
      "",
      "This is just an informative message.  When you continue,",
      "the currently installed loader will remain in effect."
   };

   static final Object[][] contents =
   {
      {SETUP_HELP, setupHelp},
      {DOWNLOAD_HELP, downloadHelp},
      {NEW_VERSION_HELP, newVersionHelp},
      {SETUP_LABEL, "Windchill Bootstrap Setup"},
      {SETUP_INSTRUCTIONS, "Saving bootstrap properties.  Please select local cache location."},
      {SETUP_CACHE_LOCATION_LABEL, "Cache Location:"},
      {SETUP_PROPERTIES_LOCATION_LABEL, "Bootstrap Properties File:"},
      {SETUP_CACHE_LOCATION_BROWSE_BUTTON, "Browse..."},
      {SETUP_OK_BUTTON, "Ok"},
      {SETUP_CANCEL_BUTTON, "Cancel"},
      {SETUP_HELP_BUTTON, "Help"},
      {SETUP_DIALOG_TITLE, "Setup"},
      {SETUP_BROWSE_DIALOG_TITLE, "Cache Location"},
      {SETUP_BROWSE_DIALOG_FILE, "Cached-Files"},
      {DOWNLOAD_LABEL, "Windchill Bootstrap Download"},
      {DOWNLOAD_INSTRUCTIONS, "A new version of the following locally cached file is available for download:"},
      {DOWNLOAD_REMOTE_FILE_LABEL, "Remote File:"},
      {DOWNLOAD_REMOTE_MODIFIED_LABEL, "Last Modified:"},
      {DOWNLOAD_SIZE_LABEL, "Size:"},
      {DOWNLOAD_LOCAL_FILE_LABEL, "Local File:"},
      {DOWNLOAD_LOCAL_MODIFIED_LABEL, "Last Modified:"},
      {DOWNLOAD_INFLATE_RADIO_BUTTON, "Download and inflate new file"},
      {DOWNLOAD_AS_IS_RADIO_BUTTON, "Download new file as is"},
      {DOWNLOAD_OLD_FILE_RADIO_BUTTON, "Use old local file"},
      {DOWNLOAD_IGNORE_RADIO_BUTTON, "Ignore local file"},
      {DOWNLOAD_CONTINUE_BUTTON, "Continue"},
      {DOWNLOAD_HELP_BUTTON, "Help"},
      {DOWNLOAD_DIALOG_TITLE, "Download"},
      {DOWNLOAD_PROGRESS_LABEL, "Downloading"},
      {DOWNLOAD_PROGRESS_TOTAL_LABEL, "Total Size:"},
      {DOWNLOAD_PROGRESS_RECEIVED_LABEL, "Received:"},
      {DOWNLOAD_PROGRESS_PERCENT_LABEL, "Complete:"},
      {DOWNLOAD_PROGRESS_CANCEL_BUTTON, "Cancel"},
      {DOWNLOAD_PROGRESS_DIALOG_TITLE, "Progress"},
      {INFLATE_PROGRESS_LABEL, "Inflating"},
      {INFLATE_PROGRESS_TOTAL_LABEL, "Total Entries:"},
      {INFLATE_PROGRESS_INFLATED_ENTRIES_LABEL, "Inflated Entries:"},
      {INFLATE_PROGRESS_PERCENT_LABEL, "Complete:"},
      {INFLATE_PROGRESS_CANCEL_BUTTON, "Cancel"},
      {INFLATE_PROGRESS_DIALOG_TITLE, "Progress"},
      {EXCEPTION_LABEL, "Windchill Bootstrap Exception"},
      {EXCEPTION_DESCRIPTION, "The local bootstrap cache will be ignored."},
      {EXCEPTION_CONTINUE_BUTTON, "Continue"},
      {EXCEPTION_DIALOG_TITLE, "Bootstrap Exception"},
      {HELP_OK_BUTTON, "Ok"},
      {HELP_DIALOG_TITLE, "Bootstrap Help"},
      {NEW_VERSION_ANNOUNCEMENT_LABEL, "A new version of the Windchill Bootstrap Loader is available."},
      {NEW_VERSION_REMOTE_VERSION_LABEL, "Remote Version:"},
      {NEW_VERSION_LOCAL_VERSION_LABEL, "Local Version:"},
      {NEW_VERSION_DOWNLOAD_URL_LABEL, "Download URL:"},
      {NEW_VERSION_CONTINUE_BUTTON, "Continue"},
      {NEW_VERSION_DOWNLOAD_BUTTON, "Download"},
      {NEW_VERSION_HELP_BUTTON, "Help"},
      {NEW_VERSION_DIALOG_TITLE, "New Loader Available"},
      {MISSING_PARAMETER_EXCEPTION, "missing {0} parameter"}, // {0} is a parameter name
      {BOOTSTRAP_LOADER_INSTALLED, "Windchill bootstrap loader installed."},
      {BOOTSTRAP_LOADER_NOT_INSTALLED, "Windchill bootstrap loader not installed."},
      {NOT_FOUND_OR_MISSING_DATE, "{0} not found or missing modification date."}, // {0} is a URL
      {NOT_AVAILABLE, "{0} not available"}, // {0} is a URL
      {IGNORING_LOCAL_CACHE, "Ignoring local cache."}
   };
}
