/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */

package wt.boot;

import java.util.ListResourceBundle;

/**
 * Default BootRB message resource bundle [English/US]
 *
 * Usage notes:
 * DO NOT REUSE IDS.  If an message becomes obsolete, comment it out
 * but do not create a new message with that id.
 *
 **/
public class BootRB_en_GB extends ListResourceBundle {


   public Object[][] getContents()
   {
      return contents;
   }

   static final String[] setupHelp =
   {
      "The Windchill Bootstrap Loader uses a local cache of",
      "JAR or ZIP files to load classes and resource files",
      "when executing Java applets or applications.  Each",
      "JAR or ZIP file corresponds to the contents of a remote",
      "codebase.",
      "",
      "Please enter the location of a local directory where",
      "the local files will be cached.  The bootstrap loader",
      "will store local files in subdirectories of this directory",
      "corresponding to each remote codebase.",
      "",
      "The cache location is stored in a properties file",
      "called .wtboot.properties found in the directory",
      "identified by the user.home Java system property."
   };

   static final String[] downloadHelp =
   {
      "A newer version of a locally cached JAR or ZIP file",
      "has been detected in the remote codebase.  You may",
      "download the file now or chose to ignore it until later.",
      "",
      "When downloading, the file may optionally be inflated",
      "since it was most likely created as a compressed file.",
      "Inflating the file immediately is recommended because",
      "it reduces the CPU overhead when reading entries",
      "later, but the resulting file is larger which possibly",
      "results in more disk I/O."
   };

   static final String[] newVersionHelp =
   {
      "You currently have a version of the Windchill Bootstrap",
      "Loader installed in your local Java class path.",
      "",
      "A newer version of the loader is available for download,",
      "but you are not required to install it at this time.",
      "",
      "This is just an informative message.  When you continue,",
      "the currently installed loader will remain in effect."
   };

   static final Object[][] contents =
   {
/*<NEW>*/      {BootRB.SETUP_HELP, setupHelp},
/*<NEW>*/      {BootRB.DOWNLOAD_HELP, downloadHelp},
/*<NEW>*/      {BootRB.NEW_VERSION_HELP, newVersionHelp},
/*<NEW>*/      {BootRB.SETUP_LABEL, "Windchill Bootstrap Setup"},
/*<NEW>*/      {BootRB.SETUP_INSTRUCTIONS, "Saving bootstrap properties.  Please select local cache location."},
/*<NEW>*/      {BootRB.SETUP_CACHE_LOCATION_LABEL, "Cache Location:"},
/*<NEW>*/      {BootRB.SETUP_PROPERTIES_LOCATION_LABEL, "Bootstrap Properties File:"},
/*<NEW>*/      {BootRB.SETUP_CACHE_LOCATION_BROWSE_BUTTON, "Browse..."},
/*<NEW>*/      {BootRB.SETUP_OK_BUTTON, "Ok"},
/*<NEW>*/      {BootRB.SETUP_CANCEL_BUTTON, "Cancel"},
/*<NEW>*/      {BootRB.SETUP_HELP_BUTTON, "Help"},
/*<NEW>*/      {BootRB.SETUP_DIALOG_TITLE, "Setup"},
/*<NEW>*/      {BootRB.SETUP_BROWSE_DIALOG_TITLE, "Cache Location"},
/*<NEW>*/      {BootRB.SETUP_BROWSE_DIALOG_FILE, "Cached-Files"},
/*<NEW>*/      {BootRB.DOWNLOAD_LABEL, "Windchill Bootstrap Download"},
/*<NEW>*/      {BootRB.DOWNLOAD_INSTRUCTIONS, "A new version of the following locally cached file is available for download:"},
/*<NEW>*/      {BootRB.DOWNLOAD_REMOTE_FILE_LABEL, "Remote File:"},
/*<NEW>*/      {BootRB.DOWNLOAD_REMOTE_MODIFIED_LABEL, "Last Modified:"},
/*<NEW>*/      {BootRB.DOWNLOAD_SIZE_LABEL, "Size:"},
/*<NEW>*/      {BootRB.DOWNLOAD_LOCAL_FILE_LABEL, "Local File:"},
/*<NEW>*/      {BootRB.DOWNLOAD_LOCAL_MODIFIED_LABEL, "Last Modified:"},
/*<NEW>*/      {BootRB.DOWNLOAD_INFLATE_RADIO_BUTTON, "Download and inflate new file"},
/*<NEW>*/      {BootRB.DOWNLOAD_AS_IS_RADIO_BUTTON, "Download new file as is"},
/*<NEW>*/      {BootRB.DOWNLOAD_OLD_FILE_RADIO_BUTTON, "Use old local file"},
/*<NEW>*/      {BootRB.DOWNLOAD_IGNORE_RADIO_BUTTON, "Ignore local file"},
/*<NEW>*/      {BootRB.DOWNLOAD_CONTINUE_BUTTON, "Continue"},
/*<NEW>*/      {BootRB.DOWNLOAD_HELP_BUTTON, "Help"},
/*<NEW>*/      {BootRB.DOWNLOAD_DIALOG_TITLE, "Download"},
/*<NEW>*/      {BootRB.DOWNLOAD_PROGRESS_LABEL, "Downloading"},
/*<NEW>*/      {BootRB.DOWNLOAD_PROGRESS_TOTAL_LABEL, "Total Size:"},
/*<NEW>*/      {BootRB.DOWNLOAD_PROGRESS_RECEIVED_LABEL, "Received:"},
/*<NEW>*/      {BootRB.DOWNLOAD_PROGRESS_PERCENT_LABEL, "Complete:"},
/*<NEW>*/      {BootRB.DOWNLOAD_PROGRESS_CANCEL_BUTTON, "Cancel"},
/*<NEW>*/      {BootRB.DOWNLOAD_PROGRESS_DIALOG_TITLE, "Progress"},
/*<NEW>*/      {BootRB.INFLATE_PROGRESS_LABEL, "Inflating"},
/*<NEW>*/      {BootRB.INFLATE_PROGRESS_TOTAL_LABEL, "Total Entries:"},
/*<NEW>*/      {BootRB.INFLATE_PROGRESS_INFLATED_ENTRIES_LABEL, "Inflated Entries:"},
/*<NEW>*/      {BootRB.INFLATE_PROGRESS_PERCENT_LABEL, "Complete:"},
/*<NEW>*/      {BootRB.INFLATE_PROGRESS_CANCEL_BUTTON, "Cancel"},
/*<NEW>*/      {BootRB.INFLATE_PROGRESS_DIALOG_TITLE, "Progress"},
/*<NEW>*/      {BootRB.EXCEPTION_LABEL, "Windchill Bootstrap Exception"},
/*<NEW>*/      {BootRB.EXCEPTION_DESCRIPTION, "The local bootstrap cache will be ignored."},
/*<NEW>*/      {BootRB.EXCEPTION_CONTINUE_BUTTON, "Continue"},
/*<NEW>*/      {BootRB.EXCEPTION_DIALOG_TITLE, "Bootstrap Exception"},
/*<NEW>*/      {BootRB.HELP_OK_BUTTON, "Ok"},
/*<NEW>*/      {BootRB.HELP_DIALOG_TITLE, "Bootstrap Help"},
/*<NEW>*/      {BootRB.NEW_VERSION_ANNOUNCEMENT_LABEL, "A new version of the Windchill Bootstrap Loader is available."},
/*<NEW>*/      {BootRB.NEW_VERSION_REMOTE_VERSION_LABEL, "Remote Version:"},
/*<NEW>*/      {BootRB.NEW_VERSION_LOCAL_VERSION_LABEL, "Local Version:"},
/*<NEW>*/      {BootRB.NEW_VERSION_DOWNLOAD_URL_LABEL, "Download URL:"},
/*<NEW>*/      {BootRB.NEW_VERSION_CONTINUE_BUTTON, "Continue"},
/*<NEW>*/      {BootRB.NEW_VERSION_DOWNLOAD_BUTTON, "Download"},
/*<NEW>*/      {BootRB.NEW_VERSION_HELP_BUTTON, "Help"},
/*<NEW>*/      {BootRB.NEW_VERSION_DIALOG_TITLE, "New Loader Available"},
/*<NEW>*/      {BootRB.MISSING_PARAMETER_EXCEPTION, "missing {0} parameter"}, // {0} is a parameter name
/*<NEW>*/      {BootRB.BOOTSTRAP_LOADER_INSTALLED, "Windchill bootstrap loader installed."},
/*<NEW>*/      {BootRB.BOOTSTRAP_LOADER_NOT_INSTALLED, "Windchill bootstrap loader not installed."},
/*<NEW>*/      {BootRB.NOT_FOUND_OR_MISSING_DATE, "{0} not found or missing modification date."}, // {0} is a URL
/*<NEW>*/      {BootRB.NOT_AVAILABLE, "{0} not available"}, // {0} is a URL
/*<NEW>*/      {BootRB.IGNORING_LOCAL_CACHE, "Ignoring local cache."}
   };
}
