/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.viewmarkup;

import wt.util.resource.*;

@RBUUID("loadFiles.wvs.handlers.configurationTemplateLoadResource")
public final class configurationTemplateLoadResource extends WTListResourceBundle {

   @RBEntry("The following files do not exist in {0}: {1}")
   @RBArgComment0("{0} refers to the configuration template upload directory.")
   @RBArgComment1("{1} refers to the files that are not found in the directory.")
   public static final String MISSING_FILES = "MISSING_FILES";

   @RBEntry("The file, {0}, cannot be uploaded to configuration template {1} because it already contains a file by that name.")
   @RBArgComment0("{0} refers to the file that is being uploaded to the configuration template.")
   @RBArgComment1("{1} refers to the name of the configuration template.")
   public static final String FILE_ALREADY_EXISTS = "FILE_ALREADY_EXISTS";

   @RBEntry("Multiple references to the file, {0}, encountered in load file.")
   @RBArgComment0("{0} refers to a file referenced more than once in the load file.")
   public static final String DUPLICATE_REFERENCES = "DUPLICATE_REFERENCES";

   @RBEntry("{0} failed schema validation - {1}.")
   @RBArgComment0("{0} refers to the file that failed schema validation.")
   @RBArgComment1("{1} provides details about the failure.")
   public static final String SCHEMA_VALIDATION_FAILED = "SCHEMA_VALIDATION_FAILED";
}
