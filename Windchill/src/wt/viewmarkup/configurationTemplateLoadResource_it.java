/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.viewmarkup;

import wt.util.resource.*;

@RBUUID("loadFiles.wvs.handlers.configurationTemplateLoadResource")
public final class configurationTemplateLoadResource_it extends WTListResourceBundle {

   @RBEntry("I file seguenti non esistono in {0}: {1}")
   @RBArgComment0("{0} refers to the configuration template upload directory.")
   @RBArgComment1("{1} refers to the files that are not found in the directory.")
   public static final String MISSING_FILES = "MISSING_FILES";

   @RBEntry("Impossibile caricare il file {0} nel modello di configurazione {1}, poiché contiene già un file con lo stesso nome.")
   @RBArgComment0("{0} refers to the file that is being uploaded to the configuration template.")
   @RBArgComment1("{1} refers to the name of the configuration template.")
   public static final String FILE_ALREADY_EXISTS = "FILE_ALREADY_EXISTS";

   @RBEntry("Il file di caricamento contiene più riferimenti al file {0}.")
   @RBArgComment0("{0} refers to a file referenced more than once in the load file.")
   public static final String DUPLICATE_REFERENCES = "DUPLICATE_REFERENCES";

   @RBEntry("Convalida dello schema non riuscita per il file {0} - {1}.")
   @RBArgComment0("{0} refers to the file that failed schema validation.")
   @RBArgComment1("{1} provides details about the failure.")
   public static final String SCHEMA_VALIDATION_FAILED = "SCHEMA_VALIDATION_FAILED";
}
