/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.viewmarkup;

import wt.util.resource.*;

@RBUUID("wt.viewmarkup.viewmarkupResource")
public final class viewmarkupResource_it extends WTListResourceBundle {

    @RBEntry("Il markup trasmesso non ha un elemento visualizzabile valido. Contattare l'amministratore di sistema.")
    public static final String INVALID_MARKUP_LINK = "INVALID_MARKUP_LINK";

    @RBEntry("Parametri dell'applet non validi: {0} ")
    public static final String INVALID_PARAMETERS = "INVALID_PARAMETERS";

    @RBEntry("Errore durante il recupero dell'oggetto: {0} ")
    public static final String RETRIEVING_OBJECT = "RETRIEVING_OBJECT";

    @RBEntry("Crea nuova immagine derivata")
    public static final String CREATE_NEW_D_I = "CREATE_NEW_D_I";

    @RBEntry("All'oggetto sono associate troppe miniature. Contattare l'amministratore di sistema.")
    public static final String TOO_MANY_THUMBNAILS = "TOO_MANY_THUMBNAILS";

    @RBEntry("Nessuna miniatura è associata all'oggetto.")
    public static final String NO_THUMBNAILS = "NO_THUMBNAILS";

    @RBEntry("Non si dispone dei permessi per leggere l'oggetto rappresentabile {0}.")
    public static final String NO_PERMISSION_REP = "NO_PERMISSION_REP";

    @RBEntry("L'oggetto derivedFromConfigSpec associato a DerivedImage {0} non implementa l'interfaccia ConfigSpec.")
    public static final String NOT_A_CONFIG_SPEC = "NOT_A_CONFIG_SPEC";

    @RBEntry("La spec config trasmessa al metodo {0} deve essere un WTObject.")
    public static final String CONFIG_SPEC_NOT_A_WTOBJECT = "CONFIG_SPEC_NOT_A_WTOBJECT";

    @RBEntry("Il DerivedImage trasmesso al metodo {0} deve essere persistente.")
    public static final String IMAGE_NOT_PERSISTED = "IMAGE_NOT_PERSISTED";

    @RBEntry("VISUALIZZAZIONE E MARKUP")
    public static final String VIEWS_AND_MARKUPS = "VIEWS_AND_MARKUPS";

    @RBEntry("L'elemento visualizzabile (Viewable) trasmesso non ha un oggetto graficamente rappresentabile (GraphicallyRepresentable) valido. Contattare l'amministratore di sistema.")
    public static final String INVALID_GR_LINK = "INVALID_GR_LINK";

    @RBEntry("Non si dispone dei permessi per modificare l'elemento visualizzabile (Viewable) trasmesso - {0}")
    public static final String NO_PERMISSION_VIEWABLE = "NO_PERMISSION_VIEWABLE";

    @RBEntry("Non si dispone dei permessi per modificare l'oggetto rappresentabile trasmesso")
    public static final String NO_PERMISSION_GR = "NO_PERMISSION_GR";

    @RBEntry("Il markup {0} è al momento bloccato da un altro utente")
    public static final String MARKUP_LOCKED = "MARKUP_LOCKED";

    @RBEntry("Visualizza/Markup")
    public static final String VIEW_MARKUP_LABEL = "VIEW_MARKUP_LABEL";

    @RBEntry("La classe DisplayMarkUpDelegate deve essere istanziata con un elemento visualizzabile (Viewable). Contattare l'amministratore di sistema.")
    public static final String NOT_A_VIEWABLE = "NOT_A_VIEWABLE";

    @RBEntry("Errore durante l'inizializzazione statica della classe {0}.")
    public static final String ERROR_INITIALIZING = "ERROR_INITIALIZING";

    @RBEntry("Markup")
    public static final String MARKUPS = "MARKUPS";

    @RBEntry("Rappresentazione")
    @RBComment("Word to be displayed instead of DerivedImage")
    public static final String DERIVEDIMAGE_DISPLAY_NAME = "DERIVEDIMAGE_DISPLAY_NAME";

    @RBEntry("Markup")
    @RBComment("Word to be displayed instead of WTMark Up")
    public static final String WTMARKUP_DISPLAY_NAME = "WTMARKUP_DISPLAY_NAME";
    
    @RBEntry("Report rilevamento interferenze")
    @RBComment("Word to be displayed if we are dealing with an Interference Detection Report")
    public static final String TN_REPORT = "TN_REPORT";
    
    @RBEntry("Interferenza")
    @RBComment("Interference Detection type Interference")
    public static final String TN_HARD = "TN_HARD";

    @RBEntry("Distanza")
    @RBComment("Interference Detection type Clearance")
    public static final String TN_SOFT = "TN_SOFT";

    @RBEntry("Contatto")
    @RBComment("Interference Detection type Contact")
    public static final String TN_TOUCH = "TN_TOUCH";
    
    @RBEntry("Per il markup {1} non esiste un modello del ciclo di vita \"{0}\". Contattare l'amministratore di sistema.")
    @RBArgComment0("The name of the Life Cycle Template.")
    @RBArgComment1("The identity of the MarkUp - which is it's name.")
    @RBComment("System Administrators can specify a Life Cycle Template to be used for Interference or Interference Report MarkUps using a preference. This message will get displayed if the Life Cycle Template doesn't exist or is not enabled when the View MarkUp Service attempts to create a Life Cycle Managed MarkUp - which includes Interference MarkUps and Interference Report MarkUps.")
    public static final String MARKUP_NO_LIFE_CYCLE_TEMPLATE = "MARKUP_NO_LIFE_CYCLE_TEMPLATE";
}
