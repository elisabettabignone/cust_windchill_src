/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.viewmarkup;

import wt.util.resource.*;

@RBUUID("wt.viewmarkup.viewmarkupResource")
public final class viewmarkupResource extends WTListResourceBundle {

    @RBEntry("The markup passed does not have a valid Viewable. Contact your system administrator.")
    public static final String INVALID_MARKUP_LINK = "INVALID_MARKUP_LINK";

    @RBEntry("Invalid applet parameters: {0} ")
    public static final String INVALID_PARAMETERS = "INVALID_PARAMETERS";

    @RBEntry("Exception retrieving the object: {0} ")
    public static final String RETRIEVING_OBJECT = "RETRIEVING_OBJECT";

    @RBEntry("Create New Derived Image")
    public static final String CREATE_NEW_D_I = "CREATE_NEW_D_I";

    @RBEntry("Object has too many thumbnails associated with it. Contact your system administrator.")
    public static final String TOO_MANY_THUMBNAILS = "TOO_MANY_THUMBNAILS";

    @RBEntry("Object has no thumbnails associated with it.")
    public static final String NO_THUMBNAILS = "NO_THUMBNAILS";

    @RBEntry("You do not have permission to read the Representable object {0}.")
    public static final String NO_PERMISSION_REP = "NO_PERMISSION_REP";

    @RBEntry("The derivedFromConfigSpec object associated with DerivedImage {0} does not implement the ConfigSpec interface.")
    public static final String NOT_A_CONFIG_SPEC = "NOT_A_CONFIG_SPEC";

    @RBEntry("The config spec passed to method {0} must be a WTObject.")
    public static final String CONFIG_SPEC_NOT_A_WTOBJECT = "CONFIG_SPEC_NOT_A_WTOBJECT";

    @RBEntry("The DerivedImage passed to method {0} must be persisted.")
    public static final String IMAGE_NOT_PERSISTED = "IMAGE_NOT_PERSISTED";

    @RBEntry("VIEWS AND MARKUPS")
    public static final String VIEWS_AND_MARKUPS = "VIEWS_AND_MARKUPS";

    @RBEntry("The Viewable passed does not have a valid GraphicallyRepresentable. Contact your system administrator.")
    public static final String INVALID_GR_LINK = "INVALID_GR_LINK";

    @RBEntry("You do not have permission to modify the passed Viewable - {0}")
    public static final String NO_PERMISSION_VIEWABLE = "NO_PERMISSION_VIEWABLE";

    @RBEntry("You do not have permission to modify the passed Representable")
    public static final String NO_PERMISSION_GR = "NO_PERMISSION_GR";

    @RBEntry("The markup {0} is currently locked by another user")
    public static final String MARKUP_LOCKED = "MARKUP_LOCKED";

    @RBEntry("View/Markup")
    public static final String VIEW_MARKUP_LABEL = "VIEW_MARKUP_LABEL";

    @RBEntry("The DisplayMarkUpDelegate class must be instantiated with a Viewable. Contact your system administrator.")
    public static final String NOT_A_VIEWABLE = "NOT_A_VIEWABLE";

    @RBEntry("Error during static initialization of class {0}.")
    public static final String ERROR_INITIALIZING = "ERROR_INITIALIZING";

    @RBEntry("Markups")
    public static final String MARKUPS = "MARKUPS";

    @RBEntry("Representation")
    @RBComment("Word to be displayed instead of DerivedImage")
    public static final String DERIVEDIMAGE_DISPLAY_NAME = "DERIVEDIMAGE_DISPLAY_NAME";

    @RBEntry("Markup")
    @RBComment("Word to be displayed instead of WTMark Up")
    public static final String WTMARKUP_DISPLAY_NAME = "WTMARKUP_DISPLAY_NAME";
    
    @RBEntry("Interference Detection Report")
    @RBComment("Word to be displayed if we are dealing with an Interference Detection Report")
    public static final String TN_REPORT = "TN_REPORT";
    
    @RBEntry("Interference")
    @RBComment("Interference Detection type Interference")
    public static final String TN_HARD = "TN_HARD";

    @RBEntry("Clearance")
    @RBComment("Interference Detection type Clearance")
    public static final String TN_SOFT = "TN_SOFT";

    @RBEntry("Contact")
    @RBComment("Interference Detection type Contact")
    public static final String TN_TOUCH = "TN_TOUCH";
    
    @RBEntry("Life Cycle Template \"{0}\" doesn't exist for markup {1}. Contact your system administrator.")
    @RBArgComment0("The name of the Life Cycle Template.")
    @RBArgComment1("The identity of the MarkUp - which is it's name.")
    @RBComment("System Administrators can specify a Life Cycle Template to be used for Interference or Interference Report MarkUps using a preference. This message will get displayed if the Life Cycle Template doesn't exist or is not enabled when the View MarkUp Service attempts to create a Life Cycle Managed MarkUp - which includes Interference MarkUps and Interference Report MarkUps.")
    public static final String MARKUP_NO_LIFE_CYCLE_TEMPLATE = "MARKUP_NO_LIFE_CYCLE_TEMPLATE";
}
