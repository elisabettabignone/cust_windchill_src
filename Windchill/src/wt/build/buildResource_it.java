/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.build;

import wt.util.resource.*;

@RBUUID("wt.build.buildResource")
public final class buildResource_it extends WTListResourceBundle {
   /**
    * $$NONE
    **/
   @RBEntry("Il valore per l'argomento \"{0}\" non può essere nullo")
   public static final String NULL_ARGUMENT = "0";

   @RBEntry("Oggetto costruzione mancante. Non è stata trovata alcuna regola per la costruzione di \"{0}\"")
   public static final String NOTHING_TO_BUILD = "1";

   @RBEntry("Violazione di univocità: i link creati per \"{0}\" devono avere un'identificazione d'origine univoca. Contattare l'amministratore di sistema.")
   public static final String SOURCE_IDENTIFICATION_MUST_BE_UNIQUE = "2";

   @RBEntry("Fornire identificatore per questo link.")
   public static final String MUST_PROVIDE_IDENTIFIER = "3";

   @RBEntry("Argomento \"buildRule\" non valido: \"{0}\" non è un tipo di \"{1}\"")
   public static final String TFS_DELEGATE_INVALID_CLASS = "4";

   @RBEntry("Impossibile aggiornare o eliminare il link perché è di proprietà di Build EPM.")
   public static final String BUILDABLE_LINK_INVALID_ACTION = "5";

   @RBEntry("L'oggetto \"{0}\" non è un oggetto origine valido per il link \"{1}\"")
   public static final String INVALID_SOURCE_CLASS = "6";

   @RBEntry("Impossibile creare: non si dispone dell'accesso necessario")
   public static final String ACCESS_FAILURE = "7";

   @RBEntry("È possibile creare solo le ultime iterazioni")
   public static final String NON_LATEST_ITERATION = "8";

   @RBEntry("Nessun TargetsForSourcesDelegate implementato per \"{0}\".  Contattare l'amministratore di sistema.")
   public static final String TFS_DELEGATE_NOT_IMPLEMENTED = "9";

   @RBEntry("Argomento \"{0}\", oggetto \"{1}\", non \"{2}\" come previsto.")
   public static final String INVALID_DELEGATE_ARGUMENT_TYPE = "10";

   @RBEntry("A \"{0}\" sono associate delle regole. Non può essere eliminato.")
   public static final String CAN_NOT_DELETE = "11";

   @RBEntry("La destinazione è in una baseline: impossibile creare localmente")
   public static final String BASELINEABLE_CAN_NOT_BE_BUILT_LOCALLY = "12";

   @RBEntry("La destinazione non si trova nello schedario personale: impossibile creare localmente.")
   public static final String CABINETBASED_CAN_NOT_BE_BUILT_LOCALLY = "13";

   @RBEntry("L'utente non ha bloccato la destinazione: impossibile creare localmente")
   public static final String LOCKABLE_CAN_NOT_BE_BUILT_LOCALLY = "14";

   @RBEntry("L'utente non è proprietario della destinazione: impossibile creare localmente.")
   public static final String OWNABLE_CAN_NOT_BE_BUILT_LOCALLY = "15";

   @RBEntry("Impossibile creare \"{0}\": si trova nello schedario personale di un altro utente")
   public static final String CAN_NOT_BUILD_CABINET_BASED = "16";

   @RBEntry("Impossibile creare \"{0}\": è bloccato da un altro utente")
   public static final String CAN_NOT_BUILD_LOCKABLE = "17";

   @RBEntry("Impossibile creare \"{0}\": è proprietà di un altro utente")
   public static final String CAN_NOT_BUILD_OWNABLE = "18";

   @RBEntry("Impossibile creare \"{0}\": è una copia di Check-Out. È possibile creare solo le versioni sottoposte a Check-In o in modifica.")
   public static final String CAN_NOT_BUILD_WORKABLE = "19";

   @RBEntry("L'associazione BuildHistory è completamente gestita dalla build e non può essere creata, aggiornata o eliminata manualmente")
   public static final String CAN_NOT_CHANGE_HISTORY = "20";

   @RBEntry("Impossibile creare \"{0}\": ha \"{1}\" link verso \"{2}\". La build non può contenere più di un link.")
   public static final String TOO_MANY_LINKS_TO_SUBSUME = "21";

   @RBEntry("Impossibile modificare il nome o eliminare l'indicatore di riferimento perché è di proprietà di Build EPM")
   public static final String BUILDABLE_OCCURRENCE_INVALID_ACTION = "23";

   @RBEntry("Impossibile creare \"{0}\":  è stato sottoposto a Check-Out nel progetto. \"{0}\" ha regole di creazione con \"{1}\"")
   @RBComment("Error message when user tries to build a target and target is checked out to  project.")
   @RBArgComment0("Name of the build target.")
   @RBArgComment1("Name of the build source.")
   public static final String CANNOT_BUILD_WORKABLE_CHECKEDOUT_TO_SANDBOX = "24";

   @RBEntry("Impossibile creare regola di creazione fra \"{0}\" e \"{1}\" . \"{0}\" dispone già di una regola di creazione.")
   @RBComment("Error message to show when user tries to create link when a link already exists for build source or build target.")
   @RBArgComment0("Name of the source EPMDocument.")
   @RBArgComment1("Name of the target Part.")
   public static final String CANNOT_CREATE_LINK_IF_EXISTING_LINK = "25";

   @RBEntry("Impossibile creare \"{0}\":  appartiene già a qualcuno. \"{0}\" ha regole di creazione con \"{1}\"")
   @RBComment("Error message to show when user tries to build a target and target is owned by other user.")
   @RBArgComment0("Name of the build target.")
   @RBArgComment1("Name of the build source.")
   public static final String CAN_NOT_BUILD_OWNABLE_TARGET = "26";

   @RBEntry("Impossibile creare \"{0}\": la copia è sottoposta a Check-Out.  Solo le versioni in stato di Check-In o in modifica possono essere create. \"{0}\" ha regole di creazione con \"{1}\"")
   @RBComment("Error message to show when user tries to build a target and target is checked out copy.")
   @RBArgComment0("Name of the build target.")
   @RBArgComment1("Name of the build source.")
   public static final String CAN_NOT_BUILD_WORKABLE_TARGET = "27";

   @RBEntry("L'oggetto BuildSourceMaster deve essere di tipo IBAHolder.")
   @RBComment("BuildSourceMaster must implement IBAHolder interface.")
   public static final String BUILD_SOURCE_MASTER_MUST_BE_IBAHOLDER = "28";

   @RBEntry("L'oggetto BuildSourceMaster deve essere di tipo Master.")
   @RBComment("BuildSourceMaster must implement Master interface.")
   public static final String BUILD_SOURCE_MASTER_MUST_BE_MASTER = "29";

   @RBEntry("L'oggetto BuildTargetMaster deve essere di tipo IBAHolder.")
   @RBComment("BuildTargetMaster must implement IBAHolder interface.")
   public static final String BUILD_TARGET_MASTER_MUST_BE_IBAHOLDER = "30";

   @RBEntry("L'oggetto BuildTargetMaster deve essere di tipo Master.")
   @RBComment("BuildTargetMaster must implement Master interface.")
   public static final String BUILD_TARGET_MASTER_MUST_BE_MASTER = "31";

   @RBEntry("Non è stata trovata alcuna regola BuildRule per gli oggetti BuildSourceMaster specificati.")
   @RBComment("No BuildRule found for given BuildSourceMasters")
   public static final String NO_BUILD_RULE_FOR_BUILDSOURCEMASTER = "32";

   @RBEntry("Non è stata trovata alcuna regola BuildRule per gli oggetti BuildTargetMaster specificati.")
   @RBComment("No BuildRule found for given BuildTargetMasters")
   public static final String NO_BUILD_RULE_FOR_BUILDTARGETMASTER = "33";

   @RBEntry("Impossibile creare \"{0}\": gli oggetti sono in stato di Check-Out. È possibile creare solo le versioni in stato di Check-In.")
   @RBComment("Error message to show when user tries to build checkout versions of Build Sources or Build Targets.")
   @RBArgComment0("Names of the build targets and sources.")
   public static final String CAN_NOT_BUILD_CHECKED_OUT_COPY = "34";

   @RBEntry("Impossibile creare \"{0}\": gli oggetti sono in cartelle personali. È possibile creare solo gli oggetti negli schedari condivisi.")
   @RBComment("Error message to show when user tries to build Build Sources or Build Targets in personal folder.")
   @RBArgComment0("Names of the build targets and sources.")
   public static final String CAN_NOT_BUILD_IN_PERSONAL_FOLDER = "35";

   @RBEntry("Impossibile creare \"{0}\" perché già creati. Gli oggetti possono essere creati solamente una volta.")
   @RBComment("Error message to show when user tries to build already built Build Sources or Build Targets.")
   @RBArgComment0("Names of the build targets and sources.")
   public static final String CAN_NOT_BUILD_ALREADY_BUILT = "36";

   @RBEntry("Impossibile eliminare o modificare l'utilizzo di \"{0}\" in \"{1}\". Le modifiche devono essere effettuate nel documento CAD associato.")
   @RBComment("Error displayed when user attempts to remove usage or modify quantity of component WTPart used in assembly WTPart. This message is displayed if assembly and component WTParts are actively associated with CADDocuments.")
   @RBArgComment0("Display identity of component WTPartMaster")
   @RBArgComment1("Display identity of assembly WTPart.")
   public static final String CANNOT_CHANGE_BUILDABLE_LINK = "37";

   @RBEntry("Impossibile eliminare o modificare \"{0}\" in \"{1}\". Le modifiche devono essere effettuate nel documento CAD associato.")
   @RBComment("Error displayed when user attempts to remove part use occurrence in assembly WTPart. This message is displayed if assembly and component WTParts are actively associated with CADDocuments.")
   @RBArgComment0("Display identity of Part Uses Occurrence")
   @RBArgComment1("Display identity of assembly WTPart.")
   public static final String CANNOT_CHANGE_BUILDABLE_OCCURRENCE = "38";

   @RBEntry("Impossibile creare regola di creazione per \"{0}\" in quanto dispone già di una regola di creazione.")
   @RBComment("Error message to show when user tries to create link when a link already exists for build source or build target, and the user does not have a read permission.")
   @RBArgComment0("Name of the source EPMDocument.")
   public static final String CANNOT_CREATE_LINK_IF_EXISTING_LINK_NOPERMISSION = "39";

   @RBEntry("Impossibile sottoporre a Check-In a meno di non sottoporre a Check-In anche le parti/documenti \"{0}\" per aggiornare le associazioni.")
   @RBComment("Error message is shown when user is trying to checkin part/document but there is another associated part(with original copy of part/document) which is also required to be checked in.")
   @RBArgComment0("Name of the source EPMDocuments or target WTParts")
   public static final String CANNOT_CHECKIN_WITHOUT_ASSOCIATED_PART_DOC = "40";

   @RBEntry("Impossibile annullare il Check-Out a meno di non aggiornare tutte le associazioni con le parti/documenti \"{0}\".")
   @RBComment("Error message is shown when user is trying to undo-checkout part/document but there is another associated part/document which is also required to be undo-checkedout.")
   @RBArgComment0("Name of the source EPMDocuments of target WTParts")
   public static final String CANNOT_UNDOCHECKOUT_WITHOUT_ASSOCIATED_PART_DOC = "41";

   @RBEntry("\"{0}\": impossibile eseguire l'azione, in quanto non è possibile creare un'associazione con una parte a cui l'utente non ha accesso. Ottenere l'accesso alla parte o eliminare la parte dal database.")
   @RBComment("Error message is shown when user is trying build part from given document but user donot have access to part.")
   @RBArgComment0("Name of the source EPMDocuments used to build the target WTParts")
   public static final String CANNOT_BUILD_DONOT_HAVE_ACCESS_TO_PART = "42";

   @RBEntry("Impossibile creare la cronologia per l'origine nuova/sottoposta a Check-Out \"{0}\".")
   @RBComment("Error message is shown when user is trying to create derived rep history or build history for reverse build and build source is checkedout by user or build source is new in workspace.")
   @RBArgComment0("Name of the source EPMDocuments")
   public static final String CANNOT_CREATE_HISTORY_FOR_CHECKED_OUT_SOURCE = "43";

   @RBEntry("Le origini non verificate \"{0}\" non possono creare la destinazione associata.")
   @RBComment("Info message is shown when unverified doucments are skipped from build process.")
   @RBArgComment0("Name of the source EPMDocuments")
   public static final String UNVERIFIED_SOURCES = "44";

   @RBEntry("Impossibile creare destinazioni \"{0}\" da origini non verificate.")
   @RBComment("Info message is shown when targets to be built from unverified sources are skipped from build process.")
   @RBArgComment0("Name of the targets skipped")
   public static final String UNVERIFIED_SOURCES_FOR_TARGETS = "45";

   @RBEntry("L'utente non è proprietario della destinazione \"{0}\": impossibile creare localmente")
   @RBComment("Error message to show when user tries to build a target which is owned by other user.")
   @RBArgComment0("Name of the build target.")
   public static final String OWNABLE_WITH_ACCESS_CAN_NOT_BE_BUILT_LOCALLY = "46";

   @RBEntry("Impossibile creare regola di creazione fra \"{0}\" e \"{1}\" per \"{2}\" poiché \"{3}\" dispone già di una regola di creazione.")
   @RBComment("Error message to show when user tries to create link when a link already exists for build source or build target.")
   @RBArgComment0("Name of the source EPMDocument.")
   @RBArgComment1("Name of the target Part.")
   @RBArgComment2("Name of the model item.")
   @RBArgComment3("Name Part/Doc or ModelItem for which there is already existing rule.")
   public static final String CANNOT_CREATE_LINK_FOR_MODELITEM_IF_EXISTING_LINK = "47";

   @RBEntry("I documenti figlio \"{0}\" presentano più associazioni immagine con le parti \"{1}\" ")
   @RBComment("Error message to show user during build when child document mulitiple image associations and build is unable to select associated part.")
   @RBArgComment0("Name of the build source.")
   public static final String MULTIPLE_IMAGE_ASSOC_FOR_CHILD_DOC = "48";

   @RBEntry("I documenti figlio \"{0}\" presentano più associazioni immagine per gli elementi modello \"{1}\" con le parti \"{2}\"")
   @RBComment("Error message to show user during build when child document mulitiple image associations for model items in the document and build is unable to select associated part for model item.")
   @RBArgComment0("Name of the build source.")
   public static final String MULTIPLE_IMAGE_ASSOC_FOR_MODEL_ITEM = "49";

   @RBEntry("Impossibile creare la build. Esistono più associazioni per il documento figlio/elemento modello.")
   @RBComment("Error message to show user during build when child document mulitiple image associations.")
   public static final String MULTIPLE_IMAGE_ASSOC_CHILDS = "50";


   @RBEntry("Impossibile creare le scelte assegnate ai link utilizzo parte. Nella struttura del documento CAD sono presenti numerosi componenti del figlio \"{0}\" con diverse scelte di opzione assegnate ai figli.")
   @RBComment("Error message during build when child document is used more than once in document structure and all uses do not have same choices.")
   @RBArgComment0("List of Identity of child document used more than once in document structure and all uses do not have same choices.")
   public static final String INVALID_CHOICES_ON_MEMBER_LINK = "51";

   @RBEntry("Impossibile creare \"{0}\": gli oggetti sono sottoposti a modifica localmente. È possibile creare solo le versioni in stato di Check-In.")
   @RBComment("Error message to show when user tries to build private working versions of Build Sources.")
   @RBArgComment0("Names of the build sources.")
   public static final String CAN_NOT_BUILD_PRIVATE_CHECKED_OUT_COPY = "52";

   @RBEntry("Impossibile creare la cronologia per l'origine sottoposta a modifica localmente \"{0}\".")
   @RBComment("Error message is shown when user is trying to create derived rep history or build history for reverse build and build source is private working copy in workspace.")
   @RBArgComment0("Name of the source EPMDocuments")
   public static final String CANNOT_CREATE_HISTORY_FOR_PRIVATE_CHECKED_OUT_SOURCE = "53";
   
   @RBEntry("Impossibile creare regola di creazione fra \"{0}\" e \"{1}\". \"{0}\" dispone già di una regola di creazione associata a \"{2}\".")
   @RBComment("Error message to show when user tries to create link when a link already exists for build source or build target.")
   @RBArgComment0("Identity of the source EPMDocument.")
   @RBArgComment1("Identity of the target Part.")
   @RBArgComment2("Identity of the existing associated object.")
   public static final String CANNOT_CREATE_LINK_IF_EXISTING_LINK_BOTH_DOC = "54";

   @RBEntry("Impossibile creare regola di creazione fra \"{0}\" e \"{1}\" per \"{2}\". \"{0}\" dispone già di una regola di creazione associata a \"{3}\".")
   @RBComment("Error message to show when user tries to create link when a link already exists for build source or build target.")
   @RBArgComment0("Identity of the source/target.")
   @RBArgComment1("Identity of the target/source.")
   @RBArgComment2("Identity of the model item.")
   @RBArgComment3("Identity of associated Part/Doc of existing rule.")
   public static final String CANNOT_CREATE_LINK_FOR_MODELITEM_IF_EXISTING_LINK_DOC = "55";

   @RBEntry("Impossibile creare regola di creazione fra \"{0}\" e \"{1}\". \"{0}\" dispone già di una regola di creazione associata a \"{2}\" per \"{3}\".")
   @RBComment("Error message to show when user tries to create link when a link already exists for build source or build target.")
   @RBArgComment0("Identity of the source/target.")
   @RBArgComment1("Identity of the target/source.")
   @RBArgComment2("Identity of the existing associated object.")
   @RBArgComment3("Identity of model item of existing rule.")
   public static final String CANNOT_CREATE_LINK_FOR_DOC_IF_EXISTING_LINK_MI = "56";

   @RBEntry("Impossibile creare regola di creazione fra \"{0}\" e \"{1}\" per \"{2}\". \"{0}\" dispone già di una regola di creazione associata a \"{3}\" per \"{4}\".")
   @RBComment("Error message to show when user tries to create link when a link already exists for build source or build target.")
   @RBArgComment0("Identity of the source/target.")
   @RBArgComment1("Identity of the target/source.")
   @RBArgComment2("Identity of the model item.")
   @RBArgComment3("Identity of the existing associated object.")
   @RBArgComment4("Identity of model item of existing rule.")
   public static final String CANNOT_CREATE_LINK_IF_EXISTING_LINK_BOTH_MI = "57";
}
