/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.build;

import wt.util.resource.*;

@RBUUID("wt.build.buildResource")
public final class buildResource extends WTListResourceBundle {
   /**
    * $$NONE
    **/
   @RBEntry("The value for the argument \"{0}\" can not be null")
   public static final String NULL_ARGUMENT = "0";

   @RBEntry("Nothing to build.  No rules were found to build this \"{0}\"")
   public static final String NOTHING_TO_BUILD = "1";

   @RBEntry("Uniqueness violation:  the links built for \"{0}\" must have unique source identifications.  Contact your system administrator")
   public static final String SOURCE_IDENTIFICATION_MUST_BE_UNIQUE = "2";

   @RBEntry("Must provide an identifier for this link")
   public static final String MUST_PROVIDE_IDENTIFIER = "3";

   @RBEntry("Argument \"buildRule\" invalid:  \"{0}\" is not a type of \"{1}\"")
   public static final String TFS_DELEGATE_INVALID_CLASS = "4";

   @RBEntry("Can not update or delete this link because it is owned by EPM Build")
   public static final String BUILDABLE_LINK_INVALID_ACTION = "5";

   @RBEntry("The object \"{0}\" is not a valid source object for the link \"{1}\"")
   public static final String INVALID_SOURCE_CLASS = "6";

   @RBEntry("Can not build:  you do not have sufficient access")
   public static final String ACCESS_FAILURE = "7";

   @RBEntry("Can not build non-latest iterations")
   public static final String NON_LATEST_ITERATION = "8";

   @RBEntry("No TargetsForSourcesDelegate implemented for \"{0}\".  Contact your system administrator")
   public static final String TFS_DELEGATE_NOT_IMPLEMENTED = "9";

   @RBEntry("The \"{0}\" argument, a \"{1}\" object, is not \"{2}\" as expected")
   public static final String INVALID_DELEGATE_ARGUMENT_TYPE = "10";

   @RBEntry("\"{0}\" has rules associated to it.  It can not be deleted")
   public static final String CAN_NOT_DELETE = "11";

   @RBEntry("The target is in a baseline:  can not be built locally")
   public static final String BASELINEABLE_CAN_NOT_BE_BUILT_LOCALLY = "12";

   @RBEntry("The target is not in your personal cabinet:  it can not be built locally")
   public static final String CABINETBASED_CAN_NOT_BE_BUILT_LOCALLY = "13";

   @RBEntry("The target is not in locked by you:  it can not be built locally")
   public static final String LOCKABLE_CAN_NOT_BE_BUILT_LOCALLY = "14";

   @RBEntry("The target is not owned by you:  it can not be built locally")
   public static final String OWNABLE_CAN_NOT_BE_BUILT_LOCALLY = "15";

   @RBEntry("Can not build \"{0}\":  it is in someone else's personal cabinet")
   public static final String CAN_NOT_BUILD_CABINET_BASED = "16";

   @RBEntry("Can not build \"{0}\":  someone else has locked it")
   public static final String CAN_NOT_BUILD_LOCKABLE = "17";

   @RBEntry("Can not build \"{0}\":  someone else owns it")
   public static final String CAN_NOT_BUILD_OWNABLE = "18";

   @RBEntry("Can not build \"{0}\":  it is the checked out copy.  Only checked-in or working versions may be built")
   public static final String CAN_NOT_BUILD_WORKABLE = "19";

   @RBEntry("The BuildHistory association is completely managed by the build and can not be created, updated, or deleted by hand")
   public static final String CAN_NOT_CHANGE_HISTORY = "20";

   @RBEntry("Can not build \"{0}\":  it has \"{1}\" links to \"{2}\", build cannot subsume more than one link")
   public static final String TOO_MANY_LINKS_TO_SUBSUME = "21";

   @RBEntry("Can not change the name or delete this reference designator because it is owned by EPM Build")
   public static final String BUILDABLE_OCCURRENCE_INVALID_ACTION = "23";

   @RBEntry("Can not build \"{0}\":  it is checked out to project. \"{0}\" has build rules with \"{1}\"")
   @RBComment("Error message when user tries to build a target and target is checked out to  project.")
   @RBArgComment0("Name of the build target.")
   @RBArgComment1("Name of the build source.")
   public static final String CANNOT_BUILD_WORKABLE_CHECKEDOUT_TO_SANDBOX = "24";

   @RBEntry("Cannot create Build Rule between \"{0}\" and \"{1}\", since \"{0}\" has already existing Build Rule.")
   @RBComment("Error message to show when user tries to create link when a link already exists for build source or build target.")
   @RBArgComment0("Name of the source EPMDocument.")
   @RBArgComment1("Name of the target Part.")
   public static final String CANNOT_CREATE_LINK_IF_EXISTING_LINK = "25";

   @RBEntry("Can not build \"{0}\":  someone else owns it. \"{0}\" has build rules with \"{1}\"")
   @RBComment("Error message to show when user tries to build a target and target is owned by other user.")
   @RBArgComment0("Name of the build target.")
   @RBArgComment1("Name of the build source.")
   public static final String CAN_NOT_BUILD_OWNABLE_TARGET = "26";

   @RBEntry("Can not build \"{0}\":  it is the checked out copy.  Only checked-in or working versions may be built.  \"{0}\" has build rules with \"{1}\"")
   @RBComment("Error message to show when user tries to build a target and target is checked out copy.")
   @RBArgComment0("Name of the build target.")
   @RBArgComment1("Name of the build source.")
   public static final String CAN_NOT_BUILD_WORKABLE_TARGET = "27";

   @RBEntry("BuildSourceMaster must be a IBAHolder type object.")
   @RBComment("BuildSourceMaster must implement IBAHolder interface.")
   public static final String BUILD_SOURCE_MASTER_MUST_BE_IBAHOLDER = "28";

   @RBEntry("BuildSourceMaster must be a Master type object.")
   @RBComment("BuildSourceMaster must implement Master interface.")
   public static final String BUILD_SOURCE_MASTER_MUST_BE_MASTER = "29";

   @RBEntry("BuildTargetMaster must be a IBAHolder type object.")
   @RBComment("BuildTargetMaster must implement IBAHolder interface.")
   public static final String BUILD_TARGET_MASTER_MUST_BE_IBAHOLDER = "30";

   @RBEntry("BuildTargetMaster must be a Master type object.")
   @RBComment("BuildTargetMaster must implement Master interface.")
   public static final String BUILD_TARGET_MASTER_MUST_BE_MASTER = "31";

   @RBEntry("No BuildRule found for given BuildSourceMasters.")
   @RBComment("No BuildRule found for given BuildSourceMasters")
   public static final String NO_BUILD_RULE_FOR_BUILDSOURCEMASTER = "32";

   @RBEntry("No BuildRule found for given BuildTargetMasters.")
   @RBComment("No BuildRule found for given BuildTargetMasters")
   public static final String NO_BUILD_RULE_FOR_BUILDTARGETMASTER = "33";

   @RBEntry("Can not build \"{0}\" because they are checked out. Only checked-in versions may be built.")
   @RBComment("Error message to show when user tries to build checkout versions of Build Sources or Build Targets.")
   @RBArgComment0("Names of the build targets and sources.")
   public static final String CAN_NOT_BUILD_CHECKED_OUT_COPY = "34";

   @RBEntry("Can not build \"{0}\" because they are in personal folder.  Only objects in shared cabinets may be built.")
   @RBComment("Error message to show when user tries to build Build Sources or Build Targets in personal folder.")
   @RBArgComment0("Names of the build targets and sources.")
   public static final String CAN_NOT_BUILD_IN_PERSONAL_FOLDER = "35";

   @RBEntry("Can not build \"{0}\" because they are already built. Objects may only be built once.")
   @RBComment("Error message to show when user tries to build already built Build Sources or Build Targets.")
   @RBArgComment0("Names of the build targets and sources.")
   public static final String CAN_NOT_BUILD_ALREADY_BUILT = "36";

   @RBEntry("Can not remove or change usage of \"{0}\" in \"{1}\". Changes should be done in associated CAD Document.")
   @RBComment("Error displayed when user attempts to remove usage or modify quantity of component WTPart used in assembly WTPart. This message is displayed if assembly and component WTParts are actively associated with CADDocuments.")
   @RBArgComment0("Display identity of component WTPartMaster")
   @RBArgComment1("Display identity of assembly WTPart.")
   public static final String CANNOT_CHANGE_BUILDABLE_LINK = "37";

   @RBEntry("Can not remove or change \"{0}\" in \"{1}\". Changes should be done in associated CAD Document.")
   @RBComment("Error displayed when user attempts to remove part use occurrence in assembly WTPart. This message is displayed if assembly and component WTParts are actively associated with CADDocuments.")
   @RBArgComment0("Display identity of Part Uses Occurrence")
   @RBArgComment1("Display identity of assembly WTPart.")
   public static final String CANNOT_CHANGE_BUILDABLE_OCCURRENCE = "38";

   @RBEntry("Cannot create Build Rule for \"{0}\", since \"{0}\" has already existing Build Rule.")
   @RBComment("Error message to show when user tries to create link when a link already exists for build source or build target, and the user does not have a read permission.")
   @RBArgComment0("Name of the source EPMDocument.")
   public static final String CANNOT_CREATE_LINK_IF_EXISTING_LINK_NOPERMISSION = "39";

   @RBEntry("Cannot checkin unless modified parts/documents \"{0}\" are also checked in to update the associations.")
   @RBComment("Error message is shown when user is trying to checkin part/document but there is another associated part(with original copy of part/document) which is also required to be checked in.")
   @RBArgComment0("Name of the source EPMDocuments or target WTParts")
   public static final String CANNOT_CHECKIN_WITHOUT_ASSOCIATED_PART_DOC = "40";

   @RBEntry("Cannot undo-checkout unless all associations with parts/documents \"{0}\" are updated.")
   @RBComment("Error message is shown when user is trying to undo-checkout part/document but there is another associated part/document which is also required to be undo-checkedout.")
   @RBArgComment0("Name of the source EPMDocuments of target WTParts")
   public static final String CANNOT_UNDOCHECKOUT_WITHOUT_ASSOCIATED_PART_DOC = "41";

   @RBEntry("\"{0}\" : Action cannot be completed as it cannot build association with Part that you do not have access to. You must gain access to the Part or the Part must be deleted from the database.")
   @RBComment("Error message is shown when user is trying build part from given document but user donot have access to part.")
   @RBArgComment0("Name of the source EPMDocuments used to build the target WTParts")
   public static final String CANNOT_BUILD_DONOT_HAVE_ACCESS_TO_PART = "42";

   @RBEntry("Cannot create history for checkedout/new source \"{0}\".")
   @RBComment("Error message is shown when user is trying to create derived rep history or build history for reverse build and build source is checkedout by user or build source is new in workspace.")
   @RBArgComment0("Name of the source EPMDocuments")
   public static final String CANNOT_CREATE_HISTORY_FOR_CHECKED_OUT_SOURCE = "43";

   @RBEntry("Unverified sources \"{0}\" cannot build the associated target.")
   @RBComment("Info message is shown when unverified doucments are skipped from build process.")
   @RBArgComment0("Name of the source EPMDocuments")
   public static final String UNVERIFIED_SOURCES = "44";

   @RBEntry("Cannot build targets \"{0}\" from unverified sources.")
   @RBComment("Info message is shown when targets to be built from unverified sources are skipped from build process.")
   @RBArgComment0("Name of the targets skipped")
   public static final String UNVERIFIED_SOURCES_FOR_TARGETS = "45";

   @RBEntry("The target \"{0}\" is not owned by you:  it can not be built locally")
   @RBComment("Error message to show when user tries to build a target which is owned by other user.")
   @RBArgComment0("Name of the build target.")
   public static final String OWNABLE_WITH_ACCESS_CAN_NOT_BE_BUILT_LOCALLY = "46";

   @RBEntry("Cannot create Build Rule between \"{0}\" and \"{1}\" for \"{2}\", since \"{3}\" has already existing Build Rule.")
   @RBComment("Error message to show when user tries to create link when a link already exists for build source or build target.")
   @RBArgComment0("Name of the source EPMDocument.")
   @RBArgComment1("Name of the target Part.")
   @RBArgComment2("Name of the model item.")
   @RBArgComment3("Name Part/Doc or ModelItem for which there is already existing rule.")
   public static final String CANNOT_CREATE_LINK_FOR_MODELITEM_IF_EXISTING_LINK = "47";

   @RBEntry("The child documents \"{0}\" have multiple image type associations with parts \"{1}\" ")
   @RBComment("Error message to show user during build when child document mulitiple image associations and build is unable to select associated part.")
   @RBArgComment0("Name of the build source.")
   public static final String MULTIPLE_IMAGE_ASSOC_FOR_CHILD_DOC = "48";

   @RBEntry("The child documents \"{0}\" have multiple image type associations for model items \"{1}\" with parts \"{2}\"")
   @RBComment("Error message to show user during build when child document mulitiple image associations for model items in the document and build is unable to select associated part for model item.")
   @RBArgComment0("Name of the build source.")
   public static final String MULTIPLE_IMAGE_ASSOC_FOR_MODEL_ITEM = "49";

   @RBEntry("Unable to build. Multiple associations for child document/model item.")
   @RBComment("Error message to show user during build when child document mulitiple image associations.")
   public static final String MULTIPLE_IMAGE_ASSOC_CHILDS = "50";


   @RBEntry("Unable to build the assigned choices to the Part Usage Links. The CAD Document structure has many uses of this child \"{0}\" with different option choices assigned to the children.")
   @RBComment("Error message during build when child document is used more than once in document structure and all uses do not have same choices.")
   @RBArgComment0("List of Identity of child document used more than once in document structure and all uses do not have same choices.")
   public static final String INVALID_CHOICES_ON_MEMBER_LINK = "51";

   @RBEntry("Can not build \"{0}\" because they are locally modified objects. Only checked-in versions may be built.")
   @RBComment("Error message to show when user tries to build private working versions of Build Sources.")
   @RBArgComment0("Names of the build sources.")
   public static final String CAN_NOT_BUILD_PRIVATE_CHECKED_OUT_COPY = "52";

   @RBEntry("Cannot create history for locally modified source \"{0}\".")
   @RBComment("Error message is shown when user is trying to create derived rep history or build history for reverse build and build source is private working copy in workspace.")
   @RBArgComment0("Name of the source EPMDocuments")
   public static final String CANNOT_CREATE_HISTORY_FOR_PRIVATE_CHECKED_OUT_SOURCE = "53";
   
   @RBEntry("Cannot create Build Rule between \"{0}\" and \"{1}\", since \"{0}\" has already existing Build Rule to \"{2}\".")
   @RBComment("Error message to show when user tries to create link when a link already exists for build source or build target.")
   @RBArgComment0("Identity of the source EPMDocument.")
   @RBArgComment1("Identity of the target Part.")
   @RBArgComment2("Identity of the existing associated object.")
   public static final String CANNOT_CREATE_LINK_IF_EXISTING_LINK_BOTH_DOC = "54";

   @RBEntry("Cannot create Build Rule between \"{0}\" and \"{1}\" for \"{2}\", since \"{0}\" has already existing Build Rule to \"{3}\".")
   @RBComment("Error message to show when user tries to create link when a link already exists for build source or build target.")
   @RBArgComment0("Identity of the source/target.")
   @RBArgComment1("Identity of the target/source.")
   @RBArgComment2("Identity of the model item.")
   @RBArgComment3("Identity of associated Part/Doc of existing rule.")
   public static final String CANNOT_CREATE_LINK_FOR_MODELITEM_IF_EXISTING_LINK_DOC = "55";

   @RBEntry("Cannot create Build Rule between \"{0}\" and \"{1}\", since \"{0}\" has already existing Build Rule to \"{2}\" for \"{3}\".")
   @RBComment("Error message to show when user tries to create link when a link already exists for build source or build target.")
   @RBArgComment0("Identity of the source/target.")
   @RBArgComment1("Identity of the target/source.")
   @RBArgComment2("Identity of the existing associated object.")
   @RBArgComment3("Identity of model item of existing rule.")
   public static final String CANNOT_CREATE_LINK_FOR_DOC_IF_EXISTING_LINK_MI = "56";

   @RBEntry("Cannot create Build Rule between \"{0}\" and \"{1}\" for \"{2}\", since \"{0}\" has already existing Build Rule to \"{3}\" for \"{4}\".")
   @RBComment("Error message to show when user tries to create link when a link already exists for build source or build target.")
   @RBArgComment0("Identity of the source/target.")
   @RBArgComment1("Identity of the target/source.")
   @RBArgComment2("Identity of the model item.")
   @RBArgComment3("Identity of the existing associated object.")
   @RBArgComment4("Identity of model item of existing rule.")
   public static final String CANNOT_CREATE_LINK_IF_EXISTING_LINK_BOTH_MI = "57";
}
