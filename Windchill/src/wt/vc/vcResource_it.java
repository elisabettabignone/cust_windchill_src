/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.vc;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBArgComment1;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.vc.vcResource")
public final class vcResource_it extends WTListResourceBundle {
   @RBEntry("L'oggetto è nullo. Inizializzazione dell'oggetto non corretta.")
   public static final String IMPROPER_INITIALIZATION = "0";

   @RBEntry("La versione contiene un master provvisorio. È necessario che il master sia persistente prima di memorizzare la versione.")
   public static final String NONPERSISTENT_MASTER = "1";

   @RBEntry("La versione contiene un'iterazione provvisoria. È necessario che l'iterazione sia persistente prima di memorizzare la versione.")
   public static final String NONPERSISTENT_ITERATION = "2";

   @RBEntry("Impossibile memorizzare il master o l'iterazione indipendentemente dalla versione. È necessario che siano memorizzati tutti all'interno di una stessa transazione.")
   public static final String INVALID_DATABASE_INSERTION = "3";

   @RBEntry("Una transazione del database non è riuscita. Per ulteriori informazioni, vedere i seguenti errori.")
   public static final String FAILED_TRANSACTION = "4";

   @RBEntry("Impostazione di un attributo disapprovata. Per ulteriori informazioni, vedere i seguenti errori.")
   public static final String VETOED_SETTING = "5";

   @RBEntry("L'identificatore specificato deve essere maggiore dell'identificatore dell'iterazione specificata.")
   public static final String INVALID_IDENTIFIER = "6";

   @RBEntry("L'oggetto non ha alcun predecessore per la navigazione.")
   public static final String NO_PREDECESSOR = "7";

   @RBEntry("L'oggetto non ha alcun successore per la navigazione.")
   public static final String NO_SUCCESSOR = "8";

   @RBEntry("Impossibile eliminare l'oggetto. La versione dell'oggetto non è l'ultima.")
   public static final String INVALID_DATABASE_DELETION = "9";

   @RBEntry("Rilevati {0} oggetti ma soltanto 1 è pertinente")
   public static final String MANY_OBJECTS_FOUND = "10";

   @RBEntry("Impossibile rivedere o creare una diramazione di un oggetto diverso dall'ultima iterazione.")
   public static final String INVALID_BRANCH_ATTEMPT = "11";

   @RBEntry("L'oggetto può essere riportato a uno stadio precedente partendo dall'ultima iterazione di un ramo e arrivando a un'altra iterazione dello stesso ramo.")
   public static final String INVALID_ROLLBACK_ATTEMPT = "12";

   @RBEntry("L'oggetto può essere portato a uno stadio successivo partendo dalla prima iterazione di un ramo e arrivando a un'altra iterazione dello stesso ramo.")
   public static final String INVALID_ROLLUP_ATTEMPT = "13";

   @RBEntry("L'oggetto iterato allegato al ramo {0} non può essere persistente.")
   public static final String APPEND_ITERATION_MUST_BE_NONPERSISTENT = "14";

   @RBEntry("{0} è una versione variante. Impossibile creare una nuova revisione.")
   @RBArgComment0("The display identity of the one-off object")
   public static final String CANNOT_REVISE_ONEOFF = "15";

   @RBEntry("Se il presente metodo è in uso, eliminarlo dal codice. Non ha raggiunto l'obiettivo prefisso ed è stato sostituito da un vincolo gestito dal database.")
   public static final String IS_NEW_VERSION_CHECKED_OUT_DEPRECATED = "16";

   @RBEntry("Errore duranta l'impostazione degli attributi della nuova iterazione.")
   public static final String ATTRIBUTE_ERROR = "17";

   @RBEntry("Per l'id del ramo {0} è stata trovata più di una versione più recente")
   public static final String MULTIPLE_LATEST_FOR_BRANCH = "18";

   @RBEntry("Per l'id del ramo {0} non è stata trovata alcuna versione più recente")
   public static final String NO_LATEST_FOR_BRANCH = "19";

   @RBEntry("La destinazione non è configurata correttamente. Cookie iterazione mancante.")
   public static final String MISSING_ITERATION_COOKIE = "20";

   @RBEntry("Impostare ID ramo.")
   public static final String NO_BRANCH_ID = "21";

   @RBEntry("L'iterazione di destinazione non può essere resa persistente prima dell'inserimento.")
   public static final String TARGET_PERSISTED = "22";

   @RBEntry("Nessuna iterazione per il ramo: {0}")
   public static final String NO_ITERATIONS_FOR_BRANCH = "23";

   @RBEntry("L'iterazione esiste già.")
   public static final String EXISTING_ITERATION = "24";

   @RBEntry("Non è stata trovata alcuna distanza nella serie di iterazioni in cui effettuare l'inserimento per l'iterazione: {0}")
   public static final String MISSING_GAP = "25";

   @RBEntry("Nessuna ultima iterazione trovata per l'id del ramo: {0}")
   public static final String NO_LATEST_ITERATION_FOR_BRANCH = "26";

   @RBEntry("Un successore è necessario.")
   public static final String AT_LEAST_ONE_REQUIRED = "27";

   @RBEntry("L'iterazione target deve essere persistente: {0}")
   public static final String TARGET_NOT_PERSISTENT = "28";

   @RBEntry("L'oggetto versionabile discendente deve essere persistente: {0}")
   public static final String SUCCESSOR_NOT_PERSISTENT = "29";

   @RBEntry("L'oggetto di destinazione e quello versionabile devono essere in rami diversi")
   public static final String TARGET_AND_VERSIONABLE_SAME_BRANCH = "30";

   @RBEntry("Dati danneggiati: voce ramo di controllo mancante per versionabile")
   public static final String BAD_DATA_NO_CONTROL_BRANCH = "31";

   @RBEntry("Le versioni origine e destinazione dell'operazione di unione devono avere lo stesso master (master difformi: {0} e {1})")
   public static final String MERGE_BETWEEN_DIFFERENT_MASTERS = "32";

   @RBEntry("Le versioni di origine e destinazione devono differire perché l'unione sia possibile (versioni distinte: {0} e {1})")
   public static final String SAME_MERGE_SOURCE_AND_DESTINATION = "33";

   @RBEntry("La versione non è stata aggiornata")
   public static final String VERSION_NOT_UPDATED = "34";

   @RBEntry("Ultime iterazioni di cui si è tentata l'eliminazione: \"{0}\".")
   public static final String DELETION_OF_LATEST_ITERATIONS = "35";

   @RBEntry("Si sta tentando di eliminare un'iterazione dell'oggetto senza eliminare le iterazioni nelle revisioni successive.")
   public static final String DELETION_OF_BRANCH_POINT = "36";

   @RBEntry("Una chiamata deleteIterations deve essere effettuata a partire dall'iterazione con orario precedente verso una con orario successivo in una stessa diramazione. Non deve essere effettuata in modo da eliminare l'intera versione.")
   public static final String INVALID_DELETE_ITERATIONS_ATTEMPT = "37";

   @RBEntry("Errore di configurazione della destinazione. Cookie di versione mancante.")
   public static final String MISSING_VERSION_COOKIE = "38";

   @RBEntry("L'UFID impostato per la destinazione differisce dall'UFID di destinazione specificato come argomento dell'istruzione insertNode.")
   public static final String INCONSISTENT_UFID = "39";

   @RBEntry("Si sta tentando di eliminare iterazioni dell'oggetto senza eliminare le iterazioni nelle revisioni successive.")
   public static final String DELETION_OF_BRANCH_POINTS = "40";

   @RBEntry("L'iterazione \"{0}\" non è la più recente. Non è possibile eliminarla se non si selezionano per l'eliminazione anche tutte le iterazioni nelle revisioni successive.")
   public static final String DELETION_OF_BRANCH_POINT_ITERATION = "41";

   @RBEntry("Impossibile eseguire l'unione. L'iterazione di origine {0} non è la più recente.")
   public static final String CANNOT_MERGE_WITH_NON_LATEST_SOURCE_ITERATION = "42";

   @RBEntry("Impossibile eseguire l'unione. L'iterazione di destinazione {1} non è la più recente.")
   public static final String CANNOT_MERGE_WITH_NON_LATEST_DESTINATION_ITERATION = "43";

   @RBEntry("Impossibile spostare la versione {0} nel contesto di destinazione {1}. Schemi delle versioni non corrispondenti.")
   public static final String VERSION_SCHEME_MISMATCH = "44";

   @RBEntry("Seguono messaggi.")
   @RBArgComment0("Top level message.")
   public static final String INDIVIDUAL_CONFLICT = "46";

   @RBEntry("Impossibile eliminare l'oggetto. Non è in uno stato del ciclo di vita valido per l'eliminazione.")
   public static final String INVALID_DELETION_STATE = "47";

   @RBEntry("È possibile modificare il dominio solo per l'ultima iterazione di una versione. Iterazione non aggiornata: \"{0}\" Dominio di destinazione: \"{1}\"")
   @RBArgComment0("The display identity of the object that could not be assigned to a new administrative domain")
   @RBArgComment1("The display identity of the administrative domain the object could not be assigned to ")
   public static final String CANNOT_CHANGE_DOMAIN_OF_NON_LATEST_ITERATION = "48";

   @RBEntry("Impossibile eliminare l'oggetto {0}. È possibile eliminare solo la versione più recente di un oggetto.")
   @RBComment("Use this message if deleting an intermediate version and user does have access to the object.")
   public static final String CANNOT_DELETE_INTERMEDIATE_VERSIONS = "49";

   @RBEntry("Errore irreversibile: {0} non è l'iterazione più recente nella revisione corrente. È possibile creare nuove revisioni solo a partire dall'iterazione più recente di una revisione.")
   @RBArgComment0("The display identity of the non-latest iteration")
   public static final String CANNOT_REVISE_NON_LATEST_ITERATION = "50";

   @RBEntry("{0} è la copia di lavoro di un elemento sottoposto a Check-Out. Impossibile creare una nuova revisione.")
   @RBArgComment0("The display identity of the checked out item")
   public static final String CANNOT_REVISE_WORKING_COPY = "51";

   @RBEntry("{0} non è la versione più recente. Impossibile creare una nuova revisione.")
   @RBArgComment0("The display identity of the non-latest version")
   public static final String CANNOT_REVISE_NON_LATEST_VERSION = "52";

   @RBEntry("Uno o più elementi non sono la versione più recente. Impossibile creare una nuova revisione.")
   public static final String CANNOT_REVISE_NON_LATEST_VERSION_MULTI = "53";

   @RBEntry("Impossibile creare {0} dalla versione {1}.")
   @RBArgComment0("The display identity of the object")
   @RBArgComment1("The existing version from which the new version is being created")
   public static final String CANNOT_CREATE_VERSION_FROM_VERSION = "54";

   @RBEntry("Impossibile modificare l'identità di {0}. L'identità può essere modificata solo se l'elemento non è sottoposto a Check-Out e si dispone dei permessi {1} per tutte le versioni.")
   @RBArgComment0("The display identity of the object")
   @RBArgComment1("Permission needed to change the object's identity")
   public static final String IDENTITY_CHANGE_NOT_ALLOWED = "55";

   @RBEntry("Il livello di revisione non corrisponde a quello della versione vista: {0}")
   @RBArgComment0("The mismatched revision ID value")
   public static final String VIEW_VERSION_LEVEL_MISMATCH = "56";

   @RBEntry("La revisione dell'oggetto non corrisponde a quella del ramo: {0}")
   @RBArgComment0("The object display identity.")
   public static final String CONTROL_BRANCH_REVISION_MISMATCH = "57";

   @RBEntry("Il metodo può essere richiamato solo per oggetti ViewManageable.")
   public static final String VIEW_MANAGEABLE_METHOD_ERROR = "58";

   @RBEntry("Impossibile creare l'identificatore di versione per la serie: {0}")
   @RBArgComment0("The series name.")
   public static final String CANNOT_CREATE_VERSION_IDENTIFIER = "59";

   @RBEntry("Versione già esistente, non è possibile creare duplicati: {0}")
   @RBArgComment0("The object display identity.")
   public static final String VERSION_NOT_UNIQUE = "60";

   @RBEntry("Assegnazione vista non conforme. A una vista devono essere assegnate o tutte le iterazioni di un oggetto o nessuna: {0}")
   @RBArgComment0("The object display identity.")
   public static final String INCONSISTENT_VIEW_ASSIGNMENT = "61";

   @RBEntry("Impossibile assegnare la serie versione {0} a un oggetto che richiede la serie versione {1}")
   @RBArgComment0("The illegal version identifier series and value.")
   public static final String VERSION_SERIES_MISMATCH = "62";

   @RBEntry("Le versioni vista non sono supportate da questo metodo.")
   public static final String VIEW_VERSIONS_NOT_ALLOWED = "64";

   @RBEntry("Impossibile modificare la revisione di {0} nella revisione di destinazione {1}.")
   @RBArgComment0("The display identity of the object")
   @RBArgComment1("The target revision ID")
   public static final String CANNOT_CHANGE_REVISION = "65";

   @RBEntry("{0} è la copia originale di un elemento sottoposto a Check-Out e non ne può essere creata una nuova versione.")
   @RBArgComment0("The display identity of the checked out item")
   public static final String CANNOT_REVISE_ORIGINAL_COPY = "66";

   @RBEntry("Impossibile creare una nuova revisione di {0} in quanto la revisione successiva {1} è sottoposta a Check-Out.")
   @RBArgComment0("The first argument is the display identity of a non-latest revision that will be revised.  The second argument is the working copy of a successor revision.")
   public static final String CANNOT_REVISE_SUCCESSOR_CHECKED_OUT = "67";

   @RBEntry("Impossibile portare la revisione {0} al valore {1} in quanto esiste una revisione più recente per la vista.")
   @RBArgComment0("The first argument is the display identity of a non-latest revision that will be revised.  The second argument is the revision label being revised to.")
   public static final String CANNOT_REVISE_TO_GAP = "68";

   @RBEntry("Impossibile determinare la revisione più recente per gli oggetti non persistenti.")
   @RBComment("Used for error message when user tries to find the latest revision of an object that is not persistent.")
   public static final String NO_LATEST_FOR_NON_PERSISTENT = "69";

   @RBEntry("Impossibile creare una nuova revisione di {0} in quanto la revisione successiva {1} è sottoposta a Check-Out in un progetto.")
   @RBArgComment0("The first argument is the display identity of a non-latest revision that will be revised.  The second argument is the working copy of a successor revision.")
   public static final String CANNOT_REVISE_SUCCESSOR_SANDBOX_CHECKED_OUT = "70";

   @RBEntry("Livello di revisione diverso da 1: {0}")
   @RBArgComment0("The revision ID level is required to be 1 in the single-level revision label mode for view versions.")
   public static final String SINGLE_LEVEL_VIEW_VERSION_LEVEL_MISMATCH = "71";

   @RBEntry("Impossibile creare una nuova revisione di {0} perché è sottoposto a Check-Out in un progetto.")
   public static final String CANNOT_REVISE_SANDBOX_CHECKED_OUT = "72";

   @RBEntry("Impossibile creare una nuova revisione di {0} in quanto un'iterazione non aggiornata della revisione è sottoposta a Check-Out.")
   @RBArgComment0("The target object for revision.")
   public static final String CANNOT_REVISE_NONLATEST_ITERATION_CHECKED_OUT = "73";

   /**
    * System level non-latest revise setting
    **/
   @RBEntry("Consenti revisione di iterazioni non aggiornate")
   public static final String PRIVATE_CONSTANT_0 = "ENABLE_NON_LATEST_REVISE";

   @RBEntry("Se impostato su \"Sì\", è consentita la revisione delle iterazioni non aggiornate per i tipi di oggetto validi.")
   public static final String PRIVATE_CONSTANT_1 = "ENABLE_NON_LATEST_REVISE_DESC";
   
   @RBEntry("Impossibile creare una nuova iterazione o revisione dell'oggetto \\'{0}\\', in quanto bloccato a livello amministrativo")
   @RBArgComment0("The object display identity.")
   public static final String ITERATE_NOT_ALLOWED_FOR_ADMIN_LOCKED_OBJECT = "ITERATE_NOT_ALLOWED_FOR_ADMIN_LOCKED_OBJECT";
   
   @RBEntry("Argomento non valido. I nodi da inserire devono essere di tipo 'Iterato'")
   public static final String INSERT_NODES_ILLEGAL_ARGUMENT_NOT_ITERATED = "INSERT_NODES_ILLEGAL_ARGUMENT_NOT_ITERATED";
   
   @RBEntry("Argomento non valido. I nodi da inserire devono essere di tipo non persistente")
   public static final String INSERT_NODES_ILLEGAL_ARGUMENT_ALREADY_PERSISTED_OBJECT = "INSERT_NODES_ILLEGAL_ARGUMENT_ALREADY_PERSISTED_OBJECT";
   
   @RBEntry("Argomento non valido. È necessario specificare un master per i nodi da inserire")
   public static final String INSERT_NODES_ILLEGAL_ARGUMENT_NO_MASTER = "INSERT_NODES_ILLEGAL_ARGUMENT_NO_MASTER";
   
   @RBEntry("Argomento non valido. È necessario specificare un identificatore iterazione per i nodi da inserire")
   public static final String INSERT_NODES_ILLEGAL_ARGUMENT_MISSING_ITERATION_COOKIE = "INSERT_NODES_ILLEGAL_ARGUMENT_MISSING_ITERATION_COOKIE";
   
   @RBEntry("Argomento non valido. È necessario specificare un identificatore versione per i nodi da inserire")
   public static final String INSERT_NODES_ILLEGAL_ARGUMENT_MISSING_VERSION_COOKIE = "INSERT_NODES_ILLEGAL_ARGUMENT_MISSING_VERSION_COOKIE";
   
   @RBEntry("Impossibile inserire {0}. Ramo controllo non individuato")
   public static final String INSERT_NODES_CAN_NOT_LOCATE_CONTROL_BRANCH = "INSERT_NODES_CAN_NOT_LOCATE_CONTROL_BRANCH";
   
}
