/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.vc.wip;

import wt.util.resource.*;

@RBUUID("wt.vc.wip.wipResource")
public final class wipResource_it extends WTListResourceBundle {
   @RBEntry("L'oggetto è nullo. Inizializzazione dell'oggetto non corretta.")
   public static final String IMPROPER_INITIALIZATION = "0";

   @RBEntry("L'oggetto risulta già sottoposto a Check-Out. Impossibile sottoporlo a Check-Out più di una volta.")
   public static final String ALREADY_CHECKED_OUT = "1";

   @RBEntry("Impossibile sottoporre a Check-Out un oggetto diverso dall'ultima iterazione")
   public static final String INVALID_CHECKOUT_ATTEMPT = "10";

   @RBEntry("{0} non può essere sottoposto a Check-Out perché non si dispone dei permessi per modificarlo.")
   public static final String NO_MODIFY_ACCESS_ON_CHECKOUT = "14";

   @RBEntry("{0} non può essere sottoposto a Check-In perché non si dispone dei permessi per modificare la copia originale.\" +\"È necessario annullare il Check-Out.")
   public static final String NO_MODIFY_ACCESS_ON_CHECKIN = "15";

   @RBEntry("Errore durante l'eliminazione del link di Check-Out per {0} durante il Check-In.")
   public static final String ERR_CLEANUP_CHECKOUT_LINK = "19";

   @RBEntry("ATTENZIONE: l'elemento deve essere già sottoposto a Check-Out perché sia possibile sottoporlo a Check-In.")
   public static final String NOT_CHECKED_OUT = "2";

   @RBEntry("{0} non ha una versione originale. Probabilmente è già stato sottoposto a Check-In.")
   public static final String NO_ORIGINAL_COPY = "3";

   @RBEntry("Non esiste una copia in modifica di {0}. Per ottenere una copia in modifica è necessario sottoporre l'oggetto a Check-Out.")
   public static final String NO_WORKING_COPY = "4";

   @RBEntry("{0} è la versione originale di una copia in modifica. Impossibile modificarlo.")
   public static final String INVALID_DATABASE_MODIFICATION = "5";

   @RBEntry("Impossibile individuare lo schedario personale dell'utente: \"{0}\".")
   public static final String NO_PERSONAL_CABINET = "6";

   @RBEntry("Impossibile individuare la cartella Check-Out dell'utente.")
   public static final String NO_CHECKOUT_FOLDER = "7";

   @RBEntry("{0} non può essere eliminato perché è sottoposto a Check-Out. Annullare il Check-Out prima di eliminarlo.")
   public static final String INVALID_DATABASE_DELETION = "8";

   @RBEntry("{0} è una copia in modifica. Impossibile crearne una diramazione, una nuova revisione o spostarlo in un'altra cartella.")
   public static final String INVALID_WORKING_COPY_USAGE = "9";

   @RBEntry("Impossibile eseguire l'operazione in una transazione annidata.")
   public static final String NESTED_TRANSACTION = "16";

   @RBEntry("La classe {0} non può essere usata in transazioni multi-thread")
   public static final String UNSUPPORTED_CLASS = "17";

   @RBEntry("Impossibile sottoporre {0} a Check-Out perché è stato inviato a PDM.")
   public static final String CANNOT_CHECKOUT_TERMINAL_OBJECTS = "18";

   @RBEntry("Impossibile sottoporre {0} a Check-Out perché è stato abbandonato.")
   public static final String CANNOT_CHECKOUT_ABANDONED_OBJECTS = "CANNOT_CHECKOUT_ABANDONED_OBJECTS";

   @RBEntry("L'oggetto è stato sottoposto a Check-Out e non può essere spostato.")
   public static final String INVALID_WORKING_COPY_MOVE = "21";

   @RBEntry("Gli oggetti sono stati sottoposti a Check-Out e non possono essere spostati.")
   public static final String INVALID_WORKING_COPIES_MOVE = "22";

   @RBEntry("Impossibile spostare l'oggetto {0} poiché l'utente {1} lo ha sottoposto a Check-Out.")
   public static final String WORKING_COPY_MOVE_NOT_ALLOWED = "23";

   @RBEntry("Operazione di annullamento Check-Out non riuscita per {0}: l'utente non dispone dei permessi necessari sulla copia in Check-Out o sulla relativa cartella.")
   public static final String NOT_AUTH_FOR_UNDO_CHECKOUT = "24";

   @RBEntry("Seguono messaggi.")
   @RBArgComment0("Top level message.")
   public static final String INDIVIDUAL_CONFLICT = "25";

   @RBEntry("Gli oggetti che seguono sono stati sottoposti a Check-Out e non possono essere eliminati. Selezionare Annulla Check-Out per continuare. Il sistema annullerà il Check-Out per consentire l'eliminazione degli oggetti: {0}")
   public static final String CHECKED_OUT_CONFLICTS = "26";

   @RBEntry("Durante il tentativo di Check-Out si sono verificati gli errori che seguono::")
   public static final String MULTI_OBJECT_PRE_CHECKOUT_VALIDATE_MESSAGE = "30";

   @RBEntry("Impossibile sottoporre {0} a Check-Out. Non è l'iterazione più recente.")
   @RBArgComment0("Identity of invalid element.")
   public static final String INVALID_CHECKOUT_ATTEMPT_NON_LATEST_ITERATION = "31";

   @RBEntry("L'oggetto {0} risulta già sottoposto a Check-Out. Impossibile sottoporlo a Check-Out più di una volta.")
   @RBArgComment0("Identity of invalid element.")
   public static final String INVALID_CHECKOUT_ATTEMPT_ALREADY_CHECKED_OUT = "32";

   @RBEntry("Durante il tentativo di Check-In si sono verificati gli errori che seguono:")
   public static final String MULTI_OBJECT_PRE_CHECKIN_VALIDATE_MESSAGE = "35";

   @RBEntry("ATTENZIONE: l'elemento {0} deve essere già sottoposto a Check-Out perché sia possibile sottoporlo a Check-In.")
   @RBArgComment0("Identity of invalid element.")
   public static final String INVALID_CHECKIN_ATTEMPT_NOT_CHECKED_OUT = "36";

   @RBEntry("Impossibile ottenere cartelle di lavoro per la raccolta. Se un oggetto in modifica è memorizzato in cartella, è necessario che lo siano tutti gli oggetti in modifica.")
   public static final String INVALID_COLLECTION_NON_FOLDERED_OBJECTS = "40";

   @RBEntry("Impossibile ottenere la cartella di lavoro per la raccolta.")
   public static final String UNABLE_TO_DETERMINE_WORKING_FOLDERS = "41";

   @RBEntry("Gli oggetti in modifica si trovano in cartelle di lavoro differenti. È necessario che tutti gli oggetti in modifica memorizzati in cartelle provengano dalla stessa cartella di lavoro.")
   public static final String INVALID_COLLECTION_DIFFERENT_WORKING_FOLDERS = "42";

   @RBEntry("Durante il tentativo di eliminazione si sono verificati gli errori che seguono:")
   public static final String MULTI_OBJECT_PRE_DELETE_VALIDATE_MESSAGE = "44";

   @RBEntry("{0} non può essere eliminato perché è sottoposto a Check-Out in {1}. Annullare il Check-Out prima di eliminarlo.")
   public static final String INVALID_SANDBOX_DATABASE_DELETION = "45";

   @RBEntry("{0} non può essere eliminato perché è sottoposto a Check-Out da {1}. Annullare il Check-Out prima di eliminarlo.")
   public static final String INVALID_DATABASE_DELETION_CHECKED_OUT = "46";

   @RBEntry("Impossibile modificare {0} perché non è stato sottoposto a Check-Out.")
   public static final String INVALID_MODIFICATION = "47";

   @RBEntry("Check-Out non riuscito. È possibile che l'oggetto sia già sottoposto a Check-Out. Aggiornare l'oggetto e verificarne lo stato.")
   public static final String MIGHT_ALREADY_BE_CHECKED_OUT = "48";

   /**
    * System level non-latest iterations checkout setting
    **/
   @RBEntry("Consenti il Check-Out delle iterazioni non aggiornate")
   public static final String PRIVATE_CONSTANT_0 = "ENABLE_NON_LATEST_CHECKOUT";

   @RBEntry("Esistono 3 opzioni possibili: non consentire il Check-Out delle iterazioni non aggiornate; consentire il Check-Out delle iterazioni non aggiornate per i tipi di oggetto validi, quali i documenti CAD e relativi discendenti; restituire un conflitto ignorabile se l'utente cerca di sottoporre a Check-Out un'iterazione non aggiornata.")
   public static final String PRIVATE_CONSTANT_1 = "ENABLE_NON_LATEST_CHECKOUT_DESC";

   @RBEntry("Consenti il Check-Out delle iterazioni non aggiornate")
   public static final String NON_LATEST_CHECKOUT_TRUE = "NON_LATEST_CHECKOUT_TRUE";

   @RBEntry("Non consentire il Check-Out delle iterazioni non aggiornate")
   public static final String NON_LATEST_CHECKOUT_FALSE = "NON_LATEST_CHECKOUT_FALSE";

   @RBEntry("Restituisci un conflitto ignorabile se l'utente cerca di sottoporre a Check-Out un'iterazione non aggiornata ")
   public static final String NON_LATEST_CHECKOUT_CONFLICT = "NON_LATEST_CHECKOUT_CONFLICT";

   @RBEntry("Operazione non riuscita. L'utente non dispone dei permessi necessari ad eseguire l'azione per gli oggetti che seguono: {0}")
   public static final String MULTI_OBJECT_ACCESS_ERROR = "49";

   @RBEntry("Stato di Check-Out specificato non supportato ({0}). Gli stati di Check-Out supportati sono {1} e {2}")
   public static final String UNSUPPORTED_CHECKOUT_WIP_STATE = "60";

   @RBEntry("Tutti gli oggetti sottoposti a Check-Out devono essere nella stessa cartella")
   public static final String ALL_OBJECT_MUST_BE_IN_SAME_FOLDER = "61";

   @RBEntry("Lo stato di Check-Out di {0} è \"{1}\". Impossibile convertirne lo stato di Check-Out in \"{2}\".")
   public static final String INVALID_WIP_STATE_FOR_CONVERT_CHECKOUT = "62";

   @RBEntry("Check-Out privato non supportato per gli oggetti di tipo {0}")
   public static final String PRIVATE_CHECKOUT_NOT_SUPPORTED = "64";
}
