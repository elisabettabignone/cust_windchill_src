/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.vc.wip;

import wt.util.resource.*;

@RBUUID("wt.vc.wip.wipResource")
public final class wipResource extends WTListResourceBundle {
   @RBEntry("The object was improperly initialized (i.e., it was null).")
   public static final String IMPROPER_INITIALIZATION = "0";

   @RBEntry("The object has already been checked out and cannot be checked out more than once.")
   public static final String ALREADY_CHECKED_OUT = "1";

   @RBEntry("An object other than the latest iteration cannot be checked out")
   public static final String INVALID_CHECKOUT_ATTEMPT = "10";

   @RBEntry("{0} cannot be checked out because you do not have permission to modify it.")
   public static final String NO_MODIFY_ACCESS_ON_CHECKOUT = "14";

   @RBEntry("{0} cannot be checked in because you do not have permission to modify the original copy.  You must undo the checkout.")
   public static final String NO_MODIFY_ACCESS_ON_CHECKIN = "15";

   @RBEntry("Error while deleting CheckoutLink record for {0} during checkin.")
   public static final String ERR_CLEANUP_CHECKOUT_LINK = "19";

   @RBEntry("ATTENTION: Item must be checked out before checking it in.")
   public static final String NOT_CHECKED_OUT = "2";

   @RBEntry("{0} has no original version. More than likely it has already been checked in.")
   public static final String NO_ORIGINAL_COPY = "3";

   @RBEntry("{0} has no working copy. It must be checked out for a working copy to exist.")
   public static final String NO_WORKING_COPY = "4";

   @RBEntry("{0} is the original version of a working copy and cannot be modified.")
   public static final String INVALID_DATABASE_MODIFICATION = "5";

   @RBEntry("Could not locate Personal Cabinet for user: \"{0}\".")
   public static final String NO_PERSONAL_CABINET = "6";

   @RBEntry("Could not locate user's 'Checked Out' folder.")
   public static final String NO_CHECKOUT_FOLDER = "7";

   @RBEntry("{0} cannot be deleted because it is checked out.  Undo the checkout before deleting it.")
   public static final String INVALID_DATABASE_DELETION = "8";

   @RBEntry("{0} is a working copy and cannot be branched, revised, or moved to another folder.")
   public static final String INVALID_WORKING_COPY_USAGE = "9";

   @RBEntry("Cannot perform operation in a nested transaction.")
   public static final String NESTED_TRANSACTION = "16";

   @RBEntry("{0} class cannot be used in multi-threaded transaction")
   public static final String UNSUPPORTED_CLASS = "17";

   @RBEntry("{0} cannot be checked out as it is sent to PDM.")
   public static final String CANNOT_CHECKOUT_TERMINAL_OBJECTS = "18";

   @RBEntry("{0} cannot be checked out as it is abandoned.")
   public static final String CANNOT_CHECKOUT_ABANDONED_OBJECTS = "CANNOT_CHECKOUT_ABANDONED_OBJECTS";

   @RBEntry("The object is checked out and cannot be moved.")
   public static final String INVALID_WORKING_COPY_MOVE = "21";

   @RBEntry("Objects are checked out and cannot be moved.")
   public static final String INVALID_WORKING_COPIES_MOVE = "22";

   @RBEntry("Unable to move object {0} because user {1} has it checked out.")
   public static final String WORKING_COPY_MOVE_NOT_ALLOWED = "23";

   @RBEntry("Undo checkout operation failed for {0} because user does not have necessary permission on checkedout copy or its folder.")
   public static final String NOT_AUTH_FOR_UNDO_CHECKOUT = "24";

   @RBEntry("Messages follow.")
   @RBArgComment0("Top level message.")
   public static final String INDIVIDUAL_CONFLICT = "25";

   @RBEntry("The following objects cannot be deleted as they are checked out. To continue, select Undo Checkout.  The system will undo the check outs so that the objects can be deleted: {0}")
   public static final String CHECKED_OUT_CONFLICTS = "26";

   @RBEntry("The following errors occurred while attempting to checkout:")
   public static final String MULTI_OBJECT_PRE_CHECKOUT_VALIDATE_MESSAGE = "30";

   @RBEntry("{0} can not be checked out because it is not the latest iteration.")
   @RBArgComment0("Identity of invalid element.")
   public static final String INVALID_CHECKOUT_ATTEMPT_NON_LATEST_ITERATION = "31";

   @RBEntry("{0} has already been checked out and cannot be checked out more than once.")
   @RBArgComment0("Identity of invalid element.")
   public static final String INVALID_CHECKOUT_ATTEMPT_ALREADY_CHECKED_OUT = "32";

   @RBEntry("The following errors occurred while attempting to checkin:")
   public static final String MULTI_OBJECT_PRE_CHECKIN_VALIDATE_MESSAGE = "35";

   @RBEntry("ATTENTION: Item {0} must be checked out before checking it in.")
   @RBArgComment0("Identity of invalid element.")
   public static final String INVALID_CHECKIN_ATTEMPT_NOT_CHECKED_OUT = "36";

   @RBEntry("Unable to get working folders for collection, if one workable is Foldered, all workables must be Foldered.")
   public static final String INVALID_COLLECTION_NON_FOLDERED_OBJECTS = "40";

   @RBEntry("Unable to get the working Folder for collection.")
   public static final String UNABLE_TO_DETERMINE_WORKING_FOLDERS = "41";

   @RBEntry("Workables in different working folders. All Foldered workables must be from the same working folder.")
   public static final String INVALID_COLLECTION_DIFFERENT_WORKING_FOLDERS = "42";

   @RBEntry("The following errors occurred while attempting to delete:")
   public static final String MULTI_OBJECT_PRE_DELETE_VALIDATE_MESSAGE = "44";

   @RBEntry("{0} cannot be deleted because it is checked out to {1}.  Undo the checkout before deleting it.")
   public static final String INVALID_SANDBOX_DATABASE_DELETION = "45";

   @RBEntry("{0} cannot be deleted because it is checked out by {1}.  Undo the checkout before deleting it.")
   public static final String INVALID_DATABASE_DELETION_CHECKED_OUT = "46";

   @RBEntry("{0} is not checked out and cannot be modified.")
   public static final String INVALID_MODIFICATION = "47";

   @RBEntry("Checkout failed.  The object might already be checked out.  Refresh the object and check its status.")
   public static final String MIGHT_ALREADY_BE_CHECKED_OUT = "48";

   /**
    * System level non-latest iterations checkout setting
    **/
   @RBEntry("Allow checkout of non-latest iterations")
   public static final String PRIVATE_CONSTANT_0 = "ENABLE_NON_LATEST_CHECKOUT";

   @RBEntry("There are three options possible.  Do not allow the checkout of non-latest iterations.  Allow the checkout of non-latest iterations for valid object types like CAD documents and their descendants. Present an overrideable conflict when attempting to checkout a non-latest iteration.")
   public static final String PRIVATE_CONSTANT_1 = "ENABLE_NON_LATEST_CHECKOUT_DESC";

   @RBEntry("Allow checkout of non-latest iterations")
   public static final String NON_LATEST_CHECKOUT_TRUE = "NON_LATEST_CHECKOUT_TRUE";

   @RBEntry("Do not allow checkout of non-latest iterations")
   public static final String NON_LATEST_CHECKOUT_FALSE = "NON_LATEST_CHECKOUT_FALSE";

   @RBEntry("Throw an overrideable conflict if a user tries to checkout a non-latest iteration ")
   public static final String NON_LATEST_CHECKOUT_CONFLICT = "NON_LATEST_CHECKOUT_CONFLICT";

   @RBEntry("Operation failed because the user did not have the necessary permission on the following objects to perform the operation: {0}")
   public static final String MULTI_OBJECT_ACCESS_ERROR = "49";

   @RBEntry("Unsupported checkout state supplied ({0}).  The supported checkout states are {1} or {2}")
   public static final String UNSUPPORTED_CHECKOUT_WIP_STATE = "60";

   @RBEntry("All objects being checked out must be in the same folder")
   public static final String ALL_OBJECT_MUST_BE_IN_SAME_FOLDER = "61";

   @RBEntry("{0} has checkout state \"{1}\" and so cannot be converted to checkout state \"{2}\".")
   public static final String INVALID_WIP_STATE_FOR_CONVERT_CHECKOUT = "62";

   @RBEntry("Private checkout is not supported for objects of type {0}")
   public static final String PRIVATE_CHECKOUT_NOT_SUPPORTED = "64";
}
