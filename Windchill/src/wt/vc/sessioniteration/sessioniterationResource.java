/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.vc.sessioniteration;

import wt.util.resource.*;

@RBUUID("wt.vc.sessioniteration.sessioniterationResource")
public final class sessioniterationResource extends WTListResourceBundle {
   @RBEntry("You can not create a session iteration for an object that has not been persisted.")
   public static final String NO_CREATE_NOT_PERSISTENT = "0";

   @RBEntry("An object other than the latest iteration cannot be updated.")
   public static final String NO_CREATE_NOT_LATEST = "1";

   @RBEntry("You can not create a session iteration from another.")
   public static final String NO_CREATE_ALREADY_SESSION_ITERATION = "2";

   @RBEntry("You do not have permission to modify {0}.")
   @RBArgComment0("The identity of the object the user can't modify.")
   public static final String NO_CREATE_USER_CANNOT_MODIFY = "3";

   @RBEntry("You are already editing {0}.  Either resume editing it, or delete the object you are editing.")
   @RBArgComment0("The identity of the object for which the user wishes to create an editing session object.")
   public static final String NO_CREATE_USER_HAS_SESSION_ITERATION = "4";

   @RBEntry("You can not commit a session iteration that has not been persisted.")
   public static final String NO_COMMIT_NOT_PERSISTENT = "5";

   @RBEntry("You can not save the changes made to {0} because it has been superseded by other changes.")
   @RBArgComment0("The identity of the stale object.")
   public static final String NO_COMMIT_NOT_LATEST = "6";

   @RBEntry("You can not commit an iteration that is not a session iteration.")
   public static final String NO_COMMIT_NOT_SESSION_ITERATION = "7";

   @RBEntry("You can not commit this session iteration because you do not own it.")
   public static final String NO_COMMIT_NOT_OWNER = "8";

   @RBEntry("You can not delete a session iteration that has not been persisted.")
   public static final String NO_DELETE_NOT_PERSISTENT = "9";

   @RBEntry("Use normal delete when iteration is not a session iteration.")
   public static final String NO_DELETE_NOT_SESSION_ITERATION = "10";

   @RBEntry("You can not delete this session iteration because you do not own it.")
   public static final String NO_DELETE_NOT_OWNER = "11";

   @RBEntry("The first iteration of a master can not be created as a session iteration.")
   public static final String NO_CREATE_NO_PREDECESSOR = "12";

   @RBEntry("You can not create a new version as a session iteration.  You must first persist the new version, then create a session iteration from it.")
   public static final String CAN_NOT_ASSIGN_SESSION_TO_NEW_VERSION = "13";

   @RBEntry("You can not create a session iteration from a checked out iteration.")
   public static final String CAN_NOT_CREATE_SESSION_ITERATION_FROM_CHECKED_OUT = "14";

   @RBEntry("You can not check out a session iteration.")
   public static final String CAN_NOT_CHECK_OUT_SESSION_ITERATION = "15";

   @RBEntry("The object is not a session iteration.  You can not modify it.")
   public static final String CAN_NOT_MODIFY_ITERATION = "16";

   @RBEntry("The object you are attempting to update is checked out.  You can not modify it.")
   public static final String NO_COMMIT_CHECKED_OUT = "17";

   @RBEntry("You do not have permission to modify {0}.")
   @RBArgComment0("The identity of the object the user can't modify.")
   public static final String NO_COMMIT_USER_CANNOT_MODIFY = "18";
}
