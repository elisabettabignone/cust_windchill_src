/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.vc.sessioniteration;

import wt.util.resource.*;

@RBUUID("wt.vc.sessioniteration.sessioniterationResource")
public final class sessioniterationResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile creare un'iterazione di sessione per un oggetto non reso persistente.")
   public static final String NO_CREATE_NOT_PERSISTENT = "0";

   @RBEntry("Impossibile aggiornare un oggetto diverso dall'ultima iterazione")
   public static final String NO_CREATE_NOT_LATEST = "1";

   @RBEntry("Impossibile creare un'iterazione di sessione da un'altra")
   public static final String NO_CREATE_ALREADY_SESSION_ITERATION = "2";

   @RBEntry("Non si dispone dei permessi per modificare {0}.")
   @RBArgComment0("The identity of the object the user can't modify.")
   public static final String NO_CREATE_USER_CANNOT_MODIFY = "3";

   @RBEntry("L'utente sta già modificando {0}.  Ripendere la modifica o eliminare l'oggetto che si sta modificando.")
   @RBArgComment0("The identity of the object for which the user wishes to create an editing session object.")
   public static final String NO_CREATE_USER_HAS_SESSION_ITERATION = "4";

   @RBEntry("Impossibile effettuare il commit di un'iterazione di sessione non resa persistente.")
   public static final String NO_COMMIT_NOT_PERSISTENT = "5";

   @RBEntry("Impossibile effettuare le modifiche apportate a {0} perché sono sopravvenute altre modifiche.")
   @RBArgComment0("The identity of the stale object.")
   public static final String NO_COMMIT_NOT_LATEST = "6";

   @RBEntry("Impossibile effettuare il commit di un'iterazione che non è un'iterazione di sessione.")
   public static final String NO_COMMIT_NOT_SESSION_ITERATION = "7";

   @RBEntry("L'utente non può effettuare il commit di un'iterazione della sessione peché non ne è il responsabile.")
   public static final String NO_COMMIT_NOT_OWNER = "8";

   @RBEntry("Impossibile eliminare un'iterazione di sessione non resa persistente.")
   public static final String NO_DELETE_NOT_PERSISTENT = "9";

   @RBEntry("Usa la normale funzione di eliminazione quando l'iterazione non è un'iterazione di sessione.")
   public static final String NO_DELETE_NOT_SESSION_ITERATION = "10";

   @RBEntry("L'utente non può eliminare l'iterazione della sessione peché non ne è il responsabile.")
   public static final String NO_DELETE_NOT_OWNER = "11";

   @RBEntry("La prima iterazione di un master non può essere creata come iterazione di sessione.")
   public static final String NO_CREATE_NO_PREDECESSOR = "12";

   @RBEntry("Impossibile creare una nuova versione come iterazione di sessione.  È innanzitutto necessario rendere persistente la nuova versione e quindi da essa creare un'iterazione di sessione.")
   public static final String CAN_NOT_ASSIGN_SESSION_TO_NEW_VERSION = "13";

   @RBEntry("Impossibile creare un'iterazione di sessione con un'iterazione sottoposta a Check-Out.")
   public static final String CAN_NOT_CREATE_SESSION_ITERATION_FROM_CHECKED_OUT = "14";

   @RBEntry("Impossibile effettuare il Check-Out di un'iterazione di sessione.")
   public static final String CAN_NOT_CHECK_OUT_SESSION_ITERATION = "15";

   @RBEntry("L'oggetto non è un'iterazione di sessione. È possibile modificarlo.")
   public static final String CAN_NOT_MODIFY_ITERATION = "16";

   @RBEntry("L'oggetto che si sta cercando di aggiornare è sottoposto a Check-Out. Impossibile modificarlo.")
   public static final String NO_COMMIT_CHECKED_OUT = "17";

   @RBEntry("Non si dispone dei permessi per modificare {0}.")
   @RBArgComment0("The identity of the object the user can't modify.")
   public static final String NO_COMMIT_USER_CANNOT_MODIFY = "18";
}
