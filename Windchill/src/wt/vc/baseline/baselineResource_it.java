/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.vc.baseline;

import wt.util.resource.*;

@RBUUID("wt.vc.baseline.baselineResource")
public final class baselineResource_it extends WTListResourceBundle {
   /**
    * Exceptions
    **/
   @RBEntry("Impossibile eliminare gli elementi che seguono, poiché sono inclusi in una o più baseline inaccessibili all'utente: \"{0}\"")
   public static final String DELETE_ITEM = "0";

   @RBEntry("Un altro utente sta modificando la baseline \"{0}\".  Impossibile completare l'operazione.")
   public static final String BASELINE_LOCKED = "1";

   @RBEntry("Questo elemento non può essere creato in uno schedario personale.")
   public static final String NOT_IN_SHARE_CABINET = "2";

   @RBEntry("Impossibile modificare l'elemento \"{0} ({1})\" poiché è contenuto in una o più baseline.")
   public static final String MODIFY_ITEM = "3";

   /**
    * Messages
    **/
   @RBEntry("Aggiunta di \"{0} ({1})\"")
   public static final String ADD_ITEM = "4";

   @RBEntry("Sostituzione di \"{0} ({1})\" con \"{2} ({3})\"")
   public static final String REPLACE_ITEM = "5";

   @RBEntry("Rimozione di \"{0} ({1})\"")
   public static final String REMOVE_ITEM = "6";

   /**
    * Exceptions
    **/
   @RBEntry("Impossibile eliminare l'elemento \"{0} ({1})\" poiché è contenuto in una o più baseline o configurazioni di prodotto.")
   public static final String PRODUCT_DELETE_ITEM = "7";

   @RBEntry("Impossibile modificare l'elemento \"{0} ({1})\" poiché è contenuto in una o più baseline o configurazioni di prodotto.")
   public static final String PRODUCT_MODIFY_ITEM = "8";

   @RBEntry("Le informazioni fornite relative al membro della baseline sono di tipo non corretto per questo delegato.")
   public static final String BASELINEMEMBERINFO_WRONG_TYPE = "9";

   @RBEntry("Impossibile aggiungere \"{0}\" ad una baseline perché è impossibile aggiungere due oggetti con lo stesso master ad una baseline.  ")
   public static final String SAME_MASTER_NOT_ALLOWED = "10";

   @RBEntry("Impossibile eliminare gli oggetti che seguono perché si trovano in una o più baseline.")
   public static final String MULTI_DELETE_ITEM = "11";

   @RBEntry("Impossibile modificare gli oggetti che seguono perché si trovano in una o più baseline.")
   public static final String MULTI_MODIFY_ITEM = "12";

   @RBEntry("Impossibile eliminare gli oggetti che seguono perché si trovano in una o più baseline o configurazioni di prodotto.")
   public static final String MULTI_PRODUCT_DELETE_ITEM = "13";

   @RBEntry("Impossibile modificare gli oggetti che seguono perché si trovano in una o più baseline o configurazioni di prodotto.")
   public static final String MULTI_PRODUCT_MODIFY_ITEM = "14";

   @RBEntry("{0} ({1})")
   public static final String ITERATION_IDENTITY = "15";

   @RBEntry("Impossibile eliminare gli elementi che seguono perché sono membri di \"{1}\". \"{0}\"")
   public static final String DELETE_BASELINEABLE = "16";

   @RBEntry("Impossibile eliminare gli elementi che seguono perché sono membri della baseline protetta \"{1}\". \"{0}\"")
   public static final String DELETE_BASELINEABLE_PROTECTED = "17";

   @RBEntry("L'elemento \"{0}\" è membro delle baseline non protette \"{1}\". Per proseguire nell'eliminazione dell'elemento, scegliere \"Elimina membro\".")
   public static final String MEMBER_OF_UNPROTECTED_BASELINES = "18";

   @RBEntry("Le baseline gestite non possono avere membri contenuti in progetti. Gli oggetti che seguono appartengono a un progetto e non sono pertanto validi:")
   public static final String BASELINEABLES_IN_PROJECTS = "19";

   @RBEntry("Avviso: il messaggio di eccezione supera la dimensione massima di visualizzazione.")
   public static final String MESSAGE_LIMIT_REACHED = "20";

   @RBEntry("Impossibile modificare l'appartenenza di una baseline gestita con blocco a livello amministrativo")
   public static final String ADMIN_LOCKED_BASELINE_MEMBERS = "21";
}
