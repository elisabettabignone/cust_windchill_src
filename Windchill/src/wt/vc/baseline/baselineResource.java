/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.vc.baseline;

import wt.util.resource.*;

@RBUUID("wt.vc.baseline.baselineResource")
public final class baselineResource extends WTListResourceBundle {
   /**
    * Exceptions
    **/
   @RBEntry("The following items cannot be deleted because they are in one or more baselines that are inaccessible to you. \"{0}\"")
   public static final String DELETE_ITEM = "0";

   @RBEntry("Baseline \"{0}\" is currently being modified by another user.  The operation cannot be completed.")
   public static final String BASELINE_LOCKED = "1";

   @RBEntry("This item cannot be created in a personal cabinet.")
   public static final String NOT_IN_SHARE_CABINET = "2";

   @RBEntry("Item \"{0} ({1})\" cannot be modified because it is in one or more baselines.")
   public static final String MODIFY_ITEM = "3";

   /**
    * Messages
    **/
   @RBEntry("Adding \"{0} ({1})\"")
   public static final String ADD_ITEM = "4";

   @RBEntry("Replacing \"{0} ({1})\" with \"{2} ({3})\"")
   public static final String REPLACE_ITEM = "5";

   @RBEntry("Removing \"{0} ({1})\"")
   public static final String REMOVE_ITEM = "6";

   /**
    * Exceptions
    **/
   @RBEntry("Item \"{0} ({1})\" cannot be deleted because it is in one or more baselines or Product Configurations.")
   public static final String PRODUCT_DELETE_ITEM = "7";

   @RBEntry("Item \"{0} ({1})\" cannot be modified because it is in one or more baselines or Product Configurations.")
   public static final String PRODUCT_MODIFY_ITEM = "8";

   @RBEntry("The BaselineMemberInfo provided is the wrong type for this delegate.")
   public static final String BASELINEMEMBERINFO_WRONG_TYPE = "9";

   @RBEntry("Cannot add \"{0}\" to baseline because two objects with the same master cannot be added to a baseline.  ")
   public static final String SAME_MASTER_NOT_ALLOWED = "10";

   @RBEntry("The following objects cannot be deleted because they are in one or more baselines.")
   public static final String MULTI_DELETE_ITEM = "11";

   @RBEntry("The following objects cannot be modified because they are in one or more baselines.")
   public static final String MULTI_MODIFY_ITEM = "12";

   @RBEntry("The following objects cannot be deleted because they are in one or more baselines or Product Configurations.")
   public static final String MULTI_PRODUCT_DELETE_ITEM = "13";

   @RBEntry("The following objects cannot be modified because they are in one or more baselines or Product Configurations.")
   public static final String MULTI_PRODUCT_MODIFY_ITEM = "14";

   @RBEntry("{0} ({1})")
   public static final String ITERATION_IDENTITY = "15";

   @RBEntry("The following items cannot be deleted because they are members of \"{1}\". \"{0}\"")
   public static final String DELETE_BASELINEABLE = "16";

   @RBEntry("The following items cannot be deleted because they are members of protected baseline \"{1}\". \"{0}\"")
   public static final String DELETE_BASELINEABLE_PROTECTED = "17";

   @RBEntry("Item \"{0}\" is a member of the unprotected baseline(s) \"{1}\".  To continue deleting the item, select \"Delete Member\".")
   public static final String MEMBER_OF_UNPROTECTED_BASELINES = "18";

   @RBEntry("Managed baselines can not have members that are contained in projects.  The following objects are invalid because they belong to a project:")
   public static final String BASELINEABLES_IN_PROJECTS = "19";

   @RBEntry("Notice: the exception message exceeds the limit for this display.")
   public static final String MESSAGE_LIMIT_REACHED = "20";

   @RBEntry("Membership of a administratively locked managed baseline cannot be changed")
   public static final String ADMIN_LOCKED_BASELINE_MEMBERS = "21";
}
