/* bcwti
 *
 * Copyright (c) 2011 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.vc.baseline.ixb.handlers.forclasses;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBArgComment1;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.vc.baseline.ixb.handlers.forclasses.ixbManagedBaselineResource")
public final class ixbManagedBaselineResource_it extends WTListResourceBundle {
    @RBEntry("Impossibile esportare l'oggetto seguente come parte della baseline '{0}', perché non è un tipo di oggetto supportato per l'esportazione: '{1}'")
    @RBComment("Only federatable baseline members can be exported at this time")
    @RBArgComment0("Managed Baseline identity")
    @RBArgComment1("Baseline Member identity, but could say 'SECURED INFORMATION'")
    public static final String NON_FEDERATABLE_BASELINE_MEMBER = "NON_FEDERATABLE_BASELINE_MEMBER";

    @RBEntry("Impossibile trovare uno o più oggetti nella baseline seguente: '{0}'")
    @RBComment("The baseline member does not exist")
    @RBArgComment0("Managed Baseline identity")
    public static final String NON_EXISTENT_BASELINE_MEMBER = "NON_EXISTENT_BASELINE_MEMBER";

    @RBEntry("La baseline seguente contiene uno o più oggetti per cui l'utente non dispone dell'autorizzazione: '{0}'")
    @RBComment("A general comment for the entry string")
    @RBArgComment0("Managed Baseline identity")
    public static final String NO_ACCESS_TO_BASELINE_MEMBER = "NO_ACCESS_TO_BASELINE_MEMBER";
}
