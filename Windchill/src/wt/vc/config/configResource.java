/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.vc.config;

import wt.util.resource.*;

@RBUUID("wt.vc.config.configResource")
public final class configResource extends WTListResourceBundle {
   @RBEntry("Cannot append search condition:  the wt.query.QuerySpec is either null or specifies a target class that is not \"{0}\"")
   public static final String CAN_NOT_APPEND_SEARCH_CONDITION = "0";

   @RBEntry("An element of the wt.fc.QueryResult\"{0}\" is invalid: all elements should be \"{1}\"")
   public static final String INVALID_ELEMENT = "1";

   @RBEntry("No {0} object was found with a {1} of {2}.")
   public static final String NO_OBJECT_FOUND = "10";

   @RBEntry("The role \"{0}\" is invalid:  it is either null or does not resolve to an element that is \"{1}\"")
   public static final String INVALID_ROLE = "2";

   @RBEntry("The index specified is out of range")
   public static final String INDEX_OUT_OF_RANGE = "3";

   @RBEntry("The \"{0}\" is not valid:  it may have been deleted")
   public static final String INVALID_REFERENCE = "4";

   @RBEntry("The value for the argument \"{0}\" can not be null")
   public static final String NULL_ARGUMENT = "5";

   @RBEntry("The \"{0}\" argument, a \"{1}\" object, is not \"{2}\" as expected")
   public static final String INVALID_DELEGATE_ARGUMENT_TYPE = "6";

   @RBEntry("The wt.vc.config.EffectivityConfigSpec is set up to use a date effectivity, but no date is set")
   public static final String INVALID_DATE_EFFECTIVITY = "7";

   @RBEntry("The wt.vc.config.EffectivityConfigSpec is set up to use a unit-based effectivity (lot or serial number), but the \"{0}\" is not set")
   public static final String INVALID_SERIAL_LOT_EFFECTIVITY = "8";

   @RBEntry("The wt.query.QuerySpec is invalid:  it is either null, or is not setup to query wt.vc.Iterated objects")
   public static final String INVALID_QUERY_SPEC = "9";
}
