/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.vc.config;

import wt.util.resource.*;

@RBUUID("wt.vc.config.configResource")
public final class configResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile aggiungere la condizione di ricerca:  wt.query.QuerySpec è pari a zero oppure specifica una classe di destinazione che non è \"{0}\"")
   public static final String CAN_NOT_APPEND_SEARCH_CONDITION = "0";

   @RBEntry("Un elemento del wt.fc.QueryResult \"{0}\" non è valido: tutti gli elementi devono essere \"{1}\"")
   public static final String INVALID_ELEMENT = "1";

   @RBEntry("Nessun oggetto {0} corrisponde al criterio di ricerca \"{1}\" = {2}.")
   public static final String NO_OBJECT_FOUND = "10";

   @RBEntry("Il ruolo \"{0}\" non è valido: è pari a zero oppure non corrisponde a un elemento \"{1}\"")
   public static final String INVALID_ROLE = "2";

   @RBEntry("L'indice specificato non rientra nell'intervallo consentito")
   public static final String INDEX_OUT_OF_RANGE = "3";

   @RBEntry("\"{0}\" non è valido: potrebbe essere stato eliminato")
   public static final String INVALID_REFERENCE = "4";

   @RBEntry("Il valore per l'argomento \"{0}\" non può essere nullo")
   public static final String NULL_ARGUMENT = "5";

   @RBEntry("Argomento \"{0}\", oggetto \"{1}\", non \"{2}\" come previsto.")
   public static final String INVALID_DELEGATE_ARGUMENT_TYPE = "6";

   @RBEntry("wt.vc.config.EffectivityConfigSpec è configurato come effettività per data ma non è stata immessa alcuna data")
   public static final String INVALID_DATE_EFFECTIVITY = "7";

   @RBEntry("wt.vc.config.EffectivityConfigSpec è configurato come effettività per unità (numero di lotto o serie), ma \"{0}\" non è stato immesso")
   public static final String INVALID_SERIAL_LOT_EFFECTIVITY = "8";

   @RBEntry("wt.query.QuerySpec è invalido: è nullo o non è configurato per ricercare gli oggetti wt.vc.Iterated")
   public static final String INVALID_QUERY_SPEC = "9";
}
