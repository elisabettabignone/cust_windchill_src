/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.vc;

import wt.util.resource.RBArgComment0;
import wt.util.resource.RBArgComment1;
import wt.util.resource.RBComment;
import wt.util.resource.RBEntry;
import wt.util.resource.RBUUID;
import wt.util.resource.WTListResourceBundle;

@RBUUID("wt.vc.vcResource")
public final class vcResource extends WTListResourceBundle {
   @RBEntry("The object was improperly initialized (i.e., it was null).")
   public static final String IMPROPER_INITIALIZATION = "0";

   @RBEntry("The version has a non-persisted master; it must be persisted before storing the version.")
   public static final String NONPERSISTENT_MASTER = "1";

   @RBEntry("The version has a non-persisted iteration; it must be persisted before storing the version.")
   public static final String NONPERSISTENT_ITERATION = "2";

   @RBEntry("The master/iteration cannot be stored independent of the version; they all must be stored within the same transaction.")
   public static final String INVALID_DATABASE_INSERTION = "3";

   @RBEntry("A database transaction failed. Please see following error(s) for clarification.")
   public static final String FAILED_TRANSACTION = "4";

   @RBEntry("An attribute setting was vetoed. Please see following error(s) for clarification.")
   public static final String VETOED_SETTING = "5";

   @RBEntry("The given identifier has to be greater than the given iteration's identifier.")
   public static final String INVALID_IDENTIFIER = "6";

   @RBEntry("The object has no predecessor for navigation.")
   public static final String NO_PREDECESSOR = "7";

   @RBEntry("The object has no successor for navigation.")
   public static final String NO_SUCCESSOR = "8";

   @RBEntry("The object is not the latest version and cannot be deleted.")
   public static final String INVALID_DATABASE_DELETION = "9";

   @RBEntry("{0} objects found where only 1 is applicable")
   public static final String MANY_OBJECTS_FOUND = "10";

   @RBEntry("An object other than the latest iteration cannot be revised or branched.")
   public static final String INVALID_BRANCH_ATTEMPT = "11";

   @RBEntry("A rollback must be from the latest iteration in a branch to another iteration in the same branch.")
   public static final String INVALID_ROLLBACK_ATTEMPT = "12";

   @RBEntry("A rollup must be from the first iteration in a branch to another iteration in the same branch.")
   public static final String INVALID_ROLLUP_ATTEMPT = "13";

   @RBEntry("The Iterated object being appended to branch {0} can not be Persistent.")
   public static final String APPEND_ITERATION_MUST_BE_NONPERSISTENT = "14";

   @RBEntry("{0} is a one-off version and cannot be revised.")
   @RBArgComment0("The display identity of the one-off object")
   public static final String CANNOT_REVISE_ONEOFF = "15";

   @RBEntry("If you are using this method, please remove it from your code.  It did not accomplish it's intended purpose and has been replaced by a database-managed constraint that does.")
   public static final String IS_NEW_VERSION_CHECKED_OUT_DEPRECATED = "16";

   @RBEntry("Error in setting attributes of the new iteration.")
   public static final String ATTRIBUTE_ERROR = "17";

   @RBEntry("More than one latest version found for Branch id: {0}")
   public static final String MULTIPLE_LATEST_FOR_BRANCH = "18";

   @RBEntry("No latest version found for Branch id: {0}")
   public static final String NO_LATEST_FOR_BRANCH = "19";

   @RBEntry("Target is not configured correctly, missing iteration cookie.")
   public static final String MISSING_ITERATION_COOKIE = "20";

   @RBEntry("Branch ID Must be set.")
   public static final String NO_BRANCH_ID = "21";

   @RBEntry("Target iteration can not be persisted prior to Insert.")
   public static final String TARGET_PERSISTED = "22";

   @RBEntry("No iterations found for Branch: {0}")
   public static final String NO_ITERATIONS_FOR_BRANCH = "23";

   @RBEntry("Iteration already exists.")
   public static final String EXISTING_ITERATION = "24";

   @RBEntry("No gap in the Iteration Series found to insert into for iteration: {0}")
   public static final String MISSING_GAP = "25";

   @RBEntry("No latest iteration found for Branch id: {0}")
   public static final String NO_LATEST_ITERATION_FOR_BRANCH = "26";

   @RBEntry("At least one successor is required")
   public static final String AT_LEAST_ONE_REQUIRED = "27";

   @RBEntry("Target Iteration must be persistent: {0}")
   public static final String TARGET_NOT_PERSISTENT = "28";

   @RBEntry("Successor Versionable must be persistent : {0}")
   public static final String SUCCESSOR_NOT_PERSISTENT = "29";

   @RBEntry("Target and Versionable must be in different branches")
   public static final String TARGET_AND_VERSIONABLE_SAME_BRANCH = "30";

   @RBEntry("Data corruption::missing control branch entry for Versionable")
   public static final String BAD_DATA_NO_CONTROL_BRANCH = "31";

   @RBEntry("Merge source and destination versions must share same master (disparate masters: {0} and {1})")
   public static final String MERGE_BETWEEN_DIFFERENT_MASTERS = "32";

   @RBEntry("Source and destination versions must differ for merge (disparate versions: {0} and {1})")
   public static final String SAME_MERGE_SOURCE_AND_DESTINATION = "33";

   @RBEntry("Version was not updated")
   public static final String VERSION_NOT_UPDATED = "34";

   @RBEntry("Latest iterations attempted for deletion: '{0}'.")
   public static final String DELETION_OF_LATEST_ITERATIONS = "35";

   @RBEntry("You are attempting to delete an iteration of this object without deleting iterations on later revisions.")
   public static final String DELETION_OF_BRANCH_POINT = "36";

   @RBEntry("A deleteIterations call must be from chronologically smaller iteration to larger iteration in the same branch. It should not delete whole version.")
   public static final String INVALID_DELETE_ITERATIONS_ATTEMPT = "37";

   @RBEntry("Target is not configured correctly, missing version cookie.")
   public static final String MISSING_VERSION_COOKIE = "38";

   @RBEntry("Ufid set on target is different from its ufid passed as argument in insertNode.")
   public static final String INCONSISTENT_UFID = "39";

   @RBEntry("You are attempting to delete iterations of this object without deleting iterations on later revisions.")
   public static final String DELETION_OF_BRANCH_POINTS = "40";

   @RBEntry("Iteration '{0}' is not the latest iteration and cannot be deleted unless all iterations on later revisions are also selected for deletion.")
   public static final String DELETION_OF_BRANCH_POINT_ITERATION = "41";

   @RBEntry("Cannot apply merge with non-latest source iteration: {0}")
   public static final String CANNOT_MERGE_WITH_NON_LATEST_SOURCE_ITERATION = "42";

   @RBEntry("Cannot apply merge with non-latest destination iteration: {1}")
   public static final String CANNOT_MERGE_WITH_NON_LATEST_DESTINATION_ITERATION = "43";

   @RBEntry("Unable to move version {0} to target context {1} because versions schemes do not match.")
   public static final String VERSION_SCHEME_MISMATCH = "44";

   @RBEntry("Messages follow.")
   @RBArgComment0("Top level message.")
   public static final String INDIVIDUAL_CONFLICT = "46";

   @RBEntry("Unable to delete.  Object is in an invalid state for deletion.")
   public static final String INVALID_DELETION_STATE = "47";

   @RBEntry("You can only change the domain of the latest iteration of a version. Non-latest iteration: \"{0}\" Target domain: \"{1}\"")
   @RBArgComment0("The display identity of the object that could not be assigned to a new administrative domain")
   @RBArgComment1("The display identity of the administrative domain the object could not be assigned to ")
   public static final String CANNOT_CHANGE_DOMAIN_OF_NON_LATEST_ITERATION = "48";

   @RBEntry("Unable to delete the object {0} because only the latest version of an object may be deleted.")
   @RBComment("Use this message if deleting an intermediate version and user does have access to the object.")
   public static final String CANNOT_DELETE_INTERMEDIATE_VERSIONS = "49";

   @RBEntry("Critical error: {0} is not the latest iteration in this revision.  A Revise can only be performed on the latest iteration of a revision.")
   @RBArgComment0("The display identity of the non-latest iteration")
   public static final String CANNOT_REVISE_NON_LATEST_ITERATION = "50";

   @RBEntry("{0} is a working copy of a checked out item and cannot be revised.")
   @RBArgComment0("The display identity of the checked out item")
   public static final String CANNOT_REVISE_WORKING_COPY = "51";

   @RBEntry("{0} is not the latest version and cannot be revised.")
   @RBArgComment0("The display identity of the non-latest version")
   public static final String CANNOT_REVISE_NON_LATEST_VERSION = "52";

   @RBEntry("One or more items are not the latest version and cannot be revised.")
   public static final String CANNOT_REVISE_NON_LATEST_VERSION_MULTI = "53";

   @RBEntry("Cannot create {0} from version {1}.")
   @RBArgComment0("The display identity of the object")
   @RBArgComment1("The existing version from which the new version is being created")
   public static final String CANNOT_CREATE_VERSION_FROM_VERSION = "54";

   @RBEntry("{0} may not have its identity changed.  The identity of this item may only be changed if it is not checked out and you have {1} permission for all versions.")
   @RBArgComment0("The display identity of the object")
   @RBArgComment1("Permission needed to change the object's identity")
   public static final String IDENTITY_CHANGE_NOT_ALLOWED = "55";

   @RBEntry("Revision level does not match the view level: {0}")
   @RBArgComment0("The mismatched revision ID value")
   public static final String VIEW_VERSION_LEVEL_MISMATCH = "56";

   @RBEntry("Object revision does not match branch revision: {0}")
   @RBArgComment0("The object display identity.")
   public static final String CONTROL_BRANCH_REVISION_MISMATCH = "57";

   @RBEntry("This method may only be called for ViewManageable objects.")
   public static final String VIEW_MANAGEABLE_METHOD_ERROR = "58";

   @RBEntry("Unable to construct version identifier for series: {0}")
   @RBArgComment0("The series name.")
   public static final String CANNOT_CREATE_VERSION_IDENTIFIER = "59";

   @RBEntry("Version already exists, cannot create a duplicate: {0}")
   @RBArgComment0("The object display identity.")
   public static final String VERSION_NOT_UNIQUE = "60";

   @RBEntry("Inconsistent view assignment.  Either all iterations of an object must be assigned a view, or no iterations may be assigned a view: {0}")
   @RBArgComment0("The object display identity.")
   public static final String INCONSISTENT_VIEW_ASSIGNMENT = "61";

   @RBEntry("Cannot assign version series {0} to an object that requires {1}")
   @RBArgComment0("The illegal version identifier series and value.")
   public static final String VERSION_SERIES_MISMATCH = "62";

   @RBEntry("View versions not supported in this method.")
   public static final String VIEW_VERSIONS_NOT_ALLOWED = "64";

   @RBEntry("Cannot change revision of {0} to target revision {1}.")
   @RBArgComment0("The display identity of the object")
   @RBArgComment1("The target revision ID")
   public static final String CANNOT_CHANGE_REVISION = "65";

   @RBEntry("{0} is an original copy of a checked out item and cannot be revised.")
   @RBArgComment0("The display identity of the checked out item")
   public static final String CANNOT_REVISE_ORIGINAL_COPY = "66";

   @RBEntry("Cannot revise {0} because a successor revision {1} is checked out.")
   @RBArgComment0("The first argument is the display identity of a non-latest revision that will be revised.  The second argument is the working copy of a successor revision.")
   public static final String CANNOT_REVISE_SUCCESSOR_CHECKED_OUT = "67";

   @RBEntry("Cannot revise {0} to {1} because a later revision exists for this view.")
   @RBArgComment0("The first argument is the display identity of a non-latest revision that will be revised.  The second argument is the revision label being revised to.")
   public static final String CANNOT_REVISE_TO_GAP = "68";

   @RBEntry("Cannot determine latest revision for objects that are not persisted.")
   @RBComment("Used for error message when user tries to find the latest revision of an object that is not persistent.")
   public static final String NO_LATEST_FOR_NON_PERSISTENT = "69";

   @RBEntry("Cannot revise {0} because a successor revision {1} is checked out to a project.")
   @RBArgComment0("The first argument is the display identity of a non-latest revision that will be revised.  The second argument is the working copy of a successor revision.")
   public static final String CANNOT_REVISE_SUCCESSOR_SANDBOX_CHECKED_OUT = "70";

   @RBEntry("Revision level is not 1: {0}")
   @RBArgComment0("The revision ID level is required to be 1 in the single-level revision label mode for view versions.")
   public static final String SINGLE_LEVEL_VIEW_VERSION_LEVEL_MISMATCH = "71";

   @RBEntry("Cannot revise {0} because it is checked out to a project.")
   public static final String CANNOT_REVISE_SANDBOX_CHECKED_OUT = "72";

   @RBEntry("Cannot revise {0} because this revision has a non-latest iteration checked out.")
   @RBArgComment0("The target object for revision.")
   public static final String CANNOT_REVISE_NONLATEST_ITERATION_CHECKED_OUT = "73";

   /**
    * System level non-latest revise setting
    **/
   @RBEntry("Allow revise of non-latest revisions")
   public static final String PRIVATE_CONSTANT_0 = "ENABLE_NON_LATEST_REVISE";

   @RBEntry("If \"Yes\", then system will allow the revise of non-latest revisions for valid object types.")
   public static final String PRIVATE_CONSTANT_1 = "ENABLE_NON_LATEST_REVISE_DESC";
   
   @RBEntry("Cannot iterate or revise \'{0}\' because it is administratively locked")
   @RBArgComment0("The object display identity.")
   public static final String ITERATE_NOT_ALLOWED_FOR_ADMIN_LOCKED_OBJECT = "ITERATE_NOT_ALLOWED_FOR_ADMIN_LOCKED_OBJECT";
   
   @RBEntry("Illegal Argument. The nodes to be inserted must be of the type \"Iterated\". ")
   public static final String INSERT_NODES_ILLEGAL_ARGUMENT_NOT_ITERATED = "INSERT_NODES_ILLEGAL_ARGUMENT_NOT_ITERATED";
   
   @RBEntry("Illegal Argument. The nodes to be inserted must be non persistent")
   public static final String INSERT_NODES_ILLEGAL_ARGUMENT_ALREADY_PERSISTED_OBJECT = "INSERT_NODES_ILLEGAL_ARGUMENT_ALREADY_PERSISTED_OBJECT";
   
   @RBEntry("Illegal Argument. The nodes to be inserted must have a master specified")
   public static final String INSERT_NODES_ILLEGAL_ARGUMENT_NO_MASTER = "INSERT_NODES_ILLEGAL_ARGUMENT_NO_MASTER";
   
   @RBEntry("Illegal Argument. The nodes to be inserted must have an iteration identifier specified")
   public static final String INSERT_NODES_ILLEGAL_ARGUMENT_MISSING_ITERATION_COOKIE = "INSERT_NODES_ILLEGAL_ARGUMENT_MISSING_ITERATION_COOKIE";
   
   @RBEntry("Illegal Argument. The nodes to be inserted must have a version identifier specified")
   public static final String INSERT_NODES_ILLEGAL_ARGUMENT_MISSING_VERSION_COOKIE = "INSERT_NODES_ILLEGAL_ARGUMENT_MISSING_VERSION_COOKIE";
   
   @RBEntry("Unable to insert {0}. Can not locate the Control Branch")
   public static final String INSERT_NODES_CAN_NOT_LOCATE_CONTROL_BRANCH = "INSERT_NODES_CAN_NOT_LOCATE_CONTROL_BRANCH";
   
}