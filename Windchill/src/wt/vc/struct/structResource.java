/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.vc.struct;

import wt.util.resource.*;

@RBUUID("wt.vc.struct.structResource")
public final class structResource extends WTListResourceBundle {
   @RBEntry("Cannot create a reflexive Usage/Reference Link. The following object is trying to make a link to itself: \"{0}\"")
   public static final String REFLEXIVE_LINK = "0";

   @RBEntry("Cannot delete a master that is usedBy or referencedBy another version")
   public static final String CAN_NOT_DELETE = "1";

   @RBEntry("The value for the argument \"{0}\" cannot be null")
   public static final String NULL_ARGUMENT = "2";

   @RBEntry("This operation cannot be performed since \"{0}\" is checked out")
   public static final String CHECKED_OUT_COPY_PROHIBITED = "3";

   @RBEntry("Cannot check item(s) into PDM because they depend on other item(s) that are still in the project.")
   public static final String PDM_CHECKIN_VIOLATION = "300001";

   @RBEntry("Item \"{0}\" uses \"{1}\" that is still in the project.")
   public static final String PDM_CHECKIN_USAGE_VIOLATION = "4";

   @RBEntry("Item \"{0}\" references \"{1}\" that is still in the project.")
   public static final String PDM_CHECKIN_REFERENCE_VIOLATION = "5";

   @RBEntry("Item \"{0}\" is described by \"{1}\" that is still in the project.")
   public static final String PDM_CHECKIN_DESCRIBE_VIOLATION = "6";

   @RBEntry("Illegal usage -- child is in project.  \"{0}\" cannot use \"{1}\" because it is not in the same project.")
   public static final String USES_IS_IN_PROJECT = "7";

   @RBEntry("Illegal reference -- referenced object is in project.  \"{0}\" cannot reference \"{1}\" because it is not in the same project.")
   public static final String REFERENCES_IS_IN_PROJECT = "8";

   @RBEntry("Illegal describe -- describing object is in project.  \"{0}\" cannot be described by \"{1}\" because it is not in the same project.")
   public static final String DESCRIBED_BY_IS_IN_PROJECT = "9";

   @RBEntry("Cannot delete master \"{0}\" because it is referenced by following version(s) : {1}")
   @RBArgComment0("Display identity of the object being deleted.")
   @RBArgComment1("Display identity of the object which references the deleted object.")
   public static final String CAN_NOT_DELETE_REFERENCED_BY = "10";

   @RBEntry("Cannot delete master \"{0}\" that is usedBy following versions : {1}")
   @RBArgComment0("Display identity of the object being deleted.")
   @RBArgComment1("Display identity of the object which uses the deleted object.")
   public static final String CAN_NOT_DELETE_USED_BY = "11";

   @RBEntry("Cannot delete described by \"{0}\" that describes following versions : {1}")
   @RBArgComment0("Display identity of the object being deleted.")
   @RBArgComment1("Display identity of the object which uses the deleted object.")
   public static final String CAN_NOT_DELETE_DESCRIBED_BY = "12";

   @RBEntry("Illegal link creation.  Cannot link objects \"{0}\" and \"{1}\" because they are not in the same organization context.")
   public static final String DIFFERENT_NAMESPACES = "13";

   @RBEntry("Illegal usage link creation.  \"{0}\" cannot use \"{1}\" because both are not in Site.")
   public static final String USES_IS_IN_SITE = "14";

   @RBEntry("Illegal reference link creation.  \"{0}\" cannot reference \"{1}\" because both are not in the site context.")
   public static final String REFERENCES_IS_IN_SITE = "15";

   @RBEntry("Illegal describe link creation.  \"{0}\" cannot be described by \"{1}\" because both are not in the site context.")
   public static final String DESCRIBED_BY_IS_IN_SITE = "16";

   @RBEntry("Illegal usage link creation. \"{0}\" cannot use \"{1}\" because both are not in the same organization context.")
   public static final String USES_IS_IN_ORG = "17";

   @RBEntry("Illegal reference link creation.  \"{0}\" cannot reference \"{1}\" because both are not in the same organization context.")
   public static final String REFERENCES_IS_IN_ORG = "18";

   @RBEntry("Illegal describe link creation.  \"{0}\" cannot be described by \"{1}\" because both are not in the same organization context.")
   public static final String DESCRIBED_BY_IS_IN_ORG = "19";

   @RBEntry("Illegal link.  \"{0}\" cannot be linked to \"{1}\" because both objects are not in a PDM context.")
   public static final String BOTH_MUST_BE_IN_PDM_CONTAINER = "20";

   @RBEntry("Illegal usage link creation. \"{0}\" is in a project and cannot use \"{1}\" because it is in a PDM context and is not checked out or shared to the same project.")
   public static final String USEDBY_IS_IN_PROJ_USES_NOT_IN_VALID_PDM_CONTAINER = "21";

   @RBEntry("Illegal reference link creation.  \"{0}\" is in a project and cannot reference \"{1}\" because it is in a PDM context and is not checked out or shared to the same project.")
   public static final String REFERENCEDBY_IS_IN_PROJ_REFERENCES_NOT_IN_VALID_PDM_CONTAINER = "22";

   @RBEntry("Illegal describe link creation.  \"{0}\" is in a project and cannot be described by \"{1}\" because it is in a PDM context and is not checked out or shared to the same project.")
   public static final String DESCRIBES_IS_IN_PROJ_DESCRIBEDBY_NOT_IN_VALID_PDM_CONTAINER = "23";
   
   @RBEntry("\"{0}\" is not valid filter links delegate for copying forward links.")
   public static final String INVALID_FILTER_LINKS_DELEGATE = "INVALID_FILTER_LINKS_DELEGATE";
   
   @RBEntry("This operation cannot be performed due to an internal error.")
   public static final String INTERNAL_ERROR = "INTERNAL_ERROR";
}
