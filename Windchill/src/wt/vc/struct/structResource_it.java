/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.vc.struct;

import wt.util.resource.*;

@RBUUID("wt.vc.struct.structResource")
public final class structResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile creare un link utilizzo/riferimento riflessivo. L'oggetto seguente sta tentando di creare un link a se stesso: \"{0}\"")
   public static final String REFLEXIVE_LINK = "0";

   @RBEntry("Impossibile eliminare un master usato o referenziato da un'altra versione")
   public static final String CAN_NOT_DELETE = "1";

   @RBEntry("Il valore per l'argomento  \"{0}\" non può essere nullo")
   public static final String NULL_ARGUMENT = "2";

   @RBEntry("Questa operazione non può essere eseguita poiché \"{0}\" è sottoposto a Check-Out")
   public static final String CHECKED_OUT_COPY_PROHIBITED = "3";

   @RBEntry("Impossibile effettuare il Check-In degli elementi in PDM in quanto dipendono da altri elementi che sono ancora nel progetto.")
   public static final String PDM_CHECKIN_VIOLATION = "300001";

   @RBEntry("L'elemento \"{0}\" utilizza \"{1}\" che è ancora nel progetto.")
   public static final String PDM_CHECKIN_USAGE_VIOLATION = "4";

   @RBEntry("L'elemento \"{0}\" fa riferimento a \"{1}\" che è ancora nel progetto.")
   public static final String PDM_CHECKIN_REFERENCE_VIOLATION = "5";

   @RBEntry("L'elemento \"{0}\" è descritto da \"{1}\" che è ancora nel progetto.")
   public static final String PDM_CHECKIN_DESCRIBE_VIOLATION = "6";

   @RBEntry("Utilizzo non valido. Il figlio è presente nel progetto. \"{0}\" non può utilizzare \"{1}\" perché non è contenuto nello stesso progetto.")
   public static final String USES_IS_IN_PROJECT = "7";

   @RBEntry("Riferimento non valido. L'oggetto di riferimento è presente nel progetto. \"{0}\" non può fare riferimento a \"{1}\" perché non è contenuto nello stesso progetto.")
   public static final String REFERENCES_IS_IN_PROJECT = "8";

   @RBEntry("Descrizione non valida. L'oggetto descrittivo è presente nel progetto. \"{0}\" non può essere descritto da \"{1}\" perché non è contenuto nello stesso progetto.")
   public static final String DESCRIBED_BY_IS_IN_PROJECT = "9";

   @RBEntry("Impossibile eliminare il master \"{0}\" poiché è referenziato dalle seguenti versioni: {1}")
   @RBArgComment0("Display identity of the object being deleted.")
   @RBArgComment1("Display identity of the object which references the deleted object.")
   public static final String CAN_NOT_DELETE_REFERENCED_BY = "10";

   @RBEntry("Impossibile eliminare il master \"{0}\" poiché è utilizzato dalle seguenti versioni: {1}")
   @RBArgComment0("Display identity of the object being deleted.")
   @RBArgComment1("Display identity of the object which uses the deleted object.")
   public static final String CAN_NOT_DELETE_USED_BY = "11";

   @RBEntry("Impossibile eliminare l'oggetto descritto \"{0}\" poiché descrive le seguenti versioni: {1}")
   @RBArgComment0("Display identity of the object being deleted.")
   @RBArgComment1("Display identity of the object which uses the deleted object.")
   public static final String CAN_NOT_DELETE_DESCRIBED_BY = "12";

   @RBEntry("Creazione link non valida. Impossibile creare link fra gli oggetti \"{0}\" e \"{1}\" in quanto non si trovano nello stesso contesto di organizzazione.")
   public static final String DIFFERENT_NAMESPACES = "13";

   @RBEntry("Creazione link utilizzo non valida. \"{0}\" non può utilizzare \"{1}\" perché non sono entrambi nel sito.")
   public static final String USES_IS_IN_SITE = "14";

   @RBEntry("Creazione link riferimento non valida. \"{0}\" non può fare riferimento a \"{1}\" perché non sono entrambi nel contesto del sito.")
   public static final String REFERENCES_IS_IN_SITE = "15";

   @RBEntry("Creazione link descrizione non valida. \"{0}\" non può essere descritto da \"{1}\" perché non sono entrambi nel contesto di sito.")
   public static final String DESCRIBED_BY_IS_IN_SITE = "16";

   @RBEntry("Creazione link utilizzo non valida. \"{0}\" non può utilizzare \"{1}\" in quanto gli oggetti non si trovano nello stesso contesto di organizzazione.")
   public static final String USES_IS_IN_ORG = "17";

   @RBEntry("Creazione link riferimento non valida. \"{0}\" non può fare riferimento a \"{1}\" in quanto gli oggetti non si trovano nello stesso contesto di organizzazione.")
   public static final String REFERENCES_IS_IN_ORG = "18";

   @RBEntry("Creazione link descrizione non valida. \"{0}\" non può essere descritto da \"{1}\" in quanto gli oggetti non si trovano nello stesso contesto di organizzazione.")
   public static final String DESCRIBED_BY_IS_IN_ORG = "19";

   @RBEntry("Link non valido. Non è possibile creare un link fra \"{0}\" e \"{1}\" in quanto gli oggetti non si trovano entrambi in un contesto PDM.")
   public static final String BOTH_MUST_BE_IN_PDM_CONTAINER = "20";

   @RBEntry("Creazione link utilizzo non valida. \"{0}\" è in un progetto e non può usare \"{1}\" in quanto quest'ultimo è in un contesto PDM e non è sottoposto a Check-Out o condiviso nello stesso progetto.")
   public static final String USEDBY_IS_IN_PROJ_USES_NOT_IN_VALID_PDM_CONTAINER = "21";

   @RBEntry("Creazione link riferimento non valida. \"{0}\" è in un progetto e non può fare riferimento a \"{1}\" in quanto quest'ultimo è in un contesto PDM e non è sottoposto a Check-Out o condiviso nello stesso progetto.")
   public static final String REFERENCEDBY_IS_IN_PROJ_REFERENCES_NOT_IN_VALID_PDM_CONTAINER = "22";

   @RBEntry("Creazione link descrizione non valida. \"{0}\" è in un progetto e non può essere descritto da \"{1}\" in quanto quest'ultimo è in un contesto PDM e non è sottoposto a Check-Out o condiviso nello stesso progetto.")
   public static final String DESCRIBES_IS_IN_PROJ_DESCRIBEDBY_NOT_IN_VALID_PDM_CONTAINER = "23";
   
   @RBEntry("\"{0}\" non è un delegato valido dei link filtro per la copia in successivo dei link.")
   public static final String INVALID_FILTER_LINKS_DELEGATE = "INVALID_FILTER_LINKS_DELEGATE";
   
   @RBEntry("Errore interno. Impossibile eseguire l'operazione.")
   public static final String INTERNAL_ERROR = "INTERNAL_ERROR";
}
