/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.vc.views;

import wt.util.resource.*;

@RBUUID("wt.vc.views.viewsResource")
public final class viewsResource_it extends WTListResourceBundle {
   @RBEntry("La vista \"{0}\" non è valida")
   public static final String INVALID_VIEW_NAME = "0";

   @RBEntry("La vista specificata non è valida")
   public static final String INVALID_VIEW_ID = "1";

   @RBEntry("Impossibile creare un nuovo ramo:  \"{0}\" non è un figlio di \"{1}\"")
   public static final String INVALID_VIEW_FOR_NEWBRANCH = "2";

   @RBEntry("L'eliminazione di \"{0}\" non è supportata")
   public static final String DELETE_NOT_SUPPORTED = "3";

   @RBEntry("La modifica di \"{0}\" non è supportata")
   public static final String MODIFY_NOT_SUPPORTED = "4";

   @RBEntry("La vista \"{0}\" ha già un padre e non può averne un altro")
   public static final String PARENT_ALREADY_EXISTS = "5";

   @RBEntry("La creazione dell'associazione determinerebbe una dipendenza circolare. Impossibile creare l'associazione")
   public static final String CIRCULAR_VIEW_ASSOCIATION = "6";

   @RBEntry("La vista \"{0}\" è associata a un'altra vista. Impossibile inserirla.")
   public static final String INSERT_OBJ_IN_ASSOCIATION = "7";

   @RBEntry("Impossibile eseguire l'inserimento: padre e figlio non sono associati come padre-figlio")
   public static final String INSERT_PARENT_CHILD_FAILURE = "8";

   @RBEntry("Impossibile eseguire l'assegnazione: la versione è stata resa persistente")
   public static final String CAN_NOT_ASSIGN_PERSISTED = "9";

   @RBEntry("Impossibile eseguire l'assegnazione: la versione ha un predecessore")
   public static final String CAN_NOT_ASSIGN_SUCCESSOR = "10";

   @RBEntry("La versione è indipendente dalla vista. Impossibile creare una diramazione")
   public static final String CAN_NOT_BRANCH_INDEPENDENT = "11";

   @RBEntry("Impossibile completare gli attributi della vista nell'oggetto.")
   public static final String INFLATE_FAILED = "12";

   @RBEntry("Impossibile impostare il criterio di ricerca \"vista = {0}\".")
   public static final String SEARCH_CRITERIA = "13";

   @RBEntry("La nuova versione di vista è già stata creata.")
   public static final String SUCCESSOR_VIEW_VERSION_ALREADY_EXISTS = "14";

   @RBEntry("Impossibile creare una nuova vesione di vista:  {0} non ha viste downstream.")
   @RBComment("Indicates that new view version will fail because there are no valid views.")
   @RBArgComment0("The view the object is currently assigned to.")
   public static final String NO_VALID_VIEWS = "15";

   @RBEntry("Impossibile eliminare la vista \"{0}\" in quanto dispone di oggetti assegnati.")
   @RBComment("Indicates that the view can't be deleted because ViewManageables (parts) have been assigned to it.")
   public static final String VIEW_IN_USE = "16";

   @RBEntry("Impossibile eliminare \"{0}\" in quanto la vista o le relative viste figlio dispongono di oggetti assegnati.")
   @RBComment("Indicates that the view can't be moved because ViewManageables (parts) have been assigned to it or to one of its child views.")
   public static final String MOVE_NOT_ALLOWED = "17";

   @RBEntry("*Nome:")
   public static final String VIEW_NAME = "18";

   @RBEntry("Vista padre:")
   public static final String PARENT_VIEW_NAME = "19";

   @RBEntry("Viste")
   public static final String VIEW_TABLE_TITLE = "20";

   @RBEntry("Impossibile riassegnare la vista a oggetti la cui vista non può essere gestita: {0}")
   @RBComment("Indicates that the iterations of the master are not ViewManageable.")
   public static final String ITERATIONS_NOT_VIEWMANAGEABLE = "21";

   @RBEntry("Impossibile riassegnare la vista a oggetti che si trovano in due o più viste: {0}")
   @RBComment("Indicates that the view cannot be changed as not all the iterations of the master belong to the same view.")
   public static final String ITERATIONS_NOT_IN_SAME_VIEW = "22";

   @RBEntry("Impossibile creare un nuovo ramo:  \"{0}\" non è la medesima cosa, un pari livello, o un figlio di \"{1}\"")
   @RBComment("Indicates that the view the user is trying to branch to is not the same as, a sibling of, nor a child of the view the user is trying to branch from.")
   public static final String INVALID_VIEW_FOR_NEWVIEWVARIATION = "24";

   @RBEntry("Impossibile aggiornare l'oggetto:  \"{0}\" non è la medesima cosa, un pari livello, o un figlio di \"{1}\"")
   @RBComment("Indicates that the view the user is trying to update to is not the same as, a sibling of, nor a child of the view of the predecessor version.")
   public static final String INVALID_VIEW_FOR_UPDATEVIEWVARIATION = "25";

   @RBEntry("Variazioni vista disattivate.")
   @RBComment("Used as error message when actions requiring view variations are attempted when the preference is disabled.")
   public static final String VIEW_VARIATIONS_DISABLED = "26";

   @RBEntry("Le varianti vista non sono supportate se la proprietà wt.vc.views.newViewVersionClassicBehavior è impostata su false.")
   @RBComment("Used as error message when actions requiring view variations are attempted when the property wt.vc.views.newViewVersionClassicBehavior is false.")
   public static final String VIEW_VARIATIONS_NOT_SUPPORTED = "27";

   @RBEntry("Il limite di {0} valori legittimi è stato superato per la variazione {1}.")
   @RBComment("Used as error message when it is found that a variation has more values than is allowed.")
   public static final String VARIATION_VALUE_LIMIT_EXCEEDED = "28";
   
   @RBEntry("Impossibile spostare l'oggetto {0} nel contesto {1}. Le due entità non condividono la stessa modalità a livello di vista.")
   @RBComment("Error message when the user attempts to move an object between containers that have different view level modes.")
   public static final String VIEW_MODE_DIFFERENT = "29";

}
