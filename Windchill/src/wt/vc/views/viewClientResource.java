/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.vc.views;

import wt.util.resource.*;

@RBUUID("wt.vc.views.viewClientResource")
public final class viewClientResource extends WTListResourceBundle {
   @RBEntry("[V]iew structure\n[C]reate view\n[R]ename view\n[D]elete view\n[A]ssociate views\n[I]nsert view\n[Q]uit\nYour choice:  ")
   public static final String MENU = "0";

   @RBEntry("V")
   public static final String V = "1";

   @RBEntry("C")
   public static final String C = "2";

   @RBEntry("R")
   public static final String R = "3";

   @RBEntry("A")
   public static final String A = "4";

   @RBEntry("I")
   public static final String I = "5";

   @RBEntry("Q")
   public static final String Q = "6";

   @RBEntry("Enter view name:  ")
   public static final String ENTER_VIEW_NAME = "7";

   @RBEntry("Enter new view name:  ")
   public static final String ENTER_NEW_VIEW_NAME = "8";

   @RBEntry("Enter parent's view name:  ")
   public static final String ENTER_PARENT_NAME = "9";

   @RBEntry("Enter child's view name:  ")
   public static final String ENTER_CHILD_NAME = "10";

   @RBEntry("Enter insert's view name:  ")
   public static final String ENTER_INSERT_NAME = "11";

   @RBEntry("Create View \"{0}\"")
   public static final String CREATE_VIEW = "12";

   @RBEntry("Rename View \"{0}\" to \"{1}\"")
   public static final String RENAME_VIEW = "13";

   @RBEntry("Create Association between \"{0}\" and \"{1}\"")
   public static final String CREATE_ASSOCIATION = "14";

   @RBEntry("Insert \"{0}\" between \"{1}\" and \"{2}\"")
   public static final String INSERT_ASSOCIATION = "15";

   @RBEntry("OK")
   public static final String OK = "16";

   @RBEntry("FAILED")
   public static final String FAILED = "17";

   @RBEntry("Unrecognized command")
   public static final String UNRECOGNIZED = "18";

   @RBEntry("Note:  leave child blank to insert between parent and all children")
   public static final String INSERT_NOTE = "19";

   @RBEntry("D")
   public static final String D = "20";

   @RBEntry("Delete View \"{0}\"")
   public static final String DELETE_VIEW = "21";
}
