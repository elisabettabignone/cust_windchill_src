/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.vc.views;

import wt.util.resource.*;

@RBUUID("wt.vc.views.viewsResource")
public final class viewsResource extends WTListResourceBundle {
   @RBEntry("View \"{0}\" is not valid")
   public static final String INVALID_VIEW_NAME = "0";

   @RBEntry("The view specified is not valid")
   public static final String INVALID_VIEW_ID = "1";

   @RBEntry("Cannot create new branch:  \"{0}\" is not a child of \"{1}\"")
   public static final String INVALID_VIEW_FOR_NEWBRANCH = "2";

   @RBEntry("The deletion of \"{0}\" is not supported")
   public static final String DELETE_NOT_SUPPORTED = "3";

   @RBEntry("The modification of \"{0}\" is not supported")
   public static final String MODIFY_NOT_SUPPORTED = "4";

   @RBEntry("The view \"{0}\" already has a parent, it can not have another")
   public static final String PARENT_ALREADY_EXISTS = "5";

   @RBEntry("Cannot create association:  would result in circular dependency")
   public static final String CIRCULAR_VIEW_ASSOCIATION = "6";

   @RBEntry("Cannot insert view \"{0}\" because it is associated with another view")
   public static final String INSERT_OBJ_IN_ASSOCIATION = "7";

   @RBEntry("Cannot insert: parent and child are not associated as parent/child")
   public static final String INSERT_PARENT_CHILD_FAILURE = "8";

   @RBEntry("Cannot assign: the version has been persisted")
   public static final String CAN_NOT_ASSIGN_PERSISTED = "9";

   @RBEntry("Cannot assign: the version has a predecessor")
   public static final String CAN_NOT_ASSIGN_SUCCESSOR = "10";

   @RBEntry("The version is view-independent, it cannot be branched")
   public static final String CAN_NOT_BRANCH_INDEPENDENT = "11";

   @RBEntry("Unable to fill in the attributes for the view in the object.")
   public static final String INFLATE_FAILED = "12";

   @RBEntry("Unable to set search criteria to view = {0}.")
   public static final String SEARCH_CRITERIA = "13";

   @RBEntry("The new view version has already been created.")
   public static final String SUCCESSOR_VIEW_VERSION_ALREADY_EXISTS = "14";

   @RBEntry("Cannot create a new view version:  {0} has no downstream views.")
   @RBComment("Indicates that new view version will fail because there are no valid views.")
   @RBArgComment0("The view the object is currently assigned to.")
   public static final String NO_VALID_VIEWS = "15";

   @RBEntry("Cannot delete view \"{0}\" because objects have been assigned to it.")
   @RBComment("Indicates that the view can't be deleted because ViewManageables (parts) have been assigned to it.")
   public static final String VIEW_IN_USE = "16";

   @RBEntry("Cannot move view \"{0}\" because objects have been assigned to it or its child views.")
   @RBComment("Indicates that the view can't be moved because ViewManageables (parts) have been assigned to it or to one of its child views.")
   public static final String MOVE_NOT_ALLOWED = "17";

   @RBEntry("*Name:")
   public static final String VIEW_NAME = "18";

   @RBEntry("Parent View:")
   public static final String PARENT_VIEW_NAME = "19";

   @RBEntry("Views")
   public static final String VIEW_TABLE_TITLE = "20";

   @RBEntry("Cannot reassign the view of objects that are not view manageable: {0}")
   @RBComment("Indicates that the iterations of the master are not ViewManageable.")
   public static final String ITERATIONS_NOT_VIEWMANAGEABLE = "21";

   @RBEntry("Cannot reassign the view of objects that are in already in two or more views: {0}")
   @RBComment("Indicates that the view cannot be changed as not all the iterations of the master belong to the same view.")
   public static final String ITERATIONS_NOT_IN_SAME_VIEW = "22";

   @RBEntry("Cannot create new branch:  \"{0}\" is neither the same as, a sibling of, nor a child of \"{1}\"")
   @RBComment("Indicates that the view the user is trying to branch to is not the same as, a sibling of, nor a child of the view the user is trying to branch from.")
   public static final String INVALID_VIEW_FOR_NEWVIEWVARIATION = "24";

   @RBEntry("Cannot update object:  \"{0}\" is neither the same as, a sibling of, nor a child of \"{1}\"")
   @RBComment("Indicates that the view the user is trying to update to is not the same as, a sibling of, nor a child of the view of the predecessor version.")
   public static final String INVALID_VIEW_FOR_UPDATEVIEWVARIATION = "25";

   @RBEntry("View variations are disabled.")
   @RBComment("Used as error message when actions requiring view variations are attempted when the preference is disabled.")
   public static final String VIEW_VARIATIONS_DISABLED = "26";

   @RBEntry("View variations are not supported when the property wt.vc.views.newViewVersionClassicBehavior is false.")
   @RBComment("Used as error message when actions requiring view variations are attempted when the property wt.vc.views.newViewVersionClassicBehavior is false.")
   public static final String VIEW_VARIATIONS_NOT_SUPPORTED = "27";

   @RBEntry("The limit of {0} legal values has been exceeded for the variation {1}.")
   @RBComment("Used as error message when it is found that a variation has more values than is allowed.")
   public static final String VARIATION_VALUE_LIMIT_EXCEEDED = "28";
   
   @RBEntry("The object {0} can not be moved to context {1} because they have different view level modes.")
   @RBComment("Error message when the user attempts to move an object between containers that have different view level modes.")
   public static final String VIEW_MODE_DIFFERENT = "29";

}
