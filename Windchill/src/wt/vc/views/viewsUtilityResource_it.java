/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.vc.views;

import wt.util.resource.*;

@RBUUID("wt.vc.views.viewsUtilityResource")
public final class viewsUtilityResource_it extends WTListResourceBundle {
   @RBEntry("Definisce o visualizza un valore di variazione impostato per una specifica organizzazione.")
   @RBComment("Used in the help as a brief description of the purpose of the tool.")
   public static final String DEFINE_VARIATION_TOOL_DESC = "DEFINE_VARIATION_TOOL_DESC";

   @RBEntry("Uso: {0} [-help] [-variation1] [-variation2] [-org <nome organizzazione>] [-includeset <gruppo da includere>] [-excludeset <gruppo da escludere>] [-all]")
   @RBComment("Used in help usage message.  \"java <utility path+name>\" will be substituted for {0}.")
   public static final String DEFINE_VARIATION_TOOL_USAGE = "DEFINE_VARIATION_TOOL_USAGE";

   @RBEntry("-help")
   public static final String HELP_ARG = "HELP_ARG";

   @RBEntry("Mostra questo messaggio informativo ed esce.")
   @RBComment("Used to describe -help argument.")
   public static final String HELP_ARG_DESC = "HELP_ARG_DESC";

   @RBEntry("-variation1")
   public static final String VARIATION1_ARG = "VARIATION1_ARG";

   @RBEntry("Definisce o visualizza il valore Variation1 impostato per l'organizzazione. Non è possibile utilizzarlo assieme a -variation2. È richiesto un valore per -variation1 o -variation2.")
   @RBComment("Used to describe the -variation1 argument.")
   public static final String VARIATION1_ARG_DESC = "VARIATION1_ARG_DESC";

   @RBEntry("-variation2")
   public static final String VARIATION2_ARG = "VARIATION2_ARG";

   @RBEntry("Definisce o visualizza il valore Variation2 impostato per l'organizzazione. Non è possibile utilizzarlo assieme a -variation1. È richiesto un valore per -variation1 o -variation2.")
   @RBComment("Used to describe the -variation2 argument.")
   public static final String VARIATION2_ARG_DESC = "VARIATION2_ARG_DESC";

   @RBEntry("-org")
   public static final String ORG_ARG = "ORG_ARG";

   @RBEntry("Nome dell'organizzazione di cui definire o impostare il valore di variazione. Il parametro è richiesto.")
   @RBComment("Used to describe the -org argument.")
   public static final String ORG_ARG_DESC = "ORG_ARG_DESC";

   @RBEntry("-includeset")
   public static final String INCLUDE_SET_ARG = "INCLUDE_SET_ARG";

   @RBEntry("Lista di nomi interni separati da virgole dei valori di variazione da includere nel gruppo di valori di variazione per l'organizzazione. Non può essere utilizzato assieme a -excludeset o -all. Se non si specifica -includeset, -excludeset o -all, viene visualizzato il gruppo di valori corrente.")
   @RBComment("Used to describe the -includeset argument.")
   public static final String INCLUDE_SET_ARG_DESC = "INCLUDE_SET_ARG_DESC";

   @RBEntry("-excludeset")
   public static final String EXCLUDE_SET_ARG = "EXCLUDE_SET_ARG";

   @RBEntry("Lista di nomi interni separati da virgole dei valori di variazione da escludere dal gruppo di valori di variazione per l'organizzazione. Non può essere utilizzato assieme a -excludeset o -all. Se non si specifica -includeset, -excludeset o -all, viene visualizzato il gruppo di valori corrente.")
   @RBComment("Used to describe the -excludeset argument.")
   public static final String EXCLUDE_SET_ARG_DESC = "EXCLUDE_SET_ARG_DESC";

   @RBEntry("-all")
   public static final String ALL_ARG = "ALL_ARG";

   @RBEntry("Utilizza l'intero gruppo di valori per la variazione specificata per l'organizzazione. Non può essere utilizzato assieme a -includeset o -excludeset. Se non si specifica -includeset, -excludeset o -all, viene visualizzato il gruppo di valori corrente.")
   @RBComment("Used to describe the -all argument.")
   public static final String ALL_ARG_DESC = "ALL_ARG_DESC";

   @RBEntry("Accedere al sito come amministratore.")
   @RBComment("Used to prompt user to login as site administrator.")
   public static final String PLEASE_LOGIN = "PLEASE_LOGIN";

   @RBEntry("Errore: accesso non valido.")
   @RBComment("Used to indicate that the user logged in as an invalid user.")
   public static final String ERROR_INVALID_LOGIN = "ERROR_INVALID_LOGIN";

   @RBEntry("Errore: è necessario fornire il gruppo da includere dopo -includeset.")
   @RBComment("Used to indicate that an include set string was not specified after -includeset.")
   public static final String ERROR_INCLUDE_SET_SYNTAX = "ERROR_INCLUDE_SET_SYNTAX";

   @RBEntry("Errore: è necessario fornire il gruppo da escludere dopo -excludeset.")
   @RBComment("Used to indicate that an exclude set string was not specified after -includeset.")
   public static final String ERROR_EXCLUDE_SET_SYNTAX = "ERROR_EXCLUDE_SET_SYNTAX";

   @RBEntry("Errore: è necessario fornire il nome organizzazione dopo -org.")
   @RBComment("Used to indicate that an organization name was not specified after -org.")
   public static final String ERROR_ORG_SYNTAX = "ERROR_ORG_SYNTAX";

   @RBEntry("Errore: il nome '{0}' specificato non è un nome di organizzazione valido.")
   @RBComment("Used to indicate that the given organization name is invalid: no organization exists with the name.")
   public static final String ERROR_INVALID_ORG = "ERROR_INVALID_ORG";

   @RBEntry("Errore: argomento {0} sconosciuto.")
   @RBComment("Used to indicate an unrecognized command-line argument was encountered.")
   public static final String ERROR_UNKNOWN_ARG = "ERROR_UNKNOWN_ARG";

   @RBEntry("Errore: è necessario specificare -variation1 o -variation2.")
   @RBComment("Used to indicate an error that neither -variation1 nor -variation2 was found in the command-line arguments.")
   public static final String ERROR_ONE_VARIATION_REQUIRED = "ERROR_ONE_VARIATION_REQUIRED";

   @RBEntry("Errore: impossibile specificare -variation1 assieme a -variation2.")
   @RBComment("Used to indicate that both -variation1 and -variation2 were found in the command-line arguments.")
   public static final String ERROR_VARIATION_CONFLICT = "ERROR_VARIATION_CONFLICT";

   @RBEntry("Errore: è necessario specificare -org.")
   @RBComment("Used to indicate that -org was not found in the command-line arguments. ")
   public static final String ERROR_ORG_REQUIRED = "ERROR_ORG_REQUIRED";

   @RBEntry("Errore: impossibile specificare assieme i gli argomenti -includeset, -excludeset o -all.")
   @RBComment("Used to indicate a conflict of \"value set\" args, including -includeset, -excludeset and -all.")
   public static final String ERROR_VALUE_SET_CONFLICT = "ERROR_VALUE_SET_CONFLICT";

   @RBEntry("Organizzazione contesto: '{0}'.")
   @RBComment("Used to display the name of the organization.")
   public static final String CONTEXT_ORG = "CONTEXT_ORG";

   @RBEntry("Gruppo valori corrente per {0}:")
   @RBComment("Used to display the current value set for a variation.")
   public static final String CURRENT_VARIATION_VALUE_SET = "CURRENT_VARIATION_VALUE_SET";

   @RBEntry("Gruppo valori modificato per {0}:")
   @RBComment("Used to display the modified value set for a variation.")
   public static final String MODIFIED_VARIATION_VALUE_SET = "MODIFIED_VARIATION_VALUE_SET";

   @RBEntry("Errore: valori non validi nel gruppo da includere '{0}'. I valori validi comprendono '{1}'.")
   @RBComment("Used to indicate that invalid variation values were specified in the include set.")
   public static final String ERROR_INVALID_INCLUDE_SET = "ERROR_INVALID_INCLUDE_SET";

   @RBEntry("Errore: valori non validi nel gruppo da escludere '{0}'. I valori validi comprendono '{1}'.")
   @RBComment("Used to indicate that invalid variation values were specified in the exclude set.")
   public static final String ERROR_INVALID_EXCLUDE_SET = "ERROR_INVALID_EXCLUDE_SET";
}
