/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.vc.views;

import wt.util.resource.*;

@RBUUID("wt.vc.views.viewsUtilityResource")
public final class viewsUtilityResource extends WTListResourceBundle {
   @RBEntry("Define or view a variation value set for a specific organization.")
   @RBComment("Used in the help as a brief description of the purpose of the tool.")
   public static final String DEFINE_VARIATION_TOOL_DESC = "DEFINE_VARIATION_TOOL_DESC";

   @RBEntry("Usage: {0} [-help] [-variation1] [-variation2] [-org <organization name>] [-includeset <include set>] [-excludeset <exclude set>] [-all]")
   @RBComment("Used in help usage message.  \"java <utility path+name>\" will be substituted for {0}.")
   public static final String DEFINE_VARIATION_TOOL_USAGE = "DEFINE_VARIATION_TOOL_USAGE";

   @RBEntry("-help")
   public static final String HELP_ARG = "HELP_ARG";

   @RBEntry("Show this help and terminate.")
   @RBComment("Used to describe -help argument.")
   public static final String HELP_ARG_DESC = "HELP_ARG_DESC";

   @RBEntry("-variation1")
   public static final String VARIATION1_ARG = "VARIATION1_ARG";

   @RBEntry("Define or view the Variation1 value set for the organization. Cannot be used in conjunction with -variation2. One of -variation1 or -variation2 is required.")
   @RBComment("Used to describe the -variation1 argument.")
   public static final String VARIATION1_ARG_DESC = "VARIATION1_ARG_DESC";

   @RBEntry("-variation2")
   public static final String VARIATION2_ARG = "VARIATION2_ARG";

   @RBEntry("Define or view the Variation2 value set for the organization. Cannot be used in conjunction with -variation1. One of -variation1 or -variation2 is required.")
   @RBComment("Used to describe the -variation2 argument.")
   public static final String VARIATION2_ARG_DESC = "VARIATION2_ARG_DESC";

   @RBEntry("-org")
   public static final String ORG_ARG = "ORG_ARG";

   @RBEntry("Name of the organization whose variation value set is to be defined or viewed. This is required.")
   @RBComment("Used to describe the -org argument.")
   public static final String ORG_ARG_DESC = "ORG_ARG_DESC";

   @RBEntry("-includeset")
   public static final String INCLUDE_SET_ARG = "INCLUDE_SET_ARG";

   @RBEntry("Comma separated list of variation value internal names that are to be included in the value set of the variation for this organization. Cannot be used in conjunction with -excludeset or -all. If none of -includeset, -excludeset or -all are specified the current value set will be displayed for viewing.")
   @RBComment("Used to describe the -includeset argument.")
   public static final String INCLUDE_SET_ARG_DESC = "INCLUDE_SET_ARG_DESC";

   @RBEntry("-excludeset")
   public static final String EXCLUDE_SET_ARG = "EXCLUDE_SET_ARG";

   @RBEntry("Comma separated list of variation value internal names that are to be excluded in the value set of the variation for this organization. Cannot be used in conjunction with -includeset or -all. If none of -includeset, -excludeset or -all are specified the current value set will be displayed for viewing.")
   @RBComment("Used to describe the -excludeset argument.")
   public static final String EXCLUDE_SET_ARG_DESC = "EXCLUDE_SET_ARG_DESC";

   @RBEntry("-all")
   public static final String ALL_ARG = "ALL_ARG";

   @RBEntry("Use the entire value set of the given variation for this organization. Cannot be used in conjunction with -includeset or -excludeset. If none of -includeset, -excludeset or -all are specified the current value set will be displayed for viewing.")
   @RBComment("Used to describe the -all argument.")
   public static final String ALL_ARG_DESC = "ALL_ARG_DESC";

   @RBEntry("Please log in as a site administrator.")
   @RBComment("Used to prompt user to login as site administrator.")
   public static final String PLEASE_LOGIN = "PLEASE_LOGIN";

   @RBEntry("Error: invalid log in.")
   @RBComment("Used to indicate that the user logged in as an invalid user.")
   public static final String ERROR_INVALID_LOGIN = "ERROR_INVALID_LOGIN";

   @RBEntry("Error: include set must be given after -includeset.")
   @RBComment("Used to indicate that an include set string was not specified after -includeset.")
   public static final String ERROR_INCLUDE_SET_SYNTAX = "ERROR_INCLUDE_SET_SYNTAX";

   @RBEntry("Error: exclude set must be given after -excludeset.")
   @RBComment("Used to indicate that an exclude set string was not specified after -includeset.")
   public static final String ERROR_EXCLUDE_SET_SYNTAX = "ERROR_EXCLUDE_SET_SYNTAX";

   @RBEntry("Error: organization name must be given after -org.")
   @RBComment("Used to indicate that an organization name was not specified after -org.")
   public static final String ERROR_ORG_SYNTAX = "ERROR_ORG_SYNTAX";

   @RBEntry("Error: the given organization name '{0}' is not a valid organization name.")
   @RBComment("Used to indicate that the given organization name is invalid: no organization exists with the name.")
   public static final String ERROR_INVALID_ORG = "ERROR_INVALID_ORG";

   @RBEntry("Error: unknown argument {0}.")
   @RBComment("Used to indicate an unrecognized command-line argument was encountered.")
   public static final String ERROR_UNKNOWN_ARG = "ERROR_UNKNOWN_ARG";

   @RBEntry("Error: one of -variation1 or -variation2 must be specified.")
   @RBComment("Used to indicate an error that neither -variation1 nor -variation2 was found in the command-line arguments.")
   public static final String ERROR_ONE_VARIATION_REQUIRED = "ERROR_ONE_VARIATION_REQUIRED";

   @RBEntry("Error: -variation1 cannot be specified in conjunction with -variation2.")
   @RBComment("Used to indicate that both -variation1 and -variation2 were found in the command-line arguments.")
   public static final String ERROR_VARIATION_CONFLICT = "ERROR_VARIATION_CONFLICT";

   @RBEntry("Error: -org must be specified.")
   @RBComment("Used to indicate that -org was not found in the command-line arguments. ")
   public static final String ERROR_ORG_REQUIRED = "ERROR_ORG_REQUIRED";

   @RBEntry("Error: cannot specify -includeset, -excludeset or -all in conjunction.")
   @RBComment("Used to indicate a conflict of \"value set\" args, including -includeset, -excludeset and -all.")
   public static final String ERROR_VALUE_SET_CONFLICT = "ERROR_VALUE_SET_CONFLICT";

   @RBEntry("Context organization: '{0}'.")
   @RBComment("Used to display the name of the organization.")
   public static final String CONTEXT_ORG = "CONTEXT_ORG";

   @RBEntry("Current Value Set for {0}:")
   @RBComment("Used to display the current value set for a variation.")
   public static final String CURRENT_VARIATION_VALUE_SET = "CURRENT_VARIATION_VALUE_SET";

   @RBEntry("Modified Value Set for {0}:")
   @RBComment("Used to display the modified value set for a variation.")
   public static final String MODIFIED_VARIATION_VALUE_SET = "MODIFIED_VARIATION_VALUE_SET";

   @RBEntry("Error: invalid values in include set '{0}'. Valid values include '{1}'.")
   @RBComment("Used to indicate that invalid variation values were specified in the include set.")
   public static final String ERROR_INVALID_INCLUDE_SET = "ERROR_INVALID_INCLUDE_SET";

   @RBEntry("Error: invalid values in exclude set '{0}'. Valid values include '{1}'.")
   @RBComment("Used to indicate that invalid variation values were specified in the exclude set.")
   public static final String ERROR_INVALID_EXCLUDE_SET = "ERROR_INVALID_EXCLUDE_SET";
}
