/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.vc.views;

import wt.util.resource.*;

@RBUUID("wt.vc.views.viewClientResource")
public final class viewClientResource_it extends WTListResourceBundle {
   @RBEntry("[V]isualizza struttura\n[C]rea vista\n[R]inomina vista\nE[L]imina vista\n[A]ssocia viste\n[I]nserisci vista\n[E]sci\nScelta:  ")
   public static final String MENU = "0";

   @RBEntry("V")
   public static final String V = "1";

   @RBEntry("C")
   public static final String C = "2";

   @RBEntry("R")
   public static final String R = "3";

   @RBEntry("A")
   public static final String A = "4";

   @RBEntry("I")
   public static final String I = "5";

   @RBEntry("E")
   public static final String Q = "6";

   @RBEntry("Immettere il nome della vista:")
   public static final String ENTER_VIEW_NAME = "7";

   @RBEntry("Immettere il nuovo nome della vista:")
   public static final String ENTER_NEW_VIEW_NAME = "8";

   @RBEntry("Immettere il nome della vista padre:")
   public static final String ENTER_PARENT_NAME = "9";

   @RBEntry("Immettere il nome della vista figlio:")
   public static final String ENTER_CHILD_NAME = "10";

   @RBEntry("Immettere il nome della vista da inserire:")
   public static final String ENTER_INSERT_NAME = "11";

   @RBEntry("Crea vista \"{0}\"")
   public static final String CREATE_VIEW = "12";

   @RBEntry("Rinomina vista da \"{0}\" a \"{1}\"")
   public static final String RENAME_VIEW = "13";

   @RBEntry("Crea associazione tra \"{0}\" e \"{1}\"")
   public static final String CREATE_ASSOCIATION = "14";

   @RBEntry("Inserisci \"{0}\" tra \"{1}\" e \"{2}\"")
   public static final String INSERT_ASSOCIATION = "15";

   @RBEntry("OK")
   public static final String OK = "16";

   @RBEntry("NON RIUSCITO")
   public static final String FAILED = "17";

   @RBEntry("Comando non riconosciuto")
   public static final String UNRECOGNIZED = "18";

   @RBEntry("Nota:  lasciare vuota la vista figlio da inserire tra la vista padre e tutte le viste figlio")
   public static final String INSERT_NOTE = "19";

   @RBEntry("L")
   public static final String D = "20";

   @RBEntry("Eliminare la vista \"{0}\"")
   public static final String DELETE_VIEW = "21";
}
