/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.fedInfra;

import wt.util.resource.*;

@RBUUID("wt.fedInfra.fedInfraResource")
public final class fedInfraResource_it extends WTListResourceBundle {
    /**
     * Entry
     **/
    @RBEntry("Alcuni oggetti sono già locali nel repository corrente.")
    public static final String OBJECTS_ALREADY_LOCAL = "1";

    @RBEntry("Un link di stato Federatable è già associato ad alcuni oggetti.")
    public static final String OBJECTS_ALREADY_HAVE_STATUS_LINK = "2";

    @RBEntry("Alcuni oggetti hanno il link di stato Federatable associato a un repository diverso.")
    public static final String OBJECTS_ALREADY_HAVE_STATUS_LINK_IN_DIFFERENT_REPOSITORY = "3";

    @RBEntry("Alcuni oggetti non hanno associato un link di stato Federatable di accettazione.")
    public static final String OBJECTS_DO_NOT_HAVE_STATUS_LINK_ACCEPT = "4";

    @RBEntry("Impossibile accettare il diritto di modifica. Alcuni oggetti non sono l'ultima iterazione.")
    public static final String OBJECTS_NOT_LATEST_ITERATION_TO_ACCEPT = "5";

    @RBEntry("Impossibile accettare il diritto di modifica. Per alcuni oggetti l'ultimo repository noto è diverso da quello che ha concesso il diritto di modifica.")
    public static final String OBJECTS_LAST_REPOSITORY_DIFFERENT = "6";

    @RBEntry("Alcuni oggetti sono già sottoposti a un'operazione di ambiente federato.")
    public static final String OBJECTS_ALREADY_UNDERGOING_FEDERATION_ACTION = "7";

    @RBEntry("Azione di accettazione del diritto di modifica richiesta nuovamente per gli oggetti")
    public static final String INFO_OBJECTS_ACCEPT_RIGHT_TO_MODIFY_REQUESTED_AGAIN = "8";

    @RBEntry("Convalida dell'eliminazione non riuscita.")
    public static final String FEDERATED_DELETE_VALIDATION_FAILED = "9";

    @RBEntry("Impossibile completare l'azione di eliminazione federata.")
    public static final String FEDERATED_DELETE_ACTION_FAILED = "10";

    @RBEntry("Operazione parallela eseguita sugli oggetti.")
    public static final String PARALLEL_OPERATIONS_ON_OBJECTS = "11";

    @RBEntry("Alcuni oggetti sono già remoti.")
    public static final String OBJECTS_ALREADY_REMOTE = "12";

    @RBEntry("Alcuni oggetti non hanno associato un link di stato Federatable di concessione.")
    public static final String OBJECTS_DO_NOT_HAVE_STATUS_LINK_GRANT = "13";

    @RBEntry("Impossibile concedere il diritto di modifica. Alcuni oggetti sono già nella transazione di ambiente federato.")
    public static final String OBJECTS_ALREADY_IN_FEDERATION_TRANSACTION = "14";

    @RBEntry("impossibile concedere il diritto di modifica. Alcuni degli oggetti iterati non sono l'ultima iterazione della versione.")
    public static final String OBJECTS_NOT_LATEST_ITERATION_TO_GRANT = "15";

    @RBEntry("Impossibile concedere il diritto di modifica. Alcuni oggetti remoti non dispongono di diritti di modifica.")
    public static final String REMOTE_OBJECTS_DO_NOT_HAVE_MODIFY_RIGHTS = "16";

    @RBEntry("Alcuni oggetti dispongono del diritto di modifica per altri repository.")
    public static final String OBJECTS_HAVE_RIGHT_TO_MODIFY_FOR_OTHER_REPOSITORY = "17";

    @RBEntry("L'ultima o la stessa iterazione dell'oggetto è già stata esportata.")
    public static final String OBJECT_ALREADY_EXPORTED = "18";

    @RBEntry("Alcuni oggetti sono di un repository di origine diverso.")
    public static final String OBJECTS_DIFFERENT_SOURCE_REPOSITORY = "19";

    @RBEntry("A tutti gli oggetti non è associato lo stesso link di stato Federatable.")
    public static final String OBJECTS_DO_NOT_HAVE_SAME_FEDERATABLE_STATUS_LINK = "20";

    @RBEntry("Ad alcuni oggetti non è associato un link di stato Federatable di concessione o accettazione.")
    public static final String OBJECTS_DO_NOT_HAVE_STATUS_LINK_GRANT_ACCEPT = "21";

    @RBEntry("Alcuni oggetti con il diritto originale di modifica sono presenti nell'operazione di eliminazione.")
    public static final String OBJECTS_WITH_ORIGNAL_RIGHT_TO_MODIFY_PRESENT_IN_DELETE_OPERATION = "22";

    @RBEntry("Repository di origine e di destinazione non impostati correttamente.")
    public static final String SOURCE_AND_TARGET_REPOSITORIES_NOT_SET_CORRECTLY = "23";

    @RBEntry("Richiesta azione non relativa al repository locale.")
    public static final String ACTION_REQUEST_NOT_FOR_LOCAL_REPOSITORY = "24";

    @RBEntry("Richiesta azione non valida.")
    public static final String INVALID_ACTION_REQUESTED = "25";

    @RBEntry("Impossibile importare oggetti precedenti.")
    public static final String CAN_NOT_IMPORT_OLDER_OBJECTS = "26";

    @RBEntry("Database is inconsistent;Master exists,but there is no version for the object.")
    @RBPseudo(false)
    @RBComment("DO NOT TRANSLATE - It is an exception, not visible to end user")
    public static final String MASTER_EXISTS_BUT_NO_VERSION_ITERATION_FOR_OBJECT = "27";

    @RBEntry("L'oggetto è bloccato.")
    public static final String OBJECT_LOCKED = "28";

    @RBEntry("Serie versione nulla.")
    public static final String VERSION_SERIES_NULL = "29";

    @RBEntry("L'oggetto non esiste per la modifica non in linea.")
    public static final String OBJECT_NOT_EXISTS_FOR_OFFLINE_EDIT = "30";

    @RBEntry("Il contenuto è stato modificato, ma l'oggetto non è stato sottoposto a Check-Out dall'ambiente federato.")
    public static final String CONTENT_MODIFIED_OBJECT_NOT_FEDERATION_CHECKED_OUT = "31";

    @RBEntry("I repository di origine e di destinazione sono identici.")
    public static final String SOURCE_AND_TARGET_REPOSITORIES_SAME = "32";

    @RBEntry("Convalida dell'oggetto non riuscita.")
    public static final String OBJECT_VALIDATION_FAILED = "34";

    @RBEntry("L'utente non dispone dell'autorizzazione necessaria per l'operazione. ")
    public static final String SECURED_ACTION = "35";

    @RBEntry("Alcuni oggetti sono sottoposti a Check-Out.")
    public static final String OBJECTS_CHECKED_OUT = "36";

    @RBEntry("Il repository di origine che effettua la richiesta non esiste nel database.")
    public static final String SOURCE_REPOSITORY_NOT_FOUND = "37";

    @RBEntry("L'oggetto non è più incluso nel contenuto della consegna ricevuta.")
    public static final String LOCATABLE_ABSENT_LOG_MESSAGE = "38";

    @RBEntry("Impossibile individuare uno o più oggetti.")
    public static final String LOCATABLE_ABSENT_FAILED_MESSAGE = "39";

    @RBEntry("L'oggetto è stato eliminato.")
    public static final String LOCATABLE_DELETE_LOG_MESSAGE = "40";

    @RBEntry("Eliminazione non riuscita.")
    public static final String LOCATABLE_DELETE_FAILED_MESSAGE = "41";
}
