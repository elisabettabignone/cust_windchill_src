/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.fedInfra.mapping;

import wt.util.resource.*;

@RBUUID("wt.fedInfra.mapping.federationmappingResource")
public final class federationmappingResource extends WTListResourceBundle {
   /**
    * Entry
    **/
   @RBEntry(" FederationMapping")
   public static final String PRIVATE_CONSTANT_0 = "FEDERATIONMAPPING_CATEGORY";

   @RBEntry("Federation Mappings")
   public static final String PRIVATE_CONSTANT_1 = "FEDERATIONMAPPING_CATEGORY_DESC";

   @RBEntry(" FederationMapping Client")
   public static final String PRIVATE_CONSTANT_2 = "FEDERATIONMAPPING_CLIENT";

   @RBEntry("FederationMapping Client")
   public static final String PRIVATE_CONSTANT_3 = "FEDERATIONMAPPING_CLIENT_DESC";

   @RBEntry(" Lifecycle")
   public static final String PRIVATE_CONSTANT_4 = "LIFECYCLE";

   @RBEntry(" Lifecycle Mappings")
   public static final String PRIVATE_CONSTANT_5 = "LIFECYCLE_SHORT_DESC";

   @RBEntry(" Lifecycle Mappings")
   public static final String PRIVATE_CONSTANT_6 = "LIFECYCLE_LONG_DESC";

   @RBEntry(" Revision")
   public static final String PRIVATE_CONSTANT_7 = "REVISION";

   @RBEntry(" Revision Mappings")
   public static final String PRIVATE_CONSTANT_8 = "REVISION_SHORT_DESC";

   @RBEntry(" Revision Mappings")
   public static final String PRIVATE_CONSTANT_9 = "REVISION_LONG_DESC";

   @RBEntry(" Location")
   public static final String PRIVATE_CONSTANT_10 = "LOCATION";

   @RBEntry(" Location Mappings")
   public static final String PRIVATE_CONSTANT_11 = "LOCATION_SHORT_DESC";

   @RBEntry(" Location Mappings")
   public static final String PRIVATE_CONSTANT_12 = "LOCATION_LONG_DESC";
   
   @RBEntry(" Context")
   public static final String PRIVATE_CONSTANT_13 = "CONTEXT";

   @RBEntry(" Context Mappings")
   public static final String PRIVATE_CONSTANT_14 = "CONTEXT_SHORT_DESC";

   @RBEntry(" Context Mappings")
   public static final String PRIVATE_CONSTANT_15 = "CONTEXT_LONG_DESC";
   
   @RBEntry(" Owning Org")
   public static final String PRIVATE_CONSTANT_16 = "OWNING_ORG";

   @RBEntry(" Owning Org Mappings")
   public static final String PRIVATE_CONSTANT_17 = "OWNING_ORG_SHORT_DESC";

   @RBEntry(" Owning Org Mappings")
   public static final String PRIVATE_CONSTANT_18 = "OWNING_ORG_LONG_DESC";
   
   @RBEntry(" View")
   public static final String PRIVATE_CONSTANT_19 = "VIEW";

   @RBEntry(" View Mappings")
   public static final String PRIVATE_CONSTANT_20 = "VIEW_SHORT_DESC";

   @RBEntry(" View Mappings")
   public static final String PRIVATE_CONSTANT_21 = "VIEW_LONG_DESC";

   @RBEntry(" Folder")
   public static final String PRIVATE_CONSTANT_22 = "FOLDER";

   @RBEntry(" Folder Mappings")
   public static final String PRIVATE_CONSTANT_23 = "FOLDER_SHORT_DESC";

   @RBEntry(" Folder Mappings")
   public static final String PRIVATE_CONSTANT_24 = "FOLDER_LONG_DESC";
   
   @RBEntry(" User")
   public static final String PRIVATE_CONSTANT_25 = "USER";

   @RBEntry(" User Mappings")
   public static final String PRIVATE_CONSTANT_26 = "USER_SHORT_DESC";

   @RBEntry(" User Mappings")
   public static final String PRIVATE_CONSTANT_27 = "USER_LONG_DESC";
   
   @RBEntry(" Security Label")
   public static final String PRIVATE_CONSTANT_28 = "SECURITY_LABEL";

   @RBEntry(" Security Label Mappings")
   public static final String PRIVATE_CONSTANT_29 = "SECURITY_LABEL_SHORT_DESC";

   @RBEntry(" Security Label Mappings")
   public static final String PRIVATE_CONSTANT_30 = "SECURITY_LABEL_LONG_DESC";
   
   @RBEntry("Error while deleting mappings")
   public static final String ERROR_DELETING_MAPPING = "ERROR_DELETING_MAPPING";
   
   @RBEntry("Invalid organization identifier format: \"{0}\".")
   public static final String INVALID_ORG_ID_FORMAT = "invalid_org_id_format";

}
