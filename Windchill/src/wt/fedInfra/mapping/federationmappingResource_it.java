/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.fedInfra.mapping;

import wt.util.resource.*;

@RBUUID("wt.fedInfra.mapping.federationmappingResource")
public final class federationmappingResource_it extends WTListResourceBundle {
   /**
    * Entry
    **/
   @RBEntry(" Mappatura ambiente federato")
   public static final String PRIVATE_CONSTANT_0 = "FEDERATIONMAPPING_CATEGORY";

   @RBEntry("Mappature ambiente federato")
   public static final String PRIVATE_CONSTANT_1 = "FEDERATIONMAPPING_CATEGORY_DESC";

   @RBEntry(" Client mappatura ambiente federato")
   public static final String PRIVATE_CONSTANT_2 = "FEDERATIONMAPPING_CLIENT";

   @RBEntry("Client mappatura ambiente federato")
   public static final String PRIVATE_CONSTANT_3 = "FEDERATIONMAPPING_CLIENT_DESC";

   @RBEntry(" Ciclo di vita")
   public static final String PRIVATE_CONSTANT_4 = "LIFECYCLE";

   @RBEntry(" Mappature ciclo di vita")
   public static final String PRIVATE_CONSTANT_5 = "LIFECYCLE_SHORT_DESC";

   @RBEntry(" Mappature ciclo di vita")
   public static final String PRIVATE_CONSTANT_6 = "LIFECYCLE_LONG_DESC";

   @RBEntry(" Revisione")
   public static final String PRIVATE_CONSTANT_7 = "REVISION";

   @RBEntry(" Mappature revisione")
   public static final String PRIVATE_CONSTANT_8 = "REVISION_SHORT_DESC";

   @RBEntry(" Mappature revisione")
   public static final String PRIVATE_CONSTANT_9 = "REVISION_LONG_DESC";

   @RBEntry(" Posizione")
   public static final String PRIVATE_CONSTANT_10 = "LOCATION";

   @RBEntry(" Mappature posizione")
   public static final String PRIVATE_CONSTANT_11 = "LOCATION_SHORT_DESC";

   @RBEntry(" Mappature posizione")
   public static final String PRIVATE_CONSTANT_12 = "LOCATION_LONG_DESC";
   
   @RBEntry(" Contesto")
   public static final String PRIVATE_CONSTANT_13 = "CONTEXT";

   @RBEntry(" Mappature contesto")
   public static final String PRIVATE_CONSTANT_14 = "CONTEXT_SHORT_DESC";

   @RBEntry(" Mappature contesto")
   public static final String PRIVATE_CONSTANT_15 = "CONTEXT_LONG_DESC";
   
   @RBEntry(" Organizzazione proprietaria")
   public static final String PRIVATE_CONSTANT_16 = "OWNING_ORG";

   @RBEntry(" Mappature organizzazione proprietaria")
   public static final String PRIVATE_CONSTANT_17 = "OWNING_ORG_SHORT_DESC";

   @RBEntry(" Mappature organizzazione proprietaria")
   public static final String PRIVATE_CONSTANT_18 = "OWNING_ORG_LONG_DESC";
   
   @RBEntry(" Vista")
   public static final String PRIVATE_CONSTANT_19 = "VIEW";

   @RBEntry(" Mappature vista")
   public static final String PRIVATE_CONSTANT_20 = "VIEW_SHORT_DESC";

   @RBEntry(" Mappature vista")
   public static final String PRIVATE_CONSTANT_21 = "VIEW_LONG_DESC";

   @RBEntry(" Cartella")
   public static final String PRIVATE_CONSTANT_22 = "FOLDER";

   @RBEntry(" Mappature cartella")
   public static final String PRIVATE_CONSTANT_23 = "FOLDER_SHORT_DESC";

   @RBEntry(" Mappature cartella")
   public static final String PRIVATE_CONSTANT_24 = "FOLDER_LONG_DESC";
   
   @RBEntry(" Utente")
   public static final String PRIVATE_CONSTANT_25 = "USER";

   @RBEntry(" Mappature utente")
   public static final String PRIVATE_CONSTANT_26 = "USER_SHORT_DESC";

   @RBEntry(" Mappature utente")
   public static final String PRIVATE_CONSTANT_27 = "USER_LONG_DESC";
   
   @RBEntry(" Etichetta di sicurezza")
   public static final String PRIVATE_CONSTANT_28 = "SECURITY_LABEL";

   @RBEntry(" Mappature etichetta di sicurezza")
   public static final String PRIVATE_CONSTANT_29 = "SECURITY_LABEL_SHORT_DESC";

   @RBEntry(" Mappature etichetta di sicurezza")
   public static final String PRIVATE_CONSTANT_30 = "SECURITY_LABEL_LONG_DESC";
   
   @RBEntry("Errore durante l'eliminazione delle mappature")
   public static final String ERROR_DELETING_MAPPING = "ERROR_DELETING_MAPPING";
   
   @RBEntry("Formato di identificatore organizzazione non valido: \"{0}\"")
   public static final String INVALID_ORG_ID_FORMAT = "invalid_org_id_format";

}
