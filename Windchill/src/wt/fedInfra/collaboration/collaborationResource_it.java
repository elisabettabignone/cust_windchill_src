/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.fedInfra.collaboration;

import wt.util.resource.*;

@RBUUID("wt.fedInfra.collaboration.collaborationResource")
public final class collaborationResource_it extends WTListResourceBundle {
   @RBEntry("Esiste già un altro sito di collaborazione con lo stesso nome.")
   public static final String DUPLICATE_SITE_NAME_ERR = "0";

   @RBEntry("Uno o più parametri repository mancanti.")
   public static final String REPOSITORY_INFO_MISSING = "1";

   @RBEntry("Informazioni utente mancanti.")
   public static final String USER_INFO_MISSING = "2";

   @RBEntry("Informazioni collaborazione")
   public static final String CollaborationInfo = "3";
}
