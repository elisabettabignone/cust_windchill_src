/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.fedInfra.collaboration;

import wt.util.resource.*;

@RBUUID("wt.fedInfra.collaboration.collaborationResource")
public final class collaborationResource extends WTListResourceBundle {
   @RBEntry("Another collaboration site with the same name already exists.")
   public static final String DUPLICATE_SITE_NAME_ERR = "0";

   @RBEntry("One or more repository parameters are missing.")
   public static final String REPOSITORY_INFO_MISSING = "1";

   @RBEntry("User information is missing.")
   public static final String USER_INFO_MISSING = "2";

   @RBEntry("Collaboration Information")
   public static final String CollaborationInfo = "3";
}
