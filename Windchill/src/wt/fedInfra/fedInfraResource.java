/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.fedInfra;

import wt.util.resource.*;

@RBUUID("wt.fedInfra.fedInfraResource")
public final class fedInfraResource extends WTListResourceBundle {
    /**
     * Entry
     **/
    @RBEntry("Some objects are already local in current repository.")
    public static final String OBJECTS_ALREADY_LOCAL = "1";

    @RBEntry("Some objects already have a Federatable Status Link associated with them.")
    public static final String OBJECTS_ALREADY_HAVE_STATUS_LINK = "2";

    @RBEntry("Some objects have a Federatable Status Link associated to a different repository.")
    public static final String OBJECTS_ALREADY_HAVE_STATUS_LINK_IN_DIFFERENT_REPOSITORY = "3";

    @RBEntry("Some objects do not have Federatable Status Link of accept.")
    public static final String OBJECTS_DO_NOT_HAVE_STATUS_LINK_ACCEPT = "4";

    @RBEntry("Cannot accept right to modify because some objects are not latest iteration.")
    public static final String OBJECTS_NOT_LATEST_ITERATION_TO_ACCEPT = "5";

    @RBEntry("Cannot accept right to modify because, for some objects, the last known repository is different than the one that granted the right to modify.")
    public static final String OBJECTS_LAST_REPOSITORY_DIFFERENT = "6";

    @RBEntry("Some objects are already undergoing a federation operation.")
    public static final String OBJECTS_ALREADY_UNDERGOING_FEDERATION_ACTION = "7";

    @RBEntry("Accept right to modify re-requested for objects.")
    public static final String INFO_OBJECTS_ACCEPT_RIGHT_TO_MODIFY_REQUESTED_AGAIN = "8";

    @RBEntry("Validation for delete failed.")
    public static final String FEDERATED_DELETE_VALIDATION_FAILED = "9";

    @RBEntry("Could not complete federated delete action.")
    public static final String FEDERATED_DELETE_ACTION_FAILED = "10";

    @RBEntry("Parallel operation happening on objects.")
    public static final String PARALLEL_OPERATIONS_ON_OBJECTS = "11";

    @RBEntry("Some objects are already remote.")
    public static final String OBJECTS_ALREADY_REMOTE = "12";

    @RBEntry("Some objects do not have a federatable status link of grant.")
    public static final String OBJECTS_DO_NOT_HAVE_STATUS_LINK_GRANT = "13";

    @RBEntry("Cannot grant right to modify because some objects are already in federation transaction.")
    public static final String OBJECTS_ALREADY_IN_FEDERATION_TRANSACTION = "14";

    @RBEntry("Cannot grant right to modify because some of iterated objects are not latest iteration of the version.")
    public static final String OBJECTS_NOT_LATEST_ITERATION_TO_GRANT = "15";

    @RBEntry("Cannot grant right to modify because some remote objects do not have modify rights.")
    public static final String REMOTE_OBJECTS_DO_NOT_HAVE_MODIFY_RIGHTS = "16";

    @RBEntry("Some objects have right to modify for some other repository.")
    public static final String OBJECTS_HAVE_RIGHT_TO_MODIFY_FOR_OTHER_REPOSITORY = "17";

    @RBEntry("Latter or same iteration of object is already exported.")
    public static final String OBJECT_ALREADY_EXPORTED = "18";

    @RBEntry("Some objects are of a different source repository.")
    public static final String OBJECTS_DIFFERENT_SOURCE_REPOSITORY = "19";

    @RBEntry("All objects do not have same federatable status link.")
    public static final String OBJECTS_DO_NOT_HAVE_SAME_FEDERATABLE_STATUS_LINK = "20";

    @RBEntry("Some objects do not have federatable status link of grant or accept.")
    public static final String OBJECTS_DO_NOT_HAVE_STATUS_LINK_GRANT_ACCEPT = "21";

    @RBEntry("Some objects with original right to modify present in delete operation.")
    public static final String OBJECTS_WITH_ORIGNAL_RIGHT_TO_MODIFY_PRESENT_IN_DELETE_OPERATION = "22";

    @RBEntry("Source and target repositories not correctly set.")
    public static final String SOURCE_AND_TARGET_REPOSITORIES_NOT_SET_CORRECTLY = "23";

    @RBEntry("Action request not for local repository.")
    public static final String ACTION_REQUEST_NOT_FOR_LOCAL_REPOSITORY = "24";

    @RBEntry("Invalid action requested.")
    public static final String INVALID_ACTION_REQUESTED = "25";

    @RBEntry("Cannot import older object.")
    public static final String CAN_NOT_IMPORT_OLDER_OBJECTS = "26";

    @RBEntry("Database is inconsistent;Master exists,but there is no version for the object.")
    @RBPseudo(false)
    @RBComment("DO NOT TRANSLATE - It is an exception, not visible to end user")
    public static final String MASTER_EXISTS_BUT_NO_VERSION_ITERATION_FOR_OBJECT = "27";

    @RBEntry("Object is locked.")
    public static final String OBJECT_LOCKED = "28";

    @RBEntry("Version series is null.")
    public static final String VERSION_SERIES_NULL = "29";

    @RBEntry("Object does not exist for offline edit case.")
    public static final String OBJECT_NOT_EXISTS_FOR_OFFLINE_EDIT = "30";

    @RBEntry("Content was modified, but object is not federation checked out.")
    public static final String CONTENT_MODIFIED_OBJECT_NOT_FEDERATION_CHECKED_OUT = "31";

    @RBEntry("Source and target repositories are same.")
    public static final String SOURCE_AND_TARGET_REPOSITORIES_SAME = "32";

    @RBEntry("Object validation failed.")
    public static final String OBJECT_VALIDATION_FAILED = "34";

    @RBEntry("You do not have the necessary authorization for this operation. ")
    public static final String SECURED_ACTION = "35";

    @RBEntry("Some objects are checked out.")
    public static final String OBJECTS_CHECKED_OUT = "36";

    @RBEntry("The requesting source repository does not exist in the database.")
    public static final String SOURCE_REPOSITORY_NOT_FOUND = "37";

    @RBEntry("The  object is no longer included in the received delivery content.")
    public static final String LOCATABLE_ABSENT_LOG_MESSAGE = "38";

    @RBEntry("One or more objects could not be located.")
    public static final String LOCATABLE_ABSENT_FAILED_MESSAGE = "39";

    @RBEntry("The object has been deleted.")
    public static final String LOCATABLE_DELETE_LOG_MESSAGE = "40";

    @RBEntry("Delete action failed.")
    public static final String LOCATABLE_DELETE_FAILED_MESSAGE = "41";
}
