package wt.meta;

import wt.util.resource.*;

public final class metaResource_it extends WTListResourceBundle {
   @RBEntry("Messaggio test con argomentot: \"{0}\".")
   @RBArgComment0("  {0} don't worry, any translation will do. thanks, mbm")
   public static final String TEST_MSG = "11";

   @RBEntry("Non esiste alcun tipo enumerativo con \"{0}\" come valore interno.")
   @RBArgComment0("  {0} internal (persistent) name (key)")
   public static final String INVALID_ENUM_VALUE = "25";
}
