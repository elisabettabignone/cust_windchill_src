/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.meta;

import wt.util.resource.*;

@RBUUID("wt.meta.metaResource")
public final class metaResource extends WTListResourceBundle {
   @RBEntry("Test message with argument: \"{0}\".")
   @RBArgComment0("  {0} don't worry, any translation will do. thanks, mbm")
   public static final String TEST_MSG = "11";

   @RBEntry("No EnumeratedType exists with \"{0}\" as its internal value.")
   @RBArgComment0("  {0} internal (persistent) name (key)")
   public static final String INVALID_ENUM_VALUE = "25";

   @RBEntry("No EnumeratedType exists with \"{0}\" as its constant name.")
   @RBArgComment0("  {0} constant field name")
   public static final String INVALID_ENUM_CONSTANT = "26";
}
