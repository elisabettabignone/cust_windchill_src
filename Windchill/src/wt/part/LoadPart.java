/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.part;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.vecmath.Matrix4d;
import javax.vecmath.Vector3d;

import org.apache.log4j.Logger;

import wt.access.AccessControlServerHelper;
import wt.access.SecurityLabeled;
import wt.configuration.TraceCode;
import wt.doc.LoadDoc;
import wt.doc.WTDocument;
import wt.doc.WTDocumentMaster;
import wt.eff.EffHelper;
import wt.eff.EffTypeModifier;
import wt.eff.form.EffFormHelper;
import wt.fc.ObjectReference;
import wt.fc.Persistable;
import wt.fc.PersistenceHelper;
import wt.fc.PersistenceServerHelper;
import wt.fc.QueryResult;
import wt.fc.ReferenceFactory;
import wt.fc.collections.CollectionsHelper;
import wt.fc.collections.WTArrayList;
import wt.fc.collections.WTCollection;
import wt.fc.collections.WTHashSet;
import wt.fc.collections.WTKeyedMap;
import wt.fc.collections.WTSet;
import wt.filter.NavigationFilterHelper;
import wt.folder.Folder;
import wt.folder.FolderEntry;
import wt.folder.FolderHelper;
import wt.folder.FolderNotFoundException;
import wt.generic.GenericType;
import wt.iba.value.AttributeContainer;
import wt.iba.value.DefaultAttributeContainer;
import wt.iba.value.service.IBAValueHelper;
import wt.iba.value.service.LoadValue;
import wt.identity.IdentityFactory;
import wt.inf.container.LookupSpec;
import wt.inf.container.OrgContainer;
import wt.inf.container.WTContained;
import wt.inf.container.WTContainer;
import wt.inf.container.WTContainerHelper;
import wt.inf.container.WTContainerRef;
import wt.inf.container.WTContainerTemplate;
import wt.inf.team.ContainerTeam;
import wt.inf.team.ContainerTeamHelper;
import wt.inf.team.ContainerTeamReference;
import wt.inf.template.ContainerTemplateHelper;
import wt.inf.template.WTContainerTemplateMaster;
import wt.introspection.ReflectionHelper;
import wt.ixb.publicforhandlers.IxbHndHelper;
import wt.lifecycle.LifeCycleHelper;
import wt.lifecycle.LifeCycleManaged;
import wt.lifecycle.LifeCycleServerHelper;
import wt.lifecycle.State;
import wt.load.LoadServerHelper;
import wt.locks.LockException;
import wt.locks.LockHelper;
import wt.log4j.LogR;
import wt.method.MethodContext;
import wt.occurrence.OccurrenceHelper;
import wt.option.ChoiceMappable;
import wt.option.ExpressionAssignable;
import wt.org.DirectoryContextProvider;
import wt.org.OrganizationServicesHelper;
import wt.org.WTOrganization;
import wt.org.WTUser;
import wt.pdmlink.PDMLinkProduct;
import wt.pds.StatementSpec;
import wt.pom.UniquenessException;
import wt.prefs.PreferenceHelper;
import wt.prefs.WTPreferences;
import wt.project.Role;
import wt.query.LogicalOperator;
import wt.query.QuerySpec;
import wt.query.SearchCondition;
import wt.sandbox.SandboxHelper;
import wt.series.MultilevelSeries;
import wt.series.Series;
import wt.session.SessionHelper;
import wt.team.TeamHelper;
import wt.team.TeamManaged;
import wt.type.TypeManaged;
import wt.type.Typed;
import wt.type.TypedUtility;
import wt.ufid.FederatableInfo;
import wt.util.WTAttributeNameIfc;
import wt.util.WTContext;
import wt.util.WTException;
import wt.util.WTInvalidParameterException;
import wt.util.WTMessage;
import wt.util.WTProperties;
import wt.util.WTPropertyVetoException;
import wt.vc.Iterated;
import wt.vc.IterationIdentifier;
import wt.vc.IterationInfo;
import wt.vc.Mastered;
import wt.vc.VersionControlHelper;
import wt.vc.VersionControlServerHelper;
import wt.vc.VersionIdentifier;
import wt.vc.VersionInfo;
import wt.vc.Versioned;
import wt.vc.config.LatestConfigSpec;
import wt.vc.struct.StructHelper;
import wt.vc.struct.StructServerHelper;
import wt.vc.views.Variation1;
import wt.vc.views.Variation2;
import wt.vc.views.View;
import wt.vc.views.ViewException;
import wt.vc.views.ViewHelper;
import wt.vc.views.ViewManageable;
import wt.vc.views.ViewReference;
import wt.vc.wip.WorkInProgressHelper;
import wt.vc.wip.Workable;

import com.ptc.core.lwc.server.LoadAttValues;
import com.ptc.core.meta.common.TypeIdentifier;
import com.ptc.core.meta.common.TypeIdentifierHelper;
import com.ptc.core.meta.type.mgmt.server.impl.association.AssociationConstraintHelper;

/**
 * Creates and persists part objects based on input from a comma seperated value
 * (csv) file.  Method names and parameters are defined in csvmapfile.txt.
 * <code>wt.load.StandardLoadService.load</code> reads records from a .csv file
 * and passes the fields in a hashtable to methods defined in this class.
 * <p>
 * The load methods use a wt.load.StandardLoadService cache to cache
 * part masters and part version objects to improve performance of creating
 * assemblies and updating attrbiutes.
 *
 * The load methods use wt.doc.LoadDoc to retrieve documents, taking advantage
 * of its internal caching.
 *
 * <BR><BR><B>Supported API: </B>true
 * <BR><BR><B>Extendable: </B>false
 *
 * @see wt.load.StandardLoadService
 * @see wt.iba.value.service.LoadValue
 * @see wt.doc.LoadDoc
 **/
 /*
  * TO DO:
  *   1) Use resource bundles for all user messages.
  *   2) Document wt.load.verboseServer in properties.html
  *   3) Use StandardLoadService.getValue instead of LoadPart.getValue
  */
public class LoadPart {

   /*
    * Keys for the StandardLoadService cache.  This cache stores objects
    * used by multiple load methods.
    *
    * To Do - All hashtable keys should be public fields in wt.load.StandardLoadService.
    */
   private static final String CURRENT_CONTENT_HOLDER = "Current ContentHolder"; //Used by wt.load.LoadContent

   private static final String PART_CACHE_KEY         = "PART_CACHE_KEY:";
   private static final String PART_MASTER_CACHE_KEY  = "PART_MASTER_CACHE_KEY:";
   private static final String PARTUSAGELINK_CACHE_KEY = "PARTUSAGElINK_KEY:";
   private static final String PARTDOCLINK_CACHE_KEY = "PARTDOCLINK_CACHE_KEY:";

   /*
    * Variable used to store the session user during the user switch
    */
   private static final String PART_PREVIOUS_USER   = "PART_PREVIOUS_USER:";

   // SHOULD NOT BE USED.  PRESERVED TO SUPPORT OTHER LOADERS THAT ASSUME THIS IS USED.
   // USE LoadPart.getPart() INSTEAD
   private static String CURRENT_PART = "Current Part";

   private static String RESOURCE = "wt.part.partResource";

   private static final int REFERENCE_LINK = 0;
   private static final int DESCRIBES_LINK = 1;
   private static final int UNKNOWN_LINK   = 2;
   private static ResourceBundle rb;
   private static final String TYPEDEF = "typedef";

   // Variables for creating view preferences
   private static final String DEFAULT_VIEW_PREF_NODE = "wt/part";
   private static final String DEFAULT_VIEW_PREF_KEY = "DefaultConfigSpecView";

  /**
   * Flag to control vervbose debugging output during part loading.
   * This constant is controlled via <code>wt.properties</code> file entry
   * <br>
   * <code>wt.part.load.verbose</code>
   * <p>
   * The default value is <code>false</code>.
   **/
   public static final boolean VERBOSE;
   private static final String WTHOME;
   private static final String DIRSEP;

   private static final String REPHELPER_CLASS = "com.ptc.wvs.server.ui.RepHelper";
   private static final String REPHELPER_METHOD = "loadRepresentation";
   private static final double DTOR = Math.PI/180.0;
   private static final TypeIdentifier PART_TI;
   private static final HashMap<String, String> codingSystemMap = new HashMap<String, String>();

   private static final Object LAST_PART_KEY = new Object() {
      public String toString() { return "wt.part.LoadPart.LAST_PART_KEY"; }
   };
   private static final Logger logger = LogR.getLogger(LoadPart.class.getName());

   static {
      try {
         rb = ResourceBundle.getBundle(RESOURCE,
                                       WTContext.getContext().getLocale());
         WTProperties properties = WTProperties.getLocalProperties();
         VERBOSE = properties.getProperty("wt.part.load.verbose", false);
         WTHOME = properties.getProperty("wt.home", "");
         DIRSEP = properties.getProperty("dir.sep", "");
         //PART_TI = TypeHelper.getTypeIdentifier(WTPart.class.getName());
         PART_TI = (TypeIdentifier)ReflectionHelper.dynamicInvoke( "com.ptc.core.foundation.type.server.impl.TypeHelper",
                           "getTypeIdentifier", new Class[] {String.class}, new Object[] {WTPart.class.getName()} );

         codingSystemMap.put("CAGE", "0141");
         codingSystemMap.put("DUNS", "0060");
         codingSystemMap.put("ISO65233", "0026");
      }
      catch (Throwable t)
      {
         System.err.println("Error initializing " + LoadPart.class.getName ());
         t.printStackTrace(System.err);
         throw new ExceptionInInitializerError(t);
      }
   }
  /**
   * Processes the "csvPart" directive in the XML load file.
   * Creates a part object, persists it in the database, checks it out,
   * applies the default attribute values associated with the specified type
   * definition, persists the part, checks it back in, and caches it in
   * the loader's memory.
   * <p>
   *  Establishes the part as the CURRENT_CONTENT_HOLDER for use by "ContentFile" lines.
   * <p>
   * This format cannot add Soft Attributes(IBA).  For that see {@link #beginCreateWTPart(Hashtable, Hashtable, Vector) beginCreateWTPart}.
   * <p>
   * When using {@link wt.load.LoadFromFile wt.load.LoadFromFile} to load parts, end items and serial numbered parts,
   * use the following XML format:
   * <p>
   * <h2>XML Format</h2>
   * <p>
   * <TABLE WIDTH="100%" BORDER=0>
   * <TR><TD COLSPAN=4>&lt;csvPart handler="wt.part.LoadPart.createPart" &gt;</TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvuser&gt;               </TD><TD><I>created by user          </I></TD><TD>&lt;/csvuser&gt;                </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvpartName&gt;           </TD><TD><I>part name                </I></TD><TD>&lt;/csvpartName&gt;            </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvpartNumber&gt;         </TD><TD><I>part number              </I></TD><TD>&lt;/csvpartNumber&gt;          </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvtype&gt;               </TD><TD><I>assembly mode            </I></TD><TD>&lt;/csvtype&gt;                </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvgenericType&gt;        </TD><TD><I>generic type             </I></TD><TD>&lt;/csvgenericType&gt;                </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvlogicbasePath&gt;      </TD><TD><I>logic base path          </I></TD><TD>&lt;/csvlogicbasePath&gt;                </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvsource&gt;             </TD><TD><I>source                   </I></TD><TD>&lt;/csvsource&gt;              </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvfolder&gt;             </TD><TD><I> folder                  </I></TD><TD>&lt;/csvfolder&gt;              </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvlifecycle&gt;          </TD><TD><I>lifecycle                </I></TD><TD>&lt;/csvlifecycle&gt;           </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvview&gt;               </TD><TD><I>view                     </I></TD><TD>&lt;/csvview&gt;                </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvteamTemplate&gt;       </TD><TD><I>team template            </I></TD><TD>&lt;/csvteamTemplate&gt;        </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvlifecyclestate&gt;     </TD><TD><I>lifecycle state          </I></TD><TD>&lt;/csvlifecyclestate&gt;      </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvtypedef&gt;            </TD><TD><I>typedef                  </I></TD><TD>&lt;/csvtypedef&gt;             </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvversion&gt;            </TD><TD><I>revision                 </I></TD><TD>&lt;/csvversion&gt;             </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csviteration&gt;          </TD><TD><I>iteration                </I></TD><TD>&lt;/csviteration&gt;           </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvparentContainerPath&gt;</TD><TD><I>parent container path    </I></TD><TD>&lt;/csvparentContainerPath&gt; </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvenditem&gt;            </TD><TD><I>end item                 </I></TD><TD>&lt;/csvenditem&gt; </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvtraceCode&gt;          </TD><TD><I>trace code               </I></TD><TD>&lt;/csvtraceCode&gt; </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvorganizationName&gt;   </TD><TD><I>organization name        </I></TD><TD>&lt;/csvorganizationName&gt; </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvorganizationID&gt;     </TD><TD><I>organization id          </I></TD><TD>&lt;/csvorganizationID&gt; </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvcreateTimestamp&gt;   </TD><TD><I>creation time stamp       </I></TD><TD>&lt;/csvcreateTimestamp&gt; </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvmodifyTimestamp&gt;     </TD><TD><I>modification time stamp </I></TD><TD>&lt;/csvmodifyTimestamp&gt; </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvminRequired&gt;     </TD><TD><I>minumin required </I></TD><TD>&lt;/csvminRequired&gt; </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvmaxAllowed&gt;     </TD><TD><I>maximum allowed </I></TD><TD>&lt;/csvmaxAllowed&gt; </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvdefaultUnit&gt;        </TD><TD><I>default unit             </I></TD><TD>&lt;/csvdefaultUnit&gt; </TD></TR>
   * <TR><TD COLSPAN=4>&lt;/csvPart&gt;</TD></TR>
   * </TABLE>
   * <p>
   * <h2>Tag Definitions</h2>
   * <p>
   * <TABLE WIDTH="100%" BORDER=0>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>created by user          </I></TD>
   *       <TD>
   *                 This tag allows a user other than the user loading the file to be the creator of this part.
   *                 This value is optional and defaults to the user loading the file.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>part name                </I></TD>
   *       <TD>
   *                 The name of the part. A value is required for this attribute.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>part number              </I></TD>
   *       <TD>
   *                 The number of the part. This value is optional.  If a number is provided, a search
   *                 will be done to locate a part with this number.  If a part is found, a new revision/iteration
   *                 of the part will be created, and if one is not found, a new part will be created.
   *                 If a number is not provided, a new part will be created and the next system-generated number will be used.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>assembly mode            </I></TD>
   *       <TD>
   *                 The assembly mode for the part.   A value is required for this attribute and must be one of the following:<p>
   *                 <ul>
   *                   <li><code>separable</code></li>
   *                   <li><code>inseparable</code></li>
   *                   <li><code>component</code></li>
   *                 </ul>
   *       </TD>
   *    </TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>generic type            </I></TD>
   *       <TD>
   *                 The generic type of the part.  This tag is optional. If the tag is used and a value specified, the value must be one of the following:<p>
   *                 <ul>
   *                   <li><code>standard</code></li>
   *                   <li><code>generic</code></li>
   *                   <li><code>configurable_generic</code></li>
   *                   <li><code>variant</code></li>
   *                 </ul>
   *                If the tag is not used or used but left blank, the default value is <code>standard</code>.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>logic base path            </I></TD>
   *       <TD>
   *                Path to an XML file with the logic base information for a generic or configurable_generic part.
   *                The path is given relative to the location of the file in which the attribute is used.
   *                This tag is optional, and if not used or used but left blank, there is no logic base
   *                information associated with the generic or configurable generic part.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>source                   </I></TD>
   *       <TD>
   *                 The source of the part.  A value is required for this attribute and must be one of the following:
   *                 <ul>
   *                   <li><code>buy</code></li>
   *                   <li><code>make</code></li>
   *                   <li><code>singlesource</code></li>
   *                 </ul>
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I> folder                  </I></TD>
   *       <TD>
   *                 The full path to the folder in which the part is created,  i.e. <code>/Default/Design</code>.
   *                 This value is optional and will default to <code>/Default</code>.  An error message that
   *                 this is a required field may appear if left blank, but this message can be ignored.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>lifecycle                </I></TD>
   *       <TD>
   *                 The lifecycle that the part will be created under.  This value is optional and defaults to <code>Basic</code>.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>view                     </I></TD>
   *       <TD>
   *                 The view that the part will be created under, i.e. <code>Design, Manufacturing</code>.
   *                 This value is optional.  Leave this value blank for serial numbered parts.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>team template            </I></TD>
   *       <TD>
   *                 The team template that the part will be created under.  This value is optional and defaults to
   *                 no team template being associated with the part.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>lifecycle state          </I></TD>
   *       <TD>
   *                 The lifecycle state that the created part will be in.  This value is optional and defaults
   *                 to the initial state of the lifecycle that the part is created under.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>typedef                  </I></TD>
   *       <TD>
   *                 This contains the identifier string of a {@link com.ptc.core.meta.common.TypeIdentifier com.ptc.core.meta.common.TypeIdentifier}.<br><br>
   *                 i.e.     wt.part.WTPart|com.mycompany.SoftPart2<br><br>
   *                 This would create a soft typed part.  This currently supports the following modeled types:<p>
   *                 <ul>
   *                   <li><code>wt.part.WTPart</code></li>
   *                 </ul>
   *                 <p>
   *                 This currently does not handle custom modeled types.  If there is a need for one of these, then
   *                 a custom loader needs to be created.
   *                 <p>
   *                 This value is optional and defaults to <code>wt.part.WTPart</code>.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>version                  </I></TD>
   *       <TD>
   *                 The revision of the part.  This value is optional and defaults to the latest revision of the part.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>iteration                </I></TD>
   *       <TD>
   *                 The iteration of the part.  This value is optional and defaults to the next iteration of the revision of the part.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>parent container path    </I></TD>
   *       <TD>
   *                 DEPRECATED.  This property is ignored, although the tag is still required.  Instead the parent container is obtained from the CONT_PATH command line argument when running wt.load.LoadFromFile, or from the Load Set data.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>end item    </I></TD>
   *       <TD>
   *                 Whether the part will be created as an end item or not.  This tag is optional.  The default is that the item is not created as an end item.
   *                 To create the part as an end item, use this tag and set the value to <code>yes</code>.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>trace code    </I></TD>
   *       <TD>
   *                 The trace code for the part.  This tag is optional. If the tag is used and a value specified, the value must be one of the following:<p>
   *                 <ul>
   *                   <li><code>O</code></li>
   *                   <li><code>L</code></li>
   *                   <li><code>S</code></li>
   *                   <li><code>X</code></li>
   *                 </ul>
   *                 These stand for Untraced, Lot, Serial Number, and Lot/Serial Number, respectively.
   *                 <p>
   *                If the tag is not used or used but left blank, the default value is
   *                <code>O</code>, or Untraced.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>organization name    </I></TD>
   *       <TD>
   *                 The name of the organization that the part will be created under.  This is also the organization in
   *                 which a search for the part will occur if a part number is specified.  This tag is optional and defaults
   *                 to the organization determined by the command line argument CONT_PATH.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>organization id    </I></TD>
   *       <TD>
   *                 The id of the organization that the part will be created under.  This is also the organization in
   *                 which a search for the part will occur if a part number is specified.  This tag is optional and defaults
   *                 to the organization determined by the command line argument CONT_PATH.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>creation time stamp    </I></TD>
   *       <TD>
   *                 The creation time stamp for the part.  This tag is optional and defaults to the
   *                 modification time stamp, if that value is set, or to the current system time if
   *                 neither the creation nor modification time stamps are set.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>modification time stamp   </I></TD>
   *       <TD>
   *                 The modification time stamp for the part.  This tag is optional and defaults to the
   *                 creation time stamp, if that value is set, or to the current system time if
   *                 neither the creation nor modification time stamps are set.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>minimum required   </I></TD>
   *       <TD>
   *                 Minimum number of children to be present to have a valid configuration
   *       </TD>
   *    </TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>maximum allowed  </I></TD>
   *       <TD>
   *                 Maximum number of children to be present to have a valid configuration
   *       </TD>
   *    </TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>part default unit    </I></TD>
   *       <TD>
   *                 The default unit for the part.  This tag is optional and defaults to each.
   *                 Valid values are ea, kg, m, l, sq_m, cu_m, as_needed.
   *       </TD>
   *    </TR>
   * </TABLE>
   * <p>
   * <h2>Revisions and Iterations</h2>
   * <p>
   * This method supports a part to being created at a specified revision and
   * iteration.  Multiple part revisions imply an "order", i.e. subsequent bulk load
   * runs can "fill in the gaps", but it does so by attaching to the latest iteration of the
   * previous revision.  If a newer iteration is added to the previous revision, the new revision
   * will attached to the new latest iteration.  For example:  Load set 1 (E.1, A.1, C.2) will
   * result in (A.1, C.2, E.1).  The predecssors of: C.2 is A.1, E.1 is C.2.  Load set 2
   * (B.1, A.2., C.1, C.3) will result in (A.1, A.2, B.1, C.1, C.2, C.3, E.1).
   * The predecessors of: B.1 is A.2, C.1 is B.1, E.1 is C.3.  Any new revision/iterations added
   * will continue to change the predecessor links to the new latest iteration of the previous revision.
   * <p>
   * Gaps in the ordering <B>ARE</B> supported.
   * <p>
   * Examples of valid revisions/iterations are: (A.1,A.3,B.2,B.5,E.4,E.5)
   * <p>
   * NOTE TO USERS OF THE SOURCE CODE: The ability to load part revisions out of order is implemented
   * by using the VersionControlHelper.service.insertNode() method.  Calls to this method must not
   * be removed from the code.  To turn off this behavior, you should instead set the
   * insert_on_latest_iteration flag to false in the constructPart() method.
   * <p>
   * When loading a revision out of sequence and loading relationships to those parts there are
   * a few things that you should understand while ordering your data.
   * <p>
   * 1.  Relationships are copied forward from what is identified as the predecessor to the
   * new revision.  So in an ordered case if you create part 1 A.1 that has references or a describe
   * relationship to doc 2.  Then you revise to get part 1 B.1, it will have the relationships
   * of A.1 copied to it so it will have the relationship to doc 2 as well.
   * <p>
   * 2.  Relationships are not copied forward if they are created after the new revision has already
   * been created.  For example if part 1 A.1 and B.1 are created and then part A.1 has a relationship
   * created to doc 2, part 1 B.1 will not have the relationship to doc 2 copied to it.  You must
   * explicitely create that relationship if you want it created.
   * <p>
   * 3.  Relationships from predecessors are not cumulative.  This is really just a further clarification
   * of the first point.  Relationships are only copied from the one identified predecessor and not
   * the accumulation of predecessors.  So if you create part 1 B.1 that is related to doc 2, create
   * part 1 A.1 that is related to doc 3 and then create part 1 C.1 it will inherit the relationship
   * to doc 2 only.
   * <p>
   * These are just a few pointers to loading revisions and relationships out of sequence.  There
   * are other scenarios given these basic rules that can also be imagined.  So order the creation
   * of revisions/iterations and relationship very carefully to get the results that you intend.
   * <p>
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   *
   * @param nv               Name/Value pairs of part attributes.
   * @param cmd_line         command line argument that can contain supplemental load data
   * @param return_objects   <code>Vector</code> of the object(s) created by this method.
   *                         Used by <code>wt.load.StandardLoadService</code>
   *                         for accurate user feedback messages.
   * @return <table><tr><td>true</td><td>success</td></tr><tr><td>false</td><td>failure</td></tr></table>
   **/
   public static boolean createPart(Hashtable nv, Hashtable cmd_line, Vector return_objects) {
      try {
         checkTypedef(PART_TI, nv);
      }
      catch (WTException wte) {
         LoadServerHelper.printMessage("Typedef check failed");
         wte.printStackTrace();
         return false;
      }
      return createPartObject(nv,cmd_line,return_objects) &&
             updatePartObject(nv,cmd_line,return_objects);
   }

   protected static void checkTypedef(TypeIdentifier root_type, Hashtable nv) throws WTException {
     String typedef = (String)nv.get(TYPEDEF);

     if (typedef == null || typedef.trim().equals(""))
     {
         nv.put(TYPEDEF,root_type.toExternalForm());
     }
     else {
        if (typedef.startsWith("wt.part.WTPart")) {
           typedef = "WCTYPE|" + typedef;
        }
        else if (typedef.startsWith("WCTYPE|")) {
           TypeIdentifier type_id;
           try {
              //type_id = TypeHelper.getTypeIdentifier(typedef);
              type_id = (TypeIdentifier)ReflectionHelper.dynamicInvoke( "com.ptc.core.foundation.type.server.impl.TypeHelper",
                           "getTypeIdentifier", new Class[] {String.class}, new Object[] {typedef} );
           }
           catch (Exception e) {
              throw new WTException("Could not find TypeIdentifier for typedef: " + typedef);
           }
           if (!type_id.isDescendedFrom(root_type)) {
              throw new WTException("Typedef does not descend from required parent. Child: " + type_id + " Parent: " + root_type);
           }
        }
        else {
           typedef = root_type.toExternalForm() + '|' + typedef;
        }
        nv.put(TYPEDEF, typedef);
     }
   }
  /**
   * Processes the "csvBeginWTPart" directive in the XML load file.
   * <p>
   * Creates a part object, persists it in the database, checks it out,
   * applies the default attribute values associated with the specified type
   * definition, persists the part, checks it back in, and caches it in
   * the loader's memory.
   * <p>
   * If a part with this number already exists, the part is looked up and a new iteration of
   * the latest revision is created, unless the revision/iteration are specified using attributes in the
   * the load file.
   * <p>
   *  Establishes the part as the CURRENT_CONTENT_HOLDER for use by "ContentFile" lines.
   * <p>
   * When using  {@link wt.load.LoadFromFile wt.load.LoadFromFile} to load parts, end items and serial numbered parts,
   * use the following XML format:
   * <p>
   * <h2>XML Format</h2>
   * <p>
   * <TABLE WIDTH="100%" BORDER=0>
   * <TR><TD COLSPAN=4>&lt;csvBeginWTPart  handler="wt.part.LoadPart.beginCreateWTPart" &gt;</TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvuser&gt;               </TD><TD><I>created by user          </I></TD><TD>&lt;/csvuser&gt;                </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvpartName&gt;           </TD><TD><I>part name                </I></TD><TD>&lt;/csvpartName&gt;            </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvpartNumber&gt;         </TD><TD><I>part number              </I></TD><TD>&lt;/csvpartNumber&gt;          </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvtype&gt;               </TD><TD><I>assembly mode            </I></TD><TD>&lt;/csvtype&gt;                </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvgenericType&gt;        </TD><TD><I>generic type             </I></TD><TD>&lt;/csvgenericType&gt;                </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvlogicbasePath&gt;      </TD><TD><I>logic base path             </I></TD><TD>&lt;/csvlogicbasePath&gt;                </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvsource&gt;             </TD><TD><I>source                   </I></TD><TD>&lt;/csvsource&gt;              </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvfolder&gt;             </TD><TD><I> folder                  </I></TD><TD>&lt;/csvfolder&gt;              </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvlifecycle&gt;          </TD><TD><I>lifecycle                </I></TD><TD>&lt;/csvlifecycle&gt;           </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvview&gt;               </TD><TD><I>view                     </I></TD><TD>&lt;/csvview&gt;                </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvteamTemplate&gt;       </TD><TD><I>team template            </I></TD><TD>&lt;/csvteamTemplate&gt;        </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvlifecyclestate&gt;     </TD><TD><I>lifecycle state          </I></TD><TD>&lt;/csvlifecyclestate&gt;      </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvtypedef&gt;            </TD><TD><I>typedef                  </I></TD><TD>&lt;/csvtypedef&gt;             </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvversion&gt;            </TD><TD><I>revision                  </I></TD><TD>&lt;/csvversion&gt;             </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csviteration&gt;          </TD><TD><I>iteration                </I></TD><TD>&lt;/csviteration&gt;           </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvenditem&gt;            </TD><TD><I>end item                 </I></TD><TD>&lt;/csvenditem&gt; </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvtraceCode&gt;          </TD><TD><I>trace code               </I></TD><TD>&lt;/csvtraceCode&gt; </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvorganizationName&gt;   </TD><TD><I>organization name        </I></TD><TD>&lt;/csvorganizationName&gt; </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvorganizationID&gt;     </TD><TD><I>organization id          </I></TD><TD>&lt;/csvorganizationID&gt; </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvcreateTimestamp&gt;   </TD><TD><I>creation time stamp       </I></TD><TD>&lt;/csvcreateTimestamp&gt; </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvmodifyTimestamp&gt;     </TD><TD><I>modification time stamp </I></TD><TD>&lt;/csvmodifyTimestamp&gt; </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvminRequired&gt;     </TD><TD><I>minimum required </I></TD><TD>&lt;/csvminRequired&gt; </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvmaxAllowed&gt;     </TD><TD><I>maximum allowed</I></TD><TD>&lt;/csvmaxAllowed&gt; </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvdefaultUnit&gt;        </TD><TD><I>part default unit        </I></TD><TD>&lt;/csvdefaultUnit&gt; </TD></TR>

   * <TR><TD COLSPAN=4>&lt;/csvBeginWTPart &gt;</TD></TR>
   * <TR><TD COLSPAN=4>&nbsp;</TD></TR>
   * <TR><TD COLSPAN=4>&lt;csvIBAValue handler="wt.iba.value.service.LoadValue.createIBAValue" &gt;</TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD COLSPAN=3>...</TD></TR>
   * <TR><TD COLSPAN=4>&lt;/csvIBAValue &gt;</TD></TR>
   * <TR><TD COLSPAN=4>&nbsp;</TD></TR>
   * <TR><TD COLSPAN=4>&lt;csvIBAValue handler="wt.iba.value.service.LoadValue.createIBAValue" &gt;</TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD COLSPAN=3>...</TD></TR>
   * <TR><TD COLSPAN=4>&lt;/csvIBAValue &gt;</TD></TR>
   * <TR><TD COLSPAN=4>&nbsp;</TD></TR>
   * <TR><TD COLSPAN=4>&lt;csvIBAValue handler="wt.iba.value.service.LoadValue.createIBAValue" &gt;</TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD COLSPAN=3>...</TD></TR>
   * <TR><TD COLSPAN=4>&lt;/csvIBAValue &gt;</TD></TR>
   * <TR><TD COLSPAN=4>&nbsp;</TD></TR>
   * <TR><TD COLSPAN=4>&lt;csvIBAValue handler="wt.iba.value.service.LoadValue.createIBAValue" &gt;</TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD COLSPAN=3>...</TD></TR>
   * <TR><TD COLSPAN=4>&lt;/csvIBAValue &gt;</TD></TR>
   * <TR><TD COLSPAN=4>&nbsp;</TD></TR>
   * <TR><TD COLSPAN=4>&lt;csvEndWTPart  handler="wt.part.LoadPart.endCreateWTPart" &gt;</TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD COLSPAN=3>...</TD></TR>
   * <TR><TD COLSPAN=4>&lt;/csvEndWTPart &gt;</TD></TR>
   * </TABLE>
   * <p>
   * <h2>Tag Definitions</h2>
   * <p>
   * <TABLE WIDTH="100%" BORDER=0>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>created by user          </I></TD>
   *       <TD>
   *                 This tag allows a user other than the user loading the file to be the creator of this part.
   *                 This value is optional and defaults to the user loading the file.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>part name                </I></TD>
   *       <TD>
   *                 The name of the part. A value is required for this attribute.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>part number              </I></TD>
   *       <TD>
   *                 The number of the part. This value is optional.  If a number is provided, a search
   *                 will be done to locate a part with this number.  If a part is found, a new revision/iteration
   *                 of the part will be created, and if one is not found, a new part will be created.
   *                 If a number is not provided, a new part will be created and the next system-generated number will be used.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>assembly mode            </I></TD>
   *       <TD>
   *                 The assembly mode for the part.   A value is required for this attribute and must be one of the following:<p>
   *                 <ul>
   *                   <li><code>separable</code></li>
   *                   <li><code>inseparable</code></li>
   *                   <li><code>component</code></li>
   *                 </ul>
   *       </TD>
   *    </TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>generic type            </I></TD>
   *       <TD>
   *                 The generic type of the part.  This tag is optional. If the tag is used and a value specified, the value must be one of the following:<p>
   *                 <ul>
   *                   <li><code>standard</code></li>
   *                   <li><code>generic</code></li>
   *                   <li><code>configurable_generic</code></li>
   *                   <li><code>variant</code></li>
   *                 </ul>
   *                If the tag is not used or used but left blank, the default value is <code>standard</code>.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>logic base path            </I></TD>
   *       <TD>
   *                Path to an XML file with the logic base information for a generic or configurable_generic part.
   *                The path is given relative to the location of the file in which the attribute is used.
   *                This tag is optional, and if not used or used but left blank, there is no logic base
   *                information associated with the generic or configurable generic part.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>source                   </I></TD>
   *       <TD>
   *                 The source of the part.  A value is required for this attribute and must be one of the following:
   *                 <ul>
   *                   <li><code>buy</code></li>
   *                   <li><code>make</code></li>
   *                   <li><code>singlesource</code></li>
   *                 </ul>
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I> folder                  </I></TD>
   *       <TD>
   *                 The full path to the folder in which the part is created,  i.e. <code>/Default/Design</code>.
   *                 This value is optional and will default to <code>/Default</code>.  An error message that
   *                 this is a required field may appear if left blank, but this message can be ignored.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>lifecycle                </I></TD>
   *       <TD>
   *                 The lifecycle that the part will be created under.  This value is optional and defaults to <code>Basic</code>.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>view                     </I></TD>
   *       <TD>
   *                 The view that the part will be created under, i.e. <code>Design, Manufacturing</code>.
   *                 This value is optional.  Leave this value blank for serial numbered parts.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>team template            </I></TD>
   *       <TD>
   *                 The team template that the part will be created under.  This value is optional and defaults to
   *                 no team template being associated with the part.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>lifecycle state          </I></TD>
   *       <TD>
   *                 The lifecycle state that the created part will be in.  This value is optional and defaults
   *                 to the initial state of the lifecycle that the part is created under.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>typedef                  </I></TD>
   *       <TD>
   *                 This contains the identifier string of a {@link com.ptc.core.meta.common.TypeIdentifier com.ptc.core.meta.common.TypeIdentifier}.<br><br>
   *                 i.e.     wt.part.WTPart|com.mycompany.SoftPart2<br><br>
   *                 This would create a soft typed part.  This currently supports the following modeled types:<p>
   *                 <ul>
   *                   <li><code>wt.part.WTPart</code></li>
   *                 </ul>
   *                 <p>
   *                 This currently does not handle custom modeled types.  If there is a need for one of these, then
   *                 a custom loader needs to be created.
   *                 <p>
   *                 This value is optional and defaults to <code>wt.part.WTPart</code>.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>revision                  </I></TD>
   *       <TD>
   *                 The revision of the part.  This value is optional and defaults to the latest revision of the part.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>iteration                </I></TD>
   *       <TD>
   *                 The iteration of the part.  This value is optional and defaults to the next iteration of the revision of the part.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>end item    </I></TD>
   *       <TD>
   *                 Whether the part will be created as an end item or not.  This tag is optional.  The default is that the item is not created as an end item.
   *                 To create the part as an end item, use this tag and set the value to <code>yes</code>.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>trace code    </I></TD>
   *       <TD>
   *                 The trace code for the part.  This tag is optional. If the tag is used and a value specified, the value must be one of the following:<p>
   *                 <ul>
   *                   <li><code>O</code></li>
   *                   <li><code>L</code></li>
   *                   <li><code>S</code></li>
   *                   <li><code>X</code></li>
   *                 </ul>
   *                 These stand for Untraced, Lot, Serial Number, and Lot/Serial Number, respectively.
   *                 <p>
   *                If the tag is not used or used but left blank, the default value is
   *                <code>O</code>, or Untraced.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>organization name    </I></TD>
   *       <TD>
   *                 The name of the organization that the part will be created under.  This is also the organization in
   *                 which a search for the part will occur if a part number is specified.  This tag is optional and defaults
   *                 to the organization determined by the command line argument CONT_PATH.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>organization id    </I></TD>
   *       <TD>
   *                 The id of the organization that the part will be created under.  This is also the organization in
   *                 which a search for the part will occur if a part number is specified.  This tag is optional and defaults
   *                 to the organization determined by the command line argument CONT_PATH.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>creation time stamp    </I></TD>
   *       <TD>
   *                 The creation time stamp for the part.  This tag is optional and defaults to the
   *                 modification time stamp, if that value is set, or to the current system time if
   *                 neither the creation nor modification time stamps are set.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>modification time stamp   </I></TD>
   *       <TD>
   *                 The modification time stamp for the part.  This tag is optional and defaults to the
   *                 creation time stamp, if that value is set, or to the current system time if
   *                 neither the creation nor modification time stamps are set.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>minimum required   </I></TD>
   *       <TD>
   *                 Minimum number of children to be present to have a valid configuration
   *       </TD>
   *    </TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>maximum allowed  </I></TD>
   *       <TD>
   *                 Maximum number of children to be present to have a valid configuration
   *       </TD>
   *    </TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>part default unit   </I></TD>
   *       <TD>
   *                 The default unit for the part.  This tag is optional and defaults to each.
   *                 Valid values are ea, kg, m, l, sq_m, cu_m, as_needed.
   *       </TD>
   *    </TR>
   * </TABLE>
   * <p>
   * <h2>Revisions and Iterations</h2>
   * <p>
   * This method supports a part to being created at a specified revision and
   * iteration.  Multiple part revisions imply an "order", i.e. subsequent bulk load
   * runs can "fill in the gaps", but it does so by attaching to the latest iteration of the
   * previous revision.  If a newer iteration is added to the previous revision, the new revision
   * will attached to the new latest iteration.  For example:  Load set 1 (E.1, A.1, C.2) will
   * result in (A.1, C.2, E.1).  The predecssors of: C.2 is A.1, E.1 is C.2.  Load set 2
   * (B.1, A.2., C.1, C.3) will result in (A.1, A.2, B.1, C.1, C.2, C.3, E.1).
   * The predecessors of: B.1 is A.2, C.1 is B.1, E.1 is C.3.  Any new revision/iterations added
   * will continue to change the predecessor links to the new latest iteration of the previous revision.
   * <p>
   * Gaps in the ordering <B>ARE</B> supported.
   * <p>
   * Examples of valid revisions/iterations are: (A.1,A.3,B.2,B.5,E.4,E.5)
   * <p>
   * NOTE TO USERS OF THE SOURCE CODE: The ability to load part revisions out of order is implemented
   * by using the VersionControlHelper.service.insertNode() method.  Calls to this method must not
   * be removed from the code.  To turn off this behavior, you should instead set the
   * insert_on_latest_iteration flag to false in the constructPart() method.
   * <p>
   * When loading a revision out of sequence and loading relationships to those parts there are
   * a few things that you should understand while ordering your data.
   * <p>
   * 1.  Relationships are copied forward from what is identified as the predecessor to the
   * new revision.  So in an ordered case if you create part 1 A.1 that has references or a describe
   * relationship to doc 2.  Then you revise to get part 1 B.1, it will have the relationships
   * of A.1 copied to it so it will have the relationship to doc 2 as well.
   * <p>
   * 2.  Relationships are not copied forward if they are created after the new revision has already
   * been created.  For example if part 1 A.1 and B.1 are created and then part A.1 has a relationship
   * created to doc 2, part 1 B.1 will not have the relationship to doc 2 copied to it.  You must
   * explicitely create that relationship if you want it created.
   * <p>
   * 3.  Relationships from predecessors are not cumulative.  This is really just a further clarification
   * of the first point.  Relationships are only copied from the one identified predecessor and not
   * the accumulation of predecessors.  So if you create part 1 B.1 that is related to doc 2, create
   * part 1 A.1 that is related to doc 3 and then create part 1 C.1 it will inherit the relationship
   * to doc 2 only.
   * <p>
   * These are just a few pointers to loading revisions and relationships out of sequence.  There
   * are other scenarios given these basic rules that can also be imagined.  So order the creation
   * of revisions/iterations and relationship very carefully to get the results that you intend.
   * <p>
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   * <p>
   * @see #endCreateWTPart
   * @see wt.load.LoadFromFile
   * <p>
   * @param nv               Name/Value pairs of part attributes.
   * @param cmd_line         command line argument that can contain supplemental load data
   * @param return_objects   <code>Vector</code> of the object(s) created by this method.
   *                         Used by <code>wt.load.StandardLoadService</code>
   *                         for accurate user feedback messages.
   * @return <table><tr><td>true</td><td>success</td></tr><tr><td>false</td><td>failure</td></tr></table>
   **/
   public static boolean beginCreateWTPart( Hashtable nv, Hashtable cmd_line, Vector return_objects ) {
      resetAttDirectiveFlags();
      try {
         checkTypedef(PART_TI, nv);
      }
      catch (WTException wte) {
         LoadServerHelper.printMessage("Typedef check failed");
         wte.printStackTrace();
         return false;
        }
      return createPartObject(nv,cmd_line,return_objects);
   }

   /**
   * Processes the "EndWTPart" directive in the csv load file.
   * Causes the cached part to be checked-out, associates soft attribues from preceding
   * <code>IBAValue</code> load file lines with the part, applies the default attribute values
   * associated with the specified type definition, persists the part, checks it back in,
   * and caches it in the loader's memory.
   * <p>
   * Establishes the part as the CURRENT_CONTENT_HOLDER for use by "ContentFile" lines.
   * <p>
   *
   * <h2>XML Format</h2>
   * <p>
   * <TABLE WIDTH="100%" BORDER=0>
   * <TR><TD COLSPAN=4>&lt;csvEndWTPart  handler="wt.part.LoadPart.endCreateWTPart" &gt;</TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvparentContainerPath &gt;         </TD><TD><I>parent container path              </I></TD><TD>&lt;/csvparentContainerPath &gt;          </TD></TR>
   * <TR><TD COLSPAN=4>&lt;/csvEndWTPart &gt;</TD></TR>
   * </TABLE>
   * <p>
   * <h2>Tag Definitions</h2>
   * <p>
   * <TABLE WIDTH="100%" BORDER=0>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>parent container path    </I></TD>
   *       <TD>
   *                 DEPRECATED.  This property is ignored, although the tag is still required.  Instead the parent container is obtained from the CONT_PATH command line argument when running wt.load.LoadFromFile, or from the Load Set data.
   *       </TD>
   *    </TR>
   * </TABLE>
   * <B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
   * @see #beginCreateWTPart
   * @see wt.load.LoadFromFile
   * <p>
   * @param nv               not used
   * @param cmd_line         command line argument that can contain supplemental load data
   * @param return_objects   <code>Vector</code> of the object(s) created by this method.
   *                         Used by <code>wt.load.StandardLoadService</code>
   *                         for accurate user feedback messages.

   **/
   public static boolean endCreateWTPart( Hashtable nv, Hashtable cmd_line, Vector return_objects ) {
        return updatePartObject(nv, cmd_line, return_objects);
   }

  /**
   * Processes the "BeginCreateOrUpdateWTPart" directive in the csv load file.
   * <p>
   * Identical to beginCreateWTPart except in that "BeginCreateOrUpdateWTPart"
   * directive does not support specification of 'version' or 'iteration'.
   * The BeginCreateOrUpdateWTPart directive should not be used.
   * It is maintain here in order to support legacy load files.
   * <p>
    * Creates a part object, persists it in the database, and caches it
    * in the loader's memory.
    * <p>
   * Subsequent <code>IBAValue</code> load file lines may be used to associate
   * soft attribute values with the part.  These values will not
   * be persisted until a "EndWTPart" load file line is processed.
   * <p>
   * Establishes the part as the CURRENT_CONTENT_HOLDER for use by "ContentFile" lines.
   * <p>
   * A typical sequence using this directive might be
   * <blockquote>
   *       <tt>BeginCreateOrUpdateWTPart,part-name,part-number,...</tt>
   *   <br><tt>IBAValue,definition1,value1,...</tt>
   *   <br><tt>IBAValue,definition2,value2,...</tt>
   *   <br><tt>EndCreateOrUpdateWTPart</tt>
   *   <br><tt>ContentFile,...</tt>
   *   <br><tt>PartDocReference,...</tt>
   * </blockquote>
    * <p>
   *
   * @see #endCreateOrUpdateWTPart
   *
   * @param nv  Hashtable containing tokens parsed from the csv load file
   *   The attributes are as follows: (arguments in &lt;&gt; are optional)
   *                         <ul>
   *                            <li>partName
   *                            <li>partNumber
   *                            <li>type
   *                            <li>source
   *                            <li>folder
   *                            <li>lifecycle
   *                            <li>&lt;view&gt;
   *                            <li>&lt;teamTemplate&gt;
   *                            <li>&lt;lifecyclestate&gt;
   *                            <li>&lt;typedef&gt;
   *                            <li>&lt;classToUpdate&gt; (NOT LONGER USED)
   *                         </ul>
   * @param cmd_line         command line argument that can contain supplemental load data
   * @param return_objects   <code>Vector</code> of the object(s) created by this method.
   *                         Used by <code>wt.load.StandardLoadService</code>
   *                         for accurate user feedback messages.
   */
    public static boolean beginCreateOrUpdateWTPart( Hashtable nv, Hashtable cmd_line, Vector return_objects ) {
      resetAttDirectiveFlags();
      try {
         checkTypedef(PART_TI, nv);
      }
      catch (WTException wte) {
         LoadServerHelper.printMessage("Typedef check failed");
         wte.printStackTrace();
         return false;
        }
      return createPartObject(nv,cmd_line,return_objects);
    }

   /**
   * Processes the "EndCreateOrUpdateWTPart" directive in the csv load file.
   * Causes the cached part to be checked-out, associates soft attribues from preceding
   * <code>IBAValue</code> load file lines with the part, applies the default attribute values
   * associated with the specified type definition, persists the part, checks it back in,
   * and caches it in the loader's memory.
   * <p>
   * Identical to endCreateWTPart.
   * The EndCreateOrUpdateWTPart directive should not be used.
   * It is maintain here in order to support legacy load files.
   * <p>
   * Establishes the part as the CURRENT_CONTENT_HOLDER for use by "ContentFile" lines.
   * <p>
   * @see #beginCreateOrUpdateWTPart
   * @see #endCreateWTPart
   *
   * @param nv               not used
   * @param cmd_line         command line argument that can contain supplemental load data
   * @param return_objects   <code>Vector</code> of the object(s) created by this method.
   *                         Used by <code>wt.load.StandardLoadService</code>
   *                         for accurate user feedback messages.
   **/
    public static boolean endCreateOrUpdateWTPart( Hashtable nv, Hashtable cmd_line, Vector return_objects ) {
      return updatePartObject(nv,cmd_line,return_objects);
    }

   /**
   * Processes the "BeginWTPM"  directive in the csv load file.
    * Creates a part master object without persisting it and caches it in the loader's memory.
   *
    * @see #endWTPM
    *
   * @param nv  Hashtable containing tokens parsed from the csv load file
   *   The attributes are as follows:
   *                         <ul>
   *                            <li>name   (required)
   *                            <li>number (required)
    *                         </ul>
   * @param cmd_line         command line argument that can contain supplemental load data
   * @param return_objects   <code>Vector</code> of the object(s) created by this method.
   *                         Used by <code>wt.load.StandardLoadService</code>
   *                         for accurate user feedback messages.
   **/
   public static boolean beginWTPM( Hashtable nv, Hashtable cmd_line, Vector return_objects ) {
      try {
         WTPartMaster master = WTPartMaster.newWTPartMaster();

         master.setName(getValue("name",nv,cmd_line,true));
         master.setNumber(getValue("number",nv,cmd_line,true));

         master = cacheMaster(master);

         return true;
      }
      catch (WTException e){
         LoadServerHelper.printMessage("\nCreate Part Master Failed: " + e.getLocalizedMessage());
         e.printStackTrace();
      }
      catch (Exception e){
         LoadServerHelper.printMessage("\nCreate Part Master Failed: " + e.getMessage());
         e.printStackTrace();
      }

      return false;
   }

   /**
   * Processes the "EndWTPM" directive in the csv load file.
   * Associates soft attribues from preceding <code>IBAValue</code> load file lines with
   * the cached part master, persists the part master, and caches it in the loader's memory.
    *
    * @see #beginWTPM
   *
   * @param nv               not used
   * @param cmd_line         command line argument that can contain supplemental load data
   * @param return_objects   <code>Vector</code> of the object(s) created by this method.
   *                         Used by <code>wt.load.StandardLoadService</code>
   *                         for accurate user feedback messages.
   **/
   public static boolean endWTPM( Hashtable nv, Hashtable cmd_line, Vector return_objects ) {
      try {
         LoadValue.endIBAContainer();
         AttributeContainer container = LoadValue.extractDefaultIBAContainer();
         WTPartMaster master = getMaster();

         if(container!=null) master.setAttributeContainer(container);

         master = (WTPartMaster) PersistenceHelper.manager.store(master);
         master = cacheMaster(master);

         return_objects.addElement(master);

         return true;
      }
      catch( WTException e ) {
         System.out.println("EndCreateWTPM: Exception: " + e);
         e.printStackTrace();
      }
      return false;
   }

   /**
    * Processes the "csvBeginOccurrencedAssemblyAdd" directive in the XML load file.
    * Creates a partusagelink object, persists it in the database, checks it out,
    * applies the default attribute values associated with the specified type
    * definition, persists the partusagelink, checks it back in, and caches it in
    * the loader's memory.
    *
    * The behavior for defining the "csvreferenceDesignator" tag is as follows.
    * 1. The following will create n uses occurrences where n = constituentPartQuantity. E.g.,
    *    <csvconstituentPartQty>3</csvconstituentPartQty>
    *    <csvconstituentPartUnit>ea</csvconstituentPartUnit>
    *    <csvreferenceDesignator>bd10/bd11/bd12</csvreferenceDesignator>
    *
    *    Results in 3 uses occurrences being created having reference designators
    *    bd10, bd11, and bd12.
    * 2. A referenceDesignator value that does not have a "/" will output that value.  E.g.,
    *    <csvconstituentPartQty>3</csvconstituentPartQty>
    *    <csvconstituentPartUnit>ea</csvconstituentPartUnit>
    *    <csvreferenceDesignator>bd10-bd12</csvreferenceDesignator>
    *
    *    Results in 1 uses occurrence being created with reference designator "bd10-bd12".
    *
    * Additionally, if you want to add IBAs to a usage link created when adding a reference designator
    * then you need to add the createIBAValue entries after the "</csvBeginOccurrencedAssemblyAdd>" tag
    * and before the "<csvEndOccurrencedAssemblyAdd handler="wt.part.LoadPart.endOccurrencedAssemblyAdd"/>"
    * in the load file.  E.g.,
    *
    * <csvBeginOccurrencedAssemblyAdd handler="wt.part.LoadPart.beginOccurrencedAssemblyAdd" >
    * ...
    * </csvBeginOccurrencedAssemblyAdd>
    * <csvIBAValue handler="wt.iba.value.service.LoadValue.createIBAValue" >
    * <csvdefinition>SomeIBADefinition</csvdefinition>
    * <csvvalue1>SomeIBAValue</csvvalue1>
    * <csvvalue2></csvvalue2>
    * <csvdependency_id></csvdependency_id>
    * </csvIBAValue>
    * <csvEndOccurrencedAssemblyAdd handler="wt.part.LoadPart.endOccurrencedAssemblyAdd"/>
    *
    * @param nv               Name/Value pairs of partusagelink attributes.
    * @param cmd_line         command line argument that can contain supplemental load data
    * @param return_objects   <code>Vector</code> of the object(s) created by this method.
    *                         Used by <code>wt.load.StandardLoadService</code>
    *                         for accurate user feedback messages.
    * @return <table><tr><td>true</td><td>success</td></tr><tr><td>false</td><td>failure</td></tr></table>
    **/
    public static boolean beginOccurrencedAssemblyAdd(Hashtable nv, Hashtable cmd_line, Vector return_objects) {
        // create, persist and cache the partusagelink
        boolean success = addPartToOccurrencedAssembly(nv,cmd_line,return_objects);

        if (success == false){
            return success;
        }

        // prepare to add soft attributes
        try {
            resetAttDirectiveFlags();
            // prepare to add soft attributes
            WTPartUsageLink partUsage = getCachedPartUsage();
            LoadValue.establishCurrentIBAHolder(partUsage);
            LoadValue.beginIBAContainer();

            WTPart workingUsedBy =  partUsage.getUsedBy();

            WTPart usedBy = partUsage.getUsedBy();

            if (!WorkInProgressHelper.isCheckedOut(usedBy)) {
                try {
                    workingUsedBy = (WTPart) WorkInProgressHelper.service.checkout(usedBy, usedBy.getContainer().getDefaultCabinet(), "").getWorkingCopy();
                } catch (WTPropertyVetoException e) {
                    e.printStackTrace();
                }
                //Retrieves working parent part's associated usage links
                String componentId = partUsage.getComponentId();
                partUsage = getUsageLink(workingUsedBy, componentId);
                if(partUsage == null) { //this should never happen, safety check
                    throw new WTException("For some reason cannot find usagelink for "  +workingUsedBy.getNumber()+ 
                            "With componentId " + componentId);
                }
            } 

            LoadAttValues.establishCurrentTypeManaged((TypeManaged)partUsage);
            
            //recache the correct usagelink
            cachePartUsageObject(partUsage);
            cacheChoiceMappableObject(partUsage);      
            cacheExpressionAssignableObject(partUsage);
            
        } catch (WTException wte) {
            LoadServerHelper.printMessage("beginAddPartToOccurrencedAssembly failed");
            wte.printStackTrace();
            return false;
        }

        return success;
    }
    
    /**
     * Query to get the WTPartUsageLink having the componentId for a given usedBy.
     * @param usedBy
     * @param componentId
     * @return WTPartUsageLink link that has the given componentId for the usedBy object.
    **/
    protected static WTPartUsageLink getUsageLink(final Persistable usedBy, final String componentId) 
        throws WTException {
        QuerySpec spec = new QuerySpec(WTPartUsageLink.class);
        spec.appendWhere(new SearchCondition(WTPartUsageLink.class, "roleAObjectRef.key.id",
                SearchCondition.EQUAL,PersistenceHelper.getObjectIdentifier(usedBy).getId()), 
                new int[] {0});
        spec.appendAnd();
        spec.appendWhere(new SearchCondition(WTPartUsageLink.class, WTPartUsageLink.COMPONENT_ID,
                SearchCondition.EQUAL, componentId), new int[] {0});
        QueryResult result = PersistenceHelper.manager.find((StatementSpec) spec);
        WTPartUsageLink link = null;
        if(result.hasMoreElements()) { // there should be atleast one link
            link = (WTPartUsageLink) result.nextElement();
        }

        return link;
    }
    /**
    * Processes the "csvEndOccurrencedAssemblyAdd" directive in the csv load file.
    * Causes the cached partusagelink to be checked-out, associates soft attribues from preceding
    * <code>IBAValue</code> load file lines with the partusagelink, applies the default attribute values
    * associated with the specified type definition, persists the part, checks it back in,
    * and caches it in the loader's memory.
    * <p>
    * Establishes the part as the CURRENT_CONTENT_HOLDER for use by "ContentFile" lines.
    * <p>
    * @see #beginCreateWTPart
    *
    * @param nv               not used
    * @param cmd_line         command line argument that can contain supplemental load data
    * @param return_objects   <code>Vector</code> of the object(s) created by this method.
    *                         Used by <code>wt.load.StandardLoadService</code>
    *                         for accurate user feedback messages.
    **/
    @SuppressWarnings("unchecked")
	public static boolean endOccurrencedAssemblyAdd(Hashtable nv, Hashtable cmd_line, Vector return_objects) {
        try {
            // Loads standard attribute value(s) on the working part usage link
            final boolean loadValDirectiveUsed = 
                    Boolean.TRUE.equals( MethodContext.getContext().get(LoadAttValues.LOAD_VAL_DIRECTIVE_USED_KEY) );

            // Retrieves working copy of cached usage links to proceed with the logic of applying soft attributes logic
            WTPartUsageLink usageLink = getCachedPartUsage();
            if(usageLink == null) { //this should never happen, safety check
                throw new WTException("For some reason cannot find cached usagelink ");
            }
            
            if ( loadValDirectiveUsed ) { 
                usageLink = (WTPartUsageLink) LoadAttValues.getCurrentTypeManaged();
                usageLink = (WTPartUsageLink)PersistenceHelper.manager.modify(usageLink);
            }          
            
            WTCollection workingUsageLinks =  new WTHashSet(WTHashSet.getInitialCapacity(1));
            workingUsageLinks.add(usageLink);
            workingUsageLinks = IBAValueHelper.service.refreshAttributeContainer(workingUsageLinks);

            // if an IBA attributeContainer already exists then assume that this link is being copied forward and preserve the
            // existing container.
            if (!isAttributeContainerPopulated(usageLink.getAttributeContainer())) {
                usageLink = (WTPartUsageLink) PersistenceHelper.manager.save(usageLink);
            }

            WTPart checkedInPart = usageLink.getUsedBy();
            checkedInPart = (WTPart) WorkInProgressHelper.service.checkin(checkedInPart, "");

            //cache part & usagelink here
            cachePart(checkedInPart); 
            cachePartUsageObject(usageLink);

            // clear ChoiceMappable cache
            cacheExpressionAssignableObject(null);
            cacheChoiceMappableObject(null);
            return true;
        } catch( WTException e ) {
            System.out.println("endAddPartToOccurrencedAssembly: Exception: " + e);
            e.printStackTrace();
        } catch (WTPropertyVetoException wtpve) {
            System.out.println("endAddPartToOccurrencedAssembly: Exception: " + wtpve);
            wtpve.printStackTrace();
        } catch (RemoteException re) {
            System.out.println("endAddPartToOccurrencedAssembly: Exception: " + re);
            re.printStackTrace();
        }
        finally { LoadValue.endIBAContainer(); resetAttDirectiveFlags();}

        return false;

    }

    /**
     * return true if the IBA attribute container is populated with values.
     *
     * @param ac
     * @return boolean
     * @throws WTException
     */
    private static boolean isAttributeContainerPopulated(final AttributeContainer ac) throws WTException {
       return ac != null && ac instanceof DefaultAttributeContainer &&
          ((DefaultAttributeContainer) ac).getAttributeValues() != null &&
          ((DefaultAttributeContainer) ac).getAttributeValues().length > 0;
    }

   /**
    * Processes the "SetAsPrimaryEndItem" directive in the csv load file.
    * When loading into a PDMLinkProduct we set the given End Item Part as the Primary End Item.
    * <p>
    * Fails if we are not loading into a PDMLinkProduct
    * or if there is no End Item Part with the given number in the PDMLinkProduct.
    * <p>
    *
    * @param nv               Name/Value pairs of arguments.
    *                         The arguments are as follows:
    *                         <ul>
    *                            <li>partNumber
    *                         </ul>
    * @param cmd_line         command line argument that can contain supplemental load data
    * @param return_objects   <code>Vector</code> of the object(s) created by this method.
    *                         Used by <code>wt.load.StandardLoadService</code>
    *                         for accurate user feedback messages.
    **/
    public static boolean setAsPrimaryEndItem( Hashtable nv, Hashtable cmd_line, Vector return_objects ) {
      try {
            String number = getValue("partNumber",nv,cmd_line,false);
            WTContainerRef containerRef = LoadServerHelper.getTargetContainer( nv, cmd_line );
          WTContainer container = containerRef.getReferencedContainer();
          if (!(container instanceof PDMLinkProduct)) {
            LoadServerHelper.printMessage("SetAsPrimaryEndItem failed because the target container is not a Product");
            return false;
          }
         PDMLinkProduct product = (PDMLinkProduct) container;
            WTPartMaster partMaster = getMaster(number, containerRef);
            if (partMaster == null) {
                LoadServerHelper.printMessage("SetAsPrimaryEndItem failed because the target Product does not contain a Part with NUMBER=" + number);
            return false;
            } else if (partMaster.isEndItem()) {
            product.setProduct(partMaster);
            product = (PDMLinkProduct) PersistenceHelper.manager.save(product);
            return true;
            } else {
                LoadServerHelper.printMessage("SetAsPrimaryEndItem failed because Part '" + number + "' is not an End Item");
               return false;
            }
      } catch (WTException wte) {
            wte.printStackTrace();
         return false;
      } catch (WTPropertyVetoException wtpve) {
         wtpve.printStackTrace();
         return false;
      }
    }

    /**
   * Processes the "AssemblyAdd" directive in the csv load file.
   * Creates a uses relationship between an assembly part and constituent part.
   * <p>
    * Fails if both parts are not in loader's cache.
    * <p>
   *
    * @see #addPartToAssemblyLoad
    *
   * @param nv               Name/Value pairs of arguments.
   *                         The arguments are as follows:
   *                         <ul>
   *                            <li>assemblyPartNumber
   *                            <li>constitutentPartNumber
   *                            <li>constituentPartQty
   *                            <li>constituentPartUnit
   *                            <li>lineNumber
   *                    <li>findNumber
   *                            <li>occurrenceLocation
   *                            <li>referenceDesignator
   *                         </ul>
   * @param cmd_line         command line argument that can contain supplemental load data
   * @param return_objects   <code>Vector</code> of the object(s) created by this method.
   *                         Used by <code>wt.load.StandardLoadService</code>
   *                         for accurate user feedback messages.
   **/
   public static boolean addPartToAssembly( Hashtable nv, Hashtable cmd_line, Vector return_objects ) {
      return addPartToAssembly(nv,cmd_line,return_objects, false);
   }

   /**
   * Processes the "AssemblyAddLoad" directive in the csv load file.
   * Creates a uses relationship between an assembly part and constituent part.
    * <p>
    * Does not assume either part is in loader's cache.
    * <p>
    *
    * @see #addPartToAssembly
    *
   * @param nv               Name/Value pairs of arguments.
   *                         The arguments are as follows:
   *                         <ul>
   *                            <li>assemblyPartNumber
   *                            <li>constitutentPartNumber
   *                            <li>constituentPartQty
   *                            <li>constituentPartUnit
   *                         </ul>
   * @param cmd_line         command line argument that can contain supplemental load data
   * @param return_objects   <code>Vector</code> of the object(s) created by this method.
   *
   **/
   public static boolean addPartToAssemblyLoad( Hashtable nv, Hashtable cmd_line, Vector return_objects ) {
      return addPartToAssembly(nv,cmd_line,return_objects, false);

   }

   /**
   * Processes the "PartDocReference" directive in the csv load file.
   * Adds a reference document to a part:
   * <p>
    * Optionally checks for parts with versions.
    * Checks cache for part, queries database if not found.
    * If partNumber is not supplied, uses most recently cached part.
    * Checks cache for document, queries database if not found.
    * If docNumber is not supplied, uses most recently cached document.
    * If part and doc exist, creates a document reference link for the part.
    * <p>
   *
    * @see #createPartDocDescribes
    *
   * @param nv               Name/Value pairs of arguments.
   *                         The arguments are as follows: (arguments in &lt;&gt; are optional)
   *                         <ul>
   *                            <li>&lt;docNumber&gt;     (optional)
   *                            <li>&lt;partNumber&gt;    (optional)
   *                            <li>&lt;partVersion&gt;   (optional)
   *                            <li>&lt;partIteration&gt; (optional)
   *                         </ul>
   * @param cmd_line         command line argument that can contain supplemental load data
   * @param return_objects   <code>Vector</code> of the object(s) created by this method.
   *                         Used by <code>wt.load.StandardLoadService</code>
   *                         for accurate user feedback messages.
   **/
   public static boolean createPartDocReference( Hashtable nv, Hashtable cmd_line, Vector return_objects ) {
      return createPartDocLink(nv,cmd_line,return_objects,REFERENCE_LINK,false);
   }

   /**
   * Processes the "PartDocDescribes" directive in the csv load file.
   * Adds a describing document to a part:
   * <p>
    * Optionally checks for parts with versions.
    * Checks cache for part, queries database if not found.
    * If partNumber is not supplied, uses most recently cached part.
    * Optionally checks for documents with versions.
    * Checks cache for document, queries database if not found.
    * If docNumber is not supplied, uses most recently cached document.
    * If part and doc exist, creates a document describes link for the part.
    * <p>
    *
    * @see #createPartDocReference
    * @see wt.doc.LoadDoc
    *
   * @param nv               Name/Value pairs of arguments.
   *                         The arguments are as follows: (arguments in &lt;&gt; are optional)
   *                         <ul>
   *                            <li>&lt;docNumber&gt;     (optional)
    *                            <li>&lt;docVersion&gt;    (optional)
    *                            <li>&lt;docIteration&gt;  (optional)
   *                            <li>&lt;partNumber&gt;    (optional)
   *                            <li>&lt;partVersion&gt;   (optional)
   *                            <li>&lt;partIteration&gt; (optional)
   *                         </ul>
   * @param cmd_line         command line argument that can contain supplemental load data
   * @param return_objects   <code>Vector</code> of the object(s) created by this method.
   *                         Used by <code>wt.load.StandardLoadService</code>
   *                         for accurate user feedback messages.
   **/
   public static boolean createPartDocDescribes(Hashtable nv, Hashtable cmd_line, Vector return_objects) {
      return createPartDocLink(nv,cmd_line,return_objects,DESCRIBES_LINK,false);
   }
    /**
   * Adds documents to parts for PLMLink.
   * <p>
    * Optionally checks for parts with versions.
    * Checks cache for part, queries database if not found.
    * If partNumber is not supplied, uses most recently cached part.
    * Optionally checks for documents with versions.
    * Checks cache for document, queries database if not found.
    * If docNumber is not supplied, uses most recently cached document.
    * If part and doc exist, creates either a document reference link
    * or a document describes link for the part, depending on the soft type
    * of the document.
    * <p>
    *
    * @see wt.doc.LoadDoc
    *
   * @param nv               Name/Value pairs of arguments.
   *                         The arguments are as follows: (arguments in &lt;&gt; are optional)
   *                         <ul>
   *                            <li>&lt;docNumber&gt;     (optional)
    *                            <li>&lt;docVersion&gt;    (optional)
    *                            <li>&lt;docIteration&gt;  (optional)
   *                            <li>&lt;partNumber&gt;    (optional)
   *                            <li>&lt;partVersion&gt;   (optional)
   *                            <li>&lt;partIteration&gt; (optional)
   *                         </ul>
   * @param cmd_line         command line argument that can contain supplemental load data
   * @param return_objects   <code>Vector</code> of the object(s) created by this method.
   *                         Used by <code>wt.load.StandardLoadService</code>
   *                         for accurate user feedback messages.
   **/
   public static boolean createPartDocLink(Hashtable nv, Hashtable cmd_line, Vector return_objects) {
      return createPartDocLink(nv,cmd_line,return_objects,UNKNOWN_LINK,false);
   }

   /**
    * Processes the "csvBeginWTPartDescribeLink" directive in the XML load file.
    * Creates a partdescibelink object, persists it in the database, checks it out,
    * applies the default attribute values associated with the specified type
    * definition, persists the partdescibelink, checks it back in, and caches it in
    * the loader's memory.
    *
    * @see #createPartDocDescribes
    *
    * @param nv               Name/Value pairs of partdescibelink attributes.
    * @param cmd_line         command line argument that can contain supplemental load data
    * @param return_objects   <code>Vector</code> of the object(s) created by this method.
    *                         Used by <code>wt.load.StandardLoadService</code>
    *                         for accurate user feedback messages.
    * @return <table><tr><td>true</td><td>success</td></tr><tr><td>false</td><td>failure</td></tr></table>
    **/
   public static boolean beginWTPartDescribeLink(Hashtable nv, Hashtable cmd_line, Vector return_objects) {
	   // create, persist and cache the partdescibelink
	   boolean success = createPartDocDescribes(nv,cmd_line,return_objects);

	   if (success == false){
		   return success;
	   }

	   // prepare to add soft attributes
	   try {
		   WTPartDescribeLink partDescribes = (WTPartDescribeLink)getCachedPartDocLinks();
		   LoadValue.establishCurrentIBAHolder(partDescribes);
		   LoadValue.beginIBAContainer();
	   } catch (WTException wte) {
		   LoadServerHelper.printMessage("beginWTPartDescribeLink failed");
		   wte.printStackTrace();
		   return false;
	   }

	   return success;
   }

   /**
    * Processes the "csvEndWTPartDescribeLink" directive in the xml load file.
    * Causes the cached partdescibelink to be checked-out, associates soft attribues from preceding
    * <code>IBAValue</code> load file lines with the partdescibelink, applies the default attribute values
    * associated with the specified type definition, persists the part, checks it back in,
    * and caches it in the loader's memory.
    * <p>
    * Establishes the part as the CURRENT_CONTENT_HOLDER for use by "ContentFile" lines.
    * <p>
    *
    * @param nv               not used
    * @param cmd_line         command line argument that can contain supplemental load data
    * @param return_objects   <code>Vector</code> of the object(s) created by this method.
    *                         Used by <code>wt.load.StandardLoadService</code>
    *                         for accurate user feedback messages.
    **/
   public static boolean endWTPartDescribeLink(Hashtable nv, Hashtable cmd_line, Vector return_objects) {
	   try {
		   WTPartDescribeLink partDescribes = (WTPartDescribeLink)getCachedPartDocLinks();
		   partDescribes = (WTPartDescribeLink)LoadValue.applySoftAttributes(partDescribes);
		   return true;
	   } catch( WTException e ) {
		   System.out.println("endWTPartDescribeLink: Exception: " + e);
		   e.printStackTrace();
	   } finally {
		   LoadValue.endIBAContainer();
	   }

	   return false;
   }

   /**
    * Processes the "csvBeginWTPartReferenceLink" directive in the XML load file.
    * Creates a partreferencelink object, persists it in the database, checks it out,
    * applies the default attribute values associated with the specified type
    * definition, persists the partreferencelink, checks it back in, and caches it in
    * the loader's memory.
    *
    * @see #createPartDocReference
    *
    * @param nv               Name/Value pairs of partreferencelink attributes.
    * @param cmd_line         command line argument that can contain supplemental load data
    * @param return_objects   <code>Vector</code> of the object(s) created by this method.
    *                         Used by <code>wt.load.StandardLoadService</code>
    *                         for accurate user feedback messages.
    * @return <table><tr><td>true</td><td>success</td></tr><tr><td>false</td><td>failure</td></tr></table>
    **/
   public static boolean beginWTPartReferenceLink(Hashtable nv, Hashtable cmd_line, Vector return_objects) {
	   // create, persist and cache the partreferencelink
	   boolean success = createPartDocReference(nv,cmd_line,return_objects);

	   if (success == false){
		   return success;
	   }

	   // prepare to add soft attributes
	   try {
		   WTPartReferenceLink partReferences = (WTPartReferenceLink)getCachedPartDocLinks();
		   LoadValue.establishCurrentIBAHolder(partReferences);
		   LoadValue.beginIBAContainer();
	   } catch (WTException wte) {
		   LoadServerHelper.printMessage("beginWTPartReferenceLink failed");
		   wte.printStackTrace();
		   return false;
	   }
	   return success;
   }

   /**
    * Processes the "csvEndWTPartReferenceLink" directive in the xml load file.
    * Causes the cached partreferencelink to be checked-out, associates soft attribues from preceding
    * <code>IBAValue</code> load file lines with the partreferencelink, applies the default attribute values
    * associated with the specified type definition, persists the part, checks it back in,
    * and caches it in the loader's memory.
    * <p>
    * Establishes the part as the CURRENT_CONTENT_HOLDER for use by "ContentFile" lines.
    * <p>
    *
    * @param nv               not used
    * @param cmd_line         command line argument that can contain supplemental load data
    * @param return_objects   <code>Vector</code> of the object(s) created by this method.
    *                         Used by <code>wt.load.StandardLoadService</code>
    *                         for accurate user feedback messages.
    **/
   public static boolean endWTPartReferenceLink(Hashtable nv, Hashtable cmd_line, Vector return_objects) {
	   try {
		   WTPartReferenceLink partReferences = (WTPartReferenceLink)getCachedPartDocLinks();
		   partReferences = (WTPartReferenceLink)LoadValue.applySoftAttributes(partReferences);
		   return true;
	   } catch( WTException e ) {
		   System.out.println("endWTPartReferenceLink: Exception: " + e);
		   e.printStackTrace();
	   } finally {
		   LoadValue.endIBAContainer();
	   }
	   return false;
   }

   /**
   * Processes the "PartRepresentationLoad" directive in the csv load file.
   * Imports the information to create a Representation, and associate it with a part
   * <p>
   * Does not assume either part is in loader's cache.
   * <p>
   * @param nv               Name/Value pairs of arguments.
   *                         The arguments are as follows:
   *                         <ul>
   *                            <li>partNumber
   *                            <li>partVersion
   *                            <li>partIteration
   *                            <li>repName
   *                            <li>repDirectory
   *                            <li>repDefault
   *                            <li>repCreateThumbnail
   *                            <li>repStoreEdz
   *                         </ul>
   * @param cmd_line         command line argument that can contain supplemental load data
   * @param return_objects   <code>Vector</code> of the object(s) created by this method.
   *
   **/
   public static boolean createPartRepresentationLoad(Hashtable nv, Hashtable cmd_line, Vector return_objects) {
      return createPartRepresentation(nv,cmd_line,return_objects,UNKNOWN_LINK,false);
   }

   /**
   * Processes the "PartRepresentation" directive in the csv load file.
   * Imports the information to create a Representation, and associate it with a part
   * @param nv               Name/Value pairs of arguments.
   *                         The arguments are as follows:
   *                         <ul>
   *                            <li>partNumber
   *                            <li>partVersion
   *                            <li>partIteration
   *                            <li>repName
   *                            <li>repDirectory
   *                            <li>repDefault
   *                            <li>repCreateThumbnail
   *                            <li>repStoreEdz
   *                         </ul>
   * @param cmd_line         command line argument that can contain supplemental load data
   * @param return_objects   <code>Vector</code> of the object(s) created by this method.
   *
   **/
   public static boolean createPartRepresentation(Hashtable nv, Hashtable cmd_line, Vector return_objects) {
      return createPartRepresentation(nv,cmd_line,return_objects,UNKNOWN_LINK, true);
   }

   /**
   * Processes the "AssemblyRemove" directive in the csv load file.
   * Removes a uses relationship between an assembly part and constituent part
   *
   * @param nv               Name/Value pairs of arguments.
   *                         The arguments are as follows:
   *                         <ul>
   *                            <li>assemblyPartNumber
   *                            <li>assemblyPartVersion
   *                            <li>constitutentPartNumber
   *                         </ul>
   * @param cmd_line         command line argument that can contain supplemental load data
   * @param return_objects   <code>Vector</code> of the object(s) created by this method.
   *
   **/
   public static boolean removePartFromAssembly(Hashtable nv, Hashtable cmd_line, Vector return_objects) {
      return removePartFromAssembly(nv,cmd_line, return_objects, true);
   }

   /**
   * Processes the "AssemblyRemoveLoad" directive in the csv load file.
   * Removes a uses relationship between an assembly part and constituent part
   * <p>
   * Does not assume either part is in loader's cache.
   * <p>
   *
   * @param nv               Name/Value pairs of arguments.
   *                         The arguments are as follows:
   *                         <ul>
   *                            <li>assemblyPartNumber
   *                            <li>assemblyPartVersion
   *                            <li>constitutentPartNumber
   *                         </ul>
   * @param cmd_line         command line argument that can contain supplemental load data
   * @param return_objects   <code>Vector</code> of the object(s) created by this method.
   *
   **/
   public static boolean removePartFromAssemblyLoad(Hashtable nv, Hashtable cmd_line, Vector return_objects) {
      return removePartFromAssembly(nv,cmd_line, return_objects, false);
   }

   /**
   * Processes the "OccurrenceAssemblyAdd" directive in the csv load file.
   * Creates a uses relationship between an assembly part and constituent part, and then
   * populate occurrence information
   *
   * @see #addPartToAssembly
   *
   * @param nv               Name/Value pairs of arguments.
   *                         The arguments are as follows:
   *                         <ul>
   *                            <li>assemblyPartNumber
   *                            <li>assemblyPartVersion
   *                            <li>constitutentPartNumber
   *                            <li>constituentPartQty
   *                            <li>constituentPartUnit
   *                            <li>lineNumber
   *                    <li>findNumber
   *                            <li>occurrenceLocation
   *                            <li>referenceDesignator
   *                         </ul>
   * @param cmd_line         command line argument that can contain supplemental load data
   * @param return_objects   <code>Vector</code> of the object(s) created by this method.
   *
   **/
   public static boolean addPartToOccurrencedAssembly( Hashtable nv, Hashtable cmd_line, Vector return_objects ) {
      return addPartToAssembly(nv,cmd_line,return_objects, true);
   }

   /**
   * Processes the "OccurrenceAssemblyAddLoad" directive in the csv load file.
   * Creates a uses relationship between an assembly part and constituent part, and then
   * populate occurrence information
   * <p>
   * Does not assume either part is in loader's cache.
   * <p>
   *
   * @see #addPartToAssembly
   *
   * @param nv               Name/Value pairs of arguments.
   *                         The arguments are as follows:
   *                         <ul>
   *                            <li>assemblyPartNumber
   *                            <li>assemblyPartVersion
   *                            <li>constitutentPartNumber
   *                            <li>constituentPartQty
   *                            <li>constituentPartUnit
   *                            <li>lineNumber
   *                    <li>findNumber
   *                            <li>occurrenceLocation
   *                            <li>referenceDesignator
   *                         </ul>
   * @param cmd_line         command line argument that can contain supplemental load data
   * @param return_objects   <code>Vector</code> of the object(s) created by this method.
   *
   **/
   public static boolean addPartToOccurrencedAssemblyLoad( Hashtable nv, Hashtable cmd_line, Vector return_objects ) {
      return addPartToAssembly(nv,cmd_line,return_objects, true);
   }

   /**
   * Processes the "PartDocReference" directive in the csv load file.
   * Adds a reference document to a part:
   * <p>
    * Uses most recently cached part.
    * Checks cache for document, queries database if not found.
    * If docNumber is not supplied, uses most recently cached document.
    * If part and doc exist, creates a document reference link for the part.
    * <p>
    * Kept around for old loaders.  (Does not support versioning.)
   *
    * @deprecated
    * @see #createPartDocReference
    * @see wt.doc.LoadDoc
    *
   * @param nv               Name/Value pair containing the document reference name, docName.
   *                         The arguments are as follows: (arguments in &lt;&gt; are optional)
   *                         <ul>
   *                            <li>&lt;docNumber&gt;     (optional)
    *                         </ul>
   * @param cmd_line         command line argument that can contain supplemental load data
   * @param return_objects   <code>Vector</code> of the object(s) created by this method.
   *                         Used by <code>wt.load.StandardLoadService</code>
   *                         for accurate user feedback messages.
   **/
   public static boolean createPartDocReferenceOld(Hashtable nv, Hashtable cmd_line, Vector return_objects) {
      return createPartDocReference(nv,cmd_line,return_objects);
   }

   /**
   * Processes the "PartDocReference" directive in the csv load file.
   * Adds a reference document to a part:
   * <p>
    * Checks cache for part, fails if not found.
    * If partNumber is not supplied, uses most recently cached part.
    * Checks cache for document, queries database if not found.
    * If docNumber is not supplied, uses most recently cached document.
    * If part and doc exist, creates a document reference link for the part.
    * <p>
   *
    * Kept around for old loaders.  (Does not support versioning.)
   *
    * @deprecated
    * @see #createPartDocReference
    * @see wt.doc.LoadDoc
    *
   * @param nv               Name/Value pair containing the document reference name, docName.
   *                         The arguments are as follows: (arguments in &lt;&gt; are optional)
   *                         <ul>
   *                            <li>&lt;docNumber&gt;     (optional)
   *                            <li>&lt;partNumber&gt;    (optional)
    *                         </ul>
   * @param cmd_line         not used with CounterPart bulk data loader
   * @param return_objects   <code>Vector</code> of the object(s) created by this method.
   *                         Used by <code>wt.load.StandardLoadService</code>
   *                         for accurate user feedback messages.
   **/
   public static boolean createPartDocReferenceLoadOld(Hashtable nv, Hashtable cmd_line, Vector return_objects) {
      return createPartDocLink(nv,cmd_line,return_objects,REFERENCE_LINK,true);
   }

   /**
   * Processes the "ProductUser" directive in the csv load file.
   * Associates a user to a named product in a specified role
   * <p>
   * Assumes user, product and role exists.
   * <p>
   *
   * @param nv               Name/Value pairs of arguments.
   *                         The arguments are as follows:
   *                         <ul>
   *                            <li>user
   *                            <li>name
   *                            <li>role
   *                         </ul>
   * @param cmd_line         command line argument that can contain supplemental load data
   * @param return_objects   <code>Vector</code> not used
   *
   * @deprecated This method and the <csvProductUser> tag are replaced with the <csvAddPrincipalToRole> tag and LoadContainerTeam.addPrincipalToRole() handler.
   **/
   public static boolean assignUserToProduct(Hashtable nv, Hashtable cmd_line, Vector return_objects) {
      try {
         String[] messageArgs = new String[1];
         String message = null;
         String userName = getValue( "user" ,nv ,cmd_line ,true );
         String productName = getValue( "name",nv ,cmd_line ,true );
         String roleStr = getValue( "role" ,nv ,cmd_line ,true );

         WTContainerRef containerRef = LoadServerHelper.getTargetContainer( nv, cmd_line );

         if ( VERBOSE ) {
             System.out.println("In assignUserToProduct");
             System.out.println("<<user>> = <<" + userName + ">>");
             System.out.println("<<name>> = <<" + productName + ">>");
             System.out.println("<<role>> = <<" + roleStr + ">>");
             System.out.println("<<containerRef>> = <<" + containerRef + ">>");
         }

         // Find the User
         WTUser user = null;
         Enumeration eUser = OrganizationServicesHelper.manager.findUser(WTUser.NAME, userName);
         while ( eUser.hasMoreElements() ) {
             user = (WTUser)eUser.nextElement();
             if ( VERBOSE ) System.out.println("Located user " + userName);
         }

         if ( user == null ) {
             messageArgs[0] = userName;
             message = WTMessage.getLocalizedMessage(RESOURCE, partResource.FAILED_LOCATE_USER, messageArgs);
             LoadServerHelper.printMessage("\n" + message);
             return false;
         }

         // Find the Product
         PDMLinkProduct productContainer =  lookupProduct(productName, containerRef);
         if ( productContainer == null ) {
             messageArgs[0] = productName;
             message = WTMessage.getLocalizedMessage(RESOURCE, partResource.FAILED_LOCATE_PRODUCT, messageArgs);
             LoadServerHelper.printMessage("\n" + message);
             return false;
         }
         if ( VERBOSE ) System.out.println("Found  Product " + productContainer);

         ContainerTeam containerTeam = ContainerTeamHelper.service.getContainerTeam(productContainer);
         if ( VERBOSE ) System.out.println("Found  ContainerTeam " + containerTeam);

         // Find the role
         Role role = Role.toRole(roleStr);
         if ( role == null ) {
             messageArgs[0] = roleStr;
             message = WTMessage.getLocalizedMessage(RESOURCE, partResource.FAILED_LOCATE_ROLE, messageArgs);
             LoadServerHelper.printMessage("\n" + message);
             return false;
         }

         // Assign user to Product in the role
         containerTeam.addPrincipal( role, user );
         if ( VERBOSE ) System.out.println("Successfully added " + userName + " to " + productName + " with the role " + roleStr);

         return true;
      }
      catch (WTException e){
         LoadServerHelper.printMessage("\nAssign User to Product Failed: " + e.getLocalizedMessage());
         e.printStackTrace();
         return false;
      }
      catch (Exception e){
         LoadServerHelper.printMessage("\nAssign User to Product Failed: " + e.getMessage());
         e.printStackTrace();
         return false;
      }
   }

   /**
   * Processes the "ViewPreference" directive in the csv load file.
   * Associates a view preference used when creating the default config spec to a user
   * <p>
   * Assumes user, and view exists.
   * <p>
   *
   * @param nv               Name/Value pairs of arguments.
   *                         The arguments are as follows:
   *                         <ul>
   *                            <li>user
   *                            <li>view
   *                         </ul>
   * @param cmd_line         command line argument that can contain supplemental load data
   * @param return_objects   <code>Vector</code> not used
   *
   **/
   public static boolean setUserDefaultView(final Hashtable nv, final Hashtable cmd_line, final Vector return_objects) {
      String currentUser=null;
      try {
         String viewName = LoadServerHelper.getValue("view", nv, cmd_line, LoadServerHelper.REQUIRED);
         String userName = LoadServerHelper.getValue("user", nv, cmd_line, LoadServerHelper.REQUIRED);
         currentUser=wt.session.SessionHelper.getPrincipal().getName();

         LoadServerHelper.changePrincipal(userName);
         View view = ViewHelper.service.getView( viewName );
         if ( view == null ) return false;

         WTPartHelper.service.setDefaultConfigSpecViewPref( view );
         return true;
      }
      catch (WTException e){
         LoadServerHelper.printMessage("\nSetUserdefaultView Failed: " + e.getLocalizedMessage());
         e.printStackTrace();
         return false;
      }
      finally {
          try {
            LoadServerHelper.changePrincipal(currentUser);
          }
          catch (WTException e) {
              e.printStackTrace();
          }
      }
   }
    /**
      * Processes the "SiteViewPreference" directive in the csv load file.
      * Creates a Site level view preference which is used when creating the default config spec for users.
      * This preference is overrideable allow users to set their own view preference.
      * <p>
      * Assumes user, and view exists.
      * <p>
      *
      * @param nv               Name/Value pairs of arguments.
      *                         The arguments are as follows:
      *                         <ul>
      *                            <li>view
      *                         </ul>
      * @param cmd_line         command line argument that can contain supplemental load data
      * @param return_objects   <code>Vector</code> not used
      * @deprecated This method is obsolete now that the preference definition contains a default value for the part structure view.
      *
      **/
      public static boolean setSiteDefaultView(final Hashtable nv, final Hashtable cmd_line, final Vector return_objects) {
         try {
            String viewName = LoadServerHelper.getValue("view", nv, cmd_line, LoadServerHelper.REQUIRED);
            View view = ViewHelper.service.getView( viewName );
            if ( view == null ) {
               if (VERBOSE) System.out.println("View "+viewName+" could not be found");
               return false;
            }

            WTPreferences viewPref = (WTPreferences) WTPreferences.root().node(DEFAULT_VIEW_PREF_NODE);
            viewPref.setEditContext(PreferenceHelper.createEditMask(WTContainerHelper.getExchangeRef(), (WTUser) SessionHelper.getPrincipal(),false));
            if (VERBOSE) System.out.println("Setting Site Default View Preference to " + view.getName());
            viewPref.put(DEFAULT_VIEW_PREF_KEY, view.getName());
             return true;
         }
         catch (WTException e){
            LoadServerHelper.printMessage("\nSetSiteDefaultView Failed: " + e.getLocalizedMessage());
            e.printStackTrace();
            return false;
         }
      }
   public static String getValue( Hashtable nv, String name, boolean required ) throws WTException {
       // THIS METHOD SHOULD BE ELIMINATED
       // IT HAS ONLY BEEN MAINTAINED BECAUSE IT WAS PREVIOUSLY RELEASED
       // AS A "public" METHOD.
      return getValue(name,nv,new Hashtable(),required);
   }
    /**
     * See if checkout is allowed.
     * @param workable Workable
     * @return boolean yes or no
     */
    public static boolean isCheckoutAllowed( Workable workable ) throws LockException, WTException {
       // THIS METHOD SHOULD BE ELIMINATED
       // IT HAS ONLY BEEN MAINTAINED BECAUSE IT WAS PREVIOUSLY RELEASED
       // AS A "public" METHOD.
      return !(WorkInProgressHelper.isWorkingCopy(workable) ||
               WorkInProgressHelper.isCheckedOut(workable) ||
               LockHelper.isLocked(workable));
    }

    /**
     * See if undo-checkout is allowed.
     * @param workable Workable
     * @return boolean yes or no
     */
    public static boolean isUndoCheckoutAllowed( Workable workable ) {
       // THIS METHOD SHOULD BE ELIMINATED
       // IT HAS ONLY BEEN MAINTAINED BECAUSE IT WAS PREVIOUSLY RELEASED
       // AS A "public" METHOD.
       try {
          return WorkInProgressHelper.isWorkingCopy( workable ) &&
                 WorkInProgressHelper.isCheckedOut( workable );
       }
       catch( WTException e ) {
          return false;
       }
    }

    /**
     * check a workable out.
     * @param workable workable
     * @return the checked-out object
     */
     public static Workable getCheckOutObject( Workable workable ) throws LockException, WTException {
       // THIS METHOD SHOULD BE ELIMINATED
       // IT HAS ONLY BEEN MAINTAINED BECAUSE IT WAS PREVIOUSLY RELEASED
       // AS A "public" METHOD.
      Workable retVal = null;
      try {
         if(isCheckoutAllowed(workable)) {
            WorkInProgressHelper.service.checkout(workable,WorkInProgressHelper.service.getCheckoutFolder(),"Updating attributes during load.");
            retVal = WorkInProgressHelper.service.workingCopyOf(workable);
         }
      }
      catch ( Exception e ) {
         e.printStackTrace();
         throw new WTException(e.getMessage());
      }
      if(retVal==null) throw new WTException("Checkout Failed!");
      return retVal;
     }
    /////////////////////
    // PRIVATE METHODS //
    /////////////////////
   protected static boolean createPartObject( Hashtable nv, Hashtable cmd_line, Vector return_objects) {
      try {
         setUser(nv,cmd_line);
         WTPart part = constructPart(nv,cmd_line);
         part = cachePart(part);

         return true;
      }
      catch (WTException wte){
         LoadServerHelper.printMessage("\nCreate Part Failed (" + getDisplayInfo(nv,cmd_line) + "): " + wte.getLocalizedMessage());
         wte.printStackTrace();
      }
      catch (Exception e){
         LoadServerHelper.printMessage("\nCreate Part Failed (" + getDisplayInfo(nv,cmd_line) + "): " + e.getMessage());
         e.printStackTrace();
      }
      return false;
   }

   protected static String getDisplayInfo(Hashtable nv, Hashtable cmd_line) {
      String number = null;
      String version = null;
      String iteration = null;
      String view = null;
      try {
         number = getValue("number",nv,cmd_line,false);
         if (number == null)
            number = getValue("partNumber",nv,cmd_line,false);
         version = getValue("version",nv,cmd_line,false);
         iteration = getValue("iteration",nv,cmd_line,false);
         view = getValue("view",nv,cmd_line,false);
      }
      catch (WTException wte) {
         // This is only used in exception cases to display more info so this shouldn't throw a new one
         // it should just move on valiantly defaulting the the value that has a problem.
      }
      if (number == null) number = "<no number>";
      if (version == null) version = "<version>";
      if (iteration == null) iteration = "<iteration>";
      if (view == null) view = "<view>";
      return number + " " + version + "." + iteration + "," + view;
   }

   protected static boolean updatePartObject( Hashtable nv, Hashtable cmd_line, Vector return_objects) {
      try {
         WTPart part;

         final boolean loadValDirectiveUsed = Boolean.TRUE.equals( MethodContext.getContext().get(LoadAttValues.LOAD_VAL_DIRECTIVE_USED_KEY) );
         if ( loadValDirectiveUsed )
         {
             part = (WTPart) LoadAttValues.getCurrentTypeManaged();
             part = (WTPart)PersistenceHelper.manager.modify(part);
             if ( logger.isDebugEnabled() )
                 logger.error( "new soft att directive was used for part: " + part.getName() );
         }
         else
         {
         part = getPart();
             if ( logger.isDebugEnabled() )
                 logger.error( "new soft att value directive was *not* used for part: " + part.getName() );
         }

         final boolean ibaValDirectiveUsed = Boolean.TRUE.equals( MethodContext.getContext().get(LoadValue.IBA_VAL_DIRECTIVE_USED_KEY) );
         if ( ibaValDirectiveUsed )
         {
         part = (WTPart)LoadValue.applySoftAttributes(part);
             if ( logger.isDebugEnabled() )
                 logger.debug( "old iba att value directive was used for part: " + part.getName() );
         }
         else
         {
             if ( logger.isDebugEnabled() )
                 logger.debug( "old iba att value directive was *not* used for part: " + part.getName() );
         }

         if ( loadValDirectiveUsed || ibaValDirectiveUsed )
         {
         part = cachePart(part);
         wt.fc.PersistenceHelper.manager.save(part.getMaster());
         }
         // else {// nothing was changed in here so skip these db calls }

         return_objects.addElement(part);

         return true;
      }
      catch (WTException wte){
         LoadServerHelper.printMessage("\nUpdate Part Failed: " + wte.getLocalizedMessage());
         wte.printStackTrace();
      }
      catch (Exception e){
         LoadServerHelper.printMessage("\nUpdate Part Failed: " + e.getMessage());
         e.printStackTrace();
      }
      finally{
            try{
                resetUser();
            }
            catch( WTException e ) {
                LoadServerHelper.printMessage("\nUpdate Part Failed: " + e.getMessage());
            }
            resetAttDirectiveFlags();
      }
      return false;
   }

   private static void resetAttDirectiveFlags() {
       // clean up these flags once we're done processing the soft attributes on this instance
       final MethodContext mc = MethodContext.getContext();
       mc.remove( LoadAttValues.LOAD_VAL_DIRECTIVE_USED_KEY );
       mc.remove( LoadValue.IBA_VAL_DIRECTIVE_USED_KEY );
   }

   protected static Object getFirstIterationOf(Mastered master) throws WTException {
      Class iterated_class = master.getClassInfo().getOtherSideRole(wt.vc.MasterIteration.ITERATION_ROLE).getValidClassInfo().getBusinessClass();

      QuerySpec query = new QuerySpec(iterated_class);
      query.appendWhere(VersionControlHelper.getSearchCondition(iterated_class, master), new int[] { 0 });
      query.appendAnd();
      query.appendWhere(new SearchCondition( iterated_class,
         Iterated.ITERATION_INFO + "." + IterationInfo.PREDECESSOR + "." + ObjectReference.KEY +".id",
         SearchCondition.EQUAL,
         new Long(0) ));
      QueryResult qr = PersistenceHelper.manager.find(query);
      if(qr.size() == 0) {
         return null;
      }
      return qr.nextElement();
   }
   protected static boolean addPartToAssembly( Hashtable nv, Hashtable cmd_line, Vector return_objects, boolean occurrenced) {
      TypeIdentifier PartUsageLink_TI = null;

      try {
         String[] messageArgs = new String[2];
         String message;

         String assemblyPartVersion = getValue("assemblyPartVersion",nv,cmd_line,false);
         String assemblyPartIteration = getValue("assemblyPartIteration",nv,cmd_line,false);
         String assemblyPartView = getValue("assemblyPartView",nv,cmd_line,false);
         String assemblyPartVariation1 = getValue("assemblyPartVariation1",nv,cmd_line,false);
         String assemblyPartVariation2 = getValue("assemblyPartVariation2",nv,cmd_line,false);
         String assemblyOrgName = getValue("organizationName",nv,cmd_line,false);
         String assemblyOrgID = getValue("organizationID",nv,cmd_line,false);

         String componentId = getValue("componentId",nv,cmd_line,false);
         String inclusionOption = getValue("inclusionOption",nv,cmd_line,false);
         String quantityOption = getValue("quantityOption",nv,cmd_line,false);
         String reference = getValue("reference",nv,cmd_line,false);

         if ( assemblyPartVersion != null && assemblyPartVersion.equals("") ) assemblyPartVersion = null;
         if ( assemblyPartIteration != null && assemblyPartIteration.equals("") ) assemblyPartIteration = null;
         if ( assemblyPartView != null && assemblyPartView.equals("") ) assemblyPartView = null;
         if ( assemblyPartVariation1 != null && assemblyPartVariation1.equals("") ) assemblyPartVariation1 = null;
         if ( assemblyPartVariation2 != null && assemblyPartVariation2.equals("") ) assemblyPartVariation2 = null;
         if ( assemblyOrgName != null && assemblyOrgName.equals("")) assemblyOrgName = null;
         if ( assemblyOrgID != null && assemblyOrgID.equals("")) assemblyOrgID = null;

         String assemblyPartNumber = getValue("assemblyPartNumber",nv,cmd_line,true);

         WTPart assemblyPart = getPart(assemblyPartNumber, assemblyPartVersion, assemblyPartIteration, assemblyPartView,
                                       assemblyPartVariation1, assemblyPartVariation2, assemblyOrgID, assemblyOrgName);
         if(assemblyPart==null) {
            messageArgs[0] = getValue("assemblyPartNumber",nv,cmd_line,true);
            message = WTMessage.getLocalizedMessage(RESOURCE, partResource.LOAD_NO_PART, messageArgs);
            LoadServerHelper.printMessage(message);
         }
         else {
            WTPartMaster constituentPart = getMaster(getValue("constituentPartNumber",nv,cmd_line,true));

            if(constituentPart==null) {
               messageArgs[0] = getValue("constituentPartNumber",nv,cmd_line,true);
               message = WTMessage.getLocalizedMessage(RESOURCE, partResource.LOAD_NO_PART, messageArgs);
               LoadServerHelper.printMessage(message);
            }
            else {
               // check to see if child is the same as one of its parent
               if (isChildSameAsParent(assemblyPart, constituentPart)) {
                  messageArgs[0] = constituentPart.getIdentity();
                  message = WTMessage.getLocalizedMessage(RESOURCE, partResource.CAN_NOT_ADD_CHILD_TO_SELF, messageArgs);
                  LoadServerHelper.printMessage(message);
                  return false;
               }

               //check for association constraint
               TypeIdentifier roleA_ti = TypeIdentifierHelper.getType(assemblyPart);
               PartUsageLink_TI = WTPartHelper.service.getValidUsageLinkType(roleA_ti);
               if (!AssociationConstraintHelper.service.isValidAssociation(assemblyPart, PartUsageLink_TI, constituentPart)) {
                  Object roleB_obj = getFirstIterationOf(constituentPart);
                  Object[]params = {assemblyPart.getIdentity(), constituentPart.getIdentity(), roleA_ti.getTypename(),TypeIdentifierHelper.getType(roleB_obj).getTypename()};
                  message = WTMessage.getLocalizedMessage(RESOURCE, partResource.INVALID_ROLE_B_TYPE, params);
                  LoadServerHelper.printMessage(message);
                  return false;
               }


               //Create and persist uses relationship between assembly part and constituent part.
               WTPartUsageLink uses = WTPartUsageLink.newWTPartUsageLink(assemblyPart,constituentPart);
               uses.setTypeDefinitionReference(TypedUtility.getTypeDefinitionReference(PartUsageLink_TI.getTypename()));

               // Validate that the link role objects are in valid container
               WTCollection linkCollection = new WTHashSet(WTHashSet.getInitialCapacity(1));
               linkCollection.add(uses);
               StructServerHelper.validateRoleObjectContainers(linkCollection);

               double constituentPartQty = Double.valueOf(getValue("constituentPartQty",nv,cmd_line,true)).doubleValue();
               QuantityUnit constituentPartUnit = QuantityUnit.toQuantityUnit(getValue("constituentPartUnit",nv,cmd_line,true));
               if (QuantityUnit.EA.equals(constituentPartUnit) && constituentPartQty == 0) {
                  messageArgs[0] = constituentPart.getIdentity();
                  messageArgs[1] = QuantityUnit.EA.getDisplay();
                  message = WTMessage.getLocalizedMessage(RESOURCE, partResource.CANNOT_ADD_QTY_ZERO, messageArgs);
                  LoadServerHelper.printMessage(message);
                  return false;
               }
               uses.setQuantity(Quantity.newQuantity(constituentPartQty, constituentPartUnit));

               // the componentId is a private field
               // Link does not have a setComponentId method
               // hence use reflection to set the componentId
               if( componentId!=null && !componentId.equals("")) {
                   boolean fieldFound = false;

                   // first check WTPartUsageLink
                   // this is the correct for location until X20 annotations
                   Field[] declaredFields = uses.getClass().getSuperclass().getDeclaredFields();
                   for (Field field : declaredFields) {
                       if (field.getName().equals("componentId")) {
                           field.setAccessible(true);
                           field.set(uses, componentId);
                           fieldFound = true;
                           break;
                       }
                   }

                   // next check the parent class because
                   // annotations code generation in X20
                   // moved fields to the parent class
                   if (!fieldFound) {
                       declaredFields = uses.getClass().getSuperclass().getDeclaredFields();
                       for (Field field : declaredFields) {
                           if (field.getName().equals("componentId")) {
                               field.setAccessible(true);
                               field.set(uses, componentId);
                               fieldFound = true;
                               break;
                           }
                       }
                   }
               }

               if(inclusionOption!=null && ! inclusionOption.equals(""))
                  uses.setInclusionOption(inclusionOption);

               if(quantityOption!=null && !quantityOption.equals(""))
                  uses.setQuantityOption(quantityOption);

               if(reference!=null && ! reference.equals(""))
                  uses.setReference(reference);

               String lineNo = getValue("lineNumber", nv, cmd_line, false);
               if ( lineNo != null && ! lineNo.equals("") ) {
                  LineNumber lineNumber = LineNumber.newLineNumber( Long.valueOf( lineNo ).longValue() );
                  uses.setLineNumber( lineNumber );
               }

               String findNo = getValue("findNumber", nv, cmd_line, false);
               if ( findNo != null && ! findNo.equals("") ) {
                  uses.setFindNumber( findNo );
               }

               PersistenceServerHelper.manager.insert(uses);
               cachePartUsageObject(uses);
               // comment this out until WTPartUsageLink implements ObjectMappable
               cacheChoiceMappableObject(uses);
               cacheExpressionAssignableObject(uses);

               if ( occurrenced ) {
//             If each, and occurrence information specified, create occurrences
                  if (QuantityUnit.EA.equals(constituentPartUnit)) {
                     int noOccur = Double.valueOf(constituentPartQty).intValue();
                     //System.out.println("Need to create occurences " + noOccur);

                     String[] locations =  getValues("occurrenceLocation",nv,cmd_line,false);
                     String[] designators=  getValues("referenceDesignator",nv,cmd_line,false);

                     if ( locations != null || designators != null ) {
                        String location = "";
                        String designator = "";

                        for ( int i=0; i < noOccur; i++) {
                           if ( locations != null && i < locations.length  ) {
                              location = locations[i];
                           }
                           else {
                              location = "";
                           }

                           if ( designators != null && i < designators.length  ) {
                              designator = designators[i];
                           }
                           else {
                              designator = "";
                           }

                           //System.out.println("Occurence " + i + " location " + location);
                           if ( location == null ) location  = "";

                           PartUsesOccurrence puo = PartUsesOccurrence.newPartUsesOccurrence(uses);
                           puo.setName(designator);

                           puo.setTransform( getLocationMatrix( location ) );
                           Vector data = null;

                           try {
                              OccurrenceHelper.service.setSkipValidation(true);
                           } catch(Exception e) { e.printStackTrace(); }

                           try {
                                 //insert into db only designator is not null
                                 if ( designator != null && !designator.equals("")) {
                                     QueryResult qr = OccurrenceHelper.service.saveUsesOccurrenceAndData( puo, data);
                                 }
                              //System.out.println("Saved location");
                           }
                           finally {
                              try {
                                 OccurrenceHelper.service.setSkipValidation(false);
                              } catch(Exception e) { e.printStackTrace(); }
                           }
                        }
                     }
                  }
               }

               messageArgs[0] = constituentPart.getIdentity();
               messageArgs[1] = assemblyPart.getIdentity();
               message = WTMessage.getLocalizedMessage(RESOURCE, partResource.LOAD_PARTS_ADDED, messageArgs);
               return_objects.addElement(message);
               return true;
            }
         }
      }
      catch (WTException wte){
         LoadServerHelper.printMessage("\naddPartToAssembly: " + wte.getLocalizedMessage());
         wte.printStackTrace();
      }
      catch (Exception e){
         LoadServerHelper.printMessage("\naddPartToAssembly: " + e.getMessage());
         e.printStackTrace();
      }
      return false;
   }
   protected static boolean createPartDocLink( Hashtable nv, Hashtable cmd_line, Vector return_objects, int type, boolean cached_only ) {
      try {
         String[] messageArgs = new String[2];
         String message;

         String partNumber = getValue("partNumber",nv,cmd_line,false);
         String partVersion = getValue("partVersion",nv,cmd_line,false);
         String partIteration = getValue("partIteration",nv,cmd_line,false);
         String partView = getValue("partView",nv,cmd_line,false);
         String partVariation1 = getValue("partVariation1",nv,cmd_line,false);
         String partVariation2 = getValue("partVariation2",nv,cmd_line,false);
         String orgName = getValue("organizationName",nv,cmd_line,false);
         String orgID = getValue("organizationID",nv,cmd_line,false);

         if( partView != null && partView.equals("") ) partView = null;
         if( partVariation1 != null && partVariation1.equals("") ) partVariation1 = null;
         if( partVariation2 != null && partVariation2.equals("") ) partVariation2 = null;
         if ( orgName != null && orgName.equals("")) orgName = null;
         if ( orgID != null && orgID.equals("")) orgID = null;

         WTPart part = getPart(partNumber, partVersion, partIteration, partView, partVariation1, partVariation2, orgID, orgName);

         if(part==null) {
            message = WTMessage.getLocalizedMessage(RESOURCE,partResource.LOAD_NO_DOC_PART,messageArgs);
            LoadServerHelper.printMessage(message);
         }
         else {
            String docNumber = getValue("docNumber",nv,cmd_line,false);
            String docVersion = getValue("docVersion",nv,cmd_line,false);
            String docIteration = getValue("docIteration",nv,cmd_line,false);

            WTDocument document = LoadDoc.getDocument(docNumber,docVersion,docIteration);
            if(document==null) {
               LoadServerHelper.printMessage("Document not found");
            }
            else {
               Persistable link = null;
               boolean isDuplicateDescribeLink = isDuplicatePartDocRecord(part, document);

               switch(type) {
                  case REFERENCE_LINK:
                     if (PartDocHelper.isWcPDMMethod() || PartDocHelper.isReferenceDocument(document)) {
                        link = WTPartReferenceLink.newWTPartReferenceLink(part,(WTDocumentMaster)document.getMaster());
                     } else {
                        logger.error("Cannot load a part reference link:");
                        logger.error("  part[" + part + "], document[" + document + "]");
                        logger.error("since the specified document is not a reference document");
                        throw new WTException("Not a reference document: number[" + document.getNumber() + ", name[" + document.getName() + "]");
                     }
                     break;
                  case DESCRIBES_LINK:
                    boolean removeOldDocRevisions = false;
                    String docRemoveOldRev = getValue("docRemoveOldRev",nv,cmd_line,false);
                    if(docRemoveOldRev != null && docRemoveOldRev.length() > 0) {
                       removeOldDocRevisions = new Boolean(docRemoveOldRev).booleanValue();
                    }
                    if(removeOldDocRevisions) {
                       WTCollection parts = new WTArrayList();
                       parts.add(part);
                       WTKeyedMap resultMap = PartDocHelper.service.getAssociatedDescribeDocuments(parts);
                       if(resultMap != null) {
                          WTCollection docs = (WTCollection)resultMap.get(part);
                          if(docs != null) {
                             for(Iterator it = docs.persistableIterator(); it.hasNext();) {
                                WTDocument relatedDoc = (WTDocument) it.next();
                                String relatedDocNumber = relatedDoc.getNumber();
                                if(relatedDocNumber.equalsIgnoreCase(docNumber)) {
                                   PartDocHelper.service.deletePartDocRelation(part, relatedDoc);
                                }
                             }
                          }
                       }
                    }
                // Fix for SPR#1569219
                    /* A check to determine if the ROLEA_OBJECT_ID (WTPart)and ROLEB_OBJECT_ID (WTDocument) are already present
                     * in the WTPARTDESCRIBELINK table. This redundancy may be caused due to multiple execution of the same
                     * .csv file from LoadFromFile Utility (during importData operation)*/
                     if(!isDuplicateDescribeLink){
                      link = WTPartDescribeLink.newWTPartDescribeLink(part,document);
                     }else{
                      logger.error("Part-Doc describe link from [" + part.getNumber() + " - " + part.getName() + " ] to " + "["+ document.getNumber()+ " - " + document.getName()+" ] already exists." );
                      return true;
                     }

                     break;
                  default:
                        //Would like to use createPartDocRelation, it requires a check-out to work.
                        //part = PartDocHelper.service.createPartDocRelation(part, doc);

                        //THE STRING IN THE FOLLOWING LINE SHOULD NOT BE HARDCODED!!!
                        String refDocPersistedType = TypedUtility.getPersistedType("WCTYPE|wt.doc.WTDocument|com.ptc.ReferenceDocument");
                        if(refDocPersistedType==null){
                            System.out.println(" WARNING - WCTYPE|wt.doc.WTDocument|Reference Document is not defined.\n Run loaddata.bat PLMLinkPartDocs.csv");
                        }
                        if(TypedUtility.isInstanceOf(TypedUtility.getPersistedType(document),refDocPersistedType)) {
                        link = WTPartReferenceLink.newWTPartReferenceLink(part,(WTDocumentMaster)document.getMaster());
                        } else {

                        	if(!isDuplicateDescribeLink){
                        		link = WTPartDescribeLink.newWTPartDescribeLink(part,document);
                        	}else{
                                logger.error("Part-Doc describe link from [" + part.getNumber() + " - " + part.getName() + " ] to " + "["+ document.getNumber()+ " - " + document.getName()+" ] already exists." );
                                return true;
                            }
                        }
               }
               PersistenceServerHelper.manager.insert(link);
               link = cachePartDocLinkObject(link);
               messageArgs[0] = document.getIdentity();
               messageArgs[1] = part.getIdentity();
               message = WTMessage.getLocalizedMessage(RESOURCE,partResource.LOAD_DOC_ADDED,messageArgs);
               return_objects.addElement(message);
               return true;
            }
          }
      }
      catch (WTException wte){
         LoadServerHelper.printMessage("\ncreatePartDocLink: " + wte.getLocalizedMessage());
         wte.printStackTrace();
      }
      catch (Exception e) {
         LoadServerHelper.printMessage("\ncreatePartDocLink: " + e.getMessage());
         e.printStackTrace();
      }

      return false;
   }

   protected static boolean removePartFromAssembly( Hashtable nv, Hashtable cmd_line, Vector return_objects, boolean cached_only ) {
      try {
         String[] messageArgs = new String[2];
         String message;

         String assemblyPartNumber = getValue("assemblyPartNumber",nv,cmd_line,true);
         String assemblyPartVersion = getValue("assemblyPartVersion",nv,cmd_line,false);
         String assemblyPartIteration = getValue("assemblyPartIteration",nv,cmd_line,false);
         String assemblyPartView = getValue("assemblyPartView",nv,cmd_line,false);
         String assemblyPartVariation1 = getValue("assemblyPartVariation1",nv,cmd_line,false);
         String assemblyPartVariation2 = getValue("assemblyPartVariation2",nv,cmd_line,false);
         String assemblyOrgName = getValue("organizationName",nv,cmd_line,false);
         String assemblyOrgID = getValue("organizationID",nv,cmd_line,false);

         if ( assemblyPartVersion != null && assemblyPartVersion.equals("") ) assemblyPartVersion = null;
         if ( assemblyPartIteration != null && assemblyPartIteration.equals("") ) assemblyPartIteration = null;
         if ( assemblyPartView != null && assemblyPartView.equals("") ) assemblyPartView = null;
         if ( assemblyPartVariation1 != null && assemblyPartVariation1.equals("") ) assemblyPartVariation1 = null;
         if ( assemblyPartVariation2 != null && assemblyPartVariation2.equals("") ) assemblyPartVariation2 = null;
         if ( assemblyOrgName != null && assemblyOrgName.equals("")) assemblyOrgName = null;
         if ( assemblyOrgID != null && assemblyOrgID.equals("")) assemblyOrgID = null;

         WTPart assemblyPart = getPart(assemblyPartNumber, assemblyPartVersion, assemblyPartIteration, assemblyPartView,
                                       assemblyPartVariation1, assemblyPartVariation2, assemblyOrgID, assemblyOrgName);
         if(assemblyPart==null) {
            messageArgs[0] = getValue("assemblyPartNumber",nv,cmd_line,true);
            message = WTMessage.getLocalizedMessage(RESOURCE, partResource.REMOVE_NO_PART, messageArgs);
            LoadServerHelper.printMessage(message);
         }
         else {
            WTPartMaster constituentPart = getMaster(getValue("constituentPartNumber",nv,cmd_line,true));

            if(constituentPart==null) {
               messageArgs[0] = getValue("constituentPartNumber",nv,cmd_line,true);
               message = WTMessage.getLocalizedMessage(RESOURCE, partResource.REMOVE_NO_PART, messageArgs);
               LoadServerHelper.printMessage(message);
            }
            else {
               try
               {
                   QueryResult qr = null;

                   qr = StructHelper.service.navigateUses(assemblyPart, false);
                   // Make sure usage links are populated with their uses occurrences
                   Vector links = OccurrenceHelper.service.getPopulatedOccurrenceableLinks(qr.getObjectVectorIfc().getVector());

                   for(Iterator i = links.iterator(); i.hasNext();)
                   {
                       WTPartUsageLink link = (WTPartUsageLink)i.next();
                       WTPartMaster childMaster = (WTPartMaster)link.getRoleBObject();
                       if ( childMaster.getNumber().equals( constituentPart.getNumber() ) ) {
                          try
                          {
                             // Collect any associated PartUsesOccurrences and PartPathOccurrences
                             Vector<PartUsesOccurrence> usesOccurrences = link.getUsesOccurrenceVector();
                             if (usesOccurrences == null) {
                                usesOccurrences = new Vector<PartUsesOccurrence>();
                             }
                             WTSet wtUsesOccurrences = new WTHashSet(usesOccurrences);
                             WTSet wtPathOccurrences = new WTHashSet();
                             for (PartUsesOccurrence usesOccurrence : usesOccurrences) {
                                PartPathOccurrence pathOccurrence = (PartPathOccurrence)usesOccurrence.getPathOccurrence();
                                if (pathOccurrence != null) {
                                   wtPathOccurrences.add(pathOccurrence);
                                }
                             }
                             // Delete PathOccurrences first then UsesOccurrences to avoid referential integrity issues
                             PersistenceServerHelper.manager.remove(wtPathOccurrences);
                             PersistenceServerHelper.manager.remove(wtUsesOccurrences);
                             // Refresh the link in case its been updated for quantity and then delete the link
                             link = (WTPartUsageLink) PersistenceServerHelper.manager.restore(link);
                             PersistenceServerHelper.manager.remove(link);
                             assemblyPart = cachePart(assemblyPart);
                             return true;
                          }
                          catch (WTException e)
                          {
                             e.printStackTrace();
                          }
                       }
                   }
                   throw new RuntimeException( "Cannot remove usage link because no usage link exists from "
                           + IdentityFactory.getDisplayIdentity( assemblyPart )
                           + " to " + IdentityFactory.getDisplayIdentity( constituentPart ) );
               }
               catch ( Exception ee ) {
                    ee.printStackTrace();
               }
            }
         }
      }
      catch ( Exception e ) {
         e.printStackTrace();
      }
      return false;
   }

   /**
    * Processes the "NewViewVersion" directive in the csv load file.
    * <p>
    * Creates a new revision of a part from an existing part and assigns it to
    * the specified view (and optionally variations if they are enabled).  The
    * new view must be a child view of the view the original was assigned to
    * (or if view variations are enabled it can be a sibling view or the same
    * view with different variations).  For example, the part could have been
    * created in the Design view, and this method can be used to create a new
    * revision of the part in the Manufacturing view.
    * <p>
   * When using {@link wt.load.LoadFromFile wt.load.LoadFromFile} to create a new view version of a part,
   * use the following XML format:
   * <p>
   * <h2>XML Format</h2>
   * <p>
   * <TABLE WIDTH="100%" BORDER=0>
   * <TR><TD COLSPAN=4>&lt;csvNewViewVersion handler="wt.part.LoadPart.createNewViewVersion" &gt;</TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvpartName&gt;           </TD><TD><I>part name                </I></TD><TD>&lt;/csvpartName&gt;            </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvpartNumber&gt;         </TD><TD><I>part number              </I></TD><TD>&lt;/csvpartNumber&gt;          </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvpartVersion&gt;            </TD><TD><I>part revision                 </I></TD><TD>&lt;/csvpartVersion&gt;             </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvpartIteration&gt;          </TD><TD><I>part iteration                </I></TD><TD>&lt;/csvpartIteration&gt;           </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvpartView&gt;           </TD><TD><I>part view                </I></TD><TD>&lt;/csvpartView&gt;            </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvpartVariation1&gt;     </TD><TD><I>part variation1          </I></TD><TD>&lt;/csvpartVariation1&gt;      </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvpartVariation2&gt;     </TD><TD><I>part variation2          </I></TD><TD>&lt;/csvpartVariation2&gt;      </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvtype&gt;               </TD><TD><I>assembly mode            </I></TD><TD>&lt;/csvtype&gt;                </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvgenericType&gt;        </TD><TD><I>generic type             </I></TD><TD>&lt;/csvgenericType&gt;                </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvlogicbasePath&gt;      </TD><TD><I>logic base path          </I></TD><TD>&lt;/csvlogicbasePath&gt;                </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvsource&gt;             </TD><TD><I>source                   </I></TD><TD>&lt;/csvsource&gt;              </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvfolder&gt;             </TD><TD><I>folder                   </I></TD><TD>&lt;/csvfolder&gt;              </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvlifecycle&gt;          </TD><TD><I>lifecycle                </I></TD><TD>&lt;/csvlifecycle&gt;           </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvview&gt;               </TD><TD><I>view                     </I></TD><TD>&lt;/csvview&gt;                </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvvariation1&gt;         </TD><TD><I>variation1               </I></TD><TD>&lt;/csvvariation1&gt;          </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvvariation2&gt;         </TD><TD><I>variation2               </I></TD><TD>&lt;/csvvariation2&gt;          </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvteamTemplate&gt;       </TD><TD><I>team template            </I></TD><TD>&lt;/csvteamTemplate&gt;        </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvlifecyclestate&gt;     </TD><TD><I>lifecycle state          </I></TD><TD>&lt;/csvlifecyclestate&gt;      </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvtypedef&gt;            </TD><TD><I>typedef                  </I></TD><TD>&lt;/csvtypedef&gt;             </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvversion&gt;            </TD><TD><I>revision                 </I></TD><TD>&lt;/csvversion&gt;             </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csviteration&gt;          </TD><TD><I>iteration                </I></TD><TD>&lt;/csviteration&gt;           </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvparentContainerPath&gt;</TD><TD><I>parent container path    </I></TD><TD>&lt;/csvparentContainerPath&gt; </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvorganizationName&gt;   </TD><TD><I>organization name        </I></TD><TD>&lt;/csvorganizationName&gt; </TD></TR>
   * <TR><TD WIDTH="15%">&nbsp;</TD><TD>&lt;csvorganizationID&gt;     </TD><TD><I>organization id          </I></TD><TD>&lt;/csvorganizationID&gt; </TD></TR>
   * <TR><TD COLSPAN=4>&lt;/csvNewViewVersion&gt;</TD></TR>
   * </TABLE>
   * <p>
   * <h2>Tag Definitions</h2>
   * <p>
   * <TABLE WIDTH="100%" BORDER=0>
    *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>part name                </I></TD>
   *       <TD>
   *                 The name of the part. This value is ignored, although the tag is required.
   *                 The name used for the part in the new view is the same as the original part name.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>part number              </I></TD>
   *       <TD>
   *                 The number of the part. A value is required for this attribute and must
   *                 be the number of the part for which a new view version is being created.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>part revision                  </I></TD>
   *       <TD>
   *                 The revision of the existing part to locate and create the new view version from.
   *                 This value is optional and defaults to the latest revision of the part.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>part iteration                </I></TD>
   *       <TD>
   *                 The iteration of the existing part to locate and create the new view version from.
   *                 This value is optional and defaults to the latest iteration of the revision of the part.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>part view                </I></TD>
   *       <TD>
   *                 The view of the existing part to locate and create the new view version from.
   *                 This value is optional and defaults to the an ancestor view of the new "view" passed in.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>part variation1                </I></TD>
   *       <TD>
   *                 The variation1 of the existing part to locate and create the new view version from.
   *                 This value is optional and defaults to null.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>part variation2                </I></TD>
   *       <TD>
   *                 The variation2 of the existing part to locate and create the new view version from.
   *                 This value is optional and defaults to null.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>assembly mode            </I></TD>
   *       <TD>
   *                 The assembly mode for the new part version.   A value is required for this attribute and must be one of the following:<p>
   *                 <ul>
   *                   <li><code>separable</code></li>
   *                   <li><code>inseparable</code></li>
   *                   <li><code>component</code></li>
   *                 </ul>
   *       </TD>
   *    </TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>generic type            </I></TD>
   *       <TD>
   *                 The generic type of the new part.  This tag is optional. If the tag is used and a value specified, the value must be one of the following:<p>
   *                 <ul>
   *                   <li><code>standard</code></li>
   *                   <li><code>generic</code></li>
   *                   <li><code>configurable_generic</code></li>
   *                   <li><code>variant</code></li>
   *                 </ul>
   *                If the tag is not used or used but left blank, the default value is <code>standard</code>.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>logic base path            </I></TD>
   *       <TD>
   *                Path to an XML file with the logic base information for a generic or configurable_generic part.
   *                The path is given relative to the location of the file in which the attribute is used.
   *                This tag is optional, and if not used or used but left blank, there is no logic base
   *                information associated with the generic or configurable generic part.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>source                   </I></TD>
   *       <TD>
   *                 The source of the new part version.  A value is required for this attribute and must be one of the following:
   *                 <ul>
   *                   <li><code>buy</code></li>
   *                   <li><code>make</code></li>
   *                   <li><code>singlesource</code></li>
   *                 </ul>
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I> folder                  </I></TD>
   *       <TD>
   *                 The full path to the folder in which the new part version is created,  i.e. <code>/Default/Design</code>.
   *                 This value is optional and will default to <code>/Default</code>.  An error message that
   *                 this is a required field may appear if left blank, but this message can be ignored.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>lifecycle                </I></TD>
   *       <TD>
   *                 The lifecycle that the new part version will be created under.  This value is optional and defaults to <code>Basic</code>.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>view                     </I></TD>
   *       <TD>
   *                 The view that the new part version will be created under, i.e. <code>Design, Manufacturing</code>.
   *                 This value is optional.  Leave this value blank for serial numbered parts.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>variation1                     </I></TD>
   *       <TD>
   *                 The variation1 that the new part version will be created with.
   *                 This value is optional and will default to null.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>variation2                     </I></TD>
   *       <TD>
   *                 The variation2 that the new part version will be created with.
   *                 This value is optional and will default to null.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>team template            </I></TD>
   *       <TD>
   *                 The team template that the new part version will be created under.  This value is optional and defaults to
   *                 no team template being associated with the part.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>lifecycle state          </I></TD>
   *       <TD>
   *                 The lifecycle state that the new part version will be in.  This value is optional and defaults
   *                 to the initial state of the lifecycle that the part is created under.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>typedef                  </I></TD>
   *       <TD>
   *                 This contains the identifier string of a {@link com.ptc.core.meta.common.TypeIdentifier com.ptc.core.meta.common.TypeIdentifier}.<br><br>
   *                 i.e.     wt.part.WTPart|com.mycompany.SoftPart2<br><br>
   *                 This would create a soft typed part.  This currently supports the following modeled types:<p>
   *                 <ul>
   *                   <li><code>wt.part.WTPart</code></li>
   *                 </ul>
   *                 <p>
   *                 This currently does not handle custom modeled types.  If there is a need for one of these, then
   *                 a custom loader needs to be created.
   *                 <p>
   *                 This value is optional and defaults to <code>wt.part.WTPart</code>.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>version                  </I></TD>
   *       <TD>
   *                 The revision of the new part.
   *                 This value is optional and defaults to the next revision of the part.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>iteration                </I></TD>
   *       <TD>
   *                 The iteration of the new part revision.  This value is optional and defaults to the next iteration of the new revision of the part.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>parent container path    </I></TD>
   *       <TD>
   *                 DEPRECATED.  This property is ignored, although the tag is still required.
   *                 Instead the parent container is obtained from the CONT_PATH command line argument when running wt.load.LoadFromFile, or from the Load Set data.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>organization name    </I></TD>
   *       <TD>
   *                 The name of the organization that will be searched for the part to create the new view version from.
   *                 This tag is optional and defaults to the organization determined by the command line argument CONT_PATH.
   *       </TD>
   *    </TR>
   *    <TR><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>
   *    <TR>
   *       <TD WIDTH="20%" VALIGN=top><I>organization id    </I></TD>
   *       <TD>
   *                 The id of the organization that will be searched for the part to create the new view version from.
   *                 This tag is optional and defaults
   *                 to the organization determined by the command line argument CONT_PATH.
   *       </TD>
   *    </TR>
   * </TABLE>
   *
   * <BR><BR><B>Supported API: </B>true
   * <BR><BR><B>Extendable: </B>false
    *
    * @param nv               Name/Value pairs of part attributes.
    * @param cmd_line command line argument that can contain supplemental load data
    * @param return_objects <code>Vector</code> of the object(s) created by this method.
    */
   public static boolean createNewViewVersion(Hashtable nv, Hashtable cmd_line, Vector return_objects) throws WTException
   {
      String partNumber = getValue("partNumber", nv, cmd_line, true);
      String partVersion = getValue("partVersion", nv, cmd_line, false);
      String partIteration = getValue("partIteration", nv, cmd_line, false);
      String partViewName = getValue("partView", nv, cmd_line, false);
      String partVariation1Name = getValue("partVariation1", nv, cmd_line, false);
      String partVariation2Name = getValue("partVariation2", nv, cmd_line, false);
      String viewName = getValue("view", nv, cmd_line, true);
      String variation1Name = getValue("variation1", nv, cmd_line, false);
      String variation2Name = getValue("variation2", nv, cmd_line, false);
      String orgName = getValue("organizationName",nv,cmd_line,false);
      String orgID = getValue("organizationID",nv,cmd_line,false);

      if(partVersion != null && partVersion.equals("")) partVersion = null;
      if(partIteration != null && partIteration.equals("")) partIteration = null;
      if(partViewName != null && partViewName.equals("")) partViewName = null;
      if(partVariation1Name != null && partVariation1Name.equals("")) partVariation1Name = null;
      if(partVariation2Name != null && partVariation2Name.equals("")) partVariation2Name = null;
      if(variation1Name != null && variation1Name.equals("")) variation1Name = null;
      if(variation2Name != null && variation2Name.equals("")) variation2Name = null;
      if(orgName !=null && orgName.equals("")) orgName = null;
      if(orgID !=null && orgID.equals("")) orgID = null;

      WTPart part = null;
      View partView = null;
      View view = null;
      Variation1 variation1 = Variation1.toVariation1(variation1Name);
      Variation2 variation2 = Variation2.toVariation2(variation2Name);
      try {
         view = ViewHelper.service.getView(viewName);
         if (partViewName != null) {
            // If a view name for the part to branch from was specified look for exactly that part.
            part = getPart(partNumber, partVersion, partIteration, partViewName, partVariation1Name, partVariation2Name, orgID, orgName);
         }
         else {
            // If a view name for the part to branch from was not specified, look up the view ancestor
            // hierarchy to find a part in an ancestor view.
         View parentView = ViewHelper.service.getParent(view);
         while(part == null && parentView != null) {
               part = getPart(partNumber, partVersion, partIteration, parentView.getName(), partVariation1Name, partVariation2Name, orgID, orgName);
            parentView = ViewHelper.service.getParent(parentView);
         }
      }
      }
      catch(ViewException ve) {
         String[] messageArgs = new String[]{viewName};
         String message = WTMessage.getLocalizedMessage(RESOURCE, partResource.NEWVIEWVERSION_NO_VIEW, messageArgs);
         LoadServerHelper.printMessage(message);
         ve.printStackTrace();
         return false;
      }
      catch(WTException wte) {
         String[] messageArgs = new String[]{viewName};
         String message = WTMessage.getLocalizedMessage(RESOURCE, partResource.NEWVIEWVERSION_NO_VIEW, messageArgs);
         LoadServerHelper.printMessage(message);
         wte.printStackTrace();
         return false;
      }

      if(part == null) {
         String[] messageArgs = new String[] {partNumber};
         String message = WTMessage.getLocalizedMessage(RESOURCE, partResource.NEWVIEWVERSION_NO_PART, messageArgs);
         LoadServerHelper.printMessage(message);
         return false;
      }

      try {
         WTPart revisedPart = (WTPart) ViewHelper.service.newBranchForViewAndVariations(part, view, variation1, variation2);
         revisedPart = applyHardAttributes(revisedPart, nv, cmd_line);
         revisedPart = (WTPart) PersistenceHelper.manager.store(revisedPart);
         revisedPart = cachePart(revisedPart);
      }
      catch(WTPropertyVetoException wtpve) {
         String[] messageArgs = new String[] {partNumber};
         String message = WTMessage.getLocalizedMessage(RESOURCE, partResource.NEWVIEWVERSION_FAILED, messageArgs);
         LoadServerHelper.printMessage(message);
         wtpve.printStackTrace();
         return false;
      }
      catch(WTException wte) {
         String[] messageArgs = new String[] {partNumber};
         String message = WTMessage.getLocalizedMessage(RESOURCE, partResource.NEWVIEWVERSION_FAILED, messageArgs);
         LoadServerHelper.printMessage(message);
         wte.printStackTrace();
         return false;
      }
      return true;
   }
   protected static boolean createPartRepresentation( Hashtable nv, Hashtable cmd_line, Vector return_objects, int type, boolean cached_only ) {
      try {
         String[] messageArgs = new String[2];
         String message;

         String partNumber = getValue("partNumber",nv,cmd_line,false);
         String partVersion = getValue("partVersion",nv,cmd_line,false);
         String partIteration = getValue("partIteration",nv,cmd_line,false);
         String partView = getValue("partView",nv,cmd_line,false);
         String partVariation1 = getValue("partVariation1",nv,cmd_line,false);
         String partVariation2 = getValue("partVariation2",nv,cmd_line,false);
         String orgName = getValue("organizationName",nv,cmd_line,false);
         String orgID = getValue("organizationID",nv,cmd_line,false);

         if (partView != null && partView.equals("")) partView = null;
         if (partVariation1 != null && partVariation1.equals("")) partVariation1 = null;
         if (partVariation2 != null && partVariation2.equals("")) partVariation2 = null;
         if (orgName != null && orgName.equals("")) orgName = null;
         if (orgID != null && orgID.equals("")) orgID = null;

         WTPart part = getPart(partNumber, partVersion, partIteration, partView, partVariation1, partVariation2, orgID, orgName);

         if( part == null ) {
            messageArgs[0] = getDisplayInfo(nv,cmd_line);
            message = WTMessage.getLocalizedMessage(RESOURCE,partResource.LOAD_NO_PART_REPRESENTATION,messageArgs);
            LoadServerHelper.printMessage(message);
         }
         else {
            String inDir = getValue("repDirectory",nv,cmd_line, true);
            // The inpur directory should be relative to WTHOME
            inDir = WTHOME + DIRSEP + inDir;

            String poid = getRefFromObject(part);
            boolean republishable = false;
            String repName = getValue("repName",nv,cmd_line,false);
            String repDesc = getValue("repDescription",nv,cmd_line,false);
            boolean repDefault = getBooleanValue("repDefault",nv,cmd_line, false, false);
            boolean createThumbnail = getBooleanValue("repCreateThumbnail",nv,cmd_line, false, true);
            boolean storeEDZ = getBooleanValue("repStoreEdz",nv,cmd_line, false, false);

            // This pervents double nodes being shown when performing a dynamic Part Structure launch into PV
            Vector options = new Vector();
            options.addElement( "ignoreonmerge=true" );

            Boolean ret = loadRepresentation(inDir, poid, republishable,
                                    repName, repDesc, repDefault, createThumbnail, storeEDZ, options);

            messageArgs[0] = repName;
            messageArgs[1] = part.getIdentity();

            if ( ret.booleanValue() ) {
               message = WTMessage.getLocalizedMessage(RESOURCE,partResource.LOAD_REPRESENTATION_ADDED,messageArgs);
            }
            else {
               message = WTMessage.getLocalizedMessage(RESOURCE,partResource.LOAD_REPRESENTATION_FAILED,messageArgs);
            }
            //System.out.println(message);
            return_objects.addElement(message);
            return ret.booleanValue();
         }
      }
      catch (WTException wte){
         LoadServerHelper.printMessage("\ncreatePartRepresentation: " + wte.getLocalizedMessage());
         wte.printStackTrace();
      }
      catch (Exception e) {
         LoadServerHelper.printMessage("\ncreatePartRepresentation: " + e.getMessage());
         e.printStackTrace();
      }
      return false;
   }

   public static boolean cachePart(Hashtable nv, Hashtable cmd_line, Vector return_objects) {
      try {
         String partNumber = getValue("partNumber",nv,cmd_line,false);
         String partVersion = getValue("partVersion",nv,cmd_line,false);
         String partIteration = getValue("partIteration",nv,cmd_line,false);
         String partView = getValue("partView",nv,cmd_line,false);
         String partVariation1 = getValue("partVariation1",nv,cmd_line,false);
         String partVariation2 = getValue("partVariation2",nv,cmd_line,false);
         String orgName = getValue("organizationName",nv,cmd_line,false);
         String orgID = getValue("organizationID",nv,cmd_line,false);

         if (partVariation1 != null && partVariation1.equals("")) partVariation1 = null;
         if (partVariation2 != null && partVariation2.equals("")) partVariation2 = null;
         if (orgName !=null && orgName.equals("")) orgName = null;
         if (orgID !=null && orgID.equals("")) orgID = null;

         WTPart part = getPart(partNumber, partVersion, partIteration, partView, partVariation1, partVariation2, orgID, orgName);
         cachePart(part);
      }
      catch(WTException e) {
         LoadServerHelper.printMessage("\nCache Part Failed: " + e.getLocalizedMessage());
         e.printStackTrace();
      }
      return true;
   }

   protected static Boolean loadRepresentation( String directory, String poid, boolean republishable,
                                    String repName, String repDesc, boolean repDefault,
                                    boolean createThumbnail, boolean storeEDZ, Vector options)  {
      Class[] argTypes = {String.class, String.class, boolean.class,
                                    String.class, String.class, boolean.class,
                                    boolean.class, boolean.class, Vector.class};
      Object[] args = { directory, poid, new Boolean(republishable),
                                    repName, repDesc, new Boolean(repDefault),
                                    new Boolean(createThumbnail), new Boolean(storeEDZ), options  };
      try {
         Class cls = Class.forName(REPHELPER_CLASS);
         Method m = cls.getMethod(REPHELPER_METHOD, argTypes);
         return (Boolean)m.invoke(null, args);
      }
      catch( Exception e ) {
         e.printStackTrace();
      }
      return Boolean.FALSE;
   }

   protected static String getRefFromObject( Persistable obj ) {
      try {
         ReferenceFactory refFactory = new ReferenceFactory();
         return refFactory.getReferenceString(ObjectReference.newObjectReference(obj.getPersistInfo().getObjectIdentifier()));
      }
      catch(Exception e) {}
      return null;
   }
   protected static Matrix4d getLocationMatrix( String location ) {
      Matrix4d mat;

      if ( location != null && ! location.equals("") ) {
         StringTokenizer tok = new StringTokenizer(location);
         if ( tok.countTokens() > 5 ) {
            double ax = Double.parseDouble( tok.nextToken() );
            double ay = Double.parseDouble( tok.nextToken() );
            double az = Double.parseDouble( tok.nextToken() );
            double  x = Double.parseDouble( tok.nextToken() );
            double  y = Double.parseDouble( tok.nextToken() );
            double  z = Double.parseDouble( tok.nextToken() );
            double scale = 1.0;

            if( tok.hasMoreTokens() ) {
               scale = Double.parseDouble( tok.nextToken() );
            }

            mat = getMatrix4dFromLocation(ax, ay, az, x, y, z, scale);
         }
         else {
            //System.out.println("Insufficient location values");
            mat = new Matrix4d();
            mat.setIdentity();
         }
      }
      else {
         mat = new Matrix4d();
         mat.setIdentity();
      }
      return mat;
   }

   protected static Matrix4d getMatrix4dFromLocation(double ax, double ay, double az,
                                                   double x, double y, double z, double scale) {
      Matrix4d m1 = new Matrix4d();
      Matrix4d m2 = new Matrix4d();

      m1.rotZ(az*DTOR);
      m2.rotY(ay*DTOR);
      m1.mul(m2);
      m2.rotX(ax*DTOR);
      m1.mul(m2);
      m1.setTranslation(new Vector3d(x, y, z));
      m1.setScale(scale);

      return m1;
   }

   private static Timestamp parseTimestamp(String time)
   throws WTException {
       SimpleDateFormat sdf = new SimpleDateFormat();
       Date date = null;
       ParsePosition pos = new ParsePosition(0);
       String localizedPattern = null;

       if(time == null || time.trim().equals(""))
           return null;

       localizedPattern = sdf.toLocalizedPattern();
       if (date == null) {
           sdf.applyLocalizedPattern(localizedPattern);
           date = sdf.parse(time, pos);
           pos.setIndex(0);
       }

       if (date == null) {
           sdf.applyPattern("MM/dd/yyyy HH:mm:ss z");
           date = sdf.parse(time, pos);
           pos.setIndex(0);
       }

       if (date == null) {
           sdf.applyPattern("MM/dd/yyyy HH:mm z");
           date = sdf.parse(time, pos);
           pos.setIndex(0);
       }

       if (date == null) {
           //GMT assumption
           sdf.applyPattern("MM/dd/yyyy HH:mm");
           date = sdf.parse(time, pos);
           pos.setIndex(0);
       }

       if (date == null) {
           //GMT assumption
           sdf.applyPattern("MM/dd/yyyy");
           date = sdf.parse(time, pos);
           pos.setIndex(0);
       }

       if (date == null) {
           StringBuffer errmsg = new StringBuffer();
           errmsg.append("\n Timestamp date format: '"+ time +"' is incorrect! \n");
           errmsg.append("Timestamp must be in this format (choose one): \n");
           errmsg.append("1. "+localizedPattern +"\n");
           errmsg.append("2. MM/dd/yyyy HH:mm:ss z  ( 12/31/2005 15:30:01 CST ) \n");
           errmsg.append("3. MM/dd/yyyy HH:mm z     ( 12/31/2005 15:30 CST ) \n");
           errmsg.append("4. MM/dd/yyyy HH:mm       ( 12/31/2005 15:30 ) - GMT implied\n");
           errmsg.append("5. MM/dd/yyyy             ( 12/31/2005 ) - GMT implied \n");
           throw new WTException(errmsg.toString());
       }

       return new Timestamp(date.getTime());
   }
    /**
     * Constructs a part (cached).<p>
     * Constructs a new part if a part with the supplied part number does not exist.
     * Otherwise if a version is supplied, then it
     *    constructs a new version of the existing part if the supplied version does not exist.
     *    Otherwise it constructs a new iteration of the supplied version.
     * Otherwise constructs a new iteration of the latest version.
     */
    protected static WTPart constructPart( Hashtable nv, Hashtable cmd_line ) throws WTException
    {
      //Fix for SPR 1487183: Create "EXCLUSION" reference attribute definition
        //and singleton object of type OptionExclusionRReference if they dont
        //already exist in the database. This will ensure successful loading
        //of a configurable part structures with exclusion ibas set on usageLinks.
        NavigationFilterHelper.service.getOptionExclusionReference();

        Timestamp createTimestampObj = null;
        Timestamp modifyTimestampObj = null;
        boolean insert_on_latest_iteration = false;

        String number    = getValue("partNumber",nv,cmd_line,false);
        String version   = getValue("version",nv,cmd_line,false);
        String iteration = getValue("iteration",nv,cmd_line,false);
        String view      = getValue("view",nv,cmd_line,false);
        String variation1 = getValue("variation1",nv,cmd_line,false);
        String variation2 = getValue("variation2",nv,cmd_line,false);
        String orgName = getValue("organizationName",nv,cmd_line,false);
        String orgID = getValue("organizationID",nv,cmd_line,false);
		String phantom = getValue("phantom",nv,cmd_line,false);


        if (variation1 != null && variation1.equals("")) variation1 = null;
        if (variation2 != null && variation2.equals("")) variation2 = null;
        if (orgName != null && orgName.equals("")) orgName = null;
        if (orgID != null && orgID.equals("")) orgID = null;

        String createTimestamp = getValue("createTimestamp",nv,cmd_line,false);
        String modifyTimestamp = getValue("modifyTimestamp",nv,cmd_line,false);

        createTimestampObj = parseTimestamp(createTimestamp);
        modifyTimestampObj = parseTimestamp(modifyTimestamp);
        if (createTimestampObj == null) {
            if (modifyTimestampObj != null)
                createTimestampObj = modifyTimestampObj;
        }else{
            if (modifyTimestampObj == null)
                modifyTimestampObj = createTimestampObj;
        }

        //If no timestamp, then always do insert WTPart
        if( createTimestampObj == null && modifyTimestamp == null )
            insert_on_latest_iteration = true;

        WTPart part = null;
        //Only try and find part if number is specified; otherwise, assume this is going to create a part.
      if( number != null ) {
         if(version == null) {
            part = getPart(number,null,null,view,variation1,variation2,orgID,orgName);
         }
         else if(version != null && iteration == null) {
            part = getPart(number,version,null,view,variation1,variation2,orgID,orgName);
            if(part == null) {
               part = getPart(number,null,null,view,variation1,variation2,orgID,orgName);
            }
         }
         else if(version != null && iteration != null) {
            part = getPart(number,version,iteration,view,variation1,variation2,orgID,orgName);
            if(part == null) {
               part = getPart(number,version,null,view,variation1,variation2,orgID,orgName);
            }
            if(part == null) {
               part = getPart(number,null,null,view,variation1,variation2,orgID,orgName);
            }
            }
        }

        //CONSTRUCT PART
        if(part==null)
        {
            String typedef = getValue(TYPEDEF,nv,cmd_line,false);
            if (typedef == null || typedef.trim().equals(""))
            {
                typedef = "WCTYPE|wt.part.WTPart";
            }

            //TypeIdentifier typeidentifier = TypeHelper.getTypeIdentifier(typedef);
            //Object o = TypeHelper.newInstance(typeidentifier);
            TypeIdentifier typeidentifier = (TypeIdentifier)ReflectionHelper.dynamicInvoke(
                           "com.ptc.core.foundation.type.server.impl.TypeHelper", "getTypeIdentifier",
                           new Class[] {String.class}, new Object[] {typedef} );
            Object o = ReflectionHelper.dynamicInvoke( "com.ptc.core.foundation.type.server.impl.TypeHelper",
                           "newInstance", new Class[] {TypeIdentifier.class}, new Object[] {typeidentifier} );
            if (!(o instanceof WTPart)) {
               throw new WTException("Expected instance of wtpart, but was: " + o.getClass().getName());
            }
            part = (WTPart)o;

            try
            {
               // Construct a default IterationInfo object.  Cannot do the same for the VersionInfo
               // object because there isn't enough information in the object to look up the correct
               // version series from the OIRs yet.
                if (part.getIterationInfo() == null)
                    part.setIterationInfo( IterationInfo.newIterationInfo() );
            }
            catch (WTPropertyVetoException error)
            {
                throw new WTException(error);
            }
            part = applyConstructionTimeAttributes(part,nv,cmd_line);
        }
        else
        {
            if (insert_on_latest_iteration && !VersionControlHelper.isLatestIteration(part))
                part = (WTPart)VersionControlHelper.getLatestIteration(part);

            if (VERBOSE)
            {
                String vers_strg = part.getVersionDisplayIdentifier().getLocalizedMessage(WTContext.getContext().getLocale()) + "." + part.getIterationIdentifier().getValue();
                String checkoutstate = WorkInProgressHelper.getState(part).getDisplay();
                System.out.println("Iterating on an existing part = " + number + " " + vers_strg + " with state of " + checkoutstate);
            }

            try
            {
                //If Part already exist and target Container is different then existing Part container, abort create
                WTContainerRef partContainerRef = part.getContainerReference();
                WTContainerRef targetContainerRef = LoadServerHelper.getTargetContainer( nv, cmd_line );
                if(!partContainerRef.equals(targetContainerRef)) {
                    throw new WTException("Can not create '"+number+" "+part.getName()+ " - "+version+"' in Container: '"+targetContainerRef.getName()
                        +"', because it already exists in Container: '"+partContainerRef.getName()+"'" );
                }

                if (insert_on_latest_iteration)
                {
                    // All non-creates should be inserts now, so the rest is just here for documentation purposes.
                    // Even though newIteration is used, if needed a new revision will be created during the insertNode below.
                    String calc_iter = null;
                    boolean new_version = isNewVersion(part,version);
                    if (iteration == null)
                    {
                        // Need a little help if no iteration is given.
                        if (!new_version)
                        {
                            int old_iter = Integer.parseInt(part.getIterationIdentifier().getValue());
                            calc_iter = Integer.toString(old_iter + 1);
                        }
                        else
                        {
                            calc_iter = Integer.toString(1);
                        }
                    }
                    part = (WTPart)VersionControlHelper.service.newIteration(part);
                    if (calc_iter != null)
                        setIteration(part,calc_iter);
                }
                // the next two elses are not reachable with the insert flag set to true, are here only if
                // someone wants to revert back to the old behavior of this code.
                else if((version==null)||version.equals(VersionControlHelper.getVersionIdentifier(part).getValue()))
                {
                    WTPart temp = (WTPart)VersionControlHelper.service.newIteration(part);
                    part = (WTPart)PersistenceHelper.manager.refresh(part);
                    part = (WTPart)VersionControlHelper.service.supersede(part,temp);
                }
                else
                {
                    part = (WTPart)VersionControlHelper.service.newVersion(part);
                }
            }
            catch( WTPropertyVetoException e )
            {
                throw new WTException(e);
            }
        }
        part = applyHardAttributes(part,nv,cmd_line);

        if (insert_on_latest_iteration)
        {
            try
            {
                part.setFederatableInfo(new FederatableInfo());
            }
            catch (WTPropertyVetoException wtpve)
            {
                throw new WTException(wtpve,"Error creating the FederatableInfo required for inserting versions/iterations");
            }

            // Use null for the ufids of this object and the branch, because we want the default
            // insert behavior of using the latest iteration as the branch point on all new versions.
            //
            // The following call to insertNode MUST NOT BE REMOVED.
            // To turn off the "insert out of order" behavior, you should instead set the
            // insert_on_latest_iteration flag to false
            part = (WTPart) VersionControlHelper.service.insertNode(part,null,null,null,null);
            // SPR 2039786: Master needs to be updated to store the master attributes correctly.
            WTPartMaster partMaster = (WTPartMaster)part.getMaster();
            PersistenceServerHelper.manager.update(partMaster, false /* changeModifyDate */);
        }
        else
        {
            // This else is not reachable with the insert flag set to true, this store is now
            // done in the insertNode for both the new object, new iteration, and new version cases.
            part = (WTPart)PersistenceServerHelper.manager.store(part, createTimestampObj, modifyTimestampObj);
        }

        if (VERBOSE)
        {
            String vers_strg = part.getVersionDisplayIdentifier().getLocalizedMessage(WTContext.getContext().getLocale()) + "." + part.getIterationIdentifier().getValue();
            String checkoutstate = WorkInProgressHelper.getState(part).getDisplay();
            System.out.println("New part = " + number + " " + vers_strg + " with state of " + checkoutstate);
        }
        setLogicBase(part, nv, cmd_line);
        return part;
    }
   // Check if new version is going to be created.
   protected static boolean isNewVersion(WTPart part, String version) throws WTException {
       if (version == null)
          return false;
       if (version.equals(VersionControlHelper.getVersionIdentifier(part).getValue()))
          return false;
       else
          return true;
    }

   /**
    * RETRIEVE PART MOST RECENTLY ADDED TO THE CACHE
    *
    * @return WTPart
    **/
   public static WTPart getPart() throws WTException {
      return getCachedPart(null,null,null,null);
   }
    /**
     * RETRIEVE A PART BASED ON PART NUMBER, VERSION, ITERATION AND VIEW (CACHED)
     *
     * @param number If null, RETURNS PART MOST RECENTLY ADDED TO THE CACHE
     * @param version If null, RETURNS PART BASED ON PART NUMBER ONLY
     * @param iteration If null, RETURNS PART BASED ON PART NUMBER AND VERSION ONLY
     * @param view If null, RETURNS PART BASED ON PART NUMBER, VERSION AND ITERATION ONLY
     *
     * @return WTPart
     **/
    public static WTPart getPart(String number, String version, String iteration, String view) throws WTException
    {
        return getPart(number, version, iteration, view, null, null);
    }

    /**
     * RETRIEVE A PART BASED ON PART NUMBER, VERSION, ITERATION, VIEW, ORG_ID, ORG_NAME (CACHED)
     *
     * @param number If null, RETURNS PART MOST RECENTLY ADDED TO THE CACHE
     * @param version If null, RETURNS PART BASED ON PART NUMBER ONLY
     * @param iteration If null, RETURNS PART BASED ON PART NUMBER AND VERSION ONLY
     * @param view If null, RETURNS PART BASED ON PART NUMBER, VERSION AND ITERATION ONLY
     * @param org_id If null, ORG_NAME IS USED.
     * @param org_name If null, AND NO ORG_ID, THEN NO ORG CRITERIA ARE USED
     *
     * @return WTPart
     **/
    public static WTPart getPart(String number, String version, String iteration, String view, String org_id, String org_name) throws WTException
    {
        return getPart(number, version, iteration, view, null, null, org_id, org_name);
    }

    /**
     * RETRIEVE A PART BASED ON PART NUMBER, VERSION, ITERATION, VIEW, VARIATION1, VARIATION2, ORG_ID, ORG_NAME (CACHED)
     *
     * @param number If null, RETURNS PART MOST RECENTLY ADDED TO THE CACHE
     * @param version If null, RETURNS PART BASED ON PART NUMBER ONLY
     * @param iteration If null, RETURNS PART BASED ON PART NUMBER AND VERSION ONLY
     * @param view If null, RETURNS PART BASED ON PART NUMBER, VERSION AND ITERATION ONLY
     * @param variation1 If null, RETURNS PART BASED ON OTHER ATTRIBUTES
     * @param variation2 If null, RETURNS PART BASED ON OTHER ATTRIBUTES
     * @param org_id If null, ORG_NAME IS USED.
     * @param org_name If null, AND NO ORG_ID, THEN NO ORG CRITERIA ARE USED
     *
     * @return WTPart
     **/
    public static WTPart getPart(String number, String version, String iteration, String view, String variation1, String variation2, String org_id, String org_name) throws WTException
    {
       WTPart cachedPart = getCachedPart(number, version, iteration, view, variation1, variation2, org_id, org_name);
       if( cachedPart != null ) {
          return cachedPart;
       }

       if(number == null) {
          return null;
       }

       LatestConfigSpec configSpec = null;
       QuerySpec qs = new QuerySpec(WTPart.class);
       qs.appendWhere(new SearchCondition(WTPart.class,
             WTPart.NUMBER,
             SearchCondition.EQUAL,
             number.toUpperCase(),
             false));

       WTOrganization wtorg = getOrganization(org_id, org_name);
       if (wtorg != null) {
          qs.appendAnd();
          qs.appendWhere(new SearchCondition(WTPart.class,
               WTPart.ORGANIZATION_REFERENCE +'.' + WTAttributeNameIfc.REF_OBJECT_ID,
               SearchCondition.EQUAL,
               PersistenceHelper.getObjectIdentifier(wtorg).getId()));
       }

       if(view != null) {
          View viewObj = ViewHelper.service.getView(view);
          qs.appendAnd();
          qs.appendWhere(new SearchCondition(WTPart.class,
                ViewManageable.VIEW + "." + ObjectReference.KEY,
                SearchCondition.EQUAL,
                PersistenceHelper.getObjectIdentifier(viewObj)));
       }
       if(variation1 != null) {
          Variation1 variation1Obj = Variation1.toVariation1(variation1);
          qs = ViewHelper.appendWhereVariation(qs, WTPart.class, new int[]{0}, Variation1.class, variation1Obj, false);
       }
       if(variation2 != null) {
          Variation2 variation2Obj = Variation2.toVariation2(variation2);
          qs = ViewHelper.appendWhereVariation(qs, WTPart.class, new int[]{0}, Variation2.class, variation2Obj, false);
       }
       if(version != null) {
          qs.appendAnd();
          qs.appendWhere(new SearchCondition(WTPart.class,
                Versioned.VERSION_INFO + "." + VersionInfo.IDENTIFIER + "." + "versionId",
                SearchCondition.EQUAL,
                version,
                false));
          if(iteration != null) {
             qs.appendAnd();
             qs.appendWhere(new SearchCondition(WTPart.class,
                   Iterated.ITERATION_INFO + "." + IterationInfo.IDENTIFIER + "." + "iterationId",
                   SearchCondition.EQUAL,
                   iteration,
                   false));
          }
          else {
             //iteration==null
             qs.appendAnd();
             qs.appendWhere(new SearchCondition(WTPart.class,
                   Iterated.ITERATION_INFO + "." + IterationInfo.LATEST,
                   SearchCondition.IS_TRUE));
          }
       }
       else {
          //version == null && assume iteration == null
          configSpec = new LatestConfigSpec();
          configSpec.appendSearchCriteria(qs);
       }

       // Filter out sandbox hidden objects
       SandboxHelper.addHiddenObjectFilter(qs, WTPart.class, 0, LogicalOperator.AND);

       if(VERBOSE) {
           System.out.println("getPart(" + number + "," + version + "," + iteration + "," + view + "," + org_id + "," + org_name + ")");
          System.out.println("getPart() SQL: " + qs.toString());
       }

       QueryResult qr = PersistenceHelper.manager.find(qs);
       if(VERBOSE) {
          System.out.println("Query found " + qr.size() + " matching parts.");
       }
       if(configSpec != null) {
          qr = configSpec.process(qr);
       }
       if(VERBOSE) {
          if(configSpec != null) {
            System.out.println("Query filtered by ConfigSpec found " + qr.size() + " matching parts.");
          }
       }
       if(qr.size() == 1) {
          WTPart part = (WTPart) qr.nextElement();
          if( WorkInProgressHelper.isCheckedOut(part) ) {
             String[] messageArgs = {};
             String msg = WTMessage.getLocalizedMessage("wt.vc.wip.wipResource",wt.vc.wip.wipResource.ALREADY_CHECKED_OUT, messageArgs );
             LoadServerHelper.printMessage(msg);
             throw new WTException(msg);
          }
          part = cachePart(part);
          return part;
       }
       else if(qr.size() > 1) {
          String msg = "Found " + qr.size() + " parts that match number=" + number + " version=" + version
                + " iteration=" + iteration + " view=" + view + "." + " Expecting only one part.";
          LoadServerHelper.printMessage(msg);
          throw new WTException(msg);
       }
       else { //qr.size() == 0
          return null;
       }
    }
   // RETRIEVE PART MASTER MOST RECENTLY ADDED TO THE CACHE
   protected static WTPartMaster getMaster() throws WTException {
      return getMaster(null);
   }

   // RETRIEVE A PART MASTER BASED ON PART NUMBER(CACHED)
   // IF number IS null, RETURNS PART MASTER MOST RECENTLY ADDED TO THE CACHE
   protected static WTPartMaster getMaster( String number ) throws WTException {
      WTPartMaster master = getCachedMaster(number);
      if(master==null) {
         if(number!=null) {
            QuerySpec qs = new QuerySpec(WTPartMaster.class);
            qs.appendWhere(new SearchCondition( WTPartMaster.class,
                                                WTPartMaster.NUMBER,
                                                SearchCondition.EQUAL,
                                                number.toUpperCase()));
            QueryResult qr = PersistenceHelper.manager.find(qs);
            if(qr.size()==1) {
               master = cacheMaster((WTPartMaster)qr.nextElement());
            }
         }
      }
      return master;
   }

   // RETRIEVE A PART MASTER BASED ON PART NUMBER AND CONTAINER (CACHED)
   // IF number IS null, RETURNS PART MASTER MOST RECENTLY ADDED TO THE CACHE
   protected static WTPartMaster getMaster( String number, WTContainerRef containerRef ) throws WTException {
     if(number!=null) {
        QuerySpec qs = new QuerySpec(WTPartMaster.class);
        qs.appendWhere(new SearchCondition( WTPartMaster.class,
                                            WTPartMaster.NUMBER,
                                            SearchCondition.EQUAL,
                                            number.toUpperCase()));
        QueryResult qr = PersistenceHelper.manager.find(qs);
        while (qr.hasMoreElements()) {
         WTPartMaster partMaster = (WTPartMaster) qr.nextElement();
         if (partMaster.getContainerReference().equals(containerRef)) {
            return partMaster;
         }
        }
     }
     return null;
   }

   ////////////////
   // PART CACHE //
   ////////////////
   protected static WTPart getCachedPart() throws WTException {
        return getCachedPart(null,null,null,null);
   }

   protected static WTPart getCachedPart( String number ) throws WTException {
        return getCachedPart(number,null,null,null);
   }

   protected static WTPart getCachedPart( String number, String version ) throws WTException {
        return getCachedPart(number,version,null,null);
   }

   protected static WTPart getCachedPart( String number, String version, String iteration ) throws WTException {
        return getCachedPart(number,version,iteration,null);
   }
    protected static WTPart getCachedPart( String number, String version, String iteration, String view ) throws WTException {
        return getCachedPart(number,version,iteration,view,null,null,null,null);
    }

    protected static WTPart getCachedPart( String number, String version, String iteration, String view, String variation1, String variation2, String org_id, String org_name ) throws WTException {
       Object key = getPartCacheKey(number,version,iteration,view,variation1,variation2,org_id,org_name);
       WTPart part = (WTPart)LoadServerHelper.getFromCache(key);
       if(VERBOSE) {
          if(part == null) {
             System.out.println("Getting part from cache using key: " + key + ". Part not found.");
          }
          else {
             System.out.println("Getting part from cache using key: " + key + ". Part found.");
          }
       }
       return part;
    }

    protected static Object getPartCacheKey(final String number, final String version, final String iteration,
                                            final String view, final String variation1, final String variation2,
                                            final String org_id, final String org_name ) throws WTException {
       if (number == null) {
          return LAST_PART_KEY;
       }
       class PartKey {
          final String partNumber = (number == null ? null : number.toUpperCase());
          final String partVersion = (version == null ? null : version.toUpperCase());
          final String partIteration = (iteration == null ? null : iteration.toUpperCase());
          final String partView = (view == null ? null : view.toUpperCase());
          final String partVariation1 = (variation1 == null ? null : variation1.toUpperCase());
          final String partVariation2 = (variation2 == null ? null : variation2.toUpperCase());
          final String partOrgID = (org_id == null ? null : org_id.toUpperCase());
          final String partOrgName = (org_name == null ? null : org_name.toUpperCase());
          volatile int hashCode;
          public boolean equals(Object o) {
             if (o == this) {
                return true;
             }
             if (!(o instanceof PartKey)) {
                return false;
             }
             PartKey other = (PartKey)o;
             return (equals(partNumber,other.partNumber)
               && (equals(partVersion,other.partVersion))
               && (equals(partIteration,other.partIteration))
               && (equals(partView,other.partView))
               && (equals(partVariation1,other.partVariation1))
               && (equals(partVariation2,other.partVariation2))
               && (equals(partOrgID,other.partOrgID))
               && (equals(partOrgName,other.partOrgName)));
          }
          boolean equals(String a, String b) {
             if (a == null) {
                return (b == null);
             }
             if (b == null) {
                return false;
             }
             return (a.equals(b));
          }
          public int hashCode() {
             if (hashCode == 0) {
                int result = 13;
                result = 37*result + partNumber.hashCode();
                if (partVersion != null) {
                  result = 37*result + partVersion.hashCode();
                }
                if (partIteration != null) {
                  result = 37*result + partIteration.hashCode();
                }
                if (partView != null) {
                  result = 37*result + partView.hashCode();
                }
                if (partVariation1 != null) {
                   result = 37*result + partVariation1.hashCode();
                }
                if (partVariation2 != null) {
                   result = 37*result + partVariation2.hashCode();
                }
                if (partOrgID != null) {
                  result = 37*result + partOrgID.hashCode();
                }
                if (partOrgName != null) {
                  result = 37*result + partOrgName.hashCode();
                }
                hashCode = result;
             }
             return hashCode;
          }
          public String toString() {
             return "wt.load.LoadPart.PartKey [number=\""+partNumber
                        +"\" version=\""+partVersion
                        +"\" iteration=\""+partIteration
                        +"\" view=\""+partView
                        +"\" variation1=\""+partVariation1
                        +"\" variation2=\""+partVariation2
                        +"\" orgID=\""+partOrgID
                        +"\" orgName=\""+partOrgName+"\"]";
          }
       }
       return new PartKey();
   }

   /**
    * Cache the part in any conceivable way that it may later be retrieved.
    */
   protected static WTPart cachePart( WTPart part ) throws WTException {
      if(part==null) {
         LoadServerHelper.removeFromCache(getPartCacheKey(null,null,null,null,null,null,null,null));
         LoadServerHelper.removeFromCache(CURRENT_CONTENT_HOLDER);
         LoadValue.establishCurrentIBAHolder(null);
         LoadAttValues.establishCurrentTypeManaged(null);
         cacheChoiceMappableObject(null);
         cacheExpressionAssignableObject(null);
         //KEPT FOR LEGACY SUPPORT
         LoadServerHelper.removeFromCache(CURRENT_PART);
      }
      else {
         String number = part.getNumber();
         String version = VersionControlHelper.getVersionIdentifier(part).getValue();
         String iteration = VersionControlHelper.getIterationIdentifier(part).getValue();
         String view = ViewHelper.getViewName(part);
         String variation1 = part.getVariation1() == null ? null : part.getVariation1().toString();
         String variation2 = part.getVariation2() == null ? null : part.getVariation2().toString();
         String orgName = null;
         String orgId = null;
         WTOrganization org = part.getOrganization();
         if (org != null) {
            orgName = org.getName();
            orgId = getOrgID(org);
         }

         if (VERBOSE) {
            System.out.println("Caching part with all combinations of key " +
                  "[" +
                  " number=\"" + number + "\"" +
                  " version=\"" + version + "\"" +
                  " iteration=\"" + iteration + "\"" +
                  " view=\"" + view + "\"" +
                  " variation1=\"" + variation1 + "\"" +
                  " variation2=\"" + variation2 + "\"" +
                  " orgID=\"" + orgId + "\"" +
                  " orgName=\"" + orgName + "\"" +
                  " ]");
         }

         // Cache the part without view or org id or org name
         LoadServerHelper.putCacheValue(getPartCacheKey(null,null,null,null,null,null,null,null),part);
         LoadServerHelper.putCacheValue(getPartCacheKey(number,null,null,null,null,null,null,null),part);
         LoadServerHelper.putCacheValue(getPartCacheKey(number,version,null,null,null,null,null,null),part);
         LoadServerHelper.putCacheValue(getPartCacheKey(number,version,iteration,null,null,null,null,null),part);

         //If the part is in a view, also cache the part with the view name (and include variation names).
         if( view != null ) {
            LoadServerHelper.putCacheValue(getPartCacheKey(number,null,null,view,variation1,variation2,null,null),part);
            LoadServerHelper.putCacheValue(getPartCacheKey(number,version,null,view,variation1,variation2,null,null),part);
            LoadServerHelper.putCacheValue(getPartCacheKey(number,version,iteration,view,variation1,variation2,null,null),part);
         }

         //If the part has an org, also cache it with the org ID
         if (org != null) {
            // With both org id and name
            LoadServerHelper.putCacheValue(getPartCacheKey(number,null,null,null,null,null,orgId,orgName),part);
            LoadServerHelper.putCacheValue(getPartCacheKey(number,version,null,null,null,null,orgId,orgName),part);
            LoadServerHelper.putCacheValue(getPartCacheKey(number,version,iteration,null,null,null,orgId,orgName),part);

            // With just org id
            LoadServerHelper.putCacheValue(getPartCacheKey(number,null,null,null,null,null,orgId,null),part);
            LoadServerHelper.putCacheValue(getPartCacheKey(number,version,null,null,null,null,orgId,null),part);
            LoadServerHelper.putCacheValue(getPartCacheKey(number,version,iteration,null,null,null,orgId,null),part);

            // With just org name
            LoadServerHelper.putCacheValue(getPartCacheKey(number,null,null,null,null,null,null,orgName),part);
            LoadServerHelper.putCacheValue(getPartCacheKey(number,version,null,null,null,null,null,orgName),part);
            LoadServerHelper.putCacheValue(getPartCacheKey(number,version,iteration,null,null,null,null,orgName),part);
         }

         //If the part has a view and an org, cache it with both
         if (view != null && org != null) {
            // With both org id and name
            LoadServerHelper.putCacheValue(getPartCacheKey(number,null,null,view,variation1,variation2,orgId,orgName),part);
            LoadServerHelper.putCacheValue(getPartCacheKey(number,version,null,view,variation1,variation2,orgId,orgName),part);
            LoadServerHelper.putCacheValue(getPartCacheKey(number,version,iteration,view,variation1,variation2,orgId,orgName),part);

            // With just org id
            LoadServerHelper.putCacheValue(getPartCacheKey(number,null,null,view,variation1,variation2,orgId,null),part);
            LoadServerHelper.putCacheValue(getPartCacheKey(number,version,null,view,variation1,variation2,orgId,null),part);
            LoadServerHelper.putCacheValue(getPartCacheKey(number,version,iteration,view,variation1,variation2,orgId,null),part);

            // With just org name
            LoadServerHelper.putCacheValue(getPartCacheKey(number,null,null,view,variation1,variation2,null,orgName),part);
            LoadServerHelper.putCacheValue(getPartCacheKey(number,version,null,view,variation1,variation2,null,orgName),part);
            LoadServerHelper.putCacheValue(getPartCacheKey(number,version,iteration,view,variation1,variation2,null,orgName),part);
         }

         LoadServerHelper.putCacheValue(CURRENT_CONTENT_HOLDER,part);
         // Cache part with ChoiceMappable key for use in creation of ChoiceMappableChoiceLink
         cacheChoiceMappableObject((ChoiceMappable)part);
         cacheExpressionAssignableObject((ExpressionAssignable)part);

         //KEPT FOR LEGACY SUPPORT
         LoadServerHelper.putCacheValue(CURRENT_PART,part);

         cacheMaster((WTPartMaster)part.getMaster());
      }
      LoadValue.establishCurrentIBAHolder(part);
      LoadAttValues.establishCurrentTypeManaged((TypeManaged)part);
      LoadValue.beginIBAContainer();
      return part;
   }

   //////////////////
   // MASTER CACHE //
   //////////////////
   protected static WTPartMaster getCachedMaster() throws WTException {
      return getCachedMaster(null);
   }

   protected static WTPartMaster getCachedMaster( String number ) throws WTException {
      return (WTPartMaster)LoadServerHelper.getCacheValue(getMasterCacheKey(number));
   }

   protected static String getMasterCacheKey( String number ) throws WTException {
      StringBuffer key = new StringBuffer(PART_MASTER_CACHE_KEY);
      if(number!=null) {
         key.append(number.toUpperCase());
      }
      return key.toString();
   }

   protected static WTPartMaster cacheMaster( WTPartMaster master ) throws WTException {
      if(master==null) {
         LoadServerHelper.removeCacheValue(getMasterCacheKey(null));
      }
      else {
         String number = master.getNumber();

         LoadServerHelper.setCacheValue(getMasterCacheKey(null), master);
         LoadServerHelper.setCacheValue(getMasterCacheKey(number), master);
      }
      LoadValue.establishCurrentIBAHolder(master);
      LoadValue.beginIBAContainer();
      return master;
   }

   /////////////////////////
   // PARTUSAGELINK CACHE //
   /////////////////////////
   protected static WTPartUsageLink getCachedPartUsage() throws WTException {
      return (WTPartUsageLink)LoadServerHelper.getCacheValue(PARTUSAGELINK_CACHE_KEY);
   }

   protected static WTPartUsageLink cachePartUsageObject(WTPartUsageLink partUsageLink) throws WTException {
      if(partUsageLink==null) {
         LoadServerHelper.removeCacheValue(PARTUSAGELINK_CACHE_KEY);
      }
      else {
         LoadServerHelper.setCacheValue(PARTUSAGELINK_CACHE_KEY, partUsageLink);
      }

      return partUsageLink;
   }

   ////////////////////////////
   // PARTDOCUMENTLINK CACHE //
   ////////////////////////////
   //Returns the cached Part-Doc link as a Persistable object
   protected static Persistable getCachedPartDocLinks() throws WTException {
      return (Persistable)LoadServerHelper.getCacheValue(PARTDOCLINK_CACHE_KEY);
   }

   // Caches the Part-Doc Link
   protected static Persistable cachePartDocLinkObject(Persistable partDocLink) throws WTException {
      if(partDocLink==null) {
         LoadServerHelper.removeCacheValue(PARTDOCLINK_CACHE_KEY);
      }
      else {
         LoadServerHelper.setCacheValue(PARTDOCLINK_CACHE_KEY, partDocLink);
      }

      return partDocLink;
   }

   /////////////////////////
   // ChoiceMappable CACHE //
   /////////////////////////
   public static ChoiceMappable getCachedChoiceMappable() throws WTException {
      return (ChoiceMappable)LoadServerHelper.getCacheValue(ChoiceMappable.LOADER_CACHE_KEY);
   }

   public static ChoiceMappable cacheChoiceMappableObject(ChoiceMappable choiceMappable) throws WTException {
      if(choiceMappable==null) {
         LoadServerHelper.removeCacheValue(ChoiceMappable.LOADER_CACHE_KEY);
      }
      else {
         LoadServerHelper.setCacheValue(ChoiceMappable.LOADER_CACHE_KEY, choiceMappable);
      }

      return choiceMappable;
   }

   ////////////////////////////////
   // ExpressionAssignable CACHE //
   ////////////////////////////////
   public static ExpressionAssignable getCachedExpressionAssignable() throws WTException {
      return (ExpressionAssignable)LoadServerHelper.getCacheValue(ExpressionAssignable.LOADER_CACHE_KEY);
   }

   public static ExpressionAssignable cacheExpressionAssignableObject(ExpressionAssignable expressionAssignable) throws WTException {
      if (expressionAssignable == null) {
         LoadServerHelper.removeCacheValue(ExpressionAssignable.LOADER_CACHE_KEY);
      }
      else {
         LoadServerHelper.setCacheValue(ExpressionAssignable.LOADER_CACHE_KEY, expressionAssignable);
      }

      return expressionAssignable;
   }

    //////////////////////////////////
    // PART SPECIFIC HELPER METHODS //
    //////////////////////////////////
    protected static WTPart applyHardAttributes( WTPart part, Hashtable nv, Hashtable cmd_line )
        throws WTException {

        WTContainerRef containerRef = LoadServerHelper.getTargetContainer( nv, cmd_line );

        //System.out.println("Set containerRef " + containerRef);
        setContainer(part,containerRef);

        //System.out.println("Iteration " +  getValue("iteration",nv,cmd_line,false));
        setIteration(part,getValue("iteration",nv,cmd_line,false));

        // System.out.println("*****Security Labels " +  getValue("securityLabels",nv,cmd_line,false));
        // Proj.14220950: load SecurityLabels attribute
        setSecurityLabels(part, getValue("securityLabels", nv, cmd_line, false));

        //System.out.println("Type " +  getValue("type",nv,cmd_line,true));
        setPartType(part,getValue("type",nv,cmd_line,true));

        //System.out.println("Source " +  getValue("source",nv,cmd_line,true));
        setSource(part,getValue("source",nv,cmd_line,true));

        //System.out.println("Folder " +  getValue("folder",nv,cmd_line,true));
        setFolder(containerRef, part,getValue("folder",nv,cmd_line,true));

        //System.out.println("Lifecycle " +  getValue("lifecycle",nv,cmd_line,false));
        setLifeCycle(containerRef,part,getValue("lifecycle",nv,cmd_line,false));

        String[] temp = parseTeamTemplate(nv,cmd_line);
        //System.out.println("team " +  temp[0] + " " + temp[1]);
        setTeamTemplate(containerRef,part,temp[0],temp[1]);

        //System.out.println("Lifecyclestate " +  getValue("lifecyclestate",nv,cmd_line,false));
        setState(part,getValue("lifecyclestate",nv,cmd_line,false));

        //System.out.println("Version " +  getValue("version",nv,cmd_line,false));
        setVersion(part,getValue("version",nv,cmd_line,false));

        setEndItemFlag(part, getValue("enditem",nv,cmd_line,false));
        setTraceCode(part, getValue("traceCode",nv,cmd_line,false));

        setMinimumRequired(part,getValue("minRequired",nv,cmd_line,false));
        setMaximumAllowed(part,getValue("maxAllowed",nv,cmd_line,false));
        setDefaultUnit(part,getValue("defaultUnit",nv,cmd_line,false));
        setServiceable(part,getValue("serviceable",nv,cmd_line,false));
        setServicekit(part,getValue("servicekit",nv,cmd_line,false));
        setAuthoringLanguage(part, getValue("authoringLanguage", nv, cmd_line, false));
        //System.out.println("Done");

        return part;
    }
    /**
     * Set Authoring Language on Part.
     * @param part
     * @param value
     * @throws WTException
     */
    protected static void setAuthoringLanguage(WTPart part, String value) throws WTException {
        if (value != null) {
            if (!isPersisted(part)) {
                if (logger.isDebugEnabled()) {//new Part
                    logger.debug("Setting Authoring Language for NEW Part");
                }
                try {
                    part.setAuthoringLanguage(value);
                } catch (WTPropertyVetoException e) {
                    throw new WTException(e);
                }
            } else {// existing part
                if (logger.isDebugEnabled()) {
                    logger.debug("Setting Authoring Language for Existing Part");
                }
                if (value.equals(part.getAuthoringLanguage())) {
                    return;
                } else {
                    throw new WTException("AuthoringLanguage cannot be changed for persisted parts");
                }
            }
        }
    }

    /**
     * Returns if PartMaster is persisted (refactored for wrting unit test.)
     *
     * @param part
     * @return
     */
    private static boolean isPersisted(WTPart part) {
        return part.getMaster().getPersistInfo().isPersisted();
    }

   protected static WTPart applyConstructionTimeAttributes( WTPart part, Hashtable nv, Hashtable cmd_line ) throws WTException {
      setName(part,getValue("partName",nv,cmd_line,true));
      setNumber(part,getValue("partNumber",nv,cmd_line,false));

      setView(part,getValue("view",nv,cmd_line,false));
      setVariation1(part, getValue("variation1",nv,cmd_line,false));
      setVariation2(part, getValue("variation2",nv,cmd_line,false));

		setPhantom(part, getValue("phantom",nv,cmd_line,false));

      setEndItemFlag(part, getValue("enditem",nv,cmd_line,false));
      setTraceCode(part, getValue("traceCode",nv,cmd_line,false));

      String org_id = getValue("organizationID", nv, cmd_line, false);
      String org_name = getValue("organizationName", nv, cmd_line, false);
      setOrganization(part, org_id, org_name);

      setGenericType(part,getValue("genericType",nv,cmd_line,false));
      setCollapsible(part, getValue("collapsible",nv,cmd_line,false));

      return part;
   }

   protected static void setName( WTPart the_part, String name ) throws WTException {
      try {
         the_part.setName(name);
      }
      catch( WTPropertyVetoException e ) {
        LoadServerHelper.printMessage("\nsetName: " + e.getMessage());
        e.printStackTrace();
        throw new WTException(e);
      }
   }

   protected static void setNumber( WTPart the_part, String number ) throws WTException {
      try {
         if( number != null ) {
            the_part.setNumber(number);
         }
      }
      catch( WTPropertyVetoException e ) {
        LoadServerHelper.printMessage("\nsetNumber: " + e.getMessage());
        e.printStackTrace();
        throw new WTException(e);
      }
   }

   protected static void setView( WTPart the_part, String viewName ) throws WTException {
      try {
         if(viewName != null){
            ViewHelper.assignToView(the_part,ViewHelper.service.getView(viewName));
         }
      }
      catch( Exception e ) {
        LoadServerHelper.printMessage("\nsetView: " + e.getMessage());
        e.printStackTrace();
        throw new WTException(e);
      }
   }

	protected static void setCollapsible(WTPart the_part, String collapsible) throws WTException {
		try {
			if(collapsible != null){
				boolean collapsibleValue = Boolean.parseBoolean(collapsible);
				the_part.setCollapsible(collapsibleValue);
			}
			else {
				the_part.setCollapsible(false);
			}
		}
		catch( Exception e ) {
			LoadServerHelper.printMessage("\nsetPhantom: " + e.getMessage());
        e.printStackTrace();
        throw new WTException(e);
      }
   }

   protected static void setPhantom(WTPart the_part, String phantom) throws WTException {
        try {
            if(phantom != null){
                boolean phantomValue = Boolean.parseBoolean(phantom);
                the_part.setPhantom(phantomValue);
            }
            else {
                the_part.setPhantom(false);
            }
        }
        catch( Exception e ) {
            LoadServerHelper.printMessage("\nsetPhantom: " + e.getMessage());
        e.printStackTrace();
        throw new WTException(e);
      }
   }

   protected static void setVariation1( WTPart the_part, String variation1Name ) throws WTException {
      try {
         if(variation1Name != null){
            the_part.setVariation1(Variation1.toVariation1(variation1Name));
         }
         else {
            the_part.setVariation1(null);
         }
      }
      catch( Exception e ) {
        LoadServerHelper.printMessage("\nsetVariation1: " + e.getMessage());
        e.printStackTrace();
        throw new WTException(e);
      }
   }

   protected static void setVariation2( WTPart the_part, String variation2Name ) throws WTException {
      try {
         if(variation2Name != null){
            the_part.setVariation2(Variation2.toVariation2(variation2Name));
         }
         else {
            the_part.setVariation2(null);
         }
      }
      catch( Exception e ) {
        LoadServerHelper.printMessage("\nsetVariation2: " + e.getMessage());
        e.printStackTrace();
        throw new WTException(e);
      }
   }

   private static void setEndItemFlag( WTPart the_part, String endItemFlagString ) throws WTException {
      try {
         if(endItemFlagString != null && endItemFlagString.equalsIgnoreCase("yes")){
            the_part.setEndItem(true);
         }
         else{
           the_part.setEndItem(false);
             }
      }
      catch( Exception e ) {
        LoadServerHelper.printMessage("\nsetEndItemFlag: " + e.getMessage());
        e.printStackTrace();
        throw new WTException(e);
      }
   }

   private static void setTraceCode( WTPart the_part, String traceCode ) throws WTException {
      try {
         if( traceCode != null ) {
            the_part.setDefaultTraceCode(TraceCode.toTraceCode(traceCode));
         }
      }
      catch( WTPropertyVetoException e ) {
        LoadServerHelper.printMessage("\nsetTraceCode: " + e.getMessage());
        e.printStackTrace();
        throw new WTException(e);
      }
   }
    private static void setMinimumRequired( WTPart the_part, String minReqd )
        throws WTException {

        if( minReqd != null  &&
            the_part.getGenericType().equals(GenericType.DYNAMIC) ) {
            try {
                Integer min = Integer.valueOf(minReqd.trim());
                the_part.setMinimumRequired( min );
            }
            catch( WTPropertyVetoException e ) {
                LoadServerHelper.printMessage("\nsetMinimumRequired: " + e.getMessage());
                e.printStackTrace();
                throw new WTException(e);
            }
        }
    }

    private static void setMaximumAllowed( WTPart the_part, String maxAllowed )
        throws WTException {

        if( maxAllowed != null &&
            the_part.getGenericType().equals(GenericType.DYNAMIC) ) {
            try {
                Integer max = Integer.valueOf(maxAllowed.trim());
                the_part.setMaximumAllowed( max );
            }
            catch( WTPropertyVetoException e ) {
                LoadServerHelper.printMessage("\nsetMaximumAllowed: " + e.getMessage());
                e.printStackTrace();
                throw new WTException(e);
            }
        }
    }
    private static void setDefaultUnit( WTPart part, String defaultUnit ) {
        if( defaultUnit != null && !defaultUnit.trim().equals("")) {
            WTPartMaster master =(WTPartMaster)part.getMaster();
            try {
                master.setDefaultUnit((QuantityUnit) QuantityUnit.toQuantityUnit(defaultUnit));
            } catch (WTInvalidParameterException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (WTPropertyVetoException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
         }
       }
    private static void setServiceable(WTPart part, String serviceable)
            throws WTException {

        if (serviceable != null) {
            try {
                WTPartMaster master = (WTPartMaster) part.getMaster();
                Boolean serviceableBoolean = Boolean.valueOf(serviceable.trim());
                master.setServiceable(serviceableBoolean);
            } catch (WTPropertyVetoException e) {
                LoadServerHelper.printMessage("\nserviceable: " + e.getMessage());
                e.printStackTrace();
                throw new WTException(e);
            }
        }
    }

    private static void setServicekit(WTPart part, String servicekit)
            throws WTException {

        if (servicekit != null) {
            try {
                WTPartMaster master = (WTPartMaster) part.getMaster();
                Boolean servicekitBoolean = Boolean.valueOf(servicekit.trim());
                master.setServicekit(servicekitBoolean);
            } catch (WTPropertyVetoException e) {
                LoadServerHelper.printMessage("\nservicekit: " + e.getMessage());
                e.printStackTrace();
                throw new WTException(e);
            }
        }
    }
   protected static void setOrganization( WTPart part, String org_id, String org_name ) throws WTException {
      try {
         WTOrganization org = getOrganization(org_id, org_name);
         if (org != null) {
            part.setOrganization(org);
         }
      }
      catch( Exception e ) {
        LoadServerHelper.printMessage("\nsetOrganization: " + e.getMessage());
        e.printStackTrace();
        throw new WTException(e);
      }
   }

   protected static void setPartType( WTPart the_part, String type ) throws WTException {
      the_part.setPartType(PartType.toPartType(type));
   }

   protected static void setSource( WTPart the_part, String source ) throws WTException {
      the_part.setSource(Source.toSource(source));
   }

   protected static String[] parseTeamTemplate( Hashtable nv, Hashtable cmd_line ) throws WTException {
      String[] retVal = new String[2];
      String fullTeamTemplateName = getValue("teamTemplate",nv,cmd_line,false);
      try {
         if (fullTeamTemplateName != null) {
            //Parse the team template and domain names from the full team template name.
            StringTokenizer teamTemplateTokenizer = new StringTokenizer(fullTeamTemplateName, ".");
            retVal[1] = teamTemplateTokenizer.nextToken();
            retVal[0] = teamTemplateTokenizer.nextToken();
         }
      }
      catch (NoSuchElementException e) {
        String[] messageArgs = { fullTeamTemplateName };
        LoadServerHelper.printMessage(WTMessage.getLocalizedMessage(RESOURCE,
            partResource.LOAD_INVALID_TEAMTEMPLATE, messageArgs ));
        e.printStackTrace();
        throw new WTException(e);
      }
      return retVal;
   }

   ////////////////////////////
   // GENERIC HELPER METHODS //
   ////////////////////////////
   protected static String getValue( String name, Hashtable nv, Hashtable cmd_line, boolean required ) throws WTException {
      String value = LoadServerHelper.getValue(name,nv,cmd_line,required?LoadServerHelper.REQUIRED:LoadServerHelper.NOT_REQUIRED);
      if (value != null) {
         value = value.trim();
         if (value.equals("")) {
            value = null;
         }
      }
      return value;
   }
   protected static String[] getValues( String name, Hashtable nv, Hashtable cmd_line, boolean required ) throws WTException {
      String value = LoadServerHelper.getValue(name,nv,cmd_line,required?LoadServerHelper.REQUIRED:LoadServerHelper.NOT_REQUIRED);
      if(required && value == null) throw new WTException("Required value for " + name + " not provided in input file.");
      if ( value == null ) return null;

      ArrayList values = new ArrayList();
      final char DELIMITER = '/';
      final int IN_TOKEN = 0;
      final int FIRST_DELIMITER = 1;
      char token[] = new char[value.length()];
      int tokenSize = 0;
      int state = IN_TOKEN;
      for(int i = 0; i < value.length(); i++) {
         char c = value.charAt(i);
         switch(state) {
            case IN_TOKEN:
               if(c == DELIMITER) {
                  state = FIRST_DELIMITER;
               }
               else {
                  token[tokenSize++] = c;
               }
               break;
            case FIRST_DELIMITER:
               if(c == DELIMITER) {
                  state = IN_TOKEN;
                  token[tokenSize++] = DELIMITER;
               }
               else {
                  values.add(new String(token, 0, tokenSize));
                  tokenSize = 0;
                  token[tokenSize++] = c;
                  state = IN_TOKEN;
               }
               break;
         }
      }
      values.add(new String(token, 0, tokenSize));

      if(values.size() == 0) {
         return null;
      }

      return (String[])values.toArray(new String[values.size()]);
   }

   protected static boolean getBooleanValue( String name, Hashtable nv, Hashtable cmd_line, boolean required, boolean defaultValue ) throws WTException {
      boolean ret = defaultValue;
      String value = LoadServerHelper.getValue(name,nv,cmd_line,required?LoadServerHelper.REQUIRED:LoadServerHelper.NOT_REQUIRED);

      if(required && value == null) throw new WTException("Required value for " + name + " not provided in input file.");

      if( (value!=null) ) {
         if ( value.trim().equalsIgnoreCase("true") ) {
             ret = true;
         }
         else if ( value.trim().equalsIgnoreCase("false") ) {
             ret = false;
         }
      }

      return ret;
   }

   protected static void setContainer( WTContained the_contained, WTContainerRef containerRef ) throws WTException {
      try {
         if( containerRef != null ) the_contained.setContainerReference( containerRef );
      }
      catch( WTPropertyVetoException e ) {
        LoadServerHelper.printMessage("\nsetContainer: " + e.getMessage());
        e.printStackTrace();
        throw new WTException(e);
      }
   }

   protected static void setType( Typed the_typed, String subtypedef ) throws WTException {
      LoadValue.setType(the_typed,subtypedef);
   }

   private static void setVersion( Versioned the_versioned, String version ) throws WTException {
      try {
         if (version == null  ||  version.trim().length() == 0) {
            // If the version ID string is null then the load file did not specify it.
            version = null;
            if (the_versioned.getVersionInfo() != null)
               // If the object already has a VersionInfo object then assume it is correct
               // and no further action is needed.  Otherwise, make a default VersionInfo object.
               return;
         }

         // Get the version series of the object.
         MultilevelSeries mls = null;
         final Mastered master = the_versioned.getMaster();
         if (master != null) {
            final String masterSeriesName = master.getSeries();
            if (masterSeriesName == null) {
               if (the_versioned instanceof WTContained  &&  ((WTContained) the_versioned).getContainer() != null) {
                  // Retrieve the series based on the OIR in effect for the container and object type/soft type.
                  mls = VersionControlHelper.getVersionIdentifierSeries(the_versioned);
                  wt.vc.VersionControlServerHelper.changeSeries(master, mls.getUniqueSeriesName());
               }
            }
            else {
               // Series name was already set in the master, just use it.
               mls = MultilevelSeries.newMultilevelSeries(masterSeriesName);
            }
         }
         if (mls == null) {
            // Unable to get the series from the master, just use the default series.
            mls = MultilevelSeries.newMultilevelSeries("wt.vc.VersionIdentifier",version);
         }

         if (version != null) {
            // Set the revision ID value if it was given in the load file.
            mls.setValueWithoutValidating(version.trim());
         }

         // Replace the default VID object (if there is one) with the correct one.
         VersionIdentifier vid = VersionIdentifier.newVersionIdentifier(mls);
         VersionControlServerHelper.setVersionIdentifier(the_versioned, vid, false /* validateIncreasing */);
      }
      catch ( WTPropertyVetoException e ) {
        LoadServerHelper.printMessage("\nsetVersion: " + e.getMessage());
        e.printStackTrace();
        throw new WTException(e);
      }
      catch (Exception e) {
         throw new WTException(e);
      }
   }

   protected static void setIteration( Iterated the_iterated, String iteration ) throws WTException {
      try {
         if(iteration != null){
            Series ser = Series.newSeries("wt.vc.IterationIdentifier", iteration);
            IterationIdentifier iid = IterationIdentifier.newIterationIdentifier(ser);
            VersionControlHelper.setIterationIdentifier(the_iterated, iid);
         }
      }
      catch ( WTPropertyVetoException e ) {
        LoadServerHelper.printMessage("\nsetIteration: " + e.getMessage());
        e.printStackTrace();
        throw new WTException(e);
      }
   }

   protected static void setFolder( WTContainerRef containerRef, FolderEntry the_folder_entry, String folderpath ) throws WTException {
      if(folderpath!=null) {
         Folder folder;
         try {
            folder = FolderHelper.service.getFolder(folderpath, containerRef);
         }
         catch(FolderNotFoundException e) {
            folder = null;
         }

         if( folder == null )
            folder = FolderHelper.service.createSubFolder( folderpath, containerRef );

         FolderHelper.assignLocation(the_folder_entry, folder);
      }
   }

   protected static void setLifeCycle(WTContainerRef containerRef, LifeCycleManaged the_lifecycle_managed, String lctemplate ) throws WTException {
      try {
         if(lctemplate!=null) {
            LifeCycleHelper.setLifeCycle(the_lifecycle_managed,LifeCycleHelper.service.getLifeCycleTemplate(lctemplate, containerRef));
         }
      }
      catch ( WTPropertyVetoException e ) {
        LoadServerHelper.printMessage("\nsetLifeCycle: " + e.getMessage());
        e.printStackTrace();
        throw new WTException(e);
      }
   }

   // FOR SOME REASON THIS METHOD IS NOT THE SAME AS IT IS IN wt.doc.LoadDoc
   // SHOULD THEY BE DIFFERENT???
   protected static void setState( LifeCycleManaged the_lifecycle_managed, String state ) throws WTException {
      try {
         if(state!=null) {
              String current_user = wt.session.SessionMgr.getPrincipal().getName();
              String admin_user = wt.admin.AdministrativeDomainHelper.ADMINISTRATOR_NAME;
              boolean is_admin = admin_user.equals(current_user);
            if(!is_admin) {
               LoadServerHelper.changePrincipal(admin_user);
               LoadServerHelper.printMessage("\nSwitching user to Administrator (Setting lifecycle state...)");
            }
            try {
               LifeCycleServerHelper.setState(the_lifecycle_managed,State.toState(state));
            }
            finally {
               if(!is_admin) {
                  LoadServerHelper.changePrincipal(current_user);
                  LoadServerHelper.printMessage("\nSwitching user from Administrator (...lifecycle state set)");
               }
            }
         }
      }
      catch ( WTPropertyVetoException e ) {
        LoadServerHelper.printMessage("\nsetState: " + e.getMessage());
        e.printStackTrace();
        throw new WTException(e);
      }
   }

   protected static void setTeamTemplate( WTContainerRef containerRef, TeamManaged the_team_managed, String teamTemplate, String domain ) throws WTException {
      try {
         if((teamTemplate!=null)&&(domain!=null)) {
            TeamHelper.service.setTeamTemplate(containerRef,the_team_managed,teamTemplate,domain);
         }
      }
      catch ( WTPropertyVetoException e ) {
        LoadServerHelper.printMessage("\nsetTeamTemplate: " + e.getMessage());
        e.printStackTrace();
        throw new WTException(e);
      }
   }

   protected static PDMLinkProduct lookupProduct(String name, WTContainerRef containerRef) throws WTException {
      // Search for the Product
      QuerySpec qs = new QuerySpec (PDMLinkProduct.class);
      qs.appendWhere (new SearchCondition (
             PDMLinkProduct.class,
             PDMLinkProduct.NAME,
             SearchCondition.EQUAL,
             name), 0);
      QueryResult qr = PersistenceHelper.manager.find (qs);

      // Make sure it is in the same organization
      PDMLinkProduct pdmlinkProduct = null;
      while ( qr.hasMoreElements () ) {
         pdmlinkProduct = (PDMLinkProduct)qr.nextElement();
         WTContainerRef ref = pdmlinkProduct.getContainerReference();
         if ( ref.equals(containerRef) ) {
             return pdmlinkProduct;
         }
      }
      return null;
   }

   public static boolean createProductContainer(Hashtable nv, Hashtable cmd_line, Vector return_objects) {
      try {
         setUser(nv,cmd_line);
         String name = getValue( "name", nv, cmd_line, true );
         String number = getValue( "number", nv, cmd_line, false );
         String sharedTeamName = getValue( "sharedTeamName", nv, cmd_line, false);
         String containerExtendable = getValue( "containerExtendable", nv, cmd_line, false);
         String description = getValue( "description", nv, cmd_line, false );
         String viewStr = getValue( "view", nv, cmd_line, false );
         String variation1Str = getValue( "variation1", nv, cmd_line, false );
         String variation2Str = getValue( "variation2", nv, cmd_line, false );
         String sourceStr = getValue( "source", nv, cmd_line, false );
         String defaultUnitStr = getValue( "defaultUnit", nv, cmd_line, false );
         String typeStr = getValue( "type", nv, cmd_line, false );
         String containerTemplateStr = getValue( "containerTemplate", nv, cmd_line, true );
         String orgName = getValue("organizationName",nv,cmd_line,false);
         String orgID = getValue("organizationID",nv,cmd_line,false);
         boolean createEndItem = Boolean.valueOf(getValue("createPrimaryEndItem",nv,cmd_line,false)).booleanValue();

         //Retrieve command line org container
         WTContainerRef orgContainerRef = LoadServerHelper.getTargetContainer( nv, cmd_line );
         OrgContainer orgContainer = null;
         if( orgContainerRef != null ) {
            WTContainer container = orgContainerRef.getReferencedContainer();
            if (container instanceof OrgContainer) {
               orgContainer = (OrgContainer)container;
            }
         }
         //Convert empty string values into null values
         if( name != null && name.length() == 0 )
            name = null;
         if( number != null && number.length() == 0 )
            number = null;
         if( sharedTeamName != null && sharedTeamName.length() == 0 )
            sharedTeamName = null;
         if( containerExtendable != null && containerExtendable.length() == 0 )
            containerExtendable = null;
         if( description != null && description.length() == 0 )
            description = null;
         if( viewStr != null && viewStr.length() == 0 )
            viewStr = null;
         if( variation1Str != null && variation1Str.length() == 0 )
            variation1Str = null;
         if( variation2Str != null && variation2Str.length() == 0 )
            variation2Str = null;
         if( sourceStr != null && sourceStr.length() == 0 )
            sourceStr = null;
         if( defaultUnitStr != null && defaultUnitStr.length() == 0 )
            defaultUnitStr = null;
         if( typeStr != null && typeStr.length() == 0 )
            typeStr = null;
         if( containerTemplateStr != null && containerTemplateStr.length() == 0 )
            containerTemplateStr = null;
         if( orgName != null && orgName.length() == 0 )
            orgName = null;
         if( orgID != null && orgID.length() == 0 )
            orgID = null;

         //Set container extendability
         if( containerExtendable != null ) {
            if( containerExtendable.equalsIgnoreCase("true")) {
               MethodContext.getContext().put(WTContainerHelper.EXTENDABLE_CONTAINER, true);
            }
         }

         //Get the container ref
         WTContainerRef containerRef = LoadServerHelper.getTargetContainer( nv, cmd_line );
         PDMLinkProduct productContainer;

         WTOrganization wtOrg = getOrganization(orgID, orgName);

         //Create end item
         if (createEndItem) {
            WTPart product = WTPart.newWTPart();
            product.setEndItem(true);
            product.setDefaultTraceCode(TraceCode.SERIAL_NUMBER);
            if( number != null )  {
               product.setNumber( number );
            }
            if( name != null ) {
                product.setName( name );
            }

            // Set organization
            if (wtOrg != null) {
               product.setOrganization(wtOrg);
            }

            //Set source
            if( sourceStr != null ) {
               Source source = Source.toSource( sourceStr );
               product.setSource( source );
            }

            //Set view
            if( viewStr != null ) {
               View view = ViewHelper.service.getView( viewStr );
               ViewReference viewRef = ViewReference.newViewReference( view );
               product.setView( viewRef );

               // Set variation1
               if (variation1Str != null) {
                  Variation1 variation1 = Variation1.toVariation1(variation1Str);
                  product.setVariation1(variation1);
               }

               // Set variation2
               if (variation2Str != null) {
                  Variation2 variation2 = Variation2.toVariation2(variation2Str);
                  product.setVariation2(variation2);
               }
            }

            //Set unit
            if( defaultUnitStr != null ) {
               QuantityUnit defaultUnit = QuantityUnit.toQuantityUnit( defaultUnitStr );
               product.setDefaultUnit( defaultUnit );
            }

            //Set type
            if( typeStr != null ) {
               PartType type = PartType.toPartType( typeStr );
               product.setPartType( type );
            }

            //Create product container with enditem
            productContainer = PDMLinkProduct.newPDMLinkProduct( product );
         } else {
            //Create product container
            productContainer = PDMLinkProduct.newPDMLinkProduct();
         }

         //Set shared team
         if( sharedTeamName != null && orgContainer != null ) {
            ContainerTeam sharedTeam = ContainerTeamHelper.service.getSharedTeamByName(orgContainer, sharedTeamName);
            if (sharedTeam != null) {
               ContainerTeamReference sharedTeamRef = ContainerTeamReference.newContainerTeamReference(sharedTeam);
               productContainer = (PDMLinkProduct)ContainerTeamHelper.assignSharedTeamToContainer(productContainer, sharedTeamRef, false);
            }
         }

         //Set easy attributes
         productContainer.setName( name );
         productContainer.setDescription( description );
         productContainer.setContainerReference( containerRef );
         if (wtOrg != null)
            productContainer.setOrganization(wtOrg);

         //Set container template
         if( containerTemplateStr != null && containerTemplateStr.length() != 0 ) {
            QuerySpec query = new QuerySpec( WTContainerTemplateMaster.class );
            SearchCondition where = new SearchCondition( WTContainerTemplateMaster.class, WTContainerTemplateMaster.NAME, SearchCondition.EQUAL, containerTemplateStr );
            query.appendWhere( where, new int[]{0, 1} );

            LookupSpec lookup = new LookupSpec( query, containerRef );
            lookup.setFirstMatchOnly( true );
            QueryResult results = WTContainerHelper.service.lookup( lookup );

            if( !results.hasMoreElements() ) {
               String[] parms = {containerTemplateStr};
               throw new WTException( RESOURCE, partResource.CONTAINER_TEMPLATE_NOT_FOUND, parms );
            }
            WTContainerTemplateMaster templateMaster = (WTContainerTemplateMaster)results.nextElement();
            WTContainerTemplate containerTemplate = ContainerTemplateHelper.service.getContainerTemplateRef( templateMaster ).getTemplate();
            productContainer.setContainerTemplate( containerTemplate );
         }

         //Create container and end item
         WTContainerHelper.service.create( productContainer );

         //Make sure folders got created
         WTContainerRef productRef = WTContainerRef.newWTContainerRef( productContainer );
         try {
            FolderHelper.service.createSubFolder( "/System/Reports", productRef );
         }
         catch(UniquenessException e) {
            //ignore - may exist from the template
         }
         try {
            FolderHelper.service.createSubFolder( "/System/Reports/ChangeMonitor", productRef );
         }
         catch(UniquenessException e) {
            //ignore - may exist from the template
         }
         try {
            FolderHelper.service.createSubFolder( "/System/Reports/ChangeMonitor/Custom", productRef );
         }
         catch(UniquenessException e) {
            //ignore - may exist from the template
         }

         return_objects.add( productContainer );
         return true;
      }
      catch(WTException e) {
         LoadServerHelper.printMessage( "createProductContainer: " + e.getLocalizedMessage() );
         e.printStackTrace();
         return false;
      }
      catch(WTInvalidParameterException e) {
         LoadServerHelper.printMessage( "createProductContainer: " + e.getLocalizedMessage() );
         e.printStackTrace();
         return false;
      }
      catch(WTPropertyVetoException e) {
         LoadServerHelper.printMessage( "createProductContainer: " + e.getLocalizedMessage() );
         e.printStackTrace();
         return false;
      }
      finally{
        try{
            resetUser();
            // When loading several containers, make sure the EXTENDABLE flag is turned off after it is turned on
            // to prevent subsequent containers from having the value set to 1 when it should be 0.
            MethodContext.getContext().remove(WTContainerHelper.EXTENDABLE_CONTAINER);
        }
        catch( WTException e ) {
            LoadServerHelper.printMessage("\ncreateProductContainer: " + e.getMessage());
        }
      }
   }
   public static boolean createProductSerialNumberEffectivity(Hashtable nv, Hashtable cmd_line, Vector return_objects) {
      return createProductEffectivity(nv, cmd_line, return_objects, "wt.part.ProductSerialNumberEffectivity");
   }

   public static boolean createProductLotNumberEffectivity(Hashtable nv, Hashtable cmd_line, Vector return_objects) {
      return createProductEffectivity(nv, cmd_line, return_objects, "wt.part.ProductLotNumberEffectivity");
   }

   public static boolean createProductMSNEffectivity(Hashtable nv, Hashtable cmd_line, Vector return_objects) {
       return createProductEffectivity(nv, cmd_line, return_objects, "wt.part.ProductMSNEffectivity");
    }
   public static boolean createProductBlockEffectivity(Hashtable nv, Hashtable cmd_line, Vector return_objects) {
       return createProductEffectivity(nv, cmd_line, return_objects, "wt.part.ProductBlockEffectivity");
   }

   protected static boolean createProductEffectivity(Hashtable nv, Hashtable cmd_line, Vector return_objects, String effType) {
      String[] messageArgs = new String[2];
      String message = null;

      try {
         String prodNumber = getValue("prodNumber", nv, cmd_line, true);
         String type = getValue("type", nv, cmd_line, false);
         String targetNumber = getValue("targetNumber", nv, cmd_line, true);
         String targetVer = getValue("targetVer", nv, cmd_line, false);
         String typeModifier = getValue("typeModifier", nv, cmd_line, false);
         String start = getValue("startNumber", nv, cmd_line, true);
         String end = getValue("endNumber", nv, cmd_line, false);

         //Convert empty string values into null values
         if (prodNumber != null && prodNumber.length() == 0) prodNumber = null;
         if (type == null || type.length() == 0) type = "wt.part.WTPart";
         if (targetNumber != null && targetNumber.length() == 0) targetNumber = null;
         if (targetVer != null && targetVer.length() == 0) targetVer = null;
         if (typeModifier != null && typeModifier.length() == 0) typeModifier = null;
         if (start != null && start.length() == 0) start = null;
         if (end != null && end.length() == 0) end = null;

         if ((prodNumber == null) || (targetNumber == null) || (start == null)) {
            throw new WTException();
         }

         //Get the product master
         QuerySpec qs = new QuerySpec(WTPartMaster.class);
         qs.appendWhere(new SearchCondition(WTPartMaster.class,
                                            WTPartMaster.NUMBER,
                                            SearchCondition.EQUAL,
                                            prodNumber.toUpperCase()), new int[] { 0 });
         QueryResult qr = PersistenceHelper.manager.find(qs);
         WTPartMaster master = null;
         if (qr.size() == 0 ) {
            messageArgs[0] = prodNumber;
            message = WTMessage.getLocalizedMessage(RESOURCE, partResource.FAILED_LOCATE_PRODUCT, messageArgs);
            LoadServerHelper.printMessage("\n" + message);
            return false;
         } else if(qr.size() > 1 ) {
            messageArgs[0] = type;
            messageArgs[1] = prodNumber;
            message = WTMessage.getLocalizedMessage(RESOURCE, partResource.MULTIPLE_OBJECTS_FOUND, messageArgs);
            LoadServerHelper.printMessage("\n" + message);
            return false;
         } else {
            master = (WTPartMaster)qr.nextElement();
         }
         Class[] effTypeValidValues= EffHelper.getValidEffForms(master);
         Class effRecordClass = Class.forName(effType);
         Class inputEffTypeClass = EffHelper.getEffFormClass(effRecordClass);
         boolean validEffType = false;
         for (int i = 0; i < effTypeValidValues.length; i++) {
             if ( effTypeValidValues[i].getName().equals(inputEffTypeClass.getName()) ) {
                 validEffType = true;
                 break;
             }
         }
         if ( !validEffType ) {
             messageArgs[0] = EffFormHelper.getFormDisplayName(inputEffTypeClass);
             messageArgs[1] = prodNumber;
             message = WTMessage.getLocalizedMessage(RESOURCE, partResource.EFF_TYPE_ERROR, messageArgs);
             LoadServerHelper.printMessage("\n" + message);
             return false;
         }

         //Get the part or product
         WTHashSet effVers = new WTHashSet(10, CollectionsHelper.VERSION_FOREIGN_KEY);
         if (type.equals("wt.part.WTPart")) {
            effVers.add(getPart(targetNumber, targetVer, null, null));
         }
         if (effVers.size() ==0){
            messageArgs[0] = type;
            messageArgs[1] = targetNumber;
            message = WTMessage.getLocalizedMessage(RESOURCE, partResource.OBJECT_NOT_FOUND, messageArgs);
            LoadServerHelper.printMessage("\n" + message);
            return false;
         }

         String effValue = start + wt.eff.EffGroupAssistant.getDash();
         if (end != null) effValue = effValue + end;
         wt.eff.EffGroup[] effGroups = new wt.eff.EffGroup[1];
         effGroups[0] = new wt.eff.EffGroup((ObjectReference) EffHelper.getWTReference(master), Class.forName(effType), effValue);
         if(typeModifier != null)
         {
             EffTypeModifier modifier = EffTypeModifier.toEffTypeModifier(typeModifier);
             effGroups[0].setEffTypeModifier(modifier);
         }
         wt.eff.EffGroupAssistant.appendEffGroups(effVers, effGroups);

         return true;
      } catch(Exception e) {
         LoadServerHelper.printMessage("createProductEffectivity: " + e.getLocalizedMessage());
         e.printStackTrace();
         return false;
      }
   }

   protected static void setUser( Hashtable nv, Hashtable cmd_line ) throws WTException {
    LoadServerHelper.setCacheValue(PART_PREVIOUS_USER,wt.session.SessionMgr.getPrincipal().getName());
    String user = getValue("user",nv,cmd_line,false);
    if(user!=null) LoadServerHelper.changePrincipal(user);
}
   protected static void resetUser() throws WTException {
    String user = (String)LoadServerHelper.getCacheValue(PART_PREVIOUS_USER);
    if(user!=null) LoadServerHelper.changePrincipal(user);
}

   /**
    * Get the organization with the given ID or name
    * The org id represents the unique identifier in LDAP and consists of both values for the search to work.
    * Note that the org id value before the '$' represents the Type of the Organization ID.  If you look in LDAP you will see
    * that each Organization has an attribute named 'ptcUniqueOrganizationIdentifier' and this identifier is the concatenation
    * of the Type ('0141' for Cage, '0060' for DUNS and '0026' for ISO65233) and the Org ID with a '$' as a separator between the two.
    * The org id format is <coding system>$<value>.
    * The coding system is convert to number representation that stored in LDAP before use to retrieve org.
    * If org id and org name are provided,
    * first use org id to retrieve organization then vertify org name.
    * If organization is retrieved by org name and if org id was also provided, vertify org id.
    *
    * @param org_id
    * @param org_name
    *
    * @return WTOrganization
    **/
   public static WTOrganization getOrganization(final String org_id, final String org_name) throws WTException {
      if ((org_id == null) && (org_name == null)) {
         return null;
      }
      class OrgKey {
         volatile int hashCode;
         final String key;
         OrgKey(String k) {
            key = k;
         }
         public boolean equals(Object o) {
            if (o == this) {
               return true;
            }
            if (!(o instanceof OrgKey)) {
               return false;
            }
            OrgKey other = (OrgKey)o;
            return key.equals(other.key);
         }
         public int hashCode() {
            if (hashCode == 0) {
               int result = 17;
               result = 37*result + key.hashCode();
               hashCode = result;
            }
            return hashCode;
         }
         public String toString() {
            return "wt.load.LoadPart.OrgKey [key=\""+key+"\"]";
         }
      }

      String orgId = null;
      String codingSystemStr = null;
      String uniqueId = null;
      String codingSystem = null;
      OrgKey id_key = null;

      if (org_id != null) {
         int delimiterIndex = org_id.indexOf("$");
         if (delimiterIndex < 0) {
            throw new WTException("Invalid orgainzation id. The format is <coding system>$<value>.");
         }

         codingSystemStr = org_id.substring(0, delimiterIndex);
         uniqueId = org_id.substring(delimiterIndex+1);
         codingSystem = getCodingSystem(codingSystemStr);

         orgId = codingSystem + "$" + uniqueId;
         id_key = new OrgKey(orgId);
      }

      OrgKey name_key = (org_name == null ? null : new OrgKey(org_name));
      WTOrganization org = null;
      if (id_key != null) {
         org = (WTOrganization)LoadServerHelper.getFromCache(id_key);
      }

      if (org == null && name_key != null) {
         org = (WTOrganization)LoadServerHelper.getFromCache(name_key);
      }

      if (org == null) {
         if (org_id != null) {
            org = IxbHndHelper.getOrganizationByGlobalOrgId(orgId);

            if (VERBOSE) {
               System.out.println("LoadPart.getOrganization() found organization. ID Key: " + id_key + " Org: " + org);
            }

            if (org_name != null)
               vertifyOrgName(org_name, org);
         }

         if ((org == null) && (org_name != null)) {
            DirectoryContextProvider dcp = WTContainerHelper.service.getExchangeContainer().getContextProvider();
            org = OrganizationServicesHelper.manager.getOrganization(org_name, dcp);

            if (VERBOSE) {
               System.out.println("LoadPart.getOrganization() found organization. Name Key: " + name_key + " Org: " + org);
            }

            if (org_id != null)
               vertifyOrgId(codingSystemStr, uniqueId, codingSystem, org);
         }

         if (org != null) {
            if (id_key == null) {
               String id = getOrgID(org);
               if (id != null) {
                  id_key = new OrgKey(id);
               }
            }
               LoadServerHelper.putCacheValue(id_key, org);
            if (name_key == null) {
               name_key = new OrgKey(org.getName());
            }
            LoadServerHelper.putCacheValue(name_key, org);
         }
      }

      return org;
   }

   private static String getCodingSystem(String codingStr)
      throws WTException {

      String value = null;
      if(codingSystemMap.values().contains(codingStr)) {
         value = codingStr;
      } else {
         value = codingSystemMap.get(codingStr.toUpperCase());

         if(value == null) {
           throw new WTException("Invalid coding system.");
         }
      }

      return value;
   }

   private static void vertifyOrgId(String codingSystemStr, String uniqueId, String codingSystem, WTOrganization org) throws WTException {
      if (org != null) {
         String expectedCodingSystem = null;
         Set keySet = codingSystemMap.keySet();
         for(Object key : keySet) {
            if(codingSystemMap.get(key).equals((org.getCodingSystem()))) {
               expectedCodingSystem = (String)key;
            }
         }

         if (!org.getUniqueIdentifier().equals(uniqueId) || !org.getCodingSystem().equals(codingSystem)) {
            throw new WTException("Org with matching name does not have matching ID.\n"
               + " Actual coding system: " + expectedCodingSystem + " (" + org.getCodingSystem() + ")" + "\n"
               + " Expected org name: " + codingSystemStr + "\n"
               + " Actual unique identifier: " + org.getUniqueIdentifier() + "\n"
               + " Expected unique identifier: " + uniqueId + "\n"
               + " Org: " + org);
         }
      }
   }

   private static void vertifyOrgName(String org_name, WTOrganization org) throws WTException {
      if ((org_name != null) && org!= null && !org.getName().equals(org_name)) {
         throw new WTException("Org with matching ID does not have matching name.\n"
            + "Actual org name: " + org.getName() + "\n"
            + "Expected org name: " + org_name + "\n"
            + "Org: " + org);
      }
   }

   private static String getOrgID(WTOrganization org) {
      if (org.getCodingSystem() == null) {
         return null;
      }
      return org.getCodingSystem() + '$' + org.getUniqueIdentifier();
   }

   protected static void setGenericType(WTPart the_part, String genericType)
      throws WTException
   {
      try {
         if(genericType!=null) {
            the_part.setGenericType(GenericType.toGenericType(genericType));
         }
      } catch(WTPropertyVetoException wtpve){
         System.out.println(wtpve.getMessage());
      }
   }

   protected static void setSecurityLabels( SecurityLabeled obj, String securityLabels )
   throws WTException {
      if ((securityLabels!=null) && (securityLabels.length() > 0)) {
         try {
               AccessControlServerHelper.manager.setSecurityLabels(obj, securityLabels, false);
         } catch (WTPropertyVetoException wtpve) {
            throw new WTException (wtpve);
         }
      }
   }

   protected static void setLogicBase(WTPart part, Hashtable nv, Hashtable cmd_line)
   throws WTException
   {
      try {
         String path = LoadServerHelper.getValue("logicbasePath", nv, cmd_line,
                 LoadServerHelper.NOT_REQUIRED);
         if (path != null) {
            Class name = Class.forName("com.ptc.wpcfg.load.LoadHelper");
            Method method = name.getMethod("setLogicBase",
                  new Class[] {WTPart.class, String.class,
                     Hashtable.class, Hashtable.class});
            method.invoke(null, new Object[] {part, path, nv, cmd_line});
         }
      } catch (SecurityException e) {
          e.printStackTrace();
      } catch (IllegalArgumentException e) {
          e.printStackTrace();
      } catch (InvocationTargetException e) {
          e.printStackTrace();
      } catch (ClassNotFoundException e) {
          e.printStackTrace();
      } catch (NoSuchMethodException e) {
          e.printStackTrace();
      } catch (IllegalAccessException e) {
          e.printStackTrace();
      }
   }

   /**
    * Determines if the child is the same as the parent.
    *
    * @param parent_ti the parent type instance
    * @param child_ti the child type instance
    * @return true, if the child is the same as the parent; false, otherwise
    */
   private static boolean isChildSameAsParent(WTPart parent, WTPartMaster child)
   {
      if (child.equals(parent.getMaster())){
         return true;
      }

      // theoretically we should check all of its parents, however, we don't
      // have the product structure here.  So we are going just to assume it is fine
      // with all of its parents.
      return false;
   }
    /***
    * Checks if the ROLEA_OBJECT_ID and ROLEB_OBJECT_ID are already present in the WTPartDescribeLink table.
    * @param part
    * @param document
    * @return
    * @throws WTException
    */
   private static boolean isDuplicatePartDocRecord(WTPart part, WTDocument document) throws WTException{
   boolean flag = false;

   QuerySpec querySpec = new QuerySpec();
   querySpec.appendClassList(WTPartDescribeLink.class, true);

   querySpec.appendWhere(new SearchCondition(WTPartDescribeLink.class,WTAttributeNameIfc.ROLEA_OBJECT_ID,
         SearchCondition.EQUAL,part.getPersistInfo().getObjectIdentifier().getId()));
   querySpec.appendAnd();
   querySpec.appendWhere(new SearchCondition(WTPartDescribeLink.class,WTAttributeNameIfc.ROLEB_OBJECT_ID,
         SearchCondition.EQUAL,document.getPersistInfo().getObjectIdentifier().getId()));

   QueryResult qr = PersistenceHelper.manager.find(querySpec);

   if(qr.hasMoreElements()){
      flag = true;
      return flag;
   }

   return flag;
   }
}

/*
public static WTPartUsageLink deleteUsageLink( WTPartUsageLink usagelink ) throws WTException
  {
    try
    {
        usagelink = (WTPartUsageLink)PersistenceHelper.manager.delete(usagelink);
    }
    catch (WTException e)
    {
        e.printStackTrace();
        throw(e);
    }
    return usagelink;
  }
*/
