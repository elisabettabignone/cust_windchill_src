/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.part;

import wt.util.resource.*;

@RBUUID("wt.part.partResource")
public final class partResource_it extends WTListResourceBundle {
   @RBEntry("Il valore per l'argomento \"{0}\" non può essere nullo")
   public static final String NULL_ARGUMENT = "0";

   @RBEntry("La vista specificata non è valida: specificarne una persistente")
   public static final String VIEW_NOT_PERSISTENT = "1";

   @RBEntry("È stato restituito più di un WTPartConfigSpec. Se ne attendeva solo uno.")
   public static final String QUERY_WTPARTCONFIGSPEC_QTY = "2";

   @RBEntry("L'attributo numero deve avere un valore")
   public static final String NULL_NUMBER = "3";

   @RBEntry("All'attributo nome deve essere associato un valore")
   public static final String NULL_NAME = "19";

   @RBEntry("L'assegnazione dei numeri di riga dell'assieme \"{0}\" non è conforme. I numeri di riga devono essere assegnati a tutte le parti usate dall'assieme o a nessuna.")
   public static final String INCONSISTENT_LINENUMBER_USAGE = "170";

   /**
    * Messages for LoadPart -----------------------------------------------------
    *
    **/
   @RBEntry("\naddPartToAssembly: La parte \"{0}\" non esiste nel file di caricamento oppure non è stata creata.")
   @RBArgComment0("part number")
   @RBComment("Method name \"addPartToAssembly\" at beginning of this message should not be translated.  The rest of the message text can be translated. This message is displayed only in the output from running the command line tool LoadFromFile.")
   public static final String LOAD_NO_PART = "4";

   @RBEntry("\"{0}\" aggiunta a \"{1}\"")
   public static final String LOAD_PARTS_ADDED = "5";

   @RBEntry("Documento di riferimento \"{0}\" alla parte \"{1}\"")
   public static final String LOAD_DOC_ADDED = "6";

   @RBEntry("\ncreatePartDocReference: Il documento \"{0}\" non esiste.")
   @RBComment("Method name \"createPartDocReference\" at beginning of this message should not be translated.  The rest of the message text can be translated.  This message is displayed only in the output from running the command line tool LoadFromFile.")
   public static final String LOAD_NO_DOC = "7";

   @RBEntry("\ncreatePartDocReference: Nessuna parte disponibile. È necessario prima creare un assieme o una parte componente.")
   @RBComment("Method name \"createPartDocReference\" at beginning of this message should not be translated.  The rest of the message text can be translated.  This message is displayed only in the output from running the command line tool LoadFromFile.")
   public static final String LOAD_NO_DOC_PART = "8";

   @RBEntry("\nsetPartAttributes: \"{0}\" è un nome di team non valido. Il nome deve avere la forma 'Dominio.Team'")
   @RBComment("Method name \"setPartAttributes\" at beginning of this message should not be translated.  The rest of the message text can be translated.  This message is displayed only in the output from running the command line tool LoadFromFile.")
   public static final String LOAD_INVALID_PROJECT = "9";

   @RBEntry("\"{0}\" è un nome di campo non valido. Verificare il nome corretto nel file csvmapfile.txt.")
   @RBComment("File name \"csvmapfile.txt\" at beginning of this message should not be translated.  The rest of the message text can be translated.  This message is displayed only in the output from running the command line tool LoadFromFile.")
   public static final String LOAD_INVALID_NAME = "10";

   @RBEntry("Nel file di input non è stato trovato un valore per il campo obbligatorio \"{0}\".")
   public static final String LOAD_REQUIRED_FIELD = "11";

   /**
    * Messages for HTML Template Processing --------------------------------------
    *
    **/
   @RBEntry("Parametri CGI non validi. Il parametro \"{0}\" non è stato trovato nella stringa di interrogazione \"{1}\"")
   public static final String INVALID_PARAMETERS = "12";

   @RBEntry(" {0}{1} {2} {3} di {4}")
   @RBComment("For example, \"100: 16 each of Bolt - 1234\" or \"200: 5 gallons of Paint - 333\"")
   @RBArgComment0("is a line number")
   @RBArgComment1("is a separator")
   @RBArgComment2("is a quantity")
   @RBArgComment3("is a unit")
   @RBArgComment4("is and identity")
   public static final String PART_USAGE = "13";

   @RBEntry("Impossibile completare gli attributi del configuration item nell'oggetto.")
   public static final String INFLATE_FAILED = "14";

   /**
    * Messages for PartEffectivityConfigSpec -------------------------------------
    *
    **/
   @RBEntry("Impossibile aggiungere criteri di ricerca per la selezione di parti in base alle informazioni di effettività.  Il metodo PartEffectivityConfigSpec non è stato impostato correttamente.")
   public static final String INVALID_PART_EFF_CONFIG_SPEC = "15";

   /**
    * Messages for BaselineEffectivityConfigSpec ---------------------------------
    *
    **/
   @RBEntry("Impossibile aggiungere criteri di ricerca per la selezione di parti in base alle informazioni relative alle baseline.  È possibile che la baseline sia stata eliminata.")
   public static final String INVALID_PART_BASELINE_CONFIG_SPEC = "16";

   /**
    * Messages for MultilevelBomCompare ------------------------------------------
    *
    **/
   @RBEntry("Interrogazione di confronto di distinte base multilivello")
   public static final String MULTI_BOM_QUERY_TITLE = "40";

   @RBEntry("Origine")
   public static final String SOURCE_HEADER = "41";

   @RBEntry("Destinazione")
   public static final String TARGET_HEADER = "42";

   @RBEntry("Parte:")
   public static final String PART_LABEL = "43";

   @RBEntry("Specifica di configurazione:")
   public static final String CONFIG_SPEC_LABEL = "44";

   @RBEntry("Confronto di distinte base multilivello")
   public static final String MULTI_BOM_TITLE = "45";

   @RBEntry("Parte origine")
   public static final String SOURCE_PART_LABEL = "46";

   @RBEntry("Parte di destinazione")
   public static final String TARGET_PART_LABEL = "47";

   @RBEntry("Reimposta parte destinazione")
   public static final String RESET_LINK_LABEL = "48";

   @RBEntry("Livello")
   public static final String LEVEL_HEADER = "49";

   @RBEntry("Differenze della struttura di prodotto")
   public static final String PRODUCT_STRUCTURE_HEADER = "50";

   @RBEntry("Versione usata")
   public static final String VERSION_HEADER = "51";

   @RBEntry("Quantità")
   public static final String QUANTITY_HEADER = "52";

   @RBEntry("Versione non specificata")
   public static final String VERSION_NOT_SPECIFIED = "53";

   @RBEntry("Confronto di distinte base multilivello")
   public static final String MULTI_BOM_COMPARE_LABEL = "54";

   @RBEntry("Dove usato (multilivello)")
   public static final String MULTI_WHERE_USED_TITLE = "55";

   @RBEntry("Dove usato (multilivello)")
   public static final String MULTI_WHERE_USED_LABEL = "56";

   @RBEntry("Annulla")
   public static final String CANCEL_BUTTON_LABEL = "57";

   @RBEntry("Genera report")
   public static final String GENERATE_REPORT_BUTTON_LABEL = "58";

   @RBEntry("Cerca parti...")
   public static final String SEARCH_PARTS_BUTTON_LABEL = "59";

   @RBEntry("Imposta spec config...")
   public static final String SET_CONFIG_SPEC_BUTTON_LABEL = "60";

   @RBEntry("Nome:")
   public static final String GENERIC_PART_LABEL = "158";

   @RBEntry("Cerca...")
   public static final String GENERIC_SEARCH_PARTS_BUTTON_LABEL = "159";

   @RBEntry("Nome origine:")
   public static final String GENERIC_SOURCE_PART_LABEL = "160";

   @RBEntry("Nome destinazione:")
   public static final String GENERIC_TARGET_PART_LABEL = "161";

   @RBEntry("La specifica di configurazione di destinazione non seleziona una versione della parte di destinazione; è necessario selezionare una parte e/o una specifica di configurazione di destinazione diversi.")
   public static final String MULTI_BOM_QUERY_NO_TARGET_VERSION = "74";

   @RBEntry("Le parti di origine e di destinazione e la specifica di configurazione sono identici; è necessario selezionare una parte e/o una specifica di configurazione di destinazione diversi.")
   @RBPseudo(false)
   @RBComment("Messages for Instance and Config are in com.ptc.windchill.pdmlink.part.server.processors.processorsResource.rbInfo")
   public static final String MULTI_BOM_QUERY_SAME_VERSIONS = "75";

   /**
    * ----------------------------------------------------------------------
    **/
   @RBEntry("{0} {1}")
   @RBComment(" Used to identify the quantity of a QTPartUsageLink. Example: 1.0 each")
   @RBArgComment0("Value of the amount attribute of WTPartUsageLink")
   @RBArgComment1("Value of the unit attribute of WTPartUsageLink")
   public static final String QUANTITY_FORMAT = "61";

   @RBEntry("{0}")
   @RBComment(" PART_TYPE - Used to identify the part type, Example Result,  Part")
   @RBArgComment0("Class name from Introspection, eg. Part")
   public static final String PART_TYPE = "17";

   /**
    * ----------------------------------------------------------------------
    **/
   @RBEntry("La baseline non può essere eliminata perché costituisce un riferimento in una specifica di configurazione.")
   public static final String CANNOT_DELETE_BASELINE = "18";

   @RBEntry("Distinta base parti per {0}")
   @RBArgComment0("part number")
   public static final String PARTS_LIST_DISPLAY_NAME = "20";

   @RBEntry("Distinta base gerarchica per {0}")
   @RBArgComment0("part number")
   public static final String HIERARCHY_DISPLAY_NAME = "21";

   @RBEntry("Distinta base per {0}")
   @RBArgComment0("part number")
   public static final String BILL_OF_MATERIALS_DISPLAY_NAME = "22";

   @RBEntry("{0} ({1}) {2}")
   @RBComment("Format for displaying part information.")
   @RBArgComment0("part number")
   @RBArgComment1("part name")
   @RBArgComment2("part version")
   public static final String PART_DISPLAY_FORMAT = "23";

   @RBEntry("Non si dispone del permesso di pubblicare {0}")
   @RBArgComment0("part number")
   public static final String PUBLISH_NOT_ALLOWED = "24";

   @RBEntry("Imposta specifica di configurazione")
   public static final String SET_CONFIG_SPEC_LABEL = "25";

   @RBEntry("Defisci alternative")
   public static final String CANNOT_UNDO_CHECKOUT = "26";

   @RBEntry("Rimuovi")
   public static final String MULTIPLE_QUERY_RESULT = "27";

   @RBEntry("Doppio senso")
   public static final String TWOWAY = "28";

   @RBEntry("   OK   ")
   public static final String OK_LABEL = "29";

   @RBEntry("Non sono state definite parti alternative")
   public static final String NO_ALTERNATES_DEFINED = "30";

   @RBEntry("{0} è già un'alternativa di {1}.")
   @RBArgComment0("part number")
   @RBArgComment1("part name")
   public static final String ALREADY_AN_ALT = "31";

   @RBEntry("La chiamata di \"{0}\" manca del parametro \"{1}\".")
   @RBArgComment0("part number")
   @RBArgComment1("part name")
   public static final String MISSING_PARAMETER = "32";

   @RBEntry("La chiamata di \"{0}\" ha un parametro invalido.")
   @RBArgComment0("part number")
   public static final String BAD_PARAMETERS = "33";

   @RBEntry("I dati \"{0}\" mancano dal modulo \"{1}\".")
   @RBArgComment0("part number")
   @RBArgComment1("part name")
   public static final String MISSING_FORM_DATA = "34";

   @RBEntry("Trova parte")
   @RBComment(" Header for Part Local Search Form")
   public static final String FIND_PART_TITLE = "35";

   @RBEntry("Risultati della ricerca locale sulle alternative")
   @RBComment("Window frame title for page displaying search results when adding part alternates")
   public static final String ALTS_SEARCH_RESULTS_HEADER = "36";

   @RBEntry("Risultati della ricerca")
   @RBComment("The next is the first line of the title on the search results page used when adding part alternates.")
   public static final String ALTS_SEARCH_RESULTS_TITLE = "37";

   @RBEntry("per l'aggiunta di alternative di {0}")
   @RBComment("The next is the second line of the header on the search results page used when adding part alternates.  It appears in smaller type than the first line above.")
   @RBArgComment0("part number")
   public static final String FOR_ADDING_ALTERNATES_OF = "38";

   @RBEntry("Aggiungi alternative")
   @RBComment("The next is the label on the submit button of the alternates local search results form.")
   public static final String ADD_ALTERNATES_LABEL = "39";

   @RBEntry("Definisci sostituzioni")
   @RBComment("Window frame title for Define Substitutes page")
   public static final String DEFINE_SUBSTITUTES_TITLE = "62";

   @RBEntry("Definisci sostituzioni per la parte {0} nell'assieme {1}")
   @RBComment("Page title for Define Substitutes page.")
   @RBArgComment0("part number")
   @RBArgComment1("part name")
   public static final String DEFINE_SUBSTITUTES_HEADER = "63";

   @RBEntry("Definisci le alternative per la parte {0}")
   @RBArgComment0("part number")
   public static final String DEFINE_ALTERNATES_HEADER = "64";

   @RBEntry("Risultati della ricerca locale sulle sostituzioni")
   @RBComment("Window frame title for page displaying search results when adding part substitutes")
   public static final String SUBS_SEARCH_RESULTS_HEADER = "65";

   @RBEntry("Risultati della ricerca")
   @RBComment("The next is the first line of the title on the search results page used when adding part substitutes.")
   public static final String SUBS_SEARCH_RESULTS_TITLE = "66";

   @RBEntry("per l'aggiunta di sostituzioni per la parte {0} <BR> nell'assieme {1}")
   @RBComment("The next is the second line of the header on the search results page used when adding part alternates.  It appears in smaller type than the first line above.  The inserts are part identifiers, e.g. \"123 (Big Bolt)\".  Line feeds can be forced by inserting the characters \"<BR>\" at desired line breaks.")
   @RBArgComment0("part number")
   @RBArgComment1("part name")
   public static final String FOR_ADDING_SUBSTITUTES_FOR = "67";

   @RBEntry("Aggiungi sostituzioni")
   @RBComment("The next is the label on the submit button of the substitutes local search results form")
   public static final String ADD_SUBSTITUTES_LABEL = "68";

   @RBEntry("La parte {0} è già una sostituzione di questa parte")
   @RBComment("Insert in  next is a part identifier string")
   @RBArgComment0("part number")
   public static final String ALREADY_A_SUB = "69";

   @RBEntry("Modifica i criteri di ricerca")
   @RBComment("Label for Modify Search Criteria button on Alternates/Substitutes Local Search Results page")
   public static final String MODIFY_SEARCH_CRITERIA_LABEL = "72";

   @RBEntry("Una parte non può essere l'alternativa o la sostituzione di se stessa.")
   public static final String CANNOT_REPLACE_BY_SAME_OBJ = "73";

   /**
    * Product Structure Page ---------------------------------------------------
    *
    **/
   @RBEntry("Elenco parti")
   public static final String PART_LIST_LABEL = "70";

   @RBEntry("Report distinta base")
   public static final String BOM_REPORT_LABEL = "71";

   @RBEntry("Sì")
   public static final String YES = "76";

   @RBEntry("No")
   public static final String NO = "77";

   @RBEntry("La specifica di configurazione sopra identificata non seleziona una versione di parte di {0} per {1}. Fare clic sul link della specifica di configurazione soprastante  per scegliere una nuova configurazione.")
   public static final String NO_SOLUTION_FOR_PRODUCT_INSTANCE = "78";

   /**
    * ----------------------------------------------------------------------------
    * Generic Resource Bundle entry for appending ellipses to some given string.
    *
    **/
   @RBEntry("{0}...")
   public static final String APPEND_ELLIPSES = "79";

   @RBEntry("Aggiungi alternative...")
   public static final String ADD_ALTERNATES_BUTTON = "80";

   @RBEntry("Aggiungi sostituzioni...")
   public static final String ADD_SUBSTITUTES_BUTTON = "81";

   @RBEntry("Sostituzioni per {0}")
   public static final String REPLACEMENTS_FOR_LABEL = "82";

   /**
    * ----------------------------------------------------------------------
    **/
   @RBEntry("La versione di parte di livello superiore è stata modificata da {0} a {1} per riflettere la specifica di configurazione usata.")
   public static final String PART_VERSION_HAS_CHANGED = "83";

   @RBEntry("La specifica di configurazione sopra identificata non seleziona una versione di parte di {0}. Fare clic sul link della specifica di configurazione soprastante per scegliere una nuova configurazione.")
   public static final String NO_PART_VERSION_FOUND = "84";

   @RBEntry("{0} {2} casi d'impiego di '{3}' nel contesto di {1}")
   @RBArgComment0("The Icon for the context master (or version) for the uses occurrences that follow")
   @RBArgComment1("An Identifier for the context master (or version) for the uses occurrences that follow")
   @RBArgComment2("The reference designator of the part")
   @RBArgComment3("The part itself")
   public static final String USES_OCCURRENCE_CONTEXT = "85";

   @RBEntry("{0} {2} casi d'impiego di '{3}' nel contesto di {1}")
   @RBArgComment0("The Icon for the context master (or version) for the uses occurrences that follow")
   @RBArgComment1("An Identifier for the context master (or version) for the uses occurrences that follow")
   @RBArgComment2("The reference designator of the part")
   @RBArgComment3("The part itself")
   public static final String PATH_OCCURRENCE_CONTEXT = "86";

   @RBEntry("{0} {2} casi d'impiego di '{3}' nel contesto di {1}")
   @RBArgComment0("The Icon for the context master (or version) for the uses occurrences that follow")
   @RBArgComment1("An Identifier for the context master (or version) for the uses occurrences that follow")
   @RBArgComment2("The reference designator of the part")
   @RBArgComment3("The part itself")
   public static final String COMBINED_PATH_OCCURRENCE_CONTEXT = "87";

   @RBEntry("Indicatore di riferimento")
   @RBArgComment0("The name of the uses occurrence")
   public static final String REFERENCE_DESIGNATOR_LABEL = "88";

   @RBEntry("Indicatore di riferimento: {0}")
   @RBArgComment0("The name of the path occurrence")
   public static final String PATH_OCCURRENCE_NAME = "89";

   @RBEntry("Indicatore di riferimento: {0}")
   @RBArgComment0("The name of the combined path occurrence")
   public static final String COMBINED_PATH_OCCURRENCE_NAME = "90";

   @RBEntry("Senza nome")
   public static final String USES_OCCURRENCE_UNNAMED = "91";

   @RBEntry("Senza nome")
   public static final String PATH_OCCURRENCE_UNNAMED = "92";

   @RBEntry("Senza nome")
   public static final String COMBINED_PATH_OCCURRENCE_UNNAMED = "93";

   @RBEntry("Nel contesto di")
   public static final String OCCURRENCE_CONTEXT_LABEL = "94";

   @RBEntry("{0} casi d'impiego di {1}")
   @RBArgComment0(" The name of the occurrence")
   @RBArgComment1(" The name of the part")
   public static final String OCCURRENCE_HEADER = "95";

   @RBEntry("Impossibile ridurre l'utilizzo al di sotto del numero di casi d'impiego definiti")
   public static final String USAGE_QUANTITY_BELOW_MINIMUM = "96";

   @RBEntry("L'unità di misura per la quantità di utilizzo nei casi d'impiego definiti deve essere /\"ogni/\" (EA)")
   @RBArgComment0("The required usage quantity")
   public static final String USAGE_QUANTITY_UNIT_WRONG = "97";

   @RBEntry("Referenziato:")
   public static final String REFERENCE_DOCUMENT_LABEL = "98";

   @RBEntry("Descrivente:")
   public static final String DESCRIPTION_DOCUMENT_LABEL = "99";

   @RBEntry("I casi d'impiego sono ordinati in base al livello di specificità: dal contesto più generico a quello più specifico.")
   public static final String OCCURRENCES_ORDERED_GENERAL_TO_SPECIFIC = "100";

   @RBEntry("I casi d'impiego sono ordinati in base al livello di specificità: dal contesto più specifico a quello più generico.")
   public static final String OCCURRENCES_ORDERED_SPECIFIC_TO_GENERAL = "101";

   @RBEntry("Report collegati")
   public static final String STRUCTURE_REPORT_LABEL = "102";

   @RBEntry("Mostra casi d'impiego")
   public static final String SHOW_OCCURRENCES_LABEL = "103";

   @RBEntry("Nascondi casi d'impiego")
   public static final String HIDE_OCCURRENCES_LABEL = "104";

   @RBEntry("Mostra i documenti di descrizione")
   public static final String SHOW_DESCRIBING_DOCUMENTS_LABEL = "105";

   @RBEntry("Nascondi i documenti di descrizione")
   public static final String HIDE_DESCRIBING_DOCUMENTS_LABEL = "106";

   @RBEntry("Mostra i documenti referenziati")
   public static final String SHOW_REFERENCED_DOCUMENTS_LABEL = "107";

   @RBEntry("Nascondi i documenti referenziati")
   public static final String HIDE_REFERENCED_DOCUMENTS_LABEL = "108";

   @RBEntry("Espandi tutto")
   public static final String EXPAND_ALL_LABEL = "109";

   @RBEntry("{0} non può usare {1}.  Gli oggetti {2} non possono usare gli oggetti  {3}.")
   @RBArgComment0("Identity of the used by part")
   @RBArgComment1("Identity of the uses part")
   @RBArgComment2("Type of the used by part")
   @RBArgComment3("Type of the uses part")
   public static final String INVALID_PART_USAGE = "110";

   @RBEntry("Le parti con numero di serie non possono essere assegnate alle viste.")
   public static final String SERIAL_NUMBERED_PARTS_MUST_BE_VIEW_INDEPENDENT = "111";

   @RBEntry("Tutte le configurazioni")
   public static final String ALL_CONFIGURATIONS = "112";

   @RBEntry("Tutte le istanze di prodotto")
   public static final String ALL_INSTANCES = "113";

   @RBEntry("I numeri di riga non sono stati assegnati ad una o più parti usate dall'assieme {0}.")
   @RBArgComment0("Identity of the assembly")
   public static final String NO_LINENUMBER_ON_LINK = "114";

   @RBEntry("La parte {0} non è persistente.")
   @RBArgComment0("Identity of the part")
   public static final String PART_NOT_PERSISTENT = "115";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String SET_PLANNED_INCORPORATION = "116";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String SET_INCORPORATED_DATE = "117";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String INVALID_PLANNED_INCORPORATION = "118";

   @RBEntry("Impossibile aggiungere {0} alla configurazione perché non è un componente di nessuna parte nella configurazione o la parte è tracciata da utenti nella configurazione.")
   @RBArgComment0("Identity of the part")
   public static final String CAN_NOT_ADD_TO_CONFIGURATION = "119";

   @RBEntry("()")
   @RBPseudo(false)
   @RBComment("Identity for a product master when used in the identity of a configuration/product instance.")
   public static final String WTPRODUCTMASTER_NUMBERNAME_BOTH_NULL = "120";

   @RBEntry("({0})")
   @RBComment("Identity for a product master when used in the identity of a configuration/product instance.")
   @RBArgComment0("The product master's name.")
   public static final String WTPRODUCTMASTER_NUMBERNAME_NUMBER_NULL = "121";

   @RBEntry("({0})")
   @RBComment("Identity for a product master when used in the identity of a configuration/product instance.")
   @RBArgComment0("The product master's number.")
   public static final String WTPRODUCTMASTER_NUMBERNAME_NAME_NULL = "122";

   @RBEntry("({0} - {1})")
   @RBComment("Identity for a product master when used in the identity of a configuration/product instance.")
   @RBArgComment0("The product master's number.")
   @RBArgComment1("The product master's name.")
   public static final String WTPRODUCTMASTER_NUMBERNAME = "123";

   @RBEntry("{0}")
   @RBComment("Identity of a product configuration.")
   @RBArgComment0("The product configuration's name.")
   public static final String PRODUCT_CONFIGURATION_DISPLAY_IDENTIFIER = "124";

   @RBEntry("{0} {1}")
   @RBComment("Identity of a product instance master.")
   @RBArgComment0("The product instance's serial number.")
   @RBArgComment1("The product master's identity.")
   public static final String PRODUCT_INSTANCE_MASTER_DISPLAY_IDENTIFIER = "125";

   @RBEntry("{0} {2} {1}")
   @RBComment("Identity of a product instance.")
   @RBArgComment0("The product instance's serial number.")
   @RBArgComment1("The product master's identity.")
   @RBArgComment2("The product instance's version identity.")
   public static final String PRODUCT_INSTANCE2_DISPLAY_IDENTIFIER = "126";

   @RBEntry("Sono stati trovati {0} casi d'impiego del percorso ma se ne attendeva solo uno.")
   @RBArgComment0("A number.")
   public static final String INVALID_NUMBER_OF_PATH_OCCURRENCE_DATA_OBJECTS = "127";

   @RBEntry("Allocazione per")
   public static final String ALLOCATION_FOR = "128";

   @RBEntry("Allocazioni")
   public static final String ALLOCATES = "129";

   @RBEntry("Allocato da")
   public static final String ALLOCATED_BY = "130";

   @RBEntry("Proprietà di {0}")
   @RBComment("Page Title.")
   @RBArgComment0("The object's display identity.")
   public static final String PROPERTIES_OF = "137";

   @RBEntry("Defisci alternative")
   public static final String DEFINE_ALTERNATES_TITLE = "138";

   @RBEntry("Rimuovi")
   public static final String REMOVE = "139";

   @RBEntry("Nome")
   public static final String NAME = "140";

   @RBEntry("Numero")
   public static final String NUMBER = "141";

   @RBEntry("Impossibile trovare una versione della parte da assegnare all'istanza")
   public static final String COULD_NOT_FIND_SERIALNUMBEREDPART = "142";

   @RBEntry("Imposta la specifica di configurazione dell'istanza di prodotto")
   public static final String SET_PRODUCT_INSTANCE_CONFIG_SPEC = "143";

   @RBEntry("Specifica di configurazione dell'istanza di prodotto")
   public static final String PRODUCT_INSTANCE_CONFIG_SPEC = "144";

   @RBEntry("Usa data pianificata")
   public static final String USE_PLANNING_DATE = "145";

   @RBEntry("true")
   public static final String TRUE_STRING = "146";

   @RBEntry("false")
   public static final String FALSE_STRING = "147";

   @RBEntry("\nsetPartAttributes: \"{0}\" è un nome di team non valido. Il nome deve avere la forma 'Dominio.Team'")
   @RBComment("Method name \"setPartAttributes\" at beginning of this message should not be translated.  The rest of the message text can be translated.  This message is displayed only in the output from running the command line tool LoadFromFile.")
   public static final String LOAD_INVALID_TEAMTEMPLATE = "148";

   @RBEntry("Configurazioni")
   public static final String CONFIGURATIONS = "149";

   @RBEntry("Sostituzioni")
   public static final String REPLACEMENTS = "150";

   @RBEntry("La parte sostitutiva non può essere uguale alla parte sostituita.")
   @RBComment("Exception thrown when the replacement part is the same as the part it is supposed to replace.")
   public static final String REPLACEMENT_REFLEXIVE = "152";

   @RBEntry("Aggiunta di alternative di {0} - Risultati della ricerca")
   @RBComment("Title on the search results page used when adding part alternates (PDMLink)")
   public static final String ALTS_SEARCH_RESULTS_TITLE_2 = "153";

   @RBEntry("Aggiunta di sostituzioni per la parte {0} dell'assieme {1} - Risultati della ricerca")
   @RBComment("Title on the search results page used when adding part substitutes (PDMLink)")
   public static final String SUBS_SEARCH_RESULTS_TITLE_2 = "154";

   @RBEntry("Data incorporazione non valida")
   public static final String INVALID_INCORPORATED = "155";

   @RBEntry("Il documento \"{0}\" deve trovarsi nello stato rilasciato per poter essere associato alla parte.")
   @RBComment("Documents of type Reference Document must be released before the PartDoc service will allow users to create reference associations to them.")
   @RBArgComment0("The Reference Document that is not in a released state.")
   public static final String REFDOC_NOT_RELEASED = "156";

   @RBEntry("'Espandi tutto' non è stato completato perché il limite {0} di {1} nodi è stato superato. È possibile espandere manualmente qualsiasi nodo non espanso o usare il report della distinta base per visualizzare l'intera struttura.")
   @RBComment("There is a limit to how many rows an Expand All action will show in the Product Structure page--this message is used when that limit is exceeded")
   @RBArgComment0("Name of the properties file entry that specifies the limit.")
   @RBArgComment1("The value of the limit.")
   public static final String ABORT_EXPAND_ALL = "157";

   @RBEntry("{0} ({1}) - {2} {3}")
   @RBComment("Format for displaying part information with orgId.")
   @RBArgComment0("part number")
   @RBArgComment1("part orgId")
   @RBArgComment2("part name")
   @RBArgComment3("part version")
   public static final String PART_DISPLAY_FORMAT_WITH_ORGID = "162";

   @RBEntry("{0} - {1} {2}")
   @RBComment("Format for displaying part information withot orgId.")
   @RBArgComment0("part number")
   @RBArgComment1("part name")
   @RBArgComment2("part version")
   public static final String PART_DISPLAY_FORMAT_NO_ORGID = "163";

   @RBEntry("Configurazione parte di origine")
   @RBComment("Lable for the Source Configuration when displaying Multilevel BOM Compare Report")
   public static final String SOURCE_CONFIGURATION_LABEL = "164";

   @RBEntry("Istanza parte di origine")
   @RBComment("Lable for the Source Instance when displaying Multilevel BOM Compare Report")
   public static final String SOURCE_INSTANCE_LABEL = "165";

   @RBEntry("Configurazione parte di destinazione")
   @RBComment("Lable for the Target Configuration when displaying Multilevel BOM Compare Report")
   public static final String TARGET_CONFIGURATION_LABEL = "166";

   @RBEntry("Istanza parte di destinazione")
   @RBComment("Lable for the Target Instance when displaying Multilevel BOM Compare Report")
   public static final String TARGET_INSTANCE_LABEL = "167";

   @RBEntry("Report esteso di distinta base")
   @RBComment("Label for Extended BOM Report drop down item")
   public static final String EXTENDED_BOM_REPORT_LABEL = "168";

   @RBEntry("Elenco parti esteso")
   @RBComment("Label for Extended Part List report drop down item")
   public static final String EXTENDED_PART_LIST_LABEL = "169";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String NEED_BASELINE_FOR_CONFIG_SPEC = "171";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String NEED_EFFECTIVITY_CONTEXT_FOR_CONFIG_SPEC = "172";

   @RBEntry("Struttura Dove usato")
   @RBComment("Column header for the Multilevel Where Used Report for column that shows the structure")
   public static final String WHERE_USED_STRUCTURE_HEADER = "173";

   /**
    * Column Headers for report.  Most are accessed by key number not constant.
    **/
   @RBEntry("Istanza parte:")
   @RBComment("Used in Multilevel Where Used Report when its a product instance and not a part being reported on")
   public static final String PRODUCT_INSTANCE_LABEL = "174";

   @RBEntry("Data di incorporazione stimata")
   @RBComment("Used in Multilevel Where Used Report for a part instance")
   public static final String PLANNED_INCORPORATION_DATE_HEADER = "175";

   @RBEntry("Data iniziale di incorporazione")
   @RBComment("Used in Multilevel Where Used Report for a part instance")
   public static final String INCORPORATED_DATE_HEADER = "176";

   @RBEntry("Data finale di incorporazione")
   @RBComment("Used in Multilevel Where Used Report for a part instance")
   public static final String UNINCORPORATED_DATE_HEADER = "177";

   /**
    * Messages for PartRepresentation-------------------------------------------
    *
    **/
   @RBEntry("\ncreatePartRepresentation: la parte \"{0}\" non esiste nel file di caricamento oppure non è stata creata.")
   @RBArgComment0("part number")
   @RBComment("Method name \"createPartRepresentation\" at beginning of this message should not be translated.  The rest of the message text can be translated. This message is displayed only in the output from running the command line tool LoadFromFile.")
   public static final String LOAD_NO_PART_REPRESENTATION = "180";

   @RBEntry("La rappresentazione \"{0}\" è stata aggiunta alla parte \"{1}\"")
   @RBArgComment0("Representation name")
   @RBArgComment1("part number")
   public static final String LOAD_REPRESENTATION_ADDED = "181";

   @RBEntry("Errore durante l'aggiunta della rappresentazione \"{0}\" alla parte \"{1}\"")
   @RBArgComment0("Representation name")
   @RBArgComment1("part number")
   public static final String LOAD_REPRESENTATION_FAILED = "182";

   /**
    * Messages for PartRepresentation-------------------------------------------
    *
    **/
   @RBEntry("\nremovePartFromAssembly: la parte \"{0}\" non esiste nel file di caricamento oppure non è stata creata.")
   @RBArgComment0("part number")
   @RBComment("Method name \"removePartFromAssembly\" at beginning of this message should not be translated.  The rest of the message text can be translated. This message is displayed only in the output from running the command line tool LoadFromFile.")
   public static final String REMOVE_NO_PART = "185";

   /**
    * Messages for PartRepresentation-------------------------------------------
    *
    **/
   @RBEntry("\ncreateNewViewVersion: la parte \"{0}\" non esiste nel file di caricamento oppure non è stata creata.")
   @RBArgComment0("part number")
   @RBComment("Method name \"createNewViewVersion\" at beginning of this message should not be translated.  The rest of the message text can be translated. This message is displayed only in the output from running the command line tool LoadFromFile.")
   public static final String NEWVIEWVERSION_NO_PART = "190";

   @RBEntry("\ncreateNewViewVersion: impossibile individuare la vista \"{0}\"")
   @RBArgComment0("Named View")
   @RBComment("Method name \"createNewViewVersion\" at beginning of this message should not be translated.  The rest of the message text can be translated. This message is displayed only in the output from running the command line tool LoadFromFile.")
   public static final String NEWVIEWVERSION_NO_VIEW = "191";

   @RBEntry("\ncreateNewViewVersion: impossibile creare una nuova versione della vista per \"{0}\"")
   @RBArgComment0("Part Number")
   @RBComment("Method name \"createNewViewVersion\" at beginning of this message should not be translated.  The rest of the message text can be translated. This message is displayed only in the output from running the command line tool LoadFromFile.")
   public static final String NEWVIEWVERSION_FAILED = "192";

   /**
    * Labels for End Item Name for Products in PDMLink-------------------------------------------
    *
    **/
   @RBEntry("Prodotto finale")
   @RBComment("Label for property for WTPart in PDMLink")
   public static final String PDMLINK_END_ITEM_PROPERTY = "193";

   @RBEntry("Prodotto finale:")
   @RBComment("Label for WTPart in PDMLink")
   public static final String PDMLINK_END_ITEM_LABEL = "194";

   @RBEntry("Configurazione parte")
   @RBComment("Label for property of WTProductConfiguration in PDMLink")
   public static final String PDMLINK_END_ITEM_CONFIGURATION_PROPERTY = "195";

   @RBEntry("Configurazione parte:")
   @RBComment("Label for WTProductConfiguration in PDMLink")
   public static final String PDMLINK_END_ITEM_CONFIGURATION_LABEL = "196";

   @RBEntry("Istanza parte")
   @RBComment("Label for property of WTProductInstance2 in PDMLink")
   public static final String PDMLINK_END_ITEM_INSTANCE_PROPERTY = "197";

   @RBEntry("Istanza parte:")
   @RBComment("Label for WTProductInstance2 in PDMLink")
   public static final String PDMLINK_END_ITEM_INSTANCE_LABEL = "198";

   /**
    * Messages for assignUserToProduct in LoadPart
    **/
   @RBEntry("Impossibile individuare l'utente \"{0}\"")
   public static final String FAILED_LOCATE_USER = "201";

   @RBEntry("Impossibile individuare il prodotto \"{0}\"")
   public static final String FAILED_LOCATE_PRODUCT = "202";

   @RBEntry("Impossibile individuare il ruolo \"{0}\"")
   public static final String FAILED_LOCATE_ROLE = "203";

   @RBEntry("La specifica di configurazione non indica alcuna versione dell'istanza parte corrente.")
   public static final String CONFIG_SPEC_SELECTS_NO_INSTANCE_VERSION = "204";

   /**
    * Message for LoadPart.createProductCotainer
    **/
   @RBEntry("Impossibile trovare il modello di contenitore \"{0}\".")
   public static final String CONTAINER_TEMPLATE_NOT_FOUND = "205";

   @RBEntry("Si sono verificati i seguenti errori durante il tentativo di aggiunta parti a {0}:")
   public static final String MULTI_OBJECT_VALIDATE_ADD_TO_CONFIGURATION = "206";

   /**
    * Message for trying to store a part with proi isntalled
    **/
   @RBEntry("La creazione di WTParts non è consentita se ProIntralink è installato")
   public static final String CANNOT_CREATE_WTPART_WITH_PROI_INSTALLED = "207";

   /**
    * Message for LoadPart.createProductEffectivity
    **/
   @RBEntry("Impossibile trovare l'oggetto {0} con il numero {1}.")
   public static final String OBJECT_NOT_FOUND = "208";

   @RBEntry("Sono stati trovati più  oggetti {0} con il numero {1}.")
   public static final String MULTIPLE_OBJECTS_FOUND = "209";

   @RBEntry("Proprietà di {0} {1}")
   @RBComment("Page Title.")
   @RBArgComment0("The class name.")
   @RBArgComment1("The class value.")
   public static final String PROPERTIES_OF_2 = "210";

   @RBEntry("Dettagli per {0}")
   @RBComment("Page Title.")
   @RBArgComment0("The object's display identity.")
   public static final String DETAILS_FOR = "211";

   @RBEntry("Il valore '{0}' per l'indicatore di riferimento non è valido. I caratteri '{1}' e '{2}' sono riservati e non possono fare parte di un indicatore di riferimento a meno che questo non inizi con '{3}' e finisca con '{4}'.")
   @RBComment("Error message in exception when reference designator contains invalid range or sequence delimiter")
   @RBArgComment0("The reference designator set being parsed")
   @RBArgComment1("The current range delimiter")
   @RBArgComment2("The current sequence delimiter")
   @RBArgComment3("The current begin escape delimiter")
   @RBArgComment4("The current end escape delimiter")
   public static final String REFERENCE_DESIGNATOR_DELIMITER_PARSE_ERROR = "212";

   @RBEntry("La vista specificata non è valida per l'iterazione di parte corrente.")
   public static final String VIEW_NOT_ASSIGNABLE = "213";

   @RBEntry("Uno o più componenti nella lista non esistono.")
   public static final String USES_RELATION_NOT_EXISTS = "214";

   @RBEntry("Impossibile allocare {0}.  Non è un'istanza del figlio o una parte alternativa o di sostituzione del figlio.")
   @RBArgComment0("The instance's display identity")
   public static final String INVALID_CHILD_FOR_ALLOCATION = "215";

   @RBEntry("{0} non può essere specificato come sostituzione. Non è una versione del figlio né ne è un sostituto valido.")
   @RBArgComment0("The part version's display identity")
   public static final String INVALID_CHILD_FOR_OVERRIDE = "216";

   @RBEntry("Casi d'impiego di {0}")
   @RBArgComment0(" The name of the part")
   public static final String OCCURRENCES_HEADER = "217";

   @RBEntry("Impossibile aggiungere {0} alla struttura come figlio di se stesso.")
   @RBArgComment0(" The name of the part")
   public static final String CAN_NOT_ADD_CHILD_TO_SELF = "218";

   @RBEntry("L'indice bolla {0} non è univoco. Assegnare un indice bolla univoco.")
   @RBArgComment0(" Find Number")
   public static final String FIND_NUMBER_NOT_UNIQUE = "219";

   @RBEntry("Ricerca istanze parte...")
   public static final String SEARCH_INSTANCES_BUTTON_LABEL = "220";

   @RBEntry("Ricerca configurazioni parte...")
   public static final String SEARCH_CONFIGURATIONS_BUTTON_LABEL = "221";

   @RBEntry("Configurazione:")
   public static final String MULTI_LEVEL_CONFIGURATION_LABEL = "222";

   @RBEntry("Risultati confronto istanze parte")
   public static final String MULTI_LEVEL_INSTANCE_RESULT_TITLE = "223";

   @RBEntry("Risultati confronto configurazioni parte")
   public static final String MULTI_LEVEL_PART_CONFIG_RESULT_TITLE = "224";

   @RBEntry("Risultati confronto di distinte base multilivello")
   public static final String MULTILEVEL_COMPARE = "225";

   @RBEntry("Interrogazione confronto istanze parte")
   public static final String PART_INSTANCE_COMPARE_TITLE = "226";

   @RBEntry("Interrogazione confronto configurazioni parte")
   public static final String PART_CONFIGURATION_COMPARE_TITLE = "227";

   @RBEntry("Parte")
   public static final String FIND_JCA_PART_LABEL = "228";

   @RBEntry("La ricerca delle istanze parte restituisce solo quelle che dispongono di una configurazione di parte")
   @RBComment("Message to explain filtering on the search results")
   public static final String PART_INSTANCE_COMPARE_SEARCH_MSG = "229";

   /**
    * Message for trying to create a part without PDMLink or ProjectLink isntalled
    **/
   @RBEntry("Creazione di parti non consentita se PDMLink o ProjectLink non sono installati.")
   public static final String CANNOT_CREATE_WTPART_WITHOUT_PDMLINK_AND_PROJECTLINK_INSTALLED = "230";

   @RBEntry("La modifica del codice traccia di una parte che è il contesto di un'effettività in sospeso per una modifica non è consentita.")
   @RBComment("Error message when user tries to change the trace code of a part that is currently the context of a pending effectivity for a change.")
   public static final String PART_MASTER_TRACE_CODE_CHANGE_ERROR_MESSAGE = "231";

   @RBEntry("Impossibile modificare l'attributo prodotto finale per una parte referenziata da una richiesta di modifica come prodotto finale interessato oppure che è il prodotto finale principale di un prodotto.")
   @RBComment("Error message when user tries to uncheck end item flag for primary end items or for a part that is referenced by a Change Request as an affected end item.")
   public static final String PART_MASTER_END_ITEM_CHANGE_ERROR_MESSAGE = "232";

   @RBEntry("Prodotto finale")
   public static final String END_ITEM_TOOLTIP = "233";

   @RBEntry("L'ndice bolla può contenere solo caratteri alfanumerici. Modificare {0}.")
   public static final String FIND_NUMBER_NOT_ALPHANUMERICAL_ERROR = "234";

   @RBEntry("Le parti nel progetto non possono disporre di alternative.")
   public static final String ALTS_NOT_ALLOWED_IN_PROJECT = "235";

   @RBEntry("Creazione link alternativa o di sostituzione non valida. Impossibile creare un link fra {0} e {1} in quanto uno degli oggetti risiede in un progetto.")
   @RBArgComment0("RoleA object.")
   @RBArgComment1("RoleB object.")
   public static final String CANT_CREATE_ACROSS_CONT = "236";

   @RBEntry("Impossibile creare un'associazione di componenti per il padre {0} e il figlio {1}.\nL'associazione di componenti non è valida fra padri di tipo {2} e figli di tipo {3}.")
   @RBComment("Message for validationg of pre_store event.")
   public static final String INVALID_ROLE_B_TYPE = "237";

   @RBEntry("Variante modulo")
   @RBComment("Tool tip used to display on icons")
   public static final String VARIANT_TOOLTIP = "238";

   @RBEntry("Modulo configurabile")
   @RBComment("Tool tip used to display on icons")
   public static final String CONFIGURABLE_PART = "240";

   @RBEntry("Prodotto finale configurabile avanzato")
   @RBComment("Tool tip used to display on icons")
   public static final String ADVANCED_CONFIGURABLE_ENDITEM = "241";

   @RBEntry("Parte configurabile avanzata")
   @RBComment("Tool tip used to display on icons")
   public static final String ADVANCED_CONFIGURABLE = "242";

   @RBEntry("Prodotto configurabile")
   @RBComment("Tool tip used to display on icons")
   public static final String CONFIGURABLE_ENDITEM = "243";

   @RBEntry("Il tipo di effettività {0} non è valido per il contesto di effettività {1}.")
   @RBComment("Message for validating effectivity type for loading effectivity.")
   public static final String EFF_TYPE_ERROR = "244";

   /**
    * Display Identification for Part Uses Occurrence (aka Reference Designator)
    **/
   @RBEntry("Indicatore di riferimento")
   @RBComment("display type of part uses occurrence")
   public static final String PART_USES_OCCURRENCE_DISPLAY_TYPE = "245";

   @RBEntry("Componenti parte: {0}, {1}: {2}")
   @RBComment("display identifier of reference designator")
   public static final String PART_USES_OCCURRENCE_DISPLAY_IDENTIFIER = "246";

   @RBEntry("Componenti: {0}, {1}: {2}")
   @RBComment("display identity of reference designator")
   public static final String PART_USES_OCCURRENCE_DISPLAY_IDENTITY = "247";
   
   @RBComment("The display identifier for a WTPartUsageLink.  This is displayed when an attempt is made to save a new child part to a structure that violates uniqueness constraints")
   @RBArgComment0("The identity of the parent part")
   @RBArgComment1("The identity of the child part")
   @RBArgComment2("The line number assigned to the child in the part structure")
   @RBEntry("Padre {0} di figlio {1} con numero di riga {2}")
   public static final String PART_USAGE_DISPLAY_IDENTIFIER = "246.1";

   @RBEntry("Specifica di configurazione")
   public static final String MULTI_BOM_COMPARE_CONFIG_LABEL = "248";

   @RBEntry("Filtro opzioni")
   public static final String MULTI_BOM_COMPARE_FILTER_LABEL = "249";

   @RBEntry("Nessuna")
   public static final String NONE = "250";

   @RBEntry("Criteri di espansione")
   @RBComment("Label preceding the expansion criteria in HTML")
   public static final String EXPANSION_CRITERIA_LABEL = "251";

   @RBEntry("Operazione in corso")
   @RBComment("Working label")
   public static final String WORKING_LABEL = "252";

   @RBEntry("Applicata a principale")
   @RBComment("Applied to Top label")
   public static final String APPLIED_TO_TOP_LABEL = "253";

   @RBEntry("Usa default")
   @RBComment("Use Default label")
   public static final String USE_DEFAULT_LABEL = "254";

   @RBEntry("Contesto")
   @RBComment("Context label")
   public static final String CONTEXT_LABEL = "255";

   @RBEntry("Numero parte base")
   @RBComment("This label indicates the base part number  for a part configuration")
   public static final String BASE_PART_NUMBER_LABEL = "256";

   @RBEntry("Nome parte base")
   @RBComment("This label indicates the base part name for a part configuration")
   public static final String BASE_PART_NAME_LABEL = "257";

   @RBEntry("Versione parte base")
   @RBComment("This label indicates the base part version  for a part configuration")
   public static final String BASE_PART_VERSION_LABEL = "258";

   @RBEntry("Numero di lotto")
   @RBComment("This label indicates the Lot Number for a part instance")
   public static final String LOT_NUMBER_LABEL = "259";

   @RBEntry("Numero di serie")
   @RBComment("This label indicates the Serial Number for a part instance")
   public static final String SERIAL_NUMBER_LABEL = "260";

   @RBEntry("Numero di serie/lotto")
   @RBComment("This label indicates both Lot Number and Serial Number for a part instance")
   public static final String LOT_SERIAL_LABEL = "261";

   @RBEntry("Nome configurazione")
   @RBComment("This label indicates the Serial Number for a part instance and part configuration")
   public static final String CONFIGURATION_NAME_LABEL = "262";

   @RBEntry("Il valore '{0}' per l'indicatore di riferimento non è valido. Le parti numeriche sostituite da zeri in un intervallo di indicatori di riferimento devono avere la stessa lunghezza.")
   @RBComment("Error message in exception when reference designator contains a range with zero-padded numeric portions of different lengths.")
   @RBArgComment0("The reference designator set being parsed")
   public static final String REFERENCE_DESIGNATOR_DELIMITER_ZERO_PADDING_PARSE_ERROR = "263";

   @RBEntry("Elemento fittizio o phantom")
   @RBComment("The label for Phantom Assemblies")
   public static final String PHANTOM_ASSMEBLY_LABEL = "264";

   @RBEntry("Non è possibile che sussistano più link di utilizzo per la stessa parte figlio con casi d'impiego completati o da creare.")
   @RBComment("Error message in exception when an occurrence is added/modified which results in the cadSynched status being changed to Yes or Pending.")
   public static final String BUILT_USAGELINK_CHANGE_PIB_ERROR = "265";

   @RBEntry("Parte di raccolta")
   @RBComment("The tooltip for Gathering Parts")
   public static final String CAD_PHANTOM_TOOLTIP = "266";

   @RBEntry("Impossibile modificare il caso d'impiego sincronizzato CAD '{0}'.")
   @RBComment("Error message when a built part uses occurrence is modified.")
   @RBArgComment0("reference designator of the part uses occurrence.")
   public static final String CANNOT_MODIFY_BUILT_OCCURRENCE = "267";

   @RBEntry("Impossibile impostare lo stato di creazione completata per il caso d'impiego. L'impostazione è possibile solo tramite le azioni Crea struttura documenti CAD o Crea struttura parti.")
   @RBComment("Error message when attempting to set the Completed value on build status of a uses occurrence.")
   public static final String CANNOT_SET_BUILT_STATUS_ON_OCCURRENCE = "268";

   @RBEntry("Variante prodotto")
   @RBComment("Tool tip for a Part with Configurable Module=Variant and End Item=Yes")
   public static final String VARIANT_ENDITEM_TOOLTIP = "269";

   @RBEntry("Impossibile aggiungere {0} con quantità 0 e unità {1} alla struttura. La quantità deve essere diversa da zero.")
   @RBArgComment0("The name of the part")
   @RBArgComment1("The name of the unit")
   public static final String CANNOT_ADD_QTY_ZERO = "270";

   @RBEntry("Creazione di parti non consentita se PdmLink, ProjectLink o QualityLink non sono installati")
   @RBComment("Error message when attempting to create part without PDM Link,Project Link or Quality Link installed.")
   public static final String CANNOT_CREATE_WTPART_WITHOUT_PDMLINK_AND_PROJECTLINK_AND_QMS_INSTALLED = "271";

   @RBEntry("Impossibile creare un caso d'impiego per un link con un'unità quantitativa diversa da {0}, che ha più di un caso d'impiego")
   @RBArgComment0("QuantityUnit.EA display value")
   public static final String CANNOT_CREATE_OCCURRENCE_WHERE_EA_AND_MORE_THAN_ZERO = "272";

   @RBEntry("Impossibile completare l'azione. Il sistema sta cercando di creare un caso d'impiego di percorso duplicato. Segnalare l'errore all'amministratore di sistema.")
   @RBArgComment0("Duplicate path occurrences error message")
   public static final String DUPLICATE_PATH_OCCURRENCES_NOT_ALLOWED = "273";

   @RBEntry("L'indicatore di riferimento non è univoco\r\rIl valore dell'indicatore di riferimento ({0}) deve essere univoco per il sottoassieme.")
   @RBComment("Error message when the reference designator entered by the user for an occurrence is not unique within a sub-assembly")
   public static final String REF_DESIG_NOT_UNIQUE = "REF_DESIG_NOT_UNIQUE";
}
