ResourceInfo.class=wt.tools.resource.MetadataResourceInfo
ResourceInfo.customizable=true
ResourceInfo.deprecated=false

# Collapsible attribute for a WTPartMaster
type[wt.part.WTPartMaster].attr[collapsible].prop[description].value=Collapsible
type[wt.part.WTPartMaster].attr[collapsible].prop[tooltip].value=Collapsible

# Context attribute for a WTPartMaster
type[wt.part.WTPartMaster].attr[containerReference].prop[description].value=Context
type[wt.part.WTPartMaster].attr[containerReference].prop[tooltip].value=Context

# Default Trace Code attribute for a WTPartMaster
type[wt.part.WTPartMaster].attr[defaultTraceCode].prop[description].value=Default Trace Code
type[wt.part.WTPartMaster].attr[defaultTraceCode].prop[tooltip].value=Default Trace Code

# Default Unit attribute for a WTPartMaster
type[wt.part.WTPartMaster].attr[defaultUnit].prop[description].value=Default Unit
type[wt.part.WTPartMaster].attr[defaultUnit].prop[tooltip].value=Default Unit

# effCalculationStatus attribute for a WTPartMaster
type[wt.part.WTPartMaster].attr[effCalculationStatus].prop[description].value=Calculation Status
type[wt.part.WTPartMaster].attr[effCalculationStatus].prop[tooltip].value=Calculation Status

# Stop Effectivity Propagation attribute for a WTPartMaster
type[wt.part.WTPartMaster].attr[effPropagationStop].prop[description].value=Stop Effectivity Propagation
type[wt.part.WTPartMaster].attr[effPropagationStop].prop[tooltip].value=Stop Effectivity Propagation

# End Item attribute for a WTPartMaster
type[wt.part.WTPartMaster].attr[endItem].prop[description].value=End Item
type[wt.part.WTPartMaster].attr[endItem].prop[tooltip].value=End Item

# Configurable attribute for a WTPartMaster
type[wt.part.WTPartMaster].attr[genericType].prop[description].value=Configurable Module
type[wt.part.WTPartMaster].attr[genericType].prop[tooltip].value=Configurable Module

# Gathering Part attribute for a WTPartMaster
type[wt.part.WTPartMaster].attr[hidePartInStructure].prop[description].value=Gathering Part
type[wt.part.WTPartMaster].attr[hidePartInStructure].prop[tooltip].value=Gathering Part

# Name attribute for a WTPartMaster
type[wt.part.WTPartMaster].attr[name].prop[description].value=Name
type[wt.part.WTPartMaster].attr[name].prop[tooltip].value=Name

# Number attribute for a WTPartMaster
type[wt.part.WTPartMaster].attr[number].prop[description].value=Number
type[wt.part.WTPartMaster].attr[number].prop[tooltip].value=Number

# Organization Name attribute for a WTPartMaster
type[wt.part.WTPartMaster].attr[organizationReference].prop[description].value=Organization Name
type[wt.part.WTPartMaster].attr[organizationReference].prop[displayName].value=Organization Name
type[wt.part.WTPartMaster].attr[organizationReference].prop[tooltip].value=Organization Name

# Organization attribute for a WTPartMaster
type[wt.part.WTPartMaster].attr[orgid].prop[description].value=Organization
type[wt.part.WTPartMaster].attr[orgid].prop[displayName].value=Organization
type[wt.part.WTPartMaster].attr[orgid].prop[tooltip].value=Organization

# Phantom attribute for a WTPartMaster
type[wt.part.WTPartMaster].attr[phantom].prop[description].value=Phantom
type[wt.part.WTPartMaster].attr[phantom].prop[tooltip].value=Phantom

# Series attribute for a WTPartMaster
type[wt.part.WTPartMaster].attr[series].prop[description].value=Series
type[wt.part.WTPartMaster].attr[series].prop[tooltip].value=Series

# Created On attribute for a WTPartMaster, when this WTPartMaster was created
type[wt.part.WTPartMaster].attr[thePersistInfo.createStamp].prop[description].value=Created On
type[wt.part.WTPartMaster].attr[thePersistInfo.createStamp].prop[tooltip].value=Created On

# Last Modified attribute for a WTPartMaster, when this WTPartMaster was last modified
type[wt.part.WTPartMaster].attr[thePersistInfo.modifyStamp].prop[description].value=Last Modified
type[wt.part.WTPartMaster].attr[thePersistInfo.modifyStamp].prop[tooltip].value=Last Modified

# Serviceable attribute for a WTPartMaster
type[wt.part.WTPartMaster].attr[serviceable].prop[description].value=Serviceable
type[wt.part.WTPartMaster].attr[serviceable].prop[displayName].value=Serviceable
type[wt.part.WTPartMaster].attr[serviceable].prop[tooltip].value=Serviceable

# Service Kit attribute for a WTPartMaster
type[wt.part.WTPartMaster].attr[servicekit].prop[description].value=Service Kit
type[wt.part.WTPartMaster].attr[servicekit].prop[displayName].value=Service Kit
type[wt.part.WTPartMaster].attr[servicekit].prop[tooltip].value=Service Kit

# Attributes group for a WTPartMaster displayed in the Compare to Part Structure UI
type[wt.part.WTPartMaster].layout[-PART_COMPARE_ATTRIBUTES].group[dsbInfoScreenGroup1].prop[description].value=Part Master Attributes
type[wt.part.WTPartMaster].layout[-PART_COMPARE_ATTRIBUTES].group[dsbInfoScreenGroup1].prop[displayName].value=Part Master Attributes

# Attributes group for a WTPartMaster displayed in the info page for a WTPartMaster
type[wt.part.WTPartMaster].layout[-INFO].group[Attributes].prop[description].value=Attributes
type[wt.part.WTPartMaster].layout[-INFO].group[Attributes].prop[displayName].value=Attributes

# Attributes group for a WTPartMaster displayed in the info page for a WTPartMaster
type[wt.part.WTPartMaster].layout[-PSB_INFO].group[psbInfoScreenGroup1].prop[description].value=Part Master Attributes
type[wt.part.WTPartMaster].layout[-PSB_INFO].group[psbInfoScreenGroup1].prop[displayName].value=Part Master Attributes

# Usage Attributes group for a WTPartMaster displayed in the info page for a WTPartMaster
type[wt.part.WTPartMaster].layout[-PSB_INFO].group[psbInfoScreenGroup2].prop[description].value=Usage Attributes
type[wt.part.WTPartMaster].layout[-PSB_INFO].group[psbInfoScreenGroup2].prop[displayName].value=Usage Attributes

# Attributes group for a WTPartMaster displayed in the replacement info page
type[wt.part.WTPartMaster].layout[-PSB_REP_INFO].group[psbRepInfoScreenGroup1].prop[description].value=Part Master Attributes
type[wt.part.WTPartMaster].layout[-PSB_REP_INFO].group[psbRepInfoScreenGroup1].prop[displayName].value=Part Master Attributes

# The display name for a WTPartMaster
type[wt.part.WTPartMaster].prop[description].value=Part Master
type[wt.part.WTPartMaster].prop[displayName].value=Part Master
type[wt.part.WTPartMaster].prop[tooltip].value=Part Master
