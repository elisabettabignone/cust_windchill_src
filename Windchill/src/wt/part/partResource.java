/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.part;

import wt.util.resource.*;

@RBUUID("wt.part.partResource")
public final class partResource extends WTListResourceBundle {
   @RBEntry("The value for the argument \"{0}\" can not be null")
   public static final String NULL_ARGUMENT = "0";

   @RBEntry("The view specified is not valid:  it must be persistent")
   public static final String VIEW_NOT_PERSISTENT = "1";

   @RBEntry("More than one wt.part.WTPartConfigSpec returned, when only one was expected")
   public static final String QUERY_WTPARTCONFIGSPEC_QTY = "2";

   @RBEntry("The number attribute must have a value")
   public static final String NULL_NUMBER = "3";

   @RBEntry("The name attribute must have a value")
   public static final String NULL_NAME = "19";

   @RBEntry("Line number assignments for Assembly \"{0}\" are inconsistent.  Either line numbers must be assigned to all the parts that are used by the assembly or to none of them.")
   public static final String INCONSISTENT_LINENUMBER_USAGE = "170";

   /**
    * Messages for LoadPart -----------------------------------------------------
    *
    **/
   @RBEntry("\naddPartToAssembly: Part \"{0}\" doesn't exist in the load file or it was not created successfully.")
   @RBArgComment0("part number")
   @RBComment("Method name \"addPartToAssembly\" at beginning of this message should not be translated.  The rest of the message text can be translated. This message is displayed only in the output from running the command line tool LoadFromFile.")
   public static final String LOAD_NO_PART = "4";

   @RBEntry("Added \"{0}\" to \"{1}\"")
   public static final String LOAD_PARTS_ADDED = "5";

   @RBEntry("Referenced document \"{0}\" to part \"{1}\"")
   public static final String LOAD_DOC_ADDED = "6";

   @RBEntry("\ncreatePartDocReference: Document \"{0}\" doesn't exist.")
   @RBComment("Method name \"createPartDocReference\" at beginning of this message should not be translated.  The rest of the message text can be translated.  This message is displayed only in the output from running the command line tool LoadFromFile.")
   public static final String LOAD_NO_DOC = "7";

   @RBEntry("\ncreatePartDocReference: No current part.  Either an assembly or constituent part must be successfully created first.")
   @RBComment("Method name \"createPartDocReference\" at beginning of this message should not be translated.  The rest of the message text can be translated.  This message is displayed only in the output from running the command line tool LoadFromFile.")
   public static final String LOAD_NO_DOC_PART = "8";

   @RBEntry("\nsetPartAttributes: \"{0}\" is an invalid team name. Must be of the form 'Domain.Team'")
   @RBComment("Method name \"setPartAttributes\" at beginning of this message should not be translated.  The rest of the message text can be translated.  This message is displayed only in the output from running the command line tool LoadFromFile.")
   public static final String LOAD_INVALID_PROJECT = "9";

   @RBEntry("\"{0}\" is an invalid field name.  Check csvmapfile.txt for correct name.")
   @RBComment("File name \"csvmapfile.txt\" at beginning of this message should not be translated.  The rest of the message text can be translated.  This message is displayed only in the output from running the command line tool LoadFromFile.")
   public static final String LOAD_INVALID_NAME = "10";

   @RBEntry("A value for the required field \"{0}\" was not found in the input file.")
   public static final String LOAD_REQUIRED_FIELD = "11";

   /**
    * Messages for HTML Template Processing --------------------------------------
    *
    **/
   @RBEntry("Invalid CGI parameters. Parameter \"{0}\" not found in query string \"{1}\"")
   public static final String INVALID_PARAMETERS = "12";

   @RBEntry(" {0}{1} {2} {3} of {4}")
   @RBComment("For example, \"100: 16 each of Bolt - 1234\" or \"200: 5 gallons of Paint - 333\"")
   @RBArgComment0("is a line number")
   @RBArgComment1("is a separator")
   @RBArgComment2("is a quantity")
   @RBArgComment3("is a unit")
   @RBArgComment4("is and identity")
   public static final String PART_USAGE = "13";

   @RBEntry("Unable to fill in the attributes for the configuration item in the object.")
   public static final String INFLATE_FAILED = "14";

   /**
    * Messages for PartEffectivityConfigSpec -------------------------------------
    *
    **/
   @RBEntry("Unable to append search criteria to select parts based on effectivity information.  PartEffectivityConfigSpec not set up properly.")
   public static final String INVALID_PART_EFF_CONFIG_SPEC = "15";

   /**
    * Messages for BaselineEffectivityConfigSpec ---------------------------------
    *
    **/
   @RBEntry("Unable to append search criteria to select parts based on baseline information.  The referenced baseline may have been deleted.")
   public static final String INVALID_PART_BASELINE_CONFIG_SPEC = "16";

   /**
    * Messages for MultilevelBomCompare ------------------------------------------
    *
    **/
   @RBEntry("Multi-Level Bill of Materials Compare Query")
   public static final String MULTI_BOM_QUERY_TITLE = "40";

   @RBEntry("Source")
   public static final String SOURCE_HEADER = "41";

   @RBEntry("Target")
   public static final String TARGET_HEADER = "42";

   @RBEntry("Part:")
   public static final String PART_LABEL = "43";

   @RBEntry("Config Spec:")
   public static final String CONFIG_SPEC_LABEL = "44";

   @RBEntry("Multilevel BOM Compare")
   public static final String MULTI_BOM_TITLE = "45";

   @RBEntry("Source Part")
   public static final String SOURCE_PART_LABEL = "46";

   @RBEntry("Target Part")
   public static final String TARGET_PART_LABEL = "47";

   @RBEntry("Reset Target")
   public static final String RESET_LINK_LABEL = "48";

   @RBEntry("Level")
   public static final String LEVEL_HEADER = "49";

   @RBEntry("Product Structure Differences")
   public static final String PRODUCT_STRUCTURE_HEADER = "50";

   @RBEntry("Version Used")
   public static final String VERSION_HEADER = "51";

   @RBEntry("Quantity")
   public static final String QUANTITY_HEADER = "52";

   @RBEntry("Version not specified")
   public static final String VERSION_NOT_SPECIFIED = "53";

   @RBEntry("Multilevel BOM Compare")
   public static final String MULTI_BOM_COMPARE_LABEL = "54";

   @RBEntry("Multilevel Where Used")
   public static final String MULTI_WHERE_USED_TITLE = "55";

   @RBEntry("Multilevel Where Used")
   public static final String MULTI_WHERE_USED_LABEL = "56";

   @RBEntry("Cancel")
   public static final String CANCEL_BUTTON_LABEL = "57";

   @RBEntry("Generate Report")
   public static final String GENERATE_REPORT_BUTTON_LABEL = "58";

   @RBEntry("Search Parts...")
   public static final String SEARCH_PARTS_BUTTON_LABEL = "59";

   @RBEntry("Set Config Spec...")
   public static final String SET_CONFIG_SPEC_BUTTON_LABEL = "60";

   @RBEntry("Name:")
   public static final String GENERIC_PART_LABEL = "158";

   @RBEntry("Search...")
   public static final String GENERIC_SEARCH_PARTS_BUTTON_LABEL = "159";

   @RBEntry("Source Name:")
   public static final String GENERIC_SOURCE_PART_LABEL = "160";

   @RBEntry("Target Name:")
   public static final String GENERIC_TARGET_PART_LABEL = "161";

   @RBEntry("The target config spec does not select a version of the target part; you need to select a different target part and/or config spec.")
   public static final String MULTI_BOM_QUERY_NO_TARGET_VERSION = "74";

   @RBEntry("The source and target parts and config specs are identical; you need to select a different target part and/or config spec.")
   @RBPseudo(false)
   @RBComment("Messages for Instance and Config are in com.ptc.windchill.pdmlink.part.server.processors.processorsResource.rbInfo")
   public static final String MULTI_BOM_QUERY_SAME_VERSIONS = "75";

   /**
    * ----------------------------------------------------------------------
    **/
   @RBEntry("{0} {1}")
   @RBComment(" Used to identify the quantity of a QTPartUsageLink. Example: 1.0 each")
   @RBArgComment0("Value of the amount attribute of WTPartUsageLink")
   @RBArgComment1("Value of the unit attribute of WTPartUsageLink")
   public static final String QUANTITY_FORMAT = "61";

   @RBEntry("{0}")
   @RBComment(" PART_TYPE - Used to identify the part type, Example Result,  Part")
   @RBArgComment0("Class name from Introspection, eg. Part")
   public static final String PART_TYPE = "17";

   /**
    * ----------------------------------------------------------------------
    **/
   @RBEntry("The baseline cannot be deleted becuase it is referenced in a Config Spec.")
   public static final String CANNOT_DELETE_BASELINE = "18";

   @RBEntry("Parts List Bill of Materials for {0}")
   @RBArgComment0("part number")
   public static final String PARTS_LIST_DISPLAY_NAME = "20";

   @RBEntry("Hierarchy Bill of Materials for {0}")
   @RBArgComment0("part number")
   public static final String HIERARCHY_DISPLAY_NAME = "21";

   @RBEntry("Bill of Materials for {0}")
   @RBArgComment0("part number")
   public static final String BILL_OF_MATERIALS_DISPLAY_NAME = "22";

   @RBEntry("{0} ({1}) {2}")
   @RBComment("Format for displaying part information.")
   @RBArgComment0("part number")
   @RBArgComment1("part name")
   @RBArgComment2("part version")
   public static final String PART_DISPLAY_FORMAT = "23";

   @RBEntry("You are not allowed to publish {0}")
   @RBArgComment0("part number")
   public static final String PUBLISH_NOT_ALLOWED = "24";

   @RBEntry("Set Configuration Specification")
   public static final String SET_CONFIG_SPEC_LABEL = "25";

   @RBEntry("Define Alternates")
   public static final String CANNOT_UNDO_CHECKOUT = "26";

   @RBEntry("Remove")
   public static final String MULTIPLE_QUERY_RESULT = "27";

   @RBEntry("Two-Way")
   public static final String TWOWAY = "28";

   @RBEntry("   OK")
   public static final String OK_LABEL = "29";

   @RBEntry("No alternate parts have been defined")
   public static final String NO_ALTERNATES_DEFINED = "30";

   @RBEntry("{0} is already an alternate of {1}.")
   @RBArgComment0("part number")
   @RBArgComment1("part name")
   public static final String ALREADY_AN_ALT = "31";

   @RBEntry("Call to \"{0}\" is missing parameter \"{1}\".")
   @RBArgComment0("part number")
   @RBArgComment1("part name")
   public static final String MISSING_PARAMETER = "32";

   @RBEntry("Call to \"{0}\" has an invalid parameter(s).")
   @RBArgComment0("part number")
   public static final String BAD_PARAMETERS = "33";

   @RBEntry("Data \"{0}\" is missing from form \"{1}\".")
   @RBArgComment0("part number")
   @RBArgComment1("part name")
   public static final String MISSING_FORM_DATA = "34";

   @RBEntry("Find Part")
   @RBComment(" Header for Part Local Search Form")
   public static final String FIND_PART_TITLE = "35";

   @RBEntry("Alternates Local Search Results")
   @RBComment("Window frame title for page displaying search results when adding part alternates")
   public static final String ALTS_SEARCH_RESULTS_HEADER = "36";

   @RBEntry("Search Results")
   @RBComment("The next is the first line of the title on the search results page used when adding part alternates.")
   public static final String ALTS_SEARCH_RESULTS_TITLE = "37";

   @RBEntry("for Adding Alternates of {0}")
   @RBComment("The next is the second line of the header on the search results page used when adding part alternates.  It appears in smaller type than the first line above.")
   @RBArgComment0("part number")
   public static final String FOR_ADDING_ALTERNATES_OF = "38";

   @RBEntry("Add Alternates")
   @RBComment("The next is the label on the submit button of the alternates local search results form.")
   public static final String ADD_ALTERNATES_LABEL = "39";

   @RBEntry("Define Substitutes")
   @RBComment("Window frame title for Define Substitutes page")
   public static final String DEFINE_SUBSTITUTES_TITLE = "62";

   @RBEntry("Define Substitutes for Part {0} in Assembly {1}")
   @RBComment("Page title for Define Substitutes page.")
   @RBArgComment0("part number")
   @RBArgComment1("part name")
   public static final String DEFINE_SUBSTITUTES_HEADER = "63";

   @RBEntry("Define Alternates for Part {0}")
   @RBArgComment0("part number")
   public static final String DEFINE_ALTERNATES_HEADER = "64";

   @RBEntry("Substitutes Local Search Results")
   @RBComment("Window frame title for page displaying search results when adding part substitutes")
   public static final String SUBS_SEARCH_RESULTS_HEADER = "65";

   @RBEntry("Search Results")
   @RBComment("The next is the first line of the title on the search results page used when adding part substitutes.")
   public static final String SUBS_SEARCH_RESULTS_TITLE = "66";

   @RBEntry("for Adding Substitutes for Part {0}<BR> in Assembly {1}")
   @RBComment("The next is the second line of the header on the search results page used when adding part alternates.  It appears in smaller type than the first line above.  The inserts are part identifiers, e.g. \"123 (Big Bolt)\".  Line feeds can be forced by inserting the characters \"<BR>\" at desired line breaks.")
   @RBArgComment0("part number")
   @RBArgComment1("part name")
   public static final String FOR_ADDING_SUBSTITUTES_FOR = "67";

   @RBEntry("Add Substitutes")
   @RBComment("The next is the label on the submit button of the substitutes local search results form")
   public static final String ADD_SUBSTITUTES_LABEL = "68";

   @RBEntry("Part {0} is already a substitute for this part")
   @RBComment("Insert in  next is a part identifier string")
   @RBArgComment0("part number")
   public static final String ALREADY_A_SUB = "69";

   @RBEntry("Modify Search Criteria")
   @RBComment("Label for Modify Search Criteria button on Alternates/Substitutes Local Search Results page")
   public static final String MODIFY_SEARCH_CRITERIA_LABEL = "72";

   @RBEntry("You cannot make a part an alternate or substitute for itself.")
   public static final String CANNOT_REPLACE_BY_SAME_OBJ = "73";

   /**
    * Product Structure Page ---------------------------------------------------
    *
    **/
   @RBEntry("Part List")
   public static final String PART_LIST_LABEL = "70";

   @RBEntry("BOM Report")
   public static final String BOM_REPORT_LABEL = "71";

   @RBEntry("Yes")
   public static final String YES = "76";

   @RBEntry("No")
   public static final String NO = "77";

   @RBEntry("The configuration specification identified above did not select a part version of {0} for {1}.  Please follow the configuration specification link above to choose a new configuration.")
   public static final String NO_SOLUTION_FOR_PRODUCT_INSTANCE = "78";

   /**
    * ----------------------------------------------------------------------------
    * Generic Resource Bundle entry for appending ellipses to some given string.
    *
    **/
   @RBEntry("{0}...")
   public static final String APPEND_ELLIPSES = "79";

   @RBEntry("Add Alternates...")
   public static final String ADD_ALTERNATES_BUTTON = "80";

   @RBEntry("Add Substitutes...")
   public static final String ADD_SUBSTITUTES_BUTTON = "81";

   @RBEntry("Replacements for {0}")
   public static final String REPLACEMENTS_FOR_LABEL = "82";

   /**
    * ----------------------------------------------------------------------
    **/
   @RBEntry("The top level part version has been changed from {0} to {1} to reflect the configuration specification used.")
   public static final String PART_VERSION_HAS_CHANGED = "83";

   @RBEntry("The configuration specification identified above did not select a part version of {0}.  Please follow the configuration specification link above to choose a new configuration.")
   public static final String NO_PART_VERSION_FOUND = "84";

   @RBEntry("{0} {2} occurrence of '{3}' in the context of {1}")
   @RBArgComment0("The Icon for the context master (or version) for the uses occurrences that follow")
   @RBArgComment1("An Identifier for the context master (or version) for the uses occurrences that follow")
   @RBArgComment2("The reference designator of the part")
   @RBArgComment3("The part itself")
   public static final String USES_OCCURRENCE_CONTEXT = "85";

   @RBEntry("{0} {2} occurrence of '{3}' in the context of {1}")
   @RBArgComment0("The Icon for the context master (or version) for the uses occurrences that follow")
   @RBArgComment1("An Identifier for the context master (or version) for the uses occurrences that follow")
   @RBArgComment2("The reference designator of the part")
   @RBArgComment3("The part itself")
   public static final String PATH_OCCURRENCE_CONTEXT = "86";

   @RBEntry("{0} {2} occurrence of '{3}' in the context of {1}")
   @RBArgComment0("The Icon for the context master (or version) for the uses occurrences that follow")
   @RBArgComment1("An Identifier for the context master (or version) for the uses occurrences that follow")
   @RBArgComment2("The reference designator of the part")
   @RBArgComment3("The part itself")
   public static final String COMBINED_PATH_OCCURRENCE_CONTEXT = "87";

   @RBEntry("Reference Designator")
   @RBArgComment0("The name of the uses occurrence")
   public static final String REFERENCE_DESIGNATOR_LABEL = "88";

   @RBEntry("Reference Designator: {0}")
   @RBArgComment0("The name of the path occurrence")
   public static final String PATH_OCCURRENCE_NAME = "89";

   @RBEntry("Reference Designator: {0}")
   @RBArgComment0("The name of the combined path occurrence")
   public static final String COMBINED_PATH_OCCURRENCE_NAME = "90";

   @RBEntry("Unnamed")
   public static final String USES_OCCURRENCE_UNNAMED = "91";

   @RBEntry("Unnamed")
   public static final String PATH_OCCURRENCE_UNNAMED = "92";

   @RBEntry("Unnamed")
   public static final String COMBINED_PATH_OCCURRENCE_UNNAMED = "93";

   @RBEntry("In the context of")
   public static final String OCCURRENCE_CONTEXT_LABEL = "94";

   @RBEntry("{0} occurrences of {1}")
   @RBArgComment0(" The name of the occurrence")
   @RBArgComment1(" The name of the part")
   public static final String OCCURRENCE_HEADER = "95";

   @RBEntry("You cannot reduce the usage quantity below the number of occurrences defined")
   public static final String USAGE_QUANTITY_BELOW_MINIMUM = "96";

   @RBEntry("The unit of measure for a usage quantity with occurrences defined must be 'each' (EA)")
   @RBArgComment0("The required usage quantity")
   public static final String USAGE_QUANTITY_UNIT_WRONG = "97";

   @RBEntry("Referenced:")
   public static final String REFERENCE_DOCUMENT_LABEL = "98";

   @RBEntry("Describing:")
   public static final String DESCRIPTION_DOCUMENT_LABEL = "99";

   @RBEntry("Occurrences are ordered from most general to most specific context")
   public static final String OCCURRENCES_ORDERED_GENERAL_TO_SPECIFIC = "100";

   @RBEntry("Occurrences are ordered from most specific to most general context")
   public static final String OCCURRENCES_ORDERED_SPECIFIC_TO_GENERAL = "101";

   @RBEntry("Related Reports")
   public static final String STRUCTURE_REPORT_LABEL = "102";

   @RBEntry("Show Occurrences")
   public static final String SHOW_OCCURRENCES_LABEL = "103";

   @RBEntry("Hide Occurrences")
   public static final String HIDE_OCCURRENCES_LABEL = "104";

   @RBEntry("Show Describing Documents")
   public static final String SHOW_DESCRIBING_DOCUMENTS_LABEL = "105";

   @RBEntry("Hide Describing Documents")
   public static final String HIDE_DESCRIBING_DOCUMENTS_LABEL = "106";

   @RBEntry("Show Referenced Documents")
   public static final String SHOW_REFERENCED_DOCUMENTS_LABEL = "107";

   @RBEntry("Hide Referenced Documents")
   public static final String HIDE_REFERENCED_DOCUMENTS_LABEL = "108";

   @RBEntry("Expand All")
   public static final String EXPAND_ALL_LABEL = "109";

   @RBEntry("{0} can not use {1}.  {2} objects can not use {3} objects.")
   @RBArgComment0("Identity of the used by part")
   @RBArgComment1("Identity of the uses part")
   @RBArgComment2("Type of the used by part")
   @RBArgComment3("Type of the uses part")
   public static final String INVALID_PART_USAGE = "110";

   @RBEntry("Serial numbered parts can not be assigned to views.")
   public static final String SERIAL_NUMBERED_PARTS_MUST_BE_VIEW_INDEPENDENT = "111";

   @RBEntry("All Configurations")
   public static final String ALL_CONFIGURATIONS = "112";

   @RBEntry("All Product Instances")
   public static final String ALL_INSTANCES = "113";

   @RBEntry("Line numbers have not been assigned to one or more parts being used by the assembly {0}")
   @RBArgComment0("Identity of the assembly")
   public static final String NO_LINENUMBER_ON_LINK = "114";

   @RBEntry("Part {0} is not persistent.")
   @RBArgComment0("Identity of the part")
   public static final String PART_NOT_PERSISTENT = "115";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String SET_PLANNED_INCORPORATION = "116";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String SET_INCORPORATED_DATE = "117";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String INVALID_PLANNED_INCORPORATION = "118";

   @RBEntry("{0} can not be added to the configuration because there is either no part that uses it in the configuration or the part is being traced by users in the configuration.")
   @RBArgComment0("Identity of the part")
   public static final String CAN_NOT_ADD_TO_CONFIGURATION = "119";

   @RBEntry("()")
   @RBPseudo(false)
   @RBComment("Identity for a product master when used in the identity of a configuration/product instance.")
   public static final String WTPRODUCTMASTER_NUMBERNAME_BOTH_NULL = "120";

   @RBEntry("({0})")
   @RBComment("Identity for a product master when used in the identity of a configuration/product instance.")
   @RBArgComment0("The product master's name.")
   public static final String WTPRODUCTMASTER_NUMBERNAME_NUMBER_NULL = "121";

   @RBEntry("({0})")
   @RBComment("Identity for a product master when used in the identity of a configuration/product instance.")
   @RBArgComment0("The product master's number.")
   public static final String WTPRODUCTMASTER_NUMBERNAME_NAME_NULL = "122";

   @RBEntry("({0} - {1})")
   @RBComment("Identity for a product master when used in the identity of a configuration/product instance.")
   @RBArgComment0("The product master's number.")
   @RBArgComment1("The product master's name.")
   public static final String WTPRODUCTMASTER_NUMBERNAME = "123";

   @RBEntry("{0}")
   @RBComment("Identity of a product configuration.")
   @RBArgComment0("The product configuration's name.")
   public static final String PRODUCT_CONFIGURATION_DISPLAY_IDENTIFIER = "124";

   @RBEntry("{0} {1}")
   @RBComment("Identity of a product instance master.")
   @RBArgComment0("The product instance's serial number.")
   @RBArgComment1("The product master's identity.")
   public static final String PRODUCT_INSTANCE_MASTER_DISPLAY_IDENTIFIER = "125";

   @RBEntry("{0} {2} {1}")
   @RBComment("Identity of a product instance.")
   @RBArgComment0("The product instance's serial number.")
   @RBArgComment1("The product master's identity.")
   @RBArgComment2("The product instance's version identity.")
   public static final String PRODUCT_INSTANCE2_DISPLAY_IDENTIFIER = "126";

   @RBEntry("{0} path occurrence data objects were found where one and only one was expected.")
   @RBArgComment0("A number.")
   public static final String INVALID_NUMBER_OF_PATH_OCCURRENCE_DATA_OBJECTS = "127";

   @RBEntry("Allocation for")
   public static final String ALLOCATION_FOR = "128";

   @RBEntry("Allocates")
   public static final String ALLOCATES = "129";

   @RBEntry("Allocated by")
   public static final String ALLOCATED_BY = "130";

   @RBEntry("Properties of {0}")
   @RBComment("Page Title.")
   @RBArgComment0("The object's display identity.")
   public static final String PROPERTIES_OF = "137";

   @RBEntry("Define Alternates")
   public static final String DEFINE_ALTERNATES_TITLE = "138";

   @RBEntry("Remove")
   public static final String REMOVE = "139";

   @RBEntry("Name")
   public static final String NAME = "140";

   @RBEntry("Number")
   public static final String NUMBER = "141";

   @RBEntry("Unable to find a version of the part to assign to this instance.")
   public static final String COULD_NOT_FIND_SERIALNUMBEREDPART = "142";

   @RBEntry("Set Product Instance Configuration Specification")
   public static final String SET_PRODUCT_INSTANCE_CONFIG_SPEC = "143";

   @RBEntry("Product Instance Configuration Specification")
   public static final String PRODUCT_INSTANCE_CONFIG_SPEC = "144";

   @RBEntry("Use Planning Date")
   public static final String USE_PLANNING_DATE = "145";

   @RBEntry("true")
   public static final String TRUE_STRING = "146";

   @RBEntry("false")
   public static final String FALSE_STRING = "147";

   @RBEntry("\nsetPartAttributes: \"{0}\" is an invalid team name. Must be of the form 'Domain.Team'")
   @RBComment("Method name \"setPartAttributes\" at beginning of this message should not be translated.  The rest of the message text can be translated.  This message is displayed only in the output from running the command line tool LoadFromFile.")
   public static final String LOAD_INVALID_TEAMTEMPLATE = "148";

   @RBEntry("Configurations")
   public static final String CONFIGURATIONS = "149";

   @RBEntry("Replacements")
   public static final String REPLACEMENTS = "150";

   @RBEntry("A replacement part can not be the same as the part it is intended to replace.")
   @RBComment("Exception thrown when the replacement part is the same as the part it is supposed to replace.")
   public static final String REPLACEMENT_REFLEXIVE = "152";

   @RBEntry("Search Results for Adding Alternates of {0}")
   @RBComment("Title on the search results page used when adding part alternates (PDMLink)")
   public static final String ALTS_SEARCH_RESULTS_TITLE_2 = "153";

   @RBEntry("Search Results for Adding Substitutes of Part {0} in Assembly {1}")
   @RBComment("Title on the search results page used when adding part substitutes (PDMLink)")
   public static final String SUBS_SEARCH_RESULTS_TITLE_2 = "154";

   @RBEntry("Invalid Incorporated Date")
   public static final String INVALID_INCORPORATED = "155";

   @RBEntry("The document \"{0}\" must be released before it can be associated to the part.")
   @RBComment("Documents of type Reference Document must be released before the PartDoc service will allow users to create reference associations to them.")
   @RBArgComment0("The Reference Document that is not in a released state.")
   public static final String REFDOC_NOT_RELEASED = "156";

   @RBEntry("'Expand All' was not completed because the {0} limit of {1} nodes was exceeded.  You can manually expand any unexpanded nodes or instead use the BOM Report to view the entire structure.")
   @RBComment("There is a limit to how many rows an Expand All action will show in the Product Structure page--this message is used when that limit is exceeded")
   @RBArgComment0("Name of the properties file entry that specifies the limit.")
   @RBArgComment1("The value of the limit.")
   public static final String ABORT_EXPAND_ALL = "157";

   @RBEntry("{0} ({1}) - {2} {3}")
   @RBComment("Format for displaying part information with orgId.")
   @RBArgComment0("part number")
   @RBArgComment1("part orgId")
   @RBArgComment2("part name")
   @RBArgComment3("part version")
   public static final String PART_DISPLAY_FORMAT_WITH_ORGID = "162";

   @RBEntry("{0} - {1} {2}")
   @RBComment("Format for displaying part information withot orgId.")
   @RBArgComment0("part number")
   @RBArgComment1("part name")
   @RBArgComment2("part version")
   public static final String PART_DISPLAY_FORMAT_NO_ORGID = "163";

   @RBEntry("Source Part Configuration")
   @RBComment("Lable for the Source Configuration when displaying Multilevel BOM Compare Report")
   public static final String SOURCE_CONFIGURATION_LABEL = "164";

   @RBEntry("Source Part Instance")
   @RBComment("Lable for the Source Instance when displaying Multilevel BOM Compare Report")
   public static final String SOURCE_INSTANCE_LABEL = "165";

   @RBEntry("Target Part Configuration")
   @RBComment("Lable for the Target Configuration when displaying Multilevel BOM Compare Report")
   public static final String TARGET_CONFIGURATION_LABEL = "166";

   @RBEntry("Target Part Instance")
   @RBComment("Lable for the Target Instance when displaying Multilevel BOM Compare Report")
   public static final String TARGET_INSTANCE_LABEL = "167";

   @RBEntry("Extended BOM Report")
   @RBComment("Label for Extended BOM Report drop down item")
   public static final String EXTENDED_BOM_REPORT_LABEL = "168";

   @RBEntry("Extended Part List")
   @RBComment("Label for Extended Part List report drop down item")
   public static final String EXTENDED_PART_LIST_LABEL = "169";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String NEED_BASELINE_FOR_CONFIG_SPEC = "171";

   @RBEntry("")
   @RBComment("String no longer used")
   public static final String NEED_EFFECTIVITY_CONTEXT_FOR_CONFIG_SPEC = "172";

   @RBEntry("Where Used Structure")
   @RBComment("Column header for the Multilevel Where Used Report for column that shows the structure")
   public static final String WHERE_USED_STRUCTURE_HEADER = "173";

   /**
    * Column Headers for report.  Most are accessed by key number not constant.
    **/
   @RBEntry("Part Instance:")
   @RBComment("Used in Multilevel Where Used Report when its a product instance and not a part being reported on")
   public static final String PRODUCT_INSTANCE_LABEL = "174";

   @RBEntry("Estimated Incorporation Date")
   @RBComment("Used in Multilevel Where Used Report for a part instance")
   public static final String PLANNED_INCORPORATION_DATE_HEADER = "175";

   @RBEntry("Start Incorporation Date")
   @RBComment("Used in Multilevel Where Used Report for a part instance")
   public static final String INCORPORATED_DATE_HEADER = "176";

   @RBEntry("End Incorporation Date")
   @RBComment("Used in Multilevel Where Used Report for a part instance")
   public static final String UNINCORPORATED_DATE_HEADER = "177";

   /**
    * Messages for PartRepresentation-------------------------------------------
    *
    **/
   @RBEntry("\ncreatePartRepresentation: Part \"{0}\" doesn't exist in the load file or it was not created successfully.")
   @RBArgComment0("part number")
   @RBComment("Method name \"createPartRepresentation\" at beginning of this message should not be translated.  The rest of the message text can be translated. This message is displayed only in the output from running the command line tool LoadFromFile.")
   public static final String LOAD_NO_PART_REPRESENTATION = "180";

   @RBEntry("Added Representation \"{0}\" to Part \"{1}\"")
   @RBArgComment0("Representation name")
   @RBArgComment1("part number")
   public static final String LOAD_REPRESENTATION_ADDED = "181";

   @RBEntry("Failed to add Representation \"{0}\" to Part \"{1}\"")
   @RBArgComment0("Representation name")
   @RBArgComment1("part number")
   public static final String LOAD_REPRESENTATION_FAILED = "182";

   /**
    * Messages for PartRepresentation-------------------------------------------
    *
    **/
   @RBEntry("\nremovePartFromAssembly: Part \"{0}\" doesn't exist in the load file or it was not created successfully.")
   @RBArgComment0("part number")
   @RBComment("Method name \"removePartFromAssembly\" at beginning of this message should not be translated.  The rest of the message text can be translated. This message is displayed only in the output from running the command line tool LoadFromFile.")
   public static final String REMOVE_NO_PART = "185";

   /**
    * Messages for PartRepresentation-------------------------------------------
    *
    **/
   @RBEntry("\ncreateNewViewVersion: Part \"{0}\" doesn't exist in the load file or it was not created successfully.")
   @RBArgComment0("part number")
   @RBComment("Method name \"createNewViewVersion\" at beginning of this message should not be translated.  The rest of the message text can be translated. This message is displayed only in the output from running the command line tool LoadFromFile.")
   public static final String NEWVIEWVERSION_NO_PART = "190";

   @RBEntry("\ncreateNewViewVersion: Failed to locate view \"{0}\"")
   @RBArgComment0("Named View")
   @RBComment("Method name \"createNewViewVersion\" at beginning of this message should not be translated.  The rest of the message text can be translated. This message is displayed only in the output from running the command line tool LoadFromFile.")
   public static final String NEWVIEWVERSION_NO_VIEW = "191";

   @RBEntry("\ncreateNewViewVersion: Failed to create new view version for \"{0}\"")
   @RBArgComment0("Part Number")
   @RBComment("Method name \"createNewViewVersion\" at beginning of this message should not be translated.  The rest of the message text can be translated. This message is displayed only in the output from running the command line tool LoadFromFile.")
   public static final String NEWVIEWVERSION_FAILED = "192";

   /**
    * Labels for End Item Name for Products in PDMLink-------------------------------------------
    *
    **/
   @RBEntry("End Item")
   @RBComment("Label for property for WTPart in PDMLink")
   public static final String PDMLINK_END_ITEM_PROPERTY = "193";

   @RBEntry("End Item:")
   @RBComment("Label for WTPart in PDMLink")
   public static final String PDMLINK_END_ITEM_LABEL = "194";

   @RBEntry("Part Configuration")
   @RBComment("Label for property of WTProductConfiguration in PDMLink")
   public static final String PDMLINK_END_ITEM_CONFIGURATION_PROPERTY = "195";

   @RBEntry("Part Configuration:")
   @RBComment("Label for WTProductConfiguration in PDMLink")
   public static final String PDMLINK_END_ITEM_CONFIGURATION_LABEL = "196";

   @RBEntry("Part Instance")
   @RBComment("Label for property of WTProductInstance2 in PDMLink")
   public static final String PDMLINK_END_ITEM_INSTANCE_PROPERTY = "197";

   @RBEntry("Part Instance:")
   @RBComment("Label for WTProductInstance2 in PDMLink")
   public static final String PDMLINK_END_ITEM_INSTANCE_LABEL = "198";

   /**
    * Messages for assignUserToProduct in LoadPart
    **/
   @RBEntry("Failed to locate user \"{0}\"")
   public static final String FAILED_LOCATE_USER = "201";

   @RBEntry("Failed to locate product \"{0}\"")
   public static final String FAILED_LOCATE_PRODUCT = "202";

   @RBEntry("Failed to locate role \"{0}\"")
   public static final String FAILED_LOCATE_ROLE = "203";

   @RBEntry("The Configuration Specification specifies no version of the current Part instance!")
   public static final String CONFIG_SPEC_SELECTS_NO_INSTANCE_VERSION = "204";

   /**
    * Message for LoadPart.createProductCotainer
    **/
   @RBEntry("Container template \"{0}\" not found.")
   public static final String CONTAINER_TEMPLATE_NOT_FOUND = "205";

   @RBEntry("The following errors occurred while attempting to add parts to {0}:")
   public static final String MULTI_OBJECT_VALIDATE_ADD_TO_CONFIGURATION = "206";

   /**
    * Message for trying to store a part with proi isntalled
    **/
   @RBEntry("Not allowed to create WTParts with ProIntralink installed")
   public static final String CANNOT_CREATE_WTPART_WITH_PROI_INSTALLED = "207";

   /**
    * Message for LoadPart.createProductEffectivity
    **/
   @RBEntry("Could not find {0} with number {1}.")
   public static final String OBJECT_NOT_FOUND = "208";

   @RBEntry("More than one {0} found with number {1}.")
   public static final String MULTIPLE_OBJECTS_FOUND = "209";

   @RBEntry("Properties of {0} {1}")
   @RBComment("Page Title.")
   @RBArgComment0("The class name.")
   @RBArgComment1("The class value.")
   public static final String PROPERTIES_OF_2 = "210";

   @RBEntry("Details for {0}")
   @RBComment("Page Title.")
   @RBArgComment0("The object's display identity.")
   public static final String DETAILS_FOR = "211";

   @RBEntry("The value '{0}' for Reference Designator is not valid. The characters '{1}' and '{2}' are reserved and can not be used as a part of the reference designator unless the reference designator begins with '{3}' and ends with '{4}'.")
   @RBComment("Error message in exception when reference designator contains invalid range or sequence delimiter")
   @RBArgComment0("The reference designator set being parsed")
   @RBArgComment1("The current range delimiter")
   @RBArgComment2("The current sequence delimiter")
   @RBArgComment3("The current begin escape delimiter")
   @RBArgComment4("The current end escape delimiter")
   public static final String REFERENCE_DESIGNATOR_DELIMITER_PARSE_ERROR = "212";

   @RBEntry("The view specified is not valid for current part iteration.")
   public static final String VIEW_NOT_ASSIGNABLE = "213";

   @RBEntry("One or more uses relations in the list do not exist.")
   public static final String USES_RELATION_NOT_EXISTS = "214";

   @RBEntry("{0} cannot be allocated.  It is not an instance of the child or an alternate or substitute of the child.")
   @RBArgComment0("The instance's display identity")
   public static final String INVALID_CHILD_FOR_ALLOCATION = "215";

   @RBEntry("{0} cannot be specified as an override.  It is not a version of the child, nor is it a valid replacement of it.")
   @RBArgComment0("The part version's display identity")
   public static final String INVALID_CHILD_FOR_OVERRIDE = "216";

   @RBEntry("Occurrences of {0}")
   @RBArgComment0(" The name of the part")
   public static final String OCCURRENCES_HEADER = "217";

   @RBEntry("Can't add {0} as a child of itself in the structure.")
   @RBArgComment0(" The name of the part")
   public static final String CAN_NOT_ADD_CHILD_TO_SELF = "218";

   @RBEntry("Find number, {0}, is not unique.  Please change it to be unique.")
   @RBArgComment0(" Find Number")
   public static final String FIND_NUMBER_NOT_UNIQUE = "219";

   @RBEntry("Search Part Instances...")
   public static final String SEARCH_INSTANCES_BUTTON_LABEL = "220";

   @RBEntry("Search Part Configurations...")
   public static final String SEARCH_CONFIGURATIONS_BUTTON_LABEL = "221";

   @RBEntry("Configuration:")
   public static final String MULTI_LEVEL_CONFIGURATION_LABEL = "222";

   @RBEntry("Part Instance Compare Results")
   public static final String MULTI_LEVEL_INSTANCE_RESULT_TITLE = "223";

   @RBEntry("Part Configuration Compare Results")
   public static final String MULTI_LEVEL_PART_CONFIG_RESULT_TITLE = "224";

   @RBEntry("Multi-Level Bill of Materials Compare Results")
   public static final String MULTILEVEL_COMPARE = "225";

   @RBEntry("Part Instance Compare Query")
   public static final String PART_INSTANCE_COMPARE_TITLE = "226";

   @RBEntry("Part Configuration Compare Query")
   public static final String PART_CONFIGURATION_COMPARE_TITLE = "227";

   @RBEntry("Part")
   public static final String FIND_JCA_PART_LABEL = "228";

   @RBEntry("Part Instance Search returns only those with a Part Configuration")
   @RBComment("Message to explain filtering on the search results")
   public static final String PART_INSTANCE_COMPARE_SEARCH_MSG = "229";

   /**
    * Message for trying to create a part without PDMLink or ProjectLink isntalled
    **/
   @RBEntry("Not allowed to create Parts without PdmLink or ProjectLink installed")
   public static final String CANNOT_CREATE_WTPART_WITHOUT_PDMLINK_AND_PROJECTLINK_INSTALLED = "230";

   @RBEntry("Not allowed to change the trace code of a part that is the context of a pending effectivity for a change.")
   @RBComment("Error message when user tries to change the trace code of a part that is currently the context of a pending effectivity for a change.")
   public static final String PART_MASTER_TRACE_CODE_CHANGE_ERROR_MESSAGE = "231";

   @RBEntry("Not allowed to change end item attribute for part that is referenced by a Change Request as an affected end item or is the primary end item of a product.")
   @RBComment("Error message when user tries to uncheck end item flag for primary end items or for a part that is referenced by a Change Request as an affected end item.")
   public static final String PART_MASTER_END_ITEM_CHANGE_ERROR_MESSAGE = "232";

   @RBEntry("End Item")
   public static final String END_ITEM_TOOLTIP = "233";

   @RBEntry("Find Number can only be alphanumerical.  Thus, {0}, needs to be changed.")
   public static final String FIND_NUMBER_NOT_ALPHANUMERICAL_ERROR = "234";

   @RBEntry("Parts in Projects are not allowed to have alternates.")
   public static final String ALTS_NOT_ALLOWED_IN_PROJECT = "235";

   @RBEntry("Illegal Alternate or Substitute link creation.  {0} cannot be linked to {1} because one of the objects reside in a project.")
   @RBArgComment0("RoleA object.")
   @RBArgComment1("RoleB object.")
   public static final String CANT_CREATE_ACROSS_CONT = "236";

   @RBEntry("Cannot create uses association for parent {0} and child {1}.\nThe uses association between parent type [{2}] and child type [{3}] is not valid.")
   @RBComment("Message for validationg of pre_store event.")
   public static final String INVALID_ROLE_B_TYPE = "237";

   @RBEntry("Module Variant")
   @RBComment("Tool tip used to display on icons")
   public static final String VARIANT_TOOLTIP = "238";

   @RBEntry("Configurable Module")
   @RBComment("Tool tip used to display on icons")
   public static final String CONFIGURABLE_PART = "240";

   @RBEntry("Advanced Configurable End Item")
   @RBComment("Tool tip used to display on icons")
   public static final String ADVANCED_CONFIGURABLE_ENDITEM = "241";

   @RBEntry("Advanced Configurable Part")
   @RBComment("Tool tip used to display on icons")
   public static final String ADVANCED_CONFIGURABLE = "242";

   @RBEntry("Configurable Product")
   @RBComment("Tool tip used to display on icons")
   public static final String CONFIGURABLE_ENDITEM = "243";

   @RBEntry("Effectivity Type {0} is not valid for effectivity context {1}.")
   @RBComment("Message for validating effectivity type for loading effectivity.")
   public static final String EFF_TYPE_ERROR = "244";

   /**
    * Display Identification for Part Uses Occurrence (aka Reference Designator)
    **/
   @RBEntry("Reference Designator")
   @RBComment("display type of part uses occurrence")
   public static final String PART_USES_OCCURRENCE_DISPLAY_TYPE = "245";

   @RBEntry("Uses Part: {0}, {1}: {2}")
   @RBComment("display identifier of reference designator")
   public static final String PART_USES_OCCURRENCE_DISPLAY_IDENTIFIER = "246";

   @RBEntry("Uses: {0}, {1}: {2}")
   @RBComment("display identity of reference designator")
   public static final String PART_USES_OCCURRENCE_DISPLAY_IDENTITY = "247";
   
   @RBComment("The display identifier for a WTPartUsageLink.  This is displayed when an attempt is made to save a new child part to a structure that violates uniqueness constraints")
   @RBArgComment0("The identity of the parent part")
   @RBArgComment1("The identity of the child part")
   @RBArgComment2("The line number assigned to the child in the part structure")
   @RBEntry("Parent {0} to Child {1} with Line Number {2}")
   public static final String PART_USAGE_DISPLAY_IDENTIFIER = "246.1";

   @RBEntry("Configuration Specification")
   public static final String MULTI_BOM_COMPARE_CONFIG_LABEL = "248";

   @RBEntry("Options Filter")
   public static final String MULTI_BOM_COMPARE_FILTER_LABEL = "249";

   @RBEntry("None")
   public static final String NONE = "250";

   @RBEntry("Expansion Criteria")
   @RBComment("Label preceding the expansion criteria in HTML")
   public static final String EXPANSION_CRITERIA_LABEL = "251";

   @RBEntry("Working")
   @RBComment("Working label")
   public static final String WORKING_LABEL = "252";

   @RBEntry("Applied to Top")
   @RBComment("Applied to Top label")
   public static final String APPLIED_TO_TOP_LABEL = "253";

   @RBEntry("Use Default")
   @RBComment("Use Default label")
   public static final String USE_DEFAULT_LABEL = "254";

   @RBEntry("Context")
   @RBComment("Context label")
   public static final String CONTEXT_LABEL = "255";

   @RBEntry("Base Part Number")
   @RBComment("This label indicates the base part number  for a part configuration")
   public static final String BASE_PART_NUMBER_LABEL = "256";

   @RBEntry("Base Part Name")
   @RBComment("This label indicates the base part name for a part configuration")
   public static final String BASE_PART_NAME_LABEL = "257";

   @RBEntry("Base Part Version")
   @RBComment("This label indicates the base part version  for a part configuration")
   public static final String BASE_PART_VERSION_LABEL = "258";

   @RBEntry("Lot Number")
   @RBComment("This label indicates the Lot Number for a part instance")
   public static final String LOT_NUMBER_LABEL = "259";

   @RBEntry("Serial Number")
   @RBComment("This label indicates the Serial Number for a part instance")
   public static final String SERIAL_NUMBER_LABEL = "260";

   @RBEntry("Lot/Serial Number")
   @RBComment("This label indicates both Lot Number and Serial Number for a part instance")
   public static final String LOT_SERIAL_LABEL = "261";

   @RBEntry("Configuration Name")
   @RBComment("This label indicates the Serial Number for a part instance and part configuration")
   public static final String CONFIGURATION_NAME_LABEL = "262";

   @RBEntry("The value '{0}' for Reference Designator is not valid. Zero-padded numeric portions in a range of reference designators must be the same length.")
   @RBComment("Error message in exception when reference designator contains a range with zero-padded numeric portions of different lengths.")
   @RBArgComment0("The reference designator set being parsed")
   public static final String REFERENCE_DESIGNATOR_DELIMITER_ZERO_PADDING_PARSE_ERROR = "263";

   @RBEntry("Phantom")
   @RBComment("The label for Phantom Assemblies")
   public static final String PHANTOM_ASSMEBLY_LABEL = "264";

   @RBEntry("Cannot have multiple usagelinks to the same child part with Completed or To Be Built occurrences.")
   @RBComment("Error message in exception when an occurrence is added/modified which results in the cadSynched status being changed to Yes or Pending.")
   public static final String BUILT_USAGELINK_CHANGE_PIB_ERROR = "265";

   @RBEntry("Gathering Part")
   @RBComment("The tooltip for Gathering Parts")
   public static final String CAD_PHANTOM_TOOLTIP = "266";

   @RBEntry("Cannot modify a CAD Synchronized occurrence, '{0}'.")
   @RBComment("Error message when a built part uses occurrence is modified.")
   @RBArgComment0("reference designator of the part uses occurrence.")
   public static final String CANNOT_MODIFY_BUILT_OCCURRENCE = "267";

   @RBEntry("Cannot set build status Completed on occurrence. This can only be set through Build CAD or Build Part actions.")
   @RBComment("Error message when attempting to set the Completed value on build status of a uses occurrence.")
   public static final String CANNOT_SET_BUILT_STATUS_ON_OCCURRENCE = "268";

   @RBEntry("Product Variant")
   @RBComment("Tool tip for a Part with Configurable Module=Variant and End Item=Yes")
   public static final String VARIANT_ENDITEM_TOOLTIP = "269";

   @RBEntry("Cannot add {0} with a quantity 0 and unit {1} to the structure. The quantity must be non-zero.")
   @RBArgComment0("The name of the part")
   @RBArgComment1("The name of the unit")
   public static final String CANNOT_ADD_QTY_ZERO = "270";

   @RBEntry("Not allowed to create Parts without PdmLink, ProjectLink or QualityLink installed")
   @RBComment("Error message when attempting to create part without PDM Link,Project Link or Quality Link installed.")
   public static final String CANNOT_CREATE_WTPART_WITHOUT_PDMLINK_AND_PROJECTLINK_AND_QMS_INSTALLED = "271";

   @RBEntry("Cannot create a usage occurrence for a link with a quantity unit other than {0} that has more than 1 occurrence")
   @RBArgComment0("QuantityUnit.EA display value")
   public static final String CANNOT_CREATE_OCCURRENCE_WHERE_EA_AND_MORE_THAN_ZERO = "272";

   @RBEntry("Cannot complete this action because the system is trying to create a duplicate path occurrence. Please report this error to your system administrator.")
   @RBArgComment0("Duplicate path occurrences error message")
   public static final String DUPLICATE_PATH_OCCURRENCES_NOT_ALLOWED = "273";

   @RBEntry("Reference designator is not unique\r\rThe reference designator value {0}, must be unique within this sub-assembly.")
   @RBComment("Error message when the reference designator entered by the user for an occurrence is not unique within a sub-assembly")
   public static final String REF_DESIG_NOT_UNIQUE = "REF_DESIG_NOT_UNIQUE";
}
