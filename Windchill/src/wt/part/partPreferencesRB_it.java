/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.part;

import wt.util.resource.*;

@RBUUID("wt.part.partPreferencesRB")
@RBNameException //Grandfathered by conversion
public final class partPreferencesRB_it extends WTListResourceBundle {
   @RBEntry("Lunghezza max visualizzata indicatori di riferimento")
   @RBComment("Name of preference that controls the maximum display length of a reference designator range.")
   public static final String REFERENCE_DESIGNATOR_DISPLAY_LENGTH = "REFERENCE_DESIGNATOR_DISPLAY_LENGTH";

   @RBEntry("Lunghezza massima visualizzata delle liste di indicatori di riferimento")
   @RBComment("Short description of preference that controls the maximum display length of a reference designator range.")
   public static final String REFERENCE_DESIGNATOR_DISPLAY_LENGTH_SD = "REFERENCE_DESIGNATOR_DISPLAY_LENGTH_SD";

   @RBEntry("Durante la visualizzazione dei casi d'impiego in una struttura di prodotto, questa preferenza controlla la lunghezza massima di visualizzazione delle liste di indicatori di riferimento")
   @RBComment("Long description of preference that controls the maximum display length of a reference designator range.")
   public static final String REFERENCE_DESIGNATOR_DISPLAY_LENGTH_LD = "REFERENCE_DESIGNATOR_DISPLAY_LENGTH_LD";

   @RBEntry("Carattere intervallo indicatori di riferimento")
   @RBComment("Name of preference that controls the character that is used to designate a range of reference designators.")
   public static final String REFERENCE_DESIGNATOR_RANGE_CHARACTER = "REFERENCE_DESIGNATOR_RANGE_CHARACTER";

   @RBEntry("Carattere utilizzato per indicare un intervallo di indicatori di riferimento")
   @RBComment("Short description of preference that controls the character that is used to designate a range of reference designators.")
   public static final String REFERENCE_DESIGNATOR_RANGE_CHARACTER_SD = "REFERENCE_DESIGNATOR_RANGE_CHARACTER_SD";

   @RBEntry("Durante la visualizzazione dei casi d'impiego in una struttura di prodotto, questa preferenza controlla il carattere utilizzato per indicare un intervallo di indicatori di riferimento")
   @RBComment("Long description of preference that controls the character that is used to designate a range of reference designators.")
   public static final String REFERENCE_DESIGNATOR_RANGE_CHARACTER_LD = "REFERENCE_DESIGNATOR_RANGE_CHARACTER_LD";

   @RBEntry("Carattere di separazione indicatori di riferimento")
   @RBComment("Name of preference that controls the character that is used to separate individual reference designators in a list of reference designators.")
   public static final String REFERENCE_DESIGNATOR_SEPARATOR_CHARACTER = "REFERENCE_DESIGNATOR_SEPARATOR_CHARACTER";

   @RBEntry("Carattere utilizzato per separare i singoli indicatori in una lista di indicatori di riferimento")
   @RBComment("Short description of preference that controls the character that is used to separate individual reference designators in a list of reference designators.")
   public static final String REFERENCE_DESIGNATOR_SEPARATOR_CHARACTER_SD = "REFERENCE_DESIGNATOR_SEPARATOR_CHARACTER_SD";

   @RBEntry("Durante la visualizzazione dei casi d'impiego in una struttura di prodotto, questa preferenza controlla il carattere utilizzato per separare i singoli indicatori in una lista di indicatori di riferimento")
   @RBComment("Long description of preference that controls the character that is used to separate individual reference designators in a list of reference designators.")
   public static final String REFERENCE_DESIGNATOR_SEPARATOR_CHARACTER_LD = "REFERENCE_DESIGNATOR_SEPARATOR_CHARACTER_LD";

   @RBEntry("Carattere di escape all'inizio dell'indicatore di riferimento")
   @RBComment("Name of preference that controls the character that is used at the beginning of a reference designator to allow entry of special characters (Reference Designator Range Character and Reference Designator Sequence Character) as normal data input rather than as special characters.")
   public static final String REFERENCE_DESIGNATOR_START_ESCAPE_CHARACTER = "REFERENCE_DESIGNATOR_START_ESCAPE_CHARACTER";

   @RBEntry("Carattere utilizzato all'inizio di un indicatore di riferimento per consentire l'immissione di caratteri speciali (carattere intervallo indicatori di riferimento e carattere sequenza indicatori di riferimento) come normali dati di input anziché come caratteri speciali.")
   @RBComment("Short description of preference that controls the character that is used at the beginning of a reference designator to allow entry of special characters (Reference Designator Range Character and Reference Designator Sequence Character) as normal data input rather than as special characters.")
   public static final String REFERENCE_DESIGNATOR_START_ESCAPE_CHARACTER_SD = "REFERENCE_DESIGNATOR_START_ESCAPE_CHARACTER_SD";

   @RBEntry("Durante la visualizzazione dei casi d'impiego in una struttura di prodotto, questa preferenza controlla il carattere utilizzato all'inizio di un indicatore di riferimento per consentire l'immissione di caratteri speciali (carattere intervallo indicatori di riferimento e carattere sequenza indicatori di riferimento) come normali dati di input anziché come caratteri speciali.")
   @RBComment("Long description of preference that controls the character that is used at the beginning of a reference designator to allow entry of special characters (Reference Designator Range Character and Reference Designator Sequence Character) as normal data input rather than as special characters.")
   public static final String REFERENCE_DESIGNATOR_START_ESCAPE_CHARACTER_LD = "REFERENCE_DESIGNATOR_START_ESCAPE_CHARACTER_LD";

   @RBEntry("Carattere di escape alla fine dell'indicatore di riferimento")
   @RBComment("Name of preference that controls the character that is used at the end of a reference designator to allow entry of special characters (Reference Designator Range Character and Reference Designator Sequence Character) as normal data input rather than as special characters.")
   public static final String REFERENCE_DESIGNATOR_END_ESCAPE_CHARACTER = "REFERENCE_DESIGNATOR_END_ESCAPE_CHARACTER";

   @RBEntry("Carattere utilizzato alla fine di un indicatore di riferimento per consentire l'immissione di caratteri speciali (carattere intervallo indicatori di riferimento e carattere sequenza indicatori di riferimento) come normali dati di input anziché come caratteri speciali.")
   @RBComment("Short description of preference that controls the character that is used at the end of a reference designator to allow entry of special characters (Reference Designator Range Character and Reference Designator Sequence Character) as normal data input rather than as special characters.")
   public static final String REFERENCE_DESIGNATOR_END_ESCAPE_CHARACTER_SD = "REFERENCE_DESIGNATOR_END_ESCAPE_CHARACTER_SD";

   @RBEntry("Durante la visualizzazione dei casi d'impiego in una struttura di prodotto, questa preferenza controlla il carattere utilizzato alla fine di un indicatore di riferimento per consentire l'immissione di caratteri speciali (carattere intervallo indicatori di riferimento e carattere sequenza indicatori di riferimento) come normali dati di input anziché come caratteri speciali.")
   @RBComment("Long description of preference that controls the character that is used at the end of a reference designator to allow entry of special characters (Reference Designator Range Character and Reference Designator Sequence Character) as normal data input rather than as special characters.")
   public static final String REFERENCE_DESIGNATOR_END_ESCAPE_CHARACTER_LD = "REFERENCE_DESIGNATOR_END_ESCAPE_CHARACTER_LD";

   @RBEntry("Gestione parti")
   @RBComment("Name of the part management category in preferences.")
   public static final String PART_MANAGEMENT_CATEGORY = "PART_MANAGEMENT_CATEGORY";

   @RBEntry("Indicatori di riferimento")
   @RBComment("Name of the reference designator sub-category in preferences.")
   public static final String REFERENCE_DESIGNATOR_CATEGORY = "REFERENCE_DESIGNATOR_CATEGORY";

   @RBEntry("Logica di associazione parte a documento")
   public static final String PREFERENCE_RELATED_DOCUMENT_PMD_METHOD_NAME = "PREFERENCE_RELATED_DOCUMENT_PMD_METHOD_NAME";

   @RBEntry("Controlla la logica alla base dell'associazione tra una parte e un documento. Il valore false limita le associazioni basate sul tipo di link e sul tipo soft del documento e impedisce l'associazione di più versioni di un documento a una parte. Il valore true rimuove queste limitazioni.")
   public static final String PREFERENCE_RELATED_DOCUMENT_PMD_METHOD_SHORT_DESC = "PREFERENCE_RELATED_DOCUMENT_PMD_METHOD_SHORT_DESC";

   @RBEntry(" Controlla la logica in base alla quale i documenti sono messi in relazione con una parte e visualizzati. Se false, verrà utilizzata la logica di PDMLink. Se true, verrà utilizzata la logica di Windchill PDM. La logica di PDMLink impone che un documento di tipo Riferimento possa essere associato alla parte esclusivamente come documento Riferimenti e che tutti gli altri tipi di documento siano associati alla parte come documenti Descritto con. La logica consente di creare relazioni Riferimenti e Descritto con da parte a documento a prescindere dal tipo di documento. Vedere le tabelle Riferimenti e Descritto con della pagina di informazioni della parte per una descrizione del funzionamento dei documenti Riferimenti e Descritto con. Infine, la logica impone che una parte possa essere correlata solo a una versione di un documento, ma consente che una parte sia associata a più versioni di uno stesso documento.")
   public static final String PREFERENCE_RELATED_DOCUMENT_PMD_METHOD_LONG_DESC = "PREFERENCE_RELATED_DOCUMENT_PMD_METHOD_LONG_DESC";

   @RBEntry("Elenco con virgole di separazione di stati del ciclo di vita dei documenti utilizzati per visualizzare i documenti di riferimento associati alla parte.")
   public static final String PREFERENCE_RELATED_DOCUMENT_RELEASED_STATE_NAME = "PREFERENCE_RELATED_DOCUMENT_RELEASED_STATE_NAME";

   @RBEntry("Elenco con virgole di separazione di stati del ciclo di vita dei documenti utilizzati per visualizzare i documenti di riferimento associati alla parte.")
   public static final String PREFERENCE_RELATED_DOCUMENT_RELEASED_STATE_SHORT_DESC = "PREFERENCE_RELATED_DOCUMENT_RELEASED_STATE_SHORT_DESC";

   @RBEntry("Gli stati del ciclo di vita elencati in questa preferenza vengono usati per scegliere la versione del documento di riferimento da mostrare nella tabella nella pagina di informazioni sulla parte. Il valore della preferenza, per il quale viene fatta distinzione fra maiuscole e minuscole, deve corrispondere esattamente alla costante definita nel file wt\\lifecyclw\\StateRB.rbInfo file.")
   public static final String PREFERENCE_RELATED_DOCUMENT_RELEASED_STATE_LONG_DESC = "PREFERENCE_RELATED_DOCUMENT_RELEASED_STATE_LONG_DESC";

   @RBEntry("Attiva quantità e indicatori di riferimento per parti di sostituzione")
   public static final String QUANTITY_AND_REFERENCE_DESIGNATOR_FOR_SUBSTITUTE_PART = "QUANTITY_AND_REFERENCE_DESIGNATOR_FOR_SUBSTITUTE_PART";

   @RBEntry("Se impostata, gli utenti possono definire la quantità e gli indicatori di riferimento per le parti di sostituzione. Questi attributi vengono utilizzati per sostituire la parte.")
   public static final String QUANTITY_AND_REFERENCE_DESIGNATOR_FOR_SUBSTITUTE_PART_SHORT_DESC = "QUANTITY_AND_REFERENCE_DESIGNATOR_FOR_SUBSTITUTE_PART_SHORT_DESC";

   @RBEntry("Se impostata, gli utenti possono definire la quantità e gli indicatori di riferimento per le parti di sostituzione. Questi attributi vengono utilizzati per sostituire la parte.")
   public static final String QUANTITY_AND_REFERENCE_DESIGNATOR_FOR_SUBSTITUTE_PART_LONG_DESC = "QUANTITY_AND_REFERENCE_DESIGNATOR_FOR_SUBSTITUTE_PART_LONG_DESC";

   @RBEntry("Tipo WTPart di default")
   @RBComment("Name of preference that allows users to configure the default type of WTPart to use.")
   public static final String ON_DEMAND_DEFAULT_TYPE_WTPART = "ON_DEMAND_DEFAULT_TYPE_WTPART";

   @RBEntry("Tipo WTPart di default utilizzato durante il processo di creazione")
   @RBComment("Short description of preference that controls the default WTPart type to use during the build process.")
   public static final String ON_DEMAND_DEFAULT_TYPE_WTPART_SD = "ON_DEMAND_DEFAULT_TYPE_WTPART_SD";

   @RBEntry("Tipo WTPart di default utilizzato durante il processo di creazione. Specificato in forma esterna, ad esempio WCTYPE|wt.part.WTPart|com.example.MyType.")
   @RBComment("Long description of preference that controls the default WTPart type to use during the build process.")
   public static final String ON_DEMAND_DEFAULT_TYPE_WTPART_LD = "ON_DEMAND_DEFAULT_TYPE_WTPART_LD";

   @RBEntry("Tipo WTPartUsageLink di default")
   @RBComment("Name of preference that allows users to configure the default type of WTPartUsageLink to use.")
   public static final String ON_DEMAND_DEFAULT_TYPE_WTPART_USAGE_LINK = "ON_DEMAND_DEFAULT_TYPE_WTPART_USAGE_LINK";

   @RBEntry("Tipo WTPartUsageLink di default utilizzato durante il processo di creazione")
   @RBComment("Short description of preference that controls the default WTPartUsageLink type to use during the build process.")
   public static final String ON_DEMAND_DEFAULT_TYPE_WTPART_USAGE_LINK_SD = "ON_DEMAND_DEFAULT_TYPE_WTPART_USAGE_LINK_SD";

   @RBEntry("Tipo WTPartUsageLink di default utilizzato durante il processo di creazione. Specificato in forma esterna, ad esempio WCTYPE|wt.part.WTPartUsageLink|com.example.MyUsageLinkType.")
   @RBComment("Long description of preference that controls the default WTPartUsageLink type to use during the build process.")
   public static final String ON_DEMAND_DEFAULT_TYPE_WTPART_USAGE_LINK_LD = "ON_DEMAND_DEFAULT_TYPE_WTPART_USAGE_LINK_LD";

   @RBEntry("Tipo WTPart di default")
   @RBComment("Name of preference that allows users to configure the root type of WTPart to use.")
   public static final String ON_DEMAND_ROOT_TYPE_WTPART = "ON_DEMAND_ROOT_TYPE_WTPART";

   @RBEntry("Tipo WTPart radice utilizzato durante il processo di creazione")
   @RBComment("Short description of preference that controls the root WTPart type to use during the build process.")
   public static final String ON_DEMAND_ROOT_TYPE_WTPART_SD = "ON_DEMAND_ROOT_TYPE_WTPART_SD";

   @RBEntry("Tipo WTPart radice utilizzato durante il processo di creazione. Specificato in forma esterna, ad esempio WCTYPE|wt.part.WTPart|com.example.MyType.")
   @RBComment("Long description of preference that controls the root WTPart type to use during the build process.")
   public static final String ON_DEMAND_ROOT_TYPE_WTPART_LD = "ON_DEMAND_ROOT_TYPE_WTPART_LD";

   @RBEntry("Tipo WTPartUsageLink radice")
   @RBComment("Name of preference that allows users to configure the root type of WTPartUsageLink to use.")
   public static final String ON_DEMAND_ROOT_TYPE_WTPART_USAGE_LINK = "ON_DEMAND_ROOT_TYPE_WTPART_USAGE_LINK";

   @RBEntry("Tipo WTPartUsageLink radice utilizzato durante il processo di creazione")
   @RBComment("Short description of preference that controls the root WTPartUsageLink type to use during the build process.")
   public static final String ON_DEMAND_ROOT_TYPE_WTPART_USAGE_LINK_SD = "ON_DEMAND_ROOT_TYPE_WTPART_USAGE_LINK_SD";

   @RBEntry("Tipo WTPartUsageLink radice utilizzato durante il processo di creazione. Specificato in forma esterna, ad esempio WCTYPE|wt.part.WTPartUsageLink|com.example.MyUsageLinkType.")
   @RBComment("Long description of preference that controls the root WTPartUsageLink type to use during the build process.")
   public static final String ON_DEMAND_ROOT_TYPE_WTPART_USAGE_LINK_LD = "ON_DEMAND_ROOT_TYPE_WTPART_USAGE_LINK_LD";

   @RBEntry("Vista parte di default")
   @RBComment("The product structure view specified below will be used by the default configuration specification to expand part or product versions within the Product Structure Explorer (PSE) or when viewing the structure in HTML.")
   public static final String DEFAULT_CONFIGSPEC_VIEW = "DEFAULT_CONFIGSPEC_VIEW";

   @RBEntry("Nome di una vista della parte")
   @RBComment("The product structure view specified below will be used by the default configuration specification to expand part or product versions within the Product Structure Explorer (PSE) or when viewing the structure in HTML.")
   public static final String DEFAULT_CONFIGSPEC_VIEW_SD = "DEFAULT_CONFIGSPEC_VIEW_SD";

   @RBEntry("Impostare il valore sul nome di una vista parte. Il valore verrà utilizzato per selezionare la vista di default durante la creazione delle parti e per impostare una vista parte per i client che applicano la specifica di configurazione più recente.")
   @RBComment("Set the value to the name of a part view. This value will be used for selecting a default view during the creation of a new part and for setting a part view for clients that apply latest configuration specification.")
   public static final String DEFAULT_CONFIGSPEC_VIEW_LD = "DEFAULT_CONFIGSPEC_VIEW_LD";

   @RBEntry("Ordinamento per le colonne numero e nome nel report distinta base multilivello")
   @RBComment("Name of the preference that controls which sorting algorithm to apply to the number and name columns of the Multi-level BOM report.")
   public static final String SORT_ORDER_FOR_NUMBER_AND_NAME_IN_PSB = "SORT_ORDER_FOR_NUMBER_AND_NAME_IN_PSB";

   @RBEntry("Ordinamento per le colonne numero e nome nel report distinta base multilivello")
   @RBComment("Name of the preference that controls which sorting algorithm to apply to the number and name columns of the Multi-level BOM report.")
   public static final String SORT_ORDER_FOR_NUMBER_AND_NAME_IN_PSB_SD = "SORT_ORDER_FOR_NUMBER_AND_NAME_IN_PSB_SD";

   @RBEntry("L'ordinamento per numero e nome nella tabella della struttura di prodotto è determinato dall'algoritmo selezionato. \"Alfanumerico\" esegue un ordinamento alfanumerico standard dei valori contenuti nella colonna. \"Separa sezioni alfabetiche e numeriche\" ordina i valori in maniera diversa a seconda che siano formattati \"parte alfabetica - parte numerica\" o \"parte numerica - parte alfabetica\". Nel primo caso, i valori vengono ordinati prima alfabeticamente dalla parte alfabetica, quindi numericamente dalla parte numerica. Nel secondo caso, i valori vengono ordinati prima numericamente dalla parte numerica, quindi alfabeticamente dalla parte alfabetica. Se non si utilizza uno dei due formati, ai valori viene applicata una sequenza di ordinamento alfanumerico standard.")
   @RBComment("Name of the preference that controls which sorting algorithm to apply to the number and name columns of the Multi-level BOM report.")
   public static final String SORT_ORDER_FOR_NUMBER_AND_NAME_IN_PSB_LD = "SORT_ORDER_FOR_NUMBER_AND_NAME_IN_PSB_LD";

   @RBEntry("Alfanumerico")
   @RBComment("Value for Sort order for number and name columns in the Product Structure table preference")
   public static final String ALPHANUMERIC_PREF_VALUE = "ALPHANUMERIC_PREF_VALUE";

   @RBEntry("Separa sezioni alfabetiche e numeriche")
   @RBComment("Value for Sort order for number and name columns in the Product Structure table preference")
   public static final String SEPARATE_SECTIONS_PREF_VALUE = "SEPARATE_SECTIONS_PREF_VALUE";

   @RBEntry("Visualizza azione Creazione automatica casi d'impiego")
   @RBComment("This preference determines if the Auto Create Occurrences action should be enabled in the PSB or not.")
   public static final String PREFERENCE_AUTO_CREATE_OCCURRENCES_NAME = "PREFERENCE_AUTO_CREATE_OCCURRENCES_NAME";

   @RBEntry("Controlla la visualizzazione dell'azione Creazione automatica casi d'impiego nella barra degli strumenti della struttura della parte.")
   @RBComment("This preference determines if the Auto Create Occurrences action should be enabled in the PSB or not.")
   public static final String PREFERENCE_AUTO_CREATE_OCCURRENCES_DESC = "PREFERENCE_AUTO_CREATE_OCCURRENCES_DESC";

   @RBEntry("Controlla la visualizzazione dell'azione Creazione automatica casi d'impiego nella barra degli strumenti della struttura della parte.")
   @RBComment("This preference determines if the Auto Create Occurrences action should be enabled in the PSB or not.")
   public static final String PREFERENCE_AUTO_CREATE_OCCURRENCES_LONG_DESC = "PREFERENCE_AUTO_CREATE_OCCURRENCES_LONG_DESC";

   @RBEntry("Allegati alle parti")
   @RBComment("Name of preference that supports for adding attachments on parts.")
   public static final String ATTACHMENTS_TO_PART_NAME = "ATTACHMENTS_TO_PART_NAME";

   @RBEntry("Attiva il supporto per gli allegati alle parti.")
   @RBComment("Short description of preference that supports for adding attachments on parts.")
   public static final String ATTACHMENTS_TO_PART_NAME_SHORT = "ATTACHMENTS_TO_PART_NAME_SHORT";

   @RBEntry("Determina se sia possibile aggiungere allegati alle parti. Il valore di default è No. Se la preferenza è impostata su Sì: <BR/><B>&#8226;</B>  la finestra <B>Nuova parte</B> include il passo <B>Allegato</B>.<BR/><B>&#8226;</B>  nelle pagine delle informazioni sulle parti viene visualizzata la scheda <B>Contenuto</B> con la tabella <B>Allegati</B>.<BR/><B>&#8226;</B>  il menu <B>Personalizza</B> presente nelle schede delle pagine delle informazioni sulle parti include l'opzione <B>Allegati</B>.")
   @RBComment("Long description of preference that supports for adding attachments on parts. The HTML elements be preserved when the entry is localized.")
   public static final String ATTACHMENTS_TO_PART_NAME_LONG = "ATTACHMENTS_TO_PART_NAME_LONG";
}
