/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.part;

import wt.util.resource.*;

@RBUUID("wt.part.partPreferencesRB")
@RBNameException //Grandfathered by conversion
public final class partPreferencesRB extends WTListResourceBundle {
   @RBEntry("Reference Designator Maximum Display Length")
   @RBComment("Name of preference that controls the maximum display length of a reference designator range.")
   public static final String REFERENCE_DESIGNATOR_DISPLAY_LENGTH = "REFERENCE_DESIGNATOR_DISPLAY_LENGTH";

   @RBEntry("Maximum display length of reference designator lists")
   @RBComment("Short description of preference that controls the maximum display length of a reference designator range.")
   public static final String REFERENCE_DESIGNATOR_DISPLAY_LENGTH_SD = "REFERENCE_DESIGNATOR_DISPLAY_LENGTH_SD";

   @RBEntry("When showing occurrences in a product structure, this preference controls the maximum display length of the reference designator list.")
   @RBComment("Long description of preference that controls the maximum display length of a reference designator range.")
   public static final String REFERENCE_DESIGNATOR_DISPLAY_LENGTH_LD = "REFERENCE_DESIGNATOR_DISPLAY_LENGTH_LD";

   @RBEntry("Reference Designator Range Character")
   @RBComment("Name of preference that controls the character that is used to designate a range of reference designators.")
   public static final String REFERENCE_DESIGNATOR_RANGE_CHARACTER = "REFERENCE_DESIGNATOR_RANGE_CHARACTER";

   @RBEntry("Character that is used to designate a range of reference designators.")
   @RBComment("Short description of preference that controls the character that is used to designate a range of reference designators.")
   public static final String REFERENCE_DESIGNATOR_RANGE_CHARACTER_SD = "REFERENCE_DESIGNATOR_RANGE_CHARACTER_SD";

   @RBEntry("When showing occurrences in a product structure, this preference controls the character that is used to designate a range of reference designators.")
   @RBComment("Long description of preference that controls the character that is used to designate a range of reference designators.")
   public static final String REFERENCE_DESIGNATOR_RANGE_CHARACTER_LD = "REFERENCE_DESIGNATOR_RANGE_CHARACTER_LD";

   @RBEntry("Reference Designator Separator Character")
   @RBComment("Name of preference that controls the character that is used to separate individual reference designators in a list of reference designators.")
   public static final String REFERENCE_DESIGNATOR_SEPARATOR_CHARACTER = "REFERENCE_DESIGNATOR_SEPARATOR_CHARACTER";

   @RBEntry("Character that is used to separate individual reference designators in a list of reference designators.")
   @RBComment("Short description of preference that controls the character that is used to separate individual reference designators in a list of reference designators.")
   public static final String REFERENCE_DESIGNATOR_SEPARATOR_CHARACTER_SD = "REFERENCE_DESIGNATOR_SEPARATOR_CHARACTER_SD";

   @RBEntry("When showing occurrences in a product structure, this preference controls the character that is used to separate individual reference designators in a list of reference designators.")
   @RBComment("Long description of preference that controls the character that is used to separate individual reference designators in a list of reference designators.")
   public static final String REFERENCE_DESIGNATOR_SEPARATOR_CHARACTER_LD = "REFERENCE_DESIGNATOR_SEPARATOR_CHARACTER_LD";

   @RBEntry("Reference Designator Begin Escape Character")
   @RBComment("Name of preference that controls the character that is used at the beginning of a reference designator to allow entry of special characters (Reference Designator Range Character and Reference Designator Sequence Character) as normal data input rather than as special characters.")
   public static final String REFERENCE_DESIGNATOR_START_ESCAPE_CHARACTER = "REFERENCE_DESIGNATOR_START_ESCAPE_CHARACTER";

   @RBEntry("Character that is used at the beginning of a reference designator to allow entry of special characters (Reference Designator Range Character and Reference Designator Sequence Character) as normal data input rather than as special characters.")
   @RBComment("Short description of preference that controls the character that is used at the beginning of a reference designator to allow entry of special characters (Reference Designator Range Character and Reference Designator Sequence Character) as normal data input rather than as special characters.")
   public static final String REFERENCE_DESIGNATOR_START_ESCAPE_CHARACTER_SD = "REFERENCE_DESIGNATOR_START_ESCAPE_CHARACTER_SD";

   @RBEntry("When showing occurrences in a product structure, this preference controls the character that is used at the beginning of a reference designator to allow entry of special characters (Reference Designator Range Character and Reference Designator Sequence Character) as normal data input rather than as special characters.")
   @RBComment("Long description of preference that controls the character that is used at the beginning of a reference designator to allow entry of special characters (Reference Designator Range Character and Reference Designator Sequence Character) as normal data input rather than as special characters.")
   public static final String REFERENCE_DESIGNATOR_START_ESCAPE_CHARACTER_LD = "REFERENCE_DESIGNATOR_START_ESCAPE_CHARACTER_LD";

   @RBEntry("Reference Designator End Escape Character")
   @RBComment("Name of preference that controls the character that is used at the end of a reference designator to allow entry of special characters (Reference Designator Range Character and Reference Designator Sequence Character) as normal data input rather than as special characters.")
   public static final String REFERENCE_DESIGNATOR_END_ESCAPE_CHARACTER = "REFERENCE_DESIGNATOR_END_ESCAPE_CHARACTER";

   @RBEntry("Character that is used at the end of a reference designator to allow entry of special characters (Reference Designator Range Character and Reference Designator Sequence Character) as normal data input rather than as special characters.")
   @RBComment("Short description of preference that controls the character that is used at the end of a reference designator to allow entry of special characters (Reference Designator Range Character and Reference Designator Sequence Character) as normal data input rather than as special characters.")
   public static final String REFERENCE_DESIGNATOR_END_ESCAPE_CHARACTER_SD = "REFERENCE_DESIGNATOR_END_ESCAPE_CHARACTER_SD";

   @RBEntry("When showing occurrences in a product structure, this preference controls the character that is used at the end of a reference designator to allow entry of special characters (Reference Designator Range Character and Reference Designator Sequence Character) as normal data input rather than as special characters.")
   @RBComment("Long description of preference that controls the character that is used at the end of a reference designator to allow entry of special characters (Reference Designator Range Character and Reference Designator Sequence Character) as normal data input rather than as special characters.")
   public static final String REFERENCE_DESIGNATOR_END_ESCAPE_CHARACTER_LD = "REFERENCE_DESIGNATOR_END_ESCAPE_CHARACTER_LD";

   @RBEntry("Part Management")
   @RBComment("Name of the part management category in preferences.")
   public static final String PART_MANAGEMENT_CATEGORY = "PART_MANAGEMENT_CATEGORY";

   @RBEntry("Reference Designators")
   @RBComment("Name of the reference designator sub-category in preferences.")
   public static final String REFERENCE_DESIGNATOR_CATEGORY = "REFERENCE_DESIGNATOR_CATEGORY";

   @RBEntry("Part to Document Association Logic")
   public static final String PREFERENCE_RELATED_DOCUMENT_PMD_METHOD_NAME = "PREFERENCE_RELATED_DOCUMENT_PMD_METHOD_NAME";

   @RBEntry("Controls part to document association logic. A value of false restricts associations based on link type and document soft type and prevents associating multiple versions of a document to a part. A value of true removes those restrictions.")
   public static final String PREFERENCE_RELATED_DOCUMENT_PMD_METHOD_SHORT_DESC = "PREFERENCE_RELATED_DOCUMENT_PMD_METHOD_SHORT_DESC";

   @RBEntry(" Controls the logic for how documents are related to and displayed for a part. A value of false uses the PDMLink logic and a value of true uses the Windchill PDM logic. The PDMLink logic enforces that a document of type Reference document can only be associated to the part as a References document and all other document types can only be associated to a part as a Described By document. The Windchill PDM logic allows References and Described By relationships to be created from part to document regardless of the document type. See the References and Described By tables on the part information page for a description of the behavior of References documents and Described By documents.  In addition, the PDMLink logic enforces that a part be related only to one version of a document; whereas the Windchill PDM logic allows a part to be associated to versions of the same document.")
   public static final String PREFERENCE_RELATED_DOCUMENT_PMD_METHOD_LONG_DESC = "PREFERENCE_RELATED_DOCUMENT_PMD_METHOD_LONG_DESC";

   @RBEntry("List of comma separated life cycle states of documents used to display associated Reference documents to part.")
   public static final String PREFERENCE_RELATED_DOCUMENT_RELEASED_STATE_NAME = "PREFERENCE_RELATED_DOCUMENT_RELEASED_STATE_NAME";

   @RBEntry("List of comma separated life cycle states of documents used to display associated Reference documents to part.")
   public static final String PREFERENCE_RELATED_DOCUMENT_RELEASED_STATE_SHORT_DESC = "PREFERENCE_RELATED_DOCUMENT_RELEASED_STATE_SHORT_DESC";

   @RBEntry("The life cycle states listed in this preference are used to select the document version shown in the reference documents table on the part information page.  The value of the preference is case sensitive and should exactly match the corresponding constant defined in wt\\lifecyclw\\StateRB.rbInfo file.")
   public static final String PREFERENCE_RELATED_DOCUMENT_RELEASED_STATE_LONG_DESC = "PREFERENCE_RELATED_DOCUMENT_RELEASED_STATE_LONG_DESC";

   @RBEntry("Enable quantity and reference designators for substitute parts")
   public static final String QUANTITY_AND_REFERENCE_DESIGNATOR_FOR_SUBSTITUTE_PART = "QUANTITY_AND_REFERENCE_DESIGNATOR_FOR_SUBSTITUTE_PART";

   @RBEntry("When set, users can define quantity and reference designators for substitute parts. These attributes will be used when performing replace part.")
   public static final String QUANTITY_AND_REFERENCE_DESIGNATOR_FOR_SUBSTITUTE_PART_SHORT_DESC = "QUANTITY_AND_REFERENCE_DESIGNATOR_FOR_SUBSTITUTE_PART_SHORT_DESC";

   @RBEntry("When set, users can define quantity and reference designators for substitute parts. These attributes will be used when performing replace part.")
   public static final String QUANTITY_AND_REFERENCE_DESIGNATOR_FOR_SUBSTITUTE_PART_LONG_DESC = "QUANTITY_AND_REFERENCE_DESIGNATOR_FOR_SUBSTITUTE_PART_LONG_DESC";

   @RBEntry("Default WTPart type")
   @RBComment("Name of preference that allows users to configure the default type of WTPart to use.")
   public static final String ON_DEMAND_DEFAULT_TYPE_WTPART = "ON_DEMAND_DEFAULT_TYPE_WTPART";

   @RBEntry("Default WTPart type used during the build process")
   @RBComment("Short description of preference that controls the default WTPart type to use during the build process.")
   public static final String ON_DEMAND_DEFAULT_TYPE_WTPART_SD = "ON_DEMAND_DEFAULT_TYPE_WTPART_SD";

   @RBEntry("Default WTPart type used during the build process. Specified in the external form, for example, WCTYPE|wt.part.WTPart|com.example.MyType.")
   @RBComment("Long description of preference that controls the default WTPart type to use during the build process.")
   public static final String ON_DEMAND_DEFAULT_TYPE_WTPART_LD = "ON_DEMAND_DEFAULT_TYPE_WTPART_LD";

   @RBEntry("Default WTPartUsageLink type")
   @RBComment("Name of preference that allows users to configure the default type of WTPartUsageLink to use.")
   public static final String ON_DEMAND_DEFAULT_TYPE_WTPART_USAGE_LINK = "ON_DEMAND_DEFAULT_TYPE_WTPART_USAGE_LINK";

   @RBEntry("Default WTPartUsageLink type used during the build process")
   @RBComment("Short description of preference that controls the default WTPartUsageLink type to use during the build process.")
   public static final String ON_DEMAND_DEFAULT_TYPE_WTPART_USAGE_LINK_SD = "ON_DEMAND_DEFAULT_TYPE_WTPART_USAGE_LINK_SD";

   @RBEntry("Default WTPartUsageLink type used during the build process. Specified in the external form, for example, WCTYPE|wt.part.WTPartUsageLink|com.example.MyUsageLinkType.")
   @RBComment("Long description of preference that controls the default WTPartUsageLink type to use during the build process.")
   public static final String ON_DEMAND_DEFAULT_TYPE_WTPART_USAGE_LINK_LD = "ON_DEMAND_DEFAULT_TYPE_WTPART_USAGE_LINK_LD";

   @RBEntry("Default WTPart type")
   @RBComment("Name of preference that allows users to configure the root type of WTPart to use.")
   public static final String ON_DEMAND_ROOT_TYPE_WTPART = "ON_DEMAND_ROOT_TYPE_WTPART";

   @RBEntry("Root WTPart type used during the build process")
   @RBComment("Short description of preference that controls the root WTPart type to use during the build process.")
   public static final String ON_DEMAND_ROOT_TYPE_WTPART_SD = "ON_DEMAND_ROOT_TYPE_WTPART_SD";

   @RBEntry("Root WTPart type used during the build process. Specified in the external form, for example, WCTYPE|wt.part.WTPart|com.example.MyType.")
   @RBComment("Long description of preference that controls the root WTPart type to use during the build process.")
   public static final String ON_DEMAND_ROOT_TYPE_WTPART_LD = "ON_DEMAND_ROOT_TYPE_WTPART_LD";

   @RBEntry("Root WTPartUsageLink type")
   @RBComment("Name of preference that allows users to configure the root type of WTPartUsageLink to use.")
   public static final String ON_DEMAND_ROOT_TYPE_WTPART_USAGE_LINK = "ON_DEMAND_ROOT_TYPE_WTPART_USAGE_LINK";

   @RBEntry("Root WTPartUsageLink type used during the build process")
   @RBComment("Short description of preference that controls the root WTPartUsageLink type to use during the build process.")
   public static final String ON_DEMAND_ROOT_TYPE_WTPART_USAGE_LINK_SD = "ON_DEMAND_ROOT_TYPE_WTPART_USAGE_LINK_SD";

   @RBEntry("Root WTPartUsageLink type used during the build process. Specified in the external form, for example, WCTYPE|wt.part.WTPartUsageLink|com.example.MyUsageLinkType.")
   @RBComment("Long description of preference that controls the root WTPartUsageLink type to use during the build process.")
   public static final String ON_DEMAND_ROOT_TYPE_WTPART_USAGE_LINK_LD = "ON_DEMAND_ROOT_TYPE_WTPART_USAGE_LINK_LD";

   @RBEntry("Default Part View")
   @RBComment("The product structure view specified below will be used by the default configuration specification to expand part or product versions within the Product Structure Explorer (PSE) or when viewing the structure in HTML.")
   public static final String DEFAULT_CONFIGSPEC_VIEW = "DEFAULT_CONFIGSPEC_VIEW";

   @RBEntry("The name of a part view")
   @RBComment("The product structure view specified below will be used by the default configuration specification to expand part or product versions within the Product Structure Explorer (PSE) or when viewing the structure in HTML.")
   public static final String DEFAULT_CONFIGSPEC_VIEW_SD = "DEFAULT_CONFIGSPEC_VIEW_SD";

   @RBEntry("Set the value to the name of a part view. This value will be used for selecting a default view during the creation of a new part and for setting a part view for clients that apply latest configuration specification.")
   @RBComment("Set the value to the name of a part view. This value will be used for selecting a default view during the creation of a new part and for setting a part view for clients that apply latest configuration specification.")
   public static final String DEFAULT_CONFIGSPEC_VIEW_LD = "DEFAULT_CONFIGSPEC_VIEW_LD";

   @RBEntry("Sort order for number and name columns in the Multi-level BOM report")
   @RBComment("Name of the preference that controls which sorting algorithm to apply to the number and name columns of the Multi-level BOM report.")
   public static final String SORT_ORDER_FOR_NUMBER_AND_NAME_IN_PSB = "SORT_ORDER_FOR_NUMBER_AND_NAME_IN_PSB";

   @RBEntry("Sort order for number and name columns in the Multi-level BOM report")
   @RBComment("Name of the preference that controls which sorting algorithm to apply to the number and name columns of the Multi-level BOM report.")
   public static final String SORT_ORDER_FOR_NUMBER_AND_NAME_IN_PSB_SD = "SORT_ORDER_FOR_NUMBER_AND_NAME_IN_PSB_SD";

   @RBEntry("Sort order for number and name in the Product Structure table is determined by the algorithm selected.  \"Alphanumeric\" does a standard alphanumeric sort of the values in the column.  \"Separate alpha and numeric sections\" sorts the values differently if they are formatted \"Alpha-portion Numeric-portion\" or \"Numeric-portion Alpha-portion\".  In the first case, the values are sorted first alphabetically by Alpha-portion and then numerically by the Numeric-portion.  In the second case, the values are sorted first numerically by the Numeric-portion and then alphabetically by Alpha-portion.  If the values do not fit one of these two formats, a standard alphanumeric sort is applied.")
   @RBComment("Name of the preference that controls which sorting algorithm to apply to the number and name columns of the Multi-level BOM report.")
   public static final String SORT_ORDER_FOR_NUMBER_AND_NAME_IN_PSB_LD = "SORT_ORDER_FOR_NUMBER_AND_NAME_IN_PSB_LD";

   @RBEntry("Alphanumeric")
   @RBComment("Value for Sort order for number and name columns in the Product Structure table preference")
   public static final String ALPHANUMERIC_PREF_VALUE = "ALPHANUMERIC_PREF_VALUE";

   @RBEntry("Separate alpha and numeric sections")
   @RBComment("Value for Sort order for number and name columns in the Product Structure table preference")
   public static final String SEPARATE_SECTIONS_PREF_VALUE = "SEPARATE_SECTIONS_PREF_VALUE";

   @RBEntry("Display Auto Create Occurrences action")
   @RBComment("This preference determines if the Auto Create Occurrences action should be enabled in the PSB or not.")
   public static final String PREFERENCE_AUTO_CREATE_OCCURRENCES_NAME = "PREFERENCE_AUTO_CREATE_OCCURRENCES_NAME";

   @RBEntry("Controls the display of Auto Create Occurrences action within the part structure toolbar actions.")
   @RBComment("This preference determines if the Auto Create Occurrences action should be enabled in the PSB or not.")
   public static final String PREFERENCE_AUTO_CREATE_OCCURRENCES_DESC = "PREFERENCE_AUTO_CREATE_OCCURRENCES_DESC";

   @RBEntry("Controls the display of Auto Create Occurrences action within the part structure toolbar actions.")
   @RBComment("This preference determines if the Auto Create Occurrences action should be enabled in the PSB or not.")
   public static final String PREFERENCE_AUTO_CREATE_OCCURRENCES_LONG_DESC = "PREFERENCE_AUTO_CREATE_OCCURRENCES_LONG_DESC";

   @RBEntry("Attachments on parts")
   @RBComment("Name of preference that supports for adding attachments on parts.")
   public static final String ATTACHMENTS_TO_PART_NAME = "ATTACHMENTS_TO_PART_NAME";

   @RBEntry("Enable support for attachments on parts.")
   @RBComment("Short description of preference that supports for adding attachments on parts.")
   public static final String ATTACHMENTS_TO_PART_NAME_SHORT = "ATTACHMENTS_TO_PART_NAME_SHORT";

   @RBEntry("Determines whether or not attachments can be added to parts. The default value is No. If the preference is set to Yes: <BR/><B>&#8226;  New Part</B> window includes the <B>Attachment</B> step.<BR/><B>&#8226;</B>  Part information pages display the <B>Content</B> tab with the <B>Attachments</B> table.<BR/><B>&#8226;  Customize</B> menu on part information page tabs includes <B>Attachments</B>.")
   @RBComment("Long description of preference that supports for adding attachments on parts. The HTML elements be preserved when the entry is localized.")
   public static final String ATTACHMENTS_TO_PART_NAME_LONG = "ATTACHMENTS_TO_PART_NAME_LONG";
}
