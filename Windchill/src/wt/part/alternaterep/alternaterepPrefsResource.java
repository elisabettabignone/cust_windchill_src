/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.part.alternaterep;

import wt.util.resource.*;

@RBUUID("wt.part.alternaterep.alternaterepPrefsResource")
public final class alternaterepPrefsResource extends WTListResourceBundle {
   @RBEntry("Configuration Context Preferences")
   public static final String PRIVATE_CONSTANT_0 = "PREFS_TITLE";

   @RBEntry("Enable Spatial Expansion Criteria")
   public static final String PRIVATE_CONSTANT_1 = "ENABLE_SPATIAL_FILTER";

   @RBEntry("Enables the ability to filter a part or CAD document structure to include parts that are within a specified volume. The allowable volume definitions include Box, Sphere, and Proximity from a selected part. Visualization images must be generated from CAD geometry for this form of filtering to be useful. Set this preference to Yes to enable volumetric filtering, set to No to disable volumetric filtering.")
   @RBComment("Description of Enable Spatial Expansion Criteria preference")
   public static final String PRIVATE_CONSTANT_2 = "ENABLE_SPATIAL_FILTER_DESC";

   @RBEntry("Enable Attribute Filter")
   @RBComment("Preference in the Preference Manager that controls whether or not the Attributes tab is shown in the PSB, PAR, or CAR GWT apps")
   public static final String SHOW_ATTRIBUTE_FILTER = "SHOW_ATTRIBUTE_FILTER";

   @RBEntry("Enables the ability to filter a part or CAD document structure to include parts that match certain attribute criteria. Set this preference to Yes to enable attribute filtering, set to No to disable attribute filtering.")
   @RBComment("Long description for the Enable Attribute Filter preference ")
   public static final String SHOW_ATTRIBUTE_FILTER_DESC = "SHOW_ATTRIBUTE_FILTER_DESC";
}
