/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.part.alternaterep.service;

import wt.util.resource.*;

@RBUUID("wt.part.alternaterep.service.alternateRepServiceResource")
public final class alternateRepServiceResource extends WTListResourceBundle {
   @RBEntry("You cannot add or remove a part when the Configuration Context is not checked out.")
   public static final String CANNOT_MODIFY_PAR_SECTION = "0";

   @RBEntry("WTPartAlternateRepMemberLink must have its getWTPartAlternateRep().getDesignAssemblyMaster() == getMember().getContext()")
   public static final String INVALID_DESIGN_ASSEMBLY = "1";

   @RBEntry("Cannot create the rule because the Configuration Context is not a working copy.")
   public static final String CANNOT_CREATE_RULE_NON_WORKING = "CANNOT_CREATE_RULE_NON_WORKING";

    @RBEntry("Cannot create the result rule because the Configuration Context is not a working copy.")
    public static final String CANNOT_CREATE_RESULT_RULE_NON_WORKING = "CANNOT_CREATE_RESULT_RULE_NON_WORKING";

   @RBEntry("Cannot edit the expansion criteria because the Configuration Context is not a working copy.")
   public static final String CANNOT_EDIT_EC_NON_WORKING = "CANNOT_EDIT_EC_NON_WORKING";

   @RBEntry("Cannot edit the expansion criteria because the Configuration Context is checked out to someone else.")
   public static final String CANNOT_EDIT_EC_CHECKED_OUT_TO_SOME_OTHER_USER = "CANNOT_EDIT_EC_CHECKED_OUT_TO_SOME_OTHER_USER";
   
   @RBEntry("Cannot delete the selected part because it is a context part for one or more Configuration Contexts which you are not allowed to delete.")
   public static final String CANNOT_DELETE_PART_ACCESS_TO_PARS_DENIED = "CANNOT_DELETE_PART_ACCESS_TO_PARS_DENIED";
   
   @RBEntry("Cannot delete the selected part because it is a context part for the following Configuration Contexts which you are not allowed to delete: \n\n{0}")
   public static final String CANNOT_DELETE_PART_ACCESS_TO_FOLLOWING_PARS_DENIED = "CANNOT_DELETE_PART_ACCESS_TO_FOLLOWING_PARS_DENIED";

    @RBEntry("Cannot link Configuration Context to more than one Design Context.")
    public static final String CANNOT_LINK_CONFIG_CONTEXT_TO_MULTIPLE_DESIGN_CONTEXT = "CANNOT_LINK_CONFIG_CONTEXT_TO_MULTIPLE_DESIGN_CONTEXT";
    
    @RBEntry("Cannot create the Configuration Context because there is one or more cases of circular dependency in the structure.")
    public static final String CANNOT_CREATE_CONFIG_CONTEXT_CIRCULAR_DEPENDENCIES = "CANNOT_CREATE_CONFIG_CONTEXT_CYCLICAL_DEPENDENCIES";
}
