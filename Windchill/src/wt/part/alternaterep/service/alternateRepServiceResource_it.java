/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.part.alternaterep.service;

import wt.util.resource.*;

@RBUUID("wt.part.alternaterep.service.alternateRepServiceResource")
public final class alternateRepServiceResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile aggiungere o rimuovere una parte quando il contesto di configurazione non è stato sottoposto a Check-Out.")
   public static final String CANNOT_MODIFY_PAR_SECTION = "0";

   @RBEntry("WTPartAlternateRepMemberLink deve avere getWTPartAlternateRep().getDesignAssemblyMaster() == getMember().getContext()")
   public static final String INVALID_DESIGN_ASSEMBLY = "1";

   @RBEntry("Impossibile creare la regola. Il contesto di configurazione non è una copia in modifica.")
   public static final String CANNOT_CREATE_RULE_NON_WORKING = "CANNOT_CREATE_RULE_NON_WORKING";

    @RBEntry("Impossibile creare la regola dei risultati. Il contesto di configurazione non è una copia in modifica.")
    public static final String CANNOT_CREATE_RESULT_RULE_NON_WORKING = "CANNOT_CREATE_RESULT_RULE_NON_WORKING";

   @RBEntry("Impossibile modificare i criteri di espansione. Il contesto di configurazione non è una copia in modifica.")
   public static final String CANNOT_EDIT_EC_NON_WORKING = "CANNOT_EDIT_EC_NON_WORKING";

   @RBEntry("Impossibile modificare i criteri di espansione. Il contesto di configurazione è sottoposto a Check-Out da un altro utente.")
   public static final String CANNOT_EDIT_EC_CHECKED_OUT_TO_SOME_OTHER_USER = "CANNOT_EDIT_EC_CHECKED_OUT_TO_SOME_OTHER_USER";
   
   @RBEntry("Impossibile eliminare la parte selezionata. Si tratta di una parte di contesto di uno o più contesti di configurazione di cui non si dispone dei diritti per l'eliminazione.")
   public static final String CANNOT_DELETE_PART_ACCESS_TO_PARS_DENIED = "CANNOT_DELETE_PART_ACCESS_TO_PARS_DENIED";
   
   @RBEntry("Impossibile eliminare la parte selezionata. Si tratta di una parte di contesto dei seguenti contesti di configurazione di cui non si dispone dei diritti per l'eliminazione: \n\n{0}")
   public static final String CANNOT_DELETE_PART_ACCESS_TO_FOLLOWING_PARS_DENIED = "CANNOT_DELETE_PART_ACCESS_TO_FOLLOWING_PARS_DENIED";

    @RBEntry("Impossibile collegare il contesto di configurazione a più di un contesto di progettazione.")
    public static final String CANNOT_LINK_CONFIG_CONTEXT_TO_MULTIPLE_DESIGN_CONTEXT = "CANNOT_LINK_CONFIG_CONTEXT_TO_MULTIPLE_DESIGN_CONTEXT";
    
    @RBEntry("Impossibile creare il contesto di configurazione a causa di uno o più casi di dipendenza circolare nella struttura.")
    public static final String CANNOT_CREATE_CONFIG_CONTEXT_CIRCULAR_DEPENDENCIES = "CANNOT_CREATE_CONFIG_CONTEXT_CYCLICAL_DEPENDENCIES";
}
