/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.part.alternaterep;

import wt.util.resource.*;

@RBUUID("wt.part.alternaterep.alternaterepPrefsResource")
public final class alternaterepPrefsResource_it extends WTListResourceBundle {
   @RBEntry("Preferenze contesto configurazione")
   public static final String PRIVATE_CONSTANT_0 = "PREFS_TITLE";

   @RBEntry("Attiva criteri di espansione spaziale")
   public static final String PRIVATE_CONSTANT_1 = "ENABLE_SPATIAL_FILTER";

   @RBEntry("Attiva la possibilità di filtrare una struttura di parti o di documenti CAD in modo da includere le parti all'interno di un volume specificato. Le definizioni di volume consentite includono parallelepipedo, sfera e prossimità di una parte selezionata. Questo tipo di filtro è utile se le immagini di Visualization sono generate dalla geometria CAD. Per attivare il filtro volumetrico, impostare questa preferenza su Sì. Per disattivarlo, impostarla su No.")
   @RBComment("Description of Enable Spatial Expansion Criteria preference")
   public static final String PRIVATE_CONSTANT_2 = "ENABLE_SPATIAL_FILTER_DESC";

   @RBEntry("Attiva filtro attributi")
   @RBComment("Preference in the Preference Manager that controls whether or not the Attributes tab is shown in the PSB, PAR, or CAR GWT apps")
   public static final String SHOW_ATTRIBUTE_FILTER = "SHOW_ATTRIBUTE_FILTER";

   @RBEntry("Consente di filtrare una struttura parte o documento CAD per includere le parti che corrispondono a determinati criteri di attributi. Impostare la preferenza su Sì per attivare l'applicazione filtri per gli attributi o su No per disattivarla.")
   @RBComment("Long description for the Enable Attribute Filter preference ")
   public static final String SHOW_ATTRIBUTE_FILTER_DESC = "SHOW_ATTRIBUTE_FILTER_DESC";
}
