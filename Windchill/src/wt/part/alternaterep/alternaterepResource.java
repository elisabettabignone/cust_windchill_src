/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.part.alternaterep;

import wt.util.resource.*;

@RBUUID("wt.part.alternaterep.alternaterepResource")
public final class alternaterepResource extends WTListResourceBundle {

   @RBEntry("A required parameter has not been specified or is invalid: {0}")
   @RBComment("This is a runtime error that occurs it the PARToCARController is called and does not contain a needed parameter, or the value of the parameter is invalid.  This shouldn't be displayed in normal operation, and would only occur if the PARToCARController is called with missing or invalid information.")
   @RBArgComment0("This will be the name of the parameter which was not specified or is invalid.  An example would be \"A required parameter has not been specified or is invalid: OPERATION\".")
   public static final String ERROR_PARAM_MISSING_OR_INVALID = "ERROR_PARAM_MISSING_OR_INVALID";

   @RBEntry("Configuration Context Expansion Criteria specifies no version of context part.")
   @RBComment("This is a runtime error that occurs it the PARToCARController is called and cannot find the design assembly associated with the WTPartAlternateRep.  This shouldn't be displayed in normal operation, and would only occur if the PARToCARController is called with a WTPartAlternateRep that has no associated design assembly.")
   public static final String ERROR_NO_ITERATIONS_OF_PAR_CONTEXT_PART = "ERROR_NO_ITERATIONS_OF_PAR_CONTEXT_PART";

   @RBEntry("Configuration Context Expansion Criteria specifies multiple versions of context part.")
   @RBComment("This is a runtime error that occurs it the PARToCARController is called and finds multiple iterations the design assembly associated with the WTPartAlternateRep.  This shouldn't be displayed in normal operation, and would only occur if the PARToCARController is called with a WTPartAlternateRep that has incorrect expansion criteria.")
   public static final String ERROR_MULTIPLE_ITERATIONS_OF_PAR_CONTEXT_PART = "ERROR_MULTIPLE_ITERATIONS_OF_PAR_CONTEXT_PART";

   @RBEntry("Design Context not found: {0}.")
   @RBComment("This is a runtime error that occurs if the PARToCARController is called to updated a design context, but given context file cannot be located.  This shouldn't be displayed in normal operation, and would only occur if the PARToCARController is called with missing or invalid information.")
   @RBArgComment0("This will be the name of the ESR which was not found.  An example would be \"ESR not found: offroad_bike.asm\".")
   public static final String ERROR_ESR_NOT_FOUND= "ERROR_ESR_NOT_FOUND";

   @RBEntry("Failed to create Design Context from Configuration Context.  Only Standard and Effectivity Configuration Specification types are supported.")
   @RBComment("This is a runtime error that occurs if an attempt is made to create a Design Context from a Configuration Context that uses a Configuration Specification that is not a Standard or Effectivity Configuration Specification")
   public static final String ERROR_UNSUPPORTED_CONFIGSPEC = "ERROR_UNSUPPORTED_CONFIGSPEC";

   @RBEntry("The Design Context was not created or updated because there is either no CAD data with an owner association to the top level Part of the right authoring application, or there was no version of the associated CAD Document resolved by the navigation criteria.")
   @RBComment("This is a runtime error that occurs if an attempt is made to create a Design Context from a Configuration Context and the design assembly associated to the Configuration Context isn't associated to any CAD Documents.")
   public static final String ERROR_NO_RELATED_CAD = "ERROR_NO_RELATED_CAD";

   @RBEntry("An occurrence of part \"{0}\" has no associated CAD occurrence.")
   @RBComment("There is a path occurrences that has not associated CAD occurrence.")
   public static final String WARN_DSG_SOURCE_EDGE_WITHOUT_TARGET = "WARN_DSG_SOURCE_EDGE_WITHOUT_TARGET";

   @RBEntry("Unable to translate rules on Part: {0}.  The Part occurrence has no valid associated CAD occurrence in the CAD Structure.  The Design Context has included: {1}. ")
   @RBComment("An include rule exists on a part path occurrence, but the path to the associated CAD occurrence is incomplete.")
   public static final String WARN_SUMMARY_INCLUDE_RULE_CREATED = "WARN_SUMMARY_INCLUDE_RULE_CREATED";

   @RBEntry("Unable to translate rules on Part: {0}.  The Part occurrence has no valid associated CAD occurrence in the CAD Structure.  The Design Context has included as assembly only: {1}. ")
   @RBComment("An include rule exists on a part path occurrence, but the path to the associated CAD occurrence is incomplete.")
   public static final String WARN_SUMMARY_INCLUDE_THIS_ONLY_RULE_CREATED = "WARN_SUMMARY_INCLUDE_THIS_ONLY_RULE_CREATED";

   @RBEntry("Configuration Context to Design Context")
   @RBComment("Name of task run to convert PAR to CAR.")
   public static final String PARToCARTaskName = "PARToCARTaskName";

   @RBEntry("The Design Context template was not created in a valid version of Creo. Choose a Design Context template created in Creo 1.0 or later.")
   @RBComment("This error occurs if an attempt is made to create a Design Context using a template version prior to Creo 1.0")
   public static final String ERROR_UNSUPPORTED_TEMPLATE_VERSION = "ERROR_UNSUPPORTED_TEMPLATE_VERSION";
}
