/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.rn;

import wt.util.resource.*;

@RBUUID("wt.rn.rnResource")
public final class rnResource extends WTListResourceBundle {
   @RBEntry("\"{0}\" is not a valid URL.")
   public static final String MALFORMED_URL = "0";
}
