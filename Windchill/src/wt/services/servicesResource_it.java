/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.services;

import wt.util.resource.*;

@RBUUID("wt.services.servicesResource")
public final class servicesResource_it extends WTListResourceBundle {
   @RBEntry("Ramo già esistente: {0}")
   @RBArgComment0(" refers to the branch")
   public static final String BRANCH_EXISTS = "2";

   @RBEntry("Impossibile caricare il file della classe {0} per la gestione/il servizio {1} a causa di {2}")
   @RBArgComment0(" refers to the class file")
   @RBArgComment1(" refers to the manager/service")
   @RBArgComment2(" refers to the reason")
   public static final String CLASS_LOAD_ERROR = "3";

   @RBEntry("La creazione di tutti i gestori non è riuscita")
   public static final String CREATE_MANAGER_ERROR = "4";

   @RBEntry("Impossibile avviare tutti i gestori.")
   public static final String START_MANAGER_ERROR = "5";

   @RBEntry("L'avvio di tutti i gestori non è riuscito.")
   public static final String START_MANAGER_FAIL = "6";

   @RBEntry("Impossibile spegnere tutti i gestori")
   public static final String SHUTDOWN_FAIL = "7";

   @RBEntry("Tutti i manager sono stati avviati.")
   public static final String ALL_MANAGERS_STARTED = "8";

   @RBEntry("Tutti i manager sono stati disattivati.")
   public static final String ALL_MANAGERS_SHUTDOWN = "9";

   @RBEntry("Impossibile inizializzare il servizio StandardManagerService.")
   public static final String UNABLE_TO_INITIALIZE_MANAGER_SERVICE = "10";

   @RBEntry("Impossibile caricare il file: \"{0}\"")
   @RBArgComment0("The file that couldn't be loaded")
   public static final String UNABLE_TO_LOAD_FILE = "11";

   @RBEntry("Impossibile chiudere il file: \"{0}\"")
   @RBArgComment0("The file that couldn't be closed")
   public static final String UNABLE_TO_CLOSE_FILE = "12";

   @RBEntry("La voce del servizio \"{0}\" esisteva già nel gruppo \"{1}\". Impossibile aggiungere una voce diversa per lo stesso servizio al gruppo \"{2}\".")
   @RBComment("If a service has already been loaded as a member of one group, it cannot be loaded again as a member of a different group.")
   @RBArgComment0("The name of the service in question.")
   @RBArgComment1("The name of the group the service is already in. ")
   @RBArgComment2("The name of the new group that the service can't be added to.")
   public static final String ENTRY_EXISTS_IN_OTHER_GROUP = "13";

   @RBEntry("Impossibile creare il gruppo: \"{0}\"")
   @RBArgComment0("The name of the group that couldn't be created.")
   public static final String UNABLE_TO_CREATE_GROUP = "14";

   @RBEntry("Impossibile creare repository del manager.")
   @RBComment("When the manager repository could not be created.")
   public static final String UNABLE_TO_CREATE_REPOSITORY = "15";

   @RBEntry("Formato imprevisto per la voce dei servizi: \"{0}\"")
   @RBComment("When a services entry did not have the expected format")
   @RBArgComment0("The offending service entry.")
   public static final String UNEXPECTED_FORMAT = "16";

   @RBEntry("Impossibile trovare la classe manager nella voce: \"{0}\"")
   @RBComment("When the manager class specified in the service startup entry couldn't be loaded.")
   @RBArgComment0("The offending service startup entry.")
   public static final String UNABLE_TO_FIND_MANAGER_CLASS = "17";

   @RBEntry("Impossibile trovare la classe di implementazione del manager nella voce: \"{0}\"")
   @RBComment("When the manager implementation class specified in the service startup entry couldn't be loaded.")
   @RBArgComment0("The offending service startup entry.")
   public static final String UNABLE_TO_FIND_MANAGER_IMPLEMENTATION = "18";

   @RBEntry("Impossibile creare il manager: \"{0}\" con la classe di implementazione: \"{1}\"")
   @RBComment("When a manager can't be instantiated with the given implementation class")
   @RBArgComment0("The classname of the manager interface")
   @RBArgComment1("The classname of the manager implementation")
   public static final String UNABLE_TO_CREATE_MANAGER = "19";

   @RBEntry("Avviato il servizio StandardManagerService")
   @RBComment("Printed to the MethodServer standard out after all services have started up.")
   public static final String STANDARD_MANAGER_SERVICE_STARTED = "20";

   @RBEntry("Impossibile avviare il manager: \"{0}\" con la classe di implementazione: \"{1}\"")
   @RBComment("When a manager can't be started using the given implementation class")
   @RBArgComment0("The classname of the manager interface")
   @RBArgComment1("The classname of the manager implementation")
   public static final String UNABLE_TO_START_MANAGER = "21";
}
