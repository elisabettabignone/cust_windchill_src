/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.services.ac;

import wt.util.resource.*;

@RBUUID("wt.services.ac.acResource")
public final class acResource extends WTListResourceBundle {
   @RBEntry("Unable to create ACServices implementation for context name \"{0}\"")
   @RBComment("When no implementation class can be instantiated for the supplied context name")
   @RBArgComment0("The name of the context for which an ACServices instance was requested")
   public static final String UNABLE_TO_CREATE_ACSERVICES_CLASS = "0";

   @RBEntry("Unable to create a loader for context \"{0}\"")
   @RBComment("When a ServiceLoader instance can't be instantiated for the supplied context")
   @RBArgComment0("The name of the context for which a loader was requested ")
   public static final String UNABLE_TO_CREATE_SERVICES_LOADER = "1";
}
