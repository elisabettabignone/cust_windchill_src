package wt.services.ac.impl;

import wt.util.resource.*;

public final class implResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile creare un fornitore di servizio per il nome di servizio: \"{0}\"")
   @RBComment("When a service provider can't be instantiated for the passed in service name")
   @RBArgComment0("The name of the service for which an implementation was uncessfully instantiated")
   public static final String UNABLE_TO_CREATE_PROVIDER = "0";

   @RBEntry("Impossibile caricare le proprietà")
   @RBComment("When properties can't be loaded")
   public static final String UNABLE_TO_LOAD_PROPERTIES = "1";

   @RBEntry("Impossibile trovare la classe d'implementazione \"{0}\" per il nome di contesto \"{1}\".")
   @RBComment("When the provider implementation can't be found for the supplied context")
   @RBArgComment0("The name of the implementation class that can't be found")
   @RBArgComment1("The name of the context for which an implementation class was searched.")
   public static final String UNABLE_TO_FIND_PROVIDER = "2";

   @RBEntry("I descrittori devono avere lo stesso nome di contesto dell'istanza ACService cui vengono aggiunti. L'utente ha tentato di aggiungere un descrittore con contesto \"{0}\" ad un ACService con contesto \"{1}\".")
   @RBComment("When an attempt is made to add a ServiceDescriptor to an ACServices object in a different context.")
   @RBArgComment0("The name of the context of the descriptor.")
   @RBArgComment1("The name of the context of the ACServices object.")
   public static final String WRONG_CONTEXT_ON_DESCRIPTOR = "3";

   @RBEntry("Cardinalità non supportata: \"{0}\"")
   @RBComment("When a cardinality is set on a DefaultServiceDescriptor that is unsupported.")
   @RBArgComment0("The offending cardinality")
   public static final String UNSUPPORTED_CARDINALITY = "4";

   @RBEntry("Tipo di voce non supportata.")
   @RBComment("When an entry type is set on a DefaultServiceDescriptor that is unsupported.")
   public static final String UNSUPPORTED_ENTRY_TYPE = "5";

   @RBEntry("Impossibile invocare il costruttore pubblico per la classe d'implementazione \"{0}\".")
   @RBComment("When an implementation class was found with a public no-arg constructor, but it couldn't be invoked.")
   @RBArgComment0("The name of the implementation class.")
   public static final String COULD_NOT_INVOKE_PUBLIC_CONSTRUCTOR = "6";

   @RBEntry("Per la classe d'implementazione \"{0}\" non è stato trovato alcun metodo singleton.")
   @RBComment("When an implementation class did not have a singleton method that could be invoked.")
   @RBArgComment0("The name of the implementation class.")
   public static final String NO_SINGLETON_METHOD_FOUND = "7";

   @RBEntry("Impossibile invocare il metodo singleton per la classe d'implementazione \"{0}\".")
   @RBComment("When a singleton method was found on the implementation class, but it couldn't be invoked.")
   @RBArgComment0("The name of the implementation class.")
   public static final String COULD_NOT_INVOKE_SINGLETON_METHOD = "8";

   @RBEntry("Impossibile trovare una classe d'implementazione \"{0}\".")
   @RBComment("When an the named implementation class can't be found.")
   @RBArgComment0("The name of the implementation class.")
   public static final String UNABLE_TO_FIND_IMPLEMENTATION_CLASS = "9";

   @RBEntry("Impossibile creare una classe d'implementazione \"{0}\".")
   @RBComment("When an the named implementation class was found but couldn't be instantiated.")
   @RBArgComment0("The name of the implementation class.")
   public static final String UNABLE_TO_CREATE_IMPLEMENTATION_CLASS = "10";

   @RBEntry("Le implementazioni del servizio di default funzionano solo con il descrittore del servizio di default.")
   @RBComment("When a ServiceDescriptor that is not a DefaultServiceDescriptor is set on a DefaultServiceImplementation.")
   public static final String INVALID_DESCRIPTOR = "11";

   @RBEntry("Il metodo load() può essere invocato solo dopo l'impostazione di un'istanza di servizio sul caricatore.")
   @RBComment("When load() is called on a ServiceLoader without a services object having previously been set.")
   public static final String ILLEGAL_LOADER_STATE = "12";

   @RBEntry("Impossibile caricare il file \"{0}\" durante l'avvio del contesto.")
   @RBComment("When a file can't be loaded at context startup")
   @RBArgComment0("The name of the file that can't be loaded.")
   public static final String UNABLE_TO_LOAD_FILE = "13";

   @RBEntry("Eccezione durante l'analisi del file \"{0}\".")
   @RBComment("When an exception is caught during the parsing of a file.")
   @RBArgComment0("The file name where the exception occurred.")
   public static final String PARSE_ERROR = "14";

   @RBEntry("Eccezione durane il caricamento del file \"{0}\"")
   @RBComment("When an exception occurs during file load.")
   @RBArgComment0("The file name where the exception occurred.")
   public static final String LOAD_ERROR = "15";

   @RBEntry("Numero di token non previsto: \"{0}\"")
   @RBComment("When a configuration line did not contain the correct number of tokens")
   @RBArgComment0("The unexpected number of tokens")
   public static final String UNEXPECTED_NUMBER_OF_TOKENS = "16";

   @RBEntry("Impossibile analizzare la priorità: \"{0}\"")
   @RBComment("When the priority could not be found in the configuration line.")
   @RBArgComment0("The priority that couldn't be parsed into an integer.")
   public static final String UNABLE_TO_PARSE_PRIORITY = "17";
}
