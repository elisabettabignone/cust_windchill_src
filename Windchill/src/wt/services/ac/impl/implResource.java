/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.services.ac.impl;

import wt.util.resource.*;

@RBUUID("wt.services.ac.impl.implResource")
public final class implResource extends WTListResourceBundle {
   @RBEntry("Unable to create a service provider for service name: \"{0}\"")
   @RBComment("When a service provider can't be instantiated for the passed in service name")
   @RBArgComment0("The name of the service for which an implementation was uncessfully instantiated")
   public static final String UNABLE_TO_CREATE_PROVIDER = "0";

   @RBEntry("Unable to load properties")
   @RBComment("When properties can't be loaded")
   public static final String UNABLE_TO_LOAD_PROPERTIES = "1";

   @RBEntry("Unable to find implementation class \"{0}\" for context name \"{1}\".")
   @RBComment("When the provider implementation can't be found for the supplied context")
   @RBArgComment0("The name of the implementation class that can't be found")
   @RBArgComment1("The name of the context for which an implementation class was searched.")
   public static final String UNABLE_TO_FIND_PROVIDER = "2";

   @RBEntry("Descriptors must have the same context name as the ACServices instance they are added to. You tried to add a descriptor with context \"{0}\" to an ACServices with context \"{1}\".")
   @RBComment("When an attempt is made to add a ServiceDescriptor to an ACServices object in a different context.")
   @RBArgComment0("The name of the context of the descriptor.")
   @RBArgComment1("The name of the context of the ACServices object.")
   public static final String WRONG_CONTEXT_ON_DESCRIPTOR = "3";

   @RBEntry("Unsupported cardinality: \"{0}\"")
   @RBComment("When a cardinality is set on a DefaultServiceDescriptor that is unsupported.")
   @RBArgComment0("The offending cardinality")
   public static final String UNSUPPORTED_CARDINALITY = "4";

   @RBEntry("Unsupported entry type.")
   @RBComment("When an entry type is set on a DefaultServiceDescriptor that is unsupported.")
   public static final String UNSUPPORTED_ENTRY_TYPE = "5";

   @RBEntry("Could not invoke the public constructor for implementation class \"{0}\".")
   @RBComment("When an implementation class was found with a public no-arg constructor, but it couldn't be invoked.")
   @RBArgComment0("The name of the implementation class.")
   public static final String COULD_NOT_INVOKE_PUBLIC_CONSTRUCTOR = "6";

   @RBEntry("No singleton method found for implementation class \"{0}\".")
   @RBComment("When an implementation class did not have a singleton method that could be invoked.")
   @RBArgComment0("The name of the implementation class.")
   public static final String NO_SINGLETON_METHOD_FOUND = "7";

   @RBEntry("Could not invoke singleton method for implementation class \"{0}\".")
   @RBComment("When a singleton method was found on the implementation class, but it couldn't be invoked.")
   @RBArgComment0("The name of the implementation class.")
   public static final String COULD_NOT_INVOKE_SINGLETON_METHOD = "8";

   @RBEntry("Unable to find implementation class \"{0}\".")
   @RBComment("When an the named implementation class can't be found.")
   @RBArgComment0("The name of the implementation class.")
   public static final String UNABLE_TO_FIND_IMPLEMENTATION_CLASS = "9";

   @RBEntry("Unable to create implementation class \"{0}\".")
   @RBComment("When an the named implementation class was found but couldn't be instantiated.")
   @RBArgComment0("The name of the implementation class.")
   public static final String UNABLE_TO_CREATE_IMPLEMENTATION_CLASS = "10";

   @RBEntry("DefaultServiceImplementations only work with instances of DefaultServiceDescriptor.")
   @RBComment("When a ServiceDescriptor that is not a DefaultServiceDescriptor is set on a DefaultServiceImplementation.")
   public static final String INVALID_DESCRIPTOR = "11";

   @RBEntry("The load() method cannot be called until a services instance is set on the loader.")
   @RBComment("When load() is called on a ServiceLoader without a services object having previously been set.")
   public static final String ILLEGAL_LOADER_STATE = "12";

   @RBEntry("During context startup, unable to load file \"{0}\"")
   @RBComment("When a file can't be loaded at context startup")
   @RBArgComment0("The name of the file that can't be loaded.")
   public static final String UNABLE_TO_LOAD_FILE = "13";

   @RBEntry("Exception while parsing file \"{0}\".")
   @RBComment("When an exception is caught during the parsing of a file.")
   @RBArgComment0("The file name where the exception occurred.")
   public static final String PARSE_ERROR = "14";

   @RBEntry("Exception While loading file \"{0}\"")
   @RBComment("When an exception occurs during file load.")
   @RBArgComment0("The file name where the exception occurred.")
   public static final String LOAD_ERROR = "15";

   @RBEntry("Unexpected number of tokens: \"{0}\"")
   @RBComment("When a configuration line did not contain the correct number of tokens")
   @RBArgComment0("The unexpected number of tokens")
   public static final String UNEXPECTED_NUMBER_OF_TOKENS = "16";

   @RBEntry("Unable to parse priority: \"{0}\"")
   @RBComment("When the priority could not be found in the configuration line.")
   @RBArgComment0("The priority that couldn't be parsed into an integer.")
   public static final String UNABLE_TO_PARSE_PRIORITY = "17";
}
