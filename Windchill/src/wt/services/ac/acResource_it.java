package wt.services.ac;

import wt.util.resource.*;

public final class acResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile implementare i servizi ACService per il nome di contesto \"{0}\".")
   @RBComment("When no implementation class can be instantiated for the supplied context name")
   @RBArgComment0("The name of the context for which an ACServices instance was requested")
   public static final String UNABLE_TO_CREATE_ACSERVICES_CLASS = "0";

   @RBEntry("Impossibile creare un caricatore per il contesto \"{0}\"")
   @RBComment("When a ServiceLoader instance can't be instantiated for the supplied context")
   @RBArgComment0("The name of the context for which a loader was requested ")
   public static final String UNABLE_TO_CREATE_SERVICES_LOADER = "1";
}
