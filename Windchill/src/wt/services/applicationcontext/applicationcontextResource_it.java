package wt.services.applicationcontext;

import wt.util.resource.*;

public final class applicationcontextResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile creare un servizio chiamato \"{0}\" con il richiedente \"{1}\" e il selettore \"{2}\".")
   @RBComment("When a service that is requested from an ACServices instance can't be found. ")
   @RBArgComment0("The service name is the name of the service for which an implementation is desired.")
   @RBArgComment1("The requestor is the object that wants the service.")
   @RBArgComment2("The selector is a string used to help identify the desired service. ")
   public static final String UNABLE_TO_CREATE_SERVICE = "0";
}
