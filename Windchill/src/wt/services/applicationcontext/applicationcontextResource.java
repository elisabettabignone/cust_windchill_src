/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.services.applicationcontext;

import wt.util.resource.*;

@RBUUID("wt.services.applicationcontext.applicationcontextResource")
public final class applicationcontextResource extends WTListResourceBundle {
   @RBEntry("Unable to create service named \"{0}\" with requestor \"{1}\" and selector \"{2}\".")
   @RBComment("When a service that is requested from an ACServices instance can't be found. ")
   @RBArgComment0("The service name is the name of the service for which an implementation is desired.")
   @RBArgComment1("The requestor is the object that wants the service.")
   @RBArgComment2("The selector is a string used to help identify the desired service. ")
   public static final String UNABLE_TO_CREATE_SERVICE = "0";
}
