/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.services;

import wt.util.resource.*;

@RBUUID("wt.services.servicesResource")
public final class servicesResource extends WTListResourceBundle {
   @RBEntry("Branch already exists: {0}")
   @RBArgComment0(" refers to the branch")
   public static final String BRANCH_EXISTS = "2";

   @RBEntry("Could not load class file {0} for manager/service {1} due to {2}")
   @RBArgComment0(" refers to the class file")
   @RBArgComment1(" refers to the manager/service")
   @RBArgComment2(" refers to the reason")
   public static final String CLASS_LOAD_ERROR = "3";

   @RBEntry("Failure to create all managers!")
   public static final String CREATE_MANAGER_ERROR = "4";

   @RBEntry("Unable to start all managers.")
   public static final String START_MANAGER_ERROR = "5";

   @RBEntry("Failure to start all managers.")
   public static final String START_MANAGER_FAIL = "6";

   @RBEntry("Failure to shutdown all managers!")
   public static final String SHUTDOWN_FAIL = "7";

   @RBEntry("All managers started.")
   public static final String ALL_MANAGERS_STARTED = "8";

   @RBEntry("All managers shut down.")
   public static final String ALL_MANAGERS_SHUTDOWN = "9";

   @RBEntry("Unable to initialize the StandardManagerService.")
   public static final String UNABLE_TO_INITIALIZE_MANAGER_SERVICE = "10";

   @RBEntry("Unable to load file: \"{0}\"")
   @RBArgComment0("The file that couldn't be loaded")
   public static final String UNABLE_TO_LOAD_FILE = "11";

   @RBEntry("Unable to close file: \"{0}\"")
   @RBArgComment0("The file that couldn't be closed")
   public static final String UNABLE_TO_CLOSE_FILE = "12";

   @RBEntry("An entry for service: \"{0}\" already existed in group: \"{1}\". You cannot add a different entry for the same service to group: \"{2}\".")
   @RBComment("If a service has already been loaded as a member of one group, it cannot be loaded again as a member of a different group.")
   @RBArgComment0("The name of the service in question.")
   @RBArgComment1("The name of the group the service is already in. ")
   @RBArgComment2("The name of the new group that the service can't be added to.")
   public static final String ENTRY_EXISTS_IN_OTHER_GROUP = "13";

   @RBEntry("Unable to create group: \"{0}\"")
   @RBArgComment0("The name of the group that couldn't be created.")
   public static final String UNABLE_TO_CREATE_GROUP = "14";

   @RBEntry("Unable to create manager repository.")
   @RBComment("When the manager repository could not be created.")
   public static final String UNABLE_TO_CREATE_REPOSITORY = "15";

   @RBEntry("Unexpected format for services entry: \"{0}\"")
   @RBComment("When a services entry did not have the expected format")
   @RBArgComment0("The offending service entry.")
   public static final String UNEXPECTED_FORMAT = "16";

   @RBEntry("Unable to find manager class from entry: \"{0}\"")
   @RBComment("When the manager class specified in the service startup entry couldn't be loaded.")
   @RBArgComment0("The offending service startup entry.")
   public static final String UNABLE_TO_FIND_MANAGER_CLASS = "17";

   @RBEntry("Unable to find manager implementation class from entry: \"{0}\"")
   @RBComment("When the manager implementation class specified in the service startup entry couldn't be loaded.")
   @RBArgComment0("The offending service startup entry.")
   public static final String UNABLE_TO_FIND_MANAGER_IMPLEMENTATION = "18";

   @RBEntry("Unable to create manager: \"{0}\" with implementation class: \"{1}\"")
   @RBComment("When a manager can't be instantiated with the given implementation class")
   @RBArgComment0("The classname of the manager interface")
   @RBArgComment1("The classname of the manager implementation")
   public static final String UNABLE_TO_CREATE_MANAGER = "19";

   @RBEntry("StandardManagerService started")
   @RBComment("Printed to the MethodServer standard out after all services have started up.")
   public static final String STANDARD_MANAGER_SERVICE_STARTED = "20";

   @RBEntry("Unable to start manager: \"{0}\" with implementation class: \"{1}\"")
   @RBComment("When a manager can't be started using the given implementation class")
   @RBArgComment0("The classname of the manager interface")
   @RBArgComment1("The classname of the manager implementation")
   public static final String UNABLE_TO_START_MANAGER = "21";
}
