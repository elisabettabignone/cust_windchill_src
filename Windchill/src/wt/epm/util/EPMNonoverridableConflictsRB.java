/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.epm.util;

import wt.util.resource.*;

@RBUUID("wt.epm.util.EPMNonoverridableConflictsRB")
public final class EPMNonoverridableConflictsRB extends WTListResourceBundle {
   /**
    * Numbers bellow 100 reserved for conflict supertypes
    **/
   @RBEntry("Unknown Conflict")
   public static final String UNKNOWN_CONFLICT = "0";

   @RBEntry("{0}")
   public static final String OBJECT = "1";

   @RBEntry("Add to Workspace conflict")
   public static final String ADD_TO_WS_CONFLICT = "10";

   @RBEntry("Checkout conflict")
   public static final String CHECKOUT_CONFLICT = "20";

   @RBEntry(" Check in conflict")
   public static final String CHECKIN_CONFLICT = "30";

   @RBEntry(" Undo checkout conflict")
   public static final String UNDO_CHECKOUT_CONFLICT = "40";

   @RBEntry(" Upload conflict")
   public static final String UPLOAD_CONFLICT = "50";

   @RBEntry(" Download conflict")
   public static final String DOWNLOAD_CONFLICT = "60";

   @RBEntry(" PDM check out conflict")
   public static final String SANDBOX_CHECKOUT_CONFLICT = "65";

   @RBEntry(" Send to PDM conflict")
   public static final String SANDBOX_CHECKIN_CONFLICT = "70";

   @RBEntry(" Undo PDM check out conflict")
   public static final String SANDBOX_UNDO_CHECKOUT_CONFLICT = "80";

   @RBEntry(" Delete conflict")
   public static final String DELETE_CONFLICT = "90";
   
   @RBEntry(" Validation conflict")
   public static final String VALIDATION_CONFLICT = "95";

   /**
    * Add to Workspace conflict
    **/
   @RBEntry("Cannot update workspace. It would cause the family with generic {0} to become incompatible.")
   public static final String WORKASPACE_ADD_INCOMPATIBLE_FAMILY_TABLE_MEMBERS = "100";

   @RBEntry("Adds an iteration of objects{0} different from the one that is currently checked out to the workspace")
   public static final String WORKASPACE_ADD_EXISTING_OBJECT = "110";

   @RBEntry("Family table {0} has incompatible instances from different workspaces.")
   public static final String INCOMPATIBLE_FT_IN_WORKASPACE = "120";

   @RBEntry("Undo checkout failed because the family table associated with the generic {0} was checked out from an earlier iteration. Use the 'remove' command to remove this family from the workspace first.")
   public static final String INCOMPATIBLE_FT_IN_WORKASPACE_ON_UNCHECKOUT = "130";

   @RBEntry("Family Tables of top generics: {0} belong to the same Family Table Master and cannot be added to Workspace {1} simultaneously.")
   public static final String VALIDATE_ONLY_ONE_ITERATION_FOR_EACH_FT = "140";

   /**
    * 2. Checkout
    **/
   @RBEntry("Can not perform check out of nonlatest iteration of document \"{0}\". All members of Family Table must be checked out together..")
   public static final String NONLATEST_CHECKOUT_INCOMPLETE_FT = "210";

   @RBEntry("The object has already been checked out by \"{0}\" to \"{1}\" and cannot be checked out more than once.")
   public static final String ALREADY_CHECKEDOUT = "211";

   @RBEntry("The object has already been checked out by unknown user to \"{0}\" and cannot be checked out more than once.")
   public static final String ALREADY_CHECKEDOUT_UNKNOWN = "212";

   /**
    * 3. Checkin
    **/
   @RBEntry("User doesn't have MODIFY privileges to folder \"{0}\" and cannot Checkin this object to the folder.")
   public static final String MODIFY_ACCESS_ERROR = "64";

   @RBEntry("All checked out family table members and/or family table members with uploaded modifications must be checked in together. {0}")
   public static final String CHECKIN_FAMILY_TABLE = "300";

   @RBEntry("Dependent objects must be checked in together. {0}")
   public static final String CHECKIN_DEEPENDENCY = "310";

   @RBEntry("You have insufficient permissions to create objects.")
   public static final String CREATE_INSUFFICIENT_PERMISSION_ERROR = "320";

   /**
    * 4. UndoCheckout
    **/
   @RBEntry("To successfully execute this operation all modified family table members must be selected together. {0}")
   public static final String UNDOCHECKOUT_FAMILY_TABLE = "400";

   @RBEntry("{0} does not belong to workspace {1} Unable to undo check out.")
   public static final String UNDOCHECKOUT_OBJECT_NOT_BELONG_TO_WORKSPACE = "410";

   /**
    * 5. Upload
    **/
   @RBEntry("Can not create instance as ghost because generic is not ghost")
   public static final String UPLOAD_INSTANCE_IS_A_GHOST_GENERIC_IS_NOT = "500";

   @RBEntry(" Can not delete checked out family table")
   public static final String CHECKEDOUT_FAMILY_TABLE = "900";

   @RBEntry("Unable to delete {0} without also deleting {1}.")
   public static final String DELETE_FAMILY_TABLE = "901";

   @RBEntry("  Unable to delete object {0} because its associated family table cannot be deleted")
   public static final String DELETE_MIGRATED_FAMILY_TABLE = "902";

   /**
    * <0>=The display identity of a Family Table object that cannot be deleted.
    * <1>=List of EPMDocuments that are family table members.
    **/
   @RBEntry("Unable to delete {0} without also deleting the following members of the family table: {1}.")
   public static final String DELETE_FAMILY_TABLE_PLURAL = "903";

   /**
    * <0>=The display identity of a EPMDocument object that cannot be deleted.
    * <1>=The display identity of a EPMDocument that is a family table member.
    **/
   @RBEntry("Unable to delete {0} without also deleting {1}.")
   public static final String DELETE_FAMILY_TABLE_MEMBER = "904";

   /**
    * <0>=The display identity of a Family Table object that cannot be deleted.
    * <1>=List of EPMDocuments that are family table members.
    **/
   @RBEntry("Unable to delete {0} without also deleting the following members of the family table: {1}.")
   public static final String DELETE_FAMILY_TABLE_MEMBER_PLURAL = "905";
   
   /**
    * Validation
    **/
   @RBEntry("Instance(s) {0} do not have same neutral data version as the generic {1} and hence cannot belong to the same family table.")
   public static final String INCOMPATIBLE_AUTHORING_APP_VERSIONS = "951";

   /**
    * Overridable messages
    **/
   @RBEntry("More than one compatible family table object {0}")
   public static final String MORE_THAN_ONE_COMPATIBLE_FT = "1010";

   @RBEntry("Version {0} of this object already exists in the workspace {1}. Overriding this conflict would overwrite the existing version with different version {2}.")
   public static final String WORKASPACE_ADD_EXISTING_OBJECT_OVERRIDABLE = "1020";

   @RBEntry("This instance belongs to the Generic version that is different from what is added to the Workspace. In order to proceed with this task, you must merge this instance into workspace version of the Generic")
   public static final String FAMILY_TABLE_MERGE = "1030";

   @RBEntry("A CAD Drawing is being checked in without a corresponding Model or/and Drawing Format")
   public static final String CHECKIN_DRAWING_WITHOUT_MODEL = "1040";

   @RBEntry("After undo checkout is complete the document {0} may become incompatible with the Family Table {1} in Workspace , the most probable cause is previous merge of conflict instance into Family Table")
   public static final String UNCHECKOUT_FT_INCOMPATIBILITY = "1050";

   @RBEntry("There are non committed changes for WTPart")
   public static final String RESTORE_FRAME_WITH_WTPART = "1060";
}
