/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.epm.util;

import wt.util.resource.*;

@RBUUID("wt.epm.util.EPMPreferencesRB")
@RBNameException //Grandfathered by conversion
public final class EPMPreferencesRB_it extends WTListResourceBundle {
   @RBEntry("Preferenze servizio EPM")
   public static final String EPM_PREFERENCES = "EPM_PREFERENCES";

   @RBEntry("Preferenze servizio EPM")
   public static final String EPM_PREFERENCES_DESC = "EPM_PREFERENCES_DESC";

   @RBEntry("Preferenze servizio Build")
   public static final String BUILD_PREFERENCES = "BUILD_PREFERENCES";

   @RBEntry("Preferenze utilizzate dal servizio Build")
   public static final String BUILD_PREFERENCES_DESC = "BUILD_PREFERENCES_DESC";

   @RBEntry("Delimitatore attributi")
   public static final String ATTRIBUTES_DELIMITER = "ATTRIBUTES_DELIMITER";

   @RBEntry("La preferenza specifica il carattere di delimitazione per separare gli attributi da pubblicare.")
   public static final String ATTRIBUTES_DELIMITER_SHORT_DESC = "ATTRIBUTES_DELIMITER_SHORT_DESC";

   @RBEntry("La preferenza definisce il carattere di delimitazione che separa gli attributi specificati nelle preferenze Attributi da pubblicare sulla parte, Attributi da pubblicare sul link, Attributi da pubblicare sul master e Attributi da pubblicare sul caso d'impiego.")
   public static final String ATTRIBUTES_DELIMITER_DESC = "ATTRIBUTES_DELIMITER_DESC";

   @RBEntry("Attributi da pubblicare sulla parte")
   public static final String BUILD_ATTRIBUTES_TO_PART = "BUILD_ATTRIBUTES_TO_PART";

   @RBEntry("Attributi da pubblicare sulla parte")
   public static final String BUILD_ATTRIBUTES_TO_PART_SHORT_DESC = "BUILD_ATTRIBUTES_TO_PART_SHORT_DESC";

   @RBEntry("Attributi da pubblicare sulla parte. Gli attributi sono delimitati dal carattere specificato nella preferenza Delimitatore attributi")
   public static final String BUILD_ATTRIBUTES_TO_PART_DESC = "BUILD_ATTRIBUTES_TO_PART_DESC";

   @RBEntry("Attributi da pubblicare sul link")
   public static final String BUILD_ATTRIBUTES_TO_LINK = "BUILD_ATTRIBUTES_TO_LINK";

   @RBEntry("Attributi da pubblicare sul link")
   public static final String BUILD_ATTRIBUTES_TO_LINK_SHORT_DESC = "BUILD_ATTRIBUTES_TO_LINK_SHORT_DESC";

   @RBEntry("Attributi da pubblicare sul link. Gli attributi sono delimitati dal carattere specificato nella preferenza Delimitatore attributi")
   public static final String BUILD_ATTRIBUTES_TO_LINK_DESC = "BUILD_ATTRIBUTES_TO_LINK_DESC";

   @RBEntry("Attributi da pubblicare sul master")
   public static final String BUILD_ATTRIBUTES_TO_MASTER = "BUILD_ATTRIBUTES_TO_MASTER";

   @RBEntry("Attributi da pubblicare sul master")
   public static final String BUILD_ATTRIBUTES_TO_MASTER_SHORT_DESC = "BUILD_ATTRIBUTES_TO_MASTER_SHORT_DESC";

   @RBEntry("Attributi da pubblicare sul master. Gli attributi sono delimitati dal carattere specificato nella preferenza Delimitatore attributi")
   public static final String BUILD_ATTRIBUTES_TO_MASTER_DESC = "BUILD_ATTRIBUTES_TO_MASTER_DESC";

   @RBEntry("Attributi da pubblicare sul caso d'impiego")
   public static final String BUILD_ATTRIBUTES_TO_OCCURRENCE = "BUILD_ATTRIBUTES_TO_OCCURRENCE";

   @RBEntry("Attributi da pubblicare sul caso d'impiego")
   public static final String BUILD_ATTRIBUTES_TO_OCCURRENCE_SHORT_DESC = "BUILD_ATTRIBUTES_TO_OCCURRENCE_SHORT_DESC";

   @RBEntry("Attributi da pubblicare sul caso d'impiego. Gli attributi sono delimitati dal carattere specificato nella preferenza Delimitatore attributi")
   public static final String BUILD_ATTRIBUTES_TO_OCCURRENCE_DESC = "BUILD_ATTRIBUTES_TO_OCCURRENCE_DESC";

   @RBEntry("Crea associazione immagine per default")
   public static final String BUILD_IMAGES = "BUILD_IMAGES";

   @RBEntry("Crea associazione immagine per default")
   public static final String BUILD_IMAGES_SHORT_DESC = "BUILD_IMAGES_SHORT_DESC";

   @RBEntry("Poiché le associazioni immagine non trasmettono i valori a una parte, potrebbe non essere necessario crearle nella struttura del prodotto. <br/><ul><li>Sì - Le parti associate a un documento CAD con un'associazione immagine vengono create e viene registrata l'iterazione del documento CAD disponibile al momento della creazione. </li><li>No  - La parte associata al documento CAD non viene né creata né sottoposta a nuova iterazione.</li></ul>")
   public static final String BUILD_IMAGES_DESC = "BUILD_IMAGES_DESC";

   @RBEntry("Ignora dipendenze opzionali")
   public static final String GHOST_IGNORE_OPTIONAL_DEPENDENCIES = "GHOST_IGNORE_OPTIONAL_DEPENDENCIES";

   @RBEntry("Ignora dipendenze riferimenti opzionali")
   public static final String GHOST_IGNORE_OPTIONAL_REFERENCE_DEPENDENCIES = "GHOST_IGNORE_OPTIONAL_REFERENCE_DEPENDENCIES";

   @RBEntry("Ignora solo dipendenze interne")
   public static final String GHOST_IGNORE_INTERNAL_DEPENDENCIES_ONLY = "GHOST_IGNORE_INTERNAL_DEPENDENCIES_ONLY";

   @RBEntry("Non ignorare mai")
   public static final String GHOST_DO_NOT_ALLOW_IGNORE = "GHOST_DO_NOT_ALLOW_IGNORE";

   @RBEntry("Risoluzione oggetto incompleto")
   public static final String GHOST_SERVER_RESOLUTION = "GHOST_SERVER_RESOLUTION";

   @RBEntry("Imposta il tipo di dipendenti dei documenti CAD/dinamici che l'utente può ignorare durante il caricamento e il Check-In.")
   public static final String GHOST_SERVER_RESOLUTION_SHORT_DESC = "GHOST_SERVER_RESOLUTION_SHORT_DESC";

   @RBEntry("Imposta il tipo di dipendenti dei documenti CAD/dinamici che l'utente può ignorare durante il caricamento e il Check-In. I valori consentiti sono Ignora dipendenze opzionali, Ignora dipendenze riferimenti opzionali, Ignora solo dipendenze interne, Non ignorare mai. Il default è Ignora dipendenze opzionali. <li>Ignora dipendenze opzionali - Sarà possibile ignorare qualunque dipendenza mancante non necessaria, compresi i componenti soppressi in un assieme. <li>Ignora dipendenze riferimenti opzionali - Sarà possibile ignorare eventuali riferimenti non necessari. <li>Ignora solo dipendenze interne - Sarà possibile ignorare solo le dipendenze interne di Creo. <li>Non ignorare mai - Non sarà possibile ignorare i dipendenti mancanti.")
   public static final String GHOST_SERVER_RESOLUTION_DESC = "GHOST_SERVER_RESOLUTION_DESC";

   @RBEntry("Invia un documento CAD/dinamico a PDM senza i dipendenti opzionali")
   public static final String MISSING_DEPENDENTS_FOR_CHECKIN = "MISSING_DEPENDENTS_FOR_CHECKIN";

   @RBEntry("Consente l'invio al sistema PDM di un documento CAD/dinamico senza i nuovi dipendenti opzionali")
   public static final String MISSING_DEPENDENTS_FOR_CHECKIN_SHORT_DESC = "MISSING_DEPENDENTS_FOR_CHECKIN_SHORT_DESC";

   @RBEntry("Consente l'invio al sistema PDM di un documento CAD/dinamico senza i nuovi dipendenti opzionali. I link ai dipendenti opzionali sono rimossi.")
   public static final String MISSING_DEPENDENTS_FOR_CHECKIN_DESC = "MISSING_DEPENDENTS_FOR_CHECKIN_DESC";

   @RBEntry("Attributi contenuto contribuente")
   public static final String BUILD_CONTRIBUTING_CONTENT_ATTRIBUTES_TO_PART = "BUILD_CONTRIBUTING_CONTENT_ATTRIBUTES_TO_PART";

   @RBEntry("Attributi da pubblicare sulla parte in base alla relazione Contenuto contribuente.")
   public static final String BUILD_CONTRIBUTING_CONTENT_ATTRIBUTES_TO_PART_SHORT_DESC = "BUILD_CONTRIBUTING_CONTENT_ATTRIBUTES_TO_PART_SHORT_DESC";

   @RBEntry("Attributi da pubblicare sulla parte in base alla relazione Contenuto contribuente. Gli attributi sono delimitati dal carattere specificato nella preferenza Delimitatore attributi")
   public static final String BUILD_CONTRIBUTING_CONTENT_ATTRIBUTES_TO_PART_DESC = "BUILD_CONTRIBUTING_CONTENT_ATTRIBUTES_TO_PART_DESC";

   @RBEntry("Attributi immagine contribuente")
   public static final String BUILD_CONTRIBUTING_IMAGE_ATTRIBUTES_TO_PART = "BUILD_CONTRIBUTING_IMAGE_ATTRIBUTES_TO_PART";

   @RBEntry("Attributi da pubblicare sulla parte in base alla relazione Immagine contribuente.")
   public static final String BUILD_CONTRIBUTING_IMAGE_ATTRIBUTES_TO_PART_SHORT_DESC = "BUILD_CONTRIBUTING_IMAGE_ATTRIBUTES_TO_PART_SHORT_DESC";

   @RBEntry("Attributi da pubblicare sulla parte in base alla relazione Immagine contribuente. Gli attributi sono delimitati dal carattere specificato nella preferenza Delimitatore attributi")
   public static final String BUILD_CONTRIBUTING_IMAGE_ATTRIBUTES_TO_PART_DESC = "BUILD_CONTRIBUTING_IMAGE_ATTRIBUTES_TO_PART_DESC";

   @RBEntry("Attributo indice bolla")
   public static final String FIND_NUMBER_ATTRIBUTE = "FIND_NUMBER_ATTRIBUTE";

   @RBEntry("Nome di un attributo di un link componenti di un documento CAD che deve essere copiato nell'indice bolla dell'utilizzo parte.")
   public static final String FIND_NUMBER_ATTRIBUTE_SHORT_DESC = "FIND_NUMBER_ATTRIBUTE_SHORT_DESC";

   @RBEntry("Nome di un attributo di un link componenti di un documento CAD che deve essere copiato nell'indice bolla dell'utilizzo parte. Questa preferenza non è impostata per default.")
   public static final String FIND_NUMBER_ATTRIBUTE_DESC = "FIND_NUMBER_ATTRIBUTE_DESC";

   @RBEntry("Attributo numero di riga")
   public static final String LINE_NUMBER_ATTRIBUTE = "LINE_NUMBER_ATTRIBUTE";

   @RBEntry("Nome di un attributo di un link componenti di un documento CAD che deve essere copiato nel numero di riga dell'utilizzo parte.")
   public static final String LINE_NUMBER_ATTRIBUTE_SHORT_DESC = "LINE_NUMBER_ATTRIBUTE_SHORT_DESC";

   @RBEntry("Nome di un attributo di un link componenti di un documento CAD che deve essere copiato nel numero di riga dell'utilizzo parte. Il tipo di attributo deve essere un numero intero. Questa preferenza non è impostata per default.")
   public static final String LINE_NUMBER_ATTRIBUTE_DESC = "LINE_NUMBER_ATTRIBUTE_DESC";

   @RBEntry("Riporta associazione contenuto per nuova revisione documento CAD")
   public static final String CARRY_FORWARD_CONTENT_ASSOCIATION = "CARRY_FORWARD_CONTENT_ASSOCIATION";

   @RBEntry("Quando si crea una nuova revisione SOLO del documento CAD, è possibile configurare il sistema perché riporti le associazioni contenuto esistenti.")
   public static final String CARRY_FORWARD_CONTENT_ASSOCIATION_SHORT_DESC = "CARRY_FORWARD_CONTENT_ASSOCIATION_SHORT_DESC";

   @RBEntry("Quando si crea una nuova revisione SOLO del documento CAD, è possibile configurare il sistema perché riporti le associazioni contenuto esistenti. Per default, il valore della preferenza è impostato su 'No' e quando si crea una nuova revisione SOLO del documento CAD, la parte esistente non viene associata alla nuova versione del documento CAD. Grazie a questo sistema, nessuna versione rilasciata della parte può essere modificata senza creare una nuova iterazione o revisione della parte. Se la propria azienda consente l'associazione tra parti rilasciate e documenti CAD non rilasciati, è possibile cambiare il valore della preferenza in 'Sì' e la versione più recente della parte viene associata alla nuova versione del documento CAD una volta creata una nuova revisione.")
   public static final String CARRY_FORWARD_CONTENT_ASSOCIATION_DESC = "CARRY_FORWARD_CONTENT_ASSOCIATION_DESC";

}
