/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.epm.util;

import wt.util.resource.*;

@RBUUID("wt.epm.util.EPMNonoverridableConflictsRB")
public final class EPMNonoverridableConflictsRB_it extends WTListResourceBundle {
   /**
    * Numbers bellow 100 reserved for conflict supertypes
    **/
   @RBEntry("Conflitto di tipo sconosciuto")
   public static final String UNKNOWN_CONFLICT = "0";

   @RBEntry("{0}")
   public static final String OBJECT = "1";

   @RBEntry("Conflitto di aggiunta al workspace")
   public static final String ADD_TO_WS_CONFLICT = "10";

   @RBEntry("Conflitto di Check-Out")
   public static final String CHECKOUT_CONFLICT = "20";

   @RBEntry(" Conflitto di Check-In")
   public static final String CHECKIN_CONFLICT = "30";

   @RBEntry(" Conflitto di annullamento Check-Out")
   public static final String UNDO_CHECKOUT_CONFLICT = "40";

   @RBEntry(" Conflitto di caricamento")
   public static final String UPLOAD_CONFLICT = "50";

   @RBEntry(" Conflitto di scaricamento")
   public static final String DOWNLOAD_CONFLICT = "60";

   @RBEntry(" Conflitto di Check-Out PDM")
   public static final String SANDBOX_CHECKOUT_CONFLICT = "65";

   @RBEntry(" Conflitto di invio a PDM")
   public static final String SANDBOX_CHECKIN_CONFLICT = "70";

   @RBEntry(" Conflitto di annullamento Check-Out PDM")
   public static final String SANDBOX_UNDO_CHECKOUT_CONFLICT = "80";

   @RBEntry(" Conflitto di eliminazione")
   public static final String DELETE_CONFLICT = "90";
   
   @RBEntry(" Conflitto di convalida")
   public static final String VALIDATION_CONFLICT = "95";

   /**
    * Add to Workspace conflict
    **/
   @RBEntry("Impossibile aggiornare il workspace. L'operazione renderebbe incompatibile la famiglia con generico {0}.")
   public static final String WORKASPACE_ADD_INCOMPATIBLE_FAMILY_TABLE_MEMBERS = "100";

   @RBEntry("Aggiunge un'iterazione degli oggetti {0} diversa da quella attualmente sottoposta a Check-Out nel workspace")
   public static final String WORKASPACE_ADD_EXISTING_OBJECT = "110";

   @RBEntry("La family table {0} presenta istanze incompatibili provenienti da workspace diversi.")
   public static final String INCOMPATIBLE_FT_IN_WORKASPACE = "120";

   @RBEntry("Annulla Check-Out non riuscito in quanto la family table associata al generico {0} è stata sottoposta a Check-Out a partire da un'iterazione precedente. Utilizzare il comando Rimuovi per rimuovere la family table dal workspace prima di procedere.")
   public static final String INCOMPATIBLE_FT_IN_WORKASPACE_ON_UNCHECKOUT = "130";

   @RBEntry("Family table di generici di livello superiore: {0} appartengono allo stesso master di family table e non possono essere aggiunte al workspace {1} contemporaneamente.")
   public static final String VALIDATE_ONLY_ONE_ITERATION_FOR_EACH_FT = "140";

   /**
    * 2. Checkout
    **/
   @RBEntry("Impossibile sottoporre a Check-Out l'iterazione non aggiornata del documento \"{0}\". Tutti i membri di una family table devono essere sottoposti a Check-Out assieme.")
   public static final String NONLATEST_CHECKOUT_INCOMPLETE_FT = "210";

   @RBEntry("L'oggetto è stato già sottoposto a Check-Out da \"{0}\" in \"{1}\" e non può essere sottoposto nuovamente a Check-Out.")
   public static final String ALREADY_CHECKEDOUT = "211";

   @RBEntry("L'oggetto è stato già sottoposto a Check-Out da un utente sconosciuto in \"{0}\" e non può essere sottoposto nuovamente a Check-Out.")
   public static final String ALREADY_CHECKEDOUT_UNKNOWN = "212";

   /**
    * 3. Checkin
    **/
   @RBEntry("L'utente non dispone dei privilegi di MODIFICA per la cartella \"{0}\" e non può sottoporre l'oggetto a Check-In nella cartella.")
   public static final String MODIFY_ACCESS_ERROR = "64";

   @RBEntry("Tutti i membri sottoposti a Check-Out di una stessa family table e/o i membri di una family table con modifiche caricate devono essere sottoposti a Check-In assieme. {0}")
   public static final String CHECKIN_FAMILY_TABLE = "300";

   @RBEntry("Gli oggetti dipendenti devono essere sottoposti a Check-In assieme. {0}")
   public static final String CHECKIN_DEEPENDENCY = "310";

   @RBEntry("Non si dispone di permessi sufficienti per creare oggetti")
   public static final String CREATE_INSUFFICIENT_PERMISSION_ERROR = "320";

   /**
    * 4. UndoCheckout
    **/
   @RBEntry("Per eseguire correttamente l'operazione, tutti i membri modificati della family table devono essere selezionati insieme. {0}")
   public static final String UNDOCHECKOUT_FAMILY_TABLE = "400";

   @RBEntry("{0} non appartiene al workspace {1}. Impossibile annullare il Check-Out.")
   public static final String UNDOCHECKOUT_OBJECT_NOT_BELONG_TO_WORKSPACE = "410";

   /**
    * 5. Upload
    **/
   @RBEntry("Impossibile creare l'istanza come fantasma. Il generico non è fantasma")
   public static final String UPLOAD_INSTANCE_IS_A_GHOST_GENERIC_IS_NOT = "500";

   @RBEntry(" Impossibile eliminare una family table sottoposta a Check-Out")
   public static final String CHECKEDOUT_FAMILY_TABLE = "900";

   @RBEntry("Impossibile eliminare {0} senza eliminare anche {1}.")
   public static final String DELETE_FAMILY_TABLE = "901";

   @RBEntry("  Impossibile eliminare l'oggetto {0} perché la family table associata non è eliminabile")
   public static final String DELETE_MIGRATED_FAMILY_TABLE = "902";

   /**
    * <0>=The display identity of a Family Table object that cannot be deleted.
    * <1>=List of EPMDocuments that are family table members.
    **/
   @RBEntry("Impossibile eliminare {0} senza eliminare anche i seguenti membri della family table: {1}.")
   public static final String DELETE_FAMILY_TABLE_PLURAL = "903";

   /**
    * <0>=The display identity of a EPMDocument object that cannot be deleted.
    * <1>=The display identity of a EPMDocument that is a family table member.
    **/
   @RBEntry("Impossibile eliminare {0} senza eliminare anche {1}.")
   public static final String DELETE_FAMILY_TABLE_MEMBER = "904";

   /**
    * <0>=The display identity of a Family Table object that cannot be deleted.
    * <1>=List of EPMDocuments that are family table members.
    **/
   @RBEntry("Impossibile eliminare {0} senza eliminare anche i seguenti membri della family table: {1}.")
   public static final String DELETE_FAMILY_TABLE_MEMBER_PLURAL = "905";
   
   /**
    * Validation
    **/
   @RBEntry("Le istanze {0} non hanno la stessa versione di dati neutri del generico {1}. Non possono quindi appartenere alla stessa family table.")
   public static final String INCOMPATIBLE_AUTHORING_APP_VERSIONS = "951";

   /**
    * Overridable messages
    **/
   @RBEntry("Più di un oggetto family table compatibile {0}")
   public static final String MORE_THAN_ONE_COMPATIBLE_FT = "1010";

   @RBEntry("La versione {0} dell'oggetto esiste già nel workspace {1}. Se si ignora il conflitto, la versione esistente verrà sovrascritta dalla versione differente {2}.")
   public static final String WORKASPACE_ADD_EXISTING_OBJECT_OVERRIDABLE = "1020";

   @RBEntry("L'istanza appartiene a una versione del generico diversa da quella aggiunta al workspace. Per continuare con il task, unire l'istanza alla versione del generico nel workspace")
   public static final String FAMILY_TABLE_MERGE = "1030";

   @RBEntry("Si sta tentando di sottoporre un disegno CAD a Check-In senza un modello e/o un formato di disegno corrispondente")
   public static final String CHECKIN_DRAWING_WITHOUT_MODEL = "1040";

   @RBEntry("Dopo l'annullamento del Check-Out, è possibile che il documento {0} diventi incompatibile con la Family Table {1} nel workspace. La causa più probabile è una precedente unione nella Family Table di un'istanza in conflitto.")
   public static final String UNCHECKOUT_FT_INCOMPATIBILITY = "1050";

   @RBEntry("WTPart non presenta modifiche non salvate")
   public static final String RESTORE_FRAME_WITH_WTPART = "1060";
}
