/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.epm.util;

import wt.util.resource.*;

@RBUUID("wt.epm.util.EPMResource")
public final class EPMResource extends WTListResourceBundle {
   @RBEntry("Name is required")
   public static final String NAME_REQUIRED = "1";

   @RBEntry("Data is a required field")
   public static final String DATA_REQUIRED = "2";

   @RBEntry("Cannot delete link, there are overriding links")
   public static final String GENERICMEMBERLINK_DELETE_NOT_ALLOWED = "3";

   @RBEntry("Cannot store/modify {0}, either link is not persistent or does not originate from same CAD document")
   public static final String INSTMEMBERLINK_STORE_NOT_ALLOWED1 = "4";

   @RBEntry("Cannot store/modify {0}, either Family Instance is not persistent or does not belong to source CAD document")
   public static final String INSTMEMBERLINK_STORE_NOT_ALLOWED3 = "6";

   @RBEntry("Cannot store/modify {0} between {1} and {2}. The link has transform attribute set, but either the quantity is not one or member is unplaced")
   public static final String GENERICMEMBERLINK_STORE_NOT_ALLOWED = "7";

   @RBEntry("Application is not authorized to change this object")
   public static final String INVALID_CHANGING_APPLICATION = "11";

   @RBEntry("Cannot duplicate, SupportingData not created")
   public static final String CANNOT_DUPLICATE = "15";

   @RBEntry("Cannot get value from sequence generator")
   public static final String CANNOT_GET_SEQUENCE = "16";

   @RBEntry("The {0} link class is for navigation purposes only; one may not construct, modify or delete instances of this class")
   public static final String CANNOT_MODIFY_OR_DELETE_LINK = "18";

   @RBEntry("Unable to select a version of {0}. Your Configuration Spec did not specify which one to use.")
   public static final String BUILD_CANNOT_SELECT_VERSION = "19";

   @RBEntry("Family Instance with identifier {0} does not belong to CAD document {1}.")
   public static final String NO_SUCH_FAMILY_INSTANCE = "20";

   @RBEntry("{0} builds multiple parts. Cannot select which one to use.")
   public static final String SOURCE_BUILDS_MULTIPLE_PARTS = "21";

   @RBEntry("Cannot delete {0}. It has build rules associated to it.")
   public static final String CANNOT_DELETE_BUILD_SOURCE = "22";

   @RBEntry("Cannot move {0} to a shared folder. It has a build history whose source ({1}) is not in a shared folder.")
   public static final String ILLEGAL_BUILDHIST_MOVE_FOLDER = "23";

   @RBEntry("Cannot check in {0} to a shared folder. It has a build rule whose source ({1}) is not in a shared folder.User must first check in {1}.")
   public static final String ILLEGAL_BUILDRULE_MOVE_FOLDER = "24";

   @RBEntry("Cannot check in {0} to a shared folder. It has an active association to a source ({1}) which is not in a shared folder.")
   public static final String BUILD_SOURCE_IN_PERSONAL_FOLDER = "25";

   @RBEntry("Cannot check in {0}, since the following CAD Documents are still checked out: {1}. All related, checked out CAD Documents must also be checked in to check in and build the Part.")
   public static final String MUST_CHECK_IN_BUILD_SOURCE = "26";

   @RBEntry("Unable to append search criteria to select documents based on baseline information.  The referenced baseline may have been deleted.")
   public static final String INVALID_DOC_BASELINE_CONFIG_SPEC = "27";

   @RBEntry("An Occurrence ID already exists for the specified path.")
   public static final String PATH_ALREADY_HAS_ID = "28";

   @RBEntry("No Occurrence ID exists for the specified path.")
   public static final String PATH_HAS_NO_ID = "29";

   @RBEntry("Cannot checkin Document {0}.  It depends on {1}, which is checked out.  They must be checked in together.")
   public static final String CANNOT_VAULT_DOCUMENT_CHECKOUT = "30";

   @RBEntry("Cannot checkin Document {0}.  It depends on {1}, which is in a personal cabinet.")
   public static final String CANNOT_VAULT_DOCUMENT_PERSONAL = "31";

   @RBEntry("Folder {0} belongs to Workspace {1}, and may not be deleted.")
   public static final String FOLDER_BELONGS_TO_WORKSPACE = "32";

   @RBEntry("The object {0} is owned by {1}, which will not permit changes by {2}")
   public static final String OPERATION_VETOED = "33";

   @RBEntry("The property wt.epm.BaselineFolder is not set.  Please set the property to a valid folder.")
   public static final String BASELINE_FOLDER_NOT_SET = "34";

   @RBEntry("The Class has no number property")
   public static final String NUMBER_MISSING = "35";

   @RBEntry("The Class has no name property")
   public static final String NAME_MISSING = "36";

   @RBEntry("{0}")
   public static final String NUMBER = "37";

   @RBEntry("{0}")
   public static final String NAME = "38";

   @RBEntry("The value for the argument \"{0}\" can not be null")
   public static final String NULL_ARGUMENT = "39";

   @RBEntry("An EPMDocument can not have Build Rules and an Effectivity")
   public static final String BUILD_EFFECTIVITY_CONFLICT = "40";

   @RBEntry("Cannot Modify or delete this reference designator because it is owned by the application \"{0}\"")
   public static final String REFERENCE_DESIGNATOR_MODIFY_OR_DELETE_ON_BUILT_LINK = "41";

   @RBEntry("Must provide an identifier for this reference designator")
   public static final String MUST_PROVIDE_IDENTIFICATION_TAG = "42";

   @RBEntry("Cannot revise {0} because it is checked out.")
   public static final String CANNOT_REVISE_CHECKED_OUT_OBJECT = "43";

   @RBEntry("Cannot change CAD Name of {0} because {1} is in {2} ({3}).")
   @RBComment("Error message to show when document whose CADName is being changed is in workspace")
   @RBArgComment0("EPMDocumentMaster whose CADName is being changed")
   @RBArgComment1("Document which is in workspace")
   @RBArgComment2("Workspace name containing (arg1)")
   @RBArgComment3("Workspace owner name.")
   public static final String CANNOT_RENUMBER_IN_WORKSPACE = "44";

   @RBEntry("Cannot change CAD Name of {0} because content {1} already used.")
   public static final String NEW_NUMBER_IN_USE = "45";

   @RBEntry("Cannot change CAD Name of {0} because new CAD name {1} has an extension which does not match the old.")
   public static final String INVALID_EXTENSION = "46";

   @RBEntry("Cannot change CAD Name of {0} because iteration {1} has been released.")
   public static final String RELEASED_ITERATION = "47";

   @RBEntry("Cannot change CAD Name of {0} because {1} is checked out by {2}.")
   @RBComment("Error message to show when document whose CADName is being changed is checkedout")
   @RBArgComment0("EPMDocumentMaster whose CADName is being changed")
   @RBArgComment1("Document which is checked-out")
   @RBArgComment2("User name to whom document (arg1) is checked out")
   public static final String CANNOT_RENUMBER_IF_CHECKED_OUT = "48";

   @RBEntry("Cannot decrease the quantity on this link to less than the number of occurrences on this link, which is {0}.")
   public static final String CANNOT_DECREASE_LINK_QUANTITY = "49";

   @RBEntry("Cannot add another occurrence to this link. It already has {0} and only allows {0}.")
   public static final String ONE_TOO_MANY_OCCURRENCES = "50";

   @RBEntry("Cannot add {0} occurrences to this link. It only allows {1}.")
   public static final String TOO_MANY_OCCURRENCES = "51";

   @RBEntry("CAD documents with Authoring application {0} can not have File Name.")
   @RBArgComment0("Authoring Application value")
   public static final String FILE_NAME_NOT_ALLOWED = "52";

   @RBEntry("CAD document / Dynamic Document must have a non null File Name.")
   @RBArgComment0("Identity of EPMDocumentMaster")
   public static final String FILE_NAME_REQUIRED = "53";

   @RBEntry("Unable to get delegate for Authoring application {0}. Verify entries in service.properties for wt.epm.CADNameDelegate.")
   @RBArgComment0("Authoring Application value")
   public static final String AUTH_APP_DELEGATE_NOT_FOUND = "54";

   @RBEntry("()")
   @RBComment("Error message to show CADName + CADContext is not unique - use when CAD name, CAD Context are null")
   public static final String CONTEXTNAME_BOTH_NULL = "55";

   @RBEntry("({0})")
   @RBComment("Error message to show CADName + CADContext is not unique -use when there is no CAD context")
   @RBArgComment0("CAD name value")
   public static final String CONTEXTNAME_CONTEXT_NULL = "56";

   @RBEntry("{0} ()")
   @RBComment("Error message to show CADName + CADContext is not unique -use when there is no CAD name")
   @RBArgComment0("CAD context value")
   public static final String CONTEXTNAME_NAME_NULL = "57";

   @RBEntry("{0} ({1})")
   @RBComment("Error message to show CADName + CADContext is not unique - Use when there is CAD Name and CAD Context")
   @RBArgComment0("CAD context value")
   @RBArgComment1("CAD name value")
   public static final String CONTEXTNAME = "58";

   @RBEntry("{0} cannot be contained in {1}. {0} is already a container.")
   @RBComment("Error message to show that a container document cannot become contained in another container document.")
   @RBArgComment0("Document that is already a container document")
   @RBArgComment1("Container Document")
   public static final String CANNOT_BECOME_CONTAINED = "59";

   @RBEntry("{0} cannot be contained in {1}.  {0} is already contained in {2}.")
   @RBComment("Error message to show when contained document is already contained in some other container")
   @RBArgComment0("Contained Document")
   @RBArgComment1("Container Document")
   @RBArgComment2("Another Container Document that already contains the contained Document (arg0)")
   public static final String CANNOT_BE_IN_TWO_CONTAINERS = "60";

   @RBEntry("Cannot check in {0}. User must also check in {1}.")
   @RBComment("Error message to show when a checkin dependency exists for a document")
   @RBArgComment0("Name of the EPMDocument being checked in")
   @RBArgComment1("Name of the EPMDocument which also must be checked in")
   public static final String MUST_CHECK_IN_DEPENDENT = "61";

   @RBEntry("Cannot undo checkout of {0}. User must also undo checkout of {1}.")
   @RBComment("Error message to show when a undoCheckout dependency exists for a document")
   @RBArgComment0("Name of the EPMDocument being checked in")
   @RBArgComment1("Name of the EPMDocument which also must be checked in")
   public static final String MUST_UNDO_CHECKOUT_OF_DEPENDENT = "62";

   @RBEntry("Can not revise a newly created document.")
   @RBComment("Error message to show when a newly created document is being revised")
   public static final String CANNOT_REVISE_NEW_DOCUMENT = "63";

   @RBEntry("Cannot move {0} to a shared folder. It is contained in document ({1}) which is checked out or in a personal folder.")
   @RBComment("Error message to show when user attempts to move a contained document to shared folder while its container is checked out or in a personal folder. User must first check in or move the container document to a shared folder.")
   @RBArgComment0("Name of the contained document being moved to shared folder")
   @RBArgComment1("Name of the container document for arg0")
   public static final String CANNOT_MOVE_CONTAINED_DOCUMENT = "64";

   @RBEntry("Cannot set geometryModified flag to false.  EPMContainedLink between container {0} and contained object {1} is already marked as modified.")
   @RBComment("Error message to show when user attempts to set the geometryModified flag to false on an EPMContainedLink which is already marked as true.")
   @RBArgComment0("Name of the container document associated to the link")
   @RBArgComment1("Name of the contained document associated to the link")
   public static final String CANNOT_RESET_GEOMETRY_MODIFIED_FLAG_TO_FALSE = "65";

   @RBEntry("Cannot set geometryModified flag to true.  EPMContainedLink between container {0} and contained object {1} is superceded.")
   @RBComment("Error message to show when user attempts to set the geometryModified flag to true on a superceded EPMContainedLink.")
   @RBArgComment0("Name of the container document associated to the link")
   @RBArgComment1("Name of the contained document associated to the link")
   public static final String SUPERCEDED_CANNOT_CHANGE_GEOMETRY_MODIFIED_FLAG = "66";

   @RBEntry("{0} is a generic.  It may not become a standalone CADDocument")
   @RBComment("Error message indicating that a generic cannot be made into a standalone CADDocument")
   @RBArgComment0("Name of the generic EPMDocument")
   public static final String CANNOT_MAKE_GENERIC_STANDALONE = "67";

   @RBEntry("Can not make {0} as varaint of {2}.  {0} is already a variant of {1}.")
   @RBComment("Error message to show when varaint document is already variant of some other generic")
   @RBArgComment0("variant Document")
   @RBArgComment1("Another Generic Document that already  is generic  for variant(arg0)")
   @RBArgComment2("Generic Document")
   public static final String CANNOT_MAKE_AS_VARIANT = "68";

   @RBEntry("Cannot branch {0}. Can not branch container/contained document.")
   @RBComment("Error message to show when user tries to branch document that is either a generic or instance")
   @RBArgComment0("Name of the EPMDocument being branched")
   public static final String CANNOT_BRANCH_DOCUMENT = "69";

   @RBEntry("{0} can not reference {1}.  CAD documents can not reference {2} objects.")
   @RBArgComment0("Identity of the CAD document")
   @RBArgComment1("Identity of the object that the CAD document wants to reference")
   @RBArgComment2("Type of the object that the CAD document wants to reference")
   public static final String INVALID_REFERENCES = "70";

   @RBEntry("Incomplete EPM Family {0} can not be revised.")
   @RBArgComment0("Identity of the EPMFamily(top level generic EPMDocument)")
   public static final String CANNOT_REVISE_INCOMPLETE_FAMILY = "71";

   @RBEntry("Cannot copy {0}. Can not copy container/contained document.")
   @RBComment("Error message to show when user tries to copy document that is either a generic or instance")
   @RBArgComment0("Name of the EPMDocument being copied")
   public static final String CANNOT_COPY_DOCUMENT = "72";

   @RBEntry("{0} is a contained object.  It may not be deleted.")
   @RBComment("Error message indicating that a contained CADDocument (family instance) cannot be deleted")
   @RBArgComment0("Name of the contained EPMDocument")
   public static final String CANNOT_DELETE_CONTAINED_DOCUMENT = "73";

   @RBEntry("The instance {0} has later iterations which are no longer part of a family.  These standalone iterations must be rolled back before deleting or rolling back the family {1}.")
   @RBComment("Error message indicating that a contained CADDocument (family instance) has later iterations which are standalone.  The standalone iterations must be rolled back before deleting the family.")
   @RBArgComment0("Name of the contained EPMDocument (instance)")
   @RBArgComment1("Name of the container (family)")
   public static final String MUST_ROLLBACK_STANDALONE_INSTANCE = "74";

   @RBEntry("Cannot make {0} standalone document. Remove {0} from {1} before making {0} standalone.")
   @RBComment("Error message indicating that a instance can not be made standalone if it exists in latest generic. User should use removeFromFamily before using makeStandalone.")
   @RBArgComment0("Name of the instance document that user tried to makeStandalone")
   @RBArgComment1("Name of the latest generic document that contains this instance")
   public static final String CANNOT_MAKE_STANDALONE_REMOVE_FROM_LATEST = "75";

   @RBEntry("Cannot make {0} standalone document. You need to check in {1} first.")
   @RBComment("Error message indicating that a instance can not be made standalone unless the latest generic is checked in.")
   @RBArgComment0("Name of the instance document that user tried to makeStandalone")
   @RBArgComment1("Name of the checked out latest generic document that contains this instance")
   public static final String CANNOT_MAKE_STANDALONE_CHECKIN_LATEST = "76";

   @RBEntry("{0} cannot be contained in {1}.  {1} is already contained in {2}.")
   @RBComment("Error message to show that contained document cannot become a container.")
   @RBArgComment0("Contained Document")
   @RBArgComment1("Another Contained Document that is already contained in (arg2)")
   @RBArgComment2("Container Document")
   public static final String CANNOT_BECOME_CONTAINER = "77";

   @RBEntry("{0} cannot contained itself.")
   @RBComment("Error message to show that a document cannot contain itself.")
   @RBArgComment0("Document")
   public static final String CANNOT_CONTAIN_ITSELF = "78";

   @RBEntry("This iteration of {0} already has a Contained In link to this iteration of {1}.")
   @RBComment("Error message to show that an iteration of the contained document cannot have multiple Contained In links to the same iteration of its container document.")
   @RBArgComment0("Contained Document")
   @RBArgComment1("Container Document")
   public static final String NO_DUPLICATE_CONTAINEDIN_LINKS = "79";

   @RBEntry("{0} cannot be contained in {1}.  {1} already contains a later version of the same document.")
   @RBComment("Error message to show that you cannot add an earlier version of document to a container that already contains a later version of the same document.")
   @RBArgComment0("Contained Document (earlier version)")
   @RBArgComment1("Container Document")
   public static final String CONTAINER_HAS_LATER_VERSION = "80";

   @RBEntry("{0} cannot be contained in {1}.  {0} is already contained in a later version of the same container.")
   @RBComment("Error message to show that add a document to a earlier version of its container if it is already contained in a later version.")
   @RBArgComment0("Contained Document")
   @RBArgComment1("Container Document (earlier version)")
   public static final String CONTAINED_IN_LATER_VERSION = "81";

   @RBEntry("Cannot change CAD Name of {0} because document is checked out by {1}.")
   @RBComment("Error message to show that you cannot change CADName of a document if it is checked out by another user.")
   @RBArgComment0("Identity of EPMDocumentMaster")
   @RBArgComment1("User name to whom document is checked out")
   public static final String CANNOT_RENUMBER_IF_CHECKED_OUT_BY_OTHERS = "82";

   @RBEntry("Cannot change CAD Name of {0} because you do not have modify permission on the master.")
   @RBComment("Error message to show that you cannot change CADName of a document if more than one versions of the document are checked out to you.")
   @RBArgComment0("Identity of EPMDocumentMaster")
   public static final String NO_MODIFY_PERMISSION_TO_CHANGE_CAD_NAME = "83";

   @RBEntry("Cannot create place holder document {0} in shared folder {1}.")
   @RBComment("Error message to show that you cannot create a place holder document in a shared folder.")
   @RBArgComment0("Identity of EPMDocument")
   public static final String PLACEHODER_NOT_ALLOWED_IN_SHARED_FOLDER = "84";

   @RBEntry("Cannot check in place holder document {0}.")
   @RBComment("Error message to show that you cannot move a place holder document from personal folder to a shared folder.")
   public static final String CANNOT_MOVE_PLACEHODER_TO_SHARED_FOLDER = "85";

   @RBEntry("Cannot change existing document {0} to place holder.")
   @RBComment("Error message to show that you cannot change an existing document to a place holder.")
   @RBArgComment0("Identity of EPMDocument")
   public static final String CANNOT_CHANGE_TO_PLACEHOLDER = "86";

   @RBEntry("Cannot move {0} to shared folder because it depends on place holder {1}.")
   @RBComment("Error message to show that you cannot move an assembly to shared folder if it depends on a place holder.")
   @RBArgComment0("Identity of EPMDocument")
   public static final String CANNOT_VAULT_ASSEMBLY_DEPENDENT_ON_PLACEHOLDER = "87";

   @RBEntry("Cannot change CAD Name of {0} because you do not have modify permission to one or more iterations.")
   @RBComment("Error message to show that you cannot change CADName of a document as you do not have modify access to one or more iterations.")
   @RBArgComment0("Identity of EPMDocumentMaster")
   public static final String CANNOT_RENUMBER_IF_NO_ACCESS_TO_ITERATION = "88";

   @RBEntry("An owner of an as-stored configuration can only be an EPMDocument.")
   @RBComment("Error message to show that you can only add EPMDocument to an as-stored configuration as owner.")
   public static final String OWNER_MUST_BE_EPMDOCUMENT = "89";

   @RBEntry("A member of an as-stored configuration can only be a WTDocument or EPMDocument.")
   @RBComment("Error message to show that you can only add EPMDocument or WTDocument to an as-stored configuration as member.")
   public static final String MEMBER_MUST_BE_DOCUMENT = "90";

   @RBEntry("Cannot add objects that are checked out to an as-stored configuration.")
   @RBComment("Error message to show that you can only add EPMDocument or WTDocument that are not checked out to an as-stored configuration.")
   public static final String CANNOT_ADD_WORKING_COPIES_TO_AS_STORED = "91";

   @RBEntry("Cannot delete EPMAsStoredConfig because you don't have modify permission on all owners in the as-stored configuration.")
   @RBComment("Error message to show that the user does not have the permission to modify all the owners in the as-stored configuration.")
   public static final String CANNOT_DELETE_AS_STORED_CONFIG = "92";

   @RBEntry("cannot add the same object to an as-stored configuration twice, either as owner or member.")
   @RBComment("Error message to show that you cannot add the same object to an as-stored configuration twice, either as owner or member.")
   public static final String CANNOT_ADD_OBJECT_TWICE_TO_AS_STORED = "93";

   @RBEntry("cannot add two objects with the same master to an as-stored configuration, either as owner or member.")
   @RBComment("Error message to show that you cannot add two objects with the same master to an as-stored configuration, either as owner or member.")
   public static final String CANNOT_ADD_OBJECTS_WITH_SAME_MASTER_TO_AS_STORED = "94";

   @RBEntry("Cannot add owners that already have an As-Stored configuration associated to a new as-stored configuration.")
   @RBComment("Error message to show that the owners already have an associated as-stored configurations, therefore, they cannot be added to a new as-stored configuration.")
   public static final String OWNERS_ALREADY_HAVE_AS_STORED = "95";

   @RBEntry("Cannot change Workspace container of {0}.Workspace cannot be used to work with {1}")
   @RBComment("Error message to show that you cannot change WTContainer associated with the EPMWorkspace.")
   @RBArgComment0("Identity of EPMWorkspace")
   @RBArgComment1("Identity of new WTContainer")
   public static final String CANNOT_CHANGE_WS_CONTAINER = "96";

   @RBEntry("Cannot add {0} to Workspace {1}. Workspace container {2} can not share objects from {3}.")
   @RBComment("Error message to show that you cannot add given object to EPMWorkspace.")
   @RBArgComment0("Identity of object that user tried to add to EPMWorkspace")
   @RBArgComment1("Identity of EPMWorkspace")
   @RBArgComment2("Identity of WTContainer currently associated with EPMWorkspace.")
   @RBArgComment3("Identity of WTContainer currently associated with object.")
   public static final String CANNOT_ADD_TO_WS_CONTAINER = "97";

   @RBEntry("Cannot create link {0}. {1} cannot share objects from {2}.")
   @RBComment("Error message to show that you cannot link across these WTContainers/Solutions.")
   @RBArgComment0("Class name of the link that user tried to create")
   @RBArgComment1("WTContainer for the Role A")
   @RBArgComment2("WTContainer for the Role B")
   public static final String CANNOT_CREATE_CROSS_CONTAINER_LINK = "98";

   @RBEntry("Cannot copy because EPMDocuments to be copied contain incomplete and/or incompatible family for generic {0}.")
   @RBComment("Error message to show during multi-object copy if the set of objects contain incomplete or incompatible family table.")
   @RBArgComment0("Identity of generic EPMDocument")
   public static final String CANNOT_COPY_FAMILY_INCOMPLETE = "99";

   @RBEntry("Cannot copy because EPMDocuments to be copied contain incomplete and/or incompatible family for instance(s) {0}.")
   @RBComment("Error message to show during multi-object copy if the set of objects contain incomplete or incompatible family table.")
   @RBArgComment0("Comma seperated Identities of instance EPMDocuments")
   public static final String CANNOT_COPY_FAMILY_INCOMPLETE_INSTANCE = "100";

   @RBEntry("The copyRelationship method for single object is not supported for links of class {0}.")
   @RBComment("Error message to show when single object copyRelationship method is called.")
   @RBArgComment0("Link class name")
   public static final String NOT_SUPPORTED_COPY_RELATIONSHIP = "101";

   @RBEntry("Cannot handle objects from following class for deletion : {0}.")
   @RBComment("Error message to show, when objects other than WTPart and EPMDocument, are selected for deletion.")
   @RBArgComment0("Class name of the object selected by the user.")
   public static final String CANNOT_HANDLE_OBJECTS_FOR_DELETION = "102";

   @RBEntry("Select latest iterations for deletion. Cannot delete intermediate iterations.")
   @RBComment("Error message to show, when intermediate iterations of the objects are selected for deletion.")
   public static final String CANNOT_DELETE_INTERMEDIATE_ITERATIONS = "103";

   @RBEntry("Instance document {0}, of Generic document {1}, missing in the list of objects to be deleted.")
   @RBComment("Error message to show during multi-delete when generic is deleted without its instance.")
   @RBArgComment0("Identity of instance EPMDocument")
   @RBArgComment1("Identity of generic EPMDocument")
   public static final String INSTANCE_MISSING_IN_DELETE = "104";

   @RBEntry("Generic document {0}, of Instance document {1}, missing in the list of objects to be deleted.")
   @RBComment("Error message to show during multi-delete when instance is deleted without its generic.")
   @RBArgComment0("Identity of generic EPMDocument")
   @RBArgComment1("Identity of instance EPMDocument")
   public static final String GENERIC_MISSING_IN_DELETE = "105";

   @RBEntry("Deletion of {0} failed.")
   @RBComment("Error message to show when error occurs during deleting.")
   @RBArgComment0("Identity of EPMDocument/WTPart/WTDocument")
   public static final String ERROR_IN_DELETE = "106";

   @RBEntry("Cannot add one or more of the documents to {0} because their cad names are not unique in {0}.  The duplicate CAD names are {1}.")
   @RBComment("Error message to show when user tries to create/share CADDocument and CADDocument with same already exists in Sandbox")
   @RBArgComment0("Identity of the Project (Sandbox Container)")
   @RBArgComment1("List of duplicate CADNames of the document")
   public static final String DUPLICATE_FILE_NAME = "107";

   @RBEntry("Cannot checkin CAD document / Dynamic Document from {0}. PDM Container already has CAD document / Dynamic Document with File Name {1}.")
   @RBComment("Error message to show when user tries to checkin CADDocument and CADDocument with same CADName already exists in the PDM.")
   @RBArgComment0("Identity of the Project (Sandbox Container)")
   @RBArgComment1("CADName of the document")
   public static final String DUPLICATE_FILE_NAME_IN_SB_CHECKIN = "108";

   @RBEntry("Cannot modify Share of {0}. It is in a Workspace in Project {1}.")
   @RBComment("Error message for removing/switching/disabling share to an object in a Workspace of the given Project")
   @RBArgComment0("Identity of the object")
   @RBArgComment1("Identity of the Project (Sandbox Container)")
   public static final String MODIFY_SHARE = "109";

   @RBEntry("Cannot check in or undo checkout {0}. It requires {1}.")
   @RBComment("Error message for SandBox Checkin/undoCheckout of an EPMDocument with build dependencies")
   @RBArgComment0("Identity of the EPMDocument")
   @RBArgComment1("Identity of the WTPart/Image document")
   public static final String SBCHECKIN_BUILD_DEPENDENT = "110";

   @RBEntry("Cannot check in or undo checkout {0}. It has a build dependency on {1}.")
   @RBComment("Error message for SandBox Checkin/undoCheckout of a WTPart/Image Document which has been built.")
   @RBArgComment0("Identity of the WTPart/Image Document")
   @RBArgComment1("Identity of the EPMDocument")
   public static final String SBCHECKIN_BUILT_DEPENDENT = "111";

   @RBEntry("Cannot checkin or undo checkout of CAD document {0}. It is part of a family with other modified member: {1}")
   @RBComment("Error message for SandBox Checkin/undoCheckout of an incomplete family")
   @RBArgComment0("Name of the EPMDocument being processed")
   public static final String SBCHECKIN_INCOMPLETE_FAMILY = "112";

   @RBEntry("Cannot check in {0}. User must also check in its optional dependent {1}.")
   @RBComment("Error message to show when user tries to Checkin EPMDocument without its Optional Dependents, when it is enforced to include its Optional Dependents.")
   @RBArgComment0("Name of the EPMDocument being checked in")
   @RBArgComment1("Name of the EPMDocument which also must be checked in")
   public static final String MUST_CHECK_IN_OPTIONAL_DEPENDENT = "113";

   @RBEntry("Cannot update the workspace {0}. It would cause the family with generic {1} to become incompatible.")
   @RBComment("Error message for update of a Workspace causing an incompatible family to exist in the workspace")
   @RBArgComment0("Name of the EPMWorkspace being updated.")
   @RBArgComment1("Name of the EPMDocument representing the generic of the incompatible family.")
   public static final String INCOMPATIBLE_FAMILY = "114";

   @RBEntry("Cannot mark for delete project {0}. The project has associated workspaces: {1}.")
   @RBComment("Error message for marking for delete a project with associated workspace(s)")
   @RBArgComment0("project name")
   @RBArgComment1("list of associated workspaces")
   public static final String DELETE_WORKSPACES_PRIOR_TO_PROJECT_DELETE = "115";

   @RBEntry("{0} is part of a family, some members of which cannot be accessed by you.")
   @RBComment("Error message for document which is part of a family, cannot be accessed by the current user.")
   @RBArgComment0("Name of the document")
   public static final String PART_OF_INACCESSIBLE_FAMILY = "116";

   @RBEntry("Cannot delete {0} because it is associated to workspace {1}")
   @RBComment("Error message for deleting a configspec with associated workspace(s)")
   public static final String CAN_NOT_DELETE_CONFIGSPEC_ASSOCIATE_TO_WORKSPACE = "117";

   @RBEntry("Project to Project sharing of CAD documents / Dynamic Documents is not supported.")
   @RBComment("Error message when the user tries to share CAD documents from project to project.")
   public static final String PROJECT_TO_PROJECT_SHARING_NOT_SUPPORTED = "118";

   @RBEntry("Revise of part to shared folder {0} and associated document to personal folder {1} is not allowed.")
   @RBComment("Error message for revise of docs and parts because of incompatible folder options")
   @RBArgComment0("Path of the part folder.")
   @RBArgComment1("Path of the document folder.")
   public static final String CANNOT_REVISE_INCOMPATIBLE_FOLDER_OPTIONS = "119";

   @RBEntry("Cannot add one or more of the following CAD documents to {0}, {1}.  One or more of the File names of the documents' dependents, instances or generics are not unique in {0}.  The duplicate CAD names are {2}.")
   @RBComment("Error message for Duplicate CADName for Dependents")
   @RBArgComment0("Identity of the Project (Sandbox Container)")
   @RBArgComment1("List of CAD documents' CAD names.")
   @RBArgComment2("List of dependent documents' CADNames")
   public static final String DUPLICATE_FILE_NAME_FOR_DEPENDENT = "120";

   @RBEntry("ReviseOptions.objectToRevise cannot be set to null when calling reviseAll")
   @RBComment("Error message for ReviseOptions.objectToRevise set to null in reviseAll")
   public static final String REVISEOPTIONS_OBJECTTOREVISE_CANNOT_BE_NULL = "121";

   @RBEntry("Only EPMDocuments and WTParts are supported in reviseAll")
   @RBComment("Error message for reviseAll only supports EPMDocuments and WTParts")
   public static final String ONLY_EPMDOCUMENTS_AND_WTPARTS_SUPPORTED_FOR_REVISEALL = "122";

   @RBEntry("Cannot checkin {0} because it is new and has been removed from family {1}")
   @RBComment("Error message for SBCheckin of new instance removed from family")
   public static final String CAN_NOT_SBCHECKIN_ORPHANED_INSTANCE = "123";

   @RBEntry("{0} is not compatible with the authoring application, {1}.")
   @RBComment("Error message for setting an incompatible Authoring App Version onto EPMDocument or EPMSupportingData.  That is, the version's authoring application is different from the authoring application on document or supporting data.")
   @RBArgComment0("Value of the Authoring Application Version.")
   @RBArgComment1("Value of the Authoring Application on EPMDocument or EPMSupportingData.")
   public static final String IMCOMPATIBLE_AUTHORING_APP = "124";

   @RBEntry("The following Authoring Application Version is not definied in the system, {0} {1} {2}")
   @RBComment("Error message at import if the authoring application is not defined in the target Windchill.  Authoring Application, Sequence Number and an internal string identifies the version of an Authoring Application.")
   @RBArgComment0("Authoring Application value")
   @RBArgComment1("Authoring Application's version string")
   @RBArgComment2("Authoring Application's version's sequence number")
   public static final String AUTHORING_APP_VERSION_NOT_EXIST = "125";

   @RBEntry("CAD documents / Dynamic Documents cannot be checked in because their file names are not unique in PDM.  The duplicate file names are {0}.")
   @RBComment("Error message at checking multiple EPMDocument from a sandbox project to PDM.  A subset of the EPMDocuments cannot be checked in to PDM because their CAD name or file name are not unique in PDM.  Therefore, the multi-object sandbox check in fails.")
   @RBArgComment0("List of duplicate CAD names")
   public static final String NON_UNIQUE_CAD_NAME_IN_PDM_FOUND_AT_SBCI = "126";

   @RBEntry("CAD documents / Dynamic Documents cannot be checked in because their new file names are not unique in {0}.  The duplicate file names are {1}")
   @RBComment("Error message at checking multiple EPMDocument with new CAD names from a sandbox project to PDM.  The EPMDocument with new CAD names cannot be shared back to sandbox project because their new CAD names are not unique in the sandbox.  Thus, the check in operation fails.")
   @RBArgComment0("Identity of the Sandbox project")
   @RBArgComment1("List of duplicate CAD names")
   public static final String NON_UNIQUE_CAD_NAME_IN_SB_FOUND_AT_SBCI = "127";

   @RBEntry("Cannot checkin of CAD document {0}. It requires WTPart {1} which was disassociated from {0} in Project.")
   @RBComment("Error message for SandBox Checkin of an EPMDocument without build dependencies")
   @RBArgComment0("Identity of the EPMDocument")
   @RBArgComment1("Identity of the WTPart")
   public static final String SBCHECKIN_NO_BUILD_DEPENDENT = "128";

   @RBEntry("System provided EPM type of {0} cannot be deleted.")
   @RBComment("Error message for Deleting EPM SoftType")
   @RBArgComment0("Identity of the Soft Type")
   public static final String CANNOT_DELETE_EPM_SOFTTYPE = "129";

   @RBEntry("{0} contains attribute {1} that is not part of type definition.")
   @RBComment("Error message when EPM Object contains attribute not existing in default type definition")
   @RBArgComment0("Identity of the object")
   @RBArgComment1("Attribute Name")
   public static final String ATTR_NOT_IN_SOFTTYPE = "130";

   @RBEntry("Cannot delete CAD document / Dynamic Document {0} because it is in following workspace(s): {1}. Deleting doc in workspace is not allowed for {2} authoring application.")
   @RBComment("Error message for deleting EPMDocument in workspace when it is not allowed by authoring application delegate")
   @RBArgComment0("Identity of the EPMDocument")
   @RBArgComment1("Names of the workspaces in which the document exists.")
   @RBArgComment2("Authoring Application value")
   public static final String DO_NOT_ALLOW_DELETE_IN_WORKSPACE = "131";

   @RBEntry("{0} cannot have more than one type.")
   @RBComment("Error message for Creating another SoftType")
   @RBArgComment0("Identity of the Soft Type")
   public static final String CANNOT_CREATE_ANOTHER_SOFTTYPE = "132";

   @RBEntry("{0} cannot be cut from {1} and pasted to {2}. Cut from / Paste to EPM nodes is not allowed.")
   @RBComment("Error message for vetoing cut/paste of EPM SoftType.")
   @RBArgComment0("Name of Type definition being pasted")
   @RBArgComment1("Cut from type definition name")
   @RBArgComment2("Paste to type definition name")
   public static final String CANNOT_CUTPASTE_EPM_SOFTTYPE = "133";

   @RBEntry("Cannot delete Single Valued Constraint for EPM Type {0}.")
   @RBComment("Error message for vetoing EPM SoftType with missing single value attribute constraint.")
   @RBArgComment0("Name of type definition")
   public static final String MISSING_SINGLE_VALUE_ATTRIBUTE_CONSTRAINT = "134";

   @RBEntry("Either one Range Constraint or one Discrete Set Constraint allowed for EPM Type {0}.")
   @RBComment("Error message for vetoing EPM SoftType with conflicting constraint.")
   @RBArgComment0("Name of type definition")
   public static final String CONFLICTING_CONSTRAINT = "135";

   @RBEntry("New object can only be WTPart, EPMDocument or their subclasses.")
   @RBComment("Error message for adding new object of something other than EPMDocument or WTPart.")
   public static final String INVALID_NEW_OBJECT_CLASS = "136";

   @RBEntry("Cannot set target folder for objects that are not newly created.  {0} out of {1} objects are not new.")
   @RBComment("Error message for setting target folder for objects that are not newly created")
   @RBArgComment0("Number of objects that are not new")
   @RBArgComment1("Number of objects passed to set target folder.")
   public static final String SET_TARGET_FOLDER_FOR_NEW_OBJECTS_ONLY = "137";

   @RBEntry("Cannot get target folder for objects that are not newly created EPMDocument or WTPart.  {0} out of {1} objects are not new EPMDocument or WTPart.")
   @RBComment("Error message for getting target folder for objects that are not newly created EPMDocument or WTPart.")
   @RBArgComment0("Number of objects that are not new EPMDocument or WTPart.")
   @RBArgComment1("Number of objects passed to get target folder.")
   public static final String GET_TARGET_FOLDER_FOR_NEW_OBJECTS_ONLY = "138";

   @RBEntry("Cannot add \"{0}\" to baseline because two objects with the same master cannot be added to a baseline.")
   public static final String SAME_MASTER_NOT_ALLOWED = "139";

   @RBEntry("Family Table conflict during checkin")
   public static final String CHECKIN_FAMILY_TABLE_CONFLICT = "140";

   @RBEntry(" {0} : Family Table cannot be copied because one or more immediate or intermediate generics are missing in Save As list. The following generic(s) need to be added to the Save As list {1}")
   @RBComment("Error message to show during multi-object copy when user tries to copy family with one or more immediate or intermediate generics missing in Save As list.")
   @RBArgComment0("Original Family Table Object Name.")
   @RBArgComment1("List of missing immediate or intermediate generics.")
   public static final String CANNOT_COPY_FAMILY_WITHOUT_INTERMEDIATE_GENERIC = "141";

   @RBEntry("Cannot copy family members {0} to existing family table {1} because one or more generics are checked out. The following generic(s) need to be checked in. {2}")
   @RBComment("Error message to show during single or multi-object copy when user tries to copy instance(s) with immediate generic or intermediate generic or top generic checked out.")
   @RBArgComment0("List of family members selected for copy")
   @RBArgComment1("Original Family Table Object Name.")
   @RBArgComment2("List of immediate generics or intermediate generics or top generic checked out")
   public static final String CANNOT_COPY_INSTANCE_WHEN_GENERIC_CHECKEDOUT = "142";

   @RBEntry(" Cannot copy family members {0} to existing family table {1} because one or more immediate or intermediate generics are missing in Save As list. The following generic(s) need to be added to the Save As list {2}")
   @RBComment("Error message to show during multi-object copy when user tries to copy family with one or more immediate or intermediate generics missing in Save As list.")
   @RBArgComment0("List of family members selected for copy")
   @RBArgComment1("Original Family Table Object Name.")
   @RBArgComment2("List of missing immediate or intermediate generics.")
   public static final String CANNOT_COPY_NESTEDGENERIC_WITHOUT_INTERMEDIATE_GENERIC = "143";

   @RBEntry("Unable to change CAD name for the given documents.  Some document may be checked out by others or you don't have modify permission for all versions of some document.")
   @RBComment("Error message for changing the CAD name of multiple documents' masters.")
   public static final String NO_MODIFY_PERMISSION_ON_ALL_VERSIONS_TO_CHANGE_CAD_NAME = "144";

   @RBEntry("Cannot change CAD Name for {0} from {1} to {2}.  The new CAD name must have the same extension.")
   @RBComment("Error message for renaming a file name of a CAD document.")
   @RBArgComment0("Identity of EPMDocumentMaster")
   @RBArgComment1("Old CAD name")
   @RBArgComment2("New CAD name")
   public static final String INVALID_CAD_NAME_EXTENSION = "145";

   @RBEntry("Cannot delete {0} because it is used by {1}")
   @RBArgComment0("Display identity of the part node being deleted.")
   @RBArgComment1("Display identity of the assembly node which uses the part node.")
   public static final String CANNOT_DELETE_USED_BY = "146";

   @RBEntry("Cannot delete {0} because it is referenced by {1}")
   @RBArgComment0("Display identity of the document being deleted.")
   @RBArgComment1("Display identity of the document which references the document being deleted.")
   public static final String CANNOT_DELETE_REFERENCED_BY = "147";

   @RBEntry("Cannot delete {0} because it is generic for following variant(s) : {1}")
   @RBArgComment0("Display identity of the generic being deleted.")
   @RBArgComment1("Display identity of the variant.")
   public static final String CANNOT_DELETE_GENERIC = "148";

   @RBEntry("Cannot delete {0} because it is in following workspace(s) : {1}.")
   @RBArgComment0("Display identity of the document being deleted.")
   @RBArgComment1("Names of the workspaces in which the document exists.")
   public static final String OBJECT_IN_WORKSPACE = "149";

   @RBEntry("Cannot delete {0} because it is used by following supporting data : {1}")
   @RBArgComment0("Display identity of the authoring app version being deleted.")
   @RBArgComment1("Display identity of the supporting data which uses the app version being deleted.")
   public static final String CANNOT_DELETE_APP_DATA_VERSION_LINK = "150";

   @RBEntry("Cannot delete {0} because it is used by following CAD document(s) : {1}")
   @RBArgComment0("Display identity of the authoring app version being deleted.")
   @RBArgComment1("Display identity of the document which uses the app version being deleted.")
   public static final String CANNOT_DELETE_AUTHORING_APP_VERSION_LINK = "151";

   @RBEntry("Cannot delete {0} because it is associated to following workspace(s) : {1}")
   @RBArgComment0("Display identity of the checkpoint being deleted.")
   @RBArgComment1("Display identity of the workspace associated to the checkpoint being deleted.")
   public static final String CANNOT_DELETE_EPMCHECKPOINT = "152";

   @RBEntry("Parameter \"{0}\" of type \"{1}\" could not be uploaded to the server due to inconsistent type with Windchill parameter type \"{2}\".")
   @RBArgComment0("Parameter/Attribute Name.")
   @RBArgComment1("data type of parameter from CAD file.")
   @RBArgComment2("data type of IBA in Windchill.")
   public static final String INCONSISTENT_IBA_TYPE = "153";

   @RBEntry("{0} does not have an attribute container.")
   @RBArgComment0("Identity of object")
   public static final String NO_ATTR_CONTAINER = "154";

   @RBEntry("Cannot add one or more of the following CAD documents to {0}, {1}.  One or more of the CAD names of the documents, their dependents, their instances or their generics are not unique in {0}.  The duplicate CAD names are {2}.")
   @RBComment("Error message for a combination of duplicate CAD name and duplicate CAD name for Dependents")
   @RBArgComment0("List of Identity of documents.")
   @RBArgComment1("Identity of the Project (Sandbox Container)")
   @RBArgComment2("List of duplicate CADNames in the project.")
   public static final String DUPLICATE_DOCUMENT_AND_DEPENDENT_FILE_NAME = "155";

   @RBEntry("Incompatible documents found. Can not generate neutral data for incompatible documents.")
   @RBComment("Mostly a programming error. Exception message is logged if caller attempts to generate neutral data for incompatible set of documents/family table")
   public static final String CANNOT_GET_ND_FOR_INCOMPATIBLE_DOCUMENTS = "156";

   @RBEntry("Cannot move object to Shared folder.")
   @RBComment("Cannot move to Shared folder.")
   public static final String CANNOT_MOVE_TO_SHARED_FOLDER = "157";

   @RBEntry("Cannot checkin object to Shared folder.")
   @RBComment("Cannot checkin to Shared folder.")
   public static final String CANNOT_CHECKIN_TO_SHARED_FOLDER = "158";

   @RBEntry("Cannot remove share of {0} because it is in following workspace(s) : {1}.")
   @RBArgComment0("Display identity of the document that is being unshared.")
   @RBArgComment1("Identity of the workspaces in which the document exists.")
   public static final String SHARED_OBJECT_IN_PROJECT_WORKSPACE = "159";

   @RBEntry("Cannot import this feature definition, {0}, because it is different from the definition in the database.")
   @RBComment("Error message for import if the feature definition in the database is different from the xml file being imported.  This is an override conflict in Import/Export.")
   @RBArgComment0("Name of the feature definition.")
   public static final String CONFLICT_FTM_FEATURE_DEF = "160";

   @RBEntry("Cannot import this feature, {0}, because its feature definition, {1}, is not in the database.")
   @RBComment("Error message at import if the feature definition does not already exists in the database.")
   @RBArgComment0("Name of the feature.")
   @RBArgComment1("Name of the feature definition.")
   public static final String FTM_FEATURE_DEF_NOT_FOUND = "161";

   @RBEntry("Cannot import this parameter definition, {0}, because it is different from the definition in the database.")
   @RBComment("Error message at import if the parameter definition is different from the xml file being imported.  This is an override conflict in Import/Export.")
   @RBArgComment0("Name of the parameter definition.")
   public static final String CONFLICT_FTM_PARAMETER_DEF = "162";

   @RBEntry("Cannot import this parameter, {0}, because its parameter definition, {1}, is not in the database.")
   @RBComment("Error message at import if the feature definition with the given name is not found on the family table master in the database.")
   @RBArgComment0("Name of the parameter.")
   @RBArgComment1("Name of the parameter definition.")
   public static final String FTM_PARAMETER_DEF_NOT_FOUND = "163";

   @RBEntry("Cannot import the feature, {0}, on the family table, {1}, because the xml file does not contain a feature definition name.")
   @RBComment("Error message at import if the feature definition name is not in XML file for the family table object.")
   @RBArgComment0("Name of the family table feature.")
   @RBArgComment1("Identity of the family table.")
   public static final String FTM_FEATURE_DEF_NAME_NOT_FOUND = "164";

   @RBEntry("Cannot import the parameter, {0}, on the family table, {1}, because the xml file does not contain a parameter definition name.")
   @RBComment("Error message at import if the parameter definition name is not in XML file for the family table object.")
   @RBArgComment0("Name of the family table parameter.")
   @RBArgComment1("Identity of the family table.")
   public static final String FTM_PARAMETER_DEF_NAME_NOT_FOUND = "165";

   @RBEntry("Cannot create/modify/delete any EPMAuthoringAppVersion object.")
   @RBComment("Error message for adding/modifying/deleting any EPMAuthoringAppVersion object.  EPMAuthoringAppVersion is loaded via Windchill loader.")
   public static final String CANNOT_CHANGE_AUTHORING_APP_VERSION = "166";

   @RBEntry("Incoming attribute(s) {0} ignored as they are not part of type definition.")
   @RBComment("Warning message during upload when designated parameters could not be added as IBA values as IBAs are not part of type definition.")
   @RBArgComment0("List of Attributes")
   public static final String INCOMING_ATTR_IGNORED = "167";

   @RBEntry("Incoming attribute(s) {0} ignored as null value returned by attribute delegate.")
   @RBComment("Warning message during upload when no value is constructed by EPMAttributeDelegate for designated parameters and hence upload has ignored it.")
   @RBArgComment0("List of Attributes")
   public static final String IGNORED_BY_DELEGATE = "168";

   @RBEntry("Attribute {0} value is not in format \"{1}\" and could not be parsed to Date. Exception occurred: {2}")
   @RBComment("Warning message from attribute delegate when it fails to parse string value to date.")
   @RBArgComment0("Name of Attributes")
   @RBArgComment1("Format in which date is expected")
   @RBArgComment2("Message from ParseException")
   public static final String DATE_PARSE_WARN = "169";

   @RBEntry(" Dependency conflict")
   public static final String DEPENDENCY_CONFLICT = "170";

   @RBEntry("Attribute {0} value changed to lower case.")
   @RBComment("Warning message when IBA value is changed to lower case by server.")
   @RBArgComment0("Name of Attributes")
   public static final String CHANGED_TO_LOWER = "171";

   @RBEntry("Attribute {0} value changed to upper case.")
   @RBComment("Warning message when IBA value is changed to upper case by server.")
   @RBArgComment0("Name of Attributes")
   public static final String CHANGED_TO_UPPER = "172";

   @RBEntry("Attribute {0} value is not in format \"{1}\" and could not be parsed to Date.")
   @RBComment("Message from attribute delegate when it fails to parse string value to date.")
   @RBArgComment0("Name of Attributes")
   @RBArgComment1("Format in which date is expected")
   public static final String DATE_PARSE_ERROR = "174";

   @RBEntry(" Revise conflict")
   public static final String REVISE_CONFLICT = "175";

   @RBEntry(" Can not revise \"{0}\" - the iteration was created in or is checked out to a project.")
   @RBArgComment0("Name of an object being revised.")
   public static final String CANNOT_REVISE_PROJECT_ITERATION = "176";

   @RBEntry("Attribute {0} is not part of type definition.")
   @RBComment("Error message from setAttributes when IBA is not part of type definition.")
   @RBArgComment0("ExtHid of the attribute.")
   public static final String IBA_NOT_PART_OF_DEF = "177";

   @RBEntry("Attribute {0} included in the family table cannot be unset.")
   @RBComment("Error message from setAttributes when IBA that is part of family table is being unset")
   @RBArgComment0("ExtHid of the attribute.")
   public static final String IBA_PART_OF_FT_UNSET = "178";

   @RBEntry("Attribute {0} is not part of family table and cannot be modified directly in instance {1}. The attribute needs to be changed in top level generic.")
   @RBComment("Error message from setAttributes when IBA not part of family table is being modified directly at instance level.")
   @RBArgComment0("ExtHid of the attribute.")
   @RBArgComment1("Identity of instance EPMDocument.")
   public static final String IBA_NOT_PART_OF_FT = "179";

   @RBEntry("Attribute {0} has more than one value on {1}.")
   @RBComment("Error message from setAttributes when more than one value exists for given document.")
   @RBArgComment0("ExtHid of the attribute.")
   @RBArgComment1("Identity of EPMDocument.")
   public static final String IBA_MULTIPLE_VALUE = "180";

   @RBEntry("Value of type \"{0}\" cannot be set to attribute \"{1}\" of type \"{2}\".")
   @RBArgComment0("data type of value passed.")
   @RBArgComment1("Attribute Name.")
   @RBArgComment2("data type of IBA in Windchill.")
   public static final String INCONSISTENT_IBA_VALUE = "181";

   @RBEntry(" Attribute \"{0}\" of type \"{1}\" is not supported by setAttributes.")
   @RBArgComment0("Attribute Name.")
   @RBArgComment1("data type of value passed.")
   public static final String UNSUPPORTED_IBA_TYPE = "182";

   @RBEntry(" Copy in workspace is not supported for cad document(s) \"{0}\" authored by \"{1}\" authoring application(s).")
   @RBArgComment0("Identity of generic/instance EPMDocument")
   @RBArgComment1("Authoring Application value.")
   public static final String CANNOT_COPY_IN_WORKSPACE = "183";

   @RBEntry("Workspace(s) have been deleted by a user who has administrative privilege to their Workspace context.")
   public static final String WORKSPACE_DELETED_MAIL_SUB  = "184";

   @RBEntry("Delete Workspace Notification")
   public static final String WORKSPACE_DELETED_MAIL_MESSAGE = "185";

   @RBEntry("Workspace {0} has checked out objects.")
   @RBArgComment0("Name of workspace to be deleted.")
   public static final String CHECKED_OUT_OBJECT_IN_WORKSPACE = "186";

   @RBEntry("Workspace {0} has new objects.")
   @RBArgComment0("Name of workspace to be deleted.")
   public static final String NEW_OBJECT_IN_WORKSPACE = "187";

   @RBEntry("One or more objects being copied to workspace are not in workspace \"{0}\". All objects being copied to workspace should exist in the workspace.")
   @RBArgComment0("Identity of workspace")
   public static final String ALL_OBJECTS_NOT_IN_WORKSPACE = "188";

   @RBEntry("One or more of objects passed to updateParent is null.")
   public static final String NOT_PROPER_REPLACE_DATA = "189";

   @RBEntry("Cannot replace \"{0}\" with \"{1}\", as their document subcategories are different.")
   @RBArgComment0("original document identity")
   @RBArgComment1("replace with document identity")
   public static final String DOCSUBTYPE_MISMATCH = "190";

   @RBEntry("Cannot replace \"{0}\" with \"{1}\", as their document categories are different.")
   @RBArgComment0("original document identity")
   @RBArgComment1("replace with document identity")
   public static final String DOCTYPE_MISMATCH = "191";

   @RBEntry("Cannot replace \"{0}\" with \"{1}\", as their authoring applications are different.")
   @RBArgComment0("original document identity")
   @RBArgComment1("replace with document identity")
   public static final String AUTHTYPE_MISMATCH  = "192";

   @RBEntry("Cannot add \"{1}\" as component of \"{0}\" as they are members of the same family table.")
   @RBArgComment0("Assembly document identity")
   @RBArgComment1("replace with document identity")
   public static final String PARENT_REPLACE_CHILD_IN_SAME_FT = "193";

   @RBEntry("Checkin failed because {0} has a REQUIRED dependency on {1}. You must resolve this dependency manually for checkin to succeed.")
   public static final String CHECKIN_FAILED_DEP_REQUIRED = "194";

   @RBEntry("Checkin failed because {0} has a NON-REQUIRED dependency on {1}. Selecting the checkin option \"Auto resolve incomplete objects\" will allow you to ignore this dependency. Otherwise you must resolve this dependency manually.")
   public static final String CHECKIN_FAILED_DEP_NON_REQUIRED = "195";

   @RBEntry("Cannot checkin CAD document {0}. All checked out family table members have to be checked in together. {1} must also be checked in.")
   @RBComment("Error message for SandBox Checkin of an incomplete family")
   @RBArgComment0("Name of the EPMDocument being processed")
   public static final String SBCHECKIN_INCOMPLETE_FAMILY_1 = "196";

   @RBEntry("Cannot create cross-organization relationship, {0} because its end points, in {1} and in {2}, are in different CAD name namespaces.")
   @RBComment("Error message to show that you cannot create a cross organization relationship.")
   @RBArgComment0("Class name of the link that user tried to create")
   @RBArgComment1("CAD Name Namespace of Role A ")
   @RBArgComment2("CAD Name Namespace of Role B")
   public static final String CANNOT_CREATE_CROSS_ORG_CONTAINER_LINK = "197";

   @RBEntry("Cannot move Workspace from {0} to {1} because they are in different CAD name namespaces.")
   @RBComment("Error message to show that you cannot move the EPMWorkspace from one organization to another organization.")
   @RBArgComment0("Identity of Workspace container")
   @RBArgComment1("Identity of the new container")
   public static final String CANNOT_MOVE_WORKSPACE = "198";

   @RBEntry("Cannot add {0} to {1}.  Workspace in {2} and the object in {3} are in different CAD name namespaces.")
   @RBComment("Error message to show that you cannot add given object to EPMWorkspace because the workspace and the object are in different organizations.")
   @RBArgComment0("Identity of object that user tried to add to EPMWorkspace ")
   @RBArgComment1("Identity of EPMWorkspace")
   @RBArgComment2("Identity of CAD Name Namespace (Org Container) of the workspace.")
   @RBArgComment3("Identity of CAD Name Namespace (Org Container) of the object.")
   public static final String CANNOT_ADD_TO_WORKSPACE = "199";

   @RBEntry("You can only associate Product, Library or Project to a workspace.  This is not a valid workspace container type, {0}.")
   @RBComment("Error message to show that you cannot associate the given container to EPMWorkspace in multi-tenant Windchill.")
   @RBArgComment0("Name of the container class")
   public static final String INVALID_WORKSPACE_CONTAINER_TYPE = "200";

   @RBEntry("The organization container for {0} in {1} is null.")
   @RBComment("Error message to show that the document master is in a null org-container.  Usually, it only happens when the object is in Site.")
   @RBArgComment0("Identity of a EPMDocument, WTPart, WTDocument, a PDMLink container or a Project")
   @RBArgComment1("Identity of the container where the object resides.")
   public static final String NULL_ORG_CONTAINER_FOUND = "201";

   @RBEntry("The value wt.epm.CADNameUniquenessOption in wt.properties is not recognized, {0}.  The value must either be {1} or {2}.")
   @RBComment("Error message to show that the value of wt.epm.CADNameUniquenessOption in wt.properties is not valid.")
   @RBArgComment0("Value of wt.epm.CADNameUniquenessOption in wt.properties ")
   @RBArgComment1("One of the supported values.")
   @RBArgComment2("One of the supported values.")
   public static final String INVALID_FILE_NAME_UNIQUENESS_OPTION = "202";

   @RBEntry("Incoming attributes {0} are non-file-based attributes and cannot be added or modified by the authoring application.")
   @RBComment("Occurs during upload when designated parameter mapping to non-file based attribute is added by the authoring application")
   @RBArgComment0("List of Attributes")
   public static final String IGNORE_NONFILEBASED_ATTRIBUTE = "203";

   @RBEntry("\"{0}\" cannot be deleted because it is currently in use.")
   @RBComment("Error message for Deleting EPM SoftType")
   @RBArgComment0("Type definition name")
   public static final String CANNOT_DELETE_INUSE_SOFTTYPE = "204";

   @RBEntry("Failed to get default type definition \"{0}\". EPM default type definitions not configured properly. Please contact administrator.")
   @RBComment("Error message when default type definition is not found.")
   @RBArgComment0("Type definition logical identifier / external form")
   public static final String DEFAULT_TYPE_DEF_NOT_FOUND = "205";

   @RBEntry("Cannot assign type definition \"{0}\" to {1} as the type definition belongs to different organization.")
   @RBComment("Error message when type definition cannot be assigned to CAD document as it belongs to different organization.")
   @RBArgComment0("Type definition display name")
   @RBArgComment1("Identity of EPMDocument")
   public static final String CANNOT_ASSIGN_TYPE_DEF = "206";

   @RBEntry("You do not have administrative privileges to delete Workspace {0}.")
   @RBComment("Error message to show that you do not have admin access privilege to perform delete operation.")
   @RBArgComment0("Name of workspace")
   public static final String NO_ADMIN_PRIVILEGE_FOR_WORKSPACE_DELETE = "207";

   @RBEntry("Non-latest checkout of {0} not allowed. Non-latest iteration is not built while latest iteration has build rule.")
   @RBComment("Error message in case when Build rule is added after the iteration we are trying to check out")
   @RBArgComment0("Display identity of WTPart")
   public static final String NONLATEST_CHECKOUT_NOT_ALLOWED_WHEN_LATEST_PART_HAS_BUILD_RULE = "208";

   @RBEntry("Non-latest checkout of {0} not allowed unless {1} is also checked out together.")
   @RBComment("Error message when nonlatest part with build rule and history is checked out alone")
   @RBArgComment0("Display identity of WTPart")
   @RBArgComment1("Display identity of EPMDocument")
   public static final String NONLATEST_CHECKOUT_OF_PART_ALONE_NOT_ALLOWED = "209";

   @RBEntry("Non-latest checkout of {0} not allowed. Latest iteration is either built by different source or has different build rule as compared to non-latest iteration.")
   @RBComment("Error message when build rule and build history does not match")
   @RBArgComment0("Display identity of WTPart")
   public static final String NONLATEST_CHECKOUT_NOT_ALLOWED_WHEN_LATEST_PART_BUILD_BY_DIFF_DOCUMENT = "210";

   @RBEntry("Part cannot be checked out")
   public static final String CANNOT_CHECKOUT_PART_EXCEPTION = "211";

   @RBEntry("The drawing {0} references model {1} and the model is not in the workspace.")
   @RBComment("Error message to show that a given drawing is being checked in without a model in the workspace.")
   @RBArgComment0("name of the drawing")
   @RBArgComment1("name of the missing model.")
   public static final String CHECKIN_DRAWING_WITHOUT_MODEL = "212";

   @RBEntry("Administrative Workspace Deletion")
   @RBComment("message to set Transaction Description field in Delete Workspace Notification mail")
   public static final String WORKSPACE_DELETED_MAIL_TRX_DESCRIPTION = "213";

   @RBEntry("Delete Workspace")
   @RBComment("message to set Event field in Delete Workspace Notification mail")
   public static final String WORKSPACE_DELETED_EVENT = "214";

   @RBEntry("cannot undo checkout {0} without {1}, as it is checked out from non latest. Undo Checkout both together.")
   @RBComment("Error message for non latest, in case when Target is still checked out and trying to undo checked out the Source")
   @RBArgComment0("Display identity of EPMDocument")
   @RBArgComment1("Display identity of WTPart")
   public static final String NONLATEST_SOURCE_UNDOCHECKOUT_NOT_ALLOWED = "215";

   @RBEntry("Attribute {0} could not be implicitly mapped due to multiple possible choices.")
   @RBComment("Warning message for an Attribute name which cannot be unambiguously mapped")
   @RBArgComment0("Attribute name")
   public static final String AMBIGUOUS_ATTRIBUTE = "216";

   @RBEntry("The object has been saved as a stand alone object from {0}.")
   @RBComment("Info message for an save as of only generic to make it standalone.")
   @RBArgComment0("Display identity of generic.")
   public static final String SAVE_FT_MEMBER_AS_STANDALONE = "217";

   @RBEntry("The object has been added into an existing family table {0} as a new instance.")
   @RBComment("Info message for an save as of instance into existing FT.")
   @RBArgComment0("Name of the family table.")
   public static final String UPDATING_FT_WITH_SAVE_AS_OBJECT = "218";

   @RBEntry("Cannot perform partial family table copy of family table instance along with components that are not family driven. Instance \"{1}\" is being copied with following non family driven components:\n {0}")
   @RBComment("Error message when performing a partial ASM FT Save As (reusing generic)")
   @RBArgComment0("Identity of component EPMDocument")
   @RBArgComment1("Identity of Instance EPMDocument")
   public static final String CANNOT_SAVEAS_PARTIAL_FTASM_WITH_FTCOLUMN_OF_TYPE_EPMFAMILYTABLEMEMBER = "219";

   @RBEntry("Insufficient data to add structure annotation for annotatable {1} with annotation type \"{0}\".")
   @RBComment("Error message when sufficient data is not available to add Structure Annotation.")
   @RBArgComment0(" Structure Annotation Type.")
   @RBArgComment1(" Identity of annotatable.")
   public static final String INSUFFICIENT_DATA_TO_CREATE_ANNOTATION = "220";

   @RBEntry("{0} does not have any annotations.")
   @RBArgComment0("List of identity of annotatables without annotations")
   public static final String ANNOTATION_NOT_AVAILABLE_FOR_ABORT = "221";

   @RBEntry("{1} is already persisted. Annotation type \"{0}\" cannot be added to already persisted annotatables.")
   @RBArgComment0(" Structure Annotation Type")
   @RBArgComment1(" Identity of annotatable")
   public static final String NEWLY_CREATED_ANNOTATBLE_ALREADY_PERSISTED = "222";

   @RBEntry("Multiple structure annotations not allowed for Authoring application version {0}.")
   @RBArgComment0("Authoring application version ")
   public static final String MULTI_EDIT_NOT_ALLOWED = "225";

   @RBEntry("Structure annotation of type \"{1}\" is not allowed for authoring application version {0}.")
   @RBArgComment0("Authoring application version ")
   @RBArgComment1("Structure Annotation Type.")
   public static final String STRUCTURE_ANNOTATION_NOT_ALLOWED = "226";

   @RBEntry("The annotations cannot be added onto annotatables {0} because they are already locked by another session.")
   @RBArgComment0(" Identity of annotatable")
   public static final String ANNOTATABLE_ALREADY_LOCKED = "227";

   @RBEntry("Cannot assign type definition {0} to {1} as the type definition is not the descendant of {1}.")
   @RBComment("Error message when type definition cannot be assigned as it does not belongs to the same root.")
   @RBArgComment0("Type definition display name")
   @RBArgComment1("Identity of Object")
   public static final String TYPE_DEF_NOT_CORRECT = "228";

   @RBEntry("Structure annotation of type \"{1}\" is not allowed for older versions of authoring application {0}.")
   @RBArgComment0("Authoring application Type")
   @RBArgComment1("Structure Annotation Type.")
   public static final String STRUCTURE_ANNOTATION_NOT_SUPPORTED_FOR_AUTH_APP = "229";

   @RBEntry("Cannot copy the representations since copy requires to copy all representations in the set and representation/s {0} are not copied.")
   @RBArgComment0("additional representations required to be copied")
   public static final String CANNOT_COPY_ALL_IMAGES_IN_THE_SET_NEEDS_TO_BE_COPIED = "230";

   @RBEntry("Cannot revise the representations since revise requires to revise all representations in the set and representation/s {0} are not revised.")
   @RBArgComment0("additional representations required to be revised")
   public static final String CANNOT_REVISE_ALL_IMAGES_IN_THE_SET_NEEDS_TO_BE_REVISED = "231";

   @RBEntry("Some instance(s) may have changed after merging into this Family Table generic as they were sharing different set of Family Table column(s). Column {0} has been removed.")
   @RBArgComment0("Column Name")
   public static final String COLUMN_DOES_NOT_EXIST_IN_NEW_FT = "232";

   @RBEntry("Note Text")
   @RBComment("Display name of attribute PTC_NOTE_TEXT")
   public static final String PTC_NOTE_TEXT = "233";

   @RBEntry("Cannot update the workspace {0}. Family table with generic {1} and family table with generic {2} have the same master {3}. ")
   @RBComment("Error message for update of a Workspace causing a more then one family table with the same master to exist in the workspace")
   @RBArgComment0("Name of the EPMWorkspace being updated.")
   @RBArgComment1("Name of the EPMDocument representing the generic of the incompatible family.")
   public static final String MORE_THAN_ONE_FAMILY_TABLE_WITH_THE_SAME_MASTER = "235";

   @RBEntry("Cannot update {0} with information about the state of its associated cache on the local machine because no cache has been associated with this workspace.")
   @RBComment("Error message occurs when attempting to update a workspace with information about the state of its cache on the local machine when it does not have one.")
   @RBArgComment0("Identity of an EPMWorkspace")
   public static final String WORKSPACE_HAS_NO_CLIENT_CACHE = "236";

   @RBEntry("A workspace only records information about the local cache state of CAD documents / Dynamic Documents.")
   @RBComment("Error message occurs when attempting to update a workspace with information about the local cache state of an object that is not a CAD document.")
   @RBArgComment0("Identity of an EPMWorkspace")
   public static final String CANNOT_SET_CLIENT_CACHE_STATE_FOR_NON_CADDOC = "237";

   @RBEntry("Archive conflict")
   @RBComment("Non overridable conflict as a result of Archive")
   public static final String ARCHIVE_CONFLICT = "238";

   @RBEntry("CAD document, {0}, cannot be archived because it is associated to the latest family table.")
   @RBComment("Error message for Archiving CAD documents associated to the latest family table.")
   @RBArgComment0("Identity of the CAD document")
   public static final String CANNOT_ARCHIVE_DOC_ASSOCIATED_LATEST_FT = "239";

   @RBEntry("Cannot checkout non latest iteration of image {0}.")
   @RBComment("Error message occurres when non-latest iteration of image is checked out.")
   @RBArgComment0("Identity of the image")
   public static final String CANNOT_CHECKOUT_NON_LATEST_IMAGE = "240";

   @RBEntry("Cannot checkout image.")
   @RBComment("Error message occurres when non-latest iteration of image is checked out.")
   public static final String CANNOT_CHECKOUT_IMAGE = "241";

   @RBEntry("Cannot Create Note Soft Type. Dynamic Document soft type {0} not found.")
   @RBComment("Error message occurs when attempting to create Note soft type and Dynamic document soft type is not found.")
   @RBArgComment0("Logicalid or ExtHid for Dynamic document retrived from prefrence settings.")
   public static final String ARBORTEXT_SOFTTYPE_MISSING = "242";

   @RBEntry("The object {0} could not be restored because the version was deleted from database.")
   public static final String FRAME_NO_VERSION = "250";

   @RBEntry("The object {0} could not be restored because its Master object has been deleted from database.")
   public static final String FRAME_NO_MASTER = "251";

   @RBEntry("The family table member {0} could not be restored due to incompatible family table object version.")
   public static final String FRAME_INCOMPATIBLE_DOCS = "252";

   @RBEntry("Undo checkout action was performed on this object by the system as a result of applying a previous frame.")
   public static final String FRAME_UNDO_CHECKOUT = "253";

   @RBEntry("Cannot perform partial family table copy of family table instance which is referenced by components that are not family driven. Instance \"{1}\" being copied is referenced by the following non-family driven components:\n {0}")
   @RBComment("Error message when performing a partial ASM FT(containing non-family driven references) Save As (reusing generic)")
   @RBArgComment0("Identity of reference EPMDocument")
   @RBArgComment1("Identity of Instance EPMDocument")
   public static final String CANNOT_SAVEAS_PARTIAL_FTASM_WITH_FTCOLUMN_OF_TYPE_EPMFAMILYTABLEREFERENCE = "255";

   @RBEntry("Neutral File")
   @RBComment("Tool tip for glyph of any CAD authored documents whose document types are IGES, STEP, VDA, ACIS, DXF, PARASOLID, ZIP or JT.")
   public static final String TOOLTIP_NEUTRAL_FILE = "260";

   @RBEntry("Assembly Generic")
   public static final String TOOLTIP_CADASSEMBLYGENERIC = "261";

   @RBEntry("Assembly Instance")
   public static final String TOOLTIP_CADASSEMBLYINSTANCE = "262";

   @RBEntry("CAD Part Generic")
   public static final String TOOLTIP_CADCOMPONENTGENERIC = "263";

   @RBEntry("CAD Part Instance")
   public static final String TOOLTIP_CADCOMPONENTINSTANCE = "264";

   @RBEntry("Cannot copy {0} because you do not have access to its top generic=\"{1}\".")
   public static final String CANNOT_COPY_FAMILY_INSUFFICIENT_ACCESS = "265";

   @RBEntry("Content with file name \"{0}\" already exists. Cannot add another content with same name.")
   public static final String DUPLICATE_CONTENT_FILE_NAME = "266";

   @RBEntry("The object {0} is owned by {1}, which will not permit changes by the current application")
   public static final String OPERATION_VETOED_AND_NO_APPLICATION_SET = "267";

   @RBEntry("Unable to add annotations.")
   public static final String UNABLE_TO_ADD_ANNOTATIONS = "268";

   @RBEntry("Parameter \"{0}\" could not be uploaded to server due to inconsistent quantity of measure with Windchill attribute \"{1}\"")
   public static final String QUANTITY_TYPE_MISMATCH = "269";

   @RBEntry("A CAD document with the file name {0} is already the member of another family table. Cannot create a new family table instance in the workspace with this name.")
   @RBComment("Error message when the target object of save-as operation on an FT object is another FT object.")
   @RBArgComment0("Display identity of the target FT object.")
   public static final String FTOBJECT_CANNOT_BE_SAVEAS_TARGET = "270";

   @RBEntry("Cannot make generic {0} standalone because it has one or more instances. Instances should also be made standalone.")
   @RBComment("Error message when a generic is made standalone without all its instances.")
   @RBArgComment0("Identity of generic EPMDocument which cannot be made standalone.")
   public static final String CANNOT_MAKE_GENERIC_STANDALONE_WITHOUT_INSTANCES = "271";

   @RBEntry("Following family table objects should be made standalone before using them as the target of a save-as operation {0}.")
   @RBComment("Error message when the copy is a persistent FT object during pre_copy event.")
   @RBArgComment0("List of persistent FT objects.")
   public static final String COPY_CANNOT_BE_FTOBJECT_IN_PRECOPY = "272";

   @RBEntry("Default type definition for {0} not found.")
   @RBComment("Error message when the default type definition for a class is not found.")
   @RBArgComment0("Class name.")
   public static final String DEFAULT_TYPEDEF_NOT_FOUND = "273";

   @RBEntry("Type definition {0} is in a checked out state. Cannot add new attributes.")
   @RBComment("Error message when a type definition is in checkedout state.")
   @RBArgComment0("Type definition name.")
   public static final String TYPEDEF_CHECKEDOUT = "274";

   @RBEntry("Successfully added the attributes for Creo Elements/Direct.")
   @RBComment("Message when the attributes for Creo Elements/Direct are successfully added to soft types.")
   public static final String COCREATE_ATTRS_ADDED = "275";

   @RBEntry("One or more attributes for Creo Elements/Direct already exist in soft types. Soft types not updated.")
   @RBComment("Error message when the attributes for Creo Elements/Direct cannot be added to soft types.")
   public static final String CANNOT_ADD_COCREATE_ATTRS = "276";

   @RBEntry("{0} is not a top generic.")
   @RBArgComment0("Identity of EPMDocument")
   public static final String DOC_IS_NOT_TOP_GENERIC = "277";

   @RBEntry("CAD document / Dynamic Document is not new or working copy.")
   public static final String DOC_IS_NOT_MODIFIABLE = "278";

   @RBEntry("Two or more rules have the same order. Cannot process further.")
   public static final String DUPLICATE_RULE_ORDER = "280";

   @RBEntry("No parent rule has been set on one or more result rules. Cannot process further.")
   public static final String PARENTRULE_NOT_SET = "281";

   @RBEntry("File name has been renamed in the commonspace. Perform workspace synchronization.")
   public static final String FILE_NAME_HAS_BEEN_RENAMED_IN_COMMONSPACE = "282";

   @RBEntry("Cannot change CAD name of one or more of the CAD documents because their new CAD names are not unique. The duplicate CAD names are {0}.")
   @RBComment("Error message to show when user tries to rename CADDocument/s and CADDocument/s with same name already exist in databse.")
   @RBArgComment0("List of duplicate CADNames of the document")
   public static final String CANNOT_RENAME_DOCUMENTS = "283";

   @RBEntry("Cannot save as \"{0}\" to \"{1}\", as their document subcategories are different.")
   @RBArgComment0("original document identity")
   @RBArgComment1("save-as to document identity")
   public static final String SAVEAS_DOCSUBTYPE_MISMATCH = "284";

   @RBEntry("Cannot Save As \"{0}\" to \"{1}\", as their document categories are different.")
   @RBArgComment0("original document identity")
   @RBArgComment1("save-as to document identity")
   public static final String SAVEAS_DOCTYPE_MISMATCH = "285";

   @RBEntry("Cannot save as \"{0}\" to \"{1}\", as their authoring applications are different.")
   @RBArgComment0("original document identity")
   @RBArgComment1("save-as to document identity")
   public static final String SAVEAS_AUTHTYPE_MISMATCH = "286";

   @RBEntry("User does not have Read access to the part \"{0}\".")
   @RBArgComment0("part identity")
   public static final String NO_READ_ACCESS_TO_PART = "287";

   @RBEntry("User does not have Modify access to part \"{0}\".")
   @RBArgComment0("part identity")
   public static final String NO_MODIFY_ACCESS_TO_PART = "288";

   @RBEntry("The file {0} may have been renamed in the commonspace. Perform a workspace synchronization to resolve the issue.")
   public static final String FILE_MAY_HAVE_BEEN_RENAMED_IN_COMMONSPACE = "290";

   @RBEntry("Parts associated to CAD documents with an image association were skipped from build.")
   public static final String IMAGE_ASSOCIATION_NOT_BUILT = "291";

   @RBEntry("Parts already built from CAD documents were skipped from build.")
   public static final String ALREADY_BUILT_ASSOCIATION_NOT_BUILT = "292";

   @RBEntry("Cannot store or modify a temporary EPM Document Uses Link that was created during annotation processing.")
   @RBComment("This message is for Windchill developer.  Fabricated EPMMemberLink is created for an annotation.  It cannot be saved to the database.")
   public static final String CANNOT_SAVE_FABRICATED_MEMBER_LINKS = "293";

   @RBEntry("{0} is not associated to any Alternate Representation or its associated Alternate Representation is not associated to any Resulting Rule.")
   public static final String INVALID_ESR_CONTAINER = "294";

   @RBEntry("{0} are not associated to any Alternate Representation or their associated Alternate Representation is not associated to any Resulting Rule.")
   public static final String INVALID_ESR_CONTAINER_PLURAL = "295";

   @RBEntry("Cannot save-as {0} to {1} as their number of family table members are not equal.")
   @RBArgComment0("original document identity")
   @RBArgComment1("save-as to document identity")
   public static final String SAVEAS_NO_OF_INSTANCES_DO_NOT_MATCH= "296";

   @RBEntry("Cannot save-as \"{0}\" to \"{1}\". Partial family table copy is not allowed for save-as of family table CAD documents to existing family table CAD documents.")
   @RBArgComment0("original document identity")
   @RBArgComment1("save-as to document identity")
   public static final String SAVEAS_OF_PARTIAL_FT_NOT_ALLOWED= "297";

   @RBEntry("Cannot save-as {0} to {1}. {2} is intermediate generic and is not supported for save-as to existing.")
   @RBArgComment0("original document identity")
   @RBArgComment1("save-as to document identity")
   @RBArgComment2("intermediate generic document identity")
   public static final String SAVEAS_OF_NESTED_FT_NOT_ALLOWED= "298";

   @RBEntry("Cannot save-as {0} to {1}. All members of family table {2} are not being copied to members of single family table ")
   @RBArgComment0("original document identity")
   @RBArgComment1("save-as to document identity")
   @RBArgComment2("original family table name")
   public static final String SAVEAS_SOURCE_FT_TO_MULTIPLE= "299";

   @RBEntry("Found CAD document {0} that is not a template.")
   @RBArgComment0("found document identity")
   public static final String EXISTING_OBJ_NOT_TEMPLATE= "300";

   @RBEntry("Internal error: Structure annotation of type \"{0}\" is not supported.")
   @RBArgComment0("Structure Annotation Type.")
   public static final String STRUCTURE_ANNOTATION_NOT_SUPPORTED = "301";

   @RBEntry("Internal error: Structure annotation of type \"{0}\" is not supported for unsuppressed persistent memberlink.")
   @RBArgComment0("Structure Annotation Type.")
   public static final String STRUCTURE_ANNOTATION_NOT_SUPPORTED_UNSUPPRESSED_PERSISTENT_LINK = "302";

   @RBEntry("Internal error: Structure annotation of type \"{0}\" is not supported for non-persistent memberlink.")
   @RBArgComment0("Structure Annotation Type.")
   public static final String STRUCTURE_ANNOTATION_NOT_SUPPORTED_NON_PERSISTENT_LINK = "303";

   @RBEntry("Internal error: Structure annotation of type \"{0}\" is not supported for suppressed persistent memberlink.")
   @RBArgComment0("Structure Annotation Type.")
   public static final String STRUCTURE_ANNOTATION_NOT_SUPPORTED_SUPPRESSED_PERSISTENT_LINK = "304";

   @RBEntry("{0} cannot be child softtype of {1}.")
   @RBComment("Error message for hierarchy of soft type")
   @RBArgComment0("Identity of the child soft type")
   @RBArgComment1("Identity of the parent soft type")
   public static final String CANNOT_BE_CHILD_OF_SOFTTYPE = "305";

   @RBEntry("Unable to perform Save As on \"{0}\", since it has pending component operations that have not been applied in the CAD tool.")
   @RBComment("Error message to show when user tries to Save As of assembly which has annotated member link(s).")
   @RBArgComment0("Display identity of an Assembly")
   public static final String SAVEAS_OF_ASSEMBLY_WITH_ANNOTATED_MEMBER_LINK_NOT_ALLOWED = "306";

   @RBEntry("Unable to replace the content of the document \"{0}\" with the file in the WS, because either it or its parent assembly has pending component operations that have not been applied in the CAD tool.")
   @RBComment("Error message to show when user tries to SaveAsToExisting of assembly and either parent or component of that assembly has annotated member link(s).")
   @RBArgComment0("Display identity of an Assembly")
   public static final String REPLACE_ASSEMBLY_WITH_ANNOTATED_MEMBER_LINK_NOT_ALLOWED = "307";

   @RBEntry("Unable to update this assembly to use the new child. The uses link to the child \"{0}\" has a pending component operation that must be applied in the CAD tool before modification in Windchill.")
   @RBComment("Error message for update parent in case of annotated member link")
   @RBArgComment0("FILE NAME OF ORIGINAL ITEM")
   public static final String CANNOT_UPDATE_PARENT_WITH_ANNOTATED_MEMBER_LINK = "308";

   @RBEntry("{0} could not be deleted/purged. There are objects associated to it through EPMBuildHistory link.")
   @RBArgComment0("persistable which could not be deleted/purged.")
   public static final String CANNOT_DELETE_PERSISTABLE_WITH_EXISTING_BUILDHISTORYLINK = "309";

   @RBEntry("Cannot update as all family table members are not selected for update parent.")
   public static final String NOT_ALL_FTMEMBERS_IN_UPDATE = "310";

   @RBEntry("All members of family table are not available with same update request.")
   public static final String NOT_SAME_UPDATE_IN_FT = "311";

   @RBEntry("Internal error: Structure annotation of type \"{0}\" is not supported for annotated persistent member link.")
   @RBArgComment0("Structure Annotation Type.")
   public static final String STRUCTURE_ANNOTATION_NOT_SUPPORTED_ANNOTATED_PERSISTENT_LINK = "312";

   @RBEntry("Cannot save-as \"{0}\" to \"{1}\" as their familytableStatus does not match.")
   @RBArgComment0("original document identity")
   @RBArgComment1("save-as to document identity")
   public static final String FAMILYTABLESTATUS_DOES_NOT_MATCH = "313";

   @RBEntry("Cannot save-as \"{0}\" to \"{1}\". Variant structure of source and target does not match.")
   @RBArgComment0("original document identity")
   @RBArgComment1("save-as to document identity")
   public static final String SOURCE_TARGET_VARIANT_STRUCTURE_DOES_NOT_MATCH = "314";

   @RBEntry("Expected annotation not present on annotatable \"{0}\".")
   public static final String EXPECTED_ANNOTATION_NOT_PRESENT_ON_ANNOTATABLE = "315";

   @RBEntry("Invalid Change Structure Annotation Operation for \"{0}\". Structure annotation of type 'add placed' can be replaced only by annotation of type 'add packaged'.")
   public static final String INVALID_CHANGE_STRUCTURE_ANNOTATION_OPERATION = "316";

   @RBEntry("Unable to change annotation.")
   public static final String UNABLE_TO_CHANGE_ANNOTATIONS = "317";

   @RBEntry("Cannot create or update Design Context since it will create rules to Windchill modified Uses Links.  It is required to regenerate the following changed assemblies in authoring application before continuing: {0}")
   @RBComment("Error message during creation of new Design Context if an annotated member link is present.")
   @RBArgComment0("List of assemblies with annotated member links")
   public static final String CANNOT_CREATE_OR_UPDATE_CAR_WITH_ANNOTATED_MEMBER_LINK = "318";

   @RBEntry("Cannot edit  Design Context with Windchill modified Uses Links. It is required to regenerate the following changed assemblies in authoring application before continuing: {0}")
   @RBComment("Error message during edit of a Design Context if an annotated member link is present.")
   @RBArgComment0("List of assemblies with annotated member links")
   public static final String CANNOT_EDIT_CAR_WITH_ANNOTATED_MEMBER_LINK = "319";

   @RBEntry("Cannot save  Design Context because it has rules on Windchill modified Uses Links.")
   @RBComment("Error message during save  of Design Context if an annotated member link is present.")
   public static final String CANNOT_SAVE_CAR_WITH_ANNOTATED_MEMBER_LINK = "320";

   @RBEntry("Cannot import \"{0}\" because the component name must be unique among its Geometric Model Relationships.  The duplicate component names are {1}.")
   @RBComment("PLM Error message - a EPMDocument cannot be imported because the component names on EPMMemberLink (or uses links) are not unique.")
   @RBArgComment0("Identity of EPMDocument")
   @RBArgComment1("List of component names, some are duplicates")
   public static final String PLM_DUPLICATE_ML_NAMES = "322";

   @RBEntry("Cannot import \"{0}\" because the component name must be unique among its Geometric Model Relationships.  The duplicate component name is {1}.")
   @RBComment("PLM Error message - a EPMDocument cannot be imported because the component names on EPMMemberLink (or uses links) are not unique.")
   @RBArgComment0("Identity of EPMDocument")
   @RBArgComment1("Duplicate component name")
   public static final String PLM_DUPLICATE_ML_NAME = "323";

   @RBEntry("Failed to retrieve the root type of Type Definition \"{0}\".")
   @RBComment("Failed to retrieve the root type of Type Definition.")
   @RBArgComment0("Default Type Definition Name")
   public static final String ROOT_TYPE_DEFINITION_RETRIEVAL_FAILED = "324";

   @RBEntry("\"{0}\" Property Definition not yet defined on \"{1}\".")
   @RBComment("Property Definition not yet defined.")
   @RBArgComment0("Identity of Property Definition")
   @RBArgComment1("Property Holder Classname")
   public static final String PROPERTY_DEFINITION_NOT_DEFINED = "325";

   @RBEntry("Cannot add images to project since related images \"{0}\" also need to be added to project.")
   @RBArgComment0("additional images required to be checked out to project")
   public static final String CANNOT_ADD_IMAGES_TO_PROJECT = "326";

   @RBEntry("Cannot checkin images since related images \"{0}\" also need to be checked in.")
   @RBArgComment0("additional images required to be checked in")
   public static final String  CANNOT_CHECKIN_IMAGES = "327";

   @RBEntry("Image CAD documents with file name \"{0}\" need to be replaced. Can not create or replace the content of image CAD documents \"{1}\".")
   @RBComment("Error message when all images of set are not being replaced at the same time.")
   @RBArgComment0("List of Identity of additional images which need to be replaced.")
   @RBArgComment1("List of identity of images being replaced.")
   public static final String ADDITIONAL_IMAGES_NEED_TO_BE_REPLACED = "328";

   @RBEntry("A source CAD document with the file name \"{0}\" already exists in the namespace of the context \"{1}\". Cannot create or replace the content of this CAD document, since it is a source CAD document.")
   @RBComment("Error message when all images of set are not being replaced at the same time.")
   @RBArgComment0("Identity of source being replaced.")
   @RBArgComment1("Name of the context in which the source name exists.")
   public static final String SOURCE_CANNOT_TO_BE_REPLACED = "329";

   @RBEntry("The current action results in the workspace \"{0}\" exceeding the limit of {1} set on the number of objects set by your administrator. The action cannot be completed until the number of objects is reduced.")
   @RBComment("Error message when workspace size limit is exceeded as a result of an action.")
   @RBArgComment0("Workspace name.")
   @RBArgComment1("Limit on the size of workspace.")
   public static final String TOO_LARGE_WORKSPACE = "330";

   @RBEntry("Cannot save following Design Contexts without the default result rule: {0}")
   @RBComment("Error message during create/edit of a Design Context if the default result rule is not present.")
   @RBArgComment0("List of Design Context without the default result rule.")
   public static final String CANNOT_SAVE_CAR_WITHOUT_DEFAULT_RESULT_RULE = "331";

   @RBEntry("The Source CAD documents with file name \"{0}\" need to be checked in.")
   @RBComment("Error message when all the sources are not checked in during creation of new rules and history objects.")
   @RBArgComment0("List of Identity of Source CAD documents which need to be checked in for creation of new rules and history objects.")
   public static final String SOURCE_NEEDS_TO_BE_CHECKED_IN = "332";

   @RBEntry("The Image CAD documents with file name \"{0}\" need to be checked out or new.")
   @RBComment("Error message when all the images are not checked out or new during creation of new rules and history objects.")
   @RBArgComment0("List of Identity of Image CAD documents which need to be checked out or new for creation of new rules and history objects.")
   public static final String IMAGES_NEEDS_TO_BE_CHECKED_OUT_OR_NEW = "333";

   @RBEntry("Can not create new rules and history objects for image CAD documents with file name \"{0}\" as they have already existing rule/history.")
   @RBComment("Error message when an image being used for creation of new rules and history objects has already existing rule/history.")
   @RBArgComment0("List of identity of images for which new links and rules cannot be created.")
   public static final String CANNOT_CREATE_RULES_AND_LINKS_FOR_IMAGES = "334";

   @RBEntry("The Design Context and its assembly could not be created/updated in the following format, \"{0}\". Some CAD Modules are missing in this CAD format: \"{1}\"")
   @RBArgComment0("List of identity of document for which no image authored in given authoring application found")
   public static final String CONTINUE_TO_CREATE_WITHOUT_MISSING_CAD_MODULES = "335";

   @RBEntry("The Design Context and its assembly could not be created/updated in the following format, \"{0}\". Some CAD Modules are missing in this CAD format: \"{1}\"")
   @RBArgComment0("Image Authoring application.")
   @RBArgComment1("List of identity of document for which no image authored in given authoring application found")
   public static final String UNABLE_TO_CREATE_WITHOUT_MISSING_CAD_MODULES = "336";

   @RBEntry("Cannot build CAD Structure. Image CAD documents \"{0}\" need to be either new or checked out in the workspace.")
   @RBComment("Error message occurs when non-latest iteration of image is checked out.")
   @RBArgComment0("List of identity of image CAD documents")
   public static final String IMAGE_SHOULD_BE_CHECKEDOUT_OR_NEW_IN_WORKSPACE = "337";

   @RBEntry("Cannot build CAD structure. Image CAD documents \"{0}\" are non latest iterations.")
   @RBComment("Error message occurs when non-latest iteration of image is used during build of CAD Structure.")
   @RBArgComment0("List of identity of image CAD documents")
   public static final String NON_LATEST_IMAGE = "338";

   @RBEntry("Cannot build CAD structure. Image CAD documents \"{0}\" belong to a family table.")
   @RBComment("Error message occurs when image CAD document used for build of CAD structure belongs to a family table.")
   @RBArgComment0("List of identity of image CAD documents")
   public static final String IMAGE_PART_OF_FAMILY_TABLE = "339";

   @RBEntry("Cannot build CAD Structure. User does not have modify access to Image CAD documents \"{0}\".")
   @RBComment("Error message occurs when user does not have modify access to image CAD documents.")
   @RBArgComment0("List of identity of image CAD documents")
   public static final String NO_MODIFY_PERMISSION_ON_IMAGE = "340";

   @RBEntry("Cannot build CAD Structure. Image CAD documents \"{0}\" are not in the same context as workspace \"{1}\".")
   @RBComment("Error message occurs when image CAD documents are in a different context as compared to the workspace.")
   @RBArgComment0("List of identity of image CAD documents")
   @RBArgComment1("Identity of workspace in which objects are build")
   public static final String DIFFERENT_CONTEXT_FOR_IMAGE_AND_WORKSPACE = "341";

   @RBEntry("Cannot build CAD Structure. Source CAD documents \"{0}\" need to be checked in.")
   @RBComment("Error message occurs when non-latest iteration of source is checked out.")
   @RBArgComment0("List of identity of source CAD documents")
   public static final String SOURCE_NEED_TO_BE_CHECKED_IN = "342";

   @RBEntry("Cannot build CAD Structure. Source CAD documents \"{0}\" should be Windchill authored assemblies.")
   @RBComment("Error message occurs when source CAD document is not authored by Windchill.")
   @RBArgComment0("List of identity of source CAD documents")
   public static final String SOURCE_NEED_TO_BE_WINDCHILL_AUTHORED = "343";

   @RBEntry("Cannot build CAD structure. Source CAD documents \"{0}\" belong to a family table.")
   @RBComment("Error message occurs when Source CAD document used for build of CAD structure belongs to a family table.")
   @RBArgComment0("List of identity of Source CAD documents")
   public static final String SOURCE_PART_OF_FAMILY_TABLE = "344";

   @RBEntry("Cannot build CAD Structure. Source CAD documents \"{0}\" are not in the same context as workspace \"{1}\".")
   @RBComment("Error message occurs when Source CAD documents are in a different context as compared to the workspace.")
   @RBArgComment0("List of identity of Source CAD documents")
   @RBArgComment1("Identity of workspace in which objects are build")
   public static final String DIFFERENT_CONTEXT_FOR_SOURCE_AND_WORKSPACE = "345";

   @RBEntry("Cannot build CAD Structure. Image CAD documents \"{0}\" are already built from later iteration of source.")
   @RBComment("Error message occurs when Images are already built from lateration of source and hence cannot be built from earlier iteration of source.")
   @RBArgComment0("List of identity of Image CAD documents")
   public static final String IMAGE_ALREADY_BUILT_FROM_LATER_ITERATION_OF_SOURCE = "346";

   @RBEntry("Cannot build CAD Structure. Image CAD documents \"{0}\" do not have association with given source document.")
   @RBComment("Error message occurs when Images do not have EPMDerivedRepRule with corresponding input source document.")
   @RBArgComment0("List of identity of Image CAD documents")
   public static final String NO_EPMDERIVEDREPRULE_BWTWEEN_SOURCE_AND_IMAGE = "347";

   @RBEntry("Unable to update the placement of the component \"{0}\" in the assembly \"{1}\". The component is constrained in the CAD file.")
   @RBComment("Error message occurs when placement of component in assembly cannot be updated since component is constrained in CAD file.")
   @RBArgComment0("List of child file names")
   public static final String CONTINUE_WITHOUT_UPDATING_PLACEMENT = "348";

   @RBEntry("Cannot copy instance=\"{0}\" because you do not have access to its Immediate Generic=\"{1}\".")
   @RBArgComment0("original document identity")
   @RBArgComment1("Immediate Generic identity")
   public static final String CANNOT_COPY_INSTANCE_INSUFFICIENT_ACCESS = "349";

   @RBEntry("Cannot copy instance=\"{0}\" because you do not have access to its Immediate Generic.")
   @RBArgComment0("original document identity")
   public static final String CANNOT_COPY_INSTANCE_IMMEDIATEGENERIC_NOT_AVAILABLE = "350";

   @RBEntry("Quantity change for member link of Source CAD document \"{0}\" has been ignored while updating member link of Image CAD document \"{1}\" during build of CAD Structure.")
   @RBComment("Warning message when quantity change between Source and Image CAD document is ignored during subsequent build of CAD document structure.")
   @RBArgComment0("Identity of Source CAD document")
   @RBArgComment1("Identity of Image CAD document")
   public static final String QUANTITY_CHANGE_IGNORED = "351";

   @RBEntry(" External Simplified Reps with Substitutions by Dynamic Envelope cannot be edited in Windchill.  Please open the ESR in Creo to update")
   @RBArgComment0("Error message when user tries to edit CAR having Substitute by envelope rule")
   public static final String CANNOT_EDIT_ESR_HAVING_SUBSTITUTE_BY_ENVELOPE_RULE = "352";

   @RBEntry("Cannot update assembly, \"{0}\". The following children are no longer compatible with the source CAD documents: \"{1}\".  If a new CAD document image was created for the source, then create a New Image Design Context, instead of updating.")
   @RBComment("Error message when user tries to update image structure where childs are not compatible with correspondign source.")
   @RBArgComment0("Identity of assembly CAD document")
   @RBArgComment1("Identity of child CAD documents incompatible with source")
   public static final String CANNOT_UPDATE_IMAGE_INCOMPATIBLE_CHILDS = "353";

   @RBEntry("Cannot update assembly.")
   @RBComment("Error message when update of image assembly fails.")
   public static final String CANNOT_UPDATE_ASSEMBLY = "354";

   @RBEntry("Structure annotation is not supported for persistent annotated EPMMemberlink.")
   public static final String STRUCTURE_ANNOTATION_NOT_SUPPORTED_PERSISTENT_ANNOTATED_LINK = "355";

   @RBEntry("The following path occurrence on Design Context, {1}, cannot be found in the Image CAD Document structure: {0}.  No rule will be created in the Image Design Context for this path.")
   @RBArgComment0("Path of rule")
   @RBArgComment1("Target ESR")
   public static final String SOURCE_PATH_NOT_FOUND_UPDATING_IMAGE_ESR = "356";

   @RBEntry("Could not import EPMMemberlink as Expected UniqueNDId = {0} and Actual UniqueNDId = {1}.")
   @RBArgComment0("Expected UniqueNDId")
   @RBArgComment1("Actual UniqueNDId")
   public static final String UNIQUENDID_DIFFERENT_WHILE_IMPORT = "357";

   @RBEntry("Image CAD documents with file name \"{0}\" need to be deleted. Can not delete image CAD documents \"{1}\".")
   @RBComment("Error message when all images of set are not being deleted at the same time.")
   @RBArgComment0("List of Identity of additional images which need to be deleted.")
   @RBArgComment1("List of identity of images being deleted.")
   public static final String ADDITIONAL_IMAGES_NEED_TO_BE_DELETED = "358";

   @RBEntry("Update of parent model \"{0}\" is not allowed. Configurable CAD documents cannot be children of standard assemblies. Update of parent is cancelled.")
   @RBComment("Update of parent model \"{0}\" is not allowed. Configurable CAD documents cannot be children of standard assemblies. Update of parent is cancelled.")
   public static final String PARENT_DOC_NOT_CONFIGURABLE_CHILD_CONFIGURABLE = "359";

   @RBEntry("Cannot add the following revised CAD documents to multiple as-stored baselines as Owner: {0}")
   @RBComment("Error message for adding the same EPMDocument as Owner to multiple as-stored baselines during Revise operation")
   @RBArgComment0("List of Identity of EPMDocument.")
   public static final String CANNOT_CREATE_AS_STORED_BASELINE_AT_REVISE = "360";

   @RBEntry("IBAHolderReference and AttributeDefinitionReference cannot be set to null when calling EPMParameterUnitInfo.newEPMParameterUnitInfo")
   @RBComment("Error message for EPMParameterUnitInfo.newEPMParameterUnitInfo when IBAHolderReference OR AttributeDefinitionReference is null")
   public static final String NULL_VALUE_PROVIDED_FOR_EPMPARAMETERUNITINFO = "361";

   @RBEntry("Cannot delete the version for \"{0}\". There are iterations associated to version through EPMBuildHistory link and these iterations have changes published to associated document structure.")
   @RBArgComment0("persistable which could not be deleted.")
   public static final String CANNOT_DELETE_VERSION_WITH_EXISTING_REVERSE_BUILDHISTORYLINK = "362";

   @RBEntry("Cannot delete model item masters \"{0}\". The build rule for the model items must be disassociated.")
   @RBArgComment0("mode item master which could not be deleted.")
   public static final String CANNOT_DELETE_MODEL_ITEM_MASTER = "363";

   @RBEntry("Family table validations failed. Please contact PTC unless this occurred during a package import. Failures on package import may be due to a compatible generic not being available at time of import.")
   @RBArgComment0("Family table validations failed. Please contact PTC unless this occurred during a package import. Failures on package import may be due to a compatible generic not being available at time of import.")
   public static final String FAMILY_TABLE_VALIDATIONS_FAILED = "364";

   @RBEntry("Part \"{0}\" is out of date with its related CAD document \"{1}\". Parts must be built before sending changes back to PDM.")
   @RBArgComment0("Conflict when part being sent to PDM is out of date with its related CAD document.")
   public static final String CANNOT_SEND_PART_TO_PDM = "365";

   @RBEntry("Cannot save rule without having alternate representation")
   public static final String CANNOT_SAVE_ALTERNATREPRULE_WITHOUT_ALTERNATEREPSENTATION = "366";

   @RBEntry("CAD Part")
   public static final String TOOLTIP_CADCOMPONENT = "367";

   @RBEntry("The image Document cannot be copied with the following source Document(s) because they are out of date: \"{0}\".  Either exclude the Image Documents from the Save As action or update the Images and try again.")
   public static final String OUT_OF_DATE_IMAGE_BEING_COPIED = "368";

   @RBEntry("Content Center Part")
   public static final String TOOLTIP_LIBRARY_PART = "369";

   @RBEntry("Cannot convert to share a partial family.  All family members must be included in convert to share operation.")
   @RBComment("Convert to share operation must have all family table members included")
   public static final String PARTIAL_FAMILY_CONVERT_TO_SHARE_NOT_ALLOWED = "370";

   @RBEntry("Toolbox Part")
   public static final String TOOLTIP_SOLIDWORKS_LIBRARY_PART = "371";

   @RBEntry("One or more CAD documents with the filenames {0} already exist or are the filenames of dependents of an object currently in the workspace. Cannot change the filenames of the documents to {0}.")
   @RBComment("Error message to show that CAD documents could not be renamed on workspace.")
   @RBArgComment0("List of duplicate filenames of the documents")
   public static final String CANNOT_RENAME_DOCUMENTS_IN_WORKSPACE = "372";

   @RBEntry("One or more CAD documents with the numbers \"{0}\" cannot be added to the workspace, as they share the same filenames \"{1}\" as CAD documents \"{2}\" or are the filenames of dependents of an object currently in the workspace \"{3}\".")
   @RBComment("Error message to show that CAD documents could not be added to workspace.")
   @RBArgComment0("List of duplicate filenames of the documents")
   public static final String CANNOT_ADD_DOCUMENTS_IN_WORKSPACE_WITH_DUPLICATE_FILENAME = "373";

   @RBEntry("The selected CAD Documents \"{0}\" do not have any as-stored baselines.")
   @RBArgComment0("List of CAD Documents")
   public static final String NO_AS_STORED_CONFIGS_FOR_SEEDS = "374";

   @RBEntry("\"{0}\" has been assigned a temporary number to enable workspace upload.  Using rename a permanent number must be reassigned prior to check-in")
   public static final String CANNOT_CHECKIN_TEMP_NUMBER = "375";

   @RBEntry("The document with the filename \"{0}\" has been assigned a temporary number in your workspace due to a numbering conflict.  Renumbering this document will be required for check-in. ")
   public static final String DUPLICATE_NUMBER_ERROR = "376";

   @RBEntry("Upload of locally modified shared objects is not allowed.")
   public static final String CANNOT_PRIVATE_CHECKOUT_SHARED_OBJECTS = "377";

   @RBEntry("Upload of locally modified representations is not allowed.")
   public static final String CANNOT_PRIVATE_CHECKOUT_IMAGES = "378";

   @RBEntry("Unable to overwrite already uploaded modification for representations.")
   public static final String CANNOT_CONVERT_REGULAR_CHECKOUT_TO_PRIVATE_CHECKOUT_FOR_IMAGES = "379";
   @RBEntry("AutoCAD Electrical Project")
   public static final String TOOLTIP_ACAD_PROJECT = "380";

   @RBEntry("AutoCAD Electrical Configuration File")
   public static final String TOOLTIP_ACAD_CONFIG_FILE = "381";

   @RBEntry("A document with the filename \"{0}\" already exists in workspace \"{1}\". A rename of one of the conflicting documents may be required for check-in.")
   @RBComment("CAD document has a filename constraint violation which may need to be resolved prior to check-in.")
   public static final String DUPLICATE_FILENAME_CONFLICT_ON_USER_WS = "382";

   @RBEntry("A document with the filename \"{0}\" already exists in workspace \"{1}\" of user \"{2}\". A rename of one of the conflicting documents may be required for check-in.")
   @RBComment("CAD document has a filename constraint violation which may need to be resolved prior to check-in.")
   public static final String DUPLICATE_FILENAME_CONFLICT_ON_ANOTHER_USER_WS = "383";

   @RBEntry("A document with the filename \"{0}\" already exists in workspace in a restricted location for which you do not have permissions. A rename of one of the conflicting documents may be required for check-in.")
   @RBComment("CAD document has a filename constraint violation which may need to be resolved prior to check-in.")
   public static final String DUPLICATE_FILENAME_CONFLICT_ON_RESTRICTED_WS_CONTEXT = "384";

   @RBEntry("A document with the filename \"{0}\" already exist in \"{1}\", folder \"{2}\". A rename of one of these documents is required for check-in.")
   @RBComment("CAD document has a filename constraint violation which may need to be resolved prior to check-in.")
   public static final String DUPLICATE_FILENAME_CONFLICT_ON_CS = "385";

   @RBEntry("A document with the filename \"{0}\" already exists in a restricted location for which you do not have permissions. A rename of one of the conflicting documents is required for check-in")
   @RBComment("CAD document has a filename constraint violation which may need to be resolved prior to check-in.")
   public static final String DUPLICATE_FILENAME_CONFLICT_ON_RESTRICTED_CS_CONTEXT = "386";

   @RBEntry("\"{0}\": This CAD Document child has different values for the \"{1}\" attribute on its Uses links. The \"{1}\" must be the same for all Uses of this object.")
   public static final String LINKS_WITH_DIFFERENT_IBA_FOR_FIND_NUMBER = "387";

   @RBEntry("\"{0}\": This CAD Document child has different values for the \"{1}\" attribute on its Uses links. The \"{1}\" must be the same for all Uses of this object.")
   public static final String LINKS_WITH_DIFFERENT_IBA_FOR_LINE_NUMBER = "388";

   @RBEntry("Invalid IBA value for Find Number or Line Number.")
   public static final String INVALID_IBA_VALUE_FOR_FIND_NUMBER_OR_LINE_NUMBER = "389";

   @RBEntry("Attribute \"{0}\" of type \"{1}\" cannot be propagated to \"{2}\". Attribute should be of type String or Integer.")
   @RBComment("Inconsistent IBA type on Find Number Attribute. It should be of type String or Integer")
   public static final String INCONSISTENT_IBA_TYPE_FIND_NUMBER = "390";

   @RBEntry("Attribute \"{0}\" of type \"{1}\" cannot be propagated to \"{2}\". Attribute should be of type Integer.")
   @RBComment("Inconsistent IBA type on Line Number Attribute. It should be of type Integer")
   public static final String INCONSISTENT_IBA_TYPE_LINE_NUMBER = "391";

   @RBEntry("Document \"{0}\" cannot be sent to PDM as referenced parts \"{1}\" are not selected for same operation")
   public static final String CAN_NOT_SEND_TO_PDM_MODELITEMPARTLINK = "392";

   @RBEntry("Cannot check in \"{0}\" as referenced part is in personal cabinet")
   public static final String CAN_NOT_CHECKIN_MODELITEMPARTLINK_PERSONAL_CABINATE = "393";

   @RBEntry("Cannot delete \"{0}\" as it is linked to one or more CAD Documents or Dynamic Documents")
   public static final String CAN_NOT_DELETE_WTPART_MASTER_MODELITEMPARTLINK = "394";
   
   @RBEntry("Cannot overwrite \"{0}\" and \"{1}\" together as their contexts do not match. Either both should belong to PDM contexts or both should belong to the same project.")
   @RBComment("Error message thrown by prepareForSaveAs() when one copy is from PDM context and other is from project.")
   @RBArgComment0("Identity of the first copy")
   @RBArgComment1("Identity of the second copy")
   public static final String SAVEAS_CONTAINER_MISMATCH = "395";

   @RBEntry("Cannot overwrite \"{0}\" and \"{1}\" together as they belong to different projects.")
   @RBComment("Error message thrown by prepareForSaveAs() when two copies are from different projects.")
   @RBArgComment0("Identity of the first copy")
   @RBArgComment1("Identity of the second copy")
   public static final String SAVEAS_MULTIPLE_PROJECT_CONTAINERS = "396";

   @RBEntry("Cannot copy this instance, because its generic is not in the project: \"{0}\".  Save As of only instance edits the generic's Family Table which must be modifiable in this project.")
   public static final String CANNOT_COPY_INSTANCE_GENERIC_NOT_IN_PROJECT = "397";

   @RBEntry("Cannot copy objects in the list together as the contexts of their copies do not match. Either all copies should belong to PDM contexts or all of them should belong to the same project.")
   @RBComment("Error message thrown by CopyManagerUtility when some copies are from PDM context and others are from project.")
   public static final String CANNOT_COPY_PDM_PROJECT = "398";

   @RBEntry("Cannot copy objects in the list together as their copies belong to different projects.")
   @RBComment("Error message thrown by CopyManagerUtility when copies are from different projects.")
   public static final String CANNOT_COPY_MULTIPLE_PROJECTS = "399";
   
   @RBEntry("Part \"{0}\" cannot be saved as a new object. The new Part needs to be linked to the new Dynamic Document and this action is not supported. To change the Part reference save the Dynamic Document and the Part separately, then edit the document to reference the new Part.")
   public static final String CAN_NO_SAVE_AS_REFERENCE_PART_TOGETHER = "400";
}
