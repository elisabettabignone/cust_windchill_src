/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.epm.util;

import wt.util.resource.*;

@RBUUID("wt.epm.util.EPMResource")
public final class EPMResource_it extends WTListResourceBundle {
   @RBEntry("Nome obbligatorio")
   public static final String NAME_REQUIRED = "1";

   @RBEntry("Dati è un campo obbligatorio")
   public static final String DATA_REQUIRED = "2";

   @RBEntry("Impossibile eliminare il link: esistono link di sovrascrittura")
   public static final String GENERICMEMBERLINK_DELETE_NOT_ALLOWED = "3";

   @RBEntry("Impossibile memorizzare/modificare {0}, link non persistente o non originante dallo stesso documento CAD")
   public static final String INSTMEMBERLINK_STORE_NOT_ALLOWED1 = "4";

   @RBEntry("Impossibile memorizzare/modificare {0}: l'istanza di family table non è persistente o non appartiene al documento CAD originale")
   public static final String INSTMEMBERLINK_STORE_NOT_ALLOWED3 = "6";

   @RBEntry("Impossibile memorizzare/modificare {0} fra {1} e {2}. L'attributo di trasformazione è impostato per il link, ma la quantità è diversa da uno o il membro non è in posizione corretta")
   public static final String GENERICMEMBERLINK_STORE_NOT_ALLOWED = "7";

   @RBEntry("L'applicazione non è autorizzata a modificare l'oggetto")
   public static final String INVALID_CHANGING_APPLICATION = "11";

   @RBEntry("Impossibile duplicare. SupportingData non è stato creato.")
   public static final String CANNOT_DUPLICATE = "15";

   @RBEntry("Impossibile ottenere il valore dal generatore di sequenza.")
   public static final String CANNOT_GET_SEQUENCE = "16";

   @RBEntry("La classe link {0} serve solo alla navigazione; impossibile costruire, modificare o eliminare le istanze di questa classe.")
   public static final String CANNOT_MODIFY_OR_DELETE_LINK = "18";

   @RBEntry("Impossibile selezionare una versione di {0}. La spec di config non specifica quale usare.")
   public static final String BUILD_CANNOT_SELECT_VERSION = "19";

   @RBEntry("L'istanza di family table con l'identificatore {0} non appartiene al documento CAD {1}.")
   public static final String NO_SUCH_FAMILY_INSTANCE = "20";

   @RBEntry("{0} costruisce più parti. Impossibile selezionare quale usare.")
   public static final String SOURCE_BUILDS_MULTIPLE_PARTS = "21";

   @RBEntry("Impossibile eliminare {0}. Ad esso sono associate regole di creazione.")
   public static final String CANNOT_DELETE_BUILD_SOURCE = "22";

   @RBEntry("Impossibile spostare {0} in una cartella condivisa. L'origine ({1}) della cronologia build non è in uno schedario condiviso.")
   public static final String ILLEGAL_BUILDHIST_MOVE_FOLDER = "23";

   @RBEntry("Impossibile sottoporre {0} a Check-In in uno schedario condiviso. L'elemento ha una regola di creazione la cui origine ({1}) non è in una cartella condivisa. È necessario sottoporre prima a Check-In {1}.")
   public static final String ILLEGAL_BUILDRULE_MOVE_FOLDER = "24";

   @RBEntry("Impossibile effettuare il Check-In di {0} in una cartella condivisa. L'elemento è associato in modo attivo a un'origine ({1}) che non è in uno schedario condiviso.")
   public static final String BUILD_SOURCE_IN_PERSONAL_FOLDER = "25";

   @RBEntry("Impossibile effettuare il Check-In di {0}. I seguenti documenti CAD sono ancora sottoposti a Check-Out: {1}. Per il Check-In e la creazione della parte è necessario effettuare il Check-In anche di tutti i documenti CAD correlati sottoposti a Check-Out.")
   public static final String MUST_CHECK_IN_BUILD_SOURCE = "26";

   @RBEntry("Impossibile allegare criteri di ricerca ai documenti selezionati in base alle informazioni della baseline. La baseline referenziata potrebbe essere stata eliminata.")
   public static final String INVALID_DOC_BASELINE_CONFIG_SPEC = "27";

   @RBEntry("Un ID di caso d'impiego esiste già per il percorso specificato.")
   public static final String PATH_ALREADY_HAS_ID = "28";

   @RBEntry("Per il percorso specificato non esiste ID del caso d'impiego.")
   public static final String PATH_HAS_NO_ID = "29";

   @RBEntry("Impossibile effettuare Check-In del documento {0}. Dipende da {1}, che è sottoposto a Check-Out. Devono essere sottoposti a Check-In assieme.")
   public static final String CANNOT_VAULT_DOCUMENT_CHECKOUT = "30";

   @RBEntry("Impossibile effettuare Check-In del documento {0}. Dipende da {1}, che è in uno schedario personale.")
   public static final String CANNOT_VAULT_DOCUMENT_PERSONAL = "31";

   @RBEntry("La cartella {0} appartiene al Workspace {1}, e non può essere eliminata.")
   public static final String FOLDER_BELONGS_TO_WORKSPACE = "32";

   @RBEntry("L'oggetto {0} è posseduto da {1}, che non permette modifiche di {2}")
   public static final String OPERATION_VETOED = "33";

   @RBEntry("La proprietà wt.epm.BaselineFolder non è impostata. Impostarla su una cartella valida.")
   public static final String BASELINE_FOLDER_NOT_SET = "34";

   @RBEntry("La classe non ha la proprietà numero")
   public static final String NUMBER_MISSING = "35";

   @RBEntry("La classe non ha la proprietà nome")
   public static final String NAME_MISSING = "36";

   @RBEntry("{0}")
   public static final String NUMBER = "37";

   @RBEntry("{0}")
   public static final String NAME = "38";

   @RBEntry("Il valore per l'argomento \"{0}\" non può essere nullo")
   public static final String NULL_ARGUMENT = "39";

   @RBEntry("Un EPMDocument non può avere regole di creazione e un'effettività")
   public static final String BUILD_EFFECTIVITY_CONFLICT = "40";

   @RBEntry("Impossibile modificare o eliminare questo indicatore di riferimento perché è posseduto dall'applicazione \"{0}\"")
   public static final String REFERENCE_DESIGNATOR_MODIFY_OR_DELETE_ON_BUILT_LINK = "41";

   @RBEntry("Fornire identificatore per l'indicatore di riferimento")
   public static final String MUST_PROVIDE_IDENTIFICATION_TAG = "42";

   @RBEntry("Impossibile creare una nuova revisione di {0} perché è sottoposto a Check-Out.")
   public static final String CANNOT_REVISE_CHECKED_OUT_OBJECT = "43";

   @RBEntry("Impossibile modificare il nome CAD di {0} perché {1} è in {2} ({3}).")
   @RBComment("Error message to show when document whose CADName is being changed is in workspace")
   @RBArgComment0("EPMDocumentMaster whose CADName is being changed")
   @RBArgComment1("Document which is in workspace")
   @RBArgComment2("Workspace name containing (arg1)")
   @RBArgComment3("Workspace owner name.")
   public static final String CANNOT_RENUMBER_IN_WORKSPACE = "44";

   @RBEntry("Impossibile modificare il nome CAD di {0} perché il contenuto {1} è già in uso.")
   public static final String NEW_NUMBER_IN_USE = "45";

   @RBEntry("Impossibile modificare il nome CAD di {0} perché il nuovo nome CAD {1} ha un'estensione che non corrisponde alla vecchia.")
   public static final String INVALID_EXTENSION = "46";

   @RBEntry("Impossibile modificare il nome CAD di {0} perché l'iterazione {1} è stata rilasciata.")
   public static final String RELEASED_ITERATION = "47";

   @RBEntry("Impossibile modificare il nome CAD di {0} perché {1} è sottoposto a Check-Out da {2}.")
   @RBComment("Error message to show when document whose CADName is being changed is checkedout")
   @RBArgComment0("EPMDocumentMaster whose CADName is being changed")
   @RBArgComment1("Document which is checked-out")
   @RBArgComment2("User name to whom document (arg1) is checked out")
   public static final String CANNOT_RENUMBER_IF_CHECKED_OUT = "48";

   @RBEntry("Impossibile ridurre la quantità di questo link ad un numero inferiore a quello di casi d'impiego del link: {0}.")
   public static final String CANNOT_DECREASE_LINK_QUANTITY = "49";

   @RBEntry("Impossibile aggiungere un altro caso d'impiego al link. Ne contiene già {0} e solo {0} sono consentiti.")
   public static final String ONE_TOO_MANY_OCCURRENCES = "50";

   @RBEntry("Impossibile aggiungere {0} casi d'impiego al link. Ne consente solo {1}.")
   public static final String TOO_MANY_OCCURRENCES = "51";

   @RBEntry("I documenti CAD creati con l'applicazione {0} non possono avere un nome file.")
   @RBArgComment0("Authoring Application value")
   public static final String FILE_NAME_NOT_ALLOWED = "52";

   @RBEntry("Il documento CAD/dinamico deve avere un nome file non nullo.")
   @RBArgComment0("Identity of EPMDocumentMaster")
   public static final String FILE_NAME_REQUIRED = "53";

   @RBEntry("Impossibile ottenere il delegato per l'applicazione di progettazione {0}. Verificare le voci di service.properties per wt.epm.CADNameDelegate.")
   @RBArgComment0("Authoring Application value")
   public static final String AUTH_APP_DELEGATE_NOT_FOUND = "54";

   @RBEntry("()")
   @RBComment("Error message to show CADName + CADContext is not unique - use when CAD name, CAD Context are null")
   public static final String CONTEXTNAME_BOTH_NULL = "55";

   @RBEntry("({0})")
   @RBComment("Error message to show CADName + CADContext is not unique -use when there is no CAD context")
   @RBArgComment0("CAD name value")
   public static final String CONTEXTNAME_CONTEXT_NULL = "56";

   @RBEntry("{0} ()")
   @RBComment("Error message to show CADName + CADContext is not unique -use when there is no CAD name")
   @RBArgComment0("CAD context value")
   public static final String CONTEXTNAME_NAME_NULL = "57";

   @RBEntry("{0} ({1})")
   @RBComment("Error message to show CADName + CADContext is not unique - Use when there is CAD Name and CAD Context")
   @RBArgComment0("CAD context value")
   @RBArgComment1("CAD name value")
   public static final String CONTEXTNAME = "58";

   @RBEntry("{0} non può essere contenuto in {1}. {0} è un contenitore.")
   @RBComment("Error message to show that a container document cannot become contained in another container document.")
   @RBArgComment0("Document that is already a container document")
   @RBArgComment1("Container Document")
   public static final String CANNOT_BECOME_CONTAINED = "59";

   @RBEntry("{0} non può essere contenuto in {1}.  {0} è già contenuto in {2}.")
   @RBComment("Error message to show when contained document is already contained in some other container")
   @RBArgComment0("Contained Document")
   @RBArgComment1("Container Document")
   @RBArgComment2("Another Container Document that already contains the contained Document (arg0)")
   public static final String CANNOT_BE_IN_TWO_CONTAINERS = "60";

   @RBEntry("Impossibile effettuare il Check-In di {0}. L'utente deve anche effettuare il Check-In di {1}.")
   @RBComment("Error message to show when a checkin dependency exists for a document")
   @RBArgComment0("Name of the EPMDocument being checked in")
   @RBArgComment1("Name of the EPMDocument which also must be checked in")
   public static final String MUST_CHECK_IN_DEPENDENT = "61";

   @RBEntry("Impossibile annullare il Check-Out di {0}. L'utente deve anche annullare il Check-Out di {1}.")
   @RBComment("Error message to show when a undoCheckout dependency exists for a document")
   @RBArgComment0("Name of the EPMDocument being checked in")
   @RBArgComment1("Name of the EPMDocument which also must be checked in")
   public static final String MUST_UNDO_CHECKOUT_OF_DEPENDENT = "62";

   @RBEntry("Impossibile creare una nuova revisione di un documento appena creato.")
   @RBComment("Error message to show when a newly created document is being revised")
   public static final String CANNOT_REVISE_NEW_DOCUMENT = "63";

   @RBEntry("Impossibile spostare {0} in una cartella condivisa, perché è contenuto nel documento ({1}), che è sottoposto a Check-Out o si trova in una cartella personale.")
   @RBComment("Error message to show when user attempts to move a contained document to shared folder while its container is checked out or in a personal folder. User must first check in or move the container document to a shared folder.")
   @RBArgComment0("Name of the contained document being moved to shared folder")
   @RBArgComment1("Name of the container document for arg0")
   public static final String CANNOT_MOVE_CONTAINED_DOCUMENT = "64";

   @RBEntry("Impossibile impostare geometryModified su false. L'EPMContainedLink tra il contenitore {0} e l'oggetto contenuto {1} è già contrassegnato come modificato.")
   @RBComment("Error message to show when user attempts to set the geometryModified flag to false on an EPMContainedLink which is already marked as true.")
   @RBArgComment0("Name of the container document associated to the link")
   @RBArgComment1("Name of the contained document associated to the link")
   public static final String CANNOT_RESET_GEOMETRY_MODIFIED_FLAG_TO_FALSE = "65";

   @RBEntry("Impossibile impostare geometryModified su true. L'EPMContainedLink tra il contenitore {0} e l'oggetto contenuto {1} è stato sostituito.")
   @RBComment("Error message to show when user attempts to set the geometryModified flag to true on a superceded EPMContainedLink.")
   @RBArgComment0("Name of the container document associated to the link")
   @RBArgComment1("Name of the contained document associated to the link")
   public static final String SUPERCEDED_CANNOT_CHANGE_GEOMETRY_MODIFIED_FLAG = "66";

   @RBEntry("{0} è un documento generico. Non può diventare un documento CAD autonomo.")
   @RBComment("Error message indicating that a generic cannot be made into a standalone CADDocument")
   @RBArgComment0("Name of the generic EPMDocument")
   public static final String CANNOT_MAKE_GENERIC_STANDALONE = "67";

   @RBEntry("{0} non può essere una variante di {2}.  {0} è già una variante di {1}.")
   @RBComment("Error message to show when varaint document is already variant of some other generic")
   @RBArgComment0("variant Document")
   @RBArgComment1("Another Generic Document that already  is generic  for variant(arg0)")
   @RBArgComment2("Generic Document")
   public static final String CANNOT_MAKE_AS_VARIANT = "68";

   @RBEntry("Impossibile interrompere il collegamento di {0}. Impossibile interrompere il collegamento contenitore/documento contenuto.")
   @RBComment("Error message to show when user tries to branch document that is either a generic or instance")
   @RBArgComment0("Name of the EPMDocument being branched")
   public static final String CANNOT_BRANCH_DOCUMENT = "69";

   @RBEntry("{0} non può referenziare {1}. I documenti CAD non possono referenziare gli oggetti {2}.")
   @RBArgComment0("Identity of the CAD document")
   @RBArgComment1("Identity of the object that the CAD document wants to reference")
   @RBArgComment2("Type of the object that the CAD document wants to reference")
   public static final String INVALID_REFERENCES = "70";

   @RBEntry("Impossibile creare una nuova revisione della famiglia EPM incompleta {0}.")
   @RBArgComment0("Identity of the EPMFamily(top level generic EPMDocument)")
   public static final String CANNOT_REVISE_INCOMPLETE_FAMILY = "71";

   @RBEntry("Impossibile copiare {0}. Impossibile copiare il contenuto/documento contenuto.")
   @RBComment("Error message to show when user tries to copy document that is either a generic or instance")
   @RBArgComment0("Name of the EPMDocument being copied")
   public static final String CANNOT_COPY_DOCUMENT = "72";

   @RBEntry("{0} è un oggetto contenuto. Non può essere eliminato.")
   @RBComment("Error message indicating that a contained CADDocument (family instance) cannot be deleted")
   @RBArgComment0("Name of the contained EPMDocument")
   public static final String CANNOT_DELETE_CONTAINED_DOCUMENT = "73";

   @RBEntry("L'istanza {0} ha delle iterazioni successive che non sono più parte della famiglia. Queste iterazioni autonome devono essere sottoposte a rollback prima di essere eliminate o prima di effettuare il rollback della famiglia {1}.")
   @RBComment("Error message indicating that a contained CADDocument (family instance) has later iterations which are standalone.  The standalone iterations must be rolled back before deleting the family.")
   @RBArgComment0("Name of the contained EPMDocument (instance)")
   @RBArgComment1("Name of the container (family)")
   public static final String MUST_ROLLBACK_STANDALONE_INSTANCE = "74";

   @RBEntry("{0} non può essere un documento autonomo. {0} può essere autonomo dopo aver rimosso {0} da {1}.")
   @RBComment("Error message indicating that a instance can not be made standalone if it exists in latest generic. User should use removeFromFamily before using makeStandalone.")
   @RBArgComment0("Name of the instance document that user tried to makeStandalone")
   @RBArgComment1("Name of the latest generic document that contains this instance")
   public static final String CANNOT_MAKE_STANDALONE_REMOVE_FROM_LATEST = "75";

   @RBEntry("{0} non può essere un documento autonomo. È necessario effettuare il Check-In di {1}.")
   @RBComment("Error message indicating that a instance can not be made standalone unless the latest generic is checked in.")
   @RBArgComment0("Name of the instance document that user tried to makeStandalone")
   @RBArgComment1("Name of the checked out latest generic document that contains this instance")
   public static final String CANNOT_MAKE_STANDALONE_CHECKIN_LATEST = "76";

   @RBEntry("{0} non può essere contenuto in {1}.  {1} è già contenuto in {2}.")
   @RBComment("Error message to show that contained document cannot become a container.")
   @RBArgComment0("Contained Document")
   @RBArgComment1("Another Contained Document that is already contained in (arg2)")
   @RBArgComment2("Container Document")
   public static final String CANNOT_BECOME_CONTAINER = "77";

   @RBEntry("{0} non può contenere se stesso.")
   @RBComment("Error message to show that a document cannot contain itself.")
   @RBArgComment0("Document")
   public static final String CANNOT_CONTAIN_ITSELF = "78";

   @RBEntry("Questa iterazione di {0} ha già un link Contenuto in all'iterazione di {1}.")
   @RBComment("Error message to show that an iteration of the contained document cannot have multiple Contained In links to the same iteration of its container document.")
   @RBArgComment0("Contained Document")
   @RBArgComment1("Container Document")
   public static final String NO_DUPLICATE_CONTAINEDIN_LINKS = "79";

   @RBEntry("{0} non può essere contenuto in {1}.  {1} contiene già una versione successiva dello stesso documento.")
   @RBComment("Error message to show that you cannot add an earlier version of document to a container that already contains a later version of the same document.")
   @RBArgComment0("Contained Document (earlier version)")
   @RBArgComment1("Container Document")
   public static final String CONTAINER_HAS_LATER_VERSION = "80";

   @RBEntry("{0} non può essere contenuto in {1}.  {0} è già contenuto in una versione successiva dello stesso contenitore.")
   @RBComment("Error message to show that add a document to a earlier version of its container if it is already contained in a later version.")
   @RBArgComment0("Contained Document")
   @RBArgComment1("Container Document (earlier version)")
   public static final String CONTAINED_IN_LATER_VERSION = "81";

   @RBEntry("Impossibile modificare il nome CAD di {0} perché il documento è stato sottoposto a Check-Out da {1}.")
   @RBComment("Error message to show that you cannot change CADName of a document if it is checked out by another user.")
   @RBArgComment0("Identity of EPMDocumentMaster")
   @RBArgComment1("User name to whom document is checked out")
   public static final String CANNOT_RENUMBER_IF_CHECKED_OUT_BY_OTHERS = "82";

   @RBEntry("Impossibile modificare il nome CAD di {0}. Non si dispone dei permessi di modifica per il master.")
   @RBComment("Error message to show that you cannot change CADName of a document if more than one versions of the document are checked out to you.")
   @RBArgComment0("Identity of EPMDocumentMaster")
   public static final String NO_MODIFY_PERMISSION_TO_CHANGE_CAD_NAME = "83";

   @RBEntry("Impossibile creare il documento segnaposto {0} nella cartella condivisa {1}.")
   @RBComment("Error message to show that you cannot create a place holder document in a shared folder.")
   @RBArgComment0("Identity of EPMDocument")
   public static final String PLACEHODER_NOT_ALLOWED_IN_SHARED_FOLDER = "84";

   @RBEntry("Impossibile sottoporre a Check-In il documento segnaposto {0}.")
   @RBComment("Error message to show that you cannot move a place holder document from personal folder to a shared folder.")
   public static final String CANNOT_MOVE_PLACEHODER_TO_SHARED_FOLDER = "85";

   @RBEntry("Impossibile trasformare il documento esistente {0} in un segnaposto.")
   @RBComment("Error message to show that you cannot change an existing document to a place holder.")
   @RBArgComment0("Identity of EPMDocument")
   public static final String CANNOT_CHANGE_TO_PLACEHOLDER = "86";

   @RBEntry("Impossibile spostare {0} in una cartella condivisa perché dipende dal segnaposto {1}.")
   @RBComment("Error message to show that you cannot move an assembly to shared folder if it depends on a place holder.")
   @RBArgComment0("Identity of EPMDocument")
   public static final String CANNOT_VAULT_ASSEMBLY_DEPENDENT_ON_PLACEHOLDER = "87";

   @RBEntry("Impossibile modificare il nome CAD di {0}. Non si dispone dei permessi di modifica per una o più iterazioni.")
   @RBComment("Error message to show that you cannot change CADName of a document as you do not have modify access to one or more iterations.")
   @RBArgComment0("Identity of EPMDocumentMaster")
   public static final String CANNOT_RENUMBER_IF_NO_ACCESS_TO_ITERATION = "88";

   @RBEntry("Solo un EPMDocument può essere proprietario di una configurazione archiviata.")
   @RBComment("Error message to show that you can only add EPMDocument to an as-stored configuration as owner.")
   public static final String OWNER_MUST_BE_EPMDOCUMENT = "89";

   @RBEntry("Solo WTDocument e EPMDocument possono essere membri di una configurazione archiviata")
   @RBComment("Error message to show that you can only add EPMDocument or WTDocument to an as-stored configuration as member.")
   public static final String MEMBER_MUST_BE_DOCUMENT = "90";

   @RBEntry("Impossibile aggiungere oggetti sottoposti a Check-Out in una configurazione archiviata.")
   @RBComment("Error message to show that you can only add EPMDocument or WTDocument that are not checked out to an as-stored configuration.")
   public static final String CANNOT_ADD_WORKING_COPIES_TO_AS_STORED = "91";

   @RBEntry("Impossibile eliminare le configurazioni archiviate EPM (EPMAsStoredConfig). Non si dispone dei permessi di modifica di tutti i proprietari della stessa configurazione archiviata.")
   @RBComment("Error message to show that the user does not have the permission to modify all the owners in the as-stored configuration.")
   public static final String CANNOT_DELETE_AS_STORED_CONFIG = "92";

   @RBEntry("impossibile aggiungere lo stesso oggetto ad una configurazione archiviata due volte (come proprietario e come membro).")
   @RBComment("Error message to show that you cannot add the same object to an as-stored configuration twice, either as owner or member.")
   public static final String CANNOT_ADD_OBJECT_TWICE_TO_AS_STORED = "93";

   @RBEntry("impossibile aggiungere due oggetti con lo stesso master ad una configurazione archiviata (come proprietario e come membro).")
   @RBComment("Error message to show that you cannot add two objects with the same master to an as-stored configuration, either as owner or member.")
   public static final String CANNOT_ADD_OBJECTS_WITH_SAME_MASTER_TO_AS_STORED = "94";

   @RBEntry("Impossibile aggiungere proprietari che hanno una configurazione archiviata già associata ad una nuova configurazione archiviata.")
   @RBComment("Error message to show that the owners already have an associated as-stored configurations, therefore, they cannot be added to a new as-stored configuration.")
   public static final String OWNERS_ALREADY_HAVE_AS_STORED = "95";

   @RBEntry("Impossibile modificare il contenitore di workspace di {0}. Impossibile usare il workspace per l'utilizzo con {1}")
   @RBComment("Error message to show that you cannot change WTContainer associated with the EPMWorkspace.")
   @RBArgComment0("Identity of EPMWorkspace")
   @RBArgComment1("Identity of new WTContainer")
   public static final String CANNOT_CHANGE_WS_CONTAINER = "96";

   @RBEntry("Impossibile aggiungere {0} al workspace {1}. Il contenitore del workspace {2} non può condividere gli oggetti del {3}.")
   @RBComment("Error message to show that you cannot add given object to EPMWorkspace.")
   @RBArgComment0("Identity of object that user tried to add to EPMWorkspace")
   @RBArgComment1("Identity of EPMWorkspace")
   @RBArgComment2("Identity of WTContainer currently associated with EPMWorkspace.")
   @RBArgComment3("Identity of WTContainer currently associated with object.")
   public static final String CANNOT_ADD_TO_WS_CONTAINER = "97";

   @RBEntry("Impossibile creare il link {0}. {1} non può condividere gli oggetti di {2}.")
   @RBComment("Error message to show that you cannot link across these WTContainers/Solutions.")
   @RBArgComment0("Class name of the link that user tried to create")
   @RBArgComment1("WTContainer for the Role A")
   @RBArgComment2("WTContainer for the Role B")
   public static final String CANNOT_CREATE_CROSS_CONTAINER_LINK = "98";

   @RBEntry("Impossibile copiare perché i documenti EPMDocuments da copiare contengono una famiglia incompleta e/o incompatibile per l'oggetto generico {0}.")
   @RBComment("Error message to show during multi-object copy if the set of objects contain incomplete or incompatible family table.")
   @RBArgComment0("Identity of generic EPMDocument")
   public static final String CANNOT_COPY_FAMILY_INCOMPLETE = "99";

   @RBEntry("Impossibile copiare perché i documenti EPMDocuments da copiare contengono una famiglia incompleta e/o incompatibile per l'istanza {0}.")
   @RBComment("Error message to show during multi-object copy if the set of objects contain incomplete or incompatible family table.")
   @RBArgComment0("Comma seperated Identities of instance EPMDocuments")
   public static final String CANNOT_COPY_FAMILY_INCOMPLETE_INSTANCE = "100";

   @RBEntry("Il metodo copyRelationship per un oggetto singolo non è supportato per i link della classe {0}.")
   @RBComment("Error message to show when single object copyRelationship method is called.")
   @RBArgComment0("Link class name")
   public static final String NOT_SUPPORTED_COPY_RELATIONSHIP = "101";

   @RBEntry("Impossibile gestire gli oggetti della classe seguente per l'eliminazione: {0}.")
   @RBComment("Error message to show, when objects other than WTPart and EPMDocument, are selected for deletion.")
   @RBArgComment0("Class name of the object selected by the user.")
   public static final String CANNOT_HANDLE_OBJECTS_FOR_DELETION = "102";

   @RBEntry("Selezionare le ultime iterazioni per l'eliminazione. Impossibile eliminare le iterazioni intermedie.")
   @RBComment("Error message to show, when intermediate iterations of the objects are selected for deletion.")
   public static final String CANNOT_DELETE_INTERMEDIATE_ITERATIONS = "103";

   @RBEntry("Il documento d'istanza {0} del documento generico {1} manca dall'elenco di oggetti da eliminare.")
   @RBComment("Error message to show during multi-delete when generic is deleted without its instance.")
   @RBArgComment0("Identity of instance EPMDocument")
   @RBArgComment1("Identity of generic EPMDocument")
   public static final String INSTANCE_MISSING_IN_DELETE = "104";

   @RBEntry("Il documento generico {0} del documento d'istanza {1} manca dall'elenco di oggetti da eliminare.")
   @RBComment("Error message to show during multi-delete when instance is deleted without its generic.")
   @RBArgComment0("Identity of generic EPMDocument")
   @RBArgComment1("Identity of instance EPMDocument")
   public static final String GENERIC_MISSING_IN_DELETE = "105";

   @RBEntry("L'eliminazione di {0} è fallita.")
   @RBComment("Error message to show when error occurs during deleting.")
   @RBArgComment0("Identity of EPMDocument/WTPart/WTDocument")
   public static final String ERROR_IN_DELETE = "106";

   @RBEntry("Impossibile aggiungere uno o più documenti in {0} in quanto i relativi nomi CAD non sono univoci nella posizione. I nomi CAD duplicati sono {1}.")
   @RBComment("Error message to show when user tries to create/share CADDocument and CADDocument with same already exists in Sandbox")
   @RBArgComment0("Identity of the Project (Sandbox Container)")
   @RBArgComment1("List of duplicate CADNames of the document")
   public static final String DUPLICATE_FILE_NAME = "107";

   @RBEntry("Impossibile effettuare il Check-In del documento CAD/dinamico da {0}. Il contenitore PDM contiene già un documento CAD/dinamico con nome file {1}.")
   @RBComment("Error message to show when user tries to checkin CADDocument and CADDocument with same CADName already exists in the PDM.")
   @RBArgComment0("Identity of the Project (Sandbox Container)")
   @RBArgComment1("CADName of the document")
   public static final String DUPLICATE_FILE_NAME_IN_SB_CHECKIN = "108";

   @RBEntry("La condivisione di {0} non è modificabile. Si trova in un workspace nel progetto {1}.")
   @RBComment("Error message for removing/switching/disabling share to an object in a Workspace of the given Project")
   @RBArgComment0("Identity of the object")
   @RBArgComment1("Identity of the Project (Sandbox Container)")
   public static final String MODIFY_SHARE = "109";

   @RBEntry("Impossibile effettuare il Check-In o annullare il Check-Out di {0}. È necessario il documento {1}.")
   @RBComment("Error message for SandBox Checkin/undoCheckout of an EPMDocument with build dependencies")
   @RBArgComment0("Identity of the EPMDocument")
   @RBArgComment1("Identity of the WTPart/Image document")
   public static final String SBCHECKIN_BUILD_DEPENDENT = "110";

   @RBEntry("Impossibile effettuare il Check-In o annullare il Check-Out di {0}. Esiste un rapporto di dipendenza con {1}.")
   @RBComment("Error message for SandBox Checkin/undoCheckout of a WTPart/Image Document which has been built.")
   @RBArgComment0("Identity of the WTPart/Image Document")
   @RBArgComment1("Identity of the EPMDocument")
   public static final String SBCHECKIN_BUILT_DEPENDENT = "111";

   @RBEntry("Impossibile effettuare il Check-In o annullare il Check-Out del documento CAD {0} in quanto fa parte della stessa famiglia di un altro membro modificato: {1}")
   @RBComment("Error message for SandBox Checkin/undoCheckout of an incomplete family")
   @RBArgComment0("Name of the EPMDocument being processed")
   public static final String SBCHECKIN_INCOMPLETE_FAMILY = "112";

   @RBEntry("Impossibile effettuare il Check-In di {0}. L'utente deve anche effettuare il Check-In del dipendente facoltativo {1}.")
   @RBComment("Error message to show when user tries to Checkin EPMDocument without its Optional Dependents, when it is enforced to include its Optional Dependents.")
   @RBArgComment0("Name of the EPMDocument being checked in")
   @RBArgComment1("Name of the EPMDocument which also must be checked in")
   public static final String MUST_CHECK_IN_OPTIONAL_DEPENDENT = "113";

   @RBEntry("Impossibile aggiornare il workspace {0}. Renderebbe incompatibile la famiglia con generico {1}.")
   @RBComment("Error message for update of a Workspace causing an incompatible family to exist in the workspace")
   @RBArgComment0("Name of the EPMWorkspace being updated.")
   @RBArgComment1("Name of the EPMDocument representing the generic of the incompatible family.")
   public static final String INCOMPATIBLE_FAMILY = "114";

   @RBEntry("Impossibile contrassegnare il progetto {0} per l'eliminazione. Al progetto sono associati dei workspace: {1}.")
   @RBComment("Error message for marking for delete a project with associated workspace(s)")
   @RBArgComment0("project name")
   @RBArgComment1("list of associated workspaces")
   public static final String DELETE_WORKSPACES_PRIOR_TO_PROJECT_DELETE = "115";

   @RBEntry("{0} è parte di una family, alcuni membri della quale non sono accessibili.")
   @RBComment("Error message for document which is part of a family, cannot be accessed by the current user.")
   @RBArgComment0("Name of the document")
   public static final String PART_OF_INACCESSIBLE_FAMILY = "116";

   @RBEntry("{0} non può essere eliminato perché è associato al workspace {1}")
   @RBComment("Error message for deleting a configspec with associated workspace(s)")
   public static final String CAN_NOT_DELETE_CONFIGSPEC_ASSOCIATE_TO_WORKSPACE = "117";

   @RBEntry("La condivisione di documenti CAD/dinamici tra progetti non è supportata.")
   @RBComment("Error message when the user tries to share CAD documents from project to project.")
   public static final String PROJECT_TO_PROJECT_SHARING_NOT_SUPPORTED = "118";

   @RBEntry("La revisione di parti nella cartella condivisa {0} e di documenti associati nella cartella personale {1} non è consentita.")
   @RBComment("Error message for revise of docs and parts because of incompatible folder options")
   @RBArgComment0("Path of the part folder.")
   @RBArgComment1("Path of the document folder.")
   public static final String CANNOT_REVISE_INCOMPATIBLE_FOLDER_OPTIONS = "119";

   @RBEntry("Impossibile aggiungere uno o più dei seguenti documenti CAD in {0}, {1}. Uno o più nomi file di dipendenti, istanze o generici di documenti non sono univoci in {0}. I nomi file duplicati sono {2}.")
   @RBComment("Error message for Duplicate CADName for Dependents")
   @RBArgComment0("Identity of the Project (Sandbox Container)")
   @RBArgComment1("List of CAD documents' CAD names.")
   @RBArgComment2("List of dependent documents' CADNames")
   public static final String DUPLICATE_FILE_NAME_FOR_DEPENDENT = "120";

   @RBEntry("Impossibile impostare ReviseOptions.objectToRevise su nullo durante la chiamata di reviseAll")
   @RBComment("Error message for ReviseOptions.objectToRevise set to null in reviseAll")
   public static final String REVISEOPTIONS_OBJECTTOREVISE_CANNOT_BE_NULL = "121";

   @RBEntry("reviseAll supporta solo oggetti EPMDocument e WTPart")
   @RBComment("Error message for reviseAll only supports EPMDocuments and WTParts")
   public static final String ONLY_EPMDOCUMENTS_AND_WTPARTS_SUPPORTED_FOR_REVISEALL = "122";

   @RBEntry("Impossibile effettuare il Check-In di {0}. L'istanza è nuova ed è stata rimossa dalla family table {1}")
   @RBComment("Error message for SBCheckin of new instance removed from family")
   public static final String CAN_NOT_SBCHECKIN_ORPHANED_INSTANCE = "123";

   @RBEntry("{0} non è compatibile con l'applicazione di creazione, {1}.")
   @RBComment("Error message for setting an incompatible Authoring App Version onto EPMDocument or EPMSupportingData.  That is, the version's authoring application is different from the authoring application on document or supporting data.")
   @RBArgComment0("Value of the Authoring Application Version.")
   @RBArgComment1("Value of the Authoring Application on EPMDocument or EPMSupportingData.")
   public static final String IMCOMPATIBLE_AUTHORING_APP = "124";

   @RBEntry("La seguente versione dell'applicazione di creazione non è definita nel sistema: {0} {1} {2}")
   @RBComment("Error message at import if the authoring application is not defined in the target Windchill.  Authoring Application, Sequence Number and an internal string identifies the version of an Authoring Application.")
   @RBArgComment0("Authoring Application value")
   @RBArgComment1("Authoring Application's version string")
   @RBArgComment2("Authoring Application's version's sequence number")
   public static final String AUTHORING_APP_VERSION_NOT_EXIST = "125";

   @RBEntry("Impossibile sottoporre a Check-In i documenti CAD/dinamici in quanto i relativi nomi file non sono univoci in PDM. I nomi duplicati sono {0}.")
   @RBComment("Error message at checking multiple EPMDocument from a sandbox project to PDM.  A subset of the EPMDocuments cannot be checked in to PDM because their CAD name or file name are not unique in PDM.  Therefore, the multi-object sandbox check in fails.")
   @RBArgComment0("List of duplicate CAD names")
   public static final String NON_UNIQUE_CAD_NAME_IN_PDM_FOUND_AT_SBCI = "126";

   @RBEntry("Impossibile sottoporre a Check-In i documenti CAD/dinamici in quanto i relativi nomi nuovi file non sono univoci in {0}. I nomi duplicati sono {1}")
   @RBComment("Error message at checking multiple EPMDocument with new CAD names from a sandbox project to PDM.  The EPMDocument with new CAD names cannot be shared back to sandbox project because their new CAD names are not unique in the sandbox.  Thus, the check in operation fails.")
   @RBArgComment0("Identity of the Sandbox project")
   @RBArgComment1("List of duplicate CAD names")
   public static final String NON_UNIQUE_CAD_NAME_IN_SB_FOUND_AT_SBCI = "127";

   @RBEntry("Impossibile effettuare il Check-In del documento CAD {0}. È necessario includere la WTPart {1} che è stata dissociata da {0} nel progetto.")
   @RBComment("Error message for SandBox Checkin of an EPMDocument without build dependencies")
   @RBArgComment0("Identity of the EPMDocument")
   @RBArgComment1("Identity of the WTPart")
   public static final String SBCHECKIN_NO_BUILD_DEPENDENT = "128";

   @RBEntry("Impossibile eliminare il tipo EPM {0} fornito dal sistema.")
   @RBComment("Error message for Deleting EPM SoftType")
   @RBArgComment0("Identity of the Soft Type")
   public static final String CANNOT_DELETE_EPM_SOFTTYPE = "129";

   @RBEntry("{0} contiene l'attributo {1} che non fa parte della definizione del tipo.")
   @RBComment("Error message when EPM Object contains attribute not existing in default type definition")
   @RBArgComment0("Identity of the object")
   @RBArgComment1("Attribute Name")
   public static final String ATTR_NOT_IN_SOFTTYPE = "130";

   @RBEntry("Impossibile eliminare il documento CAD/dinamico {0} perché si trova nei workspace che seguono: {1}. L'eliminazione di documenti nel workspace non è consentita per l'applicazione di creazione {2}.")
   @RBComment("Error message for deleting EPMDocument in workspace when it is not allowed by authoring application delegate")
   @RBArgComment0("Identity of the EPMDocument")
   @RBArgComment1("Names of the workspaces in which the document exists.")
   @RBArgComment2("Authoring Application value")
   public static final String DO_NOT_ALLOW_DELETE_IN_WORKSPACE = "131";

   @RBEntry("{0} non può avere più di un tipo.")
   @RBComment("Error message for Creating another SoftType")
   @RBArgComment0("Identity of the Soft Type")
   public static final String CANNOT_CREATE_ANOTHER_SOFTTYPE = "132";

   @RBEntry("Impossibile tagliare {0} da {1} e incollarlo in {2}. Le operazioni di taglia e incolla non sono consentite sui nodi EPM.")
   @RBComment("Error message for vetoing cut/paste of EPM SoftType.")
   @RBArgComment0("Name of Type definition being pasted")
   @RBArgComment1("Cut from type definition name")
   @RBArgComment2("Paste to type definition name")
   public static final String CANNOT_CUTPASTE_EPM_SOFTTYPE = "133";

   @RBEntry("Impossibile eliminare il vincolo valore unico per il tipo EPM {0}.")
   @RBComment("Error message for vetoing EPM SoftType with missing single value attribute constraint.")
   @RBArgComment0("Name of type definition")
   public static final String MISSING_SINGLE_VALUE_ATTRIBUTE_CONSTRAINT = "134";

   @RBEntry("Per il tipo EPM {0} è consentito un solo vincolo intervallo o vincolo serie valori.")
   @RBComment("Error message for vetoing EPM SoftType with conflicting constraint.")
   @RBArgComment0("Name of type definition")
   public static final String CONFLICTING_CONSTRAINT = "135";

   @RBEntry("I nuovi oggetti possono essere solo WTPart, EPMDocument o relative sottoclassi.")
   @RBComment("Error message for adding new object of something other than EPMDocument or WTPart.")
   public static final String INVALID_NEW_OBJECT_CLASS = "136";

   @RBEntry("Impossibile impostare la cartella di destinazione per oggetti che non siano stati appena creati. {0} di {1} oggetti non sono nuovi.")
   @RBComment("Error message for setting target folder for objects that are not newly created")
   @RBArgComment0("Number of objects that are not new")
   @RBArgComment1("Number of objects passed to set target folder.")
   public static final String SET_TARGET_FOLDER_FOR_NEW_OBJECTS_ONLY = "137";

   @RBEntry("Impossibile ottenere la cartella destinazione per oggetti che non siano nuovi EPMDocument o WTPart. {0} di {1} oggetti non sono nuovi EPMDocument o WTPart.")
   @RBComment("Error message for getting target folder for objects that are not newly created EPMDocument or WTPart.")
   @RBArgComment0("Number of objects that are not new EPMDocument or WTPart.")
   @RBArgComment1("Number of objects passed to get target folder.")
   public static final String GET_TARGET_FOLDER_FOR_NEW_OBJECTS_ONLY = "138";

   @RBEntry("Impossibile aggiungere \"{0}\" ad una baseline perché è impossibile aggiungere due oggetti con lo stesso master ad una baseline.")
   public static final String SAME_MASTER_NOT_ALLOWED = "139";

   @RBEntry("Conflitto family table durante il Check-In")
   public static final String CHECKIN_FAMILY_TABLE_CONFLICT = "140";

   @RBEntry(" {0}: Impossibile copiare la family table perché uno o più generici immediati o intermedi mancano nell'elenco per l'operazione Salva con nome. È necessario aggiungere i seguenti generici all'elenco {1}")
   @RBComment("Error message to show during multi-object copy when user tries to copy family with one or more immediate or intermediate generics missing in Save As list.")
   @RBArgComment0("Original Family Table Object Name.")
   @RBArgComment1("List of missing immediate or intermediate generics.")
   public static final String CANNOT_COPY_FAMILY_WITHOUT_INTERMEDIATE_GENERIC = "141";

   @RBEntry("Impossibile copiare i membri della famiglia {0} nella family table esistente {1} perché uno o più generici sono sottoposti a Check-Out. È necessario sottoporre a Check-In i generici che seguono: {2}")
   @RBComment("Error message to show during single or multi-object copy when user tries to copy instance(s) with immediate generic or intermediate generic or top generic checked out.")
   @RBArgComment0("List of family members selected for copy")
   @RBArgComment1("Original Family Table Object Name.")
   @RBArgComment2("List of immediate generics or intermediate generics or top generic checked out")
   public static final String CANNOT_COPY_INSTANCE_WHEN_GENERIC_CHECKEDOUT = "142";

   @RBEntry(" Impossibile copiare i membri della famiglia {0} nella family table esistente {1} perché uno o più generici immediati o intermedi mancano nell'elenco per l'operazione Salva con nome. È necessario aggiungere i seguenti generici all'elenco {2}")
   @RBComment("Error message to show during multi-object copy when user tries to copy family with one or more immediate or intermediate generics missing in Save As list.")
   @RBArgComment0("List of family members selected for copy")
   @RBArgComment1("Original Family Table Object Name.")
   @RBArgComment2("List of missing immediate or intermediate generics.")
   public static final String CANNOT_COPY_NESTEDGENERIC_WITHOUT_INTERMEDIATE_GENERIC = "143";

   @RBEntry("Impossibile modificare il nome CAD dei documenti. È possibile che alcuni documenti siano stati sottoposti a Check-Out da altri utenti o che non si disponga dei permessi di modifica per tutte le versioni di alcuni documenti.")
   @RBComment("Error message for changing the CAD name of multiple documents' masters.")
   public static final String NO_MODIFY_PERMISSION_ON_ALL_VERSIONS_TO_CHANGE_CAD_NAME = "144";

   @RBEntry("Impossibile cambiare il nome CAD di {0} da {1} a {2}. Il nuovo nome CAD deve mantenere la medesima estensione.")
   @RBComment("Error message for renaming a file name of a CAD document.")
   @RBArgComment0("Identity of EPMDocumentMaster")
   @RBArgComment1("Old CAD name")
   @RBArgComment2("New CAD name")
   public static final String INVALID_CAD_NAME_EXTENSION = "145";

   @RBEntry("Impossibile eliminare {0} perché è utilizzato da {1}")
   @RBArgComment0("Display identity of the part node being deleted.")
   @RBArgComment1("Display identity of the assembly node which uses the part node.")
   public static final String CANNOT_DELETE_USED_BY = "146";

   @RBEntry("Impossibile eliminare {0} perché è referenziato da {1}")
   @RBArgComment0("Display identity of the document being deleted.")
   @RBArgComment1("Display identity of the document which references the document being deleted.")
   public static final String CANNOT_DELETE_REFERENCED_BY = "147";

   @RBEntry("Impossibile eliminare {0} perché è un generico per le varianti che seguono: {1}")
   @RBArgComment0("Display identity of the generic being deleted.")
   @RBArgComment1("Display identity of the variant.")
   public static final String CANNOT_DELETE_GENERIC = "148";

   @RBEntry("Impossibile eliminare {0} perché si trova nei workspace che seguono: {1}")
   @RBArgComment0("Display identity of the document being deleted.")
   @RBArgComment1("Names of the workspaces in which the document exists.")
   public static final String OBJECT_IN_WORKSPACE = "149";

   @RBEntry("Impossibile eliminare {0} perché è utilizzato dai dati di supporto che seguono: {1}")
   @RBArgComment0("Display identity of the authoring app version being deleted.")
   @RBArgComment1("Display identity of the supporting data which uses the app version being deleted.")
   public static final String CANNOT_DELETE_APP_DATA_VERSION_LINK = "150";

   @RBEntry("Impossibile eliminare {0} perché è utilizzato dai documenti CAD che seguono: {1}")
   @RBArgComment0("Display identity of the authoring app version being deleted.")
   @RBArgComment1("Display identity of the document which uses the app version being deleted.")
   public static final String CANNOT_DELETE_AUTHORING_APP_VERSION_LINK = "151";

   @RBEntry("Impossibile eliminare {0} perché è associato ai workspace che seguono: {1}")
   @RBArgComment0("Display identity of the checkpoint being deleted.")
   @RBArgComment1("Display identity of the workspace associated to the checkpoint being deleted.")
   public static final String CANNOT_DELETE_EPMCHECKPOINT = "152";

   @RBEntry("Impossibile caricare sul server il parametro \"{0}\" di tipo \"{1}\". Il tipo non è conforme al tipo di parametro Windchill \"{2}\".")
   @RBArgComment0("Parameter/Attribute Name.")
   @RBArgComment1("data type of parameter from CAD file.")
   @RBArgComment2("data type of IBA in Windchill.")
   public static final String INCONSISTENT_IBA_TYPE = "153";

   @RBEntry("L'oggetto {0} non dispone di un contenitore di attributi.")
   @RBArgComment0("Identity of object")
   public static final String NO_ATTR_CONTAINER = "154";

   @RBEntry("Impossibile aggiungere uno o più dei seguenti documenti CAD in {0}, {1}. Uno o più nomi di documento o dei relativi dipendenti, istanze o generici non sono univoci in {0}. I nomi CAD duplicati sono {2}.")
   @RBComment("Error message for a combination of duplicate CAD name and duplicate CAD name for Dependents")
   @RBArgComment0("List of Identity of documents.")
   @RBArgComment1("Identity of the Project (Sandbox Container)")
   @RBArgComment2("List of duplicate CADNames in the project.")
   public static final String DUPLICATE_DOCUMENT_AND_DEPENDENT_FILE_NAME = "155";

   @RBEntry("Sono stati trovati documenti incompatibili. Non è possibile generare dati neutri per documenti incompatibili.")
   @RBComment("Mostly a programming error. Exception message is logged if caller attempts to generate neutral data for incompatible set of documents/family table")
   public static final String CANNOT_GET_ND_FOR_INCOMPATIBLE_DOCUMENTS = "156";

   @RBEntry("Impossibile spostare l'oggetto nella cartella condivisa.")
   @RBComment("Cannot move to Shared folder.")
   public static final String CANNOT_MOVE_TO_SHARED_FOLDER = "157";

   @RBEntry("Impossibile sottoporre l'oggetto a Check-In nella cartella condivisa.")
   @RBComment("Cannot checkin to Shared folder.")
   public static final String CANNOT_CHECKIN_TO_SHARED_FOLDER = "158";

   @RBEntry("Impossibile eliminare la condivisione di {0} perché si trova nei workspace che seguono: {1}")
   @RBArgComment0("Display identity of the document that is being unshared.")
   @RBArgComment1("Identity of the workspaces in which the document exists.")
   public static final String SHARED_OBJECT_IN_PROJECT_WORKSPACE = "159";

   @RBEntry("Impossibile importare la definizione di feature {0}. È differente dalla definizione presente nel database.")
   @RBComment("Error message for import if the feature definition in the database is different from the xml file being imported.  This is an override conflict in Import/Export.")
   @RBArgComment0("Name of the feature definition.")
   public static final String CONFLICT_FTM_FEATURE_DEF = "160";

   @RBEntry("Impossibile importare la feature {0}. La relativa definizione {1} non è presente nel database.")
   @RBComment("Error message at import if the feature definition does not already exists in the database.")
   @RBArgComment0("Name of the feature.")
   @RBArgComment1("Name of the feature definition.")
   public static final String FTM_FEATURE_DEF_NOT_FOUND = "161";

   @RBEntry("Impossibile importare la definizione di parametro {0}. È differente dalla definizione presente nel database.")
   @RBComment("Error message at import if the parameter definition is different from the xml file being imported.  This is an override conflict in Import/Export.")
   @RBArgComment0("Name of the parameter definition.")
   public static final String CONFLICT_FTM_PARAMETER_DEF = "162";

   @RBEntry("Impossibile importare il parametro {0}. La relativa definizione {1} non è presente nel database.")
   @RBComment("Error message at import if the feature definition with the given name is not found on the family table master in the database.")
   @RBArgComment0("Name of the parameter.")
   @RBArgComment1("Name of the parameter definition.")
   public static final String FTM_PARAMETER_DEF_NOT_FOUND = "163";

   @RBEntry("Impossibile importare la feature {0} nella family table {1}. Il file xml non contiene un nome di definizione di feature.")
   @RBComment("Error message at import if the feature definition name is not in XML file for the family table object.")
   @RBArgComment0("Name of the family table feature.")
   @RBArgComment1("Identity of the family table.")
   public static final String FTM_FEATURE_DEF_NAME_NOT_FOUND = "164";

   @RBEntry("Impossibile importare il parametro {0} nella family table {1}. Il file xml non contiene un nome di definizione di parametro.")
   @RBComment("Error message at import if the parameter definition name is not in XML file for the family table object.")
   @RBArgComment0("Name of the family table parameter.")
   @RBArgComment1("Identity of the family table.")
   public static final String FTM_PARAMETER_DEF_NAME_NOT_FOUND = "165";

   @RBEntry("Impossibile creare/modificare/eliminare un oggetto EPMAuthoringAppVersion.")
   @RBComment("Error message for adding/modifying/deleting any EPMAuthoringAppVersion object.  EPMAuthoringAppVersion is loaded via Windchill loader.")
   public static final String CANNOT_CHANGE_AUTHORING_APP_VERSION = "166";

   @RBEntry("Attributi in arrivo {0} ignorati poiché non fanno parte della definizione del tipo.")
   @RBComment("Warning message during upload when designated parameters could not be added as IBA values as IBAs are not part of type definition.")
   @RBArgComment0("List of Attributes")
   public static final String INCOMING_ATTR_IGNORED = "167";

   @RBEntry("Attributi in arrivo {0} ignorati poiché il delegato attributo ha restituito un valore nullo.")
   @RBComment("Warning message during upload when no value is constructed by EPMAttributeDelegate for designated parameters and hence upload has ignored it.")
   @RBArgComment0("List of Attributes")
   public static final String IGNORED_BY_DELEGATE = "168";

   @RBEntry("Il valore dell'attributo {0} non è nel formato \"{1}\" e non può essere quindi analizzato come data. Eccezione: {2}")
   @RBComment("Warning message from attribute delegate when it fails to parse string value to date.")
   @RBArgComment0("Name of Attributes")
   @RBArgComment1("Format in which date is expected")
   @RBArgComment2("Message from ParseException")
   public static final String DATE_PARSE_WARN = "169";

   @RBEntry(" Conflitto di dipendenza")
   public static final String DEPENDENCY_CONFLICT = "170";

   @RBEntry("Il valore dell'attributo {0} è stato modificato in lettere minuscole.")
   @RBComment("Warning message when IBA value is changed to lower case by server.")
   @RBArgComment0("Name of Attributes")
   public static final String CHANGED_TO_LOWER = "171";

   @RBEntry("Il valore dell'attributo {0} è stato modificato in lettere maiuscole.")
   @RBComment("Warning message when IBA value is changed to upper case by server.")
   @RBArgComment0("Name of Attributes")
   public static final String CHANGED_TO_UPPER = "172";

   @RBEntry("Il valore dell'attributo {0} non è nel formato \"{1}\" e non può essere quindi analizzato come data.")
   @RBComment("Message from attribute delegate when it fails to parse string value to date.")
   @RBArgComment0("Name of Attributes")
   @RBArgComment1("Format in which date is expected")
   public static final String DATE_PARSE_ERROR = "174";

   @RBEntry(" Conflitto di creazione nuova revisione")
   public static final String REVISE_CONFLICT = "175";

   @RBEntry(" Impossibile creare una nuova revisione di \"{0}\": l'iterazione è stata creata in un progetto o è attualmente sottoposta a Check-Out in un progetto.")
   @RBArgComment0("Name of an object being revised.")
   public static final String CANNOT_REVISE_PROJECT_ITERATION = "176";

   @RBEntry("L'attributo {0} non fa parte della definizione del tipo.")
   @RBComment("Error message from setAttributes when IBA is not part of type definition.")
   @RBArgComment0("ExtHid of the attribute.")
   public static final String IBA_NOT_PART_OF_DEF = "177";

   @RBEntry("Impossibile annullare l'impostazione dell'attributo {0} incluso nella family table.")
   @RBComment("Error message from setAttributes when IBA that is part of family table is being unset")
   @RBArgComment0("ExtHid of the attribute.")
   public static final String IBA_PART_OF_FT_UNSET = "178";

   @RBEntry("L'attributo {0} non fa parte della family table e non può essere modificato direttamente nell'istanza {1}. È necessario modificare l'attributo nel generico di livello superiore.")
   @RBComment("Error message from setAttributes when IBA not part of family table is being modified directly at instance level.")
   @RBArgComment0("ExtHid of the attribute.")
   @RBArgComment1("Identity of instance EPMDocument.")
   public static final String IBA_NOT_PART_OF_FT = "179";

   @RBEntry("L'attributo {0} ha più di un valore in {1}.")
   @RBComment("Error message from setAttributes when more than one value exists for given document.")
   @RBArgComment0("ExtHid of the attribute.")
   @RBArgComment1("Identity of EPMDocument.")
   public static final String IBA_MULTIPLE_VALUE = "180";

   @RBEntry("Impossibile impostare un valore di tipo \"{0}\" per l'attributo \"{1}\" di tipo \"{2}\".")
   @RBArgComment0("data type of value passed.")
   @RBArgComment1("Attribute Name.")
   @RBArgComment2("data type of IBA in Windchill.")
   public static final String INCONSISTENT_IBA_VALUE = "181";

   @RBEntry(" L'attributo \"{0}\" di tipo \"{1}\" non è supportato da setAttributes.")
   @RBArgComment0("Attribute Name.")
   @RBArgComment1("data type of value passed.")
   public static final String UNSUPPORTED_IBA_TYPE = "182";

   @RBEntry(" La copia nel workspace non è supportata per i documenti CAD \"{0}\" creati con \"{1}\".")
   @RBArgComment0("Identity of generic/instance EPMDocument")
   @RBArgComment1("Authoring Application value.")
   public static final String CANNOT_COPY_IN_WORKSPACE = "183";

   @RBEntry("I workspace sono stati eliminati da un utente con privilegi amministrativi sul relativo contesto.")
   public static final String WORKSPACE_DELETED_MAIL_SUB  = "184";

   @RBEntry("Notifica di eliminazione workspace")
   public static final String WORKSPACE_DELETED_MAIL_MESSAGE = "185";

   @RBEntry("Il workspace {0} ha oggetti sottoposti a Check-Out.")
   @RBArgComment0("Name of workspace to be deleted.")
   public static final String CHECKED_OUT_OBJECT_IN_WORKSPACE = "186";

   @RBEntry("Il workspace {0} ha nuovi oggetti.")
   @RBArgComment0("Name of workspace to be deleted.")
   public static final String NEW_OBJECT_IN_WORKSPACE = "187";

   @RBEntry("Uno o più degli oggetti che si stanno copiando nel workspace non sono nel workspace \"{0}\". Tutti gli oggetti che si desidera copiare nel workspace devono esistere nel workspace.")
   @RBArgComment0("Identity of workspace")
   public static final String ALL_OBJECTS_NOT_IN_WORKSPACE = "188";

   @RBEntry("Uno o più degli oggetti trasmessi a updateParent sono nulli.")
   public static final String NOT_PROPER_REPLACE_DATA = "189";

   @RBEntry("Impossibile sostituire \"{0}\" con \"{1}\" perché le rispettive categorie secondarie di documento sono differenti.")
   @RBArgComment0("original document identity")
   @RBArgComment1("replace with document identity")
   public static final String DOCSUBTYPE_MISMATCH = "190";

   @RBEntry("Impossibile sostituire \"{0}\" con \"{1}\" perché le rispettive categorie di documento sono differenti.")
   @RBArgComment0("original document identity")
   @RBArgComment1("replace with document identity")
   public static final String DOCTYPE_MISMATCH = "191";

   @RBEntry("Impossibile sostituire \"{0}\" con \"{1}\" perché le rispettive applicazioni di creazione sono differenti.")
   @RBArgComment0("original document identity")
   @RBArgComment1("replace with document identity")
   public static final String AUTHTYPE_MISMATCH  = "192";

   @RBEntry("Impossibile aggiungere \"{1}\" come componente di \"{0}\" perché sono membri della stessa family table.")
   @RBArgComment0("Assembly document identity")
   @RBArgComment1("replace with document identity")
   public static final String PARENT_REPLACE_CHILD_IN_SAME_FT = "193";

   @RBEntry("Check-In non riuscito. {0} ha una dipendenza di tipo obbligatorio nei confronti di {1}. Risolvere la dipendenza manualmente e ritentare il Check-In.")
   public static final String CHECKIN_FAILED_DEP_REQUIRED = "194";

   @RBEntry("Check-In non riuscito. {0} ha una dipendenza di tipo non obbligatorio nei confronti di {1}. Utilizzare l'opzione di Check-In \"Definisci automaticamente gli oggetti incompleti\" per ignorare la dipendenza, oppure risolvere la dipendenza manualmente.")
   public static final String CHECKIN_FAILED_DEP_NON_REQUIRED = "195";

   @RBEntry("Impossibile sottoporre a Check-In il documento CAD {0} in quanto tutti i membri sottoposti a Check-Out di una stessa family table devono essere sottoposti a Check-In assieme. È necessario sottoporre a Check-In anche {1}.")
   @RBComment("Error message for SandBox Checkin of an incomplete family")
   @RBArgComment0("Name of the EPMDocument being processed")
   public static final String SBCHECKIN_INCOMPLETE_FAMILY_1 = "196";

   @RBEntry("Impossibile creare la relazione attraverso organizzazioni {0} perché i relativi punti finali, {1} e {2}, si trovano in namespace nome CAD differenti.")
   @RBComment("Error message to show that you cannot create a cross organization relationship.")
   @RBArgComment0("Class name of the link that user tried to create")
   @RBArgComment1("CAD Name Namespace of Role A ")
   @RBArgComment2("CAD Name Namespace of Role B")
   public static final String CANNOT_CREATE_CROSS_ORG_CONTAINER_LINK = "197";

   @RBEntry("Impossibile spostare il workspace da {0} a {1} perché si trovano in namespace nome CAD differenti.")
   @RBComment("Error message to show that you cannot move the EPMWorkspace from one organization to another organization.")
   @RBArgComment0("Identity of Workspace container")
   @RBArgComment1("Identity of the new container")
   public static final String CANNOT_MOVE_WORKSPACE = "198";

   @RBEntry("Impossibile aggiungere {0} a {1}. Il Workspace in {2} e l'oggetto in {3} si trovano in namespace nome CAD differenti.")
   @RBComment("Error message to show that you cannot add given object to EPMWorkspace because the workspace and the object are in different organizations.")
   @RBArgComment0("Identity of object that user tried to add to EPMWorkspace ")
   @RBArgComment1("Identity of EPMWorkspace")
   @RBArgComment2("Identity of CAD Name Namespace (Org Container) of the workspace.")
   @RBArgComment3("Identity of CAD Name Namespace (Org Container) of the object.")
   public static final String CANNOT_ADD_TO_WORKSPACE = "199";

   @RBEntry("{0} non è un tipo di contenitore workspace valido. Solo prodotti, librerie e progetti possono essere associati a un workspace.")
   @RBComment("Error message to show that you cannot associate the given container to EPMWorkspace in multi-tenant Windchill.")
   @RBArgComment0("Name of the container class")
   public static final String INVALID_WORKSPACE_CONTAINER_TYPE = "200";

   @RBEntry("Il contenitore organizzazione per {0} in {1} è nullo.")
   @RBComment("Error message to show that the document master is in a null org-container.  Usually, it only happens when the object is in Site.")
   @RBArgComment0("Identity of a EPMDocument, WTPart, WTDocument, a PDMLink container or a Project")
   @RBArgComment1("Identity of the container where the object resides.")
   public static final String NULL_ORG_CONTAINER_FOUND = "201";

   @RBEntry("Il valore wt.epm.CADNameUniquenessOption in wt.properties non è riconosciuto ({0}). Deve essere {1} o {2}.")
   @RBComment("Error message to show that the value of wt.epm.CADNameUniquenessOption in wt.properties is not valid.")
   @RBArgComment0("Value of wt.epm.CADNameUniquenessOption in wt.properties ")
   @RBArgComment1("One of the supported values.")
   @RBArgComment2("One of the supported values.")
   public static final String INVALID_FILE_NAME_UNIQUENESS_OPTION = "202";

   @RBEntry("Gli attributi in arrivo {0} non sono basati su file, pertanto non è possibile modificarli tramite l'applicazione di creazione.")
   @RBComment("Occurs during upload when designated parameter mapping to non-file based attribute is added by the authoring application")
   @RBArgComment0("List of Attributes")
   public static final String IGNORE_NONFILEBASED_ATTRIBUTE = "203";

   @RBEntry("\"{0}\" non può essere eliminato perché è in uso al momento.")
   @RBComment("Error message for Deleting EPM SoftType")
   @RBArgComment0("Type definition name")
   public static final String CANNOT_DELETE_INUSE_SOFTTYPE = "204";

   @RBEntry("Impossibile ottenere la definizione tipo di default \"{0}\". Definizioni tipo EPM di default non configurate correttamente, contattare l'amministratore.")
   @RBComment("Error message when default type definition is not found.")
   @RBArgComment0("Type definition logical identifier / external form")
   public static final String DEFAULT_TYPE_DEF_NOT_FOUND = "205";

   @RBEntry("Impossibile assegnare la definizione tipo \"{0}\" a {1}. La definizione tipo appartiene a un'organizzazione differente.")
   @RBComment("Error message when type definition cannot be assigned to CAD document as it belongs to different organization.")
   @RBArgComment0("Type definition display name")
   @RBArgComment1("Identity of EPMDocument")
   public static final String CANNOT_ASSIGN_TYPE_DEF = "206";

   @RBEntry("L'utente non dispone dei privilegi necessari per eliminare il workspace {0}.")
   @RBComment("Error message to show that you do not have admin access privilege to perform delete operation.")
   @RBArgComment0("Name of workspace")
   public static final String NO_ADMIN_PRIVILEGE_FOR_WORKSPACE_DELETE = "207";

   @RBEntry("Non è consentito sottoporre a Check-Out l'iterazione meno recente di {0}. L'iterazione meno recente non è stata creata, mentre l'iterazione più recente dispone di una regola di creazione.")
   @RBComment("Error message in case when Build rule is added after the iteration we are trying to check out")
   @RBArgComment0("Display identity of WTPart")
   public static final String NONLATEST_CHECKOUT_NOT_ALLOWED_WHEN_LATEST_PART_HAS_BUILD_RULE = "208";

   @RBEntry("Non è consentito sottoporre a Check-Out l'iterazione meno recente di {0} a meno di non sottoporre contemporaneamente a Check-Out {1}.")
   @RBComment("Error message when nonlatest part with build rule and history is checked out alone")
   @RBArgComment0("Display identity of WTPart")
   @RBArgComment1("Display identity of EPMDocument")
   public static final String NONLATEST_CHECKOUT_OF_PART_ALONE_NOT_ALLOWED = "209";

   @RBEntry("Non è consentito sottoporre a Check-Out l'iterazione meno recente di {0}. L'iterazione meno recente è stata creata da un'origine differente o dispone di una regola di creazione differente rispetto all'iterazione più recente.")
   @RBComment("Error message when build rule and build history does not match")
   @RBArgComment0("Display identity of WTPart")
   public static final String NONLATEST_CHECKOUT_NOT_ALLOWED_WHEN_LATEST_PART_BUILD_BY_DIFF_DOCUMENT = "210";

   @RBEntry("Impossibile sottoporre la parte a Check-Out")
   public static final String CANNOT_CHECKOUT_PART_EXCEPTION = "211";

   @RBEntry("Il disegno {0} fa riferimento al modello {1} che non è nel workspace.")
   @RBComment("Error message to show that a given drawing is being checked in without a model in the workspace.")
   @RBArgComment0("name of the drawing")
   @RBArgComment1("name of the missing model.")
   public static final String CHECKIN_DRAWING_WITHOUT_MODEL = "212";

   @RBEntry("Eliminazione amministrativa del workspace")
   @RBComment("message to set Transaction Description field in Delete Workspace Notification mail")
   public static final String WORKSPACE_DELETED_MAIL_TRX_DESCRIPTION = "213";

   @RBEntry("Elimina workspace")
   @RBComment("message to set Event field in Delete Workspace Notification mail")
   public static final String WORKSPACE_DELETED_EVENT = "214";

   @RBEntry("Il Check-Out di {0} non può essere annullato senza annullare anche il Check-Out di {1} in quanto il l'iterazione sottoposta a Check-Out non è la più recente. Annullare il Check-Out di entrambi gli oggetti simultaneamente..")
   @RBComment("Error message for non latest, in case when Target is still checked out and trying to undo checked out the Source")
   @RBArgComment0("Display identity of EPMDocument")
   @RBArgComment1("Display identity of WTPart")
   public static final String NONLATEST_SOURCE_UNDOCHECKOUT_NOT_ALLOWED = "215";

   @RBEntry("Impossibile eseguire la mappatura implicita di {0}. Esistono più possibilità di mappatura.")
   @RBComment("Warning message for an Attribute name which cannot be unambiguously mapped")
   @RBArgComment0("Attribute name")
   public static final String AMBIGUOUS_ATTRIBUTE = "216";

   @RBEntry("L'oggetto è stato salvato come oggetto autonomo a partire da {0}.")
   @RBComment("Info message for an save as of only generic to make it standalone.")
   @RBArgComment0("Display identity of generic.")
   public static final String SAVE_FT_MEMBER_AS_STANDALONE = "217";

   @RBEntry("L'oggetto è stato aggiunto nella family table esistente {0} come nuova istanza.")
   @RBComment("Info message for an save as of instance into existing FT.")
   @RBArgComment0("Name of the family table.")
   public static final String UPDATING_FT_WITH_SAVE_AS_OBJECT = "218";

   @RBEntry("Impossibile eseguire una copia parziale di un'istanza di family table assieme a componenti non gestiti da family table. È stata richiesta la copia dell'istanza \"{1}\" assieme ai seguenti componenti non gestiti da family table:\n {0}")
   @RBComment("Error message when performing a partial ASM FT Save As (reusing generic)")
   @RBArgComment0("Identity of component EPMDocument")
   @RBArgComment1("Identity of Instance EPMDocument")
   public static final String CANNOT_SAVEAS_PARTIAL_FTASM_WITH_FTCOLUMN_OF_TYPE_EPMFAMILYTABLEMEMBER = "219";

   @RBEntry("Dati insufficienti per aggiungere l'annotazione struttura per l'elemento {1} con tipo di annotazione \"{0}\".")
   @RBComment("Error message when sufficient data is not available to add Structure Annotation.")
   @RBArgComment0(" Structure Annotation Type.")
   @RBArgComment1(" Identity of annotatable.")
   public static final String INSUFFICIENT_DATA_TO_CREATE_ANNOTATION = "220";

   @RBEntry("{0} non dispone di annotazioni.")
   @RBArgComment0("List of identity of annotatables without annotations")
   public static final String ANNOTATION_NOT_AVAILABLE_FOR_ABORT = "221";

   @RBEntry("{1} è già persistente. Non è possibile aggiungere il tipo di annotazione \"{0}\" ad interfacce annotable che sono già persistenti.")
   @RBArgComment0(" Structure Annotation Type")
   @RBArgComment1(" Identity of annotatable")
   public static final String NEWLY_CREATED_ANNOTATBLE_ALREADY_PERSISTED = "222";

   @RBEntry("La versione {0} dell'applicazione di creazione non consente più annotazioni struttura.")
   @RBArgComment0("Authoring application version ")
   public static final String MULTI_EDIT_NOT_ALLOWED = "225";

   @RBEntry("L'annotazione struttura di tipo \"{1}\" non è consentita per la versione {0} dell'applicazione di creazione.")
   @RBArgComment0("Authoring application version ")
   @RBArgComment1("Structure Annotation Type.")
   public static final String STRUCTURE_ANNOTATION_NOT_ALLOWED = "226";

   @RBEntry("Impossibile aggiungere le annotazioni negli elementi {0} poiché sono già bloccate da un'altra sessione.")
   @RBArgComment0(" Identity of annotatable")
   public static final String ANNOTATABLE_ALREADY_LOCKED = "227";

   @RBEntry("Impossibile assegnare la definizione tipo {0} a {1}. La definizione tipo non è un discendente di {1}.")
   @RBComment("Error message when type definition cannot be assigned as it does not belongs to the same root.")
   @RBArgComment0("Type definition display name")
   @RBArgComment1("Identity of Object")
   public static final String TYPE_DEF_NOT_CORRECT = "228";

   @RBEntry("Il tipo di annotazione struttura \"{1}\" non è consentito per versioni precedenti dell'applicazione di creazione {0}.")
   @RBArgComment0("Authoring application Type")
   @RBArgComment1("Structure Annotation Type.")
   public static final String STRUCTURE_ANNOTATION_NOT_SUPPORTED_FOR_AUTH_APP = "229";

   @RBEntry("Impossibile copiare le rappresentazioni. Il comando richiede la copia di tutte le rappresentazioni nel gruppo, ma le rappresentazioni {0} non sono copiate.")
   @RBArgComment0("additional representations required to be copied")
   public static final String CANNOT_COPY_ALL_IMAGES_IN_THE_SET_NEEDS_TO_BE_COPIED = "230";

   @RBEntry("Impossibile creare una nuova revisione delle rappresentazioni. Il comando richiede che tutte le rappresentazioni nel gruppo vengano sottoposte a revisione, ma le rappresentazioni {0} non sono revisionate.")
   @RBArgComment0("additional representations required to be revised")
   public static final String CANNOT_REVISE_ALL_IMAGES_IN_THE_SET_NEEDS_TO_BE_REVISED = "231";

   @RBEntry("È possibile che alcune istanze abbiano subito modifiche dopo l'unione nel generico di family table in quanto condividevano gruppi di colonne di family table differenti. La colonna {0} è stata rimossa.")
   @RBArgComment0("Column Name")
   public static final String COLUMN_DOES_NOT_EXIST_IN_NEW_FT = "232";

   @RBEntry("Testo nota")
   @RBComment("Display name of attribute PTC_NOTE_TEXT")
   public static final String PTC_NOTE_TEXT = "233";

   @RBEntry("Impossibile aggiornare il workspace {0}. La family table con il generico {1} e quella con il generico {2} hanno lo stesso master {3}. ")
   @RBComment("Error message for update of a Workspace causing a more then one family table with the same master to exist in the workspace")
   @RBArgComment0("Name of the EPMWorkspace being updated.")
   @RBArgComment1("Name of the EPMDocument representing the generic of the incompatible family.")
   public static final String MORE_THAN_ONE_FAMILY_TABLE_WITH_THE_SAME_MASTER = "235";

   @RBEntry("Impossibile aggiornare {0} con informazioni relative allo stato della cache associata sul computer locale. Nessuna cache associata a questo workspace.")
   @RBComment("Error message occurs when attempting to update a workspace with information about the state of its cache on the local machine when it does not have one.")
   @RBArgComment0("Identity of an EPMWorkspace")
   public static final String WORKSPACE_HAS_NO_CLIENT_CACHE = "236";

   @RBEntry("Un workspace registra solo le informazioni relative allo stato della cache locale di documenti CAD/dinamici.")
   @RBComment("Error message occurs when attempting to update a workspace with information about the local cache state of an object that is not a CAD document.")
   @RBArgComment0("Identity of an EPMWorkspace")
   public static final String CANNOT_SET_CLIENT_CACHE_STATE_FOR_NON_CADDOC = "237";

   @RBEntry("Conflitto di archiviazione")
   @RBComment("Non overridable conflict as a result of Archive")
   public static final String ARCHIVE_CONFLICT = "238";

   @RBEntry("Impossibile archiviare il documento CAD {0} in quanto è associato alla family table più recente.")
   @RBComment("Error message for Archiving CAD documents associated to the latest family table.")
   @RBArgComment0("Identity of the CAD document")
   public static final String CANNOT_ARCHIVE_DOC_ASSOCIATED_LATEST_FT = "239";

   @RBEntry("Impossibile sottoporre a Check-Out l'iterazione non aggiornata dell'immagine {0}.")
   @RBComment("Error message occurres when non-latest iteration of image is checked out.")
   @RBArgComment0("Identity of the image")
   public static final String CANNOT_CHECKOUT_NON_LATEST_IMAGE = "240";

   @RBEntry("Impossibile sottoporre a Check-Out l'immagine")
   @RBComment("Error message occurres when non-latest iteration of image is checked out.")
   public static final String CANNOT_CHECKOUT_IMAGE = "241";

   @RBEntry("Impossibile creare tipo soft nota. Tipo soft di documento dinamico {0} non trovato.")
   @RBComment("Error message occurs when attempting to create Note soft type and Dynamic document soft type is not found.")
   @RBArgComment0("Logicalid or ExtHid for Dynamic document retrived from prefrence settings.")
   public static final String ARBORTEXT_SOFTTYPE_MISSING = "242";

   @RBEntry("Impossibile ripristinare l'oggetto {0}. La versione è stata eliminata dal database.")
   public static final String FRAME_NO_VERSION = "250";

   @RBEntry("Impossibile ripristinare l'oggetto {0}. Il relativo oggetto master è stato eliminato dal database.")
   public static final String FRAME_NO_MASTER = "251";

   @RBEntry("Impossibile ripristinare il membro di family table {0}. La versione oggetto di family table è incompatibile.")
   public static final String FRAME_INCOMPATIBLE_DOCS = "252";

   @RBEntry("Il sistema ha annullato il Check-Out dell'oggetto in seguito all'applicazione di un frame precedente.")
   public static final String FRAME_UNDO_CHECKOUT = "253";

   @RBEntry("Impossibile eseguire una copia parziale di un'istanza di family table referenziata da componenti non gestiti da family table. È stata richiesta la copia dell'istanza \"{1}\", referenziata dai seguenti componenti non gestiti da family table:\n {0}")
   @RBComment("Error message when performing a partial ASM FT(containing non-family driven references) Save As (reusing generic)")
   @RBArgComment0("Identity of reference EPMDocument")
   @RBArgComment1("Identity of Instance EPMDocument")
   public static final String CANNOT_SAVEAS_PARTIAL_FTASM_WITH_FTCOLUMN_OF_TYPE_EPMFAMILYTABLEREFERENCE = "255";

   @RBEntry("File neutro")
   @RBComment("Tool tip for glyph of any CAD authored documents whose document types are IGES, STEP, VDA, ACIS, DXF, PARASOLID, ZIP or JT.")
   public static final String TOOLTIP_NEUTRAL_FILE = "260";

   @RBEntry("Generico assieme")
   public static final String TOOLTIP_CADASSEMBLYGENERIC = "261";

   @RBEntry("Istanza assieme")
   public static final String TOOLTIP_CADASSEMBLYINSTANCE = "262";

   @RBEntry("Generico parte CAD")
   public static final String TOOLTIP_CADCOMPONENTGENERIC = "263";

   @RBEntry("Istanza parte CAD")
   public static final String TOOLTIP_CADCOMPONENTINSTANCE = "264";

   @RBEntry("Impossibile copiare {0} perché non si dispone dell'accesso al relativo generico di livello superiore=\"{1}\".")
   public static final String CANNOT_COPY_FAMILY_INSUFFICIENT_ACCESS = "265";

   @RBEntry("Il contenuto con il nome file \"{0}\" esiste già. Impossibile aggiungere altro contenuto con lo stesso nome file.")
   public static final String DUPLICATE_CONTENT_FILE_NAME = "266";

   @RBEntry("L'oggetto {0} appartiene a {1}. Non sarà possibile apportare modifiche con l'applicazione corrente.")
   public static final String OPERATION_VETOED_AND_NO_APPLICATION_SET = "267";

   @RBEntry("Impossibile aggiungere le annotazioni.")
   public static final String UNABLE_TO_ADD_ANNOTATIONS = "268";

   @RBEntry("Impossibile caricare nel server il parametro \"{0}\". La quantità di misura non è conforme all'attributo di Windchill \"{1}\"")
   public static final String QUANTITY_TYPE_MISMATCH = "269";

   @RBEntry("Un documento CAD con nome file {0} è già membro di un'altra family table. Impossibile creare una nuova istanza di family table nel workspace con questo nome.")
   @RBComment("Error message when the target object of save-as operation on an FT object is another FT object.")
   @RBArgComment0("Display identity of the target FT object.")
   public static final String FTOBJECT_CANNOT_BE_SAVEAS_TARGET = "270";

   @RBEntry("Impossibile impostare il generico {0} come autonomo poiché dispone di una o più istanze. È necessario impostare anche le istanze come autonome.")
   @RBComment("Error message when a generic is made standalone without all its instances.")
   @RBArgComment0("Identity of generic EPMDocument which cannot be made standalone.")
   public static final String CANNOT_MAKE_GENERIC_STANDALONE_WITHOUT_INSTANCES = "271";

   @RBEntry("È necessario impostare come autonomi gli oggetti di family table che seguono prima di utilizzarli come destinazione di un'operazione di salvataggio con nome: {0}.")
   @RBComment("Error message when the copy is a persistent FT object during pre_copy event.")
   @RBArgComment0("List of persistent FT objects.")
   public static final String COPY_CANNOT_BE_FTOBJECT_IN_PRECOPY = "272";

   @RBEntry("Impossibile trovare la definizione tipo di default per {0}.")
   @RBComment("Error message when the default type definition for a class is not found.")
   @RBArgComment0("Class name.")
   public static final String DEFAULT_TYPEDEF_NOT_FOUND = "273";

   @RBEntry("La definizione tipo {0} è sottoposta a Check-Out. Impossibile aggiungere nuovi attributi.")
   @RBComment("Error message when a type definition is in checkedout state.")
   @RBArgComment0("Type definition name.")
   public static final String TYPEDEF_CHECKEDOUT = "274";

   @RBEntry("Gli attributi per Creo Elements/Direct sono stati aggiunti correttamente.")
   @RBComment("Message when the attributes for Creo Elements/Direct are successfully added to soft types.")
   public static final String COCREATE_ATTRS_ADDED = "275";

   @RBEntry("Uno o più attributi di Creo Elements/Direct sono già presenti nei tipi soft. Aggiornamento dei tipi soft non eseguito.")
   @RBComment("Error message when the attributes for Creo Elements/Direct cannot be added to soft types.")
   public static final String CANNOT_ADD_COCREATE_ATTRS = "276";

   @RBEntry("{0} non è un generico di livello principale.")
   @RBArgComment0("Identity of EPMDocument")
   public static final String DOC_IS_NOT_TOP_GENERIC = "277";

   @RBEntry("Il documento CAD/dinamico non è né nuovo né una copia in modifica.")
   public static final String DOC_IS_NOT_MODIFIABLE = "278";

   @RBEntry("Due o più regole hanno lo stesso ordine. Impossibile continuare l'elaborazione.")
   public static final String DUPLICATE_RULE_ORDER = "280";

   @RBEntry("Non è stata impostata una regola padre per una o più regole dei risultati. Impossibile continuare l'elaborazione.")
   public static final String PARENTRULE_NOT_SET = "281";

   @RBEntry("Nome file rinominato nel commonspace. Eseguire la sincronizzazione del workspace.")
   public static final String FILE_NAME_HAS_BEEN_RENAMED_IN_COMMONSPACE = "282";

   @RBEntry("Impossibile modificare il nome CAD di uno o più documenti CAD in quanto i relativi nuovi nomi CAD non sono univoci. I nomi CAD duplicati sono {0}.")
   @RBComment("Error message to show when user tries to rename CADDocument/s and CADDocument/s with same name already exist in databse.")
   @RBArgComment0("List of duplicate CADNames of the document")
   public static final String CANNOT_RENAME_DOCUMENTS = "283";

   @RBEntry("Impossibile eseguire il salvataggio con nome di \"{0}\" in \"{1}\" perché le rispettive categorie secondarie di documento sono differenti.")
   @RBArgComment0("original document identity")
   @RBArgComment1("save-as to document identity")
   public static final String SAVEAS_DOCSUBTYPE_MISMATCH = "284";

   @RBEntry("Impossibile eseguire il salvataggio con nome di \"{0}\" in \"{1}\" perché le categorie di documento sono differenti.")
   @RBArgComment0("original document identity")
   @RBArgComment1("save-as to document identity")
   public static final String SAVEAS_DOCTYPE_MISMATCH = "285";

   @RBEntry("Impossibile eseguire il salvataggio con nome di \"{0}\" in \"{1}\" perché le rispettive applicazioni di creazione sono differenti.")
   @RBArgComment0("original document identity")
   @RBArgComment1("save-as to document identity")
   public static final String SAVEAS_AUTHTYPE_MISMATCH = "286";

   @RBEntry("L'utente non dispone di accesso in lettura per la parte \"{0}\".")
   @RBArgComment0("part identity")
   public static final String NO_READ_ACCESS_TO_PART = "287";

   @RBEntry("L'utente non dispone di accesso per la modifica per la parte \"{0}\".")
   @RBArgComment0("part identity")
   public static final String NO_MODIFY_ACCESS_TO_PART = "288";

   @RBEntry("È possibile che il file {0} sia stato rinominato nel commonspace. Per risolvere il problema, sincronizzare il workspace.")
   public static final String FILE_MAY_HAVE_BEEN_RENAMED_IN_COMMONSPACE = "290";

   @RBEntry("Le parti associate ai documenti CAD con un'associazione immagine sono state ignorate dalla build.")
   public static final String IMAGE_ASSOCIATION_NOT_BUILT = "291";

   @RBEntry("Le parti già create da documenti CAD sono state ignorate dalla build.")
   public static final String ALREADY_BUILT_ASSOCIATION_NOT_BUILT = "292";

   @RBEntry("Impossibile memorizzare o modificare un link componenti documento EPM temporaneo creato durante l'elaborazione dell'annotazione.")
   @RBComment("This message is for Windchill developer.  Fabricated EPMMemberLink is created for an annotation.  It cannot be saved to the database.")
   public static final String CANNOT_SAVE_FABRICATED_MEMBER_LINKS = "293";

   @RBEntry("{0} non è associato ad alcuna rappresentazione alternativa o la relativa rappresentazione alternativa associata non è associata ad alcuna regola risultante.")
   public static final String INVALID_ESR_CONTAINER = "294";

   @RBEntry("{0} non sono associati ad alcuna rappresentazione alternativa o la relativa rappresentazione alternativa associata non è associata ad alcuna regola risultante.")
   public static final String INVALID_ESR_CONTAINER_PLURAL = "295";

   @RBEntry("Impossibile eseguire il salvataggio con nome di {0} in {1} perché il numero dei membri di family table è differente.")
   @RBArgComment0("original document identity")
   @RBArgComment1("save-as to document identity")
   public static final String SAVEAS_NO_OF_INSTANCES_DO_NOT_MATCH= "296";

   @RBEntry("Impossibile eseguire il salvataggio con nome di \"{0}\" in \"{1}\". La copia parziale di family table non è consentita per il salvataggio con nome di documenti CAD di family table in documenti CAD di family table esistenti.")
   @RBArgComment0("original document identity")
   @RBArgComment1("save-as to document identity")
   public static final String SAVEAS_OF_PARTIAL_FT_NOT_ALLOWED= "297";

   @RBEntry("Impossibile eseguire il salvataggio con nome di {0} in {1}. {2} è un generico intermedio e non è supportato per le operazioni di salvataggio con nome in un file esistente.")
   @RBArgComment0("original document identity")
   @RBArgComment1("save-as to document identity")
   @RBArgComment2("intermediate generic document identity")
   public static final String SAVEAS_OF_NESTED_FT_NOT_ALLOWED= "298";

   @RBEntry("Impossibile eseguire il salvataggio con nome di {0} in {1}. Tutti i membri della family table {2} non vengono copiati nei membri di un'unica family table ")
   @RBArgComment0("original document identity")
   @RBArgComment1("save-as to document identity")
   @RBArgComment2("original family table name")
   public static final String SAVEAS_SOURCE_FT_TO_MULTIPLE= "299";

   @RBEntry("Trovato il documento CAD {0} che non è un modello.")
   @RBArgComment0("found document identity")
   public static final String EXISTING_OBJ_NOT_TEMPLATE= "300";

   @RBEntry("Errore interno: l'annotazione struttura di tipo \"{0}\" non è supportata.")
   @RBArgComment0("Structure Annotation Type.")
   public static final String STRUCTURE_ANNOTATION_NOT_SUPPORTED = "301";

   @RBEntry("Errore interno: l'annotazione struttura di tipo \"{0}\" non è supportata per il link membro persistente non soppresso.")
   @RBArgComment0("Structure Annotation Type.")
   public static final String STRUCTURE_ANNOTATION_NOT_SUPPORTED_UNSUPPRESSED_PERSISTENT_LINK = "302";

   @RBEntry("Errore interno: l'annotazione struttura di tipo \"{0}\" non è supportata per il link membro non persistente.")
   @RBArgComment0("Structure Annotation Type.")
   public static final String STRUCTURE_ANNOTATION_NOT_SUPPORTED_NON_PERSISTENT_LINK = "303";

   @RBEntry("Errore interno: l'annotazione struttura di tipo \"{0}\" non è supportata per il link membro persistente soppresso.")
   @RBArgComment0("Structure Annotation Type.")
   public static final String STRUCTURE_ANNOTATION_NOT_SUPPORTED_SUPPRESSED_PERSISTENT_LINK = "304";

   @RBEntry("{0} non può essere un tipo soft figlio di {1}.")
   @RBComment("Error message for hierarchy of soft type")
   @RBArgComment0("Identity of the child soft type")
   @RBArgComment1("Identity of the parent soft type")
   public static final String CANNOT_BE_CHILD_OF_SOFTTYPE = "305";

   @RBEntry("Impossibile eseguire il salvataggio con nome di \"{0}\" in quanto vi sono delle operazioni in sospeso per i componenti che non sono state applicate nello strumento CAD.")
   @RBComment("Error message to show when user tries to Save As of assembly which has annotated member link(s).")
   @RBArgComment0("Display identity of an Assembly")
   public static final String SAVEAS_OF_ASSEMBLY_WITH_ANNOTATED_MEMBER_LINK_NOT_ALLOWED = "306";

   @RBEntry("Impossibile sostituire il contenuto del documento \"{0}\" con il file nel workspace perché per il contenuto o l'assieme padre vi sono delle operazioni in sospeso per i componenti che non sono state applicate nello strumento CAD.")
   @RBComment("Error message to show when user tries to SaveAsToExisting of assembly and either parent or component of that assembly has annotated member link(s).")
   @RBArgComment0("Display identity of an Assembly")
   public static final String REPLACE_ASSEMBLY_WITH_ANNOTATED_MEMBER_LINK_NOT_ALLOWED = "307";

   @RBEntry("Impossibile aggiornare l'assieme per utilizzare il nuovo figlio. Per il link componenti del figlio \"{0}\" vi è un'operazione in sospeso per il componente che deve essere applicata nello strumento CAD prima della modifica in Windchill.")
   @RBComment("Error message for update parent in case of annotated member link")
   @RBArgComment0("FILE NAME OF ORIGINAL ITEM")
   public static final String CANNOT_UPDATE_PARENT_WITH_ANNOTATED_MEMBER_LINK = "308";

   @RBEntry("Impossibile eliminare {0} poiché include l'associazione a oggetti tramite il link EPMBuildHistory.")
   @RBArgComment0("persistable which could not be deleted/purged.")
   public static final String CANNOT_DELETE_PERSISTABLE_WITH_EXISTING_BUILDHISTORYLINK = "309";

   @RBEntry("Impossibile eseguire l'aggiornamento perché non solo selezionati tutti i membri della family table per l'aggiornamento del padre.")
   public static final String NOT_ALL_FTMEMBERS_IN_UPDATE = "310";

   @RBEntry("Non tutti i membri della family table sono disponibili con la stessa richiesta di aggiornamento.")
   public static final String NOT_SAME_UPDATE_IN_FT = "311";

   @RBEntry("Errore interno: l'annotazione struttura di tipo \"{0}\" non è supportata per il link membro persistente annotato.")
   @RBArgComment0("Structure Annotation Type.")
   public static final String STRUCTURE_ANNOTATION_NOT_SUPPORTED_ANNOTATED_PERSISTENT_LINK = "312";

   @RBEntry("Impossibile eseguire il salvataggio con nome di \"{0}\" in \"{1}\" perché lo stato di family table non corrisponde.")
   @RBArgComment0("original document identity")
   @RBArgComment1("save-as to document identity")
   public static final String FAMILYTABLESTATUS_DOES_NOT_MATCH = "313";

   @RBEntry("Impossibile eseguire il salvataggio con nome di \"{0}\" in \"{1}\". Le strutture della variante di origine e di destinazione non corrispondono.")
   @RBArgComment0("original document identity")
   @RBArgComment1("save-as to document identity")
   public static final String SOURCE_TARGET_VARIANT_STRUCTURE_DOES_NOT_MATCH = "314";

   @RBEntry("Annotazione prevista non presente per l'elemento annotabile \"{0}\".")
   public static final String EXPECTED_ANNOTATION_NOT_PRESENT_ON_ANNOTATABLE = "315";

   @RBEntry("Modifica annotazione struttura non valida per \"{0}\". L'annotazione struttura di tipo 'aggiungi posizionato' può essere sostituita solo con un'annotazione di tipo 'aggiungi collocato'.")
   public static final String INVALID_CHANGE_STRUCTURE_ANNOTATION_OPERATION = "316";

   @RBEntry("Impossibile modificare l'annotazione.")
   public static final String UNABLE_TO_CHANGE_ANNOTATIONS = "317";

   @RBEntry("Impossibile creare o aggiornare il contesto di progettazione poiché creerà regole per i link componenti modificati Windchill. Prima di continuare, è necessario rigenerare i seguenti assiemi modificati nell'applicazione di creazione: {0}")
   @RBComment("Error message during creation of new Design Context if an annotated member link is present.")
   @RBArgComment0("List of assemblies with annotated member links")
   public static final String CANNOT_CREATE_OR_UPDATE_CAR_WITH_ANNOTATED_MEMBER_LINK = "318";

   @RBEntry("Impossibile modificare il contesto di progettazione con i link componenti modificati Windchill. Prima di continuare, è necessario rigenerare i seguenti assiemi modificati nell'applicazione di creazione: {0}")
   @RBComment("Error message during edit of a Design Context if an annotated member link is present.")
   @RBArgComment0("List of assemblies with annotated member links")
   public static final String CANNOT_EDIT_CAR_WITH_ANNOTATED_MEMBER_LINK = "319";

   @RBEntry("Impossibile salvare il contesto di progettazione poiché include regole per i link componenti modificati Windchill.")
   @RBComment("Error message during save  of Design Context if an annotated member link is present.")
   public static final String CANNOT_SAVE_CAR_WITH_ANNOTATED_MEMBER_LINK = "320";

   @RBEntry("Impossibile importare \"{0}\" poiché il nome del componente deve essere univoco nelle relative relazioni tra modelli geometrici. I nomi componente duplicati sono {1}.")
   @RBComment("PLM Error message - a EPMDocument cannot be imported because the component names on EPMMemberLink (or uses links) are not unique.")
   @RBArgComment0("Identity of EPMDocument")
   @RBArgComment1("List of component names, some are duplicates")
   public static final String PLM_DUPLICATE_ML_NAMES = "322";

   @RBEntry("Impossibile importare \"{0}\" poiché il nome del componente deve essere univoco nelle relative relazioni tra modelli geometrici. Il nome componente duplicato è {1}.")
   @RBComment("PLM Error message - a EPMDocument cannot be imported because the component names on EPMMemberLink (or uses links) are not unique.")
   @RBArgComment0("Identity of EPMDocument")
   @RBArgComment1("Duplicate component name")
   public static final String PLM_DUPLICATE_ML_NAME = "323";

   @RBEntry("Impossibile recuperare il tipo di radice della definizione tipo \"{0}\".")
   @RBComment("Failed to retrieve the root type of Type Definition.")
   @RBArgComment0("Default Type Definition Name")
   public static final String ROOT_TYPE_DEFINITION_RETRIEVAL_FAILED = "324";

   @RBEntry("Definizione della proprietà \"{0}\" non ancora specificata in \"{1}\".")
   @RBComment("Property Definition not yet defined.")
   @RBArgComment0("Identity of Property Definition")
   @RBArgComment1("Property Holder Classname")
   public static final String PROPERTY_DEFINITION_NOT_DEFINED = "325";

   @RBEntry("Impossibile aggiungere immagini al progetto. È necessario aggiungervi anche le immagini correlate \"{0}\".")
   @RBArgComment0("additional images required to be checked out to project")
   public static final String CANNOT_ADD_IMAGES_TO_PROJECT = "326";

   @RBEntry("Impossibile effettuare il Check-In delle immagini. È necessario effettuare il Check-In anche delle immagini correlate \"{0}\".")
   @RBArgComment0("additional images required to be checked in")
   public static final String  CANNOT_CHECKIN_IMAGES = "327";

   @RBEntry("È necessario sostituire i documenti CAD immagine con nome file \"{0}\". Non è possibile creare o sostituire il contenuto dei documenti CAD immagine \"{1}\".")
   @RBComment("Error message when all images of set are not being replaced at the same time.")
   @RBArgComment0("List of Identity of additional images which need to be replaced.")
   @RBArgComment1("List of identity of images being replaced.")
   public static final String ADDITIONAL_IMAGES_NEED_TO_BE_REPLACED = "328";

   @RBEntry("Un documento CAD di origine con il nome file \"{0}\" esiste già nel namespace del contesto \"{1}\". Impossibile creare o sostituire il contenuto del documento CAD, in quanto documento CAD di origine.")
   @RBComment("Error message when all images of set are not being replaced at the same time.")
   @RBArgComment0("Identity of source being replaced.")
   @RBArgComment1("Name of the context in which the source name exists.")
   public static final String SOURCE_CANNOT_TO_BE_REPLACED = "329";

   @RBEntry("L'azione corrente ha come risultato che il workspace \"{0}\" supera il limite di {1} impostato per il numero di oggetti dall'amministratore. L'azione non può essere completata se non si riduce prima il numero di oggetti.")
   @RBComment("Error message when workspace size limit is exceeded as a result of an action.")
   @RBArgComment0("Workspace name.")
   @RBArgComment1("Limit on the size of workspace.")
   public static final String TOO_LARGE_WORKSPACE = "330";

   @RBEntry("Impossibile salvare i seguenti contesti di progettazione senza la regola dei risultati di default: {0}")
   @RBComment("Error message during create/edit of a Design Context if the default result rule is not present.")
   @RBArgComment0("List of Design Context without the default result rule.")
   public static final String CANNOT_SAVE_CAR_WITHOUT_DEFAULT_RESULT_RULE = "331";

   @RBEntry("I documenti CAD di origine con nome file \"{0}\" devono essere sottoposti a Check-In.")
   @RBComment("Error message when all the sources are not checked in during creation of new rules and history objects.")
   @RBArgComment0("List of Identity of Source CAD documents which need to be checked in for creation of new rules and history objects.")
   public static final String SOURCE_NEEDS_TO_BE_CHECKED_IN = "332";

   @RBEntry("I documenti CAD immagine con nome file \"{0}\" devono essere sottoposti a Check-Out o essere nuovi.")
   @RBComment("Error message when all the images are not checked out or new during creation of new rules and history objects.")
   @RBArgComment0("List of Identity of Image CAD documents which need to be checked out or new for creation of new rules and history objects.")
   public static final String IMAGES_NEEDS_TO_BE_CHECKED_OUT_OR_NEW = "333";

   @RBEntry("Impossibile creare nuove regole e oggetti di cronologia per i documenti CAD immagine con nome file \"{0}\" poiché dispongono già di regola/cronologia.")
   @RBComment("Error message when an image being used for creation of new rules and history objects has already existing rule/history.")
   @RBArgComment0("List of identity of images for which new links and rules cannot be created.")
   public static final String CANNOT_CREATE_RULES_AND_LINKS_FOR_IMAGES = "334";

   @RBEntry("Impossibile creare/aggiornare il contesto di progettazione e il relativo assieme nel seguente formato, \"{0}\". Alcuni moduli CAD non sono presenti in questo formato CAD: \"{1}\"")
   @RBArgComment0("List of identity of document for which no image authored in given authoring application found")
   public static final String CONTINUE_TO_CREATE_WITHOUT_MISSING_CAD_MODULES = "335";

   @RBEntry("Impossibile creare/aggiornare il contesto di progettazione e il relativo assieme nel seguente formato, \"{0}\". Alcuni moduli CAD non sono presenti in questo formato CAD: \"{1}\"")
   @RBArgComment0("Image Authoring application.")
   @RBArgComment1("List of identity of document for which no image authored in given authoring application found")
   public static final String UNABLE_TO_CREATE_WITHOUT_MISSING_CAD_MODULES = "336";

   @RBEntry("Impossibile creare la struttura CAD. I documenti CAD immagine \"{0}\" devono essere nuovi o sottoposti a Check-Out nel workspace.")
   @RBComment("Error message occurs when non-latest iteration of image is checked out.")
   @RBArgComment0("List of identity of image CAD documents")
   public static final String IMAGE_SHOULD_BE_CHECKEDOUT_OR_NEW_IN_WORKSPACE = "337";

   @RBEntry("Impossibile creare la struttura CAD. I documenti CAD immagine \"{0}\" non sono le iterazioni più recenti.")
   @RBComment("Error message occurs when non-latest iteration of image is used during build of CAD Structure.")
   @RBArgComment0("List of identity of image CAD documents")
   public static final String NON_LATEST_IMAGE = "338";

   @RBEntry("Impossibile creare la struttura CAD. I documenti CAD immagine \"{0}\" appartengono a una family table.")
   @RBComment("Error message occurs when image CAD document used for build of CAD structure belongs to a family table.")
   @RBArgComment0("List of identity of image CAD documents")
   public static final String IMAGE_PART_OF_FAMILY_TABLE = "339";

   @RBEntry("Impossibile creare la struttura CAD. L'utente non dispone dell'accesso per la modifica di documenti CAD immagine \"{0}\".")
   @RBComment("Error message occurs when user does not have modify access to image CAD documents.")
   @RBArgComment0("List of identity of image CAD documents")
   public static final String NO_MODIFY_PERMISSION_ON_IMAGE = "340";

   @RBEntry("Impossibile creare la struttura CAD. I documenti CAD immagine \"{0}\" non si trovano nello stesso contesto del workspace \"{1}\".")
   @RBComment("Error message occurs when image CAD documents are in a different context as compared to the workspace.")
   @RBArgComment0("List of identity of image CAD documents")
   @RBArgComment1("Identity of workspace in which objects are build")
   public static final String DIFFERENT_CONTEXT_FOR_IMAGE_AND_WORKSPACE = "341";

   @RBEntry("Impossibile creare la struttura CAD. I documenti CAD di origine \"{0}\" devono essere sottoposti a Check-In.")
   @RBComment("Error message occurs when non-latest iteration of source is checked out.")
   @RBArgComment0("List of identity of source CAD documents")
   public static final String SOURCE_NEED_TO_BE_CHECKED_IN = "342";

   @RBEntry("Impossibile creare la struttura CAD. I documenti CAD di origine \"{0}\" devono essere assiemi creati in Windchill.")
   @RBComment("Error message occurs when source CAD document is not authored by Windchill.")
   @RBArgComment0("List of identity of source CAD documents")
   public static final String SOURCE_NEED_TO_BE_WINDCHILL_AUTHORED = "343";

   @RBEntry("Impossibile creare la struttura CAD. I documenti CAD di origine \"{0}\" appartengono a una family table.")
   @RBComment("Error message occurs when Source CAD document used for build of CAD structure belongs to a family table.")
   @RBArgComment0("List of identity of Source CAD documents")
   public static final String SOURCE_PART_OF_FAMILY_TABLE = "344";

   @RBEntry("Impossibile creare la struttura CAD. I documenti CAD di origine \"{0}\" non si trovano nello stesso contesto del workspace \"{1}\".")
   @RBComment("Error message occurs when Source CAD documents are in a different context as compared to the workspace.")
   @RBArgComment0("List of identity of Source CAD documents")
   @RBArgComment1("Identity of workspace in which objects are build")
   public static final String DIFFERENT_CONTEXT_FOR_SOURCE_AND_WORKSPACE = "345";

   @RBEntry("Impossibile creare la struttura CAD. I documenti CAD immagine \"{0}\" sono già stati creati da un'iterazione dell'origine più recente.")
   @RBComment("Error message occurs when Images are already built from lateration of source and hence cannot be built from earlier iteration of source.")
   @RBArgComment0("List of identity of Image CAD documents")
   public static final String IMAGE_ALREADY_BUILT_FROM_LATER_ITERATION_OF_SOURCE = "346";

   @RBEntry("Impossibile creare la struttura CAD. I documenti CAD immagine \"{0}\" non sono associati al documento di origine specificato.")
   @RBComment("Error message occurs when Images do not have EPMDerivedRepRule with corresponding input source document.")
   @RBArgComment0("List of identity of Image CAD documents")
   public static final String NO_EPMDERIVEDREPRULE_BWTWEEN_SOURCE_AND_IMAGE = "347";

   @RBEntry("Impossibile aggiornare il posizionamento del componente  \"{0}\" nell'assieme \"{1}\". Il componente è vincolato nel file CAD.")
   @RBComment("Error message occurs when placement of component in assembly cannot be updated since component is constrained in CAD file.")
   @RBArgComment0("List of child file names")
   public static final String CONTINUE_WITHOUT_UPDATING_PLACEMENT = "348";

   @RBEntry("Impossibile copiare l'istanza=\"{0}\" perché non si dispone dell'accesso al relativo generico immediato=\"{1}\".")
   @RBArgComment0("original document identity")
   @RBArgComment1("Immediate Generic identity")
   public static final String CANNOT_COPY_INSTANCE_INSUFFICIENT_ACCESS = "349";

   @RBEntry("Impossibile copiare l'istanza=\"{0}\" perché non si dispone dell'accesso al relativo generico immediato.")
   @RBArgComment0("original document identity")
   public static final String CANNOT_COPY_INSTANCE_IMMEDIATEGENERIC_NOT_AVAILABLE = "350";

   @RBEntry("La modifica della quantità per il link membro del documento CAD di origine \"{0}\" è stata ignorata durante l'aggiornamento del link membro del documento CAD immagine \"{1}\" quando è stata creata la struttura CAD.")
   @RBComment("Warning message when quantity change between Source and Image CAD document is ignored during subsequent build of CAD document structure.")
   @RBArgComment0("Identity of Source CAD document")
   @RBArgComment1("Identity of Image CAD document")
   public static final String QUANTITY_CHANGE_IGNORED = "351";

   @RBEntry(" Impossibile modificare in Windchill le rappresentazioni semplificate esterne con sostituzioni per inviluppo dinamico. Aprire la rappresentazione semplificata esterna in Creo per eseguire l'aggiornamento")
   @RBArgComment0("Error message when user tries to edit CAR having Substitute by envelope rule")
   public static final String CANNOT_EDIT_ESR_HAVING_SUBSTITUTE_BY_ENVELOPE_RULE = "352";

   @RBEntry("Impossibile aggiornare l'assieme, \"{0}\". I seguenti figli non sono più compatibili con i documenti CAD di origine: \"{1}\". Se è stata creata una nuova immagine del documento CAD per l'origine, creare un nuovo contesto di progettazione immagine anziché eseguire l'aggiornamento.")
   @RBComment("Error message when user tries to update image structure where childs are not compatible with correspondign source.")
   @RBArgComment0("Identity of assembly CAD document")
   @RBArgComment1("Identity of child CAD documents incompatible with source")
   public static final String CANNOT_UPDATE_IMAGE_INCOMPATIBLE_CHILDS = "353";

   @RBEntry("Impossibile aggiornare l'assieme.")
   @RBComment("Error message when update of image assembly fails.")
   public static final String CANNOT_UPDATE_ASSEMBLY = "354";

   @RBEntry("L'annotazione struttura non è supportata per un EPMMemberlink persistente annotato.")
   public static final String STRUCTURE_ANNOTATION_NOT_SUPPORTED_PERSISTENT_ANNOTATED_LINK = "355";

   @RBEntry("Impossibile trovare il seguente caso d'impiego del percorso del contesto di progettazione {1} nella struttura del documento CAD immagine: {0}. Nessuna regola viene creata nel contesto di progettazione immagine per questo percorso.")
   @RBArgComment0("Path of rule")
   @RBArgComment1("Target ESR")
   public static final String SOURCE_PATH_NOT_FOUND_UPDATING_IMAGE_ESR = "356";

   @RBEntry("Impossibile importare EPMMemberlink come UniqueNDId previsto = {0} e UniqueNDId effettivo = {1}.")
   @RBArgComment0("Expected UniqueNDId")
   @RBArgComment1("Actual UniqueNDId")
   public static final String UNIQUENDID_DIFFERENT_WHILE_IMPORT = "357";

   @RBEntry("È necessario eliminare i documenti CAD immagine con nome file \"{0}\". Non è possibile eliminare i documenti CAD immagine \"{1}\".")
   @RBComment("Error message when all images of set are not being deleted at the same time.")
   @RBArgComment0("List of Identity of additional images which need to be deleted.")
   @RBArgComment1("List of identity of images being deleted.")
   public static final String ADDITIONAL_IMAGES_NEED_TO_BE_DELETED = "358";

   @RBEntry("Aggiornamento del modello padre \"{0}\" non consentito. I documenti CAD configurabili non possono essere figli di assiemi standard. Aggiornamento del padre annullato.")
   @RBComment("Update of parent model \"{0}\" is not allowed. Configurable CAD documents cannot be children of standard assemblies. Update of parent is cancelled.")
   public static final String PARENT_DOC_NOT_CONFIGURABLE_CHILD_CONFIGURABLE = "359";

   @RBEntry("Impossibile aggiungere i seguenti documenti CAD di cui è stata creata una nuova revisione a più baseline archiviate (come proprietario): {0}")
   @RBComment("Error message for adding the same EPMDocument as Owner to multiple as-stored baselines during Revise operation")
   @RBArgComment0("List of Identity of EPMDocument.")
   public static final String CANNOT_CREATE_AS_STORED_BASELINE_AT_REVISE = "360";

   @RBEntry("È necessario assegnare un valore a IBAHolderReference e AttributeDefinitionReference per la chiamata di EPMParameterUnitInfo.newEPMParameterUnitInfo")
   @RBComment("Error message for EPMParameterUnitInfo.newEPMParameterUnitInfo when IBAHolderReference OR AttributeDefinitionReference is null")
   public static final String NULL_VALUE_PROVIDED_FOR_EPMPARAMETERUNITINFO = "361";

   @RBEntry("Impossibile eliminare la versione di \"{0}\". Sono presenti iterazioni, associate a questa versione tramite il link EPMBuildHistory, che hanno modifiche pubblicate nella struttura documento associata.")
   @RBArgComment0("persistable which could not be deleted.")
   public static final String CANNOT_DELETE_VERSION_WITH_EXISTING_REVERSE_BUILDHISTORYLINK = "362";

   @RBEntry("Impossibile eliminare i master dell'elemento modello \"{0}\". È necessario dissociare la regola di creazione per gli elementi modello.")
   @RBArgComment0("mode item master which could not be deleted.")
   public static final String CANNOT_DELETE_MODEL_ITEM_MASTER = "363";

   @RBEntry("Convalida family table non riuscita. Contattare PTC a meno che il problema non si sia verificato durante l'importazione di un package. Gli errori durante l'importazione di un package possono essere dovuti alla mancata disponibilità di un generico compatibile al momento dell'importazione.")
   @RBArgComment0("Family table validations failed. Please contact PTC unless this occurred during a package import. Failures on package import may be due to a compatible generic not being available at time of import.")
   public static final String FAMILY_TABLE_VALIDATIONS_FAILED = "364";

   @RBEntry("La parte \"{0}\" non è aggiornata rispetto al documento CAD correlato \"{1}\". Le parti devono essere create prima di reinviare le modifiche nel sistema PDM.")
   @RBArgComment0("Conflict when part being sent to PDM is out of date with its related CAD document.")
   public static final String CANNOT_SEND_PART_TO_PDM = "365";

   @RBEntry("Impossibile salvare la regola senza la rappresentazione alternativa")
   public static final String CANNOT_SAVE_ALTERNATREPRULE_WITHOUT_ALTERNATEREPSENTATION = "366";

   @RBEntry("Parte CAD")
   public static final String TOOLTIP_CADCOMPONENT = "367";

   @RBEntry("I documenti immagine non sono aggiornati. Impossibile copiarli con i seguenti documenti di origine: \"{0}\". Escludere i documenti immagine dall'azione Salva con nome o aggiornare le immagini e riprovare.")
   public static final String OUT_OF_DATE_IMAGE_BEING_COPIED = "368";

   @RBEntry("Parte Centro contenuti")
   public static final String TOOLTIP_LIBRARY_PART = "369";

   @RBEntry("Impossibile convertire in oggetto condiviso una family table parziale. È necessario che tutti i membri della family table vengano inclusi nell'operazione di conversione in oggetto condiviso.")
   @RBComment("Convert to share operation must have all family table members included")
   public static final String PARTIAL_FAMILY_CONVERT_TO_SHARE_NOT_ALLOWED = "370";

   @RBEntry("Parte casella strumenti")
   public static final String TOOLTIP_SOLIDWORKS_LIBRARY_PART = "371";

   @RBEntry("Uno o più documenti CAD con i nomi di file {0} esistono già o sono nomi di dipendenti di un oggetto presente nel workspace. Impossibile cambiare i nomi di file dei documenti in {0}.")
   @RBComment("Error message to show that CAD documents could not be renamed on workspace.")
   @RBArgComment0("List of duplicate filenames of the documents")
   public static final String CANNOT_RENAME_DOCUMENTS_IN_WORKSPACE = "372";

   @RBEntry("Impossibile aggiungere al workspace uno o più documenti CAD contraddistinti dai numeri \"{0}\", perché condividono gli stessi nomi di file \"{1}\" dei documenti CAD \"{2}\" o sono nomi di dipendenti di un oggetto già presente nel workspace \"{3}\".")
   @RBComment("Error message to show that CAD documents could not be added to workspace.")
   @RBArgComment0("List of duplicate filenames of the documents")
   public static final String CANNOT_ADD_DOCUMENTS_IN_WORKSPACE_WITH_DUPLICATE_FILENAME = "373";

   @RBEntry("I documenti CAD selezionati \"{0}\" non presentano baseline archiviate.")
   @RBArgComment0("List of CAD Documents")
   public static final String NO_AS_STORED_CONFIGS_FOR_SEEDS = "374";

   @RBEntry("È stato assegnato un numero provvisorio a \"{0}\" per consentire il caricamento del workspace. È necessario riassegnare un numero definitivo tramite l'azione Rinomina prima di effettuare il Check-In")
   public static final String CANNOT_CHECKIN_TEMP_NUMBER = "375";

   @RBEntry("È stato assegnato un numero provvisorio al documento \"{0}\" nel workspace a causa di un conflitto di numerazione. È necessario rinumerare il documento per potere effettuare il Check-In. ")
   public static final String DUPLICATE_NUMBER_ERROR = "376";

   @RBEntry("Impossibile caricare oggetti condivisi modificati localmente.")
   public static final String CANNOT_PRIVATE_CHECKOUT_SHARED_OBJECTS = "377";

   @RBEntry("Impossibile caricare rappresentazioni modificate localmente.")
   public static final String CANNOT_PRIVATE_CHECKOUT_IMAGES = "378";

   @RBEntry("Impossibile sovrascrivere una modifica già caricata per le rappresentazioni.")
   public static final String CANNOT_CONVERT_REGULAR_CHECKOUT_TO_PRIVATE_CHECKOUT_FOR_IMAGES = "379";
   @RBEntry("Progetto AutoCAD Electrical")
   public static final String TOOLTIP_ACAD_PROJECT = "380";

   @RBEntry("File di configurazione AutoCAD Electrical")
   public static final String TOOLTIP_ACAD_CONFIG_FILE = "381";

   @RBEntry("Un documento con il nome file \"{0}\" esiste già nel workspace \"{1}\". È necessario rinominare uno dei documenti in conflitto per effettuare il Check-In.")
   @RBComment("CAD document has a filename constraint violation which may need to be resolved prior to check-in.")
   public static final String DUPLICATE_FILENAME_CONFLICT_ON_USER_WS = "382";

   @RBEntry("Un documento con il nome file \"{0}\" esiste già nel workspace \"{1}\" dell'utente \"{2}\". È necessario rinominare uno dei documenti in conflitto per effettuare il Check-In.")
   @RBComment("CAD document has a filename constraint violation which may need to be resolved prior to check-in.")
   public static final String DUPLICATE_FILENAME_CONFLICT_ON_ANOTHER_USER_WS = "383";

   @RBEntry("Un documento con il nome file \"{0}\" esiste già nel workspace in una posizione con accesso limitato di cui non si dispongono i permessi. È necessario rinominare uno dei documenti in conflitto per effettuare il Check-In.")
   @RBComment("CAD document has a filename constraint violation which may need to be resolved prior to check-in.")
   public static final String DUPLICATE_FILENAME_CONFLICT_ON_RESTRICTED_WS_CONTEXT = "384";

   @RBEntry("Un documento con il nome file \"{0}\" esiste già in \"{1}\", cartella \"{2}\". È necessario rinominare uno di questi documenti per effettuare il Check-In.")
   @RBComment("CAD document has a filename constraint violation which may need to be resolved prior to check-in.")
   public static final String DUPLICATE_FILENAME_CONFLICT_ON_CS = "385";

   @RBEntry("Un documento con il nome file \"{0}\" esiste già in una posizione con accesso limitato di cui non si dispongono i permessi. È necessario rinominare uno dei documenti in conflitto per effettuare il Check-In.")
   @RBComment("CAD document has a filename constraint violation which may need to be resolved prior to check-in.")
   public static final String DUPLICATE_FILENAME_CONFLICT_ON_RESTRICTED_CS_CONTEXT = "386";

   @RBEntry("\"{0}\": questo figlio di documento CAD ha valori diversi per l'attributo \"{1}\" per i link componenti. L'attributo \"{1}\" deve essere lo stesso per tutti i componenti di questo oggetto.")
   public static final String LINKS_WITH_DIFFERENT_IBA_FOR_FIND_NUMBER = "387";

   @RBEntry("\"{0}\": questo figlio di documento CAD ha valori diversi per l'attributo \"{1}\" per i link componenti. L'attributo \"{1}\" deve essere lo stesso per tutti i componenti di questo oggetto.")
   public static final String LINKS_WITH_DIFFERENT_IBA_FOR_LINE_NUMBER = "388";

   @RBEntry("Valore attributo d'istanza non valido per l'indice bolla o il numero di riga.")
   public static final String INVALID_IBA_VALUE_FOR_FIND_NUMBER_OR_LINE_NUMBER = "389";

   @RBEntry("L'attributo \"{0}\" di tipo \"{1}\" non può essere propagato a \"{2}\". L'attributo deve essere di tipo stringa o numero intero.")
   @RBComment("Inconsistent IBA type on Find Number Attribute. It should be of type String or Integer")
   public static final String INCONSISTENT_IBA_TYPE_FIND_NUMBER = "390";

   @RBEntry("L'attributo \"{0}\" di tipo \"{1}\" non può essere propagato a \"{2}\". L'attributo deve essere di tipo numero intero.")
   @RBComment("Inconsistent IBA type on Line Number Attribute. It should be of type Integer")
   public static final String INCONSISTENT_IBA_TYPE_LINE_NUMBER = "391";

   @RBEntry("Il documento \"{0}\" non può essere inviato al sistema PDM in quanto la parti referenziate \"{1}\" non sono selezionata per la stessa operazione")
   public static final String CAN_NOT_SEND_TO_PDM_MODELITEMPARTLINK = "392";

   @RBEntry("Impossibile effettuare il Check-In di \"{0}\" in quanto la parte referenziata è nello schedario personale")
   public static final String CAN_NOT_CHECKIN_MODELITEMPARTLINK_PERSONAL_CABINATE = "393";

   @RBEntry("Impossibile eliminare \"{0}\" in quanto è collegato a uno o più documenti CAD o dinamici")
   public static final String CAN_NOT_DELETE_WTPART_MASTER_MODELITEMPARTLINK = "394";
   
   @RBEntry("Impossibile sovrascrivere \"{0}\" e \"{1}\" contemporaneamente. I rispettivi contesti non corrispondono. Le due copie devono appartenere a contesti PDM o allo stesso progetto.")
   @RBComment("Error message thrown by prepareForSaveAs() when one copy is from PDM context and other is from project.")
   @RBArgComment0("Identity of the first copy")
   @RBArgComment1("Identity of the second copy")
   public static final String SAVEAS_CONTAINER_MISMATCH = "395";

   @RBEntry("Impossibile sovrascrivere \"{0}\" e \"{1}\" contemporaneamente in quanto non appartengono allo stesso progetto.")
   @RBComment("Error message thrown by prepareForSaveAs() when two copies are from different projects.")
   @RBArgComment0("Identity of the first copy")
   @RBArgComment1("Identity of the second copy")
   public static final String SAVEAS_MULTIPLE_PROJECT_CONTAINERS = "396";

   @RBEntry("Impossibile copiare l'istanza perché il suo generico non figura nel progetto: \"{0}\". Il salvataggio della sola istanza modifica la family table del generico che deve essere modificabile in questo progetto.")
   public static final String CANNOT_COPY_INSTANCE_GENERIC_NOT_IN_PROJECT = "397";

   @RBEntry("Impossibile copiare gli oggetti dell'elenco contemporaneamente. I contesti delle rispettive copie non corrispondono. Tutte le copie devono appartenere a contesti PDM o allo stesso progetto.")
   @RBComment("Error message thrown by CopyManagerUtility when some copies are from PDM context and others are from project.")
   public static final String CANNOT_COPY_PDM_PROJECT = "398";

   @RBEntry("Impossibile copiare gli oggetti dell'elenco contemporaneamente. Le rispettive copie non appartengono allo stesso progetto.")
   @RBComment("Error message thrown by CopyManagerUtility when copies are from different projects.")
   public static final String CANNOT_COPY_MULTIPLE_PROJECTS = "399";
   
   @RBEntry("Impossibile salvare la parte \"{0}\" come nuovo oggetto. La nuova parte deve essere collegata al nuovo documento dinamico e l'azione non è supportata. Per cambiare il riferimento della parte, salvare il documento dinamico e la parte separatamente, quindi modificare il documento in modo che faccia riferimento alla nuova parte.")
   public static final String CAN_NO_SAVE_AS_REFERENCE_PART_TOGETHER = "400";
}
