/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.epm.util;

import wt.util.resource.*;

@RBUUID("wt.epm.util.EPMPreferencesRB")
@RBNameException //Grandfathered by conversion
public final class EPMPreferencesRB extends WTListResourceBundle {
   @RBEntry("EPM Service Preferences")
   public static final String EPM_PREFERENCES = "EPM_PREFERENCES";

   @RBEntry("EPM service preferences")
   public static final String EPM_PREFERENCES_DESC = "EPM_PREFERENCES_DESC";

   @RBEntry("Build Service Preferences")
   public static final String BUILD_PREFERENCES = "BUILD_PREFERENCES";

   @RBEntry("Preferences used by build service")
   public static final String BUILD_PREFERENCES_DESC = "BUILD_PREFERENCES_DESC";

   @RBEntry("Attributes Delimiter")
   public static final String ATTRIBUTES_DELIMITER = "ATTRIBUTES_DELIMITER";

   @RBEntry("This preference defines the delimiter character that separates the attributes to be published.")
   public static final String ATTRIBUTES_DELIMITER_SHORT_DESC = "ATTRIBUTES_DELIMITER_SHORT_DESC";

   @RBEntry("This preference defines the delimiter character that separates the attributes specified in preference Attributes to be published on Part, Attributes to be published on Link, Attributes to be published on Master and Attributes to be published on Occurrence")
   public static final String ATTRIBUTES_DELIMITER_DESC = "ATTRIBUTES_DELIMITER_DESC";

   @RBEntry("Attributes to be published on Part")
   public static final String BUILD_ATTRIBUTES_TO_PART = "BUILD_ATTRIBUTES_TO_PART";

   @RBEntry("Attributes to be published on Part")
   public static final String BUILD_ATTRIBUTES_TO_PART_SHORT_DESC = "BUILD_ATTRIBUTES_TO_PART_SHORT_DESC";

   @RBEntry("Attributes to be published on Part. Attributes are delimited by character specified in preference Attributes Delimiter")
   public static final String BUILD_ATTRIBUTES_TO_PART_DESC = "BUILD_ATTRIBUTES_TO_PART_DESC";

   @RBEntry("Attributes to be published on Link")
   public static final String BUILD_ATTRIBUTES_TO_LINK = "BUILD_ATTRIBUTES_TO_LINK";

   @RBEntry("Attributes to be published on Link")
   public static final String BUILD_ATTRIBUTES_TO_LINK_SHORT_DESC = "BUILD_ATTRIBUTES_TO_LINK_SHORT_DESC";

   @RBEntry("Attributes to be published on Link. Attributes are delimited by character specified in preference Attributes Delimiter")
   public static final String BUILD_ATTRIBUTES_TO_LINK_DESC = "BUILD_ATTRIBUTES_TO_LINK_DESC";

   @RBEntry("Attributes to be published on Master")
   public static final String BUILD_ATTRIBUTES_TO_MASTER = "BUILD_ATTRIBUTES_TO_MASTER";

   @RBEntry("Attributes to be published on Master")
   public static final String BUILD_ATTRIBUTES_TO_MASTER_SHORT_DESC = "BUILD_ATTRIBUTES_TO_MASTER_SHORT_DESC";

   @RBEntry("Attributes to be published on Master. Attributes are delimited by character specified in preference Attributes Delimiter")
   public static final String BUILD_ATTRIBUTES_TO_MASTER_DESC = "BUILD_ATTRIBUTES_TO_MASTER_DESC";

   @RBEntry("Attributes to be published on Occurrence")
   public static final String BUILD_ATTRIBUTES_TO_OCCURRENCE = "BUILD_ATTRIBUTES_TO_OCCURRENCE";

   @RBEntry("Attributes to be published on Occurrence")
   public static final String BUILD_ATTRIBUTES_TO_OCCURRENCE_SHORT_DESC = "BUILD_ATTRIBUTES_TO_OCCURRENCE_SHORT_DESC";

   @RBEntry("Attributes to be published on Occurrence. Attributes are delimited by character specified in preference Attributes Delimiter")
   public static final String BUILD_ATTRIBUTES_TO_OCCURRENCE_DESC = "BUILD_ATTRIBUTES_TO_OCCURRENCE_DESC";

   @RBEntry("Build Image Association by Default")
   public static final String BUILD_IMAGES = "BUILD_IMAGES";

   @RBEntry("Build Image Association by Default")
   public static final String BUILD_IMAGES_SHORT_DESC = "BUILD_IMAGES_SHORT_DESC";

   @RBEntry("Since Image associations do not push values to a part, it might not be necessary for you to build the image associations in your product structure. <br/><ul><li>Yes - The parts associated to a CAD document with an image association will be built and record the CAD document iteration available at the time of build. </li><li>No  - The part associated to the CAD document will not be iterated or built.</li></ul>")
   public static final String BUILD_IMAGES_DESC = "BUILD_IMAGES_DESC";

   @RBEntry("Ignore optional dependencies")
   public static final String GHOST_IGNORE_OPTIONAL_DEPENDENCIES = "GHOST_IGNORE_OPTIONAL_DEPENDENCIES";

   @RBEntry("Ignore optional reference dependencies")
   public static final String GHOST_IGNORE_OPTIONAL_REFERENCE_DEPENDENCIES = "GHOST_IGNORE_OPTIONAL_REFERENCE_DEPENDENCIES";

   @RBEntry("Ignore internal dependencies only ")
   public static final String GHOST_IGNORE_INTERNAL_DEPENDENCIES_ONLY = "GHOST_IGNORE_INTERNAL_DEPENDENCIES_ONLY";

   @RBEntry("Do not allow to ignore")
   public static final String GHOST_DO_NOT_ALLOW_IGNORE = "GHOST_DO_NOT_ALLOW_IGNORE";

   @RBEntry("Incomplete object resolution")
   public static final String GHOST_SERVER_RESOLUTION = "GHOST_SERVER_RESOLUTION";

   @RBEntry("Sets the type of CAD document / Dynamic Document dependents that a user may ignore during upload and check in.")
   public static final String GHOST_SERVER_RESOLUTION_SHORT_DESC = "GHOST_SERVER_RESOLUTION_SHORT_DESC";

   @RBEntry("Sets the type of CAD document / Dynamic Document dependents that a user may ignore during upload and check in. Allowed values: \"Ignore optional dependencies\", \"Ignore optional reference dependencies\", \"Ignore internal dependencies only\", \"Do not allow to ignore\". The default value is: \"Ignore optional dependencies\". <li>Ignore optional dependencies - Any non-required missing dependent may be ignored. This includes suppressed components in an assembly. <li>Ignore optional reference dependencies - Any non-required missing references may be ignored. <li>Ignore internal dependencies only - Only Creo internal dependencies may be ignored. <li>Do not allow to ignore - No missing dependents can be ignored.")
   public static final String GHOST_SERVER_RESOLUTION_DESC = "GHOST_SERVER_RESOLUTION_DESC";

   @RBEntry("Send a CAD document / Dynamic Document to PDM without optional dependents")
   public static final String MISSING_DEPENDENTS_FOR_CHECKIN = "MISSING_DEPENDENTS_FOR_CHECKIN";

   @RBEntry("Allow a CAD document / Dynamic Document to be sent to PDM without new optional dependents")
   public static final String MISSING_DEPENDENTS_FOR_CHECKIN_SHORT_DESC = "MISSING_DEPENDENTS_FOR_CHECKIN_SHORT_DESC";

   @RBEntry("Allow a CAD document / Dynamic Document to be sent to PDM without new optional dependents, Links to optional dependents are removed.")
   public static final String MISSING_DEPENDENTS_FOR_CHECKIN_DESC = "MISSING_DEPENDENTS_FOR_CHECKIN_DESC";

   @RBEntry("Contributing Content Attributes")
   public static final String BUILD_CONTRIBUTING_CONTENT_ATTRIBUTES_TO_PART = "BUILD_CONTRIBUTING_CONTENT_ATTRIBUTES_TO_PART";

   @RBEntry("Attributes to be published on Part by Contributing Content relationship.")
   public static final String BUILD_CONTRIBUTING_CONTENT_ATTRIBUTES_TO_PART_SHORT_DESC = "BUILD_CONTRIBUTING_CONTENT_ATTRIBUTES_TO_PART_SHORT_DESC";

   @RBEntry("Attributes to be published on Part by Contributing Content Relationship. Attributes are delimited by character specified in preference Attributes Delimiter")
   public static final String BUILD_CONTRIBUTING_CONTENT_ATTRIBUTES_TO_PART_DESC = "BUILD_CONTRIBUTING_CONTENT_ATTRIBUTES_TO_PART_DESC";

   @RBEntry("Contributing Image Attributes")
   public static final String BUILD_CONTRIBUTING_IMAGE_ATTRIBUTES_TO_PART = "BUILD_CONTRIBUTING_IMAGE_ATTRIBUTES_TO_PART";

   @RBEntry("Attributes to be published on Part by Contributing Image relationship.")
   public static final String BUILD_CONTRIBUTING_IMAGE_ATTRIBUTES_TO_PART_SHORT_DESC = "BUILD_CONTRIBUTING_IMAGE_ATTRIBUTES_TO_PART_SHORT_DESC";

   @RBEntry("Attributes to be published on Part by Contributing Image Relationship. Attributes are delimited by character specified in preference Attributes Delimiter")
   public static final String BUILD_CONTRIBUTING_IMAGE_ATTRIBUTES_TO_PART_DESC = "BUILD_CONTRIBUTING_IMAGE_ATTRIBUTES_TO_PART_DESC";

   @RBEntry("Find Number Attribute")
   public static final String FIND_NUMBER_ATTRIBUTE = "FIND_NUMBER_ATTRIBUTE";

   @RBEntry("Name of an attribute on CAD Document Uses Link that should be copied to the Part Usage's Find Number.")
   public static final String FIND_NUMBER_ATTRIBUTE_SHORT_DESC = "FIND_NUMBER_ATTRIBUTE_SHORT_DESC";

   @RBEntry("Name of an attribute on CAD Document Uses Link that should be copied to the Part Usage's Find Number. This preference is unset by default.")
   public static final String FIND_NUMBER_ATTRIBUTE_DESC = "FIND_NUMBER_ATTRIBUTE_DESC";

   @RBEntry("Line Number Attribute")
   public static final String LINE_NUMBER_ATTRIBUTE = "LINE_NUMBER_ATTRIBUTE";

   @RBEntry("Name of an attribute on CAD Document Uses Link that should be copied to the Part Usage's Line Number.")
   public static final String LINE_NUMBER_ATTRIBUTE_SHORT_DESC = "LINE_NUMBER_ATTRIBUTE_SHORT_DESC";

   @RBEntry("Name of an attribute on CAD Document Uses Link that should be copied to the Part Usage's Line Number. The attribute type must be an integer. This preference is unset by default.")
   public static final String LINE_NUMBER_ATTRIBUTE_DESC = "LINE_NUMBER_ATTRIBUTE_DESC";

   @RBEntry("Carry Forward Content Association for CAD Document Revise")
   public static final String CARRY_FORWARD_CONTENT_ASSOCIATION = "CARRY_FORWARD_CONTENT_ASSOCIATION";

   @RBEntry("When revising ONLY the CAD Document, the system can be configured to carry forward existing Content associations.")
   public static final String CARRY_FORWARD_CONTENT_ASSOCIATION_SHORT_DESC = "CARRY_FORWARD_CONTENT_ASSOCIATION_SHORT_DESC";

   @RBEntry("When revising ONLY the CAD Document, the system can be configured to carry forward existing Content associations. By default the value of this preference is set to 'No' and when you revise ONLY the CAD Document, the existing Part version will not be associated to the new CAD Document version. This ensures that released Part versions do not have any change in content without an iteration or revise of the Part. When your company allows released Parts to be associated to not-released CAD Documents, you can change the value of this preference to 'Yes' and the latest version of the Part will be associated to the new version of the CAD Document after revise.")
   public static final String CARRY_FORWARD_CONTENT_ASSOCIATION_DESC = "CARRY_FORWARD_CONTENT_ASSOCIATION_DESC";

}
