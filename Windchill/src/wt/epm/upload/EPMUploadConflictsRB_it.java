/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.epm.upload;

import wt.util.resource.*;

@RBUUID("wt.epm.upload.EPMUploadConflictsRB")
public final class EPMUploadConflictsRB_it extends WTListResourceBundle {
   @RBEntry("Conflitto di tipo sconosciuto")
   public static final String UNKNOWN_CONFLICT = "0";

   @RBEntry("Conflitto di caricamento")
   public static final String UPLOAD_CONFLICT = "1";

   @RBEntry("Il nome file del nuovo oggetto {0} esiste già nel database. Per caricare l'oggetto, è necessario aggiornarlo nel workspace.")
   public static final String UPLOAD_NAME_DUPLICATION = "10";

   @RBEntry("Impossibile modificare il documento, deve essere nuovo o sottoposto a Check-Out.")
   public static final String DOCUMENT_CAN_NOT_BE_MODIFIED = "11";

   @RBEntry("È necessario sottoporre l'istanza a Check-Out per poter caricare la family table modificata.")
   public static final String FAMILYTABLE_CAN_NOT_BE_MODIFIED = "12";

   @RBEntry("Il file è stato rinominato nel commonspace. Per caricare l'oggetto è necessario sincronizzare il workspace.")
   public static final String DOC_FILE_NAMES_DO_NOT_MATCH = "19";

   @RBEntry("Il file è stato rinominato nel commonspace. Per caricare l'oggetto è necessario sincronizzare il workspace.")
   public static final String DEPENDENCIES_FILE_NAMES_DO_NOT_MATCH = "21";

   @RBEntry("Impossibile creare un'entità autonoma da un'istanza non sottoposta a Check-Out.")
   public static final String CANNOT_MAKE_STANDALONE = "22";

   @RBEntry("L'organizzazione dell'istanza {0} differisce dall'organizzazione {1} a cui appartiene il generico.")
   @RBArgComment0("Name of the organization the instance belongs to")
   @RBArgComment1("Name of the organization the generic belongs to")
   public static final String UPLOAD_ORGANIZATION_CONFLICT = "23";

   @RBEntry("La definizione tipo soft {0} non esiste, contattare l'amministratore.")
   public static final String INVALID_TYPEDEF = "30";

   @RBEntry("Attributo 'derived' non true per le rappresentazioni {0}.")
   public static final String REPRESENTATIONS_ARE_NOT_DERIVED = "31";

   @RBEntry("L'attributo 'derived' impostato su true per i documenti {0}, ma i documenti non sono rappresentazioni.")
   public static final String STANDALONES_ARE_IMAGES = "32";

   @RBEntry("Le rappresentazioni {0} sono già aggiornate in base all'iterazione più recente dell'originale.")
   public static final String IMAGE_GETTING_UPDATED_FROM_NON_LATEST_ORIGINAL = "33";

   @RBEntry("Le rappresentazioni {0} sono state aggiornate in base a una diversa versione dell'originale.")
   public static final String IMAGE_HAS_REP_RULE_WITH_OTHER_VERSION_OF_ORIGINAL = "34";

   @RBEntry("ID iterazione non valido specificato per le rappresentazioni {0} in dati neutri.")
   public static final String INVALID_ITERATION_ID_FOR_IMAGE = "35";

   @RBEntry("ID master non valido specificato per gli originali {0} in dati neutri.")
   public static final String INVALID_MASTER_ID_FOR_ORIGINAL = "36";

   @RBEntry("Nome CAD non valido specificato per gli originali {0} in dati neutri.")
   public static final String INVALID_CAD_NAME_FOR_ORIGINAL = "37";

   @RBEntry("Impossibile caricare l'oggetto, in quanto non è sottoposto a Check-Out. Sottoporlo a Check-Out o escluderlo dall'azione.")
   public static final String UPLOAD_NEED_CHECKOUT = "38";

   @RBEntry("Impossibile caricare le seguenti immagini {0} senza caricare tutte le immagini dell'origine.")
   public static final String IMAGES_NEEDS_TO_BE_UPLOADED = "39";

   @RBEntry("Il percorso di una delle regole dei risultati nei dati neutri non inizia con il nodo radice (-1).")
   public static final String INCORRECT_RESULT_RULE_PATH = "40";

   @RBEntry("Regola padre mancante per una delle regole dei risultati nei dati neutri.")
   public static final String RESULT_RULE_PARENT_MISSING = "41";

   @RBEntry("Sono state trovate più rappresentazioni nei dati neutri del contesto delle rappresentazioni {0}.")
   public static final String MULTIPLE_REPS_FOUND = "42";

   @RBEntry("Due o più regole di definizione hanno lo stesso ordine nei dati neutri.")
   public static final String DUPLICATE_RULE_ORDER = "43";

   @RBEntry("Tipo di azione mancante per una delle regole dei risultati nei dati neutri.")
   public static final String RESULT_RULE_ATYPE_MISSING = "44";

   @RBEntry("Tipo di azione mancante per una delle regole di definizione nei dati neutri.")
   public static final String DEFINITION_RULE_ATYPE_MISSING = "45";

   @RBEntry("Impossibile trovare la regola dei risultati di default nei dati neutri.")
   public static final String DEFAULT_RESULT_RULE_NOT_FOUND = "46";

   @RBEntry("Impossibile trovare la regola di definizione di default nei dati neutri.")
   public static final String DEFAULT_DEFINITION_RULE_NOT_FOUND = "47";

   @RBEntry("Ambito mancante per più di una regola di definizione nei dati neutri.")
   public static final String DEFINITION_RULE_SCOPE_MISSING = "48";

   @RBEntry("Percorso degli indicatori di riferimento mancante per più di una regola dei risultati nei dati neutri.")
   public static final String RESULT_RULE_PATH_MISSING = "49";

   @RBEntry("Caricamento delle regole dei risultati non riuscito. Almeno una delle regole di definizione esistenti corrispondenti non è una regola di definizione semplice.")
   public static final String DEF_RULES_ALREADY_EXIST = "50";

   @RBEntry("Caricamento della rappresentazione alternativa non riuscito. La rappresentazione non contiene regole di risultati.")
   public static final String NO_RESULT_RULE_IN_ALTERNATE_REP = "51";

   @RBEntry("{0} contiene un inviluppo di vecchio tipo. L'inviluppo deve essere ridefinito in Creo prima del caricamento in Windchill.")
   public static final String ENVELOPE_RULE_MISSING = "52";

   @RBEntry("Caricamento del modulo configurabile {0} non riuscito. Il valore dell'attributo Massimo consentito o Minimo richiesto non è 1.")
   public static final String INCORRECT_MAXREQD_OR_MINALLOWED_VALUE = "53";

   @RBEntry("Impossibile caricare il modulo (o il prodotto) configurabile {0}. L'attributo Modulo configurabile non è impostato. Verificare che la versione di Creo in uso supporti le strutture configurabili.")
   public static final String INVALID_GENERIC_TYPE = "54";

   @RBEntry("Impossibile definire le scelte che seguono, identificate dal nome opzione e dal nome scelta specificati, poiché hanno più di una corrispondenza: \n{0}")
   public static final String DOC_AMBIGUOUS_CHOICES = "55";

   @RBEntry("Impossibile trovare le scelte che seguono identificate dal nome opzione e dal nome scelta specificati: \n{0}")
   public static final String DOC_MISSING_CHOICES = "56";

   @RBEntry("Impossibile definire le scelte che seguono, identificate dal nome opzione e dal nome scelta specificati sulle dipendenze dei membri, poiché hanno più di una corrispondenza: \n{0}")
   public static final String LINK_AMBIGUOUS_CHOICES = "57";

   @RBEntry("Impossibile trovare le scelte che seguono identificate dal nome opzione e dal nome scelta specificati sulle dipendenze dei membri: \n{0}")
   public static final String LINK_MISSING_CHOICES = "58";

   @RBEntry("Il nome file del nuovo oggetto \"{0}\" esiste già nel database in \"{1}\", cartella \"{2}\". Per caricare l'oggetto, è necessario aggiornarlo nel workspace.")
   public static final String UPLOAD_NAME_DUPLICATION_CHECKED_IN = "59";

   @RBEntry("Il nome file del nuovo oggetto \"{0}\" esiste già nel database in un contesto ristretto. Per caricare l'oggetto, è necessario aggiornarlo nel workspace.")
   public static final String UPLOAD_NAME_DUPLICATION_RESTRICTED = "60";

   @RBEntry("Il nome file del nuovo oggetto \"{0}\" esiste già nel database in \"{1}\" dell'utente \"{2}\". Per caricare l'oggetto, è necessario aggiornarlo nel workspace.")
   public static final String UPLOAD_NAME_DUPLICATION_NEW_IN_WORKSPACE= "61";

   @RBEntry("\"{0}\" del generico obsoleto per la creazione dell'istanza. Rimuovere il generico dal workspace o dalla sessione client o aggiornare il workspace con la versione più recente di \"{1}\" e ricreare l'istanza.")
   public static final String INVALID_NON_LATEST_GENERIC_IN_WORKSPACE = "62";

   @RBEntry("Caricamento non riuscito. Ripetere l'operazione o annullare ed eseguire il caricamento dal workspace.")
   public static final String UPLOAD_FAILED_TO_PREVENT_FAMILY_TABLE_BRANCHING = "63";

   @RBEntry("Un documento CAD \"{0}\" con questo nome di file esiste già in \"{1}\", cartella \"{2}\". Il problema deve essere risolto prima di effettuare il Check-In.")
   public static final String UPLOAD_NAME_DUPLICATION_IF_EXISTS_IN_COMMONSPACE = "64";

   @RBEntry("Un documento CAD \"{0}\" con questo nome di file esiste già in un contesto di cui non si dispone dei permessi di accesso. Il problema deve essere risolto prima di effettuare il Check-In.")
   public static final String UPLOAD_NAME_DUPLICATION_IF_EXISTS_IN_COMMONSPACE_RESTRICTED = "65";

   @RBEntry("Caricamento non riiuscito. Impossibile caricare \"{0}\" perché non sono state trovate nel sistema le parti master con gli identificatori oggetto seguenti: \"{1}\".")
   public static final String UPLOAD_PARTMASTER_OID_NOT_IN_SYSTEM = "66";
}
