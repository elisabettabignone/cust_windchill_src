/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.epm.upload;

import wt.util.resource.*;

@RBUUID("wt.epm.upload.EPMUploadConflictsRB")
public final class EPMUploadConflictsRB extends WTListResourceBundle {
   @RBEntry("Unknown Conflict")
   public static final String UNKNOWN_CONFLICT = "0";

   @RBEntry("Upload Conflict")
   public static final String UPLOAD_CONFLICT = "1";

   @RBEntry("New object {0} has the same file name that already exists in the database. In order to upload this object, you must update this object in the Workspace.")
   public static final String UPLOAD_NAME_DUPLICATION = "10";

   @RBEntry("Document must be new or checked out in order to be modified.")
   public static final String DOCUMENT_CAN_NOT_BE_MODIFIED = "11";

   @RBEntry("This instance must be checked out in order to upload the modified Family Table.")
   public static final String FAMILYTABLE_CAN_NOT_BE_MODIFIED = "12";

   @RBEntry("File name has been renamed in the Commonspace. To upload this object, you must perform Workspace Synchronization.")
   public static final String DOC_FILE_NAMES_DO_NOT_MATCH = "19";

   @RBEntry("File name has been renamed in the Commonspace. To upload this object, you must perform Workspace Synchronization.")
   public static final String DEPENDENCIES_FILE_NAMES_DO_NOT_MATCH = "21";

   @RBEntry("Can not make standalone from an instance which is not checkedout.")
   public static final String CANNOT_MAKE_STANDALONE = "22";

   @RBEntry("The instance's organization {0} differs from the organization {1} the generic belongs to.")
   @RBArgComment0("Name of the organization the instance belongs to")
   @RBArgComment1("Name of the organization the generic belongs to")
   public static final String UPLOAD_ORGANIZATION_CONFLICT = "23";

   @RBEntry("Soft Type definition {0} does not exist. Please contact administrator.")
   public static final String INVALID_TYPEDEF = "30";

   @RBEntry("'derived' attribute on representation/s {0} is not set to true.")
   public static final String REPRESENTATIONS_ARE_NOT_DERIVED = "31";

   @RBEntry("'derived' attribute on documents {0} is set to true although documents are not representations.")
   public static final String STANDALONES_ARE_IMAGES = "32";

   @RBEntry("Representation/s {0} are already updated from latest iteration of original.")
   public static final String IMAGE_GETTING_UPDATED_FROM_NON_LATEST_ORIGINAL = "33";

   @RBEntry("Representation/s {0} has been updated from other version of original.")
   public static final String IMAGE_HAS_REP_RULE_WITH_OTHER_VERSION_OF_ORIGINAL = "34";

   @RBEntry("Invalid iteration id specified for Representation/s {0} in neutral data.")
   public static final String INVALID_ITERATION_ID_FOR_IMAGE = "35";

   @RBEntry("Invalid master id specified for Original/s {0} in neutral data.")
   public static final String INVALID_MASTER_ID_FOR_ORIGINAL = "36";

   @RBEntry("Invalid cad name specified for Original/s {0} in neutral data.")
   public static final String INVALID_CAD_NAME_FOR_ORIGINAL = "37";

   @RBEntry("Unable to upload this object because it is not checked out. You must first check out this object or exclude it from this action.")
   public static final String UPLOAD_NEED_CHECKOUT = "38";

   @RBEntry("The following images {0} cannot be uploaded without uploading all images of this source.")
   public static final String IMAGES_NEEDS_TO_BE_UPLOADED = "39";

   @RBEntry("Path for one of the result rules in neutral data does not start with root node (-1).")
   public static final String INCORRECT_RESULT_RULE_PATH = "40";

   @RBEntry("Parent rule is missing for one of the result rules in neutral data.")
   public static final String RESULT_RULE_PARENT_MISSING = "41";

   @RBEntry("Multiple representations found in neutral data for representation context {0}.")
   public static final String MULTIPLE_REPS_FOUND = "42";

   @RBEntry("Two or more definition rules have the same order in neutral data.")
   public static final String DUPLICATE_RULE_ORDER = "43";

   @RBEntry("Action type is missing for one of the result rules in neutral data.")
   public static final String RESULT_RULE_ATYPE_MISSING = "44";

   @RBEntry("Action type is missing for one of the definition rules in neutral data.")
   public static final String DEFINITION_RULE_ATYPE_MISSING = "45";

   @RBEntry("Default result rule not found in neutral data.")
   public static final String DEFAULT_RESULT_RULE_NOT_FOUND = "46";

   @RBEntry("Default definition rule not found in neutral data.")
   public static final String DEFAULT_DEFINITION_RULE_NOT_FOUND = "47";

   @RBEntry("Scope is missing for more than one definition rule in neutral data.")
   public static final String DEFINITION_RULE_SCOPE_MISSING = "48";

   @RBEntry("Reference designator path is missing for more than one result rule in neutral data.")
   public static final String RESULT_RULE_PATH_MISSING = "49";

   @RBEntry("Upload of result rules failed since at least one of the corresponding existing definition rules is not a simple definition rule.")
   public static final String DEF_RULES_ALREADY_EXIST = "50";

   @RBEntry("Upload of alternate representation failed since it does not contain any result rule.")
   public static final String NO_RESULT_RULE_IN_ALTERNATE_REP = "51";

   @RBEntry("{0} contains an old style envelope. Envelope should be redefined in Creo before uploading to Windchill.")
   public static final String ENVELOPE_RULE_MISSING = "52";

   @RBEntry("Upload of configurable module {0} failed since value of Maximum Allowed/Minimum Required attribute is not 1.")
   public static final String INCORRECT_MAXREQD_OR_MINALLOWED_VALUE = "53";

   @RBEntry("The configurable module (or configurable product) {0} cannot be uploaded because the Configurable Module attribute is not set. Verify that your Creo version includes support for configurable structures.")
   public static final String INVALID_GENERIC_TYPE = "54";

   @RBEntry("The following choices identified by specified option name and choice name have more than one match and cannot be resolved: \n{0}")
   public static final String DOC_AMBIGUOUS_CHOICES = "55";

   @RBEntry("The following choices identified by specified option name and choice name cannot be found: \n{0}")
   public static final String DOC_MISSING_CHOICES = "56";

   @RBEntry("The following choices identified by specified option name and choice name on member dependencies have more than one match and cannot be resolved: \n{0}")
   public static final String LINK_AMBIGUOUS_CHOICES = "57";

   @RBEntry("The following choices identified by specified option name and choice name on member dependencies cannot be found: \n{0}")
   public static final String LINK_MISSING_CHOICES = "58";

   @RBEntry("New object \"{0}\" has the same filename that already exists in the database in \"{1}\", folder \"{2}\". In order to upload this object, you must update this object in the workspace.")
   public static final String UPLOAD_NAME_DUPLICATION_CHECKED_IN = "59";

   @RBEntry("New object \"{0}\" has the same filename that already exists in the database in a restricted context. In order to upload this object, you must update this object in the workspace.")
   public static final String UPLOAD_NAME_DUPLICATION_RESTRICTED = "60";

   @RBEntry("New object \"{0}\" has the same filename that already exists in the database in \"{1}\" owned by \"{2}\". In order to upload this object, you must update this object in the workspace.")
   public static final String UPLOAD_NAME_DUPLICATION_NEW_IN_WORKSPACE= "61";

   @RBEntry("The Generic's \"{0}\" for Instance creation is out of date. Please remove this Generic from the client session and workspace or update the workspace with latest \"{1}\" and recreate Instance.")
   public static final String INVALID_NON_LATEST_GENERIC_IN_WORKSPACE = "62";

   @RBEntry("Upload failed. Please Retry the operation or Cancel and Upload from Workspace.")
   public static final String UPLOAD_FAILED_TO_PREVENT_FAMILY_TABLE_BRANCHING = "63";

   @RBEntry("A CAD document \"{0}\" with this filename already exists in \"{1}\", folder \"{2}\". This will need to be resolved before check-in.")
   public static final String UPLOAD_NAME_DUPLICATION_IF_EXISTS_IN_COMMONSPACE = "64";

   @RBEntry("A CAD document \"{0}\" with this filename already exists in a Context you do not have access. This will need to be resolved before check-in.")
   public static final String UPLOAD_NAME_DUPLICATION_IF_EXISTS_IN_COMMONSPACE_RESTRICTED = "65";

   @RBEntry("Upload Failed. Can not upload \"{0}\" as part master(s) with object identifier(s) \"{1}\" not found in system.")
   public static final String UPLOAD_PARTMASTER_OID_NOT_IN_SYSTEM = "66";
}
