/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.epm.clients.htmltemplates;

import wt.util.resource.*;

@RBUUID("wt.epm.clients.htmltemplates.htmltemplatesResource")
public final class htmltemplatesResource_it extends WTListResourceBundle {
   @RBEntry("Nome modello")
   public static final String EPM_CAD_NAME = "1";

   @RBEntry("Istanza")
   public static final String EPM_INSTANCE = "2";

   @RBEntry("Generico")
   public static final String EPM_GENERIC = "3";

   @RBEntry("Tipo istanza")
   public static final String EPM_INSTANCE_TYPE = "4";

   @RBEntry("Family di")
   public static final String EPM_FAMILY = "5";

   @RBEntry("Family table")
   public static final String EPM_FAMILY_LINK_LABEL = "6";

   @RBEntry("Generico")
   public static final String EPM_GENERIC_LABEL = "7";

   @RBEntry("Altre istanze")
   public static final String EPM_OTHER_INSTANCE_LABEL = "8";

   @RBEntry("Istanze")
   public static final String EPM_INSTANCE_LABEL = "9";

   @RBEntry("Attivo")
   public static final String EPM_ACTIVE_LINK = "10";

   @RBEntry("Passivo")
   public static final String EPM_PASSIVE_LINK = "11";

   @RBEntry("Associazione")
   public static final String EPM_LINK_TYPE_TITLE = "12";
}
