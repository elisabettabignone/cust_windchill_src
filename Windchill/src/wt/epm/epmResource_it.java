/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.epm;

import wt.util.resource.*;

@RBUUID("wt.epm.epmResource")
public final class epmResource_it extends WTListResourceBundle {
   @RBEntry("{0} {1}")
   public static final String DOC_TYPE = "1";

   /**
    * Family Tables and Upload resources
    **/
   @RBEntry("Workspace inesistente: \"{0}\".")
   public static final String WORKSPACE_NOT_FOUND = "20";

   @RBEntry("Tipo di oggetto errato \"{0}\" nel risultato dell'interrogazione.")
   public static final String WRONG_TYPE_IN_QUERYRESULT = "30";

   @RBEntry("L'istanza \"{0}\" non ha una family table.")
   public static final String INSTANCE_HAS_NO_FAMILYTABLE = "40";

   @RBEntry("Non esiste un generico o istanza per il link della variante.")
   public static final String NO_GENERIC_OR_INSTANCE_FOR_VARIANT_LINK = "50";

   @RBEntry("CacheObject: l'oggetto da inserire in cache è nullo.")
   public static final String NULL_CACHE_OBJECT = "60";

   @RBEntry("Impossibile creare la cella della family table. Non è presente un link  containedIn")
   public static final String NO_CONTAINEDIN_TO_CREATE_CELL = "70";

   @RBEntry("Impossibile trovare una definizione attributo per {0}.")
   public static final String ATTR_DEF_NOT_FOUND = "90";

   @RBEntry("doResolution(): ERRORE. Non sono state fornite risoluzioni per {0} nel padre {1}.")
   public static final String NO_GHOST_RESOLUTION = "100";

   @RBEntry("Impossibile creare l'istanza \"{0}\" del dipendente mancante, poiché il suo generico non è un dipendente mancante.")
   public static final String GENERIC_NOT_A_GHOST = "110";

   @RBEntry("doResolution(): ERRORE. Il membro della family table non dispone di un documento.")
   public static final String NO_DOC_FOR_FT_MEMBER = "120";

   @RBEntry(" ERRORE. Il generico di livello superiore non dispone di una family table. Nome = {0}")
   public static final String NO_FT_FOR_TOPGENERIC = "130";

   @RBEntry("ERRORE. La family table non ha un membro di livello superiore.")
   public static final String NO_GENERIC_FOR_FT = "140";

   @RBEntry("Workspace nullo.")
   public static final String NULL_WORKSPACE = "150";

   @RBEntry("Dipendenza non valida \" {0} \" nome modello destinazione \" {1} \"")
   public static final String INVALID_DEPENDENCY = "160";

   @RBEntry("Tipo di dipendenza non valido. ID univoco dipendenza: \" {0} \"")
   public static final String INVALID_DEPENDENCY_TYPE = "170";

   @RBEntry(" ERRORE. Il generico non dispone di una family table. Nome = {0}")
   public static final String NO_FT_FOR_GENERIC = "180";

   @RBEntry("L'istanza non può avere elementi: nome = {0}")
   public static final String INTANCE_CANT_HAVE_ITEMS = "190";

   @RBEntry("Nessun nome di colonna: documento = {0}, colonna (elemento) = {1}\"")
   public static final String NO_COLUMN_DEFINITION = "200";

   @RBEntry("Il generico non ha un valore per la colonna definita: documento = {0}, colonna (elemento) = {1}")
   public static final String NO_COLUMN_VALUE = "210";

   @RBEntry("Nessun generico: documento = {0}, colonna (elemento) = {1}")
   public static final String NO_GENERIC = "220";

   @RBEntry("Il generico non ha valori per l'eredità: documento = {0}, colonna (elemento) = {1}")
   public static final String NO_INHERITANCE_VALUE = "230";

   @RBEntry("Definizione colonna parametri non valida: documento = {0}, colonna (elemento) = {1}")
   public static final String INVALID_PARAMETER_COLUMN_DEFINITION = "240";

   @RBEntry("Definizione feature non valida: documento = \"{0}\", colonna (elemento) = \"{1}\"")
   public static final String INVALID_FEATURE_DEFINITION = "250";

   @RBEntry("I documenti seguenti sono stati creati come documenti fantasma: {0}.")
   public static final String CREATED_GHOSTS_WARNING = "260";

   @RBEntry("Il dipendente mancante è stato risolto mediante snap a un documento CAD esistente. Il documento CAD {0} è stato aggiunto automaticamente al workspace corrente.")
   public static final String MISSING_DEPENDENT_RESOLVED_WARNING = "270";

   @RBEntry("Associazione eliminata per i documenti \"{0}\".")
   @RBComment("Warning message is shown when user deleting a document (new in workspace) having associated EPMBuildRule/EPMDescribeLink")
   @RBArgComment0("Name of the EPMDocuments")
   public static final String ASSOCIATION_DELETED_FOR_DOCUMENT = "280";

   @RBEntry("Iterazione non aggiornata sottoposta a Check-Out.")
   @RBComment("Warning message is shown after the user checks out nonlatest iteration of a document and container preference is set to \"Allow Nonlatest checkout without warning\"")
   public static final String CHECKOUT_NONLATEST_WITHOUT_CONFLICT_WARNING = "290";
}
