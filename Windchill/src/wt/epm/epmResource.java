/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.epm;

import wt.util.resource.*;

@RBUUID("wt.epm.epmResource")
public final class epmResource extends WTListResourceBundle {
   @RBEntry("{0} {1}")
   public static final String DOC_TYPE = "1";

   /**
    * Family Tables and Upload resources
    **/
   @RBEntry("Workspace does not exist: \"{0}\"")
   public static final String WORKSPACE_NOT_FOUND = "20";

   @RBEntry("Wrong object type \"{0}\" in QueryResult")
   public static final String WRONG_TYPE_IN_QUERYRESULT = "30";

   @RBEntry("Instance \"{0}\" has no family table.")
   public static final String INSTANCE_HAS_NO_FAMILYTABLE = "40";

   @RBEntry("No generic or instance for variant link.")
   public static final String NO_GENERIC_OR_INSTANCE_FOR_VARIANT_LINK = "50";

   @RBEntry("CacheObject: object for caching is null")
   public static final String NULL_CACHE_OBJECT = "60";

   @RBEntry("Cannot create family table cell. No containedIn link")
   public static final String NO_CONTAINEDIN_TO_CREATE_CELL = "70";

   @RBEntry("No attribute definition found for {0}.")
   public static final String ATTR_DEF_NOT_FOUND = "90";

   @RBEntry("doResolution() : ERROR! No Resolutions were provided for: {0} in the parent: {1}.")
   public static final String NO_GHOST_RESOLUTION = "100";

   @RBEntry("Failed to create the Missing Dependent instance: \"{0}\", since it's generic is not a missing Dependent.")
   public static final String GENERIC_NOT_A_GHOST = "110";

   @RBEntry("doResolution() : ERROR: family table member does not have document.")
   public static final String NO_DOC_FOR_FT_MEMBER = "120";

   @RBEntry(" ERROR: Top Generic does not have family table. name= {0}")
   public static final String NO_FT_FOR_TOPGENERIC = "130";

   @RBEntry("ERROR: Family table does not have top member.")
   public static final String NO_GENERIC_FOR_FT = "140";

   @RBEntry("Workspace is null.")
   public static final String NULL_WORKSPACE = "150";

   @RBEntry("Invalid dependency \" {0} \" target model name \" {1} \"")
   public static final String INVALID_DEPENDENCY = "160";

   @RBEntry("Invalid dependency type. Dependency unique id is \" {0} \"")
   public static final String INVALID_DEPENDENCY_TYPE = "170";

   @RBEntry(" ERROR: Generic does not have family table. name= {0}")
   public static final String NO_FT_FOR_GENERIC = "180";

   @RBEntry("Instance can not have items: name= {0}")
   public static final String INTANCE_CANT_HAVE_ITEMS = "190";

   @RBEntry("No column definition: : document={0}, column(item)={1}\"")
   public static final String NO_COLUMN_DEFINITION = "200";

   @RBEntry("Generic does not have value for defined column : document={0}, column(item)={1}")
   public static final String NO_COLUMN_VALUE = "210";

   @RBEntry("No Generic: document={0}, column(item)={1}")
   public static final String NO_GENERIC = "220";

   @RBEntry("Generic does not have value for inheritance: document={0}, column(item)={1}")
   public static final String NO_INHERITANCE_VALUE = "230";

   @RBEntry("Invalid parameter column definition: document={0}, column(item)={1}")
   public static final String INVALID_PARAMETER_COLUMN_DEFINITION = "240";

   @RBEntry("Invalid feature definition: document=\"{0}\", column(item)= \"{1}\"")
   public static final String INVALID_FEATURE_DEFINITION = "250";

   @RBEntry("The following documents were created as ghosts: {0}.")
   public static final String CREATED_GHOSTS_WARNING = "260";

   @RBEntry("Missing dependent resolved by snapping to existing CAD Document. CAD Document {0} automatically added to current workspace.")
   public static final String MISSING_DEPENDENT_RESOLVED_WARNING = "270";

   @RBEntry("Association is deleted for documents \"{0}\".")
   @RBComment("Warning message is shown when user deleting a document (new in workspace) having associated EPMBuildRule/EPMDescribeLink")
   @RBArgComment0("Name of the EPMDocuments")
   public static final String ASSOCIATION_DELETED_FOR_DOCUMENT = "280";

   @RBEntry("Non-latest iteration has been checked out.")
   @RBComment("Warning message is shown after the user checks out nonlatest iteration of a document and container preference is set to \"Allow Nonlatest checkout without warning\"")
   public static final String CHECKOUT_NONLATEST_WITHOUT_CONFLICT_WARNING = "290";
}
