/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.folder;

import wt.util.resource.*;

@RBUUID("wt.folder.folderResource")
public final class folderResource extends WTListResourceBundle {
   @RBEntry("A foldering error occurred. System message follows:")
   public static final String PRIVATE_CONSTANT_0 = "0";

   @RBEntry("The object is already in the folder and may not be added again.")
   public static final String PRIVATE_CONSTANT_1 = "1";

   @RBEntry("\"{0}\" is not a valid name.  It may be too long or contain illegal characters such as a slash(/).")
   public static final String INVALID_NAME = "2";

   @RBEntry("Object \"{0}\"<\"{1}\"> can only have one parent folder. It has none or more than one which is invalid. Contact your administrator.")
   public static final String INVALID_PARENT = "3";

   @RBEntry("Object \"{0}\" is already persistent and may not be assigned to a folder with this API.")
   public static final String FHASSIGN_ERROR = "4";

   @RBEntry("Object \"{0}\" must be assigned to a folder.")
   public static final String NEEDS_FOLDER = "5";

   @RBEntry("\"{0}\" is not a valid description for a Cabinet.")
   public static final String INVALID_DESCRIPTION = "6";

   @RBEntry("The folder (\"{0}\") is not persistent. Objects may only be assigned to folders that have been saved in the database.  Contact your administrator.")
   public static final String FOLDER_NOT_PERSISTENT = "7";

   @RBEntry("invalid character in name")
   public static final String INVALID_CHAR_IN_NAME = "8";

   @RBEntry("Could not locate OWNER \"{0}\".")
   public static final String OWNER_NOT_FOUND = "9";

   @RBEntry("Could not locate Administritive Domain \"{0}\".")
   public static final String DOMAIN_NOT_FOUND = "10";

   @RBEntry("The path <\"{0}\"> is not syntactically valid.")
   public static final String MALFORMED_PATH = "11";

   @RBEntry("The folder path <\"{0}\"> could not be found.")
   public static final String FOLDER_NOT_FOUND = "12";

   @RBEntry("The path specified <\"{0}\"> is not a SubFolder path. Use createCabinet to create cabinets.")
   public static final String NOT_SUBFOLDER = "13";

   @RBEntry("\"{0}\" is not a known operation delegate class name. Inform your administrator.")
   public static final String NO_KNOWN_DELEGATE = "14";

   @RBEntry("Unexpected DB Response. The Folder for object \"{0}\" no longer exists. ")
   public static final String FOLDER_NO_LONGER_EXISTS = "15";

   @RBEntry("\"{0}\" has no CabinetReference and it should. Inform your administrator.")
   public static final String NO_CAB_REF = "16";

   @RBEntry("There was more than one instance of \"{0}\" when there only should be one. Inform your administrator.")
   public static final String DUP_DATA_ERROR = "17";

   @RBEntry("The path specified <\"{0}\"> is not for a subfolder or member of a subfolder.")
   public static final String NOT_FOLDER_ENTRY = "18";

   @RBEntry("The folder entry <\"{0}\"> could not be found.")
   public static final String FOLDERENTRY_NOT_FOUND = "19";

   @RBEntry("The <\"{0}\"> <\"{1}\"> no longer exists in the database.")
   public static final String NO_LONGER_EXISTS = "20";

   @RBEntry("Could not find a <\"{0}\"> delegate for object <\"{1}\">. Inform your administrator.")
   public static final String NO_DELEGATE_FOUND = "21";

   @RBEntry("You may not perform the following operation on {1} {2}: {0}.  You do not have the following perission on {4} {5}: {3};")
   @RBArgComment0(" Refers to the operation, like move")
   @RBArgComment1(" Is the type portion of the identity of the object being operated on.")
   @RBArgComment2(" Is the identifier portion of the object being operated on")
   @RBArgComment3(" Is the permission that is lacking")
   @RBArgComment4(" Is the type portion of the identity of the destination object")
   @RBArgComment5(" Is the identifier portion of the identity of the destination object")
   public static final String NO_FOLDER_PERMISSION = "22";

   @RBEntry("Moving {0} {1} to {2} {3}...")
   public static final String FEEDBACK_MOVING = "23";

   @RBEntry("{0} {1} moved.")
   public static final String FEEDBACK_MOVING_COMPLETE = "24";

   @RBEntry("You may not directly move the {0} {1} to the {2} {3} because it is a move to a new cabinet.  To move the contents of {0} {1}, create a new {0} in the destination and move the members individually.")
   public static final String MOVE_NOT_SUPPORTED = "25";

   @RBEntry("The move of {0} {1} is not permitted.  You may not move an object from a shared cabinet to a personal one.")
   public static final String MOVE_TO_PCAB_DISALLOWED = "26";

   @RBEntry("Object \"{0}\" OID \"{1}\" has more than one parent folder. This is an error. Inform your administrator.")
   public static final String MULTIPLE_PARENTS = "27";

   @RBEntry("You may not move the {0} {1} onto itself.")
   public static final String CANNOT_MOVE_TO_SELF = "28";

   @RBEntry("Unable to fill in the attributes for the cabinet in the object.")
   public static final String INFLATE_FAILED = "29";

   @RBEntry("ATTENTION: Secured Action.  Access was denied for the following action: {0}.  You may not {1} objects in {2}. ")
   @RBArgComment0(" is the permission that you don't have")
   @RBArgComment1(" is the operation you can't perform")
   @RBArgComment2(" is the identity of the destination")
   public static final String NO_FOLDER_PERMISSION2 = "30";

   @RBEntry("move")
   public static final String MOVE_OPERATION = "31";

   @RBEntry("You may not perform the following operation on {1}: {0}. You do not have the following permission: {2}.")
   @RBArgComment0(" refers to the operation, like \"move\"")
   @RBArgComment1(" is the identity of the object being moved")
   @RBArgComment2(" is the permission that you don't have")
   public static final String NO_OBJECT_PERMISSION = "32";

   @RBEntry("Moving {0} to {1}...")
   @RBArgComment0(" Identity of object being moved")
   @RBArgComment1(" Identity of the destination")
   public static final String FEEDBACK_MOVING2 = "33";

   @RBEntry("{0} moved.")
   @RBArgComment0(" Identity of the object that was moved.")
   public static final String FEEDBACK_MOVING_COMPLETE2 = "34";

   @RBEntry("You may not move the {0} into itself.")
   @RBArgComment0(" Identity of the object that couldn't be moved")
   public static final String CANNOT_MOVE_TO_SELF2 = "35";

   @RBEntry("{0} {1}")
   @RBArgComment0(" The \"type\" portion of an object's identity")
   @RBArgComment1(" Identifier portion of the identity")
   public static final String IDENTITY = "36";

   @RBEntry("{0}")
   @RBArgComment0(" A value that must be localized (the text is coming from a resource bundle")
   public static final String VALUE = "37";

   @RBEntry("You may not directly move the {0} to the {1} because it is a move to a new cabinet.  To move the contents of {0}, create a new {2} in the destination and move the members individually.")
   @RBArgComment0(" Identity object being moved")
   @RBArgComment1(" Identity of the destination")
   @RBArgComment2(" Type of the object being moved")
   public static final String MOVE_NOT_SUPPORTED2 = "38";

   @RBEntry("You may only establish shortcut's to objects's that are already persistent, {0} is not.")
   @RBComment("Issues error if try to create shortcut to non-persistent object")
   @RBArgComment0(" Identity of the non-persistent object")
   public static final String SHORTCUT_TARGET_NOT_PERSISTENT = "39";

   @RBEntry("Unexpected error retrieving object's location property.")
   public static final String LOCATION_RETRIEVE_ERROR = "40";

   @RBEntry("Unexpected error retrieving object's cabinetReference property.")
   public static final String CABINETREF_RETRIEVE_ERROR = "41";

   @RBEntry("Unexpected error retrieving object's folderPath property.")
   public static final String FOLDERPATH_RETRIEVE_ERROR = "42";

   @RBEntry("You may not move the {0} into the same folder it's already in.")
   @RBArgComment0(" Identity of the object that couldn't be moved")
   public static final String CANNOT_MOVE_IN_PLACE = "43";

   @RBEntry("The move of {0} is not permitted.  You may not move an object from a shared cabinet to a personal one.")
   @RBArgComment0(" Identity of the object which can't be moved")
   public static final String MOVE_TO_PCAB_DISALLOWED2 = "44";

   @RBEntry("Create Cabinet failed.  A cabinet already exists with name: {0}.")
   @RBArgComment0(" name of the Cabinet which already esists.")
   public static final String CABINET_CREATE_NAME_CONFLICT = "45";

   @RBEntry("Personal Cabinet")
   public static final String PERSONAL_CABINET = "46";

   @RBEntry("Shared Cabinet")
   public static final String SHARED_CABINET = "47";

   @RBEntry("rename")
   public static final String RENAME_OPERATION = "48";

   @RBEntry("create")
   public static final String CREATE_OPERATION = "49";

   @RBEntry("You may not delete your personal cabinet.")
   public static final String CANT_DELETE_PERSONAL_CABINET = "50";

   @RBEntry("You may not delete the personal cabinet of an active user.")
   public static final String CANT_DELETE_ACTIVE_PERSONAL_CABINET = "51";

   @RBEntry("The domain reference specified <\"{0}\"> for the folder to inherit is not the domain of parent {1}.")
   @RBComment("Reports a conflict between domain_ref and inherits parameters specified for updateSubFolder API")
   @RBArgComment0(" domain reference specified")
   @RBArgComment1(" folder path of parent cabinet or folder")
   public static final String DOMAIN_NOT_PARENT = "52";

   @RBEntry("Rename Cabinet failed.  A cabinet already exists with name: {0}.")
   @RBArgComment0(" name of the Cabinet which already esists.")
   public static final String CABINET_RENAME_CONFLICT = "53";

   @RBEntry("Rename failed for: {0}.  A folder with the same name already exists in: {1}.")
   @RBArgComment0("identity the folder being renamed.")
   @RBArgComment1("identity of the parent folder.")
   public static final String SUBFOLDER_RENAME_CONFLICT = "54";

   @RBEntry("Move failed for: {0}.  A folder with the same name already exists in: {1}")
   @RBArgComment0("identity of the folder being moved.")
   @RBArgComment1("identity of the folder into which the folder was being moved.")
   public static final String SUBFOLDER_MOVE_NAME_CONFLICT = "55";

   @RBEntry("Create failed for: {0}.  A Folder with the same name already exists in: {1}.")
   @RBArgComment0("identity of the folder being created.")
   @RBArgComment1("identity of the parent folder where the folder was being created.")
   public static final String SUBFOLDER_CREATE_NAME_CONFLICT = "56";

   @RBEntry("FolderService.findFolderContents can only find objects that implement FolderEntry. The provided class \"{0}\" does not implement FolderEntry.")
   @RBArgComment0("The name of the class that did not implement FolderEntry")
   public static final String INVALID_ENTRY_CLASS = "57";

   @RBEntry("You may not move the {0} because it is checked out.")
   @RBArgComment0("Identity of the object that couldn't be moved")
   public static final String CANNOT_MOVE_CHECKED_OUT_OBJECT = "58";

   @RBEntry("The folder change operation could not be completed.")
   @RBComment("General message for multi-object folder change. Specific messages will be nested beneath this one.")
   public static final String CHANGE_FOLDER_FAILED = "59";

   @RBEntry("A folder cannot be moved itself and be a destination for the move of another object within the same call to changeFolder(). Problem folder was: \"{0}\".")
   @RBComment("The multi-object folder change takes a mapping of foldered objects to their new destinations. The operation is vetoed ff one of the foldered objects that you are relocating is also specified as the destination object for some other foldered object or objects.")
   public static final String CANNOT_BE_SOURCE_AND_DESTINATION = "60";

   @RBEntry("The parent folder could not be found for folder member: \"{0}\"")
   @RBComment("Like FOLDER_NOT_FOUND, except that it includes the display identity of the object rather than its path")
   @RBArgComment0("The display identity of the object whose folder couldn't be found")
   public static final String FOLDER_NOT_FOUND_2 = "61";

   @RBEntry("The child folder could not be found. Parent folder: \"{0}\" Child folder name: \"{1}\"")
   @RBArgComment0("The identity of the parent folder")
   @RBArgComment1("The name of the subfolder")
   public static final String SUBFOLDER_NOT_FOUND = "62";

   @RBEntry("The cabinet could not be found. Container: \"{0}\" Cabinet name: \"{1}\"")
   @RBArgComment0("The identity of the parent container")
   @RBArgComment1("The name of the cabinet")
   public static final String CABINET_NOT_FOUND = "63";

   @RBEntry("The object is already persistent and cannot be assigned to a folder using assignLocation. Object: \"{0}\"")
   @RBArgComment0("The object that the already is persistent")
   public static final String OBJECT_ALREADY_PERSISTENT = "64";

   @RBEntry("A cabinet member must be assigned to a cabinet, not a subfolder. Cabinet member: \"{0}\".  Subfolder: \"{0}=.")
   @RBArgComment0("The identity of the cabinet member that the user attempted to assign to a non-cabinet")
   @RBArgComment1("The identity of the subfolder that the user attempted to assign the cabinet member to. ")
   public static final String CABINET_MEMBER_MUST_GO_IN_CABINET = "65";

   @RBEntry("store")
   public static final String STORE_OPERATION = "66";

   @RBEntry("Invalid input.  Must be a reference to a Cabinet or SubFolder;")
   @RBArgComment0("The input to this method must be a CabinetReference or SubFolderReference object.")
   public static final String MUST_BE_SUBFOLDER_OR_CABINET_REF = "67";

   @RBEntry("delete")
   public static final String DELETE_OPERATION = "68";

   @RBEntry("New iteration of {0} must be created in same folder as all previous iterations of {0}.")
   @RBComment("A new iteration of an object version must be in the same folder as all other iterations of that version.")
   public static final String FOLDER_OF_NEW_ITERATION_BAD = "69";

   @RBEntry("Modify")
   public static final String MODIFY_PERMISSION = "70";
   
   @RBEntry("Access was denied for the following action: {0}. Please contact your administrator.")
   @RBComment("General message for delete of foldered objects. Specific messages, if any, will be nested beneath this one.")
   public static final String DELETE_OF_FOLDERED_OBJECT_FAILED = "71";

}
