/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.folder;

import wt.util.resource.*;

@RBUUID("wt.folder.folderResource")
public final class folderResource_it extends WTListResourceBundle {
   @RBEntry("Si è verificato un errore di memorizzazione in cartella. Messaggio di sistema:")
   public static final String PRIVATE_CONSTANT_0 = "0";

   @RBEntry("Impossibile aggiungere l'oggetto, poiché si trova già nella cartella.")
   public static final String PRIVATE_CONSTANT_1 = "1";

   @RBEntry("\"{0}\" non è un nome valido.  Potrebbe essere troppo lungo o contenere caratteri non validi, ad esempio una barra (/).")
   public static final String INVALID_NAME = "2";

   @RBEntry("L'oggetto \"{0}\"<\"{1}\"> può avere solo una cartella padre. L'oggetto non ne ha nessuna o ne ha più di una. Contattare l'amministratore.")
   public static final String INVALID_PARENT = "3";

   @RBEntry("L'oggetto \"{0}\" è già persistente e non può essere assegnato a una cartella con questa API.")
   public static final String FHASSIGN_ERROR = "4";

   @RBEntry("L'oggetto \"{0}\" deve essere assegnato a una cartella.")
   public static final String NEEDS_FOLDER = "5";

   @RBEntry("\"{0}\" non è una descrizione di schedario valida.")
   public static final String INVALID_DESCRIPTION = "6";

   @RBEntry("La cartella (\"{0}\") non è persistente. È possibile assegnare oggetti solo a cartelle salvate nel database.   Contattare l'amministratore.")
   public static final String FOLDER_NOT_PERSISTENT = "7";

   @RBEntry("Carattere non valido nel nome")
   public static final String INVALID_CHAR_IN_NAME = "8";

   @RBEntry("Impossibile individuare il Proprietario \"{0}\".")
   public static final String OWNER_NOT_FOUND = "9";

   @RBEntry("Impossibile individuare il dominio amministrativo \"{0}\".")
   public static final String DOMAIN_NOT_FOUND = "10";

   @RBEntry("La sintassi del percorso <\"{0}\"> non è valida.")
   public static final String MALFORMED_PATH = "11";

   @RBEntry("Impossibile trovare il percorso della cartella <\"{0}\">.")
   public static final String FOLDER_NOT_FOUND = "12";

   @RBEntry("Il percorso <\"{0}\"> specificato non è un percorso di sottocartella. Utilizzare createCabinet per la creazione di schedari.")
   public static final String NOT_SUBFOLDER = "13";

   @RBEntry("\"{0}\" non è un nome di classe conosciuto per il delegato operazione. Informare l'amministratore.")
   public static final String NO_KNOWN_DELEGATE = "14";

   @RBEntry("Risposta di database non prevista. La cartella per l'oggetto \"{0}\" non esiste più.")
   public static final String FOLDER_NO_LONGER_EXISTS = "15";

   @RBEntry("\"{0}\" non ha CabinetReference. Informare l'amministratore.")
   public static final String NO_CAB_REF = "16";

   @RBEntry("Errore dovuto alla presenza di più di una istanza di \"{0}\".  Informare l'amministratore.")
   public static final String DUP_DATA_ERROR = "17";

   @RBEntry("Il percorso <\"{0}\"> specificato non è per una sottocartella o per un membro di sottocartella.")
   public static final String NOT_FOLDER_ENTRY = "18";

   @RBEntry("Impossibile trovare l'elemento della cartella <\"{0}\">.")
   public static final String FOLDERENTRY_NOT_FOUND = "19";

   @RBEntry("<\"{0}\"> <\"{1}\"> non è più presente nel database.")
   public static final String NO_LONGER_EXISTS = "20";

   @RBEntry("Impossibile trovare un delegato <\"{0}\"> per l'oggetto <\"{1}\">. Informare l'amministratore.")
   public static final String NO_DELEGATE_FOUND = "21";

   @RBEntry("Non si è autorizzati ad eseguire l'operazione seguente su {2} {1}: {0}.  Non si dispone dei seguente permesso per {5} {4}: {3};")
   @RBArgComment0(" Refers to the operation, like move")
   @RBArgComment1(" Is the type portion of the identity of the object being operated on.")
   @RBArgComment2(" Is the identifier portion of the object being operated on")
   @RBArgComment3(" Is the permission that is lacking")
   @RBArgComment4(" Is the type portion of the identity of the destination object")
   @RBArgComment5(" Is the identifier portion of the identity of the destination object")
   public static final String NO_FOLDER_PERMISSION = "22";

   @RBEntry("Spostamento di {0} {1} a {2} {3} in corso...")
   public static final String FEEDBACK_MOVING = "23";

   @RBEntry("{0} Spostamento di {1} completato.")
   public static final String FEEDBACK_MOVING_COMPLETE = "24";

   @RBEntry("Impossibile spostare direttamente {0} {1} a {2} {3}. Si tratta di uno spostamento a un nuovo schedario.   Per spostare il contenuto di {0} {1}, creare un nuovo {0} nella destinazione e spostare i membri individualmente.")
   public static final String MOVE_NOT_SUPPORTED = "25";

   @RBEntry("Lo spostamento di {0} {1} non è consentito.  Impossibile spostare un oggetto da uno schedario condiviso a uno schedario personale.")
   public static final String MOVE_TO_PCAB_DISALLOWED = "26";

   @RBEntry("L'oggetto \"{0}\" OID \"{1}\" ha più di una cartella padre. Errore. Informare l'amministratore.")
   public static final String MULTIPLE_PARENTS = "27";

   @RBEntry("Impossibile spostare {0} {1} su se stesso.")
   public static final String CANNOT_MOVE_TO_SELF = "28";

   @RBEntry("Impossibile completare gli attributi per lo schedario nell'oggetto.")
   public static final String INFLATE_FAILED = "29";

   @RBEntry("ATTENZIONE: azione protetta. Accesso negato per l'azione che segue: {0}. Non si dispone dei permessi per {1} oggetti in {2}. ")
   @RBArgComment0(" is the permission that you don't have")
   @RBArgComment1(" is the operation you can't perform")
   @RBArgComment2(" is the identity of the destination")
   public static final String NO_FOLDER_PERMISSION2 = "30";

   @RBEntry("spostamento")
   public static final String MOVE_OPERATION = "31";

   @RBEntry("Non si è autorizzati ad eseguire l'operazione seguente su {1}: {0}.  Non si dispone dei seguente permesso: {2}.")
   @RBArgComment0(" refers to the operation, like \"move\"")
   @RBArgComment1(" is the identity of the object being moved")
   @RBArgComment2(" is the permission that you don't have")
   public static final String NO_OBJECT_PERMISSION = "32";

   @RBEntry("Spostamento di {0} a {1} in corso...")
   @RBArgComment0(" Identity of object being moved")
   @RBArgComment1(" Identity of the destination")
   public static final String FEEDBACK_MOVING2 = "33";

   @RBEntry("Spostamento di {0} completato.")
   @RBArgComment0(" Identity of the object that was moved.")
   public static final String FEEDBACK_MOVING_COMPLETE2 = "34";

   @RBEntry("Impossibile spostare {0} in se stesso.")
   @RBArgComment0(" Identity of the object that couldn't be moved")
   public static final String CANNOT_MOVE_TO_SELF2 = "35";

   @RBEntry("{0} {1}")
   @RBArgComment0(" The \"type\" portion of an object's identity")
   @RBArgComment1(" Identifier portion of the identity")
   public static final String IDENTITY = "36";

   @RBEntry("{0}")
   @RBArgComment0(" A value that must be localized (the text is coming from a resource bundle")
   public static final String VALUE = "37";

   @RBEntry("Impossibile spostare direttamente {0} a {1}. Si tratta di uno spostamento a un nuovo schedario.   Per spostare il contenuto di {0}, creare un nuovo {2} nella destinazione e spostare i membri individualmente.")
   @RBArgComment0(" Identity object being moved")
   @RBArgComment1(" Identity of the destination")
   @RBArgComment2(" Type of the object being moved")
   public static final String MOVE_NOT_SUPPORTED2 = "38";

   @RBEntry("È possibile stabilire collegamenti solo a oggetti già persistenti. {0} non è persistente.")
   @RBComment("Issues error if try to create shortcut to non-persistent object")
   @RBArgComment0(" Identity of the non-persistent object")
   public static final String SHORTCUT_TARGET_NOT_PERSISTENT = "39";

   @RBEntry("Errore non previsto durante il recupero della proprietà di posizione dell'oggetto.")
   public static final String LOCATION_RETRIEVE_ERROR = "40";

   @RBEntry("Errore non previsto durante il recupero della proprietà cabinetReference dell'oggetto.")
   public static final String CABINETREF_RETRIEVE_ERROR = "41";

   @RBEntry("Errore non previsto durante il recupero della proprietà folderPath dell'oggetto.")
   public static final String FOLDERPATH_RETRIEVE_ERROR = "42";

   @RBEntry("{0} si trova già nella cartella specificata.")
   @RBArgComment0(" Identity of the object that couldn't be moved")
   public static final String CANNOT_MOVE_IN_PLACE = "43";

   @RBEntry("Lo spostamento di {0} non è consentito.  Impossibile spostare un oggetto da uno schedario condiviso a uno schedario personale.")
   @RBArgComment0(" Identity of the object which can't be moved")
   public static final String MOVE_TO_PCAB_DISALLOWED2 = "44";

   @RBEntry("La creazione dello schedario è fallita. Uno schedario {0} esiste già.")
   @RBArgComment0(" name of the Cabinet which already esists.")
   public static final String CABINET_CREATE_NAME_CONFLICT = "45";

   @RBEntry("Schedario personale")
   public static final String PERSONAL_CABINET = "46";

   @RBEntry("Schedario condiviso")
   public static final String SHARED_CABINET = "47";

   @RBEntry("ridenominazione")
   public static final String RENAME_OPERATION = "48";

   @RBEntry("creazione")
   public static final String CREATE_OPERATION = "49";

   @RBEntry("Impossibile eliminare lo schedario personale.")
   public static final String CANT_DELETE_PERSONAL_CABINET = "50";

   @RBEntry("Non è consentita l'eliminazione dello schedario personale di un utente attivo.")
   public static final String CANT_DELETE_ACTIVE_PERSONAL_CABINET = "51";

   @RBEntry("Il riferimento al dominio specificato <\"{0}\"> ereditato dalla cartella non è il dominio del padre {1}.")
   @RBComment("Reports a conflict between domain_ref and inherits parameters specified for updateSubFolder API")
   @RBArgComment0(" domain reference specified")
   @RBArgComment1(" folder path of parent cabinet or folder")
   public static final String DOMAIN_NOT_PARENT = "52";

   @RBEntry("La rinomina dello schedario è fallita. Uno schedario {0} esiste già.")
   @RBArgComment0(" name of the Cabinet which already esists.")
   public static final String CABINET_RENAME_CONFLICT = "53";

   @RBEntry("La rinomina di  {0} è fallita. Una cartella con lo stesso nome esiste già in: {1}.")
   @RBArgComment0("identity the folder being renamed.")
   @RBArgComment1("identity of the parent folder.")
   public static final String SUBFOLDER_RENAME_CONFLICT = "54";

   @RBEntry("Lo spostamento di  {0} è fallito. Una cartella con lo stesso nome esiste già in: {1}.")
   @RBArgComment0("identity of the folder being moved.")
   @RBArgComment1("identity of the folder into which the folder was being moved.")
   public static final String SUBFOLDER_MOVE_NAME_CONFLICT = "55";

   @RBEntry("La creazione di {0} è fallita. Una cartella con lo stesso nome esiste già in: {1}.")
   @RBArgComment0("identity of the folder being created.")
   @RBArgComment1("identity of the parent folder where the folder was being created.")
   public static final String SUBFOLDER_CREATE_NAME_CONFLICT = "56";

   @RBEntry("FolderService.findFolderContents può solo trovare gli oggetti che implementano FolderEntry. La classe specificata \"{0}\" non implementa FolderEntry.")
   @RBArgComment0("The name of the class that did not implement FolderEntry")
   public static final String INVALID_ENTRY_CLASS = "57";

   @RBEntry("Impossibile spostare {0}. Oggetto sottoposto a Check-Out.")
   @RBArgComment0("Identity of the object that couldn't be moved")
   public static final String CANNOT_MOVE_CHECKED_OUT_OBJECT = "58";

   @RBEntry("Impossibile completare l'operazione di modifica cartella.")
   @RBComment("General message for multi-object folder change. Specific messages will be nested beneath this one.")
   public static final String CHANGE_FOLDER_FAILED = "59";

   @RBEntry("Una cartella non può essere spostata ed essere al contempo la destinazione dello spostamento di un altro oggetto nell'ambito della stessa chiamata della funzione changeFolder(). La cartella su cui si è verificato il problema è: \"{0}\".")
   @RBComment("The multi-object folder change takes a mapping of foldered objects to their new destinations. The operation is vetoed ff one of the foldered objects that you are relocating is also specified as the destination object for some other foldered object or objects.")
   public static final String CANNOT_BE_SOURCE_AND_DESTINATION = "60";

   @RBEntry("Impossibile trovare la cartella padre per il membro cartella: \"{0}\"")
   @RBComment("Like FOLDER_NOT_FOUND, except that it includes the display identity of the object rather than its path")
   @RBArgComment0("The display identity of the object whose folder couldn't be found")
   public static final String FOLDER_NOT_FOUND_2 = "61";

   @RBEntry("Impossibile trovare la cartella figlio. Cartella padre: \"{0}\", nome cartella figlio: \"{1}\"")
   @RBArgComment0("The identity of the parent folder")
   @RBArgComment1("The name of the subfolder")
   public static final String SUBFOLDER_NOT_FOUND = "62";

   @RBEntry("Impossibile trovare lo schedario. Contenitore: \"{0}\", nome schedario: \"{1}\"")
   @RBArgComment0("The identity of the parent container")
   @RBArgComment1("The name of the cabinet")
   public static final String CABINET_NOT_FOUND = "63";

   @RBEntry("L'oggetto è già persistente e non può essere assegnato a una cartella mediante la funzione assignLocation. Oggetto: \"{0}\"")
   @RBArgComment0("The object that the already is persistent")
   public static final String OBJECT_ALREADY_PERSISTENT = "64";

   @RBEntry("Impossibile assegnare un membro schedario a una sottocartella, è necessario assegnarlo a uno schedario. Membro schedario: \"{0}\", sottocartella: \"{0}=.")
   @RBArgComment0("The identity of the cabinet member that the user attempted to assign to a non-cabinet")
   @RBArgComment1("The identity of the subfolder that the user attempted to assign the cabinet member to. ")
   public static final String CABINET_MEMBER_MUST_GO_IN_CABINET = "65";

   @RBEntry("archiviazione")
   public static final String STORE_OPERATION = "66";

   @RBEntry("Input non valido. Deve essere un riferimento a uno schedario o a una sottocartella.")
   @RBArgComment0("The input to this method must be a CabinetReference or SubFolderReference object.")
   public static final String MUST_BE_SUBFOLDER_OR_CABINET_REF = "67";

   @RBEntry("elimina")
   public static final String DELETE_OPERATION = "68";

   @RBEntry("È necessario creare la nuova iterazione di {0} nella stessa cartella di tutte le iterazioni precedenti di {0}.")
   @RBComment("A new iteration of an object version must be in the same folder as all other iterations of that version.")
   public static final String FOLDER_OF_NEW_ITERATION_BAD = "69";

   @RBEntry("Modifica")
   public static final String MODIFY_PERMISSION = "70";
   
   @RBEntry("Accesso negato per l'azione che segue: {0}. Contattare l'amministratore.")
   @RBComment("General message for delete of foldered objects. Specific messages, if any, will be nested beneath this one.")
   public static final String DELETE_OF_FOLDERED_OBJECT_FAILED = "71";

}
