/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.projmgmt.admin;

import wt.util.resource.*;

@RBUUID("wt.projmgmt.admin.adminResource")
public final class adminResource extends WTListResourceBundle {
   @RBEntry("Insufficient rights to change project {0}.")
   public static final String NO_RIGHTS = "0";

   @RBEntry("The Project Template must be stored in a personal cabinet or the System cabinet.")
   public static final String PROJECT_TEMPLATE_CABINET_INVALID = "1";

   @RBEntry("This iteration of the {0} Project Template is used in {1} places.  The following uses must be removed before completing the delete: {2}.")
   public static final String DELETE_PROJECT_TEMPLATE_PROHIBITED = "2";

   @RBEntry("This iteration of the {0} Project Template is used in {1} places.  The following uses must be removed before updating the Project Template: {2}.")
   public static final String MODIFY_PROJECT_TEMPLATE_PROHIBITED = "3";

   @RBEntry(", ")
   public static final String IDENTITY_STRING_DELIMITER = "4";

   @RBEntry("Unable to fill in the attributes for the Project Template.")
   public static final String INFLATE_FAILED = "5";

   @RBEntry("Unable to find or create {0}.")
   public static final String UNABLE_TO_CREATE_DIR = "6";

   @RBEntry("LoadProjAdmin: No cached value for current Project2.  Insure that a Project2Begin record occurred before the Project2End record.")
   public static final String LOAD_NO_CACHE_PROJECT = "7";

   @RBEntry("LoadProjAdmin: Error detected in {0}.  Check Method Server log.")
   public static final String LOAD_EXCEPTION = "8";

   @RBEntry("You are not authorized to create a Project Template.")
   public static final String CREATE_PT_NOT_ALLOWED = "9";

   @RBEntry("LoadProjAdmin: {0} cabinet is nonexistent.  Project Template can not be created.")
   public static final String LOAD_NO_SYSTEM_CABINET = "10";

   @RBEntry("LoadProjAdmin: No folder location was given.  Attempting to save Project Template in {0}.")
   public static final String LOAD_NULL_LOCATION = "11";

   @RBEntry("LoadProjAdmin: Project Templates must be stored in the cabinet {0}.  Defaulting to folder {1}.")
   public static final String LOAD_NOT_SYSTEM_CABINET = "12";

   @RBEntry("LoadProjAdmin: Folder location {0} does not exist.  Project Template can not be saved.")
   public static final String LOAD_INVALID_FOLDER = "13";

   @RBEntry("Warning:  A new iteration was created for the {0} Project Template. ")
   public static final String NEW_ITERATION_CREATED = "14";

   @RBEntry("You are not authorized to update the members of this Team.")
   public static final String UPDATE_TEAM_NOT_ALLOWED = "15";

   @RBEntry("The {0} object belongs in the {1} Project.  The Project has a {2} state and is not available for update.")
   public static final String MODIFY_INACTIVE_PROJECT_PROHIBITED = "16";

   @RBEntry("The {0} state is not valid for inactivating a Project.  ")
   public static final String INVALID_INACTIVATE_STATE = "17";

   @RBEntry("All roles in an active project must have at least one participant.   ")
   public static final String ROLE_PARTICIPANTS_REQUIRED = "18";

   @RBEntry("The {0} role must have one or more confirmed participants.")
   public static final String MANAGER_REQUIRED = "19";

   @RBEntry("Check in the {0} project template into a shared location before attempting to generate a Project from it.")
   public static final String CHECK_IN_PT_BEFORE_CREATING_PROJECT2 = "20";

   @RBEntry("Cannot create project: non-subscribing organization {0}.")
   public static final String NON_SUBSCRIBING = "21";

   @RBEntry("Cannot access domain {0}: insufficient privileges or domain doesn't exist.")
   public static final String CANT_FIND_DOMAIN = "22";

   @RBEntry("You are not allowed to modify the Project Creators group.")
   public static final String CANT_MODIFY_PC_GROUP = "23";

   @RBEntry("You are not allowed to modify the Organization Administrators group.")
   public static final String CANT_MODIFY_OA_GROUP = "24";

   @RBEntry("My Notebook")
   public static final String MY_NOTEBOOK = "25";

   @RBEntry("The Project Creators group does not exist.  There must have been an error at the time of install or migration.  Please see your system administrator.   ")
   public static final String GLOBAL_PROJ_CREATORS_GROUP_MISSING = "26";

   @RBEntry("The {0} role name is invalid.  Role names cannot contain any of the following characters:  {1}")
   public static final String INVALID_CHARACTERS_IN_ROLE_NAME = "27";

   @RBEntry("The Project Template specified for the Project is invalid.")
   public static final String INVALID_PROJECT_TEMPLATE_SPECIFIED_FOR_PROJECT = "28";

   @RBEntry(" A Project by the name of {0} already exists.  Please change the name of the Project and try again.")
   public static final String DUPLICATE_PROJECT_NAME = "29";

   @RBEntry(" Cannot create a Project:  Project Template is required.  ")
   public static final String PROJECT_TEMPLATE_REQUIRED = "30";

   @RBEntry("Project Creators")
   public static final String PRIVATE_CONSTANT_0 = "CREATORS";

   @RBEntry("All Members")
   public static final String PRIVATE_CONSTANT_1 = "MEMBERS";

   @RBEntry("Cannot create Program because user is not a member of the Program Creators group.")
   @RBComment("The error message displayed if a non Program Creator attempts to create a Program.")
   public static final String ERROR_CANNOT_CREATE_PROGRAMS = "31";

   @RBEntry("Cannot create Program because PROGRAM MANAGER role is missing.")
   @RBComment("The error message displayed if PROGRAM MANAGER role is deleted from the configuration files")
   public static final String ERROR_CANNOT_CREATE_PROGRAMS_NO_PROGRAM_MANAGER_GROUP = "32";
}
