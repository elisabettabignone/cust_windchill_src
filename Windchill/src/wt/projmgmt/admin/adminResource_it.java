/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.projmgmt.admin;

import wt.util.resource.*;

@RBUUID("wt.projmgmt.admin.adminResource")
public final class adminResource_it extends WTListResourceBundle {
   @RBEntry("Diritti insufficienti per modificare il progetto {0}.")
   public static final String NO_RIGHTS = "0";

   @RBEntry("Il modello del progetto deve essere memorizzato in uno schedario personale o nello schedario System.")
   public static final String PROJECT_TEMPLATE_CABINET_INVALID = "1";

   @RBEntry("Questa iterazione del modello di progetto {0} è usata in {1} posizioni. I seguenti componenti devono essere rimossi prima di completare l'operazione di eliminazione: {2}.")
   public static final String DELETE_PROJECT_TEMPLATE_PROHIBITED = "2";

   @RBEntry("Questa iterazione del modello di progetto {0} è usata in {1} posizioni. I seguenti componenti devono essere rimossi prima di aggiornare l'operazione di eliminazione: {2}.")
   public static final String MODIFY_PROJECT_TEMPLATE_PROHIBITED = "3";

   @RBEntry(", ")
   public static final String IDENTITY_STRING_DELIMITER = "4";

   @RBEntry("Impossibile completare gli attributi per il modello del progetto.")
   public static final String INFLATE_FAILED = "5";

   @RBEntry("Impossibile trovare o creare {0}.")
   public static final String UNABLE_TO_CREATE_DIR = "6";

   @RBEntry("LoadProjAdmin: nessun valore memorizzato nella cache per Project2. Verificare che esista un record Project2Begin prima del record Project2End.")
   public static final String LOAD_NO_CACHE_PROJECT = "7";

   @RBEntry("LoadProjAdmin: rilevato errore in {0}. Controllare il log del method server.")
   public static final String LOAD_EXCEPTION = "8";

   @RBEntry("L'utente non è autorizzato a creare un modello di progetto.")
   public static final String CREATE_PT_NOT_ALLOWED = "9";

   @RBEntry("LoadProjAdmin: lo schedario {0} non esiste.  Impossibile creare il modello di progetto.")
   public static final String LOAD_NO_SYSTEM_CABINET = "10";

   @RBEntry("LoadProjAdmin: non è stata indicata alcuna cartella.  Tentativo di salvare il modello del progetto in {0} in corso.")
   public static final String LOAD_NULL_LOCATION = "11";

   @RBEntry("LoadProjAdmin: i modelli di progetto devono essere memorizzati nello schedario {0}. Invio di default alla cartella {1} in corso.")
   public static final String LOAD_NOT_SYSTEM_CABINET = "12";

   @RBEntry("LoadProjAdmin: la catella {0} non esiste. Impossibile salvare il modello del progetto.")
   public static final String LOAD_INVALID_FOLDER = "13";

   @RBEntry("Avvertenza: una nuova iterazione è stata creata per il modello di progetto {0}. ")
   public static final String NEW_ITERATION_CREATED = "14";

   @RBEntry("L'utente non è autorizzato ad aggiornare i membri di questo team.")
   public static final String UPDATE_TEAM_NOT_ALLOWED = "15";

   @RBEntry("L'oggetto {0} appartiene al progetto {1}. Il progetto è in stato {2} e non può essere aggiornato.")
   public static final String MODIFY_INACTIVE_PROJECT_PROHIBITED = "16";

   @RBEntry("Lo stato del ciclo di vita {0} non è valido per disattivare un progetto.  ")
   public static final String INVALID_INACTIVATE_STATE = "17";

   @RBEntry("Tutti i ruoli di un progetto attivo devono avere almeno un partecipante.  ")
   public static final String ROLE_PARTICIPANTS_REQUIRED = "18";

   @RBEntry("Il ruolo {0} deve avere uno o più partecipanti confermati.")
   public static final String MANAGER_REQUIRED = "19";

   @RBEntry("Effettuare il Check-In del modello di progetto {0} in una cartella condivisa prima di generare un progetto.")
   public static final String CHECK_IN_PT_BEFORE_CREATING_PROJECT2 = "20";

   @RBEntry("Impossibile creare il progetto: l'organizzazione  {0} non ha sottoscritto.")
   public static final String NON_SUBSCRIBING = "21";

   @RBEntry("Impossibile accedere al dominio {0}: privilegi insufficienti o dominio inesistente.")
   public static final String CANT_FIND_DOMAIN = "22";

   @RBEntry("L'utente non può modificare il gruppo Autori progetto.")
   public static final String CANT_MODIFY_PC_GROUP = "23";

   @RBEntry("L'utente non può modificare il gruppo Amministratori organizzazione.")
   public static final String CANT_MODIFY_OA_GROUP = "24";

   @RBEntry("Il mio blocco note")
   public static final String MY_NOTEBOOK = "25";

   @RBEntry("Il gruppo Autori progetto non esiste. Probabile errore durante l'installazione o la migrazione. Contattare l'amministratore di sistema.")
   public static final String GLOBAL_PROJ_CREATORS_GROUP_MISSING = "26";

   @RBEntry("Il nome di ruolo {0} non è valido. I nomi di ruolo non possono contenere i caratteri seguenti: {1}")
   public static final String INVALID_CHARACTERS_IN_ROLE_NAME = "27";

   @RBEntry("Il modello di progetto specificato per il progetto non è valido.")
   public static final String INVALID_PROJECT_TEMPLATE_SPECIFIED_FOR_PROJECT = "28";

   @RBEntry("Un progetto di nome {0} esiste già. Cambiare il nome del progetto e riprovare.")
   public static final String DUPLICATE_PROJECT_NAME = "29";

   @RBEntry(" Impossibile creare il progetto:  è necessario un modello di progetto.")
   public static final String PROJECT_TEMPLATE_REQUIRED = "30";

   @RBEntry("Autori progetto")
   public static final String PRIVATE_CONSTANT_0 = "CREATORS";

   @RBEntry("Tutti i membri")
   public static final String PRIVATE_CONSTANT_1 = "MEMBERS";

   @RBEntry("Impossibile creare il programma. L'utente non è membro del gruppo Autori programma.")
   @RBComment("The error message displayed if a non Program Creator attempts to create a Program.")
   public static final String ERROR_CANNOT_CREATE_PROGRAMS = "31";

   @RBEntry("Impossibile creare il programma. Manca il ruolo Responsabile di programma.")
   @RBComment("The error message displayed if PROGRAM MANAGER role is deleted from the configuration files")
   public static final String ERROR_CANNOT_CREATE_PROGRAMS_NO_PROGRAM_MANAGER_GROUP = "32";
}
