/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.projmgmt.ix;

import wt.util.resource.*;

@RBUUID("wt.projmgmt.ix.ProjMgmtIxResource")
public final class ProjMgmtIxResource extends WTListResourceBundle {
   @RBEntry("Export of {0} is not supported for this release.")
   public static final String EXPORT_NOT_SUPPORTED = "0";

   @RBEntry("Illegal XML element, expected: {0} found {1}.")
   public static final String ILLEGAL_TAG = "1";

   @RBEntry("Principal {0} does not exist.")
   public static final String PRINCIPAL_DOES_NOT_EXIST = "2";

   @RBEntry("Null project context")
   public static final String NULL_PRJ_CONTAINER = "3";

   @RBEntry("Illegal XML element, expected {0} or {1} found {2}.")
   public static final String ILLEGAL_TAG_CHOICE = "4";

   @RBEntry("Illegal XML")
   public static final String ILLEGAL_XML = "5";

   @RBEntry("Could not find ProjectConfig for ProjectTemplate import")
   public static final String PROJECT_CONFIG_NOT_AVAILABLE = "6";

   @RBEntry("Illegal ProjectTemplate XML: No template content specified")
   public static final String NO_PROJECT_TEMPLATE_SPEC = "7";

   @RBEntry("Warning, the user {0} does not exist in LDAP, assiging the person resource to {1}, you will need to re-assign or delete this resource")
   public static final String NO_LDAP_ENTRY = "8";

   @RBEntry("Illegal XML element, expected one of the following values {0} instead found found {1}.")
   public static final String ILLEGAL_TAG_CHOICES = "9";

   @RBEntry("The value of the <containerClassName> in the <ContainerTemplate> XML element cannot be null or an empty string")
   public static final String CONTAINER_CLASS_NAME_NEEDED = "10";

   @RBEntry("The passed in class name, {0} is not present in the system")
   public static final String CONTAINER_CLASS_NOT_THERE = "11";

   @RBEntry("{0} is not a WTContainer")
   public static final String CONTAINER_CLASS_NAME_NOT_A_WTCONTAINER = "12";

   @RBEntry("Context template must have a name")
   public static final String CONTAINER_TEMPLATE_NEEDS_A_NAME = "13";

   @RBEntry("Illegal XML: The tag {0} cannot be empty or contain only white space")
   public static final String XML_VALUE_CANNOT_BE_EMPTY = "14";

   @RBEntry("A Template of type {0} called \"{1}\" already exists in the context \"{2}\" of type \"{3}\".")
   public static final String USE_CREATE_TEMPLATE_UI_INSTEAD_OF_IMPORT = "15";

   @RBEntry("Context Template has already been persisted")
   public static final String CONTEXT_TEMPLATE_ALREADY_PERSISTED = "16";

   @RBEntry("Exported {0} of {1} objects")
   public static final String EXPORTED_N_OF_M_OBJECTS = "17";

   @RBEntry("Execution mode of imported plan is not compatible with current project.")
   public static final String INCOMPATIBLE_MODE = "18";

   @RBEntry("Program")
   public static final String WT_PROJMGMT_ADMIN_PROJECT2_PROGRAM = "19";

   @RBEntry("Unable to import classic project plan into this project. A classic project plan requires a classic project or classic project template to complete import.")
   public static final String INCOMPATIBLE_PLAN_FOR_PROJECT = "20";
}
