/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.projmgmt.ix;

import wt.util.resource.*;

@RBUUID("wt.projmgmt.ix.ProjMgmtIxResource")
public final class ProjMgmtIxResource_it extends WTListResourceBundle {
   @RBEntry("L'esportazione di {0} non è supportata per questa release.")
   public static final String EXPORT_NOT_SUPPORTED = "0";

   @RBEntry("Elemento XML non valido. Atteso {0}. Trovato {1}.")
   public static final String ILLEGAL_TAG = "1";

   @RBEntry("L'utente/gruppo/ruolo {0} è inesistente")
   public static final String PRINCIPAL_DOES_NOT_EXIST = "2";

   @RBEntry("Contesto progetto nullo")
   public static final String NULL_PRJ_CONTAINER = "3";

   @RBEntry("Elemento XML non valido. Atteso {0} o {1}. Trovato {2}.")
   public static final String ILLEGAL_TAG_CHOICE = "4";

   @RBEntry("XML non valido")
   public static final String ILLEGAL_XML = "5";

   @RBEntry("Impossibile trovare la configurazione di progetto per l'importazione del modello di progetto")
   public static final String PROJECT_CONFIG_NOT_AVAILABLE = "6";

   @RBEntry("XML ProjectTemplate non valido: non è stato specificato alcun contenuto di modello")
   public static final String NO_PROJECT_TEMPLATE_SPEC = "7";

   @RBEntry("Attenzione, l'utente {0} non esiste nella LDAP. L'assegnatario viene impostato a {1}. L'utente deve riassegnare o eliminare la risorsa.")
   public static final String NO_LDAP_ENTRY = "8";

   @RBEntry("Elemento XML non valido. Atteso: {0}. Trovato: {1}.")
   public static final String ILLEGAL_TAG_CHOICES = "9";

   @RBEntry("Il valore di <containerClassName> nell'elemento XML di <ContainerTemplate> non può essere nullo.")
   public static final String CONTAINER_CLASS_NAME_NEEDED = "10";

   @RBEntry("Il nome di classe trasmesso {0} non è presente nel sistema")
   public static final String CONTAINER_CLASS_NOT_THERE = "11";

   @RBEntry("{0} non è un WTContainer")
   public static final String CONTAINER_CLASS_NAME_NOT_A_WTCONTAINER = "12";

   @RBEntry("Il modello di contesto deve avere un nome")
   public static final String CONTAINER_TEMPLATE_NEEDS_A_NAME = "13";

   @RBEntry("XML non valido: il tag {0} non può essere vuoto o contenere solo spazi.")
   public static final String XML_VALUE_CANNOT_BE_EMPTY = "14";

   @RBEntry("Un modello di tipo {0} denominato \"{1}\" è già presente nel contesto \"{2}\" di tipo \"{3}\".")
   public static final String USE_CREATE_TEMPLATE_UI_INSTEAD_OF_IMPORT = "15";

   @RBEntry("Il modello di contesto è già stato reso persistente")
   public static final String CONTEXT_TEMPLATE_ALREADY_PERSISTED = "16";

   @RBEntry("Sono stati esportati {0} dei {1} oggetti")
   public static final String EXPORTED_N_OF_M_OBJECTS = "17";

   @RBEntry("La modalità di esecuzione del piano importato non è compatibile con il progetto corrente.")
   public static final String INCOMPATIBLE_MODE = "18";

   @RBEntry("Programma")
   public static final String WT_PROJMGMT_ADMIN_PROJECT2_PROGRAM = "19";

   @RBEntry("Impossibile importare un piano di progetto classico in questo progetto. Un piano di progetto classico richiede un progetto classico o un modello di progetto classico per completare l'importazione.")
   public static final String INCOMPATIBLE_PLAN_FOR_PROJECT = "20";
}
