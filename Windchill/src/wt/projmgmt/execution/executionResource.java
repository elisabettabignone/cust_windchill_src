/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.projmgmt.execution;

import wt.util.resource.*;

@RBUUID("wt.projmgmt.execution.executionResource")
public final class executionResource extends WTListResourceBundle {
   @RBEntry("Project")
   @RBComment("Project's plan name.")
   public static final String PROJECT_NAME = "0";

   @RBEntry("Activity")
   @RBComment("Name of a component of a project plan that involves work, also known as \"task.\"")
   public static final String ACTIVITY_NAME = "1";

   @RBEntry("Milestone")
   @RBComment("Name of a notable point in the execution of a project.")
   public static final String MILESTONE_NAME = "2";

   @RBEntry("Can't add summary: a cycle is created.")
   @RBComment("Text of the exception thrown when an atempt is made to add a summary activity such that a containment cycle is created (A contains B that contains A, for example).")
   public static final String CONTAINMENT_CYCLE_FOUND = "3";

   @RBEntry("Can't set sub project/program: a cycle is created.")
   @RBComment("Text of the exception thrown when an atempt is made to set a sub project/program that would cause a sub project/program cycle to created (A references B that references A, for example).")
   public static final String SUB_PROJECT_CYCLE_FOUND = "4";

   @RBEntry("Can't make {1} depend on {0} as it would create a cycle.")
   @RBComment("Text of the exception thrown when an atempt is made to create a dependency that causes a cycle (e.g. A depends on B that depends on A).")
   public static final String CANT_CREATE_DEPENDENCY = "5";

   @RBEntry("{0} already has an associated deliverable.")
   @RBComment("Error message issued when activity already has an associated deliverable and the \"link deliverables\" option is in force.")
   public static final String SINGLE_DELIVERABLE = "6";

   @RBEntry("{0} already has an associated activity ({1}).")
   @RBComment("Error message issued when deliverable already has an associated activity and the user tries to associate another activity to the deliverable and the \"link deliverables\" option is in force.")
   public static final String HAS_ASSOCIATED_ACTIVITY = "7";

   @RBEntry("This operation is valid if only \"link deliverables\" option is selected.")
   @RBComment("Error message issued when user attempts to perform an operation that is only valid if deliverables are linked to activities.")
   public static final String INVALID_LINK_OPERATION = "8";

   @RBEntry("{0} - {1} Task")
   @RBComment("Task notification email message subject.")
   public static final String TASK_NOTIFICATION_SUBJECT = "9";
}
