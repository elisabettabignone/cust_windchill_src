/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.projmgmt.msproject;

import wt.util.resource.*;

@RBUUID("wt.projmgmt.msproject.msprojectResource")
public final class msprojectResource extends WTListResourceBundle {
   /**
    * Entry Content
    * #########Exception Handling Messages
    **/
   @RBEntry("UID exceeded Microsoft Project limit of:")
   public static final String UID_EXCEEDED_LIMIT = "0";

   @RBEntry("Cannot import into {0}, because it is in {1}.")
   public static final String CANNOT_IMPORT_INTO = "1";

   @RBEntry("Container is missing")
   public static final String MISSING_CONTAINER = "2";

   @RBEntry("Import file is missing")
   public static final String MISSING_IMPORT_FILE = "3";

   @RBEntry("Import action is missing")
   public static final String MISSING_IMPORT_ACTION = "4";

   @RBEntry("Internal Error: unsupported type:{0}")
   public static final String INTERNAL_UNSUPPORTED_TYPE = "6";

   @RBEntry("Resource units are out of range:")
   public static final String RESOURCE_UNITS_OUT_RANGE = "7";

   @RBEntry("Unsupported project node type in assignment:{0}")
   public static final String UNSUPPORTED_PROJECT_NODE = "8";

   @RBEntry("Too many assignments found.")
   public static final String FOUND_TOO_MANY_ASSI = "9";

   @RBEntry("Cannot set the plan of a plan")
   public static final String CANNOT_SET_THEPLAN = "10";

   @RBEntry("ID Offset has not been computed yet")
   public static final String ID_OFFSET_NOT_COMPUTED = "11";

   @RBEntry("Internal Error: project plan cannot be null.")
   public static final String PROJECT_PLAN_CANNOT_BE_NULL = "12";

   @RBEntry("Tasks have already been set.")
   public static final String TASK_SET = "13";

   @RBEntry("Resources have already been set.")
   public static final String RESOURCE_SET = "14";

   @RBEntry("Internal Error: task stack is empty")
   public static final String TASK_STACK_IS_EMPTY = "15";

   @RBEntry("Internal Error: task {0} is the plan summary task.")
   public static final String PLAN_SUMMARY_TASK = "16";

   @RBEntry("Internal Error: unsupported subclass")
   public static final String UNSUPPORTED_SUBCLASS = "17";

   @RBEntry("Percentage is out of range")
   public static final String PERCENTAGE_OUT = "18";

   @RBEntry("Parent cannot be null")
   public static final String PARENT_NULL = "19";

   @RBEntry("Task Status description for \"{0}\" exceeds the upper limit of 255 characters")
   public static final String LENGTHY_TASK_STATUS_DESCRIPTION = "20";

   @RBEntry("Task Risk description for \"{0}\" exceeds the upper limit of 255 characters")
   public static final String LENGTHY_TASK_RISK_DESCRIPTION = "21";
}
