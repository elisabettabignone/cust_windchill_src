/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.projmgmt.msproject;

import wt.util.resource.*;

@RBUUID("wt.projmgmt.msproject.msprojectResource")
public final class msprojectResource_it extends WTListResourceBundle {
   /**
    * Entry Content
    * #########Exception Handling Messages
    **/
   @RBEntry("L'UID supera il limite di Microsoft Project di:")
   public static final String UID_EXCEEDED_LIMIT = "0";

   @RBEntry("Impossibile importare in {0}, perché è in {1}.")
   public static final String CANNOT_IMPORT_INTO = "1";

   @RBEntry("Contenitore mancante")
   public static final String MISSING_CONTAINER = "2";

   @RBEntry("File d'importazione mancante")
   public static final String MISSING_IMPORT_FILE = "3";

   @RBEntry("Azione d'importazione mancante")
   public static final String MISSING_IMPORT_ACTION = "4";

   @RBEntry("Errore interno: tipo non supportato:{0}")
   public static final String INTERNAL_UNSUPPORTED_TYPE = "6";

   @RBEntry("Le unità di risorsa non rientrano nell'intervallo:")
   public static final String RESOURCE_UNITS_OUT_RANGE = "7";

   @RBEntry("Il tipo di nodo del progetto non è supportato nell'assegnazione:{0}")
   public static final String UNSUPPORTED_PROJECT_NODE = "8";

   @RBEntry("Rilevate troppe assegnazioni.")
   public static final String FOUND_TOO_MANY_ASSI = "9";

   @RBEntry("Non è possibile impostare il piano di un piano")
   public static final String CANNOT_SET_THEPLAN = "10";

   @RBEntry("L'offset dell'ID non è stato ancora calcolato")
   public static final String ID_OFFSET_NOT_COMPUTED = "11";

   @RBEntry("Errore interno: il piano del progetto non può essere nullo.")
   public static final String PROJECT_PLAN_CANNOT_BE_NULL = "12";

   @RBEntry("I task sono già stati impostati.")
   public static final String TASK_SET = "13";

   @RBEntry("Le risorse sono già state impostate.")
   public static final String RESOURCE_SET = "14";

   @RBEntry("Errore interno: lo stack dei task è vuoto")
   public static final String TASK_STACK_IS_EMPTY = "15";

   @RBEntry("Errore interno: il task {0} è il riepilogo del piano.")
   public static final String PLAN_SUMMARY_TASK = "16";

   @RBEntry("Errore interno: sottoclasse non supportata")
   public static final String UNSUPPORTED_SUBCLASS = "17";

   @RBEntry("La percentuale non rientra nell'intervallo")
   public static final String PERCENTAGE_OUT = "18";

   @RBEntry("L'attributo padre non può essere nullo.")
   public static final String PARENT_NULL = "19";

   @RBEntry("La descrizione dello stato del task per \"{0}\" supera il limite di 255 caratteri")
   public static final String LENGTHY_TASK_STATUS_DESCRIPTION = "20";

   @RBEntry("La descrizione del rischio del task per \"{0}\" supera il limite di 255 caratteri")
   public static final String LENGTHY_TASK_RISK_DESCRIPTION = "21";
}
