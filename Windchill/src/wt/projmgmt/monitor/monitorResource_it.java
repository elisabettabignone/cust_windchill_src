/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.projmgmt.monitor;

import wt.util.resource.*;

@RBUUID("wt.projmgmt.monitor.monitorResource")
public final class monitorResource_it extends WTListResourceBundle {
   @RBEntry("Data di scadenza mancata per {0}.")
   @RBComment("Email subject that is sent when a deadline is missed.")
   public static final String MISSED_DEADLINE_SUBJECT = "0";

   @RBEntry("Data di scadenza per {0} in avvicinamento.")
   @RBComment("Email subject that is sent when a deadline is approaching.")
   public static final String APPROACHING_DEADLINE_SUBJECT = "1";

   @RBEntry("Data di scadenza trascorsa per {0}.")
   @RBComment("Email subject that is sent after a deadline has already passed.")
   public static final String PASSED_DEADLINE_SUBJECT = "2";

   @RBEntry("Notifica data di scadenza")
   @RBComment("Header of the message for deadline related email.")
   public static final String DEADLINE_NOTIFICATION_TITLE = "3";

   @RBEntry("Data di scadenza:")
   @RBComment("Label of the row describing the deadline.")
   public static final String DEADLINE_LABEL = "4";

   @RBEntry("{0} è iniziato.")
   @RBComment("Subject of the email that is sent when a project, activity or deliverable starts.")
   public static final String START_TRANSITION_SUBJECT = "5";

   @RBEntry("{0} è stato sospeso.")
   @RBComment("Subject of the email that is sent when a project, activity or deliverable is suspended.")
   public static final String SUSPEND_TRANSITION_SUBJECT = "6";

   @RBEntry("{0} è stato ripreso")
   @RBComment("Subject of the email that is sent when a project, activity or deliverable is resumed.")
   public static final String RESUME_TRANSITION_SUBJECT = "7";

   @RBEntry("{0} è stato completato")
   @RBComment("Subject of the email that is sent when a project, activity or deliverable completes successfully.")
   public static final String COMPLETE_TRANSITION_SUBJECT = "8";

   @RBEntry("{0} è stato annullato.")
   @RBComment("Subject of the email that is sent when a project, activity or deliverable is cancelled.")
   public static final String CANCEL_TRANSITION_SUBJECT = "9";

   @RBEntry("È stato modificato il proprietario di {0}")
   @RBComment("Subject of the email that is sent when the owner of an activity or deliverable is changed.")
   public static final String OWNER_CHANGE_SUBJECT = "10";

   @RBEntry("È stato modificato lo stato per {0}")
   @RBComment("Subject of the email that is sent when the deliverable, activity or project status is modified.")
   public static final String STATUS_CHANGE_SUBJECT = "11";

   @RBEntry("È stata modificata la data di scadenza per {0}")
   @RBComment("Subject of the email that is sent the deliverable, activity or project deadline is modified.")
   public static final String DEADLINE_CHANGE_SUBJECT = "12";

   @RBEntry("Notifica di modifica di stato del ciclo di vita")
   @RBComment("Header of the message for state transition email.")
   public static final String STATE_CHANGE_NOTIFICATION_TITLE = "13";

   @RBEntry("Stato precedente del ciclo di vita:")
   @RBComment("Label of the row describing the old state.")
   public static final String OLD_STATE_LABEL = "14";

   @RBEntry("Nuovo stato del ciclo di vita:")
   @RBComment("Label of the row describing the new state.")
   public static final String NEW_STATE_LABEL = "15";

   @RBEntry("Notifica di modifica data di scadenza")
   @RBComment("Header of the message for state transition email.")
   public static final String DEADLINE_CHANGE_NOTIFICATION_TITLE = "16";

   @RBEntry("Data di scadenza precedente:")
   @RBComment("Label of the row describing the old deadline.")
   public static final String OLD_DEADLINE_LABEL = "17";

   @RBEntry("Nuova data di scadenza:")
   @RBComment("Label of the row describing the new deadline.")
   public static final String NEW_DEADLINE_LABEL = "18";

   @RBEntry("Notifica di modifica di stato")
   @RBComment("Header of the message for state transition email.")
   public static final String STATUS_CHANGE_NOTIFICATION_TITLE = "19";

   @RBEntry("Stato precedente:")
   @RBComment("Label of the row containing the old status.")
   public static final String OLD_STATUS_LABEL = "20";

   @RBEntry("Stato:")
   @RBComment("Label of the row containing the new status.")
   public static final String STATUS_LABEL = "22";

   @RBEntry("Percentuale completata:")
   @RBComment("Label of the row describing the new completion percentages.")
   public static final String PERCENT_COMPLETE_LABEL = "23";

   @RBEntry("Fine stimata:")
   @RBComment("Label of the row describing the new estimated finish times.")
   public static final String ESTIMATED_FINISH_LABEL = "24";

   @RBEntry("Notifica di modifica di proprietario")
   @RBComment("Header of the message for state transition email.")
   public static final String OWNER_CHANGE_NOTIFICATION_TITLE = "25";

   @RBEntry("Proprietario:")
   @RBComment("Label of the row containing new owner.")
   public static final String OWNER_LABEL = "26";

   @RBEntry("La percentuale completata è stata modificata per {0}")
   @RBComment("Subject of the email that is sent when the deliverable, activity or project complete percentage is modified.")
   public static final String PERCENT_CHANGE_SUBJECT = "27";

   @RBEntry("La data prevista per il completamento è stata modificata per  {0}")
   @RBComment("Subject of the email that is sent when the deliverable, activity or project estimated finish is modified.")
   public static final String FINISH_CHANGE_SUBJECT = "28";

   @RBEntry("{0} è stato eliminato.")
   @RBComment("Subject of the email that is sent when the deliverable, activity or project is deleted.")
   public static final String OBJECT_DELETION_SUBJECT = "29";

   @RBEntry("{0} non è più in esecuzione.")
   @RBComment("Subject of the email that is sent when a project, activity or deliverable stops.")
   public static final String STOP_TRANSITION_SUBJECT = "30";

   @RBEntry("Lo stato di {0} è rosso.")
   @RBComment("Subject of the email that is sent when the status of a project, activity or deliverable turns red.")
   public static final String STATUS_TURNS_RED_SUBJECT = "31";

   @RBEntry("ATTENZIONE: azione protetta. Non si dispone dell'autorizzazione a modificare {1} in {2} (operazione consentita solo ai project manager).")
   @RBComment("User lacks privileges to change a deadline or owner attribute of the project, activity or deliverable. ")
   public static final String CANT_CHANGE_ATTRIBUTE = "32";

   @RBEntry("Data di scadenza")
   @RBComment("Name of deadline attribute of plan, activity, milestone or deliverable used in 'CANT_CHANGE_ATTRIBUTE' error message.")
   public static final String DEADLINE_ATTRIBUTE_NAME = "34";

   @RBEntry("Proprietario")
   @RBComment("Name of the owner attribute of plan, activity, milestone or deliverable used in 'CANT_CHANGE_ATTRIBUTE' error message.")
   public static final String OWNER_ATTRIBUTE_NAME = "35";

   @RBEntry("Eliminazione oggetto")
   @RBComment("Header of the message for email notifying deletion of activity or deriverable.")
   public static final String OBJECT_DELETION_NOTIFICATION_TITLE = "36";

   @RBEntry("La modifica di {0} non è consentita perché è completato o eliminato.")
   public static final String CANT_EDIT_CLOSED_OBJECT = "37";

   @RBEntry("La modifica di {0} non è consentita perché non è in esecuzione.")
   public static final String CANT_CHANGE_NOT_RUNNING_OBJECT = "38";

   @RBEntry("Progetto:")
   public static final String PROJECT_NAME_LABEL = "39";

   @RBEntry("Attività:")
   public static final String ACTIVITY_NAME_LABEL = "40";

   @RBEntry("Fase cardine:")
   public static final String MILESTONE_NAME_LABEL = "41";

   @RBEntry("Risultato finale")
   public static final String DELIVERABLE_NAME_LABEL = "42";

   @RBEntry("Inizio stimato")
   @RBComment("Name of the 'time to start' attribute of activity or milestone.")
   public static final String START_TIME_ATTRIBUTE_NAME = "43";

   @RBEntry("Non sono consentite modifiche per {0} perché {1} non è in esecuzione.")
   public static final String CANT_CHANGE_PROJECT_NOT_RUNNING = "44";

   @RBEntry("Non sono consentite modifiche per {0}, perché è già stato avviato.")
   public static final String CANT_CHANGE_OBJECT_ALREADY_STARTED = "45";

   @RBEntry("L'ora d'inizio di {0} non può essere precedente a {1}.")
   @RBComment("Error message issued when the user changes time to start to a date that is not feasible.")
   public static final String NOT_FEASIBLE_START = "46";

   @RBEntry("Impossibile aggiungere il nodo già iniziato {0} al riepilogo non iniziato {1}.")
   @RBComment("Error message issued when the user tries to add to a summary in the DEFINED state a node that has already started.")
   public static final String CANT_ADD_TO_SUMMARY = "47";

   @RBEntry("Impossibile aggiungere il nodo {0} perché dipende dal riepilogo {1}.")
   @RBComment("Error message issued when the user tries to add a node to a summary and there is a dependency relationship between the two.")
   public static final String CANT_ADD_TO_SUMMARY_01 = "48";

   @RBEntry("Attività di riepilogo:")
   public static final String SUMMARY_ACTIVITY_NAME_LABEL = "49";

   @RBEntry("Impossibile impostare la data di scadenza di {0} perché è successiva alla data di scadenza ereditata.")
   @RBComment("Error message issued when the user tries to set a deadline that conflicts with a more global deadline (for example, setting the deadline past the project's deadline).")
   public static final String DEADLINE_PAST_INHERITED = "50";

   @RBEntry("Impossibile cambiare l'ora di fine perché {0} non è chiuso.")
   @RBComment("Error message issued when the user tries to change the end time of an object that is still running.")
   public static final String OBJECT_NOT_CLOSED = "51";

   @RBEntry("Impossibile cambiare l'ora di fine perché {0} è iniziato dopo l'ora stabilita.")
   @RBComment("Error message issued when the user tries to change the end time of an object to a time earlier than the start time.")
   public static final String TIME_EARLIER_THAN_START = "52";

   @RBEntry("Impossibile cambiare l'ora di fine perché {0} è terminato prima dell'ora stabilita.")
   @RBComment("Error message issued when the user tries to change the end time of an object to a time that is past the end time of the object (end times can be set to eralier times never later).")
   public static final String TIME_AFTER_START = "53";

   @RBEntry("momento di completamento")
   @RBComment("Name of the attribute of activity, milestone or deliverable used in 'CANT_CHANGE_ATTRIBUTE' error message")
   public static final String COMPLETION_TIME = "54";

   @RBEntry("{0} è stato eliminato.")
   @RBComment("Error message issued when the user tries to perform an operation on a deleted object.")
   public static final String DELETED_OBJECT = "55";

   @RBEntry("Impossibile inserire {0} dopo se stesso.")
   @RBComment("Error message issued when the user tries to insert a node after itself.")
   public static final String CANT_INSERT_AFTER_ITSELF = "56";

   @RBEntry("Impossibile inserire {0} prima di se stesso.")
   @RBComment("Error message issued when the user tries to insert a node before itself.")
   public static final String CANT_INSERT_BEFORE_ITSELF = "57";

   @RBEntry("Impossibile inserire {0} dopo {1} perché {0} contiene {1}.")
   @RBComment("Error message issued when the user tries to insert a node after a contained node.")
   public static final String CANT_INSERT_AFTER_CONTAINED = "58";

   @RBEntry("Impossibile inserire {0} prima di {1} perché {0} contiene {1}.")
   @RBComment("Error message issued when the user tries to insert a node before a contained node.")
   public static final String CANT_INSERT_BEFORE_CONTAINED = "59";

   @RBEntry("La percentuale di completamento non può essere superiore a 100.")
   @RBComment("Error message issued when there is an attempt to set the percent complete to a value greater than 100 (usually through roll up).")
   public static final String INVALID_PERCENT = "60";

   @RBEntry("Sottoprogetto/sottoprogramma:")
   public static final String PROXY_NAME_LABEL = "61";

   @RBEntry("La durata è superiore al valore massimo (personalizzabile) di {0} giorni.")
   @RBComment("Error message issued when the user tries to set or change the duration of a node (activity, etc) to a value larger than the maximum allowed.  This limit is customizable.")
   public static final String DURATION_TOO_LARGE = "62";

   @RBEntry("È necessario assegnare un valore all'ora di fine.")
   @RBComment("Error message issued when the user tries to change an object's finish time to a null value.")
   public static final String CANT_RESTART = "63";

   @RBEntry("Impossibile modificare il completamento di {0} perchè non è iniziato.")
   @RBComment("Error message issued when the user tries to change the finish time of an object that hasn't started yet.")
   public static final String CANT_CHANGE_FINISH = "64";

   @RBEntry("Impossibile modificare il completamento di {0} a un orario precedente all'inizio.")
   @RBComment("Error message issued when the user tries to change the finish time of an object to a time that precedes its start.")
   public static final String FINISH_BEFORE_START = "65";

   @RBEntry("Impossibile modificare il completamento di {0} a un orario che ne segue la fine.")
   @RBComment("Error message issued when the user tries to change the start time of an object to a time that is after its finish.")
   public static final String START_AFTER_FINISH = "66";

   @RBEntry("Notifica al Project Manager")
   @RBComment("To be used in conjunction with email notification subjects to denote whether the email is sent to Owner or Project Manager")
   public static final String MANAGER = "67";

   @RBEntry("Notifica al proprietario")
   @RBComment("To be used in conjunction with email notification subjects to denote whether the email is sent to Owner or Project Manager")
   public static final String OWNER = "68";

   @RBEntry("{0} ({1})")
   @RBComment("combination of event and notification subscriber (usually Object Owner or Project Manager).  Example: Missed Deadline for activity1 [Owner notification] ")
   public static final String SUBJECT_EVENT_SUBSCRIBER = "69";

   @RBEntry("Stato del ciclo di vita:")
   @RBComment("A label displayed in notification email")
   public static final String STATE_LABEL = "90";

   @RBEntry("Budget:")
   @RBComment("A label displayed in notification email")
   public static final String BUDGET_LABEL = "91";

   @RBEntry("Divisione:")
   @RBComment("A label displayed in notification email")
   public static final String BUSINESS_UNIT_LABEL = "92";

   @RBEntry("Categoria:")
   @RBComment("A label displayed in notification email")
   public static final String CATEGORY_LABEL = "93";

   @RBEntry("Descrizione situazione:")
   @RBComment("A label displayed in notification email")
   public static final String HEALTH_DESCRIPTION_LABEL = "94";

   @RBEntry("Situazione:")
   @RBComment("A label displayed in notification email")
   public static final String HEALTH_STATUS_LABEL = "95";

   @RBEntry("Fase:")
   @RBComment("A label displayed in notification email")
   public static final String PHASE_LABEL = "96";

   @RBEntry("Priorità:")
   @RBComment("A label displayed in notification email")
   public static final String PRIORITY_LABEL = "97";

   @RBEntry("Numero progetto:")
   @RBComment("A label displayed in notification email")
   public static final String PROJECT_NUMBER_LABEL = "98";

   @RBEntry("Descrizione del rischio:")
   @RBComment("A label displayed in notification email")
   public static final String RISK_DESCRIPTION_LABEL = "99";

   @RBEntry("Rischio:")
   @RBComment("A label displayed in notification email")
   public static final String RISK_VALUE_LABEL = "100";

   @RBEntry("Sito:")
   @RBComment("A label displayed in notification email")
   public static final String SITE_LABEL = "101";

   @RBEntry("Inizio stimato:")
   @RBComment("A label displayed in notification email")
   public static final String START_DATE_LABEL = "102";

   @RBEntry("dd/MM/yyyy")
   @RBComment("Date format that appears in notification email (2006/08/16) (It is used as a computational string so the letters should only be rearranged)")
   public static final String NOTICATION_DATE_FORMAT = "103";
}
