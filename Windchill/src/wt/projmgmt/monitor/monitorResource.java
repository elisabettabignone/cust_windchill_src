/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.projmgmt.monitor;

import wt.util.resource.*;

@RBUUID("wt.projmgmt.monitor.monitorResource")
public final class monitorResource extends WTListResourceBundle {
   @RBEntry("Missed due date for {0}.")
   @RBComment("Email subject that is sent when a deadline is missed.")
   public static final String MISSED_DEADLINE_SUBJECT = "0";

   @RBEntry("Approaching due date for {0}.")
   @RBComment("Email subject that is sent when a deadline is approaching.")
   public static final String APPROACHING_DEADLINE_SUBJECT = "1";

   @RBEntry("Passed due date for {0}.")
   @RBComment("Email subject that is sent after a deadline has already passed.")
   public static final String PASSED_DEADLINE_SUBJECT = "2";

   @RBEntry("Due Date Notification")
   @RBComment("Header of the message for deadline related email.")
   public static final String DEADLINE_NOTIFICATION_TITLE = "3";

   @RBEntry("Due Date:")
   @RBComment("Label of the row describing the deadline.")
   public static final String DEADLINE_LABEL = "4";

   @RBEntry("{0} started.")
   @RBComment("Subject of the email that is sent when a project, activity or deliverable starts.")
   public static final String START_TRANSITION_SUBJECT = "5";

   @RBEntry("{0} was suspended.")
   @RBComment("Subject of the email that is sent when a project, activity or deliverable is suspended.")
   public static final String SUSPEND_TRANSITION_SUBJECT = "6";

   @RBEntry("{0} was resumed.")
   @RBComment("Subject of the email that is sent when a project, activity or deliverable is resumed.")
   public static final String RESUME_TRANSITION_SUBJECT = "7";

   @RBEntry("{0} was completed.")
   @RBComment("Subject of the email that is sent when a project, activity or deliverable completes successfully.")
   public static final String COMPLETE_TRANSITION_SUBJECT = "8";

   @RBEntry("{0} was canceled.")
   @RBComment("Subject of the email that is sent when a project, activity or deliverable is cancelled.")
   public static final String CANCEL_TRANSITION_SUBJECT = "9";

   @RBEntry("Owner changed for {0}")
   @RBComment("Subject of the email that is sent when the owner of an activity or deliverable is changed.")
   public static final String OWNER_CHANGE_SUBJECT = "10";

   @RBEntry("Status was changed for {0}")
   @RBComment("Subject of the email that is sent when the deliverable, activity or project status is modified.")
   public static final String STATUS_CHANGE_SUBJECT = "11";

   @RBEntry("Due date was changed for {0}")
   @RBComment("Subject of the email that is sent the deliverable, activity or project deadline is modified.")
   public static final String DEADLINE_CHANGE_SUBJECT = "12";

   @RBEntry("State Change Notification")
   @RBComment("Header of the message for state transition email.")
   public static final String STATE_CHANGE_NOTIFICATION_TITLE = "13";

   @RBEntry("Old State:")
   @RBComment("Label of the row describing the old state.")
   public static final String OLD_STATE_LABEL = "14";

   @RBEntry("New State:")
   @RBComment("Label of the row describing the new state.")
   public static final String NEW_STATE_LABEL = "15";

   @RBEntry("Due Date Change Notification")
   @RBComment("Header of the message for state transition email.")
   public static final String DEADLINE_CHANGE_NOTIFICATION_TITLE = "16";

   @RBEntry("Old Due Date:")
   @RBComment("Label of the row describing the old deadline.")
   public static final String OLD_DEADLINE_LABEL = "17";

   @RBEntry("New Due Date:")
   @RBComment("Label of the row describing the new deadline.")
   public static final String NEW_DEADLINE_LABEL = "18";

   @RBEntry("Status Change Notification")
   @RBComment("Header of the message for state transition email.")
   public static final String STATUS_CHANGE_NOTIFICATION_TITLE = "19";

   @RBEntry("Old Status:")
   @RBComment("Label of the row containing the old status.")
   public static final String OLD_STATUS_LABEL = "20";

   @RBEntry("Status:")
   @RBComment("Label of the row containing the new status.")
   public static final String STATUS_LABEL = "22";

   @RBEntry("Percent Complete:")
   @RBComment("Label of the row describing the new completion percentages.")
   public static final String PERCENT_COMPLETE_LABEL = "23";

   @RBEntry("Estimated Finish:")
   @RBComment("Label of the row describing the new estimated finish times.")
   public static final String ESTIMATED_FINISH_LABEL = "24";

   @RBEntry("Owner Change Notification")
   @RBComment("Header of the message for state transition email.")
   public static final String OWNER_CHANGE_NOTIFICATION_TITLE = "25";

   @RBEntry("Owner:")
   @RBComment("Label of the row containing new owner.")
   public static final String OWNER_LABEL = "26";

   @RBEntry("Percent complete was changed for {0}")
   @RBComment("Subject of the email that is sent when the deliverable, activity or project complete percentage is modified.")
   public static final String PERCENT_CHANGE_SUBJECT = "27";

   @RBEntry("Estimated finish date was changed for {0}")
   @RBComment("Subject of the email that is sent when the deliverable, activity or project estimated finish is modified.")
   public static final String FINISH_CHANGE_SUBJECT = "28";

   @RBEntry("{0} was deleted")
   @RBComment("Subject of the email that is sent when the deliverable, activity or project is deleted.")
   public static final String OBJECT_DELETION_SUBJECT = "29";

   @RBEntry("{0} no longer running.")
   @RBComment("Subject of the email that is sent when a project, activity or deliverable stops.")
   public static final String STOP_TRANSITION_SUBJECT = "30";

   @RBEntry("Status of {0} turned red.")
   @RBComment("Subject of the email that is sent when the status of a project, activity or deliverable turns red.")
   public static final String STATUS_TURNS_RED_SUBJECT = "31";

   @RBEntry("ATTENTION: Secured Action.  You are not authorized to change {1} in {2} (operation restricted to project managers).")
   @RBComment("User lacks privileges to change a deadline or owner attribute of the project, activity or deliverable. ")
   public static final String CANT_CHANGE_ATTRIBUTE = "32";

   @RBEntry("Due Date")
   @RBComment("Name of deadline attribute of plan, activity, milestone or deliverable used in 'CANT_CHANGE_ATTRIBUTE' error message.")
   public static final String DEADLINE_ATTRIBUTE_NAME = "34";

   @RBEntry("owner")
   @RBComment("Name of the owner attribute of plan, activity, milestone or deliverable used in 'CANT_CHANGE_ATTRIBUTE' error message.")
   public static final String OWNER_ATTRIBUTE_NAME = "35";

   @RBEntry("Object Deletion")
   @RBComment("Header of the message for email notifying deletion of activity or deriverable.")
   public static final String OBJECT_DELETION_NOTIFICATION_TITLE = "36";

   @RBEntry("No editing allowed for {0} because it is completed or cancelled.")
   public static final String CANT_EDIT_CLOSED_OBJECT = "37";

   @RBEntry("No change allowed for {0} because it is not running.")
   public static final String CANT_CHANGE_NOT_RUNNING_OBJECT = "38";

   @RBEntry("Project:")
   public static final String PROJECT_NAME_LABEL = "39";

   @RBEntry("Activity:")
   public static final String ACTIVITY_NAME_LABEL = "40";

   @RBEntry("Milestone:")
   public static final String MILESTONE_NAME_LABEL = "41";

   @RBEntry("Deliverable:")
   public static final String DELIVERABLE_NAME_LABEL = "42";

   @RBEntry("Estimated Start")
   @RBComment("Name of the 'time to start' attribute of activity or milestone.")
   public static final String START_TIME_ATTRIBUTE_NAME = "43";

   @RBEntry("No change allowed for {0} because {1} is not running.")
   public static final String CANT_CHANGE_PROJECT_NOT_RUNNING = "44";

   @RBEntry("No change allowed for {0} it has already started.")
   public static final String CANT_CHANGE_OBJECT_ALREADY_STARTED = "45";

   @RBEntry("Start time for {0} can't be earlier than {1}.")
   @RBComment("Error message issued when the user changes time to start to a date that is not feasible.")
   public static final String NOT_FEASIBLE_START = "46";

   @RBEntry("Can't add already-started node {0} to non-started sumamry {1}.")
   @RBComment("Error message issued when the user tries to add to a summary in the DEFINED state a node that has already started.")
   public static final String CANT_ADD_TO_SUMMARY = "47";

   @RBEntry("Can't add node {0} because it is dependent/depends on summary {1}.")
   @RBComment("Error message issued when the user tries to add a node to a summary and there is a dependency relationship between the two.")
   public static final String CANT_ADD_TO_SUMMARY_01 = "48";

   @RBEntry("Summary Activity:")
   public static final String SUMMARY_ACTIVITY_NAME_LABEL = "49";

   @RBEntry("Due date of {0} can't be set because it is past the inherited due date.")
   @RBComment("Error message issued when the user tries to set a deadline that conflicts with a more global deadline (for example, setting the deadline past the project's deadline).")
   public static final String DEADLINE_PAST_INHERITED = "50";

   @RBEntry("Can't change end time because {0} is not closed.")
   @RBComment("Error message issued when the user tries to change the end time of an object that is still running.")
   public static final String OBJECT_NOT_CLOSED = "51";

   @RBEntry("Can't change end time because {0} started after given time.")
   @RBComment("Error message issued when the user tries to change the end time of an object to a time earlier than the start time.")
   public static final String TIME_EARLIER_THAN_START = "52";

   @RBEntry("Can't change end time because {0} ended before given time.")
   @RBComment("Error message issued when the user tries to change the end time of an object to a time that is past the end time of the object (end times can be set to eralier times never later).")
   public static final String TIME_AFTER_START = "53";

   @RBEntry("completion time")
   @RBComment("Name of the attribute of activity, milestone or deliverable used in 'CANT_CHANGE_ATTRIBUTE' error message")
   public static final String COMPLETION_TIME = "54";

   @RBEntry("{0} has been deleted.")
   @RBComment("Error message issued when the user tries to perform an operation on a deleted object.")
   public static final String DELETED_OBJECT = "55";

   @RBEntry("Can't insert {0} after itself.")
   @RBComment("Error message issued when the user tries to insert a node after itself.")
   public static final String CANT_INSERT_AFTER_ITSELF = "56";

   @RBEntry("Can't insert {0} before itself.")
   @RBComment("Error message issued when the user tries to insert a node before itself.")
   public static final String CANT_INSERT_BEFORE_ITSELF = "57";

   @RBEntry("Can't insert {0} after {1} because {0} contains {1}.")
   @RBComment("Error message issued when the user tries to insert a node after a contained node.")
   public static final String CANT_INSERT_AFTER_CONTAINED = "58";

   @RBEntry("Can't insert {0} before {1} because {0} contains {1}.")
   @RBComment("Error message issued when the user tries to insert a node before a contained node.")
   public static final String CANT_INSERT_BEFORE_CONTAINED = "59";

   @RBEntry("Percent complete can't be greater than 100.")
   @RBComment("Error message issued when there is an attempt to set the percent complete to a value greater than 100 (usually through roll up).")
   public static final String INVALID_PERCENT = "60";

   @RBEntry("Sub Project/Program:")
   public static final String PROXY_NAME_LABEL = "61";

   @RBEntry("Duration is larger than the maximum (customizable) value of {0} days.")
   @RBComment("Error message issued when the user tries to set or change the duration of a node (activity, etc) to a value larger than the maximum allowed.  This limit is customizable.")
   public static final String DURATION_TOO_LARGE = "62";

   @RBEntry("Finish time must be given a value.")
   @RBComment("Error message issued when the user tries to change an object's finish time to a null value.")
   public static final String CANT_RESTART = "63";

   @RBEntry("Can't change finish of {0} because it hasn't started.")
   @RBComment("Error message issued when the user tries to change the finish time of an object that hasn't started yet.")
   public static final String CANT_CHANGE_FINISH = "64";

   @RBEntry("Can't change finish of {0} to a time that precedes its start.")
   @RBComment("Error message issued when the user tries to change the finish time of an object to a time that precedes its start.")
   public static final String FINISH_BEFORE_START = "65";

   @RBEntry("Can't change start of {0} to a time after its finish time.")
   @RBComment("Error message issued when the user tries to change the start time of an object to a time that is after its finish.")
   public static final String START_AFTER_FINISH = "66";

   @RBEntry("Project Manager notification")
   @RBComment("To be used in conjunction with email notification subjects to denote whether the email is sent to Owner or Project Manager")
   public static final String MANAGER = "67";

   @RBEntry("Owner notification")
   @RBComment("To be used in conjunction with email notification subjects to denote whether the email is sent to Owner or Project Manager")
   public static final String OWNER = "68";

   @RBEntry("{0} [{1}]")
   @RBComment("combination of event and notification subscriber (usually Object Owner or Project Manager).  Example: Missed Deadline for activity1 [Owner notification] ")
   public static final String SUBJECT_EVENT_SUBSCRIBER = "69";

   @RBEntry("State:")
   @RBComment("A label displayed in notification email")
   public static final String STATE_LABEL = "90";

   @RBEntry("Budget:")
   @RBComment("A label displayed in notification email")
   public static final String BUDGET_LABEL = "91";

   @RBEntry("Business Unit:")
   @RBComment("A label displayed in notification email")
   public static final String BUSINESS_UNIT_LABEL = "92";

   @RBEntry("Category:")
   @RBComment("A label displayed in notification email")
   public static final String CATEGORY_LABEL = "93";

   @RBEntry("Health Description:")
   @RBComment("A label displayed in notification email")
   public static final String HEALTH_DESCRIPTION_LABEL = "94";

   @RBEntry("Health Status:")
   @RBComment("A label displayed in notification email")
   public static final String HEALTH_STATUS_LABEL = "95";

   @RBEntry("Phase:")
   @RBComment("A label displayed in notification email")
   public static final String PHASE_LABEL = "96";

   @RBEntry("Priority:")
   @RBComment("A label displayed in notification email")
   public static final String PRIORITY_LABEL = "97";

   @RBEntry("Project Number:")
   @RBComment("A label displayed in notification email")
   public static final String PROJECT_NUMBER_LABEL = "98";

   @RBEntry("Risk Description:")
   @RBComment("A label displayed in notification email")
   public static final String RISK_DESCRIPTION_LABEL = "99";

   @RBEntry("Risk:")
   @RBComment("A label displayed in notification email")
   public static final String RISK_VALUE_LABEL = "100";

   @RBEntry("Site:")
   @RBComment("A label displayed in notification email")
   public static final String SITE_LABEL = "101";

   @RBEntry("Estimated Start:")
   @RBComment("A label displayed in notification email")
   public static final String START_DATE_LABEL = "102";

   @RBEntry("yyyy/MM/d")
   @RBComment("Date format that appears in notification email (2006/08/16) (It is used as a computational string so the letters should only be rearranged)")
   public static final String NOTICATION_DATE_FORMAT = "103";
}
