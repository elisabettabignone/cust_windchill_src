/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.projmgmt.resource;

import wt.util.resource.*;

@RBUUID("wt.projmgmt.resource.resourceResource")
public final class resourceResource_it extends WTListResourceBundle {
   @RBEntry("Risultato finale")
   public static final String DELIVERABLE_NAME = "0";

   @RBEntry("{0} ore")
   @RBComment("Unit of measure for planned or completed work")
   @RBArgComment0("the number of hours")
   public static final String HOURS = "1";

   @RBEntry("€")
   @RBComment("currency symbol to display beside amount values")
   public static final String CURRENCY_SYMBOL = "2";

   @RBEntry("0")
   @RBPseudoTrans("0")
   @RBComment("position of currency symbol in relation to amount value. Use 0 to prefix it and 1 to suffix it. Eg use 0 for en_US ($ prefixes amount in $10) and 1 for french (Franks suffixes amount in 10 Franks)")
   public static final String CURRENCY_POSITION = "3";

   @RBEntry("Solo i manager possono riavviare i risultati finali.")
   @RBComment("Error message issued when a user tries to restart a deliverable and he or she is not a manager.")
   public static final String MANAGER_RESTART = "4";

   @RBEntry("Impossibile riavviare {0} perché {1} è chiuso.")
   @RBComment("Error message issued when a user tries to restart a deliverable that is held by an activity or a milestone that is closed (completed or cancelled).")
   public static final String CLOSED_HOLDER = "5";

   @RBEntry("Si stanno assegnando più risorse-persona collegate all'utente {0}.")
   @RBComment("Error message issued when a user attempts to assign multiple person resources linked to the same user to a project activity.")
   public static final String MULTIPLE_PERSON_RESOURCES_FOR_USER = "6";

   @RBEntry("Impossibile rimuovere le risorse associate a {0} {1}.")
   @RBComment("Error message issued when user tries to delete the only resource associated to an activity.")
   public static final String DELETE_RESOURCE = "7";
}
