/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.representation;

import wt.util.resource.*;

@RBUUID("wt.representation.representationResource")
public final class representationResource_it extends WTListResourceBundle {
   @RBEntry("L'oggetto {0} ha troppe miniature associate.  Contattare l'amministratore di sistema.")
   public static final String TOO_MANY_THUMBNAILS = "1";

   @RBEntry("Nessuna miniatura è associata all'oggetto.")
   public static final String NO_THUMBNAILS = "2";

   @RBEntry("Non si dispone dei permessi per leggere l'oggetto rappresentabile {0}.")
   public static final String NO_PERMISSION_REP = "3";

   @RBEntry("Impossibile eliminare {0} perché è la rappresentazione di default per l'oggetto rappresentabile associato.")
   public static final String CANNOT_DELETE_DEFAULT_REP = "4";

   @RBEntry("L'oggetto rappresentabile {0} ha più di una rappresentazione di default.")
   public static final String TOO_MANY_DEFAULT_REPRESENTATIONS = "5";

   @RBEntry("Contenuto pubblicato")
   public static final String PUBLISHED_CONTENT_LIST = "6";

   @RBEntry("Documenti contenuto pubblicati")
   public static final String PUBLISHED_CONTENT_PART_TABLE_TITLE = "7";

   @RBEntry("Oggetti rappresentabili correlati")
   public static final String PUBLISHED_CONTENT_DOC_TABLE_TITLE = "8";

   @RBEntry("Nome rappresentazione")
   public static final String REPRESENTATION_NAME = "9";
}
