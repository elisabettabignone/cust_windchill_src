/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.representation;

import wt.util.resource.*;

@RBUUID("wt.representation.representationResource")
public final class representationResource extends WTListResourceBundle {
   @RBEntry("Object {0} has too many thumbnails associated with it.  Contact your system administrator.")
   public static final String TOO_MANY_THUMBNAILS = "1";

   @RBEntry("Object has no thumbnails associated with it.")
   public static final String NO_THUMBNAILS = "2";

   @RBEntry("You do not have permission to read the representable object {0}.")
   public static final String NO_PERMISSION_REP = "3";

   @RBEntry("Cannot delete representation {0} because it is the default representation for the associated representable object.")
   public static final String CANNOT_DELETE_DEFAULT_REP = "4";

   @RBEntry("The representable object {0} has more than one default representation.")
   public static final String TOO_MANY_DEFAULT_REPRESENTATIONS = "5";

   @RBEntry("Published Content")
   public static final String PUBLISHED_CONTENT_LIST = "6";

   @RBEntry("Published Content Documents")
   public static final String PUBLISHED_CONTENT_PART_TABLE_TITLE = "7";

   @RBEntry("Related Representables")
   public static final String PUBLISHED_CONTENT_DOC_TABLE_TITLE = "8";

   @RBEntry("Representation Name")
   public static final String REPRESENTATION_NAME = "9";
}
