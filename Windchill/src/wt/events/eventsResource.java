/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.events;

import wt.util.resource.*;

@RBUUID("wt.events.eventsResource")
public final class eventsResource extends WTListResourceBundle {
   @RBEntry("Can't change non-null class name.")
   public static final String NO_CHANGE_CLASS = "0";

   @RBEntry("Can't change non-null event type.")
   public static final String NO_CHANGE_EVENT = "1";

   @RBEntry("Agreement Expiration")
   @RBComment("Subscription label for subscribing to AGREEMENT EXPIRATION category")
   public static final String AGREEMENT_EXPIRATION = "AGREEMENT_EXPIRATION";

   @RBEntry("Agreement Expiration Pending")
   @RBComment("Subscription label for subscribing to AGREEMENT EXPIRATION PENDING category")
   public static final String AGREEMENT_EXPIRATION_PENDING = "AGREEMENT_EXPIRATION_PENDING";

   @RBEntry("Edit Access Control")
   @RBComment("Subscription label for subscribing to CHANGE ACCESS PERMISSIONS category")
   public static final String CHANGE_ACCESS_PERMISSIONS = "CHANGE_ACCESS_PERMISSIONS";

   @RBEntry("Life Cycle State")
   @RBComment("Subscription label for subscribing to LIFECYCLE STATE category")
   public static final String CHANGE_LIFECYCLE_STATE = "CHANGE_LIFECYCLE_STATE";

   @RBEntry("Check In from Project")
   @RBComment("Subscription label for subscribing to CHECK IN FROM PROJECT category")
   public static final String CHECK_IN_FROM_PROJECT = "CHECK_IN_FROM_PROJECT";

   @RBEntry("Check Out/Check In")
   @RBComment("Subscription labe for subscriptin to CHECK OUT IN category.")
   public static final String CHECK_OUT_IN = "CHECK_OUT_IN";

   @RBEntry("Complete")
   @RBComment("Subscription label for subscribing to COMPLETE category")
   public static final String COMPLETE = "COMPLETE";

   @RBEntry("Copy")
   @RBComment("Subscription label for subscribing to COPY category")
   public static final String COPY = "COPY";

   @RBEntry("Delete")
   @RBComment("Subscription label for subscribing to DELETE category")
   public static final String DELETE = "DELETE";

   @RBEntry("Edit Attributes")
   @RBComment("Subscription label for subscribing to EDIT ATTRIBUTES category")
   public static final String EDIT_ATTRIBUTES = "EDIT_ATTRIBUTES";

   @RBEntry("Edit Content")
   @RBComment("Subscription label for subscribing to EDIT CONTENT category")
   public static final String EDIT_CONTENT = "EDIT_CONTENT";

   @RBEntry("Edit Identity")
   @RBComment("Subscription label for subscribing to EDIT IDENTITY category")
   public static final String EDIT_IDENTITY = "EDIT_IDENTITY";

   @RBEntry("Edit Security Labels")
   @RBComment("Subscription label for subscription to EDIT SECURITY LABELS category")
   public static final String EDIT_SECURITY_LABELS = "EDIT_SECURITY_LABELS";

   @RBEntry("Formalize Problem Report or Variance")
   @RBComment("Subscription label for subscribing to FORMALIZE CHANGE ISSUE category")
   public static final String FORMALIZE_CHANGE_ISSUE = "FORMALIZE_CHANGE_ISSUE";

   @RBEntry("Markup")
   @RBComment("Subscription label for subscribing to MARKUP category")
   public static final String MARKUP = "MARKUP";

   @RBEntry("Move")
   @RBComment("Subscription label for subscribing to MOVE category")
   public static final String MOVE = "MOVE";

   @RBEntry("New Object")
   @RBComment("Subscription label for subscribing to NEW OBJECT category")
   public static final String NEW_OBJECT = "NEW_OBJECT";

   @RBEntry("Revise")
   @RBComment("Subscription label for subscribing to NEW REVISION category")
   public static final String NEW_REVISION = "NEW_REVISION";

   @RBEntry("New One-off Version")
   @RBComment("Subscription label for subscribing to NEW ONE-OFF VERSION category")
   public static final String NEW_ONE_OFF_VERSION = "NEW_ONE_OFF_VERSION";

   @RBEntry("New View Version")
   @RBComment("Subscription label for subscribing to NEW VIEW VERSION category")
   public static final String NEW_VIEW_VERSION = "NEW_VIEW_VERSION";

   @RBEntry("Publish Successful")
   @RBComment("Subscription label for subscribing to PUBLISH SUCCESSFUL category")
   public static final String PUBLISH_SUCCESSFUL = "PUBLISH_SUCCESSFUL";

   @RBEntry("Publish Unsuccessful")
   @RBComment("Subscription label for subscribing to PUBLISH UNSUCCESSFUL category ")
   public static final String PUBLISH_UNSUCCESSFUL = "PUBLISH_UNSUCCESSFUL";

   @RBEntry("Save Representation")
   @RBComment("Subscription label for subscribing to SAVE REPRESENTATION category")
   public static final String SAVE_REPRESENTATION = "SAVE_REPRESENTATION";

   @RBEntry("Monitor Change Implementation")
   @RBComment("Subscription label for subscribing to SET CHANGE ACTIVITY STATE category")
   public static final String SET_CHANGE_ACTIVITY_STATE = "SET_CHANGE_ACTIVITY_STATE";

   @RBEntry("Share")
   @RBComment("Subscription label for subscribing to SHARE category")
   public static final String SHARE = "SHARE";

   @RBEntry("State Change")
   @RBComment("Subscription label for subscribing to STATE CHANGE category")
   public static final String STATE_CHANGE = "STATE_CHANGE";

   @RBEntry("Status Change")
   @RBComment("Subscription label for subscribing to PROJECT PLAN STATUS CHANGE category")
   public static final String STATUS_CHANGE = "STATUS_CHANGE";

   @RBEntry("Risk Change")
   @RBComment("Subscription label for subscribing to PROJECT PLAN RISK CHANGE category")
   public static final String RISK_CHANGE = "RISK_CHANGE";

   @RBEntry("Percent Change")
   @RBComment("Subscription label for subscribing to PROJECT PLAN PERCENT CHANGE category")
   public static final String PERCENT_CHANGE = "PERCENT_CHANGE";

   @RBEntry("Finish Change")
   @RBComment("Subscription label for subscribing to PROJECT PLAN FINISH CHANGE category")
   public static final String FINISH_CHANGE = "FINISH_CHANGE";

   @RBEntry("Owner Change")
   @RBComment("Subscription label for subscribing to PROJECT PLAN OWNER CHANGE category")
   public static final String OWNER_CHANGE = "OWNER_CHANGE";

   @RBEntry("Deadline Change")
   @RBComment("Subscription label for subscribing to PROJECT PLAN DEADLINE CHANGE category")
   public static final String DEADLINE_CHANGE = "DEADLINE_CHANGE";

   @RBEntry("Missed Deadline")
   @RBComment("Subscription label for subscribing to PROJECT PLAN MISSED DEADLINE category")
   public static final String MISSED_DEADLINE = "MISSED_DEADLINE";

   @RBEntry("Unformalize Problem Report or Variance")
   @RBComment("Subscription label for subscribing to UNFORMALIZE CHANGE ISSUE category")
   public static final String UNFORMALIZE_CHANGE_ISSUE = "UNFORMALIZE_CHANGE_ISSUE";

   @RBEntry("Used By")
   @RBComment("Subscription label for subscribing to USED BY category")
   public static final String USED_BY = "USED_BY";

   @RBEntry("Workflow State Change")
   @RBComment("Subscription label for subscribing to WORKFLOW STATE CHANGE category")
   public static final String CHANGE_WORKFLOW_STATE = "CHANGE_WORKFLOW_STATE";

   @RBEntry("Zip Delivery")
   @RBComment("Subscription label for subscribing to ZIP DELIVERY category")
   public static final String ZIP_DELIVERY = "ZIP_DELIVERY";

   @RBEntry("Illustration Source Data Update")
   @RBComment("Subscription label for subscription to ILLUSTRATION SOURCE UPDATED category.")
   public static final String ILLUSTRATION_SOURCE_UPDATED = "ILLUSTRATION_SOURCE_UPDATED";

   @RBEntry("New Iteration")
   @RBComment("Subscription label for subscribing to New Iteration category.")
   public static final String NEW_ITERATION = "NEW_ITERATION";

   /**
    * Event labels to be recorded in the AuditEvent database table
    **/
   @RBEntry("Edit Identity")
   public static final String PRIVATE_CONSTANT_0 = "*/wt.events.summary.ChangeIdentitySummaryEvent/";

   @RBEntry("Change Life Cycle State")
   public static final String PRIVATE_CONSTANT_1 = "*/wt.events.summary.ChangeLifecycleStateSummaryEvent/";

   @RBEntry("Check In")
   public static final String PRIVATE_CONSTANT_2 = "*/wt.events.summary.CheckinSummaryEvent/";


   @RBEntry("Check Out")
   public static final String PRIVATE_CONSTANT_3 = "*/wt.events.summary.CheckoutSummaryEvent/";

   @RBEntry("Complete")
   public static final String PRIVATE_CONSTANT_4 = "*/wt.events.summary.CompleteSummaryEvent/";

   @RBEntry("Copy")
   public static final String PRIVATE_CONSTANT_5 = "*/wt.events.summary.CopySummaryEvent/";

   @RBEntry("New Object")
   public static final String PRIVATE_CONSTANT_6 = "*/wt.events.summary.CreateSummaryEvent/";

   @RBEntry("Delete")
   public static final String PRIVATE_CONSTANT_7 = "*/wt.events.summary.DeleteSummaryEvent/";

   @RBEntry("Export")
   public static final String PRIVATE_CONSTANT_8 = "*/wt.events.summary.ExportSummaryEvent/";

   @RBEntry("Import")
   public static final String PRIVATE_CONSTANT_9 = "*/wt.events.summary.ImportSummaryEvent/";

   @RBEntry("Markup and Annotate")
   public static final String PRIVATE_CONSTANT_10 = "*/wt.events.summary.MarkupAnnotateSummaryEvent/";

   @RBEntry("Modify Access Policy")
   public static final String PRIVATE_CONSTANT_11 = "*/wt.events.summary.ModifyAccessPolicySummaryEvent/";

   @RBEntry("Edit Content")
   public static final String PRIVATE_CONSTANT_12 = "*/wt.events.summary.ModifyContentSummaryEvent/";

   @RBEntry("Edit Access Control")
   public static final String PRIVATE_CONSTANT_13 = "*/wt.events.summary.ModifyObjectAccessSummaryEvent/";

   @RBEntry("Modify Product Structure")
   public static final String PRIVATE_CONSTANT_14 = "*/wt.events.summary.ModifyProductStructureSummaryEvent/";

   @RBEntry("Modify Security Labels")
   public static final String PRIVATE_CONSTANT_15 = "*/wt.events.summary.ModifySecurityLabelsSummaryEvent/";

   @RBEntry("Edit Attributes")
   public static final String PRIVATE_CONSTANT_16 = "*/wt.events.summary.ModifySummaryEvent/";

   @RBEntry("Edit Team")
   public static final String PRIVATE_CONSTANT_17 = "*/wt.events.summary.ModifyTeamSummaryEvent/";

   @RBEntry("Move")
   public static final String PRIVATE_CONSTANT_18 = "*/wt.events.summary.MoveSummaryEvent/";

   @RBEntry("New View Version")
   public static final String PRIVATE_CONSTANT_19 = "*/wt.events.summary.NewViewVersionSummaryEvent/";

   @RBEntry("One Off Version")
   public static final String PRIVATE_CONSTANT_20 = "*/wt.events.summary.OneOffVersionSummaryEvent/";

   @RBEntry("Revise")
   public static final String PRIVATE_CONSTANT_21 = "*/wt.events.summary.ReviseNewVersionSummaryEvent/";

   @RBEntry("Share")
   public static final String PRIVATE_CONSTANT_22 = "*/wt.events.summary.ShareSummaryEvent/";

   @RBEntry("Undo Checkout")
   public static final String PRIVATE_CONSTANT_23 = "*/wt.events.summary.UndoCheckoutSummaryEvent/";

   @RBEntry("Login")
   public static final String PRIVATE_CONSTANT_24 = "*/wt.session.SessionUserAuditEvent/login";

   @RBEntry("Logout")
   public static final String PRIVATE_CONSTANT_25 = "*/wt.session.SessionUserAuditEvent/logout";

   @RBEntry("Search")
   public static final String PRIVATE_CONSTANT_26 = "*/com.ptc.windchill.enterprise.search.server.searchaudit.SearchAuditEvent/SEARCH_AUDIT_EVENT";

   @RBEntry("Change Implementation")
   public static final String PRIVATE_CONSTANT_27 = "*/wt.change2.ChangeService2Event/CHANGE_IMPLEMENTATION";

   @RBEntry("Not Authorized Access")
   public static final String PRIVATE_CONSTANT_28 = "*/wt.access.AccessControlEvent/NOT_AUTHORIZED";

   @RBEntry("View Properties")
   public static final String PRIVATE_CONSTANT_29 = "*/wt.audit.AuditServiceEvent/VIEW_PROPERTIES";

   @RBEntry("Associate")
   public static final String PRIVATE_CONSTANT_30 = "*/wt.audit.AuditServiceEvent/ASSOCIATE";

   @RBEntry("Disassociate")
   public static final String PRIVATE_CONSTANT_31 = "*/wt.audit.AuditServiceEvent/DISASSOCIATE";

   @RBEntry("Cross Site Request Forgery")
   public static final String CSRF = "*/wt.audit.AuditServiceEvent/CSRF";

   @RBEntry("Download")
   public static final String PRIVATE_CONSTANT_32 = "*/wt.content.ContentServiceEvent/READ_CONTENT";

   @RBEntry("Archive")
   public static final String PRIVATE_CONSTANT_33 = "*/wt.fc.archive.ArchiveServiceEvent/POST_ARCHIVE";

   @RBEntry("Restore Archive")
   public static final String PRIVATE_CONSTANT_34 = "*/wt.fc.archive.RestoreServiceEvent/POST_RESTORE";

   @RBEntry("Context Logon")
   public static final String PRIVATE_CONSTANT_35 = "*/wt.inf.team.ContainerTeamServiceEvent/CONTAINER_LOGIN";

   @RBEntry("Add Role to Organization")
   public static final String PRIVATE_CONSTANT_36 = "*/wt.inf.team.NmOrganizationServiceEvent/ADD_ROLE";

   @RBEntry("Remove Role from Organization")
   public static final String PRIVATE_CONSTANT_37 = "*/wt.inf.team.NmOrganizationServiceEvent/REMOVE_ROLE";

   @RBEntry("Add Role to Life Cycle Team")
   public static final String PRIVATE_CONSTANT_38 = "*/wt.team.TeamServiceEvent/ADD_ROLE";

   @RBEntry("Remove Role from Life Cycle Team")
   public static final String PRIVATE_CONSTANT_39 = "*/wt.team.TeamServiceEvent/REMOVE_ROLE";

   @RBEntry("Add Role to Context Team")
   public static final String PRIVATE_CONSTANT_86 = "*/wt.inf.team.ContainerTeamServiceEvent/ADD_ROLE";

   @RBEntry("Remove Role from Context Team")
   public static final String PRIVATE_CONSTANT_87 = "*/wt.inf.team.ContainerTeamServiceEvent/REMOVE_ROLE";

   @RBEntry("Edit Group")
   public static final String PRIVATE_CONSTANT_40 = "*/wt.org.OrganizationServicesEvent/MEMBERSHIP_CHANGE";

   @RBEntry("User Password Change")
   public static final String PRIVATE_CONSTANT_41 = "*/wt.org.OrganizationServicesEvent/PASSWORD_CHANGE";

   @RBEntry("Due Date Change (Classic)")
   public static final String PRIVATE_CONSTANT_42 = "*/wt.projmgmt.ProjMgmtServiceEvent/DEADLINE_CHANGE";

   @RBEntry("Finish Date Change (Classic)")
   public static final String PRIVATE_CONSTANT_43 = "*/wt.projmgmt.ProjMgmtServiceEvent/FINISH_CHANGE";

   @RBEntry("Owner Change (Classic)")
   public static final String PRIVATE_CONSTANT_44 = "*/wt.projmgmt.ProjMgmtServiceEvent/OWNER_CHANGE";

   @RBEntry("Percent Complete Change (Classic)")
   public static final String PRIVATE_CONSTANT_45 = "*/wt.projmgmt.ProjMgmtServiceEvent/PERCENT_CHANGE";

   @RBEntry("State Change (Classic)")
   public static final String PRIVATE_CONSTANT_46 = "*/wt.projmgmt.ProjMgmtServiceEvent/STATE_CHANGE";

   @RBEntry("Health Status Change (Classic)")
   public static final String PRIVATE_CONSTANT_47 = "*/wt.projmgmt.ProjMgmtServiceEvent/STATUS_CHANGE";

   @RBEntry("Finish Date Change")
   public static final String PRIVATE_CONSTANT_75 = "*/com.ptc.projectmanagement.plan.ProjectManagementEvent/EPP_FINISH_CHANGE";

   @RBEntry("Owner Change")
   public static final String PRIVATE_CONSTANT_76 = "*/com.ptc.projectmanagement.plan.ProjectManagementEvent/EPP_OWNER_CHANGE";

   @RBEntry("Percent Complete Change")
   public static final String PRIVATE_CONSTANT_77 = "*/com.ptc.projectmanagement.plan.ProjectManagementEvent/EPP_PERCENT_CHANGE";

   @RBEntry("Risk Change")
   public static final String PRIVATE_CONSTANT_78 = "*/com.ptc.projectmanagement.plan.ProjectManagementEvent/EPP_RISK_CHANGE";

   @RBEntry("Health Status Change")
   public static final String PRIVATE_CONSTANT_79 = "*/com.ptc.projectmanagement.plan.ProjectManagementEvent/EPP_STATUS_CHANGE";

   @RBEntry("Deadline Change")
   public static final String PRIVATE_CONSTANT_80 = "*/com.ptc.projectmanagement.plan.ProjectManagementEvent/EPP_DEADLINE_CHANGE";

   @RBEntry("State Change")
   public static final String PLANACTIVITY_STATE_CHANGE_EVENT = "*/com.ptc.projectmanagement.plan.ProjectManagementEvent/PLANACTIVITY_STATE_CHANGE";

   @RBEntry("View Representations")
   public static final String PRIVATE_CONSTANT_48 = "*/wt.representation.ViewEvent/VIEW";

   @RBEntry("Sent To Print")
   public static final String SENT_TO_PRINT = "*/wt.representation.SentToPrintEvent/SENT_TO_PRINT";

   @RBEntry("Rollback Iteration")
   public static final String PRIVATE_CONSTANT_49 = "*/wt.vc.VersionControlServiceEvent/POST_ROLLBACK";

   @RBEntry("Purge")
   public static final String PRIVATE_CONSTANT_50 = "*/wt.vc.VersionControlServiceEvent/PRE_ROLLUP";

   @RBEntry("Workflow Activity Variable Change")
   public static final String PRIVATE_CONSTANT_51 = "*/wt.workflow.engine.WfEngineServiceEvent/ACTIVITY_CONTEXT_CHANGED";

   @RBEntry("Workflow Activity State Change")
   public static final String PRIVATE_CONSTANT_52 = "*/wt.workflow.engine.WfEngineServiceEvent/ACTIVITY_STATE_CHANGED";

   @RBEntry("Workflow Variable Change")
   public static final String PRIVATE_CONSTANT_53 = "*/wt.workflow.engine.WfEngineServiceEvent/PROCESS_CONTEXT_CHANGED";

   @RBEntry("Workflow State Change")
   public static final String PRIVATE_CONSTANT_54 = "*/wt.workflow.engine.WfEngineServiceEvent/PROCESS_STATE_CHANGED";

   @RBEntry("New Discussion")
   public static final String PRIVATE_CONSTANT_55 = "*/wt.workflow.forum.ForumServiceEvent/NEW_DISCUSSION";

   @RBEntry("Preference Change")
   public static final String PRIVATE_CONSTANT_84 = "*/wt.preference.PreferenceServiceEvent/POST_UPDATE";

   @RBEntry("Security Label Download Acknowledgment")
   public static final String PRIVATE_CONSTANT_85 = "*/wt.audit.AuditServiceEvent/SECURITY_LABEL_DOWNLOAD_ACK";

    /*
     * All Subscription UI events are done via categories and are listed at the
     * beginning of this file as <CATEGORY_NAME>.value. CATEGORY_NAME is
     * associated with that seen in codebase\wt\notify\subscriptionConfig.xml.
     * Event labels to be recorded in the AuditEvent database table are
     * listed above as actual event strings. Those that are not recorded are
     * listed below with a ** instead of * /. Both the Subscription UI and
     * StandardEventNotificationDelegate will call the notification APIs,
     * getLocalizedEvent or getLocalizedCategory. These APIs will do the extra
     * check to see if an event with ** exists.
     *
     * Task 1417615-03 addresses the issue of how the audit event picker is
     * currently using this file and what needs to be done in the future. Once
     * this task has been addressed, these events can be changed to be actual
     * events (i.e. the extra * can be removed and extra code to append * in
     * front of the incoming event can be removed from the notification APIs,
     * getLocalizedEvent and getLocalizedCategory.
     */

   @RBEntry("Agreement Expiration Pending")
   public static final String PRIVATE_CONSTANT_56 = "**wt.access.agreement.AgreementServiceEvent/AGREEMENT_PENDING_EXPIRATION";

   @RBEntry("Agreement Expiration")
   public static final String PRIVATE_CONSTANT_57 = "**wt.access.agreement.AgreementServiceEvent/AGREEMENT_EXPIRED";

   @RBEntry("Change Notice State Change")
   public static final String PRIVATE_CONSTANT_58 = "**wt.change2.ChangeService2Event/CN_STATE_CHANGED";

   @RBEntry("Set Change Task State")
   public static final String PRIVATE_CONSTANT_59 = "**wt.change2.ChangeService2Event/CA_STATE_CHANGED";

   @RBEntry("Formalize Problem Report or Variance")
   public static final String PRIVATE_CONSTANT_60 = "**wt.change2.ChangeService2Event/ISSUE_FORMALIZED";

   @RBEntry("Unformalize Problem Report or Variance")
   public static final String PRIVATE_CONSTANT_61 = "**wt.change2.ChangeService2Event/ISSUE_UNFORMALIZED";

   @RBEntry("Share")
   public static final String PRIVATE_CONSTANT_62 = "**wt.events.summary.NewShareSummaryEvent/";

   @RBEntry("Delete")
   public static final String PRIVATE_CONSTANT_63 = "**wt.projmgmt.ProjMgmtServiceEvent/OBJECT_DELETION";

   @RBEntry("Missed Due Date")
   public static final String PRIVATE_CONSTANT_64 = "**wt.projmgmt.ProjMgmtServiceEvent/DEADLINE";

   @RBEntry("Days Past Due Date")
   public static final String PRIVATE_CONSTANT_65 = "**wt.projmgmt.ProjMgmtServiceEvent/TIME_PAST_DEADLINE";

   @RBEntry("Days to Due Date")
   public static final String PRIVATE_CONSTANT_66 = "**wt.projmgmt.ProjMgmtServiceEvent/TIME_TO_DEADLINE";

   @RBEntry("Missed Deadline")
   public static final String PRIVATE_CONSTANT_81 = "**com.ptc.projectmanagement.plan.ProjectManagementEvent/EPP_MISSED_DEADLINE";

   @RBEntry("After Deadline")
   public static final String PRIVATE_CONSTANT_82 = "**com.ptc.projectmanagement.plan.ProjectManagementEvent/EPP_DEADLINE_AFTER";

   @RBEntry("Before Deadline")
   public static final String PRIVATE_CONSTANT_83 = "**com.ptc.projectmanagement.plan.ProjectManagementEvent/EPP_DEADLINE_BEFORE";

   @RBEntry("Save Representation")
   public static final String PRIVATE_CONSTANT_67 = "**com.ptc.wvs.server.loader.GraphicsServerLoaderServiceEvent/REPRESENTATION_SAVED";

   @RBEntry("Publish Unsuccessful")
   public static final String PRIVATE_CONSTANT_68 = "**com.ptc.wvs.server.publish.PublishServiceEvent/PUBLISH_NOT_SUCCESSFUL";

   @RBEntry("Publish Successful")
   public static final String PRIVATE_CONSTANT_69 = "**com.ptc.wvs.server.publish.PublishServiceEvent/PUBLISH_SUCCESSFUL";

   @RBEntry("Check In from Project")
   public static final String PRIVATE_CONSTANT_70 = "**wt.vc.VersionControlServiceEvent/POST_MERGE";

   @RBEntry("Check In from Project")
   public static final String PRIVATE_CONSTANT_71 = "**wt.sandbox.SandboxServiceCheckinEvent/POST_SB_CHECKIN_EVENT";

   @RBEntry("Illustration Source Data Update")
   public static final String PRIVATE_CONSTANT_88 = "**wt.events.summary.SourceUpdatedSummaryEvent/";

   @RBEntry("New Iteration")
   public static final String NEW_ITERATION_EVENT_LABEL = "**wt.vc.VersionControlServiceEvent/NEW_ITERATION";

   /**
    * This following three entries are required to support migration of X-05 systems with Folder subscription
    * entries on the Notification queue to X-10 (or later). This entries can be removed once support is
    * no longer required for migrating X-05 systems.
    **/
   @RBEntry("Create in Folder")
   public static final String PRIVATE_CONSTANT_72 = "**wt.folder.FolderSubscriptionEvent/POST_CREATE_IN_FOLDER";

   @RBEntry("Check In in Folder")
   public static final String PRIVATE_CONSTANT_73 = "**wt.folder.FolderSubscriptionEvent/CHECK_IN_IN_FOLDER";

   @RBEntry("State Change in Folder")
   public static final String PRIVATE_CONSTANT_74 = "**wt.folder.FolderSubscriptionEvent/STATE_CHANGE_IN_FOLDER";

   /**
    * This entry is required to support updating audit event records from pre-Windchill 10 systems with
    * an event label of "New Revision" to "Revise". This entry can be removed once support is
    * no longer required for migrating 9.0 and 9.1 systems.
    **/
   @RBEntry("New Revision")
   @RBComment("Event label which was used prior to Windchill 10.0 for the NEW REVISION category.  This should exactly match the NEW_REVISION resource value from the Windchill 9.1 release. A WinRU task updates each audit record whose eventLabel matches PRE_WINDCHILL_10_NEW_REVISION to the current value of NEW_REVISION.")
   public static final String PRE_WINDCHILL_10_NEW_REVISION = "PRE_WINDCHILL_10_NEW_REVISION";
}
