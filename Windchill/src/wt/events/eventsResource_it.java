/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.events;

import wt.util.resource.*;

@RBUUID("wt.events.eventsResource")
public final class eventsResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile modificare un nome di classe non nullo.")
   public static final String NO_CHANGE_CLASS = "0";

   @RBEntry("Impossibile modificare un tipo di evento non nullo.")
   public static final String NO_CHANGE_EVENT = "1";

   @RBEntry("Scadenza accordo")
   @RBComment("Subscription label for subscribing to AGREEMENT EXPIRATION category")
   public static final String AGREEMENT_EXPIRATION = "AGREEMENT_EXPIRATION";

   @RBEntry("Scadenza accordo prossima")
   @RBComment("Subscription label for subscribing to AGREEMENT EXPIRATION PENDING category")
   public static final String AGREEMENT_EXPIRATION_PENDING = "AGREEMENT_EXPIRATION_PENDING";

   @RBEntry("Modifica controllo di accesso")
   @RBComment("Subscription label for subscribing to CHANGE ACCESS PERMISSIONS category")
   public static final String CHANGE_ACCESS_PERMISSIONS = "CHANGE_ACCESS_PERMISSIONS";

   @RBEntry("Stato del ciclo di vita")
   @RBComment("Subscription label for subscribing to LIFECYCLE STATE category")
   public static final String CHANGE_LIFECYCLE_STATE = "CHANGE_LIFECYCLE_STATE";

   @RBEntry("Check-In dal progetto")
   @RBComment("Subscription label for subscribing to CHECK IN FROM PROJECT category")
   public static final String CHECK_IN_FROM_PROJECT = "CHECK_IN_FROM_PROJECT";

   @RBEntry("Check-Out/Check-In")
   @RBComment("Subscription labe for subscriptin to CHECK OUT IN category.")
   public static final String CHECK_OUT_IN = "CHECK_OUT_IN";

   @RBEntry("Completa")
   @RBComment("Subscription label for subscribing to COMPLETE category")
   public static final String COMPLETE = "COMPLETE";

   @RBEntry("Copia")
   @RBComment("Subscription label for subscribing to COPY category")
   public static final String COPY = "COPY";

   @RBEntry("Elimina")
   @RBComment("Subscription label for subscribing to DELETE category")
   public static final String DELETE = "DELETE";

   @RBEntry("Modifica attributi")
   @RBComment("Subscription label for subscribing to EDIT ATTRIBUTES category")
   public static final String EDIT_ATTRIBUTES = "EDIT_ATTRIBUTES";

   @RBEntry("Modifica contenuto")
   @RBComment("Subscription label for subscribing to EDIT CONTENT category")
   public static final String EDIT_CONTENT = "EDIT_CONTENT";

   @RBEntry("Modifica identità")
   @RBComment("Subscription label for subscribing to EDIT IDENTITY category")
   public static final String EDIT_IDENTITY = "EDIT_IDENTITY";

   @RBEntry("Modifica etichette di sicurezza")
   @RBComment("Subscription label for subscription to EDIT SECURITY LABELS category")
   public static final String EDIT_SECURITY_LABELS = "EDIT_SECURITY_LABELS";

   @RBEntry("Formalizza report di problema o soluzione temporanea")
   @RBComment("Subscription label for subscribing to FORMALIZE CHANGE ISSUE category")
   public static final String FORMALIZE_CHANGE_ISSUE = "FORMALIZE_CHANGE_ISSUE";

   @RBEntry("Markup")
   @RBComment("Subscription label for subscribing to MARKUP category")
   public static final String MARKUP = "MARKUP";

   @RBEntry("Sposta")
   @RBComment("Subscription label for subscribing to MOVE category")
   public static final String MOVE = "MOVE";

   @RBEntry("Nuovo oggetto")
   @RBComment("Subscription label for subscribing to NEW OBJECT category")
   public static final String NEW_OBJECT = "NEW_OBJECT";

   @RBEntry("Nuova revisione")
   @RBComment("Subscription label for subscribing to NEW REVISION category")
   public static final String NEW_REVISION = "NEW_REVISION";

   @RBEntry("Nuova versione variante")
   @RBComment("Subscription label for subscribing to NEW ONE-OFF VERSION category")
   public static final String NEW_ONE_OFF_VERSION = "NEW_ONE_OFF_VERSION";

   @RBEntry("Nuova versione vista")
   @RBComment("Subscription label for subscribing to NEW VIEW VERSION category")
   public static final String NEW_VIEW_VERSION = "NEW_VIEW_VERSION";

   @RBEntry("Pubblicazione completata")
   @RBComment("Subscription label for subscribing to PUBLISH SUCCESSFUL category")
   public static final String PUBLISH_SUCCESSFUL = "PUBLISH_SUCCESSFUL";

   @RBEntry("Pubblicazione non riuscita")
   @RBComment("Subscription label for subscribing to PUBLISH UNSUCCESSFUL category ")
   public static final String PUBLISH_UNSUCCESSFUL = "PUBLISH_UNSUCCESSFUL";

   @RBEntry("Salva rappresentazione")
   @RBComment("Subscription label for subscribing to SAVE REPRESENTATION category")
   public static final String SAVE_REPRESENTATION = "SAVE_REPRESENTATION";

   @RBEntry("Monitoraggio implementazione modifiche")
   @RBComment("Subscription label for subscribing to SET CHANGE ACTIVITY STATE category")
   public static final String SET_CHANGE_ACTIVITY_STATE = "SET_CHANGE_ACTIVITY_STATE";

   @RBEntry("Condividi")
   @RBComment("Subscription label for subscribing to SHARE category")
   public static final String SHARE = "SHARE";

   @RBEntry("Modifica di stato del ciclo di vita")
   @RBComment("Subscription label for subscribing to STATE CHANGE category")
   public static final String STATE_CHANGE = "STATE_CHANGE";

   @RBEntry("Modifica di stato")
   @RBComment("Subscription label for subscribing to PROJECT PLAN STATUS CHANGE category")
   public static final String STATUS_CHANGE = "STATUS_CHANGE";

   @RBEntry("Modifica rischio")
   @RBComment("Subscription label for subscribing to PROJECT PLAN RISK CHANGE category")
   public static final String RISK_CHANGE = "RISK_CHANGE";

   @RBEntry("Modifica percentuale")
   @RBComment("Subscription label for subscribing to PROJECT PLAN PERCENT CHANGE category")
   public static final String PERCENT_CHANGE = "PERCENT_CHANGE";

   @RBEntry("Modifica fine")
   @RBComment("Subscription label for subscribing to PROJECT PLAN FINISH CHANGE category")
   public static final String FINISH_CHANGE = "FINISH_CHANGE";

   @RBEntry("Modifica proprietario")
   @RBComment("Subscription label for subscribing to PROJECT PLAN OWNER CHANGE category")
   public static final String OWNER_CHANGE = "OWNER_CHANGE";

   @RBEntry("Modifica scadenza")
   @RBComment("Subscription label for subscribing to PROJECT PLAN DEADLINE CHANGE category")
   public static final String DEADLINE_CHANGE = "DEADLINE_CHANGE";

   @RBEntry("Scadenza superata")
   @RBComment("Subscription label for subscribing to PROJECT PLAN MISSED DEADLINE category")
   public static final String MISSED_DEADLINE = "MISSED_DEADLINE";

   @RBEntry("Annulla formalizzazione report di problema o soluzione temporanea")
   @RBComment("Subscription label for subscribing to UNFORMALIZE CHANGE ISSUE category")
   public static final String UNFORMALIZE_CHANGE_ISSUE = "UNFORMALIZE_CHANGE_ISSUE";

   @RBEntry("Impiegato da")
   @RBComment("Subscription label for subscribing to USED BY category")
   public static final String USED_BY = "USED_BY";

   @RBEntry("Modifica stato di workflow")
   @RBComment("Subscription label for subscribing to WORKFLOW STATE CHANGE category")
   public static final String CHANGE_WORKFLOW_STATE = "CHANGE_WORKFLOW_STATE";

   @RBEntry("Comprimi consegna")
   @RBComment("Subscription label for subscribing to ZIP DELIVERY category")
   public static final String ZIP_DELIVERY = "ZIP_DELIVERY";

   @RBEntry("Aggiornamento dati di origine illustrazione")
   @RBComment("Subscription label for subscription to ILLUSTRATION SOURCE UPDATED category.")
   public static final String ILLUSTRATION_SOURCE_UPDATED = "ILLUSTRATION_SOURCE_UPDATED";

   @RBEntry("Nuova iterazione")
   @RBComment("Subscription label for subscribing to New Iteration category.")
   public static final String NEW_ITERATION = "NEW_ITERATION";

   /**
    * Event labels to be recorded in the AuditEvent database table
    **/
   @RBEntry("Modifica identità")
   public static final String PRIVATE_CONSTANT_0 = "*/wt.events.summary.ChangeIdentitySummaryEvent/";

   @RBEntry("Modifica stato del ciclo di vita")
   public static final String PRIVATE_CONSTANT_1 = "*/wt.events.summary.ChangeLifecycleStateSummaryEvent/";

   @RBEntry("Check-In")
   public static final String PRIVATE_CONSTANT_2 = "*/wt.events.summary.CheckinSummaryEvent/";


   @RBEntry("Check-Out")
   public static final String PRIVATE_CONSTANT_3 = "*/wt.events.summary.CheckoutSummaryEvent/";

   @RBEntry("Completo")
   public static final String PRIVATE_CONSTANT_4 = "*/wt.events.summary.CompleteSummaryEvent/";

   @RBEntry("Copia")
   public static final String PRIVATE_CONSTANT_5 = "*/wt.events.summary.CopySummaryEvent/";

   @RBEntry("Nuovo oggetto")
   public static final String PRIVATE_CONSTANT_6 = "*/wt.events.summary.CreateSummaryEvent/";

   @RBEntry("Elimina ")
   public static final String PRIVATE_CONSTANT_7 = "*/wt.events.summary.DeleteSummaryEvent/";

   @RBEntry("Esporta")
   public static final String PRIVATE_CONSTANT_8 = "*/wt.events.summary.ExportSummaryEvent/";

   @RBEntry("Importa")
   public static final String PRIVATE_CONSTANT_9 = "*/wt.events.summary.ImportSummaryEvent/";

   @RBEntry("Annota")
   public static final String PRIVATE_CONSTANT_10 = "*/wt.events.summary.MarkupAnnotateSummaryEvent/";

   @RBEntry("Modifica regola di accesso")
   public static final String PRIVATE_CONSTANT_11 = "*/wt.events.summary.ModifyAccessPolicySummaryEvent/";

   @RBEntry("Modifica contenuto")
   public static final String PRIVATE_CONSTANT_12 = "*/wt.events.summary.ModifyContentSummaryEvent/";

   @RBEntry("Modifica controllo di accesso")
   public static final String PRIVATE_CONSTANT_13 = "*/wt.events.summary.ModifyObjectAccessSummaryEvent/";

   @RBEntry("Modifica struttura di prodotto")
   public static final String PRIVATE_CONSTANT_14 = "*/wt.events.summary.ModifyProductStructureSummaryEvent/";

   @RBEntry("Modifica etichette di sicurezza")
   public static final String PRIVATE_CONSTANT_15 = "*/wt.events.summary.ModifySecurityLabelsSummaryEvent/";

   @RBEntry("Modifica attributi")
   public static final String PRIVATE_CONSTANT_16 = "*/wt.events.summary.ModifySummaryEvent/";

   @RBEntry("Modifica team")
   public static final String PRIVATE_CONSTANT_17 = "*/wt.events.summary.ModifyTeamSummaryEvent/";

   @RBEntry("Sposta")
   public static final String PRIVATE_CONSTANT_18 = "*/wt.events.summary.MoveSummaryEvent/";

   @RBEntry("Nuova versione vista")
   public static final String PRIVATE_CONSTANT_19 = "*/wt.events.summary.NewViewVersionSummaryEvent/";

   @RBEntry("Versione variante")
   public static final String PRIVATE_CONSTANT_20 = "*/wt.events.summary.OneOffVersionSummaryEvent/";

   @RBEntry("Nuova revisione")
   public static final String PRIVATE_CONSTANT_21 = "*/wt.events.summary.ReviseNewVersionSummaryEvent/";

   @RBEntry("Condividi")
   public static final String PRIVATE_CONSTANT_22 = "*/wt.events.summary.ShareSummaryEvent/";

   @RBEntry("Annulla Check-Out")
   public static final String PRIVATE_CONSTANT_23 = "*/wt.events.summary.UndoCheckoutSummaryEvent/";

   @RBEntry("Accesso")
   public static final String PRIVATE_CONSTANT_24 = "*/wt.session.SessionUserAuditEvent/login";

   @RBEntry("Disconnetti")
   public static final String PRIVATE_CONSTANT_25 = "*/wt.session.SessionUserAuditEvent/logout";

   @RBEntry("Cerca")
   public static final String PRIVATE_CONSTANT_26 = "*/com.ptc.windchill.enterprise.search.server.searchaudit.SearchAuditEvent/SEARCH_AUDIT_EVENT";

   @RBEntry("Implementazione modifiche")
   public static final String PRIVATE_CONSTANT_27 = "*/wt.change2.ChangeService2Event/CHANGE_IMPLEMENTATION";

   @RBEntry("Accesso non autorizzato")
   public static final String PRIVATE_CONSTANT_28 = "*/wt.access.AccessControlEvent/NOT_AUTHORIZED";

   @RBEntry("Visualizza proprietà")
   public static final String PRIVATE_CONSTANT_29 = "*/wt.audit.AuditServiceEvent/VIEW_PROPERTIES";

   @RBEntry("Associa")
   public static final String PRIVATE_CONSTANT_30 = "*/wt.audit.AuditServiceEvent/ASSOCIATE";

   @RBEntry("Dissocia")
   public static final String PRIVATE_CONSTANT_31 = "*/wt.audit.AuditServiceEvent/DISASSOCIATE";

   @RBEntry("CSRF (Cross Site Request Forgery)")
   public static final String CSRF = "*/wt.audit.AuditServiceEvent/CSRF";

   @RBEntry("Scaricamento")
   public static final String PRIVATE_CONSTANT_32 = "*/wt.content.ContentServiceEvent/READ_CONTENT";

   @RBEntry("Archivio")
   public static final String PRIVATE_CONSTANT_33 = "*/wt.fc.archive.ArchiveServiceEvent/POST_ARCHIVE";

   @RBEntry("Ripristina archivio")
   public static final String PRIVATE_CONSTANT_34 = "*/wt.fc.archive.RestoreServiceEvent/POST_RESTORE";

   @RBEntry("Accesso a contesto")
   public static final String PRIVATE_CONSTANT_35 = "*/wt.inf.team.ContainerTeamServiceEvent/CONTAINER_LOGIN";

   @RBEntry("Aggiungi ruolo all'organizzazione")
   public static final String PRIVATE_CONSTANT_36 = "*/wt.inf.team.NmOrganizationServiceEvent/ADD_ROLE";

   @RBEntry("Rimuovi ruolo dall'organizzazione")
   public static final String PRIVATE_CONSTANT_37 = "*/wt.inf.team.NmOrganizationServiceEvent/REMOVE_ROLE";

   @RBEntry("Aggiungi ruolo al team ciclo di vita")
   public static final String PRIVATE_CONSTANT_38 = "*/wt.team.TeamServiceEvent/ADD_ROLE";

   @RBEntry("Rimuovi ruolo dal team ciclo di vita")
   public static final String PRIVATE_CONSTANT_39 = "*/wt.team.TeamServiceEvent/REMOVE_ROLE";

   @RBEntry("Aggiungi ruolo al team contesto")
   public static final String PRIVATE_CONSTANT_86 = "*/wt.inf.team.ContainerTeamServiceEvent/ADD_ROLE";

   @RBEntry("Rimuovi ruolo dal team contesto")
   public static final String PRIVATE_CONSTANT_87 = "*/wt.inf.team.ContainerTeamServiceEvent/REMOVE_ROLE";

   @RBEntry("Modifica gruppo")
   public static final String PRIVATE_CONSTANT_40 = "*/wt.org.OrganizationServicesEvent/MEMBERSHIP_CHANGE";

   @RBEntry("Modifica password utente")
   public static final String PRIVATE_CONSTANT_41 = "*/wt.org.OrganizationServicesEvent/PASSWORD_CHANGE";

   @RBEntry("Modifica data di scadenza (classico)")
   public static final String PRIVATE_CONSTANT_42 = "*/wt.projmgmt.ProjMgmtServiceEvent/DEADLINE_CHANGE";

   @RBEntry("Modifica data di completamento (classico)")
   public static final String PRIVATE_CONSTANT_43 = "*/wt.projmgmt.ProjMgmtServiceEvent/FINISH_CHANGE";

   @RBEntry("Modifica proprietario (classico)")
   public static final String PRIVATE_CONSTANT_44 = "*/wt.projmgmt.ProjMgmtServiceEvent/OWNER_CHANGE";

   @RBEntry("Modifica percentuale completata (classico)")
   public static final String PRIVATE_CONSTANT_45 = "*/wt.projmgmt.ProjMgmtServiceEvent/PERCENT_CHANGE";

   @RBEntry("Modifica stato del ciclo di vita (classico)")
   public static final String PRIVATE_CONSTANT_46 = "*/wt.projmgmt.ProjMgmtServiceEvent/STATE_CHANGE";

   @RBEntry("Modifica stato (classico)")
   public static final String PRIVATE_CONSTANT_47 = "*/wt.projmgmt.ProjMgmtServiceEvent/STATUS_CHANGE";

   @RBEntry("Modifica data di completamento")
   public static final String PRIVATE_CONSTANT_75 = "*/com.ptc.projectmanagement.plan.ProjectManagementEvent/EPP_FINISH_CHANGE";

   @RBEntry("Modifica proprietario")
   public static final String PRIVATE_CONSTANT_76 = "*/com.ptc.projectmanagement.plan.ProjectManagementEvent/EPP_OWNER_CHANGE";

   @RBEntry("Modifica percentuale completata")
   public static final String PRIVATE_CONSTANT_77 = "*/com.ptc.projectmanagement.plan.ProjectManagementEvent/EPP_PERCENT_CHANGE";

   @RBEntry("Modifica rischio")
   public static final String PRIVATE_CONSTANT_78 = "*/com.ptc.projectmanagement.plan.ProjectManagementEvent/EPP_RISK_CHANGE";

   @RBEntry("Modifica stato")
   public static final String PRIVATE_CONSTANT_79 = "*/com.ptc.projectmanagement.plan.ProjectManagementEvent/EPP_STATUS_CHANGE";

   @RBEntry("Modifica scadenza")
   public static final String PRIVATE_CONSTANT_80 = "*/com.ptc.projectmanagement.plan.ProjectManagementEvent/EPP_DEADLINE_CHANGE";

   @RBEntry("Modifica di stato del ciclo di vita")
   public static final String PLANACTIVITY_STATE_CHANGE_EVENT = "*/com.ptc.projectmanagement.plan.ProjectManagementEvent/PLANACTIVITY_STATE_CHANGE";

   @RBEntry("Visualizza rappresentazioni")
   public static final String PRIVATE_CONSTANT_48 = "*/wt.representation.ViewEvent/VIEW";

   @RBEntry("Inviato in stampa")
   public static final String SENT_TO_PRINT = "*/wt.representation.SentToPrintEvent/SENT_TO_PRINT";

   @RBEntry("Rollback iterazione")
   public static final String PRIVATE_CONSTANT_49 = "*/wt.vc.VersionControlServiceEvent/POST_ROLLBACK";

   @RBEntry("Elimina")
   public static final String PRIVATE_CONSTANT_50 = "*/wt.vc.VersionControlServiceEvent/PRE_ROLLUP";

   @RBEntry("Modifica variabile attività di workflow")
   public static final String PRIVATE_CONSTANT_51 = "*/wt.workflow.engine.WfEngineServiceEvent/ACTIVITY_CONTEXT_CHANGED";

   @RBEntry("Modifica stato attività di workflow")
   public static final String PRIVATE_CONSTANT_52 = "*/wt.workflow.engine.WfEngineServiceEvent/ACTIVITY_STATE_CHANGED";

   @RBEntry("Modifica variabile di workflow")
   public static final String PRIVATE_CONSTANT_53 = "*/wt.workflow.engine.WfEngineServiceEvent/PROCESS_CONTEXT_CHANGED";

   @RBEntry("Modifica stato di workflow")
   public static final String PRIVATE_CONSTANT_54 = "*/wt.workflow.engine.WfEngineServiceEvent/PROCESS_STATE_CHANGED";

   @RBEntry("Nuova discussione")
   public static final String PRIVATE_CONSTANT_55 = "*/wt.workflow.forum.ForumServiceEvent/NEW_DISCUSSION";

   @RBEntry("Modifica preferenza")
   public static final String PRIVATE_CONSTANT_84 = "*/wt.preference.PreferenceServiceEvent/POST_UPDATE";

   @RBEntry("Riconoscimento scaricamento etichetta di sicurezza")
   public static final String PRIVATE_CONSTANT_85 = "*/wt.audit.AuditServiceEvent/SECURITY_LABEL_DOWNLOAD_ACK";

    /*
     * All Subscription UI events are done via categories and are listed at the
     * beginning of this file as <CATEGORY_NAME>.value. CATEGORY_NAME is
     * associated with that seen in codebase\wt\notify\subscriptionConfig.xml.
     * Event labels to be recorded in the AuditEvent database table are
     * listed above as actual event strings. Those that are not recorded are
     * listed below with a ** instead of * /. Both the Subscription UI and
     * StandardEventNotificationDelegate will call the notification APIs,
     * getLocalizedEvent or getLocalizedCategory. These APIs will do the extra
     * check to see if an event with ** exists.
     *
     * Task 1417615-03 addresses the issue of how the audit event picker is
     * currently using this file and what needs to be done in the future. Once
     * this task has been addressed, these events can be changed to be actual
     * events (i.e. the extra * can be removed and extra code to append * in
     * front of the incoming event can be removed from the notification APIs,
     * getLocalizedEvent and getLocalizedCategory.
     */

   @RBEntry("Scadenza accordo prossima")
   public static final String PRIVATE_CONSTANT_56 = "**wt.access.agreement.AgreementServiceEvent/AGREEMENT_PENDING_EXPIRATION";

   @RBEntry("Scadenza accordo")
   public static final String PRIVATE_CONSTANT_57 = "**wt.access.agreement.AgreementServiceEvent/AGREEMENT_EXPIRED";

   @RBEntry("Modifica stato del ciclo di vita notifica di modifica:")
   public static final String PRIVATE_CONSTANT_58 = "**wt.change2.ChangeService2Event/CN_STATE_CHANGED";

   @RBEntry("Imposta stato task di modifica")
   public static final String PRIVATE_CONSTANT_59 = "**wt.change2.ChangeService2Event/CA_STATE_CHANGED";

   @RBEntry("Formalizza report di problema o soluzione temporanea")
   public static final String PRIVATE_CONSTANT_60 = "**wt.change2.ChangeService2Event/ISSUE_FORMALIZED";

   @RBEntry("Annulla formalizzazione report di problema o soluzione temporanea")
   public static final String PRIVATE_CONSTANT_61 = "**wt.change2.ChangeService2Event/ISSUE_UNFORMALIZED";

   @RBEntry("Condividi")
   public static final String PRIVATE_CONSTANT_62 = "**wt.events.summary.NewShareSummaryEvent/";

   @RBEntry("Elimina")
   public static final String PRIVATE_CONSTANT_63 = "**wt.projmgmt.ProjMgmtServiceEvent/OBJECT_DELETION";

   @RBEntry("Data di scadenza mancata")
   public static final String PRIVATE_CONSTANT_64 = "**wt.projmgmt.ProjMgmtServiceEvent/DEADLINE";

   @RBEntry("Giorni dopo la data di scadenza")
   public static final String PRIVATE_CONSTANT_65 = "**wt.projmgmt.ProjMgmtServiceEvent/TIME_PAST_DEADLINE";

   @RBEntry("Giorni alla data di scadenza")
   public static final String PRIVATE_CONSTANT_66 = "**wt.projmgmt.ProjMgmtServiceEvent/TIME_TO_DEADLINE";

   @RBEntry("Scadenza superata")
   public static final String PRIVATE_CONSTANT_81 = "**com.ptc.projectmanagement.plan.ProjectManagementEvent/EPP_MISSED_DEADLINE";

   @RBEntry("Dopo la scadenza")
   public static final String PRIVATE_CONSTANT_82 = "**com.ptc.projectmanagement.plan.ProjectManagementEvent/EPP_DEADLINE_AFTER";

   @RBEntry("Prima della scadenza")
   public static final String PRIVATE_CONSTANT_83 = "**com.ptc.projectmanagement.plan.ProjectManagementEvent/EPP_DEADLINE_BEFORE";

   @RBEntry("Salva rappresentazione")
   public static final String PRIVATE_CONSTANT_67 = "**com.ptc.wvs.server.loader.GraphicsServerLoaderServiceEvent/REPRESENTATION_SAVED";

   @RBEntry("Pubblicazione non riuscita")
   public static final String PRIVATE_CONSTANT_68 = "**com.ptc.wvs.server.publish.PublishServiceEvent/PUBLISH_NOT_SUCCESSFUL";

   @RBEntry("Pubblicazione completata")
   public static final String PRIVATE_CONSTANT_69 = "**com.ptc.wvs.server.publish.PublishServiceEvent/PUBLISH_SUCCESSFUL";

   @RBEntry("Check-In dal progetto")
   public static final String PRIVATE_CONSTANT_70 = "**wt.vc.VersionControlServiceEvent/POST_MERGE";

   @RBEntry("Check-In dal progetto")
   public static final String PRIVATE_CONSTANT_71 = "**wt.sandbox.SandboxServiceCheckinEvent/POST_SB_CHECKIN_EVENT";

   @RBEntry("Aggiornamento dati di origine illustrazione")
   public static final String PRIVATE_CONSTANT_88 = "**wt.events.summary.SourceUpdatedSummaryEvent/";

   @RBEntry("Nuova iterazione")
   public static final String NEW_ITERATION_EVENT_LABEL = "**wt.vc.VersionControlServiceEvent/NEW_ITERATION";

   /**
    * This following three entries are required to support migration of X-05 systems with Folder subscription
    * entries on the Notification queue to X-10 (or later). This entries can be removed once support is
    * no longer required for migrating X-05 systems.
    **/
   @RBEntry("Creazione nella cartella")
   public static final String PRIVATE_CONSTANT_72 = "**wt.folder.FolderSubscriptionEvent/POST_CREATE_IN_FOLDER";

   @RBEntry("Check-In nella cartella")
   public static final String PRIVATE_CONSTANT_73 = "**wt.folder.FolderSubscriptionEvent/CHECK_IN_IN_FOLDER";

   @RBEntry("Modifica stato del ciclo di vita nella cartella")
   public static final String PRIVATE_CONSTANT_74 = "**wt.folder.FolderSubscriptionEvent/STATE_CHANGE_IN_FOLDER";

   /**
    * This entry is required to support updating audit event records from pre-Windchill 10 systems with
    * an event label of "New Revision" to "Revise". This entry can be removed once support is
    * no longer required for migrating 9.0 and 9.1 systems.
    **/
   @RBEntry("Nuova revisione")
   @RBComment("Event label which was used prior to Windchill 10.0 for the NEW REVISION category.  This should exactly match the NEW_REVISION resource value from the Windchill 9.1 release. A WinRU task updates each audit record whose eventLabel matches PRE_WINDCHILL_10_NEW_REVISION to the current value of NEW_REVISION.")
   public static final String PRE_WINDCHILL_10_NEW_REVISION = "PRE_WINDCHILL_10_NEW_REVISION";
}
