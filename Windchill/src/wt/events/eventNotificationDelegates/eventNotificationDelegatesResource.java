/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.events.eventNotificationDelegates;

import wt.util.resource.*;

@RBUUID("wt.events.eventNotificationDelegates.eventNotificationDelegatesResource")
public final class eventNotificationDelegatesResource extends WTListResourceBundle {
   @RBEntry("{0}")
   @RBComment("Do not need to be translated ")
   @RBArgComment0("a palce holder")
   public static final String DUMMY_TEXT = "1";
}
