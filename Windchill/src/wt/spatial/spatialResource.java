/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.spatial;

import wt.util.resource.*;

/**
 * String resources for wt.spatial package.
 *
 * <BR>
 * <BR>
 * <B>Supported API: </B>false <BR>
 * <BR>
 * <B>Extendable: </B>false
 */
@RBUUID("wt.spatial.spatialResource")
public final class spatialResource extends WTListResourceBundle {

   /**
    * Message used when the proximity object in the spatial filter is invalid.
    */
   @RBEntry("The selected proximity object in the spatial filter does not exist in the current structure. Filtering cannot be performed.")
   public static final String PROXIMITY_OBJECT_INVALID= "PROXIMITY_OBJECT_INVALID";
   
   /**
    * Message used when incorrect octree data is detected, e.g. zero volume boxes in the octree.
    */
   @RBEntry("The object being filtered has incorrect octree data.")
   public static final String INCORRECT_OCTREE_DATA= "INCORRECT_OCTREE_DATA";
}
