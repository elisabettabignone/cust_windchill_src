/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.manager.jmx;

import wt.util.resource.*;

@RBUUID("wt.manager.jmx.jmxResource")
public final class jmxResource_it extends WTListResourceBundle {
   @RBEntry("Eventi notevoli del ciclo di vita dei method server")
   public static final String METHOD_SERVER_DATA_NOTIF_INFO_DESCR = "1";

   @RBEntry("Aggiunto method server '{0}'. URL JMX remoto: '{1}'. Avvio: {2}.")
   public static final String METHOD_SERVER_ADDED_NOTIF_MSG = "2";

   @RBEntry("Method server '{0}' rimosso.")
   public static final String METHOD_SERVER_REMOVED_NOTIF_MSG = "3";

   @RBEntry("Connessione JMX al method server '{0}' non riuscita.")
   public static final String METHOD_SERVER_JMX_CONN_FAILED_NOTIF_MSG = "4";

   @RBEntry("Dati correlati per la notifica evento del ciclo di vita dei method server")
   public static final String METHOD_SERVER_NOTIF_USER_DATA_TYPE_DESCR = "5";

   @RBEntry("Nome visualizzato del method server")
   public static final String METHOD_SERVER_NOTIF_USER_DATA_DISPLAY_NAME_ITEM_DESCR = "6";

   @RBEntry("Nome del servizio method server")
   public static final String METHOD_SERVER_NOTIF_USER_DATA_NAME_ITEM_DESCR = "7";

   @RBEntry("ID JVM del method server")
   public static final String METHOD_SERVER_NOTIF_USER_DATA_JVM_ID_ITEM_DESCR = "8";

   @RBEntry("URL del server JMX del method server")
   public static final String METHOD_SERVER_NOTIF_USER_DATA_REMOTE_JMX_URL_ITEM_DESCR = "9";

   @RBEntry("Ora di avvio del method server")
   public static final String METHOD_SERVER_NOTIF_USER_DATA_START_DATE_ITEM_DESCR = "10";

   @RBEntry("Specifica se una connessione JMX al method server è stata stabilita dal server manager e se il risultato della connessione è stato negativo")
   public static final String METHOD_SERVER_NOTIF_USER_DATA_JMX_CONNECTION_FAILED_ITEM_DESCR = "11";

   @RBEntry("Eventi notevoli del ciclo di vita dei server manager")
   public static final String SERVER_MANAGER_NOTIF_INFO_DESCR = "12";

   @RBEntry("Aggiunto server manager slave: {0}")
   public static final String SERVER_MANAGER_ADDED_NOTIF_MSG = "13";

   @RBEntry("Rimosso server manager slave: {0}")
   public static final String SERVER_MANAGER_REMOVED_NOTIF_MSG = "14";

   @RBEntry("Eventi di avvio/arresto del server manager")
   public static final String SERVER_MANAGER_MONITOR_NOTIFY_MSG = "15";

   @RBEntry("Arresto del server manager completato.")
   @RBComment("Message displayed when the server manager was successfully stopped.")
   public static final String SERVER_MGR_MONITOR_STOP_SUCCESS = "16";

   @RBEntry("Impossibile arrestare il server manager.")
   @RBComment("Message displayed when the server manager stop was unsuccessful.")
   public static final String SERVER_MGR_MONITOR_STOP_FAIL = "17";

   @RBEntry("Impossibile avviare il server manager.")
   @RBComment("Message displayed when the server manager start was unsuccessful.")
   public static final String SERVER_MGR_MONITOR_START_FAIL = "18";

   @RBEntry("Il server manager è stato avviato correttamente.")
   @RBComment("Message displayed when the server manager start was successful.")
   public static final String SERVER_MGR_MONITOR_START_SUCCESS = "19";

   @RBEntry("Il server manager è in esecuzione.")
   @RBComment("Message displayed after a successful ping of the server manager.")
   public static final String SERVER_MGR_MONITOR_PING_SUCCESS = "20";

   @RBEntry("Impossibile contattare il server manager.")
   @RBComment("Message displayed after an unsuccessful ping of the server manager.")
   public static final String SERVER_MGR_MONITOR_PING_FAIL = "21";

   @RBEntry("Il server manager è stato chiuso.")
   @RBComment("Notification message when the server manager shutsdown.")
   public static final String SERVER_MGR_MONITOR_SHUTDOWN = "22";

   @RBEntry("Il server manager è stato avviato.")
   @RBComment("Notification message when the server manager starts.")
   public static final String SERVER_MGR_MONITOR_STARTED = "23";

   @RBEntry("Numero di byte inviati al server manager")
   @RBComment("Description for number of bytes sent to the server manager")
   public static final String SERVER_MGR_INFO_BYTES_RECIEVED = "24";

   @RBEntry("Numero di byte inviati dal server manager")
   @RBComment("Description for number of bytes sent by the server manager")
   public static final String SERVER_MGR_INFO_BYTES_SENT = "25";

   @RBEntry("Spazio in memoria libero nel server manager")
   @RBComment("Description for amout of free memory in the server manager")
   public static final String SERVER_MGR_INFO_FREE_MEMORY = "26";

   @RBEntry("Numero di chiamate getInfoCall effettuate al server manager")
   @RBComment("Description for number of getInfoCalls to the server manager")
   public static final String SERVER_MGR_INFO_CALLS = "27";

   @RBEntry("Numero di chiamate getServerCall effettuate al server manager")
   @RBComment("Description for number of getServerCalls to the server manager")
   public static final String SERVER_MGR_INFO_GET_SERVER_CALLS = "28";

   @RBEntry("Numero di socket nel server manager")
   @RBComment("Description for number of sockets in the server manager")
   public static final String SERVER_MGR_INFO_NUM_SOCKETS = "29";

   @RBEntry("Numero di chiamate registerServerCall effettuate al server manager")
   @RBComment("Description for number of registerServerCalls to the server manager")
   public static final String SERVER_MGR_INFO_REGISTER_SERVER_CALLS = "30";

   @RBEntry("Numero di chiamate reportDeadServerCall effettuate al server manager")
   @RBComment("Description for number of reportDeadServerCalls to the server manager")
   public static final String SERVER_MGR_INFO_REPORT_DEAD_SERVER_CALLS = "31";

   @RBEntry("Numero di server avviati")
   @RBComment("Description for number of servers launched.")
   public static final String SERVER_MGR_INFO_NUM_SERVERS_LAUNCHED = "32";

   @RBEntry("Data di avvio del server manager")
   @RBComment("Description for when the server manager was last started")
   public static final String SERVER_MGR_INFO_SM_START_DATE = "33";

   @RBEntry("Spazio in memoria totale nel server manager")
   @RBComment("Description for total memory for the server manager")
   public static final String SERVER_MGR_INFO_TOTAL_MEMORY = "34";

   @RBEntry("URL server manager non valido")
   @RBComment("Error message for if the URL to the server manager is bad")
   public static final String SERVER_MGR_MONITOR_BAD_URL = "35";

   @RBEntry("Errore durante il recupero delle informazioni sul server manager")
   @RBComment("Error message when exception ocurrs getting server manager info")
   public static final String SERVER_MGR_MONITOR_INFO_ERROR = "36";

   @RBEntry("Informazioni sul server manager")
   @RBComment("Description used for server manager composite data.")
   public static final String SERVER_MGR_INFO_DESC = "37";

   @RBEntry("Iniziato il riavvio di tutti i method server locali.")
   @RBComment("Message that restart of all local method servers has started.")
   public static final String METHOD_SERVER_RESTART_ALL_LOCAL_STARTED = "38";

   @RBEntry("I method server locali sono stati riavviati")
   @RBComment("Message that restart of all local method servers was successful.")
   public static final String METHOD_SERVER_RESTART_ALL_LOCAL_SUCCESS = "39";

   @RBEntry("Iniziato il riavvio di tutti i method server nel cluster")
   @RBComment("Message that restart of all method servers in the cluster has started.")
   public static final String METHOD_SERVER_RESTART_CLUSTER_STARTED = "40";

   @RBEntry("I method server nel cluster sono stati riavviati")
   @RBComment("Message that restart of all method servers in the cluster was successful.")
   public static final String METHOD_SERVER_RESTART_CLUSTER_SUCCESS = "41";

   @RBEntry("Impossibile riavviare i method server che seguono: {0}")
   @RBComment("Error message when exception occurs restarting all local method servers")
   public static final String METHOD_SERVER_RESTART_ALL_LOCAL_FAILURE = "42";

   @RBEntry("Non è stato possibile riavviare alcuni method server nei nodi che seguono: {0}")
   @RBComment("Error message when exception occurs restarting all method servers in the cluster.")
   public static final String METHOD_SERVER_RESTART_CLUSTER_FAILURE = "43";

   @RBEntry("Impossibile impostare il livello di log nei method server che seguono: {0}")
   @RBComment("Error message when exception occurs setting log level for all local method servers")
   public static final String SET_LEVEL_IN_ALL_LOCAL_SERVERS_FAILURE = "44";

   @RBEntry("Si sono verificati errori durante l'impostazione del livello di log nei nodi che seguono: {0}")
   @RBComment("Error message when exception occurs setting log level for all method servers or server managers in the cluster.")
   public static final String SET_LEVEL_IN_CLUSTER_SERVERS_FAILURE = "45";

   @RBEntry("Impossibile impostare la proprietà nei method server che seguono: {0}")
   public static final String SET_PROPERTY_IN_ALL_LOCAL_SERVERS_FAILURE = "46";

   @RBEntry("Si sono verificati errori durante l'impostazione delle proprietà nei nodi che seguono: {0}")
   public static final String SET_PROPERTY_IN_CLUSTER_SERVERS_FAILURE = "47";

   @RBEntry("I seguenti nodi non sono riusciti a inviare le informazioni MBean: {0}")
   public static final String SEND_ALL_MBEANS_FROM_CLUSTER_SERVERS_FAILURE = "48";

   @RBEntry("I seguenti nodi non sono riusciti a inviare i log: {0}")
   public static final String SEND_LOG_DIRS_FAILURE_FROM_CLUSTER_SERVERS_FAILURE = "49";

   @RBEntry("Impossibile inviare via e-mail i contesti di metodo attivi nei method server: {0}")
   public static final String EMAIL_LOCAL_ACTIVE_CONTEXTS_FAILURE = "50";

   @RBEntry("Si sono verificati errori durante l'invio via e-mail dei contesti di metodo attivi sui nodi che seguono: {0}")
   public static final String EMAIL_ALL_ACTIVE_CONTEXTS_FAILURE = "51";

   @RBEntry("Dati sull'istanza server manager")
   public static final String SM_INFO_TYPE_DESCR = "52";

   @RBEntry("Nome del server manager")
   public static final String SM_INFO_TYPE_NAME_ITEM_DESCR = "53";

   @RBEntry("Il server manager è il master di cache?")
   public static final String SM_INFO_TYPE_MASTER_ITEM_DESCR = "54";

   @RBEntry("URL servizio JMX del server manager")
   public static final String SM_INFO_TYPE_JMX_URL_ITEM_DESCR = "55";

   @RBEntry("Impossibile inviare MBean dai method server che seguono: {0}")
   public static final String SEND_MS_MBEANS_FAILURE = "56";

   @RBEntry("ID del processo JVM del server manager")
   public static final String SERVER_MGR_INFO_JVM_ID = "57";

   @RBEntry("URL JMX di server manager per l'accesso remoto.")
   public static final String SERVER_MGR_INFO_REMOTE_JMX_URL = "58";

   @RBEntry("I seguenti nodi non sono riusciti a generare le informazioni MBean: {0}")
   public static final String SAVE_ALL_MBEANS_FROM_CLUSTER_SERVERS_FAILURE = "59";

   @RBEntry("Impossibile generare MBean dai method server che seguono: {0}")
   public static final String SAVE_MS_MBEANS_FAILURE = "60";

   @RBEntry("Data di avvio del server manager")
   public static final String SM_INFO_TYPE_START_DATE_ITEM_DESCR = "61";

   @RBEntry("Il server manager slave è diventato master della cache")
   public static final String SERVER_MANAGER_BECAME_MASTER_NOTIF_MSG = "62";

   @RBEntry("Per il server manager principale è stato ripristinato il server manager slave")
   public static final String SERVER_MANAGER_BECAME_SLAVE_NOTIF_MSG = "63";
}
