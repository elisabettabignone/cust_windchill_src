/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.manager.jmx;

import wt.util.resource.*;

@RBUUID("wt.manager.jmx.jmxResource")
public final class jmxResource extends WTListResourceBundle {
   @RBEntry("Important method server lifecycle events")
   public static final String METHOD_SERVER_DATA_NOTIF_INFO_DESCR = "1";

   @RBEntry("Method server '{0}' added. Remote JMX URL is '{1}'. Started at {2}.")
   public static final String METHOD_SERVER_ADDED_NOTIF_MSG = "2";

   @RBEntry("Method server '{0}' removed.")
   public static final String METHOD_SERVER_REMOVED_NOTIF_MSG = "3";

   @RBEntry("JMX connection to method server '{0}' failed.")
   public static final String METHOD_SERVER_JMX_CONN_FAILED_NOTIF_MSG = "4";

   @RBEntry("Related data for method server lifecycle event notification")
   public static final String METHOD_SERVER_NOTIF_USER_DATA_TYPE_DESCR = "5";

   @RBEntry("Display name of method server")
   public static final String METHOD_SERVER_NOTIF_USER_DATA_DISPLAY_NAME_ITEM_DESCR = "6";

   @RBEntry("Name of method server service")
   public static final String METHOD_SERVER_NOTIF_USER_DATA_NAME_ITEM_DESCR = "7";

   @RBEntry("JVM id of method server")
   public static final String METHOD_SERVER_NOTIF_USER_DATA_JVM_ID_ITEM_DESCR = "8";

   @RBEntry("URL of method server's JMX server")
   public static final String METHOD_SERVER_NOTIF_USER_DATA_REMOTE_JMX_URL_ITEM_DESCR = "9";

   @RBEntry("Time method server started")
   public static final String METHOD_SERVER_NOTIF_USER_DATA_START_DATE_ITEM_DESCR = "10";

   @RBEntry("Whether a JMX connection was established from the server manager to method server and this connection subsequently failed")
   public static final String METHOD_SERVER_NOTIF_USER_DATA_JMX_CONNECTION_FAILED_ITEM_DESCR = "11";

   @RBEntry("Important server manager lifecycle events")
   public static final String SERVER_MANAGER_NOTIF_INFO_DESCR = "12";

   @RBEntry("Slave server manager added: {0}")
   public static final String SERVER_MANAGER_ADDED_NOTIF_MSG = "13";

   @RBEntry("Slave server manager removed: {0}")
   public static final String SERVER_MANAGER_REMOVED_NOTIF_MSG = "14";

   @RBEntry("Server manager start/stop events")
   public static final String SERVER_MANAGER_MONITOR_NOTIFY_MSG = "15";

   @RBEntry("Server manager stopped successfully.")
   @RBComment("Message displayed when the server manager was successfully stopped.")
   public static final String SERVER_MGR_MONITOR_STOP_SUCCESS = "16";

   @RBEntry("Failed to stop server manager.")
   @RBComment("Message displayed when the server manager stop was unsuccessful.")
   public static final String SERVER_MGR_MONITOR_STOP_FAIL = "17";

   @RBEntry("Failed to start server manager.")
   @RBComment("Message displayed when the server manager start was unsuccessful.")
   public static final String SERVER_MGR_MONITOR_START_FAIL = "18";

   @RBEntry("Server manager started successfully.")
   @RBComment("Message displayed when the server manager start was successful.")
   public static final String SERVER_MGR_MONITOR_START_SUCCESS = "19";

   @RBEntry("Server manager is up.")
   @RBComment("Message displayed after a successful ping of the server manager.")
   public static final String SERVER_MGR_MONITOR_PING_SUCCESS = "20";

   @RBEntry("Could not contact server manager.")
   @RBComment("Message displayed after an unsuccessful ping of the server manager.")
   public static final String SERVER_MGR_MONITOR_PING_FAIL = "21";

   @RBEntry("Server manager has shutdown.")
   @RBComment("Notification message when the server manager shutsdown.")
   public static final String SERVER_MGR_MONITOR_SHUTDOWN = "22";

   @RBEntry("Server manager has started.")
   @RBComment("Notification message when the server manager starts.")
   public static final String SERVER_MGR_MONITOR_STARTED = "23";

   @RBEntry("Number of bytes sent to the server manager")
   @RBComment("Description for number of bytes sent to the server manager")
   public static final String SERVER_MGR_INFO_BYTES_RECIEVED = "24";

   @RBEntry("Number of bytes sent by the server manager")
   @RBComment("Description for number of bytes sent by the server manager")
   public static final String SERVER_MGR_INFO_BYTES_SENT = "25";

   @RBEntry("Amount of free memory in the server manager")
   @RBComment("Description for amout of free memory in the server manager")
   public static final String SERVER_MGR_INFO_FREE_MEMORY = "26";

   @RBEntry("Number of getInfoCalls made to the server manager")
   @RBComment("Description for number of getInfoCalls to the server manager")
   public static final String SERVER_MGR_INFO_CALLS = "27";

   @RBEntry("Number of getServerCalls made to the server manager")
   @RBComment("Description for number of getServerCalls to the server manager")
   public static final String SERVER_MGR_INFO_GET_SERVER_CALLS = "28";

   @RBEntry("Number of sockets in the server manager")
   @RBComment("Description for number of sockets in the server manager")
   public static final String SERVER_MGR_INFO_NUM_SOCKETS = "29";

   @RBEntry("Number of registerServerCalls made to the server manager")
   @RBComment("Description for number of registerServerCalls to the server manager")
   public static final String SERVER_MGR_INFO_REGISTER_SERVER_CALLS = "30";

   @RBEntry("Number of reportDeadServerCalls made to the server manager")
   @RBComment("Description for number of reportDeadServerCalls to the server manager")
   public static final String SERVER_MGR_INFO_REPORT_DEAD_SERVER_CALLS = "31";

   @RBEntry("Number of servers launched")
   @RBComment("Description for number of servers launched.")
   public static final String SERVER_MGR_INFO_NUM_SERVERS_LAUNCHED = "32";

   @RBEntry("Start date of the server manager")
   @RBComment("Description for when the server manager was last started")
   public static final String SERVER_MGR_INFO_SM_START_DATE = "33";

   @RBEntry("Amount of memory in the server manager")
   @RBComment("Description for total memory for the server manager")
   public static final String SERVER_MGR_INFO_TOTAL_MEMORY = "34";

   @RBEntry("Invalid server manager URL")
   @RBComment("Error message for if the URL to the server manager is bad")
   public static final String SERVER_MGR_MONITOR_BAD_URL = "35";

   @RBEntry("Error retrieving server manager info")
   @RBComment("Error message when exception ocurrs getting server manager info")
   public static final String SERVER_MGR_MONITOR_INFO_ERROR = "36";

   @RBEntry("Contains info about the server manager")
   @RBComment("Description used for server manager composite data.")
   public static final String SERVER_MGR_INFO_DESC = "37";

   @RBEntry("Restart of all local method servers has begun.")
   @RBComment("Message that restart of all local method servers has started.")
   public static final String METHOD_SERVER_RESTART_ALL_LOCAL_STARTED = "38";

   @RBEntry("Local method servers were successfully restarted")
   @RBComment("Message that restart of all local method servers was successful.")
   public static final String METHOD_SERVER_RESTART_ALL_LOCAL_SUCCESS = "39";

   @RBEntry("Restart of all method servers in the cluster has begun.")
   @RBComment("Message that restart of all method servers in the cluster has started.")
   public static final String METHOD_SERVER_RESTART_CLUSTER_STARTED = "40";

   @RBEntry("All method servers in the cluster were successfully restarted.")
   @RBComment("Message that restart of all method servers in the cluster was successful.")
   public static final String METHOD_SERVER_RESTART_CLUSTER_SUCCESS = "41";

   @RBEntry("Failed to restart method server(s): {0}")
   @RBComment("Error message when exception occurs restarting all local method servers")
   public static final String METHOD_SERVER_RESTART_ALL_LOCAL_FAILURE = "42";

   @RBEntry("There were failures restarting method servers on the following node(s): {0}")
   @RBComment("Error message when exception occurs restarting all method servers in the cluster.")
   public static final String METHOD_SERVER_RESTART_CLUSTER_FAILURE = "43";

   @RBEntry("Failed to set log level in method server(s): {0}")
   @RBComment("Error message when exception occurs setting log level for all local method servers")
   public static final String SET_LEVEL_IN_ALL_LOCAL_SERVERS_FAILURE = "44";

   @RBEntry("There were failures setting log levels on the following node(s): {0}")
   @RBComment("Error message when exception occurs setting log level for all method servers or server managers in the cluster.")
   public static final String SET_LEVEL_IN_CLUSTER_SERVERS_FAILURE = "45";

   @RBEntry("Failed to set property in method server(s): {0}")
   public static final String SET_PROPERTY_IN_ALL_LOCAL_SERVERS_FAILURE = "46";

   @RBEntry("There were failures settings properties on the following node(s): {0}")
   public static final String SET_PROPERTY_IN_CLUSTER_SERVERS_FAILURE = "47";

   @RBEntry("The following nodes failed to send MBean information: {0}")
   public static final String SEND_ALL_MBEANS_FROM_CLUSTER_SERVERS_FAILURE = "48";

   @RBEntry("The following nodes failed to send logs: {0}")
   public static final String SEND_LOG_DIRS_FAILURE_FROM_CLUSTER_SERVERS_FAILURE = "49";

   @RBEntry("Failed to email active method contexts in method servers: {0}")
   public static final String EMAIL_LOCAL_ACTIVE_CONTEXTS_FAILURE = "50";

   @RBEntry("There were failures emailing active method contexts on the following nodes: {0}")
   public static final String EMAIL_ALL_ACTIVE_CONTEXTS_FAILURE = "51";

   @RBEntry("Data on server manager instance")
   public static final String SM_INFO_TYPE_DESCR = "52";

   @RBEntry("Name of server manager")
   public static final String SM_INFO_TYPE_NAME_ITEM_DESCR = "53";

   @RBEntry("Whether server manager is the cache master")
   public static final String SM_INFO_TYPE_MASTER_ITEM_DESCR = "54";

   @RBEntry("JMX service URL of server manager")
   public static final String SM_INFO_TYPE_JMX_URL_ITEM_DESCR = "55";

   @RBEntry("Failed to send MBeans from method servers: {0}")
   public static final String SEND_MS_MBEANS_FAILURE = "56";

   @RBEntry("Id of server manager JVM process")
   public static final String SERVER_MGR_INFO_JVM_ID = "57";

   @RBEntry("JMX URL of server manager for remote access")
   public static final String SERVER_MGR_INFO_REMOTE_JMX_URL = "58";

   @RBEntry("The following nodes failed to dump MBean information: {0}")
   public static final String SAVE_ALL_MBEANS_FROM_CLUSTER_SERVERS_FAILURE = "59";

   @RBEntry("Failed to dump MBeans from method servers: {0}")
   public static final String SAVE_MS_MBEANS_FAILURE = "60";

   @RBEntry("Start date of server manager")
   public static final String SM_INFO_TYPE_START_DATE_ITEM_DESCR = "61";

   @RBEntry("Slave server manager became cache master")
   public static final String SERVER_MANAGER_BECAME_MASTER_NOTIF_MSG = "62";

   @RBEntry("Master server manager reverted to slave")
   public static final String SERVER_MANAGER_BECAME_SLAVE_NOTIF_MSG = "63";
}
