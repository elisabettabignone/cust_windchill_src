/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.manager;

import wt.util.resource.*;

@RBUUID("wt.manager.managerResource")
public final class managerResource_it extends WTListResourceBundle {
   @RBEntry("Codebase incompatibile")
   public static final String INCOMPATIBLE_CODEBASE_TITLE = "0";

   @RBEntry("ATTENZIONE: la versione del codebase locale è \"{0}\", ma la versione del server è \"{1}\". È possibile che siano presenti file installati localmente non compatibili con quelli installati sul server.")
   public static final String INCOMPATIBLE_CODEBASE_MSG = "1";

   @RBEntry("Impossibile avviare {0}")
   public static final String UNABLE_TO_START = "2";

   @RBEntry("Tempo scaduto per l'avvio di {0}.")
   public static final String START_TIMEOUT = "3";

   @RBEntry("accesso negato")
   public static final String ACCESS_DENIED = "4";

   @RBEntry("Nome servizio sconosciuto: {0}")
   public static final String UNKNOWN_SERVICE_NAME = "5";

   @RBEntry("Impossibile registrare server")
   public static final String UNABLE_REGISTER_SERVER = "6";

   @RBEntry("Impossibile trovare server")
   public static final String UNABLE_GET_SERVER = "7";

   @RBEntry("Impossibile trovare le informazioni sul server manager")
   public static final String UNABLE_GET_SERVER_MANAGER_INFO = "8";

   @RBEntry("Impossibile eseguire il ping sul server manager")
   public static final String PING_FAILED = "9";

   @RBEntry("Impossibile arrestare il server manager")
   public static final String STOP_FAILED = "10";

   @RBEntry("Impossibile individuare il server manager")
   public static final String CANNOT_LOCATE_SERVER_MANAGER = "11";

   @RBEntry("Impossibile individuare il registro del server manager")
   public static final String CANNOT_LOCATE_REGISTRY = "12";
}
