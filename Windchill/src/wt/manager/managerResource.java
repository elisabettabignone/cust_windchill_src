/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.manager;

import wt.util.resource.*;

@RBUUID("wt.manager.managerResource")
public final class managerResource extends WTListResourceBundle {
   @RBEntry("Incompatible Codebase")
   public static final String INCOMPATIBLE_CODEBASE_TITLE = "0";

   @RBEntry("WARNING: local codebase is version \"{0}\", but server codebase is version \"{1}\". You may have files installed locally that are no longer compatible with those on the server.")
   public static final String INCOMPATIBLE_CODEBASE_MSG = "1";

   @RBEntry("Unable to start {0}")
   public static final String UNABLE_TO_START = "2";

   @RBEntry("Start of {0} timed out.")
   public static final String START_TIMEOUT = "3";

   @RBEntry("access denied")
   public static final String ACCESS_DENIED = "4";

   @RBEntry("Unknown service name: {0}")
   public static final String UNKNOWN_SERVICE_NAME = "5";

   @RBEntry("Unable to register server")
   public static final String UNABLE_REGISTER_SERVER = "6";

   @RBEntry("Unable to get server")
   public static final String UNABLE_GET_SERVER = "7";

   @RBEntry("Unable to get server manager info")
   public static final String UNABLE_GET_SERVER_MANAGER_INFO = "8";

   @RBEntry("Unable to ping server manager")
   public static final String PING_FAILED = "9";

   @RBEntry("Unable to stop server manager")
   public static final String STOP_FAILED = "10";

   @RBEntry("Unable to locate server manager")
   public static final String CANNOT_LOCATE_SERVER_MANAGER = "11";

   @RBEntry("Unable to locate server manager's registry")
   public static final String CANNOT_LOCATE_REGISTRY = "12";
}
