/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.units.display;

import wt.util.resource.*;

@RBUUID("wt.units.display.displayResource")
public final class displayResource_it extends WTListResourceBundle {
   @RBEntry("Unità visualizzata invalida: \"{0}\"")
   public static final String SET_DISPLAY_UNIT_FAILED = "0";

   @RBEntry("Nome non valido: \"{0}\"")
   public static final String INITIALIZE_MS_FAILED = "1";

   @RBEntry("Nome non valido: \"{0}\"")
   public static final String INITIALIZE_QOM_NAME_FAILED = "2";

   @RBEntry("Unità di base invalida: \"{0}\"")
   public static final String INITIALIZE_QOM_UNIT_FAILED = "3";
}
