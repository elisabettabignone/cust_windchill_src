/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.units.service;

import wt.util.resource.*;

@RBUUID("wt.units.service.serviceResource")
public final class serviceResource extends WTListResourceBundle {
   @RBEntry("Could not retrieve Measurement Systems.")
   public static final String GET_ALL_MS_NAMES_FAILED = "0";

   @RBEntry("Could not retrieve \"{0}\".")
   public static final String GET_MEASUREMENT_SYSTEM_FAILED = "1";

   @RBEntry("Could not retrieve Measurement Systems.")
   public static final String GET_MEASUREMENT_SYSTEMS_FAILED = "2";

   @RBEntry("Could not retrieve \"{0}\".")
   public static final String GET_QUANTITY_OF_MEASURE_FAILED = "3";

   @RBEntry("Could not retrieve Quantities of Measure.")
   public static final String GET_QUANTITY_OF_MEASURES_FAILED = "4";

   @RBEntry("Could not retrieve Quantities of Measure.")
   public static final String GET_ALL_QOM_NAMES_FAILED = "5";
}
