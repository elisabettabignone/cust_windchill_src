/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.units.service;

import wt.util.resource.*;

@RBUUID("wt.units.service.serviceResource")
public final class serviceResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile recuperare i sistemi di misurazione.")
   public static final String GET_ALL_MS_NAMES_FAILED = "0";

   @RBEntry("Impossibile recuperare \"{0}\".")
   public static final String GET_MEASUREMENT_SYSTEM_FAILED = "1";

   @RBEntry("Impossibile recuperare i sistemi di misurazione.")
   public static final String GET_MEASUREMENT_SYSTEMS_FAILED = "2";

   @RBEntry("Impossibile recuperare \"{0}\".")
   public static final String GET_QUANTITY_OF_MEASURE_FAILED = "3";

   @RBEntry("Impossibile recuperare le quantità di misura.")
   public static final String GET_QUANTITY_OF_MEASURES_FAILED = "4";

   @RBEntry("Impossibile recuperare le quantità di misura.")
   public static final String GET_ALL_QOM_NAMES_FAILED = "5";
}
