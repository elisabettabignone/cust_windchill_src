/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.units;

import wt.util.resource.*;

@RBUUID("wt.units.unitsResource")
public final class unitsResource extends WTListResourceBundle {
   @RBEntry("Incompatible units: \"{0}\" and \"{1}\"")
   public static final String INCOMPATIBLE_UNITS_FAILED = "units0";

   @RBEntry("Invalid number format: \"{0}\"")
   public static final String INVALID_NUMBER_FORMAT = "units1";

   @RBEntry("Invalid unit format: \"{0}\"")
   public static final String INVALID_UNIT_FORMAT = "units2";
}
