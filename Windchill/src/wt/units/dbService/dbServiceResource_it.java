/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.units.dbService;

import wt.util.resource.*;

@RBUUID("wt.units.dbService.dbServiceResource")
public final class dbServiceResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile aggiornare \"{0}\" perché esiste già una versione più recente.")
   public static final String OBSELETE_MEASUREMENT_SYSTEM = "0";

   @RBEntry("Il campo della descrizione è invalido.")
   public static final String UPDATE_MS_DESCRIPTION_FAILED = "1";

   @RBEntry("L'unità \"{0}\" è incompatibile con l'unità interna \"{1}\".")
   public static final String MS_INCOMPATIBLE_UNITS_FAILED = "2";

   @RBEntry("Impossibile aggiornare \"{0}\" perché esiste già una versione più recente.")
   public static final String OBSELETE_QUANTITY_OF_MEASURE = "3";

   @RBEntry("Il campo della descrizione è invalido.")
   public static final String UPDATE_QOM_DESCRIPTION_FAILED = "4";

   @RBEntry("L'unità \"{0}\" è incompatibile con l'unità di base \"{1}\".")
   public static final String QOM_INCOMPATIBLE_UNITS_FAILED = "5";

   @RBEntry("\"{0}\" non può essere eliminato perché è impiegato dall'attributo \"{1}\".")
   public static final String QOM_IS_IN_USE = "6";
}
