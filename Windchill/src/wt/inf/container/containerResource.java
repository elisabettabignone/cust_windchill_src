/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.inf.container;

import wt.util.resource.*;

@RBUUID("wt.inf.container.containerResource")
public final class containerResource extends WTListResourceBundle {
   @RBEntry("The operation:   \"{0}\" failed.")
   @RBComment("WTException: A specified operation failed during installation")
   @RBArgComment0("Name of operation that failed")
   public static final String OPERATION_FAILED = "0";

   @RBEntry("\"{0}\" context cannot be deleted.")
   @RBComment("WTException: Deleting of system containers is not allowed")
   @RBArgComment0("Name of container being deleted")
   public static final String CANT_DELETE_CONTAINER = "1";

   @RBEntry("\"{0}\" cannot contained in \"{1}\".")
   @RBComment("Can not contain in container")
   @RBArgComment0("name of contained object")
   @RBArgComment1("name of container")
   public static final String CONTAINED_IN_CONTAINER = "2";

   @RBEntry("\"{0}\" is not a context")
   @RBComment("Not an instance of WTContainer")
   @RBArgComment0("Name of object assumed to be a container")
   public static final String NOT_CONTAINER_CLASS = "3";

   @RBEntry("System cannot find \"{0}\" context (ok if installation)")
   @RBComment("Warning message printed during method server startup if the exchange or classic container can't be found")
   @RBArgComment0("The name of the container that can't be found.")
   public static final String CANT_FIND_SYSTEM_CONTAINER = "4";

   @RBEntry("Corrupted system: The \"{0}\" context is defined more than once")
   @RBComment("Exception message when the system finds more than one classic or exchange container ")
   @RBArgComment0("Name of the container that is defined more than once")
   public static final String MULTIPLE_SYSTEM_CONTAINERS = "5";

   @RBEntry("Can not initialize \"{0}\" context")
   @RBComment("The container can not be initialized")
   @RBArgComment0("Name of the container that cannot be initialized")
   public static final String NO_INIT_CONTAINER = "6";

   @RBEntry("No context objects in query spec")
   @RBComment("No containers defined in query spec")
   public static final String NO_CONTAINERS_QS = "7";

   @RBEntry("WARNING:  This group was created for use by the Windchill System.  Modification or deletion of this group may have undesirable consequences in Windchill.")
   public static final String DO_NOT_MODIFY = "8";

   @RBEntry("Domain \"{1}\" is not a valid public parent domain for context \"{0}\".")
   @RBComment("Something was wrong with the parent public domain the user is trying to set on the container. ")
   @RBArgComment0("The container that the new parent public domain was supposed to be for")
   @RBArgComment1("The bad parent public domain")
   public static final String BAD_PUBLIC_PARENT = "9";

   @RBEntry("An error occurred while change the public parent domain of context \"{0}\" to domain \"{1}\".")
   @RBArgComment0("The container that the new parent public domain was supposed to be for")
   @RBArgComment1("The bad parent public domain")
   public static final String COULD_NOT_CHANGE_PUBLIC_PARENT = "10";

   @RBEntry("Base domains are not supported for context objects of type: \"{1}\" in context: \"{0}\".")
   @RBArgComment0("The container in which the base domain was looked for")
   @RBArgComment1("The container class that does not have base domains in the container")
   public static final String UNSUPPORTED_BASE_DOMAIN_CONTAINER = "11";

   @RBEntry("Unexpected number of base domains for context type: \"{1}\" in context: \"{0}\". Expected 1, but was: \"{2}\".")
   @RBArgComment0("The container in which the base domain was looked for")
   @RBArgComment1("The container class that has too many base domains in the container")
   public static final String UNEXPECTED_NUMBER_OF_BASE_DOMAINS = "12";

   @RBEntry("Unable to find context for path: \"{0}\".")
   @RBArgComment0("The container path that no container matched")
   public static final String NO_CONTAINER_FOR_PATH = "13";

   @RBEntry("Could not find context for path: \"{0}\" because the context class \"{1}\" could not be found.")
   @RBArgComment0("The container path that had a bad class in it")
   @RBArgComment1("The name of the container class that couldn't be found")
   public static final String CANT_FIND_CONTAINER_CLASS = "14";

   @RBEntry("More than one context matched the context path: \"{0}\". Contact your administrator.")
   @RBArgComment0("The container path that had more than one match")
   public static final String NO_UNIQUE_CONTAINER_FOR_PATH = "15";

   @RBEntry("WTContainerService.changePublicParentDomain should only be called on containers that have already been created. Use WTContainer.setPublicParentDomain to set this property on new instances.")
   public static final String CHANGE_PUBLIC_PARENT_PERSISTENT_ONLY = "16";

   @RBEntry("WTContainerService.makePrivate should only be called on containers that have already been created. Use WTContainer.setPrivateAccess to set this property on new instances.")
   public static final String MAKE_PRIVATE_PERSISTENT_ONLY = "17";

   @RBEntry("WTContainerService.makePublic should only be called on containers that have already been created. Use WTContainer.setPrivateAccess to set this property on new instances.")
   public static final String MAKE_PUBLIC_PERSISTENT_ONLY = "18";

   @RBEntry("The context \"{0}\" could not be made public.")
   public static final String COULD_NOT_MAKE_PUBLIC = "19";

   @RBEntry("The context \"{0}\" could not be made private.")
   public static final String COULD_NOT_MAKE_PRIVATE = "20";

   @RBEntry("WTContainerService.makePrivate is not supported for this container: \"{0}\"")
   public static final String MAKE_PRIVATE_UNSUPPORTED = "21";

   @RBEntry("You cannot use WTContainerHelper.setContainer to change the context of persisted object. You tried to set contained object \"{0}\"'s context to \"{1}\".")
   @RBArgComment0("The persistent contained object.")
   @RBArgComment1("The new container")
   public static final String SET_CONTAINER_UNPERSISTED_ONLY = "22";

   @RBEntry("You cannot use WTContainerHelper.setPrivateAccess to change the privateAccess property of a persistent context. You tried to set context \"{0}\"'s privateAccess property to \"{1}\".")
   @RBArgComment0("The persistent container")
   @RBArgComment1("The value of privateAccess")
   public static final String SET_PRIVATE_ACCESS_UNPERSISTED_ONLY = "23";

   @RBEntry("You cannot use WTContainerHelper.setPublicParentDomain to change the publicParentDomain property of a persistent context. You tried to set context \"{0}\"'s public domain to \"{1}\".")
   @RBArgComment0("The persistent container")
   public static final String SET_PUBLIC_PARENT_UNPERSISTED_ONLY = "24";

   @RBEntry("No Context objects were found that matched search criteria")
   public static final String NO_CONTAINERS_FOUND = "25";

   @RBEntry("Context not found: \"{0}\"")
   @RBArgComment0("The localized name of the classic container (derived from CLASSIC_CONTAINER_NAME below).")
   public static final String CLASSIC_CONTAINER_NOT_FOUND = "26";

   @RBEntry("Context objects of type \"{0}\" cannot be marked for delete.")
   @RBArgComment0("The type of container that can't be marked for delete")
   public static final String CANNOT_M4D_CONTAINER = "27";

   @RBEntry("The mark for deletion of context \"{0}\" failed, due to a problem deleting some of its contents. System message is: \"{1}\".")
   @RBComment("This message is sent by email to the administrator of the container")
   @RBArgComment0("The identity of the container that couldn't be marked for delete.")
   @RBArgComment1("The exception message thrown when trying to mark for delete an object in the container.")
   public static final String M4D_FAILED = "28";

   @RBEntry("The mark for deletion of context \"{0}\" failed, due to a problem deleting some of its contents. System message is: \"{1}\". In addition, a problem prevented the context from being restored. System message is: \"{2}\".")
   @RBComment("This message is sent by email to the administrator of the container")
   @RBArgComment0("The identity of the container that couldn't be marked for delete.")
   @RBArgComment1("The exception message thrown when trying to mark for delete an object in the container.")
   @RBArgComment2("The exception message thrown when trying to restore a container that failed to be marked for delete")
   public static final String M4D_AND_ROLLBACK_FAILED = "29";

   @RBEntry("Mark for delete failure")
   @RBComment("The subject of the email sent to an administrator when a mark for delete fails. (See M4D_FAILED)")
   public static final String M4D_FAILED_SUBJECT = "30";

   @RBEntry("The restore of context \"{0}\" failed, due to a problem restoring some of its contents. System message is: \"{1}\".")
   @RBComment("This message is sent by email to the administrator of the container.")
   @RBArgComment0("The identity of the container that couldn't be restored.")
   @RBArgComment1("The exception message thrown when trying to restore an object in the container.")
   public static final String RESTORE_FAILED = "31";

   @RBEntry("The restore of context \"{0}\" failed, due to a problem restoring some of its contents. System message is: \"{1}\". In addition, a problem prevented the context from being rolled back to \"marked for delete\" status. System message is: \"{2}\".")
   @RBComment("This message is sent by email to the administrator of the container.")
   @RBArgComment0("The identity of the container that couldn't be restored.")
   @RBArgComment1("The exception message thrown when trying to restore an object in the container.")
   @RBArgComment2("The exception message thrown when trying to roll back a container that failed to be restored.")
   public static final String RESTORE_AND_ROLLBACK_FAILED = "32";

   @RBEntry("Restore failure")
   @RBComment("The subject of the email sent to an administrator when a mark for delete fails. (See RESTORE_FAILED)")
   public static final String RESTORE_FAILED_SUBJECT = "33";

   @RBEntry("The emptying of context \"{0}\" failed, due to a problem emptying some of its contents. System message is: \"{1}\".")
   @RBComment("This message is sent by email to the administrator of the container.")
   @RBArgComment0("The identity of the container that couldn't be emptied.")
   @RBArgComment1("The exception message thrown when trying to empty an object in the container.")
   public static final String EMPTY_FAILED = "34";

   @RBEntry("The emptying of context \"{0}\" failed, due to a problem emptying some of its contents. System message is: \"{1}\". In addition, a problem prevented the context from being rolled back to \"marked for delete\" status. System message is: \"{2}\".")
   @RBComment("This message is sent by email to the administrator of the container.")
   @RBArgComment0("The identity of the container that couldn't be emptied.")
   @RBArgComment1("The exception message thrown when trying to empty an object in the container.")
   @RBArgComment2("The exception message thrown when trying to roll back a container that failed to be emptied.")
   public static final String EMPTY_AND_ROLLBACK_FAILED = "35";

   @RBEntry("Restore failure")
   @RBComment("The subject of the email sent to an administrator when a mark for delete fails. (See EMPTY_FAILED)")
   public static final String EMPTY_FAILED_SUBJECT = "36";

   @RBEntry("Default")
   @RBComment("The name of the default \"type\" of application container (i.e. project \"type\")")
   public static final String DEFAULT_PUBLIC_DOMAIN_DISPLAY = "37";

   @RBEntry("The domain \"{0}\" is not a public domain.")
   @RBArgComment0("The name of the domain")
   public static final String NOT_A_PUBLIC_DOMAIN = "38";

   @RBEntry("You cannot create a CreatorsLink with \"{0}\" because it is not an instance of WTContainer.")
   @RBArgComment0("The name of the class that is not an instance of WTContainer")
   public static final String BAD_CREATORS_CLASS = "39";

   @RBEntry("The system cannot find the context class \"{0}\". Please contact your administrator.")
   @RBArgComment0("The name of the class that couldn't be loaded.")
   public static final String CANT_FIND_CREATORS_CLASS = "40";

   @RBEntry("Unable to find context class \"{0}\" in path \"{1}\")")
   @RBArgComment0("The container classname that couldn't be found")
   @RBArgComment1("The container path with the classname that couldn't be found")
   public static final String CANT_FIND_CONTAINER_CLASS_IN_PATH = "41";

   @RBEntry("Unable to parse path \"{0}\"")
   @RBArgComment0("The container path that couldn't be parsed")
   public static final String CANT_PARSE_PATH = "42";

   @RBEntry("Invalid domain: \"{0}\". Domains must have a parent domain in either the Windchill PDM context, the default org context, or the exchange context")
   @RBArgComment0("The name of the domain that has a bad parent")
   public static final String BAD_PARENT_DOMAIN_CLASSIC = "43";

   @RBEntry("Invalid domain: \"{0}\". Domains must have a parent domain in either their own context, or their immediate parent context")
   @RBArgComment0("The name of the domain that has a bad parent")
   public static final String BAD_PARENT_DOMAIN = "44";

   @RBEntry("Invalid domain: \"{0}\". Domain paths must be unique relative to the domain's context.")
   @RBArgComment0("The name of the invalid domain")
   public static final String DOMAIN_PATH_NOT_UNIQUE = "45";

   @RBEntry("Application error: A context can only be created using the WTContainerService.create API. The context \"{0}\" was not created using this API.")
   @RBArgComment0("The name of the container that was not created via WTContainerService.create")
   public static final String MUST_USE_CREATORS_TO_CREATE_A_CONTAINER = "46";

   @RBEntry("You cannot save a project as a new project unless you are a member of the organization hosting the project. You can export the project as a template or as a project file and then import the saved project information from your file system into a project shell that you create in your own organization.")
   @RBArgComment0("The name of the container class the user is trying to create an instance of")
   @RBArgComment1("The name of the user trying to create the container")
   @RBArgComment2("The identity of the container the user tried to create a child container in")
   public static final String NO_CREATE_ACCESS_FOR_CONTAINER = "47";

   @RBEntry("User \"{1}\" cannot create a context of type \"{0}\" because the context's parent organization \"{2}\" is not a subscriber.")
   @RBArgComment0("The name of the container class the user is trying to create an instance of")
   @RBArgComment1("The name of the user trying to create the container")
   @RBArgComment2("The identity of the nonsubscribing org container the user tried to create a child container in")
   public static final String NO_CREATE_FOR_NONSUBSCRIBING_ORG = "48";

   @RBEntry("{0}")
   @RBComment("The exchange container's identity is only its display type, not its display type plus name (since there's only 1, this would be redundant, like \"Site Site\")")
   @RBArgComment0("The exchange container's display type")
   public static final String EXCHANGE_IDENTITY = "49";

   @RBEntry("Windchill system site")
   @RBComment("The description for the exchange (site) container")
   public static final String EXCHANGE_CONTAINER_DESCRIPTION = "50";

   @RBEntry("Windchill PDM")
   @RBComment("The name of the Windchill PDM container")
   public static final String CLASSIC_CONTAINER_NAME = "51";

   @RBEntry("Context for the business data in a Windchill PDM installation.")
   public static final String CLASSIC_CONTAINER_DESCRIPTION = "52";

   @RBEntry("Context for the organization hosting this Windchill installation.")
   public static final String SITE_ORG_CONTAINER_DESCRIPTION = "53";

   @RBEntry("Secured Information")
   @RBComment("Used to display a field when the user doesn't have access to it.")
   public static final String SECURED_INFORMATION = "54";

   @RBEntry("An internet domain must be configured for the context: \"{0}\". To configure this using the context's organization, use the principal administrator to add an internetDomain property to the organization: \"{1}\". Alternatively, the internetDomain can be specified in the properties file: \"{2}\"")
   @RBArgComment0("The display path of the container")
   @RBArgComment1("The DN of the container's organization")
   @RBArgComment2("The path to the internetDomain.properties file")
   public static final String INTERNET_DOMAIN_REQUIRED = "55";

   @RBEntry("Domain for principals that are members of the organization: \"{0}\"")
   @RBArgComment0("The display identity of the organization")
   public static final String ORG_DOMAIN_DESCRIPTION = "56";

   @RBEntry("WTContainerHelper.setRestrictedDirectorySearchScope can only be used with an unpersisted context. To change this property on a persistent context, use WTContainerService.changeRestrictedDirectorySearchScope. The persistent context was: \"{0}\"")
   @RBArgComment0("The display identity of the container")
   public static final String SET_RESTRICTED_SEARCH_SCOPE_UNPERSISTED_ONLY = "57";

   @RBEntry("WTContainerService.changeRestrictedDirectorySearchScope can only be used with a persistent context. To assign this property to an unpersisted context, use WTContainerHelper.setRestrictedDirectorySearchScope. The unpersisted context was: \"{0}\"")
   @RBArgComment0("The display identity of the container")
   public static final String CHANGE_RESTRICTED_SEARCH_SCOPE_PERSISTED_ONLY = "58";

   @RBEntry("ATTENTION: Secured Action. Only the Site administrator can change the 'Allow entire user and group directory selection' field on the organization. Could not modify organization: \"{0}\" with new value: \"{1}\".")
   @RBArgComment0("The name of the organization that the user couldn't modify")
   @RBArgComment1("The boolean value of restrictedDirectorySearchScope the user tried to set.")
   public static final String ONLY_SITE_ADMIN_CAN_CHANGE_RESTRICTED_SEARCH_SCOPE = "59";

   @RBEntry("Cannot delete Organization:\"{0}\" because it is associated with an Organization Context \"{1}\".")
   @RBComment("Error indicating that WTOrganization associated with an Organization Container can't be deleted.")
   @RBArgComment0("The WTOrganization that is associated with an Organization Container.")
   @RBArgComment1("The Organization Container with which the WTOrganization is associated.")
   public static final String CANT_DELETE_ORG_WITH_CONTAINER = "60";

   @RBEntry("Users that have permission to create contexts of type \"{0}\" within \"{1}\"")
   @RBArgComment0("The localized container type that the users can create")
   @RBArgComment1("The display identity of the container the users can create the container type within")
   public static final String CREATORS_GROUP_DESCRIPTION = "61";

   @RBEntry("The system was unable to change the context's identity because a conflicting directory entry already exists. \n\tContext: \"{0}\" \n\tDistinguished name: \"{1}\"")
   @RBComment("Thrown when a container is renamed, moved, or both, and the new identity conflicts with the existing directory structure")
   @RBArgComment0("The name of the container that the user tried to rename or move")
   @RBArgComment1("The DN that the system tried to move the container to, but that already existed")
   public static final String UNABLE_TO_CHANGE_CONTAINER_IDENTITY = "62";

   @RBEntry("The system was unable to change the organization's identity because an organization with that name already exists in the directory. \n\tContext: \"{0}\" \n\tDistinguished name: \"{1}\"")
   @RBComment("Thrown when an org container is renamed, moved, or both, and the new identity conflicts with the existing directory structure")
   @RBArgComment0("The name of the organization that the user tried to rename or move")
   @RBArgComment1("The DN that the system tried to move the organization to, but that already existed")
   public static final String UNABLE_TO_CHANGE_ORG_IDENTITY = "63";

   @RBEntry("Windchill On-Demand: no organization for local search")
   @RBComment("Thrown when search client has not set org to which searches are to be restricted into the method context.")
   public static final String WOD_NO_ORG_FOR_LOCAL_SEARCH = "64";

   @RBEntry("Windchill On-Demand: can't resolve reference to org context")
   @RBComment("A value was set into the method context that is a WTContainerRef, but is not an application container ref or an org container ref.")
   public static final String WOD_INVALID_ORG_FOR_LOCAL_SEARCH = "65";

   @RBEntry("Windchill On-Demand: ORG_SEARCH_RESTRICT not WTContainerRef")
   @RBComment("A value was set into the method context that is not a WTContainerRef.")
   public static final String WOD_NOT_CONTAINER_REF = "66";

   @RBEntry("Windchill On-Demand: No org for current user")
   @RBComment("No org set in method context, and the current principal has no organization.")
   public static final String WOD_NO_ORG_FOR_USER = "67";

   @RBEntry("Windchill On-Demand: User invited to more than one org")
   @RBComment("No org set in method context, and the current principal's org has no org context, and the principal is participating in more than one org.")
   public static final String WOD_MULTIPLE_ORG_USER = "68";

   @RBEntry("You are not a member of any library, and you are not able to create a library.")
   public static final String ERROR_NO_LIBRARYS_NO_CREATE = "69";

   @RBEntry("You are not a member of any product, and you are not able to create a product.")
   public static final String ERROR_NO_PRODUCTS_NO_CREATE = "70";

   @RBEntry("Windchill On-Demand: You are not a member of any tenant organization, and you have not been invited to particpate in any tenant organization.")
   @RBComment("The supplier user has not been invited to any application container in a tenant organization.  In order to login to a Windchill OnDemand system, a user must either be a member of a tenant organization, or have been invited to participate in an application container of a tenant organization.")
   public static final String WOD_SUPPLIER_NOT_INVITED = "71";

   @RBEntry("You do not have permission to create a project.")
   public static final String ERROR_NO_PROJECTS_NO_CREATE = "80";

   @RBEntry("You do not have permission to create a program.")
   public static final String ERROR_NO_PROGRAMS_NO_CREATE = "81";

   @RBEntry("Only site administrators can run OrganizationSync. Session principal is: \"{0}\"")
   @RBArgComment0("The current session principal")
   public static final String ORG_SYNCH_ADMIN_REQUIRED = "82";

   @RBEntry("You can not create \"{0}\" because the name has an invalid character \"{1}\".")
   @RBComment("A container name cannot contain any of the following characters: /    !  @  #  $  %  ^  &  *  (  )  :  ?  \"  <  >  |")
   @RBArgComment0("The name of the container the user is attempting to create.")
   @RBArgComment1("The illegal character that was detected.")
   public static final String INVALID_CHAR_IN_NAME = "83";

   @RBEntry("Products and libraries cannot be deleted.")
   public static final String CANNOT_DELETE_PROD_LIB = "84";

   @RBEntry("Additional search bases which need to be queried")
   public static final String PRIVATE_CONSTANT_0 = "ADDITIONAL_CONTEXT_PROVIDERS";

   @RBEntry("Additional search bases for which DirectoryContextProviders need to be created.")
   public static final String PRIVATE_CONSTANT_1 = "ADDITIONAL_CONTEXT_PROVIDERS_SHORT_DESC";

   @RBEntry("Additional search bases, where each search base is a comma seperated distinguished name in LDAP the contents of which need to be queried.")
   public static final String PRIVATE_CONSTANT_2 = "ADDITIONAL_CONTEXT_PROVIDERS_LONG_DESC";
   
   @RBEntry(" Organization context {0} could not be created because the specified organization name {0} is already associated with an organization context. Specify a different name.")
   @RBComment("Error message that occurs when a user tries to load an org container with the same name as one that already exists.")
   public static final String ORG_CONTAINER_ALREADY_EXISTS = "ORG_CONTAINER_ALREADY_EXISTS";
   
   @RBEntry("Context could not be created because the template with the name \"{0}\" could not be found.")
   @RBComment("Error message when a container is created through a load file and the template can not be found")
   public static final String TEMPLATE_NOT_FOUND = "TEMPLATE_NOT_FOUND";
}
