/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.inf.container;

import wt.util.resource.*;

@RBUUID("wt.inf.container.containerResource")
public final class containerResource_it extends WTListResourceBundle {
   @RBEntry("L'operazione \"{0}\" non è riuscita.")
   @RBComment("WTException: A specified operation failed during installation")
   @RBArgComment0("Name of operation that failed")
   public static final String OPERATION_FAILED = "0";

   @RBEntry("Impossibile eliminare il contesto \"{0}\".")
   @RBComment("WTException: Deleting of system containers is not allowed")
   @RBArgComment0("Name of container being deleted")
   public static final String CANT_DELETE_CONTAINER = "1";

   @RBEntry("\"{0}\" non può essere contenuto in \"{1}\".")
   @RBComment("Can not contain in container")
   @RBArgComment0("name of contained object")
   @RBArgComment1("name of container")
   public static final String CONTAINED_IN_CONTAINER = "2";

   @RBEntry("\"{0}\" non è un contesto")
   @RBComment("Not an instance of WTContainer")
   @RBArgComment0("Name of object assumed to be a container")
   public static final String NOT_CONTAINER_CLASS = "3";

   @RBEntry("Impossibile trovare il contesto \"{0}\" (ok se installazione)")
   @RBComment("Warning message printed during method server startup if the exchange or classic container can't be found")
   @RBArgComment0("The name of the container that can't be found.")
   public static final String CANT_FIND_SYSTEM_CONTAINER = "4";

   @RBEntry("Sistema danneggiato: il contesto \"{0}\" risulta definito più di una volta")
   @RBComment("Exception message when the system finds more than one classic or exchange container ")
   @RBArgComment0("Name of the container that is defined more than once")
   public static final String MULTIPLE_SYSTEM_CONTAINERS = "5";

   @RBEntry("Impossibile inizializzare il contesto \"{0}\"")
   @RBComment("The container can not be initialized")
   @RBArgComment0("Name of the container that cannot be initialized")
   public static final String NO_INIT_CONTAINER = "6";

   @RBEntry("Nessun oggetto contesto nella specifica di interrogazione")
   @RBComment("No containers defined in query spec")
   public static final String NO_CONTAINERS_QS = "7";

   @RBEntry("AVVERTENZA: questo gruppo è stato creato per essere utilizzato dal sistema Windchill. La sua modifica o l'eliminazione può determinare delle conseguenze non desiderate.")
   public static final String DO_NOT_MODIFY = "8";

   @RBEntry("Il dominio \"{1}\" non è un dominio padre pubblico valido per il contesto \"{0}\".")
   @RBComment("Something was wrong with the parent public domain the user is trying to set on the container. ")
   @RBArgComment0("The container that the new parent public domain was supposed to be for")
   @RBArgComment1("The bad parent public domain")
   public static final String BAD_PUBLIC_PARENT = "9";

   @RBEntry("Errore durante la modifica del dominio padre pubblico del contesto \"{0}\". Nuovo dominio: \"{1}\".")
   @RBArgComment0("The container that the new parent public domain was supposed to be for")
   @RBArgComment1("The bad parent public domain")
   public static final String COULD_NOT_CHANGE_PUBLIC_PARENT = "10";

   @RBEntry("I domini di base non sono supportati per gli oggetti contesto di tipo \"{1}\" nel contesto \"{0}\".")
   @RBArgComment0("The container in which the base domain was looked for")
   @RBArgComment1("The container class that does not have base domains in the container")
   public static final String UNSUPPORTED_BASE_DOMAIN_CONTAINER = "11";

   @RBEntry("Numero di domini di base imprevisto per il tipo di contesto \"{1}\" nel contesto \"{0}\". Numero atteso: 1. Numero effettivo: \"{2}\".")
   @RBArgComment0("The container in which the base domain was looked for")
   @RBArgComment1("The container class that has too many base domains in the container")
   public static final String UNEXPECTED_NUMBER_OF_BASE_DOMAINS = "12";

   @RBEntry("Impossibile trovare il contesto per il percorso \"{0}\".")
   @RBArgComment0("The container path that no container matched")
   public static final String NO_CONTAINER_FOR_PATH = "13";

   @RBEntry("Impossibile trovare il contesto per il percorso \"{0}\" perché è impossibile trovare la classe di contesto \"{1}\".")
   @RBArgComment0("The container path that had a bad class in it")
   @RBArgComment1("The name of the container class that couldn't be found")
   public static final String CANT_FIND_CONTAINER_CLASS = "14";

   @RBEntry("Più contesti corrispondono al percorso del contesto \"{0}\". Contattare l'amministratore.")
   @RBArgComment0("The container path that had more than one match")
   public static final String NO_UNIQUE_CONTAINER_FOR_PATH = "15";

   @RBEntry("WTContainerService.changePublicParentDomain deve essere invocato solo per i contenitori che sono già stati creati. Utilizzare WTContainer.setPublicParentDomain per impostare la proprietà su nuove istanze.")
   public static final String CHANGE_PUBLIC_PARENT_PERSISTENT_ONLY = "16";

   @RBEntry("WTContainerService.makePrivate deve essere invocato solo per i contenitori che sono già stati creati. Utilizzare WTContainer.setPrivateAccess per impostare la proprietà su nuove istanze.")
   public static final String MAKE_PRIVATE_PERSISTENT_ONLY = "17";

   @RBEntry("WTContainerService.makePrivate deve essere invocato solo per i contenitori che sono già stati creati. Utilizzare WTContainer.setPrivateAccess per impostare la proprietà su nuove istanze.")
   public static final String MAKE_PUBLIC_PERSISTENT_ONLY = "18";

   @RBEntry("Impossibile rendere pubblico il contesto \"{0}\".")
   public static final String COULD_NOT_MAKE_PUBLIC = "19";

   @RBEntry("Impossibile rendere privato il contesto \"{0}\".")
   public static final String COULD_NOT_MAKE_PRIVATE = "20";

   @RBEntry("WTContainerService.makePrivate non è supportato per questo contenitore: \"{0}\"")
   public static final String MAKE_PRIVATE_UNSUPPORTED = "21";

   @RBEntry("Non è consentito l'utilizzo di WTContainerHelper.setContainer per modificare il contesto di un oggetto persistente. L'utente ha tentato di impostare il contesto dell'oggetto \"{0}\" su \"{1}\".")
   @RBArgComment0("The persistent contained object.")
   @RBArgComment1("The new container")
   public static final String SET_CONTAINER_UNPERSISTED_ONLY = "22";

   @RBEntry("Non è consentito l'utilizzo di WTContainerHelper.setPrivateAccess per modificare la proprietà privateAccess di un contesto persistente. L'utente ha tentato di impostare la proprietà privateAccess del contesto \"{0}\" su \"{1}\".")
   @RBArgComment0("The persistent container")
   @RBArgComment1("The value of privateAccess")
   public static final String SET_PRIVATE_ACCESS_UNPERSISTED_ONLY = "23";

   @RBEntry("Non è consentito l'utilizzo di WTContainerHelper.setPublicParentDomain per modificare la proprietà publicParentDomain di un contesto persistente. L'utente ha tentato di impostare il dominio pubblico del contesto \"{0}\" su \"{1}\".")
   @RBArgComment0("The persistent container")
   public static final String SET_PUBLIC_PARENT_UNPERSISTED_ONLY = "24";

   @RBEntry("Non è stato trovato alcun oggetto corrispondente ai criteri di ricerca")
   public static final String NO_CONTAINERS_FOUND = "25";

   @RBEntry("Impossibile trovare il contesto: \"{0}\"")
   @RBArgComment0("The localized name of the classic container (derived from CLASSIC_CONTAINER_NAME below).")
   public static final String CLASSIC_CONTAINER_NOT_FOUND = "26";

   @RBEntry("Gli oggetti contesto di tipo \"{0}\" non possono essere contrassegnati per l'eliminazione.")
   @RBArgComment0("The type of container that can't be marked for delete")
   public static final String CANNOT_M4D_CONTAINER = "27";

   @RBEntry("Impossibile contrassegnare il contesto \"{0}\" per l'eliminazione a causa di un problema riguardante parte del suo contenuto. Messaggio di sistema: \"{1}\".")
   @RBComment("This message is sent by email to the administrator of the container")
   @RBArgComment0("The identity of the container that couldn't be marked for delete.")
   @RBArgComment1("The exception message thrown when trying to mark for delete an object in the container.")
   public static final String M4D_FAILED = "28";

   @RBEntry("Impossibile contrassegnare il contesto \"{0}\" per l'eliminazione a causa di un problema riguardante parte del suo contenuto. Messaggio di sistema: \"{1}\". Inoltre, un problema ha impedito il ripristino del contesto. Messaggio di sistema: \"{2}\".")
   @RBComment("This message is sent by email to the administrator of the container")
   @RBArgComment0("The identity of the container that couldn't be marked for delete.")
   @RBArgComment1("The exception message thrown when trying to mark for delete an object in the container.")
   @RBArgComment2("The exception message thrown when trying to restore a container that failed to be marked for delete")
   public static final String M4D_AND_ROLLBACK_FAILED = "29";

   @RBEntry("Errore durante il contrassegno per l'eliminazione")
   @RBComment("The subject of the email sent to an administrator when a mark for delete fails. (See M4D_FAILED)")
   public static final String M4D_FAILED_SUBJECT = "30";

   @RBEntry("Il ripristino del contesto \"{0}\" non è riuscito a causa di un problema riguardante parte del suo contenuto. Messaggio di sistema: \"{1}\".")
   @RBComment("This message is sent by email to the administrator of the container.")
   @RBArgComment0("The identity of the container that couldn't be restored.")
   @RBArgComment1("The exception message thrown when trying to restore an object in the container.")
   public static final String RESTORE_FAILED = "31";

   @RBEntry("Il ripristino del contesto \"{0}\" non è riuscito a causa di un problema riguardante parte del suo contenuto. Messaggio di sistema: \"{1}\". Inoltre, un problema ha impedito il ritorno del contesto allo stato \"contrassegnato per l'eliminazione\". Messaggio di sistema: \"{2}\".")
   @RBComment("This message is sent by email to the administrator of the container.")
   @RBArgComment0("The identity of the container that couldn't be restored.")
   @RBArgComment1("The exception message thrown when trying to restore an object in the container.")
   @RBArgComment2("The exception message thrown when trying to roll back a container that failed to be restored.")
   public static final String RESTORE_AND_ROLLBACK_FAILED = "32";

   @RBEntry("Ripristino fallito")
   @RBComment("The subject of the email sent to an administrator when a mark for delete fails. (See RESTORE_FAILED)")
   public static final String RESTORE_FAILED_SUBJECT = "33";

   @RBEntry("Lo svuotamento del contesto \"{0}\" non è riuscito a causa di un problema riguardante parte del suo contenuto. Messaggio di sistema: \"{1}\".")
   @RBComment("This message is sent by email to the administrator of the container.")
   @RBArgComment0("The identity of the container that couldn't be emptied.")
   @RBArgComment1("The exception message thrown when trying to empty an object in the container.")
   public static final String EMPTY_FAILED = "34";

   @RBEntry("Lo svuotamento del contesto \"{0}\" non è riuscito a causa di un problema riguardante parte del suo contenuto. Messaggio di sistema: \"{1}\". Inoltre, un problema ha impedito il ritorno del contesto allo stato \"contrassegnato per l'eliminazione\". Messaggio di sistema: \"{2}\".")
   @RBComment("This message is sent by email to the administrator of the container.")
   @RBArgComment0("The identity of the container that couldn't be emptied.")
   @RBArgComment1("The exception message thrown when trying to empty an object in the container.")
   @RBArgComment2("The exception message thrown when trying to roll back a container that failed to be emptied.")
   public static final String EMPTY_AND_ROLLBACK_FAILED = "35";

   @RBEntry("Ripristino fallito")
   @RBComment("The subject of the email sent to an administrator when a mark for delete fails. (See EMPTY_FAILED)")
   public static final String EMPTY_FAILED_SUBJECT = "36";

   @RBEntry("Default")
   @RBComment("The name of the default \"type\" of application container (i.e. project \"type\")")
   public static final String DEFAULT_PUBLIC_DOMAIN_DISPLAY = "37";

   @RBEntry("\"{0}\" non è un dominio pubblico.")
   @RBArgComment0("The name of the domain")
   public static final String NOT_A_PUBLIC_DOMAIN = "38";

   @RBEntry("Impossibile creare un link CreatorsLink con \"{0}\" perché non è un'istanza di WTContainer.")
   @RBArgComment0("The name of the class that is not an instance of WTContainer")
   public static final String BAD_CREATORS_CLASS = "39";

   @RBEntry("Il sistema non riesce a trovare la classe di contesto \"{0}\". Contattare l'amministratore.")
   @RBArgComment0("The name of the class that couldn't be loaded.")
   public static final String CANT_FIND_CREATORS_CLASS = "40";

   @RBEntry("Impossibile trovare la classe di contesto \"{0}\" nel percorso \"{1}\".")
   @RBArgComment0("The container classname that couldn't be found")
   @RBArgComment1("The container path with the classname that couldn't be found")
   public static final String CANT_FIND_CONTAINER_CLASS_IN_PATH = "41";

   @RBEntry("Impossibile analizzare il percorso: \"{0}\"")
   @RBArgComment0("The container path that couldn't be parsed")
   public static final String CANT_PARSE_PATH = "42";

   @RBEntry("Dominio \"{0}\" non valido. I domini devono avere un dominio padre nel contesto Windchill PDM, nel contesto di default dell'organizzazione o nel contesto di sistema")
   @RBArgComment0("The name of the domain that has a bad parent")
   public static final String BAD_PARENT_DOMAIN_CLASSIC = "43";

   @RBEntry("Dominio \"{0}\" non valido. I domini devono avere un dominio padre nel proprio contesto o in quello direttamente superiore")
   @RBArgComment0("The name of the domain that has a bad parent")
   public static final String BAD_PARENT_DOMAIN = "44";

   @RBEntry("Dominio non valido: \"{0}\". I percorsi di dominio devono essere univoci relativamente al contesto di dominio.")
   @RBArgComment0("The name of the invalid domain")
   public static final String DOMAIN_PATH_NOT_UNIQUE = "45";

   @RBEntry("Errore dell'applicazione: i contesti possono essere creati solo usando l'API WTContainerService.create. Il contesto \"{0}\" non è stato creato usando questa API.")
   @RBArgComment0("The name of the container that was not created via WTContainerService.create")
   public static final String MUST_USE_CREATORS_TO_CREATE_A_CONTAINER = "46";

   @RBEntry("È possibile salvare un nuovo progetto solo se si fa parte dell'organizzazione che ospita il progetto. È possibile esportare il progetto come modello o come file di progetto, quindi importare dal file system le informazioni sul progetto salvate in una shell di progetto che può essere creata nell'organizzazione.")
   @RBArgComment0("The name of the container class the user is trying to create an instance of")
   @RBArgComment1("The name of the user trying to create the container")
   @RBArgComment2("The identity of the container the user tried to create a child container in")
   public static final String NO_CREATE_ACCESS_FOR_CONTAINER = "47";

   @RBEntry("L'utente \"{1}\" non può creare un contesto di tipo \"{0}\" perché l'organizzazione padre del contesto \"{2}\" non ha effettuato la sottoscrizione.")
   @RBArgComment0("The name of the container class the user is trying to create an instance of")
   @RBArgComment1("The name of the user trying to create the container")
   @RBArgComment2("The identity of the nonsubscribing org container the user tried to create a child container in")
   public static final String NO_CREATE_FOR_NONSUBSCRIBING_ORG = "48";

   @RBEntry("{0}")
   @RBComment("The exchange container's identity is only its display type, not its display type plus name (since there's only 1, this would be redundant, like \"Site Site\")")
   @RBArgComment0("The exchange container's display type")
   public static final String EXCHANGE_IDENTITY = "49";

   @RBEntry("Sito sistema Windchill")
   @RBComment("The description for the exchange (site) container")
   public static final String EXCHANGE_CONTAINER_DESCRIPTION = "50";

   @RBEntry("Windchill PDM")
   @RBComment("The name of the Windchill PDM container")
   public static final String CLASSIC_CONTAINER_NAME = "51";

   @RBEntry("Contesto per i dati aziendali in un'installazione Windchill PDM.")
   public static final String CLASSIC_CONTAINER_DESCRIPTION = "52";

   @RBEntry("Contesto per l'organizzazione host dell'installazione Windchill.")
   public static final String SITE_ORG_CONTAINER_DESCRIPTION = "53";

   @RBEntry("Informazioni protette")
   @RBComment("Used to display a field when the user doesn't have access to it.")
   public static final String SECURED_INFORMATION = "54";

   @RBEntry("È necessario configurare il dominio Internet del contesto: \"{0}\". Per configurarlo usando l'organizzazione del contesto, utilizzare l'amministrazione utenti/gruppi/ruoli per aggiungere una proprietà internetDomain all'organizzazione: \"{1}\". Alternativamente, internetDomain può essere specificata nel file: \"{2}\"")
   @RBArgComment0("The display path of the container")
   @RBArgComment1("The DN of the container's organization")
   @RBArgComment2("The path to the internetDomain.properties file")
   public static final String INTERNET_DOMAIN_REQUIRED = "55";

   @RBEntry("Dominio per utenti/gruppi/ruoli membri dell'organizzazione: \"{0}\"")
   @RBArgComment0("The display identity of the organization")
   public static final String ORG_DOMAIN_DESCRIPTION = "56";

   @RBEntry("WTContainerHelper.setRestrictedDirectorySearchScope può essere usato solo con un contesto non persistente. Per modificare questa proprietà per un contesto persistente, usare WTContainerService.changeRestrictedDirectorySearchScope. Il contesto persistente era: \"{0}\"")
   @RBArgComment0("The display identity of the container")
   public static final String SET_RESTRICTED_SEARCH_SCOPE_UNPERSISTED_ONLY = "57";

   @RBEntry("WTContainerService.changeRestrictedDirectorySearchScope può essere usato solo con un contesto persistente. Per attribuire questa proprietà ad un contesto non persistente, usare WTContainerHelper.setRestrictedDirectorySearchScope. Il contesto non persistente era: \"{0}\"")
   @RBArgComment0("The display identity of the container")
   public static final String CHANGE_RESTRICTED_SEARCH_SCOPE_PERSISTED_ONLY = "58";

   @RBEntry("ATTENZIONE: azione protetta. Solo l'amministratore del sito può modificare il campo 'Permetti la selezione dell'intera directory di utenti e gruppi' dell'organizzazione. Impossibile modificare l'organizzazione \"{0}\" con il nuovo valore \"{1}\".")
   @RBArgComment0("The name of the organization that the user couldn't modify")
   @RBArgComment1("The boolean value of restrictedDirectorySearchScope the user tried to set.")
   public static final String ONLY_SITE_ADMIN_CAN_CHANGE_RESTRICTED_SEARCH_SCOPE = "59";

   @RBEntry("Impossibile eliminare l'organizzazione \"{0}\" perché è associata al contesto Organizzazione \"{1}\".")
   @RBComment("Error indicating that WTOrganization associated with an Organization Container can't be deleted.")
   @RBArgComment0("The WTOrganization that is associated with an Organization Container.")
   @RBArgComment1("The Organization Container with which the WTOrganization is associated.")
   public static final String CANT_DELETE_ORG_WITH_CONTAINER = "60";

   @RBEntry("Utenti che hanno il permesso di creare contesti di tipo \"{0}\" in \"{1}\"")
   @RBArgComment0("The localized container type that the users can create")
   @RBArgComment1("The display identity of the container the users can create the container type within")
   public static final String CREATORS_GROUP_DESCRIPTION = "61";

   @RBEntry("Il sistema non è riuscito a modificare l'identificatore del contesto perché una voce di directory in conflitto esiste già. \n\tContesto: \"{0}\" \n\tNome distinto: \"{1}\"")
   @RBComment("Thrown when a container is renamed, moved, or both, and the new identity conflicts with the existing directory structure")
   @RBArgComment0("The name of the container that the user tried to rename or move")
   @RBArgComment1("The DN that the system tried to move the container to, but that already existed")
   public static final String UNABLE_TO_CHANGE_CONTAINER_IDENTITY = "62";

   @RBEntry("Il sistema non è stato in grado di modificare l'identificatore dell'organizzazione perché un'organizzazione con quel nome esiste già nella directory. \n\tContesto: \"{0}\" \n\tNome distinto: \"{1}\"")
   @RBComment("Thrown when an org container is renamed, moved, or both, and the new identity conflicts with the existing directory structure")
   @RBArgComment0("The name of the organization that the user tried to rename or move")
   @RBArgComment1("The DN that the system tried to move the organization to, but that already existed")
   public static final String UNABLE_TO_CHANGE_ORG_IDENTITY = "63";

   @RBEntry("Windchill On-Demand: nessuna organizzazione per la ricerca locale")
   @RBComment("Thrown when search client has not set org to which searches are to be restricted into the method context.")
   public static final String WOD_NO_ORG_FOR_LOCAL_SEARCH = "64";

   @RBEntry("Windchill On-Demand: impossibile risolvere il riferimento a contesto organizzazione")
   @RBComment("A value was set into the method context that is a WTContainerRef, but is not an application container ref or an org container ref.")
   public static final String WOD_INVALID_ORG_FOR_LOCAL_SEARCH = "65";

   @RBEntry("Windchill On-Demand: Il riferimento non viene risolto come contenitore di applicazione o di organizzazione")
   @RBComment("A value was set into the method context that is not a WTContainerRef.")
   public static final String WOD_NOT_CONTAINER_REF = "66";

   @RBEntry("Windchill On-Demand: Nessuna organizzazione per l'utente corrente")
   @RBComment("No org set in method context, and the current principal has no organization.")
   public static final String WOD_NO_ORG_FOR_USER = "67";

   @RBEntry("Windchill On-Demand: L'utente è invitato per più di un'organizzazione")
   @RBComment("No org set in method context, and the current principal's org has no org context, and the principal is participating in more than one org.")
   public static final String WOD_MULTIPLE_ORG_USER = "68";

   @RBEntry("L'utente non è membro di alcuna libreria né ha i privilegi per crearne una.")
   public static final String ERROR_NO_LIBRARYS_NO_CREATE = "69";

   @RBEntry("L'utente non è membro di alcun prodotto né ha i privilegi per creare uno.")
   public static final String ERROR_NO_PRODUCTS_NO_CREATE = "70";

   @RBEntry("Windchill On-Demand: l'utente non è membro di un'organizzazione locataria, né è stato invitato a parteciparvi.")
   @RBComment("The supplier user has not been invited to any application container in a tenant organization.  In order to login to a Windchill OnDemand system, a user must either be a member of a tenant organization, or have been invited to participate in an application container of a tenant organization.")
   public static final String WOD_SUPPLIER_NOT_INVITED = "71";

   @RBEntry("Non si dispone del permesso per creare un progetto.")
   public static final String ERROR_NO_PROJECTS_NO_CREATE = "80";

   @RBEntry("Non si dispone del permesso per creare un programma.")
   public static final String ERROR_NO_PROGRAMS_NO_CREATE = "81";

   @RBEntry("La sincronizzazione delle organizzazioni può essere eseguita solo dagli amministratori di sito. Utente/gruppo/ruolo in sessione: \"{0}\"")
   @RBArgComment0("The current session principal")
   public static final String ORG_SYNCH_ADMIN_REQUIRED = "82";

   @RBEntry("Impossibile creare \"{0}\" in quanto il nome contiene il carattere non valido \"{1}\".")
   @RBComment("A container name cannot contain any of the following characters: /    !  @  #  $  %  ^  &  *  (  )  :  ?  \"  <  >  |")
   @RBArgComment0("The name of the container the user is attempting to create.")
   @RBArgComment1("The illegal character that was detected.")
   public static final String INVALID_CHAR_IN_NAME = "83";

   @RBEntry("Impossibile eliminare i prodotti e le librerie.")
   public static final String CANNOT_DELETE_PROD_LIB = "84";

   @RBEntry("È necessario interrogare basi di ricerca aggiuntive")
   public static final String PRIVATE_CONSTANT_0 = "ADDITIONAL_CONTEXT_PROVIDERS";

   @RBEntry("Basi di ricerca aggiuntive per cui creare DirectoryContextProvider.")
   public static final String PRIVATE_CONSTANT_1 = "ADDITIONAL_CONTEXT_PROVIDERS_SHORT_DESC";

   @RBEntry("Basi di ricerca aggiuntive, dove le basi di cui interrogare il contenuto sono rappresentate da nomi distinti LDAP separati da virgole.")
   public static final String PRIVATE_CONSTANT_2 = "ADDITIONAL_CONTEXT_PROVIDERS_LONG_DESC";
   
   @RBEntry(" Impossibile creare il contesto di organizzazione {0}. Il nome specificato per l'organizzazione {0} è già associato a un contesto di organizzazione. Specificare un nome diverso.")
   @RBComment("Error message that occurs when a user tries to load an org container with the same name as one that already exists.")
   public static final String ORG_CONTAINER_ALREADY_EXISTS = "ORG_CONTAINER_ALREADY_EXISTS";
   
   @RBEntry("Impossibile creare il contesto, poiché non è stato possibile trovare il modello \"{0}\".")
   @RBComment("Error message when a container is created through a load file and the template can not be found")
   public static final String TEMPLATE_NOT_FOUND = "TEMPLATE_NOT_FOUND";
}
