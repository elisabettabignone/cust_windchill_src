/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.inf.container.creator;

import wt.util.resource.*;

@RBUUID("wt.inf.container.creator.creatorResource")
public final class creatorResource extends WTListResourceBundle {
   @RBEntry("The OrgContainer \"{0}\" cannot be created because it does not have an Org template assigned to it.")
   @RBArgComment0("The name of the container that does not have a template")
   public static final String ORG_TEMPLATE_REQUIRED = "0";

   @RBEntry("An organization's private domain is used by child contexts that need to inherit minimal administrative rules.")
   @RBComment("The total length of the description must not exceed 200 characters, as that is the size limit for domain descriptions")
   public static final String PRIVATE_DOMAIN_DESCRIPTION = "1";

   @RBEntry("One of an organization's public domains. Public domains are used by child contexts that need to inherit an organization's administrative rules.")
   @RBComment("The total length of the description must not exceed 200 characters, as that is the size limit for domain descriptions")
   public static final String PUBLIC_DOMAIN_DESCRIPTION = "2";

   @RBEntry("Contains all users that are members of contexts within organization: \"{0}\"")
   @RBArgComment0("The display path of the organization that group is in")
   public static final String ALL_PARTICIPATING_MEMBERS_DESCRIPTION = "3";

   @RBEntry("Organizations that by default are granted read access to principals in the User domain and its child domains")
   @RBArgComment0("This is the Unrestricted Organizations group.")
   public static final String UNRESTRICTED_ORG_DESCRIPTION = "4";

   @RBEntry("Contains organizations against which access controls rules will be written.")
   @RBArgComment0("This group is used to hold the organization in which it is created.")
   public static final String THIS_ORG_DESCRIPTION = "5";

   @RBEntry("Contains groups that are used for Role based UI")
   @RBArgComment0("This group is used to hold all the groups that are used in Role base UI implementation.")
   public static final String PROFILE_GROUPS_DESCRIPTION = "6";

   @RBEntry("Contains groups that are used for Role based UI")
   @RBArgComment0("This group is used to hold all the groups that are used in Role base UI implementation.")
   public static final String DISABLED_PROFILE_GROUPS_DESCRIPTION = "7";

   @RBEntry("Contains the users who can create and modify shared teams for the owning OrgContainer.")
   @RBArgComment0("This group is used to dictate administrative privileges over shared teams.")
   public static final String SHARED_TEAM_CREATOR_DESCRIPTION = "8";

   @RBEntry("Contains the users who can create, delete, and modify Filter Templates for the owning organization context.")
   @RBArgComment0("This group is used to dictate administrative privileges over Filter Templates.")
   public static final String NFT_CREATOR_DESCRIPTION = "9";
}
