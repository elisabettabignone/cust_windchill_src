/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.inf.container.creator;

import wt.util.resource.*;

@RBUUID("wt.inf.container.creator.creatorResource")
public final class creatorResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile creare il contenitore OrgContainer \"{0}\" perché non gli è stato asseganto alcun modello Org.")
   @RBArgComment0("The name of the container that does not have a template")
   public static final String ORG_TEMPLATE_REQUIRED = "0";

   @RBEntry("Un dominio privato dell'organizzazione viene usato da contesti figlio che devono ereditare le minime regole amministrative.")
   @RBComment("The total length of the description must not exceed 200 characters, as that is the size limit for domain descriptions")
   public static final String PRIVATE_DOMAIN_DESCRIPTION = "1";

   @RBEntry("Uno dei domini pubblici dell'organizzazione. I domini pubblici sono usati dai contesti figli che devono ereditare le regole amministrative di un'organizzazione.")
   @RBComment("The total length of the description must not exceed 200 characters, as that is the size limit for domain descriptions")
   public static final String PUBLIC_DOMAIN_DESCRIPTION = "2";

   @RBEntry("Contiene tutti gli utenti che sono membri dei contesti dell'organizzazione: \"{0}\"")
   @RBArgComment0("The display path of the organization that group is in")
   public static final String ALL_PARTICIPATING_MEMBERS_DESCRIPTION = "3";

   @RBEntry("Organizzazioni a cui è consentito per default l'accesso a utenti/gruppi/ruoli nel dominio dell'utente e ai domini figlio")
   @RBArgComment0("This is the Unrestricted Organizations group.")
   public static final String UNRESTRICTED_ORG_DESCRIPTION = "4";

   @RBEntry("Contiene le organizzazioni in relazione alle quali saranno scritte le regole di controllo di accesso.")
   @RBArgComment0("This group is used to hold the organization in which it is created.")
   public static final String THIS_ORG_DESCRIPTION = "5";

   @RBEntry("Contiene i gruppi utilizzati per l'interfaccia utente basata su ruolo")
   @RBArgComment0("This group is used to hold all the groups that are used in Role base UI implementation.")
   public static final String PROFILE_GROUPS_DESCRIPTION = "6";

   @RBEntry("Contiene i gruppi utilizzati per l'interfaccia utente basata su ruolo")
   @RBArgComment0("This group is used to hold all the groups that are used in Role base UI implementation.")
   public static final String DISABLED_PROFILE_GROUPS_DESCRIPTION = "7";

   @RBEntry("Contiene gli utenti che possono creare e modificare i team condivisi per il contenitore dell'organizzazione.")
   @RBArgComment0("This group is used to dictate administrative privileges over shared teams.")
   public static final String SHARED_TEAM_CREATOR_DESCRIPTION = "8";

   @RBEntry("Contiene gli utenti che possono creare, eliminare e modificare i modelli di filtro per il contenitore dell'organizzazione.")
   @RBArgComment0("This group is used to dictate administrative privileges over Filter Templates.")
   public static final String NFT_CREATOR_DESCRIPTION = "9";
}
