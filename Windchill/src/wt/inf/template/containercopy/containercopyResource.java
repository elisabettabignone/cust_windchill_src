/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.inf.template.containercopy;

import wt.util.resource.*;

@RBUUID("wt.inf.template.containercopy.containercopyResource")
public final class containercopyResource extends WTListResourceBundle {
   @RBEntry("Invalid installation, the directory {0} does not exist")
   public static final String NO_LOAD_DIRECTORY = "10";
}
