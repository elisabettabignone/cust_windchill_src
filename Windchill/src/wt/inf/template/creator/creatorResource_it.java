/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.inf.template.creator;

import wt.util.resource.*;

@RBUUID("wt.inf.template.creator.creatorResource")
public final class creatorResource_it extends WTListResourceBundle {
   @RBEntry("La richiesta di creazione deve specificare un contesto non nullo")
   @RBComment("Someone called the container creation API without actually passing in a new container instance")
   public static final String INVALID_CREATE_REQUEST = "0";

   @RBEntry("Impossibile creare il contesto \"{0}\", perché non è stato specificato alcun contesto padre.")
   @RBArgComment0("The identity of the container that does not have a parent and thus can't be created")
   public static final String PARENT_CONTAINER_REQUIRED = "1";

   @RBEntry("Impossibile creare il contesto \"{0}\", perché era già persistente.")
   @RBArgComment0("The identity of the container that was already persistent and thus can't be created")
   public static final String CONTAINER_ALREADY_PERSISTENT = "2";

   @RBEntry("Dominio di default dei business object del contesto.")
   @RBComment("The total length of the description must not exceed 200 characters, as that is the size limit for domain descriptions")
   public static final String DEFAULT_DOMAIN_DESCRIPTION = "3";

   @RBEntry("Schedario di default per i business object del contesto.")
   @RBComment("The total length of the description must not exceed 200 characters, as that is the size limit for cabinet descriptions")
   public static final String DEFAULT_CABINET_DESCRIPTION = "4";

   @RBEntry("Dominio di sistema per gli oggetti amministrativi del contesto.")
   @RBComment("The total length of the description must not exceed 200 characters, as that is the size limit for domain descriptions")
   public static final String SYSTEM_DOMAIN_DESCRIPTION = "5";

   @RBEntry("Schedario di sistema per gli oggetti amministrativi del contesto.")
   @RBComment("The total length of the description must not exceed 200 characters, as that is the size limit for cabinet descriptions")
   public static final String SYSTEM_CABINET_DESCRIPTION = "6";

   @RBEntry("Gruppo che esercita il ruolo: \"{0}\".")
   @RBArgComment0("The display name of the administrator role for the container")
   public static final String ADMINISTRATORS_GROUP_DESCRIPTION = "7";

   @RBEntry("Impossibile creare il contesto \"{0}\" perché non dispone di un modello assegnato.")
   @RBArgComment0("The name of the container that does not have a template")
   public static final String TEMPLATE_REQUIRED = "8";
}
