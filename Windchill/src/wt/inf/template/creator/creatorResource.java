/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.inf.template.creator;

import wt.util.resource.*;

@RBUUID("wt.inf.template.creator.creatorResource")
public final class creatorResource extends WTListResourceBundle {
   @RBEntry("The create request must specify a non-null context")
   @RBComment("Someone called the container creation API without actually passing in a new container instance")
   public static final String INVALID_CREATE_REQUEST = "0";

   @RBEntry("Could not create context \"{0}\", because no parent context was specified.")
   @RBArgComment0("The identity of the container that does not have a parent and thus can't be created")
   public static final String PARENT_CONTAINER_REQUIRED = "1";

   @RBEntry("Could not create context \"{0}\", because it was already persistent.")
   @RBArgComment0("The identity of the container that was already persistent and thus can't be created")
   public static final String CONTAINER_ALREADY_PERSISTENT = "2";

   @RBEntry("The default domain for a context's business objects.")
   @RBComment("The total length of the description must not exceed 200 characters, as that is the size limit for domain descriptions")
   public static final String DEFAULT_DOMAIN_DESCRIPTION = "3";

   @RBEntry("The default cabinet for a context's business objects.")
   @RBComment("The total length of the description must not exceed 200 characters, as that is the size limit for cabinet descriptions")
   public static final String DEFAULT_CABINET_DESCRIPTION = "4";

   @RBEntry("The system domain for a context's administrative objects.")
   @RBComment("The total length of the description must not exceed 200 characters, as that is the size limit for domain descriptions")
   public static final String SYSTEM_DOMAIN_DESCRIPTION = "5";

   @RBEntry("The system cabinet for a context's administrative objects.")
   @RBComment("The total length of the description must not exceed 200 characters, as that is the size limit for cabinet descriptions")
   public static final String SYSTEM_CABINET_DESCRIPTION = "6";

   @RBEntry("The group that plays the role: \"{0}\".")
   @RBArgComment0("The display name of the administrator role for the container")
   public static final String ADMINISTRATORS_GROUP_DESCRIPTION = "7";

   @RBEntry("The context \"{0}\" cannot be created because it does not have a template assigned to it.")
   @RBArgComment0("The name of the container that does not have a template")
   public static final String TEMPLATE_REQUIRED = "8";
}
