/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.inf.template.upload;

import wt.util.resource.*;

@RBUUID("wt.inf.template.upload.uploadResource")
public final class uploadResource extends WTListResourceBundle {
   @RBEntry("Unsupported file type")
   public static final String UNSUPPORTED_FILE_TYPE = "10";

   @RBEntry("Unsupported import file type")
   public static final String UNSUPPORTED_IMPORT_FILE_TYPE = "20";

   @RBEntry("Cannot create a template request with a null file")
   public static final String NULL_FILE_FOR_TEMPLATE_REQUEST = "30";

   @RBEntry("Cannot create a template from a directory")
   public static final String CANT_CREATE_TMPL_FROM_DIR = "40";

   @RBEntry("Template XML not specified")
   public static final String NO_TMPL_XML = "50";

   @RBEntry("Import from jar file can only import a single template at a time")
   public static final String IMP_FRM_JAR_CAN_ONLY_DO_ONE = "60";

   @RBEntry("There were no XML files to upload")
   public static final String NO_XML_FILES_2_UPLOAD = "70";

   @RBEntry("Jar file had no business XML to upload")
   public static final String NO_BUSINESS_XML = "80";

   @RBEntry("Too many XML files in the request file")
   public static final String TOO_MANY_XMLS = "90";
}
