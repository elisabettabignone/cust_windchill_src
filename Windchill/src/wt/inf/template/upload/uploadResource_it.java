/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.inf.template.upload;

import wt.util.resource.*;

@RBUUID("wt.inf.template.upload.uploadResource")
public final class uploadResource_it extends WTListResourceBundle {
   @RBEntry("Tipo di file non supportato")
   public static final String UNSUPPORTED_FILE_TYPE = "10";

   @RBEntry("Tipo file di importazione non supportato")
   public static final String UNSUPPORTED_IMPORT_FILE_TYPE = "20";

   @RBEntry("Impossibile creare una richiesta modello con un file null")
   public static final String NULL_FILE_FOR_TEMPLATE_REQUEST = "30";

   @RBEntry("Impossibile creare un modello da una directory")
   public static final String CANT_CREATE_TMPL_FROM_DIR = "40";

   @RBEntry("Modello XML non specificato")
   public static final String NO_TMPL_XML = "50";

   @RBEntry("L'importazione da un file jar consente di importare solo un modello alla volta")
   public static final String IMP_FRM_JAR_CAN_ONLY_DO_ONE = "60";

   @RBEntry("Non sono presenti file XML da caricare")
   public static final String NO_XML_FILES_2_UPLOAD = "70";

   @RBEntry("Nel file Jar non è presente XML aziendale da caricare")
   public static final String NO_BUSINESS_XML = "80";

   @RBEntry("Troppi file XML nel file di richiesta")
   public static final String TOO_MANY_XMLS = "90";
}
