/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.inf.template.containerdefaults;

import wt.util.resource.*;

@RBUUID("wt.inf.template.containerdefaults.containerdefaultsResource")
public final class containerdefaultsResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile trovare il profilo {0} di valori del contesto di default per il tipo di contenitore {1}. Contattare l'amministratore.")
   public static final String PROFILE_NOT_FOUND = "10";

   @RBEntry("Trovato più di un profilo di valori di default con nome {0} per la classe di contesto {1}")
   public static final String CANNOT_RESOLVE_PROFILE = "20";
}
