/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.inf.template.containerdefaults;

import wt.util.resource.*;

@RBUUID("wt.inf.template.containerdefaults.containerdefaultsResource")
public final class containerdefaultsResource extends WTListResourceBundle {
   @RBEntry("Default Context values profile {0} for container type {1} not found, contact your administrator")
   public static final String PROFILE_NOT_FOUND = "10";

   @RBEntry("Found more than one default values profile with a name of {0} for context class {1}")
   public static final String CANNOT_RESOLVE_PROFILE = "20";
}
