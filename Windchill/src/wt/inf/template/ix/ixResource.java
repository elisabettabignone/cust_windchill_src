/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.inf.template.ix;

import wt.util.resource.*;

@RBUUID("wt.inf.template.ix.ixResource")
public final class ixResource extends WTListResourceBundle {
   @RBEntry("Invalid installation, the directory {0} does not exist")
   public static final String NO_LOAD_DIRECTORY = "10";

   @RBEntry("Could not find a handler that will generate a template corresponding to class {0} with identifier {1}")
   public static final String GENERATOR_NOT_FOUND = "20";

   @RBEntry("There is already a {0} template with a name of {1} and locale of {2}")
   public static final String TEMPLATE_ID_NOT_UNIQUE = "30";
}
