/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.inf.template.ix;

import wt.util.resource.*;

@RBUUID("wt.inf.template.ix.ixResource")
public final class ixResource_it extends WTListResourceBundle {
   @RBEntry("Installazione non valida, la directory {0} non esiste")
   public static final String NO_LOAD_DIRECTORY = "10";

   @RBEntry("Impossibile trovare un gestore che generi un modello corrispondente alla classe {0} con identificatore {1}")
   public static final String GENERATOR_NOT_FOUND = "20";

   @RBEntry("Un modello {0} di nome {1} e di lingua {2} esiste già.")
   public static final String TEMPLATE_ID_NOT_UNIQUE = "30";
}
