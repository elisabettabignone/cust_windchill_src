/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.inf.library;

import wt.util.resource.*;

@RBUUID("wt.inf.library.libraryResource")
public final class libraryResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile inizializzare il contesto classico.")
   public static final String UNABLE_TO_INITIALIZE_CLASSIC = "0";

   @RBEntry("La libreria '{0}' è stata creata.")
   @RBComment("This is the message displayed when a library is successfully created.")
   public static final String LIBRARY_SUCCESSFULLY_CREATED = "1";

   @RBEntry("Errore durante la creazione della libreria: {0}")
   @RBComment("This is the message displayed when a library is not successfully created.")
   public static final String LIBRARY_NOT_SUCCESSFULLY_CREATED = "2";

   @RBEntry("\"createLibraryContainer\n\tname = {0}\n\tdescription = {1}\n\tcontainerTemplateStr = {2}\"")
   @RBComment("If the VERBOSE flag is set to true, then this is the message shows the parameters passed in to the method.")
   public static final String LIBRARY_VERBOSE_PARAMETERS = "3";

   @RBEntry("Impossibile impostare il modello {0}")
   @RBComment("If the VERBOSE flag is set to true, then this is the message shows if the template cannot be set.")
   public static final String LIBRARY_VERBOSE_BAD_TEMPLATE = "4";

   @RBEntry("Utente/gruppo/ruolo: {0}")
   @RBComment("If the VERBOSE flag is set to true, then this is the message shows the principal.")
   public static final String LIBRARY_VERBOSE_PRINCIPAL = "5";

   @RBEntry("Organizzazione: {0}")
   @RBComment("If the VERBOSE flag is set to true, then this is the message shows the organization.")
   public static final String LIBRARY_VERBOSE_ORGANIZATION = "6";
}
