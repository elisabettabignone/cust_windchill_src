/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.inf.sharing;

import wt.util.resource.*;

@RBUUID("wt.inf.sharing.sharingResource")
public final class sharingResource extends WTListResourceBundle {
   @RBEntry("Cannot create share: {0} is already shared by {1}.")
   @RBComment("Text of the message of the exception thrown when an attempt is made to share an object that is already shared by a container.")
   public static final String DUPLICATE_SHARE = "0";

   @RBEntry("Cannot create share {0}: sharing is not enabled for {1}.")
   @RBComment("Text of the message of the exception thrown when an attempt is made to share an object but the source container (the one the object exists) doesn't allow sharing.")
   public static final String SHARING_NOT_ENABLED = "1";

   @RBEntry("The following action cannot be performed on {0} : {1} . {0} belongs to a shared folder.  The operation has to be performed in the top folder.")
   @RBComment("Text of the message of the exception thrown the operation attempted is not valid.")
   public static final String OP_NOT_VALID = "2";

   @RBEntry("disable")
   @RBComment("Name of the disable operation.  This name is used in the OP_NOT_VALID message.")
   public static final String DISABLE = "3";

   @RBEntry("enable")
   @RBComment("Name of the enable operation.  This name is used in the OP_NOT_VALID message.")
   public static final String ENABLE = "4";

   @RBEntry("remove")
   @RBComment("Name of the disable operation.  This name is used in the OP_NOT_VALID message.")
   public static final String REMOVE = "5";

   @RBEntry("Cannot create share {0}: object is in a personal cabinet.")
   @RBComment("Text of the message of the exception thrown when an attempt is made to share an object that is in a personal cabinet.")
   public static final String IN_PERSONAL_CABINET = "6";

   @RBEntry("Cannot create share {0}: object is not shareable.")
   @RBComment("Text of the message of the exception thrown when an attempt is made to share an object that is not shareable.")
   public static final String OBJECT_NOT_SHAREABLE = "7";

   @RBEntry("Cannot create share because {0} is in the {1} state.")
   @RBComment("Text of the message of the exception thrown when an attempt is made to share an object but the target container is cancelled, completed or suspended.")
   public static final String CONTAINER_CLOSED = "8";

   @RBEntry("Cannot delete the objects {0} as they are shared to a project.")
   @RBComment("Text of the message of the exception thrown when an attempt is made to delete the objects shared to Project.")
   public static final String SHARED_TO_PROJECT_CONTAINER = "9";

   @RBEntry("This system is not configured to allow sharing across organizational boundaries.")
   public static final String DIFFERENT_NAMESPACES = "10";

   @RBEntry("Cannot share object because no life cycle state is provided.")
   @RBComment("Text of the message of the exception thrown when an attempt is made to share an object in state mode but the life cycle state is not provided.")
   public static final String NO_LIFECYCLE_STATE = "11";

   @RBEntry("Cannot share checked out objects: {0}.")
   @RBComment("Text of the message of the exception thrown when an attempt is made to share an object that has been checked out from a product.")
   public static final String ONE_OFF_OBJECT = "12";

   @RBEntry("ATTENTION: Secured Action. Cannot share object because selected iteration is not accessible: {0}.")
   @RBComment("Text of the message of the exception thrown when an attempt is made to share an object such that the selected iteration is not accessible to the user.")
   public static final String NOT_ACCESSIBLE = "13";

   @RBEntry("Cannot create share {0}: object's context is the same as the target context.")
   @RBComment("Text of the message of the exception thrown when an attempt is made to share an object such to the same context it resides.")
   public static final String SAME_CONTAINER = "14";

   @RBEntry("ATTENTION: Secured Action. You do not have permissions to share the following object(s): {0}.")
   @RBComment("Text of the message of the exception thrown when an attempt is made to share one or more objects which the current user does not have sufficient permissions to.")
   public static final String NO_PERMISSIONS_TO_SHARE = "15";
   
   @RBEntry("Unable to share folder {0} from product context to project. ")
   @RBComment("Text of the message of the exception thrown when an attempt is made to share a folder from product context to project.")
   public static final String INVALID_FOLDER_SHARE = "16";

   @RBEntry("Sharing an Object to Project {0}")
   @RBComment("Text to be displayed in a notification email for the ShareSummaryEvent transaction.")
   public static final String SHARING_OBJECT = "SHARING_OBJECT";

   @RBEntry("Shared To")
   @RBComment("Text to be displayed in the ProjectRevision table indicating the object has been shared to a project")
   public static final String SHARED_TO = "SHARED_TO";
}
