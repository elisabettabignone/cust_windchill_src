/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.inf.sharing;

import wt.util.resource.*;

@RBUUID("wt.inf.sharing.sharingResource")
public final class sharingResource_it extends WTListResourceBundle {
   @RBEntry("Impossibile creare condivisione: {0} è già condiviso da {1}.")
   @RBComment("Text of the message of the exception thrown when an attempt is made to share an object that is already shared by a container.")
   public static final String DUPLICATE_SHARE = "0";

   @RBEntry("Impossibile creare condivisione: {0}. La condivisione non è attivata per {1}.")
   @RBComment("Text of the message of the exception thrown when an attempt is made to share an object but the source container (the one the object exists) doesn't allow sharing.")
   public static final String SHARING_NOT_ENABLED = "1";

   @RBEntry("Impossibile sottoporre {0} all'azione che segue: {1}. {0} appartiene ad una cartella condivisa. L'operazione deve essere eseguita nella cartella superiore.")
   @RBComment("Text of the message of the exception thrown the operation attempted is not valid.")
   public static final String OP_NOT_VALID = "2";

   @RBEntry("disattivare")
   @RBComment("Name of the disable operation.  This name is used in the OP_NOT_VALID message.")
   public static final String DISABLE = "3";

   @RBEntry("attivare")
   @RBComment("Name of the enable operation.  This name is used in the OP_NOT_VALID message.")
   public static final String ENABLE = "4";

   @RBEntry("rimuovere")
   @RBComment("Name of the disable operation.  This name is used in the OP_NOT_VALID message.")
   public static final String REMOVE = "5";

   @RBEntry("Impossibile creare condivisione per {0}: l'oggetto è in uno schedario personale.")
   @RBComment("Text of the message of the exception thrown when an attempt is made to share an object that is in a personal cabinet.")
   public static final String IN_PERSONAL_CABINET = "6";

   @RBEntry("Impossibile creare condivisione {0}: l'oggetto non è condivisibile.")
   @RBComment("Text of the message of the exception thrown when an attempt is made to share an object that is not shareable.")
   public static final String OBJECT_NOT_SHAREABLE = "7";

   @RBEntry("Impossibile creare la condivisione. {0} è in stato {1}.")
   @RBComment("Text of the message of the exception thrown when an attempt is made to share an object but the target container is cancelled, completed or suspended.")
   public static final String CONTAINER_CLOSED = "8";

   @RBEntry("Impossibile gli oggetti {0} in quanto sono condivisi in un progetto.")
   @RBComment("Text of the message of the exception thrown when an attempt is made to delete the objects shared to Project.")
   public static final String SHARED_TO_PROJECT_CONTAINER = "9";

   @RBEntry("Il sistema non è configurato per consentire la condivisione attraverso i limiti di organizzazione.")
   public static final String DIFFERENT_NAMESPACES = "10";

   @RBEntry("Impossibile condividere l'oggetto perché non è stato fornito lo stato del ciclo di vita.")
   @RBComment("Text of the message of the exception thrown when an attempt is made to share an object in state mode but the life cycle state is not provided.")
   public static final String NO_LIFECYCLE_STATE = "11";

   @RBEntry("Impossibile condividere gli oggetti sottoposti a Check-Out: {0}.")
   @RBComment("Text of the message of the exception thrown when an attempt is made to share an object that has been checked out from a product.")
   public static final String ONE_OFF_OBJECT = "12";

   @RBEntry("ATTENZIONE: azione protetta. Impossibile condividere l'oggetto perché l'iterazione selezionata non è accessibile: {0}.")
   @RBComment("Text of the message of the exception thrown when an attempt is made to share an object such that the selected iteration is not accessible to the user.")
   public static final String NOT_ACCESSIBLE = "13";

   @RBEntry("Impossibile creare condivisione per {0}: il contesto dell'oggetto coincide con il contesto di destinazione.")
   @RBComment("Text of the message of the exception thrown when an attempt is made to share an object such to the same context it resides.")
   public static final String SAME_CONTAINER = "14";

   @RBEntry("ATTENZIONE: azione protetta. Non si dispone dei permessi per condividere gli oggetti che seguono: {0}.")
   @RBComment("Text of the message of the exception thrown when an attempt is made to share one or more objects which the current user does not have sufficient permissions to.")
   public static final String NO_PERMISSIONS_TO_SHARE = "15";
   
   @RBEntry("Impossibile condividere la cartella {0} da un contesto di prodotto al progetto. ")
   @RBComment("Text of the message of the exception thrown when an attempt is made to share a folder from product context to project.")
   public static final String INVALID_FOLDER_SHARE = "16";

   @RBEntry("Condivisione di un oggetto nel progetto {0}")
   @RBComment("Text to be displayed in a notification email for the ShareSummaryEvent transaction.")
   public static final String SHARING_OBJECT = "SHARING_OBJECT";

   @RBEntry("Condiviso con")
   @RBComment("Text to be displayed in the ProjectRevision table indicating the object has been shared to a project")
   public static final String SHARED_TO = "SHARED_TO";
}
