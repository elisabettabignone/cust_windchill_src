/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.inf.team;

import wt.util.resource.*;

@RBUUID("wt.inf.team.teamResource")
public final class teamResource_it extends WTListResourceBundle {
   @RBEntry("Il nome del team di contesto non può essere nullo. Specificare un nome di team contesto e riprovare.")
   public static final String INVALID_CONTAINER_TEAM_NAME = "0";

   @RBEntry("Il dominio amministrativo per il team di contesto non è valido. Selezionarne uno valido e riprovare.")
   public static final String INVALID_CONTAINER_TEAM_DOMAIN = "1";

   @RBEntry("Il gruppo obbligatorio \"{0}\" non è stato trovato. Contattare l'amministratore per correggere l'errore.")
   public static final String GROUP_NOT_FOUND = "2";

   @RBEntry("Il ruolo \"{0}\" deve avere uno o più partecipanti per il contesto: \"{1}\".")
   public static final String MANAGER_REQUIRED = "3";

   @RBEntry("Impossibile impostare il team di contesto perché l'oggetto {0} è già persistente.")
   @RBArgComment0("{0}  the ContainerTeam Managed object")
   public static final String CANT_SET_CT_WHEN_PERSISTENT = "4";

   @RBEntry("Copia di")
   public static final String COPY_OF = "5";

   @RBEntry("LoadContextTeam. È stato rilevato un errore in {0}. Verificare log del method server.")
   public static final String LOAD_EXCEPTION = "6";

   @RBEntry("Lo stato del ciclo di vita {0} non è valido per disattivare un WTContainer.")
   public static final String INVALID_INACTIVATE_STATE = "7";

   @RBEntry("Il nome di ruolo {0} non è valido. I nomi di ruolo non possono contenere i caratteri seguenti: {1}")
   public static final String INVALID_CHARACTERS_IN_ROLE_NAME = "8";

   @RBEntry("La struttura LDAP per questo contesto è corrotta. Contattare l'amministratore per risolvere il problema.")
   public static final String LDAP_CONFIGURATION_ERROR = "9";

   @RBEntry("AVVERTENZA: questo gruppo è stato creato per essere utilizzato dal sistema Windchill. La sua modifica o l'eliminazione può determinare delle conseguenze non desiderate.")
   public static final String DO_NOT_MODIFY = "10";

   @RBEntry("Impossibile sostituire l'utente {0} perché esistono degli oggetti sottoposti a Check-Out nel contesto {1}.")
   @RBComment("Text of the exception thrown when an attempt is made to replace a user in a context but the user has checked out objects in this context.")
   public static final String CO_OBJECTS = "11";

   @RBEntry("Impossibile rimuovere l'utente {0} perché esistono degli oggetti sottoposti a Check-Out nel contesto {1}.")
   @RBComment("Text of the exception thrown when an attempt is made to remove a user in a context but the user has checked out objects in this context.")
   public static final String CO_OBJECTS_REMOVE = "80";

   @RBEntry("Impossibile rimuovere l'utente {0}; è membro attivo dei contesti che seguono: {1}.")
   @RBComment("Text of the exception thrown when an attempt is made to remove a user that belongs to one or more contexts.")
   public static final String CANT_REMOVE_USER = "81";

   @RBEntry("Tutti i membri")
   public static final String PRIVATE_CONSTANT_0 = "MEMBERS";

   @RBEntry("Membri team")
   public static final String PRIVATE_CONSTANT_1 = "teamMembers";

   @RBEntry("Ospite")
   public static final String PRIVATE_CONSTANT_2 = "GUEST";

   @RBEntry("Invitato")
   public static final String PRIVATE_CONSTANT_3 = "INVITED";

   @RBEntry("Impossibile impostare l'attributo \"richiedi conferma\" perché {0} è già persistente.")
   @RBArgComment0("{0}  the ContainerTeam Managed object")
   public static final String CANT_SET_REQ_CONFIRM_WHEN_PERSISTENT = "12";

   @RBEntry("Impossibile impostare l'attributo \"invia inviti\" perché {0} è già persistente.")
   @RBArgComment0("{0}  the ContainerTeam Managed object")
   public static final String CANT_SET_SEND_INVITE_WHEN_PERSISTENT = "13";

   @RBEntry("Impossibile impostare l'attributo \"Permetti configurazione dell'accesso\" perché {0} è già persistente.")
   @RBArgComment0("{0}  the ContainerTeam Managed object")
   public static final String CANT_SET_CONFIG_ACCESS_WHEN_PERSISTENT = "14";

   /**
    * Invitation
    **/
   @RBEntry("Nessuno")
   public static final String INVITE_NONE = "31";

   @RBEntry("d MMM yyyy z")
   @RBComment("Format for the dates that appear in the context invitation.")
   public static final String INVITE_DATE_FORMAT = "32";

   @RBEntry("{0} ha invitato l'utente: {1} {2}.")
   public static final String INVITE_CONTAINER_MESSAGE = "33";

   @RBEntry("Prima di partecipare è necessario effettuare la {0}registrazione{1}.")
   public static final String INVITE_REGISTER_MESSAGE = "34";

   @RBEntry("Registra")
   public static final String INVITE_REGISTER = "35";

   @RBEntry("Invito - {0}")
   public static final String INVITATION_SUBJECT = "36";

   @RBEntry("Invito alla sostituzione - {0}")
   public static final String INVITATION_REPLACEUSER_SUBJECT = "37";

   @RBEntry("Partecipa a {0}")
   public static final String INVITE_JOIN_CONTAINER = "38";

   @RBEntry("Windchill - Invito")
   public static final String INVITE_TITLE = "39";

   @RBEntry("Nome {0}:")
   public static final String INVITE_CONTAINER_NAME_LABEL = "40";

   @RBEntry("Data d'inizio:")
   public static final String INVITE_START_DATE_LABEL = "41";

   @RBEntry("Proprietario {0}:")
   public static final String INVITE_CONTAINER_OWNER_LABEL = "42";

   @RBEntry("Data di fine stimata:")
   public static final String INVITE_EST_END_DT_LABEL = "43";

   @RBEntry("{0} host:")
   public static final String INVITE_HOST_CONTAINER_LABEL = "44";

   @RBEntry("Ruolo dell'utente:")
   public static final String INVITE_YOUR_ROLE_LABEL = "45";

   @RBEntry("Descrizione {0}:")
   public static final String INVITE_CONTAINER_DESC_LABEL = "46";

   @RBEntry("L'utente è stato invitato da {0} a sostituire {1} in {2}  {3}.")
   public static final String INVITATION_REPLACEUSER_MESSAGE = "47";

   @RBEntry("Windchill - Invito alla sostituzione")
   public static final String INVITE_REPLACEUSER_TITLE = "48";

   @RBEntry("{0} è usato in {1} posizioni. Rimuovere i seguenti componenti prima di completare l'eliminazione: {2}")
   @RBArgComment0("{0} the name of the ContextTeam")
   @RBArgComment1("{1} the number of places where the ContextTeam is in use.")
   @RBArgComment2("{2} the identities of the objects using the ContextTeam")
   public static final String DELETE_CT_IS_IN_USE = "49";

   @RBEntry(",")
   public static final String IDENTITY_STRING_DELIMITER = "50";

   @RBEntry("Utenti che partecipano a questo contesto e rivestono questo ruolo. Il gruppo viene aggiornato automaticamente in base al contesto d'appartenenza.")
   public static final String DEFAULT_ROLE_GROUP_DESCRIPTION = "51";

   @RBEntry("Utenti che hanno aderito a questo progetto e rivestono questo ruolo. Il gruppo viene aggiornato automaticamente in base al progetto d'appartenenza.")
   public static final String PROJECT_ROLE_GROUP_DESCRIPTION = "52";

   @RBEntry("Utenti che hanno aderito a questo prodotto e rivestono questo ruolo. Il gruppo viene aggiornato automaticamente in base al prodotto d'appartenenza.")
   public static final String PRODUCT_ROLE_GROUP_DESCRIPTION = "53";

   @RBEntry("Utenti che hanno aderito a questa libreria e rivestono questo ruolo. Il gruppo viene aggiornato automaticamente in base alla libreria d'appartenenza.")
   public static final String LIBRARY_ROLE_GROUP_DESCRIPTION = "54";

   @RBEntry("Utenti dell'organizzazione specificata che hanno aderito a questo contesto. Questo gruppo viene aggiornato automaticamente in base al contesto d'appartenenza.")
   public static final String DEFAULT_ORG_GROUP_DESCRIPTION = "55";

   @RBEntry("Utenti dell'organizzazione specificata che hanno aderito al progetto. Questo gruppo viene aggiornato automaticamente in base al progetto d'appartenenza.")
   public static final String PROJECT_ORG_GROUP_DESCRIPTION = "56";

   @RBEntry("Utenti dell'organizzazione specificata che hanno aderito al prodotto. Questo gruppo viene aggiornato automaticamente in base al prodotto d'appartenenza.")
   public static final String PRODUCT_ORG_GROUP_DESCRIPTION = "57";

   @RBEntry("Utenti dell'organizzazione specificata che hanno aderito alla libreria. Questo gruppo viene aggiornato automaticamente in base alla libreria d'appartenenza.")
   public static final String LIBRARY_ORG_GROUP_DESCRIPTION = "58";

   @RBEntry("Questo gruppo concede diritto di lettura a questo contesto senza richiedere l'appartenenza al gruppo. Il gruppo viene aggiornato automaticamente in base al contesto di appartenenza.")
   public static final String DEFAULT_GUEST_GROUP_DESCRIPTION = "59";

   @RBEntry("Questo gruppo concede diritto di lettura a questo progetto senza richiedere l'appartenenza al gruppo. Il gruppo viene aggiornato automaticamente in base al progetto di appartenenza.")
   public static final String PROJECT_GUEST_GROUP_DESCRIPTION = "60";

   @RBEntry("Questo gruppo concede diritto di lettura a questo prodotto senza richiedere l'appartenenza al gruppo. Il gruppo viene aggiornato automaticamente in base al progetto di appartenenza.")
   public static final String PRODUCT_GUEST_GROUP_DESCRIPTION = "61";

   @RBEntry("Questo gruppo concede diritto di lettura a questa libreria senza richiedere l'appartenenza al gruppo. Il gruppo viene aggiornato automaticamente in base al progetto di appartenenza.")
   public static final String LIBRARY_GUEST_GROUP_DESCRIPTION = "62";

   @RBEntry("Questo gruppo concede diritto di lettura senza richiedere l'appartenenza al gruppo. Il gruppo viene gestito tramite l'interfaccia del team.")
   public static final String TEAM_GUEST_GROUP_DESCRIPTION = "63";

   @RBEntry(" Tutti gli utenti aggiunti a questo contesto. Questo gruppo viene aggiornato automaticamente in base al contesto di appartenenza.")
   public static final String DEFAULT_USERS_GROUP_DESCRIPTION = "64";

   @RBEntry(" Tutti gli utenti che sono stati aggiunti a questo progetto. Il gruppo viene aggiornato automaticamente in base al progetto d'appartenenza.")
   public static final String PROJECT_USERS_GROUP_DESCRIPTION = "65";

   @RBEntry(" Tutti gli utenti che sono stati aggiunti al prodotto. Il gruppo viene aggiornato automaticamente in base al prodotto d'appartenenza.")
   public static final String PRODUCT_USERS_GROUP_DESCRIPTION = "66";

   @RBEntry(" Tutti gli utenti aggiunti alla libreria. Il gruppo viene aggiornato automaticamente in base alla libreria d'appartenenza.")
   public static final String LIBRARY_USERS_GROUP_DESCRIPTION = "67";

   @RBEntry("Utenti che vedono questo contesto nel loro elenco contesti. Questo gruppo viene automaticamente aggiornato in base al contesto d'appartenenza.")
   public static final String DEFAULT_MYPAGEQUERYABLE_GROUP_DESCRIPTION = "68";

   @RBEntry("Utenti che vedono questo progetto sul loro elenco progetti. Questo gruppo viene aggiornato automaticamente in base al progetto di appartenenza.")
   public static final String PROJECT_MYPAGEQUERYABLE_GROUP_DESCRIPTION = "69";

   @RBEntry("Utenti che vedono questo prodotto sul loro elenco prodotti. Questo prodotto viene aggiornato automaticamente in base al prodotto d'appartenenza.")
   public static final String PRODUCT_MYPAGEQUERYABLE_GROUP_DESCRIPTION = "70";

   @RBEntry("Utenti che vedono questa libreria nel loro elenco librerie. Questo gruppo viene automaticamente aggiornato in base alla libreria d'appartenenza.")
   public static final String LIBRARY_MYPAGEQUERYABLE_GROUP_DESCRIPTION = "71";

   @RBEntry("Tutti gli utenti di questo contesto che hanno ricevuto un invito. Il gruppo viene aggiornato automaticamente in base al contesto di appartenenza.")
   public static final String DEFAULT_INVITED_GROUP_DESCRIPTION = "72";

   @RBEntry("Tutti gli utenti di questo progetto che hanno ricevuto un invito. Questo gruppo viene aggiornato automaticamente in base al progetto di appartenenza.")
   public static final String PROJECT_INVITED_GROUP_DESCRIPTION = "73";

   @RBEntry("Tutti gli utenti del prodotto che hanno ricevuto un invito. Il gruppo viene aggiornato automaticamente in base al prodotto di appartenenza.")
   public static final String PRODUCT_INVITED_GROUP_DESCRIPTION = "74";

   @RBEntry("Tutti gli utenti della libreria che hanno ricevuto un invito. Il gruppo viene aggiornato automaticamente in base alla libreria di appartenenza.")
   public static final String LIBRARY_INVITED_GROUP_DESCRIPTION = "75";

   @RBEntry("Impossibile aggiungere al team utenti di altre organizzazioni.")
   public static final String WOD_CROSS_ORG_PARTICIPATION_PROHIBITED = "76";

   @RBEntry("L'utente dell'organizzazione fornitore/produttore risulta già invitato da un'altra organizzazione locataria di On-Demand, pertanto non può essere invitato a questa organizzazione.")
   public static final String WOD_SUPPLIER_USER_IN_OTHER_ORG = "77";

   @RBEntry("Programma")
   public static final String PROGRAM_SOFT_CONTAINER_NAME = "78";

   @RBEntry("L'utente è stato invitato da {0} nel team condiviso {1}.")
   public static final String SHARED_TEAM_INVITE_MESSAGE = "79";

   @RBEntry("L'utente è stato invitato da {0} a sostituire {1} nel team condiviso {2}.")
   public static final String SHARED_TEAM_INVITATION_REPLACEUSER_MESSAGE = "82";

   @RBEntry("Impossibile eliminare il team condiviso perché è ancora utilizzato da altri contenitori.")
   public static final String SHARED_TEAM_IN_USE = "83";

   @RBEntry("Esiste già un team condiviso di nome \"{0}\" nel contesto organizzazione \"{1}\".")
   public static final String SHARED_TEAM_ALREADY_EXISTS = "84";

   @RBEntry("Fornire il percorso del contesto organizzazione con il parametro della diga di comando -CONT_PATH: {0}")
   public static final String ORG_CONTAINER_REQUIRED = "85";

   @RBEntry("Impossibile eliminare il ruolo \"{0}\" dal contesto \"{1}\".")
   public static final String MANAGER_ROLE_REQUIRED = "86";

   @RBEntry("Tutti gli utenti che sono stati aggiunti a questo programma. Il gruppo viene aggiornato automaticamente in base al programma d'appartenenza.")
   public static final String PROGRAM_USERS_GROUP_DESCRIPTION = "87";

   @RBEntry("Questo gruppo concede diritto di lettura a questo programma senza richiedere l'appartenenza al gruppo. Il gruppo viene aggiornato automaticamente in base al programma di appartenenza.")
   public static final String PROGRAM_GUEST_GROUP_DESCRIPTION = "88";

   @RBEntry("Tutti gli utenti del programma che hanno ricevuto un invito. Il gruppo viene aggiornato automaticamente in base al programma di appartenenza.")
   public static final String PROGRAM_INVITED_GROUP_DESCRIPTION = "89";

   @RBEntry("Impossibile rimuovere il partecipante dal team perché {0} ha sottoposto oggetti a Check-Out in contesti usando il team.")
   @RBComment("Text of the exception thrown when an attempt is made to remove a user in a context but the user has checked out objects in this context.")
   public static final String CO_OBJECTS_REMOVE_MULTI_OBJECT = "90";

   @RBEntry("Il nome di ruolo {0} non è valido. I nomi di ruolo non possono contenere due caratteri tilde consecutivi.")
   public static final String MULTIPLE_CONSECUTIVE_TILDE_IN_ROLE_NAME = "91";

   @RBEntry("Il gruppo concede l'accesso per lettura e scaricamento ai contesti di applicazione a cui è associato il team condiviso.")
   public static final String DEFAULT_SHARED_TEAM_GUEST_GROUP_DESCRIPTION = "92";
}
