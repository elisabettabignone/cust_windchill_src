/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package wt.inf.team;

import wt.util.resource.*;

@RBUUID("wt.inf.team.teamResource")
public final class teamResource extends WTListResourceBundle {
   @RBEntry("The context team name cannot be null.  Enter a name for the context team and try again.")
   public static final String INVALID_CONTAINER_TEAM_NAME = "0";

   @RBEntry("The administrative domain for the context team is invalid.  Select a valid administrative domain and try again.")
   public static final String INVALID_CONTAINER_TEAM_DOMAIN = "1";

   @RBEntry("Required group \"{0}\" was not found.  Contact admistrator to fix error.")
   public static final String GROUP_NOT_FOUND = "2";

   @RBEntry("The \"{0}\" role must have one or more participants for the context: \"{1}\".")
   public static final String MANAGER_REQUIRED = "3";

   @RBEntry("You cannot set the Context Team because the {0} object is already persisted.")
   @RBArgComment0("{0}  the ContainerTeam Managed object")
   public static final String CANT_SET_CT_WHEN_PERSISTENT = "4";

   @RBEntry("Copy Of")
   public static final String COPY_OF = "5";

   @RBEntry("LoadContextTeam: Error detected in {0}.  Check Method Server log.")
   public static final String LOAD_EXCEPTION = "6";

   @RBEntry("The {0} state is not valid for inactivating a WTContainer.")
   public static final String INVALID_INACTIVATE_STATE = "7";

   @RBEntry("The {0} role name is invalid.  Role names cannot contain any of the following characters:  {1}")
   public static final String INVALID_CHARACTERS_IN_ROLE_NAME = "8";

   @RBEntry("The LDAP tree for this context is corrupted.  Please contact your administrator to fix the problem.")
   public static final String LDAP_CONFIGURATION_ERROR = "9";

   @RBEntry("WARNING:  This group was created for use by the Windchill System.  Modification or deletion of this group may have undesirable consequences in Windchill.")
   public static final String DO_NOT_MODIFY = "10";

   @RBEntry("Cannot replace user {0} because there are checked out objects in context {1}.")
   @RBComment("Text of the exception thrown when an attempt is made to replace a user in a context but the user has checked out objects in this context.")
   public static final String CO_OBJECTS = "11";

   @RBEntry("Cannot remove user {0} because there are checked out objects in context {1}.")
   @RBComment("Text of the exception thrown when an attempt is made to remove a user in a context but the user has checked out objects in this context.")
   public static final String CO_OBJECTS_REMOVE = "80";

   @RBEntry("Cannot remove user {0} because they are active members in the following contexts {1}.")
   @RBComment("Text of the exception thrown when an attempt is made to remove a user that belongs to one or more contexts.")
   public static final String CANT_REMOVE_USER = "81";

   @RBEntry("All Members")
   public static final String PRIVATE_CONSTANT_0 = "MEMBERS";

   @RBEntry("Team Members")
   public static final String PRIVATE_CONSTANT_1 = "teamMembers";

   @RBEntry("Guest")
   public static final String PRIVATE_CONSTANT_2 = "GUEST";

   @RBEntry("Invited")
   public static final String PRIVATE_CONSTANT_3 = "INVITED";

   @RBEntry("You cannot set the 'require confirmation' attribute because {0} is already persisted.")
   @RBArgComment0("{0}  the ContainerTeam Managed object")
   public static final String CANT_SET_REQ_CONFIRM_WHEN_PERSISTENT = "12";

   @RBEntry("You cannot set the 'send invitations' attribute because {0} is already persisted.")
   @RBArgComment0("{0}  the ContainerTeam Managed object")
   public static final String CANT_SET_SEND_INVITE_WHEN_PERSISTENT = "13";

   @RBEntry("You cannot set the 'allow user to configure access' attribute because {0} is already persisted.")
   @RBArgComment0("{0}  the ContainerTeam Managed object")
   public static final String CANT_SET_CONFIG_ACCESS_WHEN_PERSISTENT = "14";

   /**
    * Invitation
    **/
   @RBEntry("None")
   public static final String INVITE_NONE = "31";

   @RBEntry("MMM d, yyyy z")
   @RBComment("Format for the dates that appear in the context invitation.")
   public static final String INVITE_DATE_FORMAT = "32";

   @RBEntry("{0} has invited you to the {1} {2}.")
   public static final String INVITE_CONTAINER_MESSAGE = "33";

   @RBEntry("You must first {0}register{1} before you can join.")
   public static final String INVITE_REGISTER_MESSAGE = "34";

   @RBEntry("Register")
   public static final String INVITE_REGISTER = "35";

   @RBEntry("Invitation - {0}")
   public static final String INVITATION_SUBJECT = "36";

   @RBEntry("Replacement Invitation - {0}")
   public static final String INVITATION_REPLACEUSER_SUBJECT = "37";

   @RBEntry("Join {0}")
   public static final String INVITE_JOIN_CONTAINER = "38";

   @RBEntry("Windchill Invitation")
   public static final String INVITE_TITLE = "39";

   @RBEntry("{0} Name:")
   public static final String INVITE_CONTAINER_NAME_LABEL = "40";

   @RBEntry("Start Date:")
   public static final String INVITE_START_DATE_LABEL = "41";

   @RBEntry("{0} Owner:")
   public static final String INVITE_CONTAINER_OWNER_LABEL = "42";

   @RBEntry("Estimated End Date:")
   public static final String INVITE_EST_END_DT_LABEL = "43";

   @RBEntry("Host {0}:")
   public static final String INVITE_HOST_CONTAINER_LABEL = "44";

   @RBEntry("Your Role:")
   public static final String INVITE_YOUR_ROLE_LABEL = "45";

   @RBEntry("{0} Description:")
   public static final String INVITE_CONTAINER_DESC_LABEL = "46";

   @RBEntry("{0} has invited you to replace {1} in the {2} {3}.")
   public static final String INVITATION_REPLACEUSER_MESSAGE = "47";

   @RBEntry("Windchill Replacement Invitation")
   public static final String INVITE_REPLACEUSER_TITLE = "48";

   @RBEntry(" {0} is used in {1} places.  The following uses must be removed before completing the delete: {2}")
   @RBArgComment0("{0} the name of the ContextTeam")
   @RBArgComment1("{1} the number of places where the ContextTeam is in use.")
   @RBArgComment2("{2} the identities of the objects using the ContextTeam")
   public static final String DELETE_CT_IS_IN_USE = "49";

   @RBEntry(",")
   public static final String IDENTITY_STRING_DELIMITER = "50";

   @RBEntry("Users who have joined this context and perform this role.  This group is updated automatically based on context membership.")
   public static final String DEFAULT_ROLE_GROUP_DESCRIPTION = "51";

   @RBEntry("Users who have joined this Project and perform this role.  This group is updated automatically based on project membership.")
   public static final String PROJECT_ROLE_GROUP_DESCRIPTION = "52";

   @RBEntry("Users who have joined this Product and perform this role.  This group is updated automatically based on product membership.")
   public static final String PRODUCT_ROLE_GROUP_DESCRIPTION = "53";

   @RBEntry("Users who have joined this Library and perform this role.  This group is updated automatically based on library membership.")
   public static final String LIBRARY_ROLE_GROUP_DESCRIPTION = "54";

   @RBEntry("Users from the named organization who have joined this context.  This group is updated automatically based on context membership.")
   public static final String DEFAULT_ORG_GROUP_DESCRIPTION = "55";

   @RBEntry("Users from the named organization who have joined this project.  This group is updated automatically based on project membership.")
   public static final String PROJECT_ORG_GROUP_DESCRIPTION = "56";

   @RBEntry("Users from the named organization who have joined this product.  This group is updated automatically based on product membership.")
   public static final String PRODUCT_ORG_GROUP_DESCRIPTION = "57";

   @RBEntry("Users from the named organization who have joined this library.  This group is updated automatically based on library membership.")
   public static final String LIBRARY_ORG_GROUP_DESCRIPTION = "58";

   @RBEntry("This group grants read access to this context without requiring membership.  This group is updated automatically based on context membership.")
   public static final String DEFAULT_GUEST_GROUP_DESCRIPTION = "59";

   @RBEntry("This group grants read access to this project without requiring membership.  This group is updated automatically based on project membership.")
   public static final String PROJECT_GUEST_GROUP_DESCRIPTION = "60";

   @RBEntry("This group grants read access to this product without requiring membership.  This group is updated automatically based on product membership.")
   public static final String PRODUCT_GUEST_GROUP_DESCRIPTION = "61";

   @RBEntry("This group grants read access to this library without requiring membership.  This group is updated automatically based on library membership.")
   public static final String LIBRARY_GUEST_GROUP_DESCRIPTION = "62";

   @RBEntry("This group grants read access without requiring membership.  This group is managed through the team interface.")
   public static final String TEAM_GUEST_GROUP_DESCRIPTION = "63";

   @RBEntry(" All users who have been added to this context.  This group is updated automatically based on context membership.")
   public static final String DEFAULT_USERS_GROUP_DESCRIPTION = "64";

   @RBEntry(" All users who have been added to this project.  This group is updated automatically based on project membership.")
   public static final String PROJECT_USERS_GROUP_DESCRIPTION = "65";

   @RBEntry(" All users who have been added to this product.  This group is updated automatically based on product membership.")
   public static final String PRODUCT_USERS_GROUP_DESCRIPTION = "66";

   @RBEntry(" All users who have been added to this library.  This group is updated automatically based on library membership.")
   public static final String LIBRARY_USERS_GROUP_DESCRIPTION = "67";

   @RBEntry("Users who see this context on their context list.  This group is updated automatically based on context membership.")
   public static final String DEFAULT_MYPAGEQUERYABLE_GROUP_DESCRIPTION = "68";

   @RBEntry("Users who see this project on their project list.  This group is updated automatically based on project membership.")
   public static final String PROJECT_MYPAGEQUERYABLE_GROUP_DESCRIPTION = "69";

   @RBEntry("Users who see this pducuct on their product list.  This group is updated automatically based on product membership.")
   public static final String PRODUCT_MYPAGEQUERYABLE_GROUP_DESCRIPTION = "70";

   @RBEntry("Users who see this library on their library list.  This group is updated automatically based on library membership.")
   public static final String LIBRARY_MYPAGEQUERYABLE_GROUP_DESCRIPTION = "71";

   @RBEntry("All users in this context that have received an invitation.  This group is updated automatically based on context membership.")
   public static final String DEFAULT_INVITED_GROUP_DESCRIPTION = "72";

   @RBEntry("All users in this project that have received an invitation.  This group is updated automatically based on project membership.")
   public static final String PROJECT_INVITED_GROUP_DESCRIPTION = "73";

   @RBEntry("All users in this product that have received an invitation.  This group is updated automatically based on product membership.")
   public static final String PRODUCT_INVITED_GROUP_DESCRIPTION = "74";

   @RBEntry("All users in this library that have received an invitation.  This group is updated automatically based on library membership.")
   public static final String LIBRARY_INVITED_GROUP_DESCRIPTION = "75";

   @RBEntry("Users from other organizations cannot be added to the team.")
   public static final String WOD_CROSS_ORG_PARTICIPATION_PROHIBITED = "76";

   @RBEntry("User from supplier organization already invited to another multi-tenant organization, so cannot be invited to this organization.")
   public static final String WOD_SUPPLIER_USER_IN_OTHER_ORG = "77";

   @RBEntry("Program")
   public static final String PROGRAM_SOFT_CONTAINER_NAME = "78";

   @RBEntry("{0} has invited you to the shared team {1}.")
   public static final String SHARED_TEAM_INVITE_MESSAGE = "79";

   @RBEntry("{0} has invited you to replace {1} in the shared team {2}.")
   public static final String SHARED_TEAM_INVITATION_REPLACEUSER_MESSAGE = "82";

   @RBEntry("The shared team can not be deleted because it is still being used by other containers.")
   public static final String SHARED_TEAM_IN_USE = "83";

   @RBEntry("A shared team with the name \"{0}\" already exists, within the Org context of \"{1}\".")
   public static final String SHARED_TEAM_ALREADY_EXISTS = "84";

   @RBEntry("You must supply the org context path with the -CONT_PATH command line parameter: {0}")
   public static final String ORG_CONTAINER_REQUIRED = "85";

   @RBEntry("The \"{0}\" role can not be deleted for the context: \"{1}\".")
   public static final String MANAGER_ROLE_REQUIRED = "86";

   @RBEntry("All users who have been added to this program.  This group is updated automatically based on program membership.")
   public static final String PROGRAM_USERS_GROUP_DESCRIPTION = "87";

   @RBEntry("This group grants read access to this program without requiring membership.  This group is updated automatically based on program membership.")
   public static final String PROGRAM_GUEST_GROUP_DESCRIPTION = "88";

   @RBEntry("All users in this program that have received an invitation.  This group is updated automatically based on program membership.")
   public static final String PROGRAM_INVITED_GROUP_DESCRIPTION = "89";

   @RBEntry("Cannot remove participant from team because {0} has checked out objects in context(s) using team.")
   @RBComment("Text of the exception thrown when an attempt is made to remove a user in a context but the user has checked out objects in this context.")
   public static final String CO_OBJECTS_REMOVE_MULTI_OBJECT = "90";

   @RBEntry("The {0} role name is invalid.  Role names cannot contain two consecutive tilde characters.")
   public static final String MULTIPLE_CONSECUTIVE_TILDE_IN_ROLE_NAME = "91";

   @RBEntry("This group grants read and download access to the application contexts that the shared team is associated to.")
   public static final String DEFAULT_SHARED_TEAM_GUEST_GROUP_DESCRIPTION = "92";
}
