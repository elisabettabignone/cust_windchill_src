package ext.elettric80.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

import wt.util.WTProperties;

public class Elettric80Properties {

  private static Properties props;

  static {
    try {
      props = WTProperties.getLocalProperties();
      props.load(new FileInputStream(new File(props.getProperty("wt.codebase.location") + File.separator + "ext" + File.separator + "elettric80"
          + File.separator + "elettric80.properties")));
    }
    catch (Exception ex) {
      System.out.println("ERROR: problemi a leggere i file di property!");
      ex.printStackTrace();
    }
  }

  public static String getProperty(String nameProp) throws IOException {
    if (!props.containsKey(nameProp)) {
      throw new IOException("Proprieta' [" + nameProp + "] non presente!");
    }
    return props.getProperty(nameProp);
  }

  public static String getProperty(String nameProp, String defaultValue) {
    try {
      return getProperty(nameProp);
    }
    catch (IOException ioe) {
      return defaultValue;
    }
  }

  public static boolean getProperty(String nameProp, boolean defaultValue) {
    try {
      String prop = getProperty(nameProp);
      return Boolean.valueOf(prop);
    }
    catch (IOException ioe) {
      return defaultValue;
    }
    catch (Exception ex) {
      return defaultValue;
    }
  }

  public static void main(String s[]) {
    Enumeration en = props.keys();
    while (en.hasMoreElements()) {
      String nameProp = en.nextElement().toString();
      if (s.length == 0) {
        System.out.println("Property [" + nameProp + "] = [" + props.getProperty(nameProp) + "]");
      }
      else {
        if (nameProp.startsWith(s[0]))
          System.out.println("Property [" + nameProp + "] = [" + props.getProperty(nameProp) + "]");
      }
    }
  }

}
