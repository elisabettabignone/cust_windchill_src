package ext.elettric80.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;

import wt.epm.EPMDocument;
import wt.epm.EPMDocumentMaster;
import wt.epm.structure.EPMMemberLink;
import wt.epm.structure.EPMReferenceLink;
import wt.epm.structure.EPMStructureHelper;
import wt.fc.Persistable;
import wt.fc.PersistenceHelper;
import wt.fc.QueryResult;
import wt.iba.definition.litedefinition.AttributeDefDefaultView;
import wt.iba.definition.service.StandardIBADefinitionService;
import wt.iba.value.DefaultAttributeContainer;
import wt.iba.value.IBAHolder;
import wt.iba.value.litevalue.AbstractValueView;
import wt.iba.value.litevalue.BooleanValueDefaultView;
import wt.iba.value.service.IBAValueHelper;
import wt.part.WTPartReferenceLink;
import wt.util.WTException;
import wt.util.WTStandardDateFormat;
import wt.vc.Mastered;
import wt.vc.VersionControlHelper;
import wt.vc.config.LatestConfigSpec;
import ext.caditech.utility.LogWrapper;
import ext.caditech.utility.SearchEPMDocument;

/**
 * Classe per gestire le code di pubblicazione
 *
 * @author administrator
 *
 */
public class CodaDiPubblicazione {

  /** La lista di oggetti da pubblicare. */
  private ArrayList<EPMDocumentDaPubblicare> oggettiDaPubblicare = new ArrayList<EPMDocumentDaPubblicare>();

  /**
   * Crea la lista dei drawing dove � usato il documento passato come argomento, i drawing trovati vengono messi in
   * lista solo se in release massima
   *
   * @param epmDocument
   *          parte o assieme di cui cercare i drawing
   * @return lista dei drawing trovati
   * @throws WTException
   * @throws ListenerCodaDiPubblicazioneException
   */
  private ArrayList<EPMDocument> findDrawing(EPMDocument epmDocument) throws WTException, ListenerCodaDiPubblicazioneException {
    ArrayList<EPMDocument> drawing = new ArrayList<EPMDocument>();

    // stampa("Sono state trovate " + qr.size() + " dipendenze con navigateReferencedBy true ");
    String numberPrec = "";
    String versPrec = "";
    String iterPrec = "";
    String versMax = "";
    String iterMax = "";
    EPMDocument latestDRW = null;

    QueryResult qr = PersistenceHelper.manager.navigate(epmDocument.getMaster(), WTPartReferenceLink.REFERENCED_BY_ROLE, EPMReferenceLink.class,
        false);

    LogWrapper.logMessage(LogWrapper.DEBUG, "CDT: sono state trovate " + qr.size() + " dipendenze di tipo WTPartReferenceLink.REFERENCED_BY_ROLE");
    while (qr.hasMoreElements()) {
      Persistable p = (Persistable) qr.nextElement();
      if (p instanceof EPMReferenceLink) {
        EPMReferenceLink reference = (EPMReferenceLink) p;
        // --------------------------------------------------------------------------------
        // Recupero drawing
        // --------------------------------------------------------------------------------
        if (reference.getReferenceType().getStringValue().equalsIgnoreCase("wt.epm.structure.EPMReferenceType.DRAWING")) {

          if (reference.getRoleAObjectRef().getObject() instanceof EPMDocument) {
            EPMDocument drw = (EPMDocument) reference.getRoleAObjectRef().getObject();
            String number = drw.getNumber();
            String vers = drw.getVersionIdentifier().getValue();
            String iter = drw.getIterationIdentifier().getValue();

            boolean nuovo = SearchEPMDocument.isNewObject(drw);
            if (nuovo) {
              LogWrapper.logMessage(LogWrapper.DEBUG, "CDT: oggetto nuovo che non metto in coda " + drw.getNumber() + " -versione: " + vers
                  + " - iterazione: " + iter);
              continue;
            }

            // La query restituisce pi� volte lo stesso oggetto in stessa revisione e versione, con questo controllo li
            // saltiamo
            if (number.equalsIgnoreCase(numberPrec) && vers.equalsIgnoreCase(versPrec) && iter.equalsIgnoreCase(iterPrec)) {
              continue;
            }
            // stampa("Trovato assieme " + number + " versione: " + vers + " - iterazione: " + iter);
            // Se l'oggetto � lo stesso del giro precedente non devo cercare la release massima, perch� l'ho gi� cercata
            // al
            // giro precedente
            if (!number.equalsIgnoreCase(numberPrec)) {
              try {
                // latestDRW = this.getLatestVersion(drw.getMaster());
                latestDRW = SearchEPMDocument.findLatestEPMDocuments(drw.getNumber());
                if (latestDRW == null) {
                  String msg = "CDT: ERRORE cercando la release massima del drw " + drw.getNumber() + " (nessun documento trovato) -versione: "
                      + vers + " - iterazione: " + iter;
                  LogWrapper.logMessage(LogWrapper.ERROR, msg);
                  throw new ListenerCodaDiPubblicazioneException(msg);
                }
                versMax = latestDRW.getVersionIdentifier().getValue();
                iterMax = latestDRW.getIterationIdentifier().getValue();
                // stampa("La release massima �  " + latestDRW.getNumber() + " versione: " + versMax + " - iterazione: "
                // + iterMax);
              }
              catch (Exception e) {
                LogWrapper.logMessage(LogWrapper.ERROR, "CDT: ERRORE cercando la release massima del drw " + drw.getNumber() + " versione: " + vers
                    + " - iterazione: " + iter);
                throw new ListenerCodaDiPubblicazioneException("ERRORE cercando la release massima del drw " + drw.getNumber() + " versione: " + vers
                    + " - iterazione: " + iter, e);

              }
            }
            // Metto l'oggetto in coda solo se coincide con la versione e iterazione massima
            if (versMax.equalsIgnoreCase(vers) && iterMax.equalsIgnoreCase(iter)) {
              LogWrapper.logMessage(LogWrapper.DEBUG, "CDT: metto in coda  " + drw.getNumber() + " versione: " + vers + " - iterazione: " + iter);
              drawing.add(drw);
            }
            numberPrec = number;
            versPrec = vers;
            iterPrec = iter;
          }
        }
      }
    }
    LogWrapper.logMessage(LogWrapper.DEBUG, "CDT: inseriti " + drawing.size() + " drw nella lista dei drawing");
    return drawing;
  }

  /**
   * Crea la lista con gli assiemi in cui � usato il documento passato come argomento (WHERE USED), i componenti vengono
   * aggiunti alla lista solo se non soppressi e se in massima release.
   *
   * @param epmDocument
   *          documento di cui fare il where used
   * @return la lista di assiemi in massima release in cui � usato il documento
   * @throws ListenerCodaDiPubblicazioneException
   * @throws Exception
   */
  private ArrayList<EPMDocument> whereUsedSenzaSoppressi(EPMDocument epmDocument) throws WTException, ListenerCodaDiPubblicazioneException {

    ArrayList<EPMDocument> assiemi = new ArrayList<EPMDocument>();
    QueryResult qr = EPMStructureHelper.service.navigateUsedBy((EPMDocumentMaster) epmDocument.getMaster(), null, false);
    LogWrapper.logMessage(LogWrapper.DEBUG, "CDT: sono state trovate " + qr.size() + " dipendenze con navigateUsedBy");
    String numberPrec = "";
    String versPrec = "";
    String iterPrec = "";

    String versMax = "";
    String iterMax = "";
    EPMDocument latestEPM = null;

    while (qr.hasMoreElements()) {
      Persistable p = (Persistable) qr.nextElement();
      if (p instanceof EPMMemberLink) {
        EPMMemberLink link = (EPMMemberLink) p;
        EPMDocument epm = link.getUsedBy();
        String number = epm.getNumber();
        String vers = epm.getVersionIdentifier().getValue();
        String iter = epm.getIterationIdentifier().getValue();

        boolean nuovo = SearchEPMDocument.isNewObject(epm);
        if (nuovo) {
          LogWrapper.logMessage(LogWrapper.DEBUG, "CDT: oggetto nuovo che non metto in coda " + epm.getNumber() + " - versione: " + vers
              + " - iterazione: " + iter);
          continue;
        }
        // La query restituisce pi� volte lo stesso oggetto in stessa revisione e versione, con questo controllo li
        // saltiamo
        if (number.equalsIgnoreCase(numberPrec) && vers.equalsIgnoreCase(versPrec) && iter.equalsIgnoreCase(iterPrec)) {
          continue;
        }
        LogWrapper.logMessage(LogWrapper.DEBUG, "CDT: trovato assieme " + number + " versione: " + vers + " - iterazione: " + iter);
        // Se l'oggetto � lo stesso del giro precedente non devo cercare la release massima, perch� l'ho gi� cercata al
        // giro precedente
        if (!number.equalsIgnoreCase(numberPrec)) {
          try {
            // latestEPM = this.getLatestVersion(epm.getMaster());
            latestEPM = SearchEPMDocument.findLatestEPMDocuments(epm.getNumber());
            if (latestEPM == null) {
              String msg = "CDT: ERRORE cercando la release massima di " + epm.getNumber() + " (nessun documento trovato) - versione: " + vers
                  + " - iterazione: " + iter;
              LogWrapper.logMessage(LogWrapper.ERROR, msg);
              throw new ListenerCodaDiPubblicazioneException(msg);
            }
            versMax = latestEPM.getVersionIdentifier().getValue();
            iterMax = latestEPM.getIterationIdentifier().getValue();
            LogWrapper.logMessage(LogWrapper.DEBUG, "CDT: la release massima � " + latestEPM.getNumber() + " versione: " + versMax
                + " - iterazione: " + iterMax);
          }
          catch (Exception e) {
            LogWrapper.logMessage(LogWrapper.ERROR, "CDT: ERRORE cercando la release massima di " + epm.getNumber() + " versione: " + vers
                + " - iterazione: " + iter);
            throw new ListenerCodaDiPubblicazioneException("ERRORE cercando la release massima di " + epm.getNumber() + " versione: " + vers
                + " - iterazione: " + iter, e);
          }
        }

        // Metto l'oggetto in coda solo se non � soppresso e se coincide con la versione e iterazione massima
        if (!link.isSuppressed() && versMax.equalsIgnoreCase(vers) && iterMax.equalsIgnoreCase(iter)) {
          LogWrapper.logMessage(LogWrapper.DEBUG, "CDT: metto in coda  " + epm.getNumber() + " versione: " + vers + " - iterazione: " + iter);
          assiemi.add(epm);
        }
        numberPrec = number;
        versPrec = vers;
        iterPrec = iter;
      }
    }
    LogWrapper.logMessage(LogWrapper.DEBUG, "CDT: inseriti " + assiemi.size() + " asm nella lista degli assiemi");
    return assiemi;
  }

  /***
   * Funzione trovata su Internet che restituisce l'ultima iterazione dell'ultima versione del documento passato come
   * argomento
   *
   * @param mastered
   *          oggetto di cui cerca la massima release
   * @return oggetto in massima release
   * @throws Exception
   *           in caso di errore nella ricerca
   */
  private EPMDocument getLatestVersion(Mastered mastered) throws Exception {
    QueryResult queryResult = VersionControlHelper.service.allVersionsOf(mastered);
    LatestConfigSpec latestConfigSpec = new LatestConfigSpec();
    queryResult = latestConfigSpec.process(queryResult);
    return (EPMDocument) queryResult.nextElement();
  }

  /**
   * Legge un attributo di tipo booleano
   *
   * @param epmDocument
   *          documento di cui cercare l'attributo
   * @param attributo
   *          nome dell'attributo
   * @return il valore dell'attrbuto se � stato trovato, false altrimenti
   * @throws WTException
   *           errore nella ricerca dell'attributo
   * @throws RemoteException
   *           errore nella ricerca dell'attributo
   */
  private boolean leggiAttributoBool(EPMDocument epmDocument, String attributo) throws RemoteException, WTException {
    LogWrapper.logMessage(LogWrapper.DEBUG, "CDT: cerco il valore dell'attributo " + attributo);
    boolean valore = false;
    IBAHolder ibaHolder = (IBAHolder) epmDocument;
    DefaultAttributeContainer attributeContainer = (DefaultAttributeContainer) ibaHolder.getAttributeContainer();
    ibaHolder = IBAValueHelper.service.refreshAttributeContainer(ibaHolder, null, null, null);
    attributeContainer = (DefaultAttributeContainer) ibaHolder.getAttributeContainer();
    if (attributeContainer == null) {
      LogWrapper.logMessage(LogWrapper.DEBUG, "CDT: attributo: " + attributo + " -> attributeContainer e' null");
      return valore;
    }
    StandardIBADefinitionService defService = new StandardIBADefinitionService();
    AttributeDefDefaultView attributeDefinition = defService.getAttributeDefDefaultViewByPath(attributo);
    AbstractValueView[] abstractValueView = attributeContainer.getAttributeValues(attributeDefinition);
    if (abstractValueView.length != 0) {
      String valoreStringa = ((BooleanValueDefaultView) abstractValueView[0]).getLocalizedDisplayString();
      valore = Boolean.parseBoolean(valoreStringa);
      LogWrapper.logMessage(LogWrapper.DEBUG, "CDT: attributo: " + attributo + " -> valore: " + valore);
    }
    else {
      LogWrapper.logMessage(LogWrapper.DEBUG, "CDT: attributo: " + attributo + " abstractValueView.length==0");
    }

    return valore;
  }

  /**
   * Aggiunge un oggetto alla lista degli oggetti da pubblicare: se nella lista esiste gi� un oggetto con lo stesso
   * number, allora vengono confrontati iterazione e versione in modo da tenere il documento pi� recente
   *
   * @param oggettiDaPubblicare
   *          lista degli oggetti da pubblicare
   * @param epmDocument
   *          documento da aggiungere in coda
   * @param livello
   *          livello dell'oggetto da mettere in coda
   */
  private void aggiungiInCoda(ArrayList<EPMDocumentDaPubblicare> oggettiDaPubblicare, EPMDocument epmDocument, int livello) {

    int versione = 0;
    int iterazione = 0;
    if (!epmDocument.getVersionIdentifier().getValue().equals("-")) {
      try {
        versione = Integer.parseInt(epmDocument.getVersionIdentifier().getValue());
      }
      catch (NumberFormatException e) {
        LogWrapper.logMessage(LogWrapper.WARN, "CDT: NumberFormatException nel parse della versione del documento, verr� messo 0");
      }
    }
    if (!epmDocument.getIterationIdentifier().getValue().equals("-")) {
      try {
        iterazione = Integer.parseInt(epmDocument.getIterationIdentifier().getValue());
      }
      catch (NumberFormatException e) {
        LogWrapper.logMessage(LogWrapper.WARN, "CDT: NumberFormatException nel parse dell'iterazione del documento, verr� messo 0");
      }
    }
    LogWrapper.logMessage(LogWrapper.DEBUG, "CDT: EPMDocument da aggiungere in coda: " + epmDocument.getNumber() + " - versione: " + versione
        + "- iterazione: " + iterazione);

    boolean giaInCoda = false;
    // -----------------------------------------------------
    // scorro tutti gli oggetti gi� da pubblicare per controllare che epmDocument non ci sia gi�
    // -----------------------------------------------------
    for (EPMDocumentDaPubblicare op : oggettiDaPubblicare) {

      EPMDocument inCoda = op.getDocumento();

      // stampa("EPMDocument gi� in coda:  " + inCoda.getNumber() + " - versione: " + versioneInCoda + "- iterazione: "
      // + iterazioneInCoda);
      // -----------------------------------------------------------------------------------------------------
      // se trovo in coda un elemento con stesso number confronto il livello
      // -----------------------------------------------------------------------------------------------------
      if (inCoda.getNumber().equalsIgnoreCase(epmDocument.getNumber())) {

        LogWrapper.logMessage(LogWrapper.DEBUG, "CDT: esiste in coda un elemento con stesso number: " + inCoda.getNumber() + " -versione:"
            + inCoda.getVersionIdentifier().getValue() + "-iterazione: " + inCoda.getIterationIdentifier().getValue());
        // ---------------------------------------------------
        // confronto i livelli, in modo da tenere il pi� basso
        // ---------------------------------------------------
        if (livello < op.getLivello()) {
          LogWrapper.logMessage(LogWrapper.DEBUG, "CDT: abbasso il livello da " + op.getLivello() + " a " + livello);
          op.setLivello(livello);
        }
        giaInCoda = true;
        break;
      }
    }
    if (!giaInCoda) {
      LogWrapper.logMessage(LogWrapper.DEBUG, "CDT: aggiunto in coda " + epmDocument.getNumber() + "  livello: " + livello);
      oggettiDaPubblicare.add(new EPMDocumentDaPubblicare(epmDocument, livello));
    }
  }

  /**
   * Crea la lista di oggetti da pubblicare, aggiornando la variabile di classe oggettiDaPubblicare.
   *
   * @param oggettiDaPubblicare
   *          la lista di oggetti da pubblicare.
   * @param epmDocument
   *          il documento EPM
   * @param livello
   *          il livello
   * @param promote
   *          se siamo nel caso promote non � necessario fare la chiamata ricorsiva sui livelli superiori in cui �
   *          montato il modello
   * @throws ListenerCodaDiPubblicazioneException
   *           l'eccezione sul listener delle code.
   */
  void creaListaEPMDocumentDaPubblicare(EPMDocument epmDocument, int livello, boolean promote) throws ListenerCodaDiPubblicazioneException {
    LogWrapper.logMessage(LogWrapper.INFO, "CDT: ------------ aggiungiInListaOggettiDaPubblicare: " + epmDocument.getNumber() + "-vers: "
        + epmDocument.getVersionIdentifier().getValue() + " -iter: " + epmDocument.getIterationIdentifier().getValue() + " -livello: " + livello
        + " ------------");
    // --------------------------------------------------------------------------------
    // Il documento non � un assieme o parte o disegno => non � da pubblicare
    // --------------------------------------------------------------------------------
    String number = epmDocument.getNumber();
    String ext = number.substring(number.lastIndexOf(".") + 1, number.length());

    if (!ext.equalsIgnoreCase("asm") && !ext.equalsIgnoreCase("prt") && !ext.equalsIgnoreCase("drw")) {
      LogWrapper.logMessage(LogWrapper.INFO, "CDT: " + epmDocument.getNumber() + " non � da pubblicare, perch� non � un assieme o parte o disegno");
      return;
    }
    // --------------------------------------------------------------------------------
    // Se � una parte o un assieme devo fare il where used
    // --------------------------------------------------------------------------------
    LogWrapper.logMessage(LogWrapper.INFO, "CDT: controllo se " + epmDocument.getNumber() + " � da pubblicare");

    if (ext.equalsIgnoreCase("asm") || ext.equalsIgnoreCase("prt")) {

      ArrayList<EPMDocument> drawing = new ArrayList<EPMDocument>();
      ArrayList<EPMDocument> assiemi = new ArrayList<EPMDocument>();

      try {
        LogWrapper.logMessage(LogWrapper.DEBUG, "CDT: -- Inizio findDrawing");
        drawing = findDrawing(epmDocument);
        LogWrapper.logMessage(LogWrapper.DEBUG, "CDT: -- Fine findDrawing");
      }
      catch (WTException e) {
        LogWrapper.logMessage(LogWrapper.ERROR, "CDT: errore ricercando i drawing per " + epmDocument.getNumber(), e);
        throw new ListenerCodaDiPubblicazioneException("Errore ricercando drawing per " + epmDocument.getNumber(), e);
      }
      if (!promote) {
        try {
          LogWrapper.logMessage(LogWrapper.DEBUG, "CDT: -- Inizio where used");
          assiemi = whereUsedSenzaSoppressi(epmDocument);
          LogWrapper.logMessage(LogWrapper.DEBUG, "CDT: -- Fine where used");
        }
        catch (WTException e) {
          LogWrapper.logMessage(LogWrapper.ERROR, "CDT: Errore ricercando gli assiemi in cui � usato " + epmDocument.getNumber(), e);
          throw new ListenerCodaDiPubblicazioneException("Errore ricercando gli assiemi in cui � usato " + epmDocument.getNumber(), e);
        }

      }
      else {
        LogWrapper.logMessage(LogWrapper.INFO, "CDT: caso Promote quindi non serve il where used");
      }

      // --------------------------------------------------------------------------------
      // Se un oggetto ha almeno un drw deve essere pubblicato a meno che non sia un b3*_*
      // --------------------------------------------------------------------------------
      int livelloSuccessivo = livello;
      if (drawing.size() != 0) {
        LogWrapper.logMessage(LogWrapper.INFO, "CDT: il numero di drawing trovati � " + drawing.size());
        // --------------------------------------------------------------------------------
        // � un b3*_*?
        // --------------------------------------------------------------------------------
        boolean b3 = (epmDocument.getNumber().startsWith("b3") || epmDocument.getNumber().startsWith("B3")) && epmDocument.getNumber().contains("_");
        // ---------------------------------------------------
        // Se l'oggetto � un b3*_* non va pubblicato ma i suoi drawing s�
        // ---------------------------------------------------
        if (!b3) {
          LogWrapper.logMessage(LogWrapper.INFO, "CDT: L'oggetto " + epmDocument.getNumber() + " non � un b3, quindi lo metto in coda");
          aggiungiInCoda(oggettiDaPubblicare, epmDocument, livello);
          livelloSuccessivo = livello + 1;
        }
        else {
          LogWrapper.logMessage(LogWrapper.INFO, "CDT: L'oggetto " + epmDocument.getNumber() + " � un b3, quindi NON lo metto in coda");
        }
        // ---------------------------------------------------
        // Aggiungo tutti i drawing tra le cose da pubblicare
        // ---------------------------------------------------
        LogWrapper.logMessage(LogWrapper.INFO, "CDT: metto tutti i drawing trovati in coda");
        for (EPMDocument d : drawing) {
          aggiungiInCoda(oggettiDaPubblicare, d, livello);
        }
      }
      // --------------------------------------------------------------------------------
      // altrimenti deve essere pubblicato solo se � un gruppo vettoriale
      // --------------------------------------------------------------------------------
      else {
        LogWrapper.logMessage(LogWrapper.INFO, "CDT: il documento " + epmDocument.getNumber() + " non ha nessun drawing");
        // -----------------------------------------------------------------------
        // se � un gruppo vettoriale, ossia ha l'attributo "Gruppo_elettrico" che vale true bisogna pubblicarlo anche se
        // non ha drawing
        // -----------------------------------------------------------------------
        boolean valoreAttributoGruppoElettrico;
        try {
          valoreAttributoGruppoElettrico = leggiAttributoBool(epmDocument, "GRUPPO_ELETTRICO");
        }
        catch (Exception e) {
          LogWrapper.logMessage(LogWrapper.ERROR, "CDT: Errore ricercando l'attributo GRUPPO_ELETTRICO per " + epmDocument.getNumber(), e);
          throw new ListenerCodaDiPubblicazioneException("Errore ricercando l'attributo GRUPPO_ELETTRICO per " + epmDocument.getNumber(), e);
        }
        if (valoreAttributoGruppoElettrico) {
          LogWrapper.logMessage(LogWrapper.INFO, "CDT: il documento " + epmDocument.getNumber()
              + " � un gruppo elettrico quindi lo metto in coda anche se non ha disegni");
          aggiungiInCoda(oggettiDaPubblicare, epmDocument, livello);
          livelloSuccessivo = livello + 1;
        }
        else {
          LogWrapper.logMessage(LogWrapper.INFO, "CDT: il documento " + epmDocument.getNumber()
              + " non ha disegni e NON � un gruppo elettrico quindi non lo metto in coda");
        }
      }

      // se non � promote deve cercare anche nei livelli superiori, si chiama ricorsivamente
      if (!promote) {
        for (EPMDocument asm : assiemi) {
          LogWrapper.logMessage(LogWrapper.DEBUG, "CDT: chiamata ricorsiva sull'assieme:" + asm.getNumber() + " livello:" + livelloSuccessivo);
          creaListaEPMDocumentDaPubblicare(asm, livelloSuccessivo, promote);
        }
      }
      else {
        LogWrapper.logMessage(LogWrapper.DEBUG, "CDT: Non viene effettuata la chiamata ricorsiva");
      }
    }
    else {
      LogWrapper.logMessage(LogWrapper.INFO, "CDT: Il documento � un drawing, quindi lo pubblico in caso di checkin, lo ignoro in caso di promote");
      if (!promote)
        aggiungiInCoda(oggettiDaPubblicare, epmDocument, 0);
    }
  }

  /**
   * Scrive il file di output che verr� letto da MDM con formato iei_number_giorno_ora.txt e lo salva nella cartella
   * definita nella propertiy <code>ext.elettric80.trigger.directory_interscambio</code>
   *
   * @param oggettiDaPubblicare
   *          lista degli oggetti da pubblicare che devono essere scritti nel file
   * @param nomeUtente
   *          nome dell'utente che ha fatto il checkin/promote
   * @throws ListenerCodaDiPubblicazioneException
   *           in caso di errore leggendo la properties con la directory in cui creare il file
   */
  String scriviFileDiOutput(String evento, String nomeUtente, String nomeEPM) throws ListenerCodaDiPubblicazioneException {
    LogWrapper.logMessage(LogWrapper.INFO, "CDT: Scrivo il file con la lista di oggetti da pubblicare");

    String dir = Elettric80Properties.getProperty("ext.elettric80.trigger.directory_interscambio", "C:\\Program Files\\mdmpub6\\work\\");

    Date ora = new Date();
    String dataCreazione = WTStandardDateFormat.format(ora, "dd-MM-yyyy_HH-mm-ss-SSS");
    String nomeFile = "iei_" + nomeEPM + "_" + dataCreazione + ".txt";
    LogWrapper.logMessage(LogWrapper.INFO, "CDT: directory_interscambio -> " + dir);

    if (!dir.endsWith("\\")) {
      dir = dir + "\\";
    }
    String file = dir + nomeFile;
    LogWrapper.logMessage(LogWrapper.INFO, "CDT: Percorso del file: " + file);
    File fileOutput = new File(file);
    PrintWriter pw = null;
    try {
      pw = new PrintWriter(fileOutput);
    }
    catch (FileNotFoundException e) {
      LogWrapper.logMessage(LogWrapper.ERROR,
          "CDT: errore nella creazione del PrintWriter per la scrittura del file di output del checkin, non trovato il file " + file, e);
      throw new ListenerCodaDiPubblicazioneException(
          "Errore nella creazione del PrintWriter per la scrittura del file di output del checkin, non trovato il file " + file, e);
    }

    for (EPMDocumentDaPubblicare daPubblicare : oggettiDaPubblicare) {
      EPMDocument epm = daPubblicare.getDocumento();
      String versione = epm.getVersionIdentifier().getValue();
      if (versione.equals("-")) {
        versione = "0";
      }
      String iterazione = epm.getIterationIdentifier().getValue();
      if (iterazione.equals("-")) {
        iterazione = "0";
      }
      String stato = epm.getLifeCycleState().toString();
      LogWrapper.logMessage(LogWrapper.DEBUG,
          "CDT: " + evento + ";" + epm.getNumber() + ";" + versione + ";" + iterazione + ";main;" + stato.toUpperCase() + ";" + nomeUtente + ";"
              + daPubblicare.getLivello() + ";");
      pw.print(evento + ";" + epm.getNumber() + ";" + versione + ";" + iterazione + ";main;" + stato.toUpperCase() + ";" + nomeUtente + ";"
          + daPubblicare.getLivello() + ";");
      ;
      pw.println();
    }
    pw.close();
    LogWrapper.logMessage(LogWrapper.INFO, "CDT: terminata la scrittura del file con la lista di oggetti da pubblicare");
    return file;
  }

  /**
   * Lancia il bat che si occupa di avviare MDM e creare le azioni corrispondenti agli oggetti da pubblicare che sono
   * stati scritti in un file tramite la funzione {@link #scriviFileDiOutput}
   *
   * @param fileOutput
   *
   * @throws ListenerCodaDiPubblicazioneException
   *           in caso di errore leggendo la properties ext.elettric80.trigger.cmd_import_mdm o nell'esecuzione del bat
   *           indicato dalla properties
   */
  void lanciaMDM(String fileOutput) throws ListenerCodaDiPubblicazioneException {
    LogWrapper.logMessage(LogWrapper.DEBUG, "CDT: eggo la properties ext.elettric80.trigger.cmd_import_mdm");
    String comando = Elettric80Properties.getProperty("ext.elettric80.trigger.cmd_import_mdm", "C:\\program files\\mdmpub6\\bin\\import_mdm.bat")
        + " --file-in \"" + fileOutput + "\"";

    LogWrapper.logMessage(LogWrapper.INFO, "CDT: Comando per importare in MDM): " + comando);

    try {
      Process p = Runtime.getRuntime().exec(comando);

      BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
      String line = null;
      while ((line = in.readLine()) != null) {
        LogWrapper.logMessage(LogWrapper.DEBUG, "CDT:" + line);
      }
    }
    catch (IOException e) {
      LogWrapper.logMessage(LogWrapper.ERROR, "CDT: Errore nell'esecuzione di " + comando, e);
      throw new ListenerCodaDiPubblicazioneException("Errore nell'esecuzione di " + comando, e);
    }
    LogWrapper.logMessage(LogWrapper.INFO, "CDT: Fine esecuzione " + comando);
  }

  /**
   * Recupera la lista di oggetti da pubblicare.
   *
   * @return oggettiDaPubblicare la lista di oggetti da pubblicare.
   */
  public ArrayList<EPMDocumentDaPubblicare> getOggettiDaPubblicare() {
    return oggettiDaPubblicare;
  }

  /**
   * Setta la lista di oggetti da pubblicare.
   *
   * @param oggettiDaPubblicare
   *          la lista di oggetti da settare.
   */
  public void setOggettiDaPubblicare(ArrayList<EPMDocumentDaPubblicare> oggettiDaPubblicare) {
    this.oggettiDaPubblicare = oggettiDaPubblicare;
  }

  /**
   * Il main
   *
   * @param args
   *          argomenti corrispondenti alle properties
   * @throws Exception
   *           in caso di errore
   */
  public static void main(String args[]) throws Exception {
    EPMDocument epm = SearchEPMDocument.findLatestEPMDocuments("596004050.PRT");
    CodaDiPubblicazione coda = new CodaDiPubblicazione();
    // Mastered m = epm.getMaster();
    ArrayList<EPMDocument> drawing = coda.findDrawing(epm);
    for (EPMDocument d : drawing) {
      System.out.println("drw: " + d.getNumber() + " -versione: " + d.getVersionIdentifier().getValue() + " -iterazione: "
          + d.getIterationIdentifier().getValue());
    }
  }

}
