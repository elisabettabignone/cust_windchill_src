package ext.elettric80.services;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;
import java.util.Properties;

import wt.doc.WTDocument;
import wt.epm.EPMDocument;
import wt.events.KeyedEvent;
import wt.events.KeyedEventListener;
import wt.fc.Persistable;
import wt.method.RemoteMethodServer;
import wt.part.WTPart;
import wt.services.ManagerException;
import wt.services.ServiceEventListenerAdapter;
import wt.services.StandardManager;
import wt.session.SessionContext;
import wt.session.SessionThread;
import wt.util.WTException;
import wt.vc.wip.WorkInProgressServiceEvent;
import ext.caditech.utility.CDTProperties;
import ext.caditech.utility.LogWrapper;
import ext.caditech.utility.SearchEPMDocument;
import ext.caditech.utility.SearchUtilsException;

/**
 * Classe che intercetta il checkin.
 *
 * @author s.menocci
 *
 */
public class StandardElettric80ListenerService extends StandardManager implements Elettric80ListenerService, Serializable, wt.method.RemoteAccess {

  private static final long serialVersionUID = 1L;

  static final String VERSION = "1.01.02";

  private static final String CLASSNAME = StandardElettric80ListenerService.class.getName();

  private KeyedEventListener listener;

  static final boolean SERVER;

  /** L'username con cui autenticarsi in Windchill */
  static private String user = "";

  /** La password con cui autenticarsi in Windchill */
  static private String password = "";

  static {
    SERVER = RemoteMethodServer.ServerFlag;
  }

  public String getConceptualClassname() {
    return CLASSNAME;
  }

  public static StandardElettric80ListenerService newStandardElettric80ListenerService() throws WTException {
    StandardElettric80ListenerService instance = new StandardElettric80ListenerService();
    instance.initialize();
    return instance;
  }

  /**
   * Internal Listener class
   **/
  class Elettric80EventListener extends ServiceEventListenerAdapter {

    public Elettric80EventListener(String manager_name) {
      super(manager_name);
    }

    public void notifyVetoableEvent(Object event) throws WTException, java.io.IOException, java.beans.PropertyVetoException {
      if (!(event instanceof KeyedEvent)) {
        return;
      }
      KeyedEvent keyedEvent = (KeyedEvent) event;
      try {
        if (keyedEvent.getEventType().equals(WorkInProgressServiceEvent.POST_CHECKIN)) {
          WorkInProgressServiceEvent wInProgressEvent = (WorkInProgressServiceEvent) keyedEvent;
          // Recupero l'oggetto di cui � stato fatto il checkin: con getOriginalCopy() ottengo la versione prima del
          // checkin, con getWorkingCopy() ottengo la versione post checkin
          Object workingCopy = wInProgressEvent.getWorkingCopy();
          processPostCheckinEvent((Persistable) workingCopy);
        }
      }
      catch (ListenerCodaDiPubblicazioneException e) {
        e.printStackTrace();
      }
    } // end notifyVetoableEvent
  } // end-class Elettric80EventListener

  protected void performStartupProcess() throws ManagerException {
    LogWrapper.logMessage(LogWrapper.INFO, "CDT: Elettric80 Service started");
    listener = new Elettric80EventListener(this.getConceptualClassname());
    getManagerService().addEventListener(listener, WorkInProgressServiceEvent.generateEventKey(WorkInProgressServiceEvent.POST_CHECKIN));
  } // end performStartupProcess

  // =======================================================================================================

  /**
   * Se il tipo di documento � WTPart o WTDocument non viene fatto nulla, invece nel caso sia stato fatto un checkin di
   * un EPMDocument viene letta la property ext.elettric80.trigger.checkin.tipo_avvio:
   * <ul>
   * <li>0 - procedura disabilitata, quindi non viene fatto nulla</li>
   * <li>1 - viene creata crea la lista di oggetti da pubblicare lanciando un comando in una shell windchill e scrive la
   * lista su un file che si trover� nella cartella indicata nella property
   * ext.elettric80.trigger.checkin.directory_interscambio</li>
   * <li>2 - viene creata crea la lista di oggetti da pubblicare lanciando un comando in una shell windchill e scrive la
   * lista su un file che si trover� nella cartella indicata nella property
   * ext.elettric80.trigger.checkin.directory_interscambio e lancia il bat indicato nella property
   * ext.elettric80.trigger.checkin.cmd_import_mdm che esegue la procedura di import in MDM.</li>
   *
   * @param target
   *          documento di cui � stato fatto il checkin
   * @throws ListenerCodaDiPubblicazioneException
   */
  protected void processPostCheckinEvent(Persistable target) throws ListenerCodaDiPubblicazioneException {

    LogWrapper.logMessage(LogWrapper.INFO, "CDT: -----------------------------------------------------------");
    LogWrapper.logMessage(LogWrapper.INFO, "CDT: ----------------- TRIGGER CHECKIN " + StandardElettric80ListenerService.VERSION
        + " -----------------");
    LogWrapper.logMessage(LogWrapper.INFO, "CDT: -----------------------------------------------------------");

    LogWrapper.logMessage(LogWrapper.INFO, "CDT: Post Check-In: inizio");
    if (target instanceof WTPart) {
      LogWrapper.logMessage(LogWrapper.INFO, "CDT: Effettuato check-in di una WTPart");
    }
    else if (target instanceof WTDocument) {
      LogWrapper.logMessage(LogWrapper.INFO, "CDT: Effettuato check-in di un WTDocument");
    }
    else if (target instanceof EPMDocument) {
      // --------------------------------------------------------
      // Leggo la property con il tipo di avvio della procedura
      // --------------------------------------------------------
      String tipoAvvio = Elettric80Properties.getProperty("ext.elettric80.trigger.checkin.tipo_avvio", "0");

      if (tipoAvvio.equals("0")) {
        LogWrapper.logMessage(LogWrapper.INFO, "CDT: la procedura post checkin � disabilitata");
        return;
      }
      // Recupero le informazioni che mi servono per lanciare la procedura che crea la lista degli oggetti da mettere in
      // coda di pubblicazione e che in seguito lancia MDM per creare le azioni di pubblicazione
      EPMDocument epm = (EPMDocument) target;
      String number = epm.getNumber();
      String versione = epm.getVersionIdentifier().getValue();
      String iterazione = epm.getIterationIdentifier().getValue();

      // costruisce le properties per lanciare il thread
      Properties prop = new Properties();

      prop.setProperty(CreatoreListaDiPubblicazione.AVVIO, tipoAvvio);
      prop.setProperty(CreatoreListaDiPubblicazione.NUMBER, number);
      prop.setProperty(CreatoreListaDiPubblicazione.VERSIONE, versione);
      prop.setProperty(CreatoreListaDiPubblicazione.ITERAZIONE, iterazione);
      prop.setProperty(CreatoreListaDiPubblicazione.PROMOTE, "false");

      // crea il nuovo runnable per la lista di pubblicazione e setta le properties
      CreatoreListaDiPubblicazione rclp = new CreatoreListaDiPubblicazione();
      rclp.setProp(prop);
      rclp.setDoc(epm);

      // esegue il nuovo thread
      LogWrapper.logMessage(LogWrapper.INFO, "Esecuzione nuovo thread per la coda di pubblicazione (checkin)");
      new SessionThread(rclp, new SessionContext()).start();
    }
    LogWrapper.logMessage(LogWrapper.INFO, "CDT: Post Check-In: fine");
  }

  // -------------------------------------------------------------------------------------------------------------------
  // ---------------- tutto quello che si trova qui sotto serve per testare "processPostCheckinEvent" lanciando il main
  // -------------------------------------------------------------------------------------------------------------------

  public static void eseguiTriggerCheckinServer(String epm) throws Exception {
    if (SERVER) {
      LogWrapper.logMessage(LogWrapper.DEBUG, "CDT-eseguiTriggerCheckinServer: sono server");
      eseguiTriggerCheckin(epm);
    }
    else {
      try {
        LogWrapper.logMessage(LogWrapper.DEBUG, "CDT-eseguiTriggerCheckinServer: non sono server");

        // imposta user e pass
        user = CDTProperties.getProperty("ext.caditech.login.user", "wcadmin");
        password = CDTProperties.getProperty("ext.caditech.login.password", "wcadmin");

        // imposta il method server remoto
        RemoteMethodServer method = RemoteMethodServer.getDefault();
        method.setUserName(user);
        method.setPassword(password);

        Class[] argumentTypes = { String.class };
        Object[] arguments = { epm };
        method.invoke("eseguiTriggerCheckin", "ext.elettric80.services.StandardElettric80ListenerService", null, argumentTypes, arguments);
      }
      catch (InvocationTargetException itex) {
        Throwable throwable = itex.getTargetException();
        if (throwable instanceof WTException) {
          throw (WTException) throwable;
        }
        Object aobj[] = { "eseguiTriggerCheckin" };
        throw new WTException(throwable, "wt.fc.fcResource", "0", aobj);
      }
      catch (RemoteException rex) {
        Object aobj[] = { "eseguiTriggerCheckin" };
        throw new WTException(rex, "wt.fc.fcResource", "0", aobj);
      }
    }
  }

  public static void eseguiTriggerCheckin(String epm) throws RemoteException, WTException, IOException, PropertyVetoException,
      ListenerCodaDiPubblicazioneException, SearchUtilsException {
    StandardElettric80ListenerService s = StandardElettric80ListenerService.newStandardElettric80ListenerService();
    EPMDocument epmdoc = SearchEPMDocument.findLatestEPMDocuments(epm);
    s.processPostCheckinEvent(epmdoc);
  }

  /**
   * Main per simulare il checkin da una windchill shell
   *
   * @param args
   * @throws Exception
   */
  public static void main(String[] args) throws Exception {

    // imposta user e pass
    user = CDTProperties.getProperty("ext.caditech.login.user", "wcadmin");
    password = CDTProperties.getProperty("ext.caditech.login.password", "wcadmin");

    // imposta il method server remoto
    RemoteMethodServer method = RemoteMethodServer.getDefault();
    method.setUserName(user);
    method.setPassword(password);

    LogWrapper.logMessage(LogWrapper.DEBUG, "CDT-Trigger checkin ->" + args[0]);
    eseguiTriggerCheckinServer(args[0]);
    System.exit(0);
  } // end main

} // end-class StandardElettric80ListenerService
