package ext.elettric80.services;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;
import java.util.Properties;

import wt.doc.WTDocument;
import wt.epm.EPMDocument;
import wt.fc.Persistable;
import wt.method.RemoteMethodServer;
import wt.part.WTPart;
import wt.services.StandardManager;
import wt.session.SessionContext;
import wt.session.SessionThread;
import wt.util.WTException;
import ext.caditech.utility.CDTProperties;
import ext.caditech.utility.LogWrapper;
import ext.caditech.utility.SearchEPMDocument;
import ext.caditech.utility.SearchUtilsException;

/**
 * Gestisce l'evento di cambio stato sul Promote
 *
 * @author administrator
 *
 */
public class PromoteListenerService extends StandardManager implements Elettric80ListenerService, Serializable, wt.method.RemoteAccess {

  static final String VERSION = "1.01.01";

  /** L'id univoco di classe. */
  private static final long serialVersionUID = 7382955698005362247L;

  /** booleano che mi dice se sono o meno il server */
  static final boolean SERVER;

  /** L'username con cui autenticarsi in Windchill */
  static private String user = "";

  /** La password con cui autenticarsi in Windchill */
  static private String password = "";

  static {
    SERVER = RemoteMethodServer.ServerFlag;
  }

  public static PromoteListenerService newPromoteListenerService() throws WTException {
    PromoteListenerService instance = new PromoteListenerService();
    instance.initialize();
    return instance;
  }

  /**
   * Azione sull'intercettazione dell'evento di Promote.
   *
   * @param target
   *          l'oggetto di cui � stato fatto il promote.
   * @throws ListenerCodaDiPubblicazioneException
   *           l'eccezione durante la creazione della coda di oggetti da pubblicare.
   */
  public static void intercettaPromote(Persistable target) throws ListenerCodaDiPubblicazioneException {

    LogWrapper.logMessage(LogWrapper.INFO, "CDT:-----------------------------------------------------------");
    LogWrapper.logMessage(LogWrapper.INFO, "CDT:----------------- TRIGGER PROMOTE " + PromoteListenerService.VERSION + " -----------------");
    LogWrapper.logMessage(LogWrapper.INFO, "CDT:-----------------------------------------------------------");
    LogWrapper.logMessage(LogWrapper.INFO, "CDT:Post Promote: inizio");
    if (target instanceof WTPart) {
      LogWrapper.logMessage(LogWrapper.INFO, "CDT: effettuato Promote di una WTPart");
    }
    else if (target instanceof WTDocument) {
      LogWrapper.logMessage(LogWrapper.INFO, "CDT: effettuato Promote di un WTDocument");
    }
    else if (target instanceof EPMDocument) {

      String avvioProp = "ext.elettric80.trigger.promote.tipo_avvio";
      String tipoAvvio = Elettric80Properties.getProperty(avvioProp, "0");

      if (tipoAvvio.equals("0")) {
        LogWrapper.logMessage(LogWrapper.INFO, "CDT: la procedura post promote � disabilitata");
        return;
      }

      // Recupero le informazioni che mi servono per lanciare la procedura che crea la lista degli oggetti da mettere in
      // coda di pubblicazione e che in seguito lancia MDM per creare le azioni di pubblicazione
      EPMDocument epm = (EPMDocument) target;
      String number = epm.getNumber();
      String versione = epm.getVersionIdentifier().getValue();
      String iterazione = epm.getIterationIdentifier().getValue();

      // costruisce le properties per lanciare il thread
      Properties prop = new Properties();

      prop.setProperty(CreatoreListaDiPubblicazione.AVVIO, tipoAvvio);
      prop.setProperty(CreatoreListaDiPubblicazione.NUMBER, number);
      prop.setProperty(CreatoreListaDiPubblicazione.VERSIONE, versione);
      prop.setProperty(CreatoreListaDiPubblicazione.ITERAZIONE, iterazione);
      prop.setProperty(CreatoreListaDiPubblicazione.PROMOTE, "true");

      // crea il nuovo runnable per la lista di pubblicazione e setta le properties
      CreatoreListaDiPubblicazione rclp = new CreatoreListaDiPubblicazione();
      rclp.setProp(prop);
      rclp.setDoc(epm);

      // esegue il nuovo thread
      LogWrapper.logMessage(LogWrapper.INFO, "Esecuzione nuovo thread per la coda di pubblicazione (promote)");
      new SessionThread(rclp, new SessionContext()).start();
    }

    LogWrapper.logMessage(LogWrapper.INFO, "CDT: Post Promote: fine");
  }

  // -------------------------------------------------------------------------------------------------------------------
  // ---------------- tutto quello che si trova qui sotto serve per testare "intercettaPromote" lanciando il main
  // -------------------------------------------------------------------------------------------------------------------

  public static void eseguiTriggerPromoteServer(String epm) throws Exception {
    if (SERVER) {
      LogWrapper.logMessage(LogWrapper.DEBUG, "CDT-eseguiTriggerPromoteServer: sono server");
      eseguiTriggerPromote(epm);
    }
    else {
      try {
        LogWrapper.logMessage(LogWrapper.DEBUG, "CDT-eseguiTriggerPromoteServer: non sono server");

        // imposta user e pass
        user = CDTProperties.getProperty("ext.caditech.login.user", "wcadmin");
        password = CDTProperties.getProperty("ext.caditech.login.password", "wcadmin");

        // imposta il method server remoto
        RemoteMethodServer method = RemoteMethodServer.getDefault();
        method.setUserName(user);
        method.setPassword(password);

        Class[] argumentTypes = { String.class };
        Object[] arguments = { epm };
        method.invoke("eseguiTriggerPromote", "ext.elettric80.services.PromoteListenerService", null, argumentTypes, arguments);
      }
      catch (InvocationTargetException itex) {
        Throwable throwable = itex.getTargetException();
        if (throwable instanceof WTException) {
          throw (WTException) throwable;
        }
        Object aobj[] = { "eseguiTriggerPromote" };
        throw new WTException(throwable, "wt.fc.fcResource", "0", aobj);
      }
      catch (RemoteException rex) {
        Object aobj[] = { "eseguiTriggerPromote" };
        throw new WTException(rex, "wt.fc.fcResource", "0", aobj);
      }
    }
  }

  public static void eseguiTriggerPromote(String epm) throws RemoteException, WTException, IOException, PropertyVetoException,
      ListenerCodaDiPubblicazioneException, SearchUtilsException {
    PromoteListenerService.newPromoteListenerService();
    EPMDocument epmdoc = SearchEPMDocument.findLatestEPMDocuments(epm);
    PromoteListenerService.intercettaPromote(epmdoc);
  }

  /**
   * main
   *
   * @param args
   * @throws Exception
   */
  public static void main(String[] args) throws Exception {

    // imposta user e pass
    user = CDTProperties.getProperty("ext.caditech.login.user", "wcadmin");
    password = CDTProperties.getProperty("ext.caditech.login.password", "wcadmin");

    // imposta il method server remoto
    RemoteMethodServer method = RemoteMethodServer.getDefault();
    method.setUserName(user);
    method.setPassword(password);

    LogWrapper.logMessage(LogWrapper.DEBUG, "Trigger Promote ->" + args[0]);
    eseguiTriggerPromoteServer(args[0]);

    System.exit(0);
  } // end main
}
