// Generated AlsPartAuxService%3FDEC5AB0045: Wed 10/15/08 13:17:56
/* bcwti
 *
 * Copyright (c) 2004 Parametric Technology Corporation (PTC). All Rights
 * Reserved.
 *
 * This software is the confidential and proprietary information of PTC.
 * You shall not disclose such confidential information and shall use it
 * only in accordance with the terms of the license agreement.
 *
 * ecwti
 */

package ext.elettric80.services;

//##begin user.imports preserve=yes
//##end user.imports

//##begin AlsPartAuxService%3FDEC5AB0045.doc preserve=no
/**
 *
 * @version 1.0
 **/
// ##end AlsPartAuxService%3FDEC5AB0045.doc

public interface Elettric80ListenerService {

  // ##begin user.attributes preserve=yes
  // ##end user.attributes

  // ##begin static.initialization preserve=yes
  // ##end static.initialization

  // ##begin user.operations preserve=yes
  // ##end user.operations

} // end-class Elettric80ListenerService
