package ext.elettric80.services;

import wt.epm.EPMDocument;

public class EPMDocumentDaPubblicare {

  EPMDocument documento;

  int livello = -1;

  EPMDocumentDaPubblicare(EPMDocument epm, int l) {
    this.documento = epm;
    this.livello = l;
  }

  public void setDocumento(EPMDocument documento) {
    this.documento = documento;
  }

  public void setLivello(int livello) {
    this.livello = livello;
  }

  public EPMDocument getDocumento() {
    return documento;
  }

  public int getLivello() {
    return livello;
  }

}
