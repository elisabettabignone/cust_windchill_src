package ext.elettric80.services;

import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import wt.admin.AdministrativeDomainHelper;
import wt.epm.EPMDocument;
import wt.lifecycle.LifeCycleHelper;
import wt.lifecycle.LifeCycleHistory;
import wt.method.RemoteMethodServer;
import wt.services.StandardManager;
import wt.session.SessionMgr;
import wt.util.WTException;
import ext.caditech.utility.CDTProperties;
import ext.caditech.utility.LogWrapper;
import ext.caditech.utility.Mail;
import ext.caditech.utility.SearchEPMDocument;

/**
 * Classe il cui main viene lanciato tramite una shell windchill in fase di intercettazione di un chekin o in fase di
 * promote. Viene lanciato il metodo {@link CreatoreListaDiPubblicazione#esegui(Properties)}.
 *
 * @author s.menocci
 *
 */
public class CreatoreListaDiPubblicazione extends StandardManager implements wt.method.RemoteAccess, Runnable {

  private static final long serialVersionUID = 1L;

  /** Number dell'EPMDocument di cui � stato fatto il checkin */
  public final static String NUMBER = "number";

  /** Versione dell'EPMDocument di cui � stato fatto il checkin */
  public final static String VERSIONE = "versione";

  /** Iterazione dell'EPMDocument di cui � stato fatto il checkin */
  public final static String ITERAZIONE = "iterazione";

  /** Vale true se � stato fatto il promote, false in caso di checkin */
  public final static String PROMOTE = "promote";

  /** L'username con cui autenticarsi in Windchill */
  static private String user = "";

  /** La password con cui autenticarsi in Windchill */
  static private String password = "";

  /**
   * Indica quali operazioni eseguire, pu� vale 0 o 1 o
   * <ul>
   * <li>2: crea la lista di pubblicazione e avvia MDM</li>
   * <li>1: crea solo la lista di pubblicazione senza avviare MDM</li>
   * <li>0: procedura disabilitata, non fa nulla</li>
   */
  public final static String AVVIO = "avvio";

  /** Properties per lanciare in multithread il processo di generazione della lista di pubblicazione */
  private Properties prop;

  /** Il documento del quale � necessario creare la lista di pubblicazione */
  private EPMDocument doc;

  /**
   * Getter del documento da mettere in lista di pubblicazione
   *
   * @return il documento
   */
  public EPMDocument getDoc() {
    return doc;
  }

  /**
   * Setter del documento da mettere in lista di pubblicazione
   *
   * @param doc
   *          il documento da settare
   */
  public void setDoc(EPMDocument doc) {
    this.doc = doc;
  }

  /**
   * Getter delle properties
   *
   * @return le properties per la generazione della lista di pubblicazione
   */
  public Properties getProp() {
    return prop;
  }

  /**
   * Setter delle properties
   *
   * @param prop
   *          le properties da settare per la generazione della lista di pubblicazione
   */
  public void setProp(Properties prop) {
    this.prop = prop;
  }

  static final boolean SERVER;

  static {
    SERVER = RemoteMethodServer.ServerFlag;
  }

  /**
   * Main lanciato dall'intercettore del checkin e del promote
   *
   * @param args
   *
   *          <li>{@link CreatoreListaDiPubblicazione#NUMBER}</li> <li>
   *          {@link CreatoreListaDiPubblicazione#VERSIONE}</li> <li>
   *          {@link CreatoreListaDiPubblicazione#ITERAZIONE}</li> <li>
   *          {@link CreatoreListaDiPubblicazione#PROMOTE}</li> <li>
   *          {@link CreatoreListaDiPubblicazione#AVVIO}</li>
   * @throws Exception
   */
  public static void main(String[] args) throws Exception {
    System.out.println("CDT: inizio main CreatoreListaDiPubblicazione");
    Properties p = new Properties();

    // imposta user e pass
    user = CDTProperties.getProperty("ext.caditech.login.user", "wcadmin");
    password = CDTProperties.getProperty("ext.caditech.login.password", "wcadmin");

    // imposta il method server remoto
    RemoteMethodServer method = RemoteMethodServer.getDefault();
    method.setUserName(user);
    method.setPassword(password);

    List<String> chiavi = new ArrayList<String>(Arrays.asList(NUMBER, VERSIONE, ITERAZIONE, PROMOTE, AVVIO));

    for (int i = 0; i < args.length; i++) {
      String[] coppia = args[i].split("=");
      if (coppia.length != 2) {
        System.out.println("CDT: � stata inserito un argomento non ben formattato: " + args[i]);
        System.out.println("CDT: gli argomenti devono avere la forma : chiave=valore");
        System.exit(0);
      }
      String chiave = coppia[0];
      String valore = coppia[1];
      if (!chiavi.contains(chiave)) {
        System.out.println("CDT: � stata inserito un argomento non riconosciuto: " + chiave);
        System.exit(0);
      }
      System.out.println("CDT: inserita property: " + chiave + "-->" + valore);
      p.setProperty(chiave, valore);
    }

    EPMDocument doc = SearchEPMDocument.findEPMDocuments(p.getProperty(NUMBER), p.getProperty(VERSIONE), p.getProperty(ITERAZIONE));

    // ---------------------------------------------------------------
    // Lancio la procedura
    // ---------------------------------------------------------------
    eseguiProceduraComeServer(p, doc);
    System.out.println("CDT: fine main CreatoreListaDiPubblicazione");
  }

  /**
   * <ul>
   * <li>Crea la coda dei file da inserire in coda di pubblicazione
   * {@link CodaDiPubblicazione#creaListaEPMDocumentDaPubblicare}</li>
   * <li>Scrive il file di output con la lista dei file da inserire in coda di pubblicazione
   * {@link CodaDiPubblicazione#scriviFileDiOutput}</li>
   * <li>Lancia la procedura import_mdm.bat che inserisce in MDM gli oggetti da pubblicare
   * {@link CodaDiPubblicazione#lanciaMDM}</li>
   * </ul>
   *
   * @param p
   *          propriet�: <li>{@link CreatoreListaDiPubblicazione#NUMBER}</li> <li>
   *          {@link CreatoreListaDiPubblicazione#VERSIONE}</li> <li>
   *          {@link CreatoreListaDiPubblicazione#ITERAZIONE}</li> <li>
   *          {@link CreatoreListaDiPubblicazione#PROMOTE}</li> <li>
   *          {@link CreatoreListaDiPubblicazione#AVVIO}</li>
   * @throws ListenerCodaDiPubblicazioneException
   */
  public static void esegui(Properties p, EPMDocument epmdoc) throws ListenerCodaDiPubblicazioneException {
    Date adesso = new Date();
    LogWrapper.logMessage(LogWrapper.INFO, "CDT: Inizio della procedura di creazione della coda di pubblicazione ----- " + adesso.getTime());
    String number = p.getProperty(NUMBER);
    String versione = p.getProperty(VERSIONE);
    String iterazione = p.getProperty(ITERAZIONE);
    String promote = p.getProperty(PROMOTE);
    String avvio = p.getProperty(AVVIO);
    LogWrapper.logMessage(LogWrapper.INFO, "CDT: Lette le properties " + adesso.getTime());
    // ---------------------------------------------------------------
    // Creo l'oggetto CodaDiPubblicazione che si occuper� di eseguire tutta la procedura
    // ---------------------------------------------------------------
    CodaDiPubblicazione cdp = new CodaDiPubblicazione();

    if (epmdoc == null) {
      String error = "L'EPMDocument con number:" + number + " -versione:" + versione + " -iterazione:" + iterazione + " e' NULL";
      LogWrapper.logMessage(LogWrapper.ERROR, error);
      throw new ListenerCodaDiPubblicazioneException(error);
    }
    LogWrapper.logMessage(LogWrapper.INFO, "CDT: Creo la lista di pubblicazione " + adesso.getTime());
    // --------------------------------------------------------------
    // Chiamo la funzione che crea la lista di oggetti da pubblicare
    // --------------------------------------------------------------
    cdp.creaListaEPMDocumentDaPubblicare(epmdoc, 0, promote.equalsIgnoreCase("true"));
    // --------------------------------------------------------------
    // Scrive la lista di oggetti da pubblicare su file
    // --------------------------------------------------------------
    String utente = "";
    if (!promote.equalsIgnoreCase("true")) {
      utente = epmdoc.getModifierName();
    }
    else {
      utente = "";
      LifeCycleHistory aHistory = null;
      Enumeration e;
      try {
        e = LifeCycleHelper.service.getHistory(epmdoc);
      }
      catch (Exception e1) {
        String msg = "Errore recuperando la history del lifecycle del documento" + epmdoc.getNumber();
        LogWrapper.logMessage(LogWrapper.ERROR, msg);
        throw new ListenerCodaDiPubblicazioneException(msg, e1);
      }
      if (e != null && e.hasMoreElements()) {
        aHistory = (LifeCycleHistory) e.nextElement();
        utente = aHistory.getActorName();
        LogWrapper.logMessage(LogWrapper.INFO, "actor name:" + aHistory.getActorName());
      }
      else {
        String msg = "History del lifecycle del documento" + epmdoc.getNumber() + " vuota";
        LogWrapper.logMessage(LogWrapper.ERROR, msg);
        throw new ListenerCodaDiPubblicazioneException(msg);
      }
    }

    LogWrapper.logMessage(LogWrapper.INFO, "Utente che ha fatto il checkin/promote: " + utente);
    String evento = promote.equalsIgnoreCase("true") ? "Promote" : "Check-in";
    LogWrapper.logMessage(LogWrapper.INFO, "CDT: Scrivo il file da passare a MDM");
    String fileOutput = cdp.scriviFileDiOutput(evento, utente, epmdoc.getNumber());
    // --------------------------------------------------------------
    // Lancia MDM che andr� a leggere il file appena creato e creare le azioni di scelta di pubblicazione
    // --------------------------------------------------------------
    if (avvio.equals("2")) {
      LogWrapper.logMessage(LogWrapper.INFO, "CDT: Lancio MDM");
      cdp.lanciaMDM(fileOutput);
    }
    else {
      LogWrapper.logMessage(LogWrapper.INFO, "CDT: Non lancio MDM, perch� avvio=1");
    }
    LogWrapper.logMessage(LogWrapper.INFO, "CDT: ----- Fine della procedura di creazione della coda di pubblicazione -----");
  }

  /**
   * Lancia la procedura come se fosse il server.
   *
   * @param p
   *          propriet�
   * @throws Exception
   */
  public static void eseguiProceduraComeServer(Properties p, EPMDocument doc) throws Exception {
    System.out.println("CDT: eseguiProceduraComeServer");
    if (SERVER) {
      System.out.println("CDT: sono server");
      esegui(p, doc);
    }
    else {
      try {
        System.out.println("CDT: non sono server");

        // imposta user e pass
        user = CDTProperties.getProperty("ext.caditech.login.user", "wcadmin");
        password = CDTProperties.getProperty("ext.caditech.login.password", "wcadmin");

        // imposta il method server remoto
        RemoteMethodServer method = RemoteMethodServer.getDefault();
        method.setUserName(user);
        method.setPassword(password);

        Class[] argumentTypes = { Properties.class, EPMDocument.class };
        Object[] arguments = { p, doc };
        method.invoke("esegui", "ext.elettric80.services.CreatoreListaDiPubblicazione", null, argumentTypes, arguments);
      }
      catch (InvocationTargetException itex) {
        Throwable throwable = itex.getTargetException();
        if (throwable instanceof WTException) {
          throw (WTException) throwable;
        }
        Object aobj[] = { "esegui" };
        throw new WTException(throwable, "wt.fc.fcResource", "0", aobj);
      }
      catch (RemoteException rex) {
        Object aobj[] = { "esegui" };
        throw new WTException(rex, "wt.fc.fcResource", "0", aobj);
      }
    }
  }

  /**
   * Metodo per lanciare il thread separato della lista di pubblicazione
   */
  @Override
  public void run() {
    try {
      LogWrapper.logMessage(LogWrapper.INFO, "Codice interno Run del thread");
      // esegue il thread per la lista di pubblicazione
      SessionMgr.setPrincipal(AdministrativeDomainHelper.ADMINISTRATOR_NAME);
      esegui(getProp(), getDoc());
    }
    catch (Exception e) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore nell'esecuzione del thread per la generazione della lista di pubblicazione", e);

      try {
        String[] mails = Elettric80Properties.getProperty("ext.elettric80.trigger.mail.list", "mdm@caditech.it").split(";");
        List<String> to = new ArrayList<String>();
        to.addAll(Arrays.asList(mails));
        String userMail = getDoc().getModifierEMail();
        if (userMail != null) {
          LogWrapper.logMessage(LogWrapper.INFO, "userMail: " + userMail);
          to.add(userMail);
        }
        String host = Elettric80Properties.getProperty("ext.elettric80.trigger.mail.host");
        String user = Elettric80Properties.getProperty("ext.elettric80.trigger.mail.user");
        String psw = Elettric80Properties.getProperty("ext.elettric80.trigger.mail.psw");
        String port = Elettric80Properties.getProperty("ext.elettric80.trigger.mail.port");
        boolean promote = (getProp().getProperty("promote")).equalsIgnoreCase("true");
        String msg = "Errore durante l'intercettazione del";
        if (promote) {
          msg += " cambio stato ";
        }
        else {
          msg += " checkin ";
        }
        msg += "del documento con number " + getDoc().getNumber();

        // informazioni aggiuntive sull'errore che ha impedito l'esecuzione della procedura sulla lista di pubblicazione
        msg += "\n" + System.lineSeparator();
        msg += "Seguono informazioni sullo stack delle chiamate che hanno generato l'errore:" + System.lineSeparator();
        msg += e.getMessage() + System.lineSeparator();

        // stack delle chiamate
        StackTraceElement[] ste = e.getStackTrace();
        for (int ii = 0; ii < ste.length; ii++) {
          msg += ste[ii].toString() + System.lineSeparator();
        }

        Mail sender = new Mail();
        sender.inviaMail(to, "Errore PDMLink-MDM", msg, "mdm@caditech.it", host, user, psw, port);

      }
      catch (Exception e1) {
        LogWrapper.logMessage(LogWrapper.ERROR, "Errore nell'invio dell'email in caso di problemi nella creazione della lista di pubblicazione", e1);
      }
    }
  }

}
