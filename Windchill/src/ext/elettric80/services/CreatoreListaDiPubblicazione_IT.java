package ext.elettric80.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class CreatoreListaDiPubblicazione_IT {

  private String directoryScambio = "C:\\Program Files\\mdmpub6\\work\\";

  @Before
  public void setUp() {

    File path = new File(directoryScambio);
    if (path.exists()) {
      File[] files = path.listFiles();
      for (int j = 0; j < files.length; j++) {
        files[j].delete();
      }
    }
  }

  // ------------------------------------------------------------------------------------------------
  // TEST CHECKIN
  // ------------------------------------------------------------------------------------------------

  /***
   * Test assieme_es5.asm, � da pubblicare perch� ha un drw
   *
   * Ci aspettiamo nella lista da pubblicare:
   *
   * -assieme_es5.asm -assieme_es5.adrw
   */
  @Test
  public void testCheckin_Assieme_es5() {
    System.out.println("\n------------------------------------------------------------");
    String args[] = { "number=assieme_es5.asm", "versione=A", "iterazione=1", "promote=false", "avvio=1" };
    try {
      CreatoreListaDiPubblicazione.main(args);
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }

    File esito = recuperaFileCoda();
    ArrayList<String> modelli = new ArrayList<String>();
    try {
      BufferedReader bfr = new BufferedReader(new FileReader(esito));
      String epmdocument = null;
      while ((epmdocument = bfr.readLine()) != null) {
        String[] split = epmdocument.split(";");
        modelli.add(split[1]);
      }
      bfr.close();
    }
    catch (FileNotFoundException e) {
      fail(e.getMessage());
    }
    catch (IOException e) {
      fail(e.getMessage());
    }
    assertEquals(2, modelli.size());
    assertTrue(modelli.get(0).equalsIgnoreCase("ASSIEME_ES5.ASM"));
    assertTrue(modelli.get(1).equalsIgnoreCase("ASSIEME_ES5.DRW"));
  }

  /**
   * Parall � da pubblicare perch� ha un drw, lista con lui pi� chiamata ricorsiva al padre
   *
   * Ci aspettiamo nella lista da pubblicare:
   *
   * -parall.prt -parall.drw -assieme_es5.asm -assieme_es5.adrw
   */
  @Test
  public void testCheckin_Parall() {
    System.out.println("\n------------------------------------------------------------");
    String args[] = { "number=parall.prt", "versione=A", "iterazione=1", "promote=false", "avvio=1" };
    try {
      CreatoreListaDiPubblicazione.main(args);
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }

    File esito = recuperaFileCoda();
    ArrayList<String> modelli = new ArrayList<String>();
    try {
      BufferedReader bfr = new BufferedReader(new FileReader(esito));
      String epmdocument = null;
      while ((epmdocument = bfr.readLine()) != null) {
        String[] split = epmdocument.split(";");
        modelli.add(split[1]);
      }
      bfr.close();
    }
    catch (FileNotFoundException e) {
      fail(e.getMessage());
    }
    catch (IOException e) {
      fail(e.getMessage());
    }

    assertEquals(4, modelli.size());
    assertTrue(modelli.get(0).equalsIgnoreCase("PARALL.PRT"));
    assertTrue(modelli.get(1).equalsIgnoreCase("PARALL.DRW"));
    assertTrue(modelli.get(2).equalsIgnoreCase("ASSIEME_ES5.ASM"));
    assertTrue(modelli.get(3).equalsIgnoreCase("ASSIEME_ES5.DRW"));
  }

  /**
   * Test della chiamata su un DRW
   */
  @Test
  public void testCheckin_Drw() {
    System.out.println("\n------------------------------------------------------------");
    String args[] = { "number=parall.drw", "versione=A", "iterazione=1", "promote=false", "avvio=1" };
    try {
      CreatoreListaDiPubblicazione.main(args);
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }

    File esito = recuperaFileCoda();
    ArrayList<String> modelli = new ArrayList<String>();
    try {
      BufferedReader bfr = new BufferedReader(new FileReader(esito));
      String epmdocument = null;
      while ((epmdocument = bfr.readLine()) != null) {
        String[] split = epmdocument.split(";");
        modelli.add(split[1]);
      }
      bfr.close();
    }
    catch (FileNotFoundException e) {
      fail(e.getMessage());
    }
    catch (IOException e) {
      fail(e.getMessage());
    }
    assertEquals(1, modelli.size());
    assertTrue(modelli.get(0).equalsIgnoreCase("PARALL.DRW"));
  }

  /**
   * Test del caso B3*_*, lui non deve essere pubblicato ma i suoi drw s�
   */
  @Test
  public void testCheckin_B3() {
    System.out.println("\n------------------------------------------------------------");
    String args[] = { "number=B31234_1234.prt", "versione=A", "iterazione=1", "promote=false", "avvio=1" };
    try {
      CreatoreListaDiPubblicazione.main(args);
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }

    File esito = recuperaFileCoda();
    ArrayList<String> modelli = new ArrayList<String>();
    try {
      BufferedReader bfr = new BufferedReader(new FileReader(esito));
      String epmdocument = null;
      while ((epmdocument = bfr.readLine()) != null) {
        String[] split = epmdocument.split(";");
        modelli.add(split[1]);
      }
      bfr.close();
    }
    catch (FileNotFoundException e) {
      fail(e.getMessage());
    }
    catch (IOException e) {
      fail(e.getMessage());
    }
    assertEquals(1, modelli.size());
    assertTrue(modelli.get(0).equalsIgnoreCase("B31234_1234.DRW"));
  }

  // ------------------------------------------------------------------------------------------------
  // TEST PROMOTE
  // ------------------------------------------------------------------------------------------------

  /***
   * Test assieme_es5.asm, � da pubblicare perch� ha un drw
   *
   * Ci aspettiamo nella lista da pubblicare:
   *
   * -assieme_es5.asm -assieme_es5.adrw
   */
  @Test
  public void testPromote_Assieme_es5() {
    System.out.println("\n------------------------------------------------------------");
    String args[] = { "number=assieme_es5.asm", "versione=A", "iterazione=1", "promote=true", "avvio=1" };
    try {
      CreatoreListaDiPubblicazione.main(args);
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }

    File esito = recuperaFileCoda();
    ArrayList<String> modelli = new ArrayList<String>();
    try {
      BufferedReader bfr = new BufferedReader(new FileReader(esito));
      String epmdocument = null;
      while ((epmdocument = bfr.readLine()) != null) {
        String[] split = epmdocument.split(";");
        modelli.add(split[1]);
      }
      bfr.close();
    }
    catch (FileNotFoundException e) {
      fail(e.getMessage());
    }
    catch (IOException e) {
      fail(e.getMessage());
    }
    assertEquals(2, modelli.size());
    assertTrue(modelli.get(0).equalsIgnoreCase("ASSIEME_ES5.ASM"));
    assertTrue(modelli.get(1).equalsIgnoreCase("ASSIEME_ES5.DRW"));
  }

  /**
   * Parall � da pubblicare perch� ha un drw, lista con lui pi� chiamata ricorsiva al padre
   *
   * Ci aspettiamo nella lista da pubblicare:
   *
   * -parall.prt -parall.drw
   */
  @Test
  public void testPromote_Parall() {
    System.out.println("\n------------------------------------------------------------");
    String args[] = { "number=parall.prt", "versione=A", "iterazione=1", "promote=true", "avvio=1" };
    try {
      CreatoreListaDiPubblicazione.main(args);
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }

    File esito = recuperaFileCoda();
    ArrayList<String> modelli = new ArrayList<String>();
    try {
      BufferedReader bfr = new BufferedReader(new FileReader(esito));
      String epmdocument = null;
      while ((epmdocument = bfr.readLine()) != null) {
        String[] split = epmdocument.split(";");
        modelli.add(split[1]);
      }
      bfr.close();
    }
    catch (FileNotFoundException e) {
      fail(e.getMessage());
    }
    catch (IOException e) {
      fail(e.getMessage());
    }

    assertEquals(2, modelli.size());
    assertTrue(modelli.get(0).equalsIgnoreCase("PARALL.PRT"));
    assertTrue(modelli.get(1).equalsIgnoreCase("PARALL.DRW"));
  }

  /**
   * Test della chiamata su un DRW
   */
  @Test
  public void testPromote_Drw() {
    System.out.println("\n------------------------------------------------------------");
    String args[] = { "number=parall.drw", "versione=A", "iterazione=1", "promote=true", "avvio=1" };
    try {
      CreatoreListaDiPubblicazione.main(args);
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }

    File esito = recuperaFileCoda();
    ArrayList<String> modelli = new ArrayList<String>();
    try {
      BufferedReader bfr = new BufferedReader(new FileReader(esito));
      String epmdocument = null;
      while ((epmdocument = bfr.readLine()) != null) {
        String[] split = epmdocument.split(";");
        modelli.add(split[1]);
      }
      bfr.close();
    }
    catch (FileNotFoundException e) {
      fail(e.getMessage());
    }
    catch (IOException e) {
      fail(e.getMessage());
    }
    assertEquals(0, modelli.size());
  }

  /**
   * Test del caso B3*_*, lui non deve essere pubblicato ma i suoi drw s�
   */
  @Test
  public void testPromote_B3() {
    System.out.println("\n------------------------------------------------------------");
    String args[] = { "number=B31234_1234.prt", "versione=A", "iterazione=1", "promote=true", "avvio=1" };
    try {
      CreatoreListaDiPubblicazione.main(args);
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }

    File esito = recuperaFileCoda();
    ArrayList<String> modelli = new ArrayList<String>();
    try {
      BufferedReader bfr = new BufferedReader(new FileReader(esito));
      String epmdocument = null;
      while ((epmdocument = bfr.readLine()) != null) {
        String[] split = epmdocument.split(";");
        modelli.add(split[1]);
      }
      bfr.close();
    }
    catch (FileNotFoundException e) {
      fail(e.getMessage());
    }
    catch (IOException e) {
      fail(e.getMessage());
    }
    assertEquals(1, modelli.size());
    assertTrue(modelli.get(0).equalsIgnoreCase("B31234_1234.DRW"));
  }

  // ------------------------------------------------------------------------------------------------
  // FUNZIONI AUSILIARIE
  // ------------------------------------------------------------------------------------------------

  private File recuperaFileCoda() {
    File path = new File(directoryScambio);
    File[] files = path.listFiles();
    assertTrue(files.length == 1);
    File esito = files[0];
    assertNotNull(esito);
    assertTrue(esito.exists());
    return esito;
  }
}
