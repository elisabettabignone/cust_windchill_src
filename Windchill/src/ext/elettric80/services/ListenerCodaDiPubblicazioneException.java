package ext.elettric80.services;

public class ListenerCodaDiPubblicazioneException extends Exception {

  private static final long serialVersionUID = 1L;

  public ListenerCodaDiPubblicazioneException(String msg) {
    super(msg);
  }

  public ListenerCodaDiPubblicazioneException(String msg, Throwable causa) {
    super(msg, causa);
  }

  public ListenerCodaDiPubblicazioneException(Throwable causa) {
    super(causa);
  }
}
