package ext.sgv.export.cad;
 
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import ext.caditech.utility.LogWrapper;

/**
 * Classe per la gestione delle operazioni di aggiornamento sul database
 * 
 * @author Administrator
 *
 */
public class AggiornaTabellaDestinazione {

  /** la mappa delle configurazioni per l'accesso al database */
  HashMap<String, String> mappa_configurazioni = null;

  String idLog = "-1";

  /**
   * Costruttore con parametro obbligatorio
   * 
   * @throws SQLException
   */
  public AggiornaTabellaDestinazione(HashMap<String, String> config) {
    mappa_configurazioni = config;

  }

  public void checkConnection() throws Exception {
    try {
      OperazioniSQL operaio = new OperazioniSQL(mappa_configurazioni);
      operaio.checkConnection();
      operaio.chiudiConnessione();
    }
    catch (Exception e) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Impossibile stabilire la connessione SQL: " + e.getMessage(), e);
      e.printStackTrace();
      throw e;
    }
  }

  /**
   * Effettua l'operazione di purge sulla tabella delle vecchie versioni
   */
  public void purge() {

    try {
      // legge l'id della tabella log da utilizzare (calcolandolo come ultima riga del file di log "mappazzone")
      String idLog = getLogID();

      // connect the SQL operator
      OperazioniSQL operaio = new OperazioniSQL(mappa_configurazioni);

      // effettua il purge della tabella
      operaio.updateLog("STATO", "Inizio PURGE tabella", idLog);
      LogWrapper.logMessage(LogWrapper.INFO, "Purge vecchie versioni (PURGE)");
      operaio.purgeTable();

      // chiudo la connessione
      operaio.chiudiConnessione();
      LogWrapper.logMessage(LogWrapper.INFO, "Esecuzione purge terminata");
    }
    catch (Exception e) {
      LogWrapper.logMessage(LogWrapper.ERROR, e.getMessage(), e);
    }
  }

  /**
   * Aggiorna la tabella degli attributi
   * 
   * @param svuotaTabellaAttributi
   *          svuota la tabella se <code>true</code>, altrimenti appende le modifiche
   * @throws Exception
   *           in caso di errore
   */
  public void aggiornaTabella(boolean svuotaTabellaAttributi, List<String> colonneTabella) throws Exception {
    String idLog = "0";
    try {

      // legge l'id della tabella log da utilizzare (calcolandolo come ultima riga del file di log "mappazzone")
      idLog = getLogID();

      // connect the SQL operator
      OperazioniSQL operaio = new OperazioniSQL(mappa_configurazioni);

      if (svuotaTabellaAttributi) {
        // cancello la tabella
        operaio.updateLog("STATO", "Inizio TRUNCATE tabella", idLog);
        LogWrapper.logMessage(LogWrapper.INFO, "Cancello il contenuto della tabella (TRUNCATE)");

        operaio.cancellaTabella();

        operaio.updateLog("STATO", "Inizio ALTER tabella", idLog);
        LogWrapper.logMessage(LogWrapper.INFO, "Aggiungo le eventuali colonne mancanti (ALTER)");

        // ricreo la tabella
        operaio.alterTable(colonneTabella);
      }
      else {
        operaio.updateLog("STATO", "Inizio ALTER tabella", idLog);

        // aggiornamento con i soli nuovi tipi
        LogWrapper.logMessage(LogWrapper.INFO, "Aggiungo le eventuali colonne mancanti (ALTER)");
        operaio.alterTable(colonneTabella);
      }

      // chiudo la connessione
      operaio.chiudiConnessione();
      LogWrapper.logMessage(LogWrapper.INFO, "Esecuzione aggiornamento tabella attributi terminata");
    }
    catch (Exception e) {
      LogWrapper.logMessage(LogWrapper.ERROR, e.getMessage(), e);
      throw e;
    }
    // System.exit(0);
  }

  /**
   * Popola la tabella degli attributi
   * 
   * @param nomiAttributi
   *          lista dei nomi degli attributi
   * @param listaValoriAttributi
   *          lista dei valori degli attributi
   * @param tabellaTipiWC
   *          tabella dei tipi di dati Windchill
   * @throws Exception
   *           in caso di errore
   */
  public void popolaTabella(List<String> nomiAttributi, List<List<String>> listaValoriAttributi, LinkedHashMap<String, String> tabellaTipiWC)
      throws Exception {
    String idLog = "0";

    LogWrapper.logMessage(LogWrapper.INFO, "Inizio popolamento della tabella con i documenti e rispettivi attributi");

    if (listaValoriAttributi.size() == 0) {
      LogWrapper.logMessage(LogWrapper.INFO, "Non ci sono documenti da inserire");
      return;
    }

    try {
      LogWrapper.logMessage(LogWrapper.INFO, "Ci sono " + listaValoriAttributi.size() + " righe da inserire");
      // legge l'id della tabella log da utilizzare (calcolandolo come ultima riga del file di log "mappazzone")
      idLog = getLogID();

      // connect the SQL operator
      OperazioniSQL operaio = new OperazioniSQL(mappa_configurazioni);

      operaio.updateLog("STATO", "Inizio POPOLAMENTO tabella", idLog);
      LogWrapper.logMessage(LogWrapper.INFO, "Inserimento dei nuovi attributi (INSERT)");

      // costruisco la stringa con la listona dei nomi delle colonne separate da virgola
      String nomi = "[" + nomiAttributi.get(0) + "]";
      for (int i = 1; i < nomiAttributi.size(); i++) {
        nomi = nomi + " , [" + nomiAttributi.get(i) + "]";
      }

      // costruisco la stringa con la listona dei valori degli attributi separati da virgola
      // aggiungendo gli apici per le stringhe
      List<String> listaValori = new ArrayList<String>();
      for (List<String> valoriAttributi : listaValoriAttributi) {

        String valori = formattaPerSQL(tabellaTipiWC, nomiAttributi.get(0), valoriAttributi.get(0));
        for (int i = 1; i < nomiAttributi.size(); i++) {
          valori = valori + " , " + formattaPerSQL(tabellaTipiWC, nomiAttributi.get(i), valoriAttributi.get(i));
        }
        listaValori.add(valori);
      }
      // chiamo il metodo che aggiorna il DB
      operaio.popolaTabella(nomi, listaValori, idLog);

      // chiudo la connessione
      operaio.chiudiConnessione();
      LogWrapper.logMessage(LogWrapper.INFO, "Esecuzione POPOLAMENTO terminata");
    }
    catch (Exception e) {
      LogWrapper.logMessage(LogWrapper.ERROR, e.getMessage(), e);
      throw e;
    }
  }

  /**
   * Nel caso di attributi di tipo stringa aggiunge gli apici necessari per la query SQL
   * 
   * @param tabellaTipiWC
   *          tabella contenente i tipi di dati Windchill
   * @param nomeAttributo
   *          nome dell'attributo
   * @param valore
   *          valore dell'attributo
   * @return valore dell'attributo tra apici, se di tipo String
   */
  private String formattaPerSQL(LinkedHashMap<String, String> tabellaTipiWC, String nomeAttributo, String valore) {
    String tipo = tabellaTipiWC.get(nomeAttributo);
    if (valore == null || valore.equals("") || valore.equals(" ")) {
      return "NULL";
    }
    if (tipo.equalsIgnoreCase("java.lang.String") || tipo.equalsIgnoreCase("java.lang.Boolean")) {
      return "'" + valore.replace( "'", "''" ) + "'";
    }
    else {
      return valore;
    }
  }

  /**
   * Restitiuisce l'ID della riga nella tabella Log_esportazione del run corrente
   * 
   * @return l'ID del log per la tabella dei log
   */
  private String getLogID() {
    if (idLog.equals("-1")) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Non � ancora stata creata una riga nel log per il run corrente");
    }
    return idLog;
  }

  /**
   * Crea nella tabella Log_esportazione una riga per il run corrente
   * 
   * @param tipoRun
   *          tipo dell'operazione corrente
   */
  public void creaRigaLogInTabella(String tipoRun) {
    OperazioniSQL operaio = new OperazioniSQL(mappa_configurazioni);
    idLog = operaio.inserisciLog(tipoRun);
    operaio.chiudiConnessione();
  }

  public void finish() {
    // Scrive nel log che l'esecuzione � terminata
    OperazioniSQL operaio = new OperazioniSQL(mappa_configurazioni);
    operaio.updateLog("STATO", "FINISH", idLog);
    operaio.updateLog("STOP", "valore non utilizzato", idLog);
    operaio.chiudiConnessione();
  }

  public Date getDataUltimaEsecuzione() {
    Date dataCreazione = null;
    LogWrapper.logMessage(LogWrapper.INFO, "Id log attuale: " + idLog);
    // idLog = 0 => caso del primo lancio della funzione
    if (!idLog.equals("0")) {
      int idLogAttuale = Integer.parseInt(idLog);
      int count = 1;
      // scorro fino alla prima data di stop!=null a ritroso dall'id attuale
      while (dataCreazione == null && count < idLogAttuale) {
        String idLogPrecedente = String.valueOf(idLogAttuale - count);
        OperazioniSQL operaio = new OperazioniSQL(mappa_configurazioni);
        dataCreazione = operaio.getDataStop(idLogPrecedente);
        operaio.chiudiConnessione();
        count++;
      }
    }
    return dataCreazione;
  }
}
