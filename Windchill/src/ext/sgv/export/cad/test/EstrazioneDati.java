package ext.sgv.export.cad.test;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;

import javax.swing.JOptionPane;

import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatDtdDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.ext.mssql.MsSqlConnection;

public class EstrazioneDati {

  @SuppressWarnings("unchecked")
  public static void main(String[] args) throws Exception {
    // database connection
    @SuppressWarnings("unused")
    Class<? extends Driver> driverClass = (Class<? extends Driver>) Class.forName("net.sourceforge.jtds.jdbc.Driver");
    String url = javax.swing.JOptionPane.showInputDialog(null, "URL jdbc del database da esportare",
        "jdbc:jtds:sqlserver://localhost:1433/SGV_export_intralink");
    if (url == null) {
      System.out.println("ANNULLATO");
      System.exit(0);
    }
    Connection jdbcConnection = DriverManager.getConnection(url, "exportIntralink", "exportIntralink");
    IDatabaseConnection connection = new MsSqlConnection(jdbcConnection);

    String nomeFile = javax.swing.JOptionPane.showInputDialog("Nome del file da generare");
    if (nomeFile == null || nomeFile.equals("")) {
      System.out.println("ANNULLATO");
      System.exit(0);
    }
    if (!nomeFile.toLowerCase().endsWith(".xml")) {
      nomeFile = nomeFile + ".xml";
    }
    File fxml = new File(nomeFile);
    String bareName = fxml.getName();
    if (!fxml.isAbsolute()) {
      fxml = new File("C:/temp", bareName);
    }
    File fdtd = new File(fxml.getParentFile(), bareName.substring(0, bareName.length() - 4) + ".dtd");
    int opt = javax.swing.JOptionPane.showConfirmDialog(
        null,
        "Genero il file xml del DB SGV_export_intralink \n" + "Salvo il file xml in " + fxml.getAbsolutePath() + "\nE il file dtd in "
            + fdtd.getAbsolutePath(), "Esporta dati", JOptionPane.OK_CANCEL_OPTION);
    if (opt != JOptionPane.OK_OPTION) {
      System.out.println("ANNULLATO");
      System.exit(0);
    }

    // full database export
    IDataSet fullDataSet = connection.createDataSet();
    connection.getConfig().setProperty(org.dbunit.database.DatabaseConfig.PROPERTY_ESCAPE_PATTERN, "\"?\"");
    // Non abilitare questa opzione, perch� poi non vengono importati correttamente
    // config.setProperty(DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES, "true");

    FlatXmlDataSet.write(fullDataSet, new FileOutputStream(fxml));
    System.out.println("Export in " + fxml + " terminato");

    FlatDtdDataSet.write(connection.createDataSet(), new FileOutputStream(fdtd));
    System.out.println("Export del DTD in " + fdtd + " terminato");
    System.out.println("<!DOCTYPE dataset SYSTEM \"" + fdtd + "\">");
    JOptionPane.showMessageDialog(null, "File generati.\n Apri l'xml e aggiungi \n<!DOCTYPE dataset SYSTEM \"percorso dtd\">\n"
        + "sulla seconda riga se vuoi usare il dtd");

  }

}
