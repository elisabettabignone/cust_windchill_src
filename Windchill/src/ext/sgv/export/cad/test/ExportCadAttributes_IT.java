package ext.sgv.export.cad.test;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import org.dbunit.DBTestCase;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseSequenceFilter;
import org.dbunit.dataset.FilteredDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mssql.InsertIdentityOperation;
import org.dbunit.ext.mssql.MsSqlDataTypeFactory;
import org.junit.After;
import org.junit.Test;

import wt.epm.EPMDocument;

import com.ibm.icu.text.SimpleDateFormat;

import ext.caditech.utility.LogWrapper;
import ext.caditech.utility.SearchAttributes;
import ext.caditech.utility.SearchEPMDocument;
import ext.sgv.export.cad.ExportCadAttributes;

//@formatter:off
/**
 * Test automatici per la procedura di import degli attributi da PDMLink su database.
 * Dataset al <b>07/04/2015</b>:
 * 
 * <p>
   * <TABLE BORDER="1" CELLSPACING="0" CELLPADDING="4">
   * <tr><th>Data creazione ultima ver/iter</th><th>Numero documenti</th></tr>
   * <tr><td>29/05/2014</td><td>841</td></tr>
   * <tr><td>17/07/2014</td><td>2</td></tr>
   * <tr><td>14/11/2014</td><td>80</td></tr>
   * 
   * 
   * <tr><td>25/02/2015</td><td>138</td></tr>
   * <tr><td>03/03/2015</td><td>3</td></tr>
   * <tr><td>05/03/2015</td><td>1</td></tr>
   * <tr><td>09/03/2015</td><td>10</td></tr>
   * <tr><td>11/03/2015</td><td>1</td></tr>
   * <tr><td>18/03/2015</td><td>3</td></tr>
   * <tr><td>26/03/2015</td><td>1</td></tr>
   * <tr><td>01/04/2015</td><td>1</td></tr>
   * <tr><td>08/04/2015</td><td>1</td></tr>
   * <tr><td>14/04/2015</td><td>1</td></tr>
   * <tr><td>16/04/2015</td><td>1</td></tr>
   * </table>
   * </p>
   * Nel database <b><i>Esportazione</i></b> usato per i test sono gi� presenti i documenti fino al <b>14/11/2014</b> compresi,quindi <b>923</b>. 
   * A questi ho aggiunto <b>5</b> revisioni vecchie per poter testare il purge, per un totale di <b>928</b> documenti.
   * <p>
   * La procedura in modalit� DELTA va a recuperare tutti i file a partire dall'ultima data di stop dalla tabella <b><i>Log_Esportazione</i></b> avente come stato = FINISH.
   * Nel database di partenza tale data vale <b>27/02/2015</b>. Quindi nella modalit� delta verranno "persi" <b>139</b> documenti.
   * </p>
 * @author administrator
 *
 */
//@formatter:on
public class ExportCadAttributes_IT extends DBTestCase {

  private Statement stmt = null;

  /** numero totale di documenti presenti dentro windchill */
  private int numTotaleDocInWC = 0;

  /**
   * usando la procedura in modalit� "delta" abbiamo una finestra temporale di documenti che non verrano popolati,
   * ovvero dal 15/11/2014 al 26/02/2015
   */
  private int numDocFuoriDalDeltaInWC = 0;

  /** documenti creati dopo questa data 27/02/2015, data di partenza del delta dei test */
  private int numDocDopo27_02_2015 = 0;

  /** Numero delle revisioni vecchie gi� presenti nella tabella Esportazione per poter testare il purge */
  private int numElementiDoppiPerPurge = 5;

  public ExportCadAttributes_IT() {
    System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS, "net.sourceforge.jtds.jdbc.Driver");
    System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL, "jdbc:jtds:sqlserver://localhost/SGV_export_intralink");
    System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME, "exportIntralink");
    System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD, "exportIntralink");
  }

  @Override
  protected IDataSet getDataSet() throws Exception {

    System.out.println("\n------------------------------------------------------------");
    File xmlFile = new File("C:/ptc/Windchill_10.2/Windchill/src/test/sgv/dataset/ExportCadAttributes_IT.xml");
    System.out.println("DataSource " + xmlFile.getAbsolutePath());
    IDataSet ds = new FlatXmlDataSetBuilder().build(new FileInputStream(xmlFile));
    FilteredDataSet fds = new FilteredDataSet(new DatabaseSequenceFilter(getConnection()), ds);
    return fds;
  }

  /**
   * Droppa la tabella Esportazione, che potrebbe contenere colonne di attributi "sporchi"
   */
  private void RicreaTabellaEsportazione() {
    try {
      String eliminaTabella = "DROP TABLE Esportazione";
      getStatement().executeUpdate(eliminaTabella);

      String rigeneraStruttura = "CREATE TABLE Esportazione ( "
          + " [Cartella] [nvarchar](4000) NULL, "
          + " [Nome] [varchar](200) NOT NULL, "
          + " [Revisione] [varchar](10) NOT NULL, "
          + " [Versione] [int] NOT NULL, "
          + " [Gruppo_Elettrico] [varchar](10) NULL, "
          + " [Denominazione] [nvarchar](4000) NULL, "
          + " [Dimensioni] [nvarchar](4000) NULL, "
          + " [Udm] [nvarchar](4000) NULL, "
          + " [Descrizione_Ita] [nvarchar](4000) NULL, "
          + " [Disegnatore] [nvarchar](4000) NULL, "
          + " [Cliente] [nvarchar](4000) NULL, "
          + " [data creazione] [nvarchar](4000) NULL, "
          + " [Livello fase di sviluppo] [nvarchar](4000) NULL, "
          + " [oggetto*] [nvarchar](4000) NULL, "
          + " CONSTRAINT [PK_Esportazione] PRIMARY KEY CLUSTERED "
          + "( "
          + " [Nome] ASC, "
          + " [Revisione] ASC, "
          + " [Versione] ASC "
          + ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]  "
          + ") ON [PRIMARY]";
      getStatement().executeUpdate(rigeneraStruttura);
    }
    catch (SQLException e1) {
      System.out.println("Eccezione SQL: " + e1.getMessage());
      e1.printStackTrace();
    }
  }

  @Override
  public void setUp() throws Exception {
    RicreaTabellaEsportazione();

    // insert data into db
    InsertIdentityOperation.CLEAN_INSERT.execute(getConnection(), getDataSet());
    System.out.println("Database caricato");
    getStatement();

    ArrayList<EPMDocument> tutti = SearchEPMDocument.findLatestEPMDocuments(false);
    numTotaleDocInWC = tutti.size();
    System.out.println("Numero totale di documenti in Windchill (esclusi doc nuovi): " + numTotaleDocInWC);
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    String dataI = "15/11/2014 00:00:00";
    Date dataInizio = sdf.parse(dataI);
    String dataF = "25/02/2015 23:59:59";
    Date dataFine = sdf.parse(dataF);
    ArrayList<EPMDocument> fuoriDalDelta = SearchEPMDocument.findLatestEPMDocuments(dataInizio, dataFine, false);
    System.out.println("Documenti esclusi dal popolamento in modalit� delta: " + fuoriDalDelta.size());
    numDocFuoriDalDeltaInWC = fuoriDalDelta.size();

    String ultimaEsecuzione = "27/02/2015 05:32:00";
    Date dataUltimaEsecuzione = sdf.parse(ultimaEsecuzione);
    ArrayList<EPMDocument> epmDocs = SearchEPMDocument.findLatestEPMDocuments(dataUltimaEsecuzione, false);
    numDocDopo27_02_2015 = epmDocs.size();
    System.out.println("Documenti creati dal 27/02/2015: " + numDocDopo27_02_2015);
  }

  @After
  public void tearDown() {
    chiudiConnessione();
  }

  @Override
  protected void setUpDatabaseConfig(DatabaseConfig config) {
    super.setUpDatabaseConfig(config);
    config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MsSqlDataTypeFactory());
  }

  /**
   * testa la validit� dei dati inseriti, per il singolo file "parall_prova.prt"
   */
  @Test
  public void testData() {
    String args[] = { "modo=latest", "modoSql=truncate", "purge=n", "test=true", "intervallo=7", "dataInizio=07/04/2015" };
    try {
      ExportCadAttributes.main(args);
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }

    String query = "SELECT * FROM Esportazione WHERE Nome='PARALL_PROVA.PRT'";
    try {
      String testValue = "";
      ResultSet rs = executeQuery(query);
      while (rs.next()) {
        assertTrue(1 == rs.getRow());

        testValue = rs.getString("CODICE");
        assertTrue(testValue.equalsIgnoreCase("PARALL_PROVA__"));

        testValue = rs.getString("att_bool");
        assertTrue(testValue.equalsIgnoreCase("true"));

        testValue = rs.getString("att_int");
        assertEquals(1, Integer.parseInt(testValue));

        testValue = rs.getString("att_string");
        assertEquals("pruva_stringg", testValue);
      }
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }
  }

  /**
   * controlla l'esportazione di un insieme di file, a partire dalla data di stop dell'ultima esecuzione della procedura
   * andata a buon fine, ovvero il 27/02/2015, senza cancellare i dati presenti sul database, senza eseguire il purge
   */
  @Test
  public void testDeltaAlter() {
    String args[] = { "modo=delta", "modoSql=alter", "purge=n", "test=true" };
    try {
      ExportCadAttributes.main(args);
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }
    int num = getRowNumber("Esportazione");
    System.out.println("Numero di righe dopo l'esecuzione --> " + num);
    // ci aspettiamo: numTotaleDocInWC + 5 gi� presenti nel BD aggiunti per testare il purge - i documenti esclusi dal
    // delta delle date
    assertTrue(num == numTotaleDocInWC + numElementiDoppiPerPurge - numDocFuoriDalDeltaInWC);
  }

  /**
   * controlla l'esportazione di un insieme di file, a partire dalla data di stop dell'ultima esecuzione della procedura
   * andata a buon fine, ovvero il 27/02/2015, senza cancellare i dati presenti sul database, eseguendo il purge
   */
  @Test
  public void testDeltaAlterConPurge() {
    String args[] = { "modo=delta", "modoSql=alter", "purge=s", "test=true" };
    try {
      ExportCadAttributes.main(args);
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }
    int num = getRowNumber("Esportazione");
    System.out.println("Numero di righe dopo l'esecuzione --> " + num);
    assertTrue(num == numTotaleDocInWC - numDocFuoriDalDeltaInWC);
  }

  /**
   * controlla l'esportazione di un insieme di file, a partire dalla data di stop dell'ultima esecuzione della procedura
   * andata a buon fine, ovvero il 27/02/2015, cancellando i dati presenti sul database, senza eseguire il purge
   */
  @Test
  public void testDeltaTruncate() {

    String args[] = { "modo=delta", "modoSql=truncate", "purge=n", "test=true" };
    try {
      ExportCadAttributes.main(args);
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }
    int num = getRowNumber("Esportazione");
    System.out.println("Numero di righe dopo l'esecuzione --> " + num);
    assertTrue(num == numDocDopo27_02_2015);
  }

  /**
   * Esegue il popolamento di tutti i documenti, a partire dalla data di inizio, suddividendo il thread in intervalli
   * settimanali, eliminando i dati dal database, senza eseguire il purge
   */
  @Test
  public void testLatestPerSettimanaTruncate() {
    String args[] = { "modo=latest", "modoSql=truncate", "purge=n", "test=true", "intervallo=7", "dataInizio=01/01/2010" };
    try {
      ExportCadAttributes.main(args);
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }
    int num = getRowNumber("Esportazione");
    System.out.println("Numero di righe dopo l'esecuzione --> " + num);
    assertTrue(num == numTotaleDocInWC);
  }

  /**
   * Esegue il popolamento di tutti i documenti, a partire dalla data di inizio, suddividendo il thread in intervalli
   * annuali, senza eliminare i dati dal database, senza eseguire il purge
   */
  @Test
  public void testLatestPerAnnoAlter() {
    String args[] = { "modo=latest", "modoSql=alter", "purge=n", "test=true", "intervallo=365", "dataInizio=01/01/2010" };
    try {
      ExportCadAttributes.main(args);
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }
    int num = getRowNumber("Esportazione");
    System.out.println("Numero di righe dopo l'esecuzione --> " + num);
    assertTrue(num == numTotaleDocInWC + numElementiDoppiPerPurge);
  }

  /**
   * Esegue il popolamento di tutti i documenti, a partire dalla data di inizio, suddividendo il thread in intervalli
   * annuali, senza eliminare i dati dal database, eseguendo il purge
   */
  @Test
  public void testLatestPerAnnoAlterConPurge() {
    int num = getRowNumber("Esportazione");
    System.out.println("Numero di righe prima dell'esecuzione --> " + num);
    String args[] = { "modo=latest", "modoSql=alter", "purge=s", "test=true", "intervallo=365", "dataInizio=01/01/2010" };
    try {
      ExportCadAttributes.main(args);
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }
    num = getRowNumber("Esportazione");
    System.out.println("Numero di righe dopo l'esecuzione --> " + num);
    assertTrue(num == numTotaleDocInWC);
  }

  /**
   * controlla che venga effettuato l'aggiornamento delle colonne nella tabella Esportazione, aggiungendo una colonna
   * per ogni attributo trovato in WindChill per gli EPMDocument
   */
  @Test
  public void testAggiornaTabella() {
    String args[] = { "modo=delta", "modoSql=alter", "purge=n", "test=true" };
    int prima = getColumnNumber("Esportazione");
    System.out.println("Numero di colonne prima dell'esecuzione --> " + prima);
    try {
      ExportCadAttributes.main(args);
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }
    int attributiTrovati = SearchAttributes.getSoftAttributesName("ge.caditech.DefaultEPMDocument").size();
    int dopo = getColumnNumber("Esportazione");
    System.out.println("Numero di colonne dopo l'esecuzione --> " + dopo);
    assertTrue(dopo == prima + attributiTrovati);
  }

  /** restituisce il numero di colonne per la tabella passata */
  private int getColumnNumber(String nomeTabella) {
    String query = "SELECT count(*) FROM information_schema.columns WHERE table_name = '" + nomeTabella + "'";
    String res = null;
    try {
      res = executeCountQuery(query);
    }
    catch (SQLException e) {
      System.out.println("ERRORE recuperando il numero delle colonne della tabella " + nomeTabella + "  " + e.getMessage());
      e.printStackTrace();
    }
    return Integer.parseInt(res);
  }

  /** restituisce il numero di righe per la tabella passata */
  private int getRowNumber(String nomeTabella) {
    String query = "SELECT count(*) FROM " + nomeTabella;
    String res = null;
    try {
      res = executeCountQuery(query);
    }
    catch (SQLException e) {
      System.out.println("ERRORE recuperando il numero delle colonne della tabella " + nomeTabella + "  " + e.getMessage());
      e.printStackTrace();
    }
    return Integer.parseInt(res);
  }

  /**
   * Restituisce lo statement per eseguire le query
   * 
   * @return lo statement
   */
  private Statement getStatement() {
    try {
      if (stmt == null) {
        Class.forName("net.sourceforge.jtds.jdbc.Driver");
        Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://localhost/SGV_export_intralink", "exportIntralink", "exportIntralink");
        stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
      }
    }
    catch (Exception ex) {
      System.out.println("ERRORE: Impossibile stabilire la connessione SQL: " + ex.getMessage());
      ex.printStackTrace();
    }
    return stmt;
  }

  /** restitiuisce il resultset per la query eseguita */
  private ResultSet executeQuery(String query) throws SQLException {
    System.out.println("Query --> " + query);
    ResultSet rs = getStatement().executeQuery(query);

    return rs;
  }

  /** restitiuisce il numero di righe per la query eseguita */
  private String executeCountQuery(String query) throws SQLException {
    System.out.println("Query --> " + query);
    String res = "";
    ResultSet rs = getStatement().executeQuery(query);

    while (rs.next()) {
      res = rs.getString(1);
      System.out.println("Risultato della query --> " + res);
    }
    rs.close();
    return res;
  }

  /**
   * Chiude la connessione al database e il relativo Statement
   */
  public void chiudiConnessione() {
    try {
      if (stmt != null) {
        if (stmt.getConnection() != null)
          stmt.getConnection().close();
        stmt.close();
      }
    }
    catch (Exception ex) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore nel chiudere la connessione SQL: " + ex.getMessage(), ex);
    }
  }
}
