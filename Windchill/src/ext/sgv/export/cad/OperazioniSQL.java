package ext.sgv.export.cad;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import wt.util.WTStandardDateFormat;
import ext.caditech.utility.LogWrapper;

/**
 * Classe che si occupa di eseguire le operazioni su database SQL
 * 
 * @author Sagoleo
 * 
 */
public class OperazioniSQL {

  private Statement _stmt = null;

  private HashMap<String, String> mappa_configurazioni = null;

  private String tbname = null;

  public String getTbname() {
    return tbname;
  }

  public OperazioniSQL(HashMap<String, String> _mappa) {
    mappa_configurazioni = _mappa;
    tbname = mappa_configurazioni.get("sql_table_name");
  }

  public void checkConnection() throws SQLException, ClassNotFoundException {
    this.getStatement();
  }

  /**
   * Restituisce lo statement per eseguire le query
   * 
   * @return
   * @throws SQLException
   * @throws ClassNotFoundException
   * @throws Exception
   */
  private Statement getStatement() throws SQLException, ClassNotFoundException {

    if (_stmt == null) {
      Class.forName("net.sourceforge.jtds.jdbc.Driver");
      String connessione = mappa_configurazioni.get("sql_url")
          + mappa_configurazioni.get("sql_dbname")
          + (mappa_configurazioni.containsKey("sql_istanza") && mappa_configurazioni.get("sql_istanza").trim() != "" ? ";"
              + mappa_configurazioni.get("sql_istanza") : "");
      // CDTLogWrapper.logMessage(CDTLogWrapper.INFO, "Stringa per la connessione -> " + connessione);
      Connection con;

      con = DriverManager.getConnection(connessione, mappa_configurazioni.get("sql_dbuser"), mappa_configurazioni.get("sql_dbpass"));

      _stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
    }
    return _stmt;
  }

  /**
   * Chiude la connessione al database e il relativo Statement
   */
  public void chiudiConnessione() {
    try {
      if (_stmt != null) {
        if (_stmt.getConnection() != null)
          _stmt.getConnection().close();
        _stmt.close();
      }
    }
    catch (Exception ex) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore nel chiudere la connessione SQL: " + ex.getMessage(), ex);
    }
  }

  /**
   * Svuota la tabella SQL, se esiste
   */
  public void cancellaTabella() {
    try {
      LogWrapper.logMessage(LogWrapper.INFO, "Cancello se esiste la tabella '" + getTbname() + "'");

      String query = "IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '" + getTbname() + "') TRUNCATE TABLE "
          + getTbname() + ";";
      getStatement().executeUpdate(query);
    }
    catch (Exception ex) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Impossibile recuperare la lista degli attributi per l'ordine dato", ex);
    }
  }

  /**
   * Crea la tabella di export leggendo la struttura dalla lista
   * 
   * @param mappa_tabella
   */
  public void creaTabella(List<String> listTabella) {
    String query = "";
    try {
      String campi = "";
      query = "CREATE TABLE " + mappa_configurazioni.get("sql_table_name") + " (";
      for (String s : listTabella) {
        if (!campi.equals(""))
          campi = campi + ", ";
        String nomeCampo = "";
        String[] tokens = s.split(" ");
        int high = 0;
        nomeCampo = "[";

        if ((tokens[tokens.length - 2].equalsIgnoreCase("NOT")) && (tokens[tokens.length - 1].equalsIgnoreCase("NULL"))) {
          high = tokens.length - 2;

          for (int iToken = 0; iToken < high - 1; iToken++) {
            nomeCampo += tokens[iToken] + (iToken == high - 2 ? "]" : "") + " ";
          }
          nomeCampo += tokens[high - 1] + " NOT NULL";
        }
        else {
          for (int iToken = 0; iToken < tokens.length - 1; iToken++)
            nomeCampo += tokens[iToken] + (iToken == tokens.length - 2 ? "]" : "") + " ";
          nomeCampo += tokens[tokens.length - 1];
        }

        campi = campi + nomeCampo;
      }

      query = query + campi + ");";
      LogWrapper.logMessage(LogWrapper.INFO, "Query creazione tabella: " + query);
      getStatement().executeUpdate(query);
    }
    catch (Exception ex) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore nel creare la tabella: " + ex.getMessage() + " (query: " + query + ")", ex);
    }
  }

  /**
   * Inserisce un oggetto con tutti i suoi attributi in database
   * 
   * @param sqlOggettoConAttributi
   *          la stringa SQL contenente tutti gli attributi dell'oggetto
   */
  // private boolean inserisciInTabella(String sqlOggettoConAttributi) {
  // String sqlInsert = "";
  // boolean isResultOK = true;
  // try {
  // sqlInsert = "INSERT INTO " + tbname + " VALUES (" + sqlOggettoConAttributi + ")";
  // getStatement().executeUpdate(sqlInsert);
  // CDTLogWrapper.logMessage(CDTLogWrapper.DEBUG, "Inserita riga " + sqlOggettoConAttributi);
  // }
  // catch (Exception ex) {
  // CDTLogWrapper.logMessage(CDTLogWrapper.ERROR, "Errore nell'inserire la riga in tabella (" + sqlInsert + "): " +
  // ex.getMessage(), ex);
  // isResultOK = false;
  // }
  // return isResultOK;
  // }

  /**
   * Inserisce un oggetto con tutti i suoi attributi in database
   * 
   * @param sqlOggettoConAttributi
   *          la stringa SQL contenente tutti gli attributi dell'oggetto
   */
  private boolean inserisciInTabella(String nomiColonne, String sqlOggettoConAttributi) {
    String sqlInsert = "";
    boolean isResultOK = true;
    try {
      sqlInsert = "INSERT INTO " + tbname + " ( " + nomiColonne + " ) " + " VALUES ( " + sqlOggettoConAttributi + " )";
      getStatement().executeUpdate(sqlInsert);
      LogWrapper.logMessage(LogWrapper.DEBUG, "Inserita riga " + sqlOggettoConAttributi);
    }
    catch (Exception ex) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore nell'inserire la riga in tabella (" + sqlInsert + "): " + ex.getMessage(), ex);
      isResultOK = false;
    }
    return isResultOK;
  }

  /**
   * Inserisce un record nella tabella di log con i campi start, tipo_run, nuovi_record , stato = 'running'
   * 
   * @param tipoRun
   *          modalit� di funzionamento ( uno tra Delta, Latest, All )
   */
  public String inserisciLog(String tipoRun) {
    String query = "";
    ResultSet rs = null;
    Date dNow = new Date();
    String idLog = "0";

    // SimpleDateFormat ft = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
    String dataInizio = WTStandardDateFormat.format(new Date(), "MM/dd/yyyy HH:mm:ss");

    String sqlCampi = "START, TIPO_RUN, NUOVI_RECORD , STATO";
    String sqlValori = "'" + dataInizio + "','" + tipoRun + "', 0, 'RUNNING'";

    try {
      query = "INSERT INTO LOG_ESPORTAZIONE (" + sqlCampi + ") VALUES (" + sqlValori + ")";
      getStatement().executeUpdate(query);
      LogWrapper.logMessage(LogWrapper.INFO, "Inserita riga log " + query);
    }
    catch (Exception ex) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore nell'inserimento della riga in tabella (" + query + "): " + ex.getMessage(), ex);
      idLog = "0";
    }

    try {
      // ricava l'id del record nel file di log
      query = "SELECT MAX(ID) FROM LOG_ESPORTAZIONE";
      rs = getStatement().executeQuery(query);

      if (rs.next()) {
        idLog = rs.getString(1);
        rs.close();
        LogWrapper.logMessage(LogWrapper.INFO, "Creata la riga di log con id -> " + idLog);
      }
    }
    catch (Exception ex) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore nel conto delle righe della tabella di log (" + query + "): " + ex.getMessage(), ex);
      idLog = "0";
    }
    return idLog;
  }

  /**
   * Aggiorna un record nella tabella di log
   * 
   * @param sqlCampo
   *          la stringa SQL contenente il campo da da modificare (es nuovi_record = 52)
   * 
   */
  public boolean updateLog(String sqlCampo, String sqlValore, String idLog) {
    String queryUpdate = "";
    boolean isResultOK = true;
    String dataFine = WTStandardDateFormat.format(new Date(), "MM/dd/yyyy HH:mm:ss");
    // SimpleDateFormat ft = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss"); // vero se lingua sql server = us_english
    try {

      if (sqlCampo.equalsIgnoreCase("NUOVI_RECORD")) {
        queryUpdate = "UPDATE LOG_ESPORTAZIONE SET NUOVI_RECORD = NUOVI_RECORD + " + sqlValore + " WHERE ID = " + idLog;
      }

      if (sqlCampo.equalsIgnoreCase("STOP")) {
        queryUpdate = "UPDATE LOG_ESPORTAZIONE SET STOP = '" + dataFine + "' WHERE ID = " + idLog;
      }

      if (sqlCampo.equalsIgnoreCase("STATO")) {
        queryUpdate = "UPDATE LOG_ESPORTAZIONE SET STATO = '" + sqlValore + "' WHERE ID = " + idLog;
      }

      getStatement().executeUpdate(queryUpdate);
      LogWrapper.logMessage(LogWrapper.INFO, "Aggiornata riga log " + queryUpdate);
    }
    catch (Exception ex) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore nell'update della riga in tabella log (" + queryUpdate + "): " + ex.getMessage(), ex);
      isResultOK = false;
    }

    return isResultOK;
  }

  /**
   * Legge il file contenente gli oggetti e i rispettivi valori degli attributi: per ogni riga chiama l'inserimento in
   * db
   * 
   * @param nomeFileOggetti
   *          il nome del file contenente l'sql di ogni oggetto da inserire
   * @throws IOException
   *           in caso di errore di lettura del file contenente gli oggetti
   */
  // public void popolaTabella(String nomeFileOggetti, String idLog) throws IOException {
  // int numRighe = 0;
  // int numRigheOk = 0;
  // boolean isRigaOK = true;
  // CDTLogWrapper.logMessage(CDTLogWrapper.INFO, "Prendo gli oggetti dal file " + nomeFileOggetti + "...");
  //
  // File file = new File(nomeFileOggetti);
  //
  // BufferedReader br = new BufferedReader(new FileReader(file));
  // String riga = null;
  // while ((riga = br.readLine()) != null) {
  // if (!riga.startsWith("#")) {
  // numRighe++;
  // isRigaOK = inserisciInTabella(riga.trim());
  //
  // if (isRigaOK) {
  // numRigheOk++;
  // }
  // }
  // }
  // br.close();
  // CDTLogWrapper.logMessage(CDTLogWrapper.INFO, "Processati " + numRighe + " oggetti, di cui " + numRigheOk +
  // " andati a buon fine");
  //
  // // aggiorna il numero di record nuovi inseriti
  // updateLog("NUOVI_RECORD", Integer.toString(numRigheOk), idLog);
  // }

  public void popolaTabella(String nomiColonne, List<String> listaValori, String idLog) throws IOException {
    int numRighe = listaValori.size();
    int numRigheOk = 0;
    boolean isRigaOK = true;
    LogWrapper.logMessage(LogWrapper.INFO, "Popolo la tabella con i valori degli attributi...");

    for (String valori : listaValori) {
      isRigaOK = inserisciInTabella(nomiColonne, valori);
      if (isRigaOK) {
        numRigheOk++;
      }
    }
    LogWrapper.logMessage(LogWrapper.INFO, "Processati " + numRighe + " oggetti, di cui " + numRigheOk + " andati a buon fine");

    // aggiorna il numero di record nuovi inseriti
    updateLog("NUOVI_RECORD", Integer.toString(numRigheOk), idLog);
  }

  /*
   * Return the column names for the table given
   * 
   * @return the ResultSet with the column names
   * 
   * @throw SQLException
   */
  public ResultSet getTableColumns() throws Exception {
    String sqlSelect = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + tbname + "'";
    return getStatement().executeQuery(sqlSelect);
  }

  /*
   * Return the primary key column names for the table given
   * 
   * @return the ResultSet with the column names
   * 
   * @throw SQLException
   */
  public ResultSet getPrimaryKey() throws Exception {
    String sqlSelect = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE TABLE_NAME LIKE '" + tbname
        + "' AND CONSTRAINT_NAME LIKE 'PK_ESPORTAZIONE'";
    return getStatement().executeQuery(sqlSelect);
  }

  /*
   * Alter the table adding the missing column
   * 
   * @param columnList
   * 
   * @throw SQLException
   */
  public void alterTable(List<String> columnList) throws SQLException {
    boolean finded;
    String sqlAlter, tmpName, colDef = "", colName;
    ResultSet rs = null;
    List<String> columnToAdd = new ArrayList<String>();
    String[] tokens;
    int high = 0;

    try {

      rs = getTableColumns();

      LogWrapper.logMessage(LogWrapper.INFO, "Colonne trovate in tabella:");
      while (rs.next()) {
        tmpName = rs.getString(1);

        LogWrapper.logMessage(LogWrapper.INFO, "'" + tmpName + "'");
      }

      rs.close();

      // scorro tutte le definizioni di colonna presenti nel file di anagrafica
      LogWrapper.logMessage(LogWrapper.INFO, "Numero di colonne trovate nella lista: " + columnList.size());
      for (int i = 0; i < columnList.size(); i++) {
        colDef = columnList.get(i);
        finded = false;

        tokens = colDef.split(" ");
        colName = tokens[0];

        if (tokens.length > 2) {
          if ((tokens[tokens.length - 2].equalsIgnoreCase("NOT")) && (tokens[tokens.length - 1].equalsIgnoreCase("NULL")))
            high = tokens.length - 3;
          else
            high = tokens.length - 1;

          for (int iToken = 1; iToken < high; iToken++) {
            colName += " " + tokens[iToken];
          }
        }
        LogWrapper.logMessage(LogWrapper.INFO, "Colonna della lista da verificare: '" + colName + "'");

        // si posiziona sul primo record del result set
        rs = getTableColumns();

        // ho ricavato il nome della colonna da confrontare con quelli presenti nella tabella
        while (rs.next()) {
          tmpName = rs.getString(1);

          LogWrapper.logMessage(LogWrapper.DEBUG, "Confronto con: '" + tmpName + "'");
          if (colName.equalsIgnoreCase(tmpName)) {
            finded = true;
            LogWrapper.logMessage(LogWrapper.INFO, "Confronto tra lista e sql: " + colName + " e " + tmpName
                + " -> trovata colonna, quindi non la inserisco");
            break;
          }
        }

        rs.close();

        // missing column
        if (finded) {
          LogWrapper.logMessage(LogWrapper.INFO, "Colonna " + colName + " trovata");
        }
        else {
          columnToAdd.add(colDef);
          LogWrapper.logMessage(LogWrapper.INFO, "La colonna " + colDef + " deve essere aggiunta alla tabella");
        }
      }

      // verifico se ci sono colonne da aggiungere
      if (columnToAdd.size() > 0) {
        sqlAlter = "ALTER TABLE " + tbname + " ADD";

        // add the missing colums with the datatype
        for (String s : columnToAdd) {
          String nomeCampo = "[";
          tokens = s.split(" ");
          // /////////////////////////////////////////////////////////
          // se si tratta di colonne che non ammettono valori nulli (quindi la definizione del tipo SQL � NOT NULL) devo
          // considerare due token in meno
          if ((tokens[tokens.length - 2].equalsIgnoreCase("NOT")) && (tokens[tokens.length - 1].equalsIgnoreCase("NULL"))) {
            high = tokens.length - 2;

            for (int iToken = 0; iToken < high - 1; iToken++) {
              nomeCampo += tokens[iToken] + (iToken == high - 2 ? "]" : "") + " ";
            }
            nomeCampo += tokens[high - 1] + " NOT NULL";
          }
          else {
            for (int iToken = 0; iToken < tokens.length - 1; iToken++)
              nomeCampo += tokens[iToken] + (iToken == tokens.length - 2 ? "]" : "") + " ";
            nomeCampo += tokens[tokens.length - 1];
          }

          sqlAlter += " " + nomeCampo + ",";
        }

        sqlAlter = sqlAlter.substring(0, sqlAlter.length() - 1);
        LogWrapper.logMessage(LogWrapper.INFO, "Query da eseguire: " + sqlAlter);

        // exectute the SQL Command
        getStatement().executeUpdate(sqlAlter);
      }
      else
        LogWrapper.logMessage(LogWrapper.INFO, "Nessuna colonna da aggiungere alla tabella");

    }
    catch (Exception e) {

      e.printStackTrace();
    }
    finally {
      if (rs != null)
        rs.close();
    }
  }

  /*
   * Purge the oldest entities in the table
   * 
   * @throw SQLException
   */
  public void purgeTable() throws SQLException {
    CallableStatement clstm = null;

    try {
      clstm = getStatement().getConnection().prepareCall("purgeOldRevisionVersion");
      clstm.execute();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    finally {
      if (clstm != null)
        clstm.close();
    }
  }

  /*
   * Create the primary key for the table
   */
  public void createPrimaryKey() throws SQLException {
    String sqlAlter;
    ResultSet rs = null;

    try {
      // check if the primary key already exists
      rs = getPrimaryKey();

      if (!rs.next()) {
        sqlAlter = "ALTER TABLE dbo." + getTbname() + " ADD CONSTRAINT PK_Esportazione PRIMARY KEY (Nome, Revisione, Versione)";

        LogWrapper.logMessage(LogWrapper.INFO, "Query creazione chiave primaria: " + sqlAlter);
        // exectute the SQL Command
        getStatement().executeUpdate(sqlAlter);
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    finally {
      if (rs != null)
        rs.close();
    }
  }

  /**
   * Funzione che recupera la data di fine dell'ultima esecuzione della procedura anadata a buon fine
   * 
   * @return la data come date, null se non trovata
   */
  public Date getDataStop(String idLog) {
    String querySelect = "";
    ResultSet rs = null;
    Date data = null;
    try {
      querySelect = "SELECT STOP FROM LOG_ESPORTAZIONE WHERE ID = " + idLog + " AND STATO = 'FINISH'";
      rs = getStatement().executeQuery(querySelect);
      // CDTLogWrapper.logMessage(CDTLogWrapper.INFO,"Query per selezionare la data di stop dell'ultima esecuzione della procedura: "
      // + querySelect.toString());
      if (rs.next()) {
        data = rs.getTimestamp(1);
        rs.close();
        LogWrapper.logMessage(LogWrapper.INFO, "Data di stop dell'ultima esecuzione della procedura: " + data.toString());
      }
    }
    catch (Exception ex) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore nella ricerca della data di stop dell'ultima esecuzione della procedura (" + querySelect
          + "): " + ex.getMessage(), ex);
      idLog = "0";
    }
    return data;
  }
}
