package ext.sgv.export.cad;

import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import wt.epm.EPMDocument;
import wt.method.RemoteMethodServer;
import wt.util.WTException;
import wt.util.WTStandardDateFormat;
import ext.caditech.utility.Attributes;
import ext.caditech.utility.LogWrapper;
import ext.caditech.utility.SearchAttributes;
import ext.caditech.utility.SearchEPMDocument;
import ext.caditech.utility.SearchUtilsException;

/**
 * Classe per esportare gli attributi CAD
 * 
 * @author Administrator
 *
 */
public class ExportCadAttributes implements wt.method.RemoteAccess {

  // PROPERTIES:

  /** Modalit� di avvio della procedura di export [LATEST/DELTA] */
  private static final String MODO = "modo";

  /** modalit� di aggiornamento della struttura della tabella SQL [TRUNCATE/ALTER] */
  private static final String MODOSQL = "modoSql";

  /** flag per purge della tabella SQL al termine dell'export [S/N] */
  private static final String PURGE = "purge";

  /** flag per i test automatici, vale false sempre a parte per il lancio da test automatici */
  private static final String TEST = "test";

  /** data di inizio di ricerca dei documenti per il modo=LATEST */
  private static final String DATA_INIZIO = "dataInizio";

  /** intervallo espresso in giorni per la ricerca dei documenti per il modo=LATEST */
  private static final String INTERVALLO = "intervallo";

  // DEFAULT:

  /** Oggetto che specifica il formato che deve avere la data di inizio specificata dall'utente */
  private static final SimpleDateFormat date_format = new SimpleDateFormat("dd/MM/yyyy");

  /** default della data di inizio per il popolamento LATEST */
  private static final String DEFAULT_DATA_INIZIO = "01/01/1990";

  /** default dell'intervallo espresso in giorni per la suddivisione dei documenti per il popolamento LATEST */
  private static final int DEFAULT_INTERVALLO = 7;

  // CAMPI:
  /** variabile che vale true solo per i test automatici su trasfor-lab (usata per la connessione al db di test) */
  private boolean test = false;

  /** booleano che mi dice se sono o meno il server */
  static final boolean SERVER;

  /** Mappa con i parametri per la connessione al db */
  private HashMap<String, String> configsConnessione = null;

  /**
   * Restituisce la mappa con i dati per la connessione al DB, in caso di test automatici (quindi campo test = true) i
   * valori sono cablati e puntano al db su trasfor-lab, altrimenti i valori vengono letti dalle properties
   * ext.sgv.exportCadAttributes.db.* nel file di property di windchill
   * 
   * @return
   */
  public HashMap<String, String> getConnessione() {
    if (configsConnessione == null) {
      if (isTest()) {
        // caso test automatici, valori cablati che puntano al db su trasfor-lab
        configsConnessione = getConfigConnessioneLocale();
      }
      else {
        // leggo la configurazione di connessione al DB dal file di property di windchill
        configsConnessione = getConfigConnessioneDaProperties();
      }
    }
    LogWrapper.logMessage(LogWrapper.INFO, "Properties per la connessione al DB: ");
    // Stampo i dati per la connessione
    Set<String> set = configsConnessione.keySet();
    for (String cc : set) {
      LogWrapper.logMessage(LogWrapper.INFO, cc + " -> " + configsConnessione.get(cc));
    }
    return configsConnessione;
  }

  /** recupera se windchill � in modalit� server o meno */
  static {
    SERVER = RemoteMethodServer.ServerFlag;
  }

  public boolean isTest() {
    return test;
  }

  public void setTest(boolean test) {
    this.test = test;
  }

  /**
   * Esegue le operazioni di export selezionate, passate come property
   * 
   * @param p
   *          le operazioni da eseguire
   */
  public static void esegui(Properties p) {
    ExportCadAttributes esecutore = new ExportCadAttributes();

    // controlla che i parametri passati siano validi
    boolean argomentiOK = esecutore.controllaParametri(p);
    if (!argomentiOK) {
      LogWrapper.logMessage(LogWrapper.ERROR, "CDT: errore negli argomenti passati ");
      return;
    }

    esecutore.setTest((p.getProperty("test", "false")).equals("true"));
    LogWrapper.logMessage(LogWrapper.INFO, " Valore della variabile test -> " + esecutore.isTest());

    String nomeInternoCadDocument = SGVProperties.getProperty("ext.sgv.exportCadAttributes.nome.interno.cad", "ge.caditech.DefaultEPMDocument");

    // ricava tutti gli attributi dei CADDocument: mappa <nome attributo, tipo attributo java>
    LinkedHashMap<String, String> nomeAttributoTipoWC = SearchAttributes.getSoftAttributesType(nomeInternoCadDocument);

    // lista con i nomi degli attributi
    List<String> nomiAttributi = new ArrayList<String>(nomeAttributoTipoWC.keySet());

    nomeAttributoTipoWC.put("cartella", "java.lang.String");
    nomeAttributoTipoWC.put("nome", "java.lang.String");
    nomeAttributoTipoWC.put("revisione", "java.lang.String");
    nomeAttributoTipoWC.put("versione", "java.lang.Integer");
    nomeAttributoTipoWC.put("Livello fase di sviluppo", "java.lang.String");
    nomeAttributoTipoWC.put("data creazione", "java.lang.String");
    nomeAttributoTipoWC.put("oggetto*", "java.lang.String");

    // lista con i nomi degli attributi + i dati fissi come cartella, nome, revisione...
    List<String> nomiAttributiEdati = new ArrayList<String>(nomeAttributoTipoWC.keySet());

    // converte le tipologie degli attributi per SQL, restituisce "nome tipo"
    List<String> nomeAttributoTipoSQL = esecutore.convertiTipiWC2SQL(nomeAttributoTipoWC);

    // -------------------------------------------------------
    // Istanziamo l'oggetto per aggiornare il database
    // -------------------------------------------------------
    AggiornaTabellaDestinazione atb = new AggiornaTabellaDestinazione(esecutore.getConnessione());
    try {
      // Controllo la connessione al db
      atb.checkConnection();
    }
    catch (Exception e) {
      LogWrapper.logMessage(LogWrapper.ERROR, "CDT: errore nel collegamento al DB ", e);
      return;
    }
    // Recupero la modalita' con la quale effettuare la ricerca dei documenti
    String modo = p.getProperty(MODO);
    // creo la riga di log per l'esecuzione corrente della procedura
    atb.creaRigaLogInTabella(modo.toUpperCase());

    // ------------------------------------------------------
    // Aggiorna la struttura della tabella Esportazione
    // ------------------------------------------------------
    LogWrapper.logMessage(LogWrapper.INFO, "Aggiornamento della struttura della tabella Esportazione -> modoSql = " + p.getProperty(MODOSQL));
    boolean ripulisciDB = p.getProperty(MODOSQL, "ALTER").equalsIgnoreCase("TRUNCATE");
    try {
      atb.aggiornaTabella(ripulisciDB, nomeAttributoTipoSQL);
    }
    catch (Exception e) {
      LogWrapper.logMessage(LogWrapper.ERROR, "CDT: errore nell'aggiornamento della struttura della tabella Esportazione", e);
      e.printStackTrace();
      return;
    }

    // ------------------------------------------------------------------------------------------------------
    // Recupero gli EPMDocument, recupero il valore degli attributi e li inserisco nella tabella Esportazione
    // ------------------------------------------------------------------------------------------------------

    if (modo.equalsIgnoreCase("LATEST")) {
      // cerco tutti gli EPMDocument esistenti in Windchill in ultima release a partire da una certa data specificata
      // nella property "dataInizio" e ricercando in un intervallo di tempo definito dalla property "intervallo"
      try {
        esecutore.popolaEPMDocumentLatest(p, atb, nomiAttributi, nomiAttributiEdati, nomeAttributoTipoWC);
      }
      catch (SearchUtilsException e) {
        LogWrapper.logMessage(LogWrapper.ERROR, "CDT: errore nella ricerca LATEST dei documenti ", e);
        e.printStackTrace();
        return;
      }
      catch (ParseException e) {
        LogWrapper.logMessage(LogWrapper.ERROR,
            "CDT: errore nel parse della data di inizio ricerca, nella modalit� LATEST di ricerca dei documenti ", e);
        e.printStackTrace();
        return;
      }
    }
    else if (modo.equalsIgnoreCase("DELTA")) {
      // cerco tutti gli EPMDocument esistenti in Windchill in ultima release creati da una certa data in poi
      try {
        esecutore.popolaEPMDocumentDelta(atb, nomiAttributi, nomiAttributiEdati, nomeAttributoTipoWC);
      }
      catch (SearchUtilsException e) {
        LogWrapper.logMessage(LogWrapper.ERROR, "CDT: errore nella ricerca DELTA dei documenti ", e);
        e.printStackTrace();
        return;
      }
    }
    // -----------------------------------------
    // se devo eseguire il PURGE
    // -----------------------------------------
    boolean purgaDB = p.getProperty(PURGE, "N").equalsIgnoreCase("S");
    if (purgaDB) {
      atb.purge();
    }
    atb.finish();
  }

  private void popolaEPMDocumentDelta(AggiornaTabellaDestinazione atb, List<String> nomiAttributi, List<String> nomiAttributiEdati,
      LinkedHashMap<String, String> nomeAttributoTipoWC) throws SearchUtilsException {
    List<EPMDocument> epmDocumentList = new ArrayList<EPMDocument>();
    Date data = atb.getDataUltimaEsecuzione();
    if (data != null) {
      LogWrapper.logMessage(LogWrapper.INFO, "Tipo_run = delta -> cerco tutti i documenti con data di creazione maggiore/uguale: " + data);
      epmDocumentList = SearchEPMDocument.findLatestEPMDocuments(data, false);
      if (epmDocumentList.isEmpty()) {
        LogWrapper.logMessage(LogWrapper.INFO, "Nessun documento da inserire nel db");
        return;
      }
      LogWrapper.logMessage(LogWrapper.INFO, "Trovati " + epmDocumentList.size() + " EPMDcoument");
      this.recuperaValoreAttributiEpopola(epmDocumentList, atb, nomiAttributi, nomiAttributiEdati, nomeAttributoTipoWC);
    }
    else {
      LogWrapper.logMessage(LogWrapper.INFO,
          "Tipo_run = delta -> non � stato possibile recuperare la data dell'ultima esecuzione, non verr� inserito nessun file");
    }
    return;
  }

  private void popolaEPMDocumentLatest(Properties p, AggiornaTabellaDestinazione atb, List<String> nomiAttributi, List<String> nomiAttributiEdati,
      LinkedHashMap<String, String> nomeAttributoTipoWC) throws SearchUtilsException, ParseException {

    // Recupero data inizio e intervallo per la ricerca dei documenti
    String propIntervallo = p.getProperty(INTERVALLO);
    int intervallo;
    if (propIntervallo == null || propIntervallo.equals("")) {
      LogWrapper.logMessage(LogWrapper.INFO, "Non � stata specificata la property 'intervallo' quindi verr� usato il default -> "
          + DEFAULT_INTERVALLO + " giorni");
      intervallo = DEFAULT_INTERVALLO;
    }
    else {
      intervallo = Integer.parseInt(propIntervallo);
    }
    String propDataInizio = p.getProperty(DATA_INIZIO);
    String dataInizio = "";
    if (propDataInizio == null || propDataInizio.equals("")) {
      LogWrapper.logMessage(LogWrapper.INFO, "Non � stata specificata la property 'dataInizio' quindi verr� usato il default -> "
          + DEFAULT_DATA_INIZIO);
      dataInizio = DEFAULT_DATA_INIZIO;
    }
    else {
      dataInizio = propDataInizio;
    }

    LogWrapper.logMessage(LogWrapper.INFO, "Tipo_run = latest -> cerco tutti gli EPMDocument in ultima release a partire dalla data " + dataInizio
        + " suddividendo i documenti in intervalli di " + intervallo + " giorni");

    Calendar calendarInizio = Calendar.getInstance();
    calendarInizio.setTime(date_format.parse(dataInizio));

    Calendar calendarOggi = Calendar.getInstance();
    Calendar calendarFine = Calendar.getInstance();
    calendarFine.setTime(calendarInizio.getTime());
    calendarFine.add(Calendar.DAY_OF_MONTH, intervallo);

    int totaleDocsTrovati = 0;
    boolean end = false;

    while (!end) {
      if (calendarFine.after(calendarOggi)) {
        calendarFine.setTime(calendarOggi.getTime());
        end = true;
      }
      ArrayList<EPMDocument> epmDocs = SearchEPMDocument.findLatestEPMDocuments(calendarInizio.getTime(), calendarFine.getTime(), false);
      totaleDocsTrovati += epmDocs.size();
      LogWrapper.logMessage(LogWrapper.INFO, "Range date per la ricerca dei documenti -> [" + date_format.format(calendarInizio.getTime()) + "  -  "
          + date_format.format(calendarFine.getTime()) + "]");
      LogWrapper.logMessage(LogWrapper.INFO, "Trovati " + epmDocs.size() + " EPMDcoument");

      // Chiamo la funzione che recupera il valore degli attributi e li inserisce nella tabella Esportazione
      this.recuperaValoreAttributiEpopola(epmDocs, atb, nomiAttributi, nomiAttributiEdati, nomeAttributoTipoWC);

      // incremento l'intervallo di ricerca dei documenti
      calendarInizio.setTime(calendarFine.getTime());
      calendarFine.add(Calendar.DAY_OF_MONTH, intervallo);
    }
    System.out.println("-----------------------------------------");
    System.out.println("TOTALE  " + totaleDocsTrovati + " EPMDocument trovati");

  }

  /**
   * Cerca i valori degli attributi e dati (cartella, nome, versione...) per ogni EPMDocument della lista passata e
   * richiama il metodo {@link AggiornaTabellaDestinazione#popolaTabella(List, List, LinkedHashMap)} che inserisce i
   * valori degli attributi nella tabella Esportazione
   * 
   * @param epmDocumentList
   *          lista degli EPMDocument
   * @param atb
   *          istanza della classe {@link AggiornaTabellaDestinazione} per inserire gli attributi trovati nella tabella
   *          Esportazione
   * @param nomiAttributi
   *          lista con i nomi degli attributi
   * @param nomiAttributiEdati
   *          lista con i nomi degli attributi e dei dati
   * @param nomeAttributoTipoWC
   *          mappa <nome attributo> -> tipo dell'attributo in Windchill
   */
  private void recuperaValoreAttributiEpopola(List<EPMDocument> epmDocumentList, AggiornaTabellaDestinazione atb, List<String> nomiAttributi,
      List<String> nomiAttributiEdati, LinkedHashMap<String, String> nomeAttributoTipoWC) {

    // Struttura contentente i valori degli attributi di tutti i documenti da popolare
    List<List<String>> valoriAttributiEdati = new ArrayList<List<String>>();

    // ---------------------------------------------------------------------------
    // Recupero i valori degli attributi degli EPMDocument da inserire nel db
    // ---------------------------------------------------------------------------
    for (EPMDocument epm : epmDocumentList) {
      LogWrapper.logMessage(LogWrapper.INFO, "Cerco gli attributi per il documento: " + epm.getNumber() + " -versione:"
          + epm.getVersionIdentifier().getValue() + " -iterazione:" + epm.getIterationIdentifier().getValue());
      List<String> valoriEdati;
      try {
        valoriEdati = this.creaListaValoriEdati(epm, nomiAttributi);
        LogWrapper.logMessage(LogWrapper.INFO, "Attributi ->  " + valoriEdati.toString());
      }
      catch (Exception e) {
        LogWrapper.logMessage(LogWrapper.ERROR, "CDT: errore nella lettura del valore degli attributi per il documento " + epm.getNumber(), e);
        e.printStackTrace();
        return;
      }
      // aggiungo alla lista i valori degli attributi e i dati trovati
      valoriAttributiEdati.add(valoriEdati);
    }

    // ------------------------------------------------------
    // popolo la tabella con i valori degli attributi
    // ------------------------------------------------------
    try {
      atb.popolaTabella(nomiAttributiEdati, valoriAttributiEdati, nomeAttributoTipoWC);
    }
    catch (Exception e) {
      LogWrapper.logMessage(LogWrapper.ERROR, "CDT: errore nel popolamento della tabella Esportazione con documenti e rispettivi attributi", e);
      e.printStackTrace();
      return;
    }
  }

  /**
   * Recupera i valori degli attributi e dei dati (cartella, nome, versione ...) del docmuento passato come argomento
   * 
   * @param epm
   *          EPMDocument di cui cercare i valori degli attributi
   * @param nomiAttributi
   *          lista con i nomi degli attributi
   * @return la lista con i valori degli attributi e dati
   * @throws RemoteException
   *           in caso di errore nella ricerca del valore degli attributi
   * @throws WTException
   *           in caso di errore nella ricerca del valore degli attributi
   */
  private List<String> creaListaValoriEdati(EPMDocument epm, List<String> nomiAttributi) throws RemoteException, WTException {
    // ------------------------------------------------------
    // Cerco il valore degli attributi di epm
    // ------------------------------------------------------
    List<String> valori = Attributes.getIBAAttributesValues(epm, nomiAttributi);
    // ------------------------------------------------------
    // Aggiungiamo (cartella, nome,versione,iterazione, livello fase di sviluppo,data creazione, oggetto* ) che non
    // corrispondono ad attributi
    // windchill, ma a colonne fisse
    // ------------------------------------------------------
    // cartella
    String cartella = epm.getLocation();
    valori.add(cartella);
    // number
    String nome = epm.getNumber();
    valori.add(nome);
    // versione
    String revisione = epm.getVersionIdentifier().getValue();
    valori.add(revisione);
    // iterazione
    String versione = epm.getIterationIdentifier().getValue();
    valori.add(versione);
    // livello fase di sviluppo
    String faseDiSviluppo = epm.getLifeCycleState().toString();
    valori.add(faseDiSviluppo);
    // data di creazione
    Timestamp ts = epm.getModifyTimestamp();
    String dataCreazione = WTStandardDateFormat.format(new Date(ts.getTime()), "yyyy-MM-dd HH:mm:ss");
    valori.add(dataCreazione);
    // oggetto*
    String oggetto = Attributes.getIBAAttributeValue(epm, "OGGETTO");
    valori.add(oggetto);
    return valori;
  }

  /**
   * Dati per la connessione al db lette dalle properties ext.sgv.exportCadAttributes.db.* di Windchill
   * 
   * @return la mappa con i dati per la connessione
   */
  private HashMap<String, String> getConfigConnessioneDaProperties() {
    HashMap<String, String> configs = new HashMap<String, String>();
    String sql_url = SGVProperties.getProperty("ext.sgv.exportCadAttributes.db.url", "jdbc:jtds:sqlserver://server2008:1433/");
    configs.put("sql_url", sql_url);
    String sql_istanza = SGVProperties.getProperty("ext.sgv.exportCadAttributes.db.instance", "instance=MSSQLSERVER2008");
    configs.put("sql_istanza", sql_istanza);
    String nome_db = SGVProperties.getProperty("ext.sgv.exportCadAttributes.db.name", "SGV_export_intralink");
    configs.put("sql_dbname", nome_db);
    String sql_dbuser = SGVProperties.getProperty("ext.sgv.exportCadAttributes.db.user", "MDMAdmin");
    configs.put("sql_dbuser", sql_dbuser);
    String sql_dbpass = SGVProperties.getProperty("ext.sgv.exportCadAttributes.db.psw", "MDMAdmin");
    configs.put("sql_dbpass", sql_dbpass);
    String sql_table_name = SGVProperties.getProperty("ext.sgv.exportCadAttributes.db.table.name", "Esportazione");
    configs.put("sql_table_name", sql_table_name);
    return configs;
  }

  /**
   * Dati per la connessione al db locale per i test automatici
   * 
   * @return la mappa con i dati per la connessione
   */
  private HashMap<String, String> getConfigConnessioneLocale() {
    HashMap<String, String> configs = new HashMap<String, String>();
    String sql_url = "jdbc:jtds:sqlserver://localhost/";
    configs.put("sql_url", sql_url);
    String sql_istanza = "";
    configs.put("sql_istanza", sql_istanza);
    String nome_db = "SGV_export_intralink";
    configs.put("sql_dbname", nome_db);
    String sql_dbuser = "exportIntralink";
    configs.put("sql_dbuser", sql_dbuser);
    String sql_dbpass = "exportIntralink";
    configs.put("sql_dbpass", sql_dbpass);
    String sql_table_name = "Esportazione";
    configs.put("sql_table_name", sql_table_name);
    return configs;
  }

  /**
   * Converte le tipologie di dati Windchill in SQL
   * 
   * @param tabellaTipiWC
   *          la hashtable contenete l'associazione di < nome tipo, tipo dato > di windchill
   * @return la lista di stringhe formattate con "nomeColonna tipoColonna" per SQL
   */
  private List<String> convertiTipiWC2SQL(LinkedHashMap<String, String> tabellaTipiWC) {

    List<String> listaNomeTipoSQL = new ArrayList<String>();
    List<String> listaNomiAttributi = new ArrayList<String>(tabellaTipiWC.keySet());

    for (String nomeAttributo : listaNomiAttributi) {
      String tipo = tabellaTipiWC.get(nomeAttributo);

      if (tipo.equalsIgnoreCase("java.lang.String")) {
        // stringa
        listaNomeTipoSQL.add(nomeAttributo + " nvarchar(4000)");
      }
      else if (tipo.equalsIgnoreCase("java.lang.Double") || (tipo.equalsIgnoreCase("java.lang.Float"))) {
        // real
        listaNomeTipoSQL.add(nomeAttributo + " real");
      }
      else if (tipo.equalsIgnoreCase("java.lang.Long")) {
        // long
        listaNomeTipoSQL.add(nomeAttributo + " bigint");
      }
      else if (tipo.equalsIgnoreCase("java.lang.Integer")) {
        // intero
        listaNomeTipoSQL.add(nomeAttributo + " int");
      }
      else if (tipo.equalsIgnoreCase("java.lang.Boolean")) {
        // booleano
        listaNomeTipoSQL.add(nomeAttributo + " varchar(10)");
      }
      else if (tipo.equalsIgnoreCase("java.lang.Date")) {
        // datetime
        listaNomeTipoSQL.add(nomeAttributo + " datetime");
      }
      else {
        // tutti gli altri
        listaNomeTipoSQL.add(nomeAttributo + " nvarchar(4000)");
      }
    }

    return listaNomeTipoSQL;
  }

  /**
   * Controlla i parametri passati per l'export degli attributi dei file CAD
   * 
   * @param p
   *          la property con le coppie <chiave, valore>
   * @return <code>true<code> se le properties sono corrette, <code>false</code> altrimenti.
   */
  private boolean controllaParametri(Properties p) {
    boolean res = false;

    List<String> chiavi = new ArrayList<String>(Arrays.asList(MODO, MODOSQL, PURGE, TEST, INTERVALLO, DATA_INIZIO));
    Set<Object> sKeys = p.keySet();
    LogWrapper.logMessage(LogWrapper.INFO, "Controllo degli argomenti ", null);

    for (Object key : sKeys) {
      if (!chiavi.contains((String) key)) {
        // caso in cui una chiave passata � inesistente (dita a banana?)
        LogWrapper.logMessage(LogWrapper.ERROR, "CDT: il parametro " + key.toString() + " non � ammesso", null);
        return res;
      }
    }

    // caso nel quale non abbiamo passato il modo di funzionamento
    if (!sKeys.contains(MODO)) {
      LogWrapper.logMessage(LogWrapper.ERROR, "CDT: non abbiamo passato il modo di funzionamento", null);
      return res;
    }
    // controllo i valori passati

    String valore = p.getProperty(MODO);
    if (!valore.equalsIgnoreCase("LATEST") && !valore.equalsIgnoreCase("DELTA")) {
      LogWrapper.logMessage(LogWrapper.ERROR, "CDT: " + valore + " non � ammesso come valore dell'argomento " + MODO
          + ", i valori ammessi sono: LATEST o DELTA", null);
      return res;
    }
    if (sKeys.contains(MODOSQL)) {
      valore = p.getProperty(MODOSQL);
      if (!valore.equalsIgnoreCase("TRUNCATE") && !valore.equalsIgnoreCase("ALTER")) {
        LogWrapper.logMessage(LogWrapper.ERROR, "CDT: " + valore + " non � ammesso come valore dell'argomento " + MODOSQL
            + ", i valori ammessi sono: TRUNCATE o ALTER", null);
        return res;
      }
    }
    if (sKeys.contains(PURGE)) {
      valore = p.getProperty(PURGE);
      if (!valore.equalsIgnoreCase("S") && !valore.equalsIgnoreCase("N")) {
        LogWrapper.logMessage(LogWrapper.ERROR, "CDT: " + valore + " non � ammesso come valore dell'argomento " + PURGE
            + ", i valori ammessi sono: S o N", null);
        return res;
      }
    }

    if (p.get(MODO).equals("LATEST")) {

      if (!sKeys.contains(INTERVALLO)) {
        LogWrapper.logMessage(LogWrapper.INFO,
            "CDT: tra gli argomenti non � stato inserito l'intervallo di suddivisione dei documenti nella ricerca, quindi verr� usato il default = "
                + DEFAULT_INTERVALLO + " giorni");
      }
      if (!sKeys.contains(DATA_INIZIO)) {
        LogWrapper.logMessage(LogWrapper.INFO,
            "CDT: tra gli argomenti non � stato inserito la data di inizio per la ricerca dei documenti, quindi verr� usato il default = "
                + DEFAULT_DATA_INIZIO);
      }
      else {
        String data = p.getProperty(DATA_INIZIO);
        try {
          date_format.parse(data);
        }
        catch (ParseException e) {
          LogWrapper.logMessage(LogWrapper.ERROR, "CDT: errore nel parse della dataInizio passata come argomento", null);
          LogWrapper.logMessage(LogWrapper.ERROR, "CDT: la data deve avere formato " + date_format.toString(), null);
          return res;
        }
      }
    }
    // non ci sono stati errori
    res = true;
    LogWrapper.logMessage(LogWrapper.INFO, "Argomenti ok", null);
    return res;
  }

  /**
   * metodo statico per eseguire un'operazione come server. Argomenti
   * 
   * @param p
   *          le property
   * @throws Exception
   *           l'eccezione
   */
  public static void eseguiComeServer(Properties p) throws Exception {
    if (SERVER) {
      LogWrapper.logMessage(LogWrapper.DEBUG, "CDT-eseguiComeServer: sono server");
      esegui(p);
    }
    else {
      try {
        LogWrapper.logMessage(LogWrapper.DEBUG, "CDT-eseguiComeServer: non sono server");
        RemoteMethodServer method = RemoteMethodServer.getDefault();
        method.setUserName(SGVProperties.getProperty("ext.caditech.login.user", "wcadmin"));
        method.setPassword(SGVProperties.getProperty("ext.caditech.login.password", "wcadmin"));
        Class[] argumentTypes = { Properties.class };
        Object[] arguments = { p };
        method.invoke("esegui", "ext.sgv.export.cad.ExportCadAttributes", null, argumentTypes, arguments);
      }
      catch (InvocationTargetException itex) {
        Throwable throwable = itex.getTargetException();
        if (throwable instanceof WTException) {
          throw (WTException) throwable;
        }
        Object aobj[] = { "esegui" };
        throw new WTException(throwable, "wt.fc.fcResource", "0", aobj);
      }
      catch (RemoteException rex) {
        Object aobj[] = { "esegui" };
        throw new WTException(rex, "wt.fc.fcResource", "0", aobj);
      }
    }
  }

  /**
   * Main per il lancio della procedura di export
   * 
   * Argomenti:
   * 
   * modo: modalit� di avvio della procedura di export [LATEST/DELTA]
   * 
   * modoSql: modalit� di aggiornamento della struttura della tabella SQL [TRUNCATE/ALTER]
   * 
   * purge: flag per purge della tabella SQL al termine dell'export [S/N]
   * 
   * dataInizio: data di inizio di ricerca dei documenti per il modo=LATEST
   *
   * intervallo: intervallo espresso in giorni per la ricerca dei documenti per il modo=LATEST
   *
   * @param args
   *          gli argomenti passati
   * @throws Exception
   *           l'eccezione riscontrata
   */
  public static void main(String[] args) throws Exception {
    RemoteMethodServer method = RemoteMethodServer.getDefault();

    method.setUserName(SGVProperties.getProperty("ext.caditech.login.user", "wcadmin"));
    method.setPassword(SGVProperties.getProperty("ext.caditech.login.password", "wcadmin"));

    LogWrapper.logMessage(LogWrapper.DEBUG, "CDT Main ");
    Properties p = new Properties();

    // parsa le property
    for (int i = 0; i < args.length; i++) {
      String[] coppia = args[i].split("=");
      if (coppia.length != 2) {
        System.out.println("E' stata inserito un argomento non ben formattato: " + args[i]);
        System.out.println("Gli argomenti devono avere la forma : chiave=valore");
        System.exit(0);
      }
      String chiave = coppia[0];
      String valore = coppia[1];
      System.out.println("Inserita property: " + chiave + "-->" + valore);
      p.setProperty(chiave, valore);
    }

    System.out.println("Eseguo come server");
    eseguiComeServer(p);

    if (p.getProperty(TEST) != null && p.getProperty(TEST).equalsIgnoreCase("true")) {
      return;
    }

    System.exit(0);
  }
}
