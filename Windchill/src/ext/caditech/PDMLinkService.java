package ext.caditech;

import java.util.ArrayList;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import ext.caditech.checkin.CheckinPDMLinkException;
import ext.caditech.checkin.CheckinUtils;
import ext.caditech.checkout.CheckoutPDMLinkException;
import ext.caditech.checkout.CheckoutUtils;
import ext.caditech.state.StatePDMLinkException;
import ext.caditech.state.StateUtils;
import ext.caditech.utility.SearchEPMDocument;
import ext.caditech.utility.SearchUtils;
import ext.caditech.utility.SearchUtilsException;

@WebService()
public class PDMLinkService implements wt.method.RemoteAccess {

  static final String VERSION = "1.00.04";

  /**
   * SetStato richiama il metodo per settare lo stato di un oggetto
   * 
   * @param nomeModello
   *          nome del modello
   * @param nomeStato
   *          nome dello stato da impostare
   * @throws StatePDMLinkException
   */
  @WebMethod(operationName = "setStato")
  public void setStato(@WebParam(name = "nomeModello") String nomeModello, @WebParam(name = "nomeStato") String nomeStato)
      throws StatePDMLinkException {
    StateUtils.setStato(nomeModello, nomeStato);
  }

  /**
   * GetStato richiama il metodo per ricavare lo stato di un oggetto
   * 
   * @param nomeModello
   *          nome del modello di cui si vuole conoscere lo stato
   * @throws StatePDMLinkException
   * @return Stato
   */
  @WebMethod(operationName = "getStato")
  public String getStato(@WebParam(name = "nomeModello") String nomeModello) throws StatePDMLinkException {
    return StateUtils.getStato(nomeModello);
  }

  /**
   * EseguiCheckout richiama il metodo per l'esecuzione del checkout
   * 
   * 
   * @param modelli
   *          nomi dei modelli di cui effettuare il checkout separati da ;
   * @param workspace
   *          nome del workspace
   * @param nomeUtente
   *          nome dell'utente a cui deve essere assegnato il checkout
   * @throws CheckoutPDMLinkException
   * @throws SearchUtilsException
   * @return Lista dei file presenti nel workspace
   */
  @WebMethod(operationName = "eseguiCheckout")
  public ArrayList<String> eseguiCheckout(@WebParam(name = "modelli") String modelli, @WebParam(name = "workspace") String workspace,
      @WebParam(name = "nomeUtente") String nomeUtente) throws CheckoutPDMLinkException, SearchUtilsException {
    CheckoutUtils esecutore = new CheckoutUtils();
    return esecutore.eseguiCheckout(modelli, workspace, nomeUtente);
  }

  /**
   * EseguiCopiaLocale richiama il metodo per l'esecuzione della copia locale
   * 
   * @param p
   *          properties:
   *          <ul>
   *          <li>{@link CheckoutUtils#MODELLI}</li>
   *          <li>{@link CheckoutUtils#DRAWING}</li>
   *          <li>{@link CheckoutUtils#REPORT}</li>
   *          <li>{@link CheckoutUtils#AS_STORED}</li>
   *          <li>{@link CheckoutUtils#ELIMINA_WS}</li>
   *          <li>{@link CheckoutUtils#WORKSPACE}</li>
   *          <li>{@link CheckoutUtils#DIPENDENZE}</li>
   *          <li>{@link CheckoutUtils#LOGIN}</li>
   *          </ul>
   * @throws CheckoutPDMLinkException
   * @throws SearchUtilsException
   * @return Lista dei file presenti nel workspace
   */
  @WebMethod(operationName = "eseguiCopiaLocale")
  public ArrayList<String> eseguiCopiaLocale(@WebParam(name = "p") String p) throws CheckoutPDMLinkException, SearchUtilsException {
    CheckoutUtils esecutore = new CheckoutUtils();
    return esecutore.eseguiCopiaLocale(p);
  }

  /**
   * EseguiCheckin richiama il metodo per l'esecuzione del checkin
   * 
   * @param modelli
   *          nomi dei modelli separati da ";"
   * @throws CheckinPDMLinkException
   *           l'eccezione durante il checkin
   * @throws SearchUtilsException
   *           l'eccezione nella ricerca
   * @return un'array list di stringhe con i number degli oggetti per i quali ha fatto il checkin
   */
  @WebMethod(operationName = "eseguiCheckin")
  public ArrayList<String> eseguiCheckin(@WebParam(name = "nome") String nome) throws CheckinPDMLinkException, SearchUtilsException {
    CheckinUtils esecutore = new CheckinUtils();
    return esecutore.eseguiCheckin(nome);
  }

  /**
   * EseguiUndoCheckout richiama il metodo per l'esecuzione dell'operazione di undo checkout
   * 
   * @param modelli
   *          nomi dei modelli separati da ";"
   * @param workspace
   *          nome del workspace
   * @param nomeUtente
   *          login dell'owner del workspace
   * 
   * @throws SearchUtilsException
   *           l'eccezione nella ricerca
   * @return Lista dei file per i quali � stato eseguito l'undo checkout
   */
  @WebMethod(operationName = "eseguiUndoCheckout")
  public ArrayList<String> eseguiUndoCheckout(@WebParam(name = "modelli") String modelli, @WebParam(name = "workspace") String workspace,
      @WebParam(name = "nomeUtente") String nomeUtente) throws SearchUtilsException {
    CheckoutUtils esecutore = new CheckoutUtils();
    return esecutore.eseguiUndoCheckout(modelli, workspace, nomeUtente);
  }

  /**
   * FindWorkspaceContent richiama il metodo per l'esecuzione dell'operazione di findWorkspaceContent
   * 
   * @param workspace
   *          nome del workspace
   * @param nomeUtente
   *          login dell'owner del workspace
   * 
   * @throws SearchUtilsException
   *           l'eccezione nella ricerca
   * @return Lista dei file contenuti nel workspace
   */
  @WebMethod(operationName = "cercaContenutoWorkspace")
  public ArrayList<String> cercaContenutoWorkspace(@WebParam(name = "workspace") String workspace, @WebParam(name = "nomeUtente") String nomeUtente)
      throws SearchUtilsException {
    return SearchUtils.findWorkspaceContent(workspace, nomeUtente);
  }

  /**
   * Effettua la ricerca degli EPMDocument aventi l'attributo dato con il valore richiesto (ottimizzata dalla seconda
   * esecuzione con stesso nome attributo)
   * 
   * @param nomeAttributo
   *          nome dell'attributo
   * @param valoreAttributo
   *          valore dell'attributo
   * @return la lista dei nomi degli EPMDocument trovati
   * @throws SearchUtilsException
   *           in caso di errore nella ricerca
   */
  @WebMethod(operationName = "cercaDocumentoPerAttributo")
  public ArrayList<String> cercaDocumentoPerAttributo(@WebParam(name = "nomeAttributo") String nomeAttributo,
      @WebParam(name = "valoreAttributo") String valoreAttributo) throws SearchUtilsException {
    return SearchEPMDocument.findEPMDocumentByOptimizedAttribute(nomeAttributo, valoreAttributo);
  }

  /**
   * Controlla se il documento passato � in checkout.
   * 
   * @param nome
   *          nome del documento
   * @return true se il documento passato � in checkout, false altrimenti.
   * 
   * @throws SearchUtilsException
   *           in caso di errore
   */
  @WebMethod(operationName = "isCheckedOut")
  public boolean isCheckedOut(@WebParam(name = "nome") String nome) throws SearchUtilsException {
    return CheckoutUtils.isCheckedOut(nome);
  }

}
