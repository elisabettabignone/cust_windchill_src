package ext.caditech.checkin;

/**
 * Eccezione sollevata dal checkin Windchill
 * 
 *
 */
public class CheckinPDMLinkException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = 3006245088084893757L;

  public CheckinPDMLinkException(String msg) {
    super(msg);
  }

  public CheckinPDMLinkException(String msg, Throwable causa) {
    super(msg, causa);
  }

  public CheckinPDMLinkException(Throwable causa) {
    super(causa);
  }
}
