package ext.caditech.checkin;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import ext.caditech.checkout.CheckoutUtils;
import ext.caditech.utility.Attributes;
import ext.caditech.utility.SearchUtilsException;

public class CheckinUtils_IT {

  @Test
  /**
   * Esegue il test di checkin per il documento CILINDRO_PROVA.PRT
   */
  public void TestCheckin() {
    // esegue la copia locale
    System.out.println("\n------------------------------------------------------------");
    String args[] = { "test=true", "modelli=cilindro_prova.prt", "dipendenze=ALL", "workspace=ws_test_checkin", "eliminaws=true", "asstored=latest",
        "drawing=false", "login=Administrator", "modo=copia" };
    try {
      CheckoutUtils.main(args);
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }

    // esegue il checkout del documento
    System.out.println("\n------------------------------------------------------------");
    String args2[] = { "test=true", "modelli=cilindro_prova.prt", "workspace=ws_test_checkin", "login=Administrator", "modo=checkout" };
    try {
      CheckoutUtils.main(args2);
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }

    // controllo che sia in checkout
    try {
      assertTrue(CheckoutUtils.isCheckedOut("cilindro_prova.prt"));
    }
    catch (SearchUtilsException e) {
      e.printStackTrace();
      fail();
    }

    // modifica l'attributo
    System.out.println("\n------------------------------------------------------------");
    String args3[] = { "test=true", "modello=cilindro_prova.prt", "nome=CODICE", "valore=CODICE_CILINDRO" };
    try {
      Attributes.main(args3);
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }

    // esegue il checkin
    System.out.println("\n------------------------------------------------------------");
    String args4[] = { "test=true", "modelli=cilindro_prova.prt" };
    try {
      CheckinUtils.main(args4);
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }

    // controllo che sia libero dal checkout
    try {
      assertFalse(CheckoutUtils.isCheckedOut("cilindro_prova.prt"));
    }
    catch (SearchUtilsException e) {
      e.printStackTrace();
      fail();
    }
  }
}
