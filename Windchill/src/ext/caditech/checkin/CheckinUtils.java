package ext.caditech.checkin;

import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import wt.epm.EPMDocument;
import wt.method.RemoteMethodServer;
import wt.util.WTException;
import wt.vc.wip.WorkInProgressHelper;
import wt.vc.wip.Workable;
import ext.caditech.checkout.CheckoutPDMLinkException;
import ext.caditech.utility.CDTProperties;
import ext.caditech.utility.LogWrapper;
import ext.caditech.utility.SearchEPMDocument;
import ext.caditech.utility.SearchUtilsException;

/**
 * 
 * Classe che si occupa di effettuare il checkin in Windchill di una serie di modelli
 * 
 * @author l.lusuardi
 * @author m.giudici
 *
 */
public class CheckinUtils implements wt.method.RemoteAccess {

  /** la versione dell'utility */
  private static final String VERSION = "1.00.00";

  /** flag per sapere se sono il server */
  static final boolean SERVER;

  /** il nome della classe */
  static final String CLASSNAME = "ext.caditech.checkin.CheckinUtils";

  /** L'username con cui autenticarsi in Windchill */
  static private String user = "";

  /** La password con cui autenticarsi in Windchill */
  static private String password = "";

  /** La lista dei modelli per i quali fare il checkin */
  public static final String MODELLI = "modelli";

  /** flag per i test automatici, vale false sempre a parte per il lancio da test automatici */
  private static final String TEST = "test";

  /** setta il flag SERVER */
  static {
    SERVER = RemoteMethodServer.ServerFlag;
  }

  /**
   * esegue le operazioni per il checkin
   * 
   * @param listaModelli
   *          lista dei nomi dei modelli separati da ; di cui eseguire il checkin
   * @throws CheckinPDMLinkException
   *           l'eccezione durante il checkin
   * @throws SearchUtilsException
   *           l'eccezione nella ricerca
   * @return un'array list di stringhe con i number degli oggetti per i quali ha fatto il checkin
   */
  public ArrayList<String> eseguiCheckin(String listaModelli) throws CheckinPDMLinkException, SearchUtilsException {
    ArrayList<String> checkinDocs = new ArrayList<String>();

    LogWrapper.logMessage(LogWrapper.INFO, "-----------------------------------------------------------");
    LogWrapper.logMessage(LogWrapper.INFO, "--------------------- CHECKIN " + CheckinUtils.VERSION + " ---------------------");
    LogWrapper.logMessage(LogWrapper.INFO, "-----------------------------------------------------------");

    if (listaModelli == null || "".equals(listaModelli)) {
      throw new CheckinPDMLinkException("Nessun modello passato in input!");
    }

    // scorre la lista dei modelli passata
    List<String> modelli = Arrays.asList(listaModelli.split(";"));
    for (String modello : modelli) {
      EPMDocument epm = SearchEPMDocument.findLatestEPMDocuments(modello);
      if (epm != null) {
        // recupero il number per tracciare di quale oggetto faccio il checkin
        String empNumber = epm.getNumber();

        try {
          // TODO: c'� anche una fuzione di checkin che prende una collezione di workable, con nota e folder...
          WorkInProgressHelper.service.checkin((Workable) epm, "");
          LogWrapper.logMessage(LogWrapper.INFO, "Check-in dell'oggetto: " + empNumber);
          checkinDocs.add(empNumber);
        }
        catch (Exception e) {
          LogWrapper.logMessage(LogWrapper.ERROR, "Errore nella fase di check-in per l'oggetto: " + empNumber, e);
        }
      }
    }

    return checkinDocs;
  }

  /**
   * Esegue il checkout leggendo un file di Properties con tutte le informazioni necessarie
   * 
   * @param p
   *          il file di properties
   * @throws WTException
   *           l'eccezione sul checkin
   * @throws SearchUtilsException
   *           l'eccezione durante la ricerca
   * @throws CheckoutPDMLinkException
   *           in caso di errore
   */
  private static void eseguiCheckinComeServer(Properties p) throws CheckinPDMLinkException, WTException, SearchUtilsException {
    if (SERVER) {
      checkinWindchillShell(p);
    }
    else {
      try {
        RemoteMethodServer method = RemoteMethodServer.getDefault();
        method.setUserName(user);
        method.setPassword(password);
        Class[] argumentTypes = { Properties.class };
        Object[] arguments = { p };
        method.invoke("checkinWindchillShell", "ext.caditech.checkin.CheckinUtils", null, argumentTypes, arguments);
      }
      catch (InvocationTargetException e) {
        LogWrapper.logMessage(LogWrapper.ERROR, "Errore invocando eseguiCheckinServer", e);
        throw new CheckinPDMLinkException("Errore invocando eseguiCheckinServer", e);
      }
      catch (RemoteException e) {
        LogWrapper.logMessage(LogWrapper.ERROR, "Errore invocando eseguiCheckinServer", e);
        throw new CheckinPDMLinkException("Errore invocando eseguiCheckinServer", e);
      }
    }
  }

  /**
   * il main
   * 
   * @param args
   *          i parametri passati per il checkin
   */
  public static void main(String[] args) throws Exception {
    Properties p = new Properties();

    List<String> chiavi = new ArrayList<String>(Arrays.asList(MODELLI, TEST));

    for (int i = 0; i < args.length; i++) {
      String[] coppia = args[i].split("=");
      if (coppia.length != 2) {
        System.out.println("E' stata inserito un argomento non ben formattato: " + args[i]);
        System.out.println("Gli argomenti devono avere la forma : chiave=valore");
        System.exit(0);
      }
      String chiave = coppia[0];
      String valore = coppia[1];
      if (!chiavi.contains(chiave)) {
        System.out.println("E' stata inserito un argomento non riconosciuto: " + chiave);
        System.exit(0);
      }
      System.out.println("Inserita property: " + chiave + "-->" + valore);
      p.setProperty(chiave, valore);
    }

    user = CDTProperties.getProperty("ext.caditech.login.user", "wcadmin");
    password = CDTProperties.getProperty("ext.caditech.login.password", "wcadmin");
    RemoteMethodServer method = RemoteMethodServer.getDefault();
    method.setUserName(user);
    method.setPassword(password);
    System.out.println("Sto per fare il checkin di " + p.getProperty("modelli"));
    eseguiCheckinComeServer(p);

    // chiudo la consolle
    if (p.getProperty(TEST) != null && p.getProperty(TEST).equalsIgnoreCase("true")) {
      return;
    }
    System.exit(0);
  }

  /**
   * esegue il checkin da windchill shell
   * 
   * @param p
   *          le property
   * @throws CheckinPDMLinkException
   *           l'eccezione sul checkin
   * @throws WTException
   *           l'eccezione sul documento
   * @throws SearchUtilsException
   *           l'eccezione durante la ricerca
   */
  public static void checkinWindchillShell(Properties p) throws CheckinPDMLinkException, WTException, SearchUtilsException {
    CheckinUtils esecutore = new CheckinUtils();
    // esegue il checkin
    try {
      esecutore.eseguiCheckin(p.getProperty(MODELLI));
    }
    catch (Exception e) {
      LogWrapper.logMessage(LogWrapper.ERROR, e.getMessage(), e);
      throw new CheckinPDMLinkException("Errore durante l'esecuzione del checkin", e);
    }
  }
}
