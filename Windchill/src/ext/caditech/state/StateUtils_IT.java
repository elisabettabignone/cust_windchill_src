package ext.caditech.state;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

//Running test cases in order of method names in ascending order
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class StateUtils_IT {

  @BeforeClass
  public static void setUp() {

  }

  // test:
  // 1) verifico che il modello sia in stato INWORK
  // 2) cambio lo stato in RELEASED e poi verifico che lo stato sia RELEASED
  // 3) provo a cambiare lo stato in LOCKED (non lo pu� fare perch� quello stato non appartiene al lifecycle) e verifico
  // che lo stato sia ancora RELEASED
  // 4) cambio lo stato in INWORK e verifico che lo stato sia INWORK

  @Test
  public void test_01_GetStato() {

    try {
      String stato = StateUtils.getStato("01430000.asm");
      assertTrue(stato.equalsIgnoreCase("INWORK"));
    }
    catch (StatePDMLinkException e) {
      e.printStackTrace();
      fail();
    }

  }

  @Test
  public void test_02_SetReleased() {
    try {

      StateUtils.setStato("01430000.asm", "RELEASED");
      String stato = StateUtils.getStato("01430000.asm");
      assertTrue(stato.equalsIgnoreCase("RELEASED"));
    }
    catch (StatePDMLinkException e) {
      e.printStackTrace();
      fail();
    }
  }

  @Test
  public void test_03_SetLocked() {
    try {
      StateUtils.setStato("01430000.asm", "LOCKED");
      fail();
    }
    catch (StatePDMLinkException e) {
      String stato = "";
      try {
        stato = StateUtils.getStato("01430000.asm");
      }
      catch (StatePDMLinkException e1) {
        fail();
      }
      assertTrue(stato.equalsIgnoreCase("RELEASED"));
    }
  }

  @Test
  public void test_04_SetInWork() {
    try {
      StateUtils.setStato("01430000.asm", "INWORK");
    }
    catch (StatePDMLinkException e1) {
      e1.printStackTrace();
      fail();
    }
    try {
      String stato = StateUtils.getStato("01430000.asm");
      assertTrue(stato.equalsIgnoreCase("INWORK"));
    }
    catch (StatePDMLinkException e) {
      e.printStackTrace();
      fail();
    }
  }

}
