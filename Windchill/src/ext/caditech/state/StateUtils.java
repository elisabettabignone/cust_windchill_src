package ext.caditech.state;

import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

import wt.epm.EPMDocument;
import wt.lifecycle.LifeCycleHelper;
import wt.lifecycle.LifeCycleTemplate;
import wt.lifecycle.State;
import wt.method.RemoteMethodServer;
import ext.caditech.utility.CDTProperties;
import ext.caditech.utility.LogWrapper;
import ext.caditech.utility.SearchEPMDocument;
import ext.caditech.utility.SearchUtilsException;

public class StateUtils implements wt.method.RemoteAccess {

  static final boolean SERVER;

  /** L'username con cui autenticarsi in Windchill */
  static private String user = "";

  /** La password con cui autenticarsi in Windchill */
  static private String password = "";

  /** Modello sul quale effettuare l'azione */
  private final static String MODELLO = "modello";

  /**
   * Lo stato da impostare (richiesto solo se AZIONE=SET), � il valore della chiave dello stato in PDMLINK (quella
   * maiuscola)
   */
  private final static String STATO = "stato";

  /** Azione da eseguire sul modello, pu� essere GET o SET (obbligatorio) */
  private final static String AZIONE = "azione";

  static {
    SERVER = RemoteMethodServer.ServerFlag;
  }

  /**
   * 
   * @param p
   * @throws StatePDMLinkException
   */
  private static void eseguiAzioneComeServer(Properties p) throws StatePDMLinkException {
    if (SERVER) {
      eseguiAzioneWindchillShell(p);
    }
    else {
      try {
        RemoteMethodServer method = RemoteMethodServer.getDefault();
        method.setUserName(user);
        method.setPassword(password);
        Class[] argumentTypes = { Properties.class };
        Object[] arguments = { p };
        method.invoke("eseguiAzioneWindchillShell", "ext.caditech.state.StateUtils", null, argumentTypes, arguments);
      }
      catch (InvocationTargetException e) {
        LogWrapper.logMessage(LogWrapper.ERROR, "Errore invocando eseguiAzioneServer", e);
        throw new StatePDMLinkException("Errore invocando eseguiAzioneComeServer", e);
      }
      catch (RemoteException e) {
        LogWrapper.logMessage(LogWrapper.ERROR, "Errore invocando eseguiAzioneServer", e);
        throw new StatePDMLinkException("Errore invocando eseguiAzioneComeServer", e);
      }
    }
  }

  /**
   * * Esegue il cambio di stato
   * 
   * @param nomeModello
   *          nome del modello
   * @param nomeStato
   *          nome dello stato da impostare
   * @throws StatePDMLinkException
   */
  public static void setStato(String nomeModello, String nomeStato) throws StatePDMLinkException {
    LogWrapper.logMessage(LogWrapper.INFO, "-----------------------------------------------------------");
    LogWrapper.logMessage(LogWrapper.INFO, "----------------- AZIONE CAMBIO DI STATO ------------------");
    LogWrapper.logMessage(LogWrapper.INFO, "-----------------------------------------------------------");

    // recupero il documento sul quale eseguire l'azione
    EPMDocument doc = null;
    try {
      doc = SearchEPMDocument.findLatestEPMDocuments(nomeModello);
    }
    catch (SearchUtilsException e) {
      String msg = "Errore cercando il documento con nome " + nomeModello;
      LogWrapper.logMessage(LogWrapper.ERROR, msg);
      throw new StatePDMLinkException(msg, e);
    }

    LogWrapper.logMessage(LogWrapper.INFO, " Documento trovato : " + doc.getNumber() + " -versione: " + doc.getVersionIdentifier().getValue()
        + " -iterazione: " + doc.getIterationIdentifier().getValue());
    // esegue il cambio di stato
    // azione di set stato
    Vector states = null;
    boolean esisteStato = false;
    boolean isLocked = doc.isLocked();

    try {
      if (doc != null && !isLocked) {
        String lifeCycleName = doc.getLifeCycleState().toString();
        LogWrapper.logMessage(LogWrapper.INFO, "Lo stato del modello e': " + lifeCycleName);

        LogWrapper.logMessage(LogWrapper.INFO, "Sto per fare la set dello stato '" + nomeStato + "' per il modello " + nomeModello);

        LifeCycleTemplate lct = (LifeCycleTemplate) doc.getLifeCycleTemplate().getObject();
        states = LifeCycleHelper.service.findStates(lct);
        for (Object object : states) {
          if (nomeStato.equalsIgnoreCase(((State) object).toString())) {
            LifeCycleHelper.service.setLifeCycleState(doc, State.toState(nomeStato));
            esisteStato = true;
            break;
          }
        }
      }
    }
    catch (Exception e) {
      String msg = "Errore nel cambio di stato ";
      LogWrapper.logMessage(LogWrapper.ERROR, msg, e);
      throw new StatePDMLinkException(msg, e);
    }

    if (isLocked) {
      // modello in check-out
      String msg = "Il modello e' LOCKED, quindi non e' possibile cambiare lo stato";
      LogWrapper.logMessage(LogWrapper.ERROR, msg);
      throw new StatePDMLinkException(msg);
    }
    else if (!esisteStato) {
      // stato non valido
      String msg = "Lo stato '" + nomeStato + "' non esiste per il lifecycle associato al modello " + nomeModello;
      LogWrapper.logMessage(LogWrapper.ERROR, msg);
      throw new StatePDMLinkException(msg);
    }
  }

  /**
   * Esegue la lettura dello stato
   * 
   * @param nomeModello
   *          nome del modello di cui si vuole conoscere lo stato
   * @return lo stato in forma di Stringa (equivalente alla "Key" di Windchill)
   * @throws StatePDMLinkException
   *           l'eccezione sul cambio di stato
   */
  public static String getStato(String nomeModello) throws StatePDMLinkException {

    LogWrapper.logMessage(LogWrapper.INFO, "-----------------------------------------------------------");
    LogWrapper.logMessage(LogWrapper.INFO, "------------------ AZIONE LETTURA STATO -------------------");
    LogWrapper.logMessage(LogWrapper.INFO, "-----------------------------------------------------------");

    // recupero il documento sul quale eseguire l'azione
    EPMDocument doc = null;

    try {
      doc = SearchEPMDocument.findLatestEPMDocuments(nomeModello);
    }
    catch (SearchUtilsException e) {
      String msg = "Errore cercando il documento con nome " + nomeModello;
      LogWrapper.logMessage(LogWrapper.ERROR, msg);
      throw new StatePDMLinkException(msg, e);
    }
    LogWrapper.logMessage(LogWrapper.INFO, " Documento trovato : " + doc.getNumber() + " -versione: " + doc.getVersionIdentifier().getValue()
        + " -iterazione: " + doc.getIterationIdentifier().getValue());
    // azione di get stato
    String lifeCycleName = doc.getLifeCycleState().toString();
    LogWrapper.logMessage(LogWrapper.INFO, "Lo stato del modello e': " + lifeCycleName);

    return lifeCycleName;
  }

  /**
   * il main per testare il cambio stato
   * 
   * @param args
   *          gli argomenti per il cambio di stato
   */
  public static void main(String[] args) throws Exception {
    // StateUtils esecutore = new StateUtils();

    Properties p = new Properties();
    List<String> chiavi = new ArrayList<String>(Arrays.asList(MODELLO, STATO, AZIONE));

    for (int i = 0; i < args.length; i++) {
      String[] coppia = args[i].split("=");
      if (coppia.length != 2) {
        System.out.println("E' stata inserito un argomento non ben formattato: " + args[i]);
        System.out.println("Gli argomenti devono avere la forma : chiave=valore");
        System.exit(0);
      }
      String chiave = coppia[0];
      String valore = coppia[1];
      if (!chiavi.contains(chiave)) {
        System.out.println("E' stata inserito un argomento non riconosciuto: " + chiave);
        System.exit(0);
      }
      System.out.println("Inserita property: " + chiave + "-->" + valore);
      p.setProperty(chiave, valore);
    }

    // imposta user e pass
    user = CDTProperties.getProperty("ext.caditech.login.user", "wcadmin");
    password = CDTProperties.getProperty("ext.caditech.login.password", "wcadmin");

    // imposta il method server remoto
    RemoteMethodServer method = RemoteMethodServer.getDefault();
    method.setUserName(user);
    method.setPassword(password);

    // esegue l'azione scelta
    eseguiAzioneComeServer(p);
    System.exit(0);
  } // end main

  /**
   * esegue l'azione per il cambio di stato da windchill shell
   * 
   * @param p
   *          le properties
   * 
   * @throws StatePDMLinkException
   *           l'eccezione sul cambio di stato
   */
  public static void eseguiAzioneWindchillShell(Properties p) throws StatePDMLinkException {
    if (p.getProperty(AZIONE).equalsIgnoreCase("GET")) {
      StateUtils.getStato(p.getProperty(MODELLO));
    }
    else {
      StateUtils.setStato(p.getProperty(MODELLO), p.getProperty(STATO));
    }
  }
}
