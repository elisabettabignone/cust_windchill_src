package ext.caditech.state;

/**
 * Eccezione sollevata dal gestore dello stato
 * 
 * @author l.lusuardi
 * @author m.giudici
 *
 */
public class StatePDMLinkException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public StatePDMLinkException(String msg) {
    super(msg);
  }

  public StatePDMLinkException(String msg, Throwable causa) {
    super(msg, causa);
  }

  public StatePDMLinkException(Throwable causa) {
    super(causa);
  }
}
