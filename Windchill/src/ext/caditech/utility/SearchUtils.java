package ext.caditech.utility;

import java.util.ArrayList;

import wt.enterprise.RevisionControlled;
import wt.epm.EPMDocConfigSpec;
import wt.epm.EPMDocument;
import wt.epm.workspaces.EPMWorkspace;
import wt.epm.workspaces.EPMWorkspaceHelper;
import wt.fc.ObjectReference;
import wt.fc.PersistenceHelper;
import wt.fc.QueryResult;
import wt.fc.collections.WTArrayList;
import wt.fc.collections.WTSet;
import wt.inf.container.WTContainerHelper;
import wt.inf.container.WTContainerRef;
import wt.org.WTUser;
import wt.part.WTPartConfigSpec;
import wt.query.QuerySpec;
import wt.query.SearchCondition;
import wt.util.WTException;
import wt.vc.wip.WorkInProgressHelper;
import wt.vc.wip.Workable;

public class SearchUtils {

  /**
   * Ricerca il container leggendo le info necessarie dalle {@link CDTProperties}
   *
   * @return il container trovato
   * @throws SearchUtilsException
   *           in caso di errore
   */
  public static WTContainerRef findContainer() throws SearchUtilsException {
    LogWrapper.logMessage(LogWrapper.INFO, "Ricerca WTContainerRef per nome leggendo le properties");
    String containerName = "";
    String containerType = "";
    String productName = "";
    containerName = CDTProperties.getProperty("ext.caditech.checkout.containerName", "Demo Organization");
    containerType = CDTProperties.getProperty("ext.caditech.checkout.containerType", "wt.pdmlink.PDMLinkProduct");
    productName = CDTProperties.getProperty("ext.caditech.checkout.productName", "GENERIC_COMPUTER");

    LogWrapper.logMessage(LogWrapper.INFO, "Cerco il container di nome: " + containerName + " - tipo: " + containerType + " - prodotto: "
        + productName);
    WTContainerRef containerRef = null;
    try {
      containerRef = WTContainerHelper.service.getByPath("/wt.inf.container.OrgContainer=" + containerName + "/" + containerType + "=" + productName);
    }
    catch (WTException ex) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore cercando il container", ex);
      throw new SearchUtilsException("Errore cercando il container", ex);
    }
    return containerRef;
  }

  /**
   * Effettua la ricerca di un utente per nome
   *
   * @param userName
   *          nome dell'utente da cercare
   * @return WTUser o <code>null</code> se non viene trovato
   * @throws SearchUtilsException
   */
  @SuppressWarnings("deprecation")
  public static WTUser findUser(String userName) throws SearchUtilsException {
    LogWrapper.logMessage(LogWrapper.INFO, "Ricerca WTUser per nome -> " + userName);
    WTUser user = null;
    try {
      QueryResult qr = null;
      QuerySpec qs = new QuerySpec(WTUser.class);
      SearchCondition sc = new SearchCondition(WTUser.class, WTUser.NAME, SearchCondition.EQUAL, userName);
      qs.appendWhere(sc);
      qr = PersistenceHelper.manager.find(qs);
      if (qr == null || !qr.hasMoreElements()) {
        LogWrapper.logMessage(LogWrapper.WARN, "Nessun utente con name = " + userName);
        return user;
      }
      while (qr.hasMoreElements()) {
        user = (WTUser) qr.nextElement();
      }
    }
    catch (Exception ex) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore nel metodo findUser (" + userName + ")", ex);
      throw new SearchUtilsException("Errore nel metodo findUser (" + userName + ")", ex);
    }
    return user;
  }

  /**
   * ANCORA NON FUNZIONANTE
   *
   * @param epmw_find
   * @return
   * @throws SearchUtilsException
   */
  public static boolean svuotaWorkspace(EPMWorkspace epmw_find) throws SearchUtilsException {
    boolean res = true;
    try {
      LogWrapper.logMessage(LogWrapper.INFO, "Il workspace � da svuotare");
      // se gli oggetti sono in checkout viene dato errore, quindi usiamo l'undo-checkout prima di rimuoverli
      WTSet oggettiWS = EPMWorkspaceHelper.manager.getObjectsInWorkspace(epmw_find, EPMDocument.class);
      System.out.println("Oggetti nel WS prima dell'undo: " + oggettiWS.size());

      WTArrayList tutti = new WTArrayList();
      for (Object on : oggettiWS) {
        ObjectReference epmref = (ObjectReference) on;
        EPMDocument epm = (EPMDocument) epmref.getObject();

        // boolean b = WorkInProgressHelper.isCheckedOut(epm);
        // boolean ist = epm.isInstance();
        if (epm.getNumber().equalsIgnoreCase("istanza_nuova.prt")) {
          System.out.println("istanza nuova fuori dalla robba di checkout");
          tutti.add(epm);
        }
        else {
          System.out.println("aggiunto " + epm.getNumber() + " alla robba di checkout");
          tutti.add(epm);
        }
      }

      // // aggiungo tutte le istanze
      // for (Object on : istanzaList) {
      // ObjectReference epmref = (ObjectReference) on;
      // EPMDocument epm = (EPMDocument) epmref.getObject();
      // boolean b = WorkInProgressHelper.isCheckedOut(epm);
      // boolean ist = epm.isInstance();
      // // System.out.println("- " + epm.getNumber() + " --> checkedout:" + b + " --> istanza:" + ist);
      // tutti.add(epmref);
      // }
      // // aggiungo il generico
      // tutti.add(genericoref);
      //
      // for (Object on : tutti) {
      // ObjectReference epmref = (ObjectReference) on;
      // EPMDocument epm = (EPMDocument) epmref.getObject();
      // boolean b = WorkInProgressHelper.isCheckedOut(epm);
      // boolean ist = epm.isInstance();
      // System.out.println("- " + epm.getNumber() + " --> checkedout:" + b + " --> istanza:" + ist);
      //
      // }

      // EPMFamilyTableHelper.manager.
      System.out.println("undo checkout di " + tutti.toString());

      // EPMWorkspaceHelper.manager.undoCheckout(epmw_find, tutti);
      // EPMWorkspaceHelper.manager.undoCheckout(tutti);
      // EPMWorkspaceHelper.manager.undoCheckoutAndRestoreInWorkspace(epmw_find, tutti);

      // EPMWorkspaceHelper.manager.removeFromWorkspace(epmw_find, tutti);
      // try {
      // WorkInProgressHelper.service.undoCheckouts(tutti);
      // }
      // catch (WTPropertyVetoException e) {
      // // TODO Auto-generated catch block
      // e.printStackTrace();
      // }

      // WTSet oggettiNuovi = EPMWorkspaceHelper.manager.getNewObjects(epmw_find);
      // for (Object on : oggettiNuovi) {
      // ObjectReference epmref = (ObjectReference) on;
      // EPMDocument epm = (EPMDocument) epmref.getObject();
      // CDTLogWrapper.logMessage(CDTLogWrapper.INFO, "Oggetto nuovo nel WS: " + epm.getNumber());
      // }
      // EPMWorkspaceHelper.manager.removeFromWorkspace(epmw_find, oggettiNuovi);
    }
    catch (WTException e) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore nello svuotamento del workspace " + epmw_find.getName(), e);
      throw new SearchUtilsException("Errore nello svuotamento del workspace " + epmw_find.getName(), e);
    }
    // catch (WTPropertyVetoException e) {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // }
    return res;
  }

  /**
   * Cerca il workspace, se non esiste ne crea uno nuovo
   *
   * @param workspaceName
   *          il nome del ws da cercare/creare
   * @param user
   *          l'utente proprietario del ws
   * @param containerRef
   *          il container dove cercare/creare il ws
   * @param daSvuotare
   *          se <code>true</code> svuota il ws, se <code>false</code> effettua il popolamento senza svuotare il ws
   * @return il ws creato o <code>null</code> se non viene creato nessun ws
   * @throws SearchUtilsException
   *           in caso di errore nella ricerca/creazione del ws
   */
  public static EPMWorkspace findWorkspace(String workspaceName, WTUser user, WTContainerRef containerRef, boolean daSvuotare)
      throws SearchUtilsException {
    LogWrapper.logMessage(LogWrapper.INFO, "Ricerca EPMWorkspace con nome " + workspaceName + " per l'utente " + user.getFullName()
        + " nel container " + containerRef.getName());
    EPMWorkspace epmw_find = null;
    try {
      WTSet workspaces = EPMWorkspaceHelper.manager.getWorkspaces(user, containerRef);
      if (workspaces == null || workspaces.isEmpty()) {
        LogWrapper.logMessage(LogWrapper.INFO, "Non � stato trovato nessun workspace");
        // return null;
      }
      LogWrapper.logMessage(LogWrapper.INFO, "WORKSPACE trovati: ");
      for (Object obj : workspaces) {
        ObjectReference or = (ObjectReference) obj;
        EPMWorkspace epmw = (EPMWorkspace) or.getObject();
        LogWrapper.logMessage(LogWrapper.INFO, "-" + epmw.getName());
        if (epmw.getName().equals(workspaceName)) {
          epmw_find = epmw;
          break;
        }
      }

      // --------------------------------
      // CREO IL WS
      // --------------------------------
      if (epmw_find == null) {
        // --------------------------------------------------------
        // Non esiste un workspace col nome cercato allora ne creo uno nuovo
        // --------------------------------------------------------
        try {
          LogWrapper.logMessage(LogWrapper.INFO, "Provo a creare il workspace " + workspaceName);
          // WTPartConfigSpec wtpcfs = WTPartHelper.service.findWTPartConfigSpec();
          WTPartConfigSpec wtpcfs = WTPartConfigSpec.newWTPartConfigSpec();
          EPMDocConfigSpec epmcfs = EPMDocConfigSpec.newEPMDocConfigSpec();
          // EPMWorkspaceHelper.manager.createTransparentWorkspace(arg0, arg1, arg2)
          epmw_find = EPMWorkspace.newEPMWorkspace(workspaceName, user, null, wtpcfs, epmcfs, containerRef.getReferencedContainer());
          PersistenceHelper.manager.save(wtpcfs);
          PersistenceHelper.manager.save(epmw_find);
          LogWrapper.logMessage(LogWrapper.INFO, "Workspace " + workspaceName + " � stato creato e salvato ");

        }
        catch (WTException e) {
          LogWrapper.logMessage(LogWrapper.ERROR, "Errore nella creazione del workspace ", e);
          throw new SearchUtilsException("Errore nella creazione del workspace ", e);
        }
      }
      else {
        // --------------------------------
        // SVUOTO IL WS
        // --------------------------------
        // -----------------------------------------------------------------------------
        // altrimenti il workspace esiste gi� e lo svuoto o meno in base alla property eliminaWS
        // -----------------------------------------------------------------------------
        if (daSvuotare) {
          try {
            LogWrapper.logMessage(LogWrapper.INFO, "Il workspace � da svuotare");

            WTSet oggettiWS = EPMWorkspaceHelper.manager.getObjectsInWorkspace(epmw_find, RevisionControlled.class);
            WTArrayList toUndoCheckout = new WTArrayList();
            // per ogni file presente nel workspace, verifico se � in checkout da parte dell'utente corrente e, nel
            // caso, lo aggiunge alla lista di cui fare Undo checkout
            for (Object object : oggettiWS) {
              EPMDocument epmDocument = (EPMDocument) ((ObjectReference) object).getObject();
              if (WorkInProgressHelper.isCheckedOut((Workable) epmDocument, user)) {
                LogWrapper.logMessage(LogWrapper.INFO, "Undo checkout per il file " + epmDocument.getNumber());
                toUndoCheckout.add(epmDocument);
                // WTArrayList documento = new WTArrayList();
                // documento.add(object);
                // EPMWorkspaceHelper.manager.undoCheckout(epmw_find, documento);

              }
            }
            EPMWorkspaceHelper.manager.undoCheckoutAndRestoreInWorkspace(epmw_find, toUndoCheckout);
            EPMWorkspaceHelper.manager.removeFromWorkspace(epmw_find, oggettiWS);

            // TODO: chiamare la nuova funzione che svuota il workspace, quando sar� funzionante
          }
          catch (WTException e) {
            LogWrapper.logMessage(LogWrapper.ERROR, "Errore nello svuotamento del workspace " + epmw_find.getName(), e);
            throw new SearchUtilsException("Errore nello svuotamento del workspace " + epmw_find.getName(), e);
          }
          LogWrapper.logMessage(LogWrapper.INFO, "Workspace " + epmw_find.getName() + " svuotato");
        }
        else {
          LogWrapper.logMessage(LogWrapper.INFO, "Il workspace non � da svuotare");
        }
      }
    }
    catch (WTException ex) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore cercando il workspace", ex);
      throw new SearchUtilsException("Errore cercando il workspace", ex);
    }
    return epmw_find;
  }

  /**
   * Cerca il contenuto di un workspace
   *
   * @param workspaceName
   *          il nome del ws da cercare
   * @param userName
   *          il nome dell'utente proprietario del ws
   * 
   * @return il contenuto del workspace o <code>null</code> se non viene trovato il ws
   * @throws SearchUtilsException
   *           in caso di errore nella ricerca del ws o del suo contenuto
   */
  public static ArrayList<String> findWorkspaceContent(String workspaceName, String userName) throws SearchUtilsException {

    WTContainerRef containerRef = findContainer();
    WTUser user = findUser(userName);
    LogWrapper.logMessage(LogWrapper.INFO, "Ricerca EPMWorkspace con nome " + workspaceName + " per l'utente " + user.getFullName()
        + " nel container " + containerRef.getName());
    EPMWorkspace epmw_find = null;
    ArrayList<String> contenutoWs = new ArrayList<String>();
    try {
      WTSet workspaces = EPMWorkspaceHelper.manager.getWorkspaces(user, containerRef);
      if (workspaces == null || workspaces.isEmpty()) {
        LogWrapper.logMessage(LogWrapper.INFO, "Non � stato trovato nessun workspace");
        // return null;
      }
      LogWrapper.logMessage(LogWrapper.INFO, "WORKSPACE trovati: ");
      for (Object obj : workspaces) {
        ObjectReference or = (ObjectReference) obj;
        EPMWorkspace epmw = (EPMWorkspace) or.getObject();
        LogWrapper.logMessage(LogWrapper.INFO, "-" + epmw.getName());
        if (epmw.getName().equals(workspaceName)) {
          epmw_find = epmw;
          break;
        }
      }

      // --------------------------------
      // se il vorkspace non esiste ritorno null
      // --------------------------------

      if (epmw_find == null) {
        LogWrapper.logMessage(LogWrapper.INFO, "Il workspace " + workspaceName + " non esiste");
        return null;
      }

      else {
        // -----------------------------------------------------------------------------
        // altrimenti se il workspace esiste cerco il suo contenuto
        // -----------------------------------------------------------------------------

        WTSet oggettiWS = EPMWorkspaceHelper.manager.getObjectsInWorkspace(epmw_find, RevisionControlled.class);
        for (Object object : oggettiWS) {
          EPMDocument epmDocument = (EPMDocument) ((ObjectReference) object).getObject();
          contenutoWs.add(epmDocument.getNumber());
        }
      }
    }
    catch (WTException ex) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore cercando il workspace o il suo contenuto", ex);
      throw new SearchUtilsException("Errore cercando il workspace o il suo contenuto", ex);
    }
    return contenutoWs;
  }
}
