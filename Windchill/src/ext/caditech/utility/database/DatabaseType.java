/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ext.caditech.utility.database;

/**
 *
 * @author d.petta
 */
public enum DatabaseType {
    Undefined, SQLServer, Access, SQLServerLocalDB
}
