package ext.caditech.utility.database;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author d.petta
 */
public class DBAccessCDT_SQLServer extends DBAccessCDT {

  public DBAccessCDT_SQLServer(String connectionString) throws ExceptionCDT {
    super(connectionString);
  }

  public DBAccessCDT_SQLServer(String connectionString, int idx) throws ExceptionCDT {
    super(connectionString, idx);
  }

  /*
   * (non-Javadoc)
   * 
   * @see it.caditech.JBaseLibrary.model.DBAccessCDT#createConnection(java.lang.String)
   */
  @Override
  public void createConnection(String connectionString) throws SQLException {
    connection = DriverManager.getConnection(connectionString);
  }

  /*
   * (non-Javadoc)
   * 
   * @see it.caditech.JBaseLibrary.model.DBAccessCDT#createCommand()
   */
  @Override
  public void createCommand() throws SQLException {
    command = connection.createStatement();
  }

  /*
   * (non-Javadoc)
   * 
   * @see it.caditech.JBaseLibrary.model.DBAccessCDT#getCommand()
   */
  @Override
  public Statement getCommand() throws SQLException {
    return connection.createStatement();
  }

  static {
    try {
      Class.forName("net.sourceforge.jtds.jdbc.Driver");
    }
    catch (Exception ex) {
      System.out.println("Problem to load SQL Server Driver class!");
      ex.printStackTrace();
    }
  }
}
