/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ext.caditech.utility.database;

/**
 *
 * @author d.petta
 */
public class DBAccessCDTManager {

  static private final int MAX_CONNECTION = 5;

  static private String[] connectionsString;

  static private DatabaseType[] connectionsType;

  static private DBAccessCDT[] connections = null;

  static public boolean[] connectionsUsed;

  static {
    connectionsString = new String[MAX_CONNECTION];
    connectionsType = new DatabaseType[MAX_CONNECTION];
    connections = new DBAccessCDT[MAX_CONNECTION];
    connectionsUsed = new boolean[MAX_CONNECTION];
    for (int ii = 0; ii < MAX_CONNECTION; ii++) {
      connectionsString[ii] = "";
      connectionsType[ii] = DatabaseType.Undefined;
      connections[ii] = null;
      connectionsUsed[ii] = false;
    }
  }

  static public void initConnection(DatabaseType connectionType, String connectionString) {
    initConnection(connectionType, connectionString, 0);
  }

  static public void initConnection(DatabaseType connectionType, String connectionString, int index) {
    connectionsString[index] = connectionString;
    connectionsType[index] = connectionType;
  }

  static public void addConnection(DatabaseType connectionType, String connectionString, int index) {
    connectionsString[index] = connectionString;
    connectionsType[index] = connectionType;
  }

  static DBAccessCDT createConnection() throws ExceptionCDT {
    return createConnection(0);
  }

  static DBAccessCDT createConnection(int index) throws ExceptionCDT {
    if (connectionsType[index] == DatabaseType.Undefined) {
      throw new ExceptionCDT("Internal Error: connection type not defined!");
    }
    if (connectionsString[index] == "") {
      throw new ExceptionCDT("Internal Error: connection string not defined!");
    }
    if (connectionsType[index] == DatabaseType.SQLServer) {
      return new DBAccessCDT_SQLServer(connectionsString[index], index);
    }
    else if (connectionsType[index] == DatabaseType.SQLServerLocalDB) {
      return new DBAccessCDT_SQLServer(connectionsString[index], index);
    }
    else {
      throw new ExceptionCDT("Internal Error: connection type not managed!");
    }
  }

  static public DBAccessCDT getConnection() throws ExceptionCDT {
    return getConnection(0);
  }

  static public DBAccessCDT getConnection(int index) throws ExceptionCDT {
    if (connections[index] == null) {
      connections[index] = createConnection(index);
    }

    connectionsUsed[index] = true;
    return connections[index];
  }

  static public void releaseConnection() {
    releaseConnection(0);
  }

  static public void releaseConnection(int index) {
    connectionsUsed[index] = false;
  }
}
