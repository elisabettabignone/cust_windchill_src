/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ext.caditech.utility.database;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author d.petta
 */
public abstract class DBAccessCDT {

  protected int index;

  public int getIndex() {
    return index;
  }

  public void setIndex(int index) {
    this.index = index;
  }

  protected Connection connection;

  public Connection getConnection() {
    return connection;
  }

  protected void setConnection(Connection connection) {
    this.connection = connection;
  }

  protected Statement command;

  public Statement getThisCommand() {
    return command;
  }

  public void setCommand(Statement command) {
    this.command = command;
  }

  public DBAccessCDT(String connectionString) throws ExceptionCDT {
    this(connectionString, 0);
  }

  public DBAccessCDT(String connectionString, int idx) throws ExceptionCDT {
    if (connectionString == null || connectionString.equals("")) {
      throw new ExceptionCDT("Non � stata impostata la stringa di connessione al DB!");
    }
    try {
      createConnection(connectionString);
    }
    catch (SQLException e) {
      throw new ExceptionCDT(e);
    }
    try {
      createCommand();
    }
    catch (SQLException e) {
      throw new ExceptionCDT(e);
    }
    index = idx;
  }

  public abstract void createConnection(String connectionString) throws SQLException;

  public abstract void createCommand() throws SQLException;

  public abstract Statement getCommand() throws SQLException;

  public void release() {
    DBAccessCDTManager.connectionsUsed[index] = false;
  }

  public void close() throws SQLException {
    connection.close();
  }
}
