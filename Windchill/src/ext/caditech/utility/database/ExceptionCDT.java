/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ext.caditech.utility.database;

/**
 *
 * @author d.petta
 */
public class ExceptionCDT extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = -5593865365776863688L;

  public ExceptionCDT(String msg) {
    super(msg);
  }

  public ExceptionCDT(String msg, Throwable causa) {
    super(msg, causa);
  }

  public ExceptionCDT(Throwable causa) {
    super(causa);
  }
}
