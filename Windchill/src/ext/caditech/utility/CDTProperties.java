package ext.caditech.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

import wt.util.WTProperties;

//import ext.sgv.utility.Vetri.properties.WTPropertiesFile;

public class CDTProperties {

  private static Properties props;

  static {
    try {
      props = WTProperties.getLocalProperties();
      props.load(new FileInputStream(new File(props.getProperty("wt.codebase.location") + File.separator + "ext" + File.separator + "caditech"
          + File.separator + "caditech.properties")));
    }
    catch (Exception ex) {
      System.out.println("ERROR: problemi a leggere i file di property!");
      ex.printStackTrace();
    }
  }

  public static String getProperty(String nameProp) throws IOException {
    if (!props.containsKey(nameProp)) {
      throw new IOException("Proprieta' [" + nameProp + "] non presente!");
    }
    return props.getProperty(nameProp);
  }

  public static String getProperty(String nameProp, String defaultValue) {
    try {
      return getProperty(nameProp);
    }
    catch (IOException ioe) {
      return defaultValue;
    }
  }

  public static boolean getProperty(String nameProp, boolean defaultValue) {
    try {
      String prop = getProperty(nameProp);
      return Boolean.valueOf(prop);
    }
    catch (IOException ioe) {
      return defaultValue;
    }
    catch (Exception ex) {
      return defaultValue;
    }
  }

  /**
   * Controlla se la property voluta � null
   * 
   * @param p
   *          le properties
   * @param keyToCheck
   *          la chiave da controllare
   * @return <code>true</code> se la chiave non ha un valore associato, <code>false</code> altrimenti
   */
  public static boolean isPropertyNull(Properties p, String keyToCheck) {
    return (p.getProperty(keyToCheck) == null);
  }

  // -----

  public static void main(String s[]) {
    Enumeration en = props.keys();
    while (en.hasMoreElements()) {
      String nameProp = en.nextElement().toString();
      if (s.length == 0) {
        System.out.println("Property [" + nameProp + "] = [" + props.getProperty(nameProp) + "]");
      }
      else {
        if (nameProp.startsWith(s[0]))
          System.out.println("Property [" + nameProp + "] = [" + props.getProperty(nameProp) + "]");
      }
    }
  }

}
