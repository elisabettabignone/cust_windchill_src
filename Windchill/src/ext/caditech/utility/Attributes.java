package ext.caditech.utility;

import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import wt.epm.EPMDocument;
import wt.iba.definition.litedefinition.AttributeDefDefaultView;
import wt.iba.definition.litedefinition.BooleanDefView;
import wt.iba.definition.litedefinition.FloatDefView;
import wt.iba.definition.litedefinition.IntegerDefView;
import wt.iba.definition.litedefinition.StringDefView;
import wt.iba.definition.service.IBADefinitionHelper;
import wt.iba.definition.service.StandardIBADefinitionService;
import wt.iba.value.DefaultAttributeContainer;
import wt.iba.value.IBAHolder;
import wt.iba.value.litevalue.AbstractValueView;
import wt.iba.value.litevalue.BooleanValueDefaultView;
import wt.iba.value.litevalue.FloatValueDefaultView;
import wt.iba.value.litevalue.IntegerValueDefaultView;
import wt.iba.value.litevalue.StringValueDefaultView;
import wt.iba.value.service.IBAValueDBServiceInterface;
import wt.iba.value.service.IBAValueHelper;
import wt.method.RemoteMethodServer;
import wt.services.ManagerServiceFactory;
import wt.util.WTException;
import wt.util.WTPropertyVetoException;

public class Attributes implements wt.method.RemoteAccess {

  static final boolean SERVER;

  /** L'username con cui autenticarsi in Windchill */
  static private String user = "";

  /** La password con cui autenticarsi in Windchill */
  static private String password = "";

  /** nome del modello per il quale settare un attributo */
  private static final String MODELLO = "modello";

  /** nome dell'attributo da valorizzare */
  private static final String NOMEATTRIBUTO = "nome";

  /** nuovo valore dell'attributo */
  private static final String VALOREATTRIBUTO = "valore";

  /** la modalit� di esecuzione dell'operazione sugli attributi (SET o GET) */
  private static final String MODO = "modo";

  /** flag per i test automatici, vale false sempre a parte per il lancio da test automatici */
  private static final String TEST = "test";

  static {
    SERVER = RemoteMethodServer.ServerFlag;
  }

  /**
   * Valorizza l'attributo per il documento passato
   * 
   * @param epmDocument
   *          il documento nel quale valorizzare l'attributo
   * @param theAttribute
   *          il nome dell'attributo da valorizzare
   * @param theValue
   *          il valore da settare
   * @return l'ibaHolder con l'attributo modificato
   * @throws WTException
   *           l'eccezione
   */
  public static IBAHolder setIBAAttributeValue(EPMDocument epmDocument, String theAttribute, String theValue) throws WTException {
    // This method will detect the attribute type automatically and redirect
    // to the right method.
    // If the attribute does not exist, it will do nothing...
    IBAHolder ibaHolder = null;

    try {
      ibaHolder = (IBAHolder) epmDocument;
      DefaultAttributeContainer attributeContainer = (DefaultAttributeContainer) ibaHolder.getAttributeContainer();
      ibaHolder = IBAValueHelper.service.refreshAttributeContainer(ibaHolder, null, null, null);
      attributeContainer = (DefaultAttributeContainer) ibaHolder.getAttributeContainer();
      if (attributeContainer == null) {
        return null;
      }

      // Get attribute definition
      StandardIBADefinitionService defService = new StandardIBADefinitionService();
      AttributeDefDefaultView attributeDefinition = null;
      attributeDefinition = defService.getAttributeDefDefaultViewByPath(theAttribute);

      if (attributeDefinition == null) {
        System.out.println("Attribute: " + theAttribute + " is not defined into the system. Nothing is done.");
        return ibaHolder;
      }
      else {
        IBAHolder res = null;

        if (attributeDefinition instanceof StringDefView) {
          res = setStringIBAValue(epmDocument, theAttribute, theValue);
        }
        else if (attributeDefinition instanceof FloatDefView) {
          res = setFloatIBAValue(epmDocument, theAttribute, theValue);
        }
        else if (attributeDefinition instanceof IntegerDefView) {
          res = setIntegerIBAValue(epmDocument, theAttribute, theValue);
        }
        else if (attributeDefinition instanceof BooleanDefView) {
          res = setBooleanIBAValue(epmDocument, theAttribute, theValue);
        }
        else {
          System.out.println("Attribute: " + theAttribute + " is an instance of a not expected type. Nothing is done.");
          return ibaHolder;
        }

        return updateIBA(res);
      }
    }
    catch (RemoteException rem) {
      throw new WTException(rem);
    }
  }

  /**
   * Valorizza l'attributo di tipo stringa per il documento passato
   * 
   * @param epmDocument
   *          il documento nel quale valorizzare l'attributo
   * @param theAttribute
   *          il nome dell'attributo da valorizzare
   * @param theValue
   *          il valore da settare
   * @return l'ibaHolder con l'attributo modificato
   * @throws WTException
   *           l'eccezione
   */
  private static IBAHolder setStringIBAValue(EPMDocument epmDocument, String theAttribute, String theValue) throws WTException {
    IBAHolder ibaHolder = null;

    try {
      // get attribute container
      ibaHolder = (IBAHolder) epmDocument;
      DefaultAttributeContainer attributeContainer = (DefaultAttributeContainer) ibaHolder.getAttributeContainer();
      // if (PersistenceHelper.isPersistent(theObject))
      if (attributeContainer == null) {
        ibaHolder = IBAValueHelper.service.refreshAttributeContainer(ibaHolder, null, null, null);
        attributeContainer = (DefaultAttributeContainer) ibaHolder.getAttributeContainer();
      }

      // Get attribute definition
      // StandardIBADefinitionService defService = new
      // StandardIBADefinitionService();

      AttributeDefDefaultView attributeDefinition = null;
      StringDefView stringAttrDefinition = null;
      // if (logger.isDebugEnabled()) {
      // logger.debug("[setStringIBAValue] theAttribute ="
      // + theAttribute);
      // }
      attributeDefinition = IBADefinitionHelper.service.getAttributeDefDefaultViewByPath(theAttribute);
      // if (logger.isDebugEnabled()) {
      // logger.debug("[setStringIBAValue] attributeDefinition ="
      // + attributeDefinition);
      // }

      if (!(attributeDefinition instanceof StringDefView)) {
        throw new WTException("IBA " + theAttribute + " is not of type String");
      }

      stringAttrDefinition = (StringDefView) attributeDefinition;

      // Check if the attribute is already defined
      AbstractValueView[] abstractValueView = null;
      abstractValueView = attributeContainer.getAttributeValues(stringAttrDefinition);
      if (abstractValueView.length == 0) {
        // Add new attribute value
        StringValueDefaultView attrValue = new StringValueDefaultView(stringAttrDefinition, theValue);
        attributeContainer.addAttributeValue(attrValue);

      }
      else {
        // Update current attribute value
        StringValueDefaultView attrValue = (StringValueDefaultView) abstractValueView[0];
        attrValue.setValue(theValue);
        attributeContainer.updateAttributeValue(attrValue);
      }
      System.out.println("---> Updated attribute container = " + attributeContainer);

      // Update IBAHolder
      ibaHolder.setAttributeContainer(attributeContainer);
    }
    catch (RemoteException rem) {
      throw new WTException(rem);
    }
    catch (WTPropertyVetoException wpve) {
      throw new WTException(wpve);
    }

    // Return IBAHolder
    return ibaHolder;
  }

  /**
   * Valorizza l'attributo di tipo reale per il documento passato
   * 
   * @param epmDocument
   *          il documento nel quale valorizzare l'attributo
   * @param theAttribute
   *          il nome dell'attributo da valorizzare
   * @param theValue
   *          il valore da settare
   * @return l'ibaHolder con l'attributo modificato
   * @throws WTException
   *           l'eccezione
   */
  private static IBAHolder setFloatIBAValue(EPMDocument epmDocument, String theAttribute, String theValue) throws WTException {
    IBAHolder ibaHolder = null;
    theValue = theValue.trim();
    double theFloatValue = 0.0D;
    theFloatValue = Double.valueOf(theValue).doubleValue();
    // theFloatValue = WTPartIbaTool.toDouble(theValue);

    try {
      // get attribute container
      ibaHolder = (IBAHolder) epmDocument;
      DefaultAttributeContainer attributeContainer = (DefaultAttributeContainer) ibaHolder.getAttributeContainer();

      if (attributeContainer == null) {
        ibaHolder = IBAValueHelper.service.refreshAttributeContainer(ibaHolder, null, null, null);
        attributeContainer = (DefaultAttributeContainer) ibaHolder.getAttributeContainer();
      }

      // Get attribute definition
      StandardIBADefinitionService defService = new StandardIBADefinitionService();
      AttributeDefDefaultView attributeDefinition = null;
      FloatDefView floatAttrDefinition = null;
      attributeDefinition = defService.getAttributeDefDefaultViewByPath(theAttribute);
      if (!(attributeDefinition instanceof FloatDefView)) {
        throw new WTException("IBA " + theAttribute + " is not of type Float");
      }

      floatAttrDefinition = (FloatDefView) attributeDefinition;

      // Check if the attribute is already defined
      AbstractValueView[] abstractValueView = null;
      abstractValueView = attributeContainer.getAttributeValues(floatAttrDefinition);
      if (abstractValueView.length == 0) {
        // Add new attribute value
        @SuppressWarnings("deprecation")
        FloatValueDefaultView attrValue = new FloatValueDefaultView(floatAttrDefinition, theFloatValue,
            wt.clients.widgets.NumericToolkit.countSigFigs(theValue));
        attributeContainer.addAttributeValue(attrValue);
      }
      else {
        // Update current attribute value
        FloatValueDefaultView attrValue = (FloatValueDefaultView) abstractValueView[0];
        attrValue.setValue(theFloatValue);
        attributeContainer.updateAttributeValue(attrValue);
      }

      // Update IBAHolder
      ibaHolder.setAttributeContainer(attributeContainer);
    }
    catch (RemoteException rem) {
      throw new WTException(rem);
    }
    catch (WTPropertyVetoException wpve) {
      throw new WTException(wpve);
    }

    // Return IBAHolder
    return ibaHolder;
  }

  /**
   * Valorizza l'attributo di tipo intero per il documento passato
   * 
   * @param epmDocument
   *          il documento nel quale valorizzare l'attributo
   * @param theAttribute
   *          il nome dell'attributo da valorizzare
   * @param theValue
   *          il valore da settare
   * @return l'ibaHolder con l'attributo modificato
   * @throws WTException
   *           l'eccezione
   */
  private static IBAHolder setIntegerIBAValue(EPMDocument epmDocument, String theAttribute, String theValue) throws WTException {
    IBAHolder ibaHolder = null;
    theValue = theValue.trim();
    int theIntegerValue = 0;
    theIntegerValue = Integer.valueOf(theValue).intValue();

    try {
      // get attribute container
      ibaHolder = (IBAHolder) epmDocument;
      DefaultAttributeContainer attributeContainer = (DefaultAttributeContainer) ibaHolder.getAttributeContainer();

      if (attributeContainer == null) {
        ibaHolder = IBAValueHelper.service.refreshAttributeContainer(ibaHolder, null, null, null);
        attributeContainer = (DefaultAttributeContainer) ibaHolder.getAttributeContainer();
      }

      // Get attribute definition
      StandardIBADefinitionService defService = new StandardIBADefinitionService();
      AttributeDefDefaultView attributeDefinition = null;
      IntegerDefView integerAttrDefinition = null;
      attributeDefinition = defService.getAttributeDefDefaultViewByPath(theAttribute);
      if (!(attributeDefinition instanceof IntegerDefView)) {
        throw new WTException("IBA " + theAttribute + " is not of type Integer");
      }

      integerAttrDefinition = (IntegerDefView) attributeDefinition;

      // Check if the attribute is already defined
      AbstractValueView[] abstractValueView = null;
      abstractValueView = attributeContainer.getAttributeValues(integerAttrDefinition);
      if (abstractValueView.length == 0) {
        // Add new attribute value
        IntegerValueDefaultView attrValue = new IntegerValueDefaultView(integerAttrDefinition, theIntegerValue);
        attributeContainer.addAttributeValue(attrValue);
      }
      else {
        // Update current attribute value
        IntegerValueDefaultView attrValue = (IntegerValueDefaultView) abstractValueView[0];
        attrValue.setValue(theIntegerValue);
        attributeContainer.updateAttributeValue(attrValue);
      }

      // Update IBAHolder
      ibaHolder.setAttributeContainer(attributeContainer);
    }
    catch (RemoteException rem) {
      throw new WTException(rem);
    }
    catch (WTPropertyVetoException wpve) {
      throw new WTException(wpve);
    }

    // Return IBAHolder
    return ibaHolder;
  }

  /**
   * Valorizza l'attributo di tipo booleano per il documento passato
   * 
   * @param epmDocument
   *          il documento nel quale valorizzare l'attributo
   * @param theAttribute
   *          il nome dell'attributo da valorizzare
   * @param theValue
   *          il valore da settare
   * @return l'ibaHolder con l'attributo modificato
   * @throws WTException
   *           l'eccezione
   */
  private static IBAHolder setBooleanIBAValue(EPMDocument epmDocument, String theAttribute, String theValue) throws WTException {
    IBAHolder ibaHolder = null;
    boolean theBooleanValue = false;
    theBooleanValue = Boolean.valueOf(theValue).booleanValue();

    try {
      // get attribute container
      ibaHolder = (IBAHolder) epmDocument;
      DefaultAttributeContainer attributeContainer = (DefaultAttributeContainer) ibaHolder.getAttributeContainer();
      if (attributeContainer == null) {
        ibaHolder = IBAValueHelper.service.refreshAttributeContainer(ibaHolder, null, null, null);
        attributeContainer = (DefaultAttributeContainer) ibaHolder.getAttributeContainer();
      }

      // Get attribute definition
      StandardIBADefinitionService defService = new StandardIBADefinitionService();
      AttributeDefDefaultView attributeDefinition = null;
      BooleanDefView booleanAttrDefinition = null;
      attributeDefinition = defService.getAttributeDefDefaultViewByPath(theAttribute);
      if (!(attributeDefinition instanceof BooleanDefView)) {
        throw new WTException("IBA " + theAttribute + " is not of type Boolean");
      }

      booleanAttrDefinition = (BooleanDefView) attributeDefinition;

      // Check if the attribute is already defined
      AbstractValueView[] abstractValueView = null;
      abstractValueView = attributeContainer.getAttributeValues(booleanAttrDefinition);
      if (abstractValueView.length == 0) {
        // Add new attribute value
        BooleanValueDefaultView attrValue = new BooleanValueDefaultView(booleanAttrDefinition, theBooleanValue);
        attributeContainer.addAttributeValue(attrValue);
      }
      else {
        // Update current attribute value
        BooleanValueDefaultView attrValue = (BooleanValueDefaultView) abstractValueView[0];
        attrValue.setValue(theBooleanValue);
        attributeContainer.updateAttributeValue(attrValue);
      }

      // Update IBAHolder
      ibaHolder.setAttributeContainer(attributeContainer);
    }
    catch (RemoteException rem) {
      throw new WTException(rem);
    }
    catch (WTPropertyVetoException wpve) {
      throw new WTException(wpve);
    }

    // Return IBAHolder
    return ibaHolder;
  }

  /**
   * Aggiorna l'IBAHolder nel quale sono stati modificati degli attributi
   * 
   * @param obj
   *          l'ibaHolder
   * @return l'ibaHolder aggiornato
   * @throws WTException
   *           l'eccezione
   */
  private static IBAHolder updateIBA(IBAHolder obj) throws WTException {
    // save values of IBAs

    IBAValueDBServiceInterface dbService;

    wt.services.Manager manager = ManagerServiceFactory.getDefault().getManager(wt.iba.value.service.IBAValueDBService.class);

    if (manager == null) {

      throw new WTException("Cannot get manager for IBAValueDBService");

    }

    dbService = (IBAValueDBServiceInterface) manager;

    DefaultAttributeContainer attrContainer = (DefaultAttributeContainer) obj.getAttributeContainer();

    // attrContainer = (DefaultAttributeContainer) dbService
    // .updateAttributeContainer(obj,
    // attrContainer != null ? attrContainer
    // .getConstraintParameter() : null, null, null);

    Object obj2 = null;
    if (attrContainer != null) {
      obj2 = attrContainer.getConstraintParameter();
      attrContainer = (DefaultAttributeContainer) dbService.updateAttributeContainer(obj, obj2, null, null);
    }
    else {
      attrContainer = (DefaultAttributeContainer) dbService.updateAttributeContainer(obj, null, null, null);
    }

    return obj;
  }

  /**
   * Recupera il valore di un attributo passato, per il documento dato
   *
   * @param epmDocument
   *          documento di cui cercare l'attributo
   * @param attribute
   *          nome dell'attributo
   * @return il valore dell'attributo se � stato trovato
   * @throws WTException
   *           errore nella ricerca dell'attributo
   * @throws RemoteException
   *           errore nella ricerca dell'attributo
   */
  public static String getIBAAttributeValue(EPMDocument epmDocument, String attribute) throws RemoteException, WTException {
    LogWrapper.logMessage(LogWrapper.DEBUG, "CDT: cerco il valore dell'attributo " + attribute);
    String valore = "";
    IBAHolder ibaHolder = (IBAHolder) epmDocument;
    DefaultAttributeContainer attributeContainer = (DefaultAttributeContainer) ibaHolder.getAttributeContainer();
    ibaHolder = IBAValueHelper.service.refreshAttributeContainer(ibaHolder, null, null, null);
    attributeContainer = (DefaultAttributeContainer) ibaHolder.getAttributeContainer();
    if (attributeContainer == null) {
      return valore;
    }
    StandardIBADefinitionService defService = new StandardIBADefinitionService();
    AttributeDefDefaultView attributeDefinition = defService.getAttributeDefDefaultViewByPath(attribute);
    AbstractValueView[] abstractValueView = attributeContainer.getAttributeValues(attributeDefinition);
    if (abstractValueView.length != 0) {
      valore = abstractValueView[0].getLocalizedDisplayString();
      LogWrapper.logMessage(LogWrapper.DEBUG, "CDT: attributo: " + attribute + " -> valore: " + valore);
    }
    return valore;
  }

  /**
   * Recupera per il documento dato il valore di una lista di attributi
   *
   * @param epmDocument
   *          documento di cui cercare gli attributi
   * @param attributes
   *          lista degli attributi
   * @return lista dei valori degli attributi trovati, stringa vuota nel caso in cui l'attributo non sia stato trovato
   * @throws WTException
   *           errore nella ricerca dell'attributo
   * @throws RemoteException
   *           errore nella ricerca dell'attributo
   */
  public static List<String> getIBAAttributesValues(EPMDocument epmDocument, List<String> attributes) throws RemoteException, WTException {
    // CDTLogWrapper.logMessage(CDTLogWrapper.INFO, "CDT: Ingresso getDocumentAttributesValues");
    List<String> values = new ArrayList<String>();
    String valore;
    IBAHolder ibaHolder = (IBAHolder) epmDocument;
    DefaultAttributeContainer attributeContainer = (DefaultAttributeContainer) ibaHolder.getAttributeContainer();
    ibaHolder = IBAValueHelper.service.refreshAttributeContainer(ibaHolder, null, null, null);
    attributeContainer = (DefaultAttributeContainer) ibaHolder.getAttributeContainer();
    if (attributeContainer == null) {
      LogWrapper.logMessage(LogWrapper.WARN, "Attribute container null");
      return values;
    }
    StandardIBADefinitionService defService = new StandardIBADefinitionService();

    for (String attr : attributes) {
      AttributeDefDefaultView attributeDefinition = defService.getAttributeDefDefaultViewByPath(attr);
      AbstractValueView[] abstractValueView = attributeContainer.getAttributeValues(attributeDefinition);
      if (abstractValueView.length != 0) {
        valore = abstractValueView[0].getLocalizedDisplayString();
      }
      else {
        valore = "";
      }
      values.add(valore);
      LogWrapper.logMessage(LogWrapper.DEBUG, "CDT: attributo: " + attr + " -> valore: " + valore);
    }
    // CDTLogWrapper.logMessage(CDTLogWrapper.INFO, "CDT: Uscita getDocumentAttributesValues");
    return values;
  }

  /**
   * Il main per invocare i metodi dai test, o dalla shell dei comandi
   * 
   * @param args
   *          gli argomenti
   * @throws Exception
   *           l'eccezione
   */
  public static void main(String args[]) throws Exception {
    Properties p = new Properties();
    List<String> chiavi = new ArrayList<String>(Arrays.asList(MODELLO, NOMEATTRIBUTO, VALOREATTRIBUTO, TEST, MODO));

    // parsa le property
    for (int i = 0; i < args.length; i++) {
      String[] coppia = args[i].split("=");
      if (coppia.length != 2) {
        System.out.println("E' stata inserito un argomento non ben formattato: " + args[i]);
        System.out.println("Gli argomenti devono avere la forma : chiave=valore");
        System.exit(0);
      }
      String chiave = coppia[0];
      String valore = coppia[1];
      if (!chiavi.contains(chiave)) {
        System.out.println("E' stata inserito un argomento non riconosciuto: " + chiave);
        System.exit(0);
      }
      System.out.println("Inserita property: " + chiave + "-->" + valore);
      p.setProperty(chiave, valore);
    }

    // imposta user e pass
    user = CDTProperties.getProperty("ext.caditech.login.user", "wcadmin");
    password = CDTProperties.getProperty("ext.caditech.login.password", "wcadmin");

    // imposta il method server remoto
    RemoteMethodServer method = RemoteMethodServer.getDefault();
    method.setUserName(user);
    method.setPassword(password);

    // esegue l'opreazione voluta
    eseguiComeServer(p);

    // chiudo la consolle
    if (p.getProperty(TEST) != null && p.getProperty(TEST).equalsIgnoreCase("true")) {
      return;
    }
    System.exit(0);
  }

  /**
   * Esegue il comando scelto tramite le properties passate
   * 
   * @param p
   *          le properties
   * @throws SearchUtilsException
   *           l'eccezione sulla ricerca
   */
  public static String eseguiDaWindchillShell(Properties p) throws SearchUtilsException {
    String res = "";

    // cerca il documento sul quale effettuare l'operazione voluta
    EPMDocument epmDocument = SearchEPMDocument.findLatestEPMDocuments(p.getProperty(MODELLO));

    // nel caso non ci fosse
    if (epmDocument == null) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Modello non trovato");
      return res;
    }

    try {
      if (p.getProperty(MODO).equalsIgnoreCase("SET")) {
        // operazione di SET
        Attributes.setIBAAttributeValue(epmDocument, p.getProperty(NOMEATTRIBUTO), p.getProperty(VALOREATTRIBUTO));
      }
      else if (p.getProperty(MODO).equalsIgnoreCase("GET")) {
        // operazione di GET
        res = Attributes.getIBAAttributeValue(epmDocument, p.getProperty(NOMEATTRIBUTO));
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    return res;
  }

  /**
   * Esegue il comando scelto come server
   * 
   * @param p
   *          le properties
   * @throws Exception
   *           l'eccezione
   */
  public static String eseguiComeServer(Properties p) throws Exception {
    if (SERVER) {
      return eseguiDaWindchillShell(p);
    }
    else {
      try {
        // imposta user e pass
        user = CDTProperties.getProperty("ext.caditech.login.user", "wcadmin");
        password = CDTProperties.getProperty("ext.caditech.login.password", "wcadmin");

        // imposta il method server remoto
        RemoteMethodServer method = RemoteMethodServer.getDefault();
        method.setUserName(user);
        method.setPassword(password);

        Class[] argumentTypes = { Properties.class };
        Object[] arguments = { p };
        LogWrapper.logMessage(LogWrapper.INFO, "");
        Object tmp;
        tmp = method.invoke("eseguiDaWindchillShell", "ext.caditech.utility.Attributes", null, argumentTypes, arguments);
        return (String) tmp;
      }
      catch (InvocationTargetException e) {
        LogWrapper.logMessage(LogWrapper.ERROR, "Errore invocando eseguiDaWindchillShell", e);
        throw new Exception("Errore invocando eseguiDaWindchillShell", e);
      }
      catch (RemoteException e) {
        LogWrapper.logMessage(LogWrapper.ERROR, "Errore invocando eseguiDaWindchillShell", e);
        throw new Exception("Errore invocando eseguiDaWindchillShell", e);
      }
    }
  }
}
