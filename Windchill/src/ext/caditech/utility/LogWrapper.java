package ext.caditech.utility;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 
 * Classe per scrivere nei log di windchill
 * 
 * @author l.lusuardi
 * @author m.giudici
 *
 */
public class LogWrapper {

  /** per loggare un errore */
  public final static int ERROR = 0;

  /** per loggare un warning */
  public final static int WARN = 1;

  /** per loggare un'informazione */
  public final static int INFO = 2;

  /** per loggare informazioni di debug */
  public final static int DEBUG = 3;

  /** Il file di log (del method server) */
  private final static Logger log = LogManager.getLogger(Thread.currentThread().getStackTrace()[1].getClassName());

  /**
   * Costruttore di default
   */
  public LogWrapper() {

  }

  /**
   * Scrive un messaggio nei log ed a consolle
   * 
   * @param level
   *          il livello di dettaglio nel log
   *          <ul>
   *          <li>0 = errore</li>
   *          <li>1 = warning</li>
   *          <li>2 = info</li>
   *          <li>3 = debug</li>
   *          </ul>
   * @param msg
   *          il messaggio da loggare
   * @param t
   *          l'eccezione (eventuale)
   */
  public static void logMessage(int level, String msg, Throwable t) {
    // scrive nel log con la forma corretta
    switch (level) {
    case ERROR:
      log.error(msg, t);
      break;
    case WARN:
      log.warn(msg, t);
      break;
    case INFO:
      log.info(msg, t);
      break;
    case DEBUG:
      log.debug(msg, t);
      break;
    }
  }

  /**
   * Scrive un messaggio nei log ed a consolle
   * 
   * @param level
   *          il livello di dettaglio nel log
   *          <ul>
   *          <li>0 = errore</li>
   *          <li>1 = warning</li>
   *          <li>2 = info</li>
   *          <li>3 = debug</li>
   *          </ul>
   * @param msg
   *          il messaggio
   */
  public static void logMessage(int level, String msg) {
    logMessage(level, msg, null);
  }
}
