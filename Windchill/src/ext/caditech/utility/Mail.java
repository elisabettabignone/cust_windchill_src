package ext.caditech.utility;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Mail {

  public void inviaMail(List<String> recipients, String subject, String message, String from, String host, String user, String password, String port)
      throws MessagingException {
    boolean debug = false;

    // Impostazioni SMTP
    Properties props = new Properties();
    props.put("mail.smtp.host", host);
    if (port != null && !port.equals("")) {
      props.put("mail.protocol.port", Integer.parseInt(port));
    }

    // istanzio un oggetto Session
    Session session = Session.getDefaultInstance(props, null);
    session.setDebug(debug);

    // creo l'oggetto Message partendo da Session
    Message msg = new MimeMessage(session);

    // Mittente
    InternetAddress addressFrom = new InternetAddress(from);
    msg.setFrom(addressFrom);

    // Adesso
    msg.setSentDate(new Date());

    // Destinatari
    InternetAddress[] addressTo = new InternetAddress[recipients.size()];
    for (int i = 0; i < recipients.size(); i++) {
      addressTo[i] = new InternetAddress(recipients.get(i));
    }
    msg.setRecipients(Message.RecipientType.TO, addressTo);

    // OPZIONALE: � possibile definire anche dei custom headers...
    // msg.addHeader("MyHeaderName", "myHeaderValue");

    // Imposto il subject, il contenuto ed il content type (testo semplice)
    msg.setSubject(subject);
    msg.setContent(message, "text/plain");

    // Spedisco
    if (user == null || user.equalsIgnoreCase("")) {
      Transport.send(msg);
    }
    else {
      Transport.send(msg, user, password);
    }
  }

  /**
   * Il main per testare tutto
   *
   * @param args
   */
  public static void main(String[] args) {
    Mail mailbomber = new Mail();

    List<String> to = new ArrayList<String>();
    to.add("sonia.menocci@caditech.it");
    to.add("luca.lusuardi@caditech.it");
    try {
      mailbomber.inviaMail(to, "l'oggetto", "il messaggio", "zuggue@caditech.it", "smtp.caditech.it", "luca.lusuardi@caditech.it", "lucaluca", null);
    }
    catch (MessagingException e) {
      e.printStackTrace();
    }
  }
}
