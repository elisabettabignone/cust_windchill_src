package ext.caditech.utility;

public class SearchUtilsException extends Exception {

  /**
   * l'id seriale della classe
   */
  private static final long serialVersionUID = 1L;

  public SearchUtilsException(String msg) {
    super(msg);
  }

  public SearchUtilsException(String msg, Throwable causa) {
    super(msg, causa);
  }

  public SearchUtilsException(Throwable causa) {
    super(causa);
  }
}
