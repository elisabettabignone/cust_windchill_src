package ext.caditech.utility;

import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Properties;
import java.util.Set;

import wt.epm.EPMDocument;
import wt.epm.workspaces.EPMWorkspace;
import wt.epm.workspaces.EPMWorkspaceHelper;
import wt.fc.ObjectReference;
import wt.fc.collections.WTSet;
import wt.inf.container.WTContainerRef;
import wt.lifecycle.LifeCycleException;
import wt.lifecycle.LifeCycleHelper;
import wt.lifecycle.LifeCycleHistory;
import wt.locks.LockException;
import wt.method.RemoteMethodServer;
import wt.org.WTUser;
import wt.pom.PersistenceException;
import wt.util.WTException;
import wt.util.WTPropertyVetoException;
import ext.caditech.checkout.CheckoutPDMLinkException;

/**
 * Classe per fare prove a caso con un main gi� pronto a prendere delle properties in input, scrivere nel metodo esegui
 * quello che si vuole provare
 * 
 * @author s.menocci
 *
 */
public class MainTest implements wt.method.RemoteAccess {

  /** L'username con cui autenticarsi in Windchill */
  static private String user = "";

  /** La password con cui autenticarsi in Windchill */
  static private String password = "";

  /** booleano che mi dice se sono o meno il server */
  static final boolean SERVER;

  /** booleano per fare il debug in locale, ovvero senza eseguirlo come server windchill */
  static final boolean DEBUG_LOCAL = false;

  /** recupera se windchill � in modalit� server o meno */
  static {
    SERVER = RemoteMethodServer.ServerFlag;
  }

  /**
   * Esegue il test voluto, in base ai parametri usati
   * 
   * @param p
   *          le property per i test, compresa di avvio (necessario)
   * @throws SearchUtilsException
   *           l'eccezione sulla ricerca
   * @throws LockException
   *           l'eccezione sul lock
   * @throws WTPropertyVetoException
   *           l'eccezione sulle property "veto"
   * @throws PersistenceException
   *           l'eccezione di persistenza sulle modifiche
   * @throws WTException
   *           l'eccezione generica sulle classi windchill
   */
  public static void esegui(Properties p) throws SearchUtilsException, LockException, WTPropertyVetoException, PersistenceException, WTException {

    String avvio = p.getProperty("avvio");
    if (avvio.equalsIgnoreCase("attributi")) {
      testAttributi(p);
    }
    else if (avvio.equalsIgnoreCase("attributilista")) {
      testRetriveAttributes(p);
    }
    else if (avvio.equalsIgnoreCase("state")) {
      testUtenteSetState(p);

    }
  }

  public static void testUtenteSetState(Properties p) throws LifeCycleException, WTException, SearchUtilsException {
    String nome = p.getProperty("nome");

    EPMDocument epmdoc = SearchEPMDocument.findLatestEPMDocuments(nome);
    LifeCycleHistory aHistory = null;
    Enumeration e = LifeCycleHelper.service.getHistory(epmdoc);
    while (e.hasMoreElements()) {
      aHistory = (LifeCycleHistory) e.nextElement();
      LogWrapper.logMessage(LogWrapper.INFO, "actor name:" + aHistory.getActorName());
    }
    LogWrapper.logMessage(LogWrapper.INFO, "CDT: Fine testUtenteSetState");

  }

  public static void testSvuotaWorkspace(String nomeWS, String username) throws SearchUtilsException, CheckoutPDMLinkException {
    System.out.println("Container");
    WTContainerRef container = SearchUtils.findContainer();
    System.out.println("User");
    WTUser user = SearchUtils.findUser(username);
    System.out.println("Workspace");
    EPMWorkspace ws = SearchUtils.findWorkspace(nomeWS, user, container, false);
    System.out.println("Svuoto il workspace");
    SearchUtils.svuotaWorkspace(ws);

    WTSet oggettiNelWorkspace = null;
    try {
      oggettiNelWorkspace = EPMWorkspaceHelper.manager.getObjectsInWorkspace(ws, EPMDocument.class);
      System.out.println("Ci sono " + oggettiNelWorkspace.size() + " elementi nel ws " + ws.getName());
    }
    catch (WTException e1) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore ricercando oggetti nel workspace ", e1);
      throw new CheckoutPDMLinkException("Errore ricercando oggetti nel workspace ", e1);
    }
    System.out.println("Oggetti nel workspace:");
    for (Object or : oggettiNelWorkspace) {
      EPMDocument epmDocument = (EPMDocument) ((ObjectReference) or).getObject();
      System.out.println("- " + epmDocument.getNumber());
    }
  }

  /**
   * Test sugli attributi inseriti
   * 
   * @param p
   *          le property (ad es. avvio=attributi tipo=ge.caditech.DefaultEPMDocument)
   */
  private static void testAttributi(Properties p) {
    System.out.println("-------------------------------");
    String nomeTipo = p.getProperty("tipo");
    System.out.println("Tipo inserito -> " + nomeTipo);
    System.out.println("------- SOFT ATTRIBUTES");
    ArrayList<String> attributiSoft = SearchAttributes.getSoftAttributesName(nomeTipo);
    Collections.sort(attributiSoft);
    for (String a : attributiSoft) {
      System.out.println("IBA :- " + a);
    }
    System.out.println("Trovati " + attributiSoft.size() + " attributi soft");
    System.out.println("------- HARD ATTRIBUTES");
    ArrayList<String> attributiHard = SearchAttributes.getHardAttributesName(nomeTipo);
    Collections.sort(attributiHard);
    for (String a : attributiHard) {
      System.out.println("IBA :- " + a);
    }
    System.out.println("Trovati " + attributiHard.size() + " attributi hard");
    System.out.println("-------------------------------");
  }

  /**
   * Test sugli attributi del tipo di documento passato nelle property
   * 
   * @param p
   *          le property (ad es. avvio=attributilista tipo=wt.epm.EPMDocument)
   */
  private static void testRetriveAttributes(Properties p) {
    String nomeTipo = p.getProperty("tipo");

    LinkedHashMap<String, String> res = null;
    Hashtable<String, String> hres = null;
    Set<String> keys = null;

    // soft
    res = SearchAttributes.getSoftAttributesType(nomeTipo);
    keys = res.keySet();

    for (String key : keys) {
      System.out.println("attributo (soft): " + key + "; tipo: " + res.get(key));
    }

    // hard
    hres = SearchAttributes.getHardAttributesType(nomeTipo);
    keys = hres.keySet();

    for (String key : keys) {
      System.out.println("attributo (hard): " + key + "; tipo: " + hres.get(key));
    }
  }

  /**
   * metodo statico per eseguire un'operazione come server
   * 
   * @param p
   *          le property
   * @throws Exception
   *           l'eccezione
   */
  public static void eseguiComeServer(Properties p) throws Exception {
    if (SERVER) {
      LogWrapper.logMessage(LogWrapper.DEBUG, "CDT-eseguiComeServer: sono server");
      esegui(p);
    }
    else {
      try {
        LogWrapper.logMessage(LogWrapper.DEBUG, "CDT-eseguiComeServer: non sono server");

        // imposta user e pass
        user = CDTProperties.getProperty("ext.caditech.login.user", "wcadmin");
        password = CDTProperties.getProperty("ext.caditech.login.password", "wcadmin");

        // imposta il method server remoto
        RemoteMethodServer method = RemoteMethodServer.getDefault();
        method.setUserName(user);
        method.setPassword(password);

        Class[] argumentTypes = { Properties.class };
        Object[] arguments = { p };
        method.invoke("esegui", "ext.caditech.utility.MainTest", null, argumentTypes, arguments);
      }
      catch (InvocationTargetException itex) {
        Throwable throwable = itex.getTargetException();
        if (throwable instanceof WTException) {
          throw (WTException) throwable;
        }
        Object aobj[] = { "esegui" };
        throw new WTException(throwable, "wt.fc.fcResource", "0", aobj);
      }
      catch (RemoteException rex) {
        Object aobj[] = { "esegui" };
        throw new WTException(rex, "wt.fc.fcResource", "0", aobj);
      }
    }
  }

  /**
   * Main per simulare il checkin da una windchill shell
   *
   * @param args
   *          gli argomenti passati
   * @throws Exception
   *           l'eccezione riscontrata
   */
  public static void main(String[] args) throws Exception {
    // imposta user e pass
    user = CDTProperties.getProperty("ext.caditech.login.user", "wcadmin");
    password = CDTProperties.getProperty("ext.caditech.login.password", "wcadmin");

    // imposta il method server remoto
    RemoteMethodServer method = RemoteMethodServer.getDefault();
    method.setUserName(user);
    method.setPassword(password);

    LogWrapper.logMessage(LogWrapper.DEBUG, "CDT Main ");

    Properties p = new Properties();
    // parsa le property
    for (int i = 0; i < args.length; i++) {
      String[] coppia = args[i].split("=");
      if (coppia.length != 2) {
        System.out.println("E' stata inserito un argomento non ben formattato: " + args[i]);
        System.out.println("Gli argomenti devono avere la forma : chiave=valore");
        System.exit(0);
      }
      String chiave = coppia[0];
      String valore = coppia[1];
      System.out.println("Inserita property: " + chiave + "-->" + valore);
      p.setProperty(chiave, valore);
    }

    if (DEBUG_LOCAL) {
      esegui(p);
      // testSvuotaWorkspace("ws_test_undo_checkout", "Administrator");
    }
    else {
      eseguiComeServer(p);
    }

    System.exit(0);
  }
}
