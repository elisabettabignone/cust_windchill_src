package ext.caditech.utility;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import wt.epm.EPMDocument;
import wt.epm.EPMDocumentMaster;
import wt.epm.structure.EPMReferenceLink;
import wt.epm.workspaces.EPMWorkspace;
import wt.epm.workspaces.EPMWorkspaceHelper;
import wt.fc.Persistable;
import wt.fc.PersistenceHelper;
import wt.fc.QueryResult;
import wt.fc.collections.WTArrayList;
import wt.fc.collections.WTSet;
import wt.iba.definition.StringDefinition;
import wt.iba.value.StringValue;
import wt.method.RemoteMethodServer;
import wt.part.WTPartReferenceLink;
import wt.query.ConstantExpression;
import wt.query.QueryException;
import wt.query.QuerySpec;
import wt.query.SearchCondition;
import wt.query.TableColumn;
import wt.util.WTException;
import wt.vc.Iterated;
import wt.vc.VersionControlHelper;
import wt.vc.wip.WorkInProgressHelper;
import wt.vc.wip.Workable;

/**
 * Classe con i metodi di supporto per effettuare ricerche in ambiente WindChill
 *
 * @author l.lusuardi
 *
 */
public class SearchEPMDocument {

  /** L'username con cui autenticarsi in Windchill */
  static private String user = "";

  /** La password con cui autenticarsi in Windchill */
  static private String password = "";

  /** mappa statica per avere le coppie <nome attributo, id attributo> per ottimizzare le ricerche degli EPMDocument */
  // private static HashMap<String, String> optimizedAttribute =
  // findIdAttribute(CDTProperties.getProperty("ext.caditech.optimizedAttribute", "CODICE"));

  private static HashMap<String, String> optimizedAttribute;

  /**
   * Restituisce la mappa con l'associazione della coppie <nome attributo, id attributo su db>
   * 
   * @return
   */
  private static HashMap<String, String> getOptimizedAttribute() {
    if (optimizedAttribute == null) {
      optimizedAttribute = new HashMap<String, String>();
    }
    return optimizedAttribute;
  }

  /**
   * Restituisce l'id su database dell'attributo passato con nome uguale all'argomento
   *
   * @param attributeName
   *          il nome dell'attributo per il quale vogliamo cercare l'id
   * @return la mappa con la coppia <nome attributo, id attributo su db>
   */
  private static String getIDattribute(String attributeName) {
    String id = "";
    if (getOptimizedAttribute().containsKey(attributeName)) {
      id = getOptimizedAttribute().get(attributeName);
      LogWrapper.logMessage(LogWrapper.INFO, "CDT - hashmap contiene " + attributeName + " (id su db: " + id + ")");
    }
    else {
      id = SearchAttributes.searchIDattribute(attributeName);
      LogWrapper.logMessage(LogWrapper.INFO, "CDT - inserimento in hashmap attributo " + attributeName + " con id su db: " + id);
      if (id != null && !id.equalsIgnoreCase("")) {
        getOptimizedAttribute().put(attributeName, id);
      }
    }
    return id;
  }

  /**
   * Effettua la ricerca della prima working copy di un EPMDocument, in input riceve il number (<code>nome</code>.
   * <code>estensione</code>)
   *
   * @param number
   *          il campo number dell'EPMDocument da cercare
   * @return un EPMDocument o <code>NULL</code> se la ricerca non da risultati
   * @throws SearchUtilsException
   *           in caso di errore
   */
  @SuppressWarnings("deprecation")
  public static EPMDocument findLatestEPMDocumentsWorkingCopy(String number) throws SearchUtilsException {
    LogWrapper.logMessage(LogWrapper.DEBUG, "Ricerca EPMDocument per number -> " + number);
    EPMDocument doc = null;
    try {
      EPMDocumentMaster epmDocumentMaster = null;
      QuerySpec findCriteria = new QuerySpec(EPMDocumentMaster.class);
      String operator = SearchCondition.EQUAL;
      number = number.toUpperCase().trim();

      findCriteria.appendSearchCondition(new SearchCondition(EPMDocumentMaster.class, EPMDocumentMaster.NUMBER, operator, number));
      QueryResult findResult = PersistenceHelper.manager.find(findCriteria);

      while (findResult.hasMoreElements()) {
        epmDocumentMaster = (EPMDocumentMaster) findResult.nextElement();
        QueryResult versions = VersionControlHelper.service.allVersionsOf(epmDocumentMaster);
        while (versions.hasMoreElements()) {
          EPMDocument temp = (EPMDocument) versions.nextElement();
          String v = temp.getVersionIdentifier().getValue();
          String i = temp.getIterationIdentifier().getValue();
          if (!WorkInProgressHelper.isWorkingCopy((Workable) temp)) {

            LogWrapper.logMessage(LogWrapper.INFO, "Documento con number = " + temp.getNumber() + " versione: " + v + " iterazione: " + i
                + " non � una working copy. Vado avanti");
            continue;
          }
          else {
            LogWrapper.logMessage(LogWrapper.INFO, "Documento con number = " + temp.getNumber() + " versione: " + v + " iterazione: " + i
                + " � una working copy. Elemento trovato");
            doc = temp;
            break;
          }
        }
      }
      if (doc == null) {
        LogWrapper.logMessage(LogWrapper.WARN, "findEPMDocuments: non ho trovato EPMDocument con number=" + number);
      }
    }
    catch (WTException ex) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore nel metodo findEPMDocuments(" + number + ")", ex);
      throw new SearchUtilsException("Errore nel metodo findEPMDocuments(" + number + ")", ex);
    }
    return doc;
  }

  /**
   * Effettua la ricerca di un EPMDocument (escluse le working copy), in input riceve il number (<code>nome</code>.
   * <code>estensione</code>)
   *
   * @param number
   *          il campo number dell'EPMDocument da cercare
   * @return un EPMDocument o <code>NULL</code> se la ricerca non da risultati
   * @throws SearchUtilsException
   *           in caso di errore
   */
  @SuppressWarnings("deprecation")
  public static EPMDocument findLatestEPMDocuments(String number) throws SearchUtilsException {
    LogWrapper.logMessage(LogWrapper.DEBUG, "Ricerca EPMDocument per number -> " + number);
    EPMDocument doc = null;
    try {
      EPMDocumentMaster epmDocumentMaster = null;
      QuerySpec findCriteria = new QuerySpec(EPMDocumentMaster.class);
      String operator = SearchCondition.EQUAL;
      number = number.toUpperCase().trim();

      findCriteria.appendSearchCondition(new SearchCondition(EPMDocumentMaster.class, EPMDocumentMaster.NUMBER, operator, number));
      QueryResult findResult = PersistenceHelper.manager.find(findCriteria);

      while (findResult.hasMoreElements()) {
        epmDocumentMaster = (EPMDocumentMaster) findResult.nextElement();
        QueryResult versions = VersionControlHelper.service.allVersionsOf(epmDocumentMaster);
        while (versions.hasMoreElements()) {
          EPMDocument temp = (EPMDocument) versions.nextElement();
          String v = temp.getVersionIdentifier().getValue();
          String i = temp.getIterationIdentifier().getValue();
          if (WorkInProgressHelper.isWorkingCopy((Workable) temp)) {

            LogWrapper.logMessage(LogWrapper.INFO, "Documento con number = " + temp.getNumber() + " versione: " + v + " iterazione: " + i
                + " � una working copy. Vado avanti");
            continue;
          }
          else {
            LogWrapper.logMessage(LogWrapper.INFO, "Documento con number = " + temp.getNumber() + " versione: " + v + " iterazione: " + i
                + " non � una working copy. Elemento trovato");
            doc = temp;
            break;
          }
        }
      }
      if (doc == null) {
        LogWrapper.logMessage(LogWrapper.WARN, "findEPMDocuments: non ho trovato EPMDocument con number=" + number);
      }
    }
    catch (WTException ex) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore nel metodo findEPMDocuments(" + number + ")", ex);
      throw new SearchUtilsException("Errore nel metodo findEPMDocuments(" + number + ")", ex);
    }
    return doc;
  }

  /**
   * Effettua la ricerca di un EPMDocument, in input riceve il number (<nome>.<estensione>), versione e iterazione
   *
   * @param number
   *          del documento da cercare
   * @param versione
   *          del documento da cercare
   * @param iterazione
   *          del documento da cercare
   * @return un EPMDocument o <code>NULL</code> se la ricerca non da risultati
   * @throws SearchUtilsException
   *           in caso di errore
   */
  @SuppressWarnings("deprecation")
  public static EPMDocument findEPMDocuments(String number, String versione, String iterazione) throws SearchUtilsException {
    System.out.println("Ricerca EPMDocument per number:" + number + " -versione: " + versione + " -iterazione:" + iterazione);
    EPMDocument doc = null;
    try {
      EPMDocumentMaster epmDocumentMaster = null;
      QuerySpec findCriteria = new QuerySpec(EPMDocumentMaster.class);
      String operator = SearchCondition.EQUAL;
      number = number.toUpperCase().trim();
      findCriteria.appendSearchCondition(new SearchCondition(EPMDocumentMaster.class, EPMDocumentMaster.NUMBER, operator, number));
      QueryResult findResult = PersistenceHelper.manager.find(findCriteria);

      while (findResult.hasMoreElements()) {
        epmDocumentMaster = (EPMDocumentMaster) findResult.nextElement();

        // Restituisce tutte le iterazioni per tutte le versioni
        QueryResult iterations = VersionControlHelper.service.allIterationsOf(epmDocumentMaster);

        while (iterations.hasMoreElements()) {
          doc = (EPMDocument) iterations.nextElement();
          String v = doc.getVersionIdentifier().getValue();
          String i = doc.getIterationIdentifier().getValue();
          System.out.println("number " + doc.getNumber() + " versione: " + v + " iterazione: " + i);
          if (v.equalsIgnoreCase(versione) && i.equalsIgnoreCase(iterazione)) {
            return doc;
          }
        }
      }
      if (doc == null) {
        System.out.println("findEPMDocuments: non ho trovato EPMDocument con number=" + number);
        LogWrapper.logMessage(LogWrapper.WARN, "findEPMDocuments: non ho trovato EPMDocument con number=" + number + " -versione: " + versione
            + " -iterazione:" + iterazione);
      }
    }
    catch (WTException ex) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore nel metodo findEPMDocuments(" + number + ", " + versione + "," + iterazione + ")", ex);
      throw new SearchUtilsException("Errore nel metodo findEPMDocuments(" + number + ", " + versione + "," + iterazione + ")", ex);
    }
    return null;
  }

  /**
   * Dato un EPMDocument naviga le sue dipendenze e selezione quelle di tipo
   * <code>wt.epm.structure.EPMReferenceType.DRAWING</code>. A queste dipendenze corrispondono i disegni del modello
   * passato in input.
   *
   * @param epmDocument
   *          il documento
   * @param ws
   *          il workspace
   * @return una lista con gli EPMDocument trovati
   * @throws SearchUtilsException
   *           in caso di errore
   */
  public static List<EPMDocument> findDrawing(EPMDocument epmDocument, EPMWorkspace ws) throws SearchUtilsException {
    List<EPMDocument> res = new ArrayList<EPMDocument>();
    LogWrapper.logMessage(LogWrapper.INFO, "Ricerca disegni di " + epmDocument.getNumber());

    try {
      QueryResult qr = PersistenceHelper.manager.navigate(epmDocument.getMaster(), WTPartReferenceLink.REFERENCED_BY_ROLE, EPMReferenceLink.class,
          false);
      LogWrapper.logMessage(LogWrapper.INFO, "Sono state trovate " + qr.size() + " dipendenze di tipo WTPartReferenceLink.REFERENCED_BY_ROLE");
      while (qr.hasMoreElements()) {
        Persistable p = (Persistable) qr.nextElement();
        if (p instanceof EPMReferenceLink) {
          EPMReferenceLink reference = (EPMReferenceLink) p;
          if (reference.getReferenceType().getStringValue().equalsIgnoreCase("wt.epm.structure.EPMReferenceType.DRAWING")) {
            if (reference.getRoleAObjectRef().getObject() instanceof EPMDocument) {
              EPMDocument drawing = (EPMDocument) reference.getRoleAObjectRef().getObject();
              LogWrapper.logMessage(LogWrapper.INFO, "Trovato drawing " + drawing.getNumber());
              res.add(drawing);
            }
          }
          else {
            System.out.println(reference.getReferenceType().getStringValue());
          }

        }
      }

    }
    catch (WTException e) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore ricercando drawing per " + epmDocument.getNumber(), e);
      throw new SearchUtilsException("Errore ricercando drawing per " + epmDocument.getNumber(), e);
    }
    return res;
  }

  /**
   * Dato un EPMDocument ricerca un drawing suo omonimo. Ritorna <code>null</code> in due casi: se la ricerca non da
   * risultati o se in input viene passato un drawing
   *
   * @param epmDocument
   *          il documento (parte o assieme) di cui cercare il drw omonimo.
   * @throws SearchUtilsException
   *           in caso di errore durante la ricerca
   * @return il documento trovato o <code>null</code>
   */
  public static EPMDocument findDrawingPerNome(EPMDocument epmDocument) throws SearchUtilsException {
    String nomeModello = epmDocument.getNumber().substring(0, epmDocument.getNumber().lastIndexOf("."));
    String ext = epmDocument.getNumber().substring(epmDocument.getNumber().lastIndexOf(".") + 1);
    LogWrapper.logMessage(LogWrapper.INFO, "Cerco il disegno con nome " + nomeModello + ".drw");
    EPMDocument res = null;
    if ("drw".equalsIgnoreCase(ext)) {
      LogWrapper.logMessage(LogWrapper.DEBUG, epmDocument.getNumber() + " � gi� un disegno.");
      return null;
    }
    else {
      res = SearchEPMDocument.findLatestEPMDocuments(nomeModello + ".drw");
    }
    String msg = (res == null) ? "Non ho trovato il disegno omonimo di " + epmDocument.getNumber() : "Trovato il disegno " + res.getNumber();
    LogWrapper.logMessage(LogWrapper.DEBUG, msg);
    return res;
  }

  /**
   * Restituisce tutti gli EPMDocument in Windchill in ultima versione e iterazione, compresi gli oggetti nuovi
   *
   * @return una lista di tutti gli EPMDocument trovati
   * @throws SearchUtilsException
   *           in caso di errore nella ricerca
   */
  public static ArrayList<EPMDocument> findLatestEPMDocuments() throws SearchUtilsException {
    return findLatestEPMDocuments(true);
  }

  /**
   * Restituisce tutti gli EPMDocument in Windchill in ultima versione e iterazione
   *
   * @param voglioGliOggettiNuovi
   *          true se nel risultato della ricerca vogliamo includere anche gli oggetti nuovi, false altrimenti
   * @return una lista di tutti gli EPMDocument trovati
   * @throws SearchUtilsException
   *           in caso di errore nella ricerca
   */
  public static ArrayList<EPMDocument> findLatestEPMDocuments(boolean voglioGliOggettiNuovi) throws SearchUtilsException {

    ArrayList<EPMDocument> docTrovati = new ArrayList<EPMDocument>();
    try {
      QuerySpec findCriteria = new QuerySpec(EPMDocumentMaster.class);
      @SuppressWarnings("deprecation")
      QueryResult findResult = PersistenceHelper.manager.find(findCriteria);
      EPMDocumentMaster epmDocumentMaster = null;
      EPMDocument doc = null;
      while (findResult.hasMoreElements()) {
        epmDocumentMaster = (EPMDocumentMaster) findResult.nextElement();
        QueryResult versions = VersionControlHelper.service.allVersionsOf(epmDocumentMaster);
        while (versions.hasMoreElements()) {
          doc = (EPMDocument) versions.nextElement();
          break;
        }
        boolean newObject = false;
        if (!voglioGliOggettiNuovi) {
          newObject = isNewObject(doc);
        }
        if (!newObject) {
          docTrovati.add(doc);
        }
      }
    }
    catch (QueryException e) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore ricercando tutti gli EPMDocument ");
      throw new SearchUtilsException("Errore ricercando tutti gli EPMDocument", e);
    }
    catch (WTException e) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore ricercando tutti gli EPMDocument ");
      throw new SearchUtilsException("Errore ricercando tutti gli EPMDocument", e);
    }
    LogWrapper.logMessage(LogWrapper.INFO, "Trovati " + docTrovati.size() + " docuemnti di tipo EPMDocument");
    return docTrovati;
  }

  /**
   * Restituisce tutti gli EPMDocument nuovi, ovvero di cui non � ancora stato fatto il primo checkin in Windchill
   *
   * @return una lista di tutti gli EPMDocument nuovi trovati
   * @throws SearchUtilsException
   *           in caso di errore nella ricerca
   */
  public static ArrayList<EPMDocument> findNewEPMDocuments() throws SearchUtilsException {

    ArrayList<EPMDocument> docTrovati = new ArrayList<EPMDocument>();
    try {
      QuerySpec findCriteria = new QuerySpec(EPMDocumentMaster.class);
      @SuppressWarnings("deprecation")
      QueryResult findResult = PersistenceHelper.manager.find(findCriteria);
      EPMDocumentMaster epmDocumentMaster = null;
      EPMDocument doc = null;
      while (findResult.hasMoreElements()) {
        epmDocumentMaster = (EPMDocumentMaster) findResult.nextElement();
        QueryResult versions = VersionControlHelper.service.allVersionsOf(epmDocumentMaster);
        while (versions.hasMoreElements()) {
          doc = (EPMDocument) versions.nextElement();
          break;
        }
        boolean newObject = isNewObject(doc);
        if (newObject) {
          docTrovati.add(doc);
        }
      }
    }
    catch (QueryException e) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore ricercando tutti gli EPMDocument ");
      throw new SearchUtilsException("Errore ricercando tutti gli EPMDocument", e);
    }
    catch (WTException e) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore ricercando tutti gli EPMDocument ");
      throw new SearchUtilsException("Errore ricercando tutti gli EPMDocument", e);
    }
    LogWrapper.logMessage(LogWrapper.INFO, "Trovati " + docTrovati.size() + " docuemnti di tipo EPMDocument");
    return docTrovati;
  }

  /**
   * Effettua la ricerca degli EPMDocument creati dopo la data passata in input in ultima versione e iterazione
   *
   * @param date
   *          data di creazione
   * @param voglioGliOggettiNuovi
   *          true se nel risultato della ricerca vogliamo includere anche gli oggetti nuovi, false altrimenti
   * @return un EPMDocument o lista vuota se la ricerca non da risultati
   * @throws SearchUtilsException
   *           in caso di errore
   */
  @SuppressWarnings("deprecation")
  public static ArrayList<EPMDocument> findLatestEPMDocuments(Date date, boolean voglioGliOggettiNuovi) throws SearchUtilsException {
    LogWrapper.logMessage(LogWrapper.DEBUG, "Ricerca EPMDocument per data di creazione");
    ArrayList<EPMDocument> docTrovati = new ArrayList<EPMDocument>();
    EPMDocument epmDocument = null;
    EPMDocument latestVersion = null;
    try {
      QuerySpec findCriteria = new QuerySpec(EPMDocument.class);
      Timestamp ts = new Timestamp(date.getTime());
      SearchCondition sc = new SearchCondition(EPMDocument.class, EPMDocument.MODIFY_TIMESTAMP, SearchCondition.GREATER_THAN_OR_EQUAL, ts);
      findCriteria.appendSearchCondition(sc);

      QueryResult findResult = PersistenceHelper.manager.find(findCriteria);

      // System.out.println("finded docs: " + findResult.size());

      while (findResult.hasMoreElements()) {
        epmDocument = (EPMDocument) findResult.nextElement();
        boolean inWork = epmDocument.getCheckoutInfo().getState().getStringValue().equals("wt.vc.wip.WorkInProgressState.wrk");
        boolean newObject = false;
        if (!voglioGliOggettiNuovi) {
          newObject = isNewObject(epmDocument);
        }
        if (epmDocument.isLatestIteration() && !inWork && !newObject) {

          QueryResult versions = VersionControlHelper.service.allVersionsOf(epmDocument.getMaster());
          while (versions.hasMoreElements()) {
            latestVersion = (EPMDocument) versions.nextElement();
            break;
          }
          if (latestVersion.getVersionIdentifier().getValue().equalsIgnoreCase(epmDocument.getVersionIdentifier().getValue())) {
            docTrovati.add(epmDocument);
          }
        }
      }
    }
    catch (WTException ex) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore nel metodo findEPMDocuments(" + date + ")", ex);
      throw new SearchUtilsException("Errore nel metodo findEPMDocuments(" + date + ")", ex);
    }
    return docTrovati;
  }

  /**
   * Effettua la ricerca degli EPMDocument creati in un range di tempo in ultima versione e iterazione
   * 
   * @param dataInizio
   *          data di inizio della ricerca (compresa)
   * @param dataFine
   *          data di fine (esclusa)
   * @param voglioGliOggettiNuovi
   *          true se nel risultato della ricerca vogliamo includere anche gli oggetti nuovi, false altrimenti
   * @return la lista degli EPMDocument trovati, lista vuota nel caso non ce ne siano
   * @throws SearchUtilsException
   *           in caso di errore
   */
  @SuppressWarnings("deprecation")
  public static ArrayList<EPMDocument> findLatestEPMDocuments(Date dataInizio, Date dataFine, boolean voglioGliOggettiNuovi)
      throws SearchUtilsException {
    LogWrapper.logMessage(LogWrapper.INFO, "Ricerca EPMDocument per data di creazione");
    LogWrapper.logMessage(LogWrapper.INFO, "Range -> [" + dataInizio.toString() + "-" + dataFine.toString() + "]");
    ArrayList<EPMDocument> docTrovati = new ArrayList<EPMDocument>();
    EPMDocument latestVersion = null;
    try {
      EPMDocument epmDocument = null;
      QuerySpec findCriteria = new QuerySpec(EPMDocument.class);
      Timestamp tsInizio = new Timestamp(dataInizio.getTime());
      SearchCondition scDataInizio = new SearchCondition(EPMDocument.class, EPMDocument.MODIFY_TIMESTAMP, SearchCondition.GREATER_THAN_OR_EQUAL,
          tsInizio);
      findCriteria.appendSearchCondition(scDataInizio);
      findCriteria.appendAnd();
      Timestamp tsFine = new Timestamp(dataFine.getTime());
      SearchCondition scDataFine = new SearchCondition(EPMDocument.class, EPMDocument.MODIFY_TIMESTAMP, SearchCondition.LESS_THAN, tsFine);
      findCriteria.appendSearchCondition(scDataFine);

      QueryResult findResult = PersistenceHelper.manager.find(findCriteria);

      while (findResult.hasMoreElements()) {
        epmDocument = (EPMDocument) findResult.nextElement();
        boolean inWork = epmDocument.getCheckoutInfo().getState().getStringValue().equals("wt.vc.wip.WorkInProgressState.wrk");
        boolean newObject = false;
        if (!voglioGliOggettiNuovi) {
          newObject = isNewObject(epmDocument);
        }
        if (epmDocument.isLatestIteration() && !inWork && !newObject) {
          epmDocument.getState().getState().getStringValue();
          QueryResult versions = VersionControlHelper.service.allVersionsOf(epmDocument.getMaster());
          while (versions.hasMoreElements()) {
            latestVersion = (EPMDocument) versions.nextElement();
            break;
          }
          if (latestVersion.getVersionIdentifier().getValue().equalsIgnoreCase(epmDocument.getVersionIdentifier().getValue())) {
            docTrovati.add(epmDocument);
          }
        }
      }
    }
    catch (WTException ex) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore nel metodo findEPMDocuments(" + dataInizio + " , " + dataFine + ")", ex);
      throw new SearchUtilsException("Errore nel metodo findEPMDocuments(" + dataInizio + " , " + dataFine + ")", ex);
    }
    return docTrovati;
  }

  public static boolean isNewObject(EPMDocument epm) throws WTException {
    WTArrayList oggetti = new WTArrayList();
    oggetti.add(epm);
    WTSet oggettiNuovi = EPMWorkspaceHelper.manager.getNewObjects(oggetti);
    int n = oggettiNuovi.size();
    return n == 1;
  }

  /**
   * Ricerca (NON ottimizzata) per trovare un EPMDocument tramite il valore di un attributo
   *
   * @param attrName
   *          il nome dell'attributo con il quale confrontare il suo valore
   * @param attrValue
   *          il valore dell'attributo da confrontare
   * @return il documento trovato
   */
  @SuppressWarnings("deprecation")
  private static QuerySpec searchByAttributeName(String attrName, String attrValue) {
    QuerySpec qs = null;

    try {
      // classi nella query
      Class class0 = EPMDocument.class;
      Class class1 = StringValue.class;
      Class class2 = StringDefinition.class;

      qs = new QuerySpec();

      qs.addClassList(class0, true);

      // latest iteration
      qs.appendWhere(new SearchCondition(EPMDocument.class, Iterated.ITERATION_INFO + "." + wt.vc.IterationInfo.LATEST, SearchCondition.IS_TRUE));

      if (attrValue != null) { // se e' stata inserita una family o
                               // subfamily

        System.out.println("> adding " + attrName + " to search criteria: " + attrValue);

        int i1 = qs.appendClassList(class1, false); // StringValue
        int i2 = qs.appendClassList(class2, false); // StringDefinition

        String class1Alias = "A" + String.valueOf(i1);
        String class2Alias = "A" + String.valueOf(i2);

        // join fra WTpart e StringValue
        TableColumn tablecolumn1 = new TableColumn("A0", "idA2A2");
        TableColumn tablecolumn2 = new TableColumn(class1Alias, "idA3A4");
        qs.appendAnd();
        qs.appendWhere(new SearchCondition(tablecolumn1, SearchCondition.EQUAL, tablecolumn2));

        // join fra StringValue e StringDefinition
        TableColumn tablecolumn3 = new TableColumn(class1Alias, "idA3A6");
        TableColumn tablecolumn4 = new TableColumn(class2Alias, "idA2A2");
        qs.appendAnd();
        qs.appendWhere(new SearchCondition(tablecolumn3, SearchCondition.EQUAL, tablecolumn4));

        // StringDefinition - name = Family o Subfamily
        TableColumn tablecolumn5 = new TableColumn(class2Alias, "name");
        qs.appendAnd();
        qs.appendWhere(new SearchCondition(tablecolumn5, SearchCondition.EQUAL, new ConstantExpression(attrName)));

        // StringValue - subfamily=subfamily o family=family
        TableColumn tablecolumn6 = new TableColumn(class1Alias, "value2");
        qs.appendAnd();
        qs.appendWhere(new SearchCondition(tablecolumn6, SearchCondition.EQUAL, new ConstantExpression(attrValue)));
      }
    }
    catch (Exception e) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Query non creata", e);
    }
    return qs;
  }

  /**
   * Ricerca ottimizzata usando l'id dell'attributo su db (tramite la funzione findIdAttribute) per trovare un
   * EPMDocument tramite il valore di un attributo
   *
   * @param attrName
   *          il nome dell'attributo con il quale confrontare il suo valore
   * @param attrValue
   *          il valore dell'attributo da confrontare
   * @return il documento trovato
   */
  @SuppressWarnings("deprecation")
  private static QuerySpec searchByOptimizedAttributeName(String attName, String attrValue) {
    QuerySpec qs = null;
    LogWrapper.logMessage(LogWrapper.INFO, "Ricerca epmdocument con " + attName + "=" + attrValue);
    try {
      // System.out.println(" attrName            : " + attName);
      // System.out.println(" attrValue           : " + attrValue);

      // classi nella query
      Class class0 = EPMDocument.class;
      Class class1 = StringValue.class;

      qs = new QuerySpec();
      qs.addClassList(class0, true);

      // latest iteration
      qs.appendWhere(new SearchCondition(EPMDocument.class, Iterated.ITERATION_INFO + "." + wt.vc.IterationInfo.LATEST, SearchCondition.IS_TRUE));

      if (attrValue != null) {
        // se e' stata inserita una family o subfamily
        // System.out.println("> adding " + attName + " to search criteria: " + attrValue);

        int i1 = qs.appendClassList(class1, false); // StringValue
        String class1Alias = "A" + String.valueOf(i1);

        // join fra WTpart e StringValue
        TableColumn tablecolumn1 = new TableColumn("A0", "idA2A2");
        TableColumn tablecolumn2 = new TableColumn(class1Alias, "idA3A4");
        qs.appendAnd();
        // A0.idA2A2 = A1.idA3A4
        qs.appendWhere(new SearchCondition(tablecolumn1, SearchCondition.EQUAL, tablecolumn2));

        // join fra StringValue e StringDefinition
        TableColumn tablecolumn3 = new TableColumn(class1Alias, "idA3A6");
        qs.appendAnd();
        // idA3A6 =
        String idAttr = getIDattribute(attName);
        qs.appendWhere(new SearchCondition(tablecolumn3, SearchCondition.EQUAL, new ConstantExpression(idAttr)));

        // StringValue - subfamily=subfamily o family=family
        TableColumn tablecolumn6 = new TableColumn(class1Alias, "value2");
        qs.appendAnd();
        qs.appendWhere(new SearchCondition(tablecolumn6, SearchCondition.EQUAL, new ConstantExpression(attrValue)));
      }
    }
    catch (Exception e) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Query di ricerca non creata", e);
    }
    return qs;
  }

  /**
   * Effettua la ricerca degli EPMDocument aventi l'attributo dato con il valore richiesto
   *
   * @param nomeAttributo
   *          nome dell'attributo
   * @param valoreAttributo
   *          valore dell'attributo
   * @return la lista dei nomi degli EPMDocument trovati
   * @throws SearchUtilsException
   *           in caso di errore
   */
  public static ArrayList<String> findEPMDocumentByIBAAttribute(String attrName, String attrValue) throws SearchUtilsException {
    ArrayList<String> wtpv = new ArrayList<String>();
    try {
      QuerySpec qs = null;

      // ricerca per nome e attributo
      qs = searchByAttributeName(attrName, attrValue);

      System.out.println("\n\n ======== QUERY =======\n" + qs.toString() + "\n====================\n");

      // Get current time
      long start = System.currentTimeMillis();
      @SuppressWarnings("deprecation")
      QueryResult result = PersistenceHelper.manager.find(qs);
      // Get elapsed time in milliseconds
      long elapsedTimeMillis = System.currentTimeMillis() - start;
      // Get elapsed time in seconds
      float elapsedTimeSec = elapsedTimeMillis / 1000F;
      System.out.println("\n >>> Number of epmdocument found -> " + result.size() + " in " + elapsedTimeSec + " seconds\n");

      while (result.hasMoreElements()) {
        Persistable[] p = (Persistable[]) result.nextElement();
        EPMDocument doc = (EPMDocument) p[0];
        if (!wtpv.contains(doc.getNumber())) {
          wtpv.add(doc.getNumber());
        }
        System.out.println(" EPMDocument " + doc.getNumber() + " -versione: " + doc.getVersionIdentifier().getValue() + " -iterazione: "
            + doc.getIterationIdentifier().getValue());
      }
      System.out.println("> Returning epmdocument vector of size: " + wtpv.size());
    }
    catch (Exception e) {
      String msg = "Errore nella ricerca dei documenti con attributo " + attrName + " = " + attrValue;
      LogWrapper.logMessage(LogWrapper.ERROR, msg);
      throw new SearchUtilsException(msg, e);
    }
    return wtpv;
  }

  public static ArrayList<String> findEPMDocumentByOptimizedAttribute(String attName, String attrValue) throws SearchUtilsException {
    ArrayList<String> wtpv = new ArrayList<String>();
    try {
      QuerySpec qs = null;

      // ricerca per nome e attributo
      qs = searchByOptimizedAttributeName(attName, attrValue);

      LogWrapper.logMessage(LogWrapper.INFO, "QUERY: " + qs.toString());

      // Get current time
      long start = System.currentTimeMillis();
      @SuppressWarnings("deprecation")
      QueryResult result = PersistenceHelper.manager.find(qs);
      // Get elapsed time in milliseconds
      long elapsedTimeMillis = System.currentTimeMillis() - start;
      // Get elapsed time in seconds
      float elapsedTimeSec = elapsedTimeMillis / 1000F;
      LogWrapper.logMessage(LogWrapper.INFO, " >>> Number of epmdocument found -> " + result.size() + " in " + elapsedTimeSec + " seconds\n");

      while (result.hasMoreElements()) {
        Persistable[] p = (Persistable[]) result.nextElement();
        EPMDocument doc = (EPMDocument) p[0];
        if (!wtpv.contains(doc.getNumber())) {
          wtpv.add(doc.getNumber());
        }
        LogWrapper.logMessage(LogWrapper.INFO, " EPMDocument " + doc.getNumber() + " -versione: " + doc.getVersionIdentifier().getValue()
            + " -iterazione: " + doc.getIterationIdentifier().getValue());
      }
      LogWrapper.logMessage(LogWrapper.INFO, "> Returning epmdocument vector of size: " + wtpv.size());
    }
    catch (Exception e) {
      String msg = "Errore nella ricerca dei documenti con attributo " + attName + " = " + attrValue;
      LogWrapper.logMessage(LogWrapper.ERROR, msg);
      throw new SearchUtilsException(msg, e);
    }
    return wtpv;
  }

  // ---------------------------------------------------------------------------------------------------------------------

  public static void main(String[] args) throws Exception {

    // imposta user e pass
    user = CDTProperties.getProperty("ext.caditech.login.user", "wcadmin");
    password = CDTProperties.getProperty("ext.caditech.login.password", "wcadmin");

    // imposta il method server remoto
    RemoteMethodServer method = RemoteMethodServer.getDefault();
    method.setUserName(user);
    method.setPassword(password);

    System.out.println("CDT SearchUtils inizio");
    System.out.println("CDT - -------------------------------");

    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
    String dataI = "01/01/1900 00:00:00";
    Date dataInizio = sdf.parse(dataI);
    String dataF = "04/18/2015 00:00:00";
    Date dataFine = sdf.parse(dataF);
    ArrayList<EPMDocument> epmDocs = findLatestEPMDocuments(dataInizio, dataFine, false);
    // ArrayList<EPMDocument> epmDocs = findLatestEPMDocuments(dataInizio, false);
    System.out.println("Numero documenti trovati: " + epmDocs.size());
    int ii = 1;
    for (EPMDocument epm : epmDocs) {
      // if (epm.getNumber().equalsIgnoreCase("cilindro_prova.prt")) {
      // System.out.println("- " + epm.getNumber() + " -versione: " + epm.getVersionIdentifier().getValue() +
      // " -iterazione: "
      // + epm.getIterationIdentifier().getValue() + " -data:" + epm.getModifyTimestamp().toString() + " - dataMaster:"
      // + epm.getMaster().getPersistInfo().getCreateStamp().toString() + " -checkedOut: " +
      // WorkInProgressHelper.isCheckedOut(epm));
      System.out.println("data:" + epm.getModifyTimestamp().toString() + " - number:" + epm.getNumber() + " - #:" + ii);
      ii++;

      // }
    }

  }
}
