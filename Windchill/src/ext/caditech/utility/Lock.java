package ext.caditech.utility;

import java.util.Properties;

import wt.epm.EPMDocument;
import wt.locks.LockException;
import wt.locks.Lockable;
import wt.org.WTPrincipalReference;
import wt.org.WTUser;
import wt.pom.PersistenceException;
import wt.util.WTException;
import wt.util.WTPropertyVetoException;

public class Lock {

  public static void lock(Properties p) throws SearchUtilsException, LockException, WTPropertyVetoException, PersistenceException, WTException {
    LogWrapper.logMessage(LogWrapper.INFO, "CDT - Inizio: lock");
    String nomeEPM = p.getProperty("modello");
    EPMDocument epm = SearchEPMDocument.findLatestEPMDocuments(nomeEPM);
    Lockable lockable = null;
    String nomeUtente = p.getProperty("utente");
    if (nomeUtente != null) {
      LogWrapper.logMessage(LogWrapper.INFO, "CDT - utente: " + nomeEPM);
      WTUser user = SearchUtils.findUser(nomeUtente);
      LogWrapper.logMessage(LogWrapper.INFO, "CDT - modello: " + nomeEPM);
      WTPrincipalReference ref = WTPrincipalReference.newWTPrincipalReference(user);
      lockable = wt.locks.LockHelper.service.lock(epm, ref);
    }
    else {
      lockable = wt.locks.LockHelper.service.lock(epm);
    }
    wt.locks.Lock l = lockable.getLock();
    if (l != null) {
      LogWrapper.logMessage(LogWrapper.INFO, "CDT - Lock " + l.getDate());
      if (l.getLocker() != null) {
        LogWrapper.logMessage(LogWrapper.INFO, "CDT - locker" + l.getLocker().getName());
      }
      else {
        LogWrapper.logMessage(LogWrapper.INFO, "CDT - locker null");
      }
    }
    else {
      LogWrapper.logMessage(LogWrapper.INFO, "CDT - Lock null");
    }
    LogWrapper.logMessage(LogWrapper.INFO, "CDT - Fine: lock");
  }

  public static void assignLock(Properties p) throws SearchUtilsException, LockException, WTPropertyVetoException, PersistenceException, WTException {
    LogWrapper.logMessage(LogWrapper.INFO, "CDT - Inizio: lock");
    String nomeEPM = p.getProperty("modello");
    EPMDocument epm = SearchEPMDocument.findLatestEPMDocuments(nomeEPM);
    Lockable lockable = null;
    String nomeUtente = p.getProperty("utente", "Administrator");
    LogWrapper.logMessage(LogWrapper.INFO, "CDT - utente: " + nomeEPM);
    WTUser user = SearchUtils.findUser(nomeUtente);
    LogWrapper.logMessage(LogWrapper.INFO, "CDT - modello: " + nomeEPM);
    WTPrincipalReference ref = WTPrincipalReference.newWTPrincipalReference(user);
    lockable = wt.locks.LockHelper.assignLock(epm, ref, "");
    wt.locks.Lock l = lockable.getLock();
    if (l != null) {
      LogWrapper.logMessage(LogWrapper.INFO, "CDT - Lock " + l.getDate());
      if (l.getLocker() != null) {
        LogWrapper.logMessage(LogWrapper.INFO, "CDT - locker" + l.getLocker().getName());
      }
      else {
        LogWrapper.logMessage(LogWrapper.INFO, "CDT - locker null");
      }
    }
    else {
      LogWrapper.logMessage(LogWrapper.INFO, "CDT - Lock null");
    }
    LogWrapper.logMessage(LogWrapper.INFO, "CDT - Fine: lock");
  }

  public static boolean isLocked(Properties p) throws SearchUtilsException {
    LogWrapper.logMessage(LogWrapper.INFO, "CDT - Inizio: isLocked");
    String nomeEPM = p.getProperty("modello");
    LogWrapper.logMessage(LogWrapper.INFO, "CDT - modello:" + nomeEPM);
    EPMDocument epm = SearchEPMDocument.findLatestEPMDocuments(nomeEPM);
    boolean locked = epm.isLocked();
    LogWrapper.logMessage(LogWrapper.INFO, "CDT - Il modello " + nomeEPM + " � locked? " + locked);
    if (locked) {
      wt.locks.Lock l = epm.getLock();
      if (l != null) {
        LogWrapper.logMessage(LogWrapper.INFO, "CDT - Lock " + l.getDate());
        if (l.getLocker() != null) {
          LogWrapper.logMessage(LogWrapper.INFO, "CDT - locker" + l.getLocker().getName());
        }
        else {
          LogWrapper.logMessage(LogWrapper.INFO, "CDT - locker null");
        }
      }
      else {
        LogWrapper.logMessage(LogWrapper.INFO, "CDT - Lock null");
      }
    }
    LogWrapper.logMessage(LogWrapper.INFO, "CDT - Fine: isLocked");
    return locked;
  }

  public static void unlock(Properties p) throws SearchUtilsException, LockException, WTPropertyVetoException, PersistenceException, WTException {
    LogWrapper.logMessage(LogWrapper.INFO, "CDT - -------------------------------");
    LogWrapper.logMessage(LogWrapper.INFO, "CDT - Inizio: testUnlock");
    String nomeEPM = p.getProperty("modello");
    EPMDocument epm = SearchEPMDocument.findLatestEPMDocuments(nomeEPM);
    wt.locks.Lock l = epm.getLock();
    if (l != null) {
      LogWrapper.logMessage(LogWrapper.INFO, "CDT - Lock " + l.getDate());
      if (l.getLocker() != null) {
        LogWrapper.logMessage(LogWrapper.INFO, "CDT - locker" + l.getLocker().getName());
      }
      else {
        LogWrapper.logMessage(LogWrapper.INFO, "CDT - Locker null");
      }
    }
    else {
      LogWrapper.logMessage(LogWrapper.INFO, "CDT - Lock null");
    }
    wt.locks.LockHelper.service.unlock(epm);
    LogWrapper.logMessage(LogWrapper.INFO, "CDT - Fine: testUnlock");
  }

  public static void releaseLock(Properties p) throws SearchUtilsException, LockException, WTPropertyVetoException, PersistenceException, WTException {
    LogWrapper.logMessage(LogWrapper.INFO, "CDT - -------------------------------");
    LogWrapper.logMessage(LogWrapper.INFO, "CDT - Inizio: testUnlock");
    String nomeEPM = p.getProperty("modello");
    EPMDocument epm = SearchEPMDocument.findLatestEPMDocuments(nomeEPM);
    wt.locks.Lock l = epm.getLock();
    if (l != null) {
      LogWrapper.logMessage(LogWrapper.INFO, "CDT - Lock " + l.getDate());
      if (l.getLocker() != null) {
        LogWrapper.logMessage(LogWrapper.INFO, "CDT - locker" + l.getLocker().getName());
      }
      else {
        LogWrapper.logMessage(LogWrapper.INFO, "CDT - Locker null");
      }
    }
    else {
      LogWrapper.logMessage(LogWrapper.INFO, "CDT - Lock null");
    }
    wt.locks.LockHelper.releaseLock(epm);
    LogWrapper.logMessage(LogWrapper.INFO, "CDT - Fine: testUnlock");
  }
}
