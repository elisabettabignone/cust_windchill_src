package ext.caditech.utility;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Properties;

import org.junit.Before;
import org.junit.Test;

import wt.method.RemoteMethodServer;

public class Attributes_IT {

  @Before
  public void setUp() {
    // imposta user e pass
    RemoteMethodServer method = RemoteMethodServer.getDefault();
    method.setUserName("wcadmin");
    method.setPassword("wcadmin");
  }

  @Test
  public void TestLetturaAttributi() {
    System.out.println("--------------------------------");
    Properties p = new Properties();
    p.put("modo", "GET");
    p.put("modello", "PARALL_PROVA.PRT");
    p.put("nome", "CODICE");
    System.out.println("Get dell'attributo CODICE di parall_prova.prt");
    try {
      String value = Attributes.eseguiComeServer(p);
      System.out.println("Valore trovato: " + value);
      assertTrue(value.equalsIgnoreCase("parall_prova__"));
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }
  }

  @Test
  public void TestScritturaAttributi() {
    System.out.println("--------------------------------");
    Properties pSet = new Properties();
    pSet.put("modo", "SET");
    pSet.put("modello", "cilindro_prova.prt");
    pSet.put("nome", "att_string");
    pSet.put("valore", "stringa con spazi");

    Properties pGet = new Properties();
    pGet.put("modo", "GET");
    pGet.put("modello", "cilindro_prova.prt");
    pGet.put("nome", "att_string");

    try {
      // setta l'attributo
      System.out.println("Set dell'attributo att_string di cilindro_prova.prt");
      Attributes.eseguiComeServer(pSet);
      // lo recupera per verifica
      System.out.println("Get dell'attributo att_string di cilindro_prova.prt");
      String value = Attributes.eseguiComeServer(pGet);
      System.out.println("Valore trovato: " + value);
      assertTrue(value.equalsIgnoreCase("stringa con spazi"));
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }
  }
}
