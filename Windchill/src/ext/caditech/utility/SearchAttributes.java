package ext.caditech.utility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Locale;

import wt.fc.PersistenceHelper;
import wt.fc.QueryResult;
import wt.iba.definition.StringDefinition;
import wt.method.RemoteMethodServer;
import wt.query.QuerySpec;
import wt.query.SearchCondition;
import wt.util.WTContext;
import wt.util.WTException;

import com.ptc.core.command.common.CommandException;
import com.ptc.core.lwc.server.PersistableAdapter;
import com.ptc.core.meta.common.AttributeTypeIdentifier;
import com.ptc.core.meta.common.AttributeTypeIdentifierSet;
import com.ptc.core.meta.common.TypeIdentifier;
import com.ptc.core.meta.common.impl.InstanceBasedAttributeTypeIdentifier;
import com.ptc.core.meta.common.impl.LogicalIdentifierFactory;
import com.ptc.core.meta.type.command.typemodel.common.GetHardSchemaAttributesCommand;
import com.ptc.core.meta.type.command.typemodel.common.GetSoftSchemaAttributesCommand;

/**
 * Classe di supporto per le ricerche sugli attributi
 *
 * @author administrator
 *
 */
public class SearchAttributes {

  /** L'username con cui autenticarsi in Windchill */
  static private String user = "";

  /** La password con cui autenticarsi in Windchill */
  static private String password = "";

  /**
   * Recupera la lista di attributi "soft", dato il nome del tipo di documento
   *
   * @param internalName
   *          il nome del tipo di documento
   * @return la lista di attributi
   */
  public static ArrayList<String> getSoftAttributesName(String internalName) {
    ArrayList<String> logicalNames = new ArrayList<String>();
    LogicalIdentifierFactory factory = new LogicalIdentifierFactory();
    TypeIdentifier ti = factory.newWCTypeIdentifier(internalName);
    GetSoftSchemaAttributesCommand command = new GetSoftSchemaAttributesCommand();
    command.setType_id(ti);
    try {
      command = (GetSoftSchemaAttributesCommand) command.execute();
      AttributeTypeIdentifierSet atis = command.getAttributes();
      Iterator iter = atis.iterator();
      while (iter.hasNext()) {
        InstanceBasedAttributeTypeIdentifier ati = (InstanceBasedAttributeTypeIdentifier) iter.next();
        if (!ati.getAttributeName().equals("view")) {
          try {
            String name = ati.getAttributeName();
            logicalNames.add(name);
          }
          catch (Exception e) {
            e.printStackTrace();
          }
        }
      }
    }
    catch (CommandException e) {
      e.printStackTrace();
    }
    return logicalNames;
  }

  /**
   * Recupera la lista di attributi "hard", dato il nome del tipo di documento
   *
   * @param className
   *          il nome del tipo di documento
   * @return la lista di attributi
   */
  public static ArrayList<String> getHardAttributesName(String className) {
    ArrayList<String> attributes = new ArrayList<String>();
    LogicalIdentifierFactory factory = new LogicalIdentifierFactory();
    TypeIdentifier ti = factory.newWCTypeIdentifier(className);
    GetHardSchemaAttributesCommand command = new GetHardSchemaAttributesCommand();
    command.setType_id(ti);
    try {
      command = (GetHardSchemaAttributesCommand) command.execute();
      AttributeTypeIdentifierSet atis = command.getAttributes();
      Iterator iter = atis.iterator();
      while (iter.hasNext()) {
        AttributeTypeIdentifier ati = (AttributeTypeIdentifier) iter.next();
        if (!ati.getAttributeName().equals("view")) {
          String name = ati.getAttributeName();
          attributes.add(name);
        }
      }
    }
    catch (CommandException e) {
      e.printStackTrace();
    }
    return attributes;
  }

  /**
   * Recupera la mappatura < attributo, tipo > per il tipo documento voluto, degli attributi soft.
   * 
   * @param internalTypeName
   *          il nome interno del tipo documento per il quale si vogliono gli attributi
   * @return LinkedHashMap<String, String> la mappa < nomeAttributo, tipoAttirbuto > per il tipo di documento passato
   */
  public static LinkedHashMap<String, String> getSoftAttributesType(String internalTypeName) {
    LinkedHashMap<String, String> res = new LinkedHashMap<String, String>();

    try {
      Locale locale = Locale.getDefault();
      // Locale locale = WTContext.getContext().getLocale();

      PersistableAdapter pa = new PersistableAdapter(internalTypeName, locale, null);

      ArrayList<String> softAttributes = SearchAttributes.getSoftAttributesName(internalTypeName);
      Collections.sort(softAttributes);

      // cicla su tutti gli attributi trovati
      for (String sa : softAttributes) {
        pa.load(sa);
        String type = "";
        if (pa.getAttributeDescriptor(sa).getDataType() != null)
          type = pa.getAttributeDescriptor(sa).getDataType().toString();

        // accoppia nella mappa nome <-> tipo
        res.put(sa, type);
        LogWrapper.logMessage(LogWrapper.INFO, "CDT: soft attribute: " + sa + " -> type: " + type);
      }
    }
    catch (WTException e) {
      e.printStackTrace();
    }

    return res;
  }

  /**
   * Recupera la mappatura < attributo, tipo > per il tipo documento voluto, degli attributi hard.
   * 
   * @param internalTypeName
   *          il nome interno del tipo documento per il quale si vogliono gli attributi
   * @return Hashtable<String, String> la mappa < nomeAttributo, tipoAttributo > per il tipo di documento passato
   */
  public static Hashtable<String, String> getHardAttributesType(String internalTypeName) {
    Hashtable<String, String> res = new Hashtable<String, String>();

    try {
      PersistableAdapter pa = new PersistableAdapter(internalTypeName, WTContext.getContext().getLocale(), null);

      ArrayList<String> hardAttributes = SearchAttributes.getHardAttributesName(internalTypeName);
      Collections.sort(hardAttributes);

      // cicla su tutti gli attributi trovati
      for (String ha : hardAttributes) {
        pa.load(ha);
        String type = "";
        if (pa.getAttributeDescriptor(ha).getDataType() != null)
          type = pa.getAttributeDescriptor(ha).getDataType().toString();

        // accoppia nella mappa nome <-> tipo
        res.put(ha, type);
      }
    }
    catch (WTException e) {
      e.printStackTrace();
    }

    return res;
  }

  /**
   * Resitituisce l'ID sul database dell'attributo passato
   *
   * @param attributeName
   *          il nome dell'attributo
   * @return l'ID nel database
   */
  @SuppressWarnings("deprecation")
  public static String searchIDattribute(String attributeName) {
    String res = "";

    try {
      QuerySpec findCriteria = new QuerySpec(StringDefinition.class);
      String operator = SearchCondition.EQUAL;
      findCriteria.appendSearchCondition(new SearchCondition(StringDefinition.class, StringDefinition.NAME, operator, attributeName));
      QueryResult findResult = PersistenceHelper.manager.find(findCriteria);

      if (findResult.size() > 0) {
        StringDefinition sd = (StringDefinition) findResult.nextElement();
        String tmp[] = sd.getIdentity().split(":");
        res = tmp[1];
      }
    }
    catch (Exception e) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore nella ricerca dell'ID dell'attributo " + attributeName, e);
      e.printStackTrace();
    }
    return res;
  }

  public static void main(String[] args) throws Exception {
    // imposta user e pass
    user = CDTProperties.getProperty("ext.caditech.login.user", "wcadmin");
    password = CDTProperties.getProperty("ext.caditech.login.password", "wcadmin");

    // imposta il method server remoto
    RemoteMethodServer method = RemoteMethodServer.getDefault();
    method.setUserName(user);
    method.setPassword(password);

    System.out.println("CDT Main inizio");

    String nomeTipo = "ge.caditech.DefaultEPMDocument";

    System.out.println("CDT - -------------------------------");
    System.out.println("CDT - Tipo inserito -> " + nomeTipo);

    System.out.println("CDT - ------- HARD ATTRIBUTES");
    Hashtable<String, String> hard = SearchAttributes.getHardAttributesType(nomeTipo);
    for (String chiave : hard.keySet()) {
      System.out.println(chiave + " --> " + hard.get(chiave));
    }
    System.out.println("CDT - -------------------------------");

    System.out.println("CDT - ------- SOFT ATTRIBUTES");
    LinkedHashMap<String, String> soft = SearchAttributes.getSoftAttributesType(nomeTipo);
    for (String chiave : soft.keySet()) {
      System.out.println(chiave + " --> " + soft.get(chiave));
    }
    System.out.println("CDT - -------------------------------");
    System.out.println("CDT Main fine");
  }

}
