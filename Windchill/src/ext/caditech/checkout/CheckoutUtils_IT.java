package ext.caditech.checkout;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.junit.Test;

import ext.caditech.utility.SearchUtilsException;

public class CheckoutUtils_IT {

  // -------------------------------------------------------------------------------
  // Percorsi dei file di esito
  // -------------------------------------------------------------------------------
  private String pathEsitoDrawingTrue = "C:\\temp\\Windchill_test\\esito_copia_locale_drawing_true.txt";

  private String pathEsitoDrawingFalse = "C:\\temp\\Windchill_test\\esito_copia_locale_drawing_false.txt";

  private String pathEsitoDipRequired = "C:\\temp\\Windchill_test\\esito_copia_locale_dip_required.txt";

  private String pathEsitoDipNone = "C:\\temp\\Windchill_test\\esito_copia_locale_dip_none.txt";

  private String pathEsitoAsStored = "C:\\temp\\Windchill_test\\esito_copia_locale_as_stored.txt";

  private String pathEsitoLatest = "C:\\temp\\Windchill_test\\esito_copia_locale_latest.txt";

  /**
   * Test per il popolamento dei drawing: viene controllato che i disegni siano stati prelevati
   */
  @Test
  public void testCopiaLocaleConDrwTrue() {
    System.out.println("\n------------------------------------------------------------");
    String args[] = { "test=true", "modelli=ASSIEME_ES5.ASM", "dipendenze=ALL", "workspace=ws_test_checkout", "eliminaws=true", "asstored=latest",
        "report=C:\\Temp\\Windchill_test\\esito_copia_locale_drawing_true.txt", "drawing=true", "login=Administrator", "modo=copia" };
    try {
      CheckoutUtils.main(args);
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }

    File esito = new File(pathEsitoDrawingTrue);
    assertNotNull(esito);
    assertTrue(esito.exists());
    assertTrue(esito.length() > 0);
    try {
      BufferedReader bfr = new BufferedReader(new FileReader(esito));
      String epmdocument = null;
      // boolean trovatoDrwAssieme = false;
      boolean trovatoDrwParall = false;
      while ((epmdocument = bfr.readLine()) != null) {
        String[] split = epmdocument.split(";");
        if (split[0].equalsIgnoreCase("parall.drw")) {
          trovatoDrwParall = true;
        }
        // else if (split[0].equalsIgnoreCase("assieme_es5.drw")) {
        // trovatoDrwAssieme = true;
        // }
      }
      bfr.close();
      assertTrue(trovatoDrwParall);
      // assertTrue(trovatoDrwAssieme);
    }
    catch (FileNotFoundException e) {
      fail(e.getMessage());
      e.printStackTrace();
    }
    catch (IOException e) {
      fail(e.getMessage());
      e.printStackTrace();
    }
  }

  /**
   * Test per il NON popolamento dei drawing: viene controllato che i disegni NON siano stati prelevati
   */
  @Test
  public void testCopiaLocaleConDrwFalse() {
    System.out.println("\n------------------------------------------------------------");
    String args[] = { "test=true", "modelli=ASSIEME_ES5.ASM", "dipendenze=ALL", "workspace=ws_test_checkout", "eliminaws=true", "asstored=latest",
        "report=C:\\Temp\\Windchill_test\\esito_copia_locale_drawing_false.txt", "drawing=false", "login=Administrator", "modo=copia" };
    try {
      CheckoutUtils.main(args);
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }

    File esito = new File(pathEsitoDrawingFalse);
    assertNotNull(esito);
    assertTrue(esito.exists());
    assertTrue(esito.length() > 0);

    try {
      BufferedReader bfr = new BufferedReader(new FileReader(esito));
      String epmdocument = null;
      // boolean trovatoDrwAssieme = false;
      boolean trovatoDrwParall = false;
      while ((epmdocument = bfr.readLine()) != null) {
        String[] split = epmdocument.split(";");
        if (split[0].equalsIgnoreCase("parall.drw")) {
          trovatoDrwParall = true;
        }
      }
      bfr.close();
      assertFalse(trovatoDrwParall);
    }
    catch (FileNotFoundException e) {
      fail(e.getMessage());
      e.printStackTrace();
    }
    catch (IOException e) {
      fail(e.getMessage());
      e.printStackTrace();
    }
  }

  /**
   * Test per il popolamento con dipendenze REQUIRED: viene controllato che venga prelevato solo assieme_es5.asm e non i
   * suoi sottocmponenti parall.prt e cilindro.prt
   */
  @Test
  public void testCopiaLocaleConDipRequired() {
    System.out.println("\n------------------------------------------------------------");
    String args[] = { "test=true", "modelli=ASSIEME_ES5.ASM", "dipendenze=REQUIRED", "workspace=ws_test_checkout", "eliminaws=true",
        "asstored=latest", "report=C:\\Temp\\Windchill_test\\esito_copia_locale_dip_required.txt", "drawing=false", "login=Administrator",
        "modo=copia" };
    try {
      CheckoutUtils.main(args);
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }

    File esito = new File(pathEsitoDipRequired);
    assertNotNull(esito);
    assertTrue(esito.exists());
    assertTrue(esito.length() > 0);

    try {
      BufferedReader bfr = new BufferedReader(new FileReader(esito));
      String epmdocument = null;
      boolean trovatoAssieme = false;
      boolean trovatoParall = false;
      boolean trovatoCilindro = false;
      while ((epmdocument = bfr.readLine()) != null) {
        String[] split = epmdocument.split(";");
        if (split[0].equalsIgnoreCase("parall.prt")) {
          trovatoParall = true;
        }
        else if (split[0].equalsIgnoreCase("assieme_es5.asm")) {
          trovatoAssieme = true;
        }
        else if (split[0].equalsIgnoreCase("cilindro.prt")) {
          trovatoCilindro = true;
        }
      }
      bfr.close();
      assertTrue(trovatoAssieme);
      assertTrue(trovatoParall);
      assertTrue(trovatoCilindro);
    }
    catch (FileNotFoundException e) {
      fail(e.getMessage());
      e.printStackTrace();
    }
    catch (IOException e) {
      fail(e.getMessage());
      e.printStackTrace();
    }
  }

  /**
   * Test per il popolamento con dipendenze NONE: viene controllato che venga prelevato solo assieme_es5.asm e non i
   * suoi sottocmponenti parall.prt e cilindro.prt
   */
  @Test
  public void testCopiaLocaleConDipNone() {
    System.out.println("\n------------------------------------------------------------");
    String args[] = { "test=true", "modelli=ASSIEME_ES5.ASM", "dipendenze=NONE", "workspace=ws_test_checkout", "eliminaws=true", "asstored=latest",
        "report=C:\\Temp\\Windchill_test\\esito_copia_locale_dip_none.txt", "drawing=false", "login=Administrator", "modo=copia" };
    try {
      CheckoutUtils.main(args);
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }

    File esito = new File(pathEsitoDipNone);
    assertNotNull(esito);
    assertTrue(esito.exists());
    assertTrue(esito.length() > 0);

    try {
      BufferedReader bfr = new BufferedReader(new FileReader(esito));
      String epmdocument = null;
      boolean trovatoAssieme = false;
      boolean trovatoParall = false;
      boolean trovatoCilindro = false;
      while ((epmdocument = bfr.readLine()) != null) {
        String[] split = epmdocument.split(";");
        if (split[0].equalsIgnoreCase("parall.prt")) {
          trovatoParall = true;
        }
        else if (split[0].equalsIgnoreCase("assieme_es5.asm")) {
          trovatoAssieme = true;
        }
        else if (split[0].equalsIgnoreCase("cilindro.prt")) {
          trovatoCilindro = true;
        }
      }
      bfr.close();
      assertTrue(trovatoAssieme);
      assertFalse(trovatoParall);
      assertFalse(trovatoCilindro);
    }
    catch (FileNotFoundException e) {
      fail(e.getMessage());
      e.printStackTrace();
    }
    catch (IOException e) {
      fail(e.getMessage());
      e.printStackTrace();
    }
  }

  /**
   * Effettua la copia locale di ASSIEME_ES5.ASM e 01430000.ASM in modalit� AS_STORED. Devo controllare che alla fine
   * della copia locale che siano presenti i sottocomponenti CILINDRO.PRT con versione=235 e 01430000_009.PRT con
   * versione=627
   */
  @Test
  public void testCopiaLocaleAsStored() {
    System.out.println("\n------------------------------------------------------------");
    String args[] = { "test=true", "modelli=01430000.asm;ASSIEME_ES5.ASM", "dipendenze=ALL", "workspace=ws_test_checkout", "eliminaws=true",
        "asstored=asStored", "report=C:\\Temp\\Windchill_test\\esito_copia_locale_as_stored.txt", "drawing=false", "login=Administrator",
        "modo=copia" };
    try {
      CheckoutUtils.main(args);
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }

    File esito = new File(pathEsitoAsStored);
    assertNotNull(esito);
    assertTrue(esito.exists());
    assertTrue(esito.length() > 0);
    boolean trovatoCilindro = false;
    boolean trovato009 = false;
    try {
      BufferedReader bfr = new BufferedReader(new FileReader(esito));
      String epmdocument = null;
      while ((epmdocument = bfr.readLine()) != null) {
        String[] split = epmdocument.split(";");
        if (split[0].equalsIgnoreCase("cilindro.prt")) {
          assertEquals("A", split[1]);
          assertEquals("1", split[2]);
          trovatoCilindro = true;
        }
        else if (split[0].equalsIgnoreCase("01430000_009.prt")) {
          assertEquals("A", split[1]);
          assertEquals("1", split[2]);
          trovato009 = true;
        }
      }
      bfr.close();
      assertTrue(trovato009);
      assertTrue(trovatoCilindro);
    }
    catch (FileNotFoundException e) {
      fail(e.getMessage());
      e.printStackTrace();
    }
    catch (IOException e) {
      fail(e.getMessage());
      e.printStackTrace();
    }
  }

  /**
   * Effettua la copia locale di ASSIEME_ES5.ASM e 01430000.ASM in modalit� LATEST. Devo controllare che alla fine della
   * copia locale che siano presenti i sottocomponenti CILINDRO.PRT con versione=238 e 01430000_009.PRT con versione=628
   */
  @Test
  public void testCopiaLocaleLatest() {
    System.out.println("\n------------------------------------------------------------");
    String args[] = { "test=true", "modelli=01430000.asm;ASSIEME_ES5.ASM", "dipendenze=ALL", "workspace=ws_test_checkout", "eliminaws=true",
        "asstored=latest", "report=C:\\Temp\\Windchill_test\\esito_copia_locale_latest.txt", "drawing=false", "login=Administrator", "modo=copia" };
    try {
      CheckoutUtils.main(args);
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }

    File esito = new File(pathEsitoLatest);
    assertNotNull(esito);
    assertTrue(esito.exists());
    assertTrue(esito.length() > 0);
    boolean trovatoCilindro = false;
    boolean trovato009 = false;
    try {
      BufferedReader bfr = new BufferedReader(new FileReader(esito));
      String epmdocument = null;
      while ((epmdocument = bfr.readLine()) != null) {
        String[] split = epmdocument.split(";");
        if (split[0].equalsIgnoreCase("cilindro.prt")) {
          assertEquals("A", split[1]);
          assertEquals("2", split[2]);
          trovatoCilindro = true;
        }
        else if (split[0].equalsIgnoreCase("01430000_009.prt")) {
          assertEquals("A", split[1]);
          assertEquals("1", split[2]);
          trovato009 = true;
        }
      }
      bfr.close();
      assertTrue(trovato009);
      assertTrue(trovatoCilindro);
    }
    catch (FileNotFoundException e) {
      fail(e.getMessage());
      e.printStackTrace();
    }
    catch (IOException e) {
      fail(e.getMessage());
      e.printStackTrace();
    }
  }

  /**
   * Effettua il checkout del CILINDRO.PRT, dopo aver fatto la copia locale di ASSIEME_ES5.ASM e 01430000.ASM, dove �
   * contenuto anche il cilindro, come sottocomponente
   * 
   * @throws SearchUtilsException
   */
  @Test
  public void testCheckoutEundoCheckout() throws SearchUtilsException {
    System.out.println("\n------------------------------------------------------------");
    // esegue la copia in locale
    String args[] = { "test=true", "modelli=01430000.asm;ASSIEME_ES5.ASM", "dipendenze=ALL", "workspace=ws_test_checkout", "eliminaws=true",
        "asstored=latest", "report=C:\\Temp\\Windchill_test\\esito_copia_locale_latest.txt", "drawing=false", "login=Administrator", "modo=copia" };
    try {
      CheckoutUtils.main(args);
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }

    File esito = new File(pathEsitoLatest);
    assertNotNull(esito);
    assertTrue(esito.exists());
    assertTrue(esito.length() > 0);
    System.out.println("\n------------------------------------------------------------");
    // esegue il checkout di uno dei file copiati
    String args2[] = { "test=true", "modelli=cilindro.prt", "workspace=ws_test_checkout", "login=Administrator", "modo=checkout" };
    try {
      CheckoutUtils.main(args2);
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }
    assertTrue(CheckoutUtils.isCheckedOut("cilindro.prt"));
    assertFalse(CheckoutUtils.isCheckedOut("assieme_es5.asm"));

    // esegue il checkout di uno dei file copiati
    String args3[] = { "test=true", "modelli=cilindro.prt", "modo=undo" };
    try {
      CheckoutUtils.main(args3);
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }
    assertFalse(CheckoutUtils.isCheckedOut("cilindro.prt"));
  }
}
