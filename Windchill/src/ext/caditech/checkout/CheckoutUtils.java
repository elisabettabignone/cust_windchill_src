package ext.caditech.checkout;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;

import wt.enterprise.RevisionControlled;
import wt.epm.EPMDocConfigSpec;
import wt.epm.EPMDocument;
import wt.epm.workspaces.EPMAsStoredConfigSpec;
import wt.epm.workspaces.EPMBaselineHelper;
import wt.epm.workspaces.EPMPopulateRule;
import wt.epm.workspaces.EPMWorkspace;
import wt.epm.workspaces.EPMWorkspaceHelper;
import wt.fc.ObjectReference;
import wt.fc.PersistenceHelper;
import wt.fc.QueryResult;
import wt.fc.collections.WTArrayList;
import wt.fc.collections.WTSet;
import wt.inf.container.WTContainerHelper;
import wt.inf.container.WTContainerRef;
import wt.method.RemoteMethodServer;
import wt.org.WTPrincipal;
import wt.org.WTUser;
import wt.query.QuerySpec;
import wt.query.SearchCondition;
import wt.session.SessionHelper;
import wt.util.WTException;
import wt.util.WTPropertyVetoException;
import wt.vc.wip.WorkInProgressHelper;
import wt.vc.wip.Workable;
import ext.caditech.utility.CDTProperties;
import ext.caditech.utility.LogWrapper;
import ext.caditech.utility.SearchEPMDocument;
import ext.caditech.utility.SearchUtils;
import ext.caditech.utility.SearchUtilsException;

/**
 * Classe che si occupa di effettuare il checkout da Windchill di una serie di modelli
 * 
 * @author s.menocci
 * @author e.parodi
 *
 */

public class CheckoutUtils implements wt.method.RemoteAccess {

  static final String VERSION = "1.00.00";

  static final boolean SERVER;

  static final String CLASSNAME = "ext.caditech.checkout.CheckoutUtils";

  /** L'username con cui autenticarsi in Windchill */
  static private String user = "";

  /** La password con cui autenticarsi in Windchill */
  static private String password = "";

  /** url del servizio */
  public final static String URL = "url";

  /** user name con il quale collegarsi al servizio */
  public final static String USER = "user";

  /** password dell'utente con il quale ci si collega al servizio */
  public final static String PASS = "pass";

  /** La lista dei modelli da popolare, separati da ; */
  public final static String MODELLI = "modelli";

  /** Opzione per popolare anche i drawing. Pu� valere TRUE o FALSE, default FALSE */
  public final static String DRAWING = "drawing";

  /** Opzione per il percorso del file di report, default C:\temp\copiaLocale_windchill.txt */
  public final static String REPORT = "report";

  /** Opzione per il tipo di checkout da effettuare. Pu� valere ASSTORED o LATEST, default LATEST */
  public final static String AS_STORED = "asstored";

  /** Opzione per lo svuotamento del WS nel caso esista gi�. Pu� valere TRUE o FALSE, default TRUE */
  public final static String ELIMINA_WS = "eliminaws";

  /** Opzione che indica il nome del workspace, default ws_default */
  public final static String WORKSPACE = "workspace";

  /** Opzione per il tipo di dipendenze da considerare. Pu� valere ALL, REQUIRED o NONE, default REQUIRED */
  public final static String DIPENDENZE = "dipendenze";

  /** Login utilizzata per cercare/creare il workspace, default Administrator */
  public final static String LOGIN = "login";

  /** La modalit� di lancio per testare le funzionalit� da linea di comando. Pu� valere COPIA, CHECKOUT o UNDO */
  private final static String MODO = "modo";

  /** flag per i test automatici, vale false sempre a parte per il lancio da test automatici */
  private static final String TEST = "test";

  /** Il file di log (del method server) */
  // private final static Logger log = LogManager.getLogger(Thread.currentThread().getStackTrace()[1].getClassName());

  static {
    SERVER = RemoteMethodServer.ServerFlag;
  }

  /**
   * Il costruttore
   */
  public CheckoutUtils() {
  }

  /**
   * Effettua il popolamento
   * 
   * @param documentiEPM
   *          documenti EPMDocument da popolare
   * @param epmw_find
   *          workspace da popolare
   * @param dipendenze
   *          tipo di dipendenze (ALL, REQUIRED, NONE)
   * @param asStored
   *          asStored/latest
   * @param drawing
   *          se <code>true</code> viene effettuato anche il polamento dei drawing
   * @throws CheckoutPDMLinkException
   *           in caso di errore
   * @throws SearchUtilsException
   */
  private void eseguiPopolamento(Vector<EPMDocument> documentiEPM, EPMWorkspace epmw_find, String dipendenze, boolean asStored, boolean drawing)
      throws CheckoutPDMLinkException, SearchUtilsException {
    // --------------------------------
    // POPOLAMENTO DEGLI OGGETTI IN LISTA
    // --------------------------------

    if (documentiEPM == null) {
      throw new CheckoutPDMLinkException("il Vector di documenti � NULL");
    }
    if (epmw_find == null) {
      throw new CheckoutPDMLinkException("il workspace � NULL");
    }
    // Ciclo su tutti i documenti e ne faccio il checkout singolarmente
    LogWrapper.logMessage(LogWrapper.INFO, "Eseguo il popolamento per " + documentiEPM.toString() + " nel workspace " + epmw_find.getName()
        + " con dipendenze=" + dipendenze + " - asStored=" + asStored + " -drawing=" + drawing);
    for (EPMDocument epmDocument : documentiEPM) {
      LogWrapper.logMessage(LogWrapper.DEBUG, "Inizio la copia in locale di: " + epmDocument.getNumber());
      copiaLocaleEMPDocument(epmDocument, epmw_find, asStored, dipendenze);
      LogWrapper.logMessage(LogWrapper.DEBUG, "Finita la copia in locale di: " + epmDocument.getNumber());
    }
    // ----------------------------------------------------------------
    // Recupero gli oggetti che sono nel workspace
    // ----------------------------------------------------------------
    WTSet oggettiNelWorkspace = null;
    try {
      oggettiNelWorkspace = EPMWorkspaceHelper.manager.getObjectsInWorkspace(epmw_find, EPMDocument.class);
      LogWrapper.logMessage(LogWrapper.INFO, "Ci sono " + oggettiNelWorkspace.size() + " elementi nel ws " + epmw_find.getName());
    }
    catch (WTException e1) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore ricercando oggetti nel workspace ", e1);
      throw new CheckoutPDMLinkException("Errore ricercando oggetti nel workspace ", e1);
    }
    // --------------------------------
    // POPOLAMENTO DISEGNI
    // --------------------------------
    // se l'opzione per i drawing � attiva ciclo su tutti i documenti e cerco un DRW con lo stesso nome del modello
    if (drawing) {

      LogWrapper.logMessage(LogWrapper.INFO, "Inizio popolamento dei disegni");

      for (Object or : oggettiNelWorkspace) {

        EPMDocument epmDocument = (EPMDocument) ((ObjectReference) or).getObject();
        List<EPMDocument> disegniDaDipendenze = SearchEPMDocument.findDrawing(epmDocument, epmw_find);
        for (EPMDocument disegno : disegniDaDipendenze) {
          LogWrapper.logMessage(LogWrapper.INFO, "Faccio la copia in locale di " + disegno.getNumber());
          copiaLocaleEMPDocument(disegno, epmw_find, asStored, dipendenze);
        }
      }
      LogWrapper.logMessage(LogWrapper.INFO, "Fine popolamento dei disegni");
    }
  }

  /**
   * Esegue il checkout. Questa funzione assume che prima sia stata effettuata una copia in locale dei modelli voluti.
   * 
   * @param listaModelli
   *          nomi dei modelli di cui effettuare il checkout separati da ;
   * @param nomeWS
   *          nome del workspace
   * @param nomeUtente
   *          nome dell'utente a cui deve essere assegnato il checkout
   * @return lista contenente i nomi dei file dei quali � stato fatto il checkout
   * @throws SearchUtilsException
   *           in caso di errore nella ricerca di documenti/wokspace/utente
   * @throws CheckoutPDMLinkException
   *           in caso di errore
   */
  public ArrayList<String> eseguiCheckout(String listaModelli, String nomeWS, String nomeUtente) throws SearchUtilsException,
      CheckoutPDMLinkException {
    LogWrapper.logMessage(LogWrapper.INFO, "-----------------------------------------------------------");
    LogWrapper.logMessage(LogWrapper.INFO, "--------------- CHECKOUT " + CheckoutUtils.VERSION + " --------------");
    LogWrapper.logMessage(LogWrapper.INFO, "-----------------------------------------------------------");

    // ----------------------------------------------
    // Recupero l'utente wcadmin
    // ----------------------------------------------
    WTPrincipal wcadmin = null;
    try {
      wcadmin = SessionHelper.manager.getPrincipal();
    }
    catch (WTException e) {
      String msg = "Errore recuperando il principal della sessione";
      LogWrapper.logMessage(LogWrapper.ERROR, msg, e);
      throw new SearchUtilsException(msg, e);
    }

    if (listaModelli == null || "".equals(listaModelli)) {
      throw new SearchUtilsException("Nessun modello passato in input!");
    }

    List<String> modelli = Arrays.asList(listaModelli.split(";"));
    LogWrapper.logMessage(LogWrapper.INFO, "Inizio checkout di " + modelli.toString());
    WTArrayList documentiEPM = new WTArrayList();

    // --------------------------------------------------------
    // Cerco i documenti di cui bisogna fare il checkout
    // --------------------------------------------------------
    List<String> documentiNonTrovati = new ArrayList<String>();
    for (String modello : modelli) {
      EPMDocument epm = SearchEPMDocument.findLatestEPMDocuments(modello);
      if (epm != null) {
        documentiEPM.add(epm);
        LogWrapper.logMessage(LogWrapper.INFO, "Documento con number = " + epm.getNumber() + " aggiunto alla lista per il checkout.");
      }
      else {
        documentiNonTrovati.add(modello);
        LogWrapper.logMessage(LogWrapper.WARN, "Documento con number = " + modello + " non trovato.");
      }
    }

    // --------------------------------------------------------
    // Cerco il container
    // --------------------------------------------------------
    WTContainerRef containerRef = SearchUtils.findContainer();
    if (containerRef == null) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Container non trovato");
      throw new SearchUtilsException("Container non trovato");
    }
    else {
      LogWrapper.logMessage(LogWrapper.INFO, "Container trovato");
    }

    // --------------------------------------------------------
    // Cerco l'utente
    // --------------------------------------------------------
    WTUser user = SearchUtils.findUser(nomeUtente);
    if (user == null) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Utente " + nomeUtente + " non trovato");
      throw new SearchUtilsException("Utente " + nomeUtente + " non trovato");
    }
    LogWrapper.logMessage(LogWrapper.INFO, "Utente " + user.getFullName() + " trovato");
    LogWrapper.logMessage(LogWrapper.INFO, "Dati utente: " + user.toString());

    // --------------------------------------------------------
    // Cerco/creo il workspace usando utente e container
    // --------------------------------------------------------
    EPMWorkspace epmw_find = SearchUtils.findWorkspace(nomeWS, user, containerRef, false);
    if (epmw_find == null) {
      LogWrapper.logMessage(LogWrapper.INFO, "Workspace " + nomeWS + " non trovato");
      throw new SearchUtilsException("Workspace " + nomeWS + " non creato/trovato");
    }

    // ----------------------------------------------
    // Prima di effettuare il checkout imposto il Principal come l'utente proprietario del ws
    // ----------------------------------------------
    try {
      LogWrapper.logMessage(LogWrapper.INFO, "Imposto il Principal come l'utente proprietario del ws ( " + user.getName() + " )");
      SessionHelper.manager.setPrincipal(user.getName());
    }
    catch (WTException e) {
      String msg = "Errore impostando il principal ( " + user.getName() + " )";
      LogWrapper.logMessage(LogWrapper.ERROR, msg, e);
      throw new SearchUtilsException(msg, e);
    }

    // --------------------------------------------------------
    // Controllo che gli oggetti non siano gi� checkout:
    // se non sono gi� in checkout ne faccio il CHECKOUT, altrimenti non faccio nulla
    // --------------------------------------------------------
    ArrayList<String> oggettiMessiInCheckout = new ArrayList<String>();
    for (Object e : documentiEPM) {
      ObjectReference epmref = (ObjectReference) e;
      EPMDocument epm = (EPMDocument) epmref.getObject();
      try {
        if (!WorkInProgressHelper.isCheckedOut(epm)) {
          LogWrapper.logMessage(LogWrapper.INFO, epm.getNumber() + " non in checkout, quindi provo a farne il checkout");
          LogWrapper.logMessage(LogWrapper.INFO, epm.getNumber() + " non � una working copy, quindi provo a farne il checkout");
          WTArrayList tempList = new WTArrayList();
          tempList.add(epm);
          try {
            EPMWorkspaceHelper.manager.checkout(epmw_find, tempList);
            oggettiMessiInCheckout.add(epm.getNumber());
            LogWrapper.logMessage(LogWrapper.INFO, epm.getNumber() + " in checkout");
          }
          catch (WTException e1) {
            LogWrapper.logMessage(LogWrapper.ERROR, "Errore durante il checkout di " + epm.getNumber(), e1);
          }
        }
        else {
          LogWrapper.logMessage(LogWrapper.INFO, epm.getNumber() + " � una working copy, quindi bajinghi");
        }
      }
      catch (WTException e1) {
        LogWrapper.logMessage(LogWrapper.ERROR, "Errore durante isCheckedOut di " + epm.getNumber(), e1);
      }
    }

    // ----------------------------------------------
    // Prima di effettuare il checkout imposto il Principal come l'utente proprietario del ws
    // ----------------------------------------------
    try {
      LogWrapper.logMessage(LogWrapper.INFO, "Ripristino il Principal come l'utente wcadmin ( " + wcadmin.getName() + " )");
      SessionHelper.manager.setPrincipal(wcadmin.getName());
    }
    catch (WTException e) {
      String msg = "Errore ripristinando il principal ( " + wcadmin.getName() + " )";
      LogWrapper.logMessage(LogWrapper.ERROR, msg, e);
      throw new SearchUtilsException(msg, e);
    }

    LogWrapper.logMessage(LogWrapper.INFO, "Fine checkout");
    LogWrapper.logMessage(LogWrapper.INFO, "-----------------------------------------------------------");
    return oggettiMessiInCheckout;
  }

  /**
   * Esegue la copia in locale
   * 
   * @param p
   *          properties per la copia in locale
   * @throws CheckoutPDMLinkException
   *           l'eccezione sulla copia
   * @throws SearchUtilsException
   *           l'eccezione sulla ricerca
   * @return Lista contenente i nomi dei file presenti nel workspace
   */
  public ArrayList<String> eseguiCopiaLocale(String p) throws SearchUtilsException, CheckoutPDMLinkException {
    LogWrapper.logMessage(LogWrapper.INFO, "-----------------------------------------------------------");
    LogWrapper.logMessage(LogWrapper.INFO, "--------------- COPIA IN LOCALE " + CheckoutUtils.VERSION + " --------------");
    LogWrapper.logMessage(LogWrapper.INFO, "-----------------------------------------------------------");

    Reader sr = new StringReader(p);
    Properties prop = new Properties();
    try {
      prop.load(sr);
    }
    catch (IOException e1) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore nel passaggio parametri della copia in locale!", e1);
      throw new SearchUtilsException("Errore nel passaggio parametri della copia in locale!", e1);
    }

    // controllo che i parametri abbiano dei valori validi
    if (!controllaParametri(prop)) {
      throw new SearchUtilsException("Parametri passati alla copia in locale con errori/non validi");
    }

    // public void eseguiCheckout(Properties p) throws CheckoutPDMLinkException {
    String lista = prop.getProperty(MODELLI);
    if (lista == null || "".equals(lista)) {
      throw new SearchUtilsException("Nessun modello passato in input!");
    }

    List<String> modelli = Arrays.asList(lista.split(";"));
    LogWrapper.logMessage(LogWrapper.INFO, "Inizio copia locale di " + modelli.toString());
    Vector<EPMDocument> documentiEPM = new Vector<EPMDocument>();

    // --------------------------------------------------------
    // Cerco i documenti di cui bisogna fare la copia
    // --------------------------------------------------------
    List<String> documentiNonTrovati = new ArrayList<String>();
    for (String modello : modelli) {
      EPMDocument epm = SearchEPMDocument.findLatestEPMDocuments(modello);
      if (epm != null) {
        documentiEPM.add(epm);
        LogWrapper.logMessage(LogWrapper.INFO, "Documento con number = " + epm.getNumber() + " e ver: " + epm.getVersionIdentifier().getValue()
            + " itr: " + epm.getIterationIdentifier().getValue() + " aggiunto alla lista per la copia in locale.");
      }
      else {
        documentiNonTrovati.add(modello);
        LogWrapper.logMessage(LogWrapper.WARN, "Documento con number = " + modello + " non trovato.");
      }
    }

    // --------------------------------------------------------
    // Cerco il container
    // --------------------------------------------------------
    WTContainerRef containerRef = SearchUtils.findContainer();
    if (containerRef == null) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Container non trovato");
      throw new SearchUtilsException("Container non trovato");
    }
    else {
      LogWrapper.logMessage(LogWrapper.INFO, "Container trovato");
    }

    // --------------------------------------------------------
    // Cerco l'utente
    // --------------------------------------------------------
    String userName = prop.getProperty(LOGIN, "Administrator");// "Administrator";
    WTUser user = SearchUtils.findUser(userName);
    if (user == null) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Utente " + userName + " non trovato");
      throw new SearchUtilsException("Utente " + userName + " non trovato");
    }
    LogWrapper.logMessage(LogWrapper.INFO, "Utente " + user.getFullName() + " trovato");
    LogWrapper.logMessage(LogWrapper.INFO, "Dati utente: " + user.toString());

    // --------------------------------------------------------
    // Cerco/creo il workspace usando utente e container
    // --------------------------------------------------------
    String workspaceName = prop.getProperty(WORKSPACE, "ws_default");// "prova_2";
    boolean daSvuotare = prop.getProperty(ELIMINA_WS, "true").equalsIgnoreCase("true");
    EPMWorkspace epmw_find = SearchUtils.findWorkspace(workspaceName, user, containerRef, daSvuotare);
    if (epmw_find == null) {
      LogWrapper.logMessage(LogWrapper.INFO, "Workspace " + workspaceName + " non trovato");
      throw new SearchUtilsException("Workspace " + workspaceName + " non creato/trovato");
    }
    else {
      LogWrapper.logMessage(LogWrapper.INFO, "Workspace " + workspaceName + " trovato/creato");
    }
    boolean asStored = ((prop.getProperty(AS_STORED, "latest").equalsIgnoreCase("asStored")));
    boolean drawing = prop.getProperty(DRAWING, "false").equalsIgnoreCase("true");
    String dipendenze = prop.getProperty(DIPENDENZE, "required");

    // --------------------------------------------------------
    // Eseguo il popolamento di modelli e disegni ( se l'opzione drawing � true)
    // --------------------------------------------------------
    eseguiPopolamento(documentiEPM, epmw_find, dipendenze, asStored, drawing);

    // --------------------------------------------------------
    // Scrittura file con esito copia locale
    // --------------------------------------------------------
    ArrayList<String> nomiOggettiNelWS = scriviFileDiEsito(epmw_find, prop.getProperty(REPORT, "C:\\Temp\\copiaLocale_windchill.txt"),
        documentiNonTrovati);

    LogWrapper.logMessage(LogWrapper.INFO, "Fine della copia in locale");
    LogWrapper.logMessage(LogWrapper.INFO, "-----------------------------------------------------------");
    return nomiOggettiNelWS;
  }

  /**
   * Esegue l'undo checkout sulla lista di modelli passati.
   * 
   * @param listaModelli
   *          lista dei modelli separati da ;
   * @throws SearchUtilsException
   *           l'eccezione sulla ricerca dei modelli
   * @return la lista di modelli per i quali � riuscita l'operazione di undo checkout
   * @deprecated
   */
  public ArrayList<String> eseguiUndoCheckout(String listaModelli) throws SearchUtilsException {

    LogWrapper.logMessage(LogWrapper.INFO, "-----------------------------------------------------------");
    LogWrapper.logMessage(LogWrapper.INFO, "--------------- UNDO CHECKOUT " + CheckoutUtils.VERSION + " --------------");
    LogWrapper.logMessage(LogWrapper.INFO, "-----------------------------------------------------------");

    if (listaModelli == null || "".equals(listaModelli)) {
      throw new SearchUtilsException("Nessun modello passato in input!");
    }

    List<String> modelli = Arrays.asList(listaModelli.split(";"));
    LogWrapper.logMessage(LogWrapper.INFO, "Inizio undo checkout di " + modelli.toString());
    Vector<EPMDocument> documentiEPM = new Vector<EPMDocument>();

    // --------------------------------------------------------
    // Cerco i documenti di cui bisogna fare l'undo checkout
    // --------------------------------------------------------
    List<String> documentiNonTrovati = new ArrayList<String>();
    for (String modello : modelli) {
      EPMDocument epm = SearchEPMDocument.findLatestEPMDocuments(modello);

      if (epm != null) {
        documentiEPM.add(epm);
        LogWrapper.logMessage(LogWrapper.INFO, "Documento con number = " + epm.getNumber() + " e ver: " + epm.getVersionIdentifier().getValue()
            + " itr: " + epm.getIterationIdentifier().getValue() + " aggiunto alla lista per l'undo checkout.");
      }
      else {
        documentiNonTrovati.add(modello);
        LogWrapper.logMessage(LogWrapper.WARN, "Documento con number = " + modello + " non trovato.");
      }
    }

    ArrayList<String> documentiUndocheckout = new ArrayList<String>();
    // cicla su tutti i documenti
    for (EPMDocument doc : documentiEPM) {
      WTArrayList tmp = new WTArrayList();
      String number = doc.getNumber();

      tmp.add(doc);

      try {
        WorkInProgressHelper.service.undoCheckouts(tmp);
        // EPMWorkspaceHelper.manager.undoCheckout(arg0, arg1)

      }
      catch (Exception e) {
        LogWrapper.logMessage(LogWrapper.ERROR, "Errore nell'undo checkout del documento: " + number, e);
      }

      // aggiungo alla lista i documenti undo checkati
      documentiUndocheckout.add(number);
    }

    // restituisce la lista dei successi
    return documentiUndocheckout;
  }

  /**
   * Esegue l'undo checkout sulla lista di modelli passati e fa il restore nel workspace.
   * 
   * @param listaModelli
   *          lista dei modelli separati da ;
   * @param workspace
   *          nome del workspace dove si trovano i modelli
   * @param userName
   *          login dell'utente possessore del workspace
   * 
   * @throws SearchUtilsException
   *           l'eccezione sulla ricerca dei modelli
   * @return la lista di modelli per i quali � riuscita l'operazione di undo checkout
   */
  public ArrayList<String> eseguiUndoCheckout(String listaModelli, String workspaceName, String userName) throws SearchUtilsException {

    LogWrapper.logMessage(LogWrapper.INFO, "-----------------------------------------------------------");
    LogWrapper.logMessage(LogWrapper.INFO, "--------------- UNDO CHECKOUT" + CheckoutUtils.VERSION + " --------------");
    LogWrapper.logMessage(LogWrapper.INFO, "-----------------------------------------------------------");

    // --------------------------------------------------------
    // Cerco il container
    // --------------------------------------------------------
    WTContainerRef containerRef = SearchUtils.findContainer();
    if (containerRef == null) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Container non trovato");
      throw new SearchUtilsException("Container non trovato");
    }
    else {
      LogWrapper.logMessage(LogWrapper.INFO, "Container trovato");
    }

    // --------------------------------------------------------
    // Cerco l'utente
    // --------------------------------------------------------
    WTUser user = SearchUtils.findUser(userName);
    if (user == null) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Utente " + userName + " non trovato");
      throw new SearchUtilsException("Utente " + userName + " non trovato");
    }
    LogWrapper.logMessage(LogWrapper.INFO, "Utente " + user.getFullName() + " trovato");
    LogWrapper.logMessage(LogWrapper.INFO, "Dati utente: " + user.toString());

    // --------------------------------------------------------
    // Cerco il workspace
    // --------------------------------------------------------
    EPMWorkspace epmw_find = null;
    WTSet workspaces = null;
    try {
      workspaces = EPMWorkspaceHelper.manager.getWorkspaces(user, containerRef);
    }
    catch (WTException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }
    if (workspaces == null || workspaces.isEmpty()) {
      LogWrapper.logMessage(LogWrapper.INFO, "Non � stato trovato nessun workspace");
    }
    LogWrapper.logMessage(LogWrapper.INFO, "WORKSPACE trovati: ");
    for (Object obj : workspaces) {
      ObjectReference or = (ObjectReference) obj;
      EPMWorkspace epmw = (EPMWorkspace) or.getObject();
      LogWrapper.logMessage(LogWrapper.INFO, "-" + epmw.getName());
      if (epmw.getName().equals(workspaceName)) {
        epmw_find = epmw;
        break;
      }
    }

    if (listaModelli == null || "".equals(listaModelli)) {
      throw new SearchUtilsException("Nessun modello passato in input!");
    }

    List<String> modelli = Arrays.asList(listaModelli.split(";"));
    LogWrapper.logMessage(LogWrapper.INFO, "Inizio undo checkout di " + modelli.toString());
    // Vector<EPMDocument> documentiEPM = new Vector<EPMDocument>();
    WTArrayList toUndoCheckout = new WTArrayList();
    ArrayList<String> documentiUndocheckout = new ArrayList<String>();
    // --------------------------------------------------------
    // Cerco i documenti di cui bisogna fare l'undo checkout, che
    // sono quelli della lista, nel workspace richiesto per
    // l'utente richiesto
    // --------------------------------------------------------
    try {
      List<String> documentiNonTrovati = new ArrayList<String>();
      for (String modello : modelli) {
        EPMDocument epm = SearchEPMDocument.findLatestEPMDocumentsWorkingCopy(modello);
        if (epm != null) {
          if (WorkInProgressHelper.isCheckedOut((Workable) epm, user)) {
            // documentiEPM.add(epm);
            documentiUndocheckout.add(modello);
            toUndoCheckout.add(epm);
            LogWrapper.logMessage(LogWrapper.INFO, "Documento con number = " + epm.getNumber() + " e ver: " + epm.getVersionIdentifier().getValue()
                + " itr: " + epm.getIterationIdentifier().getValue() + " aggiunto alla lista di working copy per l'undo checkout.");
          }
          else {
            LogWrapper.logMessage(LogWrapper.WARN, "Documento con number = " + modello + " non aggiunto alla lista per l'undo checkout");
          }
        }
        else {
          documentiNonTrovati.add(modello);
          LogWrapper.logMessage(LogWrapper.WARN, "Documento con number = " + modello + " non trovato.");
        }
      }
    }
    catch (WTException e) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore durante la verifica dei file di cui fare undo checkout", e);
    }

    try {
      EPMWorkspaceHelper.manager.undoCheckoutAndRestoreInWorkspace(epmw_find, toUndoCheckout);
    }
    catch (WTException e1) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore nell'undo checkout dei file", e1);
    }
    // ritorna la lista dei file sottoposti ad undo checkout
    return documentiUndocheckout;
  }

  /**
   * Svuota il workspace facendo l'undo del checkout su tutto il contenuto del workspace, anche per i file che non sono
   * in checkout o per quelli in checkout da parte di qualche altro utente.
   * 
   * @param workspace
   *          nome del workspace da svuotare
   * @param userName
   *          login dell'utente possessore del workspace
   * @throws SearchUtilsException
   *           l'eccezione sulla ricerca dei modelli
   * 
   */
  public void SvuotaWorkspace(String workspaceName, String userName) throws SearchUtilsException {

    LogWrapper.logMessage(LogWrapper.INFO, "-----------------------------------------------------------");
    LogWrapper.logMessage(LogWrapper.INFO, "--------------- SVUOTA WORKSPACE " + CheckoutUtils.VERSION + " --------------");
    LogWrapper.logMessage(LogWrapper.INFO, "-----------------------------------------------------------");

    // --------------------------------------------------------
    // Cerco il container
    // --------------------------------------------------------
    WTContainerRef containerRef = SearchUtils.findContainer();
    if (containerRef == null) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Container non trovato");
      throw new SearchUtilsException("Container non trovato");
    }
    else {
      LogWrapper.logMessage(LogWrapper.INFO, "Container trovato");
    }

    // --------------------------------------------------------
    // Cerco l'utente
    // --------------------------------------------------------
    // String userName = prop.getProperty(LOGIN, "Administrator");// "Administrator";
    WTUser user = SearchUtils.findUser(userName);
    if (user == null) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Utente " + userName + " non trovato");
      throw new SearchUtilsException("Utente " + userName + " non trovato");
    }
    LogWrapper.logMessage(LogWrapper.INFO, "Utente " + user.getFullName() + " trovato");
    LogWrapper.logMessage(LogWrapper.INFO, "Dati utente: " + user.toString());

    // --------------------------------------------------------
    // Cerco il workspace
    // --------------------------------------------------------try {
    EPMWorkspace epmw_find = null;
    WTSet workspaces = null;
    try {
      workspaces = EPMWorkspaceHelper.manager.getWorkspaces(user, containerRef);
    }
    catch (WTException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }
    if (workspaces == null || workspaces.isEmpty()) {
      LogWrapper.logMessage(LogWrapper.INFO, "Non � stato trovato nessun workspace");
      // return null;
    }
    LogWrapper.logMessage(LogWrapper.INFO, "WORKSPACE trovati: ");
    for (Object obj : workspaces) {
      ObjectReference or = (ObjectReference) obj;
      EPMWorkspace epmw = (EPMWorkspace) or.getObject();
      LogWrapper.logMessage(LogWrapper.INFO, "-" + epmw.getName());
      if (epmw.getName().equals(workspaceName)) {
        epmw_find = epmw;
        break;
      }
    }

    try {
      WTSet oggettiWS = EPMWorkspaceHelper.manager.getObjectsInWorkspace(epmw_find, RevisionControlled.class);
      EPMWorkspaceHelper.manager.undoCheckout(epmw_find, oggettiWS);

      EPMWorkspaceHelper.manager.removeFromWorkspace(epmw_find, oggettiWS);
    }
    catch (WTException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }
  }

  /**
   * Svuota il workspace facendo l'undo del checkout solo per i file che sono in checkout da parte dell'utente
   * 
   * @param workspace
   *          nome del workspace da svuotare
   * @param userName
   *          login dell'utente possessore del workspace
   * @throws SearchUtilsException
   *           l'eccezione sulla ricerca dei modelli
   * 
   */
  public void SvuotaWorkspace2(String workspaceName, String userName) throws SearchUtilsException {

    LogWrapper.logMessage(LogWrapper.INFO, "-----------------------------------------------------------");
    LogWrapper.logMessage(LogWrapper.INFO, "--------------- SVUOTA WORKSPACE 2 " + CheckoutUtils.VERSION + " --------------");
    LogWrapper.logMessage(LogWrapper.INFO, "-----------------------------------------------------------");

    // --------------------------------------------------------
    // Cerco il container
    // --------------------------------------------------------
    WTContainerRef containerRef = SearchUtils.findContainer();
    if (containerRef == null) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Container non trovato");
      throw new SearchUtilsException("Container non trovato");
    }
    else {
      LogWrapper.logMessage(LogWrapper.INFO, "Container trovato");
    }

    // --------------------------------------------------------
    // Cerco l'utente
    // --------------------------------------------------------
    // String userName = prop.getProperty(LOGIN, "Administrator");// "Administrator";
    WTUser user = SearchUtils.findUser(userName);
    if (user == null) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Utente " + userName + " non trovato");
      throw new SearchUtilsException("Utente " + userName + " non trovato");
    }
    LogWrapper.logMessage(LogWrapper.INFO, "Utente " + user.getFullName() + " trovato");
    LogWrapper.logMessage(LogWrapper.INFO, "Dati utente: " + user.toString());

    // --------------------------------------------------------
    // Cerco il workspace
    // --------------------------------------------------------try {
    EPMWorkspace epmw_find = null;
    WTSet workspaces = null;
    try {
      workspaces = EPMWorkspaceHelper.manager.getWorkspaces(user, containerRef);
    }
    catch (WTException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }
    if (workspaces == null || workspaces.isEmpty()) {
      LogWrapper.logMessage(LogWrapper.INFO, "Non � stato trovato nessun workspace");
      // return null;
    }
    LogWrapper.logMessage(LogWrapper.INFO, "WORKSPACE trovati: ");
    for (Object obj : workspaces) {
      ObjectReference or = (ObjectReference) obj;
      EPMWorkspace epmw = (EPMWorkspace) or.getObject();
      LogWrapper.logMessage(LogWrapper.INFO, "-" + epmw.getName());
      if (epmw.getName().equals(workspaceName)) {
        epmw_find = epmw;
        break;
      }
    }

    try {
      WTSet oggettiWS = EPMWorkspaceHelper.manager.getObjectsInWorkspace(epmw_find, RevisionControlled.class);

      // per ogni file presente nel workspace, verifico se � in checkout da parte dell'utente corrente e, nel
      // caso, fa Undo checkout solo per quel file
      for (Object object : oggettiWS) {
        EPMDocument epmDocument = (EPMDocument) ((ObjectReference) object).getObject();
        if (WorkInProgressHelper.isCheckedOut((Workable) epmDocument, user)) {
          LogWrapper.logMessage(LogWrapper.INFO, "Undo checkout per il file " + epmDocument.getNumber());
          WTArrayList documento = new WTArrayList();
          documento.add(object);
          EPMWorkspaceHelper.manager.undoCheckout(epmw_find, documento);
        }
      }

      // EPMWorkspaceHelper.manager.undoCheckout(epmw_find, oggettiWS);

      EPMWorkspaceHelper.manager.removeFromWorkspace(epmw_find, oggettiWS);
    }
    catch (WTException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }
  }

  /**
   * Scrive il file di esito del checkout
   * 
   * @param epmw_find
   *          workspace in cui � stato effettuato il checkout
   * @param percorsoReport
   *          file in cui viene scritto il report del checkout
   * @param documentiNonTrovati
   *          la lista dei documenti non trovati in Windchill
   * @throws CheckoutPDMLinkException
   * @return Lista contenente i nomi dei file presenti nel workspace
   */
  private ArrayList<String> scriviFileDiEsito(EPMWorkspace epmw_find, String percorsoReport, List<String> documentiNonTrovati)
      throws CheckoutPDMLinkException {
    LogWrapper.logMessage(LogWrapper.INFO, "Recupero gli oggetti nel workspace " + epmw_find.getName());
    ArrayList<String> nomiFileNelWS = new ArrayList<String>();
    // ----------------------------------------------------------------
    // Cerco i documenti nel workspace per scrivere il file di esito del checkout
    // ----------------------------------------------------------------
    WTSet oggettiNelWorkspace = null;
    try {
      oggettiNelWorkspace = EPMWorkspaceHelper.manager.getObjectsInWorkspace(epmw_find, EPMDocument.class);
      LogWrapper.logMessage(LogWrapper.INFO, "Dopo il popolamento ci sono " + oggettiNelWorkspace.size() + " elementi nel ws " + epmw_find.getName());
    }
    catch (WTException e1) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore ricercando oggetti nel workspace ", e1);
      throw new CheckoutPDMLinkException("Errore ricercando oggetti nel workspace ", e1);
    }
    // --------------------------------------------------------
    // Scrittura file con esito checkout : INIZIO
    // --------------------------------------------------------
    LogWrapper.logMessage(LogWrapper.INFO, "Scrivo il file di esito del checkout nel percorso: " + percorsoReport);
    PrintWriter pw = null;
    try {
      File esito = new File(percorsoReport);
      if (!esito.exists()) {
        esito.getParentFile().mkdirs();
        esito.createNewFile();
      }
      // --------------------------------------------------------
      // Scrivo su file il nome e revisione degli oggetti che sono nel workspace
      // --------------------------------------------------------
      pw = new PrintWriter(esito);
      LogWrapper.logMessage(LogWrapper.INFO, "Ciclo sugli oggetti nel workspace");
      for (Object o : oggettiNelWorkspace) {
        EPMDocument temp = (EPMDocument) ((ObjectReference) o).getObject();
        nomiFileNelWS.add(temp.getNumber());
        pw.println(temp.getNumber() + ";" + temp.getVersionIdentifier().getValue() + ";" + temp.getIterationIdentifier().getValue());
        LogWrapper.logMessage(LogWrapper.INFO, "- " + temp.getName() + " - versione: " + temp.getVersionIdentifier().getValue() + " -iterazione:"
            + temp.getIterationIdentifier().getValue());
      }
      if (documentiNonTrovati != null && documentiNonTrovati.size() > 0) {
        LogWrapper.logMessage(LogWrapper.INFO, "Scrivo nel file di esito i codici non trovati in Windchill");
        pw.println("------------------------------------------------");
        pw.println("Documenti non trovati:");
        for (String s : documentiNonTrovati) {
          pw.println(s);
        }
      }
      else {
        LogWrapper.logMessage(LogWrapper.INFO, "Non ci sono file non trovati in Windchill");
      }
      pw.close();
    }
    catch (IOException e) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore creando/scrivendo il file di log", e);
      throw new CheckoutPDMLinkException("Errore creando/scrivendo il file di log", e);
    }
    finally {
      if (pw != null)
        pw.close();
    }
    return nomiFileNelWS;
  }

  /**
   * Effettua il popolamento di un singolo documento
   * 
   * @param epmDocument
   *          il documento
   * @param ws
   *          il workspace
   * @param asStored
   *          in base al valore del parametro viene fatto il popolamento in modalit� asStored o latest
   * @param dipendenze
   *          le dipendenze, a scelta tra <code>all</code>, <code>required</code> o <code>none</code>
   * @throws CheckoutPDMLinkException
   *           in caso di errore
   */
  private void copiaLocaleEMPDocument(EPMDocument epmDocument, EPMWorkspace ws, boolean asStored, String dipendenze) throws CheckoutPDMLinkException {
    LogWrapper.logMessage(LogWrapper.INFO, "Effettuo il popolamento di " + epmDocument.getNumber());
    EPMDocConfigSpec dcs = ws.getDocConfigSpec();
    // ---------------------------------------
    // As Stored
    // ---------------------------------------
    try {
      if (asStored) {
        dcs.setAsStoredActive(true);
        EPMAsStoredConfigSpec asstored = EPMAsStoredConfigSpec.newEPMAsStoredConfigSpec(epmDocument);
        EPMDocConfigSpec dcs2 = EPMDocConfigSpec.newEPMDocConfigSpec(asstored);
        ws.setDocConfigSpec(dcs2);
      }
      // ---------------------------------------
      // Latest
      // ---------------------------------------
      else {
        dcs.setAsStoredActive(false);
        dcs.setLatestActive();
      }
      PersistenceHelper.manager.save(ws);
    }
    catch (WTException e) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore impostando/salvando l'EPMDocDonfigSpec del workspace " + ws.getName(), e);
      throw new CheckoutPDMLinkException("Errore impostando/salvando l'EPMDocDonfigSpec del workspace " + ws.getName(), e);
    }
    catch (WTPropertyVetoException e) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore impostando/salvando l'EPMDocDonfigSpec del workspace " + ws.getName(), e);
      throw new CheckoutPDMLinkException("Errore impostando/salvando l'EPMDocDonfigSpec del workspace " + ws.getName(), e);
    }

    // --------------------------------------------------------
    // Effettuo popolamento
    // --------------------------------------------------------
    LogWrapper.logMessage(1, "Effettuo il popolamento di " + epmDocument.getNumber() + " con dipendenze=" + dipendenze + " - asStored=" + asStored);
    Vector<EPMDocument> epmDocs = new Vector<EPMDocument>();
    epmDocs.add(epmDocument);
    try {
      if (dipendenze.equalsIgnoreCase("ALL")) {
        EPMBaselineHelper.service.populateAll(ws, epmDocs);
      }
      else if (dipendenze.equalsIgnoreCase("REQUIRED")) {
        EPMBaselineHelper.service.populateRequired(ws, epmDocs);
      }
      else if (dipendenze.equalsIgnoreCase("NONE")) {
        EPMBaselineHelper.service.populate(ws, epmDocs, EPMPopulateRule.NONE);
      }
      else if (dipendenze.equalsIgnoreCase("ADD")) {
        WTArrayList documentiEPM = new WTArrayList();
        for (Object object : epmDocs) {
          documentiEPM.add(object);
        }
        EPMWorkspaceHelper.manager.addToWorkspace(ws, documentiEPM);
      }
      LogWrapper.logMessage(LogWrapper.INFO, "Fine popolamento di " + epmDocument.getNumber());
    }
    catch (WTException ex) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore durante il popolamento ", ex);
      throw new CheckoutPDMLinkException("Errore durante il popolamento ", ex);
    }
  }

  /**
   * Controlla i parametri passati per il checkout.
   * 
   * @param p
   *          la property con le coppie <chiave, valore>
   * @return <code>true<code> se le properties sono corrette, <code>false</code> altrimenti.
   */
  private boolean controllaParametri(Properties p) {
    boolean res = false;

    List<String> chiavi = new ArrayList<String>(Arrays.asList(URL, USER, PASS, MODELLI, DIPENDENZE, WORKSPACE, ELIMINA_WS, AS_STORED, REPORT,
        DRAWING, LOGIN, MODO, TEST));

    Set<Object> sKeys = p.keySet();

    LogWrapper.logMessage(LogWrapper.INFO, "controllo parametri Check-in iniziato", null);

    // caso nel quale non abbiamo passato alcun modello da processare (pettiniamo bambole?)
    if (!sKeys.contains(MODELLI)) {
      LogWrapper.logMessage(LogWrapper.ERROR, "CDT: non abbiamo passato alcun modello", null);
      return res;
    }

    for (Object key : sKeys) {
      if (!chiavi.contains((String) key)) {
        // caso in cui una chiave passata � inesistente (dita a banana?)
        LogWrapper.logMessage(LogWrapper.ERROR, "CDT: il parametro " + key.toString() + " non � ammesso", null);
        return res;
      }

      String k = key.toString();
      if (k.equals(DIPENDENZE)) {
        // parametro senza relativo valore
        if (CDTProperties.isPropertyNull(p, DIPENDENZE)) {
          LogWrapper.logMessage(LogWrapper.ERROR, "CDT: il valore del parametro di check-in " + key.toString() + " � NULL", null);
          return res;
        }
        // il valore
        String value = p.getProperty(DIPENDENZE).toUpperCase();
        // controllo su valore non ammesso
        if (!(value.equalsIgnoreCase("ALL") || value.equalsIgnoreCase("REQUIRED") || value.equalsIgnoreCase("NONE") || value.equalsIgnoreCase("ADD"))) {
          LogWrapper.logMessage(LogWrapper.ERROR, "CDT: il valore del parametro di check-in " + key.toString() + " ha valore: " + value
              + " ma non � ammesso", null);

          return res;
        }
      }
      if (k.equals(WORKSPACE)) {
        // parametro senza relativo valore
        if (CDTProperties.isPropertyNull(p, WORKSPACE)) {
          LogWrapper.logMessage(LogWrapper.ERROR, "CDT: il valore del parametro di check-in " + key.toString() + " � NULL", null);
          return res;
        }
      }
      else if (k.equals(DRAWING)) {
        if (k.equals(DRAWING)) {
          // parametro senza relativo valore
          if (CDTProperties.isPropertyNull(p, DRAWING)) {
            LogWrapper.logMessage(LogWrapper.ERROR, "CDT: il valore del parametro di check-in " + key.toString() + " � NULL", null);
            return res;
          }
          // il valore
          String value = p.getProperty(DRAWING).toUpperCase();
          // controllo su valore non ammesso
          if (!(value.equalsIgnoreCase("TRUE") || value.equalsIgnoreCase("FALSE"))) {
            LogWrapper.logMessage(LogWrapper.ERROR, "CDT: il valore del parametro di check-in " + key.toString() + " ha valore: " + value
                + " ma non � ammesso", null);
            return res;
          }
        }
      }
      else if (k.equals(AS_STORED)) {
        if (k.equals(AS_STORED)) {
          // parametro senza relativo valore
          if (CDTProperties.isPropertyNull(p, AS_STORED)) {
            LogWrapper.logMessage(LogWrapper.ERROR, "CDT: il valore del parametro di check-in " + key.toString() + " � NULL", null);
            return res;
          }
          // il valore
          String value = p.getProperty(AS_STORED).toUpperCase();
          // controllo su valore non ammesso
          if (!(value.equalsIgnoreCase("ASSTORED") || value.equalsIgnoreCase("LATEST"))) {
            LogWrapper.logMessage(LogWrapper.ERROR, "CDT: il valore del parametro di check-in " + key.toString() + " ha valore: " + value
                + " ma non � ammesso", null);

            return res;
          }
        }
      }
      else if (k.equals(ELIMINA_WS)) {
        if (k.equals(ELIMINA_WS)) {
          // parametro senza relativo valore
          if (CDTProperties.isPropertyNull(p, ELIMINA_WS)) {
            LogWrapper.logMessage(LogWrapper.ERROR, "CDT: il valore del parametro di check-in " + key.toString() + " � NULL", null);
            return res;
          }
          // il valore
          String value = p.getProperty(ELIMINA_WS).toUpperCase();
          // controllo su valore non ammesso
          if (!(value.equalsIgnoreCase("TRUE") || value.equalsIgnoreCase("FALSE"))) {
            LogWrapper.logMessage(LogWrapper.ERROR, "CDT: il valore del parametro di check-in " + key.toString() + " ha valore: " + value
                + " ma non � ammesso", null);

            return res;
          }
        }
      }
      else if (k.equals(LOGIN)) {
        if (k.equals(LOGIN)) {
          // parametro senza relativo valore
          if (CDTProperties.isPropertyNull(p, LOGIN)) {
            LogWrapper.logMessage(LogWrapper.ERROR, "CDT: il valore del parametro di check-in " + key.toString() + " � NULL", null);
            return res;
          }
        }
      }
      else if (k.equals(REPORT)) {
        if (k.equals(REPORT)) {
          // parametro senza relativo valore
          if (CDTProperties.isPropertyNull(p, REPORT)) {
            LogWrapper.logMessage(LogWrapper.ERROR, "CDT: il valore del parametro di check-in " + key.toString() + " � NULL", null);
            return res;
          }
        }
      }
      else if (k.equals(MODO)) {
        if (k.equals(MODO)) {
          // parametro senza relativo valore
          if (CDTProperties.isPropertyNull(p, MODO)) {
            LogWrapper.logMessage(LogWrapper.ERROR, "CDT: il valore del parametro di check-in " + key.toString() + " � NULL", null);
            return res;
          }
          // il valore
          String value = p.getProperty(MODO).toUpperCase();
          // controllo su valore non ammesso
          if (!(value.equalsIgnoreCase("COPIA") || value.equalsIgnoreCase("CHECKOUT"))) {
            LogWrapper.logMessage(LogWrapper.ERROR, "CDT: il valore del parametro di check-in " + key.toString() + " ha valore: " + value
                + " ma non � ammesso", null);

            return res;
          }
        }
      }
    }
    // non ci sono stati errori
    res = true;

    LogWrapper.logMessage(LogWrapper.INFO, "controllo parametri Check-in finito", null);
    return res;
  }

  /**
   * Esegue l'operazione desiderata come server
   * 
   * @param p
   *          le properties per l'azione scelta
   * @throws CheckoutPDMLinkException
   *           l'eccezione sull'azione di checkout
   * @throws SearchUtilsException
   *           l'eccezione sulla ricerca
   */
  private static void eseguiComeServer(Properties p) throws CheckoutPDMLinkException, SearchUtilsException {
    if (SERVER) {
      eseguiDaWindchillShell(p);
    }
    else {
      try {
        // imposta il method server remoto
        RemoteMethodServer method = RemoteMethodServer.getDefault();
        method.setUserName(user);
        method.setPassword(password);

        Class[] argumentTypes = { Properties.class };
        Object[] arguments = { p };
        LogWrapper.logMessage(LogWrapper.INFO, "");
        method.invoke("eseguiDaWindchillShell", "ext.caditech.checkout.CheckoutUtils", null, argumentTypes, arguments);
      }
      catch (InvocationTargetException e) {
        LogWrapper.logMessage(LogWrapper.ERROR, "Errore invocando eseguiDaWindchillShell", e);
        throw new CheckoutPDMLinkException("Errore invocando eseguiDaWindchillShell", e);
      }
      catch (RemoteException e) {
        LogWrapper.logMessage(LogWrapper.ERROR, "Errore invocando eseguiDaWindchillShell", e);
        throw new CheckoutPDMLinkException("Errore invocando eseguiDaWindchillShell", e);
      }
    }
  }

  /**
   * Controlla se il documento passato � in checkout.
   * 
   * @param number
   *          il numero del documento da controllare
   * @return <code>true</code> se il documento � in checkout, <code>false</code> altrimenti.
   * @throws SearchUtilsException
   *           in caso di errore
   */
  public static Boolean isCheckedOut(String number) throws SearchUtilsException {
    Boolean res = false;
    LogWrapper.logMessage(LogWrapper.INFO, "-----------------------------------------------------------");
    LogWrapper.logMessage(LogWrapper.INFO, "--------------- IS CHECKED OUT " + CheckoutUtils.VERSION + " --------------");
    LogWrapper.logMessage(LogWrapper.INFO, "-----------------------------------------------------------");

    Workable doc = SearchEPMDocument.findLatestEPMDocuments(number);
    if (doc == null) {
      String msg = "Documento " + number + " non trovato";
      LogWrapper.logMessage(LogWrapper.ERROR, msg);
      throw new SearchUtilsException(msg);
    }
    try {
      res = WorkInProgressHelper.isCheckedOut(doc);
    }
    catch (WTException e) {
      String msg = "Errore nel isCheckedOut in CheckoutUtils";
      LogWrapper.logMessage(LogWrapper.ERROR, msg, e);
      throw new SearchUtilsException(msg, e);
    }
    if (res) {
      LogWrapper.logMessage(LogWrapper.INFO, number + " � in checkout");
    }
    else {
      LogWrapper.logMessage(LogWrapper.INFO, number + " NON � in checkout");
    }
    return res;
  }

  /**
   * Il main
   * 
   * @param args
   *          argomenti corrispondenti alle properties
   * @throws Exception
   *           in caso di errore
   */
  public static void main(String args[]) throws Exception {
    Properties p = new Properties();
    List<String> chiavi = new ArrayList<String>(Arrays.asList(MODELLI, DIPENDENZE, WORKSPACE, ELIMINA_WS, AS_STORED, REPORT, DRAWING, LOGIN, MODO,
        TEST));

    // parsa le property
    for (int i = 0; i < args.length; i++) {
      String[] coppia = args[i].split("=");
      if (coppia.length != 2) {
        System.out.println("E' stata inserito un argomento non ben formattato: " + args[i]);
        System.out.println("Gli argomenti devono avere la forma : chiave=valore");
        System.exit(0);
      }
      String chiave = coppia[0];
      String valore = coppia[1];
      if (!chiavi.contains(chiave)) {
        System.out.println("E' stata inserito un argomento non riconosciuto: " + chiave);
        System.exit(0);
      }
      System.out.println("Inserita property: " + chiave + "-->" + valore);
      p.setProperty(chiave, valore);
    }
    if (p.getProperty(MODO) == null) {
      System.out.println("Non hai inserito la modalit�! modo=copia o modo=checkout ");
    }

    // imposta user e pass
    user = CDTProperties.getProperty("ext.caditech.login.user", "wcadmin");
    password = CDTProperties.getProperty("ext.caditech.login.password", "wcadmin");

    // imposta il method server remoto
    RemoteMethodServer method = RemoteMethodServer.getDefault();
    method.setUserName(user);
    method.setPassword(password);

    // esegue l'opreazione voluta
    eseguiComeServer(p);

    // chiudo la consolle
    if (p.getProperty(TEST) != null && p.getProperty(TEST).equalsIgnoreCase("true")) {
      return;
    }
    System.exit(0);
  }

  /**
   * Metodo per testare da windchill shell le operazioni volute
   * 
   * @param p
   *          le properties per l'azione desiderata
   * @throws SearchUtilsException
   *           l'eccezione sulla ricerca
   * @throws CheckoutPDMLinkException
   *           l'eccezione sulle operazioni di checkout
   */
  public static void eseguiDaWindchillShell(Properties p) throws SearchUtilsException, CheckoutPDMLinkException {
    CheckoutUtils esecutore = new CheckoutUtils();
    StringWriter sw = new StringWriter();
    try {
      p.store(sw, null);
    }
    catch (IOException e) {
      LogWrapper.logMessage(LogWrapper.ERROR, "convertendo properties in string", e);
      throw new SearchUtilsException("Errore convertendo properties in string", e);
    }

    String modo = p.getProperty(MODO);

    if (modo.equalsIgnoreCase("copia")) {
      esecutore.eseguiCopiaLocale(sw.toString());
    }
    else if (modo.equalsIgnoreCase("copiatest")) {
      esecutore.testCopiaLocale(p.getProperty(LOGIN));
    }
    else if (modo.equalsIgnoreCase("checkout")) {
      esecutore.eseguiCheckout(p.getProperty(MODELLI), p.getProperty(WORKSPACE), p.getProperty(LOGIN));
    }
    else if (modo.equalsIgnoreCase("undo")) {
      esecutore.eseguiUndoCheckout(p.getProperty(MODELLI), p.getProperty(WORKSPACE), p.getProperty(LOGIN));
    }
    else if (modo.equalsIgnoreCase("undo2")) {
      esecutore.eseguiUndoCheckout(p.getProperty(MODELLI));
    }
    else if (modo.equalsIgnoreCase("svuota")) {
      esecutore.SvuotaWorkspace(p.getProperty(WORKSPACE), p.getProperty(LOGIN));
    }
    else if (modo.equalsIgnoreCase("svuota2")) {
      esecutore.SvuotaWorkspace2(p.getProperty(WORKSPACE), p.getProperty(LOGIN));
    }
    else {
      System.out.println("modalita' da eseguire non ammessa!");
    }
  }

  /**
   * Esegue la copia in locale
   * 
   * @param p
   *          properties per la copia in locale
   * @throws CheckoutPDMLinkException
   *           l'eccezione sulla copia
   * @throws SearchUtilsException
   *           l'eccezione sulla ricerca
   * @return Lista contenente i nomi dei file presenti nel workspace
   */
  @SuppressWarnings("deprecation")
  public ArrayList<String> testCopiaLocale(String login) throws SearchUtilsException, CheckoutPDMLinkException {
    LogWrapper.logMessage(LogWrapper.INFO, "-----------------------------------------------------------");
    LogWrapper.logMessage(LogWrapper.INFO, "--------------- TEST COPIA IN LOCALE " + CheckoutUtils.VERSION + " --------------");
    LogWrapper.logMessage(LogWrapper.INFO, "---------------- Metodo semplificato di copia in locale di parall.prt -------------");

    EPMDocument epm = SearchEPMDocument.findLatestEPMDocuments("parall.prt");
    // List<String> modelli = new ArrayList<String>();
    // modelli.add("parall.prt");
    //
    // LogWrapper.logMessage(LogWrapper.INFO, "Inizio copia locale di " + modelli.toString());
    // Vector<EPMDocument> documentiEPM = new Vector<EPMDocument>();
    //
    // // --------------------------------------------------------
    // // Cerco i documenti di cui bisogna fare la copia
    // // --------------------------------------------------------
    // List<String> documentiNonTrovati = new ArrayList<String>();
    // for (String modello : modelli) {
    // EPMDocument epm = SearchEPMDocument.findLatestEPMDocuments(modello);
    // if (epm != null) {
    // documentiEPM.add(epm);
    // LogWrapper.logMessage(LogWrapper.INFO, "Documento con number = " + epm.getNumber() + " e ver: " +
    // epm.getVersionIdentifier().getValue()
    // + " itr: " + epm.getIterationIdentifier().getValue() + " aggiunto alla lista per la copia in locale.");
    // }
    // else {
    // documentiNonTrovati.add(modello);
    // LogWrapper.logMessage(LogWrapper.WARN, "Documento con number = " + modello + " non trovato.");
    // }
    // }

    // --------------------------------------------------------
    // Cerco il container
    // --------------------------------------------------------
    String containerName = "Demo Organization";
    String containerType = "wt.pdmlink.PDMLinkProduct";
    String productName = "GENERIC_COMPUTER";

    LogWrapper.logMessage(LogWrapper.INFO, "Cerco il container di nome: " + containerName + " - tipo: " + containerType + " - prodotto: "
        + productName);
    WTContainerRef containerRef = null;
    try {
      containerRef = WTContainerHelper.service.getByPath("/wt.inf.container.OrgContainer=" + containerName + "/" + containerType + "=" + productName);
    }
    catch (WTException ex) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore cercando il container", ex);
      throw new SearchUtilsException("Errore cercando il container", ex);
    }
    if (containerRef == null) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Container non trovato");
      throw new SearchUtilsException("Container non trovato");
    }
    else {
      LogWrapper.logMessage(LogWrapper.INFO, "Container trovato");
    }

    // --------------------------------------------------------
    // Cerco l'utente
    // --------------------------------------------------------
    String userName = login;// prop.getProperty(LOGIN, "Administrator");// "Administrator";

    LogWrapper.logMessage(LogWrapper.INFO, "Ricerca WTUser per nome -> " + userName);
    WTUser user = null;
    try {
      QueryResult qr = null;
      QuerySpec qs = new QuerySpec(WTUser.class);
      SearchCondition sc = new SearchCondition(WTUser.class, WTUser.NAME, SearchCondition.EQUAL, userName);
      qs.appendWhere(sc);
      qr = PersistenceHelper.manager.find(qs);
      if (qr == null || !qr.hasMoreElements()) {
        LogWrapper.logMessage(LogWrapper.WARN, "Nessun utente con name = " + userName);
      }
      while (qr.hasMoreElements()) {
        user = (WTUser) qr.nextElement();
      }
    }
    catch (Exception ex) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore nel metodo findUser (" + userName + ")", ex);
      throw new SearchUtilsException("Errore nel metodo findUser (" + userName + ")", ex);
    }

    if (user == null) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Utente " + userName + " non trovato");
      throw new SearchUtilsException("Utente " + userName + " non trovato");
    }
    LogWrapper.logMessage(LogWrapper.INFO, "Utente " + user.getFullName() + " trovato");
    LogWrapper.logMessage(LogWrapper.INFO, "Dati utente: " + user.toString());

    // --------------------------------------------------------
    // Cerco/creo il workspace usando utente e container
    // --------------------------------------------------------
    String workspaceName = "test";// prop.getProperty(WORKSPACE, "ws_default");// "prova_2";

    EPMWorkspace epmw_find = null;
    try {
      WTSet workspaces = EPMWorkspaceHelper.manager.getWorkspaces(user, containerRef);
      if (workspaces == null || workspaces.isEmpty()) {
        LogWrapper.logMessage(LogWrapper.INFO, "Non � stato trovato nessun workspace");

      }
      LogWrapper.logMessage(LogWrapper.INFO, "WORKSPACE trovati: ");
      for (Object obj : workspaces) {
        ObjectReference or = (ObjectReference) obj;
        EPMWorkspace epmw = (EPMWorkspace) or.getObject();
        LogWrapper.logMessage(LogWrapper.INFO, "-" + epmw.getName());
        if (epmw.getName().equals(workspaceName)) {
          epmw_find = epmw;
          break;
        }
      }
    }
    catch (WTException ex) {
      LogWrapper.logMessage(LogWrapper.ERROR, "Errore cercando il workspace", ex);
      throw new SearchUtilsException("Errore cercando il workspace", ex);
    }

    // EPMWorkspace epmw_find = SearchUtils.findWorkspace(workspaceName, user, containerRef, daSvuotare);
    if (epmw_find == null) {
      LogWrapper.logMessage(LogWrapper.INFO, "Workspace " + workspaceName + " non trovato");
      throw new SearchUtilsException("Workspace " + workspaceName + " non creato/trovato");
    }
    else {
      LogWrapper.logMessage(LogWrapper.INFO, "Workspace " + workspaceName + " trovato");
    }
    boolean asStored = false;
    boolean drawing = false;
    // String dipendenze = "none";

    copiaLocaleEMPDocument(epm, epmw_find, drawing, "add");
    // --------------------------------------------------------
    // Eseguo il popolamento di modelli e disegni ( se l'opzione drawing � true)
    // --------------------------------------------------------
    // eseguiPopolamento(documentiEPM, epmw_find, dipendenze, asStored, drawing);

    // --------------------------------------------------------
    // Scrittura file con esito copia locale
    // --------------------------------------------------------
    ArrayList<String> nomiOggettiNelWS = new ArrayList<String>();// scriviFileDiEsito(epmw_find,
                                                                 // "C:\\Temp\\copiaLocale_windchill.txt",
    // documentiNonTrovati);

    LogWrapper.logMessage(LogWrapper.INFO, "Fine della copia in locale");
    LogWrapper.logMessage(LogWrapper.INFO, "-----------------------------------------------------------");
    return nomiOggettiNelWS;
  }
}
