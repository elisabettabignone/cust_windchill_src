package ext.caditech.checkout;

/**
 * Eccezione sollevata dal checkout Windchill
 * 
 * @author s.menocci
 * @author e.parodi
 *
 */
public class CheckoutPDMLinkException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = -4904799332005770940L;

  public CheckoutPDMLinkException(String msg) {
    super(msg);
  }

  public CheckoutPDMLinkException(String msg, Throwable causa) {
    super(msg, causa);
  }

  public CheckoutPDMLinkException(Throwable causa) {
    super(causa);
  }
}
