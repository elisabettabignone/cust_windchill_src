@REM Comandi batch per copiare dalle cartelle del progetto (windchill\src) a quelle del progetto (WebServiceCaditech)
@REM NOTA si deve eseguire da Windchill Shell, con il method server ATTIVO

@ECHO OFF
@ECHO ***********************************************************
@ECHO * Inizio procedura per rilascio web service per windchill *
@ECHO ***********************************************************
@ECHO **** Backup
@REM Setta le cartelle di origine del sorgente
SET Orig_Srv=C:\ptc\Windchill_10.2\Windchill\src\ext\caditech
SET Orig_Cln=C:\ptc\Windchill_10.2\Windchill\src_client\ext\caditech

@REM Setta le cartelle di destinazione del sorgente
SET Dest_Srv=C:\WebServiceCaditech\jws\PDMLinkServiceSource\src\ext\caditech
SET Dest_Cln=C:\WebServiceCaditech\jws\PDMLinkServiceSource\src_client\ext\caditech

@REM Setta le cartelle per il comando Ant
SET Ant_Srv=C:\WebServiceCaditech\jws\PDMLinkServiceSource\src
SET Ant_Cln=C:\WebServiceCaditech\jws\PDMLinkServiceSource\src_client

@ECHO **** Rimozione vecchia cartella di backup
@REM Rimuove la cartella con il backup precedente ed effettua il backup del corrente servizio
RMDIR C:\WebServiceCaditech\jws\PDMLinkServiceSource\backup /s /q
MKDIR C:\WebServiceCaditech\jws\PDMLinkServiceSource\backup

@ECHO **** Creazione nuova copia di backup
@REM Copia la cartella del codice sorgente
MKDIR C:\WebServiceCaditech\jws\PDMLinkServiceSource\backup\src
@ECHO ON
ROBOCOPY C:\WebServiceCaditech\jws\PDMLinkServiceSource\src C:\WebServiceCaditech\jws\PDMLinkServiceSource\backup\src /e /ns /nc /nfl /ndl /np /njh /njs
@ECHO OFF

@REM Copia la cartella del codice sorgente del client
MKDIR C:\WebServiceCaditech\jws\PDMLinkServiceSource\backup\src_client
@ECHO ON
ROBOCOPY C:\WebServiceCaditech\jws\PDMLinkServiceSource\src_client C:\WebServiceCaditech\jws\PDMLinkServiceSource\backup\src_client /e /ns /nc /nfl /ndl /np /njh /njs
@ECHO OFF

@REM Copia la cartella codebase del servizio
MKDIR C:\WebServiceCaditech\jws\PDMLinkServiceSource\backup\codebase_service
@ECHO ON
ROBOCOPY C:\WebServiceCaditech\jws\PDMLinkServiceSource\codebase_service C:\WebServiceCaditech\jws\PDMLinkServiceSource\backup\codebase_service /e /ns /nc /nfl /ndl /np /njh /njs
@ECHO OFF

@REM Copia la cartella codebase del client
MKDIR C:\WebServiceCaditech\jws\PDMLinkServiceSource\backup\codebase_client
@ECHO ON
ROBOCOPY C:\WebServiceCaditech\jws\PDMLinkServiceSource\codebase_client C:\WebServiceCaditech\jws\PDMLinkServiceSource\backup\codebase_client /e /ns /nc /nfl /ndl /np /njh /njs
@ECHO OFF

@REM Copia la cartella dist del client
MKDIR C:\WebServiceCaditech\jws\PDMLinkServiceSource\backup\dist_client
@ECHO ON
ROBOCOPY C:\WebServiceCaditech\jws\PDMLinkServiceSource\dist_client C:\WebServiceCaditech\jws\PDMLinkServiceSource\backup\dist_client /e /ns /nc /nfl /ndl /np /njh /njs
@ECHO OFF

@REM Copia la cartella gensrc del client
MKDIR C:\WebServiceCaditech\jws\PDMLinkServiceSource\backup\gensrc_client
@ECHO ON
ROBOCOPY C:\WebServiceCaditech\jws\PDMLinkServiceSource\gensrc_client C:\WebServiceCaditech\jws\PDMLinkServiceSource\backup\gensrc_client /e /ns /nc /nfl /ndl /np /njh /njs
@ECHO OFF

@ECHO **** Rimozione cartelle generate da ant
@REM Rimuove la cartella codebase del servizio
RMDIR C:\WebServiceCaditech\jws\PDMLinkServiceSource\codebase_service /s /q

@REM Rimuove la cartella codebase del client
RMDIR C:\WebServiceCaditech\jws\PDMLinkServiceSource\codebase_client /s /q

@REM Rimuove la cartella dist del client
RMDIR C:\WebServiceCaditech\jws\PDMLinkServiceSource\dist_client /s /q

@REM Rimuove la cartella gensrc del client
RMDIR C:\WebServiceCaditech\jws\PDMLinkServiceSource\gensrc_client /s /q

@ECHO **** Copia da windchill del codice sorgente
RMDIR "%Dest_Srv%" /s /q
MKDIR "%Dest_Srv%"
@ECHO ON
ROBOCOPY "%Orig_Srv%" "%Dest_Srv%" /e /ns /nc /nfl /ndl /np /njh /njs
@ECHO OFF

RMDIR "%Dest_Cln%" /s /q
MKDIR "%Dest_Cln%"
@ECHO ON
ROBOCOPY "%Orig_Cln%" "%Dest_Cln%" /e /ns /nc /nfl /ndl /np /njh /njs
@ECHO OFF

@ECHO **** Compilazione della parte Server tramite Ant
CD "%Ant_Srv%"
@ECHO ON
CALL ANT
@ECHO OFF

@REM PAUSE
@ECHO **** Compilazione della parte Client tramite Ant
CD "%Ant_Cln%"
@ECHO ON
CALL ANT
@ECHO OFF
CD..

@ECHO **************************************************************
@ECHO * Terminata procedura per rilascio web service per windchill *
@ECHO **************************************************************
@ECHO ON
