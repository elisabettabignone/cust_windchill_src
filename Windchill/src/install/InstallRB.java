/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package install;

import wt.util.resource.*;

@RBUUID("install.InstallRB")
public final class InstallRB extends WTListResourceBundle {
   @RBEntry("Unable to invoke Netscape privilege")
   public static final String NO_NETSCAPE_PRIVILEGE = "1";

   @RBEntry("To use this plugin, you will have to add the following to your CLASSPATH: {0}")
   public static final String ADD_TO_CLASSPATH = "10";

   @RBEntry("The plugins location is where the installer will write the PTC Viewer plugin.The PTC Viewer location is where the installer will write the application files used by the PTC Viewer plugin.  The application files load point should not be in the plugins directory.  The Web Browser Executable is the actual executable that starts the web browser.  Note that all three values can be browsed for by using the browse buttons.  In the case of the plugins and PTC Viewer location, the file name provided can be ignored.  It is simply a place holder as we are only interested in the directory.  In the case of the Web browser executable, you must select the executable file.  You will be prompted if it is ok to replace this executable.  This is part of the standard dialog and you can reply 'yes'.  The executable will not be replaced by this program.")
   public static final String VIEWER_HELP = "11";

   @RBEntry("The plugins location is where the installer will write the PTC Viewer plugin.  With Netscape, the plugins location is usually C:\\Program Files\\Netscape\\Communicator\\Program\\Plugins\\  The PTC Viewer location is where the installer will write the application files used by the PTC Viewer plugin.  The application files load point should not be in the plugins directory.  The Web Browser Executable is the actual executable that starts the web browser.  Note that all three values can be browsed for by using the browse buttons.  In the case of the plugins and PTC Viewer location, the file name provided can be ignored.  It is simply a place holder as we are only interested in the directory.  In the case of the Web browser executable, you must select the executable file.  You will be prompted if it is ok to replace this executable.  This is part of the standard dialog and you can reply 'yes'.  The executable will not be replaced by this program.")
   public static final String VIEWER_HELP_NETSCAPE_NT = "12";

   @RBEntry("The plugins location is where the installer will write the PTC Viewer plugin.  With Netscape, the plugins location is usually /usr/local/lib/netscape/plugins/ or $$HOME/.netscape/plugins/  The PTC Viewer location is where the installer will write the application files used by the PTC Viewer plugin.  The application files load point should not be in the plugins directory.  The Web Browser Executable is the actual executable that starts the web browser.  Note that all three values can be browsed for by using the browse buttons.  In the case of the plugins and PTC Viewer location, the file name provided can be ignored.  It is simply a place holder as we are only interested in the directory.  In the case of the Web browser executable, you must select the executable file.  You will be prompted if it is ok to replace this executable.  This is part of the standard dialog and you can reply 'yes'.  The executable will not be replaced by this program.")
   public static final String VIEWER_HELP_NETSCAPE_UNIX = "13";

   @RBEntry("The plugins location is where the installer will write the PTC Viewer plugin.  With Microsoft Internet Explorer, the plugins location is usually C:\\Program Files\\Plus!\\Microsoft Internet\\plugins\\The PTC Viewer location is where the installer will write the application files used by the PTC Viewer plugin.  The application files load point should not be in the plugins directory.  The Web Browser Executable is the actual executable that starts the web browser.  Note that all three values can be browsed for by using the browse buttons.  In the case of the plugins and PTC Viewer location, the file name provided can be ignored.  It is simply a place holder as we are only interested in the directory.  In the case of the Web browser executable, you must select the executable file.  You will be prompted if it is ok to replace this executable.  This is part of the standard dialog and you can reply 'yes'.  The executable will not be replaced by this program.")
   public static final String VIEWER_HELP_IE_NT = "14";

   @RBEntry("Installation Complete.  You must restart your browser session to enable the plugin.")
   public static final String INSTALL_COMPLETE_RESTART = "15";

   @RBEntry("{0} is not supported")
   public static final String OS_NOT_SUPPORTED = "16";

   @RBEntry("Copy of the plugin to the plugin location failed. Cannot write to {0}")
   public static final String COPY_FAILED = "17";

   @RBEntry("Copy of the plugin to the plugin location failed.  See Java Console for details.")
   public static final String COPY_FAILED_TRACE = "18";

   @RBEntry("Edit of {0} failed.")
   public static final String EDIT_FAILED = "19";

   @RBEntry("Installation Complete")
   public static final String INSTALL_COMPLETE = "2";

   @RBEntry("Edit of {0} failed.  See Java Console for details.")
   public static final String EDIT_FAILED_TRACE = "20";

   @RBEntry("Installing files...")
   public static final String INSTALLING_FILES = "21";

   @RBEntry("Editing the script file")
   public static final String EDIT_SCRIPT = "22";

   @RBEntry("Editing ptc_help.txt file")
   public static final String EDIT_HELP = "23";

   @RBEntry("Executing setup")
   public static final String EXECUTING_SETUP = "24";

   @RBEntry("Copying the plugin file to the plugins location")
   public static final String COPY_PLUGIN = "25";

   @RBEntry("You must specify a plugins location")
   public static final String PLUGIN_LOC_NEEDED = "26";

   @RBEntry("You must specify a location to put the PTC Viewer application files")
   public static final String PTCVIEWER_LOC_NEEDED = "27";

   @RBEntry("You must specify the absolute path of your Web browser executable")
   public static final String BROWSER_EXEC_NEEDED = "28";

   @RBEntry("PTC Viewer Application Files location")
   public static final String PTCVIEWER_BROWSE_DIALOG_TITLE = "29";

   @RBEntry("Installation failed. Cannot write to {0}")
   public static final String INSTALL_FAILED = "3";

   @RBEntry("Web Browser Executable")
   public static final String EXEC_BROWSE_DIALOG_TITLE = "30";

   @RBEntry("Windchill JRE location")
   public static final String IGW_BROWSE_DIALOG_TITLE = "31";

   @RBEntry("The JRE location is where JREs are to be installed on your client system.")
   public static final String INTRALINKGW_HELP = "32";

   @RBEntry("Application launch failed.  See Java Console for details.")
   public static final String APP_LAUNCH_FAILED = "33";

   @RBEntry("You are required to have the Bootstrap Loader installed on this client before installing this feature.  You will be redirected to the Bootstrap Installation page.  Once the Bootstrap Loader is installed, you must restart this installation.")
   public static final String BOOT_NEEDED = "34";

   @RBEntry("The JRE location is where JREs are to be installed on your client system.  The TEMP Directory is the value of the TEMP environment variable, which is your temporary directory.")
   public static final String DESKTOP_HELP = "35";

   @RBEntry("Unable to access environment variables.  Installation failed.")
   public static final String NO_ENVIRONMENT = "36";

   @RBEntry("JRE Location must be specified")
   public static final String MUST_SPECIFY_JRE = "37";

   @RBEntry("TEMP directory must be specified")
   public static final String MUST_SPECIFY_TEMP = "38";

   @RBEntry("Installing {0}")
   public static final String INSTALL_FILE = "39";

   @RBEntry("Installation failed.  See Java Console for details.")
   public static final String INSTALL_FAILED_TRACE = "4";

   @RBEntry("Unable to write to {0}")
   public static final String NO_WRITE = "40";

   @RBEntry("Workgroup Manager Location")
   public static final String WM_BROWSE_DIALOG_TITLE = "41";

   @RBEntry("TEMP directory")
   public static final String TEMP_BROWSE_DIALOG_TITLE = "42";

   @RBEntry("Workgroup Manager location must be specified")
   public static final String MUST_SPECIFY_WMDIR = "43";

   @RBEntry("The JRE location is where JREs are to be installed on your client system.  The Workgroup Manager Location is where files and libraries used by Workgroup Managers will be written.")
   public static final String CADDS_HELP = "44";

   @RBEntry("The JRE location is where JREs are to be installed on your client system.  The Workgroup Manager Location is where files and libraries used by Workgroup Managers will be written.")
   public static final String PROE_HELP = "45";

   @RBEntry("Checking for JRE...")
   public static final String CHECK_JRE = "46";

   @RBEntry("The script to launch this application, {0}, has been written to {1}.")
   public static final String LAUNCH_SCRIPT = "47";

   @RBEntry("The JRE location is where JREs are to be installed on your client system.  The Workgroup Manager Location is where files and libraries used by Workgroup Managers will be written.")
   public static final String AUTOCAD_HELP = "48";

   @RBEntry("The JRE location is where JREs are to be installed on your client system.  The Workgroup Manager Location is where files and libraries used by Workgroup Managers will be written.")
   public static final String HELIX_HELP = "49";

   @RBEntry("Plugins Location")
   public static final String BROWSE_DIALOG_TITLE = "5";

   @RBEntry("Updating registry...")
   public static final String SETTING_REGISTRY = "50";

   @RBEntry("Specified JRE location not found")
   public static final String JRE_NOT_FOUND = "51";

   @RBEntry("Welcome to the Windchill Workgroup Manager for {0} Installation Wizard")
   public static final String WELCOME_MESSAGE = "52";

   @RBEntry("This wizard will guide you through the installation of the Windchill Workgroup Manager for {0}.")
   public static final String WIZARD_DESC = "53";

   @RBEntry("The Java Runtime Environment (JRE) is needed by Workgroup Manager for {0} and will now be installed on your computer. Please choose the directory where you would like the JRE installed.")
   public static final String JRE_INSTRUCTION = "54";

   @RBEntry("Program files used by Workgroup Manager for {0} need to be copied to your computer. Please choose the directory where you would like these program files installed.")
   public static final String WM_INSTRUCTION = "55";

   @RBEntry("To be able to communicate with Pro/ENGINEER, the Workgroup Manager needs to know where Pro/ENGINEER is installed on your computer. Please choose the directory where Pro/ENGINEER is currently installed.")
   public static final String PROE_INSTRUCTION = "56";

   @RBEntry("The Workgroup Manager for Pro/ENGINEER needs to know the Pro/ENGINEER start command in order to start Pro/ENGINEER. Please specify the Pro/ENGINEER start command.")
   public static final String PROESTARTCOMMAND_INSTRUCTION = "57";

   @RBEntry("Completing the Windchill Workgroup Manager for {0} Installation Wizard")
   public static final String COMPLETE_INSTALL = "58";

   @RBEntry("Installation of Windchill Workgroup Manager for {0} was completed successfully. To start the Workgroup Manager, execute \"{1}\" under directory \"{2}\".")
   public static final String INSTALL_SUCCESSFUL = "59";

   @RBEntry("Select the plugins directory.  This is where the installer will write the Bootstrap loader plugin.")
   public static final String BOOT_HELP = "6";

   @RBEntry("- JRE Installation Directory:")
   public static final String JRE_INSTALL_DIRECTORY = "60";

   @RBEntry("- Workgroup Manager for {0} Installation Directory:")
   public static final String WM_INSTALL_DIRECTORY = "61";

   @RBEntry("- Pro/ENGINEER Directory:")
   public static final String PROE_INSTALL_DIRECTORY = "62";

   @RBEntry("- Pro/ENGINEER Start Command:")
   public static final String PROE_START_CMD_NAME = "63";

   @RBEntry("The Java Runtime Environment (JRE) is needed by Workgroup Manager for {0}. Please specify the directory where JRE is installed.")
   public static final String JRE_INSTRUCTION_AIX = "64";

   @RBEntry("Downloading and unpacking {0}. Please wait...")
   public static final String DOWNLOADING_AND_UNPACKING_JAR = "65";

   @RBEntry("Downloading {0}...")
   public static final String DOWNLOADING_FILE = "66";

   @RBEntry("Creating Script...")
   public static final String CREATING_SCRIPT = "67";

   @RBEntry("Are you sure that you want to exit?")
   public static final String CONFIRM_EXIT = "68";

   @RBEntry("Really exit?")
   public static final String TITLE_EXIT = "69";

   @RBEntry("Setting file permission...")
   public static final String SETTING_PERMISSION = "70";

   @RBEntry("Select the plugins directory.  This is where the installer will write the Bootstrap loader plugin.  With Netscape, the plugins location is usually C:\\Program Files\\Netscape\\Communicator\\Program\\Plugins\\  Note if using the Browse button, you can ignore the value put in for the file name.  It is simply a place  holder as we are only interested in the directory.")
   public static final String PLUGIN_HELP_NETSCAPE_NT = "7";

   @RBEntry("Cache location must be specified")
   public static final String MUST_SPECIFY_CACHEDIR = "71";

   @RBEntry("The cache directory is needed by Workgroup Manager for {0}. Please specify a cache directory.")
   public static final String CACHE_INSTRUCTION = "72";

   @RBEntry("- Cache Directory:")
   public static final String CACHE_DIRECTORY = "73";

   @RBEntry("Installation of Windchill Workgroup Manager for {0} was successful.")
   public static final String SETUP_SUCCESSFUL = "74";

   @RBEntry("Extracting {0}...")
   public static final String EXTRACTING_ARCHIVE = "75";

   @RBEntry("Copying and registering files...")
   public static final String COPYING_AND_REGISTERING = "76";

   @RBEntry("Cleaning up temporary files...")
   public static final String CLEANING_TEMP_FILES = "77";

   @RBEntry("In order to complete this setup successfully, you must have \"Cadence PCB Systems 14.0\" or later installed on your system. Setup will exit now.")
   public static final String CADENCE_REQUIRED = "78";

   @RBEntry("Global Setup is not yet available, please contact your CATIA system administrator for further details and continue with Personal Setup if you like.")
   public static final String GLOBAL_SETUP_NOT_AVAILABLE = "79";

   @RBEntry("Select the plugins directory.  This is where the installer will write the Bootstrap loader plugin.  With Netscape, the plugins location is usually /usr/local/lib/netscape/plugins/ or $$HOME/.netscape/plugins/  Note if using the Browse button, you can ignore the value put in for the file name.  It is simply a place  holder as we are only interested in the directory.")
   public static final String PLUGIN_HELP_NETSCAPE_UNIX = "8";

   @RBEntry("Global Setup is available and only supports the default Dassault CATIA user environment. Please execute the global WGM CATIA start command \"wmcat\" in order to start the Workgroup Manager. Folder \"cfg\" will be created under $HOME/db if it does not already exist.")
   public static final String GLOBAL_SETUP_AVAILABLE = "80";

   @RBEntry("Installation failed. The environment variable CAT_CUST is not set. Please notify your CATIA admistrator.")
   public static final String CATCUST_NOT_SET = "81";

   @RBEntry("Global Setup is complete. Execute \"{0}\" to start Workgroup Manager for CATIA. The global start command for all WM CATIA users is \"wmcat\" and it is located under \"{1}\" directory.")
   public static final String ADMIN_GLOBAL_SETUP_COMPLETE = "82";

   @RBEntry("Configuration Change Needed.")
   @RBComment("Title for dialog warning user to make a configuration change.")
   public static final String BOOTSTRAP_CONFIG_TITLE = "83";

   @RBEntry("The Windchill Bootstrap was not installed in default location.\nAdd the following to the Java Runtime Parameters in the Java Plugin Control Panel:\n-Xbootclasspath/p:")
   @RBComment("Instructions for how to manually change the configuration.")
   public static final String BOOTSTRAP_CONFIG_CHANGE_MSG = "84";

   @RBEntry("Select the plugins directory.  This is where the installer will write the Bootstrap loader plugin.  With Microsoft Internet Explorer, the plugins location is usually C:\\Program Files\\Plus!\\Microsoft Internet\\plugins\\  Note if using the Browse button, you can ignore the value put in for the file name.  It is simply a place  holder as we are only interested in the directory.")
   public static final String PLUGIN_HELP_IE_NT = "9";

   @RBEntry("< Back")
   public static final String PRIVATE_CONSTANT_0 = "BBack";

   @RBEntry("Browse...")
   public static final String PRIVATE_CONSTANT_1 = "BBrowse";

   @RBEntry("Cancel")
   public static final String PRIVATE_CONSTANT_2 = "BCancel";

   @RBEntry("Finish")
   public static final String PRIVATE_CONSTANT_3 = "BFinish";

   @RBEntry("Help")
   public static final String PRIVATE_CONSTANT_4 = "BHelp";

   @RBEntry("Next >")
   public static final String PRIVATE_CONSTANT_5 = "BNext";

   @RBEntry("OK")
   public static final String PRIVATE_CONSTANT_6 = "BOK";

   @RBEntry("Personal Setup")
   public static final String PRIVATE_CONSTANT_7 = "BPSButton";

   @RBEntry("Global Setup")
   public static final String PRIVATE_CONSTANT_8 = "BGSButton";

   @RBEntry("Windchill Workgroup Manager for CADDS Installation")
   public static final String PRIVATE_CONSTANT_9 = "LCADDS";

   @RBEntry("Windchill Workgroup Manager for Cadence Installation")
   public static final String PRIVATE_CONSTANT_10 = "LCadence";

   @RBEntry("Windchill Workgroup Manager for CATIA Installation")
   public static final String PRIVATE_CONSTANT_11 = "LCATIA";

   @RBEntry("Windchill Workgroup Manager for Pro/ENGINEER Installation")
   public static final String PRIVATE_CONSTANT_12 = "LProE";

   @RBEntry("TEMP Directory")
   public static final String PRIVATE_CONSTANT_13 = "LTempDirectory";

   @RBEntry("Windchill Bootstrap Install")
   public static final String PRIVATE_CONSTANT_14 = "Lboot";

   @RBEntry("Installing the Bootstrap Loader Plugin.  Select plugins location and press OK to install.")
   public static final String PRIVATE_CONSTANT_15 = "LbootInstructions";

   @RBEntry("Web Browser Executable:")
   public static final String PRIVATE_CONSTANT_16 = "LbrowseExecLocation";

   @RBEntry("Set the Cache Directory")
   public static final String PRIVATE_CONSTANT_17 = "LcacheDir";

   @RBEntry("Enter the cache directory:")
   public static final String PRIVATE_CONSTANT_18 = "LcacheLocation";

   @RBEntry("Choose a Setup Option")
   public static final String PRIVATE_CONSTANT_19 = "LchooseOption";

   @RBEntry("To continue, click next.")
   public static final String PRIVATE_CONSTANT_20 = "Lcontinue";

   @RBEntry("To close this wizard, click Finish.")
   public static final String PRIVATE_CONSTANT_21 = "Lfinish";

   @RBEntry("Pro/INTRALINK Gateway Install")
   public static final String PRIVATE_CONSTANT_22 = "Ligw";

   @RBEntry(" Installing the Pro/INTRALINK Gateway client. ")
   public static final String PRIVATE_CONSTANT_23 = "LigwInstructions";

   @RBEntry("Enter the directory to install the JRE in:")
   public static final String PRIVATE_CONSTANT_24 = "LjavaLocation";

   @RBEntry("Enter the directory where JRE is installed:")
   public static final String PRIVATE_CONSTANT_25 = "LjavaLocation_AIX";

   @RBEntry("Set the JRE Installation Directory")
   public static final String PRIVATE_CONSTANT_26 = "LJREDir";

   @RBEntry("Please choose one of the following:")
   public static final String PRIVATE_CONSTANT_27 = "LoptionInstruction";

   @RBEntry("Plugins Location:")
   public static final String PRIVATE_CONSTANT_28 = "LpluginLocation";

   @RBEntry("Set the Pro/ENGINEER Directory")
   public static final String PRIVATE_CONSTANT_29 = "LProEDir";

   @RBEntry("Enter the directory where Pro/ENGINEER is installed:")
   public static final String PRIVATE_CONSTANT_30 = "LProELocation";

   @RBEntry("Specify the Pro/ENGINEER Start Command")
   public static final String PRIVATE_CONSTANT_31 = "LProEStartCommand";

   @RBEntry("Enter the start command name used to start Pro/ENGINEER:")
   public static final String PRIVATE_CONSTANT_32 = "LProEStartCommandName";

   @RBEntry("PTC Viewer Install")
   public static final String PRIVATE_CONSTANT_33 = "Lprofly";

   @RBEntry("Installing the PTC Viewer Plugin.  Make selections then press OK to install.")
   public static final String PRIVATE_CONSTANT_34 = "LproflyInstructions";

   @RBEntry("PTC Viewer Location:")
   public static final String PRIVATE_CONSTANT_35 = "LproflyLocation";

   @RBEntry("Verify Installation Settings")
   public static final String PRIVATE_CONSTANT_36 = "LSummary";

   @RBEntry("You have given the following installation settings:")
   public static final String PRIVATE_CONSTANT_37 = "LSummarySettings";

   @RBEntry("Set the Workgroup Manager Directory")
   public static final String PRIVATE_CONSTANT_38 = "LWMDir";

   @RBEntry("Enter the directory to copy the files in:")
   public static final String PRIVATE_CONSTANT_39 = "LWMLocation";

   @RBEntry("Bootstrap Loader uses the cache directory to locally cache jar files")
   public static final String PRIVATE_CONSTANT_40 = "TcacheDesc";

   @RBEntry("To change any of the above values, click the Back button to return to previous screens where the values were set. Otherwise, click Next to begin the installation.")
   public static final String PRIVATE_CONSTANT_41 = "TchangeInstruction";

   @RBEntry("Setup is now copying all the necessary files to your computer.")
   public static final String PRIVATE_CONSTANT_42 = "TcopyFiles";

   @RBEntry("Installation Message")
   public static final String PRIVATE_CONSTANT_43 = "TinfoDialog";

   @RBEntry("The Java Runtime Environment (JRE) contains the support needed for running Java applications, including the Windchill Workgroup Manager.")
   public static final String PRIVATE_CONSTANT_44 = "TJREDesc";

   @RBEntry("Setup will perform Personal or Global Setup depending on the option you choose.")
   public static final String PRIVATE_CONSTANT_45 = "ToptionDesc";

   @RBEntry("The Workgroup Manager for Pro/ENGINEER works with your existing Pro/ENGINEER installation.")
   public static final String PRIVATE_CONSTANT_46 = "TProEDesc";

   @RBEntry("The start command for Pro/ENGINEER is the command used to start Pro/ENGINEER.")
   public static final String PRIVATE_CONSTANT_47 = "TProEStartCommandDesc";

   @RBEntry("You can verify the values you have set for the installation options before the installation completes.")
   public static final String PRIVATE_CONSTANT_48 = "TSummaryDesc";

   @RBEntry("During the installation, the Workgroup Manager program files are copied your computer.")
   public static final String PRIVATE_CONSTANT_49 = "TWMDesc";

   @RBEntry("Welcome to the Windchill Bootstrap Installation Wizard")
   public static final String PRIVATE_CONSTANT_50 = "TBootWelcome";

   @RBEntry("This wizard will guide you through the installation of the Windchill Bootstrap.")
   public static final String PRIVATE_CONSTANT_51 = "TBootInstallDesc";

   @RBEntry("Set the Bootstrap Installation Location")
   public static final String PRIVATE_CONSTANT_52 = "LBootDir";

   @RBEntry("Bootstrap download location must be specified.")
   public static final String PRIVATE_CONSTANT_53 = "TBootMustBeSpecified";

   @RBEntry("The Windchill Bootstrap is a Java Archive (JAR) file that contains the support needed for bootstrapping Windchill clients.")
   public static final String PRIVATE_CONSTANT_54 = "TBootDesc";

   @RBEntry("Please choose the file location where you would like the Bootstrap JAR installed.  The standard location is within the Java Standard Extensions directory.  If you are unable to write to this location, the JAR may be downloaded to any other location but additional configuration steps may be required.")
   public static final String PRIVATE_CONSTANT_55 = "TBootInstr";

   @RBEntry("Save Bootstrap JAR as:")
   public static final String PRIVATE_CONSTANT_56 = "LBootLocation";

   @RBEntry("Windchill Bootstrap Installation Complete.")
   public static final String PRIVATE_CONSTANT_57 = "TBootComplete";

   @RBEntry("Installation of the Windchill Bootstrap was completed successfully.")
   public static final String PRIVATE_CONSTANT_58 = "TBootSuccess";

   @RBEntry("- Save Bootstrap JAR as:")
   public static final String PRIVATE_CONSTANT_59 = "LSaveBootstrapSummary";

   @RBEntry("Save Bootstrap as...")
   public static final String PRIVATE_CONSTANT_60 = "TSaveBootstrapAs";
}
