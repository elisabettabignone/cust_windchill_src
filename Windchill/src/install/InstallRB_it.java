/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package install;

import wt.util.resource.*;

@RBUUID("install.InstallRB")
public final class InstallRB_it extends WTListResourceBundle {
   @RBEntry("Impossibile richiamare il privilegio Netscape.")
   public static final String NO_NETSCAPE_PRIVILEGE = "1";

   @RBEntry("Per utilizzare il plug-in, è necessario aggiungere quanto segue al CLASSPATH: {0}")
   public static final String ADD_TO_CLASSPATH = "10";

   @RBEntry("La posizione dei plug-in è determinata dalla directory nella quale il programma di installazione memorizza il plug-in PTC Viewer. La posizione di PTC Viewer è determinata dalla directory di installazione dei file di applicazione utilizzati dal plug-in. La directory di installazione dei file di applicazione non deve trovarsi nella directory dei plug-in. Il file eseguibile del browser Web è quello che avvia il browser Web. È possibile ricercare i tre valori utilizzando i pulsanti Sfoglia. Per quanto riguarda i plug-in e di PTC Viewer, è possibile ignorare il nome file fornito. Questo è semplicemente un segnaposto, infatti è importante specificare solo la directory. Per quanto riguarda l'eseguibile del browser Web, è necessario selezionare il file eseguibile. Il sistema richiede se sostituire il file. Nella finestra di dialogo standard è possibile selezionare \"Sì\". L'eseguibile non viene quindi sostituito dal programma.")
   public static final String VIEWER_HELP = "11";

   @RBEntry("La posizione dei plug-in è determinata dalla directory nella quale il programma di installazione memorizza il plug-in PTC Viewer. In Netscape, generalmente la posizione dei plug-in è C:\\Programmi\\Netscape\\Communicator\\Program\\Plugins\\ La posizione di PTC Viewer è determinata dalla directory di installazione dei file di applicazione utilizzati dal plug-in PTC Viewer. La directory di installazione dei file di applicazione non deve trovarsi nella directory dei plug-in. Il file eseguibile del browser Web è quello che avvia il browser Web. È possibile ricercare i tre valori utilizzando i pulsanti Sfoglia. Per quanto riguarda la posizione dei plug-in e di PTC Viewer, è possibile ignorare il nome file fornito. Questo è semplicemente un segnaposto, infatti è importante specificare solo la directory. Per quanto riguarda l'eseguibile del browser Web, è necessario selezionare il file eseguibile. Il sistema richiede se sostituire il file. Nella finestra di dialogo standard è possibile selezionare \"Sì\". L'eseguibile non viene quindi sostituito dal programma.")
   public static final String VIEWER_HELP_NETSCAPE_NT = "12";

   @RBEntry("Il programma di installazione memorizza il plug-in di PTC Viewer nella directory dei plug-in. In Netscape la posizione dei plug-in è /usr/local/lib/netscape/plugins/ oppure $$HOME/.netscape/plugins/. La posizione di PTC Viewer è determinata dalla directory di installazione dei file di applicazione utilizzati dal plug-in. La directory di installazione dei file di applicazione non deve trovarsi nella directory dei plug-in. Il file eseguibile del browser Web è quello che avvia il browser Web. È possibile ricercare i tre valori utilizzando i pulsanti Sfoglia. Per quanto riguarda i plug-in e di PTC Viewer, è possibile ignorare il nome file fornito.  Questo è semplicemente un segnaposto, infatti è importante specificare solo la directory. Per quanto riguarda l'eseguibile del browser Web, è necessario selezionare il file eseguibile. Il sistema richiede se sostituire il file. Nella finestra di dialogo standard è possibile selezionare \"Sì\". L'eseguibile non viene quindi sostituito dal programma.")
   public static final String VIEWER_HELP_NETSCAPE_UNIX = "13";

   @RBEntry("La posizione dei plug-in è determinata dalla directory nella quale il programma di installazione memorizza il plug-in PTC Viewer. In Microsoft Internet Explorer, generalmente la posizione dei plug-in è C:\\Programmi\\Plus!\\Microsoft Internet\\plugins\\ La posizione di PTC Viewer è determinata dalla directory di installazione dei file di applicazione utilizzati dal plug-in PTC Viewer. La directory di installazione dei file di applicazione non deve trovarsi nella directory dei plug-in. Il file eseguibile del browser Web è quello che avvia il browser Web. È possibile ricercare i tre valori utilizzando i pulsanti Sfoglia. Per quanto riguarda la posizione dei plug-in e di PTC Viewer, è possibile ignorare il nome file fornito. Questo è semplicemente un segnaposto, infatti è importante specificare solo la directory. Per quanto riguarda l'eseguibile del browser Web, è necessario selezionare il file eseguibile. Il sistema richiede se sostituire il file. Nella finestra di dialogo standard è possibile selezionare \"Sì\". L'eseguibile non viene quindi sostituito dal programma.")
   public static final String VIEWER_HELP_IE_NT = "14";

   @RBEntry("Installazione completata. Riavviare la sessione del browser per attivare il plug-in.")
   public static final String INSTALL_COMPLETE_RESTART = "15";

   @RBEntry("{0} non supportato.")
   public static final String OS_NOT_SUPPORTED = "16";

   @RBEntry("Copia del plug-in nella directory dei plug-in non riuscita. Impossibile scrivere su {0}")
   public static final String COPY_FAILED = "17";

   @RBEntry("Copia del plug-in nella directory dei plug-in non riuscita. Vedere la console di Java per i dettagli.")
   public static final String COPY_FAILED_TRACE = "18";

   @RBEntry("Modifica di {0} non riuscita.")
   public static final String EDIT_FAILED = "19";

   @RBEntry("Installazione completata")
   public static final String INSTALL_COMPLETE = "2";

   @RBEntry("Modifica di {0} non riuscita. Vedere la console di Java per i dettagli.")
   public static final String EDIT_FAILED_TRACE = "20";

   @RBEntry("Installazione dei file in corso...")
   public static final String INSTALLING_FILES = "21";

   @RBEntry("Modifica del file script.")
   public static final String EDIT_SCRIPT = "22";

   @RBEntry("Modifica del file ptc_help.txt.")
   public static final String EDIT_HELP = "23";

   @RBEntry("Esecuzione del programma di configurazione.")
   public static final String EXECUTING_SETUP = "24";

   @RBEntry("Copia dei file del plug-in nella directory dei plug-in.")
   public static final String COPY_PLUGIN = "25";

   @RBEntry("Specificare una posizione per i plug-in.")
   public static final String PLUGIN_LOC_NEEDED = "26";

   @RBEntry("Specificare una posizione in cui memorizzare i file di applicazione di PTC Viewer.")
   public static final String PTCVIEWER_LOC_NEEDED = "27";

   @RBEntry("Specificare il percorso assoluto per l'eseguibile del browser Web.")
   public static final String BROWSER_EXEC_NEEDED = "28";

   @RBEntry("Posizione dei file di applicazione di PTC Viewer.")
   public static final String PTCVIEWER_BROWSE_DIALOG_TITLE = "29";

   @RBEntry("Installazione non riuscita. Impossibile scrivere su  {0}")
   public static final String INSTALL_FAILED = "3";

   @RBEntry("Eseguibile del browser Web.")
   public static final String EXEC_BROWSE_DIALOG_TITLE = "30";

   @RBEntry("Posizione di Windchill JRE.")
   public static final String IGW_BROWSE_DIALOG_TITLE = "31";

   @RBEntry("La posizione di JRE è determinata dalla directory nella quale vengono installati i file JRE sul sistema client.")
   public static final String INTRALINKGW_HELP = "32";

   @RBEntry("Avvio dell'applicazione non riuscito. Vedere la console Java per i dettagli.")
   public static final String APP_LAUNCH_FAILED = "33";

   @RBEntry("Installare il caricatore per bootstrap sul client prima di installare questa caratteristica. Verrà riaperta la pagina dell'installazione del bootstrap. Una volta installato il caricatore per bootstrap, riavviare l'installazione.")
   public static final String BOOT_NEEDED = "34";

   @RBEntry("La posizione JRE è quella in cui i JRE devono essere installati sul sistema client. La directory TEMP è il valore della variabile di ambiente TEMP, che è la directory temporanea.")
   public static final String DESKTOP_HELP = "35";

   @RBEntry("Impossibile accedere alle variabili di ambiente. L'installazione è fallita.")
   public static final String NO_ENVIRONMENT = "36";

   @RBEntry("Specificare la posizione JRE")
   public static final String MUST_SPECIFY_JRE = "37";

   @RBEntry("Specificare la directory TEMP")
   public static final String MUST_SPECIFY_TEMP = "38";

   @RBEntry("Installazione di {0} in corso.")
   public static final String INSTALL_FILE = "39";

   @RBEntry("Installazione non riuscita. Vedere la console di Java per i dettagli.")
   public static final String INSTALL_FAILED_TRACE = "4";

   @RBEntry("Impossibile scrivere su {0}")
   public static final String NO_WRITE = "40";

   @RBEntry("Posizione Gestione workgroup")
   public static final String WM_BROWSE_DIALOG_TITLE = "41";

   @RBEntry("Directory TEMP")
   public static final String TEMP_BROWSE_DIALOG_TITLE = "42";

   @RBEntry("Specificare la posizione della Gestione workgroup")
   public static final String MUST_SPECIFY_WMDIR = "43";

   @RBEntry("La posizione JRE è quella in cui i JRE devono essere installati sul sistema client. La posizione Gestione workgroup è quella in cui saranno scritti i file e le librerie usati dalle Gestioni workgroup.")
   public static final String CADDS_HELP = "44";

   @RBEntry("La posizione JRE è quella in cui i JRE devono essere installati sul sistema client. La posizione Gestione workgroup è quella in cui saranno scritti i file e le librerie usati dalle Gestioni workgroup.")
   public static final String PROE_HELP = "45";

   @RBEntry("Controllo JRE in corso...")
   public static final String CHECK_JRE = "46";

   @RBEntry("La procedura per avviare questa applicazione, {0}, è stata scritta su {1}.")
   public static final String LAUNCH_SCRIPT = "47";

   @RBEntry("La posizione JRE è quella in cui i JRE devono essere installati sul sistema client. La posizione Gestione workgroup è quella in cui saranno scritti i file e le librerie usati dalle Gestioni workgroup.")
   public static final String AUTOCAD_HELP = "48";

   @RBEntry("La posizione JRE è quella in cui i JRE devono essere installati sul sistema client. La posizione Gestione workgroup è quella in cui saranno scritti i file e le librerie usati dalle Gestioni workgroup.")
   public static final String HELIX_HELP = "49";

   @RBEntry("Posizione plug-in")
   public static final String BROWSE_DIALOG_TITLE = "5";

   @RBEntry("Aggiornamento registro di configurazione in corso...")
   public static final String SETTING_REGISTRY = "50";

   @RBEntry("Posizione JRE specificata non trovata")
   public static final String JRE_NOT_FOUND = "51";

   @RBEntry("Benvenuti nell'installazione guidata di Windchill Workgroup Manager for {0}")
   public static final String WELCOME_MESSAGE = "52";

   @RBEntry("Questa procedura vi guiderà nell'installazione di Windchill Workgroup Manager for {0}.")
   public static final String WIZARD_DESC = "53";

   @RBEntry("Java Runtime Environment (JRE) è necessario per Workgroup Manager for {0} e sarà installato sul vostro computer. Scegliere la directory su cui installare JRE.")
   public static final String JRE_INSTRUCTION = "54";

   @RBEntry("I file di programma usati da Workgroup Manager for {0} devono essere copiati sul vostro computer. Scegliere la directory dove si intende installare i file di programma.")
   public static final String WM_INSTRUCTION = "55";

   @RBEntry("Per poter comunicare con Pro/ENGINEER, il Workgroup Manager deve conoscere il suo punto di installazione. Scegliere la directory su cui Pro/ENGINEER è correntemente installato.")
   public static final String PROE_INSTRUCTION = "56";

   @RBEntry("Workgroup Manager for Pro/ENGINEER deve conoscere il comando d'avvio di Pro/ENGINEER per poterlo avviare. Specificare il comando d'avvio di Pro/ENGINEER.")
   public static final String PROESTARTCOMMAND_INSTRUCTION = "57";

   @RBEntry("Completamento della procedura guidata per l'installazione di Windchill Workgroup Manager for {0} in corso.")
   public static final String COMPLETE_INSTALL = "58";

   @RBEntry("L'installazione di Windchill Workgroup Manager for {0} è stata completata. Per avviare Workgroup Manager, eseguire \"{1}\" nella directory \"{2}\".")
   public static final String INSTALL_SUCCESSFUL = "59";

   @RBEntry("Selezionare la directory per i plug-in. Il programma di installazione memorizza il plug-in del caricatore per bootstrap in questa posizione..")
   public static final String BOOT_HELP = "6";

   @RBEntry("- Directory di installazione JRE:")
   public static final String JRE_INSTALL_DIRECTORY = "60";

   @RBEntry("- Directory di installazione di Workgroup Manager for {0}")
   public static final String WM_INSTALL_DIRECTORY = "61";

   @RBEntry("- Directory di Pro/ENGINEER:")
   public static final String PROE_INSTALL_DIRECTORY = "62";

   @RBEntry("- Comando di avvio di Pro/ENGINEER:")
   public static final String PROE_START_CMD_NAME = "63";

   @RBEntry("Java Runtime Environment (JRE) è necessario per Workgroup Manager for {0}. Scegliere la directory su cui JRE è installato.")
   public static final String JRE_INSTRUCTION_AIX = "64";

   @RBEntry("Scaricamento e decompattamento di {0} in corso. Attendere...")
   public static final String DOWNLOADING_AND_UNPACKING_JAR = "65";

   @RBEntry("Scaricamento di  {0} in corso...")
   public static final String DOWNLOADING_FILE = "66";

   @RBEntry("Creazione script in corso...")
   public static final String CREATING_SCRIPT = "67";

   @RBEntry("Uscire?")
   public static final String CONFIRM_EXIT = "68";

   @RBEntry("Uscire?")
   public static final String TITLE_EXIT = "69";

   @RBEntry("Impostazione dei permessi in corso...")
   public static final String SETTING_PERMISSION = "70";

   @RBEntry("Selezionare la directory per i plug-in. Il programma di installazione memorizza il plug-in del caricatore per bootstrap in questa posizione. In Netscape, generalmente la posizione dei plug-in è C:\\Programmi\\Netscape\\Communicator\\Program\\Plugins\\ Nota: se si utilizza il pulsante Sfoglia, è possibile ignorare il valore immesso per il nome file. Questo è semplicemente un segnaposto, infatti è importante specificare solo la directory.")
   public static final String PLUGIN_HELP_NETSCAPE_NT = "7";

   @RBEntry("Specificare la posizione cache")
   public static final String MUST_SPECIFY_CACHEDIR = "71";

   @RBEntry("Una directory cache è necessaria per Workgroup Manager for {0}. Specificare una directory cache.")
   public static final String CACHE_INSTRUCTION = "72";

   @RBEntry("- Directory della cache:")
   public static final String CACHE_DIRECTORY = "73";

   @RBEntry("L'installazione di Windchill Workgroup Manager for {0} è stata effettuata.")
   public static final String SETUP_SUCCESSFUL = "74";

   @RBEntry("Estrazione di {0} in corso...")
   public static final String EXTRACTING_ARCHIVE = "75";

   @RBEntry("Copia e registrazione dei file in corso...")
   public static final String COPYING_AND_REGISTERING = "76";

   @RBEntry("Pulizia file temporanei in corso...")
   public static final String CLEANING_TEMP_FILES = "77";

   @RBEntry("Per completare l'installazione, è necessario disporre di \"Cadence PCB Systems 14.0\" o successivo sul sistema. La finestra di installazione viene chiusa.")
   public static final String CADENCE_REQUIRED = "78";

   @RBEntry("L'Installazione generale non è ancora disponibile. Per ulteriori informazioni, contattare l'amministratore di sistema CATIA e continuare con l'Installazione personale.")
   public static final String GLOBAL_SETUP_NOT_AVAILABLE = "79";

   @RBEntry("Selezionare la directory per i plug-in. Il programma di installazione memorizza il plug-in del caricatore per bootstrap in questa posizione.  In Netscape la posizione dei plug-in è /usr/local/lib/netscape/plugins/ oppure $$HOME/.netscape/plugins/.  Utilizzando il pulsante Sfoglia, è possibile ignorare il valore immesso per il nome file.  Questo è semplicemente un segnaposto,  infatti è importante specificare solo la directory.")
   public static final String PLUGIN_HELP_NETSCAPE_UNIX = "8";

   @RBEntry("L'Installazione generale è disponibile e supporta solo l'ambiente utente Dassault CATIA di default. Eseguire il comando d'avvio di WGM CATIA \"wmcat\" per avviare il Workgroup Manager. La cartella \"cfg\" verrà creata in $HOME/db (se non esiste già).")
   public static final String GLOBAL_SETUP_AVAILABLE = "80";

   @RBEntry("L'installazione è fallita. La variabile ambientale CAT_CUST non è stata impostata. Notificare l'amministratore CATIA.")
   public static final String CATCUST_NOT_SET = "81";

   @RBEntry("L'Installazione generale è stata completata. Eseguire \"{0}\" per avviare Workgroup Manager for CATIA. Il comando d'avvio generale per tutti gli utenti WM CATIA è \"wmcat\" ed è collocato nella directory \"{1}\".")
   public static final String ADMIN_GLOBAL_SETUP_COMPLETE = "82";

   @RBEntry("Necessaria modifica alla configurazione")
   @RBComment("Title for dialog warning user to make a configuration change.")
   public static final String BOOTSTRAP_CONFIG_TITLE = "83";

   @RBEntry("Il bootstrap Windchill non è installato nella posizione di default.\nAggiungere l'istruzione seguente ai parametri di runtime Java dal pannello di controllo del plug-in Java:\n-Xbootclasspath/p:")
   @RBComment("Instructions for how to manually change the configuration.")
   public static final String BOOTSTRAP_CONFIG_CHANGE_MSG = "84";

   @RBEntry("Selezionare la directory per i plug-in. Il programma di installazione memorizza il plug-in del caricatore per bootstrap in questa posizione. In Microsoft Internet Explorer la posizione dei plug-in è C:\\Programmi\\Plus!\\Microsoft Internet\\plugins\\. Nota: se si utilizza il pulsante Sfoglia, è possibile ignorare il valore immesso per il nome file. Questo è semplicemente un segnaposto, infatti è importante specificare solo la directory.")
   public static final String PLUGIN_HELP_IE_NT = "9";

   @RBEntry("< Indietro")
   public static final String PRIVATE_CONSTANT_0 = "BBack";

   @RBEntry("Sfoglia...")
   public static final String PRIVATE_CONSTANT_1 = "BBrowse";

   @RBEntry("Annulla")
   public static final String PRIVATE_CONSTANT_2 = "BCancel";

   @RBEntry("Fine")
   public static final String PRIVATE_CONSTANT_3 = "BFinish";

   @RBEntry("Guida")
   public static final String PRIVATE_CONSTANT_4 = "BHelp";

   @RBEntry("Avanti >")
   public static final String PRIVATE_CONSTANT_5 = "BNext";

   @RBEntry("OK")
   public static final String PRIVATE_CONSTANT_6 = "BOK";

   @RBEntry("Installazione personale")
   public static final String PRIVATE_CONSTANT_7 = "BPSButton";

   @RBEntry("Installazione generale")
   public static final String PRIVATE_CONSTANT_8 = "BGSButton";

   @RBEntry("Installazione di Windchill Workgroup Manager for CADDS")
   public static final String PRIVATE_CONSTANT_9 = "LCADDS";

   @RBEntry("Installazione Windchill Workgroup Manager for Cadence")
   public static final String PRIVATE_CONSTANT_10 = "LCadence";

   @RBEntry("Installazione di Windchill Workgroup Manager for CATIA")
   public static final String PRIVATE_CONSTANT_11 = "LCATIA";

   @RBEntry("Installazione di Windchill Workgroup Manager for Pro/ENGINEER")
   public static final String PRIVATE_CONSTANT_12 = "LProE";

   @RBEntry("Directory TEMP")
   public static final String PRIVATE_CONSTANT_13 = "LTempDirectory";

   @RBEntry("Installazione del bootstrap di Windchill.")
   public static final String PRIVATE_CONSTANT_14 = "Lboot";

   @RBEntry("Installazione del plug-in del caricatore per bootstrap in corso. Selezionare la posizione per i plug-in e premere OK per iniziare l'installazione.")
   public static final String PRIVATE_CONSTANT_15 = "LbootInstructions";

   @RBEntry("Eseguibile del browser Web:")
   public static final String PRIVATE_CONSTANT_16 = "LbrowseExecLocation";

   @RBEntry("Imposta la directory cache")
   public static final String PRIVATE_CONSTANT_17 = "LcacheDir";

   @RBEntry("Directory cache:")
   public static final String PRIVATE_CONSTANT_18 = "LcacheLocation";

   @RBEntry("Scegliere un opzione di installazione")
   public static final String PRIVATE_CONSTANT_19 = "LchooseOption";

   @RBEntry("Fare clic su Avanti per continuare.")
   public static final String PRIVATE_CONSTANT_20 = "Lcontinue";

   @RBEntry("Fare clic su Termina per chiudere la pagina di procedura guidata.")
   public static final String PRIVATE_CONSTANT_21 = "Lfinish";

   @RBEntry("Installazione del gateway Pro/INTRALINK.")
   public static final String PRIVATE_CONSTANT_22 = "Ligw";

   @RBEntry("Installazione del client del gateway Pro/INTRALINK.")
   public static final String PRIVATE_CONSTANT_23 = "LigwInstructions";

   @RBEntry("Indicare la directory in cui installare JRE:")
   public static final String PRIVATE_CONSTANT_24 = "LjavaLocation";

   @RBEntry("Immettere la directory in cui JRE è installato:")
   public static final String PRIVATE_CONSTANT_25 = "LjavaLocation_AIX";

   @RBEntry("Impostare la directory di installazione JRE")
   public static final String PRIVATE_CONSTANT_26 = "LJREDir";

   @RBEntry("Scegliere una delle opzioni seguenti")
   public static final String PRIVATE_CONSTANT_27 = "LoptionInstruction";

   @RBEntry("Posizione dei plug-in:")
   public static final String PRIVATE_CONSTANT_28 = "LpluginLocation";

   @RBEntry("Impostare la directory Pro/ENGINEER")
   public static final String PRIVATE_CONSTANT_29 = "LProEDir";

   @RBEntry("Immettere la directory in cui Pro/Engineer è installato:")
   public static final String PRIVATE_CONSTANT_30 = "LProELocation";

   @RBEntry("Specificare il comando di avvio di Pro/ENGINEER")
   public static final String PRIVATE_CONSTANT_31 = "LProEStartCommand";

   @RBEntry("Immettere il comando d'avvio di Pro/ENGINEER:")
   public static final String PRIVATE_CONSTANT_32 = "LProEStartCommandName";

   @RBEntry("Installazione di PTC Viewer.")
   public static final String PRIVATE_CONSTANT_33 = "Lprofly";

   @RBEntry("Installazione del plug-in di PTC Viewer.  Effettuare le selezioni e premere OK per iniziare l'installazione.")
   public static final String PRIVATE_CONSTANT_34 = "LproflyInstructions";

   @RBEntry("Posizione di PTC Viewer:")
   public static final String PRIVATE_CONSTANT_35 = "LproflyLocation";

   @RBEntry("Verificare le impostazioni di installazione")
   public static final String PRIVATE_CONSTANT_36 = "LSummary";

   @RBEntry("Sono stati forniti i seguenti parametri di installazione:")
   public static final String PRIVATE_CONSTANT_37 = "LSummarySettings";

   @RBEntry("Impostare la directory  Workgroup Manager")
   public static final String PRIVATE_CONSTANT_38 = "LWMDir";

   @RBEntry("Indicare la directory in cui copiare i file:")
   public static final String PRIVATE_CONSTANT_39 = "LWMLocation";

   @RBEntry("Il caricatore per bootstrap utilizza la directory cache per effettuare la cache locale dei file jar.")
   public static final String PRIVATE_CONSTANT_40 = "TcacheDesc";

   @RBEntry("Per modificare i valori sopra indicati, fare clic sul pulsante Indietro per tornare alle finestre in cui sono stati impostati i valori. Altrimenti fare clic su Avanti per cominciare l'installazione.")
   public static final String PRIVATE_CONSTANT_41 = "TchangeInstruction";

   @RBEntry("Copia dei file necessari in corso.")
   public static final String PRIVATE_CONSTANT_42 = "TcopyFiles";

   @RBEntry("Messaggio di installazione")
   public static final String PRIVATE_CONSTANT_43 = "TinfoDialog";

   @RBEntry("Java Runtime Environment (JRE) contiene il supporto necessario per l'esecuzione delle applicazioni Java, Windchill Workgroup Manager compreso.")
   public static final String PRIVATE_CONSTANT_44 = "TJREDesc";

   @RBEntry("L'installazione sarà personale o generale a seconda dell'opzione scelta.")
   public static final String PRIVATE_CONSTANT_45 = "ToptionDesc";

   @RBEntry("Workgroup Manager for Pro/ENGINEER opera con l'installazione attuale di Pro/ENGINEER.")
   public static final String PRIVATE_CONSTANT_46 = "TProEDesc";

   @RBEntry("Il comando d'avvio per Pro/ENGINEER è il comando usato per avviare Pro/ENGINEER.")
   public static final String PRIVATE_CONSTANT_47 = "TProEStartCommandDesc";

   @RBEntry("È possibile verificare i valori impostati per le opzioni di installazione prima del completamento dell'installazione.")
   public static final String PRIVATE_CONSTANT_48 = "TSummaryDesc";

   @RBEntry("Durante l'installazione, i file programma di Workgroup Manager sono copiati sul vostro computer.")
   public static final String PRIVATE_CONSTANT_49 = "TWMDesc";

   @RBEntry("Benvenuti nell'installazione guidata del bootstrap Windchill")
   public static final String PRIVATE_CONSTANT_50 = "TBootWelcome";

   @RBEntry("Questa procedura vi guiderà nell'installazione del bootstrap Windchill.")
   public static final String PRIVATE_CONSTANT_51 = "TBootInstallDesc";

   @RBEntry("Definire la posizione dell'installazione del bootstrap")
   public static final String PRIVATE_CONSTANT_52 = "LBootDir";

   @RBEntry("Specificare la posizione per lo scaricamento del bootstrap")
   public static final String PRIVATE_CONSTANT_53 = "TBootMustBeSpecified";

   @RBEntry("Il bootstrap Windchill è un file Java Archive (JAR) contenente il supporto necessario per effettuare il bootstrap dei client Windchill.")
   public static final String PRIVATE_CONSTANT_54 = "TBootDesc";

   @RBEntry("Selezionare la directory in cui si intende installare il file JAR Bootstrap. La posizione standard è nella directory  Java Standard Extensions. Se non è possibile scrivere in questa posizione, il file JAR può essere scaricato in qualsiasi altra posizione ma possono essere necessari ulteriori passi di configurazione.")
   public static final String PRIVATE_CONSTANT_55 = "TBootInstr";

   @RBEntry("Salva JAR Bootstrap con nome:")
   public static final String PRIVATE_CONSTANT_56 = "LBootLocation";

   @RBEntry("L'installazione del bootstrap Windchill è completata.")
   public static final String PRIVATE_CONSTANT_57 = "TBootComplete";

   @RBEntry("L'installazione del bootstrap Windchill è stata completata.")
   public static final String PRIVATE_CONSTANT_58 = "TBootSuccess";

   @RBEntry("- Salva JAR bootstrap con nome:")
   public static final String PRIVATE_CONSTANT_59 = "LSaveBootstrapSummary";

   @RBEntry("Salva bootstrap con nome...")
   public static final String PRIVATE_CONSTANT_60 = "TSaveBootstrapAs";
}
