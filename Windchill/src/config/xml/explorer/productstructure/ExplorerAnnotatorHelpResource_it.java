package config.xml.explorer.productstructure;

import wt.util.resource.*;

public final class ExplorerAnnotatorHelpResource_it extends WTListResourceBundle {
   /**
    * Help Entry Contents
    **/
   @RBEntry("test/SAChildPartAdd.html")
   public static final String PRIVATE_CONSTANT_0 = "ADD_CHILD_ASSOCIATION_HELP";

   @RBEntry("test/SAPartCutCopyPaste.html")
   public static final String PRIVATE_CONSTANT_1 = "ADD_CHILD_ASSOCIATION_FOR_CUT_COPY_PASTE_HELP";

   @RBEntry("test/SADragDrop.html")
   public static final String PRIVATE_CONSTANT_2 = "ADD_CHILD_ASSOCIATION_FOR_DRAG_DROP_HELP";

   @RBEntry("test/SAChildSearch.html")
   public static final String PRIVATE_CONSTANT_3 = "ADD_CHILD_SEARCH_HELP";

   @RBEntry("test/SAAnnotationProperties.html")
   public static final String PRIVATE_CONSTANT_4 = "ANNOTATION_PROPERTIES_HELP";

   @RBEntry("test/SAContainerSearch.html")
   public static final String PRIVATE_CONSTANT_5 = "CONTAINER_SEARCH_HELP";

   @RBEntry("test/SAOview.html")
   public static final String PRIVATE_CONSTANT_6 = "HELP_TOPICS_HELP";

   @RBEntry("test/SAAnnotationSetCreate.html")
   public static final String PRIVATE_CONSTANT_7 = "LAUNCH_ANNOTATOR_HELP";

   @RBEntry("test/SALifeCycleSearch.html")
   public static final String PRIVATE_CONSTANT_8 = "LIFECYCLE_SEARCH_HELP";

   @RBEntry("test/SAChildPartCreate.html")
   public static final String PRIVATE_CONSTANT_9 = "NEW_CHILD_OBJECT_HELP";

   @RBEntry("test/SAPartReplace.html")
   public static final String PRIVATE_CONSTANT_10 = "NEW_CHILD_OBJECT_FOR_REPLACE_HELP";

   @RBEntry("test/SAOrganizationSearch.html")
   public static final String PRIVATE_CONSTANT_11 = "ORGANIZATION_SEARCH_HELP";

   @RBEntry("test/SAPartQuantityChange.html")
   public static final String PRIVATE_CONSTANT_12 = "QUANTITY_CHANGE_HELP";

   @RBEntry("test/SAPartRemove.html")
   public static final String PRIVATE_CONSTANT_13 = "REMOVE_COMMENTS_HELP";

   @RBEntry("test/SAPartCutCopyPaste.html")
   public static final String PRIVATE_CONSTANT_14 = "REMOVE_COMMENTS_FOR_CUT_COPY_PASTE_HELP";

   @RBEntry("test/SADragDrop.html")
   public static final String PRIVATE_CONSTANT_15 = "REMOVE_COMMENTS_FOR_DRAG_DROP_HELP";

   @RBEntry("test/SAPartReplace.html")
   public static final String PRIVATE_CONSTANT_16 = "REMOVE_COMMENTS_FOR_REPLACE_HELP";

   @RBEntry("test/SAPartRevise.html")
   public static final String PRIVATE_CONSTANT_17 = "REVISE_HELP";

   @RBEntry("test/SATeamTemplateSearch.html")
   public static final String PRIVATE_CONSTANT_18 = "TEAM_SEARCH_HELP";

   @RBEntry("test/SAAnnotationComments.html")
   public static final String PRIVATE_CONSTANT_19 = "UPDATE_COMMENTS_HELP";

   @RBEntry("test/SAOccurrenceDelete.html")
   public static final String PRIVATE_CONSTANT_20 = "USAGE_CHANGE_DELETE_OCCURRENCE_COMMENTS_HELP";

   @RBEntry("test/SAPartUsageUpdate.html")
   public static final String PRIVATE_CONSTANT_21 = "USAGE_CHANGE_HELP";

   @RBEntry("test/SAPartQuantityChange.html")
   public static final String PRIVATE_CONSTANT_22 = "USAGE_CHANGE_QUANTITY_HELP";

   @RBEntry("test/SARefDesignatorRemove.html")
   public static final String PRIVATE_CONSTANT_23 = "USAGE_CHANGE_REMOVE_DESIGNATOR_COMMENTS_HELP";

   @RBEntry("test/SAAnnotationComments.html")
   public static final String PRIVATE_CONSTANT_24 = "USAGE_CHANGE_UPDATE_COMMENTS_HELP";

   @RBEntry("test/SARefDesignatorUpdate.html")
   public static final String PRIVATE_CONSTANT_25 = "USAGE_CHANGE_UPDATE_DESIGNATOR_HELP";
}
