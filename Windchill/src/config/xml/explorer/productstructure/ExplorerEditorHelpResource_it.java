package config.xml.explorer.productstructure;

import wt.util.resource.*;

public final class ExplorerEditorHelpResource_it extends WTListResourceBundle {
   /**
    * Help Entry Contents
    **/
   @RBEntry("test/SEChildPartAdd.html")
   public static final String PRIVATE_CONSTANT_0 = "ADD_CHILD_ASSOCIATION_HELP";

   @RBEntry("test/SEPartCutCopyPaste.html")
   public static final String PRIVATE_CONSTANT_1 = "ADD_CHILD_ASSOCIATION_FOR_CUT_COPY_PASTE_HELP";

   @RBEntry("test/SEDragDrop.html")
   public static final String PRIVATE_CONSTANT_2 = "ADD_CHILD_ASSOCIATION_FOR_DRAG_DROP_HELP";

   @RBEntry("test/SEChildSearch.html")
   public static final String PRIVATE_CONSTANT_3 = "ADD_CHILD_SEARCH_HELP";

   @RBEntry("test/SEAssociationAddComments.html")
   public static final String PRIVATE_CONSTANT_4 = "ASSOCIATION_ADD_COMMENTS_HELP";

   @RBEntry("test/SEAssociationAddSearch.html")
   public static final String PRIVATE_CONSTANT_5 = "ASSOCIATION_ADD_SEARCH_HELP";

   @RBEntry("test/SEAssociationRemoveComments.html")
   public static final String PRIVATE_CONSTANT_6 = "ASSOCIATION_REMOVE_COMMENTS_HELP";

   @RBEntry("test/SEAssociationRemoveOccurrenceComments.html")
   public static final String PRIVATE_CONSTANT_7 = "ASSOCIATION_REMOVE_OCCURRENCE_COMMENTS_HELP";

   @RBEntry("test/SEAssociationUpdateComments.html")
   public static final String PRIVATE_CONSTANT_8 = "ASSOCIATION_UPDATE_COMMENTS_HELP";

   @RBEntry("test/SEAssociationUpdate.html")
   public static final String PRIVATE_CONSTANT_9 = "ASSOCIATION_UPDATE_HELP";

   @RBEntry("test/SEContainerSearch.html")
   public static final String PRIVATE_CONSTANT_10 = "CONTAINER_SEARCH_HELP";

   @RBEntry("test/SEDeleteAnnotationSearch.html")
   public static final String PRIVATE_CONSTANT_11 = "DELETE_ANNOTATION_SEARCH_HELP";

   @RBEntry("test/SEAnnotationSetCreate.html")
   public static final String PRIVATE_CONSTANT_12 = "LAUNCH_ANNOTATOR_HELP";

   @RBEntry("test/SELifeCycleSearch.html")
   public static final String PRIVATE_CONSTANT_13 = "LIFECYCLE_SEARCH_HELP";

   @RBEntry("test/SELoadSearchHe.html")
   public static final String PRIVATE_CONSTANT_14 = "LOAD_SEARCH_HELP";

   @RBEntry("test/SEChildPartCreate.html")
   public static final String PRIVATE_CONSTANT_15 = "NEW_CHILD_OBJECT_HELP";

   @RBEntry("test/SEPartReplace.html")
   public static final String PRIVATE_CONSTANT_16 = "NEW_CHILD_OBJECT_FOR_REPLACE_HELP";

   @RBEntry("ctest/SENewRootObject.html")
   public static final String PRIVATE_CONSTANT_17 = "NEW_ROOT_OBJECT_HELP";

   @RBEntry("test/SEOpenAnnotationSearch.html")
   public static final String PRIVATE_CONSTANT_18 = "OPEN_ANNOTATION_SEARCH_HELP";

   @RBEntry("test/SEOrganizationSearch.html")
   public static final String PRIVATE_CONSTANT_19 = "ORGANIZATION_SEARCH_HELP";

   @RBEntry("test/SEPartQuantityChange.html")
   public static final String PRIVATE_CONSTANT_20 = "QUANTITY_CHANGE_HELP";

   @RBEntry("test/SEPartRemove.html")
   public static final String PRIVATE_CONSTANT_21 = "REMOVE_COMMENTS_HELP";

   @RBEntry("test/SEPartCutCopyPaste.html")
   public static final String PRIVATE_CONSTANT_22 = "REMOVE_COMMENTS_FOR_CUT_COPY_PASTE_HELP";

   @RBEntry("test/SEDragDrop.html")
   public static final String PRIVATE_CONSTANT_23 = "REMOVE_COMMENTS_FOR_DRAG_DROP_HELP";

   @RBEntry("test/SEPartReplace.html")
   public static final String PRIVATE_CONSTANT_24 = "REMOVE_COMMENTS_FOR_REPLACE_HELP";

   @RBEntry("test/SESetConfigSpec.html")
   public static final String PRIVATE_CONSTANT_25 = "SET_CONFIG_SPEC_HELP";

   @RBEntry("test/SETeamTemplateSearch.html")
   public static final String PRIVATE_CONSTANT_26 = "TEAM_SEARCH_HELP";

   @RBEntry("test/SEAnnotationComments.html")
   public static final String PRIVATE_CONSTANT_27 = "UPDATE_COMMENTS_HELP";

   @RBEntry("test/SEOccurrenceDelete.html")
   public static final String PRIVATE_CONSTANT_28 = "USAGE_CHANGE_DELETE_OCCURRENCE_COMMENTS_HELP";

   @RBEntry("test/SEPartUsageUpdate.html")
   public static final String PRIVATE_CONSTANT_29 = "USAGE_CHANGE_HELP";

   @RBEntry("test/SEPartQuantityChange.html")
   public static final String PRIVATE_CONSTANT_30 = "USAGE_CHANGE_QUANTITY_HELP";

   @RBEntry("test/SERefDesignatorRemove.html")
   public static final String PRIVATE_CONSTANT_31 = "USAGE_CHANGE_REMOVE_DESIGNATOR_COMMENTS_HELP";

   @RBEntry("test/SEAnnotationComments.html")
   public static final String PRIVATE_CONSTANT_32 = "USAGE_CHANGE_UPDATE_COMMENTS_HELP";

   @RBEntry("test/SERefDesignatorUpdate.html")
   public static final String PRIVATE_CONSTANT_33 = "USAGE_CHANGE_UPDATE_DESIGNATOR_HELP";
}
