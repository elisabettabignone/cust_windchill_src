/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package config.logicrepository.xml.explorer.processplanexplorer;

import wt.util.resource.*;

@RBUUID("config.logicrepository.xml.explorer.processplanexplorer.ConfigurationResource")
public final class ConfigurationResource_it extends WTListResourceBundle {
   @RBEntry("Doesn't Matter")
   public static final String PRIVATE_CONSTANT_0 = "ImNotUsedButNeedToBeHereForSomeJavaVMs";

   /**
    * file menu
    **/
   @RBEntry("Nuovo piano di produzione...")
   public static final String PRIVATE_CONSTANT_1 = "newRootLabel";

   @RBEntry("N")
   public static final String PRIVATE_CONSTANT_2 = "newRootMnemonic";

   @RBEntry("Nuovo piano di produzione")
   public static final String PRIVATE_CONSTANT_3 = "newRootToolTip";

   @RBEntry("Apri piano di produzione...")
   public static final String PRIVATE_CONSTANT_4 = "loadRootLabel";

   @RBEntry("O")
   public static final String PRIVATE_CONSTANT_5 = "loadRootMnemonic";

   @RBEntry("Apre il piano di produzione")
   public static final String PRIVATE_CONSTANT_6 = "loadRootToolTip";

   /**
    * view menu
    **/
   @RBEntry("Mostra allocazioni operazione")
   public static final String PRIVATE_CONSTANT_7 = "showOperationAllocationVisibilityLabel";

   @RBEntry("Mostra l'allocazione dell'operazione nella struttura")
   public static final String PRIVATE_CONSTANT_8 = "showOperationAllocationVisibilityToolTip";

   /**
    * selected menu
    **/
   @RBEntry("Elimina")
   public static final String PRIVATE_CONSTANT_9 = "deleteLabel";

   @RBEntry("Elimina")
   public static final String PRIVATE_CONSTANT_10 = "deleteToolTip";

   /**
    * about menu - overrides the main definitions
    **/
   @RBEntry("Informazioni sul Navigatore piani di produzione")
   public static final String PRIVATE_CONSTANT_11 = "aboutLabel";

   @RBEntry("Visualizza la finestra delle informazioni sul Navigatore piani di produzione")
   public static final String PRIVATE_CONSTANT_12 = "aboutToolTip";

   /**
    * Sequences table in Uses tab
    **/
   @RBEntry("Tipo")
   public static final String PRIVATE_CONSTANT_13 = "typeLabel";

   @RBEntry("Sequenze")
   public static final String PRIVATE_CONSTANT_14 = "sequencesLabel";

   /**
    * Menu labels
    **/
   @RBEntry("Duplica e incolla")
   public static final String PRIVATE_CONSTANT_15 = "pasteAndDuplicateLabel";

   @RBEntry("Duplica e incolla")
   public static final String PRIVATE_CONSTANT_16 = "pasteAndDuplicateToolTip";

   @RBEntry("Inserisci operazione")
   public static final String PRIVATE_CONSTANT_17 = "addDefaultOperationSingleLabel";

   @RBEntry("Inserisce un'operazione senza assistenza")
   public static final String PRIVATE_CONSTANT_18 = "addDefaultOperationSingleToolTip";

   @RBEntry("Inserisci più operazioni")
   public static final String PRIVATE_CONSTANT_19 = "addDefaultOperationMultiLabel";

   @RBEntry("Inserisce più operazioni senza assistenza")
   public static final String PRIVATE_CONSTANT_20 = "addDefaultOperationMultiToolTip";

   @RBEntry("Numero operazioni")
   public static final String PRIVATE_CONSTANT_21 = "quantityOfOperationsLabel";

   @RBEntry("Check-Out oggetto?")
   public static final String PRIVATE_CONSTANT_22 = "checkoutObject";

   @RBEntry("Incolla nuova operazione con metodologie produttive copiate...")
   public static final String PRIVATE_CONSTANT_23 = "addMfgProcessToNewOperationLabel";

   @RBEntry("Incolla una nuova operazione con metodologie produttive copiate")
   public static final String PRIVATE_CONSTANT_24 = "addMfgProcessToNewOperationToolTip";

   /**
    * Links Tab labels
    **/
   @RBEntry("Correlata")
   public static final String PRIVATE_CONSTANT_25 = "linkTabLabel";

   @RBEntry("Metodologie produttive:")
   public static final String PRIVATE_CONSTANT_26 = "mfgProcessLabel";

   @RBEntry("Associa metodologia produttiva...")
   public static final String PRIVATE_CONSTANT_27 = "associateMfgProcessLabel";

   @RBEntry("Associa una metodologia produttiva")
   public static final String PRIVATE_CONSTANT_28 = "associateMfgProcessToolTip";

   /**
    * Part Allocations tab labels
    **/
   @RBEntry("Allocazioni parti")
   public static final String PRIVATE_CONSTANT_29 = "partsAllocationTabLabel";

   @RBEntry("Parti allocate da distinta base:")
   public static final String PRIVATE_CONSTANT_30 = "bomAllocatedPartsLabel";

   @RBEntry("Parti oggetto di operazione:")
   public static final String PRIVATE_CONSTANT_31 = "operatedPartsLabel";

   @RBEntry("Alloca parte da distinta base...")
   public static final String PRIVATE_CONSTANT_32 = "allocateBOMLabel";

   @RBEntry("Alloca una parte dalla distinta base")
   public static final String PRIVATE_CONSTANT_33 = "allocateBOMToolTip";

   @RBEntry("Alloca parte...")
   public static final String PRIVATE_CONSTANT_34 = "allocatePartLabel";

   @RBEntry("Alloca una parte")
   public static final String PRIVATE_CONSTANT_35 = "allocatePartToolTip";

   @RBEntry("Sostituisci la parte con una parte di sostituzione")
   public static final String PRIVATE_CONSTANT_36 = "replacePartLabel";

   @RBEntry("Sostituisce la parte con la parte di sostituzione utilizzata nella distinta base")
   public static final String PRIVATE_CONSTANT_37 = "replacePartToolTip";

   @RBEntry("Aggiungi parte oggetto di operazione...")
   public static final String PRIVATE_CONSTANT_38 = "addOperatedPartLabel";

   @RBEntry("Aggiunge una parte oggetto di operazione")
   public static final String PRIVATE_CONSTANT_39 = "addOperatedPartLabelToolTip";

   @RBEntry("Descrizione")
   public static final String PRIVATE_CONSTANT_40 = "descriptionLabel";

   @RBEntry("Nome sequenza alternativa")
   public static final String PRIVATE_CONSTANT_41 = "seqNameLabel";

   @RBEntry("Numero sequenza alternativa")
   public static final String PRIVATE_CONSTANT_42 = "seqNumberLabel";

   @RBEntry("Tipo incongruenza")
   public static final String PRIVATE_CONSTANT_43 = "commentLabel";

   @RBEntry("Nome operazione")
   public static final String PRIVATE_CONSTANT_44 = "operNameLabel";

   @RBEntry("Numero operazione")
   public static final String PRIVATE_CONSTANT_45 = "operNumberLabel";

   @RBEntry("Contesto assieme")
   public static final String PRIVATE_CONSTANT_46 = "assemblyContextLabel";

   @RBEntry("Percorso parte fabbricazione fittizia o phantom")
   public static final String PRIVATE_CONSTANT_47 = "phantomPathLabel";

   @RBEntry("Tipo di allocazione")
   public static final String PRIVATE_CONSTANT_48 = "partLinkTypeLabel";

   @RBEntry("Riepilogo allocazioni parti")
   public static final String PRIVATE_CONSTANT_49 = "partallocationsummaryLabel";

   @RBEntry("Riepilogo allocazioni parti")
   public static final String PRIVATE_CONSTANT_50 = "partallocationsummaryTip";

   @RBEntry("Riepilogo allocazioni operazioni")
   public static final String PRIVATE_CONSTANT_51 = "operationallocationsummaryLabel";

   @RBEntry("Riepilogo allocazioni operazioni")
   public static final String PRIVATE_CONSTANT_52 = "operationallocationsummaryToolTip";

   @RBEntry("Consuma da distinta base a monte come selezionato")
   public static final String PRIVATE_CONSTANT_53 = "consumeFromUpstreamBOMAsSelectedLabel";

   @RBEntry("Consuma da distinta base a monte come selezionato")
   public static final String PRIVATE_CONSTANT_54 = "consumeFromUpstreamBOMAsSelectedToolTip";

   @RBEntry("Consuma da distinta base a monte come nuovo ramo")
   public static final String PRIVATE_CONSTANT_55 = "consumeFromUpstreamBOMAsNewBranchLabel";

   @RBEntry("Consuma da distinta base a monte come nuovo ramo")
   public static final String PRIVATE_CONSTANT_56 = "consumeFromUpstreamBOMAsNewBranchToolTip";

   @RBEntry("Consuma da distinta base a monte come nuova parte")
   public static final String PRIVATE_CONSTANT_57 = "consumeFromUpstreamBOMAsNewPartLabel";

   @RBEntry("Consuma da distinta base a monte come nuova parte")
   public static final String PRIVATE_CONSTANT_58 = "consumeFromUpstreamBOMAsNewPartToolTip";

   @RBEntry("Colonna indicatore allocazione distinta base")
   public static final String PRIVATE_CONSTANT_59 = "bomAllocationToolTip";

   @RBEntry("Stato allocazione sconosciuto")
   public static final String PRIVATE_CONSTANT_60 = "operationAllocationUnknownToolTip";

   @RBEntry("Nessuna distinta base trovata in base alla specifica di configurazione applicata")
   public static final String PRIVATE_CONSTANT_61 = "operationAllocationNotLatestToolTip";

   @RBEntry("Allocata da distinta base")
   public static final String PRIVATE_CONSTANT_62 = "bomAllocationExistsToolTip";

   @RBEntry("Non allocata da distinta base in base alla specifica di configurazione applicata")
   public static final String PRIVATE_CONSTANT_63 = "bomAllocationDoesNotExistsToolTip";

   @RBEntry("La parte è stata sostituita nella distinta base con un'altra parte di sostituzione.")
   public static final String PRIVATE_CONSTANT_64 = "bomAllocationReplacedToolTip";

   @RBEntry("Tipo di allocazione")
   public static final String PRIVATE_CONSTANT_65 = "allocationTypeLabel";

   @RBEntry("Commuta selezione assieme")
   public static final String PRIVATE_CONSTANT_66 = "assemblySelectionActionLabel";

   @RBEntry("Commuta selezione assieme")
   public static final String PRIVATE_CONSTANT_67 = "assemblySelectionActionToolTip";

   @RBEntry("Selezione assieme")
   public static final String PRIVATE_CONSTANT_68 = "assemblySelectionStatus";

   @RBEntry("Selezione assieme")
   public static final String PRIVATE_CONSTANT_69 = "assemblySelectionToolTip";

   @RBEntry("Assieme non selezionabile in base alla specifica di configurazione")
   public static final String PRIVATE_CONSTANT_70 = "assemblySelectionUnknownToolTip";

   @RBEntry("Assieme selezionato")
   public static final String PRIVATE_CONSTANT_71 = "assemblySelectionExistsToolTip";

   @RBEntry("Assieme non selezionato")
   public static final String PRIVATE_CONSTANT_72 = "assemblySelectionDoesNotExistsToolTip";

   @RBEntry("Ripristina default assieme")
   public static final String PRIVATE_CONSTANT_73 = "revertAssemblyDefaultsLabel";

   @RBEntry("Ripristina le selezioni di default dell'assieme")
   public static final String PRIVATE_CONSTANT_74 = "revertAssemblyDefaultsToolTip";

   @RBEntry("Assiemi attivi")
   public static final String PRIVATE_CONSTANT_75 = "activeAssembliesLabel";

   @RBEntry("Assiemi attivi")
   public static final String PRIVATE_CONSTANT_76 = "activeAssembliesToolTip";

   /**
    * Resource Allocations tab labels
    **/
   @RBEntry("Allocazioni risorsa")
   public static final String PRIVATE_CONSTANT_77 = "resourceTabLabel";

   @RBEntry("Allocazioni centro di lavorazione")
   public static final String PRIVATE_CONSTANT_78 = "workCenterLabel";

   @RBEntry("Allocazioni risorsa di elaborazione")
   public static final String PRIVATE_CONSTANT_79 = "cosumableResourceLabel";

   @RBEntry("Alloca risorsa di elaborazione...")
   public static final String PRIVATE_CONSTANT_80 = "addConsumableLabel";

   @RBEntry("Alloca una risorsa di elaborazione")
   public static final String PRIVATE_CONSTANT_81 = "addConsumableToolTip";

   @RBEntry("Modifica tassi di utilizzo")
   public static final String PRIVATE_CONSTANT_82 = "modifyStepUsageRateForOpToConsumableLabel";

   @RBEntry("Modifica tassi di utilizzo")
   public static final String PRIVATE_CONSTANT_83 = "modifyStepUsageRateForOpToConsumableToolTip";

   @RBEntry("Rimuovi")
   public static final String PRIVATE_CONSTANT_84 = "removeConsumableLabel";

   @RBEntry("Rimuovi")
   public static final String PRIVATE_CONSTANT_85 = "removeConsumableToolTip";

   @RBEntry("Alloca centro di lavorazione...")
   public static final String PRIVATE_CONSTANT_86 = "addWorkCenterLabel";

   @RBEntry("Alloca un centro di lavorazione")
   public static final String PRIVATE_CONSTANT_87 = "addWorkCenterToolTip";

   @RBEntry("Rimuovi")
   public static final String PRIVATE_CONSTANT_88 = "removeWorkCenterLabel";

   @RBEntry("Rimuovi")
   public static final String PRIVATE_CONSTANT_89 = "removeWorkCenterToolTip";

   @RBEntry("Tasso di utilizzo globale")
   public static final String PRIVATE_CONSTANT_90 = "usageRateLabel";

   @RBEntry("Categoria")
   public static final String PRIVATE_CONSTANT_91 = "categoryLabel";

   @RBEntry("Accettabile/Preferenziale")
   public static final String PRIVATE_CONSTANT_92 = "preferredLabel";

   @RBEntry("Prodotto finale")
   public static final String PRIVATE_CONSTANT_93 = "endItemLabel";

   @RBEntry("Tipo generico")
   public static final String PRIVATE_CONSTANT_94 = "genericTypeLabel";

   @RBEntry("Codice traccia di default")
   public static final String PRIVATE_CONSTANT_95 = "defaultTraceCodeLabel";

   @RBEntry("Contesto")
   public static final String PRIVATE_CONSTANT_96 = "containerNameLabel";

   @RBEntry("Unità")
   public static final String PRIVATE_CONSTANT_97 = "unitLabel";

   @RBEntry("Quantità")
   public static final String PRIVATE_CONSTANT_98 = "qtyLabel";

   @RBEntry("Reparto")
   public static final String PRIVATE_CONSTANT_99 = "departmentLabel";

   @RBEntry("Numero di allocazione")
   public static final String PRIVATE_CONSTANT_100 = "allocationNumberLabel";

   @RBEntry("Programmata")
   public static final String PRIVATE_CONSTANT_101 = "scheduledLabel";

   @RBEntry("Stabilimento")
   public static final String PRIVATE_CONSTANT_102 = "plantLabel";

   /**
    * Work Instruction labels
    **/
   @RBEntry("Istruzioni di lavorazione")
   public static final String PRIVATE_CONSTANT_103 = "workInstructionsToolTip";

   @RBEntry("Istruzioni di lavorazione")
   public static final String PRIVATE_CONSTANT_104 = "workInstructionsLabel";

   /**
    * StepUsageRate Labels
    **/
   @RBEntry("Tasso di utilizzo impostazione")
   public static final String PRIVATE_CONSTANT_105 = "setupUsageRateLabel";

   @RBEntry("Tasso di utilizzo elaborazione")
   public static final String PRIVATE_CONSTANT_106 = "processingUsageRateLabel";

   @RBEntry("Tasso di utilizzo smantellamento")
   public static final String PRIVATE_CONSTANT_107 = "teardownUsageRateLabel";

   @RBEntry("Tasso di utilizzo coda")
   public static final String PRIVATE_CONSTANT_108 = "queueUsageRateLabel";

   @RBEntry("Tasso di utilizzo spostamento")
   public static final String PRIVATE_CONSTANT_109 = "moveUsageRateLabel";

   @RBEntry("Tasso di utilizzo attesa")
   public static final String PRIVATE_CONSTANT_110 = "waitingUsageRateLabel";

   @RBEntry("Altro tasso di utilizzo")
   public static final String PRIVATE_CONSTANT_111 = "otherUsageRateLabel";

   /**
    * Distribution Targets
    **/
   @RBEntry("Assegna distribuzione...")
   public static final String PRIVATE_CONSTANT_112 = "assignDistributionLabel";

   @RBEntry("A")
   public static final String PRIVATE_CONSTANT_113 = "assignDistributionMnemonic";

   @RBEntry("Assegna distribuzione")
   public static final String PRIVATE_CONSTANT_114 = "assignDistributionToolTip";

   /**
    * Time and Cost Sub-tab Labels
    **/
   @RBEntry("Tempistiche e costi ")
   public static final String PRIVATE_CONSTANT_115 = "timeCostTabLabel";

   @RBEntry("Indicatore allocazione distinta base")
   public static final String PRIVATE_CONSTANT_116 = "allocBomPartAllocationStatusLabel";

   @RBEntry("Commento")
   public static final String PRIVATE_CONSTANT_117 = "ppeCommentLabel";

   /**
    * view menu
    **/
   @RBEntry("Nascondi allocazioni operazione")
   public static final String PRIVATE_CONSTANT_118 = "hideOperationAllocationVisibilityLabel";

   @RBEntry("Nasconde l'allocazione dell'operazione nella struttura")
   public static final String PRIVATE_CONSTANT_119 = "hideOperationAllocationVisibilityToolTip";
   
   @RBEntry("Nome elemento modello")
   public static final String PRIVATE_CONSTANT_120 = "modelItemInformationColumnLabel";

   @RBEntry("Rappresentazione grafica elemento modello")
   public static final String PRIVATE_CONSTANT_121 = "modelItemGraphicalRepresentationLabel";
   
   @RBEntry("Tipo elemento modello")
   public static final String PRIVATE_CONSTANT_122 = "modelItemSubTypeLabel";
}
