/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package config.logicrepository.xml.explorer.resource;

import wt.util.resource.*;

@RBUUID("config.logicrepository.xml.explorer.resource.ConfigurationResource")
public final class ConfigurationResource_it extends WTListResourceBundle {
   @RBEntry("Doesn't Matter")
   public static final String PRIVATE_CONSTANT_0 = "ImNotUsedButNeedToBeHereForSomeJavaVMs";

   /**
    * file menu
    **/
   @RBEntry("Nuova risorsa...")
   public static final String PRIVATE_CONSTANT_1 = "newRootLabel";

   @RBEntry("N")
   public static final String PRIVATE_CONSTANT_2 = "newRootMnemonic";

   @RBEntry("Nuova risorsa")
   public static final String PRIVATE_CONSTANT_3 = "newRootToolTip";

   @RBEntry("Apri risorsa...")
   public static final String PRIVATE_CONSTANT_4 = "loadRootLabel";

   @RBEntry("O")
   public static final String PRIVATE_CONSTANT_5 = "loadRootMnemonic";

   @RBEntry("Apre una risorsa")
   public static final String PRIVATE_CONSTANT_6 = "loadRootToolTip";

   @RBEntry("Inserisci risorsa esistente")
   public static final String PRIVATE_CONSTANT_7 = "insertExistingLabel";

   @RBEntry("Inserisci nuova risorsa")
   public static final String PRIVATE_CONSTANT_8 = "createAndInsertLabel";

   @RBEntry("Inserisci risorsa esistente")
   public static final String PRIVATE_CONSTANT_9 = "insertExistingToolTip";

   @RBEntry("Inserisci nuova risorsa")
   public static final String PRIVATE_CONSTANT_10 = "createAndInsertToolTip";

   /**
    * selected menu
    **/
   @RBEntry("Valori di default operazione")
   public static final String PRIVATE_CONSTANT_11 = "updateTimeCostAttributesLabel";

   @RBEntry("Valori di default operazione")
   public static final String PRIVATE_CONSTANT_12 = "updateTimeCostAttributesToolTip";

   @RBEntry("Elimina risorsa")
   public static final String PRIVATE_CONSTANT_13 = "deleteLabel";

   @RBEntry("Elimina risorsa")
   public static final String PRIVATE_CONSTANT_14 = "deleteToolTip";

   /**
    * about menu - overrides the main definitions
    **/
   @RBEntry("Informazioni su Navigatore risorse di fabbricazione")
   public static final String PRIVATE_CONSTANT_15 = "aboutLabel";

   @RBEntry("Visualizza la finestra delle informazioni sul Navigatore risorse di fabbricazione")
   public static final String PRIVATE_CONSTANT_16 = "aboutToolTip";

   /**
    * time & cost panel
    **/
   @RBEntry("Informazioni sui tempi (default: secondi)")
   public static final String PRIVATE_CONSTANT_17 = "parameterTimeSectionLabel";

   @RBEntry("Informazioni sui costi (default: USD)")
   public static final String PRIVATE_CONSTANT_18 = "parameterCostSectionLabel";

   @RBEntry("Altre informazioni")
   public static final String PRIVATE_CONSTANT_19 = "parameterOtherSectionLabel";

   /**
    * attribute panel
    **/
   @RBEntry("Categoria")
   public static final String PRIVATE_CONSTANT_20 = "categoryLabel";

   @RBEntry("Reparto")
   public static final String PRIVATE_CONSTANT_21 = "departmentLabel";

   @RBEntry("Codice di convalida ERP")
   public static final String PRIVATE_CONSTANT_22 = "erpValidationCodeLabel";

   @RBEntry("Precisione")
   public static final String PRIVATE_CONSTANT_23 = "precisionLabel";

   @RBEntry("Quota X")
   public static final String PRIVATE_CONSTANT_24 = "dimXLabel";

   @RBEntry("Quota Y")
   public static final String PRIVATE_CONSTANT_25 = "dimYLabel";

   @RBEntry("Quota Z")
   public static final String PRIVATE_CONSTANT_26 = "dimZLabel";

   @RBEntry("Volume di lavorazione X")
   public static final String PRIVATE_CONSTANT_27 = "workingVolumeXLabel";

   @RBEntry("Volume di lavorazione Y")
   public static final String PRIVATE_CONSTANT_28 = "workingVolumeYLabel";

   @RBEntry("Volume di lavorazione Z")
   public static final String PRIVATE_CONSTANT_29 = "workingVolumeZLabel";

   @RBEntry("Tempo in coda standard")
   public static final String PRIVATE_CONSTANT_30 = "queueTimeLabel";

   @RBEntry("Tempo di impostazione standard")
   public static final String PRIVATE_CONSTANT_31 = "setupTimeLabel";

   @RBEntry("Tempo di elaborazione standard")
   public static final String PRIVATE_CONSTANT_32 = "processingTimeLabel";

   @RBEntry("Tempo di smantellamento standard")
   public static final String PRIVATE_CONSTANT_33 = "teardownTimeLabel";

   @RBEntry("Tempo di lavorazione standard")
   public static final String PRIVATE_CONSTANT_34 = "laborTimeLabel";

   @RBEntry("Tempo di spostamento standard")
   public static final String PRIVATE_CONSTANT_35 = "moveTimeLabel";

   @RBEntry("Tempo di attesa standard")
   public static final String PRIVATE_CONSTANT_36 = "waitTimeLabel";

   @RBEntry("Altri tempi standard")
   public static final String PRIVATE_CONSTANT_37 = "std8TimeLabel";

   @RBEntry("Costi coda standard")
   public static final String PRIVATE_CONSTANT_38 = "queueCostLabel";

   @RBEntry("Costi di impostazione standard")
   public static final String PRIVATE_CONSTANT_39 = "setupCostLabel";

   @RBEntry("Costi di elaborazione standard")
   public static final String PRIVATE_CONSTANT_40 = "processingCostLabel";

   @RBEntry("Costi di smantellamento standard")
   public static final String PRIVATE_CONSTANT_41 = "teardownCostLabel";

   @RBEntry("Costi di lavorazione standard")
   public static final String PRIVATE_CONSTANT_42 = "laborCostLabel";

   @RBEntry("Costi di spostamento standard")
   public static final String PRIVATE_CONSTANT_43 = "moveCostLabel";

   @RBEntry("Costi di attesa standard")
   public static final String PRIVATE_CONSTANT_44 = "waitCostLabel";

   @RBEntry("Altri costi standard")
   public static final String PRIVATE_CONSTANT_45 = "std8CostLabel";

   /**
    * Distribution Targets
    **/
   @RBEntry("Assegna distribuzione...")
   public static final String PRIVATE_CONSTANT_46 = "assignDistributionLabel";

   @RBEntry("A")
   public static final String PRIVATE_CONSTANT_47 = "assignDistributionMnemonic";

   @RBEntry("Assegna distribuzione")
   public static final String PRIVATE_CONSTANT_48 = "assignDistributionToolTip";

   /**
    * Step Usage Rate Lables
    **/
   @RBEntry("Tasso di utilizzo impostazione")
   public static final String PRIVATE_CONSTANT_49 = "setupUsageRateLabel";

   @RBEntry("Tasso di utilizzo elaborazione")
   public static final String PRIVATE_CONSTANT_50 = "processingUsageRateLabel";

   @RBEntry("Tasso di utilizzo smantellamento")
   public static final String PRIVATE_CONSTANT_51 = "teardownUsageRateLabel";

   @RBEntry("Tasso di utilizzo coda")
   public static final String PRIVATE_CONSTANT_52 = "queueUsageRateLabel";

   @RBEntry("Tasso di utilizzo spostamento")
   public static final String PRIVATE_CONSTANT_53 = "moveUsageRateLabel";

   @RBEntry("Tasso di utilizzo attesa")
   public static final String PRIVATE_CONSTANT_54 = "waitingUsageRateLabel";

   @RBEntry("Altro tasso di utilizzo")
   public static final String PRIVATE_CONSTANT_55 = "otherUsageRateLabel";

   /**
    * Time and Cost Sub-tab Labels
    **/
   @RBEntry("Tempistiche e costi ")
   public static final String PRIVATE_CONSTANT_56 = "timeCostTabLabel";
   
   @RBEntry("Arresta propagazione effettività")
   public static final String PRIVATE_CONSTANT_57 = "effPropagationStopLabel";
   
   @RBEntry("Unità")
   public static final String PRIVATE_CONSTANT_58 = "usedQuantityUnitLabel";
    
   @RBEntry("Quantità")
   public static final String PRIVATE_CONSTANT_59 = "usedQuantityAmountLabel";


   @RBEntry("Incolla come istanza risorsa")
   public static final String PRIVATE_CONSTANT_60 = "pasteResourceInstanceLabel";

   @RBEntry("Incolla come istanza risorsa")
   public static final String PRIVATE_CONSTANT_61 = "pasteResourceInstanceToolTip";
   
}
