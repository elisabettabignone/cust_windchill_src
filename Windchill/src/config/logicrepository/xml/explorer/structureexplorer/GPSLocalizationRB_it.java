/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package config.logicrepository.xml.explorer.structureexplorer;

import wt.util.resource.*;

@RBUUID("config.logicrepository.xml.explorer.structureexplorer.GPSLocalizationRB")
public final class GPSLocalizationRB_it extends WTListResourceBundle {
   @RBEntry("Espressioni/parametri")
   public static final String PRIVATE_CONSTANT_0 = "constraintExpressionParametersLabel";

   @RBEntry("Nome")
   public static final String PRIVATE_CONSTANT_1 = "nameLabel";

   @RBEntry("Mappa ad attributo")
   public static final String PRIVATE_CONSTANT_2 = "mapToAttributeLabel";

   @RBEntry("Tipo di dati")
   public static final String PRIVATE_CONSTANT_3 = "dataTypeLabel";

   @RBEntry("Input attivato")
   public static final String PRIVATE_CONSTANT_4 = "inputEnabledLabel";

   @RBEntry("Options and Variants")
   public static final String PRIVATE_CONSTANT_5 = "optionsAndVariantsMenuLabel";

   @RBEntry("Options and Variants")
   public static final String PRIVATE_CONSTANT_6 = "optionsAndVariantsMenuToolTip";

   @RBEntry("Configura...")
   public static final String PRIVATE_CONSTANT_7 = "specEditorLaunchMenuItemLabel";

   @RBEntry("Configura per creare la specifica variante")
   public static final String PRIVATE_CONSTANT_8 = "specEditorLaunchMenuItemToolTip";

   @RBEntry("Ctrl+C")
   public static final String PRIVATE_CONSTANT_9 = "specEditorLaunchAccelerator";

   @RBEntry("Proprietà editor specifiche...")
   public static final String PRIVATE_CONSTANT_10 = "specEditorPropertiesMenuItemLabel";

   @RBEntry("Modifica le proprietà dell'editor specifiche")
   public static final String PRIVATE_CONSTANT_11 = "specEditorPropertiesMenuItemToolTip";

   @RBEntry("Proprietà variante...")
   public static final String PRIVATE_CONSTANT_12 = "variantSpecPropertiesMenuItemLabel";

   @RBEntry("Modifica le proprietà variante")
   public static final String PRIVATE_CONSTANT_13 = "variantSpecPropertiesMenuItemToolTip";

   @RBEntry("Convalida logica...")
   public static final String PRIVATE_CONSTANT_14 = "validateLogicLaunchMenuItemLabel";

   @RBEntry("Avvia la convalida della logica")
   public static final String PRIVATE_CONSTANT_15 = "validateLogicLaunchMenuItemToolTip";

   @RBEntry("Specificato da")
   public static final String PRIVATE_CONSTANT_16 = "specifiedByValueLabel";

   @RBEntry("Espressione/valore")
   public static final String PRIVATE_CONSTANT_17 = "expressionValueLabel";

   @RBEntry("Nome proprietà")
   public static final String PRIVATE_CONSTANT_18 = "uiPropertyNameLabel";

   @RBEntry("Valore proprietà")
   public static final String PRIVATE_CONSTANT_19 = "uiPropertyValueLabel";

   @RBEntry("Attributi varianti")
   public static final String PRIVATE_CONSTANT_20 = "variantPropertiesAttributesSectionHeading";

   @RBEntry("Numero di parte")
   public static final String PRIVATE_CONSTANT_21 = "variantPropertiesPartNumberSectionHeading";

   @RBEntry("Nome parte")
   public static final String PRIVATE_CONSTANT_22 = "variantPropertiesPartNameSectionHeading";

   @RBEntry("<interruzione di pagina>")
   public static final String PRIVATE_CONSTANT_23 = "pageBreakLabel";

   @RBEntry("Discendente")
   public static final String PRIVATE_CONSTANT_24 = "descendantLabel";

   @RBEntry("Parametro")
   public static final String PRIVATE_CONSTANT_25 = "parameterNameLabel";

   @RBEntry("Copia nel desktop")
   public static final String PRIVATE_CONSTANT_26 = "copyToDesktopLabel";

   @RBEntry("Copia la tabella dei casi nel desktop")
   public static final String PRIVATE_CONSTANT_27 = "copyToDesktopToolTip";

   @RBEntry("Incolla dal desktop")
   public static final String PRIVATE_CONSTANT_28 = "pasteFromDesktopLabel";

   @RBEntry("Incolla la tabella dei casi dal desktop")
   public static final String PRIVATE_CONSTANT_29 = "pasteFromDesktopToolTip";

   @RBEntry("Unità")
   public static final String PRIVATE_CONSTANT_30 = "unitsLabel";

   @RBEntry("Descrizione")
   public static final String PRIVATE_CONSTANT_31 = "descriptionLabel";

   @RBEntry("Prompt")
   public static final String PRIVATE_CONSTANT_32 = "promptLabel";

   @RBEntry("Proprietà interruzione di pagina")
   public static final String PRIVATE_CONSTANT_33 = "pageBreakPropertiesLabel";

   @RBEntry("Modifica le proprietà per l'interruzione di pagina")
   public static final String PRIVATE_CONSTANT_34 = "pageBreakPropertiesToolTip";

   @RBEntry("Proprietà")
   public static final String PRIVATE_CONSTANT_35 = "gpPropertiesMenuLabel";
}
