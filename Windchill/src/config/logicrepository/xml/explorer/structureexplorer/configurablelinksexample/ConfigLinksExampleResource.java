/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package config.logicrepository.xml.explorer.structureexplorer.configurablelinksexample;

import wt.util.resource.*;

@RBUUID("config.logicrepository.xml.explorer.structureexplorer.configurablelinksexample.ConfigLinksExampleResource")
public final class ConfigLinksExampleResource extends WTListResourceBundle {
   /**
    * ##############################
    * Configuration Links Example #
    * ##############################
    * ###################
    * ### Tab Labels ####
    * ###################
    **/
   @RBEntry("Configurable Link Examples")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_0 = "configurableLinksExampleMainTabLabel";

   @RBEntry("Describe Child Objects")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_1 = "customDescribeLinkTabLabel";

   @RBEntry("Describe Parent Objects")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_2 = "customDescribeLinkReverseTabLabel";

   @RBEntry("Master Child Objects")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_3 = "customMastersLinkTabLabel";

   @RBEntry("Master Parent Objects")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_4 = "customMastersLinkReverseTabLabel";

   @RBEntry("Reference Child Objects")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_5 = "customReferenceLinkTabLabel";

   @RBEntry("Reference Parent Objects")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_6 = "customReferenceLinkReverseTabLabel";

   /**
    * ##########################################
    * ### Indicator Labels - Describe Links ####
    * ##########################################
    **/
   @RBEntry("Part Has Describe Child Objects")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_7 = "hasCustomDescribeLinkLabel";

   @RBEntry("Part Has Describe Child Objects")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_8 = "hasCustomDescribeLinkToolTip";

   @RBEntry("Part Has Describe Child Objects")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_9 = "hasCustomDescribeLinkToolTipForIndicator";

   @RBEntry("Part Has Describe Parent Objects")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_10 = "hasCustomDescribeReverseLinkLabel";

   @RBEntry("Part Has Describe Parent Objects")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_11 = "hasCustomDescribeReverseLinkToolTip";

   @RBEntry("Part Has Describe Parent Objects")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_12 = "hasCustomDescribeReverseLinkToolTipForIndicator";

   /**
    * #########################################
    * ### Indicator Labels - Masters Links ####
    * #########################################
    **/
   @RBEntry("Part Has Master Child Objects")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_13 = "hasCustomMastersLinkLabel";

   @RBEntry("Part Has Master Child Objects")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_14 = "hasCustomMastersLinkToolTip";

   @RBEntry("Part Has Master Child Objects")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_15 = "hasCustomMastersLinkToolTipForIndicator";

   @RBEntry("Part Has Master Parent Objects")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_16 = "hasCustomMastersReverseLinkLabel";

   @RBEntry("Part Has Master Parent Objects")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_17 = "hasCustomMastersReverseLinkToolTip";

   @RBEntry("Part Has Master Parent Objects")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_18 = "hasCustomMastersReverseLinkToolTipForIndicator";

   /**
    * ###########################################
    * ### Indicator Labels - Reference Links ####
    * ###########################################
    **/
   @RBEntry("Part Has Reference Child Objects")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_19 = "hasCustomReferenceLinkLabel";

   @RBEntry("Part Has Reference Child Objects")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_20 = "hasCustomReferenceLinkToolTip";

   @RBEntry("Part Has Reference Child Objects")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_21 = "hasCustomReferenceLinkToolTipForIndicator";

   @RBEntry("Part Has Reference Parent Objects")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_22 = "hasCustomReferenceReverseLinkLabel";

   @RBEntry("Part Has Reference Parent Objects")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_23 = "hasCustomReferenceReverseLinkToolTip";

   @RBEntry("Part Has Reference Parent Objects")
   @RBComment("DO NOT TRANSLATE")
   public static final String PRIVATE_CONSTANT_24 = "hasCustomReferenceReverseLinkToolTipForIndicator";
}
