/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package config.logicrepository.xml.explorer.structureexplorer;

import wt.util.resource.*;

@RBUUID("config.logicrepository.xml.explorer.structureexplorer.ExplorerFileSystemViewResource")
public final class ExplorerFileSystemViewResource extends WTListResourceBundle {
   @RBEntry("Java Classes")
   public static final String PRIVATE_CONSTANT_0 = "javaClassFilter";

   @RBEntry("Directory")
   public static final String PRIVATE_CONSTANT_1 = "directoryFilter";

   @RBEntry("Help Files")
   public static final String PRIVATE_CONSTANT_2 = "helpFileFilter";

   @RBEntry("Image Files")
   public static final String PRIVATE_CONSTANT_3 = "imageFileFilter";
}
