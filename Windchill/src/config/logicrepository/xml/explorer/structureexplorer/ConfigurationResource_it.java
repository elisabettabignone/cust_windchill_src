/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package config.logicrepository.xml.explorer.structureexplorer;

import wt.util.resource.*;

@RBUUID("config.logicrepository.xml.explorer.structureexplorer.ConfigurationResource")
public final class ConfigurationResource_it extends WTListResourceBundle {

   @RBEntry("Contesto Acquisti")
   public static final String PRIVATE_CONSTANT_3 = "sourcingContextLabel";

   @RBEntry("ID produttore")
   public static final String PRIVATE_CONSTANT_4 = "manufacturerIdLabel";

   @RBEntry("ID fornitore")
   public static final String PRIVATE_CONSTANT_5 = "vendorIdLabel";

   /**
    *
    * Panel Property labels
    *
    **/
   @RBEntry("Indice bolla")
   public static final String PRIVATE_CONSTANT_6 = "findNumberLabel";

   @RBEntry("Numero di riga")
   public static final String PRIVATE_CONSTANT_7 = "lineNumberLabel";

   @RBEntry("Scelte assegnate")
   public static final String PRIVATE_CONSTANT_8 = "usagelinkDesignationLabel";

   @RBEntry("Stato")
   public static final String PRIVATE_CONSTANT_9 = "checkoutInfoStateLabel";

   @RBEntry("Nome organizzazione")
   public static final String PRIVATE_CONSTANT_10 = "organizationNameLabel";

   @RBEntry("Versione")
   public static final String PRIVATE_CONSTANT_13 = "versionNameLabel";

   @RBEntry("Tipo di oggetto")
   public static final String PRIVATE_CONSTANT_14 = "objectTypeLabel";

   @RBEntry("Criteri di ricerca")
   public static final String PRIVATE_CONSTANT_15 = "searchCritieriaPanelLabel";

   @RBEntry("Cerca...")
   public static final String PRIVATE_CONSTANT_16 = "searchLabel";

   @RBEntry("Numero di serie")
   public static final String PRIVATE_CONSTANT_17 = "serialNumberValueLabel";

   @RBEntry("Padre")
   public static final String PRIVATE_CONSTANT_18 = "parentName";

   @RBEntry("Intervallo indicatori di riferimento")
   public static final String PRIVATE_CONSTANT_19 = "referenceDesignatorRangeLabel";

   @RBEntry("Indicatore di riferimento")
   public static final String PRIVATE_CONSTANT_20 = "referenceDesignatorNameOrRangeLabel";

   @RBEntry("Chiave di ricerca")
   public static final String PRIVATE_CONSTANT_21 = "typeListLabel";

   @RBEntry("Autore")
   public static final String PRIVATE_CONSTANT_22 = "searchCriteriaCreatorNameLabel";

   @RBEntry("Autore")
   public static final String PRIVATE_CONSTANT_23 = "searchCriteriaCreatorNameToolTip";

   /**
    *
    * Tabbed Pane Labels
    *
    **/
   @RBEntry("Struttura distinta base")
   public static final String PRIVATE_CONSTANT_35 = "BOMTreeTabLabel";

   @RBEntry("Struttura casi d'impiego")
   public static final String PRIVATE_CONSTANT_36 = "OccurrencedTreeTabLabel";

   @RBEntry("Modifica")
   public static final String PRIVATE_CONSTANT_37 = "editModeLabel";

   @RBEntry("Bozza")
   public static final String PRIVATE_CONSTANT_38 = "draftModeLabel";

   @RBEntry("Annotazione")
   public static final String PRIVATE_CONSTANT_39 = "annotateModeLabel";

   @RBEntry("Sola lettura")
   public static final String PRIVATE_CONSTANT_40 = "readModeLabel";

   /**
    *
    * Tabbed Pane - Information
    *
    **/
   @RBEntry("Informazioni")
   public static final String PRIVATE_CONSTANT_41 = "propertiesTabLabel";

   @RBEntry("Identificativo")
   public static final String PRIVATE_CONSTANT_42 = "identityTabLabel";

   @RBEntry("Attributi oggetto")
   public static final String PRIVATE_CONSTANT_43 = "objAttrTabLabel";

   @RBEntry("Attributi utilizzo")
   public static final String PRIVATE_CONSTANT_44 = "usageAttrTabLabel";

   @RBEntry("Classificazione")
   public static final String PRIVATE_CONSTANT_45 = "classificationTabLabel";

   @RBEntry("Scelte espressioni di base assegnate")
   public static final String PRIVATE_CONSTANT_46 = "optionsTabLabel";

   @RBEntry("Aggiungi attributo")
   public static final String PRIVATE_CONSTANT_47 = "addValueLabel";

   @RBEntry("Aggiungi attributo")
   public static final String PRIVATE_CONSTANT_48 = "addValueToolTip";

   @RBEntry("Classifica")
   public static final String PRIVATE_CONSTANT_49 = "classifyLabel";

   @RBEntry("Classifica")
   public static final String PRIVATE_CONSTANT_50 = "classifyToolTip";

   @RBEntry("Aggiungi attributo ad hoc")
   public static final String PRIVATE_CONSTANT_51 = "addAdhocAttributeLabel";

   @RBEntry("Aggiungi attributo ad hoc")
   public static final String PRIVATE_CONSTANT_52 = "addAdhocAttributeToolTip";

   @RBEntry("Rimuovi classificazione")
   public static final String PRIVATE_CONSTANT_53 = "removeClassificationLabel";

   @RBEntry("Rimuovi classificazione")
   public static final String PRIVATE_CONSTANT_54 = "removeClassificationToolTip";

   @RBEntry("Rimuovi attributo")
   public static final String PRIVATE_CONSTANT_55 = "removeValueLabel";

   @RBEntry("Rimuovi attributo")
   public static final String PRIVATE_CONSTANT_56 = "removeValueToolTip";

   @RBEntry("Check-Out padre")
   public static final String PRIVATE_CONSTANT_57 = "checkoutParentLabel";

   @RBEntry("Check-Out padre")
   public static final String PRIVATE_CONSTANT_58 = "checkoutParentToolTip";

   @RBEntry("Check-In padre")
   public static final String PRIVATE_CONSTANT_59 = "checkinParentLabel";

   @RBEntry("Check-In padre")
   public static final String PRIVATE_CONSTANT_60 = "checkinParentToolTip";

   /**
    *
    * Tabbed Pane - Uses
    *
    **/
   @RBEntry("Componenti")
   public static final String PRIVATE_CONSTANT_61 = "usesTabLabel";

   @RBEntry("Distinta base")
   public static final String PRIVATE_CONSTANT_62 = "usesTabBOMtable";

   @RBEntry("Distinta base non filtrata")
   public static final String PRIVATE_CONSTANT_63 = "usesTabUnFilteredBOMtable";

   @RBEntry("Casi d'impiego")
   public static final String PRIVATE_CONSTANT_64 = "usesTabOccurrencesTable";

   @RBEntry("Modifica tabella")
   public static final String PRIVATE_CONSTANT_65 = "editCellsLabel";

   @RBEntry("Modifica valori nella tabella")
   public static final String PRIVATE_CONSTANT_66 = "editCellsToolTip";

   /**
    * Uses Table
    **/
   @RBEntry("Posizione visiva")
   public static final String PRIVATE_CONSTANT_67 = "visualLocationLabel";

   /**
    *
    * Tabbed Pane - View
    *
    **/
   @RBEntry("Vista")
   public static final String PRIVATE_CONSTANT_68 = "viewTabLabel";

   /**
    *
    * Tabbed Pane - UsedBy (now called Where Used)
    *
    **/
   @RBEntry("Dove usato")
   public static final String PRIVATE_CONSTANT_69 = "usedByTabLabel";

   /**
    *
    * Tabbed Pane - association constraint tree tab labels
    *
    **/
   @RBEntry("Apri oggetto correlato")
   public static final String PRIVATE_CONSTANT_70 = "openAssociationConstraintLinkLabel";

   @RBEntry("Apri oggetto correlato")
   public static final String PRIVATE_CONSTANT_71 = "openAssociationConstraintLinkToolTip";

   @RBEntry("Aggiungi link")
   public static final String PRIVATE_CONSTANT_72 = "addAssociationConstraintLinkLabel";

   @RBEntry("Aggiungi link")
   public static final String PRIVATE_CONSTANT_73 = "addAssociationConstraintLinkToolTip";

   @RBEntry("Rimuovi link")
   public static final String PRIVATE_CONSTANT_74 = "removeAssociationConstraintLinkLabel";

   @RBEntry("Rimuovi link")
   public static final String PRIVATE_CONSTANT_75 = "removeAssociationConstraintLinkToolTip";

   /**
    *
    * Tabbed Pane - Documentation
    *
    **/
   @RBEntry("Documentazione")
   public static final String PRIVATE_CONSTANT_76 = "documentationTabLabel";

   @RBEntry("Link riferimento")
   public static final String PRIVATE_CONSTANT_77 = "referenceLinkDocTableLabel";

   @RBEntry("Link descrizione")
   public static final String PRIVATE_CONSTANT_78 = "describeLinkDocTableLabel";

   @RBEntry("Documenti associati")
   public static final String PRIVATE_CONSTANT_79 = "associatedDocumentsTableTitle";

   @RBEntry("Documenti descrittivi")
   public static final String PRIVATE_CONSTANT_80 = "describingDocumentsTableTitle";

   @RBEntry("Documenti referenziati")
   public static final String PRIVATE_CONSTANT_81 = "referencedDocumentsTableTitle";

   @RBEntry("Versione")
   public static final String PRIVATE_CONSTANT_82 = "documentsTabTableVersionLabel";

   @RBEntry("Iterazione")
   public static final String PRIVATE_CONSTANT_83 = "documentsTabTableIterationLabel";

   @RBEntry("Crea e associa un documento Descritto con")
   public static final String PRIVATE_CONSTANT_84 = "createDesDocumentLabel";

   @RBEntry("Crea e associa un documento Descritto con")
   public static final String PRIVATE_CONSTANT_85 = "createDesDocumentToolTip";

   @RBEntry("Crea e associa un documento Riferimenti")
   public static final String PRIVATE_CONSTANT_86 = "createRefDocumentLabel";

   @RBEntry("Crea e associa un documento Riferimenti")
   public static final String PRIVATE_CONSTANT_87 = "createRefDocumentToolTip";

   @RBEntry("Aggiungi documento associato")
   public static final String PRIVATE_CONSTANT_88 = "addDocumentLabel";

   @RBEntry("Rimuovi documento associato")
   public static final String PRIVATE_CONSTANT_89 = "removeDocumentLabel";

   @RBEntry("Visualizza documento associato")
   public static final String PRIVATE_CONSTANT_90 = "viewDocumentLabel";

   @RBEntry("Aggiungi documento associato")
   public static final String PRIVATE_CONSTANT_91 = "addDocumentToolTip";

   @RBEntry("Rimuovi documento associato")
   public static final String PRIVATE_CONSTANT_92 = "removeDocumentToolTip";

   @RBEntry("Visualizza documento associato")
   public static final String PRIVATE_CONSTANT_93 = "viewDocumentToolTip";

   /**
    *
    * Tabbed Pane - Replacements
    *
    **/
   @RBEntry("Sostituzioni")
   public static final String PRIVATE_CONSTANT_94 = "replacementsTabLabel";

   @RBEntry("Parti alternative in qualunque assieme")
   public static final String PRIVATE_CONSTANT_95 = "alternatesReplacementsTableTitle";

   @RBEntry("Parti di sostituzione nell'assieme corrente")
   public static final String PRIVATE_CONSTANT_96 = "substitutesReplacementsTableTitle";

   /**
    * Alternates Table
    **/
   @RBEntry("Alternative")
   public static final String PRIVATE_CONSTANT_97 = "alternatesLabel";

   @RBEntry("Aggiungi parte alternativa")
   public static final String PRIVATE_CONSTANT_98 = "addAlternateLabel";

   @RBEntry("Rimuovi parte alternativa")
   public static final String PRIVATE_CONSTANT_99 = "removeAlternateLabel";

   @RBEntry("Apri parte alternativa nel Navigatore struttura di prodotto")
   public static final String PRIVATE_CONSTANT_100 = "openAlternateLabel";

   @RBEntry("Aggiungi parte alternativa")
   public static final String PRIVATE_CONSTANT_101 = "addAlternateToolTip";

   @RBEntry("Rimuovi parte alternativa")
   public static final String PRIVATE_CONSTANT_102 = "removeAlternateToolTip";

   @RBEntry("Apri parte alternativa nel Navigatore struttura di prodotto")
   public static final String PRIVATE_CONSTANT_103 = "openAlternateToolTip";

   /**
    * Substitute Table
    **/
   @RBEntry("Sostituzioni")
   public static final String PRIVATE_CONSTANT_104 = "substitutesLabel";

   @RBEntry("Aggiungi parte di sostituzione")
   public static final String PRIVATE_CONSTANT_105 = "addSubstituteLabel";

   @RBEntry("Rimuovi parte di sostituzione")
   public static final String PRIVATE_CONSTANT_106 = "removeSubstituteLabel";

   @RBEntry("Apri parte di sostituzione nel Navigatore struttura di prodotto")
   public static final String PRIVATE_CONSTANT_107 = "openSubstituteLabel";

   @RBEntry("Aggiungi parte di sostituzione")
   public static final String PRIVATE_CONSTANT_108 = "addSubstituteToolTip";

   @RBEntry("Rimuovi parte di sostituzione")
   public static final String PRIVATE_CONSTANT_109 = "removeSubstituteToolTip";

   @RBEntry("Apri parte di sostituzione nel Navigatore struttura di prodotto")
   public static final String PRIVATE_CONSTANT_110 = "openSubstituteToolTip";

   /**
    *
    * Tabbed Pane - Tree
    *
    **/
   @RBEntry("Struttura")
   public static final String PRIVATE_CONSTANT_111 = "treeTabLabel";

   /**
    *
    * Tabbed Pane - AXL
    *
    **/
   @RBEntry("El. prod. approv. - El. forn. approv.")
   public static final String PRIVATE_CONSTANT_112 = "axlTabLabel";

   @RBEntry("Parti fornitore")
   public static final String PRIVATE_CONSTANT_113 = "avlTableTitle";

   @RBEntry("Parti produttore ")
   public static final String PRIVATE_CONSTANT_114 = "amlTableTitle";

   /**
    * Manufacuturer Table
    **/
   @RBEntry("Aggiungi parte produttore")
   public static final String PRIVATE_CONSTANT_115 = "addManufacuturerLabel";

   @RBEntry("Rimuovi parte produttore")
   public static final String PRIVATE_CONSTANT_116 = "removeManufacuturerLabel";

   @RBEntry("Apri parte produttore nel Navigatore struttura di prodotto")
   public static final String PRIVATE_CONSTANT_117 = "openManufacuturerLabel";

   @RBEntry("Aggiungi parte produttore")
   public static final String PRIVATE_CONSTANT_118 = "addManufacuturerToolTip";

   @RBEntry("Rimuovi parte produttore")
   public static final String PRIVATE_CONSTANT_119 = "removeManufacuturerToolTip";

   @RBEntry("Apri parte produttore nel Navigatore struttura di prodotto")
   public static final String PRIVATE_CONSTANT_120 = "openManufacuturerToolTip";

   /**
    * Vendor Table
    **/
   @RBEntry("Aggiungi parte fornitore")
   public static final String PRIVATE_CONSTANT_121 = "addVendorLabel";

   @RBEntry("Rimuovi parte fornitore")
   public static final String PRIVATE_CONSTANT_122 = "removeVendorLabel";

   @RBEntry("Apri parte del fornitore nel Navigatore struttura di prodotto")
   public static final String PRIVATE_CONSTANT_123 = "openVendorLabel";

   @RBEntry("Aggiungi parte fornitore")
   public static final String PRIVATE_CONSTANT_124 = "addVendorToolTip";

   @RBEntry("Rimuovi parte fornitore")
   public static final String PRIVATE_CONSTANT_125 = "removeVendorToolTip";

   @RBEntry("Apri parte del fornitore nel Navigatore struttura di prodotto")
   public static final String PRIVATE_CONSTANT_126 = "openVendorToolTip";

   /**
    * Query Table
    **/
   @RBEntry("Esegui interrogazione salvata")
   public static final String PRIVATE_CONSTANT_127 = "exectuteQueryLabel";

   @RBEntry("Crea interrogazione salvata")
   public static final String PRIVATE_CONSTANT_128 = "createQueryLabel";

   @RBEntry("Modifica interrogazione salvata")
   public static final String PRIVATE_CONSTANT_129 = "editQueryLabel";

   @RBEntry("Copia interrogazione")
   public static final String PRIVATE_CONSTANT_130 = "copyQueryLabel";

   @RBEntry("Elimina voce")
   public static final String PRIVATE_CONSTANT_131 = "deleteQueryLabel";

   @RBEntry("Esegui interrogazione salvata")
   public static final String PRIVATE_CONSTANT_132 = "exectuteQueryToolTip";

   @RBEntry("Crea interrogazione salvata")
   public static final String PRIVATE_CONSTANT_133 = "createQueryToolTip";

   @RBEntry("Modifica interrogazione salvata")
   public static final String PRIVATE_CONSTANT_134 = "editQueryToolTip";

   @RBEntry("Copia un'interrogazione salvata")
   public static final String PRIVATE_CONSTANT_135 = "copyQueryToolTip";

   @RBEntry("Elimina un'interrogazione salvata")
   public static final String PRIVATE_CONSTANT_136 = "deleteQueryToolTip";

   @RBEntry("Aggiungi accesso gruppo")
   public static final String PRIVATE_CONSTANT_136_1 = "addGroupAccessQueryToolTip";

   /**
    * Query Results Table
    **/
   @RBEntry("Aggiungi voce selezionata")
   public static final String PRIVATE_CONSTANT_137 = "queryResultAppendLabel";

   @RBEntry("Visualizza icona nell'albero")
   public static final String PRIVATE_CONSTANT_138 = "queryResultIconLabel";

   @RBEntry("Selezionare colore per la riga")
   public static final String PRIVATE_CONSTANT_139 = "queryResultColorLabel";

   @RBEntry("Rimuovi voce selezionata da risultati interrogazione")
   public static final String PRIVATE_CONSTANT_140 = "queryResultRemoveLabel";

   @RBEntry("Apri pagina delle proprietà")
   public static final String PRIVATE_CONSTANT_141 = "queryResultPropertiesLabel";

   @RBEntry("Apri in una nuova finestra")
   public static final String PRIVATE_CONSTANT_142 = "queryResultLaunchToolTip";

   @RBEntry("Copia")
   public static final String PRIVATE_CONSTANT_143 = "queryResultCopyLabel";

   @RBEntry("Nome")
   public static final String PRIVATE_CONSTANT_144 = "nameLabel";

   @RBEntry("Numero")
   public static final String PRIVATE_CONSTANT_145 = "numberLabel";

   @RBEntry("Aggiungi voce selezionata")
   public static final String PRIVATE_CONSTANT_146 = "queryResultAppendToolTip";

   @RBEntry("Visualizza icona nell'albero")
   public static final String PRIVATE_CONSTANT_147 = "queryResultIconToolTip";

   @RBEntry("Selezionare colore per la riga")
   public static final String PRIVATE_CONSTANT_148 = "queryResultColorToolTip";

   @RBEntry("Rimuovi voce selezionata da risultati interrogazione")
   public static final String PRIVATE_CONSTANT_149 = "queryResultRemoveToolTip";

   @RBEntry("Apri pagina delle proprietà")
   public static final String PRIVATE_CONSTANT_150 = "queryResultPropertiesToolTip";

   @RBEntry("Copia")
   public static final String PRIVATE_CONSTANT_151 = "queryResultCopyToolTip";

   /**
    * Query Panel
    **/
   @RBEntry("Salva interrogazione")
   public static final String PRIVATE_CONSTANT_152 = "saveQueryLabel";

   @RBEntry("Nome interrogazione")
   public static final String PRIVATE_CONSTANT_153 = "queryNameLabel";

   @RBEntry("Descrizione")
   public static final String PRIVATE_CONSTANT_154 = "queryDescriptionLabel";

   @RBEntry("Accesso")
   public static final String PRIVATE_CONSTANT_155 = "queryAccessLabel";

   /**
    * Query Group Table
    **/
   @RBEntry("Nome gruppo")
   public static final String PRIVATE_CONSTANT_156 = "queryGroupNameLabel";

   @RBEntry("Nome dominio")
   public static final String PRIVATE_CONSTANT_157 = "queryDomainNameLabel";

   /**
    * Query Labels
    **/
   @RBEntry("Il nodo corrisponde all'interrogazione")
   @RBComment("This is used to indicate that this node matches the query that was just executed.")
   public static final String PRIVATE_CONSTANT_158 = "nodeMatchesQuery";

   /**
    *
    * GPS Parameters Tab
    *
    **/
   @RBEntry("Parametri")
   public static final String PRIVATE_CONSTANT_159 = "gpsParametersTabLabel";

   @RBEntry("Parametri")
   public static final String PRIVATE_CONSTANT_160 = "gpsParametersTableTitle";

   @RBEntry("Crea parametro")
   public static final String PRIVATE_CONSTANT_161 = "createParameterLabel";

   @RBEntry("Crea un nuovo parametro")
   public static final String PRIVATE_CONSTANT_162 = "createParameterToolTip";

   @RBEntry("Modifica parametro")
   public static final String PRIVATE_CONSTANT_163 = "editParameterLabel";

   @RBEntry("Modifica il parametro selezionato")
   public static final String PRIVATE_CONSTANT_164 = "editParameterToolTip";

   @RBEntry("Rimuovi parametro")
   public static final String PRIVATE_CONSTANT_165 = "removeParameterLabel";

   @RBEntry("Rimuove il parametro selezionato")
   public static final String PRIVATE_CONSTANT_166 = "removeParameterToolTip";

   @RBEntry("Crea gruppo parametri")
   public static final String PRIVATE_CONSTANT_167 = "createParameterGroupLabel";

   @RBEntry("Crea gruppo parametri")
   public static final String PRIVATE_CONSTANT_168 = "createParameterGroupToolTip";

   @RBEntry("Inserisci interruzione di pagina")
   public static final String PRIVATE_CONSTANT_169 = "addPageBreakLabel";

   @RBEntry("Inserisce un'interruzione di pagina sotto")
   public static final String PRIVATE_CONSTANT_170 = "addPageBreakToolTip";

   @RBEntry("Inserisci risoluzione figlio")
   public static final String PRIVATE_CONSTANT_171 = "addChildResolutionLabel";

   @RBEntry("Inserisce una risoluzione figlio sotto")
   public static final String PRIVATE_CONSTANT_172 = "addChildResolutionToolTip";

   /**
    * Create parameter Clerk labels
    **/
   @RBEntry("Interfaccia utente")
   public static final String PRIVATE_CONSTANT_173 = "parameterUITabLabel";

   @RBEntry("Accesso")
   public static final String PRIVATE_CONSTANT_174 = "parameterAccessTabLabel";

   @RBEntry("Equivalenza")
   public static final String PRIVATE_CONSTANT_175 = "parameterEquivalencyTabLabel";

   @RBEntry("Vincolo")
   public static final String PRIVATE_CONSTANT_176 = "parameterConstraintTabLabel";

   @RBEntry("Informazioni")
   public static final String PRIVATE_CONSTANT_177 = "parameterPropertiesTabLabel";

   @RBEntry("Nessuno")
   public static final String PRIVATE_CONSTANT_178 = "parameterConstraintNoneLabel";

   @RBEntry("Non applica vincoli al parametro")
   public static final String PRIVATE_CONSTANT_179 = "parameterConstraintNoneToolTip";

   @RBEntry("Intervallo")
   public static final String PRIVATE_CONSTANT_180 = "parameterConstraintRangeLabel";

   @RBEntry("Vincola il parametro a un intervallo")
   public static final String PRIVATE_CONSTANT_181 = "parameterConstraintRangeToolTip";

   @RBEntry("Elenco")
   public static final String PRIVATE_CONSTANT_182 = "parameterConstraintListLabel";

   @RBEntry("Vincola il parametro a un elenco di valori")
   public static final String PRIVATE_CONSTANT_183 = "parameterConstraintListToolTip";

   @RBEntry("Elenco dinamico")
   public static final String PRIVATE_CONSTANT_184 = "parameterConstraintDynamicListLabel";

   @RBEntry("Vincola il parametro in base all'output di un metodo di classe")
   public static final String PRIVATE_CONSTANT_185 = "parameterConstraintDynamicListToolTip";

   @RBEntry("Parametro")
   public static final String PRIVATE_CONSTANT_186 = "parameterInfoParameterSectionLabel";

   @RBEntry("Definizione")
   public static final String PRIVATE_CONSTANT_187 = "parameterInfoDefinitionSectionLabel";

   @RBEntry("Valore")
   public static final String PRIVATE_CONSTANT_188 = "parameterInfoValueSectionLabel";

   /**
    *
    * GPS Constraints Tab
    *
    **/
   @RBEntry("Vincoli")
   public static final String PRIVATE_CONSTANT_189 = "gpsConstraintsTabLabel";

   @RBEntry("Vincoli")
   public static final String PRIVATE_CONSTANT_190 = "gpsConstraintsTableTitle";

   @RBEntry("Seleziona parametri")
   public static final String PRIVATE_CONSTANT_191 = "selectParameterLabel";

   @RBEntry("Seleziona parametri")
   public static final String PRIVATE_CONSTANT_192 = "selectParameterToolTip";

   @RBEntry("Genera casi")
   public static final String PRIVATE_CONSTANT_193 = "generateCasesLabel";

   @RBEntry("Genera casi per parametri enumerati")
   public static final String PRIVATE_CONSTANT_194 = "generateCasesToolTip";

   /**
    *
    * Menu labels
    *
    **/
   @RBEntry("File")
   public static final String PRIVATE_CONSTANT_195 = "fileMenuLabel";

   @RBEntry("F")
   public static final String PRIVATE_CONSTANT_196 = "fileMenuMnemonic";

   @RBEntry("Modifica")
   public static final String PRIVATE_CONSTANT_197 = "editMenuLabel";

   @RBEntry("M")
   public static final String PRIVATE_CONSTANT_198 = "editMenuMnemonic";

   @RBEntry("Vista")
   public static final String PRIVATE_CONSTANT_199 = "viewMenuLabel";

   @RBEntry("V")
   public static final String PRIVATE_CONSTANT_200 = "viewMnemonic";

   @RBEntry("Selezione")
   public static final String PRIVATE_CONSTANT_201 = "selectedMenuLabel";

   @RBEntry("S")
   public static final String PRIVATE_CONSTANT_202 = "selectedMenuMnemonic";

   @RBEntry("Strumenti")
   public static final String PRIVATE_CONSTANT_203 = "toolsMenuLabel";

   @RBEntry("t")
   public static final String PRIVATE_CONSTANT_204 = "toolsMenuMnemonic";

   @RBEntry("Ciclo di vita")
   public static final String PRIVATE_CONSTANT_205 = "lifeCycleMenuLabel";

   @RBEntry("C")
   public static final String PRIVATE_CONSTANT_206 = "lifeCycleMenuMnemonic";

   @RBEntry("Guida")
   public static final String PRIVATE_CONSTANT_207 = "helpMenuLabel";

   @RBEntry("G")
   public static final String PRIVATE_CONSTANT_208 = "helpMenuMnemonic";

   /**
    * File Menu
    **/
   @RBEntry("Nuovo")
   public static final String PRIVATE_CONSTANT_209 = "newMenuLabel";

   @RBEntry("N")
   public static final String PRIVATE_CONSTANT_210 = "newMenuMnemonic";

   @RBEntry("Apri")
   public static final String PRIVATE_CONSTANT_211 = "openMenuLabel";

   @RBEntry("A")
   public static final String PRIVATE_CONSTANT_212 = "openMenuMnemonic";

   @RBEntry("Chiudi")
   public static final String PRIVATE_CONSTANT_213 = "closeLabel";

   @RBEntry("C")
   public static final String PRIVATE_CONSTANT_214 = "closeMenuMnemonic";

   @RBEntry("Chiude la struttura attiva")
   public static final String PRIVATE_CONSTANT_215 = "closeToolTip";

   @RBEntry("Salva")
   public static final String PRIVATE_CONSTANT_216 = "saveLabel";

   @RBEntry("S")
   public static final String PRIVATE_CONSTANT_217 = "saveMnemonic";

   @RBEntry("Ctrl+S")
   public static final String PRIVATE_CONSTANT_218 = "saveAccelerator";

   @RBEntry("Salva")
   public static final String PRIVATE_CONSTANT_219 = "saveToolTip";

   @RBEntry("Salva con nome...")
   public static final String PRIVATE_CONSTANT_220 = "saveAsLabel";

   @RBEntry("v")
   public static final String PRIVATE_CONSTANT_221 = "saveAsMnemonic";

   @RBEntry("Salva come gruppo di annotazione")
   public static final String PRIVATE_CONSTANT_222 = "saveAsToolTip";

   @RBEntry("Convalida modifiche")
   public static final String PRIVATE_CONSTANT_223 = "validateAnnotationLabel";

   @RBEntry("m")
   public static final String PRIVATE_CONSTANT_224 = "validateAnnotationMnemonic";

   @RBEntry("Convalida bozza/gruppo di annotazione")
   public static final String PRIVATE_CONSTANT_225 = "validateAnnotationToolTip";

   @RBEntry("Applica")
   public static final String PRIVATE_CONSTANT_226 = "applyAnnotationLabel";

   @RBEntry("l")
   public static final String PRIVATE_CONSTANT_227 = "applyAnnotationMnemonic";

   @RBEntry("Applica gruppo di annotazioni")
   public static final String PRIVATE_CONSTANT_228 = "applyAnnotationToolTip";

   @RBEntry("Proprietà...")
   public static final String PRIVATE_CONSTANT_229 = "propertiesLabel";

   @RBEntry("P")
   public static final String PRIVATE_CONSTANT_230 = "propertiesMnemonic";

   @RBEntry("Proprietà")
   public static final String PRIVATE_CONSTANT_231 = "propertiesToolTip";

   @RBEntry("Preferenze")
   public static final String PRIVATE_CONSTANT_232 = "preferencesLabel";

   @RBEntry("f")
   public static final String PRIVATE_CONSTANT_233 = "preferencesMnemonic";

   @RBEntry("Preferenze")
   public static final String PRIVATE_CONSTANT_234 = "preferencesToolTip";

   @RBEntry("Esci")
   public static final String PRIVATE_CONSTANT_235 = "exitLabel";

   @RBEntry("E")
   public static final String PRIVATE_CONSTANT_236 = "exitMnemonic";

   @RBEntry("Esci ")
   public static final String PRIVATE_CONSTANT_237 = "exitToolTip";

   /**
    * File New menu
    **/
   @RBEntry("Parte...")
   public static final String PRIVATE_CONSTANT_238 = "newPartLabel";

   @RBEntry("P")
   public static final String PRIVATE_CONSTANT_239 = "newPartMnemonic";

   @RBEntry("Ctrl+N")
   public static final String PRIVATE_CONSTANT_240 = "newPartAccelerator";

   @RBEntry("Nuova parte")
   public static final String PRIVATE_CONSTANT_241 = "newPartToolTip";

   @RBEntry("Nuova parte")
   public static final String PRIVATE_CONSTANT_242 = "newPartActionLabel";

   @RBEntry("Parte configurabile avanzata da parte...")
   public static final String PRIVATE_CONSTANT_243 = "newGenericPartLabel";

   @RBEntry("Nuova parte configurabile avanzata dalla parte")
   public static final String PRIVATE_CONSTANT_244 = "newGenericPartToolTip";

   @RBEntry("Nuova parte configurabile avanzata da parte")
   public static final String PRIVATE_CONSTANT_245 = "newGenericPartActionLabel";

   @RBEntry("Gruppo di annotazioni...")
   public static final String PRIVATE_CONSTANT_246 = "newAnnotationLabel";

   @RBEntry("A")
   public static final String PRIVATE_CONSTANT_247 = "newAnnotationMnemonic";

   @RBEntry("Ctrl+A")
   public static final String PRIVATE_CONSTANT_248 = "newAnnotaionAccelerator";

   @RBEntry("Nuovo gruppo di annotazioni")
   public static final String PRIVATE_CONSTANT_249 = "newAnnotationToolTip";

   @RBEntry("Nuovo gruppo di annotazioni")
   public static final String PRIVATE_CONSTANT_250 = "newAnnotationActionLabel";

   /**
    * File Open menu
    **/
   @RBEntry("Parte...")
   public static final String PRIVATE_CONSTANT_251 = "openPartLabel";

   @RBEntry("P")
   public static final String PRIVATE_CONSTANT_252 = "openPartMnemonic";

   @RBEntry("Apri parte")
   public static final String PRIVATE_CONSTANT_253 = "openPartToolTip";

   @RBEntry("Apri parte")
   public static final String PRIVATE_CONSTANT_254 = "openPartActionLabel";

   @RBEntry("Gruppo di annotazioni...")
   public static final String PRIVATE_CONSTANT_255 = "openAnnotationLabel";

   @RBEntry("A")
   public static final String PRIVATE_CONSTANT_256 = "openAnnotationMnemonic";

   @RBEntry("Apre un gruppo di annotazioni esistente")
   public static final String PRIVATE_CONSTANT_257 = "openAnnotationToolTip";

   @RBEntry("Apri gruppo di annotazioni")
   public static final String PRIVATE_CONSTANT_258 = "openAnnotationActionLabel";

   @RBEntry("Apri struttura...")
   public static final String PRIVATE_CONSTANT_259 = "openStructureLabel";

   @RBEntry("S")
   public static final String PRIVATE_CONSTANT_260 = "openStructureMnemonic";

   @RBEntry("Apre una struttura")
   public static final String PRIVATE_CONSTANT_261 = "openStructureToolTip";

   /**
    * Edit Menu
    **/
   @RBEntry("Modalità bozza")
   public static final String PRIVATE_CONSTANT_262 = "draftLabel";

   @RBEntry("B")
   public static final String PRIVATE_CONSTANT_263 = "draftMnemonic";

   @RBEntry("Passa alla modalità bozza")
   public static final String PRIVATE_CONSTANT_264 = "draftToolTip";

   @RBEntry("Modalità di modifica")
   public static final String PRIVATE_CONSTANT_265 = "editLabel";

   @RBEntry("m")
   public static final String PRIVATE_CONSTANT_266 = "editMnemonic";

   @RBEntry("Passa alla modalità di modifica")
   public static final String PRIVATE_CONSTANT_267 = "editToolTip";

   @RBEntry("Annulla")
   public static final String PRIVATE_CONSTANT_268 = "undoLabel";

   @RBEntry("u")
   public static final String PRIVATE_CONSTANT_269 = "undoMnemonic";

   @RBEntry("Annulla")
   public static final String PRIVATE_CONSTANT_270 = "undoToolTip";

   @RBEntry("Ctrl+Z")
   public static final String PRIVATE_CONSTANT_271 = "undoAccelerator";

   @RBEntry("Ripeti")
   public static final String PRIVATE_CONSTANT_272 = "redoLabel";

   @RBEntry("R")
   public static final String PRIVATE_CONSTANT_273 = "redoMnemonic";

   @RBEntry("Ripeti")
   public static final String PRIVATE_CONSTANT_274 = "redoToolTip";

   @RBEntry("Ctrl+Y")
   public static final String PRIVATE_CONSTANT_275 = "redoAccelerator";

   @RBEntry("Ripristina")
   public static final String PRIVATE_CONSTANT_276 = "revertLabel";

   @RBEntry("A")
   public static final String PRIVATE_CONSTANT_277 = "revertMnemonic";

   @RBEntry("Riporta un'annotazione allo stadio precedente")
   public static final String PRIVATE_CONSTANT_278 = "revertToolTip";

   @RBEntry("Commenti...")
   public static final String PRIVATE_CONSTANT_279 = "commentsLabel";

   @RBEntry("m")
   public static final String PRIVATE_CONSTANT_280 = "commentsMnemonic";

   @RBEntry("Commenti")
   public static final String PRIVATE_CONSTANT_281 = "commentsToolTip";

   @RBEntry("Taglia")
   public static final String PRIVATE_CONSTANT_282 = "cutLabel";

   @RBEntry("T")
   public static final String PRIVATE_CONSTANT_283 = "cutMnemonic";

   @RBEntry("Ctrl+X")
   public static final String PRIVATE_CONSTANT_284 = "cutAccelerator";

   @RBEntry("Taglia")
   public static final String PRIVATE_CONSTANT_285 = "cutToolTip";

   @RBEntry("Copia")
   public static final String PRIVATE_CONSTANT_286 = "copyLabel";

   @RBEntry("C")
   public static final String PRIVATE_CONSTANT_287 = "copyMnemonic";

   @RBEntry("Ctrl+C")
   public static final String PRIVATE_CONSTANT_288 = "copyAccelerator";

   @RBEntry("Copia")
   public static final String PRIVATE_CONSTANT_289 = "copyToolTip";

   @RBEntry("Incolla")
   public static final String PRIVATE_CONSTANT_290 = "pasteLabel";

   @RBEntry("I")
   public static final String PRIVATE_CONSTANT_291 = "pasteMnemonic";

   @RBEntry("Ctrl+V")
   public static final String PRIVATE_CONSTANT_292 = "pasteAccelerator";

   @RBEntry("Incolla")
   public static final String PRIVATE_CONSTANT_293 = "pasteToolTip";

   @RBEntry("Copia negli Appunti Windchill")
   public static final String PRIVATE_CONSTANT_294 = "copyToWindchillClipboardLabel";

   @RBEntry("n")
   public static final String PRIVATE_CONSTANT_295 = "copyToWindchillClipboardMnemonic";

   @RBEntry("Copia negli Appunti Windchill")
   public static final String PRIVATE_CONSTANT_296 = "copyToWindchillClipboardToolTip";

   @RBEntry("Copia dagli Appunti Windchill")
   public static final String PRIVATE_CONSTANT_297 = "copyFromWindchillClipboardLabel";

   @RBEntry("d")
   public static final String PRIVATE_CONSTANT_298 = "copyFromWindchillClipboardMnemonic";

   @RBEntry("Copia dagli Appunti Windchill")
   public static final String PRIVATE_CONSTANT_299 = "copyFromWindchillClipboardToolTip";

   @RBEntry("Modifica più righe")
   public static final String PRIVATE_CONSTANT_300 = "editMultiRow";

   @RBEntry("Modifica più righe")
   public static final String PRIVATE_CONSTANT_301 = "rowMultiEditToolTip";

   /**
    * pasteSpecialLabel.value=Paste Special
    * pasteSpecialMnemonic.value=S
    * pasteSpecialToolTip.value=Paste Special
    * Find In Structure
    **/
   @RBEntry("Trova nella struttura...")
   public static final String PRIVATE_CONSTANT_302 = "findInStructureLabel";

   @RBEntry("s")
   public static final String PRIVATE_CONSTANT_303 = "findInStructureMnemonic";

   @RBEntry("CTRL+F")
   public static final String PRIVATE_CONSTANT_304 = "findInStructureAccelerator";

   @RBEntry("Trova")
   public static final String PRIVATE_CONSTANT_305 = "findInStructureToolTip";

   @RBEntry("Trova e seleziona")
   public static final String PRIVATE_CONSTANT_306 = "findAndSelectLabel";

   @RBEntry("Trova e seleziona")
   public static final String PRIVATE_CONSTANT_307 = "findAndSelectToolTip";

   /**
    * View Menu
    **/

   @RBEntry("Interrogazione")
   public static final String PRIVATE_CONSTANT_309 = "setQuerySpecLabel";

   @RBEntry("g")
   public static final String PRIVATE_CONSTANT_310 = "setQuerySpecMnemonic";

   @RBEntry("Espandi tutto")
   public static final String PRIVATE_CONSTANT_311 = "expandAllLabel";

   @RBEntry("E")
   public static final String PRIVATE_CONSTANT_312 = "expandAllMnemonic";

   @RBEntry("Ctrl+E")
   public static final String PRIVATE_CONSTANT_313 = "expandAccelerator";

   @RBEntry("Espande la struttura da tutti i livelli")
   public static final String PRIVATE_CONSTANT_314 = "expandAllToolTip";

   @RBEntry("Aggiorna")
   public static final String PRIVATE_CONSTANT_315 = "refreshLabel";

   @RBEntry("A")
   public static final String PRIVATE_CONSTANT_316 = "refreshMnemonic";

   @RBEntry("Ctrl+R")
   public static final String PRIVATE_CONSTANT_317 = "refreshAccelerator";

   @RBEntry("Aggiorna gli oggetti selezionati")
   public static final String PRIVATE_CONSTANT_318 = "refreshToolTip";

   @RBEntry("Informazioni")
   public static final String PRIVATE_CONSTANT_319 = "informationLabel";

   @RBEntry("I")
   public static final String PRIVATE_CONSTANT_320 = "informationMnemonic";

   @RBEntry("Informazioni")
   public static final String PRIVATE_CONSTANT_321 = "informationToolTip";

   @RBEntry("Seleziona padre")
   public static final String PRIVATE_CONSTANT_322 = "selectParentLabel";

   @RBEntry("S")
   public static final String PRIVATE_CONSTANT_323 = "selectParentMenmonic";

   @RBEntry("Seleziona padre")
   public static final String PRIVATE_CONSTANT_324 = "selectParentToolTip";

   @RBEntry("Apri in Creo View")
   public static final String PRIVATE_CONSTANT_325 = "openInProductViewLabel";

   @RBEntry("Apri in Creo View")
   public static final String PRIVATE_CONSTANT_327 = "openInProductViewToolTip";

   @RBEntry("Apri in Navigatore struttura di prodotto")
   public static final String PRIVATE_CONSTANT_328 = "launchNewPSEWindowLabel";

   @RBEntry("A")
   public static final String PRIVATE_CONSTANT_329 = "launchNewPSEWindowMnemonic";

   @RBEntry("ctrl shift A")
   public static final String PRIVATE_CONSTANT_330 = "launchNewPSEWindowAccelerator";

   @RBEntry("Apre l'oggetto selezionato in una nuova finestra del Navigatore struttura di prodotto")
   public static final String PRIVATE_CONSTANT_331 = "launchNewPSEWindowToolTip";

   @RBEntry("Nuovo Navigatore struttura di prodotto")
   public static final String PRIVATE_CONSTANT_332 = "launchNewEmptyPSEWindowLabel";

   @RBEntry("N")
   public static final String PRIVATE_CONSTANT_333 = "launchNewEmptyPSEWindowMnemonic";

   @RBEntry("Apre una nuova finestra vuota del Navigatore struttura di prodotto")
   public static final String PRIVATE_CONSTANT_334 = "launchNewEmptyPSEWindowToolTip";

   @RBEntry("Report")
   public static final String PRIVATE_CONSTANT_335 = "reportsMenuLabel";

   @RBEntry("R")
   public static final String PRIVATE_CONSTANT_336 = "reportsMenuMnemonic";

   @RBEntry("Dettagli tabella struttura...")
   public static final String PRIVATE_CONSTANT_337 = "tableDetailsLabel";

   @RBEntry("d")
   public static final String PRIVATE_CONSTANT_338 = "tableDetailsMenmonic";

   @RBEntry("Dettagli tabella struttura")
   public static final String PRIVATE_CONSTANT_339 = "tableDetailsToolTip";

   @RBEntry("Reimposta")
   public static final String PRIVATE_CONSTANT_340 = "clearMenuLabel";

   @RBEntry("m")
   public static final String PRIVATE_CONSTANT_341 = "clearMenuMnemonic";

   /**
    * View Filter
    **/
   @RBEntry("Filtro corrente...")
   public static final String PRIVATE_CONSTANT_342 = "viewExpansionCriteriaLabel";

   @RBEntry("I")
   public static final String PRIVATE_CONSTANT_343 = "viewExpansionCriteriaMnemonic";

   @RBEntry("Visualizza le informazioni sul filtro corrente")
   public static final String PRIVATE_CONSTANT_344 = "viewExpansionCriteriaToolTip";

   /**
    * Edit Filter
    **/
   @RBEntry("Modifica filtro...")
   public static final String PRIVATE_CONSTANT_345 = "setExpansionCriteriaLabel";

   @RBEntry("M")
   public static final String PRIVATE_CONSTANT_346 = "setExpansionCriteriaMnemonic";

   @RBEntry("Modifica filtro")
   public static final String PRIVATE_CONSTANT_347 = "setExpansionCriteriaToolTip";

   /**
    * Save Filter
    **/
   @RBEntry("Salva...")
   public static final String PRIVATE_CONSTANT_348 = "saveNavigationCriteriaLabel";

   @RBEntry("Salva filtro")
   public static final String PRIVATE_CONSTANT_349 = "saveNavigationCriteriaToolTip";

   @RBEntry("S")
   public static final String PRIVATE_CONSTANT_350 = "saveNavigationCriteriaMnemonic";

   /**
    * Saved Filters
    **/
   @RBEntry("Filtri salvati...")
   public static final String PRIVATE_CONSTANT_351 = "savedExpansionCriteriaMenuLabel";

   @RBEntry("S")
   public static final String PRIVATE_CONSTANT_352 = "savedExpansionCriteriaMenuMnemonic";

   /**
    * Manage Filters
    **/
   @RBEntry("Gestisci...")
   public static final String PRIVATE_CONSTANT_356 = "manageExpansionCriteriaLabel";

   @RBEntry("Gestisci filtri")
   public static final String PRIVATE_CONSTANT_357 = "manageExpansionCriteriaToolTip";

   @RBEntry("G")
   public static final String PRIVATE_CONSTANT_358 = "manageExpansionCriteriaMnemonic";

   /**
    * Show Filter List
    **/
   @RBEntry("Mostra elenco...")
   public static final String PRIVATE_CONSTANT_359 = "showECListLabel";

   @RBEntry("Mostra elenco filtri...")
   public static final String PRIVATE_CONSTANT_360 = "showECListToolTip";

   @RBEntry("o")
   public static final String PRIVATE_CONSTANT_361 = "showECListMnemonic";

   /**
    * View Query Spec Menu
    **/
   @RBEntry("Nuova interrogazione...")
   public static final String PRIVATE_CONSTANT_373 = "newQueryLabel";

   @RBEntry("N")
   public static final String PRIVATE_CONSTANT_374 = "newQueryMnemonic";

   @RBEntry("Crea ed esegue una nuova interrogazione")
   public static final String PRIVATE_CONSTANT_375 = "newQueryToolTip";

   @RBEntry("Organizza/esegui interrogazioni...")
   public static final String PRIVATE_CONSTANT_376 = "organizeQueriesLabel";

   @RBEntry("O")
   public static final String PRIVATE_CONSTANT_377 = "organizeQueriesMnemonic";

   @RBEntry("Modifica un'interrogazione esistente")
   public static final String PRIVATE_CONSTANT_378 = "organizeQueriesToolTip";

   @RBEntry("Risultati interrogazione...")
   public static final String PRIVATE_CONSTANT_379 = "queryResultsLabel";

   @RBEntry("R")
   public static final String PRIVATE_CONSTANT_380 = "queryResultsMnemonic";

   @RBEntry("Visualizza i risultati dell'ultima interrogazione")
   public static final String PRIVATE_CONSTANT_381 = "queryResultsToolTip";

   /**
    * View Reports Menu
    **/
   @RBEntry("Lista componenti multilivello")
   public static final String PRIVATE_CONSTANT_382 = "reportMultiLevelComponentsListLabel";

   @RBEntry("P")
   public static final String PRIVATE_CONSTANT_383 = "reportMultiLevelComponentsListMnemonic";

   @RBEntry("Lista componenti multilivello")
   public static final String PRIVATE_CONSTANT_384 = "reportMultiLevelComponentsListToolTip";

   @RBEntry("Distinta base a livello unico consolidata")
   public static final String PRIVATE_CONSTANT_385 = "reportSingleLevelConsolidatedBOMLabel";

   @RBEntry("O")
   public static final String PRIVATE_CONSTANT_386 = "reportSingleLevelConsolidatedBOMMnemonic";

   @RBEntry("Distinta base a livello unico consolidata")
   public static final String PRIVATE_CONSTANT_387 = "reportSingleLevelConsolidatedBOMToolTip";

   @RBEntry("Distinta base a livello unico con note")
   public static final String PRIVATE_CONSTANT_388 = "reportBOMNotesByFindNumberLabel";

   @RBEntry("N")
   public static final String PRIVATE_CONSTANT_389 = "reportBOMNotesByFindNumberMnemonic";

   @RBEntry("Distinta base a livello unico con note")
   public static final String PRIVATE_CONSTANT_390 = "reportBOMNotesByFindNumberToolTip";

   @RBEntry("Distinta base a livello unico")
   public static final String PRIVATE_CONSTANT_391 = "reportSingleLevelBOMLabel";

   @RBEntry("u")
   public static final String PRIVATE_CONSTANT_392 = "reportSingleLevelBOMMnemonic";

   @RBEntry("Distinta base a livello unico")
   public static final String PRIVATE_CONSTANT_393 = "reportSingleLevelBOMToolTip";

   @RBEntry("Distinta base multilivello")
   public static final String PRIVATE_CONSTANT_394 = "reportMultiLevelBOMLabel";

   @RBEntry("m")
   public static final String PRIVATE_CONSTANT_395 = "reportMultiLevelBOMMnemonic";

   @RBEntry("Distinta base multilivello")
   public static final String PRIVATE_CONSTANT_396 = "reportMultiLevelBOMToolTip";

   @RBEntry("Distinta base multilivello con alternative/sostituzioni")
   public static final String PRIVATE_CONSTANT_397 = "reportMultiLevelBOMWithReplacementsLabel";

   @RBEntry("R")
   public static final String PRIVATE_CONSTANT_398 = "reportMultiLevelBOMWithReplacementsMnemonic";

   @RBEntry("Distinta base multilivello con alternative/sostituzioni")
   public static final String PRIVATE_CONSTANT_399 = "reportMultiLevelBOMWithReplacementsToolTip";

   @RBEntry("Dove usato - multilivello")
   public static final String PRIVATE_CONSTANT_400 = "reportMultiLevelWhereUsedLabel";

   @RBEntry("D")
   public static final String PRIVATE_CONSTANT_401 = "reportMultiLevelWhereUsedMnemonic";

   @RBEntry("Dove usato - multilivello")
   public static final String PRIVATE_CONSTANT_402 = "reportMultiLevelWhereUsedToolTip";

   @RBEntry("Confronto di distinte base multilivello...")
   public static final String PRIVATE_CONSTANT_403 = "reportMultiLevelBOMCompareLabel";

   @RBEntry("C")
   public static final String PRIVATE_CONSTANT_404 = "reportMultiLevelBOMCompareMnemonic";

   @RBEntry("Confronto di distinte base multilivello")
   public static final String PRIVATE_CONSTANT_405 = "reportMultiLevelBOMCompareToolTip";

   @RBEntry("Logica - distinta base")
   public static final String PRIVATE_CONSTANT_406 = "reportLogicBOMLabel";

   @RBEntry("L")
   public static final String PRIVATE_CONSTANT_407 = "reportLogicBOMMnemonic";

   @RBEntry("Logica - distinta base")
   public static final String PRIVATE_CONSTANT_408 = "reportLogicBOMToolTip";

   @RBEntry("Logica - tutto")
   public static final String PRIVATE_CONSTANT_409 = "reportLogicAllLabel";

   @RBEntry("T")
   public static final String PRIVATE_CONSTANT_410 = "reportLogicAllMnemonic";

   @RBEntry("Logica - tutto")
   public static final String PRIVATE_CONSTANT_411 = "reportLogicAllToolTip";

   /**
    * No need to translate
    **/
   @RBEntry("PLM")
   public static final String PRIVATE_CONSTANT_412 = "reportPLM";

   /**
    * View Clear Menu
    **/
   @RBEntry("Tutti gli elementi selezionati")
   public static final String PRIVATE_CONSTANT_413 = "clearSelectedLabel";

   @RBEntry("s")
   public static final String PRIVATE_CONSTANT_414 = "clearSelectedMnemonic";

   @RBEntry("Azzera gli elementi selezionati")
   public static final String PRIVATE_CONSTANT_415 = "clearSelectedToolTip";

   @RBEntry("Tutte le evidenziazioni")
   public static final String PRIVATE_CONSTANT_416 = "clearHighlightedLabel";

   @RBEntry("v")
   public static final String PRIVATE_CONSTANT_417 = "clearHighlightedMnemonic";

   @RBEntry("Azzera tutte le evidenziazioni")
   public static final String PRIVATE_CONSTANT_418 = "clearHighlightedToolTip";

   @RBEntry("Tutte le icone dei risultati di ricerca")
   public static final String PRIVATE_CONSTANT_419 = "clearIconsLabel";

   @RBEntry("i")
   public static final String PRIVATE_CONSTANT_420 = "clearIconsMnemonic";

   @RBEntry("Azzera tutte le icone dei risultati di ricerca")
   public static final String PRIVATE_CONSTANT_421 = "clearIconsToolTip";

   /**
    * View Recently Used Queries
    **/
   @RBEntry("Più recente")
   public static final String PRIVATE_CONSTANT_422 = "latestQueryLabel";

   @RBEntry("Esegue la ricerca più recente")
   public static final String PRIVATE_CONSTANT_423 = "latestQueryToolTip";

   @RBEntry("Seleziona nel Navigatore")
   public static final String PRIVATE_CONSTANT_424 = "selectInExplorerLabel";

   @RBEntry("l")
   public static final String PRIVATE_CONSTANT_425 = "selectInExplorerMnemonic";

   @RBEntry("Seleziona nel Navigatore")
   public static final String PRIVATE_CONSTANT_426 = "selectInExplorerToolTip";

   @RBEntry("Navigatore struttura di prodotto")
   public static final String PRIVATE_CONSTANT_427 = "launchPSELabel";

   @RBEntry("p")
   public static final String PRIVATE_CONSTANT_428 = "launchPSEMnemonic";

   @RBEntry("Navigatore struttura di prodotto")
   public static final String PRIVATE_CONSTANT_429 = "launchPSEToolTip";

   @RBEntry("Navigatore piani di produzione")
   public static final String PRIVATE_CONSTANT_430 = "launchPPELabel";

   @RBEntry("c")
   public static final String PRIVATE_CONSTANT_431 = "launchPPEMnemonic";

   @RBEntry("Navigatore piani di produzione")
   public static final String PRIVATE_CONSTANT_432 = "launchPPEToolTip";

   @RBEntry("Navigatore risorse di fabbricazione")
   public static final String PRIVATE_CONSTANT_433 = "launchMRELabel";

   @RBEntry("r")
   public static final String PRIVATE_CONSTANT_434 = "launchMREMnemonic";

   @RBEntry("Navigatore risorse di fabbricazione")
   public static final String PRIVATE_CONSTANT_435 = "launchMREToolTip";

   @RBEntry("Navigatore standard di fabbricazione")
   public static final String PRIVATE_CONSTANT_436 = "launchMSELabel";

   @RBEntry("f")
   public static final String PRIVATE_CONSTANT_437 = "launchMSEMnemonic";

   @RBEntry("Navigatore standard di fabbricazione")
   public static final String PRIVATE_CONSTANT_438 = "launchMSEToolTip";

   /**
    * Selected Menu
    **/
   @RBEntry("Check-Out")
   public static final String PRIVATE_CONSTANT_439 = "checkOutLabel";

   @RBEntry("O")
   public static final String PRIVATE_CONSTANT_440 = "checkOutMnemonic";

   @RBEntry("Ctrl+T")
   public static final String PRIVATE_CONSTANT_441 = "checkOutAccelerator";

   @RBEntry("Effettua il Check-Out degli oggetti selezionati")
   public static final String PRIVATE_CONSTANT_442 = "checkOutToolTip";

   @RBEntry("Check-Out padre")
   public static final String PRIVATE_CONSTANT_443 = "checkOutParentLabel";

   @RBEntry("P")
   public static final String PRIVATE_CONSTANT_444 = "checkOutParentMnemonic";

   @RBEntry("ctrl P")
   public static final String PRIVATE_CONSTANT_445 = "checkOutParentAccelerator";

   @RBEntry("Check-Out padre")
   public static final String PRIVATE_CONSTANT_446 = "checkOutParentToolTip";

   @RBEntry("Check-In...")
   public static final String PRIVATE_CONSTANT_447 = "checkInLabel";

   @RBEntry("I")
   public static final String PRIVATE_CONSTANT_448 = "checkInMnemonic";

   @RBEntry("Ctrl+I")
   public static final String PRIVATE_CONSTANT_449 = "checkInAccelerator";

   @RBEntry("Effettua il Check-In degli oggetti selezionati")
   public static final String PRIVATE_CONSTANT_450 = "checkInToolTip";

   @RBEntry("Annulla Check-Out")
   public static final String PRIVATE_CONSTANT_451 = "undoCheckOutLabel";

   @RBEntry("A")
   public static final String PRIVATE_CONSTANT_452 = "undoCheckOutMnemonic";

   @RBEntry("Ctrl+U")
   public static final String PRIVATE_CONSTANT_453 = "undoCheckOutAccelerator";

   @RBEntry("Annulla il Check-Out degli oggetti selezionati")
   public static final String PRIVATE_CONSTANT_454 = "undoCheckOutToolTip";

   @RBEntry("Inserisci")
   public static final String PRIVATE_CONSTANT_455 = "insertMenuLabel";

   @RBEntry("s")
   public static final String PRIVATE_CONSTANT_456 = "insertMenuMnemonic";

   @RBEntry("Sostituisci")
   public static final String PRIVATE_CONSTANT_457 = "replaceMenuLabel";

   @RBEntry("t")
   public static final String PRIVATE_CONSTANT_458 = "replaceMenuMnemonic";

   @RBEntry("Rimuovi")
   public static final String PRIVATE_CONSTANT_459 = "removeLabel";

   @RBEntry("m")
   public static final String PRIVATE_CONSTANT_460 = "removeMnemonic";

   @RBEntry("Rimuovi")
   public static final String PRIVATE_CONSTANT_461 = "removeToolTip";

   /**
    * L10N CHANGE BEGIN: comment out duplicate resource id
    * reviseLabel.value=Revise...
    * L10N CHANGE END
    **/
   @RBEntry("r")
   public static final String PRIVATE_CONSTANT_462 = "reviseMnemonic";

   @RBEntry("Nuova revisione degli oggetti selezionati")
   public static final String PRIVATE_CONSTANT_463 = "reviseToolTip";

   @RBEntry("Ottieni ultima iterazione")
   public static final String PRIVATE_CONSTANT_464 = "getLatestIterationLabel";

   @RBEntry("l")
   public static final String PRIVATE_CONSTANT_465 = "getLatestIterationMnemonic";

   @RBEntry("Ctrl+L")
   public static final String PRIVATE_CONSTANT_466 = "getLatestIterationAccelerator";

   @RBEntry("Ottiene l'ultima iterazione degli oggetti selezionati")
   public static final String PRIVATE_CONSTANT_467 = "getLatestIterationToolTip";

   @RBEntry("Rinomina...")
   public static final String PRIVATE_CONSTANT_468 = "renameLabel";

   @RBEntry("n")
   public static final String PRIVATE_CONSTANT_469 = "renameMnemonic";

   @RBEntry("Rinomina")
   public static final String PRIVATE_CONSTANT_470 = "renameToolTip";

   @RBEntry("Nuova versione vista...")
   public static final String PRIVATE_CONSTANT_471 = "newViewVersionLabel";

   @RBEntry("v")
   public static final String PRIVATE_CONSTANT_472 = "newViewVersionMnemonic";

   @RBEntry("Nuova versione vista")
   public static final String PRIVATE_CONSTANT_473 = "newViewVersionToolTip";

   @RBEntry("Nuova versione variante...")
   public static final String PRIVATE_CONSTANT_474 = "newOneOffVersionLabel";

   @RBEntry("u")
   public static final String PRIVATE_CONSTANT_475 = "newOneOffVersionMnemonic";

   @RBEntry("Nuova versione variante")
   public static final String PRIVATE_CONSTANT_476 = "newOneOffVersionToolTip";

   @RBEntry("Duplica...")
   public static final String PRIVATE_CONSTANT_477 = "duplicateLabel";

   @RBEntry("D")
   public static final String PRIVATE_CONSTANT_478 = "duplicateMnemonic";

   @RBEntry("Duplica")
   public static final String PRIVATE_CONSTANT_479 = "duplicateToolTip";

   @RBEntry("Assegna scelte elemento")
   public static final String PRIVATE_CONSTANT_480 = "assignItemChoicesToolTip";

   @RBEntry("Assegna scelte elemento...")
   public static final String PRIVATE_CONSTANT_481 = "assignItemChoicesLabel";

   @RBEntry("c")
   public static final String PRIVATE_CONSTANT_482 = "assignItemChoicesMnemonic";

   @RBEntry("Assegna distribuzione...")
   public static final String PRIVATE_CONSTANT_483 = "assignDistributionLabel";

   @RBEntry("g")
   public static final String PRIVATE_CONSTANT_484 = "assignDistributionMnemonic";

   @RBEntry("Assegna distribuzione")
   public static final String PRIVATE_CONSTANT_485 = "assignDistributionToolTip";

   @RBEntry("Baseline")
   public static final String PRIVATE_CONSTANT_486 = "baselineMenuLabel";

   @RBEntry("B")
   public static final String PRIVATE_CONSTANT_487 = "baselineMenuMnemonic";

   @RBEntry("Elimina")
   public static final String PRIVATE_CONSTANT_488 = "deleteMenuLabel";

   @RBEntry("E")
   public static final String PRIVATE_CONSTANT_489 = "deleteMenuMnemonic";

   @RBEntry("Modifica attributi comuni...")
   public static final String PRIVATE_CONSTANT_490 = "editCommonAttributesLabel";

   @RBEntry("f")
   public static final String PRIVATE_CONSTANT_491 = "editCommonAttributesMnemonic";

   @RBEntry("Modifica attributi comuni")
   public static final String PRIVATE_CONSTANT_492 = "editCommonAttributesToolTip";

   @RBEntry("Visualizzatore")
   public static final String PRIVATE_CONSTANT_493 = "visualizationMenuLabel";

   @RBEntry("V")
   public static final String PRIVATE_CONSTANT_494 = "visualizationMenuMnemonic";

   @RBEntry("Azioni del visualizzatore")
   public static final String PRIVATE_CONSTANT_495 = "visualizationMenuToolTip";

   @RBEntry("Mostra modello")
   public static final String PRIVATE_CONSTANT_496 = "showModelLabel";

   @RBEntry("s")
   public static final String PRIVATE_CONSTANT_497 = "showModelMnemonic";

   @RBEntry("Mostra il modello nel visualizzatore")
   public static final String PRIVATE_CONSTANT_498 = "showModelToolTip";

   @RBEntry("Nascondi modello")
   public static final String PRIVATE_CONSTANT_499 = "hideModelLabel";

   @RBEntry("c")
   public static final String PRIVATE_CONSTANT_500 = "hideModelMnemonic";

   @RBEntry("Nasconde il modello nel visualizzatore")
   public static final String PRIVATE_CONSTANT_501 = "hideModelToolTip";

   /**
    * Selected Insert menu
    **/
   @RBEntry("Inserisci esistente...")
   public static final String PRIVATE_CONSTANT_502 = "insertExistingLabel";

   @RBEntry("I")
   public static final String PRIVATE_CONSTANT_503 = "insertExistingMnemonic";

   @RBEntry("Inserisci esistente")
   public static final String PRIVATE_CONSTANT_504 = "insertExistingToolTip";

   @RBEntry("Inserisci nuovo...")
   public static final String PRIVATE_CONSTANT_505 = "createAndInsertLabel";

   @RBEntry("n")
   public static final String PRIVATE_CONSTANT_506 = "createAndInsertMnemonic";

   @RBEntry("Inserisci nuovo")
   public static final String PRIVATE_CONSTANT_507 = "createAndInsertToolTip";

   @RBEntry("Inserisci creato di recente...")
   public static final String PRIVATE_CONSTANT_508 = "insertNewLabel";

   @RBEntry("r")
   public static final String PRIVATE_CONSTANT_509 = "insertNewMnemonic";

   @RBEntry("Inserisce dagli oggetti creati di recente")
   public static final String PRIVATE_CONSTANT_510 = "insertNewToolTip";

   /**
    * Selected Replace menu
    **/
   @RBEntry("Sostituisci con esistente...")
   public static final String PRIVATE_CONSTANT_511 = "replaceExistingLabel";

   @RBEntry("e")
   public static final String PRIVATE_CONSTANT_512 = "replaceExistingMnemonic";

   @RBEntry("Sostituisci con esistente")
   public static final String PRIVATE_CONSTANT_513 = "replaceExistingToolTip";

   @RBEntry("Sostituisci con nuovo...")
   public static final String PRIVATE_CONSTANT_514 = "createAndReplaceLabel";

   @RBEntry("n")
   public static final String PRIVATE_CONSTANT_515 = "createAndReplaceMnemonic";

   @RBEntry("Sostituisci con nuovo")
   public static final String PRIVATE_CONSTANT_516 = "createAndReplaceToolTip";

   @RBEntry("Sostituisci con creato di recente...")
   public static final String PRIVATE_CONSTANT_517 = "replaceNewLabel";

   @RBEntry("r")
   public static final String PRIVATE_CONSTANT_518 = "replaceNewMnemonic";

   @RBEntry("Sostituisci con creato di recente")
   public static final String PRIVATE_CONSTANT_519 = "replaceNewToolTip";

   @RBEntry("Sostituisci con alternativa/sostituzione...")
   public static final String PRIVATE_CONSTANT_520 = "replaceAlternateLabel";

   @RBEntry("a")
   public static final String PRIVATE_CONSTANT_521 = "replaceAlternateMnemonic";

   @RBEntry("Sostituisci con alternativa/sostituzione")
   public static final String PRIVATE_CONSTANT_522 = "replaceAlternateToolTip";

   /**
    * Selected Baseline menu
    **/
   @RBEntry("Aggiungi a baseline...")
   public static final String PRIVATE_CONSTANT_523 = "baselineAddToLabel";

   @RBEntry("A")
   public static final String PRIVATE_CONSTANT_524 = "baselineAddToMnemonic";

   @RBEntry("Aggiunge alla baseline")
   public static final String PRIVATE_CONSTANT_525 = "baselineAddToToolTip";

   @RBEntry("Rimuovi da baseline...")
   public static final String PRIVATE_CONSTANT_526 = "baselineRemoveFromLabel";

   @RBEntry("R")
   public static final String PRIVATE_CONSTANT_527 = "baselineRemoveFromMnemonic";

   @RBEntry("Rimuove da baseline")
   public static final String PRIVATE_CONSTANT_528 = "baselineRemoveFromToolTip";

   /**
    * baselinePopulateLabel.value=Populate Baseline...
    * baselinePopulateMnemonic.value=P
    * baselinePopulateToolTip.value=Populate baseline
    * Selected Delete menu
    **/
   @RBEntry("Parte...")
   public static final String PRIVATE_CONSTANT_529 = "deletePartLabel";

   @RBEntry("P")
   public static final String PRIVATE_CONSTANT_530 = "deletePartMnemonic";

   @RBEntry("Elimina parte")
   public static final String PRIVATE_CONSTANT_531 = "deletePartToolTip";

   @RBEntry("Gruppo di annotazioni...")
   public static final String PRIVATE_CONSTANT_532 = "deleteAnnotationLabel";

   @RBEntry("A")
   public static final String PRIVATE_CONSTANT_533 = "deleteAnnotationMnemonic";

   @RBEntry("Elimina gruppo di annotazioni")
   public static final String PRIVATE_CONSTANT_534 = "deleteAnnotationToolTip";

   /**
    * Tools Menu (Windchill PDM only)
    **/
   @RBEntry("Cronologia")
   public static final String PRIVATE_CONSTANT_535 = "toolsHistoryMenuLabel";

   @RBEntry("r")
   public static final String PRIVATE_CONSTANT_536 = "toolsHistoryMenuMnemonic";

   @RBEntry("C")
   public static final String PRIVATE_CONSTANT_537 = "searchMnemonic";

   @RBEntry("Avvia la pagina di ricerca")
   public static final String PRIVATE_CONSTANT_538 = "searchToolTip";

   @RBEntry("Navigatore Windchill...")
   public static final String PRIVATE_CONSTANT_539 = "windchillExplorerLabel";

   @RBEntry("W")
   public static final String PRIVATE_CONSTANT_540 = "windchillExplorerMnemonic";

   @RBEntry("Avvia il Navigatore Windchill")
   public static final String PRIVATE_CONSTANT_541 = "windchillExplorerToolTip";

   /**
    * Tools History Menu
    **/
   @RBEntry("Cronologia versioni...")
   public static final String PRIVATE_CONSTANT_542 = "versionHistoryLabel";

   @RBEntry("v")
   public static final String PRIVATE_CONSTANT_543 = "versionHistoryMnemonic";

   @RBEntry("Avvia la pagina Cronologia versioni")
   public static final String PRIVATE_CONSTANT_544 = "versionHistoryToolTip";

   @RBEntry("Cronologia iterazioni...")
   public static final String PRIVATE_CONSTANT_545 = "iterationHistoryLabel";

   @RBEntry("I")
   public static final String PRIVATE_CONSTANT_546 = "iterationHistoryMnemonic";

   @RBEntry("Avvia la pagina Cronologia iterazioni")
   public static final String PRIVATE_CONSTANT_547 = "iterationHistoryToolTip";

   @RBEntry("Cronologia del ciclo di vita...")
   public static final String PRIVATE_CONSTANT_548 = "lifeCycleHistoryLabel";

   @RBEntry("Cronologia del ciclo di vita...")
   public static final String PRIVATE_CONSTANT_549 = "lifeCycleHistoryMnemonic";

   @RBEntry("Avvia la pagina Cronologia del ciclo di vita")
   public static final String PRIVATE_CONSTANT_550 = "lifeCycleHistoryToolTip";

   /**
    * Lifecycle Menu (Windchill PDM only)
    **/
   @RBEntry("Invia...")
   public static final String PRIVATE_CONSTANT_551 = "submitLabel";

   @RBEntry("I")
   public static final String PRIVATE_CONSTANT_552 = "submitMnemonic";

   @RBEntry("Avvia la pagina di invio al ciclo di vita")
   public static final String PRIVATE_CONSTANT_553 = "submitToolTip";

   @RBEntry("Aggiorna team...")
   public static final String PRIVATE_CONSTANT_554 = "updateTeamLabel";

   @RBEntry("A")
   public static final String PRIVATE_CONSTANT_555 = "updateTeamMnemonic";

   @RBEntry("Aggiorna team")
   public static final String PRIVATE_CONSTANT_556 = "updateTeamToolTip";

   @RBEntry("Mostra team...")
   public static final String PRIVATE_CONSTANT_557 = "showTeamLabel";

   @RBEntry("T")
   public static final String PRIVATE_CONSTANT_558 = "showTeamMnemonic";

   @RBEntry("Mostra team")
   public static final String PRIVATE_CONSTANT_559 = "showTeamToolTip";

   /**
    * Help Menu
    **/
   @RBEntry("Argomenti della Guida")
   public static final String PRIVATE_CONSTANT_560 = "helpTopicsLabel";

   @RBEntry("G")
   public static final String PRIVATE_CONSTANT_561 = "helpTopicsMnemonic";

   @RBEntry("Argomenti della Guida")
   public static final String PRIVATE_CONSTANT_562 = "helpTopicsToolTip";

   @RBEntry("Informazioni sul Navigatore")
   public static final String PRIVATE_CONSTANT_563 = "aboutLabel";

   @RBEntry("I")
   public static final String PRIVATE_CONSTANT_564 = "aboutMnemonic";

   @RBEntry("Visualizza la finestra Informazioni sul Navigatore")
   public static final String PRIVATE_CONSTANT_565 = "aboutToolTip";

   /**
    * This is for Debug only ** Do not translate **
    **/
   @RBEntry("Dump Bucket")
   public static final String PRIVATE_CONSTANT_566 = "dumpBucketLabel";

   @RBEntry("B")
   public static final String PRIVATE_CONSTANT_567 = "dumpBucketMnemonic";

   @RBEntry("Debug Dump Bucket")
   public static final String PRIVATE_CONSTANT_568 = "dumpBucketToolTip";

   /**
    * This is for Debug only ** Do not translate **
    **/
   @RBEntry("Dump Skinny")
   public static final String PRIVATE_CONSTANT_569 = "dumpSkinnyLabel";

   @RBEntry("S")
   public static final String PRIVATE_CONSTANT_570 = "dumpSkinnyMnemonic";

   @RBEntry("Debug Dump Skinny")
   public static final String PRIVATE_CONSTANT_571 = "dumpSkinnyToolTip";

   @RBEntry("View Bucket")
   public static final String PRIVATE_CONSTANT_572 = "viewBucketLabel";

   @RBEntry("G")
   public static final String PRIVATE_CONSTANT_573 = "viewBucketMnemonic";

   @RBEntry("View Graphical Representation of the Bucket")
   public static final String PRIVATE_CONSTANT_574 = "viewBucketToolTip";

   /**
    * This is for Debug only ** Do not translate **
    **/
   @RBEntry("Utilizzo memoria")
   public static final String PRIVATE_CONSTANT_575 = "memoryUsageLabel";

   @RBEntry("M")
   public static final String PRIVATE_CONSTANT_576 = "memoryUsageMnemonic";

   @RBEntry("Visualizza l'utilizzo della memoria")
   public static final String PRIVATE_CONSTANT_577 = "memoryUsageToolTip";

   /**
    * Other
    **/
   @RBEntry("Altro")
   public static final String PRIVATE_CONSTANT_578 = "moreLabel";

   @RBEntry("Altro")
   public static final String PRIVATE_CONSTANT_579 = "moreToolTip";

   /**
    * Toolbar Groups
    **/
   @RBEntry("Visualizza")
   public static final String PRIVATE_CONSTANT_580 = "viewLabel";

   @RBEntry("Markup")
   public static final String PRIVATE_CONSTANT_581 = "markupLabel";

   @RBEntry("Misurazione")
   public static final String PRIVATE_CONSTANT_582 = "measurementLabel";

   /**
    * Annotation Validation Failure Label and Types
    **/
   @RBEntry("Risolto")
   public static final String PRIVATE_CONSTANT_583 = "resolvedLabel";

   @RBEntry("Tipo di errore")
   public static final String PRIVATE_CONSTANT_584 = "failureTypeLabel";

   @RBEntry("Sottoposto a Check-Out")
   public static final String PRIVATE_CONSTANT_585 = "checkedOutLabel";

   @RBEntry("Nuova revisione")
   public static final String PRIVATE_CONSTANT_586 = "reviseLabel";

   @RBEntry("Attributo")
   public static final String PRIVATE_CONSTANT_587 = "commonAttributeLabel";

   @RBEntry("Altro")
   public static final String PRIVATE_CONSTANT_588 = "otherLabel";

   /**
    * Help buttons
    **/
   @RBEntry("Guida")
   public static final String PRIVATE_CONSTANT_589 = "helpLabel";

   @RBEntry("G")
   public static final String PRIVATE_CONSTANT_590 = "helpMnemonic";

   @RBEntry("Guida")
   public static final String PRIVATE_CONSTANT_591 = "helpToolTip";

   /**
    * Indicator tooltips
    **/
   @RBEntry("Dispone di parti alternative")
   public static final String PRIVATE_CONSTANT_592 = "hasAlternateToolTip";

   @RBEntry("Dispone di parti di sostituzione")
   public static final String PRIVATE_CONSTANT_593 = "hasSubstituteToolTip";

   @RBEntry("Indicatore di visualizzazione")
   public static final String PRIVATE_CONSTANT_594 = "visualizationToolTip";

   @RBEntry("Modello non visualizzato")
   public static final String PRIVATE_CONSTANT_595 = "visualNotDisplayedToolTip";

   @RBEntry("Modello parzialmente visualizzato")
   public static final String PRIVATE_CONSTANT_596 = "visualPartialDisplayedToolTip";

   @RBEntry("Modello visualizzato")
   public static final String PRIVATE_CONSTANT_597 = "visualDisplayedToolTip";

   @RBEntry("Modello non visualizzato a causa di un'espansione della struttura")
   public static final String PRIVATE_CONSTANT_598 = "visualExpandToolTip";

   @RBEntry("Stato totale opzione")
   public static final String PRIVATE_CONSTANT_599 = "optionCountStatusToolTip";

   @RBEntry("Numero minimo di figli non mostrati")
   public static final String PRIVATE_CONSTANT_600 = "optionCountMinReqViolatedToolTip";

   @RBEntry("Fuori intervallo")
   public static final String PRIVATE_CONSTANT_601 = "optionCountMaxReqViolatedToolTip";

   /**
    * Variant Properties Info
    **/
   @RBEntry("Nome contesto")
   @RBComment("On the Variant Properties Info dialog set the Container or Context Name.")
   public static final String PRIVATE_CONSTANT_602 = "defaultOutputContainer";

   /**
    * Saved Results Actions
    **/
   @RBEntry("Salva risultati")
   public static final String PRIVATE_CONSTANT_603 = "savedResultsAddLabel";

   @RBEntry("Salva gli oggetti selezionati nella tabella dei risultati salvati")
   public static final String PRIVATE_CONSTANT_604 = "savedResultsAddToolTip";

   @RBEntry("Rimuovi risultati")
   public static final String PRIVATE_CONSTANT_605 = "savedResultsRemoveLabel";

   @RBEntry("Rimuove gli oggetti selezionati dalla tabella dei risultati salvati")
   public static final String PRIVATE_CONSTANT_606 = "savedResultsRemoveToolTip";

   /**
    * Where Used table actions
    **/
   @RBEntry("Espandi a tipo")
   public static final String PRIVATE_CONSTANT_607 = "expandToTypeLabel";

   @RBEntry("Espande a tipo di oggetto")
   public static final String PRIVATE_CONSTANT_608 = "expandToTypeToolTip";

   @RBEntry("Espandi per livello")
   public static final String PRIVATE_CONSTANT_609 = "expandByLevelLabel";

   @RBEntry("Espande il numero di livelli")
   public static final String PRIVATE_CONSTANT_610 = "expandByLevelToolTip";

   /**
    * Usagelink option rules changed text
    **/
   @RBEntry("Le scelte assegnate sono state modificate")
   public static final String PRIVATE_CONSTANT_611 = "usagelinkDesignationsChanged";

   /**
    * Where Used table toggles
    **/
   @RBEntry("Mostra intermedi")
   public static final String PRIVATE_CONSTANT_612 = "usedByTabFullTree";

   @RBEntry("Nascondi intermedi")
   public static final String PRIVATE_CONSTANT_613 = "usedByTabHideIntermediateTree";

   /**
    * Refresh action that refreshes the entire structure, regardless of what objects are selected.
    * This is currently used on the Where Used tab in 'Hide Intermediates' mode.
    **/
   @RBEntry("Aggiorna")
   public static final String PRIVATE_CONSTANT_614 = "refreshEntireStructureLabel";

   @RBEntry("r")
   public static final String PRIVATE_CONSTANT_615 = "refreshEntireStructureMnemonic";

   @RBEntry("Ctrl+R")
   public static final String PRIVATE_CONSTANT_616 = "refreshEntireStructureAccelerator";

   @RBEntry("Aggiorna la struttura")
   public static final String PRIVATE_CONSTANT_617 = "refreshEntireStructureToolTip";

   /**
    * ConfigurableLink tab table labels
    **/
   @RBEntry("Versione")
   public static final String PRIVATE_CONSTANT_618 = "configurableLinkTable.version";

   /**
    * Related Objects tab
    **/
   @RBEntry("Oggetti correlati")
   public static final String PRIVATE_CONSTANT_619 = "relatedObjectsTabLabel";

   /**
    * Structured Annotation Set Properties
    **/
   @RBEntry("Autore")
   public static final String PRIVATE_CONSTANT_622 = "createdByLabel";

   /**
    * Security Labels feature
    **/
   @RBEntry("Etichette di sicurezza")
   public static final String PRIVATE_CONSTANT_623 = "securityLabels";

   /**
    * Phantom Assemby label
    **/
   @RBEntry("Parte di fabbricazione fittizia o phantom")
   public static final String PRIVATE_CONSTANT_624 = "phantomLabel";

   /**
    * Build Status label
    **/
   @RBEntry("Stato creazione")
   public static final String PRIVATE_CONSTANT_625 = "buildStatusLabel";

   /**
    * Accelerator for remove action.
    */
   @RBEntry("ELIMINA")
   public static final String PRIVATE_CONSTANT_626 = "removeAccelerator";

   @RBEntry("Codice traccia")
   public static final String PRIVATE_CONSTANT_627 = "traceCodeLabel";

   @RBEntry("Unità")
   public static final String PRIVATE_CONSTANT_628 = "quantityUnitLabel";

   @RBEntry("Quantità")
   public static final String PRIVATE_CONSTANT_629 = "quantityAmountLabel";
}
