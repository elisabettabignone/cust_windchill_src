/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package config.logicrepository.xml.explorer.structureexplorer;

import wt.util.resource.*;

@RBUUID("config.logicrepository.xml.explorer.structureexplorer.ConfigurationResource")
public final class ConfigurationResource extends WTListResourceBundle {

   @RBEntry("Sourcing Context")
   public static final String PRIVATE_CONSTANT_3 = "sourcingContextLabel";

   @RBEntry("Manufacturer ID")
   public static final String PRIVATE_CONSTANT_4 = "manufacturerIdLabel";

   @RBEntry("Vendor ID")
   public static final String PRIVATE_CONSTANT_5 = "vendorIdLabel";

   /**
    *
    * Panel Property labels
    *
    **/
   @RBEntry("Find Number")
   public static final String PRIVATE_CONSTANT_6 = "findNumberLabel";

   @RBEntry("Line Number")
   public static final String PRIVATE_CONSTANT_7 = "lineNumberLabel";

   @RBEntry("Assigned Choices")
   public static final String PRIVATE_CONSTANT_8 = "usagelinkDesignationLabel";

   @RBEntry("Status")
   public static final String PRIVATE_CONSTANT_9 = "checkoutInfoStateLabel";

   @RBEntry("Organization Name")
   public static final String PRIVATE_CONSTANT_10 = "organizationNameLabel";

   @RBEntry("Version")
   public static final String PRIVATE_CONSTANT_13 = "versionNameLabel";

   @RBEntry("Object Type")
   public static final String PRIVATE_CONSTANT_14 = "objectTypeLabel";

   @RBEntry("Search Criteria")
   public static final String PRIVATE_CONSTANT_15 = "searchCritieriaPanelLabel";

   @RBEntry("Search...")
   public static final String PRIVATE_CONSTANT_16 = "searchLabel";

   @RBEntry("Serial Number")
   public static final String PRIVATE_CONSTANT_17 = "serialNumberValueLabel";

   @RBEntry("Parent")
   public static final String PRIVATE_CONSTANT_18 = "parentName";

   @RBEntry("Reference Designator Range")
   public static final String PRIVATE_CONSTANT_19 = "referenceDesignatorRangeLabel";

   @RBEntry("Reference Designator")
   public static final String PRIVATE_CONSTANT_20 = "referenceDesignatorNameOrRangeLabel";

   @RBEntry("Search On")
   public static final String PRIVATE_CONSTANT_21 = "typeListLabel";

   @RBEntry("Created By")
   public static final String PRIVATE_CONSTANT_22 = "searchCriteriaCreatorNameLabel";

   @RBEntry("Created By")
   public static final String PRIVATE_CONSTANT_23 = "searchCriteriaCreatorNameToolTip";

   /**
    *
    * Tabbed Pane Labels
    *
    **/
   @RBEntry("BOM Structure")
   public static final String PRIVATE_CONSTANT_35 = "BOMTreeTabLabel";

   @RBEntry("Occurrence Structure")
   public static final String PRIVATE_CONSTANT_36 = "OccurrencedTreeTabLabel";

   @RBEntry("Edit")
   public static final String PRIVATE_CONSTANT_37 = "editModeLabel";

   @RBEntry("Draft")
   public static final String PRIVATE_CONSTANT_38 = "draftModeLabel";

   @RBEntry("Annotate")
   public static final String PRIVATE_CONSTANT_39 = "annotateModeLabel";

   @RBEntry("Read Only")
   public static final String PRIVATE_CONSTANT_40 = "readModeLabel";

   /**
    *
    * Tabbed Pane - Information
    *
    **/
   @RBEntry("Information")
   public static final String PRIVATE_CONSTANT_41 = "propertiesTabLabel";

   @RBEntry("Identity")
   public static final String PRIVATE_CONSTANT_42 = "identityTabLabel";

   @RBEntry("Object Attributes")
   public static final String PRIVATE_CONSTANT_43 = "objAttrTabLabel";

   @RBEntry("Usage Attributes")
   public static final String PRIVATE_CONSTANT_44 = "usageAttrTabLabel";

   @RBEntry("Classification")
   public static final String PRIVATE_CONSTANT_45 = "classificationTabLabel";

   @RBEntry("Assigned Basic Expression Choices")
   public static final String PRIVATE_CONSTANT_46 = "optionsTabLabel";

   @RBEntry("Add Attribute")
   public static final String PRIVATE_CONSTANT_47 = "addValueLabel";

   @RBEntry("Add Attribute")
   public static final String PRIVATE_CONSTANT_48 = "addValueToolTip";

   @RBEntry("Classify")
   public static final String PRIVATE_CONSTANT_49 = "classifyLabel";

   @RBEntry("Classify")
   public static final String PRIVATE_CONSTANT_50 = "classifyToolTip";

   @RBEntry("Add Adhoc Attribute")
   public static final String PRIVATE_CONSTANT_51 = "addAdhocAttributeLabel";

   @RBEntry("Add Adhoc Attribute")
   public static final String PRIVATE_CONSTANT_52 = "addAdhocAttributeToolTip";

   @RBEntry("Remove Classification")
   public static final String PRIVATE_CONSTANT_53 = "removeClassificationLabel";

   @RBEntry("Remove Classification")
   public static final String PRIVATE_CONSTANT_54 = "removeClassificationToolTip";

   @RBEntry("Remove Attribute")
   public static final String PRIVATE_CONSTANT_55 = "removeValueLabel";

   @RBEntry("Remove Attribute")
   public static final String PRIVATE_CONSTANT_56 = "removeValueToolTip";

   @RBEntry("Check Out Parent")
   public static final String PRIVATE_CONSTANT_57 = "checkoutParentLabel";

   @RBEntry("Check Out Parent")
   public static final String PRIVATE_CONSTANT_58 = "checkoutParentToolTip";

   @RBEntry("Check In Parent")
   public static final String PRIVATE_CONSTANT_59 = "checkinParentLabel";

   @RBEntry("Check In Parent")
   public static final String PRIVATE_CONSTANT_60 = "checkinParentToolTip";

   /**
    *
    * Tabbed Pane - Uses
    *
    **/
   @RBEntry("Uses")
   public static final String PRIVATE_CONSTANT_61 = "usesTabLabel";

   @RBEntry("BOM")
   public static final String PRIVATE_CONSTANT_62 = "usesTabBOMtable";

   @RBEntry("Unfiltered BOM")
   public static final String PRIVATE_CONSTANT_63 = "usesTabUnFilteredBOMtable";

   @RBEntry("Occurrences")
   public static final String PRIVATE_CONSTANT_64 = "usesTabOccurrencesTable";

   @RBEntry("Edit table")
   public static final String PRIVATE_CONSTANT_65 = "editCellsLabel";

   @RBEntry("Edit table values")
   public static final String PRIVATE_CONSTANT_66 = "editCellsToolTip";

   /**
    * Uses Table
    **/
   @RBEntry("Visual Location")
   public static final String PRIVATE_CONSTANT_67 = "visualLocationLabel";

   /**
    *
    * Tabbed Pane - View
    *
    **/
   @RBEntry("View")
   public static final String PRIVATE_CONSTANT_68 = "viewTabLabel";

   /**
    *
    * Tabbed Pane - UsedBy (now called Where Used)
    *
    **/
   @RBEntry("Where Used")
   public static final String PRIVATE_CONSTANT_69 = "usedByTabLabel";

   /**
    *
    * Tabbed Pane - association constraint tree tab labels
    *
    **/
   @RBEntry("Open Related Object")
   public static final String PRIVATE_CONSTANT_70 = "openAssociationConstraintLinkLabel";

   @RBEntry("Open Related Object")
   public static final String PRIVATE_CONSTANT_71 = "openAssociationConstraintLinkToolTip";

   @RBEntry("Add Link")
   public static final String PRIVATE_CONSTANT_72 = "addAssociationConstraintLinkLabel";

   @RBEntry("Add Link")
   public static final String PRIVATE_CONSTANT_73 = "addAssociationConstraintLinkToolTip";

   @RBEntry("Remove Link")
   public static final String PRIVATE_CONSTANT_74 = "removeAssociationConstraintLinkLabel";

   @RBEntry("Remove Link")
   public static final String PRIVATE_CONSTANT_75 = "removeAssociationConstraintLinkToolTip";

   /**
    *
    * Tabbed Pane - Documentation
    *
    **/
   @RBEntry("Documentation")
   public static final String PRIVATE_CONSTANT_76 = "documentationTabLabel";

   @RBEntry("Reference Linked")
   public static final String PRIVATE_CONSTANT_77 = "referenceLinkDocTableLabel";

   @RBEntry("Describe Linked")
   public static final String PRIVATE_CONSTANT_78 = "describeLinkDocTableLabel";

   @RBEntry("Associated Documents")
   public static final String PRIVATE_CONSTANT_79 = "associatedDocumentsTableTitle";

   @RBEntry("Describing Documents")
   public static final String PRIVATE_CONSTANT_80 = "describingDocumentsTableTitle";

   @RBEntry("Referenced Documents")
   public static final String PRIVATE_CONSTANT_81 = "referencedDocumentsTableTitle";

   @RBEntry("Version")
   public static final String PRIVATE_CONSTANT_82 = "documentsTabTableVersionLabel";

   @RBEntry("Iteration")
   public static final String PRIVATE_CONSTANT_83 = "documentsTabTableIterationLabel";

   @RBEntry("Create and associate a described by document")
   public static final String PRIVATE_CONSTANT_84 = "createDesDocumentLabel";

   @RBEntry("Create and associate a described by document")
   public static final String PRIVATE_CONSTANT_85 = "createDesDocumentToolTip";

   @RBEntry("Create and associate a references document")
   public static final String PRIVATE_CONSTANT_86 = "createRefDocumentLabel";

   @RBEntry("Create and associate a references document")
   public static final String PRIVATE_CONSTANT_87 = "createRefDocumentToolTip";

   @RBEntry("Add associated document")
   public static final String PRIVATE_CONSTANT_88 = "addDocumentLabel";

   @RBEntry("Remove associated document")
   public static final String PRIVATE_CONSTANT_89 = "removeDocumentLabel";

   @RBEntry("View Associated Document")
   public static final String PRIVATE_CONSTANT_90 = "viewDocumentLabel";

   @RBEntry("Add associated document")
   public static final String PRIVATE_CONSTANT_91 = "addDocumentToolTip";

   @RBEntry("Remove associated document")
   public static final String PRIVATE_CONSTANT_92 = "removeDocumentToolTip";

   @RBEntry("View Associated Document")
   public static final String PRIVATE_CONSTANT_93 = "viewDocumentToolTip";

   /**
    *
    * Tabbed Pane - Replacements
    *
    **/
   @RBEntry("Replacements")
   public static final String PRIVATE_CONSTANT_94 = "replacementsTabLabel";

   @RBEntry("Alternate in any Assembly")
   public static final String PRIVATE_CONSTANT_95 = "alternatesReplacementsTableTitle";

   @RBEntry("Substitute in current Assembly ")
   public static final String PRIVATE_CONSTANT_96 = "substitutesReplacementsTableTitle";

   /**
    * Alternates Table
    **/
   @RBEntry("Alternates")
   public static final String PRIVATE_CONSTANT_97 = "alternatesLabel";

   @RBEntry("Add alternate part")
   public static final String PRIVATE_CONSTANT_98 = "addAlternateLabel";

   @RBEntry("Remove alternate part")
   public static final String PRIVATE_CONSTANT_99 = "removeAlternateLabel";

   @RBEntry("Open alternate part in PSE")
   public static final String PRIVATE_CONSTANT_100 = "openAlternateLabel";

   @RBEntry("Add alternate part")
   public static final String PRIVATE_CONSTANT_101 = "addAlternateToolTip";

   @RBEntry("Remove alternate part")
   public static final String PRIVATE_CONSTANT_102 = "removeAlternateToolTip";

   @RBEntry("Open alternate part in PSE")
   public static final String PRIVATE_CONSTANT_103 = "openAlternateToolTip";

   /**
    * Substitute Table
    **/
   @RBEntry("Substitutes")
   public static final String PRIVATE_CONSTANT_104 = "substitutesLabel";

   @RBEntry("Add substitute part")
   public static final String PRIVATE_CONSTANT_105 = "addSubstituteLabel";

   @RBEntry("Remove substitute part")
   public static final String PRIVATE_CONSTANT_106 = "removeSubstituteLabel";

   @RBEntry("Open substitute part in PSE")
   public static final String PRIVATE_CONSTANT_107 = "openSubstituteLabel";

   @RBEntry("Add substitute part")
   public static final String PRIVATE_CONSTANT_108 = "addSubstituteToolTip";

   @RBEntry("Remove substitute part")
   public static final String PRIVATE_CONSTANT_109 = "removeSubstituteToolTip";

   @RBEntry("Open substitute part in PSE")
   public static final String PRIVATE_CONSTANT_110 = "openSubstituteToolTip";

   /**
    *
    * Tabbed Pane - Tree
    *
    **/
   @RBEntry("Tree")
   public static final String PRIVATE_CONSTANT_111 = "treeTabLabel";

   /**
    *
    * Tabbed Pane - AXL
    *
    **/
   @RBEntry("AML - AVL")
   public static final String PRIVATE_CONSTANT_112 = "axlTabLabel";

   @RBEntry("Vendor Parts")
   public static final String PRIVATE_CONSTANT_113 = "avlTableTitle";

   @RBEntry("Manufacturer Parts ")
   public static final String PRIVATE_CONSTANT_114 = "amlTableTitle";

   /**
    * Manufacuturer Table
    **/
   @RBEntry("Add manufacturer part")
   public static final String PRIVATE_CONSTANT_115 = "addManufacuturerLabel";

   @RBEntry("Remove manufacturer part")
   public static final String PRIVATE_CONSTANT_116 = "removeManufacuturerLabel";

   @RBEntry("Open manufacturer part in PSE")
   public static final String PRIVATE_CONSTANT_117 = "openManufacuturerLabel";

   @RBEntry("Add manufacturer part")
   public static final String PRIVATE_CONSTANT_118 = "addManufacuturerToolTip";

   @RBEntry("Remove manufacturer part")
   public static final String PRIVATE_CONSTANT_119 = "removeManufacuturerToolTip";

   @RBEntry("Open manufacturer part in PSE")
   public static final String PRIVATE_CONSTANT_120 = "openManufacuturerToolTip";

   /**
    * Vendor Table
    **/
   @RBEntry("Add vendor part")
   public static final String PRIVATE_CONSTANT_121 = "addVendorLabel";

   @RBEntry("Remove vendor part")
   public static final String PRIVATE_CONSTANT_122 = "removeVendorLabel";

   @RBEntry("Open vendor part in PSE")
   public static final String PRIVATE_CONSTANT_123 = "openVendorLabel";

   @RBEntry("Add vendor part")
   public static final String PRIVATE_CONSTANT_124 = "addVendorToolTip";

   @RBEntry("Remove vendor part")
   public static final String PRIVATE_CONSTANT_125 = "removeVendorToolTip";

   @RBEntry("Open vendor part in PSE")
   public static final String PRIVATE_CONSTANT_126 = "openVendorToolTip";

   /**
    * Query Table
    **/
   @RBEntry("Execute Saved Query")
   public static final String PRIVATE_CONSTANT_127 = "exectuteQueryLabel";

   @RBEntry("Create Saved Query")
   public static final String PRIVATE_CONSTANT_128 = "createQueryLabel";

   @RBEntry("Change Saved Query")
   public static final String PRIVATE_CONSTANT_129 = "editQueryLabel";

   @RBEntry("Copy Query")
   public static final String PRIVATE_CONSTANT_130 = "copyQueryLabel";

   @RBEntry("Delete Entry")
   public static final String PRIVATE_CONSTANT_131 = "deleteQueryLabel";

   @RBEntry("Execute Saved Query")
   public static final String PRIVATE_CONSTANT_132 = "exectuteQueryToolTip";

   @RBEntry("Create Saved Query")
   public static final String PRIVATE_CONSTANT_133 = "createQueryToolTip";

   @RBEntry("Change Saved Query")
   public static final String PRIVATE_CONSTANT_134 = "editQueryToolTip";

   @RBEntry("Copy Saved Query")
   public static final String PRIVATE_CONSTANT_135 = "copyQueryToolTip";

   @RBEntry("Delete Saved Query")
   public static final String PRIVATE_CONSTANT_136 = "deleteQueryToolTip";

   @RBEntry("Add Group Access")
   public static final String PRIVATE_CONSTANT_136_1 = "addGroupAccessQueryToolTip";

   /**
    * Query Results Table
    **/
   @RBEntry("Append selected entry")
   public static final String PRIVATE_CONSTANT_137 = "queryResultAppendLabel";

   @RBEntry("Display icon in structure")
   public static final String PRIVATE_CONSTANT_138 = "queryResultIconLabel";

   @RBEntry("Select color for row")
   public static final String PRIVATE_CONSTANT_139 = "queryResultColorLabel";

   @RBEntry("Remove selected entry from the query results")
   public static final String PRIVATE_CONSTANT_140 = "queryResultRemoveLabel";

   @RBEntry("Launch property page")
   public static final String PRIVATE_CONSTANT_141 = "queryResultPropertiesLabel";

   @RBEntry("Open in new window")
   public static final String PRIVATE_CONSTANT_142 = "queryResultLaunchToolTip";

   @RBEntry("Copy")
   public static final String PRIVATE_CONSTANT_143 = "queryResultCopyLabel";

   @RBEntry("Name")
   public static final String PRIVATE_CONSTANT_144 = "nameLabel";

   @RBEntry("Number")
   public static final String PRIVATE_CONSTANT_145 = "numberLabel";

   @RBEntry("Append selected entry")
   public static final String PRIVATE_CONSTANT_146 = "queryResultAppendToolTip";

   @RBEntry("Display icon in structure")
   public static final String PRIVATE_CONSTANT_147 = "queryResultIconToolTip";

   @RBEntry("Select color for row")
   public static final String PRIVATE_CONSTANT_148 = "queryResultColorToolTip";

   @RBEntry("Remove selected entry from the query results")
   public static final String PRIVATE_CONSTANT_149 = "queryResultRemoveToolTip";

   @RBEntry("Launch property page")
   public static final String PRIVATE_CONSTANT_150 = "queryResultPropertiesToolTip";

   @RBEntry("Copy")
   public static final String PRIVATE_CONSTANT_151 = "queryResultCopyToolTip";

   /**
    * Query Panel
    **/
   @RBEntry("Save query")
   public static final String PRIVATE_CONSTANT_152 = "saveQueryLabel";

   @RBEntry("Query name")
   public static final String PRIVATE_CONSTANT_153 = "queryNameLabel";

   @RBEntry("Description")
   public static final String PRIVATE_CONSTANT_154 = "queryDescriptionLabel";

   @RBEntry("Access")
   public static final String PRIVATE_CONSTANT_155 = "queryAccessLabel";

   /**
    * Query Group Table
    **/
   @RBEntry("Group Name")
   public static final String PRIVATE_CONSTANT_156 = "queryGroupNameLabel";

   @RBEntry("Domain Name")
   public static final String PRIVATE_CONSTANT_157 = "queryDomainNameLabel";

   /**
    * Query Labels
    **/
   @RBEntry("Node Matches Query")
   @RBComment("This is used to indicate that this node matches the query that was just executed.")
   public static final String PRIVATE_CONSTANT_158 = "nodeMatchesQuery";

   /**
    *
    * GPS Parameters Tab
    *
    **/
   @RBEntry("Parameters")
   public static final String PRIVATE_CONSTANT_159 = "gpsParametersTabLabel";

   @RBEntry("Parameters")
   public static final String PRIVATE_CONSTANT_160 = "gpsParametersTableTitle";

   @RBEntry("Create Parameter")
   public static final String PRIVATE_CONSTANT_161 = "createParameterLabel";

   @RBEntry("Create a New Parameter")
   public static final String PRIVATE_CONSTANT_162 = "createParameterToolTip";

   @RBEntry("Edit Parameter")
   public static final String PRIVATE_CONSTANT_163 = "editParameterLabel";

   @RBEntry("Edit Selected Parameter")
   public static final String PRIVATE_CONSTANT_164 = "editParameterToolTip";

   @RBEntry("Remove Parameter")
   public static final String PRIVATE_CONSTANT_165 = "removeParameterLabel";

   @RBEntry("Remove Selected Parameter")
   public static final String PRIVATE_CONSTANT_166 = "removeParameterToolTip";

   @RBEntry("Create Parameter Group")
   public static final String PRIVATE_CONSTANT_167 = "createParameterGroupLabel";

   @RBEntry("Create Parameter Group")
   public static final String PRIVATE_CONSTANT_168 = "createParameterGroupToolTip";

   @RBEntry("Insert Page Break")
   public static final String PRIVATE_CONSTANT_169 = "addPageBreakLabel";

   @RBEntry("Insert Page Break Below")
   public static final String PRIVATE_CONSTANT_170 = "addPageBreakToolTip";

   @RBEntry("Insert Child Resolution")
   public static final String PRIVATE_CONSTANT_171 = "addChildResolutionLabel";

   @RBEntry("Insert Child Resolution Below")
   public static final String PRIVATE_CONSTANT_172 = "addChildResolutionToolTip";

   /**
    * Create parameter Clerk labels
    **/
   @RBEntry("User Interface")
   public static final String PRIVATE_CONSTANT_173 = "parameterUITabLabel";

   @RBEntry("Access")
   public static final String PRIVATE_CONSTANT_174 = "parameterAccessTabLabel";

   @RBEntry("Equivalency")
   public static final String PRIVATE_CONSTANT_175 = "parameterEquivalencyTabLabel";

   @RBEntry("Constraint")
   public static final String PRIVATE_CONSTANT_176 = "parameterConstraintTabLabel";

   @RBEntry("Information")
   public static final String PRIVATE_CONSTANT_177 = "parameterPropertiesTabLabel";

   @RBEntry("None")
   public static final String PRIVATE_CONSTANT_178 = "parameterConstraintNoneLabel";

   @RBEntry("Do Not Constraint Parameter")
   public static final String PRIVATE_CONSTANT_179 = "parameterConstraintNoneToolTip";

   @RBEntry("Range")
   public static final String PRIVATE_CONSTANT_180 = "parameterConstraintRangeLabel";

   @RBEntry("Constrain Parameter to a Range")
   public static final String PRIVATE_CONSTANT_181 = "parameterConstraintRangeToolTip";

   @RBEntry("List")
   public static final String PRIVATE_CONSTANT_182 = "parameterConstraintListLabel";

   @RBEntry("Constrain Parameter to a List of Values")
   public static final String PRIVATE_CONSTANT_183 = "parameterConstraintListToolTip";

   @RBEntry("Dynamic List")
   public static final String PRIVATE_CONSTANT_184 = "parameterConstraintDynamicListLabel";

   @RBEntry("Constrain Parameter Based on Output of a Class Method")
   public static final String PRIVATE_CONSTANT_185 = "parameterConstraintDynamicListToolTip";

   @RBEntry("Parameter")
   public static final String PRIVATE_CONSTANT_186 = "parameterInfoParameterSectionLabel";

   @RBEntry("Definition")
   public static final String PRIVATE_CONSTANT_187 = "parameterInfoDefinitionSectionLabel";

   @RBEntry("Value")
   public static final String PRIVATE_CONSTANT_188 = "parameterInfoValueSectionLabel";

   /**
    *
    * GPS Constraints Tab
    *
    **/
   @RBEntry("Constraints")
   public static final String PRIVATE_CONSTANT_189 = "gpsConstraintsTabLabel";

   @RBEntry("Constraints")
   public static final String PRIVATE_CONSTANT_190 = "gpsConstraintsTableTitle";

   @RBEntry("Select Parameters")
   public static final String PRIVATE_CONSTANT_191 = "selectParameterLabel";

   @RBEntry("Select Parameters")
   public static final String PRIVATE_CONSTANT_192 = "selectParameterToolTip";

   @RBEntry("Generate Cases")
   public static final String PRIVATE_CONSTANT_193 = "generateCasesLabel";

   @RBEntry("Generate Cases for Enumerated Parameters")
   public static final String PRIVATE_CONSTANT_194 = "generateCasesToolTip";

   /**
    *
    * Menu labels
    *
    **/
   @RBEntry("File")
   public static final String PRIVATE_CONSTANT_195 = "fileMenuLabel";

   @RBEntry("F")
   public static final String PRIVATE_CONSTANT_196 = "fileMenuMnemonic";

   @RBEntry("Edit")
   public static final String PRIVATE_CONSTANT_197 = "editMenuLabel";

   @RBEntry("E")
   public static final String PRIVATE_CONSTANT_198 = "editMenuMnemonic";

   @RBEntry("View")
   public static final String PRIVATE_CONSTANT_199 = "viewMenuLabel";

   @RBEntry("V")
   public static final String PRIVATE_CONSTANT_200 = "viewMnemonic";

   @RBEntry("Selected")
   public static final String PRIVATE_CONSTANT_201 = "selectedMenuLabel";

   @RBEntry("S")
   public static final String PRIVATE_CONSTANT_202 = "selectedMenuMnemonic";

   @RBEntry("Tools")
   public static final String PRIVATE_CONSTANT_203 = "toolsMenuLabel";

   @RBEntry("T")
   public static final String PRIVATE_CONSTANT_204 = "toolsMenuMnemonic";

   @RBEntry("Life Cycle")
   public static final String PRIVATE_CONSTANT_205 = "lifeCycleMenuLabel";

   @RBEntry("L")
   public static final String PRIVATE_CONSTANT_206 = "lifeCycleMenuMnemonic";

   @RBEntry("Help")
   public static final String PRIVATE_CONSTANT_207 = "helpMenuLabel";

   @RBEntry("H")
   public static final String PRIVATE_CONSTANT_208 = "helpMenuMnemonic";

   /**
    * File Menu
    **/
   @RBEntry("New")
   public static final String PRIVATE_CONSTANT_209 = "newMenuLabel";

   @RBEntry("N")
   public static final String PRIVATE_CONSTANT_210 = "newMenuMnemonic";

   @RBEntry("Open")
   public static final String PRIVATE_CONSTANT_211 = "openMenuLabel";

   @RBEntry("O")
   public static final String PRIVATE_CONSTANT_212 = "openMenuMnemonic";

   @RBEntry("Close")
   public static final String PRIVATE_CONSTANT_213 = "closeLabel";

   @RBEntry("C")
   public static final String PRIVATE_CONSTANT_214 = "closeMenuMnemonic";

   @RBEntry("Close active structure")
   public static final String PRIVATE_CONSTANT_215 = "closeToolTip";

   @RBEntry("Save")
   public static final String PRIVATE_CONSTANT_216 = "saveLabel";

   @RBEntry("S")
   public static final String PRIVATE_CONSTANT_217 = "saveMnemonic";

   @RBEntry("ctrl S")
   public static final String PRIVATE_CONSTANT_218 = "saveAccelerator";

   @RBEntry("Save")
   public static final String PRIVATE_CONSTANT_219 = "saveToolTip";

   @RBEntry("Save As...")
   public static final String PRIVATE_CONSTANT_220 = "saveAsLabel";

   @RBEntry("A")
   public static final String PRIVATE_CONSTANT_221 = "saveAsMnemonic";

   @RBEntry("Save As Annotation Set")
   public static final String PRIVATE_CONSTANT_222 = "saveAsToolTip";

   @RBEntry("Validate Changes")
   public static final String PRIVATE_CONSTANT_223 = "validateAnnotationLabel";

   @RBEntry("V")
   public static final String PRIVATE_CONSTANT_224 = "validateAnnotationMnemonic";

   @RBEntry("Validate Draft/Annotation Set")
   public static final String PRIVATE_CONSTANT_225 = "validateAnnotationToolTip";

   @RBEntry("Apply")
   public static final String PRIVATE_CONSTANT_226 = "applyAnnotationLabel";

   @RBEntry("y")
   public static final String PRIVATE_CONSTANT_227 = "applyAnnotationMnemonic";

   @RBEntry("Apply Annotation Set")
   public static final String PRIVATE_CONSTANT_228 = "applyAnnotationToolTip";

   @RBEntry("Properties...")
   public static final String PRIVATE_CONSTANT_229 = "propertiesLabel";

   @RBEntry("P")
   public static final String PRIVATE_CONSTANT_230 = "propertiesMnemonic";

   @RBEntry("Properties")
   public static final String PRIVATE_CONSTANT_231 = "propertiesToolTip";

   @RBEntry("Preferences...")
   public static final String PRIVATE_CONSTANT_232 = "preferencesLabel";

   @RBEntry("f")
   public static final String PRIVATE_CONSTANT_233 = "preferencesMnemonic";

   @RBEntry("Preferences")
   public static final String PRIVATE_CONSTANT_234 = "preferencesToolTip";

   @RBEntry("Exit")
   public static final String PRIVATE_CONSTANT_235 = "exitLabel";

   @RBEntry("x")
   public static final String PRIVATE_CONSTANT_236 = "exitMnemonic";

   @RBEntry("Exit ")
   public static final String PRIVATE_CONSTANT_237 = "exitToolTip";

   /**
    * File New menu
    **/
   @RBEntry("Part...")
   public static final String PRIVATE_CONSTANT_238 = "newPartLabel";

   @RBEntry("P")
   public static final String PRIVATE_CONSTANT_239 = "newPartMnemonic";

   @RBEntry("ctrl N")
   public static final String PRIVATE_CONSTANT_240 = "newPartAccelerator";

   @RBEntry("New Part")
   public static final String PRIVATE_CONSTANT_241 = "newPartToolTip";

   @RBEntry("New Part")
   public static final String PRIVATE_CONSTANT_242 = "newPartActionLabel";

   @RBEntry("Advanced Configurable Part From Part...")
   public static final String PRIVATE_CONSTANT_243 = "newGenericPartLabel";

   @RBEntry("New Advanced Configurable Part from Part")
   public static final String PRIVATE_CONSTANT_244 = "newGenericPartToolTip";

   @RBEntry("New Advanced Configurable Part From Part")
   public static final String PRIVATE_CONSTANT_245 = "newGenericPartActionLabel";

   @RBEntry("Annotation Set...")
   public static final String PRIVATE_CONSTANT_246 = "newAnnotationLabel";

   @RBEntry("A")
   public static final String PRIVATE_CONSTANT_247 = "newAnnotationMnemonic";

   @RBEntry("ctrl A")
   public static final String PRIVATE_CONSTANT_248 = "newAnnotaionAccelerator";

   @RBEntry("New Annotation Set")
   public static final String PRIVATE_CONSTANT_249 = "newAnnotationToolTip";

   @RBEntry("New Annotation Set")
   public static final String PRIVATE_CONSTANT_250 = "newAnnotationActionLabel";

   /**
    * File Open menu
    **/
   @RBEntry("Part...")
   public static final String PRIVATE_CONSTANT_251 = "openPartLabel";

   @RBEntry("P")
   public static final String PRIVATE_CONSTANT_252 = "openPartMnemonic";

   @RBEntry("Open Part")
   public static final String PRIVATE_CONSTANT_253 = "openPartToolTip";

   @RBEntry("Open Part")
   public static final String PRIVATE_CONSTANT_254 = "openPartActionLabel";

   @RBEntry("Annotation Set...")
   public static final String PRIVATE_CONSTANT_255 = "openAnnotationLabel";

   @RBEntry("A")
   public static final String PRIVATE_CONSTANT_256 = "openAnnotationMnemonic";

   @RBEntry("Open existing annotation set")
   public static final String PRIVATE_CONSTANT_257 = "openAnnotationToolTip";

   @RBEntry("Open Annotation Set")
   public static final String PRIVATE_CONSTANT_258 = "openAnnotationActionLabel";

   @RBEntry("Open Structure...")
   public static final String PRIVATE_CONSTANT_259 = "openStructureLabel";

   @RBEntry("S")
   public static final String PRIVATE_CONSTANT_260 = "openStructureMnemonic";

   @RBEntry("Open Structure")
   public static final String PRIVATE_CONSTANT_261 = "openStructureToolTip";

   /**
    * Edit Menu
    **/
   @RBEntry("Draft Mode")
   public static final String PRIVATE_CONSTANT_262 = "draftLabel";

   @RBEntry("D")
   public static final String PRIVATE_CONSTANT_263 = "draftMnemonic";

   @RBEntry("Switch to draft mode")
   public static final String PRIVATE_CONSTANT_264 = "draftToolTip";

   @RBEntry("Edit Mode")
   public static final String PRIVATE_CONSTANT_265 = "editLabel";

   @RBEntry("E")
   public static final String PRIVATE_CONSTANT_266 = "editMnemonic";

   @RBEntry("Switch to edit mode")
   public static final String PRIVATE_CONSTANT_267 = "editToolTip";

   @RBEntry("Undo")
   public static final String PRIVATE_CONSTANT_268 = "undoLabel";

   @RBEntry("U")
   public static final String PRIVATE_CONSTANT_269 = "undoMnemonic";

   @RBEntry("Undo")
   public static final String PRIVATE_CONSTANT_270 = "undoToolTip";

   @RBEntry("ctrl Z")
   public static final String PRIVATE_CONSTANT_271 = "undoAccelerator";

   @RBEntry("Redo")
   public static final String PRIVATE_CONSTANT_272 = "redoLabel";

   @RBEntry("R")
   public static final String PRIVATE_CONSTANT_273 = "redoMnemonic";

   @RBEntry("Redo")
   public static final String PRIVATE_CONSTANT_274 = "redoToolTip";

   @RBEntry("ctrl Y")
   public static final String PRIVATE_CONSTANT_275 = "redoAccelerator";

   @RBEntry("Revert")
   public static final String PRIVATE_CONSTANT_276 = "revertLabel";

   @RBEntry("v")
   public static final String PRIVATE_CONSTANT_277 = "revertMnemonic";

   @RBEntry("Revert an annotation")
   public static final String PRIVATE_CONSTANT_278 = "revertToolTip";

   @RBEntry("Comments...")
   public static final String PRIVATE_CONSTANT_279 = "commentsLabel";

   @RBEntry("m")
   public static final String PRIVATE_CONSTANT_280 = "commentsMnemonic";

   @RBEntry("Comments")
   public static final String PRIVATE_CONSTANT_281 = "commentsToolTip";

   @RBEntry("Cut")
   public static final String PRIVATE_CONSTANT_282 = "cutLabel";

   @RBEntry("t")
   public static final String PRIVATE_CONSTANT_283 = "cutMnemonic";

   @RBEntry("ctrl X")
   public static final String PRIVATE_CONSTANT_284 = "cutAccelerator";

   @RBEntry("Cut")
   public static final String PRIVATE_CONSTANT_285 = "cutToolTip";

   @RBEntry("Copy")
   public static final String PRIVATE_CONSTANT_286 = "copyLabel";

   @RBEntry("C")
   public static final String PRIVATE_CONSTANT_287 = "copyMnemonic";

   @RBEntry("ctrl C")
   public static final String PRIVATE_CONSTANT_288 = "copyAccelerator";

   @RBEntry("Copy")
   public static final String PRIVATE_CONSTANT_289 = "copyToolTip";

   @RBEntry("Paste")
   public static final String PRIVATE_CONSTANT_290 = "pasteLabel";

   @RBEntry("P")
   public static final String PRIVATE_CONSTANT_291 = "pasteMnemonic";

   @RBEntry("ctrl V")
   public static final String PRIVATE_CONSTANT_292 = "pasteAccelerator";

   @RBEntry("Paste")
   public static final String PRIVATE_CONSTANT_293 = "pasteToolTip";

   @RBEntry("Copy to Windchill Clipboard")
   public static final String PRIVATE_CONSTANT_294 = "copyToWindchillClipboardLabel";

   @RBEntry("t")
   public static final String PRIVATE_CONSTANT_295 = "copyToWindchillClipboardMnemonic";

   @RBEntry("Copy to Windchill Clipboard")
   public static final String PRIVATE_CONSTANT_296 = "copyToWindchillClipboardToolTip";

   @RBEntry("Copy from Windchill Clipboard")
   public static final String PRIVATE_CONSTANT_297 = "copyFromWindchillClipboardLabel";

   @RBEntry("f")
   public static final String PRIVATE_CONSTANT_298 = "copyFromWindchillClipboardMnemonic";

   @RBEntry("Copy from Windchill Clipboard")
   public static final String PRIVATE_CONSTANT_299 = "copyFromWindchillClipboardToolTip";

   @RBEntry("Edit multiple rows")
   public static final String PRIVATE_CONSTANT_300 = "editMultiRow";

   @RBEntry("Edit multiple rows")
   public static final String PRIVATE_CONSTANT_301 = "rowMultiEditToolTip";

   /**
    * pasteSpecialLabel.value=Paste Special
    * pasteSpecialMnemonic.value=S
    * pasteSpecialToolTip.value=Paste Special
    * Find In Structure
    **/
   @RBEntry("Find in Structure...")
   public static final String PRIVATE_CONSTANT_302 = "findInStructureLabel";

   @RBEntry("F")
   public static final String PRIVATE_CONSTANT_303 = "findInStructureMnemonic";

   @RBEntry("ctrl F")
   public static final String PRIVATE_CONSTANT_304 = "findInStructureAccelerator";

   @RBEntry("Find")
   public static final String PRIVATE_CONSTANT_305 = "findInStructureToolTip";

   @RBEntry("Find and select")
   public static final String PRIVATE_CONSTANT_306 = "findAndSelectLabel";

   @RBEntry("Find and select")
   public static final String PRIVATE_CONSTANT_307 = "findAndSelectToolTip";

   /**
    * View Menu
    **/

   @RBEntry("Query")
   public static final String PRIVATE_CONSTANT_309 = "setQuerySpecLabel";

   @RBEntry("Q")
   public static final String PRIVATE_CONSTANT_310 = "setQuerySpecMnemonic";

   @RBEntry("Expand All")
   public static final String PRIVATE_CONSTANT_311 = "expandAllLabel";

   @RBEntry("E")
   public static final String PRIVATE_CONSTANT_312 = "expandAllMnemonic";

   @RBEntry("ctrl E")
   public static final String PRIVATE_CONSTANT_313 = "expandAccelerator";

   @RBEntry("Expand structure from all levels")
   public static final String PRIVATE_CONSTANT_314 = "expandAllToolTip";

   @RBEntry("Refresh")
   public static final String PRIVATE_CONSTANT_315 = "refreshLabel";

   @RBEntry("R")
   public static final String PRIVATE_CONSTANT_316 = "refreshMnemonic";

   @RBEntry("ctrl R")
   public static final String PRIVATE_CONSTANT_317 = "refreshAccelerator";

   @RBEntry("Refresh selected objects")
   public static final String PRIVATE_CONSTANT_318 = "refreshToolTip";

   @RBEntry("Information")
   public static final String PRIVATE_CONSTANT_319 = "informationLabel";

   @RBEntry("I")
   public static final String PRIVATE_CONSTANT_320 = "informationMnemonic";

   @RBEntry("Information Page")
   public static final String PRIVATE_CONSTANT_321 = "informationToolTip";

   @RBEntry("Select Parent")
   public static final String PRIVATE_CONSTANT_322 = "selectParentLabel";

   @RBEntry("S")
   public static final String PRIVATE_CONSTANT_323 = "selectParentMenmonic";

   @RBEntry("Select Parent ")
   public static final String PRIVATE_CONSTANT_324 = "selectParentToolTip";

   @RBEntry("Open in Creo View")
   public static final String PRIVATE_CONSTANT_325 = "openInProductViewLabel";

   @RBEntry("Open in Creo View")
   public static final String PRIVATE_CONSTANT_327 = "openInProductViewToolTip";

   @RBEntry("Open in Product Structure Explorer")
   public static final String PRIVATE_CONSTANT_328 = "launchNewPSEWindowLabel";

   @RBEntry("O")
   public static final String PRIVATE_CONSTANT_329 = "launchNewPSEWindowMnemonic";

   @RBEntry("ctrl shift O")
   public static final String PRIVATE_CONSTANT_330 = "launchNewPSEWindowAccelerator";

   @RBEntry("Launch selected object in new Explorer window")
   public static final String PRIVATE_CONSTANT_331 = "launchNewPSEWindowToolTip";

   @RBEntry("New Product Structure Explorer")
   public static final String PRIVATE_CONSTANT_332 = "launchNewEmptyPSEWindowLabel";

   @RBEntry("N")
   public static final String PRIVATE_CONSTANT_333 = "launchNewEmptyPSEWindowMnemonic";

   @RBEntry("Launch new Empty PSE Window")
   public static final String PRIVATE_CONSTANT_334 = "launchNewEmptyPSEWindowToolTip";

   @RBEntry("Reports")
   public static final String PRIVATE_CONSTANT_335 = "reportsMenuLabel";

   @RBEntry("p")
   public static final String PRIVATE_CONSTANT_336 = "reportsMenuMnemonic";

   @RBEntry("Structure Table Details...")
   public static final String PRIVATE_CONSTANT_337 = "tableDetailsLabel";

   @RBEntry("d")
   public static final String PRIVATE_CONSTANT_338 = "tableDetailsMenmonic";

   @RBEntry("Structure Table Details ")
   public static final String PRIVATE_CONSTANT_339 = "tableDetailsToolTip";

   @RBEntry("Clear")
   public static final String PRIVATE_CONSTANT_340 = "clearMenuLabel";

   @RBEntry("c")
   public static final String PRIVATE_CONSTANT_341 = "clearMenuMnemonic";

   /**
    * View Filter
    **/
   @RBEntry("Current Filter...")
   public static final String PRIVATE_CONSTANT_342 = "viewExpansionCriteriaLabel";

   @RBEntry("I")
   public static final String PRIVATE_CONSTANT_343 = "viewExpansionCriteriaMnemonic";

   @RBEntry("View the Current Filter Information")
   public static final String PRIVATE_CONSTANT_344 = "viewExpansionCriteriaToolTip";

   /**
    * Edit Filter
    **/
   @RBEntry("Edit Filter...")
   public static final String PRIVATE_CONSTANT_345 = "setExpansionCriteriaLabel";

   @RBEntry("E")
   public static final String PRIVATE_CONSTANT_346 = "setExpansionCriteriaMnemonic";

   @RBEntry("Edit Filter")
   public static final String PRIVATE_CONSTANT_347 = "setExpansionCriteriaToolTip";

   /**
    * Save Filter
    **/
   @RBEntry("Save...")
   public static final String PRIVATE_CONSTANT_348 = "saveNavigationCriteriaLabel";

   @RBEntry("Save Filter")
   public static final String PRIVATE_CONSTANT_349 = "saveNavigationCriteriaToolTip";

   @RBEntry("S")
   public static final String PRIVATE_CONSTANT_350 = "saveNavigationCriteriaMnemonic";

   /**
    * Saved Filters
    **/
   @RBEntry("Saved Filters...")
   public static final String PRIVATE_CONSTANT_351 = "savedExpansionCriteriaMenuLabel";

   @RBEntry("X")
   public static final String PRIVATE_CONSTANT_352 = "savedExpansionCriteriaMenuMnemonic";

   /**
    * Manage Filters
    **/
   @RBEntry("Manage...")
   public static final String PRIVATE_CONSTANT_356 = "manageExpansionCriteriaLabel";

   @RBEntry("Manage Filters")
   public static final String PRIVATE_CONSTANT_357 = "manageExpansionCriteriaToolTip";

   @RBEntry("M")
   public static final String PRIVATE_CONSTANT_358 = "manageExpansionCriteriaMnemonic";

   /**
    * Show Filter List
    **/
   @RBEntry("Show List...")
   public static final String PRIVATE_CONSTANT_359 = "showECListLabel";

   @RBEntry("Show Filter List...")
   public static final String PRIVATE_CONSTANT_360 = "showECListToolTip";

   @RBEntry("L")
   public static final String PRIVATE_CONSTANT_361 = "showECListMnemonic";

   /**
    * View Query Spec Menu
    **/
   @RBEntry("New Query...")
   public static final String PRIVATE_CONSTANT_373 = "newQueryLabel";

   @RBEntry("N")
   public static final String PRIVATE_CONSTANT_374 = "newQueryMnemonic";

   @RBEntry("Creates and executes a new query")
   public static final String PRIVATE_CONSTANT_375 = "newQueryToolTip";

   @RBEntry("Organize/Execute Queries...")
   public static final String PRIVATE_CONSTANT_376 = "organizeQueriesLabel";

   @RBEntry("O")
   public static final String PRIVATE_CONSTANT_377 = "organizeQueriesMnemonic";

   @RBEntry("Edits an existing named query")
   public static final String PRIVATE_CONSTANT_378 = "organizeQueriesToolTip";

   @RBEntry("Query Results...")
   public static final String PRIVATE_CONSTANT_379 = "queryResultsLabel";

   @RBEntry("R")
   public static final String PRIVATE_CONSTANT_380 = "queryResultsMnemonic";

   @RBEntry("Displays the results of the last query")
   public static final String PRIVATE_CONSTANT_381 = "queryResultsToolTip";

   /**
    * View Reports Menu
    **/
   @RBEntry("Multi-Level Components List")
   public static final String PRIVATE_CONSTANT_382 = "reportMultiLevelComponentsListLabel";

   @RBEntry("P")
   public static final String PRIVATE_CONSTANT_383 = "reportMultiLevelComponentsListMnemonic";

   @RBEntry("Multi-Level Components List")
   public static final String PRIVATE_CONSTANT_384 = "reportMultiLevelComponentsListToolTip";

   @RBEntry("Single-Level Consolidated BOM")
   public static final String PRIVATE_CONSTANT_385 = "reportSingleLevelConsolidatedBOMLabel";

   @RBEntry("O")
   public static final String PRIVATE_CONSTANT_386 = "reportSingleLevelConsolidatedBOMMnemonic";

   @RBEntry("Single-Level Consolidated BOM")
   public static final String PRIVATE_CONSTANT_387 = "reportSingleLevelConsolidatedBOMToolTip";

   @RBEntry("Single-Level BOM with Notes")
   public static final String PRIVATE_CONSTANT_388 = "reportBOMNotesByFindNumberLabel";

   @RBEntry("N")
   public static final String PRIVATE_CONSTANT_389 = "reportBOMNotesByFindNumberMnemonic";

   @RBEntry("Single-Level BOM with Notes")
   public static final String PRIVATE_CONSTANT_390 = "reportBOMNotesByFindNumberToolTip";

   @RBEntry("Single-Level BOM")
   public static final String PRIVATE_CONSTANT_391 = "reportSingleLevelBOMLabel";

   @RBEntry("S")
   public static final String PRIVATE_CONSTANT_392 = "reportSingleLevelBOMMnemonic";

   @RBEntry("Single-Level BOM")
   public static final String PRIVATE_CONSTANT_393 = "reportSingleLevelBOMToolTip";

   @RBEntry("Multi-Level BOM")
   public static final String PRIVATE_CONSTANT_394 = "reportMultiLevelBOMLabel";

   @RBEntry("M")
   public static final String PRIVATE_CONSTANT_395 = "reportMultiLevelBOMMnemonic";

   @RBEntry("Multi-Level BOM")
   public static final String PRIVATE_CONSTANT_396 = "reportMultiLevelBOMToolTip";

   @RBEntry("Multi-Level BOM with Replacements")
   public static final String PRIVATE_CONSTANT_397 = "reportMultiLevelBOMWithReplacementsLabel";

   @RBEntry("R")
   public static final String PRIVATE_CONSTANT_398 = "reportMultiLevelBOMWithReplacementsMnemonic";

   @RBEntry("Multi-Level BOM with Replacements")
   public static final String PRIVATE_CONSTANT_399 = "reportMultiLevelBOMWithReplacementsToolTip";

   @RBEntry("Multi-Level Where Used")
   public static final String PRIVATE_CONSTANT_400 = "reportMultiLevelWhereUsedLabel";

   @RBEntry("W")
   public static final String PRIVATE_CONSTANT_401 = "reportMultiLevelWhereUsedMnemonic";

   @RBEntry("Multi-Level Where Used")
   public static final String PRIVATE_CONSTANT_402 = "reportMultiLevelWhereUsedToolTip";

   @RBEntry("Multi-Level BOM Compare...")
   public static final String PRIVATE_CONSTANT_403 = "reportMultiLevelBOMCompareLabel";

   @RBEntry("C")
   public static final String PRIVATE_CONSTANT_404 = "reportMultiLevelBOMCompareMnemonic";

   @RBEntry("Multi-Level BOM Compare")
   public static final String PRIVATE_CONSTANT_405 = "reportMultiLevelBOMCompareToolTip";

   @RBEntry("Logic BOM")
   public static final String PRIVATE_CONSTANT_406 = "reportLogicBOMLabel";

   @RBEntry("L")
   public static final String PRIVATE_CONSTANT_407 = "reportLogicBOMMnemonic";

   @RBEntry("Logic BOM")
   public static final String PRIVATE_CONSTANT_408 = "reportLogicBOMToolTip";

   @RBEntry("Logic All")
   public static final String PRIVATE_CONSTANT_409 = "reportLogicAllLabel";

   @RBEntry("A")
   public static final String PRIVATE_CONSTANT_410 = "reportLogicAllMnemonic";

   @RBEntry("Logic All")
   public static final String PRIVATE_CONSTANT_411 = "reportLogicAllToolTip";

   /**
    * No need to translate
    **/
   @RBEntry("PLM")
   public static final String PRIVATE_CONSTANT_412 = "reportPLM";

   /**
    * View Clear Menu
    **/
   @RBEntry("All Selected")
   public static final String PRIVATE_CONSTANT_413 = "clearSelectedLabel";

   @RBEntry("S")
   public static final String PRIVATE_CONSTANT_414 = "clearSelectedMnemonic";

   @RBEntry("Clear all selected")
   public static final String PRIVATE_CONSTANT_415 = "clearSelectedToolTip";

   @RBEntry("All Highlights")
   public static final String PRIVATE_CONSTANT_416 = "clearHighlightedLabel";

   @RBEntry("H")
   public static final String PRIVATE_CONSTANT_417 = "clearHighlightedMnemonic";

   @RBEntry("Clear all highlights")
   public static final String PRIVATE_CONSTANT_418 = "clearHighlightedToolTip";

   @RBEntry("All Search Result Icons")
   public static final String PRIVATE_CONSTANT_419 = "clearIconsLabel";

   @RBEntry("I")
   public static final String PRIVATE_CONSTANT_420 = "clearIconsMnemonic";

   @RBEntry("Clear all search result icons")
   public static final String PRIVATE_CONSTANT_421 = "clearIconsToolTip";

   /**
    * View Recently Used Queries
    **/
   @RBEntry("Latest")
   public static final String PRIVATE_CONSTANT_422 = "latestQueryLabel";

   @RBEntry("Perform last search")
   public static final String PRIVATE_CONSTANT_423 = "latestQueryToolTip";

   @RBEntry("Select In Explorer")
   public static final String PRIVATE_CONSTANT_424 = "selectInExplorerLabel";

   @RBEntry("l")
   public static final String PRIVATE_CONSTANT_425 = "selectInExplorerMnemonic";

   @RBEntry("Select In Explorer")
   public static final String PRIVATE_CONSTANT_426 = "selectInExplorerToolTip";

   @RBEntry("Product Structure Explorer")
   public static final String PRIVATE_CONSTANT_427 = "launchPSELabel";

   @RBEntry("P")
   public static final String PRIVATE_CONSTANT_428 = "launchPSEMnemonic";

   @RBEntry("Product Structure Explorer")
   public static final String PRIVATE_CONSTANT_429 = "launchPSEToolTip";

   @RBEntry("Process Plan Explorer")
   public static final String PRIVATE_CONSTANT_430 = "launchPPELabel";

   @RBEntry("c")
   public static final String PRIVATE_CONSTANT_431 = "launchPPEMnemonic";

   @RBEntry("Process Plan Explorer")
   public static final String PRIVATE_CONSTANT_432 = "launchPPEToolTip";

   @RBEntry("Manufacturing Resource Explorer")
   public static final String PRIVATE_CONSTANT_433 = "launchMRELabel";

   @RBEntry("M")
   public static final String PRIVATE_CONSTANT_434 = "launchMREMnemonic";

   @RBEntry("Manufacturing Resource Explorer")
   public static final String PRIVATE_CONSTANT_435 = "launchMREToolTip";

   @RBEntry("Manufacturing Standards Explorer")
   public static final String PRIVATE_CONSTANT_436 = "launchMSELabel";

   @RBEntry("f")
   public static final String PRIVATE_CONSTANT_437 = "launchMSEMnemonic";

   @RBEntry("Manufacturing Standards Explorer")
   public static final String PRIVATE_CONSTANT_438 = "launchMSEToolTip";

   /**
    * Selected Menu
    **/
   @RBEntry("Check Out")
   public static final String PRIVATE_CONSTANT_439 = "checkOutLabel";

   @RBEntry("O")
   public static final String PRIVATE_CONSTANT_440 = "checkOutMnemonic";

   @RBEntry("ctrl T")
   public static final String PRIVATE_CONSTANT_441 = "checkOutAccelerator";

   @RBEntry("Check out selected objects")
   public static final String PRIVATE_CONSTANT_442 = "checkOutToolTip";

   @RBEntry("Check Out Parent")
   public static final String PRIVATE_CONSTANT_443 = "checkOutParentLabel";

   @RBEntry("P")
   public static final String PRIVATE_CONSTANT_444 = "checkOutParentMnemonic";

   @RBEntry("ctrl P")
   public static final String PRIVATE_CONSTANT_445 = "checkOutParentAccelerator";

   @RBEntry("Check out parent")
   public static final String PRIVATE_CONSTANT_446 = "checkOutParentToolTip";

   @RBEntry("Check In...")
   public static final String PRIVATE_CONSTANT_447 = "checkInLabel";

   @RBEntry("I")
   public static final String PRIVATE_CONSTANT_448 = "checkInMnemonic";

   @RBEntry("ctrl I")
   public static final String PRIVATE_CONSTANT_449 = "checkInAccelerator";

   @RBEntry("Check in selected objects")
   public static final String PRIVATE_CONSTANT_450 = "checkInToolTip";

   @RBEntry("Undo Check Out")
   public static final String PRIVATE_CONSTANT_451 = "undoCheckOutLabel";

   @RBEntry("U")
   public static final String PRIVATE_CONSTANT_452 = "undoCheckOutMnemonic";

   @RBEntry("ctrl U")
   public static final String PRIVATE_CONSTANT_453 = "undoCheckOutAccelerator";

   @RBEntry("Undo check out of selected objects")
   public static final String PRIVATE_CONSTANT_454 = "undoCheckOutToolTip";

   @RBEntry("Insert")
   public static final String PRIVATE_CONSTANT_455 = "insertMenuLabel";

   @RBEntry("s")
   public static final String PRIVATE_CONSTANT_456 = "insertMenuMnemonic";

   @RBEntry("Replace")
   public static final String PRIVATE_CONSTANT_457 = "replaceMenuLabel";

   @RBEntry("p")
   public static final String PRIVATE_CONSTANT_458 = "replaceMenuMnemonic";

   @RBEntry("Remove")
   public static final String PRIVATE_CONSTANT_459 = "removeLabel";

   @RBEntry("m")
   public static final String PRIVATE_CONSTANT_460 = "removeMnemonic";

   @RBEntry("Remove")
   public static final String PRIVATE_CONSTANT_461 = "removeToolTip";

   /**
    * L10N CHANGE BEGIN: comment out duplicate resource id
    * reviseLabel.value=Revise...
    * L10N CHANGE END
    **/
   @RBEntry("R")
   public static final String PRIVATE_CONSTANT_462 = "reviseMnemonic";

   @RBEntry("Revise selected objects")
   public static final String PRIVATE_CONSTANT_463 = "reviseToolTip";

   @RBEntry("Get Latest Iteration")
   public static final String PRIVATE_CONSTANT_464 = "getLatestIterationLabel";

   @RBEntry("L")
   public static final String PRIVATE_CONSTANT_465 = "getLatestIterationMnemonic";

   @RBEntry("ctrl L")
   public static final String PRIVATE_CONSTANT_466 = "getLatestIterationAccelerator";

   @RBEntry("Get the latest iteration of selected objects")
   public static final String PRIVATE_CONSTANT_467 = "getLatestIterationToolTip";

   @RBEntry("Rename...")
   public static final String PRIVATE_CONSTANT_468 = "renameLabel";

   @RBEntry("n")
   public static final String PRIVATE_CONSTANT_469 = "renameMnemonic";

   @RBEntry("Rename")
   public static final String PRIVATE_CONSTANT_470 = "renameToolTip";

   @RBEntry("New View Version...")
   public static final String PRIVATE_CONSTANT_471 = "newViewVersionLabel";

   @RBEntry("V")
   public static final String PRIVATE_CONSTANT_472 = "newViewVersionMnemonic";

   @RBEntry("New View Version")
   public static final String PRIVATE_CONSTANT_473 = "newViewVersionToolTip";

   @RBEntry("New One-off Version...")
   public static final String PRIVATE_CONSTANT_474 = "newOneOffVersionLabel";

   @RBEntry("f")
   public static final String PRIVATE_CONSTANT_475 = "newOneOffVersionMnemonic";

   @RBEntry("New One-off Version")
   public static final String PRIVATE_CONSTANT_476 = "newOneOffVersionToolTip";

   @RBEntry("Duplicate...")
   public static final String PRIVATE_CONSTANT_477 = "duplicateLabel";

   @RBEntry("a")
   public static final String PRIVATE_CONSTANT_478 = "duplicateMnemonic";

   @RBEntry("Duplicate")
   public static final String PRIVATE_CONSTANT_479 = "duplicateToolTip";

   @RBEntry("Assign Item Choices")
   public static final String PRIVATE_CONSTANT_480 = "assignItemChoicesToolTip";

   @RBEntry("Assign Item Choices...")
   public static final String PRIVATE_CONSTANT_481 = "assignItemChoicesLabel";

   @RBEntry("C")
   public static final String PRIVATE_CONSTANT_482 = "assignItemChoicesMnemonic";

   @RBEntry("Assign Distribution...")
   public static final String PRIVATE_CONSTANT_483 = "assignDistributionLabel";

   @RBEntry("A")
   public static final String PRIVATE_CONSTANT_484 = "assignDistributionMnemonic";

   @RBEntry("Assign Distribution")
   public static final String PRIVATE_CONSTANT_485 = "assignDistributionToolTip";

   @RBEntry("Baseline")
   public static final String PRIVATE_CONSTANT_486 = "baselineMenuLabel";

   @RBEntry("B")
   public static final String PRIVATE_CONSTANT_487 = "baselineMenuMnemonic";

   @RBEntry("Delete")
   public static final String PRIVATE_CONSTANT_488 = "deleteMenuLabel";

   @RBEntry("D")
   public static final String PRIVATE_CONSTANT_489 = "deleteMenuMnemonic";

   @RBEntry("Edit Common Attributes...")
   public static final String PRIVATE_CONSTANT_490 = "editCommonAttributesLabel";

   @RBEntry("e")
   public static final String PRIVATE_CONSTANT_491 = "editCommonAttributesMnemonic";

   @RBEntry("Edit Common Attributes")
   public static final String PRIVATE_CONSTANT_492 = "editCommonAttributesToolTip";

   @RBEntry("Viewer")
   public static final String PRIVATE_CONSTANT_493 = "visualizationMenuLabel";

   @RBEntry("V")
   public static final String PRIVATE_CONSTANT_494 = "visualizationMenuMnemonic";

   @RBEntry("Viewer Actions")
   public static final String PRIVATE_CONSTANT_495 = "visualizationMenuToolTip";

   @RBEntry("Show Model")
   public static final String PRIVATE_CONSTANT_496 = "showModelLabel";

   @RBEntry("S")
   public static final String PRIVATE_CONSTANT_497 = "showModelMnemonic";

   @RBEntry("Show Model in Viewer")
   public static final String PRIVATE_CONSTANT_498 = "showModelToolTip";

   @RBEntry("Hide Model")
   public static final String PRIVATE_CONSTANT_499 = "hideModelLabel";

   @RBEntry("H")
   public static final String PRIVATE_CONSTANT_500 = "hideModelMnemonic";

   @RBEntry("Hide Model in Viewer")
   public static final String PRIVATE_CONSTANT_501 = "hideModelToolTip";

   /**
    * Selected Insert menu
    **/
   @RBEntry("Insert Existing...")
   public static final String PRIVATE_CONSTANT_502 = "insertExistingLabel";

   @RBEntry("E")
   public static final String PRIVATE_CONSTANT_503 = "insertExistingMnemonic";

   @RBEntry("Insert Existing")
   public static final String PRIVATE_CONSTANT_504 = "insertExistingToolTip";

   @RBEntry("Insert New...")
   public static final String PRIVATE_CONSTANT_505 = "createAndInsertLabel";

   @RBEntry("N")
   public static final String PRIVATE_CONSTANT_506 = "createAndInsertMnemonic";

   @RBEntry("Insert New")
   public static final String PRIVATE_CONSTANT_507 = "createAndInsertToolTip";

   @RBEntry("Insert Recently Created...")
   public static final String PRIVATE_CONSTANT_508 = "insertNewLabel";

   @RBEntry("R")
   public static final String PRIVATE_CONSTANT_509 = "insertNewMnemonic";

   @RBEntry("Insert From Recently Created")
   public static final String PRIVATE_CONSTANT_510 = "insertNewToolTip";

   /**
    * Selected Replace menu
    **/
   @RBEntry("Replace with Existing...")
   public static final String PRIVATE_CONSTANT_511 = "replaceExistingLabel";

   @RBEntry("E")
   public static final String PRIVATE_CONSTANT_512 = "replaceExistingMnemonic";

   @RBEntry("Replace with Existing")
   public static final String PRIVATE_CONSTANT_513 = "replaceExistingToolTip";

   @RBEntry("Replace with New...")
   public static final String PRIVATE_CONSTANT_514 = "createAndReplaceLabel";

   @RBEntry("N")
   public static final String PRIVATE_CONSTANT_515 = "createAndReplaceMnemonic";

   @RBEntry("Replace with New")
   public static final String PRIVATE_CONSTANT_516 = "createAndReplaceToolTip";

   @RBEntry("Replace with Recently Created...")
   public static final String PRIVATE_CONSTANT_517 = "replaceNewLabel";

   @RBEntry("R")
   public static final String PRIVATE_CONSTANT_518 = "replaceNewMnemonic";

   @RBEntry("Replace with Recently Created")
   public static final String PRIVATE_CONSTANT_519 = "replaceNewToolTip";

   @RBEntry("Replace with Alternate/Substitute...")
   public static final String PRIVATE_CONSTANT_520 = "replaceAlternateLabel";

   @RBEntry("A")
   public static final String PRIVATE_CONSTANT_521 = "replaceAlternateMnemonic";

   @RBEntry("Replace with Alternate/Substitute")
   public static final String PRIVATE_CONSTANT_522 = "replaceAlternateToolTip";

   /**
    * Selected Baseline menu
    **/
   @RBEntry("Add to Baseline...")
   public static final String PRIVATE_CONSTANT_523 = "baselineAddToLabel";

   @RBEntry("A")
   public static final String PRIVATE_CONSTANT_524 = "baselineAddToMnemonic";

   @RBEntry("Add to baseline")
   public static final String PRIVATE_CONSTANT_525 = "baselineAddToToolTip";

   @RBEntry("Remove from Baseline...")
   public static final String PRIVATE_CONSTANT_526 = "baselineRemoveFromLabel";

   @RBEntry("R")
   public static final String PRIVATE_CONSTANT_527 = "baselineRemoveFromMnemonic";

   @RBEntry("Remove from baseline")
   public static final String PRIVATE_CONSTANT_528 = "baselineRemoveFromToolTip";

   /**
    * baselinePopulateLabel.value=Populate Baseline...
    * baselinePopulateMnemonic.value=P
    * baselinePopulateToolTip.value=Populate baseline
    * Selected Delete menu
    **/
   @RBEntry("Part...")
   public static final String PRIVATE_CONSTANT_529 = "deletePartLabel";

   @RBEntry("P")
   public static final String PRIVATE_CONSTANT_530 = "deletePartMnemonic";

   @RBEntry("Delete Part")
   public static final String PRIVATE_CONSTANT_531 = "deletePartToolTip";

   @RBEntry("Annotation Set...")
   public static final String PRIVATE_CONSTANT_532 = "deleteAnnotationLabel";

   @RBEntry("A")
   public static final String PRIVATE_CONSTANT_533 = "deleteAnnotationMnemonic";

   @RBEntry("Delete Annotation Set")
   public static final String PRIVATE_CONSTANT_534 = "deleteAnnotationToolTip";

   /**
    * Tools Menu (Windchill PDM only)
    **/
   @RBEntry("History")
   public static final String PRIVATE_CONSTANT_535 = "toolsHistoryMenuLabel";

   @RBEntry("H")
   public static final String PRIVATE_CONSTANT_536 = "toolsHistoryMenuMnemonic";

   @RBEntry("S")
   public static final String PRIVATE_CONSTANT_537 = "searchMnemonic";

   @RBEntry("Launch Search page")
   public static final String PRIVATE_CONSTANT_538 = "searchToolTip";

   @RBEntry("Windchill Explorer...")
   public static final String PRIVATE_CONSTANT_539 = "windchillExplorerLabel";

   @RBEntry("W")
   public static final String PRIVATE_CONSTANT_540 = "windchillExplorerMnemonic";

   @RBEntry("Launch Windchill Explorer")
   public static final String PRIVATE_CONSTANT_541 = "windchillExplorerToolTip";

   /**
    * Tools History Menu
    **/
   @RBEntry("Version History...")
   public static final String PRIVATE_CONSTANT_542 = "versionHistoryLabel";

   @RBEntry("V")
   public static final String PRIVATE_CONSTANT_543 = "versionHistoryMnemonic";

   @RBEntry("Launch Version History page")
   public static final String PRIVATE_CONSTANT_544 = "versionHistoryToolTip";

   @RBEntry("Iteration History...")
   public static final String PRIVATE_CONSTANT_545 = "iterationHistoryLabel";

   @RBEntry("I")
   public static final String PRIVATE_CONSTANT_546 = "iterationHistoryMnemonic";

   @RBEntry("Launch Iteration History page")
   public static final String PRIVATE_CONSTANT_547 = "iterationHistoryToolTip";

   @RBEntry("Life Cycle History...")
   public static final String PRIVATE_CONSTANT_548 = "lifeCycleHistoryLabel";

   @RBEntry("Life Cycle History...")
   public static final String PRIVATE_CONSTANT_549 = "lifeCycleHistoryMnemonic";

   @RBEntry("Launch Life Cycle History page")
   public static final String PRIVATE_CONSTANT_550 = "lifeCycleHistoryToolTip";

   /**
    * Lifecycle Menu (Windchill PDM only)
    **/
   @RBEntry("Submit..")
   public static final String PRIVATE_CONSTANT_551 = "submitLabel";

   @RBEntry("S")
   public static final String PRIVATE_CONSTANT_552 = "submitMnemonic";

   @RBEntry("Launch Life Cycle Submit page")
   public static final String PRIVATE_CONSTANT_553 = "submitToolTip";

   @RBEntry("Update Team...")
   public static final String PRIVATE_CONSTANT_554 = "updateTeamLabel";

   @RBEntry("U")
   public static final String PRIVATE_CONSTANT_555 = "updateTeamMnemonic";

   @RBEntry("Update Team")
   public static final String PRIVATE_CONSTANT_556 = "updateTeamToolTip";

   @RBEntry("Show Team...")
   public static final String PRIVATE_CONSTANT_557 = "showTeamLabel";

   @RBEntry("T")
   public static final String PRIVATE_CONSTANT_558 = "showTeamMnemonic";

   @RBEntry("Show Team")
   public static final String PRIVATE_CONSTANT_559 = "showTeamToolTip";

   /**
    * Help Menu
    **/
   @RBEntry("Help Topics")
   public static final String PRIVATE_CONSTANT_560 = "helpTopicsLabel";

   @RBEntry("H")
   public static final String PRIVATE_CONSTANT_561 = "helpTopicsMnemonic";

   @RBEntry("Help Topics")
   public static final String PRIVATE_CONSTANT_562 = "helpTopicsToolTip";

   @RBEntry("About Explorer")
   public static final String PRIVATE_CONSTANT_563 = "aboutLabel";

   @RBEntry("A")
   public static final String PRIVATE_CONSTANT_564 = "aboutMnemonic";

   @RBEntry("Displays the About Explorer dialog")
   public static final String PRIVATE_CONSTANT_565 = "aboutToolTip";

   /**
    * This is for Debug only ** Do not translate **
    **/
   @RBEntry("Dump Bucket")
   public static final String PRIVATE_CONSTANT_566 = "dumpBucketLabel";

   @RBEntry("B")
   public static final String PRIVATE_CONSTANT_567 = "dumpBucketMnemonic";

   @RBEntry("Debug Dump Bucket")
   public static final String PRIVATE_CONSTANT_568 = "dumpBucketToolTip";

   /**
    * This is for Debug only ** Do not translate **
    **/
   @RBEntry("Dump Skinny")
   public static final String PRIVATE_CONSTANT_569 = "dumpSkinnyLabel";

   @RBEntry("S")
   public static final String PRIVATE_CONSTANT_570 = "dumpSkinnyMnemonic";

   @RBEntry("Debug Dump Skinny")
   public static final String PRIVATE_CONSTANT_571 = "dumpSkinnyToolTip";

   @RBEntry("View Bucket")
   public static final String PRIVATE_CONSTANT_572 = "viewBucketLabel";

   @RBEntry("G")
   public static final String PRIVATE_CONSTANT_573 = "viewBucketMnemonic";

   @RBEntry("View Graphical Representation of the Bucket")
   public static final String PRIVATE_CONSTANT_574 = "viewBucketToolTip";

   /**
    * This is for Debug only ** Do not translate **
    **/
   @RBEntry("Memory Usage")
   public static final String PRIVATE_CONSTANT_575 = "memoryUsageLabel";

   @RBEntry("M")
   public static final String PRIVATE_CONSTANT_576 = "memoryUsageMnemonic";

   @RBEntry("Display Memory Usage")
   public static final String PRIVATE_CONSTANT_577 = "memoryUsageToolTip";

   /**
    * Other
    **/
   @RBEntry("More")
   public static final String PRIVATE_CONSTANT_578 = "moreLabel";

   @RBEntry("More")
   public static final String PRIVATE_CONSTANT_579 = "moreToolTip";

   /**
    * Toolbar Groups
    **/
   @RBEntry("View")
   public static final String PRIVATE_CONSTANT_580 = "viewLabel";

   @RBEntry("Markup")
   public static final String PRIVATE_CONSTANT_581 = "markupLabel";

   @RBEntry("Measurement")
   public static final String PRIVATE_CONSTANT_582 = "measurementLabel";

   /**
    * Annotation Validation Failure Label and Types
    **/
   @RBEntry("Resolved")
   public static final String PRIVATE_CONSTANT_583 = "resolvedLabel";

   @RBEntry("Failure Type")
   public static final String PRIVATE_CONSTANT_584 = "failureTypeLabel";

   @RBEntry("Checked Out")
   public static final String PRIVATE_CONSTANT_585 = "checkedOutLabel";

   @RBEntry("Revise")
   public static final String PRIVATE_CONSTANT_586 = "reviseLabel";

   @RBEntry("Attribute")
   public static final String PRIVATE_CONSTANT_587 = "commonAttributeLabel";

   @RBEntry("Other")
   public static final String PRIVATE_CONSTANT_588 = "otherLabel";

   /**
    * Help buttons
    **/
   @RBEntry("Help")
   public static final String PRIVATE_CONSTANT_589 = "helpLabel";

   @RBEntry("H")
   public static final String PRIVATE_CONSTANT_590 = "helpMnemonic";

   @RBEntry("Help")
   public static final String PRIVATE_CONSTANT_591 = "helpToolTip";

   /**
    * Indicator tooltips
    **/
   @RBEntry("Has Alternates")
   public static final String PRIVATE_CONSTANT_592 = "hasAlternateToolTip";

   @RBEntry("Has Substitutes")
   public static final String PRIVATE_CONSTANT_593 = "hasSubstituteToolTip";

   @RBEntry("Visualization Indicator")
   public static final String PRIVATE_CONSTANT_594 = "visualizationToolTip";

   @RBEntry("Model not displayed")
   public static final String PRIVATE_CONSTANT_595 = "visualNotDisplayedToolTip";

   @RBEntry("Model partially displayed")
   public static final String PRIVATE_CONSTANT_596 = "visualPartialDisplayedToolTip";

   @RBEntry("Model displayed")
   public static final String PRIVATE_CONSTANT_597 = "visualDisplayedToolTip";

   @RBEntry("Model not displayed due to structure expansion")
   public static final String PRIVATE_CONSTANT_598 = "visualExpandToolTip";

   @RBEntry("Option Count Status")
   public static final String PRIVATE_CONSTANT_599 = "optionCountStatusToolTip";

   @RBEntry("Minimum number of children not shown")
   public static final String PRIVATE_CONSTANT_600 = "optionCountMinReqViolatedToolTip";

   @RBEntry("Out of range")
   public static final String PRIVATE_CONSTANT_601 = "optionCountMaxReqViolatedToolTip";

   /**
    * Variant Properties Info
    **/
   @RBEntry("Context Name")
   @RBComment("On the Variant Properties Info dialog set the Container or Context Name.")
   public static final String PRIVATE_CONSTANT_602 = "defaultOutputContainer";

   /**
    * Saved Results Actions
    **/
   @RBEntry("Save Results")
   public static final String PRIVATE_CONSTANT_603 = "savedResultsAddLabel";

   @RBEntry("Save Selected Objects to Saved Results Table")
   public static final String PRIVATE_CONSTANT_604 = "savedResultsAddToolTip";

   @RBEntry("Remove Results")
   public static final String PRIVATE_CONSTANT_605 = "savedResultsRemoveLabel";

   @RBEntry("Remove Selected Objects from Saved Results Table")
   public static final String PRIVATE_CONSTANT_606 = "savedResultsRemoveToolTip";

   /**
    * Where Used table actions
    **/
   @RBEntry("Expand to type")
   public static final String PRIVATE_CONSTANT_607 = "expandToTypeLabel";

   @RBEntry("Expand to object type")
   public static final String PRIVATE_CONSTANT_608 = "expandToTypeToolTip";

   @RBEntry("Expand by level")
   public static final String PRIVATE_CONSTANT_609 = "expandByLevelLabel";

   @RBEntry("Expand number of levels")
   public static final String PRIVATE_CONSTANT_610 = "expandByLevelToolTip";

   /**
    * Usagelink option rules changed text
    **/
   @RBEntry("Assigned Choices have changed")
   public static final String PRIVATE_CONSTANT_611 = "usagelinkDesignationsChanged";

   /**
    * Where Used table toggles
    **/
   @RBEntry("Show Intermediates")
   public static final String PRIVATE_CONSTANT_612 = "usedByTabFullTree";

   @RBEntry("Hide Intermediates")
   public static final String PRIVATE_CONSTANT_613 = "usedByTabHideIntermediateTree";

   /**
    * Refresh action that refreshes the entire structure, regardless of what objects are selected.
    * This is currently used on the Where Used tab in 'Hide Intermediates' mode.
    **/
   @RBEntry("Refresh")
   public static final String PRIVATE_CONSTANT_614 = "refreshEntireStructureLabel";

   @RBEntry("R")
   public static final String PRIVATE_CONSTANT_615 = "refreshEntireStructureMnemonic";

   @RBEntry("ctrl R")
   public static final String PRIVATE_CONSTANT_616 = "refreshEntireStructureAccelerator";

   @RBEntry("Refresh structure")
   public static final String PRIVATE_CONSTANT_617 = "refreshEntireStructureToolTip";

   /**
    * ConfigurableLink tab table labels
    **/
   @RBEntry("Version")
   public static final String PRIVATE_CONSTANT_618 = "configurableLinkTable.version";

   /**
    * Related Objects tab
    **/
   @RBEntry("Related Objects")
   public static final String PRIVATE_CONSTANT_619 = "relatedObjectsTabLabel";

   /**
    * Structured Annotation Set Properties
    **/
   @RBEntry("Created By")
   public static final String PRIVATE_CONSTANT_622 = "createdByLabel";

   /**
    * Security Labels feature
    **/
   @RBEntry("Security Labels")
   public static final String PRIVATE_CONSTANT_623 = "securityLabels";

   /**
    * Phantom Assemby label
    **/
   @RBEntry("Phantom Manufacturing Part")
   public static final String PRIVATE_CONSTANT_624 = "phantomLabel";

   /**
    * Build Status label
    **/
   @RBEntry("Build Status")
   public static final String PRIVATE_CONSTANT_625 = "buildStatusLabel";

   /**
    * Accelerator for remove action.
    */
   @RBEntry("DELETE")
   public static final String PRIVATE_CONSTANT_626 = "removeAccelerator";

   @RBEntry("Trace Code")
   public static final String PRIVATE_CONSTANT_627 = "traceCodeLabel";

   @RBEntry("Unit")
   public static final String PRIVATE_CONSTANT_628 = "quantityUnitLabel";

   @RBEntry("Quantity")
   public static final String PRIVATE_CONSTANT_629 = "quantityAmountLabel";
}
