/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package config.logicrepository.xml.explorer.structureexplorer;

import wt.util.resource.*;

@RBUUID("config.logicrepository.xml.explorer.structureexplorer.GPSLocalizationRB")
public final class GPSLocalizationRB extends WTListResourceBundle {
   @RBEntry("Expression/Parameters")
   public static final String PRIVATE_CONSTANT_0 = "constraintExpressionParametersLabel";

   @RBEntry("Name")
   public static final String PRIVATE_CONSTANT_1 = "nameLabel";

   @RBEntry("Map to Attribute")
   public static final String PRIVATE_CONSTANT_2 = "mapToAttributeLabel";

   @RBEntry("Data Type")
   public static final String PRIVATE_CONSTANT_3 = "dataTypeLabel";

   @RBEntry("Input Enabled")
   public static final String PRIVATE_CONSTANT_4 = "inputEnabledLabel";

   @RBEntry("Options and Variants")
   public static final String PRIVATE_CONSTANT_5 = "optionsAndVariantsMenuLabel";

   @RBEntry("Options and Variants")
   public static final String PRIVATE_CONSTANT_6 = "optionsAndVariantsMenuToolTip";

   @RBEntry("Configure...")
   public static final String PRIVATE_CONSTANT_7 = "specEditorLaunchMenuItemLabel";

   @RBEntry("Configure to Create Variant Specification")
   public static final String PRIVATE_CONSTANT_8 = "specEditorLaunchMenuItemToolTip";

   @RBEntry("ctrl C")
   public static final String PRIVATE_CONSTANT_9 = "specEditorLaunchAccelerator";

   @RBEntry("Specification Editor Properties...")
   public static final String PRIVATE_CONSTANT_10 = "specEditorPropertiesMenuItemLabel";

   @RBEntry("Edit Specification Editor Properties")
   public static final String PRIVATE_CONSTANT_11 = "specEditorPropertiesMenuItemToolTip";

   @RBEntry("Variant Properties...")
   public static final String PRIVATE_CONSTANT_12 = "variantSpecPropertiesMenuItemLabel";

   @RBEntry("Edit Variant Properties")
   public static final String PRIVATE_CONSTANT_13 = "variantSpecPropertiesMenuItemToolTip";

   @RBEntry("Validate Logic...")
   public static final String PRIVATE_CONSTANT_14 = "validateLogicLaunchMenuItemLabel";

   @RBEntry("Launch Logic Validation")
   public static final String PRIVATE_CONSTANT_15 = "validateLogicLaunchMenuItemToolTip";

   @RBEntry("Specified By")
   public static final String PRIVATE_CONSTANT_16 = "specifiedByValueLabel";

   @RBEntry("Expression/Value")
   public static final String PRIVATE_CONSTANT_17 = "expressionValueLabel";

   @RBEntry("Property Name")
   public static final String PRIVATE_CONSTANT_18 = "uiPropertyNameLabel";

   @RBEntry("Property Value")
   public static final String PRIVATE_CONSTANT_19 = "uiPropertyValueLabel";

   @RBEntry("Variant Attributes")
   public static final String PRIVATE_CONSTANT_20 = "variantPropertiesAttributesSectionHeading";

   @RBEntry("Part Number")
   public static final String PRIVATE_CONSTANT_21 = "variantPropertiesPartNumberSectionHeading";

   @RBEntry("Part Name")
   public static final String PRIVATE_CONSTANT_22 = "variantPropertiesPartNameSectionHeading";

   @RBEntry("<Page Break>")
   public static final String PRIVATE_CONSTANT_23 = "pageBreakLabel";

   @RBEntry("Descendant")
   public static final String PRIVATE_CONSTANT_24 = "descendantLabel";

   @RBEntry("Parameter")
   public static final String PRIVATE_CONSTANT_25 = "parameterNameLabel";

   @RBEntry("Copy to Desktop")
   public static final String PRIVATE_CONSTANT_26 = "copyToDesktopLabel";

   @RBEntry("Copy casetable to desktop")
   public static final String PRIVATE_CONSTANT_27 = "copyToDesktopToolTip";

   @RBEntry("Paste from Desktop")
   public static final String PRIVATE_CONSTANT_28 = "pasteFromDesktopLabel";

   @RBEntry("Paste casetable from desktop")
   public static final String PRIVATE_CONSTANT_29 = "pasteFromDesktopToolTip";

   @RBEntry("Units")
   public static final String PRIVATE_CONSTANT_30 = "unitsLabel";

   @RBEntry("Description")
   public static final String PRIVATE_CONSTANT_31 = "descriptionLabel";

   @RBEntry("Prompt")
   public static final String PRIVATE_CONSTANT_32 = "promptLabel";

   @RBEntry("Page Break Properties")
   public static final String PRIVATE_CONSTANT_33 = "pageBreakPropertiesLabel";

   @RBEntry("Edit Page Break Properties")
   public static final String PRIVATE_CONSTANT_34 = "pageBreakPropertiesToolTip";

   @RBEntry("Properties")
   public static final String PRIVATE_CONSTANT_35 = "gpPropertiesMenuLabel";
}
