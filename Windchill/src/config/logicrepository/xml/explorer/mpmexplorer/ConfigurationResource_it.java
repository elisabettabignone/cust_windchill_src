/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package config.logicrepository.xml.explorer.mpmexplorer;

import wt.util.resource.*;

@RBUUID("config.logicrepository.xml.explorer.mpmexplorer.ConfigurationResource")
public final class ConfigurationResource_it extends WTListResourceBundle {
   @RBEntry("Doesn't Matter")
   public static final String PRIVATE_CONSTANT_0 = "ImNotUsedButNeedToBeHereForSomeJavaVMs";

   @RBEntry("Numero alternativo")
   public static final String PRIVATE_CONSTANT_1 = "alternateNumberLabel";

   /**
    * Documentation tab labels
    **/
   @RBEntry("Documenti associati")
   public static final String PRIVATE_CONSTANT_2 = "documentationTableLabel";

   @RBEntry("Documenti CAD")
   public static final String PRIVATE_CONSTANT_3 = "epmDocumentationTableLabel";

   @RBEntry("Descrizione associazione")
   public static final String PRIVATE_CONSTANT_4 = "associationDescriptionLabel";

   @RBEntry("Aggiungi documento CAD associato")
   public static final String PRIVATE_CONSTANT_5 = "addEPMDocumentLabel";

   @RBEntry("Aggiungi documento CAD associato")
   public static final String PRIVATE_CONSTANT_6 = "addEPMDocumentToolTip";

   @RBEntry("Rimuovi documento CAD associato")
   public static final String PRIVATE_CONSTANT_7 = "removeEPMDocumentLabel";

   @RBEntry("Rimuovi documento CAD associato")
   public static final String PRIVATE_CONSTANT_8 = "removeEPMDocumentToolTip";

   @RBEntry("Corrente")
   public static final String PRIVATE_CONSTANT_9 = "currentDocumentationSubTabLabel";

   @RBEntry("Equivalente a monte")
   public static final String PRIVATE_CONSTANT_10 = "upstreamDocumentationSubTabLabel";

   @RBEntry("Genera operazioni da file XML NC")
   public static final String PRIVATE_CONSTANT_11 = "generateObjectsFromNCFileLabel";

   @RBEntry("Genera operazioni da file XML NC")
   public static final String PRIVATE_CONSTANT_12 = "generateObjectsFromNCFileToolTip";

   /**
    * equivalence link
    **/
   @RBEntry("Proprietà filtro a monte")
   public static final String PRIVATE_CONSTANT_13 = "navigationCriteriaUpstreamLabel";

   @RBEntry("Proprietà filtro a valle")
   public static final String PRIVATE_CONSTANT_14 = "navigationCriteriaDownstreamLabel";

   @RBEntry("Descrizione link")
   public static final String PRIVATE_CONSTANT_17 = "equivalenceLinkDescriptionLabel";

   @RBEntry("Revisione")
   public static final String PRIVATE_CONSTANT_18 = "revisionLevelLabel";

   @RBEntry("Nome vista")
   public static final String PRIVATE_CONSTANT_19 = "viewLabel";

   @RBEntry("Parti equivalenti a monte")
   public static final String PRIVATE_CONSTANT_20 = "equivalentTo";

   @RBEntry("Parti equivalenti a valle")
   public static final String PRIVATE_CONSTANT_21 = "equivalentWith";

   @RBEntry("Parti equivalenti")
   public static final String PRIVATE_CONSTANT_22 = "EquivalenceTabLabel";

   @RBEntry("Aggiungi parte equivalente...")
   public static final String PRIVATE_CONSTANT_23 = "addEquivalenceLabel";

   @RBEntry("Aggiunge una parte equivalente")
   public static final String PRIVATE_CONSTANT_24 = "addEquivalenceToolTip";

   @RBEntry("Rimuovi parte equivalente")
   public static final String PRIVATE_CONSTANT_25 = "removeEquivalenceToolTip";

   @RBEntry("Rimuovi parte equivalente")
   public static final String PRIVATE_CONSTANT_26 = "removeEquivalenceLabel";

   @RBEntry("Attivatore caso d'impiego equivalente")
   public static final String PRIVATE_CONSTANT_27 = "isConsumable";

   @RBEntry("Aggiorna gli attributi e le associazioni comuni")
   public static final String PRIVATE_CONSTANT_28 = "updateMasterAttrAndAssocLabel";

   @RBEntry("Aggiorna gli attributi e le associazioni comuni")
   public static final String PRIVATE_CONSTANT_29 = "updateMasterAttrAndAssocToolTip";

   @RBEntry("Aggiorna gli attributi e le associazioni della parte")
   public static final String PRIVATE_CONSTANT_30 = "updateIterationAttrAndAssocLabel";

   @RBEntry("Aggiorna gli attributi e le associazioni della parte")
   public static final String PRIVATE_CONSTANT_31 = "updateIterationAttrAndAssocToolTip";

   @RBEntry("Confronta con report a valle")
   public static final String PRIVATE_CONSTANT_32 = "upstreamCompareReportLabel";

   @RBEntry("Confronta con report a monte")
   public static final String PRIVATE_CONSTANT_33 = "downstreamCompareReportLabel";

   @RBEntry("Evidenzia tutto")
   public static final String PRIVATE_CONSTANT_34 = "highlightDefaultLabel";

   @RBEntry("Evidenzia tutto")
   public static final String PRIVATE_CONSTANT_35 = "highlightDefaultToolTip";

   @RBEntry("Evidenzia differenze")
   public static final String PRIVATE_CONSTANT_36 = "highlightDiffLabel";

   @RBEntry("Evidenzia le differenze")
   public static final String PRIVATE_CONSTANT_37 = "highlightDiffToolTip";

   @RBEntry("Evidenzia problemi")
   public static final String PRIVATE_CONSTANT_38 = "highlightIssueLabel";

   @RBEntry("Evidenzia i problemi")
   public static final String PRIVATE_CONSTANT_39 = "highlightIssueToolTip";

   @RBEntry("Evidenzia assiemi")
   public static final String PRIVATE_CONSTANT_40 = "highlightAssemblyLabel";

   @RBEntry("Evidenzia gli assiemi")
   public static final String PRIVATE_CONSTANT_41 = "highlightAssemblyToolTip";

   @RBEntry("Confronto equivalenza Dove usato")
   public static final String PRIVATE_CONSTANT_42 = "detailCompareReportLabel";

   @RBEntry("Confronto equivalenza Dove usato")
   public static final String PRIVATE_CONSTANT_43 = "detailCompareReportToolTip";

   @RBEntry("Report confronto equivalenza")
   public static final String PRIVATE_CONSTANT_44 = "compareReportLabel";

   @RBEntry("Report confronto equivalenza")
   public static final String PRIVATE_CONSTANT_45 = "compareReportToolTip";

   @RBEntry("Avanti")
   public static final String PRIVATE_CONSTANT_46 = "nextLabel";

   @RBEntry("Avanti")
   public static final String PRIVATE_CONSTANT_47 = "nextToolTip";

   @RBEntry("Indietro")
   public static final String PRIVATE_CONSTANT_48 = "previousLabel";

   @RBEntry("Indietro")
   public static final String PRIVATE_CONSTANT_49 = "previousToolTip";

   @RBEntry("Più recente")
   public static final String PRIVATE_CONSTANT_50 = "latestItrLabel";

   /**
    * consumption link
    **/
   @RBEntry("Casi d'impiego equivalenti")
   public static final String PRIVATE_CONSTANT_51 = "ConsumptionTabLabel";

   @RBEntry("Casi d'impiego equivalenti a monte")
   public static final String PRIVATE_CONSTANT_52 = "consumedLabel";

   @RBEntry("Casi d'impiego equivalenti a valle")
   public static final String PRIVATE_CONSTANT_53 = "consumesLabel";

   @RBEntry("Contesto a valle")
   public static final String PRIVATE_CONSTANT_54 = "downstreamContextNameLabel";

   @RBEntry("Contesto a monte")
   public static final String PRIVATE_CONSTANT_55 = "upstreamContextNameLabel";

   @RBEntry("Aggiunge un caso d'impiego equivalente dagli Appunti")
   public static final String PRIVATE_CONSTANT_56 = "addConsumptionToolTip";

   @RBEntry("Rimuove un caso d'impiego equivalente")
   public static final String PRIVATE_CONSTANT_57 = "removeConsumptionToolTip";

   @RBEntry("Stato consumo a monte")
   public static final String PRIVATE_CONSTANT_58 = "upstreamConsumptionStatusLabel";

   @RBEntry("Stato consumo a valle")
   public static final String PRIVATE_CONSTANT_59 = "downstreamConsumptionStatusLabel";

   @RBEntry("Stato allocazione parte")
   public static final String PRIVATE_CONSTANT_60 = "partAllocationStatus";

   /**
    * Operations table in Uses tab
    **/
   @RBEntry("Operazioni")
   public static final String PRIVATE_CONSTANT_61 = "operationsLabel";

   @RBEntry("Operazioni secondarie")
   public static final String PRIVATE_CONSTANT_62 = "subOperationsLabel";

   /**
    * Process Plan tab labels
    **/
   @RBEntry("Piani di produzione")
   public static final String PRIVATE_CONSTANT_63 = "processPlanTabLabel";

   @RBEntry("Versione più recente")
   public static final String PRIVATE_CONSTANT_64 = "latestVersion";

   @RBEntry("Piani di produzione associati")
   public static final String PRIVATE_CONSTANT_65 = "associatedProcessPlansLabel";

   @RBEntry("Aggiungi piano di produzione associato")
   public static final String PRIVATE_CONSTANT_66 = "addProcessPlanLabel";

   @RBEntry("Aggiungi piano di produzione associato")
   public static final String PRIVATE_CONSTANT_67 = "addProcessPlanToolTip";

   @RBEntry("Rimuovi piano di produzione associato")
   public static final String PRIVATE_CONSTANT_68 = "removeProcessPlanLabel";

   @RBEntry("Rimuovi piano di produzione associato")
   public static final String PRIVATE_CONSTANT_69 = "removeProcessPlanToolTip";

   @RBEntry("Apri piano selezionato in Navigatore piani di produzione")
   public static final String PRIVATE_CONSTANT_70 = "openInNewExplorerLabel";

   @RBEntry("Apri piano selezionato in Navigatore piani di produzione")
   public static final String PRIVATE_CONSTANT_71 = "openInNewExplorerToolTip";

   @RBEntry("Commuta selezione piano di produzione")
   public static final String PRIVATE_CONSTANT_72 = "processplanSelectionActionLabel";

   @RBEntry("Commuta la selezione del piano di produzione")
   public static final String PRIVATE_CONSTANT_73 = "processplanSelectionActionToolTip";

   @RBEntry("Selezione piano di produzione")
   public static final String PRIVATE_CONSTANT_74 = "processplanSelectionStatus";

   @RBEntry("Selezione piano di produzione")
   public static final String PRIVATE_CONSTANT_75 = "processplanSelectionToolTip";

   @RBEntry("Piano di produzione non selezionabile in base alla specifica di configurazione")
   public static final String PRIVATE_CONSTANT_76 = "processplanSelectionUnknownToolTip";

   @RBEntry("Piano di produzione selezionato")
   public static final String PRIVATE_CONSTANT_77 = "processplanSelectionExistsToolTip";

   @RBEntry("Piano di produzione non selezionato")
   public static final String PRIVATE_CONSTANT_78 = "processplanSelectionDoesNotExistsToolTip";

   @RBEntry("Nuova parte a valle")
   public static final String PRIVATE_CONSTANT_79 = "createPartWithBOMTransformationLabel";

   @RBEntry("Crea una nuova parte a valle associata alla parte selezionata")
   public static final String PRIVATE_CONSTANT_80 = "createPartWithBOMTransformationToolTip";

   /**
    * Alternate Group tab labels
    **/
   @RBEntry("Distinta base alternativa")
   public static final String PRIVATE_CONSTANT_81 = "alternateGroupTabLabel";

   @RBEntry("Distinta base alternativa associata")
   public static final String PRIVATE_CONSTANT_82 = "associatedAlternateGroupLabel";

   /**
    * Common labels
    **/
   @RBEntry("Unità di misura di base")
   public static final String PRIVATE_CONSTANT_83 = "baseUnitOfMeasureLabel";

   @RBEntry("Categoria")
   public static final String PRIVATE_CONSTANT_84 = "categoryLabel";

   @RBEntry("Revisione")
   public static final String PRIVATE_CONSTANT_85 = "RevisionLabel";

   /**
    * Tree labels
    **/
   @RBEntry("Nome")
   public static final String PRIVATE_CONSTANT_86 = "nameLabel";

   @RBEntry("Numero")
   public static final String PRIVATE_CONSTANT_87 = "numberLabel";

   @RBEntry("Versione")
   public static final String PRIVATE_CONSTANT_88 = "versionIterationView";

   @RBEntry("Quantità")
   public static final String PRIVATE_CONSTANT_89 = "quantity.amount";

   @RBEntry("Unità")
   public static final String PRIVATE_CONSTANT_90 = "quantity.unit";

   @RBEntry("Stato del ciclo di vita")
   public static final String PRIVATE_CONSTANT_91 = "state.state";

   @RBEntry("Posizione")
   public static final String PRIVATE_CONSTANT_92 = "containerInfo.name";

   @RBEntry("Parte di fabbricazione fittizia o phantom")
   public static final String PRIVATE_CONSTANT_93 = "phantomLabel";

   /**
    * Control Characteristics Tab
    **/
   @RBEntry("Caratteristiche di controllo")
   public static final String PRIVATE_CONSTANT_94 = "processplanControlCharacteristicLabel";

   @RBEntry("Caratteristiche di controllo")
   public static final String PRIVATE_CONSTANT_95 = "partControlCharacteristicLabel";

   @RBEntry("Caratteristiche di controllo")
   public static final String PRIVATE_CONSTANT_96 = "qualityTabLabel";

   @RBEntry("Nuova caratteristica di controllo")
   public static final String PRIVATE_CONSTANT_97 = "newCCLabel";

   @RBEntry("Nuova caratteristica di controllo")
   public static final String PRIVATE_CONSTANT_98 = "newCCToolTip";

   @RBEntry("Alloca caratteristiche di controllo")
   public static final String PRIVATE_CONSTANT_99 = "allocateCCLabel";

   @RBEntry("Alloca caratteristiche di controllo")
   public static final String PRIVATE_CONSTANT_100 = "allocateCCToolTip";

   @RBEntry("Versione")
   public static final String PRIVATE_CONSTANT_101 = "versionDisplayLabel";

   @RBEntry("Gravità")
   public static final String PRIVATE_CONSTANT_102 = "severityDisplayLabel";

   @RBEntry("Nome operazione")
   public static final String PRIVATE_CONSTANT_103 = "operNameDisplayLabel";

   @RBEntry("Numero operazione")
   public static final String PRIVATE_CONSTANT_104 = "operNumberDisplayLabel";

   @RBEntry("Proprietario")
   public static final String PRIVATE_CONSTANT_105 = "ownerDisplayLabel";

   @RBEntry("Campionamento")
   public static final String PRIVATE_CONSTANT_106 = "samplingDisplayLabel";

   @RBEntry("Unità")
   public static final String PRIVATE_CONSTANT_107 = "unitDisplayLabel";

   @RBEntry("Frequenza")
   public static final String PRIVATE_CONSTANT_108 = "frequencyDisplayLabel";

   @RBEntry("Risorse")
   public static final String PRIVATE_CONSTANT_109 = "resourcesDisplayLabel";

   @RBEntry("Descrizione")
   public static final String PRIVATE_CONSTANT_110 = "descDisplayLabel";

   @RBEntry("Riepilogo allocazioni caratteristiche di controllo")
   public static final String PRIVATE_CONSTANT_111 = "controlcharacteristicsallocationsummaryLabel";

   @RBEntry("Riepilogo allocazioni caratteristiche di controllo")
   public static final String PRIVATE_CONSTANT_112 = "controlcharacteristicsallocationsummaryTip";

   @RBEntry("Allocata?")
   public static final String PRIVATE_CONSTANT_113 = "allocationCheckLabel";

   @RBEntry("Alloca risorse a caratteristica di controllo")
   public static final String PRIVATE_CONSTANT_114 = "editCCLabel";

   @RBEntry("Alloca risorse a caratteristica di controllo")
   public static final String PRIVATE_CONSTANT_115 = "editCCToolTip";

   @RBEntry("Commuta allocazione risorse")
   public static final String PRIVATE_CONSTANT_116 = "toggleResourceLabel";

   @RBEntry("Commuta allocazione risorse")
   public static final String PRIVATE_CONSTANT_117 = "toggleResourceToolTip";

   /**
    * Compatibility Tab
    **/
   @RBEntry("Compatibilità")
   public static final String PRIVATE_CONSTANT_118 = "compatibilityTabLabel";

   @RBEntry("Oggetti compatibili")
   public static final String PRIVATE_CONSTANT_119 = "compatibleToLabel";

   @RBEntry("Compatibile con")
   public static final String PRIVATE_CONSTANT_120 = "compatibleWithLabel";

   @RBEntry("Aggiungi compatibilità...")
   public static final String PRIVATE_CONSTANT_121 = "addCompatibleLabel";

   @RBEntry("Aggiunge compatibilità")
   public static final String PRIVATE_CONSTANT_122 = "addCompatibleToolTip";

   @RBEntry("Modifica tassi di utilizzo")
   public static final String PRIVATE_CONSTANT_123 = "modifyStepUsageRateForCompatibilityLabel";

   @RBEntry("Modifica tassi di utilizzo")
   public static final String PRIVATE_CONSTANT_124 = "modifyStepUsageRateForCompatibilityToolTip";

   @RBEntry("Rimuovi compatibilità")
   public static final String PRIVATE_CONSTANT_125 = "removeCompatibleLabel";

   @RBEntry("Rimuovi compatibilità")
   public static final String PRIVATE_CONSTANT_126 = "removeCompatibleToolTip";

   @RBEntry("Stabilimenti")
   public static final String PRIVATE_CONSTANT_127 = "plantTabLabel";

   @RBEntry("Stabilimento")
   public static final String PRIVATE_CONSTANT_128 = "plantMenuLabel";

   @RBEntry("Stabilimenti associati")
   public static final String PRIVATE_CONSTANT_129 = "associatedPlantLabel";

   @RBEntry("Inserisci esistente...")
   public static final String PRIVATE_CONSTANT_130 = "addPlantLabel";

   @RBEntry("Inserisci esistente")
   public static final String PRIVATE_CONSTANT_131 = "addPlantToolTip";

   @RBEntry("Rimuovi")
   public static final String PRIVATE_CONSTANT_132 = "removePlantLabel";

   @RBEntry("Rimuovi")
   public static final String PRIVATE_CONSTANT_133 = "removePlantToolTip";

   /**
    * Indicator Labels
    **/
   @RBEntry("Stato caso d'impiego equivalente a monte")
   public static final String PRIVATE_CONSTANT_134 = "upstreamEquivalentOccurranceLinkLabel";

   @RBEntry("Stato caso d'impiego equivalente a monte")
   public static final String PRIVATE_CONSTANT_135 = "upstreamEquivalentOccurranceLinkToolTip";

   @RBEntry("Stato caso d'impiego equivalente a valle")
   public static final String PRIVATE_CONSTANT_136 = "downstreamEquivalentOccurranceLinkLabel";

   @RBEntry("Stato caso d'impiego equivalente a valle")
   public static final String PRIVATE_CONSTANT_137 = "downstreamEquivalentOccurranceLinkToolTip";

   @RBEntry("Esiste un caso d'impiego equivalente")
   public static final String PRIVATE_CONSTANT_138 = "equivalentOccurranceLinkExistsToolTip";

   @RBEntry("Esiste un caso d'impiego equivalente ma non è nel contesto aperto")
   public static final String PRIVATE_CONSTANT_139 = "equivalentOccurranceLinkExistsNotInContextToolTip";

   @RBEntry("Non esiste un caso d'impiego equivalente")
   public static final String PRIVATE_CONSTANT_140 = "equivalentOccurranceLinkDoesNotExistsToolTip";

   @RBEntry("Stato del ciclo di vita caso d'impiego equivalente sconosciuto o informazioni non disponibili")
   public static final String PRIVATE_CONSTANT_141 = "equivalentOccurranceLinkUnknownToolTip";

   @RBEntry("Impossibile recuperare le informazioni sul caso d'impiego equivalente")
   public static final String PRIVATE_CONSTANT_142 = "equivalentOccurranceLinkNotRetrievedToolTip";

   @RBEntry("Stato parte equivalente a monte")
   public static final String PRIVATE_CONSTANT_143 = "upstreamEquivalentLinkLabel";

   @RBEntry("Stato parte equivalente a monte")
   public static final String PRIVATE_CONSTANT_144 = "upstreamEquivalentLinkToolTip";

   @RBEntry("Stato parte equivalente a valle")
   public static final String PRIVATE_CONSTANT_145 = "downstreamEquivalentLinkLabel";

   @RBEntry("Stato parte equivalente a valle")
   public static final String PRIVATE_CONSTANT_146 = "downstreamEquivalentLinkToolTip";

   @RBEntry("Una parte equivalente esiste e punta all'iterazione più recente")
   public static final String PRIVATE_CONSTANT_147 = "equivalentLinkExistsToolTip";

   @RBEntry("Una parte equivalente esiste ma non punta all'iterazione più recente")
   public static final String PRIVATE_CONSTANT_148 = "equivalentLinkExistsNotInLatestIterationToolTip";

   @RBEntry("Non esiste una parte equivalente in alcuna iterazione")
   public static final String PRIVATE_CONSTANT_149 = "equivalentLinkDoesNotExistsToolTip";

   @RBEntry("Impossibile recuperare le informazioni sulla parte equivalente")
   public static final String PRIVATE_CONSTANT_150 = "equivalentLinkNotRetrievedToolTip";

   @RBEntry("La parte proviene da un assieme riutilizzato a monte")
   public static final String PRIVATE_CONSTANT_151 = "equivalentLinkSameAsChildToolTip";

   @RBEntry("Colonna indicatore allocazione piano di produzione")
   public static final String PRIVATE_CONSTANT_152 = "operationAllocationLabel";

   @RBEntry("Colonna indicatore allocazione piano di produzione")
   public static final String PRIVATE_CONSTANT_153 = "operationAllocationToolTip";

   @RBEntry("Allocata a un'operazione di tutti i piani di produzione selezionati")
   public static final String PRIVATE_CONSTANT_154 = "operationAllocationExistsToolTip";

   @RBEntry("Non allocata ad alcuna operazione di tutti i piani di produzione selezionati")
   public static final String PRIVATE_CONSTANT_155 = "operationAllocationDoesNotExistsToolTip";

   @RBEntry("Nessun piano di produzione selezionato in base alla specifica di configurazione")
   public static final String PRIVATE_CONSTANT_156 = "operationAllocationNoSelectionToolTip";

   @RBEntry("Ha una modifica in sospeso")
   public static final String PRIVATE_CONSTANT_157 = "hasPendingChange";

   @RBEntry("Stato gruppo alternative")
   public static final String PRIVATE_CONSTANT_158 = "alternateGroupStatusToolTip";

   @RBEntry("Il gruppo alternative non esiste")
   public static final String PRIVATE_CONSTANT_159 = "alternateGroupStatusDoesNotExistsToolTip";

   @RBEntry("Il gruppo alternative esiste")
   public static final String PRIVATE_CONSTANT_160 = "alternateGroupStatusExistsToolTip";

   @RBEntry("Stato gruppo alternative sconosciuto")
   public static final String PRIVATE_CONSTANT_161 = "alternateGroupStatusUnknownToolTip";

   @RBEntry("Stato del ciclo di vita")
   public static final String PRIVATE_CONSTANT_162 = "stateLabel";

   @RBEntry("Versione")
   public static final String PRIVATE_CONSTANT_163 = "versionIterationViewLabel";

   /**
    * Calculated Cost and Time pop-up
    **/
   @RBEntry("Tempo totale")
   public static final String PRIVATE_CONSTANT_164 = "TotalTimeLabel";

   @RBEntry("Costo totale")
   public static final String PRIVATE_CONSTANT_165 = "TotalCostLabel";

   @RBEntry("Calcola tempistiche e costi...")
   public static final String PRIVATE_CONSTANT_166 = "cumulateTotalTimeAndCostLabel";

   @RBEntry("Calcola tempistiche e costi")
   public static final String PRIVATE_CONSTANT_167 = "cumulateTotalTimeAndCostToolTip";

   @RBEntry("Quantità lotto")
   public static final String PRIVATE_CONSTANT_168 = "lotQuantityLabel";

   @RBEntry("Quantità lotto")
   public static final String PRIVATE_CONSTANT_169 = "lotQuantityToolTip";

   @RBEntry("Preparazione")
   public static final String PRIVATE_CONSTANT_170 = "setupLabel";

   @RBEntry("Preparazione")
   public static final String PRIVATE_CONSTANT_171 = "setUpTimeToolTip";

   @RBEntry("Elaborazione")
   public static final String PRIVATE_CONSTANT_172 = "processingLabel";

   @RBEntry("Elaborazione")
   public static final String PRIVATE_CONSTANT_173 = "processingToolTip";

   @RBEntry("Coda")
   public static final String PRIVATE_CONSTANT_174 = "queueLabel";

   @RBEntry("Coda")
   public static final String PRIVATE_CONSTANT_175 = "queueToolTip";

   @RBEntry("In attesa")
   public static final String PRIVATE_CONSTANT_176 = "waitingLabel";

   @RBEntry("In attesa")
   public static final String PRIVATE_CONSTANT_177 = "waitingToolTip";

   @RBEntry("Smantellamento")
   public static final String PRIVATE_CONSTANT_178 = "teardownLabel";

   @RBEntry("Smantellamento")
   public static final String PRIVATE_CONSTANT_179 = "teardownToolTip";

   @RBEntry("Sposta")
   public static final String PRIVATE_CONSTANT_180 = "moveLabel";

   @RBEntry("Sposta")
   public static final String PRIVATE_CONSTANT_181 = "moveToolTip";

   @RBEntry("Lavorazione")
   public static final String PRIVATE_CONSTANT_182 = "laborLabel";

   @RBEntry("Lavorazione")
   public static final String PRIVATE_CONSTANT_183 = "laborToolTip";

   /**
    * Menu actions labels
    **/
   @RBEntry("Elimina")
   public static final String PRIVATE_CONSTANT_184 = "deleteProcessPlanLabel";

   @RBEntry("Elimina gli oggetti selezionati dal database")
   public static final String PRIVATE_CONSTANT_185 = "deleteProcessPlanToolTip";

   @RBEntry("Inserisci nuova sequenza...")
   public static final String PRIVATE_CONSTANT_186 = "createAndInsertSequenceLabel";

   @RBEntry("Inserisce una nuova sequenza")
   public static final String PRIVATE_CONSTANT_187 = "createAndInsertSequenceToolTip";

   @RBEntry("Nuova operazione...")
   public static final String PRIVATE_CONSTANT_188 = "createAndInsertOperationLabel";

   @RBEntry("Nuova operazione")
   public static final String PRIVATE_CONSTANT_189 = "createAndInsertOperationToolTip";

   @RBEntry("Dividi link utilizzo...")
   public static final String PRIVATE_CONSTANT_190 = "splitUsageLinkLabel";

   @RBEntry("Divide un link utilizzo")
   public static final String PRIVATE_CONSTANT_191 = "splitUsageLinkToolTip";

   @RBEntry("Ricalcola tempistiche e costi...")
   public static final String PRIVATE_CONSTANT_192 = "recalculateTotalTimeAndCostLabel";

   @RBEntry("Ricalcola tempistiche e costi")
   public static final String PRIVATE_CONSTANT_193 = "recalculateTotalTimeAndCostToolTip";

   @RBEntry("Visualizza assiemi attivi")
   public static final String PRIVATE_CONSTANT_194 = "activeAssemblyLabel";

   @RBEntry("Contesto assiemi attivi")
   public static final String PRIVATE_CONSTANT_195 = "activeAssemblyToolTip";

   @RBEntry("Navigatore piani di produzione...")
   public static final String PRIVATE_CONSTANT_196 = "launchPPELabel";

   @RBEntry("c")
   public static final String PRIVATE_CONSTANT_197 = "launchPPEMnemonic";

   @RBEntry("Navigatore piani di produzione")
   public static final String PRIVATE_CONSTANT_198 = "launchPPEToolTip";

   @RBEntry("Navigatore risorse di fabbricazione...")
   public static final String PRIVATE_CONSTANT_199 = "launchMRELabel";

   @RBEntry("N")
   public static final String PRIVATE_CONSTANT_200 = "launchMREMnemonic";

   @RBEntry("Navigatore risorse di fabbricazione")
   public static final String PRIVATE_CONSTANT_201 = "launchMREToolTip";

   @RBEntry("Navigatore standard di fabbricazione...")
   public static final String PRIVATE_CONSTANT_202 = "launchMSELabel";

   @RBEntry("f")
   public static final String PRIVATE_CONSTANT_203 = "launchMSEMnemonic";

   @RBEntry("Navigatore standard di fabbricazione")
   public static final String PRIVATE_CONSTANT_204 = "launchMSEToolTip";

   @RBEntry("Navigatore struttura di prodotto fabbricazione...")
   public static final String PRIVATE_CONSTANT_205 = "launchAssociativeBOMLabel";

   @RBEntry("a")
   public static final String PRIVATE_CONSTANT_206 = "launchAssociativeBOMMnemonic";

   @RBEntry("Navigatore struttura di prodotto fabbricazione")
   public static final String PRIVATE_CONSTANT_207 = "launchAssociativeBOMToolTip";

   @RBEntry("Navigatore Gantt di produzione...")
   public static final String PRIVATE_CONSTANT_208 = "launchMGELabel";

   @RBEntry("g")
   public static final String PRIVATE_CONSTANT_209 = "launchMGEMnemonic";

   @RBEntry("Navigatore Gantt di produzione")
   public static final String PRIVATE_CONSTANT_210 = "launchMGEToolTip";

   @RBEntry("Nuovo Navigatore")
   public static final String PRIVATE_CONSTANT_211 = "viewMenuNewExplorerLabel";

   @RBEntry("v")
   public static final String PRIVATE_CONSTANT_212 = "viewMenuNewExplorerMnemonic";

   @RBEntry("Nuovo ramo...")
   public static final String PRIVATE_CONSTANT_213 = "newBranchLabel";

   @RBEntry("Nuovo ramo")
   public static final String PRIVATE_CONSTANT_214 = "newBranchToolTip";

   @RBEntry("R")
   public static final String PRIVATE_CONSTANT_215 = "newBranchMnemonic";

   @RBEntry("Nuova distinta base alternativa...")
   public static final String PRIVATE_CONSTANT_216 = "newAlternateLabel";

   @RBEntry("Nuova distinta base alternativa")
   public static final String PRIVATE_CONSTANT_217 = "newAlternateToolTip";

   @RBEntry("A")
   public static final String PRIVATE_CONSTANT_218 = "newAlternateMnemonic";

   @RBEntry("Aggiorna numero distinta base alternativa...")
   public static final String PRIVATE_CONSTANT_219 = "updateAlternateLabel";

   @RBEntry("Aggiorna numero distinta base alternativa")
   public static final String PRIVATE_CONSTANT_220 = "updateAlternateToolTip";

   @RBEntry("G")
   public static final String PRIVATE_CONSTANT_221 = "updateAlternateMnemonic";

   /**
    * Attribute labels in Create and Insert Sequence Wizard
    **/
   @RBEntry("Contenitore operazioni")
   public static final String PRIVATE_CONSTANT_222 = "operationHolderLabel";

   @RBEntry("Operazione diramata")
   public static final String PRIVATE_CONSTANT_223 = "branchingOperationLabel";

   @RBEntry("Vincolo diramazione")
   public static final String PRIVATE_CONSTANT_224 = "branchingOperationTypeLabel";

   @RBEntry("Operazione confluente")
   public static final String PRIVATE_CONSTANT_225 = "returnOperationLabel";

   @RBEntry("Vincolo confluenza")
   public static final String PRIVATE_CONSTANT_226 = "returnOperationTypeLabel";

   /**
    * Attributes of the Standard procedure panel
    **/
   @RBEntry("Procedure standard referenziate")
   public static final String PRIVATE_CONSTANT_227 = "referencedStandardProcedureLabel";

   @RBEntry("Aggiungi procedura standard...")
   public static final String PRIVATE_CONSTANT_228 = "addStandardProcedureLabel";

   @RBEntry("Aggiunge una procedura standard")
   public static final String PRIVATE_CONSTANT_229 = "addStandardProcedureToolTip";

   @RBEntry("Rimuovi riferimento a procedura standard")
   public static final String PRIVATE_CONSTANT_230 = "removeStandardProcedureLabel";

   @RBEntry("Rimuovi riferimento a procedura standard")
   public static final String PRIVATE_CONSTANT_231 = "removeStandardProcedureToolTip";

   @RBEntry("Apri nel navigatore")
   public static final String PRIVATE_CONSTANT_232 = "openStandardProcedureInNewExplorerLabel";

   @RBEntry("Apri nel navigatore")
   public static final String PRIVATE_CONSTANT_233 = "openStandardProcedureInNewExplorerToolTip";

   @RBEntry("Operazione di inserimento")
   public static final String PRIVATE_CONSTANT_234 = "insertionOperationLabel";

   /**
    * Attributes of the Related Parts panel
    **/
   @RBEntry("Parti correlate")
   public static final String PRIVATE_CONSTANT_235 = "relatedPartsLabel";

   @RBEntry("Associa parte")
   public static final String PRIVATE_CONSTANT_236 = "addRelatedPartLabel";

   @RBEntry("Associa parte")
   public static final String PRIVATE_CONSTANT_237 = "addRelatedPartToolTip";

   @RBEntry("Rimuovi associazione parte")
   public static final String PRIVATE_CONSTANT_238 = "removeRelatedPartLabel";

   @RBEntry("Rimuovi associazione parte")
   public static final String PRIVATE_CONSTANT_239 = "removeRelatedPartToolTip";

   @RBEntry("Apri parte correlata in nuovo Navigatore struttura di prodotto")
   public static final String PRIVATE_CONSTANT_240 = "openRelatedPartInNewExplorerLabel";

   @RBEntry("Apri parte correlata in nuovo Navigatore struttura di prodotto")
   public static final String PRIVATE_CONSTANT_241 = "openRelatedPartInNewExplorerToolTip";

   /**
    * Attributes of Constrained Operations table
    **/
   @RBEntry("Operazioni con vincoli")
   public static final String PRIVATE_CONSTANT_242 = "constrainedOperationsLabel";

   @RBEntry("Tipo di vincolo")
   public static final String PRIVATE_CONSTANT_243 = "constraintTypeLabel";

   @RBEntry("Obbligatorio")
   public static final String PRIVATE_CONSTANT_244 = "requiredLabel";

   @RBEntry("Ritardo")
   public static final String PRIVATE_CONSTANT_245 = "lagLabel";

   @RBEntry("Aggiungi operazione vincolata")
   public static final String PRIVATE_CONSTANT_246 = "addConstrainedOperationLabel";

   @RBEntry("Aggiungi operazione vincolata")
   public static final String PRIVATE_CONSTANT_247 = "addConstrainedOperationToolTip";

   @RBEntry("Rimuovi operazione vincolata")
   public static final String PRIVATE_CONSTANT_248 = "removeConstrainedOperationLabel";

   @RBEntry("Rimuovi operazione vincolata")
   public static final String PRIVATE_CONSTANT_249 = "removeConstrainedOperationToolTip";

   /**
    * Attributes of the Uses tab in general
    **/
   @RBEntry("Componenti")
   public static final String PRIVATE_CONSTANT_250 = "operationsTabLabel";

   /**
    * Attributes of the Sequences Uses tab
    **/
   @RBEntry("Ritardo diramazione")
   public static final String PRIVATE_CONSTANT_251 = "branchingLagLabel";

   @RBEntry("Ritardo confluenza")
   public static final String PRIVATE_CONSTANT_252 = "returnLagLabel";

   /**
    * Attributes of the Operations Uses tab
    **/
   @RBEntry("Centro di lavorazione")
   public static final String PRIVATE_CONSTANT_253 = "workCenterNameLabel";

   @RBEntry("Metodologia produttiva")
   public static final String PRIVATE_CONSTANT_254 = "processNameLabel";

   /**
    * Picker tabs
    **/
   @RBEntry("Selezione parte")
   public static final String PRIVATE_CONSTANT_255 = "mpmBOMTreeTabLabel";

   @RBEntry("Selezione piano di produzione")
   public static final String PRIVATE_CONSTANT_256 = "processPlanTreeTabLabel";

   @RBEntry("Selezione standard di fabbricazione")
   public static final String PRIVATE_CONSTANT_257 = "mfgStandardTreeTabLabel";

   @RBEntry("Selezione risorsa")
   public static final String PRIVATE_CONSTANT_258 = "resourceTreeTabLabel";

   /**
    * Open/Select In Explorer
    **/
   @RBEntry("Apri nel navigatore...")
   public static final String PRIVATE_CONSTANT_259 = "openInExplorerLabel";

   @RBEntry("O")
   public static final String PRIVATE_CONSTANT_260 = "openInExplorerMnemonic";

   @RBEntry("Apri nel navigatore...")
   public static final String PRIVATE_CONSTANT_261 = "openInExplorerToolTip";

   @RBEntry("Seleziona nel Navigatore...")
   public static final String PRIVATE_CONSTANT_262 = "selectInExplorerLabel";

   @RBEntry("S")
   public static final String PRIVATE_CONSTANT_263 = "selectInExplorerMnemonic";

   @RBEntry("Seleziona nel Navigatore...")
   public static final String PRIVATE_CONSTANT_264 = "selectInExplorerToolTip";

   @RBEntry("Seleziona in Creo View")
   public static final String PRIVATE_CONSTANT_265 = "selectInPVLabel";

   @RBEntry("S")
   public static final String PRIVATE_CONSTANT_266 = "selectInPVMnemonic";

   @RBEntry("Seleziona in Navigatore Creo View")
   public static final String PRIVATE_CONSTANT_267 = "selectInPVToolTip";

   @RBEntry("Apri in Creo View")
   public static final String PRIVATE_CONSTANT_268 = "viewInExplorerLabel";

   @RBEntry("Apri in Navigatore Creo View")
   public static final String PRIVATE_CONSTANT_269 = "viewInExplorerToolTip";

   @RBEntry("Visualizza nel navigatore Gantt per Windchill MPMLink")
   public static final String PRIVATE_CONSTANT_270 = "viewInGanttExplorerLabel";

   @RBEntry("Visualizza nel navigatore Gantt per Windchill MPMLink")
   public static final String PRIVATE_CONSTANT_271 = "viewInGanttExplorerToolTip";

   @RBEntry("Struttura")
   public static final String PRIVATE_CONSTANT_272 = "selectedObjectLabel";

   @RBEntry("Struttura")
   public static final String PRIVATE_CONSTANT_273 = "selectedObjectToolTip";

   @RBEntry("Propaga")
   public static final String PRIVATE_CONSTANT_274 = "propagate";

   @RBEntry("Duplica link distinta base")
   public static final String PRIVATE_CONSTANT_275 = "duplicateBomLinks";

   @RBEntry("Duplica struttura")
   public static final String PRIVATE_CONSTANT_276 = "duplicateStructure";

   @RBEntry("Opzioni di aggiunta")
   public static final String PRIVATE_CONSTANT_277 = "pasteAs";

   @RBEntry("Vista a valle")
   public static final String PRIVATE_CONSTANT_278 = "downstreamView";

   @RBEntry("Nome a valle")
   public static final String PRIVATE_CONSTANT_279 = "downstreamName";

   @RBEntry("Numero a valle")
   public static final String PRIVATE_CONSTANT_280 = "downstreamNumber";

   @RBEntry("Versione a valle")
   public static final String PRIVATE_CONSTANT_281 = "downstreamVersion";

   @RBEntry("Unità di default a valle")
   public static final String PRIVATE_CONSTANT_282 = "downstreamDefaultUnit";

   @RBEntry("Tipo a valle")
   public static final String PRIVATE_CONSTANT_283 = "downstreamType";

   @RBEntry("Posizione a valle")
   public static final String PRIVATE_CONSTANT_284 = "downstreamFolder";

   @RBEntry("Tipo distinta base")
   public static final String PRIVATE_CONSTANT_285 = "bomType";

   @RBEntry("Distinta base alternativa")
   public static final String PRIVATE_CONSTANT_286 = "alternateNumber";

   @RBEntry("Configurabile a valle")
   public static final String PRIVATE_CONSTANT_287 = "downstreamGenericType";

   @RBEntry("Configurabile a monte")
   public static final String PRIVATE_CONSTANT_288 = "upstreamGenericType";

   @RBEntry("Rappresentazione")
   public static final String PRIVATE_CONSTANT_289 = "openInExplorerDefaultRepresentationLabel";

   @RBEntry("Rappresentazione")
   public static final String PRIVATE_CONSTANT_290 = "openInExplorerDefaultRepresentationToolTip";

   @RBEntry("Solo parti allocate")
   public static final String PRIVATE_CONSTANT_291 = "openInExplorerAllocatedPartsLabel";

   @RBEntry("Solo parti allocate")
   public static final String PRIVATE_CONSTANT_292 = "openInExplorerAllocatedPartsToolTip";

   @RBEntry("Solo parti allocate")
   public static final String PRIVATE_CONSTANT_293 = "selectInExplorerAllocatedPartsLabel";

   @RBEntry("Solo parti allocate")
   public static final String PRIVATE_CONSTANT_294 = "selectInExplorerAllocatedPartsToolTip";

   @RBEntry("Solo risorse allocate")
   public static final String PRIVATE_CONSTANT_295 = "openInExplorerAllocatedResourcesLabel";

   @RBEntry("Solo risorse allocate")
   public static final String PRIVATE_CONSTANT_296 = "openInExplorerAllocatedResourcesToolTip";

   @RBEntry("Solo risorse allocate")
   public static final String PRIVATE_CONSTANT_297 = "selectInExplorerAllocatedResourcesLabel";

   @RBEntry("Solo risorse allocate")
   public static final String PRIVATE_CONSTANT_298 = "selectInExplorerAllocatedResourcesToolTip";

   @RBEntry("Struttura")
   public static final String PRIVATE_CONSTANT_299 = "openInExplorerSameLabel";

   @RBEntry("Struttura")
   public static final String PRIVATE_CONSTANT_300 = "openInExplorerSameToolTip";

   @RBEntry("Struttura con tutte le allocazioni")
   public static final String PRIVATE_CONSTANT_301 = "openInExplorerSameWithAllAllocationsLabel";

   @RBEntry("Struttura con tutte le allocazioni")
   public static final String PRIVATE_CONSTANT_302 = "openInExplorerSameWithAllAllocationsToolTip";

   @RBEntry("Struttura con parti allocate")
   public static final String PRIVATE_CONSTANT_303 = "openInExplorerSameWithAllocatedPartsLabel";

   @RBEntry("Struttura con parti allocate")
   public static final String PRIVATE_CONSTANT_304 = "openInExplorerSameWithAllocatedPartsToolTip";

   @RBEntry("Struttura con risorse allocate")
   public static final String PRIVATE_CONSTANT_305 = "openInExplorerSameWithAllocatedResourcesLabel";

   @RBEntry("Struttura con risorse allocate")
   public static final String PRIVATE_CONSTANT_306 = "openInExplorerSameWithAllocatedResourcesToolTip";

   @RBEntry("Struttura")
   public static final String PRIVATE_CONSTANT_307 = "selectInExplorerSameLabel";

   @RBEntry("Struttura")
   public static final String PRIVATE_CONSTANT_308 = "selectInExplorerSameToolTip";

   @RBEntry("Struttura")
   public static final String PRIVATE_CONSTANT_309 = "openInExplorerSamePartLabel";

   @RBEntry("Struttura")
   public static final String PRIVATE_CONSTANT_310 = "openInExplorerSamePartToolTip";

   @RBEntry("Parte equivalente a monte")
   public static final String PRIVATE_CONSTANT_311 = "openInExplorerEquivalentPartLabel";

   @RBEntry("Parte equivalente a monte")
   public static final String PRIVATE_CONSTANT_312 = "openInExplorerEquivalentPartToolTip";

   @RBEntry("Istanza collegata")
   public static final String PRIVATE_CONSTANT_313 = "openInExplorerLinkedInstanceLabel";

   @RBEntry("Istanza collegata")
   public static final String PRIVATE_CONSTANT_314 = "openInExplorerLinkedInstanceToolTip";

   @RBEntry("Struttura")
   public static final String PRIVATE_CONSTANT_315 = "selectInExplorerSamePartLabel";

   @RBEntry("Struttura")
   public static final String PRIVATE_CONSTANT_316 = "selectInExplorerSamePartToolTip";

   @RBEntry("Parte equivalente a monte")
   public static final String PRIVATE_CONSTANT_317 = "selectInExplorerEquivalentPartLabel";

   @RBEntry("Parte equivalente a monte")
   public static final String PRIVATE_CONSTANT_318 = "selectInExplorerEquivalentPartToolTip";

   @RBEntry("Padre comune")
   public static final String PRIVATE_CONSTANT_319 = "commonParentLabel";

   @RBEntry("Padre comune")
   public static final String PRIVATE_CONSTANT_320 = "commonParentToolTip";

   @RBEntry(" Salva rappresentazione...")
   public static final String PRIVATE_CONSTANT_321 = "saveRepresentationLabel";

   @RBEntry(" Salva una rappresentazione")
   public static final String PRIVATE_CONSTANT_322 = "saveRepresentationToolTip";

   @RBEntry("Rappresentazioni")
   public static final String PRIVATE_CONSTANT_323 = "DisplayListOfRepresentationsLabel";

   @RBEntry("Rappresentazioni")
   public static final String PRIVATE_CONSTANT_324 = "DisplayListOfRepresentationsToolTip";

   @RBEntry("Caso d'impiego equivalente a valle")
   public static final String PRIVATE_CONSTANT_325 = "downstreamInstanceLabel";

   @RBEntry("Caso d'impiego equivalente a valle")
   public static final String PRIVATE_CONSTANT_326 = "downstreamInstanceToolTip";

   @RBEntry("Caso d'impiego equivalente a monte")
   public static final String PRIVATE_CONSTANT_327 = "upstreamInstanceLabel";

   @RBEntry("Caso d'impiego equivalente a monte")
   public static final String PRIVATE_CONSTANT_328 = "upstreamInstanceToolTip";

   /**
    * Picker labels
    **/
   @RBEntry("Apri gruppo standard...")
   public static final String PRIVATE_CONSTANT_329 = "openMfgStandardLabel";

   @RBEntry("Apre un gruppo standard")
   public static final String PRIVATE_CONSTANT_330 = "openMfgStandardToolTip";

   @RBEntry("Apri risorsa...")
   public static final String PRIVATE_CONSTANT_331 = "openResourceLabel";

   @RBEntry("Apre una risorsa")
   public static final String PRIVATE_CONSTANT_332 = "openResourceToolTip";

   @RBEntry("Apri piano di produzione...")
   public static final String PRIVATE_CONSTANT_333 = "openProcessPlanLabel";

   @RBEntry("Apre il piano di produzione")
   public static final String PRIVATE_CONSTANT_334 = "openProcessPlanToolTip";

   @RBEntry("Etichetta operazione")
   public static final String PRIVATE_CONSTANT_335 = "operationLabel";

   @RBEntry("Tempo di impostazione")
   public static final String PRIVATE_CONSTANT_336 = "setupTimeLabel";

   @RBEntry("Tempo di lavorazione")
   public static final String PRIVATE_CONSTANT_337 = "laborTimeLabel";

   @RBEntry("Tempo di elaborazione")
   public static final String PRIVATE_CONSTANT_338 = "processingTimeLabel";

   @RBEntry("Apri parte...")
   public static final String PRIVATE_CONSTANT_339 = "openPartPickerLabel";

   /**
    * Information tabs labels
    **/
   @RBEntry("Descrizione")
   public static final String PRIVATE_CONSTANT_340 = "descriptionLabel";

   @RBEntry("Descrizione lunga")
   public static final String PRIVATE_CONSTANT_341 = "longDescriptionLabel";

   @RBEntry("Quantità")
   public static final String PRIVATE_CONSTANT_342 = "quantityLabel";

   @RBEntry("Tempo di impostazione")
   public static final String PRIVATE_CONSTANT_343 = "operationSetupTimeLabel";

   @RBEntry("Tempo in coda")
   public static final String PRIVATE_CONSTANT_344 = "operationQueueTimeLabel";

   @RBEntry("Tempo di lavorazione")
   public static final String PRIVATE_CONSTANT_345 = "operationLaborTimeLabel";

   @RBEntry("Tempo di elaborazione")
   public static final String PRIVATE_CONSTANT_346 = "operationProcessingTimeLabel";

   @RBEntry("Tempo di attesa")
   public static final String PRIVATE_CONSTANT_347 = "operationWaitingTimeLabel";

   @RBEntry("Tempo di smantellamento")
   public static final String PRIVATE_CONSTANT_348 = "operationTeardownTimeLabel";

   @RBEntry("Tempo di spostamento")
   public static final String PRIVATE_CONSTANT_349 = "operationMoveTimeLabel";

   @RBEntry("Altri tempi")
   public static final String PRIVATE_CONSTANT_350 = "operationStd8TimeLabel";

   @RBEntry("Costi di impostazione")
   public static final String PRIVATE_CONSTANT_351 = "operationSetupCostLabel";

   @RBEntry("Costi coda")
   public static final String PRIVATE_CONSTANT_352 = "operationQueueCostLabel";

   @RBEntry("Costi di lavorazione")
   public static final String PRIVATE_CONSTANT_353 = "operationLaborCostLabel";

   @RBEntry("Costi di elaborazione")
   public static final String PRIVATE_CONSTANT_354 = "operationProcessingCostLabel";

   @RBEntry("Costi di attesa")
   public static final String PRIVATE_CONSTANT_355 = "operationWaitingCostLabel";

   @RBEntry("Costi di smantellamento")
   public static final String PRIVATE_CONSTANT_356 = "operationTeardownCostLabel";

   @RBEntry("Costi di spostamento")
   public static final String PRIVATE_CONSTANT_357 = "operationMoveCostLabel";

   @RBEntry("Altri costi")
   public static final String PRIVATE_CONSTANT_358 = "operationStd8CostLabel";

   @RBEntry("Inclusi tempi operazione secondaria")
   public static final String PRIVATE_CONSTANT_359 = "subOperationTimeIncludedLabel";

   @RBEntry("Operazione secondaria ERP pubblicata")
   public static final String PRIVATE_CONSTANT_360 = "erpSubOperationPublishedLabel";

   @RBEntry("Operazione secondaria in istruzione di lavorazione")
   public static final String PRIVATE_CONSTANT_361 = "subOperationInWorkInstructionLabel";

   @RBEntry("Appaltata")
   public static final String PRIVATE_CONSTANT_362 = "outSourcedLabel";

   @RBEntry("Codice di convalida ERP")
   public static final String PRIVATE_CONSTANT_363 = "erpValidationCodeLabel";

   @RBEntry("Programmata")
   public static final String PRIVATE_CONSTANT_364 = "scheduledLabel";

   @RBEntry("Necessaria ispezione")
   public static final String PRIVATE_CONSTANT_365 = "inspectionNeededLabel";

   @RBEntry("Intervallo di ispezione")
   public static final String PRIVATE_CONSTANT_366 = "inspectionIntervalLabel";

   @RBEntry("Tempo totale stimato")
   public static final String PRIVATE_CONSTANT_367 = "operationEstimatedTotalTimeLabel";

   @RBEntry("Costo totale stimato")
   public static final String PRIVATE_CONSTANT_368 = "operationEstimatedTotalCostLabel";

   @RBEntry("Standard")
   public static final String PRIVATE_CONSTANT_369 = "standardLabel";

   @RBEntry("Quantità")
   public static final String PRIVATE_CONSTANT_370 = "lotLabel";

   /**
    * Document tab labels
    **/
   @RBEntry("Mostra nelle istruzioni di lavorazione")
   public static final String PRIVATE_CONSTANT_371 = "illustrationLabel";

   @RBEntry("Copia nell'operazione")
   public static final String PRIVATE_CONSTANT_372 = "copyOverLabel";

   /**
    * view menu
    **/
   @RBEntry("Visualizza oggetti di fabbricazione associati")
   public static final String PRIVATE_CONSTANT_373 = "displayManufacturingRelatedItemsToolTip";

   @RBEntry("Visualizza oggetti di fabbricazione associati")
   public static final String PRIVATE_CONSTANT_374 = "displayManufacturingRelatedItemsLabel";

   @RBEntry("Modifica in massa")
   public static final String PRIVATE_CONSTANT_375 = "manufacturingMassChangeToolTip";

   @RBEntry("Modifica in massa")
   public static final String PRIVATE_CONSTANT_376 = "manufacturingMassChangeLabel";

   /**
    * formula tab
    **/
   @RBEntry("Gruppi di formule")
   public static final String PRIVATE_CONSTANT_377 = "formulaSetTabLabel";

   @RBEntry("Gruppi di formule associati")
   public static final String PRIVATE_CONSTANT_378 = "associatedFormulaSetLabel";

   @RBEntry("Aggiungi gruppo di formule")
   public static final String PRIVATE_CONSTANT_379 = "addFormulaSetLabel";

   @RBEntry("Aggiungi gruppo di formule")
   public static final String PRIVATE_CONSTANT_380 = "addFormulaSetToolTip";

   @RBEntry("Gruppo di formule")
   public static final String PRIVATE_CONSTANT_381 = "formulaSetNameLabel";

   @RBEntry("Gruppo di formule di default")
   public static final String PRIVATE_CONSTANT_382 = "defaultFormulaSetNameLabel";

   @RBEntry("Default")
   public static final String PRIVATE_CONSTANT_383 = "isDefaultLabel";

   /**
    * ######## start of DistributionTarget UI changes ##########
    * Attributes of the DistributionTarget in general
    **/
   @RBEntry("Target di distribuzione")
   public static final String PRIVATE_CONSTANT_384 = "distributionTargetsTabLabel";

   @RBEntry("Target di distribuzione associati")
   public static final String PRIVATE_CONSTANT_385 = "associatedDistributionTargetLabel";

   @RBEntry("Aggiungi target di distribuzione")
   public static final String PRIVATE_CONSTANT_386 = "addDistributionTargetLabel";

   @RBEntry("Aggiungi target di distribuzione")
   public static final String PRIVATE_CONSTANT_387 = "addDistributionTargetToolTip";

   @RBEntry("Mostra target di distribuzione")
   public static final String PRIVATE_CONSTANT_388 = "DTLabel";

   @RBEntry("Mostra target di distribuzione")
   public static final String PRIVATE_CONSTANT_389 = "DTToolTip";

   @RBEntry(" Stato di pubblicazione")
   public static final String PRIVATE_CONSTANT_390 = "dtStatusLabel";

   @RBEntry(" Data di pubblicazione")
   public static final String PRIVATE_CONSTANT_391 = "dtDateLabel";

   @RBEntry(" Transazioni ESI correlate")
   public static final String PRIVATE_CONSTANT_392 = "relatedEsiTxnsLabel";

   @RBEntry("  Transazioni ESI correlate")
   public static final String PRIVATE_CONSTANT_393 = "relatedEsiTxnsToolTip";

   /**
    * ########  end of  DistributionTarget UI changes###########
    * ######## [start] Tabbed Pane - ERP Data #########
    **/
   @RBEntry("Dati ERP")
   public static final String PRIVATE_CONSTANT_394 = "erpDataTabLabel";

   @RBEntry("Dati specifici vista")
   public static final String PRIVATE_CONSTANT_395 = "erpPlantSpecificDataTabLabel";

   @RBEntry("Materiale ERP")
   public static final String PRIVATE_CONSTANT_396 = "erpMaterialTableLabel";

   @RBEntry("Revisione")
   public static final String PRIVATE_CONSTANT_397 = "erpMaterialRevisionLabel";

   @RBEntry("Test 1")
   public static final String PRIVATE_CONSTANT_398 = "erpMaterialTest1Label";

   @RBEntry("Test 2")
   public static final String PRIVATE_CONSTANT_399 = "erpMaterialTest2Label";

   @RBEntry("Dati specifici vista")
   public static final String PRIVATE_CONSTANT_400 = "erpPartSpecificPlantDataTableLabel";

   @RBEntry("Vista")
   public static final String PRIVATE_CONSTANT_401 = "erpPartSpecificPlantDataViewLabel";

   @RBEntry("Posizione di archiviazione")
   public static final String PRIVATE_CONSTANT_402 = "erpPartSpecificPlantDataStorageLocationLabel";

   @RBEntry("Test 4")
   public static final String PRIVATE_CONSTANT_403 = "erpPartSpecificPlantDataTest4Label";

   @RBEntry("Aggiungi nuovi dati vista")
   public static final String PRIVATE_CONSTANT_404 = "erpPartSpecificPlantDataAddNewLabel";

   @RBEntry("Aggiungi nuovi dati vista")
   public static final String PRIVATE_CONSTANT_405 = "erpPartSpecificPlantDataAddNewToolTip";

   @RBEntry("Modifica dati vista")
   public static final String PRIVATE_CONSTANT_406 = "erpPartSpecificPlantDataViewEditLabel";

   @RBEntry("Modifica dati vista")
   public static final String PRIVATE_CONSTANT_407 = "erpPartSpecificPlantDataViewEditToolTip";

   /**
    * ######## [end] Tabbed Pane - ERP Data #########
    **/
   @RBEntry("Unità alternative")
   public static final String PRIVATE_CONSTANT_408 = "alternateUnitTabLabel";

   @RBEntry("Unità alternativa")
   public static final String PRIVATE_CONSTANT_409 = "alternateUnitMenuLabel";

   @RBEntry("Unità alternative associate (unità di default = unità alternativa *numeratore/denominatore)")
   public static final String PRIVATE_CONSTANT_410 = "associatedAlternateUnitLabel";

   @RBEntry("Inserisci nuova unità di misura alternativa")
   public static final String PRIVATE_CONSTANT_411 = "addAlternateUnitLabel";

   @RBEntry("Inserisci nuova unità di misura alternativa")
   public static final String PRIVATE_CONSTANT_412 = "addAlternateUnitToolTip";

   @RBEntry("Rimuovi")
   public static final String PRIVATE_CONSTANT_413 = "removeAlternateUnitLabel";

   @RBEntry("Rimuovi")
   public static final String PRIVATE_CONSTANT_414 = "removeAlternateUnitToolTip";

   /**
    * ##overlap time
    **/
   @RBEntry("Tempo di ritardo")
   public static final String PRIVATE_CONSTANT_415 = "overLapTimeLabel";

   /**
    * StepUsageRate Labels
    **/
   @RBEntry("Tasso di utilizzo impostazione")
   public static final String PRIVATE_CONSTANT_416 = "setupUsageRateLabel";

   @RBEntry("Tasso di utilizzo elaborazione")
   public static final String PRIVATE_CONSTANT_417 = "processingUsageRateLabel";

   @RBEntry("Tasso di utilizzo smantellamento")
   public static final String PRIVATE_CONSTANT_418 = "teardownUsageRateLabel";

   @RBEntry("Tasso di utilizzo coda")
   public static final String PRIVATE_CONSTANT_419 = "queueUsageRateLabel";

   @RBEntry("Tasso di utilizzo spostamento")
   public static final String PRIVATE_CONSTANT_420 = "moveUsageRateLabel";

   @RBEntry("Tasso di utilizzo attesa")
   public static final String PRIVATE_CONSTANT_421 = "waitingUsageRateLabel";

   @RBEntry("Altro tasso di utilizzo")
   public static final String PRIVATE_CONSTANT_422 = "otherUsageRateLabel";

   /**
    * ##ConfigSpec Panel
    **/
   @RBEntry("Tipo distinta base")
   public static final String PRIVATE_CONSTANT_423 = "variation1Label";

   @RBEntry("Numero alternativo")
   public static final String PRIVATE_CONSTANT_424 = "variation2Label";

   /**
    * ##Selected Menu
    **/
   @RBEntry("Imposta stato del ciclo di vita")
   public static final String PRIVATE_CONSTANT_425 = "setStateLabel";

   @RBEntry("Imposta stato del ciclo di vita")
   public static final String PRIVATE_CONSTANT_426 = "setStateLabelToolTip";

   @RBEntry("Promuovi")
   public static final String PRIVATE_CONSTANT_427 = "promoteLablel";

   @RBEntry("Promuovi")
   public static final String PRIVATE_CONSTANT_428 = "promoteToolTip";

   @RBEntry("Aggiorna parte equivalente")
   public static final String PRIVATE_CONSTANT_429 = "updateEquivalenceLinkLabel";

   @RBEntry("Aggiorna parte equivalente")
   public static final String PRIVATE_CONSTANT_430 = "updateEquivalenceLinkToolTip";

   @RBEntry("Duplica associazioni assieme e allocazioni correlate")
   public static final String PRIVATE_CONSTANT_431 = "duplicateAllocationsLabel";

   @RBEntry("Duplica")
   public static final String PRIVATE_CONSTANT_432 = "duplicatePPLabel";

   @RBEntry("Duplica il piano di produzione")
   public static final String PRIVATE_CONSTANT_433 = "duplicatePPToolTip";

   @RBEntry("Parte equivalente a valle")
   public static final String PRIVATE_CONSTANT_434 = "openInExplorerDownstreamEquivalentPartLabel";

   @RBEntry("Parte equivalente a valle")
   public static final String PRIVATE_CONSTANT_435 = "openInExplorerDownstreamEquivalentPartToolTip";

   @RBEntry("Parte equivalente a valle")
   public static final String PRIVATE_CONSTANT_436 = "selectInExplorerDownstreamEquivalentPartLabel";

   @RBEntry("Parte equivalente a valle")
   public static final String PRIVATE_CONSTANT_437 = "selectInExplorerDownstreamEquivalentPartToolTip";

   @RBEntry("Non aggiornato")
   public static final String PRIVATE_CONSTANT_438 = "outOfDateItrLabel";

   @RBEntry("Aggiorna tabella")
   public static final String PRIVATE_CONSTANT_439 = "refreshTableLabel";

   @RBEntry("Aggiorna le tabelle con le ultime discrepanze")
   public static final String PRIVATE_CONSTANT_440 = "refreshTableToolTip";

   @RBEntry("Filtra tabella di riconciliazione")
   public static final String PRIVATE_CONSTANT_441 = "filterTableRowsLabel";

   @RBEntry("Filtra tabella di riconciliazione")
   public static final String PRIVATE_CONSTANT_442 = "filterTableRowsToolTip";

   @RBEntry("Evidenzia discrepanza selezionata nel contesto equivalente successivo")
   public static final String PRIVATE_CONSTANT_443 = "gotToNextContextLabel";

   @RBEntry("Evidenzia discrepanza selezionata nel contesto equivalente successivo")
   public static final String PRIVATE_CONSTANT_444 = "gotToNextContextToolTip";

   @RBEntry("Seleziona utilizzo successivo")
   public static final String PRIVATE_CONSTANT_445 = "selectNextUsageLabel";

   @RBEntry("Seleziona utilizzo successivo")
   public static final String PRIVATE_CONSTANT_446 = "selectNextUsageToolTip";

   @RBEntry("Evidenzia discrepanza selezionata")
   public static final String PRIVATE_CONSTANT_447 = "HighlightInExplorerLabel";

   @RBEntry("Evidenzia discrepanza selezionata")
   public static final String PRIVATE_CONSTANT_448 = "HighlightInExplorerToolTip";

   @RBEntry("Rimuovi parte selezionata")
   public static final String PRIVATE_CONSTANT_449 = "removeSelectedPartLabel";

   @RBEntry("Rimuovi parte selezionata")
   public static final String PRIVATE_CONSTANT_450 = "removeSelectedPartToolTip";

   @RBEntry("Assembla senza modificare")
   public static final String PRIVATE_CONSTANT_451 = "BOMReconBTALabel";

   @RBEntry("Assembla senza modificare")
   public static final String PRIVATE_CONSTANT_452 = "BOMReconBTAToolTip";

   @RBEntry("Ultima iterazione non collegata di parti equivalenti a monte corrispondenti al filtro")
   public static final String PRIVATE_CONSTANT_453 = "outOfDateEquivalentTo";

   @RBEntry("Ultima iterazione non collegata di parti equivalenti a valle corrispondenti al filtro")
   public static final String PRIVATE_CONSTANT_454 = "outOfDateEquivalentWith";

   @RBEntry("Commento")
   public static final String PRIVATE_CONSTANT_455 = "commentLabel";

   @RBEntry("Parti")
   public static final String PRIVATE_CONSTANT_456 = "displayListLabel";

   @RBEntry("Qtà totale")
   public static final String PRIVATE_CONSTANT_457 = "totalQtyLabel";

   @RBEntry("Assieme")
   public static final String PRIVATE_CONSTANT_458 = "isAssemblyLabel";

   @RBEntry("Proprietario")
   public static final String PRIVATE_CONSTANT_459 = "modelItemContainerLabel";

   @RBEntry("Tipo")
   public static final String PRIVATE_CONSTANT_460 = "modelItemTypeLabel";

   @RBEntry("Nome")
   public static final String PRIVATE_CONSTANT_461 = "modelItemNameLabel";

   @RBEntry("Caratteristica di controllo")
   public static final String PRIVATE_CONSTANT_462 = "modelItemCCLabel";

   @RBEntry("Elemento modello")
   public static final String PRIVATE_CONSTANT_463 = "modelItemLabel";

   @RBEntry("Nuove caratteristiche di controllo")
   public static final String PRIVATE_CONSTANT_464 = "newMultipleCCLabel";

   @RBEntry("Nuove caratteristiche di controllo")
   public static final String PRIVATE_CONSTANT_465 = "newMultipleCCToolTip";
   
   @RBEntry("Modifica proprietà caratteristica di controllo")
   public static final String PRIVATE_CONSTANT_466 = "editCCPropertiesLabel";

   @RBEntry("Modifica proprietà caratteristica di controllo")
   public static final String PRIVATE_CONSTANT_467 = "editCCPropertiesToolTip";   
   
   @RBEntry("Modifica relazione elemento modello")
   public static final String PRIVATE_CONSTANT_468 = "editModelItemOnCCLabel";

   @RBEntry("Modifica relazione elemento modello")
   public static final String PRIVATE_CONSTANT_469 = "editModelItemOnCCToolTip";      
   
   @RBEntry("Associazione")
   public static final String PRIVATE_CONSTANT_470 = "associationLabel";
   
   @RBEntry("Seleziona caratteristiche di controllo in Creo View")
   public static final String PRIVATE_CONSTANT_471 = "selectCCInPVLabel";

   @RBEntry("Seleziona caratteristiche di controllo in Navigatore Creo View")
   public static final String PRIVATE_CONSTANT_472 = "selectCCInPVToolTip";
   
   @RBEntry("Finestra di selezione per la creazione")
   public static final String PRIVATE_CONSTANT_473 = "doCreateLabel";
   
   @RBEntry("Nome elemento modello")
   public static final String PRIVATE_CONSTANT_474 = "multiCreateModelItemNameLabel";
   
   @RBEntry("Estendi equivalenza distinta base...")
   public static final String PRIVATE_CONSTANT_475 = "extendEquivalenceLabel";

   @RBEntry("Estendi equivalenza distinta base")
   public static final String PRIVATE_CONSTANT_476 = "extendEquivalenceToolTip";

   @RBEntry("Duplica piano di produzione:")
   public static final String PRIVATE_CONSTANT_477 = "duplicateProcessPlan";

   @RBEntry("Estendi equivalenza distinta base di più assiemi...")
   public static final String PRIVATE_CONSTANT_478 = "extendEquivalenceBatchLabel";

   @RBEntry("Estende l'equivalenza della distinta base di più assiemi mediante relativa ricerca")
   public static final String PRIVATE_CONSTANT_479 = "extendEquivalenceBatchToolTip";

   @RBEntry("Nuove parti a valle")
   public static final String PRIVATE_CONSTANT_newDownstreamPartBatchLabel = "newDownstreamPartBatchLabel";

   @RBEntry("Cerca parti a monte e crea nuove parti a valle")
   public static final String PRIVATE_CONSTANT_newDownstreamPartBatchToolTip = "newDownstreamPartBatchToolTip";

   @RBEntry("Nuove viste a valle")
   public static final String PRIVATE_CONSTANT_newDownstreamViewBatchLabel = "newDownstreamViewBatchLabel";

   @RBEntry("Cerca parti a monte e crea nuove viste a valle")
   public static final String PRIVATE_CONSTANT_newDownstreamViewBatchToolTip = "newDownstreamViewBatchToolTip";

   @RBEntry("Duplica varianti vista a valle")
   public static final String PRIVATE_CONSTANT_newDownstreamViewVariantsLabel = "newDownstreamViewVariantsLabel";

   @RBEntry("Duplica varianti vista a valle...")
   public static final String PRIVATE_CONSTANT_newDownstreamViewVariantsToolTip = "newDownstreamViewVariantsToolTip";

   @RBEntry("Duplica varianti parte a valle")
   public static final String PRIVATE_CONSTANT_newDownstreamPartVariantsLabel = "newDownstreamPartVariantsLabel";

   @RBEntry("Duplica varianti parte a valle...")
   public static final String PRIVATE_CONSTANT_newDownstreamPartVariantsToolTip = "newDownstreamPartVariantsToolTip";
}
