package config.logicrepository.xml.explorer.productstructure;

import wt.util.resource.*;

@RBUUID("config.logicrepository.xml.explorer.productstructure.ConfigurationResource")
public final class ConfigurationResource_it extends WTListResourceBundle {
   @RBEntry("Doesn't Matter")
   public static final String PRIVATE_CONSTANT_0 = "ImNotUsedButNeedToBeHereForSomeJavaVMs";

   @RBEntry("Informazioni su Navigatore struttura di prodotto")
   public static final String PRIVATE_CONSTANT_1 = "aboutLabel";

   @RBEntry("Visualizza la finestra delle informazioni sul Navigatore struttura di prodotto")
   public static final String PRIVATE_CONSTANT_2 = "aboutToolTip";
}
