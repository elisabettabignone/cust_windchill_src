/* bcwti
 *
 * Copyright (c) 2010 Parametric Technology Corporation (PTC). All Rights Reserved.
 *
 * This software is the confidential and proprietary information of PTC
 * and is subject to the terms of a software license agreement. You shall
 * not disclose such confidential information and shall use it only in accordance
 * with the terms of the license agreement.
 *
 * ecwti
 */
package com.ptc.windchill.explorer.structureexplorer.renderer.component.gps.table;

import wt.util.resource.*;

@RBUUID("com.ptc.windchill.explorer.structureexplorer.renderer.component.gps.table.tableResource")
public final class tableResource extends WTListResourceBundle {
   @RBEntry("Row")
   @RBComment("Info to add to Changes recorded to display additional details when adding and removing rows.")
   public static final String CASE_TABLE_ROW_CRE_INFO = "CTR";
}
