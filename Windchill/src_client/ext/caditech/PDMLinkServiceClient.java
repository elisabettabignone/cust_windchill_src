package ext.caditech;

// import the classes generated when compiling your client
import java.io.StringWriter;
import java.net.Authenticator;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.util.List;
import java.util.Properties;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import com.ptc.jws.client.handler.Credentials;

public class PDMLinkServiceClient {

  /**
   * Main da poter usare per provare il web service da linea di comando
   * 
   * @param args
   * @throws java.lang.Exception
   */
  public static void main(String[] args) throws java.lang.Exception {

    Properties p = new Properties();
    // parsa le property
    for (int i = 0; i < args.length; i++) {
      String[] coppia = args[i].split("=");
      if (coppia.length != 2) {
        System.out.println("E' stata inserito un argomento non ben formattato: " + args[i]);
        System.out.println("Gli argomenti devono avere la forma : chiave=valore");
        System.exit(0);
      }
      String chiave = coppia[0];
      String valore = coppia[1];
      System.out.println("Inserita property: " + chiave + "-->" + valore);
      p.setProperty(chiave, valore);
    }
    if (!p.containsKey("modo") || !p.containsKey("url") || !p.containsKey("user") || !p.containsKey("pass")) {
      scriviSintassi();
    }

    // imposta user e password
    String url = p.getProperty("url");
    String user = p.getProperty("user");
    String pass = p.getProperty("pass");

    // depending on your security requirements you may need to specify credentials up
    // front, or on the command-line where most policies will prompt for them
    Credentials.setUsername(user);
    Credentials.setPassword(pass);

    // PDMLinkServiceService service = new PDMLinkServiceService();
    // PDMLinkService port = service.getPDMLinkServicePort();
    PDMLinkService port = PDMLinkServiceClient.getService(url, user, pass);

    String avvio = p.getProperty("modo");

    StringWriter sw = new StringWriter();
    p.store(sw, null);
    String properties = sw.toString();
    // Per la build di ANT, riceve dal web service delle List<String>, anche se i metodi sono segnati come
    // ArrayList<String>
    // di conseguenza per renderli "compatibili" i risultati sono List<String> invece che ArrayList<String>
    List<String> result = null;
    if (avvio.equalsIgnoreCase("cercaPerAttributo")) {
      result = port.cercaDocumentoPerAttributo(p.getProperty("nome"), p.getProperty("valore"));
    }
    else if (avvio.equalsIgnoreCase("checkin")) {
      result = port.eseguiCheckin(p.getProperty("modelli"));
    }
    else if (avvio.equalsIgnoreCase("checkout")) {
      String modelli = p.getProperty("modelli");
      String ws = p.getProperty("workspace");
      String utente = p.getProperty("login");
      result = port.eseguiCheckout(modelli, ws, utente);
    }
    else if (avvio.equalsIgnoreCase("copia")) {
      result = port.eseguiCopiaLocale(properties);
    }
    else if (avvio.equalsIgnoreCase("undocheckout")) {
      String ws = p.getProperty("workspace");
      String utente = p.getProperty("login");
      String modelli = p.getProperty("modelli");
      result = port.eseguiUndoCheckout(modelli, ws, utente);
    }
    else if (avvio.equalsIgnoreCase("cercacontenutoworkspace")) {
      String ws = p.getProperty("workspace");
      String utente = p.getProperty("login");

      result = port.cercaContenutoWorkspace(ws, utente);
    }
    else if (avvio.equalsIgnoreCase("ischeckedout")) {
      boolean b = port.isCheckedOut(p.getProperty("nome"));
      if (b)
        System.out.println("Il documento " + p.getProperty("nome") + " � in checkout");
      else
        System.out.println("Il documento " + p.getProperty("nome") + " NON � in checkout");
      System.exit(0);
    }
    if (result != null) {
      System.out.println("Risultati dell'invocazione del metodo " + avvio);
      System.out.println("Numero elementi:" + result.size());
      int i = 0;
      for (String res : result) {
        System.out.println(i + ")" + res);
      }
    }
    else {
      System.out.println("Il risultato dell'invocazione del metodo " + avvio + " � NULL");
    }

    System.exit(0);
  }

  private static void scriviSintassi() {
    String msg = "";
    System.out.println("Devi passare url, utente e password per il remote method server di windchill:");
    System.out.println("url=http://trasfor-lab.caditech.ge/Windchill/servlet/PDMLinkService?wsdl, user=utente e pass=password\n");

    System.out.println("Devi passare l'argomento per selezionare il metodo da invocare tramite la property:");
    System.out.println("modo=cercaPerAttributo/checkin/checkout/copia/undocheckout/ischeckedout\n");
    System.out.println("A seconda del metodo da lanciare sono da passare alcuni argomenti nella forma chiave=valore ");

    System.out.println("\nCERCAPERATTRIBUTO");
    msg = "Nome  dell'attributo ";
    System.out.println("nome" + " --> " + msg);
    msg = "Valore  dell'attributo ";
    System.out.println("valore" + " --> " + msg);

    System.out.println("\nISCHECKEDOUT");
    msg = "Nome  del documento ";
    System.out.println("nome" + " --> " + msg);

    System.out.println("\nCHECKIN");
    msg = "La lista dei modelli da popolare, separati da ; ";
    System.out.println("modelli" + " --> " + msg);

    System.out.println("\nCHECKOUT");
    msg = "La lista dei modelli da popolare, separati da ; ";
    System.out.println("modelli" + " --> " + msg);
    msg = "Opzione che indica il nome del workspace";
    System.out.println("workspace" + " --> " + msg);
    msg = "Login dell'utente a cui viene assegnato il checkout";
    System.out.println("login" + " --> " + msg);

    System.out.println("\nCOPIA");
    msg = "La lista dei modelli da popolare, separati da ; ";
    System.out.println("modelli" + " --> " + msg);
    msg = "Opzione per popolare anche i drawing. Pu� valere TRUE o FALSE, default FALSE ";
    System.out.println("[drawing]" + " --> " + msg);
    msg = "Opzione per il percorso del file di report, default C:\\temp\\checkout_windchill.txt";
    System.out.println("[report]" + " --> " + msg);
    msg = "Opzione per il tipo di checkout da effettuare. Pu� valere ASSTORED o LATEST, default LATEST ";
    System.out.println("[asstored]" + " --> " + msg);
    msg = "Opzione per lo svuotamento del WS nel caso esista gi�. Pu� valere TRUE o FALSE, default TRUE ";
    System.out.println("[eliminaws]" + " --> " + msg);
    msg = "Opzione che indica il nome del workspace, default ws_default ";
    System.out.println("[workspace]" + " --> " + msg);
    msg = "Opzione per il tipo di dipendenze da considerare. Pu� valere ALL, REQUIRED o NONE, default REQUIRED ";
    System.out.println("[dipendenze]" + " --> " + msg);
    msg = "Login utilizzata per cercare/creare il workspace, default Administrator ";
    System.out.println("[login]" + " --> " + msg);

    System.out.println("\nUNDOCHECKOUT");
    msg = "La lista dei modelli di cui fare undo checkout e restore nel workspace, separati da ; ";
    System.out.println("modelli" + " --> " + msg);
    msg = "Opzione che indica il nome del workspace";
    System.out.println("workspace" + " --> " + msg);
    msg = "Login dell'utente proprietario del workspace";
    System.out.println("login" + " --> " + msg);

    System.out.println("\nCERCACONTENUTOWORKSPACE");
    msg = "Opzione che indica il nome del workspace";
    System.out.println("workspace" + " --> " + msg);
    msg = "Login dell'utente proprietario del workspace";
    System.out.println("login" + " --> " + msg);
    System.exit(0);
  }

  /**
   * Metodo che restituisce l'oggetto per richiamare i metodi messi a disposizione dal web service PDMLinkService
   * 
   * @param urlString
   *          url del wsdl del web service (es. http://trasfor-lab.caditech.ge/Windchill/servlet/PDMLinkService?wsdl )
   * @param nome
   *          nome per accedere ai servizi Windchill (es. wcadmin)
   * @param password
   *          password per accedere ai servizi di Windchill (es. wcadmin)
   * @return PDMLinkService oggetto per richiamre i metodi del web service
   * @throws MalformedURLException
   *           nel caso l'url non sia scritto correttamente
   */
  public static PDMLinkService getService(String urlString, final String nome, final String password) throws MalformedURLException {

    Authenticator.setDefault(new Authenticator() {

      @Override
      protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(nome, password.toCharArray());
      }
    });
    Credentials.setUsername(nome);
    Credentials.setPassword(password);
    URL url = new URL(urlString);
    QName name = new QName("http://caditech.ext/", "PDMLinkServiceService");
    Service service = Service.create(url, name);
    PDMLinkService port = service.getPort(PDMLinkService.class);
    return port;
  }
}