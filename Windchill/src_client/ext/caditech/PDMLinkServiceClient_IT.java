package ext.caditech;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.StringWriter;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Properties;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

//Running test cases in order of method names in ascending order
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PDMLinkServiceClient_IT {

  PDMLinkService port = null;

  @Before
  public void setUp() {

    try {
      port = PDMLinkServiceClient.getService("http://trasfor-lab.caditech.ge/Windchill/servlet/PDMLinkService?wsdl", "wcadmin", "wcadmin");
    }
    catch (MalformedURLException e) {
      e.printStackTrace();
      fail();
    }
  }

  @Test
  public void Test_00_CercaPerAttributo() {
    String nome = "CODICE";
    String valore = "parall_prova__";
    List<String> result = null;
    try {
      result = port.cercaDocumentoPerAttributo(nome, valore);
    }
    catch (SearchUtilsException_Exception e) {
      fail();
      e.printStackTrace();
    }
    assertTrue(result.size() == 1);
    assertTrue(result.get(0).equalsIgnoreCase("PARALL_PROVA.PRT"));
  }

  @Test
  public void Test_01_CopiaLocale() {

    // mi collego come pdmadmin
    try {
      PDMLinkServiceClient.getService("http://trasfor-lab.caditech.ge/Windchill/servlet/PDMLinkService?wsdl", "pdmadmin", "pdmadmin");
    }
    catch (MalformedURLException e) {
      e.printStackTrace();
      fail();
    }
    Properties p = new Properties();
    p.put("test", "true");
    p.put("modelli", "ASSIEME_ES5.ASM");
    p.put("dipendenze", "ALL");
    p.put("eliminaws", "true");
    p.put("workspace", "ws_test_checkout");
    p.put("asstored", "latest");
    p.put("drawing", "false");
    p.put("login", "pdmadmin");

    StringWriter sw = new StringWriter();
    try {
      p.store(sw, null);
      List<String> result = port.eseguiCopiaLocale(sw.toString());
      assertTrue(result.size() == 3);
      assertTrue(result.contains("assieme_es5.asm") || result.contains("ASSIEME_ES5.ASM"));
      assertTrue(result.contains("cilindro.prt") || result.contains("CILINDRO.PRT"));
      assertTrue(result.contains("parall.prt") || result.contains("PARALL.PRT"));
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }

  }

  @Test
  public void Test_02_Checkout() {
    String modelli = "cilindro.prt;parall.prt";
    String ws = "ws_test_checkout";
    String utente = "pdmadmin";
    try {
      List<String> result = port.eseguiCheckout(modelli, ws, utente);
      assertEquals(2, result.size());
      assertTrue(result.contains("cilindro.prt") || result.contains("CILINDRO.PRT"));
      assertTrue(result.contains("parall.prt") || result.contains("PARALL.PRT"));
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }

  }

  @Test
  public void Test_03_IsCheckedOut() {
    try {
      boolean b1 = port.isCheckedOut("parall.prt");
      assertTrue(b1);
      boolean b2 = port.isCheckedOut("assieme_es5.asm");
      assertFalse(b2);
      boolean b3 = port.isCheckedOut("cilindro.prt");
      assertTrue(b3);
    }
    catch (SearchUtilsException_Exception e) {
      e.printStackTrace();
      fail();
    }
  }

  @Test
  public void Test_04_Checkin() {

    try {
      List<String> result = port.eseguiCheckin("cilindro.prt");
      assertTrue(result.size() == 1);
      assertTrue(result.contains("cilindro.prt") || result.contains("CILINDRO.PRT"));
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }

  }

  @Test
  public void Test_05_IsCheckedOut() {
    boolean b;
    try {
      b = port.isCheckedOut("cilindro.prt");
      assertFalse(b);
      b = port.isCheckedOut("parall.prt");
      assertTrue(b);
    }
    catch (SearchUtilsException_Exception e) {
      e.printStackTrace();
      fail();
    }

  }

  @Test
  public void Test_06_UndoCheckout() {
    try {
      List<String> result = port.eseguiUndoCheckout("parall.prt", "ws_test_checkout", "pdmadmin");
      assertTrue(result.size() == 1);
      assertTrue(result.contains("parall.prt") || result.contains("PARALL.PRT"));
    }
    catch (SearchUtilsException_Exception e) {
      e.printStackTrace();
      fail();
    }

  }

  @Test
  public void Test_07_CopiaLocale_Ut2() {

    // mi collego come ut1
    try {
      PDMLinkServiceClient.getService("http://trasfor-lab.caditech.ge/Windchill/servlet/PDMLinkService?wsdl", "UT2", "ut2");
    }
    catch (MalformedURLException e) {
      e.printStackTrace();
      fail();
    }

    // eseguo la copia in locale come ut1
    Properties p = new Properties();
    p.put("test", "true");
    p.put("modelli", "PARALL.PRT");
    p.put("dipendenze", "NONE");
    p.put("eliminaws", "true");
    p.put("workspace", "ws_test_checkout");
    p.put("asstored", "latest");
    p.put("drawing", "false");
    p.put("login", "UT2");

    StringWriter sw = new StringWriter();
    try {
      p.store(sw, null);
      List<String> result = port.eseguiCopiaLocale(sw.toString());
      assertTrue(result.size() == 1);
      assertTrue(result.contains("parall.prt") || result.contains("PARALL.PRT"));
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }
  }

  @Test
  public void Test_08_CopiaLocale_Checkout_pdmadmin() {

    // mi collego come pdmadmin
    try {
      PDMLinkServiceClient.getService("http://trasfor-lab.caditech.ge/Windchill/servlet/PDMLinkService?wsdl", "pdmadmin", "pdmadmin");
    }
    catch (MalformedURLException e) {
      e.printStackTrace();
      fail();
    }

    // eseguo la copia in locale e il checkout di un modello come pdmadmin
    Properties p = new Properties();

    p.put("test", "true");
    p.put("modelli", "PARALL.PRT");
    p.put("dipendenze", "NONE");
    p.put("eliminaws", "true");
    p.put("workspace", "ws_test_checkout");
    p.put("asstored", "latest");
    p.put("drawing", "false");
    p.put("login", "pdmadmin");

    StringWriter sw = new StringWriter();
    try {
      p.store(sw, null);
      List<String> result = port.eseguiCopiaLocale(sw.toString());
      assertTrue(result.size() == 1);
      assertTrue(result.contains("parall.prt") || result.contains("PARALL.PRT"));
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }

    String modelli = "parall.prt";
    String ws = "ws_test_checkout";
    String utente = "pdmadmin";
    try {
      List<String> result = port.eseguiCheckout(modelli, ws, utente);
      assertTrue(result.size() == 1);
      assertTrue(result.contains("parall.prt") || result.contains("PARALL.PRT"));
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }
  }

  @Test
  public void Test_09_CopiaLocale_Ut1() {

    // mi collego come ut1
    try {
      PDMLinkServiceClient.getService("http://trasfor-lab.caditech.ge/Windchill/servlet/PDMLinkService?wsdl", "UT1", "ut1");
    }
    catch (MalformedURLException e) {
      e.printStackTrace();
      fail();
    }

    // eseguo la copia in locale come ut1
    Properties p = new Properties();
    p.put("test", "true");
    p.put("modelli", "PARALL.PRT");
    p.put("dipendenze", "NONE");
    p.put("eliminaws", "true");
    p.put("workspace", "ws_test_checkout");
    p.put("asstored", "latest");
    p.put("drawing", "false");
    p.put("login", "UT1");

    StringWriter sw = new StringWriter();
    try {
      p.store(sw, null);
      List<String> result = port.eseguiCopiaLocale(sw.toString());
      assertTrue(result.size() == 1);
      assertTrue(result.contains("parall.prt") || result.contains("PARALL.PRT"));
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }
  }

  @Test
  public void Test_10_UndoCheckout() {
    try {
      List<String> result = port.eseguiUndoCheckout("parall.prt", "ws_test_checkout", "pdmadmin");
      assertEquals(1, result.size());
      assertTrue(result.contains("parall.prt") || result.contains("PARALL.PRT"));

      boolean b = port.isCheckedOut("parall.prt");
      assertFalse(b);

      List<String> contenutoWS = port.cercaContenutoWorkspace("ws_test_checkout", "pdmadmin");
      assertEquals(1, contenutoWS.size());
      assertTrue(contenutoWS.contains("parall.prt") || contenutoWS.contains("PARALL.PRT"));

      List<String> contenutoWSUT2 = port.cercaContenutoWorkspace("ws_test_checkout", "UT2");
      assertEquals(1, contenutoWSUT2.size());
      assertTrue(contenutoWSUT2.contains("parall.prt") || contenutoWSUT2.contains("PARALL.PRT"));

      List<String> contenutoWSUT1 = port.cercaContenutoWorkspace("ws_test_checkout", "UT1");
      assertEquals(1, contenutoWSUT1.size());
      assertTrue(contenutoWSUT1.contains("parall.prt") || contenutoWSUT1.contains("PARALL.PRT"));

      // il test fallisce perche se UT1 tramite web service si fa add to ws dopo che un altro utente ha fatto il
      // checkout
      // dello stesso file, nel momento in cui vierne fatto undocheckout il file viene cancellato dal ws dell'utente UT1
      // nel ws di UT2 il file invece c'� perch� ha fatto add to ws prima che il file fosse messo in checkout
    }
    catch (SearchUtilsException_Exception e) {
      e.printStackTrace();
      fail();
    }
  }

  @Test
  public void Test_11_Copia_Checkout_Ut1() {

    // mi collego come ut1
    try {
      PDMLinkServiceClient.getService("http://trasfor-lab.caditech.ge/Windchill/servlet/PDMLinkService?wsdl", "UT1", "ut1");
    }
    catch (MalformedURLException e) {
      e.printStackTrace();
      fail();
    }

    // eseguo la copia in locale come ut1
    Properties p = new Properties();
    p.put("test", "true");
    p.put("modelli", "PARALL.PRT");
    p.put("dipendenze", "NONE");
    p.put("eliminaws", "true");
    p.put("workspace", "ws_test_checkout");
    p.put("asstored", "latest");
    p.put("drawing", "false");
    p.put("login", "UT1");

    StringWriter sw = new StringWriter();
    try {
      p.store(sw, null);
      List<String> result = port.eseguiCopiaLocale(sw.toString());
      assertTrue(result.size() == 1);
      assertTrue(result.contains("parall.prt") || result.contains("PARALL.PRT"));
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }

    // esegue il checkout come UT1
    String modelli = "parall.prt";
    String ws = "ws_test_checkout";
    String utente = "UT1";
    try {
      List<String> result = port.eseguiCheckout(modelli, ws, utente);
      assertEquals(1, result.size());
      assertTrue(result.contains("parall.prt") || result.contains("PARALL.PRT"));
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }
  }

  @Test
  public void Test_12_Copia_pdmadmin() {

    // mi collego come pdmadmin
    try {
      PDMLinkServiceClient.getService("http://trasfor-lab.caditech.ge/Windchill/servlet/PDMLinkService?wsdl", "pdmadmin", "pdmadmin");
    }
    catch (MalformedURLException e) {
      e.printStackTrace();
      fail();
    }

    // verifica se il file � in checkout
    boolean b = false;
    try {
      b = port.isCheckedOut("parall.prt");
    }
    catch (SearchUtilsException_Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    assertTrue(b);

    // eseguo la copia in locale come pdmadmin
    Properties p = new Properties();
    p.put("test", "true");
    p.put("modelli", "PARALL.PRT");
    p.put("dipendenze", "NONE");
    p.put("eliminaws", "true");
    p.put("workspace", "ws_test_checkout");
    p.put("asstored", "latest");
    p.put("drawing", "false");
    p.put("login", "pdmadmin");

    StringWriter sw = new StringWriter();
    try {
      p.store(sw, null);
      List<String> result = port.eseguiCopiaLocale(sw.toString());
      assertTrue(result.size() == 1);
      assertTrue(result.contains("parall.prt") || result.contains("PARALL.PRT"));
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }

    // eseguo nuovamente la copia in locale come pdmadmin, in modo da forzare lo svuotamento del workspace
    try {
      List<String> result = port.eseguiCopiaLocale(sw.toString());
      assertTrue(result.size() == 1);
      assertTrue(result.contains("parall.prt") || result.contains("PARALL.PRT"));
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }

    // verifica se dopo la cancellazione il file � ancora in checkout
    try {
      b = port.isCheckedOut("parall.prt");
    }
    catch (SearchUtilsException_Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    assertTrue(b);
  }

  @Test
  // esegue l'undo del checkout eseguito da UT1
  public void Test_13_verificaCheckoutUT1() {
    // mi collego come ut1
    try {
      PDMLinkServiceClient.getService("http://trasfor-lab.caditech.ge/Windchill/servlet/PDMLinkService?wsdl", "UT1", "ut1");
    }
    catch (MalformedURLException e) {
      e.printStackTrace();
      fail();
    }
    try {
      boolean b = port.isCheckedOut("parall.prt");
      assertTrue(b);

      List<String> contenutoUT1 = port.cercaContenutoWorkspace("ws_test_checkout", "UT1");
      assertTrue(contenutoUT1.size() == 1);
      assertTrue(contenutoUT1.contains("parall.prt") || contenutoUT1.contains("PARALL.PRT"));
    }
    catch (SearchUtilsException_Exception e) {
      e.printStackTrace();
      fail();
    }
  }

  @Test
  public void Test_14_UndoCheckout_UT1() {
    // mi collego come ut1
    try {
      PDMLinkServiceClient.getService("http://trasfor-lab.caditech.ge/Windchill/servlet/PDMLinkService?wsdl", "UT1", "ut1");
    }
    catch (MalformedURLException e) {
      e.printStackTrace();
      fail();
    }
    try {
      List<String> result = port.eseguiUndoCheckout("parall.prt", "ws_test_checkout", "UT1");
      assertEquals(1, result.size());
      assertTrue(result.contains("parall.prt") || result.contains("PARALL.PRT"));
    }
    catch (SearchUtilsException_Exception e) {
      e.printStackTrace();
      fail();
    }

  }
}
