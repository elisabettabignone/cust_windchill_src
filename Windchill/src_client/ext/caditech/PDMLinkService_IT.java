package ext.caditech;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

/**
 * Classe per testare il funzionamento del web service su pi� macchine utilizzando sempre lo stesso jar del client.
 * 
 * 
 * Vengono lanciati test del checkout, isCheckedOut, undoCheckout sia su TRASFOR-LAB, sia su SRV-E80CADASDEV (per il
 * test test_srv_e80cadasdev � necessario essere collegati alla VPN di E80)
 * 
 * @author administrator
 *
 */
public class PDMLinkService_IT {

  /**
   * TEST SU TRASFOR-LAB
   */
  @Test
  public void test_trasfor_lab() {
    PDMLinkService port = null;
    try {
      port = PDMLinkServiceClient.getService("http://trasfor-lab.caditech.ge/Windchill/servlet/PDMLinkService?wsdl", "wcadmin", "wcadmin");

      String nomeModello = "cilindro_prova.prt";
      test(port, nomeModello);
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }
  }

  /**
   * TEST sulla macchina SRV-E80CADASDEV di E80 di TEST
   */
  @Test
  public void test_srv_e80cadasdev() {
    PDMLinkService port = null;
    try {
      port = PDMLinkServiceClient.getService("http://srv-e80cadasdev.elettric80.lan/Windchill/protocolAuth/servlet/PDMLinkService?wsdl", "wcadmin",
          "wcadmin");
      String nomeModello = "b359001852.asm";
      test(port, nomeModello);
    }
    catch (Exception e) {
      e.printStackTrace();
      fail();
    }
  }

  /**
   * Test
   * 
   * @param port
   *          oggetto PDMLinkService per accedere al web service
   * @param nomeModello
   *          nome del modello su cui eseguire il test
   * @throws SearchUtilsException_Exception
   *           in caso di errore nella ricerca del documento/container/utente
   * @throws CheckoutPDMLinkException_Exception
   *           in caso di errore nell'esecuzione del checkout
   * 
   */
  private void test(PDMLinkService port, String nomeModello) throws SearchUtilsException_Exception, CheckoutPDMLinkException_Exception {
    boolean check = port.isCheckedOut(nomeModello);
    System.out.println(nomeModello + " in checkout? " + check);
    assertFalse(check);
    // lo metto in checkout
    port.eseguiCheckout(nomeModello, "ws_test", "Administrator");
    // controllo che l'oggetto sia in checkout
    check = port.isCheckedOut(nomeModello);
    System.out.println(nomeModello + " in checkout? " + check);
    assertTrue(check);
    // faccio undocheckout
    port.eseguiUndoCheckout(nomeModello, "ws_test", "Administrator");
    // controllo che l'oggetto non sia in checkout
    check = port.isCheckedOut(nomeModello);
    System.out.println(nomeModello + " in checkout? " + check);
    assertFalse(check);
  }
}
